﻿namespace KP_RES
{
    partial class Frm_BanHang_Mavach
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_BanHang_Mavach));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            this.pn_bigparent = new DevExpress.XtraEditors.PanelControl();
            this.pn_hanghoa = new DevExpress.XtraEditors.PanelControl();
            this.pal_banhang = new DevExpress.XtraEditors.PanelControl();
            this.pal_banhang1 = new DevExpress.XtraEditors.PanelControl();
            this.pnctrlParent = new DevExpress.XtraEditors.XtraScrollableControl();
            this.palSoCot = new DevExpress.XtraEditors.PanelControl();
            this.palCot = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btn_9Cot = new DevExpress.XtraEditors.SimpleButton();
            this.btn_5cot = new DevExpress.XtraEditors.SimpleButton();
            this.btn_7cot = new DevExpress.XtraEditors.SimpleButton();
            this.palPhanTrang = new DevExpress.XtraEditors.PanelControl();
            this.btn_Last = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Prev = new DevExpress.XtraEditors.SimpleButton();
            this.btn_First = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Next = new DevExpress.XtraEditors.SimpleButton();
            this.lblPageMon = new DevExpress.XtraEditors.LabelControl();
            this.txtSarech = new DevExpress.XtraEditors.TextEdit();
            this.palPhanNganh = new DevExpress.XtraEditors.PanelControl();
            this.txtScrolNhom = new DevExpress.XtraEditors.SimpleButton();
            this.pnCtrlNgNhPhNhom = new DevExpress.XtraEditors.PanelControl();
            this.gr_ChonNhom = new DevExpress.XtraEditors.PanelControl();
            this.Scrol_ShowNhom = new DevExpress.XtraEditors.XtraScrollableControl();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.btn_NextNhom = new DevExpress.XtraEditors.SimpleButton();
            this.lblSoTrangNhom = new DevExpress.XtraEditors.LabelControl();
            this.btnBackNhom = new DevExpress.XtraEditors.SimpleButton();
            this.pn_banphim = new DevExpress.XtraEditors.PanelControl();
            this.pn_banphim_so = new DevExpress.XtraEditors.PanelControl();
            this.btnDatCoc = new DevExpress.XtraEditors.SimpleButton();
            this.btnLeft = new DevExpress.XtraEditors.SimpleButton();
            this.btnRight = new DevExpress.XtraEditors.SimpleButton();
            this.btn_dot = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Payments = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ABC = new DevExpress.XtraEditors.SimpleButton();
            this.btn_000 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_00 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_0 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_enter = new DevExpress.XtraEditors.SimpleButton();
            this.btn_delete = new DevExpress.XtraEditors.SimpleButton();
            this.btn_6 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_9 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_8 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_7 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_5 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_4 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_3 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_2 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ESC = new DevExpress.XtraEditors.SimpleButton();
            this.pn_banphim_chu = new DevExpress.XtraEditors.PanelControl();
            this.btn_backchar = new DevExpress.XtraEditors.SimpleButton();
            this.btn_PaymentsChar = new DevExpress.XtraEditors.SimpleButton();
            this.btn_OKChar = new DevExpress.XtraEditors.SimpleButton();
            this.btn_L = new DevExpress.XtraEditors.SimpleButton();
            this.btn_K = new DevExpress.XtraEditors.SimpleButton();
            this.btn_U = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Y = new DevExpress.XtraEditors.SimpleButton();
            this.btn_123 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_M = new DevExpress.XtraEditors.SimpleButton();
            this.btn_J = new DevExpress.XtraEditors.SimpleButton();
            this.btn_I = new DevExpress.XtraEditors.SimpleButton();
            this.btn_T = new DevExpress.XtraEditors.SimpleButton();
            this.btn_DeleteChar = new DevExpress.XtraEditors.SimpleButton();
            this.btn_N = new DevExpress.XtraEditors.SimpleButton();
            this.btn_H = new DevExpress.XtraEditors.SimpleButton();
            this.btn_O = new DevExpress.XtraEditors.SimpleButton();
            this.btn_R = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ESCChar = new DevExpress.XtraEditors.SimpleButton();
            this.btn_B = new DevExpress.XtraEditors.SimpleButton();
            this.btn_G = new DevExpress.XtraEditors.SimpleButton();
            this.btn_P = new DevExpress.XtraEditors.SimpleButton();
            this.btn_E = new DevExpress.XtraEditors.SimpleButton();
            this.btn_X = new DevExpress.XtraEditors.SimpleButton();
            this.btn_V = new DevExpress.XtraEditors.SimpleButton();
            this.btn_F = new DevExpress.XtraEditors.SimpleButton();
            this.btn_A = new DevExpress.XtraEditors.SimpleButton();
            this.btn_W = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Z = new DevExpress.XtraEditors.SimpleButton();
            this.btn_C = new DevExpress.XtraEditors.SimpleButton();
            this.btn_D = new DevExpress.XtraEditors.SimpleButton();
            this.btn_S = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Q = new DevExpress.XtraEditors.SimpleButton();
            this.pnViewProduct = new DevExpress.XtraEditors.PanelControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.grCtrlDSHangMua = new DevExpress.XtraGrid.GridControl();
            this.gVDSHangMua = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_VACH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIABAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CHIETKHAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Thanh_Tien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_BEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IS_COMBO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IS_INBEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IS_KHUYENMAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IS_QUATANG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.btn_Add = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.btn_Exc = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.btn_Del = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.TrungHere = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.pnUpDow = new DevExpress.XtraEditors.PanelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.lb_RowsCount = new DevExpress.XtraEditors.LabelControl();
            this.lb_STT = new DevExpress.XtraEditors.LabelControl();
            this.btn_up = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Dow = new DevExpress.XtraEditors.SimpleButton();
            this.btn_DowLast = new DevExpress.XtraEditors.SimpleButton();
            this.btn_UpFirst = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Dong = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.pn_WapperContent = new DevExpress.XtraEditors.PanelControl();
            this.pn_Luoi = new DevExpress.XtraEditors.PanelControl();
            this.pnTachban = new DevExpress.XtraEditors.PanelControl();
            this.pn_GroupTable = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btn_OkThrough = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ToLeftAll = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ToRightAll = new DevExpress.XtraEditors.SimpleButton();
            this.btn_UpDivide = new DevExpress.XtraEditors.SimpleButton();
            this.btn_DowDivide = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ESCThroughtTable = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ToRight = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Toleft = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.gr_divide_Table_1 = new DevExpress.XtraGrid.GridControl();
            this.gv_divide_Table_1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._SSTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this._TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this._SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this._GIABAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this._THANHTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this._MA_HOADON = new DevExpress.XtraGrid.Columns.GridColumn();
            this._MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this._THUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this._CHIETKHAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this._IS_COMBO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pn_TableTow = new DevExpress.XtraEditors.PanelControl();
            this.gr_divide_Table_2 = new DevExpress.XtraGrid.GridControl();
            this.gv_divide_Table_2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.@__STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__GIABAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__THANHTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__MA_HOADON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__THUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__CHIETKHAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__IS_COMBO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnInlaihoadon = new DevExpress.XtraEditors.PanelControl();
            this.panelControl53 = new DevExpress.XtraEditors.PanelControl();
            this.gr_billPaymented = new DevExpress.XtraGrid.GridControl();
            this.gvBillPaymented = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SSTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HOADON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYTAO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_KHUVUC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_BAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.XEM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.IN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.HUY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.LAYLAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.txtMaHoaDon = new DevExpress.XtraEditors.TextEdit();
            this.btnHuy = new DevExpress.XtraEditors.SimpleButton();
            this.lb_maKH = new DevExpress.XtraEditors.LabelControl();
            this.panelControl52 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.lbTongSoRow = new DevExpress.XtraEditors.LabelControl();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.btn_upbill = new DevExpress.XtraEditors.SimpleButton();
            this.btnDowBill = new DevExpress.XtraEditors.SimpleButton();
            this.btnDowLastBill = new DevExpress.XtraEditors.SimpleButton();
            this.btn_UpTopBill = new DevExpress.XtraEditors.SimpleButton();
            this.pnWapperDeletePlusBut = new DevExpress.XtraEditors.PanelControl();
            this.btnComboOption = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteAll = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Cong = new DevExpress.XtraEditors.SimpleButton();
            this.btn_xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Tru = new DevExpress.XtraEditors.SimpleButton();
            this.pn_Banhang = new DevExpress.XtraEditors.PanelControl();
            this.pnBanHang = new DevExpress.XtraEditors.PanelControl();
            this.btnLaytrongluong = new DevExpress.XtraEditors.SimpleButton();
            this.lbdiscount = new DevExpress.XtraEditors.LabelControl();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.txtdiscount = new DevExpress.XtraEditors.TextEdit();
            this.lbTienTruocThue = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelThueVAT = new DevExpress.XtraEditors.LabelControl();
            this.label_GiamGia = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txt_soluong = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.lb_tongcong = new DevExpress.XtraEditors.LabelControl();
            this.txt_Mahang = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.pnMaHoaDonTraHang = new DevExpress.XtraEditors.PanelControl();
            this.btnTra = new DevExpress.XtraEditors.SimpleButton();
            this.txtMaHoaDonTra = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.pnNghiepVu_ThuNgan = new DevExpress.XtraEditors.PanelControl();
            this.pnDacthu = new DevExpress.XtraEditors.PanelControl();
            this.btnChonbanggia = new DevExpress.XtraEditors.SimpleButton();
            this.pnThuNgan = new DevExpress.XtraEditors.PanelControl();
            this.btnTraHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuuHDDoi = new DevExpress.XtraEditors.SimpleButton();
            this.btnLayHDDoi = new DevExpress.XtraEditors.SimpleButton();
            this.btn_InLaiHoaDon = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoKet = new DevExpress.XtraEditors.SimpleButton();
            this.btnKetKa = new DevExpress.XtraEditors.SimpleButton();
            this.btn_logout = new DevExpress.XtraEditors.SimpleButton();
            this.btn_HuyHoaDon = new DevExpress.XtraEditors.SimpleButton();
            this.btnGiaBan2 = new DevExpress.XtraEditors.SimpleButton();
            this.pnNghiepvu = new DevExpress.XtraEditors.PanelControl();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnBaoCaoTraHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnTraCuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnSuaGia = new DevExpress.XtraEditors.SimpleButton();
            this.btnPhatThuong = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemKHTT = new DevExpress.XtraEditors.SimpleButton();
            this.btnXemTonKho = new DevExpress.XtraEditors.SimpleButton();
            this.btnBaoCaoBanHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnInVeOnline = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.btn_Datthu = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Nghiepvu = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ThuNgan = new DevExpress.XtraEditors.SimpleButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pnHeader = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.lb_Quay = new DevExpress.XtraEditors.LabelControl();
            this.lb_Kho = new DevExpress.XtraEditors.LabelControl();
            this.lbgiay = new DevExpress.XtraEditors.LabelControl();
            this.lbCaBan = new DevExpress.XtraEditors.LabelControl();
            this.lbGio = new DevExpress.XtraEditors.LabelControl();
            this.lb_NhanVien = new DevExpress.XtraEditors.LabelControl();
            this.lbtimeby = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bar5 = new DevExpress.XtraBars.Bar();
            ((System.ComponentModel.ISupportInitialize)(this.pn_bigparent)).BeginInit();
            this.pn_bigparent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_hanghoa)).BeginInit();
            this.pn_hanghoa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_banhang)).BeginInit();
            this.pal_banhang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_banhang1)).BeginInit();
            this.pal_banhang1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palSoCot)).BeginInit();
            this.palSoCot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palCot)).BeginInit();
            this.palCot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palPhanTrang)).BeginInit();
            this.palPhanTrang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSarech.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.palPhanNganh)).BeginInit();
            this.palPhanNganh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnCtrlNgNhPhNhom)).BeginInit();
            this.pnCtrlNgNhPhNhom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_ChonNhom)).BeginInit();
            this.gr_ChonNhom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_banphim)).BeginInit();
            this.pn_banphim.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_banphim_so)).BeginInit();
            this.pn_banphim_so.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_banphim_chu)).BeginInit();
            this.pn_banphim_chu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnViewProduct)).BeginInit();
            this.pnViewProduct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grCtrlDSHangMua)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gVDSHangMua)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Add)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Exc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Del)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrungHere)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDow)).BeginInit();
            this.pnUpDow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_WapperContent)).BeginInit();
            this.pn_WapperContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_Luoi)).BeginInit();
            this.pn_Luoi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnTachban)).BeginInit();
            this.pnTachban.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_GroupTable)).BeginInit();
            this.pn_GroupTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            this.panelControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_divide_Table_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_divide_Table_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_TableTow)).BeginInit();
            this.pn_TableTow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_divide_Table_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_divide_Table_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnInlaihoadon)).BeginInit();
            this.pnInlaihoadon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl53)).BeginInit();
            this.panelControl53.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_billPaymented)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBillPaymented)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHoaDon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl52)).BeginInit();
            this.panelControl52.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnWapperDeletePlusBut)).BeginInit();
            this.pnWapperDeletePlusBut.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_Banhang)).BeginInit();
            this.pn_Banhang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnBanHang)).BeginInit();
            this.pnBanHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtdiscount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soluong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Mahang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnMaHoaDonTraHang)).BeginInit();
            this.pnMaHoaDonTraHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHoaDonTra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnNghiepVu_ThuNgan)).BeginInit();
            this.pnNghiepVu_ThuNgan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnDacthu)).BeginInit();
            this.pnDacthu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnThuNgan)).BeginInit();
            this.pnThuNgan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnNghiepvu)).BeginInit();
            this.pnNghiepvu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnHeader)).BeginInit();
            this.pnHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pn_bigparent
            // 
            this.pn_bigparent.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pn_bigparent.Controls.Add(this.pn_hanghoa);
            this.pn_bigparent.Controls.Add(this.pn_banphim);
            this.pn_bigparent.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn_bigparent.Location = new System.Drawing.Point(625, 2);
            this.pn_bigparent.Name = "pn_bigparent";
            this.pn_bigparent.Size = new System.Drawing.Size(409, 694);
            this.pn_bigparent.TabIndex = 5;
            // 
            // pn_hanghoa
            // 
            this.pn_hanghoa.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pn_hanghoa.Controls.Add(this.pal_banhang);
            this.pn_hanghoa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_hanghoa.Location = new System.Drawing.Point(0, 0);
            this.pn_hanghoa.Name = "pn_hanghoa";
            this.pn_hanghoa.Size = new System.Drawing.Size(409, 438);
            this.pn_hanghoa.TabIndex = 125;
            // 
            // pal_banhang
            // 
            this.pal_banhang.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_banhang.Controls.Add(this.pal_banhang1);
            this.pal_banhang.Controls.Add(this.pnCtrlNgNhPhNhom);
            this.pal_banhang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_banhang.Location = new System.Drawing.Point(0, 0);
            this.pal_banhang.Name = "pal_banhang";
            this.pal_banhang.Size = new System.Drawing.Size(409, 438);
            this.pal_banhang.TabIndex = 124;
            // 
            // pal_banhang1
            // 
            this.pal_banhang1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_banhang1.Controls.Add(this.pnctrlParent);
            this.pal_banhang1.Controls.Add(this.palSoCot);
            this.pal_banhang1.Controls.Add(this.palPhanNganh);
            this.pal_banhang1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_banhang1.Location = new System.Drawing.Point(0, 0);
            this.pal_banhang1.Name = "pal_banhang1";
            this.pal_banhang1.Size = new System.Drawing.Size(409, 438);
            this.pal_banhang1.TabIndex = 124;
            // 
            // pnctrlParent
            // 
            this.pnctrlParent.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pnctrlParent.Appearance.Options.UseBackColor = true;
            this.pnctrlParent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnctrlParent.Location = new System.Drawing.Point(0, 32);
            this.pnctrlParent.Margin = new System.Windows.Forms.Padding(0);
            this.pnctrlParent.Name = "pnctrlParent";
            this.pnctrlParent.Size = new System.Drawing.Size(409, 344);
            this.pnctrlParent.TabIndex = 6;
            // 
            // palSoCot
            // 
            this.palSoCot.Controls.Add(this.palCot);
            this.palSoCot.Controls.Add(this.txtSarech);
            this.palSoCot.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palSoCot.Location = new System.Drawing.Point(0, 376);
            this.palSoCot.Name = "palSoCot";
            this.palSoCot.Size = new System.Drawing.Size(409, 62);
            this.palSoCot.TabIndex = 5;
            // 
            // palCot
            // 
            this.palCot.Controls.Add(this.panelControl4);
            this.palCot.Controls.Add(this.palPhanTrang);
            this.palCot.Dock = System.Windows.Forms.DockStyle.Top;
            this.palCot.Location = new System.Drawing.Point(2, 2);
            this.palCot.Name = "palCot";
            this.palCot.Size = new System.Drawing.Size(405, 35);
            this.palCot.TabIndex = 15;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Controls.Add(this.btn_9Cot);
            this.panelControl4.Controls.Add(this.btn_5cot);
            this.panelControl4.Controls.Add(this.btn_7cot);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl4.Location = new System.Drawing.Point(2, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(149, 31);
            this.panelControl4.TabIndex = 16;
            // 
            // btn_9Cot
            // 
            this.btn_9Cot.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn_9Cot.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_9Cot.Appearance.Options.UseFont = true;
            this.btn_9Cot.Location = new System.Drawing.Point(103, 1);
            this.btn_9Cot.Name = "btn_9Cot";
            this.btn_9Cot.Size = new System.Drawing.Size(45, 28);
            this.btn_9Cot.TabIndex = 4;
            this.btn_9Cot.Text = "8";
            this.btn_9Cot.Click += new System.EventHandler(this.btn_9Cot_Click);
            // 
            // btn_5cot
            // 
            this.btn_5cot.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn_5cot.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_5cot.Appearance.Options.UseFont = true;
            this.btn_5cot.Location = new System.Drawing.Point(1, 1);
            this.btn_5cot.Name = "btn_5cot";
            this.btn_5cot.Size = new System.Drawing.Size(45, 28);
            this.btn_5cot.TabIndex = 3;
            this.btn_5cot.Text = "4";
            this.btn_5cot.Click += new System.EventHandler(this.btn_5cot_Click);
            // 
            // btn_7cot
            // 
            this.btn_7cot.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn_7cot.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_7cot.Appearance.Options.UseFont = true;
            this.btn_7cot.Location = new System.Drawing.Point(52, 1);
            this.btn_7cot.Name = "btn_7cot";
            this.btn_7cot.Size = new System.Drawing.Size(45, 28);
            this.btn_7cot.TabIndex = 4;
            this.btn_7cot.Text = "6";
            this.btn_7cot.Click += new System.EventHandler(this.btn_7cot_Click);
            // 
            // palPhanTrang
            // 
            this.palPhanTrang.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palPhanTrang.Controls.Add(this.btn_Last);
            this.palPhanTrang.Controls.Add(this.btn_Prev);
            this.palPhanTrang.Controls.Add(this.btn_First);
            this.palPhanTrang.Controls.Add(this.btn_Next);
            this.palPhanTrang.Controls.Add(this.lblPageMon);
            this.palPhanTrang.Dock = System.Windows.Forms.DockStyle.Right;
            this.palPhanTrang.Location = new System.Drawing.Point(161, 2);
            this.palPhanTrang.Name = "palPhanTrang";
            this.palPhanTrang.Size = new System.Drawing.Size(242, 31);
            this.palPhanTrang.TabIndex = 14;
            // 
            // btn_Last
            // 
            this.btn_Last.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn_Last.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btn_Last.Location = new System.Drawing.Point(8, 1);
            this.btn_Last.Name = "btn_Last";
            this.btn_Last.Size = new System.Drawing.Size(45, 28);
            this.btn_Last.TabIndex = 2;
            this.btn_Last.Text = ">>";
            this.btn_Last.Click += new System.EventHandler(this.btn_Last_Click);
            // 
            // btn_Prev
            // 
            this.btn_Prev.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn_Prev.Location = new System.Drawing.Point(143, 1);
            this.btn_Prev.Name = "btn_Prev";
            this.btn_Prev.Size = new System.Drawing.Size(45, 28);
            this.btn_Prev.TabIndex = 4;
            this.btn_Prev.Text = "<";
            this.btn_Prev.Click += new System.EventHandler(this.btn_Prev_Click);
            // 
            // btn_First
            // 
            this.btn_First.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn_First.Location = new System.Drawing.Point(194, 1);
            this.btn_First.Name = "btn_First";
            this.btn_First.Size = new System.Drawing.Size(45, 28);
            this.btn_First.TabIndex = 5;
            this.btn_First.Text = "<<";
            this.btn_First.Click += new System.EventHandler(this.btn_First_Click);
            // 
            // btn_Next
            // 
            this.btn_Next.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn_Next.Location = new System.Drawing.Point(59, 1);
            this.btn_Next.Name = "btn_Next";
            this.btn_Next.Size = new System.Drawing.Size(45, 28);
            this.btn_Next.TabIndex = 3;
            this.btn_Next.Text = ">";
            this.btn_Next.Click += new System.EventHandler(this.btn_Next_Click);
            // 
            // lblPageMon
            // 
            this.lblPageMon.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblPageMon.Location = new System.Drawing.Point(117, 10);
            this.lblPageMon.Name = "lblPageMon";
            this.lblPageMon.Size = new System.Drawing.Size(16, 13);
            this.lblPageMon.TabIndex = 2;
            this.lblPageMon.Text = "0/0";
            // 
            // txtSarech
            // 
            this.txtSarech.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtSarech.EditValue = "Nhập tên hàng hóa...";
            this.txtSarech.Location = new System.Drawing.Point(2, 34);
            this.txtSarech.Name = "txtSarech";
            this.txtSarech.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtSarech.Properties.Appearance.Options.UseFont = true;
            this.txtSarech.Size = new System.Drawing.Size(405, 26);
            this.txtSarech.TabIndex = 2;
            this.txtSarech.TextChanged += new System.EventHandler(this.txtSarech_TextChanged);
            this.txtSarech.Click += new System.EventHandler(this.txtSarech_Click);
            this.txtSarech.Leave += new System.EventHandler(this.txtSarech_Leave);
            // 
            // palPhanNganh
            // 
            this.palPhanNganh.Controls.Add(this.txtScrolNhom);
            this.palPhanNganh.Dock = System.Windows.Forms.DockStyle.Top;
            this.palPhanNganh.Location = new System.Drawing.Point(0, 0);
            this.palPhanNganh.Name = "palPhanNganh";
            this.palPhanNganh.Size = new System.Drawing.Size(409, 32);
            this.palPhanNganh.TabIndex = 6;
            // 
            // txtScrolNhom
            // 
            this.txtScrolNhom.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtScrolNhom.Appearance.Options.UseFont = true;
            this.txtScrolNhom.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtScrolNhom.Location = new System.Drawing.Point(2, 2);
            this.txtScrolNhom.Name = "txtScrolNhom";
            this.txtScrolNhom.Size = new System.Drawing.Size(151, 28);
            this.txtScrolNhom.TabIndex = 4;
            this.txtScrolNhom.Text = "Chọn nhóm hàng";
            this.txtScrolNhom.Click += new System.EventHandler(this.txtScrolNhom_Click);
            // 
            // pnCtrlNgNhPhNhom
            // 
            this.pnCtrlNgNhPhNhom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnCtrlNgNhPhNhom.Controls.Add(this.gr_ChonNhom);
            this.pnCtrlNgNhPhNhom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnCtrlNgNhPhNhom.Location = new System.Drawing.Point(0, 0);
            this.pnCtrlNgNhPhNhom.Name = "pnCtrlNgNhPhNhom";
            this.pnCtrlNgNhPhNhom.Size = new System.Drawing.Size(409, 438);
            this.pnCtrlNgNhPhNhom.TabIndex = 10;
            // 
            // gr_ChonNhom
            // 
            this.gr_ChonNhom.Controls.Add(this.Scrol_ShowNhom);
            this.gr_ChonNhom.Controls.Add(this.panelControl10);
            this.gr_ChonNhom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gr_ChonNhom.Location = new System.Drawing.Point(0, 0);
            this.gr_ChonNhom.Name = "gr_ChonNhom";
            this.gr_ChonNhom.Size = new System.Drawing.Size(409, 438);
            this.gr_ChonNhom.TabIndex = 13;
            // 
            // Scrol_ShowNhom
            // 
            this.Scrol_ShowNhom.AutoScroll = false;
            this.Scrol_ShowNhom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Scrol_ShowNhom.Location = new System.Drawing.Point(2, 32);
            this.Scrol_ShowNhom.Name = "Scrol_ShowNhom";
            this.Scrol_ShowNhom.Size = new System.Drawing.Size(405, 404);
            this.Scrol_ShowNhom.TabIndex = 1;
            // 
            // panelControl10
            // 
            this.panelControl10.Controls.Add(this.btn_NextNhom);
            this.panelControl10.Controls.Add(this.lblSoTrangNhom);
            this.panelControl10.Controls.Add(this.btnBackNhom);
            this.panelControl10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl10.Location = new System.Drawing.Point(2, 2);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(405, 30);
            this.panelControl10.TabIndex = 8;
            // 
            // btn_NextNhom
            // 
            this.btn_NextNhom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btn_NextNhom.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btn_NextNhom.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.btn_NextNhom.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.btn_NextNhom.Appearance.Options.UseBackColor = true;
            this.btn_NextNhom.Appearance.Options.UseBorderColor = true;
            this.btn_NextNhom.Image = ((System.Drawing.Image)(resources.GetObject("btn_NextNhom.Image")));
            this.btn_NextNhom.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_NextNhom.Location = new System.Drawing.Point(198, 0);
            this.btn_NextNhom.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.btn_NextNhom.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btn_NextNhom.Name = "btn_NextNhom";
            this.btn_NextNhom.Size = new System.Drawing.Size(84, 28);
            this.btn_NextNhom.TabIndex = 6;
            this.btn_NextNhom.ToolTip = "Next";
            this.btn_NextNhom.Click += new System.EventHandler(this.btn_NextNhom_Click);
            // 
            // lblSoTrangNhom
            // 
            this.lblSoTrangNhom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lblSoTrangNhom.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblSoTrangNhom.Location = new System.Drawing.Point(161, 10);
            this.lblSoTrangNhom.Name = "lblSoTrangNhom";
            this.lblSoTrangNhom.Size = new System.Drawing.Size(20, 13);
            this.lblSoTrangNhom.TabIndex = 7;
            this.lblSoTrangNhom.Text = "0/0";
            // 
            // btnBackNhom
            // 
            this.btnBackNhom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnBackNhom.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnBackNhom.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.btnBackNhom.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.btnBackNhom.Appearance.Options.UseBackColor = true;
            this.btnBackNhom.Appearance.Options.UseBorderColor = true;
            this.btnBackNhom.Image = ((System.Drawing.Image)(resources.GetObject("btnBackNhom.Image")));
            this.btnBackNhom.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBackNhom.Location = new System.Drawing.Point(63, 0);
            this.btnBackNhom.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.btnBackNhom.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnBackNhom.Name = "btnBackNhom";
            this.btnBackNhom.Size = new System.Drawing.Size(84, 28);
            this.btnBackNhom.TabIndex = 5;
            this.btnBackNhom.ToolTip = "Back";
            this.btnBackNhom.Click += new System.EventHandler(this.btnBackNhom_Click);
            // 
            // pn_banphim
            // 
            this.pn_banphim.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pn_banphim.Controls.Add(this.pn_banphim_so);
            this.pn_banphim.Controls.Add(this.pn_banphim_chu);
            this.pn_banphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pn_banphim.Location = new System.Drawing.Point(0, 438);
            this.pn_banphim.Name = "pn_banphim";
            this.pn_banphim.Size = new System.Drawing.Size(409, 256);
            this.pn_banphim.TabIndex = 0;
            // 
            // pn_banphim_so
            // 
            this.pn_banphim_so.Controls.Add(this.btnDatCoc);
            this.pn_banphim_so.Controls.Add(this.btnLeft);
            this.pn_banphim_so.Controls.Add(this.btnRight);
            this.pn_banphim_so.Controls.Add(this.btn_dot);
            this.pn_banphim_so.Controls.Add(this.btn_Payments);
            this.pn_banphim_so.Controls.Add(this.btn_ABC);
            this.pn_banphim_so.Controls.Add(this.btn_000);
            this.pn_banphim_so.Controls.Add(this.btn_00);
            this.pn_banphim_so.Controls.Add(this.btn_0);
            this.pn_banphim_so.Controls.Add(this.btn_enter);
            this.pn_banphim_so.Controls.Add(this.btn_delete);
            this.pn_banphim_so.Controls.Add(this.btn_6);
            this.pn_banphim_so.Controls.Add(this.btn_9);
            this.pn_banphim_so.Controls.Add(this.btn_8);
            this.pn_banphim_so.Controls.Add(this.btn_7);
            this.pn_banphim_so.Controls.Add(this.btn_5);
            this.pn_banphim_so.Controls.Add(this.btn_4);
            this.pn_banphim_so.Controls.Add(this.btn_3);
            this.pn_banphim_so.Controls.Add(this.btn_2);
            this.pn_banphim_so.Controls.Add(this.btn_1);
            this.pn_banphim_so.Controls.Add(this.btn_ESC);
            this.pn_banphim_so.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_banphim_so.Location = new System.Drawing.Point(0, 0);
            this.pn_banphim_so.Name = "pn_banphim_so";
            this.pn_banphim_so.Size = new System.Drawing.Size(409, 256);
            this.pn_banphim_so.TabIndex = 4;
            // 
            // btnDatCoc
            // 
            this.btnDatCoc.AllowFocus = false;
            this.btnDatCoc.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDatCoc.Appearance.Options.UseFont = true;
            this.btnDatCoc.Location = new System.Drawing.Point(269, 128);
            this.btnDatCoc.Name = "btnDatCoc";
            this.btnDatCoc.Size = new System.Drawing.Size(126, 55);
            this.btnDatCoc.TabIndex = 89;
            this.btnDatCoc.Text = "Đặt cọc";
            this.btnDatCoc.Visible = false;
            // 
            // btnLeft
            // 
            this.btnLeft.AllowFocus = false;
            this.btnLeft.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLeft.Appearance.Options.UseFont = true;
            this.btnLeft.Image = ((System.Drawing.Image)(resources.GetObject("btnLeft.Image")));
            this.btnLeft.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLeft.Location = new System.Drawing.Point(269, 4);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(60, 55);
            this.btnLeft.TabIndex = 88;
            this.btnLeft.Text = "<---";
            this.btnLeft.Click += new System.EventHandler(this.btnLeft_Click);
            // 
            // btnRight
            // 
            this.btnRight.AllowFocus = false;
            this.btnRight.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRight.Appearance.Options.UseFont = true;
            this.btnRight.Image = ((System.Drawing.Image)(resources.GetObject("btnRight.Image")));
            this.btnRight.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnRight.Location = new System.Drawing.Point(335, 4);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(60, 55);
            this.btnRight.TabIndex = 87;
            this.btnRight.Text = "--->";
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            // 
            // btn_dot
            // 
            this.btn_dot.AllowFocus = false;
            this.btn_dot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_dot.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_dot.Appearance.Options.UseFont = true;
            this.btn_dot.Location = new System.Drawing.Point(71, 190);
            this.btn_dot.Name = "btn_dot";
            this.btn_dot.Size = new System.Drawing.Size(60, 55);
            this.btn_dot.TabIndex = 30;
            this.btn_dot.Text = ".";
            this.btn_dot.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_Payments
            // 
            this.btn_Payments.AllowFocus = false;
            this.btn_Payments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Payments.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Payments.Appearance.Options.UseFont = true;
            this.btn_Payments.Image = ((System.Drawing.Image)(resources.GetObject("btn_Payments.Image")));
            this.btn_Payments.Location = new System.Drawing.Point(269, 190);
            this.btn_Payments.Name = "btn_Payments";
            this.btn_Payments.Size = new System.Drawing.Size(126, 55);
            this.btn_Payments.TabIndex = 29;
            this.btn_Payments.Text = "Thanh Toán\r\n[ F1 ]";
            this.btn_Payments.Click += new System.EventHandler(this.btn_Payments_Click);
            // 
            // btn_ABC
            // 
            this.btn_ABC.Appearance.BorderColor = System.Drawing.Color.Black;
            this.btn_ABC.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ABC.Appearance.Options.UseBorderColor = true;
            this.btn_ABC.Appearance.Options.UseFont = true;
            this.btn_ABC.Location = new System.Drawing.Point(5, 190);
            this.btn_ABC.Name = "btn_ABC";
            this.btn_ABC.Size = new System.Drawing.Size(60, 55);
            this.btn_ABC.TabIndex = 28;
            this.btn_ABC.Text = "ABC";
            this.btn_ABC.Click += new System.EventHandler(this.btn_ABC_Click);
            // 
            // btn_000
            // 
            this.btn_000.AllowFocus = false;
            this.btn_000.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_000.Appearance.Options.UseFont = true;
            this.btn_000.Location = new System.Drawing.Point(203, 128);
            this.btn_000.Name = "btn_000";
            this.btn_000.Size = new System.Drawing.Size(60, 55);
            this.btn_000.TabIndex = 27;
            this.btn_000.Text = "000";
            this.btn_000.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_00
            // 
            this.btn_00.AllowFocus = false;
            this.btn_00.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_00.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_00.Appearance.Options.UseFont = true;
            this.btn_00.Location = new System.Drawing.Point(203, 190);
            this.btn_00.Name = "btn_00";
            this.btn_00.Size = new System.Drawing.Size(60, 55);
            this.btn_00.TabIndex = 26;
            this.btn_00.Text = "00";
            this.btn_00.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_0
            // 
            this.btn_0.AllowFocus = false;
            this.btn_0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_0.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_0.Appearance.Options.UseFont = true;
            this.btn_0.Location = new System.Drawing.Point(137, 190);
            this.btn_0.Name = "btn_0";
            this.btn_0.Size = new System.Drawing.Size(60, 55);
            this.btn_0.TabIndex = 23;
            this.btn_0.Text = "0";
            this.btn_0.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_enter
            // 
            this.btn_enter.AllowFocus = false;
            this.btn_enter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_enter.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_enter.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btn_enter.Appearance.Options.UseFont = true;
            this.btn_enter.Appearance.Options.UseForeColor = true;
            this.btn_enter.Location = new System.Drawing.Point(269, 66);
            this.btn_enter.Name = "btn_enter";
            this.btn_enter.Size = new System.Drawing.Size(126, 55);
            this.btn_enter.TabIndex = 0;
            this.btn_enter.Text = "OK";
            this.btn_enter.Click += new System.EventHandler(this.btn_enter_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.AllowFocus = false;
            this.btn_delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_delete.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_delete.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btn_delete.Appearance.Options.UseFont = true;
            this.btn_delete.Appearance.Options.UseForeColor = true;
            this.btn_delete.Location = new System.Drawing.Point(203, 66);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(60, 55);
            this.btn_delete.TabIndex = 24;
            this.btn_delete.Text = "Del";
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_6
            // 
            this.btn_6.AllowFocus = false;
            this.btn_6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_6.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_6.Appearance.Options.UseFont = true;
            this.btn_6.Location = new System.Drawing.Point(137, 66);
            this.btn_6.Name = "btn_6";
            this.btn_6.Size = new System.Drawing.Size(60, 55);
            this.btn_6.TabIndex = 19;
            this.btn_6.Text = "6";
            this.btn_6.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_9
            // 
            this.btn_9.AllowFocus = false;
            this.btn_9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_9.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_9.Appearance.Options.UseFont = true;
            this.btn_9.Location = new System.Drawing.Point(137, 4);
            this.btn_9.Name = "btn_9";
            this.btn_9.Size = new System.Drawing.Size(60, 55);
            this.btn_9.TabIndex = 22;
            this.btn_9.Text = "9";
            this.btn_9.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_8
            // 
            this.btn_8.AllowFocus = false;
            this.btn_8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_8.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_8.Appearance.Options.UseFont = true;
            this.btn_8.Location = new System.Drawing.Point(71, 4);
            this.btn_8.Name = "btn_8";
            this.btn_8.Size = new System.Drawing.Size(60, 55);
            this.btn_8.TabIndex = 21;
            this.btn_8.Text = "8";
            this.btn_8.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_7
            // 
            this.btn_7.AllowFocus = false;
            this.btn_7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_7.Appearance.Options.UseFont = true;
            this.btn_7.Location = new System.Drawing.Point(5, 4);
            this.btn_7.Name = "btn_7";
            this.btn_7.Size = new System.Drawing.Size(60, 55);
            this.btn_7.TabIndex = 20;
            this.btn_7.Text = "7";
            this.btn_7.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_5
            // 
            this.btn_5.AllowFocus = false;
            this.btn_5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_5.Appearance.Options.UseFont = true;
            this.btn_5.Location = new System.Drawing.Point(71, 66);
            this.btn_5.Name = "btn_5";
            this.btn_5.Size = new System.Drawing.Size(60, 55);
            this.btn_5.TabIndex = 15;
            this.btn_5.Text = "5";
            this.btn_5.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_4
            // 
            this.btn_4.AllowFocus = false;
            this.btn_4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_4.Appearance.Options.UseFont = true;
            this.btn_4.Location = new System.Drawing.Point(5, 66);
            this.btn_4.Name = "btn_4";
            this.btn_4.Size = new System.Drawing.Size(60, 55);
            this.btn_4.TabIndex = 14;
            this.btn_4.Text = "4";
            this.btn_4.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_3
            // 
            this.btn_3.AllowFocus = false;
            this.btn_3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_3.Appearance.Options.UseFont = true;
            this.btn_3.Location = new System.Drawing.Point(137, 128);
            this.btn_3.Name = "btn_3";
            this.btn_3.Size = new System.Drawing.Size(60, 55);
            this.btn_3.TabIndex = 13;
            this.btn_3.Text = "3";
            this.btn_3.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_2
            // 
            this.btn_2.AllowFocus = false;
            this.btn_2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_2.Appearance.Options.UseFont = true;
            this.btn_2.Location = new System.Drawing.Point(71, 128);
            this.btn_2.Name = "btn_2";
            this.btn_2.Size = new System.Drawing.Size(60, 55);
            this.btn_2.TabIndex = 12;
            this.btn_2.Text = "2";
            this.btn_2.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_1
            // 
            this.btn_1.Appearance.BorderColor = System.Drawing.Color.Black;
            this.btn_1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_1.Appearance.Options.UseBorderColor = true;
            this.btn_1.Appearance.Options.UseFont = true;
            this.btn_1.Location = new System.Drawing.Point(5, 128);
            this.btn_1.Name = "btn_1";
            this.btn_1.Size = new System.Drawing.Size(60, 55);
            this.btn_1.TabIndex = 11;
            this.btn_1.Text = "1";
            this.btn_1.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_ESC
            // 
            this.btn_ESC.AllowFocus = false;
            this.btn_ESC.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ESC.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btn_ESC.Appearance.Options.UseFont = true;
            this.btn_ESC.Appearance.Options.UseForeColor = true;
            this.btn_ESC.Location = new System.Drawing.Point(203, 4);
            this.btn_ESC.Name = "btn_ESC";
            this.btn_ESC.Size = new System.Drawing.Size(60, 55);
            this.btn_ESC.TabIndex = 10;
            this.btn_ESC.Text = "ESC";
            this.btn_ESC.Click += new System.EventHandler(this.btn_ESC_Click);
            // 
            // pn_banphim_chu
            // 
            this.pn_banphim_chu.Controls.Add(this.btn_backchar);
            this.pn_banphim_chu.Controls.Add(this.btn_PaymentsChar);
            this.pn_banphim_chu.Controls.Add(this.btn_OKChar);
            this.pn_banphim_chu.Controls.Add(this.btn_L);
            this.pn_banphim_chu.Controls.Add(this.btn_K);
            this.pn_banphim_chu.Controls.Add(this.btn_U);
            this.pn_banphim_chu.Controls.Add(this.btn_Y);
            this.pn_banphim_chu.Controls.Add(this.btn_123);
            this.pn_banphim_chu.Controls.Add(this.btn_M);
            this.pn_banphim_chu.Controls.Add(this.btn_J);
            this.pn_banphim_chu.Controls.Add(this.btn_I);
            this.pn_banphim_chu.Controls.Add(this.btn_T);
            this.pn_banphim_chu.Controls.Add(this.btn_DeleteChar);
            this.pn_banphim_chu.Controls.Add(this.btn_N);
            this.pn_banphim_chu.Controls.Add(this.btn_H);
            this.pn_banphim_chu.Controls.Add(this.btn_O);
            this.pn_banphim_chu.Controls.Add(this.btn_R);
            this.pn_banphim_chu.Controls.Add(this.btn_ESCChar);
            this.pn_banphim_chu.Controls.Add(this.btn_B);
            this.pn_banphim_chu.Controls.Add(this.btn_G);
            this.pn_banphim_chu.Controls.Add(this.btn_P);
            this.pn_banphim_chu.Controls.Add(this.btn_E);
            this.pn_banphim_chu.Controls.Add(this.btn_X);
            this.pn_banphim_chu.Controls.Add(this.btn_V);
            this.pn_banphim_chu.Controls.Add(this.btn_F);
            this.pn_banphim_chu.Controls.Add(this.btn_A);
            this.pn_banphim_chu.Controls.Add(this.btn_W);
            this.pn_banphim_chu.Controls.Add(this.btn_Z);
            this.pn_banphim_chu.Controls.Add(this.btn_C);
            this.pn_banphim_chu.Controls.Add(this.btn_D);
            this.pn_banphim_chu.Controls.Add(this.btn_S);
            this.pn_banphim_chu.Controls.Add(this.btn_Q);
            this.pn_banphim_chu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_banphim_chu.Location = new System.Drawing.Point(0, 0);
            this.pn_banphim_chu.Name = "pn_banphim_chu";
            this.pn_banphim_chu.Size = new System.Drawing.Size(409, 256);
            this.pn_banphim_chu.TabIndex = 0;
            // 
            // btn_backchar
            // 
            this.btn_backchar.Image = ((System.Drawing.Image)(resources.GetObject("btn_backchar.Image")));
            this.btn_backchar.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_backchar.Location = new System.Drawing.Point(315, 124);
            this.btn_backchar.Name = "btn_backchar";
            this.btn_backchar.Size = new System.Drawing.Size(85, 45);
            this.btn_backchar.TabIndex = 31;
            this.btn_backchar.Text = "back";
            this.btn_backchar.Click += new System.EventHandler(this.btn_backchar_Click);
            // 
            // btn_PaymentsChar
            // 
            this.btn_PaymentsChar.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_PaymentsChar.Appearance.Options.UseFont = true;
            this.btn_PaymentsChar.Image = ((System.Drawing.Image)(resources.GetObject("btn_PaymentsChar.Image")));
            this.btn_PaymentsChar.Location = new System.Drawing.Point(64, 175);
            this.btn_PaymentsChar.Name = "btn_PaymentsChar";
            this.btn_PaymentsChar.Size = new System.Drawing.Size(125, 73);
            this.btn_PaymentsChar.TabIndex = 30;
            this.btn_PaymentsChar.Text = "Thanh Toán\r\n\r\n[ F1 ]";
            this.btn_PaymentsChar.Click += new System.EventHandler(this.btn_PaymentsChar_Click);
            // 
            // btn_OKChar
            // 
            this.btn_OKChar.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_OKChar.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btn_OKChar.Appearance.Options.UseFont = true;
            this.btn_OKChar.Appearance.Options.UseForeColor = true;
            this.btn_OKChar.Location = new System.Drawing.Point(195, 175);
            this.btn_OKChar.Name = "btn_OKChar";
            this.btn_OKChar.Size = new System.Drawing.Size(85, 73);
            this.btn_OKChar.TabIndex = 29;
            this.btn_OKChar.Text = "OK";
            this.btn_OKChar.Click += new System.EventHandler(this.btn_OKChar_Click);
            // 
            // btn_L
            // 
            this.btn_L.Location = new System.Drawing.Point(349, 65);
            this.btn_L.Name = "btn_L";
            this.btn_L.Size = new System.Drawing.Size(40, 45);
            this.btn_L.TabIndex = 28;
            this.btn_L.Text = "L";
            this.btn_L.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_K
            // 
            this.btn_K.Location = new System.Drawing.Point(307, 65);
            this.btn_K.Name = "btn_K";
            this.btn_K.Size = new System.Drawing.Size(40, 45);
            this.btn_K.TabIndex = 27;
            this.btn_K.Text = "K";
            this.btn_K.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_U
            // 
            this.btn_U.Location = new System.Drawing.Point(240, 7);
            this.btn_U.Name = "btn_U";
            this.btn_U.Size = new System.Drawing.Size(40, 45);
            this.btn_U.TabIndex = 26;
            this.btn_U.Text = "U";
            this.btn_U.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_Y
            // 
            this.btn_Y.Location = new System.Drawing.Point(200, 7);
            this.btn_Y.Name = "btn_Y";
            this.btn_Y.Size = new System.Drawing.Size(40, 45);
            this.btn_Y.TabIndex = 25;
            this.btn_Y.Text = "Y";
            this.btn_Y.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_123
            // 
            this.btn_123.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_123.Appearance.Options.UseFont = true;
            this.btn_123.Location = new System.Drawing.Point(0, 175);
            this.btn_123.Name = "btn_123";
            this.btn_123.Size = new System.Drawing.Size(60, 73);
            this.btn_123.TabIndex = 24;
            this.btn_123.Text = "123";
            this.btn_123.Click += new System.EventHandler(this.btn_123_Click);
            // 
            // btn_M
            // 
            this.btn_M.Location = new System.Drawing.Point(269, 124);
            this.btn_M.Name = "btn_M";
            this.btn_M.Size = new System.Drawing.Size(40, 45);
            this.btn_M.TabIndex = 23;
            this.btn_M.Text = "M";
            this.btn_M.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_J
            // 
            this.btn_J.Location = new System.Drawing.Point(264, 65);
            this.btn_J.Name = "btn_J";
            this.btn_J.Size = new System.Drawing.Size(40, 45);
            this.btn_J.TabIndex = 22;
            this.btn_J.Text = "J";
            this.btn_J.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_I
            // 
            this.btn_I.Location = new System.Drawing.Point(320, 7);
            this.btn_I.Name = "btn_I";
            this.btn_I.Size = new System.Drawing.Size(40, 45);
            this.btn_I.TabIndex = 21;
            this.btn_I.Text = "I";
            this.btn_I.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_T
            // 
            this.btn_T.Location = new System.Drawing.Point(160, 7);
            this.btn_T.Name = "btn_T";
            this.btn_T.Size = new System.Drawing.Size(40, 45);
            this.btn_T.TabIndex = 20;
            this.btn_T.Text = "T";
            this.btn_T.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_DeleteChar
            // 
            this.btn_DeleteChar.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_DeleteChar.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btn_DeleteChar.Appearance.Options.UseFont = true;
            this.btn_DeleteChar.Appearance.Options.UseForeColor = true;
            this.btn_DeleteChar.Location = new System.Drawing.Point(286, 175);
            this.btn_DeleteChar.Name = "btn_DeleteChar";
            this.btn_DeleteChar.Size = new System.Drawing.Size(50, 73);
            this.btn_DeleteChar.TabIndex = 19;
            this.btn_DeleteChar.Text = "Del";
            this.btn_DeleteChar.Click += new System.EventHandler(this.btn_DeleteChar_Click);
            // 
            // btn_N
            // 
            this.btn_N.Location = new System.Drawing.Point(225, 124);
            this.btn_N.Name = "btn_N";
            this.btn_N.Size = new System.Drawing.Size(40, 45);
            this.btn_N.TabIndex = 18;
            this.btn_N.Text = "N";
            this.btn_N.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_H
            // 
            this.btn_H.Location = new System.Drawing.Point(224, 65);
            this.btn_H.Name = "btn_H";
            this.btn_H.Size = new System.Drawing.Size(38, 45);
            this.btn_H.TabIndex = 17;
            this.btn_H.Text = "H";
            this.btn_H.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_O
            // 
            this.btn_O.Location = new System.Drawing.Point(280, 7);
            this.btn_O.Name = "btn_O";
            this.btn_O.Size = new System.Drawing.Size(40, 45);
            this.btn_O.TabIndex = 16;
            this.btn_O.Text = "O";
            this.btn_O.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_R
            // 
            this.btn_R.Location = new System.Drawing.Point(120, 7);
            this.btn_R.Name = "btn_R";
            this.btn_R.Size = new System.Drawing.Size(40, 45);
            this.btn_R.TabIndex = 15;
            this.btn_R.Text = "R";
            this.btn_R.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_ESCChar
            // 
            this.btn_ESCChar.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ESCChar.Appearance.Options.UseFont = true;
            this.btn_ESCChar.Location = new System.Drawing.Point(342, 175);
            this.btn_ESCChar.Name = "btn_ESCChar";
            this.btn_ESCChar.Size = new System.Drawing.Size(56, 73);
            this.btn_ESCChar.TabIndex = 14;
            this.btn_ESCChar.Text = "ESC";
            this.btn_ESCChar.Click += new System.EventHandler(this.btn_ESCChar_Click);
            // 
            // btn_B
            // 
            this.btn_B.Location = new System.Drawing.Point(181, 124);
            this.btn_B.Name = "btn_B";
            this.btn_B.Size = new System.Drawing.Size(40, 45);
            this.btn_B.TabIndex = 13;
            this.btn_B.Text = "B";
            this.btn_B.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_G
            // 
            this.btn_G.Location = new System.Drawing.Point(182, 65);
            this.btn_G.Name = "btn_G";
            this.btn_G.Size = new System.Drawing.Size(40, 45);
            this.btn_G.TabIndex = 12;
            this.btn_G.Text = "G";
            this.btn_G.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_P
            // 
            this.btn_P.Location = new System.Drawing.Point(360, 7);
            this.btn_P.Name = "btn_P";
            this.btn_P.Size = new System.Drawing.Size(40, 45);
            this.btn_P.TabIndex = 11;
            this.btn_P.Text = "P";
            this.btn_P.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_E
            // 
            this.btn_E.Location = new System.Drawing.Point(80, 7);
            this.btn_E.Name = "btn_E";
            this.btn_E.Size = new System.Drawing.Size(40, 45);
            this.btn_E.TabIndex = 10;
            this.btn_E.Text = "E";
            this.btn_E.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_X
            // 
            this.btn_X.Location = new System.Drawing.Point(46, 124);
            this.btn_X.Name = "btn_X";
            this.btn_X.Size = new System.Drawing.Size(40, 45);
            this.btn_X.TabIndex = 9;
            this.btn_X.Text = "X";
            this.btn_X.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_V
            // 
            this.btn_V.Location = new System.Drawing.Point(137, 124);
            this.btn_V.Name = "btn_V";
            this.btn_V.Size = new System.Drawing.Size(40, 45);
            this.btn_V.TabIndex = 8;
            this.btn_V.Text = "V";
            this.btn_V.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_F
            // 
            this.btn_F.Location = new System.Drawing.Point(138, 65);
            this.btn_F.Name = "btn_F";
            this.btn_F.Size = new System.Drawing.Size(40, 45);
            this.btn_F.TabIndex = 7;
            this.btn_F.Text = "F";
            this.btn_F.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_A
            // 
            this.btn_A.Location = new System.Drawing.Point(11, 65);
            this.btn_A.Name = "btn_A";
            this.btn_A.Size = new System.Drawing.Size(40, 45);
            this.btn_A.TabIndex = 6;
            this.btn_A.Text = "A";
            this.btn_A.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_W
            // 
            this.btn_W.Location = new System.Drawing.Point(40, 7);
            this.btn_W.Name = "btn_W";
            this.btn_W.Size = new System.Drawing.Size(40, 45);
            this.btn_W.TabIndex = 5;
            this.btn_W.Text = "W";
            this.btn_W.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_Z
            // 
            this.btn_Z.Location = new System.Drawing.Point(0, 124);
            this.btn_Z.Name = "btn_Z";
            this.btn_Z.Size = new System.Drawing.Size(40, 45);
            this.btn_Z.TabIndex = 4;
            this.btn_Z.Text = "Z";
            this.btn_Z.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_C
            // 
            this.btn_C.Location = new System.Drawing.Point(92, 124);
            this.btn_C.Name = "btn_C";
            this.btn_C.Size = new System.Drawing.Size(40, 45);
            this.btn_C.TabIndex = 3;
            this.btn_C.Text = "C";
            this.btn_C.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_D
            // 
            this.btn_D.Location = new System.Drawing.Point(95, 65);
            this.btn_D.Name = "btn_D";
            this.btn_D.Size = new System.Drawing.Size(40, 45);
            this.btn_D.TabIndex = 2;
            this.btn_D.Text = "D";
            this.btn_D.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_S
            // 
            this.btn_S.Location = new System.Drawing.Point(53, 65);
            this.btn_S.Name = "btn_S";
            this.btn_S.Size = new System.Drawing.Size(40, 45);
            this.btn_S.TabIndex = 1;
            this.btn_S.Text = "S";
            this.btn_S.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // btn_Q
            // 
            this.btn_Q.Location = new System.Drawing.Point(0, 7);
            this.btn_Q.Name = "btn_Q";
            this.btn_Q.Size = new System.Drawing.Size(40, 45);
            this.btn_Q.TabIndex = 0;
            this.btn_Q.Text = "Q";
            this.btn_Q.Click += new System.EventHandler(this.btn_BanPhim_Click);
            // 
            // pnViewProduct
            // 
            this.pnViewProduct.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnViewProduct.Controls.Add(this.gridSplitContainer1);
            this.pnViewProduct.Controls.Add(this.pnUpDow);
            this.pnViewProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnViewProduct.Location = new System.Drawing.Point(0, 0);
            this.pnViewProduct.Name = "pnViewProduct";
            this.pnViewProduct.Size = new System.Drawing.Size(623, 405);
            this.pnViewProduct.TabIndex = 45;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.grCtrlDSHangMua;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.grCtrlDSHangMua);
            this.gridSplitContainer1.Size = new System.Drawing.Size(585, 405);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // grCtrlDSHangMua
            // 
            this.grCtrlDSHangMua.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grCtrlDSHangMua.Location = new System.Drawing.Point(0, 0);
            this.grCtrlDSHangMua.MainView = this.gVDSHangMua;
            this.grCtrlDSHangMua.Name = "grCtrlDSHangMua";
            this.grCtrlDSHangMua.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.btn_Add,
            this.repositoryItemCalcEdit1,
            this.btn_Exc,
            this.btn_Del,
            this.TrungHere,
            this.repositoryItemGridLookUpEdit1});
            this.grCtrlDSHangMua.Size = new System.Drawing.Size(585, 405);
            this.grCtrlDSHangMua.TabIndex = 15;
            this.grCtrlDSHangMua.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gVDSHangMua});
            this.grCtrlDSHangMua.Leave += new System.EventHandler(this.grCtrlDSHangMua_Leave);
            // 
            // gVDSHangMua
            // 
            this.gVDSHangMua.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gVDSHangMua.Appearance.Row.Options.UseFont = true;
            this.gVDSHangMua.Appearance.Row.Options.UseTextOptions = true;
            this.gVDSHangMua.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gVDSHangMua.ColumnPanelRowHeight = 32;
            this.gVDSHangMua.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.MA_HANGHOA,
            this.STT,
            this.MA_VACH,
            this.TEN_HANGHOA,
            this.SOLUONG,
            this.GIABAN,
            this.CHIETKHAU,
            this.Thanh_Tien,
            this.TONGTIEN,
            this.GHICHU,
            this.MA_BEP,
            this.THUE,
            this.TRANGTHAI,
            this.IS_COMBO,
            this.IS_INBEP,
            this.IS_KHUYENMAI,
            this.IS_QUATANG});
            this.gVDSHangMua.GridControl = this.grCtrlDSHangMua;
            this.gVDSHangMua.Name = "gVDSHangMua";
            this.gVDSHangMua.OptionsView.ShowGroupPanel = false;
            this.gVDSHangMua.OptionsView.ShowIndicator = false;
            this.gVDSHangMua.RowHeight = 40;
            this.gVDSHangMua.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gVDSHangMua.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gVDSHangMua_RowCellClick);
            this.gVDSHangMua.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gVDSHangMua_CustomDrawCell);
            this.gVDSHangMua.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gVDSHangMua_FocusedRowChanged);
            // 
            // MA_HANGHOA
            // 
            this.MA_HANGHOA.Caption = "gridColumn1";
            this.MA_HANGHOA.FieldName = "MA_HANGHOA";
            this.MA_HANGHOA.Name = "MA_HANGHOA";
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 31;
            // 
            // MA_VACH
            // 
            this.MA_VACH.AppearanceCell.Options.UseTextOptions = true;
            this.MA_VACH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_VACH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_VACH.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.MA_VACH.AppearanceHeader.Options.UseFont = true;
            this.MA_VACH.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_VACH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_VACH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_VACH.Caption = "Mã Hàng";
            this.MA_VACH.FieldName = "MA_VACH";
            this.MA_VACH.Name = "MA_VACH";
            this.MA_VACH.OptionsColumn.AllowEdit = false;
            this.MA_VACH.OptionsColumn.AllowFocus = false;
            this.MA_VACH.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.MA_VACH.Visible = true;
            this.MA_VACH.VisibleIndex = 1;
            this.MA_VACH.Width = 119;
            // 
            // TEN_HANGHOA
            // 
            this.TEN_HANGHOA.AppearanceCell.Options.UseTextOptions = true;
            this.TEN_HANGHOA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_HANGHOA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_HANGHOA.Caption = "Tên Hàng";
            this.TEN_HANGHOA.FieldName = "TEN_HANGHOA";
            this.TEN_HANGHOA.Name = "TEN_HANGHOA";
            this.TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this.TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this.TEN_HANGHOA.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TEN_HANGHOA.Visible = true;
            this.TEN_HANGHOA.VisibleIndex = 2;
            this.TEN_HANGHOA.Width = 153;
            // 
            // SOLUONG
            // 
            this.SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SOLUONG.Caption = "SL";
            this.SOLUONG.FieldName = "SOLUONG";
            this.SOLUONG.Name = "SOLUONG";
            this.SOLUONG.OptionsColumn.AllowEdit = false;
            this.SOLUONG.OptionsColumn.AllowFocus = false;
            this.SOLUONG.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.SOLUONG.Visible = true;
            this.SOLUONG.VisibleIndex = 3;
            this.SOLUONG.Width = 33;
            // 
            // GIABAN
            // 
            this.GIABAN.AppearanceCell.Options.UseTextOptions = true;
            this.GIABAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIABAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.GIABAN.AppearanceHeader.Options.UseFont = true;
            this.GIABAN.AppearanceHeader.Options.UseTextOptions = true;
            this.GIABAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIABAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN.Caption = "Đơn Giá ";
            this.GIABAN.DisplayFormat.FormatString = "{0:#,###0}";
            this.GIABAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GIABAN.FieldName = "GIABAN";
            this.GIABAN.Name = "GIABAN";
            this.GIABAN.OptionsColumn.AllowEdit = false;
            this.GIABAN.OptionsColumn.AllowFocus = false;
            this.GIABAN.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.GIABAN.Visible = true;
            this.GIABAN.VisibleIndex = 4;
            this.GIABAN.Width = 66;
            // 
            // CHIETKHAU
            // 
            this.CHIETKHAU.AppearanceCell.Options.UseTextOptions = true;
            this.CHIETKHAU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHIETKHAU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHIETKHAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.CHIETKHAU.AppearanceHeader.Options.UseFont = true;
            this.CHIETKHAU.AppearanceHeader.Options.UseTextOptions = true;
            this.CHIETKHAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHIETKHAU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHIETKHAU.Caption = "Chiết Khấu";
            this.CHIETKHAU.DisplayFormat.FormatString = "{0:#,###0}";
            this.CHIETKHAU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.CHIETKHAU.FieldName = "CHIETKHAU";
            this.CHIETKHAU.Name = "CHIETKHAU";
            this.CHIETKHAU.OptionsColumn.AllowEdit = false;
            this.CHIETKHAU.OptionsColumn.AllowFocus = false;
            this.CHIETKHAU.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.CHIETKHAU.Visible = true;
            this.CHIETKHAU.VisibleIndex = 5;
            this.CHIETKHAU.Width = 73;
            // 
            // Thanh_Tien
            // 
            this.Thanh_Tien.AppearanceCell.Options.UseTextOptions = true;
            this.Thanh_Tien.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Thanh_Tien.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Thanh_Tien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Thanh_Tien.AppearanceHeader.Options.UseFont = true;
            this.Thanh_Tien.AppearanceHeader.Options.UseTextOptions = true;
            this.Thanh_Tien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Thanh_Tien.Caption = "Thành Tiền";
            this.Thanh_Tien.DisplayFormat.FormatString = "{0:#,###0}";
            this.Thanh_Tien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.Thanh_Tien.FieldName = "Thanh_Tien";
            this.Thanh_Tien.Name = "Thanh_Tien";
            this.Thanh_Tien.OptionsColumn.AllowEdit = false;
            this.Thanh_Tien.OptionsColumn.AllowFocus = false;
            this.Thanh_Tien.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.Thanh_Tien.Visible = true;
            this.Thanh_Tien.VisibleIndex = 6;
            this.Thanh_Tien.Width = 80;
            // 
            // TONGTIEN
            // 
            this.TONGTIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TONGTIEN.AppearanceCell.Options.UseFont = true;
            this.TONGTIEN.AppearanceCell.Options.UseTextOptions = true;
            this.TONGTIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGTIEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TONGTIEN.AppearanceHeader.Options.UseFont = true;
            this.TONGTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGTIEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGTIEN.Caption = "TỔNG TIỀN";
            this.TONGTIEN.DisplayFormat.FormatString = "{0:#,###0}";
            this.TONGTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TONGTIEN.FieldName = "TONGTIEN";
            this.TONGTIEN.Name = "TONGTIEN";
            this.TONGTIEN.OptionsColumn.AllowEdit = false;
            this.TONGTIEN.OptionsColumn.AllowFocus = false;
            this.TONGTIEN.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TONGTIEN.Width = 100;
            // 
            // GHICHU
            // 
            this.GHICHU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GHICHU.AppearanceCell.Options.UseFont = true;
            this.GHICHU.AppearanceCell.Options.UseTextOptions = true;
            this.GHICHU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GHICHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GHICHU.AppearanceHeader.Options.UseFont = true;
            this.GHICHU.AppearanceHeader.Options.UseTextOptions = true;
            this.GHICHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.Name = "GHICHU";
            this.GHICHU.OptionsColumn.AllowEdit = false;
            this.GHICHU.OptionsColumn.AllowFocus = false;
            this.GHICHU.Width = 56;
            // 
            // MA_BEP
            // 
            this.MA_BEP.AppearanceCell.Options.UseTextOptions = true;
            this.MA_BEP.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_BEP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.MA_BEP.AppearanceHeader.Options.UseFont = true;
            this.MA_BEP.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_BEP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_BEP.FieldName = "MA_BEP";
            this.MA_BEP.Name = "MA_BEP";
            this.MA_BEP.OptionsColumn.AllowEdit = false;
            this.MA_BEP.OptionsColumn.AllowFocus = false;
            this.MA_BEP.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.MA_BEP.Width = 45;
            // 
            // THUE
            // 
            this.THUE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.THUE.AppearanceCell.Options.UseFont = true;
            this.THUE.AppearanceCell.Options.UseTextOptions = true;
            this.THUE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THUE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.THUE.AppearanceHeader.Options.UseFont = true;
            this.THUE.AppearanceHeader.Options.UseTextOptions = true;
            this.THUE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THUE.FieldName = "THUE";
            this.THUE.Name = "THUE";
            this.THUE.OptionsColumn.AllowEdit = false;
            this.THUE.OptionsColumn.AllowFocus = false;
            // 
            // TRANGTHAI
            // 
            this.TRANGTHAI.AppearanceCell.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TRANGTHAI.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TRANGTHAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TRANGTHAI.AppearanceHeader.Options.UseFont = true;
            this.TRANGTHAI.AppearanceHeader.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TRANGTHAI.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TRANGTHAI.FieldName = "TRANGTHAI";
            this.TRANGTHAI.Name = "TRANGTHAI";
            this.TRANGTHAI.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI.OptionsColumn.AllowFocus = false;
            this.TRANGTHAI.Width = 47;
            // 
            // IS_COMBO
            // 
            this.IS_COMBO.FieldName = "IS_COMBO";
            this.IS_COMBO.Name = "IS_COMBO";
            // 
            // IS_INBEP
            // 
            this.IS_INBEP.FieldName = "IS_INBEP";
            this.IS_INBEP.Name = "IS_INBEP";
            // 
            // IS_KHUYENMAI
            // 
            this.IS_KHUYENMAI.FieldName = "IS_KHUYENMAI";
            this.IS_KHUYENMAI.Name = "IS_KHUYENMAI";
            // 
            // IS_QUATANG
            // 
            this.IS_QUATANG.Caption = "IS_QUATANG";
            this.IS_QUATANG.FieldName = "IS_QUATANG";
            this.IS_QUATANG.Name = "IS_QUATANG";
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "Xóa", 20, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject11, "Xóa", null, null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "Cong", 20, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject12, "Cộng", null, null, true)});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // btn_Add
            // 
            this.btn_Add.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Add.Appearance.Options.UseFont = true;
            this.btn_Add.AutoHeight = false;
            serializableAppearanceObject13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            serializableAppearanceObject13.Options.UseFont = true;
            this.btn_Add.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, "", null, null, true)});
            this.btn_Add.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // btn_Exc
            // 
            this.btn_Exc.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Exc.Appearance.Options.UseFont = true;
            this.btn_Exc.AutoHeight = false;
            serializableAppearanceObject14.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            serializableAppearanceObject14.Options.UseFont = true;
            this.btn_Exc.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Minus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject14, "", null, null, true)});
            this.btn_Exc.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_Exc.Name = "btn_Exc";
            this.btn_Exc.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // btn_Del
            // 
            this.btn_Del.AutoHeight = false;
            this.btn_Del.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Delete", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject15, "", null, null, true)});
            this.btn_Del.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_Del.Name = "btn_Del";
            this.btn_Del.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // TrungHere
            // 
            this.TrungHere.Appearance.Options.UseTextOptions = true;
            this.TrungHere.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TrungHere.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "Here", 20, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject16, "", null, null, true)});
            this.TrungHere.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.TrungHere.Name = "TrungHere";
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.View = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // pnUpDow
            // 
            this.pnUpDow.Controls.Add(this.labelControl17);
            this.pnUpDow.Controls.Add(this.lb_RowsCount);
            this.pnUpDow.Controls.Add(this.lb_STT);
            this.pnUpDow.Controls.Add(this.btn_up);
            this.pnUpDow.Controls.Add(this.btn_Dow);
            this.pnUpDow.Controls.Add(this.btn_DowLast);
            this.pnUpDow.Controls.Add(this.btn_UpFirst);
            this.pnUpDow.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnUpDow.Location = new System.Drawing.Point(585, 0);
            this.pnUpDow.Name = "pnUpDow";
            this.pnUpDow.Size = new System.Drawing.Size(38, 405);
            this.pnUpDow.TabIndex = 16;
            // 
            // labelControl17
            // 
            this.labelControl17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl17.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl17.Location = new System.Drawing.Point(9, 174);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(20, 13);
            this.labelControl17.TabIndex = 7;
            this.labelControl17.Text = "----";
            // 
            // lb_RowsCount
            // 
            this.lb_RowsCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_RowsCount.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_RowsCount.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_RowsCount.Location = new System.Drawing.Point(11, 187);
            this.lb_RowsCount.Name = "lb_RowsCount";
            this.lb_RowsCount.Size = new System.Drawing.Size(14, 13);
            this.lb_RowsCount.TabIndex = 6;
            this.lb_RowsCount.Text = "00";
            // 
            // lb_STT
            // 
            this.lb_STT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_STT.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_STT.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_STT.Location = new System.Drawing.Point(11, 160);
            this.lb_STT.Name = "lb_STT";
            this.lb_STT.Size = new System.Drawing.Size(14, 13);
            this.lb_STT.TabIndex = 5;
            this.lb_STT.Text = "00";
            // 
            // btn_up
            // 
            this.btn_up.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_up.Image = ((System.Drawing.Image)(resources.GetObject("btn_up.Image")));
            this.btn_up.Location = new System.Drawing.Point(2, 55);
            this.btn_up.Name = "btn_up";
            this.btn_up.Size = new System.Drawing.Size(34, 53);
            this.btn_up.TabIndex = 4;
            this.btn_up.Text = "simpleButton7";
            this.btn_up.Click += new System.EventHandler(this.btn_up_Click);
            // 
            // btn_Dow
            // 
            this.btn_Dow.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_Dow.Image = ((System.Drawing.Image)(resources.GetObject("btn_Dow.Image")));
            this.btn_Dow.Location = new System.Drawing.Point(2, 297);
            this.btn_Dow.Name = "btn_Dow";
            this.btn_Dow.Size = new System.Drawing.Size(34, 53);
            this.btn_Dow.TabIndex = 3;
            this.btn_Dow.Text = "simpleButton6";
            this.btn_Dow.Click += new System.EventHandler(this.btn_Dow_Click);
            // 
            // btn_DowLast
            // 
            this.btn_DowLast.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_DowLast.Image = ((System.Drawing.Image)(resources.GetObject("btn_DowLast.Image")));
            this.btn_DowLast.Location = new System.Drawing.Point(2, 350);
            this.btn_DowLast.Name = "btn_DowLast";
            this.btn_DowLast.Size = new System.Drawing.Size(34, 53);
            this.btn_DowLast.TabIndex = 2;
            this.btn_DowLast.Text = "simpleButton4";
            this.btn_DowLast.Click += new System.EventHandler(this.btn_DowLast_Click);
            // 
            // btn_UpFirst
            // 
            this.btn_UpFirst.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_UpFirst.Image = ((System.Drawing.Image)(resources.GetObject("btn_UpFirst.Image")));
            this.btn_UpFirst.Location = new System.Drawing.Point(2, 2);
            this.btn_UpFirst.Name = "btn_UpFirst";
            this.btn_UpFirst.Size = new System.Drawing.Size(34, 53);
            this.btn_UpFirst.TabIndex = 0;
            this.btn_UpFirst.Text = "simpleButton1";
            this.btn_UpFirst.Click += new System.EventHandler(this.btn_UpFirst_Click);
            // 
            // btn_Dong
            // 
            this.btn_Dong.Border = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btn_Dong.Caption = "&7. Thoát";
            this.btn_Dong.Glyph = ((System.Drawing.Image)(resources.GetObject("btn_Dong.Glyph")));
            this.btn_Dong.Id = 12;
            this.btn_Dong.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Dong.ItemAppearance.Normal.Options.UseFont = true;
            this.btn_Dong.Name = "btn_Dong";
            // 
            // bar3
            // 
            this.bar3.BarAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bar3.BarAppearance.Normal.ForeColor = System.Drawing.Color.Navy;
            this.bar3.BarAppearance.Normal.Options.UseFont = true;
            this.bar3.BarAppearance.Normal.Options.UseForeColor = true;
            this.bar3.BarName = "Custom 4";
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btn_Dong, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar3.Offset = 4000;
            this.bar3.Text = "Custom 4";
            // 
            // pn_WapperContent
            // 
            this.pn_WapperContent.Controls.Add(this.pn_Luoi);
            this.pn_WapperContent.Controls.Add(this.pnWapperDeletePlusBut);
            this.pn_WapperContent.Controls.Add(this.pn_Banhang);
            this.pn_WapperContent.Controls.Add(this.pn_bigparent);
            this.pn_WapperContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_WapperContent.Location = new System.Drawing.Point(0, 70);
            this.pn_WapperContent.Name = "pn_WapperContent";
            this.pn_WapperContent.Size = new System.Drawing.Size(1036, 698);
            this.pn_WapperContent.TabIndex = 55;
            // 
            // pn_Luoi
            // 
            this.pn_Luoi.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pn_Luoi.Controls.Add(this.pnViewProduct);
            this.pn_Luoi.Controls.Add(this.pnTachban);
            this.pn_Luoi.Controls.Add(this.pnInlaihoadon);
            this.pn_Luoi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_Luoi.Location = new System.Drawing.Point(2, 2);
            this.pn_Luoi.Name = "pn_Luoi";
            this.pn_Luoi.Size = new System.Drawing.Size(623, 405);
            this.pn_Luoi.TabIndex = 16;
            // 
            // pnTachban
            // 
            this.pnTachban.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnTachban.Controls.Add(this.pn_GroupTable);
            this.pnTachban.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnTachban.Location = new System.Drawing.Point(0, 0);
            this.pnTachban.Name = "pnTachban";
            this.pnTachban.Size = new System.Drawing.Size(623, 405);
            this.pnTachban.TabIndex = 47;
            // 
            // pn_GroupTable
            // 
            this.pn_GroupTable.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pn_GroupTable.Controls.Add(this.panelControl6);
            this.pn_GroupTable.Controls.Add(this.panelControl15);
            this.pn_GroupTable.Controls.Add(this.pn_TableTow);
            this.pn_GroupTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_GroupTable.Location = new System.Drawing.Point(0, 0);
            this.pn_GroupTable.Name = "pn_GroupTable";
            this.pn_GroupTable.Size = new System.Drawing.Size(623, 405);
            this.pn_GroupTable.TabIndex = 3;
            // 
            // panelControl6
            // 
            this.panelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl6.Controls.Add(this.labelControl1);
            this.panelControl6.Controls.Add(this.btn_OkThrough);
            this.panelControl6.Controls.Add(this.btn_ToLeftAll);
            this.panelControl6.Controls.Add(this.btn_ToRightAll);
            this.panelControl6.Controls.Add(this.btn_UpDivide);
            this.panelControl6.Controls.Add(this.btn_DowDivide);
            this.panelControl6.Controls.Add(this.btn_ESCThroughtTable);
            this.panelControl6.Controls.Add(this.btn_ToRight);
            this.panelControl6.Controls.Add(this.btn_Toleft);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(354, 0);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(60, 405);
            this.panelControl6.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl1.Location = new System.Drawing.Point(7, 175);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(100, 13);
            this.labelControl1.TabIndex = 12;
            this.labelControl1.Text = "--------------------";
            // 
            // btn_OkThrough
            // 
            this.btn_OkThrough.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn_OkThrough.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btn_OkThrough.Appearance.Options.UseFont = true;
            this.btn_OkThrough.Appearance.Options.UseForeColor = true;
            this.btn_OkThrough.Location = new System.Drawing.Point(7, 55);
            this.btn_OkThrough.Name = "btn_OkThrough";
            this.btn_OkThrough.Size = new System.Drawing.Size(93, 40);
            this.btn_OkThrough.TabIndex = 0;
            this.btn_OkThrough.Text = "OK";
            this.btn_OkThrough.Click += new System.EventHandler(this.btn_OkThrough_Click);
            // 
            // btn_ToLeftAll
            // 
            this.btn_ToLeftAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_ToLeftAll.Image = ((System.Drawing.Image)(resources.GetObject("btn_ToLeftAll.Image")));
            this.btn_ToLeftAll.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_ToLeftAll.Location = new System.Drawing.Point(7, 247);
            this.btn_ToLeftAll.Name = "btn_ToLeftAll";
            this.btn_ToLeftAll.Size = new System.Drawing.Size(93, 40);
            this.btn_ToLeftAll.TabIndex = 4;
            this.btn_ToLeftAll.Text = "<<";
            this.btn_ToLeftAll.Click += new System.EventHandler(this.btn_ToLeftAll_Click);
            // 
            // btn_ToRightAll
            // 
            this.btn_ToRightAll.Image = ((System.Drawing.Image)(resources.GetObject("btn_ToRightAll.Image")));
            this.btn_ToRightAll.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_ToRightAll.Location = new System.Drawing.Point(7, 101);
            this.btn_ToRightAll.Name = "btn_ToRightAll";
            this.btn_ToRightAll.Size = new System.Drawing.Size(93, 40);
            this.btn_ToRightAll.TabIndex = 1;
            this.btn_ToRightAll.Text = ">>";
            this.btn_ToRightAll.Click += new System.EventHandler(this.btn_ToRightAll_Click);
            // 
            // btn_UpDivide
            // 
            this.btn_UpDivide.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_UpDivide.Image = ((System.Drawing.Image)(resources.GetObject("btn_UpDivide.Image")));
            this.btn_UpDivide.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_UpDivide.Location = new System.Drawing.Point(0, 0);
            this.btn_UpDivide.Name = "btn_UpDivide";
            this.btn_UpDivide.Size = new System.Drawing.Size(60, 50);
            this.btn_UpDivide.TabIndex = 11;
            this.btn_UpDivide.Text = "simpleButton7";
            this.btn_UpDivide.Click += new System.EventHandler(this.btn_UpDivide_Click);
            // 
            // btn_DowDivide
            // 
            this.btn_DowDivide.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_DowDivide.Image = ((System.Drawing.Image)(resources.GetObject("btn_DowDivide.Image")));
            this.btn_DowDivide.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_DowDivide.Location = new System.Drawing.Point(0, 355);
            this.btn_DowDivide.Name = "btn_DowDivide";
            this.btn_DowDivide.Size = new System.Drawing.Size(60, 50);
            this.btn_DowDivide.TabIndex = 10;
            this.btn_DowDivide.Text = "simpleButton6";
            this.btn_DowDivide.Click += new System.EventHandler(this.btn_DowDivide_Click);
            // 
            // btn_ESCThroughtTable
            // 
            this.btn_ESCThroughtTable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_ESCThroughtTable.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn_ESCThroughtTable.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btn_ESCThroughtTable.Appearance.Options.UseFont = true;
            this.btn_ESCThroughtTable.Appearance.Options.UseForeColor = true;
            this.btn_ESCThroughtTable.Location = new System.Drawing.Point(7, 293);
            this.btn_ESCThroughtTable.Name = "btn_ESCThroughtTable";
            this.btn_ESCThroughtTable.Size = new System.Drawing.Size(93, 40);
            this.btn_ESCThroughtTable.TabIndex = 5;
            this.btn_ESCThroughtTable.Text = "ESC";
            this.btn_ESCThroughtTable.Click += new System.EventHandler(this.btn_ESCThroughtTable_Click);
            // 
            // btn_ToRight
            // 
            this.btn_ToRight.Image = ((System.Drawing.Image)(resources.GetObject("btn_ToRight.Image")));
            this.btn_ToRight.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_ToRight.Location = new System.Drawing.Point(7, 147);
            this.btn_ToRight.Name = "btn_ToRight";
            this.btn_ToRight.Size = new System.Drawing.Size(93, 40);
            this.btn_ToRight.TabIndex = 2;
            this.btn_ToRight.Text = ">";
            this.btn_ToRight.Click += new System.EventHandler(this.btn_ToRight_Click);
            // 
            // btn_Toleft
            // 
            this.btn_Toleft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Toleft.Image = ((System.Drawing.Image)(resources.GetObject("btn_Toleft.Image")));
            this.btn_Toleft.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_Toleft.Location = new System.Drawing.Point(7, 201);
            this.btn_Toleft.Name = "btn_Toleft";
            this.btn_Toleft.Size = new System.Drawing.Size(93, 40);
            this.btn_Toleft.TabIndex = 3;
            this.btn_Toleft.Text = "<";
            this.btn_Toleft.Click += new System.EventHandler(this.btn_Toleft_Click);
            // 
            // panelControl15
            // 
            this.panelControl15.Controls.Add(this.gr_divide_Table_1);
            this.panelControl15.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl15.Location = new System.Drawing.Point(0, 0);
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(354, 405);
            this.panelControl15.TabIndex = 0;
            // 
            // gr_divide_Table_1
            // 
            this.gr_divide_Table_1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode3.RelationName = "Level1";
            this.gr_divide_Table_1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode3});
            this.gr_divide_Table_1.Location = new System.Drawing.Point(2, 2);
            this.gr_divide_Table_1.MainView = this.gv_divide_Table_1;
            this.gr_divide_Table_1.Name = "gr_divide_Table_1";
            this.gr_divide_Table_1.Size = new System.Drawing.Size(350, 401);
            this.gr_divide_Table_1.TabIndex = 20;
            this.gr_divide_Table_1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv_divide_Table_1});
            // 
            // gv_divide_Table_1
            // 
            this.gv_divide_Table_1.ColumnPanelRowHeight = 30;
            this.gv_divide_Table_1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._SSTT,
            this._TEN_HANGHOA,
            this._SOLUONG,
            this._GIABAN,
            this._THANHTIEN,
            this._MA_HOADON,
            this._MA_HANGHOA,
            this._THUE,
            this._CHIETKHAU,
            this._IS_COMBO});
            this.gv_divide_Table_1.GridControl = this.gr_divide_Table_1;
            this.gv_divide_Table_1.Name = "gv_divide_Table_1";
            this.gv_divide_Table_1.OptionsView.ShowGroupPanel = false;
            this.gv_divide_Table_1.OptionsView.ShowIndicator = false;
            this.gv_divide_Table_1.RowHeight = 35;
            this.gv_divide_Table_1.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gv_divide_Table_1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gv_divide_Table_1_CustomDrawCell);
            // 
            // _SSTT
            // 
            this._SSTT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._SSTT.AppearanceCell.Options.UseFont = true;
            this._SSTT.AppearanceCell.Options.UseTextOptions = true;
            this._SSTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._SSTT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._SSTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this._SSTT.AppearanceHeader.Options.UseFont = true;
            this._SSTT.AppearanceHeader.Options.UseTextOptions = true;
            this._SSTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._SSTT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._SSTT.Caption = "TT";
            this._SSTT.FieldName = "SSTT";
            this._SSTT.Name = "_SSTT";
            this._SSTT.OptionsColumn.AllowEdit = false;
            this._SSTT.OptionsColumn.AllowFocus = false;
            this._SSTT.Visible = true;
            this._SSTT.VisibleIndex = 0;
            this._SSTT.Width = 34;
            // 
            // _TEN_HANGHOA
            // 
            this._TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this._TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this._TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this._TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this._TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._TEN_HANGHOA.Caption = "Hàng Hóa";
            this._TEN_HANGHOA.FieldName = "TEN_HANGHOA";
            this._TEN_HANGHOA.Name = "_TEN_HANGHOA";
            this._TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this._TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this._TEN_HANGHOA.Visible = true;
            this._TEN_HANGHOA.VisibleIndex = 1;
            this._TEN_HANGHOA.Width = 116;
            // 
            // _SOLUONG
            // 
            this._SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._SOLUONG.AppearanceCell.Options.UseFont = true;
            this._SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this._SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this._SOLUONG.AppearanceHeader.Options.UseFont = true;
            this._SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this._SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._SOLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._SOLUONG.Caption = "SL";
            this._SOLUONG.FieldName = "SOLUONG";
            this._SOLUONG.Name = "_SOLUONG";
            this._SOLUONG.OptionsColumn.AllowEdit = false;
            this._SOLUONG.OptionsColumn.AllowFocus = false;
            this._SOLUONG.Visible = true;
            this._SOLUONG.VisibleIndex = 2;
            this._SOLUONG.Width = 51;
            // 
            // _GIABAN
            // 
            this._GIABAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._GIABAN.AppearanceCell.Options.UseFont = true;
            this._GIABAN.AppearanceCell.Options.UseTextOptions = true;
            this._GIABAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._GIABAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._GIABAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this._GIABAN.AppearanceHeader.Options.UseFont = true;
            this._GIABAN.AppearanceHeader.Options.UseTextOptions = true;
            this._GIABAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._GIABAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._GIABAN.Caption = "Đơn giá";
            this._GIABAN.DisplayFormat.FormatString = "{0:#,###0}";
            this._GIABAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this._GIABAN.FieldName = "GIABAN";
            this._GIABAN.Name = "_GIABAN";
            this._GIABAN.OptionsColumn.AllowEdit = false;
            this._GIABAN.OptionsColumn.AllowFocus = false;
            this._GIABAN.Visible = true;
            this._GIABAN.VisibleIndex = 3;
            this._GIABAN.Width = 96;
            // 
            // _THANHTIEN
            // 
            this._THANHTIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._THANHTIEN.AppearanceCell.Options.UseFont = true;
            this._THANHTIEN.AppearanceCell.Options.UseTextOptions = true;
            this._THANHTIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._THANHTIEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._THANHTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this._THANHTIEN.AppearanceHeader.Options.UseFont = true;
            this._THANHTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this._THANHTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._THANHTIEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._THANHTIEN.Caption = "T Tiền";
            this._THANHTIEN.DisplayFormat.FormatString = "{0:#,###0}";
            this._THANHTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this._THANHTIEN.FieldName = "THANHTIEN";
            this._THANHTIEN.Name = "_THANHTIEN";
            this._THANHTIEN.OptionsColumn.AllowEdit = false;
            this._THANHTIEN.OptionsColumn.AllowFocus = false;
            this._THANHTIEN.Visible = true;
            this._THANHTIEN.VisibleIndex = 4;
            this._THANHTIEN.Width = 104;
            // 
            // _MA_HOADON
            // 
            this._MA_HOADON.Caption = "MA_HOADON";
            this._MA_HOADON.FieldName = "MA_HOADON";
            this._MA_HOADON.Name = "_MA_HOADON";
            // 
            // _MA_HANGHOA
            // 
            this._MA_HANGHOA.Caption = "MA_HANGHOA";
            this._MA_HANGHOA.FieldName = "MA_HANGHOA";
            this._MA_HANGHOA.Name = "_MA_HANGHOA";
            this._MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this._MA_HANGHOA.OptionsColumn.AllowFocus = false;
            // 
            // _THUE
            // 
            this._THUE.Caption = "THUE";
            this._THUE.FieldName = "THUE";
            this._THUE.Name = "_THUE";
            // 
            // _CHIETKHAU
            // 
            this._CHIETKHAU.Caption = "CHIETKHAU";
            this._CHIETKHAU.FieldName = "CHIETKHAU";
            this._CHIETKHAU.Name = "_CHIETKHAU";
            // 
            // _IS_COMBO
            // 
            this._IS_COMBO.Caption = "_IS_COMBO";
            this._IS_COMBO.FieldName = "IS_COMBO";
            this._IS_COMBO.Name = "_IS_COMBO";
            // 
            // pn_TableTow
            // 
            this.pn_TableTow.Controls.Add(this.gr_divide_Table_2);
            this.pn_TableTow.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn_TableTow.Location = new System.Drawing.Point(414, 0);
            this.pn_TableTow.Name = "pn_TableTow";
            this.pn_TableTow.Size = new System.Drawing.Size(209, 405);
            this.pn_TableTow.TabIndex = 1;
            // 
            // gr_divide_Table_2
            // 
            this.gr_divide_Table_2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gr_divide_Table_2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gr_divide_Table_2.Location = new System.Drawing.Point(2, 2);
            this.gr_divide_Table_2.MainView = this.gv_divide_Table_2;
            this.gr_divide_Table_2.Name = "gr_divide_Table_2";
            this.gr_divide_Table_2.Size = new System.Drawing.Size(205, 401);
            this.gr_divide_Table_2.TabIndex = 21;
            this.gr_divide_Table_2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv_divide_Table_2});
            // 
            // gv_divide_Table_2
            // 
            this.gv_divide_Table_2.ColumnPanelRowHeight = 30;
            this.gv_divide_Table_2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.@__STT,
            this.@__TEN_HANGHOA,
            this.@__SOLUONG,
            this.@__GIABAN,
            this.@__THANHTIEN,
            this.@__MA_HOADON,
            this.@__MA_HANGHOA,
            this.@__THUE,
            this.@__CHIETKHAU,
            this.@__IS_COMBO});
            this.gv_divide_Table_2.GridControl = this.gr_divide_Table_2;
            this.gv_divide_Table_2.Name = "gv_divide_Table_2";
            this.gv_divide_Table_2.OptionsView.ShowGroupPanel = false;
            this.gv_divide_Table_2.OptionsView.ShowIndicator = false;
            this.gv_divide_Table_2.RowHeight = 35;
            this.gv_divide_Table_2.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gv_divide_Table_2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gv_divide_Table_2_CustomDrawCell);
            // 
            // __STT
            // 
            this.@__STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.@__STT.AppearanceCell.Options.UseFont = true;
            this.@__STT.AppearanceCell.Options.UseTextOptions = true;
            this.@__STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__STT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.@__STT.AppearanceHeader.Options.UseFont = true;
            this.@__STT.AppearanceHeader.Options.UseTextOptions = true;
            this.@__STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__STT.Caption = "TT";
            this.@__STT.FieldName = "STT";
            this.@__STT.Name = "__STT";
            this.@__STT.OptionsColumn.AllowEdit = false;
            this.@__STT.OptionsColumn.AllowFocus = false;
            this.@__STT.Visible = true;
            this.@__STT.VisibleIndex = 0;
            this.@__STT.Width = 34;
            // 
            // __TEN_HANGHOA
            // 
            this.@__TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.@__TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.@__TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.@__TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.@__TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.@__TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__TEN_HANGHOA.Caption = "Hàng Hóa";
            this.@__TEN_HANGHOA.FieldName = "TEN_HANGHOA";
            this.@__TEN_HANGHOA.Name = "__TEN_HANGHOA";
            this.@__TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this.@__TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this.@__TEN_HANGHOA.Visible = true;
            this.@__TEN_HANGHOA.VisibleIndex = 1;
            this.@__TEN_HANGHOA.Width = 116;
            // 
            // __SOLUONG
            // 
            this.@__SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.@__SOLUONG.AppearanceCell.Options.UseFont = true;
            this.@__SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.@__SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.@__SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.@__SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.@__SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__SOLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__SOLUONG.Caption = "SL";
            this.@__SOLUONG.FieldName = "SOLUONG";
            this.@__SOLUONG.Name = "__SOLUONG";
            this.@__SOLUONG.OptionsColumn.AllowEdit = false;
            this.@__SOLUONG.OptionsColumn.AllowFocus = false;
            this.@__SOLUONG.Visible = true;
            this.@__SOLUONG.VisibleIndex = 2;
            this.@__SOLUONG.Width = 51;
            // 
            // __GIABAN
            // 
            this.@__GIABAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.@__GIABAN.AppearanceCell.Options.UseFont = true;
            this.@__GIABAN.AppearanceCell.Options.UseTextOptions = true;
            this.@__GIABAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.@__GIABAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__GIABAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.@__GIABAN.AppearanceHeader.Options.UseFont = true;
            this.@__GIABAN.AppearanceHeader.Options.UseTextOptions = true;
            this.@__GIABAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__GIABAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__GIABAN.Caption = "Đơn giá";
            this.@__GIABAN.DisplayFormat.FormatString = "{0:#,###0}";
            this.@__GIABAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.@__GIABAN.FieldName = "GIABAN";
            this.@__GIABAN.Name = "__GIABAN";
            this.@__GIABAN.OptionsColumn.AllowEdit = false;
            this.@__GIABAN.OptionsColumn.AllowFocus = false;
            this.@__GIABAN.Visible = true;
            this.@__GIABAN.VisibleIndex = 3;
            this.@__GIABAN.Width = 96;
            // 
            // __THANHTIEN
            // 
            this.@__THANHTIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.@__THANHTIEN.AppearanceCell.Options.UseFont = true;
            this.@__THANHTIEN.AppearanceCell.Options.UseTextOptions = true;
            this.@__THANHTIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.@__THANHTIEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__THANHTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.@__THANHTIEN.AppearanceHeader.Options.UseFont = true;
            this.@__THANHTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.@__THANHTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__THANHTIEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__THANHTIEN.Caption = "T Tiền";
            this.@__THANHTIEN.DisplayFormat.FormatString = "{0:#,###0}";
            this.@__THANHTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.@__THANHTIEN.FieldName = "THANHTIEN";
            this.@__THANHTIEN.Name = "__THANHTIEN";
            this.@__THANHTIEN.OptionsColumn.AllowEdit = false;
            this.@__THANHTIEN.OptionsColumn.AllowFocus = false;
            this.@__THANHTIEN.Visible = true;
            this.@__THANHTIEN.VisibleIndex = 4;
            this.@__THANHTIEN.Width = 104;
            // 
            // __MA_HOADON
            // 
            this.@__MA_HOADON.Caption = "MA_HOADON";
            this.@__MA_HOADON.FieldName = "MA_HOADON";
            this.@__MA_HOADON.Name = "__MA_HOADON";
            // 
            // __MA_HANGHOA
            // 
            this.@__MA_HANGHOA.Caption = "MA_HANGHOA";
            this.@__MA_HANGHOA.FieldName = "MA_HANGHOA";
            this.@__MA_HANGHOA.Name = "__MA_HANGHOA";
            this.@__MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this.@__MA_HANGHOA.OptionsColumn.AllowFocus = false;
            // 
            // __THUE
            // 
            this.@__THUE.Caption = "THUE";
            this.@__THUE.FieldName = "THUE";
            this.@__THUE.Name = "__THUE";
            // 
            // __CHIETKHAU
            // 
            this.@__CHIETKHAU.Caption = "CHIETKHAU";
            this.@__CHIETKHAU.FieldName = "CHIETKHAU";
            this.@__CHIETKHAU.Name = "__CHIETKHAU";
            // 
            // __IS_COMBO
            // 
            this.@__IS_COMBO.Caption = "__IS_COMBO";
            this.@__IS_COMBO.FieldName = "IS_COMBO";
            this.@__IS_COMBO.Name = "__IS_COMBO";
            // 
            // pnInlaihoadon
            // 
            this.pnInlaihoadon.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnInlaihoadon.Controls.Add(this.panelControl53);
            this.pnInlaihoadon.Controls.Add(this.panelControl52);
            this.pnInlaihoadon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnInlaihoadon.Location = new System.Drawing.Point(0, 0);
            this.pnInlaihoadon.Name = "pnInlaihoadon";
            this.pnInlaihoadon.Size = new System.Drawing.Size(623, 405);
            this.pnInlaihoadon.TabIndex = 46;
            // 
            // panelControl53
            // 
            this.panelControl53.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl53.Controls.Add(this.gr_billPaymented);
            this.panelControl53.Controls.Add(this.panelControl2);
            this.panelControl53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl53.Location = new System.Drawing.Point(0, 0);
            this.panelControl53.Name = "panelControl53";
            this.panelControl53.Size = new System.Drawing.Size(583, 405);
            this.panelControl53.TabIndex = 2;
            // 
            // gr_billPaymented
            // 
            this.gr_billPaymented.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gr_billPaymented.Location = new System.Drawing.Point(0, 43);
            this.gr_billPaymented.MainView = this.gvBillPaymented;
            this.gr_billPaymented.Name = "gr_billPaymented";
            this.gr_billPaymented.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1,
            this.repositoryItemButtonEdit2,
            this.repositoryItemButtonEdit3,
            this.repositoryItemButtonEdit4});
            this.gr_billPaymented.Size = new System.Drawing.Size(583, 362);
            this.gr_billPaymented.TabIndex = 4;
            this.gr_billPaymented.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvBillPaymented});
            // 
            // gvBillPaymented
            // 
            this.gvBillPaymented.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.gvBillPaymented.Appearance.FocusedRow.Options.UseFont = true;
            this.gvBillPaymented.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.gvBillPaymented.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvBillPaymented.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.75F);
            this.gvBillPaymented.Appearance.Row.Options.UseFont = true;
            this.gvBillPaymented.ColumnPanelRowHeight = 45;
            this.gvBillPaymented.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SSTT,
            this.MA_HOADON,
            this.NGAYTAO,
            this.TEN_KHUVUC,
            this.gridColumn1,
            this.TEN_BAN,
            this.XEM,
            this.IN,
            this.HUY,
            this.LAYLAI});
            this.gvBillPaymented.GridControl = this.gr_billPaymented;
            this.gvBillPaymented.Name = "gvBillPaymented";
            this.gvBillPaymented.OptionsView.EnableAppearanceEvenRow = true;
            this.gvBillPaymented.OptionsView.ShowGroupPanel = false;
            this.gvBillPaymented.OptionsView.ShowIndicator = false;
            this.gvBillPaymented.RowHeight = 45;
            this.gvBillPaymented.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gvBillPaymented.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvBillPaymented_RowCellClick);
            this.gvBillPaymented.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gvBillPaymented_CustomDrawCell);
            // 
            // SSTT
            // 
            this.SSTT.AppearanceCell.Options.UseTextOptions = true;
            this.SSTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SSTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SSTT.AppearanceHeader.Options.UseFont = true;
            this.SSTT.AppearanceHeader.Options.UseTextOptions = true;
            this.SSTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SSTT.Caption = "STT";
            this.SSTT.FieldName = "SSTT";
            this.SSTT.MinWidth = 40;
            this.SSTT.Name = "SSTT";
            this.SSTT.OptionsColumn.AllowEdit = false;
            this.SSTT.OptionsColumn.AllowFocus = false;
            this.SSTT.OptionsFilter.AllowAutoFilter = false;
            this.SSTT.OptionsFilter.AllowFilter = false;
            this.SSTT.Visible = true;
            this.SSTT.VisibleIndex = 0;
            this.SSTT.Width = 40;
            // 
            // MA_HOADON
            // 
            this.MA_HOADON.AppearanceCell.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_HOADON.AppearanceHeader.Options.UseFont = true;
            this.MA_HOADON.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.Caption = "Mã Hóa Đơn";
            this.MA_HOADON.FieldName = "MA_HOADON";
            this.MA_HOADON.MinWidth = 135;
            this.MA_HOADON.Name = "MA_HOADON";
            this.MA_HOADON.OptionsColumn.AllowEdit = false;
            this.MA_HOADON.OptionsColumn.AllowFocus = false;
            this.MA_HOADON.Visible = true;
            this.MA_HOADON.VisibleIndex = 1;
            this.MA_HOADON.Width = 135;
            // 
            // NGAYTAO
            // 
            this.NGAYTAO.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYTAO.AppearanceHeader.Options.UseFont = true;
            this.NGAYTAO.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.Caption = "Ngày Tạo";
            this.NGAYTAO.DisplayFormat.FormatString = "{0:dd/MM/yyyy HH:mm}";
            this.NGAYTAO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYTAO.FieldName = "NGAYTAO";
            this.NGAYTAO.MinWidth = 135;
            this.NGAYTAO.Name = "NGAYTAO";
            this.NGAYTAO.OptionsColumn.AllowEdit = false;
            this.NGAYTAO.OptionsColumn.AllowFocus = false;
            this.NGAYTAO.Visible = true;
            this.NGAYTAO.VisibleIndex = 2;
            this.NGAYTAO.Width = 135;
            // 
            // TEN_KHUVUC
            // 
            this.TEN_KHUVUC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_KHUVUC.AppearanceHeader.Options.UseFont = true;
            this.TEN_KHUVUC.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_KHUVUC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_KHUVUC.Caption = "Khu vực";
            this.TEN_KHUVUC.FieldName = "TEN_KHUVUC";
            this.TEN_KHUVUC.MinWidth = 70;
            this.TEN_KHUVUC.Name = "TEN_KHUVUC";
            this.TEN_KHUVUC.OptionsColumn.AllowEdit = false;
            this.TEN_KHUVUC.OptionsColumn.AllowFocus = false;
            this.TEN_KHUVUC.OptionsFilter.AllowAutoFilter = false;
            this.TEN_KHUVUC.OptionsFilter.AllowFilter = false;
            this.TEN_KHUVUC.Width = 70;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "Tổng Tiền";
            this.gridColumn1.DisplayFormat.FormatString = "N0";
            this.gridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn1.FieldName = "TONGTIEN";
            this.gridColumn1.MinWidth = 70;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 3;
            this.gridColumn1.Width = 70;
            // 
            // TEN_BAN
            // 
            this.TEN_BAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_BAN.AppearanceHeader.Options.UseFont = true;
            this.TEN_BAN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_BAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_BAN.Caption = "Bàn";
            this.TEN_BAN.FieldName = "TEN_BAN";
            this.TEN_BAN.MinWidth = 70;
            this.TEN_BAN.Name = "TEN_BAN";
            this.TEN_BAN.OptionsColumn.AllowEdit = false;
            this.TEN_BAN.OptionsColumn.AllowFocus = false;
            this.TEN_BAN.OptionsFilter.AllowAutoFilter = false;
            this.TEN_BAN.OptionsFilter.AllowFilter = false;
            this.TEN_BAN.Width = 70;
            // 
            // XEM
            // 
            this.XEM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.XEM.AppearanceHeader.Options.UseFont = true;
            this.XEM.AppearanceHeader.Options.UseTextOptions = true;
            this.XEM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.XEM.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.XEM.Caption = "Xem";
            this.XEM.ColumnEdit = this.repositoryItemButtonEdit4;
            this.XEM.FieldName = "XEM";
            this.XEM.MaxWidth = 100;
            this.XEM.MinWidth = 60;
            this.XEM.Name = "XEM";
            this.XEM.OptionsColumn.AllowEdit = false;
            this.XEM.OptionsColumn.AllowFocus = false;
            this.XEM.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.XEM.Visible = true;
            this.XEM.VisibleIndex = 4;
            this.XEM.Width = 70;
            // 
            // repositoryItemButtonEdit4
            // 
            this.repositoryItemButtonEdit4.AutoHeight = false;
            this.repositoryItemButtonEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit4.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, "", null, null, true)});
            this.repositoryItemButtonEdit4.Name = "repositoryItemButtonEdit4";
            this.repositoryItemButtonEdit4.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // IN
            // 
            this.IN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IN.AppearanceHeader.Options.UseFont = true;
            this.IN.AppearanceHeader.Options.UseTextOptions = true;
            this.IN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.IN.Caption = "In";
            this.IN.ColumnEdit = this.repositoryItemButtonEdit1;
            this.IN.MaxWidth = 100;
            this.IN.MinWidth = 60;
            this.IN.Name = "IN";
            this.IN.OptionsColumn.AllowEdit = false;
            this.IN.OptionsColumn.AllowFocus = false;
            this.IN.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.IN.Visible = true;
            this.IN.VisibleIndex = 5;
            this.IN.Width = 70;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit1.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject18, "", null, null, true)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // HUY
            // 
            this.HUY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HUY.AppearanceHeader.Options.UseFont = true;
            this.HUY.AppearanceHeader.Options.UseTextOptions = true;
            this.HUY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HUY.Caption = "Hủy";
            this.HUY.ColumnEdit = this.repositoryItemButtonEdit2;
            this.HUY.MaxWidth = 100;
            this.HUY.MinWidth = 60;
            this.HUY.Name = "HUY";
            this.HUY.OptionsColumn.AllowEdit = false;
            this.HUY.OptionsColumn.AllowFocus = false;
            this.HUY.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.HUY.Visible = true;
            this.HUY.VisibleIndex = 6;
            this.HUY.Width = 70;
            // 
            // repositoryItemButtonEdit2
            // 
            this.repositoryItemButtonEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit2.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject19, "", null, null, true)});
            this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
            this.repositoryItemButtonEdit2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // LAYLAI
            // 
            this.LAYLAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LAYLAI.AppearanceHeader.Options.UseFont = true;
            this.LAYLAI.AppearanceHeader.Options.UseTextOptions = true;
            this.LAYLAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LAYLAI.Caption = "Lấy lại HĐ";
            this.LAYLAI.ColumnEdit = this.repositoryItemButtonEdit3;
            this.LAYLAI.MaxWidth = 100;
            this.LAYLAI.MinWidth = 60;
            this.LAYLAI.Name = "LAYLAI";
            this.LAYLAI.OptionsColumn.AllowEdit = false;
            this.LAYLAI.OptionsColumn.AllowFocus = false;
            this.LAYLAI.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.LAYLAI.Visible = true;
            this.LAYLAI.VisibleIndex = 7;
            this.LAYLAI.Width = 70;
            // 
            // repositoryItemButtonEdit3
            // 
            this.repositoryItemButtonEdit3.AutoHeight = false;
            this.repositoryItemButtonEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit3.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject20, "", null, null, true)});
            this.repositoryItemButtonEdit3.Name = "repositoryItemButtonEdit3";
            this.repositoryItemButtonEdit3.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.txtMaHoaDon);
            this.panelControl2.Controls.Add(this.btnHuy);
            this.panelControl2.Controls.Add(this.lb_maKH);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(583, 43);
            this.panelControl2.TabIndex = 3;
            // 
            // txtMaHoaDon
            // 
            this.txtMaHoaDon.EditValue = "";
            this.txtMaHoaDon.Location = new System.Drawing.Point(87, 7);
            this.txtMaHoaDon.Name = "txtMaHoaDon";
            this.txtMaHoaDon.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtMaHoaDon.Properties.Appearance.Options.UseFont = true;
            this.txtMaHoaDon.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMaHoaDon.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.txtMaHoaDon.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMaHoaDon.Size = new System.Drawing.Size(215, 30);
            this.txtMaHoaDon.TabIndex = 14;
            // 
            // btnHuy
            // 
            this.btnHuy.AllowFocus = false;
            this.btnHuy.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuy.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnHuy.Appearance.Options.UseFont = true;
            this.btnHuy.Appearance.Options.UseForeColor = true;
            this.btnHuy.Location = new System.Drawing.Point(308, 7);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(76, 30);
            this.btnHuy.TabIndex = 16;
            this.btnHuy.Text = "Hủy";
            this.btnHuy.Click += new System.EventHandler(this.btnHuy_Click);
            // 
            // lb_maKH
            // 
            this.lb_maKH.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_maKH.Location = new System.Drawing.Point(6, 14);
            this.lb_maKH.Name = "lb_maKH";
            this.lb_maKH.Size = new System.Drawing.Size(75, 16);
            this.lb_maKH.TabIndex = 15;
            this.lb_maKH.Text = "Mã hóa đơn";
            // 
            // panelControl52
            // 
            this.panelControl52.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl52.Controls.Add(this.labelControl43);
            this.panelControl52.Controls.Add(this.lbTongSoRow);
            this.panelControl52.Controls.Add(this.labelControl45);
            this.panelControl52.Controls.Add(this.btn_upbill);
            this.panelControl52.Controls.Add(this.btnDowBill);
            this.panelControl52.Controls.Add(this.btnDowLastBill);
            this.panelControl52.Controls.Add(this.btn_UpTopBill);
            this.panelControl52.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl52.Location = new System.Drawing.Point(583, 0);
            this.panelControl52.Name = "panelControl52";
            this.panelControl52.Size = new System.Drawing.Size(40, 405);
            this.panelControl52.TabIndex = 1;
            // 
            // labelControl43
            // 
            this.labelControl43.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl43.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl43.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl43.Location = new System.Drawing.Point(9, 190);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(20, 13);
            this.labelControl43.TabIndex = 14;
            this.labelControl43.Text = "----";
            // 
            // lbTongSoRow
            // 
            this.lbTongSoRow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTongSoRow.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbTongSoRow.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbTongSoRow.Location = new System.Drawing.Point(13, 204);
            this.lbTongSoRow.Name = "lbTongSoRow";
            this.lbTongSoRow.Size = new System.Drawing.Size(14, 13);
            this.lbTongSoRow.TabIndex = 13;
            this.lbTongSoRow.Text = "00";
            // 
            // labelControl45
            // 
            this.labelControl45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl45.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl45.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl45.Location = new System.Drawing.Point(13, 176);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(14, 13);
            this.labelControl45.TabIndex = 12;
            this.labelControl45.Text = "00";
            // 
            // btn_upbill
            // 
            this.btn_upbill.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_upbill.Image = ((System.Drawing.Image)(resources.GetObject("btn_upbill.Image")));
            this.btn_upbill.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_upbill.Location = new System.Drawing.Point(0, 80);
            this.btn_upbill.Name = "btn_upbill";
            this.btn_upbill.Size = new System.Drawing.Size(40, 80);
            this.btn_upbill.TabIndex = 11;
            this.btn_upbill.Click += new System.EventHandler(this.btn_upbill_Click);
            // 
            // btnDowBill
            // 
            this.btnDowBill.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnDowBill.Image = ((System.Drawing.Image)(resources.GetObject("btnDowBill.Image")));
            this.btnDowBill.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDowBill.Location = new System.Drawing.Point(0, 245);
            this.btnDowBill.Name = "btnDowBill";
            this.btnDowBill.Size = new System.Drawing.Size(40, 80);
            this.btnDowBill.TabIndex = 10;
            this.btnDowBill.Click += new System.EventHandler(this.btnDowBill_Click);
            // 
            // btnDowLastBill
            // 
            this.btnDowLastBill.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnDowLastBill.Image = ((System.Drawing.Image)(resources.GetObject("btnDowLastBill.Image")));
            this.btnDowLastBill.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDowLastBill.Location = new System.Drawing.Point(0, 325);
            this.btnDowLastBill.Name = "btnDowLastBill";
            this.btnDowLastBill.Size = new System.Drawing.Size(40, 80);
            this.btnDowLastBill.TabIndex = 9;
            this.btnDowLastBill.Text = "simpleButton4";
            this.btnDowLastBill.Click += new System.EventHandler(this.btnDowLastBill_Click);
            // 
            // btn_UpTopBill
            // 
            this.btn_UpTopBill.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_UpTopBill.Image = ((System.Drawing.Image)(resources.GetObject("btn_UpTopBill.Image")));
            this.btn_UpTopBill.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_UpTopBill.Location = new System.Drawing.Point(0, 0);
            this.btn_UpTopBill.Name = "btn_UpTopBill";
            this.btn_UpTopBill.Size = new System.Drawing.Size(40, 80);
            this.btn_UpTopBill.TabIndex = 8;
            this.btn_UpTopBill.Text = "simpleButton1";
            this.btn_UpTopBill.Click += new System.EventHandler(this.btn_UpTopBill_Click);
            // 
            // pnWapperDeletePlusBut
            // 
            this.pnWapperDeletePlusBut.Controls.Add(this.btnComboOption);
            this.pnWapperDeletePlusBut.Controls.Add(this.btnDeleteAll);
            this.pnWapperDeletePlusBut.Controls.Add(this.btn_Cong);
            this.pnWapperDeletePlusBut.Controls.Add(this.btn_xoa);
            this.pnWapperDeletePlusBut.Controls.Add(this.btn_Tru);
            this.pnWapperDeletePlusBut.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnWapperDeletePlusBut.Location = new System.Drawing.Point(2, 407);
            this.pnWapperDeletePlusBut.Name = "pnWapperDeletePlusBut";
            this.pnWapperDeletePlusBut.Size = new System.Drawing.Size(623, 33);
            this.pnWapperDeletePlusBut.TabIndex = 44;
            this.pnWapperDeletePlusBut.Visible = false;
            // 
            // btnComboOption
            // 
            this.btnComboOption.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.btnComboOption.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnComboOption.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnComboOption.Appearance.Options.UseFont = true;
            this.btnComboOption.Appearance.Options.UseForeColor = true;
            this.btnComboOption.Location = new System.Drawing.Point(126, 2);
            this.btnComboOption.Name = "btnComboOption";
            this.btnComboOption.Size = new System.Drawing.Size(116, 28);
            this.btnComboOption.TabIndex = 10;
            this.btnComboOption.Text = "Tùy chọn";
            this.btnComboOption.Click += new System.EventHandler(this.btnComboOption_Click);
            // 
            // btnDeleteAll
            // 
            this.btnDeleteAll.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDeleteAll.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteAll.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnDeleteAll.Appearance.Options.UseFont = true;
            this.btnDeleteAll.Appearance.Options.UseForeColor = true;
            this.btnDeleteAll.Location = new System.Drawing.Point(4, 2);
            this.btnDeleteAll.Name = "btnDeleteAll";
            this.btnDeleteAll.Size = new System.Drawing.Size(116, 28);
            this.btnDeleteAll.TabIndex = 9;
            this.btnDeleteAll.Text = "Xóa tất cả";
            this.btnDeleteAll.Click += new System.EventHandler(this.btnDeleteAll_Click);
            // 
            // btn_Cong
            // 
            this.btn_Cong.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Cong.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Cong.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btn_Cong.Appearance.Options.UseFont = true;
            this.btn_Cong.Appearance.Options.UseForeColor = true;
            this.btn_Cong.Location = new System.Drawing.Point(390, 2);
            this.btn_Cong.Name = "btn_Cong";
            this.btn_Cong.Size = new System.Drawing.Size(109, 28);
            this.btn_Cong.TabIndex = 6;
            this.btn_Cong.Text = "+";
            this.btn_Cong.Click += new System.EventHandler(this.btn_Cong_Click);
            this.btn_Cong.Leave += new System.EventHandler(this.btn_Cong_Leave);
            // 
            // btn_xoa
            // 
            this.btn_xoa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_xoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_xoa.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btn_xoa.Appearance.Options.UseFont = true;
            this.btn_xoa.Appearance.Options.UseForeColor = true;
            this.btn_xoa.Location = new System.Drawing.Point(271, 2);
            this.btn_xoa.Name = "btn_xoa";
            this.btn_xoa.Size = new System.Drawing.Size(116, 28);
            this.btn_xoa.TabIndex = 8;
            this.btn_xoa.Text = "Xóa";
            this.btn_xoa.Click += new System.EventHandler(this.btn_xoa_Click);
            this.btn_xoa.Leave += new System.EventHandler(this.btn_xoa_Leave);
            // 
            // btn_Tru
            // 
            this.btn_Tru.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Tru.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Tru.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btn_Tru.Appearance.Options.UseFont = true;
            this.btn_Tru.Appearance.Options.UseForeColor = true;
            this.btn_Tru.Location = new System.Drawing.Point(505, 2);
            this.btn_Tru.Name = "btn_Tru";
            this.btn_Tru.Size = new System.Drawing.Size(113, 28);
            this.btn_Tru.TabIndex = 7;
            this.btn_Tru.Text = "-";
            this.btn_Tru.Click += new System.EventHandler(this.btn_Tru_Click);
            this.btn_Tru.Leave += new System.EventHandler(this.btn_Tru_Leave);
            // 
            // pn_Banhang
            // 
            this.pn_Banhang.Appearance.BackColor = System.Drawing.Color.MediumAquamarine;
            this.pn_Banhang.Appearance.Options.UseBackColor = true;
            this.pn_Banhang.Controls.Add(this.pnBanHang);
            this.pn_Banhang.Controls.Add(this.pnMaHoaDonTraHang);
            this.pn_Banhang.Controls.Add(this.pnNghiepVu_ThuNgan);
            this.pn_Banhang.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pn_Banhang.Location = new System.Drawing.Point(2, 440);
            this.pn_Banhang.Name = "pn_Banhang";
            this.pn_Banhang.Size = new System.Drawing.Size(623, 256);
            this.pn_Banhang.TabIndex = 35;
            // 
            // pnBanHang
            // 
            this.pnBanHang.Controls.Add(this.btnLaytrongluong);
            this.pnBanHang.Controls.Add(this.lbdiscount);
            this.pnBanHang.Controls.Add(this.labelControl35);
            this.pnBanHang.Controls.Add(this.labelControl33);
            this.pnBanHang.Controls.Add(this.txtdiscount);
            this.pnBanHang.Controls.Add(this.lbTienTruocThue);
            this.pnBanHang.Controls.Add(this.labelControl20);
            this.pnBanHang.Controls.Add(this.labelThueVAT);
            this.pnBanHang.Controls.Add(this.label_GiamGia);
            this.pnBanHang.Controls.Add(this.labelControl6);
            this.pnBanHang.Controls.Add(this.labelControl2);
            this.pnBanHang.Controls.Add(this.txt_soluong);
            this.pnBanHang.Controls.Add(this.labelControl3);
            this.pnBanHang.Controls.Add(this.labelControl4);
            this.pnBanHang.Controls.Add(this.lb_tongcong);
            this.pnBanHang.Controls.Add(this.txt_Mahang);
            this.pnBanHang.Controls.Add(this.labelControl5);
            this.pnBanHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnBanHang.Location = new System.Drawing.Point(303, 2);
            this.pnBanHang.Name = "pnBanHang";
            this.pnBanHang.Size = new System.Drawing.Size(318, 252);
            this.pnBanHang.TabIndex = 12;
            // 
            // btnLaytrongluong
            // 
            this.btnLaytrongluong.AllowFocus = false;
            this.btnLaytrongluong.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLaytrongluong.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLaytrongluong.Appearance.Options.UseFont = true;
            this.btnLaytrongluong.Location = new System.Drawing.Point(272, 6);
            this.btnLaytrongluong.Name = "btnLaytrongluong";
            this.btnLaytrongluong.Size = new System.Drawing.Size(41, 30);
            this.btnLaytrongluong.TabIndex = 21;
            this.btnLaytrongluong.Text = "Kg";
            this.btnLaytrongluong.Click += new System.EventHandler(this.btnLaytrongluong_Click);
            // 
            // lbdiscount
            // 
            this.lbdiscount.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lbdiscount.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lbdiscount.Location = new System.Drawing.Point(109, 142);
            this.lbdiscount.Name = "lbdiscount";
            this.lbdiscount.Size = new System.Drawing.Size(8, 16);
            this.lbdiscount.TabIndex = 11;
            this.lbdiscount.Text = "0";
            // 
            // labelControl35
            // 
            this.labelControl35.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl35.Location = new System.Drawing.Point(11, 145);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(63, 13);
            this.labelControl35.TabIndex = 10;
            this.labelControl35.Text = "Chiết Khấu:";
            // 
            // labelControl33
            // 
            this.labelControl33.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl33.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl33.Location = new System.Drawing.Point(11, 85);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(60, 13);
            this.labelControl33.TabIndex = 8;
            this.labelControl33.Text = "Chiết Khấu";
            // 
            // txtdiscount
            // 
            this.txtdiscount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtdiscount.EditValue = "0";
            this.txtdiscount.Location = new System.Drawing.Point(82, 77);
            this.txtdiscount.MinimumSize = new System.Drawing.Size(100, 30);
            this.txtdiscount.Name = "txtdiscount";
            this.txtdiscount.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtdiscount.Properties.Appearance.Options.UseFont = true;
            this.txtdiscount.Properties.Appearance.Options.UseTextOptions = true;
            this.txtdiscount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtdiscount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtdiscount.Size = new System.Drawing.Size(231, 26);
            this.txtdiscount.TabIndex = 9;
            this.txtdiscount.Click += new System.EventHandler(this.txtdiscount_Click);
            this.txtdiscount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtdiscount_KeyDown);
            this.txtdiscount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtdiscount_KeyPress);
            this.txtdiscount.Leave += new System.EventHandler(this.txtdiscount_Leave);
            // 
            // lbTienTruocThue
            // 
            this.lbTienTruocThue.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lbTienTruocThue.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lbTienTruocThue.Location = new System.Drawing.Point(109, 188);
            this.lbTienTruocThue.Name = "lbTienTruocThue";
            this.lbTienTruocThue.Size = new System.Drawing.Size(8, 16);
            this.lbTienTruocThue.TabIndex = 7;
            this.lbTienTruocThue.Text = "0";
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl20.Location = new System.Drawing.Point(11, 191);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(84, 13);
            this.labelControl20.TabIndex = 6;
            this.labelControl20.Text = "TC Trước Thuế:";
            // 
            // labelThueVAT
            // 
            this.labelThueVAT.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelThueVAT.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.labelThueVAT.Location = new System.Drawing.Point(109, 165);
            this.labelThueVAT.Name = "labelThueVAT";
            this.labelThueVAT.Size = new System.Drawing.Size(8, 16);
            this.labelThueVAT.TabIndex = 5;
            this.labelThueVAT.Text = "0";
            // 
            // label_GiamGia
            // 
            this.label_GiamGia.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.label_GiamGia.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label_GiamGia.Location = new System.Drawing.Point(109, 119);
            this.label_GiamGia.Name = "label_GiamGia";
            this.label_GiamGia.Size = new System.Drawing.Size(8, 16);
            this.label_GiamGia.TabIndex = 4;
            this.label_GiamGia.Text = "0";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl6.Location = new System.Drawing.Point(11, 122);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(53, 13);
            this.labelControl6.TabIndex = 3;
            this.labelControl6.Text = "Giảm Giá:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl2.Location = new System.Drawing.Point(11, 168);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(56, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Thuế VAT:";
            // 
            // txt_soluong
            // 
            this.txt_soluong.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_soluong.EditValue = "1";
            this.txt_soluong.Location = new System.Drawing.Point(82, 6);
            this.txt_soluong.MinimumSize = new System.Drawing.Size(100, 30);
            this.txt_soluong.Name = "txt_soluong";
            this.txt_soluong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt_soluong.Properties.Appearance.Options.UseFont = true;
            this.txt_soluong.Properties.Appearance.Options.UseTextOptions = true;
            this.txt_soluong.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txt_soluong.Properties.EditFormat.FormatString = "G";
            this.txt_soluong.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txt_soluong.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.None;
            this.txt_soluong.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_soluong.Size = new System.Drawing.Size(184, 26);
            this.txt_soluong.TabIndex = 0;
            this.txt_soluong.TextChanged += new System.EventHandler(this.txt_soluong_TextChanged);
            this.txt_soluong.Click += new System.EventHandler(this.txt_soluong_Click);
            this.txt_soluong.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_soluong_KeyDown);
            this.txt_soluong.Leave += new System.EventHandler(this.txt_soluong_Leave);
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(11, 224);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(98, 19);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "Tổng Cộng: ";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(11, 50);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(49, 13);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "Mã Hàng";
            // 
            // lb_tongcong
            // 
            this.lb_tongcong.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_tongcong.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_tongcong.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lb_tongcong.Location = new System.Drawing.Point(109, 219);
            this.lb_tongcong.Name = "lb_tongcong";
            this.lb_tongcong.Size = new System.Drawing.Size(13, 24);
            this.lb_tongcong.TabIndex = 1;
            this.lb_tongcong.Text = "0";
            // 
            // txt_Mahang
            // 
            this.txt_Mahang.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_Mahang.Location = new System.Drawing.Point(82, 40);
            this.txt_Mahang.MinimumSize = new System.Drawing.Size(100, 30);
            this.txt_Mahang.Name = "txt_Mahang";
            this.txt_Mahang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt_Mahang.Properties.Appearance.Options.UseFont = true;
            this.txt_Mahang.Properties.Appearance.Options.UseTextOptions = true;
            this.txt_Mahang.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txt_Mahang.Size = new System.Drawing.Size(231, 26);
            this.txt_Mahang.TabIndex = 1;
            this.txt_Mahang.Click += new System.EventHandler(this.txt_Mahang_Click);
            this.txt_Mahang.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_Mahang_KeyDown);
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(11, 16);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(52, 13);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "Số Lượng";
            // 
            // pnMaHoaDonTraHang
            // 
            this.pnMaHoaDonTraHang.Controls.Add(this.btnTra);
            this.pnMaHoaDonTraHang.Controls.Add(this.txtMaHoaDonTra);
            this.pnMaHoaDonTraHang.Controls.Add(this.labelControl11);
            this.pnMaHoaDonTraHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMaHoaDonTraHang.Location = new System.Drawing.Point(303, 2);
            this.pnMaHoaDonTraHang.Name = "pnMaHoaDonTraHang";
            this.pnMaHoaDonTraHang.Size = new System.Drawing.Size(318, 252);
            this.pnMaHoaDonTraHang.TabIndex = 11;
            // 
            // btnTra
            // 
            this.btnTra.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTra.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btnTra.Appearance.Options.UseFont = true;
            this.btnTra.Location = new System.Drawing.Point(7, 76);
            this.btnTra.Name = "btnTra";
            this.btnTra.Size = new System.Drawing.Size(308, 37);
            this.btnTra.TabIndex = 3;
            this.btnTra.Text = "Trả hàng";
            this.btnTra.Click += new System.EventHandler(this.btnTra_Click);
            // 
            // txtMaHoaDonTra
            // 
            this.txtMaHoaDonTra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaHoaDonTra.Location = new System.Drawing.Point(7, 40);
            this.txtMaHoaDonTra.Name = "txtMaHoaDonTra";
            this.txtMaHoaDonTra.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtMaHoaDonTra.Properties.Appearance.Options.UseFont = true;
            this.txtMaHoaDonTra.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMaHoaDonTra.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMaHoaDonTra.Size = new System.Drawing.Size(308, 30);
            this.txtMaHoaDonTra.TabIndex = 0;
            this.txtMaHoaDonTra.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMaHoaDonTra_KeyDown);
            // 
            // labelControl11
            // 
            this.labelControl11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl11.Location = new System.Drawing.Point(76, 9);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(158, 17);
            this.labelControl11.TabIndex = 1;
            this.labelControl11.Text = "Nhập mã hóa đơn bán:";
            // 
            // pnNghiepVu_ThuNgan
            // 
            this.pnNghiepVu_ThuNgan.Controls.Add(this.pnDacthu);
            this.pnNghiepVu_ThuNgan.Controls.Add(this.pnThuNgan);
            this.pnNghiepVu_ThuNgan.Controls.Add(this.pnNghiepvu);
            this.pnNghiepVu_ThuNgan.Controls.Add(this.panelControl9);
            this.pnNghiepVu_ThuNgan.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnNghiepVu_ThuNgan.Location = new System.Drawing.Point(2, 2);
            this.pnNghiepVu_ThuNgan.Name = "pnNghiepVu_ThuNgan";
            this.pnNghiepVu_ThuNgan.Size = new System.Drawing.Size(301, 252);
            this.pnNghiepVu_ThuNgan.TabIndex = 13;
            // 
            // pnDacthu
            // 
            this.pnDacthu.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnDacthu.Controls.Add(this.btnChonbanggia);
            this.pnDacthu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnDacthu.Location = new System.Drawing.Point(2, 38);
            this.pnDacthu.Name = "pnDacthu";
            this.pnDacthu.Size = new System.Drawing.Size(297, 212);
            this.pnDacthu.TabIndex = 25;
            // 
            // btnChonbanggia
            // 
            this.btnChonbanggia.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnChonbanggia.Appearance.Options.UseFont = true;
            this.btnChonbanggia.Appearance.Options.UseTextOptions = true;
            this.btnChonbanggia.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnChonbanggia.Location = new System.Drawing.Point(6, 8);
            this.btnChonbanggia.Name = "btnChonbanggia";
            this.btnChonbanggia.Size = new System.Drawing.Size(90, 62);
            this.btnChonbanggia.TabIndex = 29;
            this.btnChonbanggia.Text = "Chọn bảng giá";
            this.btnChonbanggia.Click += new System.EventHandler(this.btnChonbanggia_Click);
            // 
            // pnThuNgan
            // 
            this.pnThuNgan.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnThuNgan.Controls.Add(this.btnTraHang);
            this.pnThuNgan.Controls.Add(this.btnLuuHDDoi);
            this.pnThuNgan.Controls.Add(this.btnLayHDDoi);
            this.pnThuNgan.Controls.Add(this.btn_InLaiHoaDon);
            this.pnThuNgan.Controls.Add(this.btnMoKet);
            this.pnThuNgan.Controls.Add(this.btnKetKa);
            this.pnThuNgan.Controls.Add(this.btn_logout);
            this.pnThuNgan.Controls.Add(this.btn_HuyHoaDon);
            this.pnThuNgan.Controls.Add(this.btnGiaBan2);
            this.pnThuNgan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnThuNgan.Location = new System.Drawing.Point(2, 38);
            this.pnThuNgan.Name = "pnThuNgan";
            this.pnThuNgan.Size = new System.Drawing.Size(297, 212);
            this.pnThuNgan.TabIndex = 24;
            // 
            // btnTraHang
            // 
            this.btnTraHang.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnTraHang.Appearance.Options.UseFont = true;
            this.btnTraHang.Location = new System.Drawing.Point(104, 142);
            this.btnTraHang.Name = "btnTraHang";
            this.btnTraHang.Size = new System.Drawing.Size(90, 62);
            this.btnTraHang.TabIndex = 41;
            this.btnTraHang.Text = "Trả hàng";
            this.btnTraHang.Click += new System.EventHandler(this.btnTraHang_Click);
            // 
            // btnLuuHDDoi
            // 
            this.btnLuuHDDoi.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnLuuHDDoi.Appearance.Options.UseFont = true;
            this.btnLuuHDDoi.Location = new System.Drawing.Point(200, 5);
            this.btnLuuHDDoi.Name = "btnLuuHDDoi";
            this.btnLuuHDDoi.Size = new System.Drawing.Size(90, 62);
            this.btnLuuHDDoi.TabIndex = 34;
            this.btnLuuHDDoi.Text = "Lưu HĐ đợi";
            this.btnLuuHDDoi.Click += new System.EventHandler(this.btnLuuHDDoi_Click);
            // 
            // btnLayHDDoi
            // 
            this.btnLayHDDoi.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnLayHDDoi.Appearance.Options.UseFont = true;
            this.btnLayHDDoi.Location = new System.Drawing.Point(200, 73);
            this.btnLayHDDoi.Name = "btnLayHDDoi";
            this.btnLayHDDoi.Size = new System.Drawing.Size(90, 62);
            this.btnLayHDDoi.TabIndex = 35;
            this.btnLayHDDoi.Text = "Lấy HĐ đợi";
            this.btnLayHDDoi.Click += new System.EventHandler(this.btnLayHDDoi_Click);
            // 
            // btn_InLaiHoaDon
            // 
            this.btn_InLaiHoaDon.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_InLaiHoaDon.Appearance.Options.UseFont = true;
            this.btn_InLaiHoaDon.Location = new System.Drawing.Point(104, 5);
            this.btn_InLaiHoaDon.Name = "btn_InLaiHoaDon";
            this.btn_InLaiHoaDon.Size = new System.Drawing.Size(90, 62);
            this.btn_InLaiHoaDon.TabIndex = 27;
            this.btn_InLaiHoaDon.Text = "In lại\r\nhóa đơn";
            this.btn_InLaiHoaDon.Click += new System.EventHandler(this.btn_InLaiHoaDon_Click);
            // 
            // btnMoKet
            // 
            this.btnMoKet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnMoKet.Appearance.Options.UseFont = true;
            this.btnMoKet.Location = new System.Drawing.Point(6, 5);
            this.btnMoKet.Name = "btnMoKet";
            this.btnMoKet.Size = new System.Drawing.Size(90, 62);
            this.btnMoKet.TabIndex = 22;
            this.btnMoKet.Text = "Mở két";
            this.btnMoKet.Click += new System.EventHandler(this.btnMoKet_Click);
            // 
            // btnKetKa
            // 
            this.btnKetKa.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnKetKa.Appearance.Options.UseFont = true;
            this.btnKetKa.Location = new System.Drawing.Point(6, 73);
            this.btnKetKa.Name = "btnKetKa";
            this.btnKetKa.Size = new System.Drawing.Size(90, 62);
            this.btnKetKa.TabIndex = 21;
            this.btnKetKa.Text = "Kết ca";
            this.btnKetKa.Click += new System.EventHandler(this.btnKetKa_Click);
            // 
            // btn_logout
            // 
            this.btn_logout.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_logout.Appearance.Options.UseFont = true;
            this.btn_logout.Location = new System.Drawing.Point(6, 142);
            this.btn_logout.Name = "btn_logout";
            this.btn_logout.Size = new System.Drawing.Size(90, 62);
            this.btn_logout.TabIndex = 20;
            this.btn_logout.Text = "Đóng";
            this.btn_logout.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // btn_HuyHoaDon
            // 
            this.btn_HuyHoaDon.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_HuyHoaDon.Appearance.Options.UseFont = true;
            this.btn_HuyHoaDon.Location = new System.Drawing.Point(104, 73);
            this.btn_HuyHoaDon.Name = "btn_HuyHoaDon";
            this.btn_HuyHoaDon.Size = new System.Drawing.Size(90, 62);
            this.btn_HuyHoaDon.TabIndex = 29;
            this.btn_HuyHoaDon.Text = "Hủy hóa đơn";
            this.btn_HuyHoaDon.Click += new System.EventHandler(this.btn_HuyHoaDon_Click);
            // 
            // btnGiaBan2
            // 
            this.btnGiaBan2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGiaBan2.Appearance.Options.UseFont = true;
            this.btnGiaBan2.Location = new System.Drawing.Point(200, 142);
            this.btnGiaBan2.Name = "btnGiaBan2";
            this.btnGiaBan2.Size = new System.Drawing.Size(90, 62);
            this.btnGiaBan2.TabIndex = 8;
            this.btnGiaBan2.Text = "Bán giá 2";
            this.btnGiaBan2.Click += new System.EventHandler(this.btnGiaBan2_Click);
            // 
            // pnNghiepvu
            // 
            this.pnNghiepvu.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnNghiepvu.Controls.Add(this.btnDong);
            this.pnNghiepvu.Controls.Add(this.btnBaoCaoTraHang);
            this.pnNghiepvu.Controls.Add(this.btnTraCuu);
            this.pnNghiepvu.Controls.Add(this.btnSuaGia);
            this.pnNghiepvu.Controls.Add(this.btnPhatThuong);
            this.pnNghiepvu.Controls.Add(this.btnThemKHTT);
            this.pnNghiepvu.Controls.Add(this.btnXemTonKho);
            this.pnNghiepvu.Controls.Add(this.btnBaoCaoBanHang);
            this.pnNghiepvu.Controls.Add(this.btnInVeOnline);
            this.pnNghiepvu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnNghiepvu.Location = new System.Drawing.Point(2, 38);
            this.pnNghiepvu.Name = "pnNghiepvu";
            this.pnNghiepvu.Size = new System.Drawing.Size(297, 212);
            this.pnNghiepvu.TabIndex = 23;
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Appearance.Options.UseTextOptions = true;
            this.btnDong.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnDong.Location = new System.Drawing.Point(6, 139);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(90, 62);
            this.btnDong.TabIndex = 42;
            this.btnDong.Text = "Thêm hàng hóa";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnBaoCaoTraHang
            // 
            this.btnBaoCaoTraHang.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnBaoCaoTraHang.Appearance.Options.UseFont = true;
            this.btnBaoCaoTraHang.Location = new System.Drawing.Point(104, 5);
            this.btnBaoCaoTraHang.Name = "btnBaoCaoTraHang";
            this.btnBaoCaoTraHang.Size = new System.Drawing.Size(90, 62);
            this.btnBaoCaoTraHang.TabIndex = 41;
            this.btnBaoCaoTraHang.Text = "BC Trả hàng";
            this.btnBaoCaoTraHang.Click += new System.EventHandler(this.btnBaoCaoTraHang_Click);
            // 
            // btnTraCuu
            // 
            this.btnTraCuu.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnTraCuu.Appearance.Options.UseFont = true;
            this.btnTraCuu.Location = new System.Drawing.Point(104, 71);
            this.btnTraCuu.Name = "btnTraCuu";
            this.btnTraCuu.Size = new System.Drawing.Size(90, 62);
            this.btnTraCuu.TabIndex = 40;
            this.btnTraCuu.Text = "Tra cứu KHTT";
            this.btnTraCuu.Click += new System.EventHandler(this.btnTraCuu_Click);
            // 
            // btnSuaGia
            // 
            this.btnSuaGia.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnSuaGia.Appearance.Options.UseFont = true;
            this.btnSuaGia.Location = new System.Drawing.Point(104, 139);
            this.btnSuaGia.Name = "btnSuaGia";
            this.btnSuaGia.Size = new System.Drawing.Size(90, 62);
            this.btnSuaGia.TabIndex = 32;
            this.btnSuaGia.Text = "Sửa Giá";
            this.btnSuaGia.Click += new System.EventHandler(this.btnSuaGia_Click);
            // 
            // btnPhatThuong
            // 
            this.btnPhatThuong.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnPhatThuong.Appearance.Options.UseFont = true;
            this.btnPhatThuong.Location = new System.Drawing.Point(6, 71);
            this.btnPhatThuong.Name = "btnPhatThuong";
            this.btnPhatThuong.Size = new System.Drawing.Size(90, 62);
            this.btnPhatThuong.TabIndex = 39;
            this.btnPhatThuong.Text = "Phát thưởng ";
            this.btnPhatThuong.Click += new System.EventHandler(this.btnPhatThuong_Click);
            // 
            // btnThemKHTT
            // 
            this.btnThemKHTT.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnThemKHTT.Appearance.Options.UseFont = true;
            this.btnThemKHTT.Location = new System.Drawing.Point(200, 71);
            this.btnThemKHTT.Name = "btnThemKHTT";
            this.btnThemKHTT.Size = new System.Drawing.Size(90, 62);
            this.btnThemKHTT.TabIndex = 38;
            this.btnThemKHTT.Text = "Thêm KHTT";
            this.btnThemKHTT.Click += new System.EventHandler(this.btnThemKHTT_Click);
            // 
            // btnXemTonKho
            // 
            this.btnXemTonKho.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnXemTonKho.Appearance.Options.UseFont = true;
            this.btnXemTonKho.Location = new System.Drawing.Point(200, 5);
            this.btnXemTonKho.Name = "btnXemTonKho";
            this.btnXemTonKho.Size = new System.Drawing.Size(90, 62);
            this.btnXemTonKho.TabIndex = 37;
            this.btnXemTonKho.Text = "Xem tồn kho";
            this.btnXemTonKho.Click += new System.EventHandler(this.btnXemTonKho_Click);
            // 
            // btnBaoCaoBanHang
            // 
            this.btnBaoCaoBanHang.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnBaoCaoBanHang.Appearance.Options.UseFont = true;
            this.btnBaoCaoBanHang.Location = new System.Drawing.Point(6, 5);
            this.btnBaoCaoBanHang.Name = "btnBaoCaoBanHang";
            this.btnBaoCaoBanHang.Size = new System.Drawing.Size(90, 62);
            this.btnBaoCaoBanHang.TabIndex = 36;
            this.btnBaoCaoBanHang.Text = "BC Bán hàng";
            this.btnBaoCaoBanHang.Click += new System.EventHandler(this.btnBaoCaoBanHang_Click);
            // 
            // btnInVeOnline
            // 
            this.btnInVeOnline.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnInVeOnline.Appearance.Options.UseFont = true;
            this.btnInVeOnline.Location = new System.Drawing.Point(200, 139);
            this.btnInVeOnline.Name = "btnInVeOnline";
            this.btnInVeOnline.Size = new System.Drawing.Size(90, 62);
            this.btnInVeOnline.TabIndex = 30;
            this.btnInVeOnline.Text = "In Bill Online";
            this.btnInVeOnline.Click += new System.EventHandler(this.btnInVeOnline_Click);
            // 
            // panelControl9
            // 
            this.panelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl9.Controls.Add(this.btn_Datthu);
            this.panelControl9.Controls.Add(this.btn_Nghiepvu);
            this.panelControl9.Controls.Add(this.btn_ThuNgan);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl9.Location = new System.Drawing.Point(2, 2);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(297, 36);
            this.panelControl9.TabIndex = 2;
            // 
            // btn_Datthu
            // 
            this.btn_Datthu.AllowFocus = false;
            this.btn_Datthu.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.btn_Datthu.Appearance.Options.UseFont = true;
            this.btn_Datthu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_Datthu.Location = new System.Drawing.Point(0, 0);
            this.btn_Datthu.Name = "btn_Datthu";
            this.btn_Datthu.Size = new System.Drawing.Size(93, 36);
            this.btn_Datthu.TabIndex = 4;
            this.btn_Datthu.Text = "Đặc Thù";
            this.btn_Datthu.Click += new System.EventHandler(this.btn_Datthu_Click);
            // 
            // btn_Nghiepvu
            // 
            this.btn_Nghiepvu.AllowFocus = false;
            this.btn_Nghiepvu.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.btn_Nghiepvu.Appearance.Options.UseFont = true;
            this.btn_Nghiepvu.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_Nghiepvu.Location = new System.Drawing.Point(93, 0);
            this.btn_Nghiepvu.Name = "btn_Nghiepvu";
            this.btn_Nghiepvu.Size = new System.Drawing.Size(107, 36);
            this.btn_Nghiepvu.TabIndex = 3;
            this.btn_Nghiepvu.Text = "Nghiệp Vụ";
            this.btn_Nghiepvu.Click += new System.EventHandler(this.btn_Nghiepvu_Click);
            // 
            // btn_ThuNgan
            // 
            this.btn_ThuNgan.AllowFocus = false;
            this.btn_ThuNgan.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.btn_ThuNgan.Appearance.Options.UseFont = true;
            this.btn_ThuNgan.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_ThuNgan.Location = new System.Drawing.Point(200, 0);
            this.btn_ThuNgan.Name = "btn_ThuNgan";
            this.btn_ThuNgan.Size = new System.Drawing.Size(97, 36);
            this.btn_ThuNgan.TabIndex = 0;
            this.btn_ThuNgan.Text = "Thu Ngân";
            this.btn_ThuNgan.Click += new System.EventHandler(this.btn_ThuNgan_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pnHeader
            // 
            this.pnHeader.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnHeader.Controls.Add(this.panelControl3);
            this.pnHeader.Controls.Add(this.panelControl1);
            this.pnHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnHeader.Location = new System.Drawing.Point(0, 0);
            this.pnHeader.Name = "pnHeader";
            this.pnHeader.Size = new System.Drawing.Size(1036, 70);
            this.pnHeader.TabIndex = 64;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.lb_Quay);
            this.panelControl3.Controls.Add(this.lb_Kho);
            this.panelControl3.Controls.Add(this.lbgiay);
            this.panelControl3.Controls.Add(this.lbCaBan);
            this.panelControl3.Controls.Add(this.lbGio);
            this.panelControl3.Controls.Add(this.lb_NhanVien);
            this.panelControl3.Controls.Add(this.lbtimeby);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(196, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(840, 70);
            this.panelControl3.TabIndex = 23;
            // 
            // lb_Quay
            // 
            this.lb_Quay.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_Quay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_Quay.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_Quay.Location = new System.Drawing.Point(15, 5);
            this.lb_Quay.Name = "lb_Quay";
            this.lb_Quay.Size = new System.Drawing.Size(41, 19);
            this.lb_Quay.TabIndex = 14;
            this.lb_Quay.Text = "Quầy";
            // 
            // lb_Kho
            // 
            this.lb_Kho.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_Kho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_Kho.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_Kho.Location = new System.Drawing.Point(15, 34);
            this.lb_Kho.Name = "lb_Kho";
            this.lb_Kho.Size = new System.Drawing.Size(41, 19);
            this.lb_Kho.TabIndex = 15;
            this.lb_Kho.Text = "Quầy";
            // 
            // lbgiay
            // 
            this.lbgiay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbgiay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbgiay.Location = new System.Drawing.Point(799, 27);
            this.lbgiay.Name = "lbgiay";
            this.lbgiay.Size = new System.Drawing.Size(18, 19);
            this.lbgiay.TabIndex = 19;
            this.lbgiay.Text = "00";
            // 
            // lbCaBan
            // 
            this.lbCaBan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCaBan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbCaBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbCaBan.Location = new System.Drawing.Point(732, 5);
            this.lbCaBan.Name = "lbCaBan";
            this.lbCaBan.Size = new System.Drawing.Size(94, 19);
            this.lbCaBan.TabIndex = 17;
            this.lbCaBan.Text = "21-12-2012";
            // 
            // lbGio
            // 
            this.lbGio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGio.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGio.Location = new System.Drawing.Point(747, 34);
            this.lbGio.Name = "lbGio";
            this.lbGio.Size = new System.Drawing.Size(46, 19);
            this.lbGio.TabIndex = 20;
            this.lbGio.Text = "00:00";
            // 
            // lb_NhanVien
            // 
            this.lb_NhanVien.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lb_NhanVien.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_NhanVien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_NhanVien.Location = new System.Drawing.Point(365, 5);
            this.lb_NhanVien.Name = "lb_NhanVien";
            this.lb_NhanVien.Size = new System.Drawing.Size(89, 19);
            this.lb_NhanVien.TabIndex = 18;
            this.lb_NhanVien.Text = "Nhân Viên:";
            // 
            // lbtimeby
            // 
            this.lbtimeby.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbtimeby.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbtimeby.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbtimeby.Location = new System.Drawing.Point(365, 42);
            this.lbtimeby.Name = "lbtimeby";
            this.lbtimeby.Size = new System.Drawing.Size(21, 19);
            this.lbtimeby.TabIndex = 16;
            this.lbtimeby.Text = "Ca";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pictureBox1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(196, 70);
            this.panelControl1.TabIndex = 22;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(8, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(177, 41);
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // bar5
            // 
            this.bar5.BarName = "Status bar";
            this.bar5.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar5.DockCol = 0;
            this.bar5.DockRow = 0;
            this.bar5.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar5.OptionsBar.AllowQuickCustomization = false;
            this.bar5.OptionsBar.DrawDragBorder = false;
            this.bar5.OptionsBar.UseWholeRow = true;
            this.bar5.Text = "Status bar";
            // 
            // Frm_BanHang_Mavach
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 768);
            this.Controls.Add(this.pn_WapperContent);
            this.Controls.Add(this.pnHeader);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "Frm_BanHang_Mavach";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frm_BanHang_Mavach_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_BanHang_Mavach_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pn_bigparent)).EndInit();
            this.pn_bigparent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_hanghoa)).EndInit();
            this.pn_hanghoa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_banhang)).EndInit();
            this.pal_banhang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_banhang1)).EndInit();
            this.pal_banhang1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palSoCot)).EndInit();
            this.palSoCot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palCot)).EndInit();
            this.palCot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palPhanTrang)).EndInit();
            this.palPhanTrang.ResumeLayout(false);
            this.palPhanTrang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSarech.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.palPhanNganh)).EndInit();
            this.palPhanNganh.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnCtrlNgNhPhNhom)).EndInit();
            this.pnCtrlNgNhPhNhom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_ChonNhom)).EndInit();
            this.gr_ChonNhom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            this.panelControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_banphim)).EndInit();
            this.pn_banphim.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_banphim_so)).EndInit();
            this.pn_banphim_so.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_banphim_chu)).EndInit();
            this.pn_banphim_chu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnViewProduct)).EndInit();
            this.pnViewProduct.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grCtrlDSHangMua)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gVDSHangMua)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Add)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Exc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Del)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrungHere)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDow)).EndInit();
            this.pnUpDow.ResumeLayout(false);
            this.pnUpDow.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_WapperContent)).EndInit();
            this.pn_WapperContent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_Luoi)).EndInit();
            this.pn_Luoi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnTachban)).EndInit();
            this.pnTachban.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_GroupTable)).EndInit();
            this.pn_GroupTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            this.panelControl15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_divide_Table_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_divide_Table_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_TableTow)).EndInit();
            this.pn_TableTow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_divide_Table_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_divide_Table_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnInlaihoadon)).EndInit();
            this.pnInlaihoadon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl53)).EndInit();
            this.panelControl53.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_billPaymented)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBillPaymented)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHoaDon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl52)).EndInit();
            this.panelControl52.ResumeLayout(false);
            this.panelControl52.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnWapperDeletePlusBut)).EndInit();
            this.pnWapperDeletePlusBut.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_Banhang)).EndInit();
            this.pn_Banhang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnBanHang)).EndInit();
            this.pnBanHang.ResumeLayout(false);
            this.pnBanHang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtdiscount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soluong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Mahang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnMaHoaDonTraHang)).EndInit();
            this.pnMaHoaDonTraHang.ResumeLayout(false);
            this.pnMaHoaDonTraHang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHoaDonTra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnNghiepVu_ThuNgan)).EndInit();
            this.pnNghiepVu_ThuNgan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnDacthu)).EndInit();
            this.pnDacthu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnThuNgan)).EndInit();
            this.pnThuNgan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnNghiepvu)).EndInit();
            this.pnNghiepvu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnHeader)).EndInit();
            this.pnHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pn_bigparent;
        private DevExpress.XtraEditors.XtraScrollableControl pnctrlParent;
        private DevExpress.XtraEditors.PanelControl palSoCot;
        private DevExpress.XtraEditors.SimpleButton btn_5cot;
        private DevExpress.XtraEditors.SimpleButton btn_Next;
        private DevExpress.XtraEditors.LabelControl lblPageMon;
        private DevExpress.XtraEditors.SimpleButton btn_Last;
        private DevExpress.XtraEditors.SimpleButton btn_First;
        private DevExpress.XtraEditors.SimpleButton btn_7cot;
        private DevExpress.XtraEditors.SimpleButton btn_Prev;
        private DevExpress.XtraEditors.PanelControl pn_banphim_so;
        private DevExpress.XtraEditors.SimpleButton btn_3;
        private DevExpress.XtraEditors.SimpleButton btn_2;
        private DevExpress.XtraEditors.SimpleButton btn_1;
        private DevExpress.XtraEditors.SimpleButton btn_ESC;
        private DevExpress.XtraEditors.SimpleButton btn_5;
        private DevExpress.XtraEditors.SimpleButton btn_4;
        private DevExpress.XtraEditors.SimpleButton btn_6;
        private DevExpress.XtraEditors.SimpleButton btn_9;
        private DevExpress.XtraEditors.SimpleButton btn_8;
        private DevExpress.XtraEditors.SimpleButton btn_7;
        private DevExpress.XtraEditors.SimpleButton btn_delete;
        private DevExpress.XtraEditors.SimpleButton btn_0;
        private DevExpress.XtraEditors.SimpleButton btn_enter;
        private DevExpress.XtraEditors.PanelControl pnViewProduct;
        private DevExpress.XtraBars.BarButtonItem btn_Dong;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraEditors.SimpleButton btn_9Cot;
        private DevExpress.XtraEditors.XtraScrollableControl Scrol_ShowNhom;
        private DevExpress.XtraEditors.LabelControl lblSoTrangNhom;
        private DevExpress.XtraEditors.SimpleButton btn_NextNhom;
        private DevExpress.XtraEditors.SimpleButton btnBackNhom;
        private DevExpress.XtraEditors.TextEdit txtSarech;
        private DevExpress.XtraEditors.SimpleButton txtScrolNhom;
        private DevExpress.XtraEditors.PanelControl pnCtrlNgNhPhNhom;
        private DevExpress.XtraEditors.PanelControl pn_WapperContent;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.PanelControl palPhanNganh;
        private DevExpress.XtraEditors.SimpleButton btn_000;
        private DevExpress.XtraEditors.SimpleButton btn_00;
        private DevExpress.XtraEditors.SimpleButton btn_ABC;
        private DevExpress.XtraEditors.SimpleButton btn_Payments;
        private DevExpress.XtraEditors.PanelControl pn_banphim;
        private DevExpress.XtraEditors.PanelControl pn_banphim_chu;
        private DevExpress.XtraEditors.SimpleButton btn_OKChar;
        private DevExpress.XtraEditors.SimpleButton btn_L;
        private DevExpress.XtraEditors.SimpleButton btn_K;
        private DevExpress.XtraEditors.SimpleButton btn_U;
        private DevExpress.XtraEditors.SimpleButton btn_Y;
        private DevExpress.XtraEditors.SimpleButton btn_123;
        private DevExpress.XtraEditors.SimpleButton btn_M;
        private DevExpress.XtraEditors.SimpleButton btn_J;
        private DevExpress.XtraEditors.SimpleButton btn_I;
        private DevExpress.XtraEditors.SimpleButton btn_T;
        private DevExpress.XtraEditors.SimpleButton btn_DeleteChar;
        private DevExpress.XtraEditors.SimpleButton btn_N;
        private DevExpress.XtraEditors.SimpleButton btn_H;
        private DevExpress.XtraEditors.SimpleButton btn_O;
        private DevExpress.XtraEditors.SimpleButton btn_R;
        private DevExpress.XtraEditors.SimpleButton btn_ESCChar;
        private DevExpress.XtraEditors.SimpleButton btn_B;
        private DevExpress.XtraEditors.SimpleButton btn_G;
        private DevExpress.XtraEditors.SimpleButton btn_P;
        private DevExpress.XtraEditors.SimpleButton btn_E;
        private DevExpress.XtraEditors.SimpleButton btn_X;
        private DevExpress.XtraEditors.SimpleButton btn_V;
        private DevExpress.XtraEditors.SimpleButton btn_F;
        private DevExpress.XtraEditors.SimpleButton btn_A;
        private DevExpress.XtraEditors.SimpleButton btn_W;
        private DevExpress.XtraEditors.SimpleButton btn_Z;
        private DevExpress.XtraEditors.SimpleButton btn_C;
        private DevExpress.XtraEditors.SimpleButton btn_D;
        private DevExpress.XtraEditors.SimpleButton btn_S;
        private DevExpress.XtraEditors.SimpleButton btn_Q;
        private DevExpress.XtraEditors.SimpleButton btn_backchar;
        private DevExpress.XtraEditors.SimpleButton btn_PaymentsChar;
        private DevExpress.XtraEditors.PanelControl pn_Luoi;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.SimpleButton btn_dot;
        private DevExpress.XtraEditors.SimpleButton btnRight;
        private DevExpress.XtraEditors.SimpleButton btnLeft;
        private DevExpress.XtraEditors.PanelControl palPhanTrang;
        private DevExpress.XtraEditors.PanelControl palCot;
        private DevExpress.XtraEditors.SimpleButton btnDatCoc;
        private DevExpress.XtraEditors.PanelControl pn_hanghoa;
        private DevExpress.XtraEditors.PanelControl pal_banhang;
        private DevExpress.XtraEditors.PanelControl pal_banhang1;
        private DevExpress.XtraEditors.PanelControl pnHeader;
        private DevExpress.XtraBars.Bar bar5;
        private DevExpress.XtraEditors.PanelControl pn_Banhang;
        private DevExpress.XtraEditors.PanelControl pnBanHang;
        private DevExpress.XtraEditors.LabelControl lbdiscount;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.TextEdit txtdiscount;
        private DevExpress.XtraEditors.LabelControl lbTienTruocThue;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl labelThueVAT;
        private DevExpress.XtraEditors.LabelControl label_GiamGia;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txt_soluong;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl lb_tongcong;
        private DevExpress.XtraEditors.TextEdit txt_Mahang;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.PanelControl pnWapperDeletePlusBut;
        private DevExpress.XtraEditors.SimpleButton btnDeleteAll;
        private DevExpress.XtraEditors.SimpleButton btn_Cong;
        private DevExpress.XtraEditors.SimpleButton btn_xoa;
        private DevExpress.XtraEditors.SimpleButton btn_Tru;
        private DevExpress.XtraEditors.PanelControl pnUpDow;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl lb_RowsCount;
        private DevExpress.XtraEditors.LabelControl lb_STT;
        private DevExpress.XtraEditors.SimpleButton btn_up;
        private DevExpress.XtraEditors.SimpleButton btn_Dow;
        private DevExpress.XtraEditors.SimpleButton btn_DowLast;
        private DevExpress.XtraEditors.SimpleButton btn_UpFirst;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.LabelControl lb_Quay;
        private DevExpress.XtraEditors.LabelControl lb_Kho;
        private DevExpress.XtraEditors.LabelControl lbgiay;
        private DevExpress.XtraEditors.LabelControl lbCaBan;
        private DevExpress.XtraEditors.LabelControl lbGio;
        private DevExpress.XtraEditors.LabelControl lb_NhanVien;
        private DevExpress.XtraEditors.LabelControl lbtimeby;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl pnNghiepVu_ThuNgan;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.SimpleButton btn_Nghiepvu;
        private DevExpress.XtraEditors.SimpleButton btn_ThuNgan;
        private DevExpress.XtraEditors.PanelControl gr_ChonNhom;
        private DevExpress.XtraEditors.PanelControl pnInlaihoadon;
        private DevExpress.XtraEditors.PanelControl panelControl53;
        private DevExpress.XtraGrid.GridControl gr_billPaymented;
        private DevExpress.XtraGrid.Views.Grid.GridView gvBillPaymented;
        private DevExpress.XtraGrid.Columns.GridColumn SSTT;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HOADON;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYTAO;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_KHUVUC;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_BAN;
        private DevExpress.XtraGrid.Columns.GridColumn IN;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn HUY;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn LAYLAI;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit3;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.TextEdit txtMaHoaDon;
        private DevExpress.XtraEditors.SimpleButton btnHuy;
        private DevExpress.XtraEditors.LabelControl lb_maKH;
        private DevExpress.XtraEditors.PanelControl panelControl52;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.LabelControl lbTongSoRow;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.SimpleButton btn_upbill;
        private DevExpress.XtraEditors.SimpleButton btnDowBill;
        private DevExpress.XtraEditors.SimpleButton btnDowLastBill;
        private DevExpress.XtraEditors.SimpleButton btn_UpTopBill;
        private DevExpress.XtraEditors.PanelControl pnNghiepvu;
        private DevExpress.XtraEditors.SimpleButton btnPhatThuong;
        private DevExpress.XtraEditors.SimpleButton btnThemKHTT;
        private DevExpress.XtraEditors.SimpleButton btnXemTonKho;
        private DevExpress.XtraEditors.SimpleButton btnBaoCaoBanHang;
        private DevExpress.XtraEditors.SimpleButton btnLayHDDoi;
        private DevExpress.XtraEditors.SimpleButton btnLuuHDDoi;
        private DevExpress.XtraEditors.SimpleButton btnInVeOnline;
        private DevExpress.XtraEditors.PanelControl pnThuNgan;
        private DevExpress.XtraEditors.SimpleButton btnSuaGia;
        private DevExpress.XtraEditors.SimpleButton btn_InLaiHoaDon;
        private DevExpress.XtraEditors.SimpleButton btnMoKet;
        private DevExpress.XtraEditors.SimpleButton btnKetKa;
        private DevExpress.XtraEditors.SimpleButton btn_logout;
        private DevExpress.XtraEditors.SimpleButton btn_HuyHoaDon;
        private DevExpress.XtraEditors.SimpleButton btnGiaBan2;
        private DevExpress.XtraEditors.SimpleButton btnTraCuu;
        private DevExpress.XtraGrid.Columns.GridColumn XEM;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit4;
        private DevExpress.XtraEditors.SimpleButton btnTraHang;
        private DevExpress.XtraEditors.PanelControl pnMaHoaDonTraHang;
        private DevExpress.XtraEditors.SimpleButton btnTra;
        private DevExpress.XtraEditors.TextEdit txtMaHoaDonTra;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.PanelControl pnTachban;
        private DevExpress.XtraEditors.PanelControl pn_GroupTable;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btn_OkThrough;
        private DevExpress.XtraEditors.SimpleButton btn_ToLeftAll;
        private DevExpress.XtraEditors.SimpleButton btn_ToRightAll;
        private DevExpress.XtraEditors.SimpleButton btn_UpDivide;
        private DevExpress.XtraEditors.SimpleButton btn_DowDivide;
        private DevExpress.XtraEditors.SimpleButton btn_ESCThroughtTable;
        private DevExpress.XtraEditors.SimpleButton btn_ToRight;
        private DevExpress.XtraEditors.SimpleButton btn_Toleft;
        private DevExpress.XtraEditors.PanelControl panelControl15;
        private DevExpress.XtraGrid.GridControl gr_divide_Table_1;
        private DevExpress.XtraGrid.Views.Grid.GridView gv_divide_Table_1;
        private DevExpress.XtraGrid.Columns.GridColumn _SSTT;
        private DevExpress.XtraGrid.Columns.GridColumn _TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn _SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn _GIABAN;
        private DevExpress.XtraGrid.Columns.GridColumn _THANHTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn _MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn _THUE;
        private DevExpress.XtraGrid.Columns.GridColumn _CHIETKHAU;
        private DevExpress.XtraGrid.Columns.GridColumn _IS_COMBO;
        private DevExpress.XtraEditors.PanelControl pn_TableTow;
        private DevExpress.XtraGrid.GridControl gr_divide_Table_2;
        private DevExpress.XtraGrid.Views.Grid.GridView gv_divide_Table_2;
        private DevExpress.XtraGrid.Columns.GridColumn __STT;
        private DevExpress.XtraGrid.Columns.GridColumn __TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn __SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn __GIABAN;
        private DevExpress.XtraGrid.Columns.GridColumn __THANHTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn __MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn __THUE;
        private DevExpress.XtraGrid.Columns.GridColumn __CHIETKHAU;
        private DevExpress.XtraGrid.Columns.GridColumn __IS_COMBO;
        private DevExpress.XtraGrid.Columns.GridColumn _MA_HOADON;
        private DevExpress.XtraGrid.Columns.GridColumn __MA_HOADON;
        private DevExpress.XtraEditors.SimpleButton btnBaoCaoTraHang;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridControl grCtrlDSHangMua;
        private DevExpress.XtraGrid.Views.Grid.GridView gVDSHangMua;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA_VACH;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn GIABAN;
        private DevExpress.XtraGrid.Columns.GridColumn CHIETKHAU;
        private DevExpress.XtraGrid.Columns.GridColumn Thanh_Tien;
        private DevExpress.XtraGrid.Columns.GridColumn TONGTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;
        private DevExpress.XtraGrid.Columns.GridColumn MA_BEP;
        private DevExpress.XtraGrid.Columns.GridColumn THUE;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI;
        private DevExpress.XtraGrid.Columns.GridColumn IS_COMBO;
        private DevExpress.XtraGrid.Columns.GridColumn IS_INBEP;
        private DevExpress.XtraGrid.Columns.GridColumn IS_KHUYENMAI;
        private DevExpress.XtraGrid.Columns.GridColumn IS_QUATANG;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn_Add;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn_Exc;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn_Del;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit TrungHere;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.SimpleButton btnComboOption;
        private DevExpress.XtraEditors.SimpleButton btnLaytrongluong;
        private DevExpress.XtraEditors.SimpleButton btn_Datthu;
        private DevExpress.XtraEditors.PanelControl pnDacthu;
        private DevExpress.XtraEditors.SimpleButton btnChonbanggia;
    }
}
