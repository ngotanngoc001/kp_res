﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using System.Data.SqlClient;

namespace KP_RES
{
    public partial class Frm_ThemHangHoa : DevExpress.XtraEditors.XtraForm
    {
        public Frm_ThemHangHoa()
        {
            InitializeComponent();
        }

        string _thongbao = "";
        string _nhap = "";
        string _nhapmahanghoa = "";
        string _trungmahh = "";
        string _trungtenhanghoa = "";
        string _trungmavach = "";
        string _luuthanhcong = "";
        string _luukothanhcong = "";

        private void Frm_ThemHangHoa_Load(object sender, EventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            this.Text = rm.GetString("themhanghoa",culture);
            lbNhom.Text = rm.GetString("nhom",culture);
            lblTENHANGHOA.Text = rm.GetString("ten",culture);
            lbGiaBan1.Text = rm.GetString("giaban",culture);
            lbMa.Text = rm.GetString("mahh", culture);
            lbMaVach.Text = rm.GetString("mavach",culture);
            lblDVT.Text = rm.GetString("dvt",culture);
            chkTONKHO.Text = rm.GetString("tonkho",culture);
            chkMayinbep.Text = rm.GetString("mayinbep",culture);
            btnLuu.Text = rm.GetString("luu",culture);
            btnThoát.Text = rm.GetString("dong",culture);
            _thongbao = rm.GetString("thongbao",culture);
            _nhap = rm.GetString("nhap",culture);
            _nhapmahanghoa = rm.GetString("nhapmahanghoa", culture);
            _trungmahh = rm.GetString("trungmahh", culture);
            _trungtenhanghoa = rm.GetString("trungtenhanghoa", culture);
            _trungmavach = rm.GetString("trungmavach", culture);
            _luuthanhcong = rm.GetString("luuthanhcong", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);
            LoadCombo();
            btnLuu.Enabled = clsUserManagement.AllowAdd("6");
        }

        private void LoadCombo()
        {
            string sSQL = "";
            DataTable dt = new DataTable();
            sSQL += "Select MA_NHOMHANG As MA,TEN_NHOMHANG As TEN" + "\n";
            sSQL += "From NHOMHANG" + "\n";
            sSQL += "Where SUDUNG=1 ";
            sSQL += "Order by  TEN_NHOMHANG ";
            dt = clsMain.ReturnDataTable(sSQL);
            cboNhomhang.Properties.DataSource = dt;
            cboNhomhang.EditValue = cboNhomhang.Properties.GetDataSourceValue(cboNhomhang.Properties.ValueMember, 0);

            sSQL = "";
            sSQL += "Select MA_DONVITINH As MA,TEN_DONVITINH As TEN" + "\n";
            sSQL += "From DONVITINH" + "\n";
            sSQL += "Where SUDUNG=1 ";
            sSQL += "Order by  TEN_DONVITINH ";
            dt = clsMain.ReturnDataTable(sSQL);
            cboDonvitinh.Properties.DataSource = dt;
            cboDonvitinh.EditValue = cboDonvitinh.Properties.GetDataSourceValue(cboDonvitinh.Properties.ValueMember, 0);

            sSQL = "";
            sSQL += "Select A.MA_BEP As MA,A.TEN_BEP As TEN,A.MAYINBEP,A.MA_CUAHANG,B.TEN_CUAHANG" + "\n";
            sSQL += "From DM_BEP A, CUAHANG B" + "\n";
            sSQL += "Where A.MA_CUAHANG=B.MA_CUAHANG" + "\n";
            sSQL += "And A.SUDUNG=1 ";
            sSQL += "Order by  B.TEN_CUAHANG,A.TEN_BEP ";
            dt = clsMain.ReturnDataTable(sSQL);
            cboBep.Properties.DataSource = dt;
            cboBep.EditValue = cboBep.Properties.GetDataSourceValue(cboBep.Properties.ValueMember, 0);

            txtMAHANGHOA.Text = clsMain.ReturnDataTable("SELECT [dbo].[fc_NewCodeID_HANGHOA]()").Rows[0][0].ToString();
            txtMAVACH.Text = clsMain.ReturnDataTable("SELECT [dbo].[fc_NewCodeID_MaVach]()").Rows[0][0].ToString();
        }


        private Boolean CheckInput()
        {
            if (txtTENHANGHOA.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_nhap + " " + lblTENHANGHOA.Text, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTENHANGHOA.Focus();
                return false;
            }
            if (txtMAHANGHOA.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_nhapmahanghoa, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMAHANGHOA.Focus();
                return false;
            }
            if (txtMAHANGHOA.Text != "" && clsMain.ReturnDataTable("Select MA_HANGHOA From HANGHOA Where ID_HANGHOA=" + clsMain.SQLString(txtMAHANGHOA.Text)).Rows.Count > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_trungmahh, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMAHANGHOA.Focus();
                return false;
            }
            if (txtTENHANGHOA.Text != "" && clsMain.ReturnDataTable("Select MA_HANGHOA From HANGHOA Where TEN_HANGHOA=" + clsMain.SQLStringUnicode(txtTENHANGHOA.Text)).Rows.Count > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show(_trungtenhanghoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    txtTENHANGHOA.Focus();
                    return false;
                }
            }
            if (txtMAVACH.Text != "" && clsMain.ReturnDataTable("Select MA_HANGHOA From HANGHOA Where MAVACH = " + clsMain.SQLString(txtMAVACH.Text)).Rows.Count > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_trungmavach, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMAVACH.Focus();
                return false;
            }
            return true;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;

            SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
            SqlCommand SqlComMain = null;
            String sSQL = "";
            sSQL += "Insert into HANGHOA (TEN_HANGHOA,ID_HANGHOA,THUE,GIANHAP,GIABAN1,GIABAN2,GHICHU,MAVACH,SUDUNG,SUAGIA,TONKHO,TONTOITHIEU,MA_THANHPHAN,SOLUONG,MA_NHOMHANG,MA_DONVITINH,MA_BEP,STT,IS_INBEP,SUADINHLUONG)" + "\n";
            sSQL += "Values (@TEN_HANGHOA,@ID_HANGHOA,@THUE,@GIANHAP,@GIABAN1,@GIABAN2,@GHICHU,@MAVACH,@SUDUNG,@SUAGIA,@TONKHO,@TONTOITHIEU,@MA_THANHPHAN,@SOLUONG,@MA_NHOMHANG,@MA_DONVITINH,@MA_BEP,@STT,@IS_INBEP,@SUADINHLUONG)" + "\n";
            SqlCommand SqlCom = new SqlCommand(sSQL, CN);

            SqlCom.Parameters.Add(new SqlParameter("@TEN_HANGHOA", (object)txtTENHANGHOA.Text));
            SqlCom.Parameters.Add(new SqlParameter("@ID_HANGHOA", (object)txtMAHANGHOA.Text));
            SqlCom.Parameters.Add(new SqlParameter("@THUE", (object)"0"));
            SqlCom.Parameters.Add(new SqlParameter("@GIANHAP", (object)"0"));
            SqlCom.Parameters.Add(new SqlParameter("@GIABAN1", (object)txtGIABAN1.Text.Replace(",", "")));
            SqlCom.Parameters.Add(new SqlParameter("@GIABAN2", (object)txtGIABAN1.Text.Replace(",", "")));
            SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)""));
            SqlCom.Parameters.Add(new SqlParameter("@MAVACH", (object)txtMAVACH.Text));
            SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)true));
            SqlCom.Parameters.Add(new SqlParameter("@SUAGIA", (object)false));
            SqlCom.Parameters.Add(new SqlParameter("@TONKHO", (object)chkTONKHO.Checked));
            SqlCom.Parameters.Add(new SqlParameter("@TONTOITHIEU", (object)"0"));
            SqlCom.Parameters.Add(new SqlParameter("@MA_THANHPHAN", (object)"0"));
            SqlCom.Parameters.Add(new SqlParameter("@SOLUONG", (object)"0"));
            SqlCom.Parameters.Add(new SqlParameter("@MA_NHOMHANG", (object)cboNhomhang.EditValue));
            SqlCom.Parameters.Add(new SqlParameter("@MA_DONVITINH", (object)cboDonvitinh.EditValue));
            SqlCom.Parameters.Add(new SqlParameter("@MA_BEP", (object)cboBep.EditValue));
            SqlCom.Parameters.Add(new SqlParameter("@STT", (object)"0"));
            SqlCom.Parameters.Add(new SqlParameter("@IS_INBEP", (object)chkMayinbep.Checked));
            SqlCom.Parameters.Add(new SqlParameter("@SUADINHLUONG", (object)false));

            SqlComMain = SqlCom;

            Boolean bRunSQL = true;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                CN.Open();
                SqlComMain.ExecuteNonQuery();
                CN.Close();
                this.Cursor = Cursors.Default;
                bRunSQL = true;
            }
            catch
            {
                bRunSQL = false;
            }

            if (bRunSQL)
            {
                LoadCombo();
                XtraMessageBox.Show(_luuthanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void Frm_ThemHangHoa_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void btnThoát_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
    }
}