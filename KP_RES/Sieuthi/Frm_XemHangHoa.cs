﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES
{
    public partial class Frm_XemHangHoa : DevExpress.XtraEditors.XtraForm
    {
        public Frm_XemHangHoa()
        {
            InitializeComponent();
        }

        private void Frm_XemHangHoa_Load(object sender, EventArgs e)
        {
            
        }

        public Frm_XemHangHoa(String pMaHoaDon, String pNgayTao, String pTongTien, String pObject)
        {
            InitializeComponent();

            String sSQL = "";
            if (pObject == "3")
            {
                sSQL = "";
                sSQL += "SELECT A.MA_HANGHOA, B.TEN_HANGHOA, A.SOLUONG, A.GIABAN, 'a' AS GHICHU, B.MA_BEP, A.THUE," + "\n";
                sSQL += "ISNULL(A.CHIETKHAU,0) AS CHIETKHAU, 0 AS Thanh_Tien, ISNULL(TRANGTHAI,0) AS TRANGTHAI," + "\n";
                sSQL += "ISNULL(TRANGTHAI,0) as IS_COMBO, B.IS_INBEP, '' AS IS_KHUYENMAI" + "\n";
                sSQL += "FROM CT_HOADON_DOI A " + "\n";
                sSQL += "INNER JOIN HANGHOA B ON A.MA_HANGHOA = B.MA_HANGHOA " + "\n";
                sSQL += "WHERE A.MA_HOADON = " + clsMain.SQLString(pMaHoaDon);
            }
            else
            {
                sSQL = "";
                sSQL += "SELECT A.MA_HANGHOA, B.TEN_HANGHOA, A.SOLUONG, A.GIABAN, 'a' AS GHICHU, B.MA_BEP, A.THUE," + "\n";
                sSQL += "ISNULL(A.CHIETKHAU,0) AS CHIETKHAU, 0 AS Thanh_Tien, ISNULL(TRANGTHAI,0) AS TRANGTHAI," + "\n";
                sSQL += "ISNULL(TRANGTHAI,0) as IS_COMBO, B.IS_INBEP, '' AS IS_KHUYENMAI" + "\n";
                sSQL += "FROM CT_HOADON A " + "\n";
                sSQL += "INNER JOIN HANGHOA B ON A.MA_HANGHOA = B.MA_HANGHOA " + "\n";
                sSQL += "WHERE A.MA_HOADON = " + clsMain.SQLString(pMaHoaDon);
            }
            
            DataTable myDT = clsMain.ReturnDataTable(sSQL);
            gr_SelectProduct.DataSource = myDT;
            if (gv_SelectProduct.RowCount > 0)
                TotalMoney();

            gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.RowCount - 1;

            lblMaHoaDon.Text = pMaHoaDon;
            lblNgayTao.Text = String.Format("{0:dd/MM/yyyy HH:mm}", DateTime.Parse(pNgayTao));
            lblTongTien.Text = String.Format("{0:#,###0}", decimal.Parse(pTongTien));
        }

        DataTable checkVAT = new DataTable();
        private void TotalMoney()
        {
            if (checkVAT.Rows.Count <= 0)
            {
                string sSQL = "Select GIATRI From CAUHINH Where TEN='GIABANTRUOCTHUE' ";
                checkVAT = clsMain.ReturnDataTable(sSQL);
            }
            bool kt = false;
            if (checkVAT.Rows[0]["GIATRI"].ToString() == "1")
                kt = true;
            double total = 0;
            try
            {
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    double thanhTien = 0, vat = 0;
                    double sl = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                    double gia = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                    double ck = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["CHIETKHAU"]).ToString());
                    thanhTien = (sl * gia) - ((sl * gia) * ck) / 100;
                    if (kt == true)
                    {
                        vat = (double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["THUE"]).ToString()) / 100) * thanhTien;
                    }
                    total += thanhTien + vat;
                    gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"], thanhTien + vat);
                }
            }
            catch (Exception ex)
            {
                
            }
        }


        private void btnUpFirst_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = 0;
                btnUpFirst.Enabled = false;
                btnUp.Enabled = false;
                btnDow.Enabled = true;
                btnDowLast.Enabled = true;
            }
            catch
            {

            }
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.FocusedRowHandle - 1;
                btnDow.Enabled = true;
                btnDowLast.Enabled = true;
                if (gv_SelectProduct.FocusedRowHandle == 0)
                {
                    btnUp.Enabled = false;
                    btnUpFirst.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnDow_Click(object sender, EventArgs e)
        {
            try
            {
                if (gv_SelectProduct.FocusedRowHandle < gv_SelectProduct.RowCount)
                {
                    gv_SelectProduct.Focus();
                    gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.FocusedRowHandle + 1;
                    btnUp.Enabled = true;
                    btnUpFirst.Enabled = true;
                    if (gv_SelectProduct.FocusedRowHandle == gv_SelectProduct.RowCount - 1)
                    {
                        btnDow.Enabled = false;
                        btnDowLast.Enabled = false;
                    }
                }
            }
            catch
            {

            }
        }

        private void btnDowLast_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.RowCount - 1;
                btnUpFirst.Enabled = true;
                btnUp.Enabled = true;
                btnDow.Enabled = false;
                btnDowLast.Enabled = false;
            }
            catch
            {
            }
        }

        private void gv_SelectProduct_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
                if (e.Column == STT)
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btn_logout_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}