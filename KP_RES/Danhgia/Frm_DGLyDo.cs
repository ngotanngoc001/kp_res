﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;

namespace KP_RES 
{
    public partial class Frm_DGLyDo : DevExpress.XtraEditors.XtraForm
    {
        private String sMa = "";
        private Byte[] sByteDataImage;

        public Frm_DGLyDo()
        {
            InitializeComponent();
        }

        private void Frm_DGLyDo_Load(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
            LoadPermission();
            LoadLang();
        }

        private void LoadLang()
        {
            var culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            var rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            cboTrangThai.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            if (clsQLCongTy.MessageQuestion("Xác nhận bạn muốn xóa ?", "Thông báo") == DialogResult.No)
                return;

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL("DELETE FROM DM_DG_LYDO WHERE MA = " + clsMain.SQLString(sMa));
            this.Cursor = Cursors.Default;

            if (bRunSQL)
                LoaddataGridView();
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sMa == "")
                return;

            KhoaMoControl(false);
            txtTen.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;

            //Byte[] sImage = clsQLCongTy.ReturnByteImage(picHinhAnh);
            Byte[] sImage = (Byte[])new ImageConverter().ConvertTo(picHinhAnh.Image, typeof(Byte[]));

            SqlParameter[] sParameter = null;
            String sSPs = "";
            if (sMa == "" && btnThem.Enabled)
            {
                sSPs = "SP_InsertDGLyDo";

                sParameter = new SqlParameter[5];
                sParameter[0] = new SqlParameter("@TEN", (Object)txtTen.Text);
                sParameter[1] = new SqlParameter("@GHICHU", (Object)txtGhiChu.EditValue);
                sParameter[2] = new SqlParameter("@SUDUNG", (Object)(chkSuDung.Checked == true ? "1" : "0"));
                sParameter[3] = new SqlParameter("@IMAGE", SqlDbType.Image);
                sParameter[3].Value = sImage;
                sParameter[4] = new SqlParameter("@DG_TRANGTHAI", (Object)cboTrangThai.EditValue.ToString());
            }
            else if (sMa != "" && btnSua.Enabled)
            {
                sSPs = "SP_UpdateDGLyDo";

                sParameter = new SqlParameter[6];
                sParameter[0] = new SqlParameter("@TEN", (Object)txtTen.Text);
                sParameter[1] = new SqlParameter("@GHICHU", (Object)txtGhiChu.EditValue);
                sParameter[2] = new SqlParameter("@SUDUNG", (Object)(chkSuDung.Checked == true ? "1" : "0"));
                sParameter[3] = new SqlParameter("@IMAGE", SqlDbType.Image);
                sParameter[3].Value = sImage;
                sParameter[4] = new SqlParameter("@MA", (Object)sMa);
                sParameter[5] = new SqlParameter("@DG_TRANGTHAI", (Object)cboTrangThai.EditValue.ToString());
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean sBoolean = clsQLCongTy.ExecuteParameterSPs(sSPs, sParameter);
            this.Cursor = Cursors.Default;
            if (sBoolean)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");

        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sMa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtTen.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN").ToString();
            cboTrangThai.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DG_TRANGTHAI").ToString());
            txtGhiChu.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GHICHU").ToString();
            chkSuDung.Checked = Boolean.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SUDUNG").ToString());
            //sByteDataImage = clsQLCongTy.ReturnByte(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "IMAGE"));
            //picHinhAnh.Image = clsQLCongTy.ReturnBitmap(sByteDataImage);

            try
            {
                byte[] tam = (byte[])gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "IMAGE");
                MemoryStream ms = new MemoryStream(tam);
                Bitmap bm = new Bitmap(ms);
                picHinhAnh.Image = bm;
                picHinhAnh.BackgroundImage = null;
            }
            catch
            {
                picHinhAnh.Image = null;
                picHinhAnh.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            }

            KhoaMoControl(true);
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            DataTable myDT = clsMain.ReturnDataTable("SELECT A.MA,A.TEN,A.GHICHU,A.SUDUNG,A.IMAGE,A.DG_TRANGTHAI,B.TEN AS TRANGTHAI FROM DM_DG_LYDO A INNER JOIN DM_DG_TRANGTHAI B ON A.DG_TRANGTHAI = B.MA WHERE B.SUDUNG = 1");
            gridControl2.DataSource = myDT;
        }

        private void LoadCombo()
        {
            DataTable myDT = new DataTable();
            myDT = clsMain.ReturnDataTable("SELECT MA,TEN FROM DM_DG_TRANGTHAI WHERE SUDUNG = 1");
            clsQLCongTy.LoadCombo(myDT, cboTrangThai);
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("22071601");
            btnSua.Enabled = clsUserManagement.AllowEdit("22071601");
            btnXoa.Enabled = clsUserManagement.AllowDelete("22071601");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(Boolean sBoolean)
        {
            txtTen.Properties.ReadOnly = sBoolean;
            cboTrangThai.Properties.ReadOnly = sBoolean;
            txtGhiChu.Properties.ReadOnly = sBoolean;
            chkSuDung.Properties.ReadOnly = sBoolean;
            picHinhAnh.Enabled = !sBoolean;
        }

        private void SetRongControl()
        {
            sMa = "";
            sByteDataImage = new Byte[1];
            txtTen.Text = "";
            txtGhiChu.Text = "";
            chkSuDung.Checked = true;
           // picHinhAnh.Image = clsQLCongTy.ReturnBitmap(sByteDataImage);
            cboTrangThai.EditValue = cboTrangThai.Properties.GetDataSourceValue(cboTrangThai.Properties.ValueMember, 0);

            picHinhAnh.Image = null;
            picHinhAnh.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26; 
        }

        private Boolean CheckInput()
        {
            if (txtTen.Text == "")
            {
                XtraMessageBox.Show("Nhập " + lblTen.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTen.Focus();
                return false;
            }
            if (cboTrangThai.EditValue == "" || cboTrangThai.EditValue == null)
            {
                XtraMessageBox.Show("Nhập " + cboTrangThai.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboTrangThai.Focus();
                return false;
            }
            
            return true;
        }

        private void picHinhAnh_Click(object sender, EventArgs e)
        {
            if (picHinhAnh.Enabled)
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = Application.StartupPath + "\\Images";
                openFileDialog1.Filter = "Image Files|*.jpg;*.bmp;*.png;*.gif|All Files|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.Title = "Chọn ảnh";
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        picHinhAnh.ImageLocation = openFileDialog1.FileName;
                        picHinhAnh.BackgroundImage = null;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {
                        picHinhAnh.Image = null;
                        picHinhAnh.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }

        }
    }
}