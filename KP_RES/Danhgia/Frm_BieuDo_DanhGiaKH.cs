﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using KP_UserManagement;
using System.IO;
using System.Data.SqlClient;
using DevExpress.XtraCharts;

namespace KP_RES
{
    public partial class Frm_BieuDo_DanhGiaKH : DevExpress.XtraEditors.XtraForm
    {
        public Frm_BieuDo_DanhGiaKH()
        {
            InitializeComponent();
        }

        private static byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                panelControl5.Controls.Clear();
                String sSQL = "EXEC SP_SelectBDDanhGiaKH ";
                sSQL += clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", dtpTuNgay.Value) + " " + txtTuGio.Text) + ",";
                sSQL += clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", dtpDenNgay.Value) + " " + txtDenGio.Text);

                DataTable myDT = clsMain.ReturnDataTable(sSQL);

                ChartControl pieChart = new ChartControl();
                pieChart.CustomDrawSeriesPoint += new CustomDrawSeriesPointEventHandler(pieChart_CustomDrawSeriesPoint);
                pieChart.Series.Clear();
                pieChart.Series.Add(new Series("Pie1", ViewType.Pie));
                pieChart.Series["Pie1"].DataSource = myDT;
                pieChart.Series["Pie1"].ArgumentDataMember = "TRANGTHAI";
                pieChart.Series["Pie1"].ValueDataMembers.AddRange(new string[] { "SOLUONG" });
                ((PiePointOptions)pieChart.Series["Pie1"].Label.PointOptions).ValueNumericOptions.Format = NumericFormat.Percent;
                ((PiePointOptions)pieChart.Series["Pie1"].Label.PointOptions).PointView = PointView.ArgumentAndValues;
                ChartTitle chartTitle1 = new ChartTitle();
                chartTitle1.Text = "";
                pieChart.Titles.Add(chartTitle1);
                pieChart.Dock = DockStyle.Fill;
                panelControl5.Controls.Add(pieChart);

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void pieChart_CustomDrawSeriesPoint(object sender, CustomDrawSeriesPointEventArgs e)
        {
            e.LegendText = e.SeriesPoint.Argument + " : " + e.SeriesPoint.Values[0].ToString();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            try
            {
                ChartControl chart = (ChartControl)panelControl5.Controls[0];
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                chart.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                chart.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            btnTimkiem_Click(sender, e);
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 223)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
            }
            else
            {
                panelControl1.Width = 223;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
            }
            this.Refresh();
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                DataColumn dc2 = new DataColumn();
                dc2.ColumnName = "TEMP";
                dc2.DefaultValue = "Temp";
                dt.Columns.Add(dc2);
                dt.Rows.Add("TEMP");

                Bitmap bmp = new Bitmap(panelControl5.ClientRectangle.Width, panelControl5.ClientRectangle.Height);
                panelControl5.DrawToBitmap(bmp, panelControl5.ClientRectangle);

                DataColumn colIMAGE = new DataColumn("IMAGE"); //Create the column.
                colIMAGE.DataType = System.Type.GetType("System.Byte[]"); //Type byte[] to store image bytes.
                colIMAGE.AllowDBNull = true;
                colIMAGE.Caption = "IMAGE";
                colIMAGE.DefaultValue = ImageToByteArray(bmp);
                dt.Columns.Add(colIMAGE);

                String Description = "";
                Description = "( Từ ngày: " + dtpTuNgay.Text + " - Đến ngày: " + dtpDenNgay.Text + " )";

                DataColumn All = new DataColumn();
                All.ColumnName = "HEARDER";
                All.DataType = System.Type.GetType("System.String");
                All.DefaultValue = Description;
                dt.Columns.Add(All);

                Frm_BCInPhieu frm = new Frm_BCInPhieu();
                frm.WindowState = FormWindowState.Maximized;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.dtSource = dt;
                frm.Mode = 84;
                frm.ShowDialog();
                frm.Dispose();
            }
            catch{
            }
        }

        private Boolean ContainColumn(String columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;
            Boolean sBoolean = (columns.Contains(columnName)) ? true : false;

            return sBoolean;
        }

       


    }
}