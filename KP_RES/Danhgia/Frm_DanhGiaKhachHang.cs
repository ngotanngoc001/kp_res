﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KP_UserManagement;
using KP_Report;
using DevExpress.XtraEditors;
using System.IO;

namespace KP_RES
{
    public partial class Frm_DanhGiaKhachHang : DevExpress.XtraEditors.XtraForm
    {
        private String pMaTrangThai = "";
        private String pMaLyDo = "";

        public Frm_DanhGiaKhachHang()
        {
            InitializeComponent();
        }

        private void Frm_DanhGiaKhachHang_Load(object sender, EventArgs e)
        {
            panel.Width = panel.Parent.Width;
            panel.Height = (panel.Parent.Height/3)*2+6;
            panel.Location = new Point(0, panel.Parent.Height / 2 - panel.Height/2);
            us_Keyboard1.SetTextbox(txtlylokhac);
            LoadTrangThai();
        }

        DataTable myDT_TrangThai = new DataTable();
        private void LoadTrangThai()
        {
            String sSQL = "";
            sSQL += "SELECT MA,TEN,GHICHU,SUDUNG,IMAGE" + "\n";
            sSQL += "FROM DM_DG_TRANGTHAI" + "\n";
            sSQL += "WHERE SUDUNG = 1" + "\n";
            sSQL += "ORDER BY TEN" + "\n";
            myDT_TrangThai = clsMain.ReturnDataTable(sSQL);
            palTrangThai.Controls.Clear();
            palLyDo.Controls.Clear();
            panel.Visible = true ;
            panel1.Visible = false ;
            foreach (DataRow dr in myDT_TrangThai.Rows)
            {
                Button btn = new Button();
                btn.Width = (palTrangThai.Width / myDT_TrangThai.Rows.Count) - 10;
                btn.Height = (palTrangThai.Height/3)*2;
                btn.Font = palTrangThai.Font;
                //btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                //btn.ImageLocation = ImageLocation.TopCenter;
                btn.Name = "N" + dr["MA"].ToString();
                btn.Tag = dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.TextAlign = ContentAlignment.BottomCenter;
                btn.FlatStyle = FlatStyle.Popup;
                try
                {
                    byte[] tam = new byte[((byte[])dr["IMAGE"]).LongLength];
                    tam = (byte[])dr["IMAGE"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    btn.BackgroundImage = bm;
                    btn.BackgroundImageLayout = ImageLayout.Stretch;
                }
                catch
                {
                }
                btn.Click += new EventHandler(btnTrangThai_Click);
                palTrangThai.Controls.Add(btn);
            }
            //panel.Height = palTrangThai.VerticalScroll.Maximum + 5;
        }

        private void btnTrangThai_Click(object sender, EventArgs e)
        {
            //btnChonKhuvuc_Click(null, null);

            Button btn = (Button)sender;
            btn.ForeColor = Color.Black;
            foreach (Button btn1 in palTrangThai.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = panel2.ForeColor;
                }
            }

            pMaTrangThai = btn.Tag.ToString();

            LoadLyDo(pMaTrangThai);
        }

        DataTable myDT_LyDo = new DataTable();
        private void LoadLyDo(String sMa)
        {
            String sSQL = "";
            sSQL += "SELECT MA,TEN,GHICHU,SUDUNG,IMAGE,DG_TRANGTHAI" + "\n";
            sSQL += "FROM DM_DG_LYDO" + "\n";
            sSQL += "WHERE SUDUNG = 1 AND DG_TRANGTHAI = " + clsMain.SQLString(sMa) + "\n";
            sSQL += "ORDER BY TEN" + "\n";
            myDT_LyDo = clsMain.ReturnDataTable(sSQL);
            palLyDo.Controls.Clear();

            if (myDT_LyDo.Rows.Count > 0)
            {
                panel.Visible = false;
                panel1.Visible = true ;
                timer1.Enabled = true;
                foreach (DataRow dr in myDT_LyDo.Rows)
                {
                    Button btn = new Button();
                    btn.Width = ((palLyDo.Width / myDT_TrangThai.Rows.Count) - 20)/2;
                    btn.Height = ((palTrangThai.Height / 3) * 2 -10)/2; 
                    btn.Font = palTrangThai.Font;
                    //btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    //btn.ImageLocation = ImageLocation.TopCenter;
                    btn.Name = "N" + dr["MA"].ToString();
                    btn.Tag = dr["MA"].ToString();
                    btn.Text = dr["TEN"].ToString();
                    btn.TextAlign = ContentAlignment.BottomCenter;
                    btn.FlatStyle = FlatStyle.Popup;
                    try
                    {
                        byte[] tam = new byte[((byte[])dr["IMAGE"]).LongLength];
                        tam = (byte[])dr["IMAGE"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        btn.BackgroundImage = bm;
                        btn.BackgroundImageLayout = ImageLayout.Stretch;
                    }
                    catch
                    {
                    }
                    btn.Click += new EventHandler(btnLyDo_Click);
                    palLyDo.Controls.Add(btn);
                }
            }
            else
            {
                pMaLyDo = "0";
                pMaTrangThai = sMa;
                btnLuu(pMaTrangThai, pMaLyDo,"");
            }
            
        }

        private void btnLyDo_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            btn.ForeColor = Color.Red;
            foreach (Button btn1 in palLyDo.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = panel2.ForeColor;
                }
            }

            pMaLyDo = btn.Tag.ToString();

            btnLuu(pMaTrangThai, pMaLyDo,"");

        }

        private void btnLuu(String sMaTrangThai, String sMaLyDo, String sGhichu)
        {
            if (pMaTrangThai == "0")
                return;

            String sSQL = "";
            sSQL += "INSERT INTO DANHGIA_KHACHHANG(NGAY,DG_TRANGTHAI,DG_LYDO,GHICHU)" + "\n";
            sSQL += "VALUES (GETDATE()," + clsMain.SQLString(sMaTrangThai) + "," + clsMain.SQLString(sMaLyDo)+"," + clsMain.SQLStringUnicode(sGhichu)+") "+ "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                foreach (Button btn1 in palTrangThai.Controls)
                {
                    btn1.ForeColor = panel2.ForeColor;
                }

                foreach (Button btn1 in palLyDo.Controls)
                {
                    btn1.ForeColor = panel2.ForeColor;
                }


                AutoClosingMessageBox.Show("Xin cảm ơn quý khách", "Thông báo", 10000);
                pMaLyDo = "0";
                pMaTrangThai = "0";
                txtlylokhac.Text = "";
                LoadTrangThai();
                palLyDo.Controls.Clear();
                timer1.Enabled = false;
            }
            else
                clsQLCongTy.MessageError("Lưu không thành công", "Thông báo");


            
        }

        private void home_Click(object sender, EventArgs e)
        {
            bool check;
            Frm_ConfigFuntion frm = new Frm_ConfigFuntion();
            frm.ShowDialog();
            check = frm.kt;
            frm.Dispose();
            if (check == false)
                return;

            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            pMaLyDo = "0";
            pMaTrangThai = "0";
            LoadTrangThai();
            palLyDo.Controls.Clear();
            timer1.Enabled = false;
        }

        private void txtlylokhac_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                pMaLyDo = "0";
                btnLuu(pMaTrangThai, pMaLyDo,txtlylokhac .Text );
            }
        }

    }

    public class AutoClosingMessageBox
    {
        System.Threading.Timer _timeoutTimer;
        string _caption;
        AutoClosingMessageBox(string text, string caption, int timeout)
        {
            _caption = caption;
            _timeoutTimer = new System.Threading.Timer(OnTimerElapsed,
                null, timeout, System.Threading.Timeout.Infinite);
            MessageBox.Show(text, caption);
        }

        public static void Show(string text, string caption, int timeout)
        {
            new AutoClosingMessageBox(text, caption, timeout);
        }

        void OnTimerElapsed(object state)
        {
            IntPtr mbWnd = FindWindow(null, _caption);
            if (mbWnd != IntPtr.Zero)
                SendMessage(mbWnd, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
            _timeoutTimer.Dispose();
        }
        const int WM_CLOSE = 0x0010;
        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);
    }

}
