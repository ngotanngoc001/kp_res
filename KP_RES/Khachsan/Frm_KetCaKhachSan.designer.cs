﻿namespace KP_RES
{
    partial class Frm_KetCaKhachSan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_KetCaKhachSan));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl14 = new DevExpress.XtraEditors.PanelControl();
            this.btnPrint = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnInHangHoa = new DevExpress.XtraEditors.PanelControl();
            this.lblInHangHoa = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btnHoantacketca = new DevExpress.XtraEditors.PanelControl();
            this.btnHoantatketca = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.btnThoat = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.gr_SelectProduct = new DevExpress.XtraGrid.GridControl();
            this.gv_SelectProduct = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SOTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.GIABAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CHIETKHAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Thanh_Tien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_BEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IS_COMBO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IS_INBEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.lb_Tongtiennop = new DevExpress.XtraEditors.TextEdit();
            this.panelControl33 = new DevExpress.XtraEditors.PanelControl();
            this.lb_tiennop = new DevExpress.XtraEditors.LabelControl();
            this.panelControl35 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl31 = new DevExpress.XtraEditors.PanelControl();
            this.lb_Tientamung = new DevExpress.XtraEditors.TextEdit();
            this.panelControl32 = new DevExpress.XtraEditors.PanelControl();
            this.lb_tientamung1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl23 = new DevExpress.XtraEditors.PanelControl();
            this.lb_tongcong = new DevExpress.XtraEditors.TextEdit();
            this.panelControl30 = new DevExpress.XtraEditors.PanelControl();
            this.lb_tongcong1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl34 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl36 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl37 = new DevExpress.XtraEditors.PanelControl();
            this.txtTraHang = new DevExpress.XtraEditors.TextEdit();
            this.panelControl38 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl13 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl28 = new DevExpress.XtraEditors.PanelControl();
            this.lbGiamGia = new DevExpress.XtraEditors.TextEdit();
            this.panelControl29 = new DevExpress.XtraEditors.PanelControl();
            this.lb_Giamgia = new DevExpress.XtraEditors.LabelControl();
            this.panelControl25 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl26 = new DevExpress.XtraEditors.PanelControl();
            this.lbPhuThu = new DevExpress.XtraEditors.TextEdit();
            this.panelControl27 = new DevExpress.XtraEditors.PanelControl();
            this.lb_Phuthu = new DevExpress.XtraEditors.LabelControl();
            this.panelControl21 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl22 = new DevExpress.XtraEditors.PanelControl();
            this.lbTotal = new DevExpress.XtraEditors.TextEdit();
            this.panelControl24 = new DevExpress.XtraEditors.PanelControl();
            this.lb_Thanhtien = new DevExpress.XtraEditors.LabelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl18 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl19 = new DevExpress.XtraEditors.PanelControl();
            this.lbtimeby = new DevExpress.XtraEditors.TextEdit();
            this.panelControl20 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl40 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl41 = new DevExpress.XtraEditors.PanelControl();
            this.lb_NhanVien = new DevExpress.XtraEditors.TextEdit();
            this.panelControl42 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl39 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl43 = new DevExpress.XtraEditors.PanelControl();
            this.btnTimkiem = new DevExpress.XtraEditors.SimpleButton();
            this.dtpNgay = new DevExpress.XtraEditors.DateEdit();
            this.panelControl44 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl16 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl17 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.btnBanve1 = new DevExpress.XtraEditors.PanelControl();
            this.title = new DevExpress.XtraEditors.LabelControl();
            this.home = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).BeginInit();
            this.panelControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnInHangHoa)).BeginInit();
            this.btnInHangHoa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnHoantacketca)).BeginInit();
            this.btnHoantacketca.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            this.panelControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gr_SelectProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_SelectProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lb_Tongtiennop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl33)).BeginInit();
            this.panelControl33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl31)).BeginInit();
            this.panelControl31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lb_Tientamung.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl32)).BeginInit();
            this.panelControl32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl23)).BeginInit();
            this.panelControl23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lb_tongcong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl30)).BeginInit();
            this.panelControl30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl36)).BeginInit();
            this.panelControl36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl37)).BeginInit();
            this.panelControl37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTraHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl38)).BeginInit();
            this.panelControl38.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).BeginInit();
            this.panelControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl28)).BeginInit();
            this.panelControl28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbGiamGia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl29)).BeginInit();
            this.panelControl29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl25)).BeginInit();
            this.panelControl25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl26)).BeginInit();
            this.panelControl26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbPhuThu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl27)).BeginInit();
            this.panelControl27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl21)).BeginInit();
            this.panelControl21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).BeginInit();
            this.panelControl22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl24)).BeginInit();
            this.panelControl24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).BeginInit();
            this.panelControl18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).BeginInit();
            this.panelControl19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbtimeby.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl20)).BeginInit();
            this.panelControl20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl40)).BeginInit();
            this.panelControl40.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl41)).BeginInit();
            this.panelControl41.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lb_NhanVien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl42)).BeginInit();
            this.panelControl42.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl39)).BeginInit();
            this.panelControl39.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl43)).BeginInit();
            this.panelControl43.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl44)).BeginInit();
            this.panelControl44.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).BeginInit();
            this.panelControl16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBanve1)).BeginInit();
            this.btnBanve1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.home)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl14);
            this.panelControl1.Controls.Add(this.btnInHangHoa);
            this.panelControl1.Controls.Add(this.btnHoantacketca);
            this.panelControl1.Controls.Add(this.panelControl15);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 729);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1029, 43);
            this.panelControl1.TabIndex = 4;
            // 
            // panelControl14
            // 
            this.panelControl14.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl14.Controls.Add(this.btnPrint);
            this.panelControl14.Controls.Add(this.pictureBox1);
            this.panelControl14.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl14.Location = new System.Drawing.Point(375, 2);
            this.panelControl14.Name = "panelControl14";
            this.panelControl14.Size = new System.Drawing.Size(158, 39);
            this.panelControl14.TabIndex = 61;
            this.panelControl14.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnPrint.Location = new System.Drawing.Point(53, 11);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(72, 19);
            this.btnPrint.TabIndex = 2;
            this.btnPrint.Text = "In kết ca";
            this.btnPrint.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::KP_RES.Properties.Resources.printer_26;
            this.pictureBox1.Location = new System.Drawing.Point(10, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(35, 32);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnInHangHoa
            // 
            this.btnInHangHoa.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnInHangHoa.Controls.Add(this.lblInHangHoa);
            this.btnInHangHoa.Controls.Add(this.pictureBox3);
            this.btnInHangHoa.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnInHangHoa.Location = new System.Drawing.Point(533, 2);
            this.btnInHangHoa.Name = "btnInHangHoa";
            this.btnInHangHoa.Size = new System.Drawing.Size(181, 39);
            this.btnInHangHoa.TabIndex = 63;
            this.btnInHangHoa.Click += new System.EventHandler(this.btnInHangHoa_Click);
            // 
            // lblInHangHoa
            // 
            this.lblInHangHoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lblInHangHoa.Location = new System.Drawing.Point(53, 11);
            this.lblInHangHoa.Name = "lblInHangHoa";
            this.lblInHangHoa.Size = new System.Drawing.Size(124, 19);
            this.lblInHangHoa.TabIndex = 2;
            this.lblInHangHoa.Text = "In DS hàng hóa";
            this.lblInHangHoa.Click += new System.EventHandler(this.btnInHangHoa_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::KP_RES.Properties.Resources.printer_26;
            this.pictureBox3.Location = new System.Drawing.Point(10, 4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(35, 32);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.btnInHangHoa_Click);
            // 
            // btnHoantacketca
            // 
            this.btnHoantacketca.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnHoantacketca.Controls.Add(this.btnHoantatketca);
            this.btnHoantacketca.Controls.Add(this.pictureBox2);
            this.btnHoantacketca.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnHoantacketca.Location = new System.Drawing.Point(714, 2);
            this.btnHoantacketca.Name = "btnHoantacketca";
            this.btnHoantacketca.Size = new System.Drawing.Size(194, 39);
            this.btnHoantacketca.TabIndex = 64;
            this.btnHoantacketca.Click += new System.EventHandler(this.btnHoantatketca_Click);
            // 
            // btnHoantatketca
            // 
            this.btnHoantatketca.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnHoantatketca.Location = new System.Drawing.Point(53, 11);
            this.btnHoantatketca.Name = "btnHoantatketca";
            this.btnHoantatketca.Size = new System.Drawing.Size(126, 19);
            this.btnHoantatketca.TabIndex = 2;
            this.btnHoantatketca.Text = "Hoàn tất kết ca";
            this.btnHoantatketca.Click += new System.EventHandler(this.btnHoantatketca_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::KP_RES.Properties.Resources.open_in_browser_26;
            this.pictureBox2.Location = new System.Drawing.Point(10, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(35, 32);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.btnHoantatketca_Click);
            // 
            // panelControl15
            // 
            this.panelControl15.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl15.Controls.Add(this.btnThoat);
            this.panelControl15.Controls.Add(this.pictureBox5);
            this.panelControl15.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl15.Location = new System.Drawing.Point(908, 2);
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(119, 39);
            this.panelControl15.TabIndex = 60;
            this.panelControl15.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // btnThoat
            // 
            this.btnThoat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnThoat.Location = new System.Drawing.Point(53, 11);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(42, 19);
            this.btnThoat.TabIndex = 2;
            this.btnThoat.Text = "Đóng";
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.pictureBox5.Location = new System.Drawing.Point(10, 4);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(35, 32);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox5.TabIndex = 0;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // gr_SelectProduct
            // 
            this.gr_SelectProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gr_SelectProduct.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gr_SelectProduct.Location = new System.Drawing.Point(2, 2);
            this.gr_SelectProduct.MainView = this.gv_SelectProduct;
            this.gr_SelectProduct.Name = "gr_SelectProduct";
            this.gr_SelectProduct.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1});
            this.gr_SelectProduct.Size = new System.Drawing.Size(515, 678);
            this.gr_SelectProduct.TabIndex = 20;
            this.gr_SelectProduct.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv_SelectProduct});
            // 
            // gv_SelectProduct
            // 
            this.gv_SelectProduct.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.gv_SelectProduct.Appearance.FocusedRow.Options.UseFont = true;
            this.gv_SelectProduct.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Red;
            this.gv_SelectProduct.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gv_SelectProduct.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.75F);
            this.gv_SelectProduct.Appearance.Row.Options.UseFont = true;
            this.gv_SelectProduct.ColumnPanelRowHeight = 43;
            this.gv_SelectProduct.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SOTT,
            this.TEN_HANGHOA,
            this.SOLUONG,
            this.GIABAN,
            this.CHIETKHAU,
            this.Thanh_Tien,
            this.MA_HANGHOA,
            this.GHICHU,
            this.MA_BEP,
            this.THUE,
            this.TRANGTHAI,
            this.IS_COMBO,
            this.IS_INBEP});
            this.gv_SelectProduct.GridControl = this.gr_SelectProduct;
            this.gv_SelectProduct.Name = "gv_SelectProduct";
            this.gv_SelectProduct.OptionsView.ShowGroupPanel = false;
            this.gv_SelectProduct.OptionsView.ShowIndicator = false;
            this.gv_SelectProduct.RowHeight = 35;
            this.gv_SelectProduct.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.TEN_HANGHOA, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gv_SelectProduct.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gv_SelectProduct.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gv_SelectProduct_CustomDrawCell);
            // 
            // SOTT
            // 
            this.SOTT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.SOTT.AppearanceCell.Options.UseFont = true;
            this.SOTT.AppearanceCell.Options.UseTextOptions = true;
            this.SOTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOTT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.SOTT.AppearanceHeader.Options.UseFont = true;
            this.SOTT.AppearanceHeader.Options.UseTextOptions = true;
            this.SOTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOTT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOTT.Caption = "TT";
            this.SOTT.FieldName = "SOTT";
            this.SOTT.Name = "SOTT";
            this.SOTT.OptionsColumn.AllowEdit = false;
            this.SOTT.OptionsColumn.AllowFocus = false;
            this.SOTT.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.SOTT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.SOTT.OptionsColumn.FixedWidth = true;
            this.SOTT.OptionsFilter.AllowFilter = false;
            this.SOTT.Visible = true;
            this.SOTT.VisibleIndex = 0;
            this.SOTT.Width = 60;
            // 
            // TEN_HANGHOA
            // 
            this.TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_HANGHOA.Caption = "Tên Hàng Hóa";
            this.TEN_HANGHOA.FieldName = "TEN_HANGHOA";
            this.TEN_HANGHOA.Name = "TEN_HANGHOA";
            this.TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this.TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this.TEN_HANGHOA.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.TEN_HANGHOA.OptionsFilter.AllowFilter = false;
            this.TEN_HANGHOA.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.TEN_HANGHOA.Visible = true;
            this.TEN_HANGHOA.VisibleIndex = 1;
            this.TEN_HANGHOA.Width = 222;
            // 
            // SOLUONG
            // 
            this.SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.SOLUONG.AppearanceCell.Options.UseFont = true;
            this.SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.Caption = "SL";
            this.SOLUONG.ColumnEdit = this.repositoryItemCalcEdit1;
            this.SOLUONG.FieldName = "SOLUONG";
            this.SOLUONG.Name = "SOLUONG";
            this.SOLUONG.OptionsColumn.AllowEdit = false;
            this.SOLUONG.OptionsColumn.AllowFocus = false;
            this.SOLUONG.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.SOLUONG.OptionsColumn.FixedWidth = true;
            this.SOLUONG.OptionsFilter.AllowFilter = false;
            this.SOLUONG.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.SOLUONG.Visible = true;
            this.SOLUONG.VisibleIndex = 2;
            this.SOLUONG.Width = 70;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // GIABAN
            // 
            this.GIABAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GIABAN.AppearanceCell.Options.UseFont = true;
            this.GIABAN.AppearanceCell.Options.UseTextOptions = true;
            this.GIABAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIABAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.GIABAN.AppearanceHeader.Options.UseFont = true;
            this.GIABAN.AppearanceHeader.Options.UseTextOptions = true;
            this.GIABAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIABAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN.Caption = "Đơn giá";
            this.GIABAN.DisplayFormat.FormatString = "{0:#,###0}";
            this.GIABAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GIABAN.FieldName = "GIABAN";
            this.GIABAN.Name = "GIABAN";
            this.GIABAN.OptionsColumn.AllowEdit = false;
            this.GIABAN.OptionsColumn.AllowFocus = false;
            this.GIABAN.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.GIABAN.OptionsColumn.FixedWidth = true;
            this.GIABAN.OptionsFilter.AllowFilter = false;
            this.GIABAN.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.GIABAN.Visible = true;
            this.GIABAN.VisibleIndex = 3;
            this.GIABAN.Width = 100;
            // 
            // CHIETKHAU
            // 
            this.CHIETKHAU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.CHIETKHAU.AppearanceCell.Options.UseFont = true;
            this.CHIETKHAU.AppearanceCell.Options.UseTextOptions = true;
            this.CHIETKHAU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CHIETKHAU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHIETKHAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.CHIETKHAU.AppearanceHeader.Options.UseFont = true;
            this.CHIETKHAU.AppearanceHeader.Options.UseTextOptions = true;
            this.CHIETKHAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHIETKHAU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHIETKHAU.Caption = "Chiết khấu";
            this.CHIETKHAU.DisplayFormat.FormatString = "{0:#,###0}%";
            this.CHIETKHAU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CHIETKHAU.FieldName = "CHIETKHAU";
            this.CHIETKHAU.Name = "CHIETKHAU";
            this.CHIETKHAU.OptionsColumn.AllowEdit = false;
            this.CHIETKHAU.OptionsColumn.AllowFocus = false;
            this.CHIETKHAU.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.CHIETKHAU.OptionsColumn.FixedWidth = true;
            this.CHIETKHAU.OptionsFilter.AllowFilter = false;
            this.CHIETKHAU.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.CHIETKHAU.Visible = true;
            this.CHIETKHAU.VisibleIndex = 4;
            // 
            // Thanh_Tien
            // 
            this.Thanh_Tien.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Thanh_Tien.AppearanceCell.Options.UseFont = true;
            this.Thanh_Tien.AppearanceCell.Options.UseTextOptions = true;
            this.Thanh_Tien.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Thanh_Tien.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Thanh_Tien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.Thanh_Tien.AppearanceHeader.Options.UseFont = true;
            this.Thanh_Tien.AppearanceHeader.Options.UseTextOptions = true;
            this.Thanh_Tien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Thanh_Tien.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Thanh_Tien.Caption = "Thành Tiền";
            this.Thanh_Tien.DisplayFormat.FormatString = "{0:#,###0}";
            this.Thanh_Tien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.Thanh_Tien.FieldName = "Thanh_Tien";
            this.Thanh_Tien.Name = "Thanh_Tien";
            this.Thanh_Tien.OptionsColumn.AllowEdit = false;
            this.Thanh_Tien.OptionsColumn.AllowFocus = false;
            this.Thanh_Tien.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.Thanh_Tien.OptionsColumn.FixedWidth = true;
            this.Thanh_Tien.OptionsFilter.AllowFilter = false;
            this.Thanh_Tien.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.Thanh_Tien.Visible = true;
            this.Thanh_Tien.VisibleIndex = 5;
            this.Thanh_Tien.Width = 120;
            // 
            // MA_HANGHOA
            // 
            this.MA_HANGHOA.Caption = "Mã Hàng Hóa";
            this.MA_HANGHOA.FieldName = "MA_HANGHOA";
            this.MA_HANGHOA.Name = "MA_HANGHOA";
            this.MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this.MA_HANGHOA.OptionsColumn.AllowFocus = false;
            // 
            // GHICHU
            // 
            this.GHICHU.Caption = "Ghi Chú";
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.Name = "GHICHU";
            // 
            // MA_BEP
            // 
            this.MA_BEP.Caption = "Máy In Bếp";
            this.MA_BEP.FieldName = "MA_BEP";
            this.MA_BEP.Name = "MA_BEP";
            // 
            // THUE
            // 
            this.THUE.Caption = "VAT";
            this.THUE.FieldName = "THUE";
            this.THUE.Name = "THUE";
            // 
            // TRANGTHAI
            // 
            this.TRANGTHAI.Caption = "TRANGTHAI";
            this.TRANGTHAI.FieldName = "TRANGTHAI";
            this.TRANGTHAI.Name = "TRANGTHAI";
            this.TRANGTHAI.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI.OptionsColumn.AllowFocus = false;
            // 
            // IS_COMBO
            // 
            this.IS_COMBO.Caption = "IS_COMBO";
            this.IS_COMBO.FieldName = "IS_COMBO";
            this.IS_COMBO.Name = "IS_COMBO";
            // 
            // IS_INBEP
            // 
            this.IS_INBEP.Caption = "IS_INBEP";
            this.IS_INBEP.FieldName = "IS_INBEP";
            this.IS_INBEP.Name = "IS_INBEP";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl4);
            this.panelControl2.Controls.Add(this.panelControl35);
            this.panelControl2.Controls.Add(this.panelControl7);
            this.panelControl2.Controls.Add(this.panelControl5);
            this.panelControl2.Controls.Add(this.panelControl34);
            this.panelControl2.Controls.Add(this.panelControl36);
            this.panelControl2.Controls.Add(this.panelControl13);
            this.panelControl2.Controls.Add(this.panelControl25);
            this.panelControl2.Controls.Add(this.panelControl21);
            this.panelControl2.Controls.Add(this.panelControl6);
            this.panelControl2.Controls.Add(this.panelControl18);
            this.panelControl2.Controls.Add(this.panelControl40);
            this.panelControl2.Controls.Add(this.panelControl39);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(467, 682);
            this.panelControl2.TabIndex = 59;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Controls.Add(this.panelControl10);
            this.panelControl4.Controls.Add(this.panelControl33);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(2, 493);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(463, 49);
            this.panelControl4.TabIndex = 68;
            // 
            // panelControl10
            // 
            this.panelControl10.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl10.Controls.Add(this.lb_Tongtiennop);
            this.panelControl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl10.Location = new System.Drawing.Point(156, 0);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(307, 49);
            this.panelControl10.TabIndex = 18;
            // 
            // lb_Tongtiennop
            // 
            this.lb_Tongtiennop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lb_Tongtiennop.EditValue = "";
            this.lb_Tongtiennop.Location = new System.Drawing.Point(0, 0);
            this.lb_Tongtiennop.Name = "lb_Tongtiennop";
            this.lb_Tongtiennop.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Tongtiennop.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lb_Tongtiennop.Properties.Appearance.Options.UseFont = true;
            this.lb_Tongtiennop.Properties.Appearance.Options.UseForeColor = true;
            this.lb_Tongtiennop.Properties.Appearance.Options.UseTextOptions = true;
            this.lb_Tongtiennop.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lb_Tongtiennop.Size = new System.Drawing.Size(307, 48);
            this.lb_Tongtiennop.TabIndex = 4;
            this.lb_Tongtiennop.Tag = "0";
            // 
            // panelControl33
            // 
            this.panelControl33.Controls.Add(this.lb_tiennop);
            this.panelControl33.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl33.Location = new System.Drawing.Point(0, 0);
            this.panelControl33.Name = "panelControl33";
            this.panelControl33.Size = new System.Drawing.Size(156, 49);
            this.panelControl33.TabIndex = 17;
            // 
            // lb_tiennop
            // 
            this.lb_tiennop.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_tiennop.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_tiennop.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_tiennop.Location = new System.Drawing.Point(10, 19);
            this.lb_tiennop.Name = "lb_tiennop";
            this.lb_tiennop.Size = new System.Drawing.Size(113, 19);
            this.lb_tiennop.TabIndex = 15;
            this.lb_tiennop.Text = "Tổng tiền nộp";
            // 
            // panelControl35
            // 
            this.panelControl35.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl35.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl35.Location = new System.Drawing.Point(2, 476);
            this.panelControl35.Name = "panelControl35";
            this.panelControl35.Size = new System.Drawing.Size(463, 17);
            this.panelControl35.TabIndex = 67;
            // 
            // panelControl7
            // 
            this.panelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl7.Controls.Add(this.panelControl31);
            this.panelControl7.Controls.Add(this.panelControl32);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(2, 427);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(463, 49);
            this.panelControl7.TabIndex = 66;
            // 
            // panelControl31
            // 
            this.panelControl31.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl31.Controls.Add(this.lb_Tientamung);
            this.panelControl31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl31.Location = new System.Drawing.Point(156, 0);
            this.panelControl31.Name = "panelControl31";
            this.panelControl31.Size = new System.Drawing.Size(307, 49);
            this.panelControl31.TabIndex = 18;
            // 
            // lb_Tientamung
            // 
            this.lb_Tientamung.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lb_Tientamung.EditValue = "";
            this.lb_Tientamung.Location = new System.Drawing.Point(0, 0);
            this.lb_Tientamung.Name = "lb_Tientamung";
            this.lb_Tientamung.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Tientamung.Properties.Appearance.Options.UseFont = true;
            this.lb_Tientamung.Properties.Appearance.Options.UseTextOptions = true;
            this.lb_Tientamung.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lb_Tientamung.Size = new System.Drawing.Size(307, 48);
            this.lb_Tientamung.TabIndex = 4;
            this.lb_Tientamung.Tag = "0";
            // 
            // panelControl32
            // 
            this.panelControl32.Controls.Add(this.lb_tientamung1);
            this.panelControl32.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl32.Location = new System.Drawing.Point(0, 0);
            this.panelControl32.Name = "panelControl32";
            this.panelControl32.Size = new System.Drawing.Size(156, 49);
            this.panelControl32.TabIndex = 17;
            // 
            // lb_tientamung1
            // 
            this.lb_tientamung1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_tientamung1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_tientamung1.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_tientamung1.Location = new System.Drawing.Point(10, 18);
            this.lb_tientamung1.Name = "lb_tientamung1";
            this.lb_tientamung1.Size = new System.Drawing.Size(109, 19);
            this.lb_tientamung1.TabIndex = 15;
            this.lb_tientamung1.Text = "Tiền tạm ứng";
            // 
            // panelControl5
            // 
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl5.Controls.Add(this.panelControl23);
            this.panelControl5.Controls.Add(this.panelControl30);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl5.Location = new System.Drawing.Point(2, 378);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(463, 49);
            this.panelControl5.TabIndex = 65;
            // 
            // panelControl23
            // 
            this.panelControl23.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl23.Controls.Add(this.lb_tongcong);
            this.panelControl23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl23.Location = new System.Drawing.Point(156, 0);
            this.panelControl23.Name = "panelControl23";
            this.panelControl23.Size = new System.Drawing.Size(307, 49);
            this.panelControl23.TabIndex = 18;
            // 
            // lb_tongcong
            // 
            this.lb_tongcong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lb_tongcong.EditValue = "";
            this.lb_tongcong.Location = new System.Drawing.Point(0, 0);
            this.lb_tongcong.Name = "lb_tongcong";
            this.lb_tongcong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_tongcong.Properties.Appearance.Options.UseFont = true;
            this.lb_tongcong.Properties.Appearance.Options.UseTextOptions = true;
            this.lb_tongcong.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lb_tongcong.Size = new System.Drawing.Size(307, 48);
            this.lb_tongcong.TabIndex = 4;
            this.lb_tongcong.Tag = "0";
            // 
            // panelControl30
            // 
            this.panelControl30.Controls.Add(this.lb_tongcong1);
            this.panelControl30.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl30.Location = new System.Drawing.Point(0, 0);
            this.panelControl30.Name = "panelControl30";
            this.panelControl30.Size = new System.Drawing.Size(156, 49);
            this.panelControl30.TabIndex = 17;
            // 
            // lb_tongcong1
            // 
            this.lb_tongcong1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_tongcong1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_tongcong1.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_tongcong1.Location = new System.Drawing.Point(10, 18);
            this.lb_tongcong1.Name = "lb_tongcong1";
            this.lb_tongcong1.Size = new System.Drawing.Size(84, 19);
            this.lb_tongcong1.TabIndex = 15;
            this.lb_tongcong1.Text = "Tổng cộng";
            // 
            // panelControl34
            // 
            this.panelControl34.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl34.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl34.Location = new System.Drawing.Point(2, 362);
            this.panelControl34.Name = "panelControl34";
            this.panelControl34.Size = new System.Drawing.Size(463, 16);
            this.panelControl34.TabIndex = 64;
            // 
            // panelControl36
            // 
            this.panelControl36.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl36.Controls.Add(this.panelControl37);
            this.panelControl36.Controls.Add(this.panelControl38);
            this.panelControl36.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl36.Location = new System.Drawing.Point(2, 313);
            this.panelControl36.Name = "panelControl36";
            this.panelControl36.Size = new System.Drawing.Size(463, 49);
            this.panelControl36.TabIndex = 63;
            // 
            // panelControl37
            // 
            this.panelControl37.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl37.Controls.Add(this.txtTraHang);
            this.panelControl37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl37.Location = new System.Drawing.Point(156, 0);
            this.panelControl37.Name = "panelControl37";
            this.panelControl37.Size = new System.Drawing.Size(307, 49);
            this.panelControl37.TabIndex = 18;
            // 
            // txtTraHang
            // 
            this.txtTraHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTraHang.EditValue = "";
            this.txtTraHang.Location = new System.Drawing.Point(0, 0);
            this.txtTraHang.Name = "txtTraHang";
            this.txtTraHang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTraHang.Properties.Appearance.Options.UseFont = true;
            this.txtTraHang.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTraHang.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTraHang.Size = new System.Drawing.Size(307, 48);
            this.txtTraHang.TabIndex = 4;
            this.txtTraHang.Tag = "0";
            // 
            // panelControl38
            // 
            this.panelControl38.Controls.Add(this.labelControl3);
            this.panelControl38.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl38.Location = new System.Drawing.Point(0, 0);
            this.panelControl38.Name = "panelControl38";
            this.panelControl38.Size = new System.Drawing.Size(156, 49);
            this.panelControl38.TabIndex = 17;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl3.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl3.Location = new System.Drawing.Point(10, 14);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(66, 19);
            this.labelControl3.TabIndex = 15;
            this.labelControl3.Text = "Tiền giờ";
            // 
            // panelControl13
            // 
            this.panelControl13.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl13.Controls.Add(this.panelControl28);
            this.panelControl13.Controls.Add(this.panelControl29);
            this.panelControl13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl13.Location = new System.Drawing.Point(2, 264);
            this.panelControl13.Name = "panelControl13";
            this.panelControl13.Size = new System.Drawing.Size(463, 49);
            this.panelControl13.TabIndex = 62;
            // 
            // panelControl28
            // 
            this.panelControl28.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl28.Controls.Add(this.lbGiamGia);
            this.panelControl28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl28.Location = new System.Drawing.Point(156, 0);
            this.panelControl28.Name = "panelControl28";
            this.panelControl28.Size = new System.Drawing.Size(307, 49);
            this.panelControl28.TabIndex = 18;
            // 
            // lbGiamGia
            // 
            this.lbGiamGia.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbGiamGia.EditValue = "";
            this.lbGiamGia.Location = new System.Drawing.Point(0, 0);
            this.lbGiamGia.Name = "lbGiamGia";
            this.lbGiamGia.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGiamGia.Properties.Appearance.Options.UseFont = true;
            this.lbGiamGia.Properties.Appearance.Options.UseTextOptions = true;
            this.lbGiamGia.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbGiamGia.Size = new System.Drawing.Size(307, 48);
            this.lbGiamGia.TabIndex = 4;
            this.lbGiamGia.Tag = "0";
            // 
            // panelControl29
            // 
            this.panelControl29.Controls.Add(this.lb_Giamgia);
            this.panelControl29.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl29.Location = new System.Drawing.Point(0, 0);
            this.panelControl29.Name = "panelControl29";
            this.panelControl29.Size = new System.Drawing.Size(156, 49);
            this.panelControl29.TabIndex = 17;
            // 
            // lb_Giamgia
            // 
            this.lb_Giamgia.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_Giamgia.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_Giamgia.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_Giamgia.Location = new System.Drawing.Point(10, 18);
            this.lb_Giamgia.Name = "lb_Giamgia";
            this.lb_Giamgia.Size = new System.Drawing.Size(72, 19);
            this.lb_Giamgia.TabIndex = 15;
            this.lb_Giamgia.Text = "Giảm giá";
            // 
            // panelControl25
            // 
            this.panelControl25.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl25.Controls.Add(this.panelControl26);
            this.panelControl25.Controls.Add(this.panelControl27);
            this.panelControl25.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl25.Location = new System.Drawing.Point(2, 215);
            this.panelControl25.Name = "panelControl25";
            this.panelControl25.Size = new System.Drawing.Size(463, 49);
            this.panelControl25.TabIndex = 61;
            // 
            // panelControl26
            // 
            this.panelControl26.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl26.Controls.Add(this.lbPhuThu);
            this.panelControl26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl26.Location = new System.Drawing.Point(156, 0);
            this.panelControl26.Name = "panelControl26";
            this.panelControl26.Size = new System.Drawing.Size(307, 49);
            this.panelControl26.TabIndex = 18;
            // 
            // lbPhuThu
            // 
            this.lbPhuThu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbPhuThu.EditValue = "";
            this.lbPhuThu.Location = new System.Drawing.Point(0, 0);
            this.lbPhuThu.Name = "lbPhuThu";
            this.lbPhuThu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPhuThu.Properties.Appearance.Options.UseFont = true;
            this.lbPhuThu.Properties.Appearance.Options.UseTextOptions = true;
            this.lbPhuThu.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbPhuThu.Size = new System.Drawing.Size(307, 48);
            this.lbPhuThu.TabIndex = 4;
            this.lbPhuThu.Tag = "0";
            // 
            // panelControl27
            // 
            this.panelControl27.Controls.Add(this.lb_Phuthu);
            this.panelControl27.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl27.Location = new System.Drawing.Point(0, 0);
            this.panelControl27.Name = "panelControl27";
            this.panelControl27.Size = new System.Drawing.Size(156, 49);
            this.panelControl27.TabIndex = 17;
            // 
            // lb_Phuthu
            // 
            this.lb_Phuthu.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_Phuthu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_Phuthu.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_Phuthu.Location = new System.Drawing.Point(10, 18);
            this.lb_Phuthu.Name = "lb_Phuthu";
            this.lb_Phuthu.Size = new System.Drawing.Size(63, 19);
            this.lb_Phuthu.TabIndex = 15;
            this.lb_Phuthu.Text = "Phụ thu";
            // 
            // panelControl21
            // 
            this.panelControl21.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl21.Controls.Add(this.panelControl22);
            this.panelControl21.Controls.Add(this.panelControl24);
            this.panelControl21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl21.Location = new System.Drawing.Point(2, 166);
            this.panelControl21.Name = "panelControl21";
            this.panelControl21.Size = new System.Drawing.Size(463, 49);
            this.panelControl21.TabIndex = 60;
            // 
            // panelControl22
            // 
            this.panelControl22.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl22.Controls.Add(this.lbTotal);
            this.panelControl22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl22.Location = new System.Drawing.Point(156, 0);
            this.panelControl22.Name = "panelControl22";
            this.panelControl22.Size = new System.Drawing.Size(307, 49);
            this.panelControl22.TabIndex = 18;
            // 
            // lbTotal
            // 
            this.lbTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTotal.EditValue = "";
            this.lbTotal.Location = new System.Drawing.Point(0, 0);
            this.lbTotal.Name = "lbTotal";
            this.lbTotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotal.Properties.Appearance.Options.UseFont = true;
            this.lbTotal.Properties.Appearance.Options.UseTextOptions = true;
            this.lbTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbTotal.Size = new System.Drawing.Size(307, 48);
            this.lbTotal.TabIndex = 4;
            this.lbTotal.Tag = "0";
            // 
            // panelControl24
            // 
            this.panelControl24.Controls.Add(this.lb_Thanhtien);
            this.panelControl24.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl24.Location = new System.Drawing.Point(0, 0);
            this.panelControl24.Name = "panelControl24";
            this.panelControl24.Size = new System.Drawing.Size(156, 49);
            this.panelControl24.TabIndex = 17;
            // 
            // lb_Thanhtien
            // 
            this.lb_Thanhtien.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_Thanhtien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_Thanhtien.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_Thanhtien.Location = new System.Drawing.Point(10, 18);
            this.lb_Thanhtien.Name = "lb_Thanhtien";
            this.lb_Thanhtien.Size = new System.Drawing.Size(88, 19);
            this.lb_Thanhtien.TabIndex = 15;
            this.lb_Thanhtien.Text = "Thành tiền";
            // 
            // panelControl6
            // 
            this.panelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl6.Location = new System.Drawing.Point(2, 149);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(463, 17);
            this.panelControl6.TabIndex = 59;
            // 
            // panelControl18
            // 
            this.panelControl18.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl18.Controls.Add(this.panelControl19);
            this.panelControl18.Controls.Add(this.panelControl20);
            this.panelControl18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl18.Location = new System.Drawing.Point(2, 100);
            this.panelControl18.Name = "panelControl18";
            this.panelControl18.Size = new System.Drawing.Size(463, 49);
            this.panelControl18.TabIndex = 58;
            // 
            // panelControl19
            // 
            this.panelControl19.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl19.Controls.Add(this.lbtimeby);
            this.panelControl19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl19.Location = new System.Drawing.Point(156, 0);
            this.panelControl19.Name = "panelControl19";
            this.panelControl19.Size = new System.Drawing.Size(307, 49);
            this.panelControl19.TabIndex = 18;
            // 
            // lbtimeby
            // 
            this.lbtimeby.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbtimeby.EditValue = "Ca sáng";
            this.lbtimeby.Location = new System.Drawing.Point(0, 0);
            this.lbtimeby.Name = "lbtimeby";
            this.lbtimeby.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtimeby.Properties.Appearance.Options.UseFont = true;
            this.lbtimeby.Size = new System.Drawing.Size(307, 48);
            this.lbtimeby.TabIndex = 4;
            this.lbtimeby.Tag = "0";
            // 
            // panelControl20
            // 
            this.panelControl20.Controls.Add(this.labelControl2);
            this.panelControl20.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl20.Location = new System.Drawing.Point(0, 0);
            this.panelControl20.Name = "panelControl20";
            this.panelControl20.Size = new System.Drawing.Size(156, 49);
            this.panelControl20.TabIndex = 17;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl2.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl2.Location = new System.Drawing.Point(10, 18);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(62, 19);
            this.labelControl2.TabIndex = 16;
            this.labelControl2.Text = "Ca bán:";
            // 
            // panelControl40
            // 
            this.panelControl40.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl40.Controls.Add(this.panelControl41);
            this.panelControl40.Controls.Add(this.panelControl42);
            this.panelControl40.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl40.Location = new System.Drawing.Point(2, 51);
            this.panelControl40.Name = "panelControl40";
            this.panelControl40.Size = new System.Drawing.Size(463, 49);
            this.panelControl40.TabIndex = 57;
            // 
            // panelControl41
            // 
            this.panelControl41.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl41.Controls.Add(this.lb_NhanVien);
            this.panelControl41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl41.Location = new System.Drawing.Point(156, 0);
            this.panelControl41.Name = "panelControl41";
            this.panelControl41.Size = new System.Drawing.Size(307, 49);
            this.panelControl41.TabIndex = 18;
            // 
            // lb_NhanVien
            // 
            this.lb_NhanVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lb_NhanVien.EditValue = "PHAN VĂN THÔNG";
            this.lb_NhanVien.Location = new System.Drawing.Point(0, 0);
            this.lb_NhanVien.Name = "lb_NhanVien";
            this.lb_NhanVien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_NhanVien.Properties.Appearance.Options.UseFont = true;
            this.lb_NhanVien.Size = new System.Drawing.Size(307, 48);
            this.lb_NhanVien.TabIndex = 4;
            this.lb_NhanVien.Tag = "0";
            // 
            // panelControl42
            // 
            this.panelControl42.Controls.Add(this.labelControl1);
            this.panelControl42.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl42.Location = new System.Drawing.Point(0, 0);
            this.panelControl42.Name = "panelControl42";
            this.panelControl42.Size = new System.Drawing.Size(156, 49);
            this.panelControl42.TabIndex = 17;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl1.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl1.Location = new System.Drawing.Point(10, 18);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(87, 19);
            this.labelControl1.TabIndex = 16;
            this.labelControl1.Text = "Nhân viên:";
            // 
            // panelControl39
            // 
            this.panelControl39.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl39.Controls.Add(this.panelControl43);
            this.panelControl39.Controls.Add(this.panelControl44);
            this.panelControl39.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl39.Location = new System.Drawing.Point(2, 2);
            this.panelControl39.Name = "panelControl39";
            this.panelControl39.Size = new System.Drawing.Size(463, 49);
            this.panelControl39.TabIndex = 56;
            // 
            // panelControl43
            // 
            this.panelControl43.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl43.Controls.Add(this.btnTimkiem);
            this.panelControl43.Controls.Add(this.dtpNgay);
            this.panelControl43.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl43.Location = new System.Drawing.Point(156, 0);
            this.panelControl43.Name = "panelControl43";
            this.panelControl43.Size = new System.Drawing.Size(307, 49);
            this.panelControl43.TabIndex = 18;
            // 
            // btnTimkiem
            // 
            this.btnTimkiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimkiem.Appearance.Options.UseFont = true;
            this.btnTimkiem.Image = global::KP_RES.Properties.Resources.search_26;
            this.btnTimkiem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnTimkiem.Location = new System.Drawing.Point(218, 7);
            this.btnTimkiem.Margin = new System.Windows.Forms.Padding(4);
            this.btnTimkiem.Name = "btnTimkiem";
            this.btnTimkiem.Size = new System.Drawing.Size(80, 35);
            this.btnTimkiem.TabIndex = 3;
            this.btnTimkiem.Text = "&Xem";
            this.btnTimkiem.Click += new System.EventHandler(this.btnTimkiem_Click);
            // 
            // dtpNgay
            // 
            this.dtpNgay.EditValue = null;
            this.dtpNgay.EnterMoveNextControl = true;
            this.dtpNgay.Location = new System.Drawing.Point(2, 12);
            this.dtpNgay.Name = "dtpNgay";
            this.dtpNgay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgay.Properties.Appearance.Options.UseFont = true;
            this.dtpNgay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgay.Size = new System.Drawing.Size(211, 26);
            this.dtpNgay.TabIndex = 2;
            // 
            // panelControl44
            // 
            this.panelControl44.Controls.Add(this.labelControl4);
            this.panelControl44.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl44.Location = new System.Drawing.Point(0, 0);
            this.panelControl44.Name = "panelControl44";
            this.panelControl44.Size = new System.Drawing.Size(156, 49);
            this.panelControl44.TabIndex = 17;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl4.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl4.Location = new System.Drawing.Point(10, 15);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(96, 19);
            this.labelControl4.TabIndex = 15;
            this.labelControl4.Text = "Ngày kết ca";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.gr_SelectProduct);
            this.panelControl3.Controls.Add(this.panelControl9);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(469, 2);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(558, 682);
            this.panelControl3.TabIndex = 31;
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.panelControl16);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl9.Location = new System.Drawing.Point(517, 2);
            this.panelControl9.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(39, 678);
            this.panelControl9.TabIndex = 21;
            // 
            // panelControl16
            // 
            this.panelControl16.AutoSize = true;
            this.panelControl16.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl16.Controls.Add(this.panelControl17);
            this.panelControl16.Controls.Add(this.btnXuongit);
            this.panelControl16.Controls.Add(this.btnXuongnhieu);
            this.panelControl16.Controls.Add(this.btnLenit);
            this.panelControl16.Controls.Add(this.btnLennhieu);
            this.panelControl16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl16.Location = new System.Drawing.Point(2, 2);
            this.panelControl16.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl16.Name = "panelControl16";
            this.panelControl16.Size = new System.Drawing.Size(35, 674);
            this.panelControl16.TabIndex = 14;
            // 
            // panelControl17
            // 
            this.panelControl17.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl17.Location = new System.Drawing.Point(0, 160);
            this.panelControl17.Name = "panelControl17";
            this.panelControl17.Size = new System.Drawing.Size(35, 354);
            this.panelControl17.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 514);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 594);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.panelControl3);
            this.panelControl8.Controls.Add(this.panelControl2);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl8.Location = new System.Drawing.Point(0, 43);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(1029, 686);
            this.panelControl8.TabIndex = 60;
            // 
            // panelControl11
            // 
            this.panelControl11.Appearance.BackColor = System.Drawing.Color.White;
            this.panelControl11.Appearance.Options.UseBackColor = true;
            this.panelControl11.Controls.Add(this.panelControl12);
            this.panelControl11.Controls.Add(this.btnBanve1);
            this.panelControl11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl11.Location = new System.Drawing.Point(0, 0);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(1029, 43);
            this.panelControl11.TabIndex = 61;
            // 
            // panelControl12
            // 
            this.panelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl12.Controls.Add(this.pictureEdit2);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl12.Location = new System.Drawing.Point(469, 2);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(558, 39);
            this.panelControl12.TabIndex = 13;
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureEdit2.EditValue = global::KP_RES.Properties.Resources.KingPos_for_Restaurant;
            this.pictureEdit2.Location = new System.Drawing.Point(353, 2);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.PictureAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit2.Size = new System.Drawing.Size(203, 35);
            this.pictureEdit2.TabIndex = 2;
            // 
            // btnBanve1
            // 
            this.btnBanve1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnBanve1.Controls.Add(this.title);
            this.btnBanve1.Controls.Add(this.home);
            this.btnBanve1.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBanve1.Location = new System.Drawing.Point(2, 2);
            this.btnBanve1.Name = "btnBanve1";
            this.btnBanve1.Size = new System.Drawing.Size(467, 39);
            this.btnBanve1.TabIndex = 8;
            // 
            // title
            // 
            this.title.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.title.Location = new System.Drawing.Point(49, 11);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(51, 19);
            this.title.TabIndex = 2;
            this.title.Text = "Kết ca";
            // 
            // home
            // 
            this.home.Image = global::KP_RES.Properties.Resources.clock_26;
            this.home.Location = new System.Drawing.Point(10, 4);
            this.home.Name = "home";
            this.home.Size = new System.Drawing.Size(35, 32);
            this.home.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.home.TabIndex = 0;
            this.home.TabStop = false;
            // 
            // Frm_KetCaKhachSan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1029, 772);
            this.Controls.Add(this.panelControl8);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl11);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(200, 200);
            this.Name = "Frm_KetCaKhachSan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kết ca";
            this.Load += new System.EventHandler(this.Frm_Combo_Option_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).EndInit();
            this.panelControl14.ResumeLayout(false);
            this.panelControl14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnInHangHoa)).EndInit();
            this.btnInHangHoa.ResumeLayout(false);
            this.btnInHangHoa.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnHoantacketca)).EndInit();
            this.btnHoantacketca.ResumeLayout(false);
            this.btnHoantacketca.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            this.panelControl15.ResumeLayout(false);
            this.panelControl15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gr_SelectProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_SelectProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lb_Tongtiennop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl33)).EndInit();
            this.panelControl33.ResumeLayout(false);
            this.panelControl33.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl31)).EndInit();
            this.panelControl31.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lb_Tientamung.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl32)).EndInit();
            this.panelControl32.ResumeLayout(false);
            this.panelControl32.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl23)).EndInit();
            this.panelControl23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lb_tongcong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl30)).EndInit();
            this.panelControl30.ResumeLayout(false);
            this.panelControl30.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl36)).EndInit();
            this.panelControl36.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl37)).EndInit();
            this.panelControl37.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTraHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl38)).EndInit();
            this.panelControl38.ResumeLayout(false);
            this.panelControl38.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).EndInit();
            this.panelControl13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl28)).EndInit();
            this.panelControl28.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbGiamGia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl29)).EndInit();
            this.panelControl29.ResumeLayout(false);
            this.panelControl29.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl25)).EndInit();
            this.panelControl25.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl26)).EndInit();
            this.panelControl26.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbPhuThu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl27)).EndInit();
            this.panelControl27.ResumeLayout(false);
            this.panelControl27.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl21)).EndInit();
            this.panelControl21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).EndInit();
            this.panelControl22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl24)).EndInit();
            this.panelControl24.ResumeLayout(false);
            this.panelControl24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).EndInit();
            this.panelControl18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).EndInit();
            this.panelControl19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbtimeby.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl20)).EndInit();
            this.panelControl20.ResumeLayout(false);
            this.panelControl20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl40)).EndInit();
            this.panelControl40.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl41)).EndInit();
            this.panelControl41.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lb_NhanVien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl42)).EndInit();
            this.panelControl42.ResumeLayout(false);
            this.panelControl42.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl39)).EndInit();
            this.panelControl39.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl43)).EndInit();
            this.panelControl43.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl44)).EndInit();
            this.panelControl44.ResumeLayout(false);
            this.panelControl44.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.panelControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).EndInit();
            this.panelControl16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBanve1)).EndInit();
            this.btnBanve1.ResumeLayout(false);
            this.btnBanve1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.home)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gr_SelectProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView gv_SelectProduct;
        private DevExpress.XtraGrid.Columns.GridColumn SOTT;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn SOLUONG;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn GIABAN;
        private DevExpress.XtraGrid.Columns.GridColumn Thanh_Tien;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;
        private DevExpress.XtraGrid.Columns.GridColumn MA_BEP;
        private DevExpress.XtraGrid.Columns.GridColumn THUE;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI;
        private DevExpress.XtraGrid.Columns.GridColumn IS_COMBO;
        private DevExpress.XtraGrid.Columns.GridColumn IS_INBEP;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PanelControl btnBanve1;
        private DevExpress.XtraEditors.LabelControl title;
        private System.Windows.Forms.PictureBox home;
        private DevExpress.XtraEditors.PanelControl panelControl14;
        private DevExpress.XtraEditors.LabelControl btnPrint;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.PanelControl panelControl15;
        private DevExpress.XtraEditors.LabelControl btnThoat;
        private System.Windows.Forms.PictureBox pictureBox5;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.PanelControl panelControl16;
        private DevExpress.XtraEditors.PanelControl panelControl17;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraGrid.Columns.GridColumn CHIETKHAU;
        private DevExpress.XtraEditors.PanelControl btnInHangHoa;
        private DevExpress.XtraEditors.LabelControl lblInHangHoa;
        private System.Windows.Forms.PictureBox pictureBox3;
        private DevExpress.XtraEditors.PanelControl btnHoantacketca;
        private DevExpress.XtraEditors.LabelControl btnHoantatketca;
        private System.Windows.Forms.PictureBox pictureBox2;
        private DevExpress.XtraEditors.PanelControl panelControl39;
        private DevExpress.XtraEditors.PanelControl panelControl43;
        private DevExpress.XtraEditors.PanelControl panelControl44;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl34;
        private DevExpress.XtraEditors.PanelControl panelControl36;
        private DevExpress.XtraEditors.PanelControl panelControl37;
        private DevExpress.XtraEditors.TextEdit txtTraHang;
        private DevExpress.XtraEditors.PanelControl panelControl38;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl13;
        private DevExpress.XtraEditors.PanelControl panelControl28;
        private DevExpress.XtraEditors.TextEdit lbGiamGia;
        private DevExpress.XtraEditors.PanelControl panelControl29;
        private DevExpress.XtraEditors.LabelControl lb_Giamgia;
        private DevExpress.XtraEditors.PanelControl panelControl25;
        private DevExpress.XtraEditors.PanelControl panelControl26;
        private DevExpress.XtraEditors.TextEdit lbPhuThu;
        private DevExpress.XtraEditors.PanelControl panelControl27;
        private DevExpress.XtraEditors.LabelControl lb_Phuthu;
        private DevExpress.XtraEditors.PanelControl panelControl21;
        private DevExpress.XtraEditors.PanelControl panelControl22;
        private DevExpress.XtraEditors.TextEdit lbTotal;
        private DevExpress.XtraEditors.PanelControl panelControl24;
        private DevExpress.XtraEditors.LabelControl lb_Thanhtien;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.PanelControl panelControl18;
        private DevExpress.XtraEditors.PanelControl panelControl19;
        private DevExpress.XtraEditors.TextEdit lbtimeby;
        private DevExpress.XtraEditors.PanelControl panelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl40;
        private DevExpress.XtraEditors.PanelControl panelControl41;
        private DevExpress.XtraEditors.TextEdit lb_NhanVien;
        private DevExpress.XtraEditors.PanelControl panelControl42;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.TextEdit lb_Tongtiennop;
        private DevExpress.XtraEditors.PanelControl panelControl33;
        private DevExpress.XtraEditors.LabelControl lb_tiennop;
        private DevExpress.XtraEditors.PanelControl panelControl35;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.PanelControl panelControl31;
        private DevExpress.XtraEditors.TextEdit lb_Tientamung;
        private DevExpress.XtraEditors.PanelControl panelControl32;
        private DevExpress.XtraEditors.LabelControl lb_tientamung1;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl23;
        private DevExpress.XtraEditors.TextEdit lb_tongcong;
        private DevExpress.XtraEditors.PanelControl panelControl30;
        private DevExpress.XtraEditors.LabelControl lb_tongcong1;
        private DevExpress.XtraEditors.DateEdit dtpNgay;
        private DevExpress.XtraEditors.SimpleButton btnTimkiem;
    }
}