﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using KP_RES.Class;
using System.IO;
using KP_Report;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors.Controls;
using System.Globalization;

namespace KP_RES
{
    public partial class Frm_DatPhong_Khachsan : DevExpress.XtraEditors.XtraForm
    {

        public Frm_DatPhong_Khachsan()
        {
            InitializeComponent();
            LoadPermission();
        }

        string KS_GIONHANPHONG = "";
        string KS_GIOTRAPHONG = "";
        private void Frm_DatPhong_Khachsan_Load(object sender, EventArgs e)
        {
            timer1.Start();
            lb_Quay.Text = cls_ConfigCashier.nameCashier;
            lb_Kho.Text = cls_ConfigCashier.nameWarehouse;
            lb_NhanVien.Text = clsUserManagement.ReturnNameOfUserLogin();
            lbCaBan.Text = clsMain.GetServerDate().ToString("dd-MM-yyyy");
            lbtimeby.Text = cls_ConfigCashier.sTenCB + ": " + cls_ConfigCashier.sGioBD + " --> " + cls_ConfigCashier.sGioKT;

            dateXem.EditValue = clsGlobal.gdServerDate;
            txtSongay.EditValue = 7;
            LoadCombo();
            LoaddataGridView();
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("63");
            btnSua.Enabled = clsUserManagement.AllowEdit("63");
            btnXoa.Enabled = clsUserManagement.AllowDelete("63");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        DataTable dtPhong = new DataTable();
        private void LoadCombo()
        {
            string sql = "select ban.ma_ban,ban.ten_ban,ban.ma_loaiphong " + "\n";
            sql += " from ban" + "\n";
            sql += " inner join KHUVUC on ban.MA_KHUVUC = KHUVUC.MA_KHUVUC " + "\n";
            sql += " where ban.sudung=1" + "\n";
            sql += " and KHUVUC .MODE =2" + "\n";
            sql += " order by ban.TEN_BAN" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sql);
            cboPhong.Properties.DataSource = dt.Copy();
            dtPhong = dt.Copy();

            sql = "" + "\n";
            sql = "select MA,	TEN	,GIATRI,	GHICHU " + "\n";
            sql += " from CAUHINH" + "\n";
            sql += " where TEN in ('KS_GIONHANPHONG','KS_GIOTRAPHONG')" + "\n";
            dt = clsMain.ReturnDataTable(sql);
            KS_GIONHANPHONG = dt.Select("TEN='KS_GIONHANPHONG'")[0]["GIATRI"].ToString();
            KS_GIOTRAPHONG = dt.Select("TEN='KS_GIOTRAPHONG'")[0]["GIATRI"].ToString();
        }

        private void LoadTrangThaiPhong()
        {
            try
            {
                DateTime dti = (DateTime)dateXem.EditValue;
                int iSongay = int.Parse(txtSongay.Text);
            }
            catch
            {
                return;
            }

            foreach (GridColumn item in gridView1.Columns)
            {
                item.VisibleIndex = int.Parse(item.Name.Substring(item.Name.Length - 2, 2)) + 1;

                if (int.Parse(item.Name.Substring(item.Name.Length - 2, 2)) <= int.Parse(txtSongay.Text))
                {
                    item.Visible = true;
                    if (int.Parse(item.Name.Substring(item.Name.Length - 2, 2)) == 0)
                    {
                        item.Caption = "Phòng";
                        item.FieldName = "PHONG";
                    }
                    else
                    {
                        DateTime dti = (DateTime)dateXem.EditValue;
                        dti = dti.AddDays(int.Parse(item.Name.Substring(item.Name.Length - 2, 2)) - 1);
                        item.Caption = cls_KP_RES.DateToString_Date(dti);
                        item.FieldName = "C_" + cls_KP_RES.DateToString_Date(dti).Replace("/", "");
                    }
                }
                else
                {
                    item.Visible = false;
                }
            }

            DateTime dtiTungay = (DateTime)dateXem.EditValue;
            DateTime dtiDenngay = dtiTungay.AddDays(int.Parse(txtSongay.Text));

            string sql = "";
            sql += "EXEC SP_GetTrangThaiPhong " + clsMain.SQLString(cls_KP_RES.DateToString_Date(dtiTungay)) + "," + clsMain.SQLString(cls_KP_RES.DateToString_Date(dtiDenngay)) + "\n";
            DataTable dt = clsMain.ReturnDataTable(sql);
            gridControl1.DataSource = dt;

        }

        string sma = "";
        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            string sSQL = "";
            sSQL += "Select MADATPHONG,	MAPHONG,TEN_BAN	,NGAYNHANPHONG as NGAYNHANPHONG1,convert(varchar(25), NGAYNHANPHONG, 103 )  as NGAYNHANPHONG ,	convert(varchar(25), NGAYTRAPHONG, 103 ) as NGAYTRAPHONG  ,	NGUOIDAT,	DIENTHOAI,	EMAIL,CMND,	TONGTIEN,	DATHANHTOAN,	NGUOITAO,	NGAYTAO	,NGUOISUA	,NGAYSUA,HOADON" + "\n";
            sSQL += "From KS_DATPHONG" + "\n";
            sSQL += "inner join ban on KS_DATPHONG.MAPHONG = ban.MA_BAN " + "\n";
            sSQL += "where trangthai=1" + "\n";
            //sSQL += "and hoadon not in (select ma_hoadon from HOADON_KHACHSAN where ispayment=1)" + "\n";
            sSQL += "order by  NGAYNHANPHONG1 desc " + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            gridControl2.DataSource = dt;

        }

        private void SetRongControl()
        {
            sma = "";
            txtTen.Text = "";
            txtDienThoai.Text = "";
            txtCMND.Text = "";
            txtEmail.Text = "";
            txtTongTien.Text = "";
            txtDaThanhToan.Text = "0";
            dtpNgaynhanphong.EditValue = clsGlobal.gdServerDate;
            dtpNgaytraphong.EditValue = clsGlobal.gdServerDate.AddDays(1);
            cboPhong.CheckAll();
            foreach (CheckedListBoxItem item in cboPhong.Properties.Items)
            {
                item.CheckState = CheckState.Unchecked;
            }
            txtTen.Focus();
        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTen.Properties.ReadOnly = true;
                txtDienThoai.Properties.ReadOnly = true;
                txtCMND.Properties.ReadOnly = true;
                txtEmail.Properties.ReadOnly = true;
                txtDaThanhToan.Properties.ReadOnly = true;
                dtpNgaynhanphong.Properties.ReadOnly = true;
                dtpNgaytraphong.Properties.ReadOnly = true;
                cboPhong.Properties.ReadOnly = true;
            }
            else//mở
            {
                txtTen.Properties.ReadOnly = false;
                txtDienThoai.Properties.ReadOnly = false;
                txtCMND.Properties.ReadOnly = false;
                txtEmail.Properties.ReadOnly = false;
                txtDaThanhToan.Properties.ReadOnly = false;
                dtpNgaynhanphong.Properties.ReadOnly = false;
                dtpNgaytraphong.Properties.ReadOnly = false;
                cboPhong.Properties.ReadOnly = false;
            }
        }

        private void gridView1_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            GridView View = sender as GridView;
            if (e.Column.FieldName == "PHONG")
            {
                string category = View.GetRowCellDisplayText(e.RowHandle, e.Column.FieldName);
                if (category != "")
                {
                    e.Appearance.BackColor = Color.MediumSlateBlue;
                    e.Appearance.BackColor2 = Color.LightCyan;
                }
            }
            else
            {
                string category = View.GetRowCellDisplayText(e.RowHandle, e.Column.FieldName);
                if (category != "")
                {
                    if (category.Contains("Checkin"))
                    {
                        e.Appearance.BackColor = Color.DarkOrange;
                        e.Appearance.BackColor2 = Color.LightCyan;
                    }
                    else
                    {
                        e.Appearance.BackColor = Color.DeepSkyBlue;
                        e.Appearance.BackColor2 = Color.LightCyan;
                    }
                }
            }
        }

        private void gridView1_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            GridView View = sender as GridView;
            string category = View.GetRowCellDisplayText(e.RowHandle, e.Column.FieldName);
            if (category.Length > 8)
            {
                string madatve = category.Substring(0, 7);
                Clipboard.SetText(madatve);
            }
        }

        private void gridView2_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        string sNGUOITAO = "";
        string sNGAYTAO = "";
        string sHOADON = "";
        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;
            sma = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MADATPHONG").ToString();
            txtTen.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGUOIDAT").ToString();
            txtDienThoai.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DIENTHOAI").ToString();
            txtEmail.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "EMAIL").ToString();
            txtCMND.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "CMND").ToString();
            txtTongTien.EditValue =int.Parse ( gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TONGTIEN").ToString().Replace (",",""));
            txtDaThanhToan.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DATHANHTOAN").ToString().Replace(",", ""));
            dtpNgaynhanphong.EditValue = DateTime.ParseExact(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAYNHANPHONG").ToString()+" " + KS_GIONHANPHONG, "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.CurrentCulture);
            dtpNgaytraphong.EditValue = DateTime.ParseExact(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAYTRAPHONG").ToString() + " " + KS_GIOTRAPHONG, "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.CurrentCulture);

            sNGUOITAO = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGUOITAO").ToString();
            sNGAYTAO = (DateTime.Parse (gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAYTAO").ToString() )).ToString("dd/MM/yyyy HH:mm:ss");
            sHOADON = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "HOADON").ToString();
            DataTable dt = (DataTable)gridControl2.DataSource;
            foreach (CheckedListBoxItem item in cboPhong.Properties.Items)
            {
                item.CheckState = CheckState.Unchecked;
            }
            foreach (CheckedListBoxItem item in cboPhong.Properties.Items)
            {
                foreach (DataRow dr in dt.Select("MADATPHONG=" +clsMain .SQLString( sma)))
                {
                    if (item.Value.ToString() == dr["MAPHONG"].ToString())
                    {
                        item.CheckState = CheckState.Checked;
                    }
                }
            }
            KhoaMoControl(true);
        }

        private void gridView2_RowStyle(object sender, RowStyleEventArgs e)
        {
            try
            {
                GridView View = sender as GridView;
                if (e.RowHandle >= 0)
                {
                    if (DateTime.ParseExact(View.GetRowCellValue(e.RowHandle, View.Columns["NGAYNHANPHONG"]).ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture) == DateTime.ParseExact(lbCaBan.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture))
                    {
                        e.Appearance.ForeColor = Color.Red;
                    }
                    else if (DateTime.ParseExact(View.GetRowCellValue(e.RowHandle, View.Columns["NGAYNHANPHONG"]).ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(lbCaBan.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture))
                    {
                        e.Appearance.ForeColor = Color.Blue;
                    }
                    else if (DateTime.ParseExact(View.GetRowCellValue(e.RowHandle, View.Columns["NGAYNHANPHONG"]).ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture) < DateTime.ParseExact(lbCaBan.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture))
                    {
                        e.Appearance.ForeColor = Color.DarkGreen;
                    }
                    
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string hour, minute, second;
            second = DateTime.Now.Second.ToString();
            hour = DateTime.Now.Hour.ToString();
            minute = DateTime.Now.Minute.ToString();
            if (second.Length < 2)
                lbgiay.Text = "0" + second;
            else
                lbgiay.Text = second;
            if (minute.Length < 2)
                lbGio.Text = hour + ":0" + minute;
            else
                lbGio.Text = hour + ":" + minute;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnToi_Click(object sender, EventArgs e)
        {
            DateTime dti = (DateTime)dateXem.EditValue;
            dateXem.EditValue = dti.AddDays((int.Parse(txtSongay.Text)));
        }

        private void btnLui_Click(object sender, EventArgs e)
        {
            DateTime dti = (DateTime)dateXem.EditValue;
            dateXem.EditValue = dti.AddDays(-(int.Parse(txtSongay.Text)));
        }

        private void dateXem_EditValueChanged(object sender, EventArgs e)
        {
            DateTime dti = (DateTime)dateXem.EditValue;
            if ((dti - DateTime.Now).Days < 0)
            {
                dateXem.EditValue = clsGlobal.gdServerDate;
            }

            LoadTrangThaiPhong();
        }

        private void txtSongay_EditValueChanged(object sender, EventArgs e)
        {
            int iSongay = int.Parse(txtSongay.Text);
            if (iSongay > 30 || iSongay < 2)
            {
                txtSongay.EditValue = 7;
            }
            LoadTrangThaiPhong();
        }

        private void txtTongTien_Click(object sender, EventArgs e)
        {
            float iTongtien = 0;
            txtTongTien.Text = string.Format("{0:#,###0}", iTongtien);
            foreach (CheckedListBoxItem item in cboPhong.Properties.Items)
            {
                if (item.CheckState == CheckState.Checked)
                {
                    string sSQL = string.Format("Exec TinhTienphong_Khachsan3 @NGAYDEN='{0}',@NGAYDI='{1}',@LOAIPHONG='{2}',@SONGUOI='{3}'", dtpNgaynhanphong.Text + " " + KS_GIONHANPHONG, dtpNgaytraphong.Text + " " + KS_GIOTRAPHONG, dtPhong.Select("Ma_ban=" + item.Value)[0]["MA_LOAIPHONG"].ToString(), "1");
                    DataTable dt = clsMain.ReturnDataTable(sSQL);
                    iTongtien = iTongtien + float.Parse(dt.Rows[0]["GIA"].ToString());
                }
            }
            txtTongTien.Text = string.Format("{0:#,###0}", iTongtien);

        }

        bool bDohoa = true;
        private void btnTatcadatphong_Click(object sender, EventArgs e)
        {
            if (bDohoa)
            {
                btnTatcadatphong.Text = "Giao diện đồ họa";
                gridControl1.Visible = false;
                xtraScrollableControl1.Visible = false;
                gridControl2.Visible = true;
            }
            else
            {
                btnTatcadatphong.Text = "Tất cả đặt phòng";
                gridControl1.Visible = true;
                xtraScrollableControl1.Visible = true;
                gridControl2.Visible = false;
            }
            bDohoa = !bDohoa;
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if (!bDohoa)
            {
                return;
            }
            SetRongControl();
            KhoaMoControl(false);
            txtTen.Focus();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            if (bDohoa)
            {
                return;
            }
            KhoaMoControl(false);
            txtTen.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (bDohoa)
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Update  KS_DATPHONG Set TRANGTHAI=0" + "\n";
            sSQL += "Where MADATPHONG=" + clsMain.SQLString(sma) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoadTrangThaiPhong();
                LoaddataGridView();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;
            String MaBarCode = "";
            String sSQL = "";
            if (sma == "" && btnThem.Enabled)
            {
                MaBarCode = CreateBarcode();
                foreach (CheckedListBoxItem item in cboPhong.Properties.Items)
                {
                    if (item.CheckState == CheckState.Checked)
                    {
                        sSQL += "INSERT INTO KS_DATPHONG(MADATPHONG, MAPHONG, NGAYNHANPHONG, NGAYTRAPHONG, NGUOIDAT, DIENTHOAI, EMAIL,CMND, TONGTIEN, DATHANHTOAN,TRANGTHAI, NGUOITAO, NGAYTAO)" + "\n";
                        sSQL += "VALUES( ";
                        sSQL += clsMain.SQLString(MaBarCode) + ",";
                        sSQL += clsMain.SQLString(item.Value.ToString()) + ",";
                        sSQL += clsMain.SQLString(dtpNgaynhanphong.Text) + ",";
                        sSQL += clsMain.SQLString(dtpNgaytraphong.Text) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtTen.Text) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtDienThoai.Text) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtEmail.Text) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtCMND.Text) + ",";
                        sSQL += clsMain.SQLString(txtTongTien.Text.Replace(",", "")) + ",";
                        sSQL += clsMain.SQLString(txtDaThanhToan.Text.Replace (",", "")) + ",";
                        sSQL += clsMain.SQLString("1") + ",";
                        sSQL += clsMain.SQLString(clsGlobal.gsUserID) + ",";
                        sSQL += "GETDATE())" + "\n";
                    }
                }
            }
            else if ((sma != "" && btnSua.Enabled))
            {
                MaBarCode = sma;
                sSQL += "Delete From KS_DATPHONG" + "\n";
                sSQL += "Where MADATPHONG=" + clsMain.SQLString(sma) + "\n";
                foreach (CheckedListBoxItem item in cboPhong.Properties.Items)
                {
                    if (item.CheckState == CheckState.Checked)
                    {
                        sSQL += "INSERT INTO KS_DATPHONG(MADATPHONG, MAPHONG, NGAYNHANPHONG, NGAYTRAPHONG, NGUOIDAT, DIENTHOAI, EMAIL,CMND, TONGTIEN, DATHANHTOAN,TRANGTHAI,HOADON, NGUOISUA,NGAYSUA,NGUOITAO, NGAYTAO)" + "\n";
                        sSQL += "VALUES( ";
                        sSQL += clsMain.SQLString(MaBarCode) + ",";
                        sSQL += clsMain.SQLString(item.Value.ToString()) + ",";
                        sSQL += clsMain.SQLString(dtpNgaynhanphong.Text) + ",";
                        sSQL += clsMain.SQLString(dtpNgaytraphong.Text) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtTen.Text) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtDienThoai.Text) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtEmail.Text) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtCMND.Text) + ",";
                        sSQL += clsMain.SQLString(txtTongTien.Text.Replace(",", "")) + ",";
                        sSQL += clsMain.SQLString(txtDaThanhToan.Text.Replace(",", "")) + ",";
                        sSQL += clsMain.SQLString("1") + ",";
                        sSQL += clsMain.SQLString(sHOADON) + ",";
                        sSQL += clsMain.SQLString(clsGlobal.gsUserID) + ",";
                        sSQL += "GETDATE()" + ",";
                        sSQL += clsMain.SQLString(sNGUOITAO) + ",";
                        sSQL += clsMain.SQLString(sNGAYTAO) + ")" + "\n";
                    }
                }
            }

            bool bRunSQL = clsMain.ExecuteSQL(sSQL);
            if (bRunSQL)
            {
                //Auto lưu thêm tên người đặt vào danh mục thông tin khách
                if (txtCMND.Text.Trim() != "")
                {
                    DataTable dt = clsMain.ReturnDataTable("select MA_KH,CMND,TEN from KS_KHACHHANG where CMND=" + clsMain.SQLString(txtCMND .Text .Trim ()));
                    if (dt.Rows.Count == 0)
                    {
                        sSQL = "";
                        sSQL += "Insert Into KS_KHACHHANG(TEN,NGAYSINH,GIOITINH,DIACHI,DIENTHOAI,EMAIL,CMND,QUOCTICH,NGAYTAO,NGUOITAO)" + "\n";
                        sSQL += "Values(";
                        sSQL += clsMain.SQLStringUnicode(txtTen.Text ) + ",";
                        sSQL += clsMain.SQLString("1990") + ",";
                        sSQL += clsMain.SQLString("1") + ",";
                        sSQL += clsMain.SQLStringUnicode("") + ",";
                        sSQL += clsMain.SQLString(txtDienThoai.Text ) + ",";
                        sSQL += clsMain.SQLString(txtEmail.Text ) + ",";
                        sSQL += clsMain.SQLString(txtCMND.Text ) + ",";
                        sSQL += clsMain.SQLStringUnicode("Viet Nam") + ",";
                        sSQL += "GETDATE()" + ",";
                        sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ")";
                        sSQL += "\n";

                        clsMain.ExecuteSQL(sSQL);
                    }
                }
                XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                SetRongControl();
                LoadTrangThaiPhong();
                LoaddataGridView();
            }
            else
            {
                XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private Boolean CheckInput()
        {
            if (txtTen.Text == "")
            {
                XtraMessageBox.Show("Nhập tên khách đặt phòng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTen.Focus();
                return false;
            }
            if (txtDienThoai.Text == "")
            {
                XtraMessageBox.Show("Nhập điện thoại khách đặt phòng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDienThoai.Focus();
                return false;
            }
            if ((DateTime)dtpNgaynhanphong.EditValue < clsGlobal.gdServerDate || (DateTime)dtpNgaytraphong.EditValue < clsGlobal.gdServerDate.AddDays(1) || (DateTime)dtpNgaytraphong.EditValue <= (DateTime)dtpNgaynhanphong.EditValue)
            {
                XtraMessageBox.Show("Ngày lưu trú không đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dtpNgaynhanphong.Focus();
                return false;
            }
            if (cboPhong.Text == "")
            {
                XtraMessageBox.Show("Chọn phòng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboPhong.Focus();
                return false;
            }
            txtTongTien_Click(null, null);
            if (txtTongTien.Text == "")
            {
                XtraMessageBox.Show("Chưa có tiền phòng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTongTien.Focus();
                return false;
            }
            if (txtTongTien.Text != "" && txtDaThanhToan .Text !="")
            {
                if (long.Parse(txtTongTien.Text.Replace(",", "")) < long.Parse(txtDaThanhToan.Text.Replace(",", "")))
                {
                    XtraMessageBox.Show("Tiền đặt cọc không đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDaThanhToan.Focus();
                    return false;
                }
            }
            foreach (CheckedListBoxItem item in cboPhong.Properties.Items)
            {
                if (item.CheckState == CheckState.Checked)
                {
                    DateTime dtiTu =(DateTime ) dtpNgaynhanphong.EditValue;
                    DateTime dtiDen =(DateTime ) dtpNgaytraphong.EditValue;
                    do
                    {
                        string sql = "";
                        if (sma != "")
                        {
                            sql = "select * from KS_DATPHONG where trangthai=1 and  maphong=" + clsMain.SQLString(item.Value.ToString()) + " and NGAYNHANPHONG<=" + clsMain.SQLString(cls_KP_RES.DateToString_Date(dtiTu)) + " and NGAYTRAPHONG>" + clsMain.SQLString(cls_KP_RES.DateToString_Date(dtiTu)) + " and MADATPHONG <>" + clsMain.SQLString(sma ); 
                        }
                        else
                        {
                            sql = "select * from KS_DATPHONG where trangthai=1 and  maphong=" + clsMain.SQLString(item.Value.ToString()) + " and NGAYNHANPHONG<=" + clsMain.SQLString(cls_KP_RES.DateToString_Date(dtiTu)) + " and NGAYTRAPHONG>" + clsMain.SQLString(cls_KP_RES.DateToString_Date(dtiTu));
                        }
                        
                        if (clsMain.ReturnDataTable(sql).Rows.Count > 0)
                        {
                            XtraMessageBox.Show("Ngày lưu trú không đúng. Đã có người đặt trước", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            dtpNgaynhanphong.Focus();
                            return false;
                        }
                       dtiTu= dtiTu.AddDays(1);
                    }
                    while (dtiTu < dtiDen);
                }
            }
            return true;
        }

        private String CreateBarcode()
        {
            try
            {
                String MaBarCode = "";
                int newbarcode = 0;
                do
                {
                    newbarcode = 0;

                    Random rd = new Random(Convert.ToInt32(DateTime.Now.Ticks % 0x7FFFFFFF));
                    MaBarCode = string.Format("{0:0000000}", rd.Next(0, 9999999));
                    String sSQL1 = "";
                    sSQL1 += "SELECT MADATPHONG" + "\n";
                    sSQL1 += "FROM KS_DATPHONG" + "\n";
                    sSQL1 += "WHERE MADATPHONG = " + clsMain.SQLString(MaBarCode) + "\n";

                    if (clsMain.ReturnDataTable(sSQL1).Rows.Count > 0)
                        newbarcode = 1;
                } 
                while (newbarcode == 1);

                return MaBarCode;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        

      

    }
}