﻿namespace KP_RES
{
    partial class Frm_Danhsachkhach
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Danhsachkhach));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pnLogo = new DevExpress.XtraEditors.PanelControl();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl25 = new DevExpress.XtraEditors.PanelControl();
            this.lb_Kho = new DevExpress.XtraEditors.LabelControl();
            this.lb_Quay = new DevExpress.XtraEditors.LabelControl();
            this.lbgiay = new DevExpress.XtraEditors.LabelControl();
            this.lbCaBan = new DevExpress.XtraEditors.LabelControl();
            this.lbGio = new DevExpress.XtraEditors.LabelControl();
            this.lb_NhanVien = new DevExpress.XtraEditors.LabelControl();
            this.lbtimeby = new DevExpress.XtraEditors.LabelControl();
            this.panelControl56 = new DevExpress.XtraEditors.PanelControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelControl57 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl58 = new DevExpress.XtraEditors.PanelControl();
            this.pnDSKhach = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENKH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CMND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYSINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIOITINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.QUOCTICH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIENTHOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIACHI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EMAIL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_KH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DATHANHTOAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HOADON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CHECKIN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CHECKOUT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_BAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl50 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl68 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl69 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuatExcel = new DevExpress.XtraEditors.SimpleButton();
            this.btbIn = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pnLogo)).BeginInit();
            this.pnLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl25)).BeginInit();
            this.panelControl25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl56)).BeginInit();
            this.panelControl56.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl57)).BeginInit();
            this.panelControl57.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnDSKhach)).BeginInit();
            this.pnDSKhach.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl50)).BeginInit();
            this.panelControl50.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl68)).BeginInit();
            this.panelControl68.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl69)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pnLogo
            // 
            this.pnLogo.Controls.Add(this.panelControl12);
            this.pnLogo.Controls.Add(this.panelControl57);
            this.pnLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnLogo.Location = new System.Drawing.Point(0, 0);
            this.pnLogo.Name = "pnLogo";
            this.pnLogo.Size = new System.Drawing.Size(1309, 70);
            this.pnLogo.TabIndex = 53;
            // 
            // panelControl12
            // 
            this.panelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl12.Controls.Add(this.panelControl6);
            this.panelControl12.Controls.Add(this.panelControl56);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl12.Location = new System.Drawing.Point(2, 5);
            this.panelControl12.Margin = new System.Windows.Forms.Padding(6);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(1305, 63);
            this.panelControl12.TabIndex = 3;
            // 
            // panelControl6
            // 
            this.panelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.panelControl6.Controls.Add(this.panelControl25);
            this.panelControl6.Controls.Add(this.lbgiay);
            this.panelControl6.Controls.Add(this.lbCaBan);
            this.panelControl6.Controls.Add(this.lbGio);
            this.panelControl6.Controls.Add(this.lb_NhanVien);
            this.panelControl6.Controls.Add(this.lbtimeby);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(196, 0);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(1109, 63);
            this.panelControl6.TabIndex = 23;
            // 
            // panelControl25
            // 
            this.panelControl25.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl25.Controls.Add(this.lb_Kho);
            this.panelControl25.Controls.Add(this.lb_Quay);
            this.panelControl25.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl25.Location = new System.Drawing.Point(2, 2);
            this.panelControl25.Name = "panelControl25";
            this.panelControl25.Size = new System.Drawing.Size(349, 59);
            this.panelControl25.TabIndex = 3;
            // 
            // lb_Kho
            // 
            this.lb_Kho.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lb_Kho.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_Kho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_Kho.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_Kho.Location = new System.Drawing.Point(13, 34);
            this.lb_Kho.Name = "lb_Kho";
            this.lb_Kho.Size = new System.Drawing.Size(41, 19);
            this.lb_Kho.TabIndex = 15;
            this.lb_Kho.Text = "Quầy";
            // 
            // lb_Quay
            // 
            this.lb_Quay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lb_Quay.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_Quay.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.lb_Quay.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_Quay.Location = new System.Drawing.Point(13, 5);
            this.lb_Quay.Name = "lb_Quay";
            this.lb_Quay.Size = new System.Drawing.Size(52, 24);
            this.lb_Quay.TabIndex = 14;
            this.lb_Quay.Text = "Quầy";
            // 
            // lbgiay
            // 
            this.lbgiay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbgiay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbgiay.Location = new System.Drawing.Point(1068, 27);
            this.lbgiay.Name = "lbgiay";
            this.lbgiay.Size = new System.Drawing.Size(18, 19);
            this.lbgiay.TabIndex = 19;
            this.lbgiay.Text = "00";
            // 
            // lbCaBan
            // 
            this.lbCaBan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCaBan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbCaBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbCaBan.Location = new System.Drawing.Point(1001, 5);
            this.lbCaBan.Name = "lbCaBan";
            this.lbCaBan.Size = new System.Drawing.Size(94, 19);
            this.lbCaBan.TabIndex = 17;
            this.lbCaBan.Text = "21-12-2012";
            // 
            // lbGio
            // 
            this.lbGio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGio.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGio.Location = new System.Drawing.Point(1016, 34);
            this.lbGio.Name = "lbGio";
            this.lbGio.Size = new System.Drawing.Size(46, 19);
            this.lbGio.TabIndex = 20;
            this.lbGio.Text = "00:00";
            // 
            // lb_NhanVien
            // 
            this.lb_NhanVien.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lb_NhanVien.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_NhanVien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_NhanVien.Location = new System.Drawing.Point(499, 5);
            this.lb_NhanVien.Name = "lb_NhanVien";
            this.lb_NhanVien.Size = new System.Drawing.Size(89, 19);
            this.lb_NhanVien.TabIndex = 18;
            this.lb_NhanVien.Text = "Nhân Viên:";
            // 
            // lbtimeby
            // 
            this.lbtimeby.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbtimeby.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbtimeby.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbtimeby.Location = new System.Drawing.Point(499, 38);
            this.lbtimeby.Name = "lbtimeby";
            this.lbtimeby.Size = new System.Drawing.Size(21, 19);
            this.lbtimeby.TabIndex = 16;
            this.lbtimeby.Text = "Ca";
            // 
            // panelControl56
            // 
            this.panelControl56.Controls.Add(this.panel2);
            this.panelControl56.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl56.Location = new System.Drawing.Point(0, 0);
            this.panelControl56.Name = "panelControl56";
            this.panelControl56.Size = new System.Drawing.Size(196, 63);
            this.panelControl56.TabIndex = 21;
            // 
            // panel2
            // 
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(192, 59);
            this.panel2.TabIndex = 22;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(6, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(177, 41);
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // panelControl57
            // 
            this.panelControl57.Appearance.BackColor = System.Drawing.Color.DarkGray;
            this.panelControl57.Appearance.ForeColor = System.Drawing.Color.Black;
            this.panelControl57.Appearance.Options.UseBackColor = true;
            this.panelControl57.Appearance.Options.UseForeColor = true;
            this.panelControl57.Controls.Add(this.panelControl58);
            this.panelControl57.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl57.Location = new System.Drawing.Point(2, 2);
            this.panelControl57.Name = "panelControl57";
            this.panelControl57.Size = new System.Drawing.Size(1305, 3);
            this.panelControl57.TabIndex = 3;
            // 
            // panelControl58
            // 
            this.panelControl58.Appearance.BackColor = System.Drawing.Color.DarkGray;
            this.panelControl58.Appearance.ForeColor = System.Drawing.Color.Black;
            this.panelControl58.Appearance.Options.UseBackColor = true;
            this.panelControl58.Appearance.Options.UseForeColor = true;
            this.panelControl58.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl58.Location = new System.Drawing.Point(2, 2);
            this.panelControl58.Name = "panelControl58";
            this.panelControl58.Size = new System.Drawing.Size(1301, 0);
            this.panelControl58.TabIndex = 4;
            // 
            // pnDSKhach
            // 
            this.pnDSKhach.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.pnDSKhach.Controls.Add(this.gridControl1);
            this.pnDSKhach.Controls.Add(this.gridControl2);
            this.pnDSKhach.Controls.Add(this.panelControl50);
            this.pnDSKhach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnDSKhach.Location = new System.Drawing.Point(0, 70);
            this.pnDSKhach.Name = "pnDSKhach";
            this.pnDSKhach.Size = new System.Drawing.Size(1309, 698);
            this.pnDSKhach.TabIndex = 54;
            // 
            // gridControl1
            // 
            this.gridControl1.AllowDrop = true;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1266, 434);
            this.gridControl1.TabIndex = 2;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 50;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.TENKH,
            this.CMND,
            this.NGAYSINH,
            this.GIOITINH,
            this.QUOCTICH,
            this.DIENTHOAI,
            this.DIACHI,
            this.EMAIL,
            this.MA_KH,
            this.TRANGTHAI,
            this.DATHANHTOAN});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView1_RowStyle);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // TENKH
            // 
            this.TENKH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENKH.AppearanceHeader.Options.UseFont = true;
            this.TENKH.AppearanceHeader.Options.UseTextOptions = true;
            this.TENKH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENKH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENKH.Caption = "Tên";
            this.TENKH.FieldName = "TEN";
            this.TENKH.Name = "TENKH";
            this.TENKH.OptionsColumn.AllowEdit = false;
            this.TENKH.OptionsColumn.AllowFocus = false;
            this.TENKH.OptionsColumn.AllowMove = false;
            this.TENKH.OptionsColumn.FixedWidth = true;
            this.TENKH.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.TENKH.Visible = true;
            this.TENKH.VisibleIndex = 3;
            this.TENKH.Width = 170;
            // 
            // CMND
            // 
            this.CMND.AppearanceCell.Options.UseTextOptions = true;
            this.CMND.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CMND.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CMND.AppearanceHeader.Options.UseFont = true;
            this.CMND.AppearanceHeader.Options.UseTextOptions = true;
            this.CMND.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CMND.Caption = "CMND ";
            this.CMND.FieldName = "CMND";
            this.CMND.Name = "CMND";
            this.CMND.OptionsColumn.AllowEdit = false;
            this.CMND.OptionsColumn.AllowFocus = false;
            this.CMND.OptionsColumn.FixedWidth = true;
            this.CMND.Visible = true;
            this.CMND.VisibleIndex = 2;
            this.CMND.Width = 114;
            // 
            // NGAYSINH
            // 
            this.NGAYSINH.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYSINH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYSINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYSINH.AppearanceHeader.Options.UseFont = true;
            this.NGAYSINH.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYSINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYSINH.Caption = "Ngày sinh";
            this.NGAYSINH.FieldName = "NGAYSINH";
            this.NGAYSINH.Name = "NGAYSINH";
            this.NGAYSINH.OptionsColumn.AllowEdit = false;
            this.NGAYSINH.OptionsColumn.AllowFocus = false;
            this.NGAYSINH.OptionsColumn.FixedWidth = true;
            this.NGAYSINH.Visible = true;
            this.NGAYSINH.VisibleIndex = 5;
            this.NGAYSINH.Width = 100;
            // 
            // GIOITINH
            // 
            this.GIOITINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIOITINH.AppearanceHeader.Options.UseFont = true;
            this.GIOITINH.AppearanceHeader.Options.UseTextOptions = true;
            this.GIOITINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIOITINH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIOITINH.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GIOITINH.Caption = "Giới tính";
            this.GIOITINH.FieldName = "GIOITINH";
            this.GIOITINH.Name = "GIOITINH";
            this.GIOITINH.OptionsColumn.AllowEdit = false;
            this.GIOITINH.OptionsColumn.AllowFocus = false;
            this.GIOITINH.OptionsColumn.AllowMove = false;
            this.GIOITINH.OptionsColumn.FixedWidth = true;
            this.GIOITINH.Visible = true;
            this.GIOITINH.VisibleIndex = 6;
            this.GIOITINH.Width = 60;
            // 
            // QUOCTICH
            // 
            this.QUOCTICH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.QUOCTICH.AppearanceHeader.Options.UseFont = true;
            this.QUOCTICH.AppearanceHeader.Options.UseTextOptions = true;
            this.QUOCTICH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.QUOCTICH.Caption = "Quốc tịch ";
            this.QUOCTICH.FieldName = "QUOCTICH";
            this.QUOCTICH.Name = "QUOCTICH";
            this.QUOCTICH.OptionsColumn.AllowEdit = false;
            this.QUOCTICH.OptionsColumn.AllowFocus = false;
            this.QUOCTICH.OptionsColumn.AllowMove = false;
            this.QUOCTICH.OptionsColumn.FixedWidth = true;
            this.QUOCTICH.Visible = true;
            this.QUOCTICH.VisibleIndex = 4;
            this.QUOCTICH.Width = 90;
            // 
            // DIENTHOAI
            // 
            this.DIENTHOAI.AppearanceCell.Options.UseTextOptions = true;
            this.DIENTHOAI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.DIENTHOAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DIENTHOAI.AppearanceHeader.Options.UseFont = true;
            this.DIENTHOAI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIENTHOAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIENTHOAI.Caption = "Điện thoại";
            this.DIENTHOAI.FieldName = "DIENTHOAI";
            this.DIENTHOAI.Name = "DIENTHOAI";
            this.DIENTHOAI.OptionsColumn.AllowEdit = false;
            this.DIENTHOAI.OptionsColumn.AllowFocus = false;
            this.DIENTHOAI.OptionsColumn.FixedWidth = true;
            this.DIENTHOAI.Visible = true;
            this.DIENTHOAI.VisibleIndex = 7;
            this.DIENTHOAI.Width = 100;
            // 
            // DIACHI
            // 
            this.DIACHI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DIACHI.AppearanceHeader.Options.UseFont = true;
            this.DIACHI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIACHI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIACHI.Caption = "Địa chỉ";
            this.DIACHI.FieldName = "DIACHI";
            this.DIACHI.MinWidth = 120;
            this.DIACHI.Name = "DIACHI";
            this.DIACHI.OptionsColumn.AllowEdit = false;
            this.DIACHI.OptionsColumn.AllowFocus = false;
            this.DIACHI.Visible = true;
            this.DIACHI.VisibleIndex = 8;
            this.DIACHI.Width = 177;
            // 
            // EMAIL
            // 
            this.EMAIL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.EMAIL.AppearanceHeader.Options.UseFont = true;
            this.EMAIL.AppearanceHeader.Options.UseTextOptions = true;
            this.EMAIL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.EMAIL.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.EMAIL.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.EMAIL.Caption = "Email";
            this.EMAIL.FieldName = "EMAIL";
            this.EMAIL.MinWidth = 120;
            this.EMAIL.Name = "EMAIL";
            this.EMAIL.OptionsColumn.AllowEdit = false;
            this.EMAIL.OptionsColumn.AllowFocus = false;
            this.EMAIL.OptionsColumn.AllowMove = false;
            this.EMAIL.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.EMAIL.Visible = true;
            this.EMAIL.VisibleIndex = 9;
            this.EMAIL.Width = 178;
            // 
            // MA_KH
            // 
            this.MA_KH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MA_KH.AppearanceHeader.Options.UseFont = true;
            this.MA_KH.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_KH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_KH.Caption = "MA_KH";
            this.MA_KH.FieldName = "MA_KH";
            this.MA_KH.Name = "MA_KH";
            this.MA_KH.OptionsColumn.AllowEdit = false;
            this.MA_KH.OptionsColumn.AllowFocus = false;
            this.MA_KH.OptionsColumn.FixedWidth = true;
            this.MA_KH.Width = 102;
            // 
            // TRANGTHAI
            // 
            this.TRANGTHAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TRANGTHAI.AppearanceHeader.Options.UseFont = true;
            this.TRANGTHAI.AppearanceHeader.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TRANGTHAI.Caption = "Trạng thái";
            this.TRANGTHAI.FieldName = "TRANGTHAI";
            this.TRANGTHAI.Name = "TRANGTHAI";
            this.TRANGTHAI.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI.OptionsColumn.AllowFocus = false;
            this.TRANGTHAI.OptionsColumn.FixedWidth = true;
            this.TRANGTHAI.Visible = true;
            this.TRANGTHAI.VisibleIndex = 1;
            this.TRANGTHAI.Width = 150;
            // 
            // DATHANHTOAN
            // 
            this.DATHANHTOAN.AppearanceCell.Options.UseTextOptions = true;
            this.DATHANHTOAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.DATHANHTOAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DATHANHTOAN.AppearanceHeader.Options.UseFont = true;
            this.DATHANHTOAN.AppearanceHeader.Options.UseTextOptions = true;
            this.DATHANHTOAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DATHANHTOAN.Caption = "Đặt cọc";
            this.DATHANHTOAN.DisplayFormat.FormatString = "N0";
            this.DATHANHTOAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.DATHANHTOAN.FieldName = "DATHANHTOAN";
            this.DATHANHTOAN.Name = "DATHANHTOAN";
            this.DATHANHTOAN.OptionsColumn.AllowEdit = false;
            this.DATHANHTOAN.OptionsColumn.AllowFocus = false;
            this.DATHANHTOAN.OptionsColumn.FixedWidth = true;
            this.DATHANHTOAN.Visible = true;
            this.DATHANHTOAN.VisibleIndex = 10;
            this.DATHANHTOAN.Width = 120;
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            gridLevelNode2.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gridControl2.Location = new System.Drawing.Point(2, 436);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1266, 260);
            this.gridControl2.TabIndex = 5;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 50;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT1,
            this.MA_HOADON,
            this.CHECKIN,
            this.CHECKOUT,
            this.TEN_BAN,
            this.gridColumn3,
            this.gridColumn2,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.FILL});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // STT1
            // 
            this.STT1.AppearanceCell.Options.UseTextOptions = true;
            this.STT1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT1.AppearanceHeader.Options.UseFont = true;
            this.STT1.AppearanceHeader.Options.UseTextOptions = true;
            this.STT1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT1.Caption = "STT";
            this.STT1.Name = "STT1";
            this.STT1.OptionsColumn.AllowEdit = false;
            this.STT1.OptionsColumn.AllowFocus = false;
            this.STT1.OptionsColumn.AllowMove = false;
            this.STT1.OptionsColumn.FixedWidth = true;
            this.STT1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT1.Visible = true;
            this.STT1.VisibleIndex = 0;
            this.STT1.Width = 50;
            // 
            // MA_HOADON
            // 
            this.MA_HOADON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA_HOADON.AppearanceHeader.Options.UseFont = true;
            this.MA_HOADON.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.Caption = "Hóa đơn";
            this.MA_HOADON.FieldName = "MA_HOADON";
            this.MA_HOADON.Name = "MA_HOADON";
            this.MA_HOADON.OptionsColumn.AllowEdit = false;
            this.MA_HOADON.OptionsColumn.AllowFocus = false;
            this.MA_HOADON.OptionsColumn.FixedWidth = true;
            this.MA_HOADON.Visible = true;
            this.MA_HOADON.VisibleIndex = 1;
            this.MA_HOADON.Width = 150;
            // 
            // CHECKIN
            // 
            this.CHECKIN.AppearanceCell.Options.UseTextOptions = true;
            this.CHECKIN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHECKIN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CHECKIN.AppearanceHeader.Options.UseFont = true;
            this.CHECKIN.AppearanceHeader.Options.UseTextOptions = true;
            this.CHECKIN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHECKIN.Caption = "Nhận phòng";
            this.CHECKIN.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.CHECKIN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.CHECKIN.FieldName = "CHECKIN";
            this.CHECKIN.Name = "CHECKIN";
            this.CHECKIN.OptionsColumn.AllowEdit = false;
            this.CHECKIN.OptionsColumn.AllowFocus = false;
            this.CHECKIN.OptionsColumn.AllowMove = false;
            this.CHECKIN.OptionsColumn.FixedWidth = true;
            this.CHECKIN.Visible = true;
            this.CHECKIN.VisibleIndex = 3;
            this.CHECKIN.Width = 150;
            // 
            // CHECKOUT
            // 
            this.CHECKOUT.AppearanceCell.Options.UseTextOptions = true;
            this.CHECKOUT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHECKOUT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CHECKOUT.AppearanceHeader.Options.UseFont = true;
            this.CHECKOUT.AppearanceHeader.Options.UseTextOptions = true;
            this.CHECKOUT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHECKOUT.Caption = "Trả phòng";
            this.CHECKOUT.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.CHECKOUT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.CHECKOUT.FieldName = "CHECKOUT";
            this.CHECKOUT.Name = "CHECKOUT";
            this.CHECKOUT.OptionsColumn.AllowEdit = false;
            this.CHECKOUT.OptionsColumn.AllowFocus = false;
            this.CHECKOUT.OptionsColumn.FixedWidth = true;
            this.CHECKOUT.Visible = true;
            this.CHECKOUT.VisibleIndex = 4;
            this.CHECKOUT.Width = 150;
            // 
            // TEN_BAN
            // 
            this.TEN_BAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_BAN.AppearanceHeader.Options.UseFont = true;
            this.TEN_BAN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_BAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_BAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_BAN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TEN_BAN.Caption = "Phòng";
            this.TEN_BAN.FieldName = "TEN_BAN";
            this.TEN_BAN.Name = "TEN_BAN";
            this.TEN_BAN.OptionsColumn.AllowEdit = false;
            this.TEN_BAN.OptionsColumn.AllowFocus = false;
            this.TEN_BAN.OptionsColumn.AllowMove = false;
            this.TEN_BAN.OptionsColumn.FixedWidth = true;
            this.TEN_BAN.Visible = true;
            this.TEN_BAN.VisibleIndex = 2;
            this.TEN_BAN.Width = 100;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "CMND ";
            this.gridColumn3.FieldName = "CMND";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.FixedWidth = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 5;
            this.gridColumn3.Width = 120;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn2.Caption = "Tên";
            this.gridColumn2.FieldName = "TEN";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.AllowMove = false;
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 6;
            this.gridColumn2.Width = 200;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "Điện thoại";
            this.gridColumn7.FieldName = "DIENTHOAI";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.FixedWidth = true;
            this.gridColumn7.Width = 100;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn8.AppearanceHeader.Options.UseFont = true;
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "Địa chỉ";
            this.gridColumn8.FieldName = "DIACHI";
            this.gridColumn8.MinWidth = 120;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.Width = 120;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn9.AppearanceHeader.Options.UseFont = true;
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn9.Caption = "Email";
            this.gridColumn9.FieldName = "EMAIL";
            this.gridColumn9.MinWidth = 120;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.AllowMove = false;
            this.gridColumn9.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn9.Width = 120;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn10.AppearanceHeader.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "MA_KH";
            this.gridColumn10.FieldName = "MA_KH";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.FixedWidth = true;
            this.gridColumn10.Width = 102;
            // 
            // FILL
            // 
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 7;
            this.FILL.Width = 59;
            // 
            // panelControl50
            // 
            this.panelControl50.Controls.Add(this.panelControl68);
            this.panelControl50.Controls.Add(this.btnXuatExcel);
            this.panelControl50.Controls.Add(this.btbIn);
            this.panelControl50.Controls.Add(this.btnDong);
            this.panelControl50.Controls.Add(this.btnBanphim);
            this.panelControl50.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl50.Location = new System.Drawing.Point(1268, 2);
            this.panelControl50.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl50.Name = "panelControl50";
            this.panelControl50.Size = new System.Drawing.Size(39, 694);
            this.panelControl50.TabIndex = 4;
            // 
            // panelControl68
            // 
            this.panelControl68.AutoSize = true;
            this.panelControl68.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl68.Controls.Add(this.panelControl69);
            this.panelControl68.Controls.Add(this.btnXuongit);
            this.panelControl68.Controls.Add(this.btnXuongnhieu);
            this.panelControl68.Controls.Add(this.btnLenit);
            this.panelControl68.Controls.Add(this.btnLennhieu);
            this.panelControl68.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl68.Location = new System.Drawing.Point(2, 242);
            this.panelControl68.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl68.Name = "panelControl68";
            this.panelControl68.Size = new System.Drawing.Size(35, 370);
            this.panelControl68.TabIndex = 14;
            // 
            // panelControl69
            // 
            this.panelControl69.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl69.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl69.Location = new System.Drawing.Point(0, 160);
            this.panelControl69.Name = "panelControl69";
            this.panelControl69.Size = new System.Drawing.Size(35, 50);
            this.panelControl69.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 210);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 290);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnXuatExcel
            // 
            this.btnXuatExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatExcel.Appearance.Options.UseFont = true;
            this.btnXuatExcel.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXuatExcel.Image = global::KP_RES.Properties.Resources.export_26;
            this.btnXuatExcel.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuatExcel.Location = new System.Drawing.Point(2, 162);
            this.btnXuatExcel.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuatExcel.Name = "btnXuatExcel";
            this.btnXuatExcel.Size = new System.Drawing.Size(35, 80);
            this.btnXuatExcel.TabIndex = 15;
            this.btnXuatExcel.Click += new System.EventHandler(this.btnXuatExcel_Click);
            // 
            // btbIn
            // 
            this.btbIn.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btbIn.Appearance.Options.UseFont = true;
            this.btbIn.Dock = System.Windows.Forms.DockStyle.Top;
            this.btbIn.Image = global::KP_RES.Properties.Resources.printer_26;
            this.btbIn.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btbIn.Location = new System.Drawing.Point(2, 82);
            this.btbIn.Margin = new System.Windows.Forms.Padding(0);
            this.btbIn.Name = "btbIn";
            this.btbIn.Size = new System.Drawing.Size(35, 80);
            this.btbIn.TabIndex = 18;
            this.btbIn.Click += new System.EventHandler(this.btbIn_Click);
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDong.Location = new System.Drawing.Point(2, 2);
            this.btnDong.Margin = new System.Windows.Forms.Padding(0);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(35, 80);
            this.btnDong.TabIndex = 16;
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 612);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // Frm_Danhsachkhach
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1309, 768);
            this.Controls.Add(this.pnDSKhach);
            this.Controls.Add(this.pnLogo);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "Frm_Danhsachkhach";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Payment";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Payment_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pnLogo)).EndInit();
            this.pnLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl25)).EndInit();
            this.panelControl25.ResumeLayout(false);
            this.panelControl25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl56)).EndInit();
            this.panelControl56.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl57)).EndInit();
            this.panelControl57.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnDSKhach)).EndInit();
            this.pnDSKhach.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl50)).EndInit();
            this.panelControl50.ResumeLayout(false);
            this.panelControl50.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl68)).EndInit();
            this.panelControl68.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl69)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.PanelControl pnLogo;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.PanelControl panelControl56;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.PanelControl panelControl57;
        private DevExpress.XtraEditors.PanelControl panelControl58;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.PanelControl panelControl25;
        private DevExpress.XtraEditors.LabelControl lb_Kho;
        private DevExpress.XtraEditors.LabelControl lb_Quay;
        private DevExpress.XtraEditors.LabelControl lbgiay;
        private DevExpress.XtraEditors.LabelControl lbCaBan;
        private DevExpress.XtraEditors.LabelControl lbGio;
        private DevExpress.XtraEditors.LabelControl lb_NhanVien;
        private DevExpress.XtraEditors.LabelControl lbtimeby;
        private DevExpress.XtraEditors.PanelControl pnDSKhach;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn TENKH;
        private DevExpress.XtraGrid.Columns.GridColumn CMND;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYSINH;
        private DevExpress.XtraGrid.Columns.GridColumn GIOITINH;
        private DevExpress.XtraGrid.Columns.GridColumn QUOCTICH;
        private DevExpress.XtraGrid.Columns.GridColumn DIENTHOAI;
        private DevExpress.XtraGrid.Columns.GridColumn DIACHI;
        private DevExpress.XtraGrid.Columns.GridColumn EMAIL;
        private DevExpress.XtraGrid.Columns.GridColumn MA_KH;
        private DevExpress.XtraEditors.PanelControl panelControl50;
        private DevExpress.XtraEditors.PanelControl panelControl68;
        private DevExpress.XtraEditors.PanelControl panelControl69;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnXuatExcel;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btbIn;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn CHECKOUT;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_BAN;
        private DevExpress.XtraGrid.Columns.GridColumn CHECKIN;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HOADON;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraGrid.Columns.GridColumn DATHANHTOAN;
    }
}