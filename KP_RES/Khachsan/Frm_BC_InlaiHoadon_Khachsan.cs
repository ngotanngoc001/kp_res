﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_BC_InlaiHoadon_Khachsan : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt = new DataTable();

        public Frm_BC_InlaiHoadon_Khachsan()
        {
            InitializeComponent();
            LoadCombo();
        }
     
        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                String sql = string.Empty;

                sql = "EXEC SP_BAOCAO_INLAIHOADON_KHACHSAN @TUNGAY='" + string.Format("{0:yyyyMMdd}", dtpTungay.EditValue)
                    + "', @DENNGAY='" + string.Format("{0:yyyyMMdd}", dtpDenngay.EditValue) + " 23:59:59',@MANHANVIEN='"
                    + cboNV.EditValue.ToString() + "'";
                dt = clsMain.ReturnDataTable(sql);
                gridControl1.DataSource = dt;
                gridView1.Columns["MA_HOADON"].GroupIndex = 0;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show(_kocodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void gridView1_CustomDrawGroupRow(object sender, DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs e)
        {
            //GridView view = sender as GridView;
            //GridGroupRowInfo info = e.Info as GridGroupRowInfo;
            //if (info.Column.Caption == "Tên nhân viên")
            //{
            //    info.GroupText = info.Column.Caption + ": <color=Blue>" + info.GroupValueText + "</color> ";
            //    info.GroupText += "<color=Red>" + view.GetGroupSummaryText(e.RowHandle) + "</color> ";
            //}
        }

        private void LoadCombo()
        {
            dtpTungay.EditValue = clsGlobal.gdServerDate;
            dtpDenngay.EditValue = clsGlobal.gdServerDate;
            DataTable dt = clsMain.ReturnDataTable("select MANHANVIEN as MA,TENNHANVIEN as TEN from DM_NHANVIEN where SUDUNG=1 order by TEN");
            dt.Rows.Add("0", "Tất cả");
            cboNV.Properties.DataSource = dt;
            cboNV.EditValue = 0;
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    DataColumn a = new DataColumn();
                    a.ColumnName = "FILTER";
                    a.DataType = System.Type.GetType("System.String");
                    a.DefaultValue = "Nhân viên : " + cboNV.Text;
                    if (ContainColumn("FILTER", dt) == false)
                        dt.Columns.Add(a);
                    DataColumn a1 = new DataColumn();
                    a1.ColumnName = "TUNGAY";
                    a1.DefaultValue = string.Format("{0:dd/MM/yyyy}", dtpTungay.EditValue);
                    if (ContainColumn("TUNGAY", dt) == false)
                        dt.Columns.Add(a1);
                    DataColumn a2 = new DataColumn();
                    a2.ColumnName = "DENNGAY";
                    a2.DefaultValue = string.Format("{0:dd/MM/yyyy}", dtpDenngay.EditValue);
                    if (ContainColumn("DENNGAY", dt) == false)
                        dt.Columns.Add(a2);


                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = dt;
                    frm.Mode = 48;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show(_kocodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception EX)
            {
                XtraMessageBox.Show(EX.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally { this.Cursor = Cursors.Default; }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
            }
            this.Refresh();
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnChitiet_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle < 0)
                    throw new Exception(_chuachondtcanxem);
                Frm_BCChitietHoaDon_Khachsan frm = new Frm_BCChitietHoaDon_Khachsan(gridView1.GetFocusedRowCellValue(MA_HOADON).ToString());
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.WindowState = FormWindowState.Maximized;
                frm.ShowDialog();
                frm.Dispose();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnInbill_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    string sSQL = "";

                    sSQL = "";
                    sSQL += "Select ROW_NUMBER() over (order by (select 1)) as STT,'' as TenNV ,0 as CHIETKHAU,";
                    sSQL += "cthd.GIABAN, (cthd.SOLUONG * cthd.GIABAN) - (cthd.SOLUONG * cthd.GIABAN) * 0 / 100 as Thanh_Tien, hd.NGAYTAO, ISNULL(hd.CHECKOUT,hd.NGAYTAO) as ngayHD,hh.TEN_HANGHOA, cthd.SOLUONG,";
                    sSQL += "hd.PHIPHUCVU as Phiphucvu,hd.MA_HOADON,TEN_QUAY as MA_QUAY, ISNULL(hd.TIENDATCOC,0) as TIENDATCOC,";
                    sSQL += "case when ISNULL(hd.MANVINHOADON,'')='' Then (hd.MANHANVIEN) Else (hd.MANVINHOADON) End as MANHANVIEN,";
                    sSQL += "hd.GIAMGIA as Giam_Gia1, hd.TONGTIEN as Tong_Cong, hd.TONGTIENPHONG as TienGio, hd.CHECKIN, hd.CHECKOUT, hd.GIAITHICH , hd.TIEN_KHACHTRA, hd.TIEN_TRAKHACH, hd.TONGTIEN, N'PHIẾU THANH TOÁN \n (IN LẠI)' as Ten_HoaDon,'' AS TenNhanVien,'' AS GhiChu,C.TEN AS TENKH,ISNULL(C.MA,'') AS MA_KHACHHANG,ISNULL(C.DIACHI,'') AS DIACHIGIAOHANG,ISNULL(C.DIENTHOAI,'') AS DIENTHOAI_KH" + "\n";
                    sSQL += "From HANGHOA hh inner join CT_HOADON_KHACHSAN cthd on hh.MA_HANGHOA = cthd.MA_HANGHOA inner join HOADON_KHACHSAN hd on cthd.MA_HOADON = hd.MA_HOADON" + "\n";
                    sSQL += "inner join QUAY q on hd.MA_QUAY = q.MA_QUAY LEFT JOIN KHTT C ON hd.MA_KHACHHANG = C.MA " + "\n";
                    sSQL += "Where hd.MA_HOADON =" + clsMain.SQLString(gridView1.GetFocusedRowCellValue(MA_HOADON).ToString());

                    DataTable dtReport = clsMain.ReturnDataTable(sSQL);
                    dtReport.Columns.Add("Giam_Gia", typeof(String));

                    string TenNV = "";
                    if (dtReport.Rows.Count > 0)
                    {
                        sSQL = "Select TENNHANVIEN From DM_NHANVIEN Where MANHANVIEN=" + clsMain.SQLString(dtReport.Rows[0]["MANHANVIEN"].ToString());
                        DataTable dtTenNV = clsMain.ReturnDataTable(sSQL);
                        if (dtTenNV.Rows.Count > 0)
                            TenNV = dtTenNV.Rows[0]["TENNHANVIEN"].ToString();
                    }

                    sSQL = "Select TEN_BAN From CT_MOBAN_KHACHSAN ct,BAN b Where ct.MA_BAN=b.MA_BAN And ct.MA_HOADON=" + clsMain.SQLString(gridView1.GetFocusedRowCellValue(MA_HOADON).ToString());
                    DataTable dtTenban = clsMain.ReturnDataTable(sSQL);
                    if (dtTenban.Rows.Count > 0)
                    {
                        dtReport.Columns.Add("TEN_BAN", typeof(String));

                        int n = dtReport.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            dtReport.Rows[i]["TEN_BAN"] = dtTenban.Rows[0]["TEN_BAN"].ToString();
                            dtReport.Rows[i]["TenNV"] = TenNV;
                            if (double.Parse(dtReport.Rows[i]["Giam_Gia1"].ToString()) > 100)
                                dtReport.Rows[i]["Giam_Gia"] = string.Format("{0:#,###0}", dtReport.Rows[i]["Giam_Gia1"]);
                            else
                                dtReport.Rows[i]["Giam_Gia"] = dtReport.Rows[i]["Giam_Gia1"].ToString();
                        }
                        Frm_BCInPhieu frm = new Frm_BCInPhieu();
                        frm.WindowState = FormWindowState.Maximized;
                        frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        frm.dtSource = dtReport;
                        //frm.Mode = (dtReport.Rows[0]["MA_KHACHHANG"].ToString() == "") ? 42 : 98;
                        if (dtReport.Rows[0]["MA_KHACHHANG"].ToString() == "")
                        {
                            if (cls_KP_RES.Mode == 8)
                                frm.Mode = 100;
                            else
                                frm.Mode = 113;
                        }
                        else
                        {
                            if (cls_KP_RES.Mode == 8)
                                frm.Mode = 101;
                            else
                                frm.Mode = 98;
                        }

                        frm.ShowDialog();
                        frm.Dispose();
                    }
                    else
                    {
                        int n = dtReport.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            dtReport.Rows[i]["TenNV"] = TenNV;
                            if (double.Parse(dtReport.Rows[i]["Giam_Gia1"].ToString()) > 100)
                                dtReport.Rows[i]["Giam_Gia"] = string.Format("{0:#,###0}", dtReport.Rows[i]["Giam_Gia1"]);
                            else
                                dtReport.Rows[i]["Giam_Gia"] = dtReport.Rows[i]["Giam_Gia1"].ToString() + "%";
                        }
                        Frm_BCInPhieu frm = new Frm_BCInPhieu();
                        frm.WindowState = FormWindowState.Maximized;
                        frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        frm.dtSource = dtReport;
                        //frm.Mode = (dtReport.Rows[0]["MA_KHACHHANG"].ToString() == "") ? 6 : 99;
                        if (dtReport.Rows[0]["MA_KHACHHANG"].ToString() == "")
                        {
                            if (cls_KP_RES.Mode == 8)
                                frm.Mode = 100;
                            else
                                frm.Mode = 113;
                        }
                        else
                        {
                            if (cls_KP_RES.Mode == 8)
                                frm.Mode = 101;
                            else
                                frm.Mode = 99;
                        }

                        frm.ShowDialog();
                        frm.Dispose();
                    }
                }
                else
                {
                    XtraMessageBox.Show(_kocodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception EX)
            {
                XtraMessageBox.Show(EX.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally { this.Cursor = Cursors.Default; }   
        }


        string _thongbao = "";
        string _kocodulieu = "";
        string _chuachondtcanxem = "";
        private void Frm_BC_InlaiHoadon_Load(object sender, EventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            this.Text = rm.GetString("danhsachhoadoninlai", culture);
            gridView1.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView1.Columns["MA_HOADON"].Caption = rm.GetString("hoadon", culture);
            gridView1.Columns["TEN_NHANVIEN"].Caption = rm.GetString("nhanvien", culture);
            gridView1.Columns["NGAYTAO"].Caption = rm.GetString("ngaytao", culture);
            gridView1.Columns["TONGTIEN"].Caption = rm.GetString("tongtien", culture);
            gridView1.Columns["NV_INLAI"].Caption = rm.GetString("nhanvieninlai", culture);
            gridView1.Columns["GIO_INLAI"].Caption = rm.GetString("gioinlai", culture);
            gridView1.GroupPanelText = rm.GetString("keothacotvaodaydenhom", culture);
            labeFrom.Text = rm.GetString("tungay", culture);
            lb_To.Text = rm.GetString("denngay", culture);
            lbNhanVien.Text = rm.GetString("nhanvien", culture);
            btnTimkiem.Text = rm.GetString("xem", culture);
            _chuachondtcanxem = rm.GetString("chuachondtcanxem", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _kocodulieu = rm.GetString("kocodulieu", culture);
        }
    }
}