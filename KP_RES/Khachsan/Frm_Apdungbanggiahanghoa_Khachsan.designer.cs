﻿namespace KP_RES 
{
    partial class Frm_Apdungbanggiahanghoa_Khachsan 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Apdungbanggiahanghoa_Khachsan));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnLUU = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_KHUVUC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lookKHUVUC = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lookHANGHOA = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.THUHAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUBA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUTU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUNAM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUSAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUBAY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CHUNHAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtGIABAN = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookKHUVUC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookHANGHOA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Controls.Add(this.btnLUU);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1254, 0);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 690);
            this.panelControl2.TabIndex = 2;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 162);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 446);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 126);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 286);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 366);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 608);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 82);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // btnLUU
            // 
            this.btnLUU.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLUU.Appearance.Options.UseFont = true;
            this.btnLUU.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLUU.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLUU.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLUU.Location = new System.Drawing.Point(2, 2);
            this.btnLUU.Margin = new System.Windows.Forms.Padding(0);
            this.btnLUU.Name = "btnLUU";
            this.btnLUU.Size = new System.Drawing.Size(35, 80);
            this.btnLUU.TabIndex = 15;
            this.btnLUU.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.lookKHUVUC,
            this.lookHANGHOA,
            this.txtGIABAN});
            this.gridControl2.Size = new System.Drawing.Size(1254, 690);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MA_KHUVUC,
            this.MA_HANGHOA,
            this.THUHAI,
            this.THUBA,
            this.THUTU,
            this.THUNAM,
            this.THUSAU,
            this.THUBAY,
            this.CHUNHAT});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 40;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA_KHUVUC
            // 
            this.MA_KHUVUC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_KHUVUC.AppearanceHeader.Options.UseFont = true;
            this.MA_KHUVUC.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_KHUVUC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_KHUVUC.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_KHUVUC.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA_KHUVUC.Caption = "Khu";
            this.MA_KHUVUC.ColumnEdit = this.lookKHUVUC;
            this.MA_KHUVUC.FieldName = "MA_KHUVUC";
            this.MA_KHUVUC.MinWidth = 150;
            this.MA_KHUVUC.Name = "MA_KHUVUC";
            this.MA_KHUVUC.OptionsColumn.AllowEdit = false;
            this.MA_KHUVUC.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.MA_KHUVUC.Visible = true;
            this.MA_KHUVUC.VisibleIndex = 1;
            this.MA_KHUVUC.Width = 150;
            // 
            // lookKHUVUC
            // 
            this.lookKHUVUC.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lookKHUVUC.AppearanceDropDown.Options.UseFont = true;
            this.lookKHUVUC.AutoHeight = false;
            this.lookKHUVUC.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookKHUVUC.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA_KHUVUC", "MA_KHUVUC", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN_KHUVUC", "TEN_KHUVUC"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN_CUAHANG", 40, "TEN_CUAHANG")});
            this.lookKHUVUC.DisplayMember = "TEN_KHUVUC";
            this.lookKHUVUC.DropDownItemHeight = 40;
            this.lookKHUVUC.Name = "lookKHUVUC";
            this.lookKHUVUC.ShowHeader = false;
            this.lookKHUVUC.ValueMember = "MA_KHUVUC";
            // 
            // MA_HANGHOA
            // 
            this.MA_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.MA_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_HANGHOA.Caption = "Hàng hóa";
            this.MA_HANGHOA.ColumnEdit = this.lookHANGHOA;
            this.MA_HANGHOA.FieldName = "MA_HANGHOA";
            this.MA_HANGHOA.Name = "MA_HANGHOA";
            this.MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this.MA_HANGHOA.OptionsColumn.FixedWidth = true;
            this.MA_HANGHOA.Width = 200;
            // 
            // lookHANGHOA
            // 
            this.lookHANGHOA.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lookHANGHOA.AppearanceDropDown.Options.UseFont = true;
            this.lookHANGHOA.AutoHeight = false;
            this.lookHANGHOA.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookHANGHOA.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA_BANGGIA", "MA_BANGGIA", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN_BANGGIA", "TEN_BANGGIA")});
            this.lookHANGHOA.DisplayMember = "TEN_BANGGIA";
            this.lookHANGHOA.DropDownItemHeight = 40;
            this.lookHANGHOA.Name = "lookHANGHOA";
            this.lookHANGHOA.ShowHeader = false;
            this.lookHANGHOA.ValueMember = "MA_BANGGIA";
            // 
            // THUHAI
            // 
            this.THUHAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.THUHAI.AppearanceHeader.Options.UseFont = true;
            this.THUHAI.AppearanceHeader.Options.UseTextOptions = true;
            this.THUHAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THUHAI.Caption = "Thứ hai";
            this.THUHAI.ColumnEdit = this.lookHANGHOA;
            this.THUHAI.FieldName = "THUHAI";
            this.THUHAI.Name = "THUHAI";
            this.THUHAI.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.THUHAI.OptionsColumn.FixedWidth = true;
            this.THUHAI.Visible = true;
            this.THUHAI.VisibleIndex = 2;
            this.THUHAI.Width = 150;
            // 
            // THUBA
            // 
            this.THUBA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.THUBA.AppearanceHeader.Options.UseFont = true;
            this.THUBA.AppearanceHeader.Options.UseTextOptions = true;
            this.THUBA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THUBA.Caption = "Thứ ba";
            this.THUBA.ColumnEdit = this.lookHANGHOA;
            this.THUBA.FieldName = "THUBA";
            this.THUBA.Name = "THUBA";
            this.THUBA.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.THUBA.OptionsColumn.FixedWidth = true;
            this.THUBA.Visible = true;
            this.THUBA.VisibleIndex = 3;
            this.THUBA.Width = 150;
            // 
            // THUTU
            // 
            this.THUTU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THUTU.AppearanceHeader.Options.UseFont = true;
            this.THUTU.AppearanceHeader.Options.UseTextOptions = true;
            this.THUTU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THUTU.Caption = "Thứ tư";
            this.THUTU.ColumnEdit = this.lookHANGHOA;
            this.THUTU.FieldName = "THUTU";
            this.THUTU.Name = "THUTU";
            this.THUTU.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.THUTU.OptionsColumn.FixedWidth = true;
            this.THUTU.Visible = true;
            this.THUTU.VisibleIndex = 4;
            this.THUTU.Width = 150;
            // 
            // THUNAM
            // 
            this.THUNAM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THUNAM.AppearanceHeader.Options.UseFont = true;
            this.THUNAM.AppearanceHeader.Options.UseTextOptions = true;
            this.THUNAM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THUNAM.Caption = "Thứ năm";
            this.THUNAM.ColumnEdit = this.lookHANGHOA;
            this.THUNAM.FieldName = "THUNAM";
            this.THUNAM.Name = "THUNAM";
            this.THUNAM.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.THUNAM.OptionsColumn.FixedWidth = true;
            this.THUNAM.Visible = true;
            this.THUNAM.VisibleIndex = 5;
            this.THUNAM.Width = 150;
            // 
            // THUSAU
            // 
            this.THUSAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THUSAU.AppearanceHeader.Options.UseFont = true;
            this.THUSAU.AppearanceHeader.Options.UseTextOptions = true;
            this.THUSAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THUSAU.Caption = "Thứ sáu";
            this.THUSAU.ColumnEdit = this.lookHANGHOA;
            this.THUSAU.FieldName = "THUSAU";
            this.THUSAU.Name = "THUSAU";
            this.THUSAU.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.THUSAU.OptionsColumn.FixedWidth = true;
            this.THUSAU.Visible = true;
            this.THUSAU.VisibleIndex = 6;
            this.THUSAU.Width = 150;
            // 
            // THUBAY
            // 
            this.THUBAY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THUBAY.AppearanceHeader.Options.UseFont = true;
            this.THUBAY.AppearanceHeader.Options.UseTextOptions = true;
            this.THUBAY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THUBAY.Caption = "Thứ bảy";
            this.THUBAY.ColumnEdit = this.lookHANGHOA;
            this.THUBAY.FieldName = "THUBAY";
            this.THUBAY.Name = "THUBAY";
            this.THUBAY.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.THUBAY.OptionsColumn.FixedWidth = true;
            this.THUBAY.Visible = true;
            this.THUBAY.VisibleIndex = 7;
            this.THUBAY.Width = 150;
            // 
            // CHUNHAT
            // 
            this.CHUNHAT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CHUNHAT.AppearanceHeader.Options.UseFont = true;
            this.CHUNHAT.AppearanceHeader.Options.UseTextOptions = true;
            this.CHUNHAT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHUNHAT.Caption = "Chủ nhật";
            this.CHUNHAT.ColumnEdit = this.lookHANGHOA;
            this.CHUNHAT.FieldName = "CHUNHAT";
            this.CHUNHAT.MinWidth = 120;
            this.CHUNHAT.Name = "CHUNHAT";
            this.CHUNHAT.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.CHUNHAT.OptionsColumn.AllowMove = false;
            this.CHUNHAT.OptionsColumn.FixedWidth = true;
            this.CHUNHAT.Visible = true;
            this.CHUNHAT.VisibleIndex = 8;
            this.CHUNHAT.Width = 150;
            // 
            // txtGIABAN
            // 
            this.txtGIABAN.AutoHeight = false;
            this.txtGIABAN.DisplayFormat.FormatString = "N0";
            this.txtGIABAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN.EditFormat.FormatString = "N0";
            this.txtGIABAN.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN.Mask.EditMask = "N0";
            this.txtGIABAN.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGIABAN.Name = "txtGIABAN";
            // 
            // Frm_Apdungbanggiahanghoa_Khachsan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1293, 690);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panelControl2);
            this.Name = "Frm_Apdungbanggiahanghoa_Khachsan";
            this.Text = "Áp dụng bảng giá hàng hóa";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Apdungbanggiahanghoa_Khachsan_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookKHUVUC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookHANGHOA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA_KHUVUC;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn THUHAI;
        private DevExpress.XtraGrid.Columns.GridColumn THUBA;
        private DevExpress.XtraGrid.Columns.GridColumn CHUNHAT;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraGrid.Columns.GridColumn THUTU;
        private DevExpress.XtraGrid.Columns.GridColumn THUNAM;
        private DevExpress.XtraGrid.Columns.GridColumn THUSAU;
        private DevExpress.XtraGrid.Columns.GridColumn THUBAY;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lookKHUVUC;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lookHANGHOA;
        private DevExpress.XtraEditors.SimpleButton btnLUU;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtGIABAN;




    }
}