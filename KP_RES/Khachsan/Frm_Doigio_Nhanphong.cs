﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES
{
    public partial class Frm_Doigio_Nhanphong : DevExpress.XtraEditors.XtraForm
    {
        string sHoadon = "";
        public Frm_Doigio_Nhanphong(String sGio, String Hoadon)
        {
            InitializeComponent();

            sHoadon = Hoadon;
            txtGhiChu_old.Text = txtGhiChu.Text = sGio;
            txtGhiChu.Focus();
            us_keyboard1.SetTextbox(txtGhiChu);
        }

        private bool checkinput()
        {
            if (txtGhiChu.Text.Trim() == "")
            {
                XtraMessageBox.Show("Nhập giờ nhận phòng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            else
            {
                try
                {
                    DateTime dti = DateTime.ParseExact(txtGhiChu.Text.Trim (), "dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                   
                }
                catch
                {
                    return false;
                }
            }
            return true;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!checkinput())
                return;

            string sSQL = "";
            sSQL +="Update HOADON_KHACHSAN SET NGAYTAO= "+clsMain .SQLString (txtGhiChu.Text.Trim())+" , CHECKIN= "+clsMain .SQLString (txtGhiChu.Text.Trim())+" Where MA_HOADON= "+clsMain .SQLString (sHoadon);
            if(clsMain .ExecuteSQL (sSQL ))
            {
                XtraMessageBox.Show("Đổi giờ nhận phòng thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void Frm_Giaohang_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnLuu_Click(sender, e);
            }
        }
    }
}