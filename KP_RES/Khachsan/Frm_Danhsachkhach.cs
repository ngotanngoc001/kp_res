﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using KP_RES.Class;
using System.IO;
using KP_Report;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;

namespace KP_RES
{
    public partial class Frm_Danhsachkhach : DevExpress.XtraEditors.XtraForm
    {
        int iGrid = 1;
        public Frm_Danhsachkhach()
        {
            InitializeComponent();
        }

        private void Payment_Load(object sender, EventArgs e)
        {
            //thông tin kho,quầy,ca bán
            timer1.Start();
            lb_Quay.Text = cls_ConfigCashier.nameCashier;
            lb_Kho.Text = cls_ConfigCashier.nameWarehouse;
            lb_NhanVien.Text = clsUserManagement.ReturnNameOfUserLogin();
            lbCaBan.Text = clsMain.GetServerDate().ToString("dd-MM-yyyy");
            lbtimeby.Text = cls_ConfigCashier.sTenCB + ": " + cls_ConfigCashier.sGioBD + " --> " + cls_ConfigCashier.sGioKT;

            LoadDanhSachKhach();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string hour, minute, second;
            second = DateTime.Now.Second.ToString();
            hour = DateTime.Now.Hour.ToString();
            minute = DateTime.Now.Minute.ToString();
            if (second.Length < 2)
                lbgiay.Text = "0" + second;
            else
                lbgiay.Text = second;
            if (minute.Length < 2)
                lbGio.Text = hour + ":0" + minute;
            else
                lbGio.Text = hour + ":" + minute;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadDanhSachKhach()
        {
            string sSQL = "";
            sSQL += "SELECT MA_KH,TEN,NGAYSINH,(CASE WHEN GIOITINH='1' THEN N'Nam' ELSE N'Nữ' END) AS GIOITINH,DIACHI,DIENTHOAI,EMAIL,CMND,QUOCTICH ," + "\n";
            sSQL += "(CASE WHEN MA_KH IN (SELECT  DISTINCT c.MA_KH  from HOADON_KHACHSAN A " + "\n";
            sSQL += "LEFT JOIN CT_TTKHACHHANG_KHACHSAN B ON A.MA_HOADON  =B.MA_HOADON " + "\n";
            sSQL += "LEFT JOIN KS_KHACHHANG  C ON B.MA_KH  =C.MA_KH " + "\n";
            sSQL += "WHERE A.ISPAYMENT =0 ) THEN N'Đang thuê phòng' ELSE N'Đã trả phòng' END) AS TRANGTHAI," + "\n";
            sSQL += "(SELECT TOP 1 KS_DATPHONG.DATHANHTOAN FROM KS_DATPHONG INNER JOIN HOADON_KHACHSAN ON KS_DATPHONG.HOADON = HOADON_KHACHSAN.MA_HOADON" + "\n";
            sSQL += "INNER JOIN CT_TTKHACHHANG_KHACHSAN  ON HOADON_KHACHSAN.MA_HOADON  =CT_TTKHACHHANG_KHACHSAN.MA_HOADON " + "\n";
            sSQL += "WHERE HOADON_KHACHSAN.ISPAYMENT =0 " + "\n";
            sSQL += "AND KS_DATPHONG.DATHANHTOAN>0" + "\n";
            sSQL += "AND CT_TTKHACHHANG_KHACHSAN.MA_KH = KS_KHACHHANG.MA_KH" + "\n";
            sSQL += ") AS DATHANHTOAN " + "\n";
            sSQL += "FROM KS_KHACHHANG" + "\n";
            sSQL += "ORDER BY TRANGTHAI DESC,TEN" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            gridControl1.DataSource = dt;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
            {
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                if (iGrid == 1)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
                else
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                if (iGrid == 1)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                    {
                        btnXuongit.Enabled = false;
                        btnXuongnhieu.Enabled = false;
                    }
                }
                else
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                    {
                        btnXuongit.Enabled = false;
                        btnXuongnhieu.Enabled = false;
                    }
                }

               
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                if (iGrid == 1)
                {

                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                    if (gridView1.FocusedRowHandle == 0)
                    {
                        btnLenit.Enabled = false;
                        btnLennhieu.Enabled = false;
                    }
                }
                else
                {

                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                    if (gridView2.FocusedRowHandle == 0)
                    {
                        btnLenit.Enabled = false;
                        btnLennhieu.Enabled = false;
                    }
                }

            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                if (iGrid == 1)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }
                else
                {

                    gridView2.Focus();
                    gridView2.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }

            }
            catch
            {
            }
        
        }

        private void btnXuatExcel_Click(object sender, EventArgs e)
        {
            if (iGrid == 1)
            {
                if (this.gridView1.RowCount == 0)
                {
                    XtraMessageBox.Show("Không có dữ liệu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    STT.Visible = false;
                  
                    using (SaveFileDialog saveDialog = new SaveFileDialog())
                    {
                        saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                        if (saveDialog.ShowDialog() != DialogResult.Cancel)
                        {
                            string exportFilePath = saveDialog.FileName;
                            string fileExtenstion = new FileInfo(exportFilePath).Extension;
                            switch (fileExtenstion)
                            {
                                case ".xls":
                                    gridControl1.ExportToXls(exportFilePath);
                                    break;
                                case ".xlsx":
                                    gridControl1.ExportToXlsx(exportFilePath);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    STT.Visible = true;
                    
                }
            }
            else
            {
                if (this.gridView2.RowCount == 0)
                {
                    XtraMessageBox.Show("Không có dữ liệu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    STT1.Visible = false;
                    FILL.Visible = false;
                    using (SaveFileDialog saveDialog = new SaveFileDialog())
                    {
                        saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                        if (saveDialog.ShowDialog() != DialogResult.Cancel)
                        {
                            string exportFilePath = saveDialog.FileName;
                            string fileExtenstion = new FileInfo(exportFilePath).Extension;
                            switch (fileExtenstion)
                            {
                                case ".xls":
                                    gridControl2.ExportToXls(exportFilePath);
                                    break;
                                case ".xlsx":
                                    gridControl2.ExportToXlsx(exportFilePath);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    STT1.Visible = true;
                    FILL.Visible = true;
                }
            }
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            iGrid = 1;
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;

            LoadChiTiet(gridView1.GetFocusedRowCellValue ("MA_KH").ToString());
        }

        private void LoadChiTiet(string maKhach)
        {
            string sSQL = "";
            sSQL += "SELECT  A.MA_HOADON ,A.CHECKIN ,A.CHECKOUT ,D.TEN_BAN,C.MA_KH ,C.TEN,C.CMND  " + "\n";
            sSQL += "FROM HOADON_KHACHSAN A " + "\n";
            sSQL += "LEFT JOIN CT_TTKHACHHANG_KHACHSAN B ON A.MA_HOADON  =B.MA_HOADON " + "\n";
            sSQL += "LEFT JOIN KS_KHACHHANG  C ON B.MA_KH  =C.MA_KH  " + "\n";
            sSQL += "LEFT JOIN BAN   D ON D.MA_BAN   =B.MA_BAN " + "\n";
            sSQL += "WHERE C.MA_KH =" +clsMain .SQLString (maKhach )+ "\n";
            sSQL += "ORDER BY A.CHECKIN DESC" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            gridControl2.DataSource = dt;
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT1 & e.RowHandle >= 0)
            {
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
            }
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            iGrid = 2;
        }

        private void gridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            try
            {
                GridView View = sender as GridView;
                if (e.RowHandle >= 0)
                {
                    if (View.GetRowCellValue(e.RowHandle, View.Columns["TRANGTHAI"]).ToString() == "Đang thuê phòng")
                    {
                        e.Appearance.ForeColor = Color.Blue;
                    }
                    else
                    {
                        //e.Appearance.ForeColor = Color.Blue;
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btbIn_Click(object sender, EventArgs e)
        {
            if (iGrid == 1)
            {
                if (this.gridView1.RowCount == 0)
                {
                    XtraMessageBox.Show("Không có dữ liệu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    DataView dv = cls_KP_RES.GetFilteredData(gridView1);
                    DataTable dt = dv.ToTable();

                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = dt;
                    frm.Mode = 93;
                    frm.ShowDialog();
                    frm.Dispose();

                }
            }
            else
            {
                if (this.gridView2.RowCount == 0)
                {
                    XtraMessageBox.Show("Không có dữ liệu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    DataView dv = cls_KP_RES . GetFilteredData(gridView2);
                    DataTable dt = dv.ToTable();

                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = dt;
                    frm.Mode = 94;
                    frm.ShowDialog();
                    frm.Dispose();
                }
            }
        }

       
        
    }
}