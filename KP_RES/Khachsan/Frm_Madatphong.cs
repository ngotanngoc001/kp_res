﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES
{
    public partial class Frm_Madatphong : DevExpress.XtraEditors.XtraForm
    {
        public String sMa_Datphong_Nhap = "";
        public String sMa_Datphong = "";

        public Frm_Madatphong()
        {
            InitializeComponent();
            us_keyboard1.SetTextbox(txtMadatphong);
        }

        private bool checkinput()
        {
            if (txtMadatphong.Text.Trim() == "")
            {
                XtraMessageBox.Show("Nhập mã đặt phòng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!checkinput())
                return;

            if (txtMadatphong.Text.Trim() == sMa_Datphong)
            {
                this.Close();
            }
            else
            {
                XtraMessageBox.Show("Mã đặt phòng không đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMadatphong.Text = "";
            }
            sMa_Datphong_Nhap = txtMadatphong.Text;
        }

        private void Frm_Giaohang_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnLuu_Click(sender, e);
            }
        }

       
    }
}