﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES
{
    public partial class Frm_Apdungbanggiahanghoa_Khachsan : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Apdungbanggiahanghoa_Khachsan()
        {
            InitializeComponent();
            LoaddataGridView();
            LoadPermission();
        }

        private void Frm_Apdungbanggiahanghoa_Khachsan_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            string sSQL = "";
            DataView dv = (DataView)gridView2.DataSource;
            DataTable dt = dv.ToTable();

            foreach (DataRow dr in dt.Rows)
            {
                sSQL += "DELETE FROM APDUNGBANGGIA" + "\n";
                sSQL += "WHERE MA_KHUVUC = " + clsMain.SQLString(dr["MA_KHUVUC"].ToString()) + "\n";

                sSQL += "INSERT INTO APDUNGBANGGIA(MA_KHUVUC, THUHAI, THUBA, THUTU, THUNAM, THUSAU, THUBAY, CHUNHAT)" + "\n";
                sSQL += "VALUES( ";
                sSQL += clsMain.SQLString(dr["MA_KHUVUC"].ToString()) + ",";
                sSQL += clsMain.SQLString(dr["THUHAI"].ToString()) + ",";
                sSQL += clsMain.SQLString(dr["THUBA"].ToString()) + ",";
                sSQL += clsMain.SQLString(dr["THUTU"].ToString()) + ",";
                sSQL += clsMain.SQLString(dr["THUNAM"].ToString()) + ",";
                sSQL += clsMain.SQLString(dr["THUSAU"].ToString()) + ",";
                sSQL += clsMain.SQLString(dr["THUBAY"].ToString()) + ",";
                sSQL += clsMain.SQLString(dr["CHUNHAT"].ToString()) + ")" + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
                LoaddataGridView();
            else
                XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void LoaddataGridView()
        {
            try
            {
                String sSQL = "";
                sSQL += "SELECT A.MA_KHUVUC, A.TEN_KHUVUC, B.TEN_CUAHANG" + "\n";
                sSQL += "FROM KHUVUC A, CUAHANG B" + "\n";
                sSQL += "WHERE A.MA_CUAHANG=B.MA_CUAHANG" + "\n";
                DataTable dtKV = clsMain.ReturnDataTable(sSQL);
                lookKHUVUC.DataSource = dtKV;

                sSQL = "";
                sSQL += "SELECT MABANGGIA AS MA_BANGGIA, TENBANGGIA AS TEN_BANGGIA, GHICHU, SUDUNG" + "\n";
                sSQL += "FROM BANGGIA" + "\n";
                sSQL += "WHERE MABANGGIA IN (SELECT MA_BANGGIA FROM THIETLAPBANGGIA)" + "\n";
                sSQL += "AND MODE = 0 AND SUDUNG = 1" + "\n";
                DataTable dtBG = clsMain.ReturnDataTable(sSQL);
                lookHANGHOA.DataSource = dtBG;

                sSQL = "";
                sSQL += "SELECT AP.MA_KHUVUC, THUHAI, THUBA, THUTU, THUNAM, THUSAU, THUBAY, CHUNHAT" + "\n";
                sSQL += "FROM APDUNGBANGGIA AP INNER JOIN KHUVUC KV ON AP.MA_KHUVUC = KV.MA_KHUVUC" + "\n";
                sSQL += "WHERE KV.MODE = 2" + "\n";
                DataTable dt = clsMain.ReturnDataTable(sSQL);

                sSQL = "";
                sSQL += "SELECT MA_KHUVUC" + "\n";
                sSQL += "FROM KHUVUC" + "\n";
                sSQL += "WHERE MODE = 2 AND (MA_KHUVUC Not In (Select  MA_KHUVUC From APDUNGBANGGIA))" + "\n";
                DataTable dtKVNew = clsMain.ReturnDataTable(sSQL);

                if (dtBG.Rows.Count > 0)
                {
                    foreach (DataRow drKVNew in dtKVNew.Rows)
                    {
                        dt.Rows.Add(drKVNew["MA_KHUVUC"].ToString(), dtBG.Rows[0]["MA_BANGGIA"].ToString(), dtBG.Rows[0]["MA_BANGGIA"].ToString(), dtBG.Rows[0]["MA_BANGGIA"].ToString(), dtBG.Rows[0]["MA_BANGGIA"].ToString(), dtBG.Rows[0]["MA_BANGGIA"].ToString(), dtBG.Rows[0]["MA_BANGGIA"].ToString(), dtBG.Rows[0]["MA_BANGGIA"].ToString());
                    }
                }

                gridControl2.DataSource = dt;
            }
            catch
            {
            }
        }

        private void LoadPermission()
        {
            btnLUU.Enabled = clsUserManagement.AllowDelete("0906201501") && clsUserManagement.AllowDelete("42");
        }
    }
}