﻿namespace KP_RES 
{
    partial class Frm_CauhinhBanggiaKhachsan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.timeKS_GIONHANPHONG = new DevExpress.XtraEditors.TimeEdit();
            this.timeKS_GIOTRAPHONG = new DevExpress.XtraEditors.TimeEdit();
            this.timeKS_GIOGIADEMTU = new DevExpress.XtraEditors.TimeEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.timeKS_GIOGIADEMDEN = new DevExpress.XtraEditors.TimeEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txtKS_LAMTRONGIORA = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.timeKS_GIONHANPHONG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeKS_GIOTRAPHONG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeKS_GIOGIADEMTU.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeKS_GIOGIADEMDEN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKS_LAMTRONGIORA.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(428, 215);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(32, 19);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Phút";
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(311, 245);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(110, 35);
            this.btnLuu.TabIndex = 5;
            this.btnLuu.Text = "&1.Đồng ý";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Location = new System.Drawing.Point(13, 40);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(114, 19);
            this.labelControl13.TabIndex = 32;
            this.labelControl13.Text = "Giờ nhận phòng";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Location = new System.Drawing.Point(13, 72);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(98, 19);
            this.labelControl14.TabIndex = 34;
            this.labelControl14.Text = "Giờ trả phòng";
            // 
            // timeKS_GIONHANPHONG
            // 
            this.timeKS_GIONHANPHONG.EditValue = new System.DateTime(2014, 7, 15, 0, 0, 0, 0);
            this.timeKS_GIONHANPHONG.EnterMoveNextControl = true;
            this.timeKS_GIONHANPHONG.Location = new System.Drawing.Point(173, 37);
            this.timeKS_GIONHANPHONG.Name = "timeKS_GIONHANPHONG";
            this.timeKS_GIONHANPHONG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeKS_GIONHANPHONG.Properties.Appearance.Options.UseFont = true;
            this.timeKS_GIONHANPHONG.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.timeKS_GIONHANPHONG.Properties.DisplayFormat.FormatString = "HH:mm:ss";
            this.timeKS_GIONHANPHONG.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeKS_GIONHANPHONG.Properties.EditFormat.FormatString = "HH:mm:ss";
            this.timeKS_GIONHANPHONG.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeKS_GIONHANPHONG.Properties.Mask.EditMask = "HH:mm:ss";
            this.timeKS_GIONHANPHONG.Size = new System.Drawing.Size(248, 26);
            this.timeKS_GIONHANPHONG.TabIndex = 35;
            // 
            // timeKS_GIOTRAPHONG
            // 
            this.timeKS_GIOTRAPHONG.EditValue = new System.DateTime(2014, 7, 15, 0, 0, 0, 0);
            this.timeKS_GIOTRAPHONG.EnterMoveNextControl = true;
            this.timeKS_GIOTRAPHONG.Location = new System.Drawing.Point(173, 69);
            this.timeKS_GIOTRAPHONG.Name = "timeKS_GIOTRAPHONG";
            this.timeKS_GIOTRAPHONG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeKS_GIOTRAPHONG.Properties.Appearance.Options.UseFont = true;
            this.timeKS_GIOTRAPHONG.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.timeKS_GIOTRAPHONG.Properties.DisplayFormat.FormatString = "HH:mm:ss";
            this.timeKS_GIOTRAPHONG.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeKS_GIOTRAPHONG.Properties.EditFormat.FormatString = "HH:mm:ss";
            this.timeKS_GIOTRAPHONG.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeKS_GIOTRAPHONG.Properties.Mask.EditMask = "HH:mm:ss";
            this.timeKS_GIOTRAPHONG.Size = new System.Drawing.Size(248, 26);
            this.timeKS_GIOTRAPHONG.TabIndex = 36;
            // 
            // timeKS_GIOGIADEMTU
            // 
            this.timeKS_GIOGIADEMTU.EditValue = new System.DateTime(2014, 7, 15, 0, 0, 0, 0);
            this.timeKS_GIOGIADEMTU.EnterMoveNextControl = true;
            this.timeKS_GIOGIADEMTU.Location = new System.Drawing.Point(173, 134);
            this.timeKS_GIOGIADEMTU.Name = "timeKS_GIOGIADEMTU";
            this.timeKS_GIOGIADEMTU.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeKS_GIOGIADEMTU.Properties.Appearance.Options.UseFont = true;
            this.timeKS_GIOGIADEMTU.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.timeKS_GIOGIADEMTU.Properties.DisplayFormat.FormatString = "HH:mm:ss";
            this.timeKS_GIOGIADEMTU.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeKS_GIOGIADEMTU.Properties.EditFormat.FormatString = "HH:mm:ss";
            this.timeKS_GIOGIADEMTU.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeKS_GIOGIADEMTU.Properties.Mask.EditMask = "HH:mm:ss";
            this.timeKS_GIOGIADEMTU.Size = new System.Drawing.Size(248, 26);
            this.timeKS_GIOGIADEMTU.TabIndex = 38;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Location = new System.Drawing.Point(13, 137);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(152, 19);
            this.labelControl15.TabIndex = 37;
            this.labelControl15.Text = "Khách nhận phòng từ";
            // 
            // timeKS_GIOGIADEMDEN
            // 
            this.timeKS_GIOGIADEMDEN.EditValue = new System.DateTime(2014, 7, 15, 0, 0, 0, 0);
            this.timeKS_GIOGIADEMDEN.EnterMoveNextControl = true;
            this.timeKS_GIOGIADEMDEN.Location = new System.Drawing.Point(173, 166);
            this.timeKS_GIOGIADEMDEN.Name = "timeKS_GIOGIADEMDEN";
            this.timeKS_GIOGIADEMDEN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeKS_GIOGIADEMDEN.Properties.Appearance.Options.UseFont = true;
            this.timeKS_GIOGIADEMDEN.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.timeKS_GIOGIADEMDEN.Properties.DisplayFormat.FormatString = "HH:mm:ss";
            this.timeKS_GIOGIADEMDEN.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeKS_GIOGIADEMDEN.Properties.EditFormat.FormatString = "HH:mm:ss";
            this.timeKS_GIOGIADEMDEN.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeKS_GIOGIADEMDEN.Properties.Mask.EditMask = "HH:mm:ss";
            this.timeKS_GIOGIADEMDEN.Size = new System.Drawing.Size(248, 26);
            this.timeKS_GIOGIADEMDEN.TabIndex = 40;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Location = new System.Drawing.Point(13, 169);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(134, 19);
            this.labelControl16.TabIndex = 39;
            this.labelControl16.Text = "Và trả phòng trước";
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Location = new System.Drawing.Point(13, 215);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(153, 19);
            this.labelControl17.TabIndex = 41;
            this.labelControl17.Text = "Làm tròn giờ nếu quá";
            // 
            // txtKS_LAMTRONGIORA
            // 
            this.txtKS_LAMTRONGIORA.EditValue = "";
            this.txtKS_LAMTRONGIORA.Location = new System.Drawing.Point(173, 212);
            this.txtKS_LAMTRONGIORA.Name = "txtKS_LAMTRONGIORA";
            this.txtKS_LAMTRONGIORA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKS_LAMTRONGIORA.Properties.Appearance.Options.UseFont = true;
            this.txtKS_LAMTRONGIORA.Properties.DisplayFormat.FormatString = "d";
            this.txtKS_LAMTRONGIORA.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtKS_LAMTRONGIORA.Properties.EditFormat.FormatString = "d";
            this.txtKS_LAMTRONGIORA.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtKS_LAMTRONGIORA.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.txtKS_LAMTRONGIORA.Properties.Mask.EditMask = "d";
            this.txtKS_LAMTRONGIORA.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtKS_LAMTRONGIORA.Size = new System.Drawing.Size(248, 26);
            this.txtKS_LAMTRONGIORA.TabIndex = 42;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(265, 102);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(156, 19);
            this.labelControl2.TabIndex = 43;
            this.labelControl2.Text = "Tính giá qua đêm nếu";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(428, 169);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(63, 19);
            this.labelControl3.TabIndex = 44;
            this.labelControl3.Text = "Hôm sau";
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(184, 245);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(110, 35);
            this.btnDong.TabIndex = 5;
            this.btnDong.Text = "&2.Đóng";
            this.btnDong.Visible = false;
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // Frm_CauhinhBanggiaKhachsan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(917, 525);
            this.ControlBox = false;
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl17);
            this.Controls.Add(this.timeKS_GIOGIADEMDEN);
            this.Controls.Add(this.labelControl16);
            this.Controls.Add(this.timeKS_GIOGIADEMTU);
            this.Controls.Add(this.labelControl15);
            this.Controls.Add(this.timeKS_GIOTRAPHONG);
            this.Controls.Add(this.timeKS_GIONHANPHONG);
            this.Controls.Add(this.labelControl14);
            this.Controls.Add(this.labelControl13);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.btnLuu);
            this.Controls.Add(this.txtKS_LAMTRONGIORA);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_CauhinhBanggiaKhachsan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cài đặt thời gian";
            ((System.ComponentModel.ISupportInitialize)(this.timeKS_GIONHANPHONG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeKS_GIOTRAPHONG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeKS_GIOGIADEMTU.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeKS_GIOGIADEMDEN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKS_LAMTRONGIORA.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TimeEdit timeKS_GIONHANPHONG;
        private DevExpress.XtraEditors.TimeEdit timeKS_GIOTRAPHONG;
        private DevExpress.XtraEditors.TimeEdit timeKS_GIOGIADEMTU;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TimeEdit timeKS_GIOGIADEMDEN;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txtKS_LAMTRONGIORA;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton btnDong;




    }
}