﻿namespace KP_RES 
{
    partial class Frm_DSHoaDon_Khachsan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_DSHoaDon_Khachsan));
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HOADON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TenNV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYTAO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CHECKIN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CHECKOUT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIENPHONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DICHVU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MONEYCASH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MONEYVOUCHER = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MONEYVISA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MONEYINNERCARD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.KHACHHANG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuatfile = new DevExpress.XtraEditors.SimpleButton();
            this.btnprint = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnXemtruockhiin = new DevExpress.XtraEditors.SimpleButton();
            this.btnChitiet = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.btnTimkiem = new DevExpress.XtraEditors.SimpleButton();
            this.lblData = new DevExpress.XtraEditors.LabelControl();
            this.cboDATA = new DevExpress.XtraEditors.LookUpEdit();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.optGroup = new DevExpress.XtraEditors.RadioGroup();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.dtpTungay = new DevExpress.XtraEditors.DateEdit();
            this.lb_From = new DevExpress.XtraEditors.LabelControl();
            this.dtpDenngay = new DevExpress.XtraEditors.DateEdit();
            this.lb_To = new DevExpress.XtraEditors.LabelControl();
            this.btnThunho = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboDATA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.gridControl1);
            this.panelControl5.Controls.Add(this.panelControl2);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(219, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(801, 733);
            this.panelControl5.TabIndex = 1;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(758, 729);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MA_HOADON,
            this.TenNV,
            this.NGAYTAO,
            this.CHECKIN,
            this.CHECKOUT,
            this.GHICHU1,
            this.TIENPHONG,
            this.DICHVU,
            this.TONGTIEN,
            this.MONEYCASH,
            this.MONEYVOUCHER,
            this.MONEYVISA,
            this.MONEYINNERCARD,
            this.KHACHHANG,
            this.FILL});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.ShowCloseButton = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // MA_HOADON
            // 
            this.MA_HOADON.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_HOADON.AppearanceCell.Options.UseFont = true;
            this.MA_HOADON.AppearanceCell.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_HOADON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_HOADON.AppearanceHeader.Options.UseFont = true;
            this.MA_HOADON.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_HOADON.Caption = "Mã hóa đơn";
            this.MA_HOADON.FieldName = "MA_HOADON";
            this.MA_HOADON.Name = "MA_HOADON";
            this.MA_HOADON.OptionsColumn.AllowEdit = false;
            this.MA_HOADON.OptionsColumn.AllowFocus = false;
            this.MA_HOADON.OptionsColumn.FixedWidth = true;
            this.MA_HOADON.Visible = true;
            this.MA_HOADON.VisibleIndex = 1;
            this.MA_HOADON.Width = 160;
            // 
            // TenNV
            // 
            this.TenNV.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TenNV.AppearanceCell.Options.UseFont = true;
            this.TenNV.AppearanceCell.Options.UseTextOptions = true;
            this.TenNV.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TenNV.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TenNV.AppearanceHeader.Options.UseFont = true;
            this.TenNV.AppearanceHeader.Options.UseTextOptions = true;
            this.TenNV.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TenNV.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TenNV.Caption = "Nhân viên bán";
            this.TenNV.FieldName = "TenNV";
            this.TenNV.Name = "TenNV";
            this.TenNV.OptionsColumn.AllowEdit = false;
            this.TenNV.OptionsColumn.AllowFocus = false;
            this.TenNV.OptionsColumn.FixedWidth = true;
            this.TenNV.Visible = true;
            this.TenNV.VisibleIndex = 2;
            this.TenNV.Width = 160;
            // 
            // NGAYTAO
            // 
            this.NGAYTAO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYTAO.AppearanceCell.Options.UseFont = true;
            this.NGAYTAO.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYTAO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYTAO.AppearanceHeader.Options.UseFont = true;
            this.NGAYTAO.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYTAO.Caption = "Ngày bán";
            this.NGAYTAO.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.NGAYTAO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYTAO.FieldName = "NGAYTAO";
            this.NGAYTAO.Name = "NGAYTAO";
            this.NGAYTAO.OptionsColumn.AllowEdit = false;
            this.NGAYTAO.OptionsColumn.AllowFocus = false;
            this.NGAYTAO.OptionsColumn.FixedWidth = true;
            this.NGAYTAO.Width = 150;
            // 
            // CHECKIN
            // 
            this.CHECKIN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.CHECKIN.AppearanceCell.Options.UseFont = true;
            this.CHECKIN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CHECKIN.AppearanceHeader.Options.UseFont = true;
            this.CHECKIN.AppearanceHeader.Options.UseTextOptions = true;
            this.CHECKIN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHECKIN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHECKIN.Caption = "Nhận phòng";
            this.CHECKIN.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.CHECKIN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.CHECKIN.FieldName = "CHECKIN";
            this.CHECKIN.Name = "CHECKIN";
            this.CHECKIN.OptionsColumn.AllowEdit = false;
            this.CHECKIN.OptionsColumn.AllowFocus = false;
            this.CHECKIN.OptionsColumn.FixedWidth = true;
            this.CHECKIN.Visible = true;
            this.CHECKIN.VisibleIndex = 3;
            this.CHECKIN.Width = 150;
            // 
            // CHECKOUT
            // 
            this.CHECKOUT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.CHECKOUT.AppearanceCell.Options.UseFont = true;
            this.CHECKOUT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CHECKOUT.AppearanceHeader.Options.UseFont = true;
            this.CHECKOUT.AppearanceHeader.Options.UseTextOptions = true;
            this.CHECKOUT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHECKOUT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHECKOUT.Caption = "Trả phòng";
            this.CHECKOUT.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.CHECKOUT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.CHECKOUT.FieldName = "CHECKOUT";
            this.CHECKOUT.Name = "CHECKOUT";
            this.CHECKOUT.OptionsColumn.AllowEdit = false;
            this.CHECKOUT.OptionsColumn.AllowFocus = false;
            this.CHECKOUT.OptionsColumn.FixedWidth = true;
            this.CHECKOUT.Visible = true;
            this.CHECKOUT.VisibleIndex = 4;
            this.CHECKOUT.Width = 150;
            // 
            // GHICHU1
            // 
            this.GHICHU1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.GHICHU1.AppearanceCell.Options.UseFont = true;
            this.GHICHU1.AppearanceCell.Options.UseTextOptions = true;
            this.GHICHU1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GHICHU1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GHICHU1.AppearanceHeader.Options.UseFont = true;
            this.GHICHU1.AppearanceHeader.Options.UseTextOptions = true;
            this.GHICHU1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GHICHU1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GHICHU1.Caption = "Ghi chú";
            this.GHICHU1.FieldName = "GHICHU1";
            this.GHICHU1.Name = "GHICHU1";
            this.GHICHU1.OptionsColumn.AllowEdit = false;
            this.GHICHU1.OptionsColumn.AllowFocus = false;
            this.GHICHU1.OptionsColumn.FixedWidth = true;
            this.GHICHU1.Visible = true;
            this.GHICHU1.VisibleIndex = 5;
            this.GHICHU1.Width = 600;
            // 
            // TIENPHONG
            // 
            this.TIENPHONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TIENPHONG.AppearanceCell.Options.UseFont = true;
            this.TIENPHONG.AppearanceCell.Options.UseTextOptions = true;
            this.TIENPHONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIENPHONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIENPHONG.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TIENPHONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIENPHONG.AppearanceHeader.Options.UseFont = true;
            this.TIENPHONG.AppearanceHeader.Options.UseTextOptions = true;
            this.TIENPHONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIENPHONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIENPHONG.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TIENPHONG.Caption = "Tiền phòng";
            this.TIENPHONG.DisplayFormat.FormatString = "{0:#,###0}";
            this.TIENPHONG.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TIENPHONG.FieldName = "TONGTIENPHONG";
            this.TIENPHONG.Name = "TIENPHONG";
            this.TIENPHONG.OptionsColumn.AllowEdit = false;
            this.TIENPHONG.OptionsColumn.AllowFocus = false;
            this.TIENPHONG.OptionsColumn.FixedWidth = true;
            this.TIENPHONG.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TONGTIENPHONG", "{0:#,###0.00}")});
            this.TIENPHONG.Visible = true;
            this.TIENPHONG.VisibleIndex = 6;
            this.TIENPHONG.Width = 160;
            // 
            // DICHVU
            // 
            this.DICHVU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.DICHVU.AppearanceCell.Options.UseFont = true;
            this.DICHVU.AppearanceCell.Options.UseTextOptions = true;
            this.DICHVU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.DICHVU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DICHVU.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DICHVU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DICHVU.AppearanceHeader.Options.UseFont = true;
            this.DICHVU.AppearanceHeader.Options.UseTextOptions = true;
            this.DICHVU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DICHVU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DICHVU.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DICHVU.Caption = "Dịch vụ";
            this.DICHVU.DisplayFormat.FormatString = "{0:#,###0}";
            this.DICHVU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.DICHVU.FieldName = "DICHVU";
            this.DICHVU.Name = "DICHVU";
            this.DICHVU.OptionsColumn.AllowEdit = false;
            this.DICHVU.OptionsColumn.AllowFocus = false;
            this.DICHVU.OptionsColumn.FixedWidth = true;
            this.DICHVU.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DICHVU", "{0:#,###0.00}")});
            this.DICHVU.Visible = true;
            this.DICHVU.VisibleIndex = 7;
            this.DICHVU.Width = 150;
            // 
            // TONGTIEN
            // 
            this.TONGTIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TONGTIEN.AppearanceCell.Options.UseFont = true;
            this.TONGTIEN.AppearanceCell.Options.UseTextOptions = true;
            this.TONGTIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TONGTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TONGTIEN.AppearanceHeader.Options.UseFont = true;
            this.TONGTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGTIEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TONGTIEN.Caption = "Tổng tiền";
            this.TONGTIEN.DisplayFormat.FormatString = "{0:#,###0}";
            this.TONGTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TONGTIEN.FieldName = "TONGTIEN";
            this.TONGTIEN.Name = "TONGTIEN";
            this.TONGTIEN.OptionsColumn.AllowEdit = false;
            this.TONGTIEN.OptionsColumn.AllowFocus = false;
            this.TONGTIEN.OptionsColumn.FixedWidth = true;
            this.TONGTIEN.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TONGTIEN", "{0:#,###0.00}")});
            this.TONGTIEN.Visible = true;
            this.TONGTIEN.VisibleIndex = 8;
            this.TONGTIEN.Width = 160;
            // 
            // MONEYCASH
            // 
            this.MONEYCASH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MONEYCASH.AppearanceCell.Options.UseFont = true;
            this.MONEYCASH.AppearanceCell.Options.UseTextOptions = true;
            this.MONEYCASH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.MONEYCASH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MONEYCASH.AppearanceHeader.Options.UseFont = true;
            this.MONEYCASH.AppearanceHeader.Options.UseTextOptions = true;
            this.MONEYCASH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MONEYCASH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MONEYCASH.Caption = "Tiền mặt";
            this.MONEYCASH.DisplayFormat.FormatString = "N0";
            this.MONEYCASH.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.MONEYCASH.FieldName = "MONEYCASH";
            this.MONEYCASH.Name = "MONEYCASH";
            this.MONEYCASH.OptionsColumn.AllowEdit = false;
            this.MONEYCASH.OptionsColumn.AllowFocus = false;
            this.MONEYCASH.OptionsColumn.FixedWidth = true;
            this.MONEYCASH.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MONEYCASH", "{0:#,###0.00}")});
            this.MONEYCASH.Visible = true;
            this.MONEYCASH.VisibleIndex = 9;
            this.MONEYCASH.Width = 150;
            // 
            // MONEYVOUCHER
            // 
            this.MONEYVOUCHER.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MONEYVOUCHER.AppearanceCell.Options.UseFont = true;
            this.MONEYVOUCHER.AppearanceCell.Options.UseTextOptions = true;
            this.MONEYVOUCHER.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.MONEYVOUCHER.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MONEYVOUCHER.AppearanceHeader.Options.UseFont = true;
            this.MONEYVOUCHER.AppearanceHeader.Options.UseTextOptions = true;
            this.MONEYVOUCHER.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MONEYVOUCHER.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MONEYVOUCHER.Caption = "Tiền Voucher";
            this.MONEYVOUCHER.DisplayFormat.FormatString = "N0";
            this.MONEYVOUCHER.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.MONEYVOUCHER.FieldName = "MONEYVOUCHER";
            this.MONEYVOUCHER.Name = "MONEYVOUCHER";
            this.MONEYVOUCHER.OptionsColumn.AllowEdit = false;
            this.MONEYVOUCHER.OptionsColumn.AllowFocus = false;
            this.MONEYVOUCHER.OptionsColumn.FixedWidth = true;
            this.MONEYVOUCHER.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MONEYVOUCHER", "{0:#,###0.00}")});
            this.MONEYVOUCHER.Visible = true;
            this.MONEYVOUCHER.VisibleIndex = 10;
            this.MONEYVOUCHER.Width = 150;
            // 
            // MONEYVISA
            // 
            this.MONEYVISA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MONEYVISA.AppearanceCell.Options.UseFont = true;
            this.MONEYVISA.AppearanceCell.Options.UseTextOptions = true;
            this.MONEYVISA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.MONEYVISA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MONEYVISA.AppearanceHeader.Options.UseFont = true;
            this.MONEYVISA.AppearanceHeader.Options.UseTextOptions = true;
            this.MONEYVISA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MONEYVISA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MONEYVISA.Caption = "Tiền Visa";
            this.MONEYVISA.DisplayFormat.FormatString = "N0";
            this.MONEYVISA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.MONEYVISA.FieldName = "MONEYVISA";
            this.MONEYVISA.Name = "MONEYVISA";
            this.MONEYVISA.OptionsColumn.AllowEdit = false;
            this.MONEYVISA.OptionsColumn.AllowFocus = false;
            this.MONEYVISA.OptionsColumn.FixedWidth = true;
            this.MONEYVISA.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MONEYVISA", "{0:#,###0.00}")});
            this.MONEYVISA.Visible = true;
            this.MONEYVISA.VisibleIndex = 11;
            this.MONEYVISA.Width = 150;
            // 
            // MONEYINNERCARD
            // 
            this.MONEYINNERCARD.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MONEYINNERCARD.AppearanceCell.Options.UseFont = true;
            this.MONEYINNERCARD.AppearanceCell.Options.UseTextOptions = true;
            this.MONEYINNERCARD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.MONEYINNERCARD.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MONEYINNERCARD.AppearanceHeader.Options.UseFont = true;
            this.MONEYINNERCARD.AppearanceHeader.Options.UseTextOptions = true;
            this.MONEYINNERCARD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MONEYINNERCARD.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MONEYINNERCARD.Caption = "Tiền TTNB";
            this.MONEYINNERCARD.DisplayFormat.FormatString = "N0";
            this.MONEYINNERCARD.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.MONEYINNERCARD.FieldName = "MONEYINNERCARD";
            this.MONEYINNERCARD.Name = "MONEYINNERCARD";
            this.MONEYINNERCARD.OptionsColumn.AllowEdit = false;
            this.MONEYINNERCARD.OptionsColumn.AllowFocus = false;
            this.MONEYINNERCARD.OptionsColumn.FixedWidth = true;
            this.MONEYINNERCARD.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MONEYINNERCARD", "{0:#,###0.00}")});
            this.MONEYINNERCARD.Visible = true;
            this.MONEYINNERCARD.VisibleIndex = 12;
            this.MONEYINNERCARD.Width = 150;
            // 
            // KHACHHANG
            // 
            this.KHACHHANG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.KHACHHANG.AppearanceCell.Options.UseFont = true;
            this.KHACHHANG.AppearanceCell.Options.UseTextOptions = true;
            this.KHACHHANG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.KHACHHANG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.KHACHHANG.AppearanceHeader.Options.UseFont = true;
            this.KHACHHANG.AppearanceHeader.Options.UseTextOptions = true;
            this.KHACHHANG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.KHACHHANG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.KHACHHANG.Caption = "Khách hàng";
            this.KHACHHANG.FieldName = "KHACHHANG";
            this.KHACHHANG.Name = "KHACHHANG";
            this.KHACHHANG.OptionsColumn.AllowEdit = false;
            this.KHACHHANG.OptionsColumn.AllowFocus = false;
            this.KHACHHANG.OptionsColumn.FixedWidth = true;
            this.KHACHHANG.Visible = true;
            this.KHACHHANG.VisibleIndex = 13;
            this.KHACHHANG.Width = 250;
            // 
            // FILL
            // 
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 14;
            this.FILL.Width = 20;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnXemtruockhiin);
            this.panelControl2.Controls.Add(this.btnChitiet);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(760, 2);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 729);
            this.panelControl2.TabIndex = 6;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Controls.Add(this.btnXuatfile);
            this.panelControl3.Controls.Add(this.btnprint);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 162);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 505);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 321);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 24);
            this.panelControl4.TabIndex = 15;
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 241);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 14;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 161);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 15;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnXuatfile
            // 
            this.btnXuatfile.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatfile.Appearance.Options.UseFont = true;
            this.btnXuatfile.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXuatfile.Image = ((System.Drawing.Image)(resources.GetObject("btnXuatfile.Image")));
            this.btnXuatfile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXuatfile.Location = new System.Drawing.Point(0, 81);
            this.btnXuatfile.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuatfile.Name = "btnXuatfile";
            this.btnXuatfile.Size = new System.Drawing.Size(35, 80);
            this.btnXuatfile.TabIndex = 16;
            this.btnXuatfile.Text = "&Xuất File";
            this.btnXuatfile.Click += new System.EventHandler(this.btnXuatfile_Click);
            // 
            // btnprint
            // 
            this.btnprint.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnprint.Appearance.Options.UseFont = true;
            this.btnprint.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnprint.Image = ((System.Drawing.Image)(resources.GetObject("btnprint.Image")));
            this.btnprint.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnprint.Location = new System.Drawing.Point(0, 0);
            this.btnprint.Margin = new System.Windows.Forms.Padding(4);
            this.btnprint.Name = "btnprint";
            this.btnprint.Size = new System.Drawing.Size(35, 81);
            this.btnprint.TabIndex = 17;
            this.btnprint.Text = "&In";
            this.btnprint.Click += new System.EventHandler(this.btnprint_Click);
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 345);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 425);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 667);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 60);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnXemtruockhiin
            // 
            this.btnXemtruockhiin.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemtruockhiin.Appearance.Options.UseFont = true;
            this.btnXemtruockhiin.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnXemtruockhiin.Image = global::KP_RES.Properties.Resources._3d_printer_26;
            this.btnXemtruockhiin.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXemtruockhiin.Location = new System.Drawing.Point(2, 82);
            this.btnXemtruockhiin.Margin = new System.Windows.Forms.Padding(4);
            this.btnXemtruockhiin.Name = "btnXemtruockhiin";
            this.btnXemtruockhiin.Size = new System.Drawing.Size(35, 80);
            this.btnXemtruockhiin.TabIndex = 15;
            this.btnXemtruockhiin.Text = "&In";
            this.btnXemtruockhiin.Click += new System.EventHandler(this.btnXemtruockhiin_Click);
            // 
            // btnChitiet
            // 
            this.btnChitiet.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChitiet.Appearance.Options.UseFont = true;
            this.btnChitiet.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnChitiet.Image = global::KP_RES.Properties.Resources.View_Details_26;
            this.btnChitiet.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnChitiet.Location = new System.Drawing.Point(2, 2);
            this.btnChitiet.Margin = new System.Windows.Forms.Padding(4);
            this.btnChitiet.Name = "btnChitiet";
            this.btnChitiet.Size = new System.Drawing.Size(35, 80);
            this.btnChitiet.TabIndex = 13;
            this.btnChitiet.Text = "&In";
            this.btnChitiet.Click += new System.EventHandler(this.btnChitiet_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl6);
            this.panelControl1.Controls.Add(this.panelControl7);
            this.panelControl1.Controls.Add(this.panelControl8);
            this.panelControl1.Controls.Add(this.btnThunho);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(219, 733);
            this.panelControl1.TabIndex = 7;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.panelControl9);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(2, 389);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(215, 307);
            this.panelControl6.TabIndex = 2;
            // 
            // panelControl9
            // 
            this.panelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl9.Controls.Add(this.btnTimkiem);
            this.panelControl9.Controls.Add(this.lblData);
            this.panelControl9.Controls.Add(this.cboDATA);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl9.Location = new System.Drawing.Point(2, 2);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(211, 114);
            this.panelControl9.TabIndex = 7;
            // 
            // btnTimkiem
            // 
            this.btnTimkiem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimkiem.Appearance.Options.UseFont = true;
            this.btnTimkiem.Image = ((System.Drawing.Image)(resources.GetObject("btnTimkiem.Image")));
            this.btnTimkiem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnTimkiem.Location = new System.Drawing.Point(129, 66);
            this.btnTimkiem.Margin = new System.Windows.Forms.Padding(4);
            this.btnTimkiem.Name = "btnTimkiem";
            this.btnTimkiem.Size = new System.Drawing.Size(80, 35);
            this.btnTimkiem.TabIndex = 2;
            this.btnTimkiem.Text = "&Xem";
            this.btnTimkiem.Click += new System.EventHandler(this.btnTimkiem_Click);
            // 
            // lblData
            // 
            this.lblData.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblData.Location = new System.Drawing.Point(3, 5);
            this.lblData.Margin = new System.Windows.Forms.Padding(4);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(68, 19);
            this.lblData.TabIndex = 0;
            this.lblData.Text = "Cửa hàng";
            // 
            // cboDATA
            // 
            this.cboDATA.EnterMoveNextControl = true;
            this.cboDATA.Location = new System.Drawing.Point(3, 33);
            this.cboDATA.Name = "cboDATA";
            this.cboDATA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDATA.Properties.Appearance.Options.UseFont = true;
            this.cboDATA.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDATA.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboDATA.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboDATA.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboDATA.Properties.DisplayMember = "TEN";
            this.cboDATA.Properties.DropDownItemHeight = 25;
            this.cboDATA.Properties.DropDownRows = 30;
            this.cboDATA.Properties.NullText = "";
            this.cboDATA.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboDATA.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.cboDATA.Properties.ShowHeader = false;
            this.cboDATA.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboDATA.Properties.ValueMember = "MA";
            this.cboDATA.Size = new System.Drawing.Size(205, 26);
            this.cboDATA.TabIndex = 1;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.optGroup);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(2, 129);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(215, 260);
            this.panelControl7.TabIndex = 1;
            // 
            // optGroup
            // 
            this.optGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.optGroup.EditValue = true;
            this.optGroup.EnterMoveNextControl = true;
            this.optGroup.Location = new System.Drawing.Point(2, 2);
            this.optGroup.Name = "optGroup";
            this.optGroup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.optGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optGroup.Properties.Appearance.Options.UseBackColor = true;
            this.optGroup.Properties.Appearance.Options.UseFont = true;
            this.optGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.optGroup.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Theo cửa hàng"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Theo kho"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Theo quầy"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Theo nhóm hàng"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Theo hàng hóa"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Theo nhân viên"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Hóa đơn giảm giá")});
            this.optGroup.Size = new System.Drawing.Size(211, 254);
            this.optGroup.TabIndex = 0;
            this.optGroup.SelectedIndexChanged += new System.EventHandler(this.optGroup_SelectedIndexChanged);
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.dtpTungay);
            this.panelControl8.Controls.Add(this.lb_From);
            this.panelControl8.Controls.Add(this.dtpDenngay);
            this.panelControl8.Controls.Add(this.lb_To);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl8.Location = new System.Drawing.Point(2, 2);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(215, 127);
            this.panelControl8.TabIndex = 0;
            // 
            // dtpTungay
            // 
            this.dtpTungay.EditValue = null;
            this.dtpTungay.EnterMoveNextControl = true;
            this.dtpTungay.Location = new System.Drawing.Point(6, 32);
            this.dtpTungay.Name = "dtpTungay";
            this.dtpTungay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTungay.Properties.Appearance.Options.UseFont = true;
            this.dtpTungay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTungay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpTungay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpTungay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpTungay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpTungay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpTungay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpTungay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpTungay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpTungay.Size = new System.Drawing.Size(205, 26);
            this.dtpTungay.TabIndex = 1;
            // 
            // lb_From
            // 
            this.lb_From.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_From.Location = new System.Drawing.Point(6, 6);
            this.lb_From.Margin = new System.Windows.Forms.Padding(4);
            this.lb_From.Name = "lb_From";
            this.lb_From.Size = new System.Drawing.Size(59, 19);
            this.lb_From.TabIndex = 0;
            this.lb_From.Text = "Từ ngày";
            // 
            // dtpDenngay
            // 
            this.dtpDenngay.EditValue = null;
            this.dtpDenngay.EnterMoveNextControl = true;
            this.dtpDenngay.Location = new System.Drawing.Point(6, 91);
            this.dtpDenngay.Name = "dtpDenngay";
            this.dtpDenngay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDenngay.Properties.Appearance.Options.UseFont = true;
            this.dtpDenngay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDenngay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpDenngay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpDenngay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpDenngay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpDenngay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpDenngay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpDenngay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpDenngay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpDenngay.Size = new System.Drawing.Size(205, 26);
            this.dtpDenngay.TabIndex = 3;
            // 
            // lb_To
            // 
            this.lb_To.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_To.Location = new System.Drawing.Point(6, 65);
            this.lb_To.Margin = new System.Windows.Forms.Padding(4);
            this.lb_To.Name = "lb_To";
            this.lb_To.Size = new System.Drawing.Size(67, 19);
            this.lb_To.TabIndex = 2;
            this.lb_To.Text = "Đến ngày";
            // 
            // btnThunho
            // 
            this.btnThunho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThunho.Appearance.Options.UseFont = true;
            this.btnThunho.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnThunho.Image = ((System.Drawing.Image)(resources.GetObject("btnThunho.Image")));
            this.btnThunho.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnThunho.Location = new System.Drawing.Point(2, 696);
            this.btnThunho.Margin = new System.Windows.Forms.Padding(4);
            this.btnThunho.Name = "btnThunho";
            this.btnThunho.Size = new System.Drawing.Size(215, 35);
            this.btnThunho.TabIndex = 5;
            this.btnThunho.Click += new System.EventHandler(this.btnThunho_Click);
            // 
            // Frm_DSHoaDon_Khachsan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 733);
            this.Controls.Add(this.panelControl5);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_DSHoaDon_Khachsan";
            this.Text = "Danh sách hóa đơn";
            this.Load += new System.EventHandler(this.Frm_DSHoaDon_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.panelControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboDATA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            this.panelControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTungay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDenngay.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HOADON;
        private DevExpress.XtraGrid.Columns.GridColumn TONGTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn TenNV;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYTAO;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnChitiet;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraEditors.SimpleButton btnXemtruockhiin;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton btnTimkiem;
        private DevExpress.XtraEditors.LabelControl lblData;
        private DevExpress.XtraEditors.LookUpEdit cboDATA;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.RadioGroup optGroup;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.DateEdit dtpTungay;
        private DevExpress.XtraEditors.LabelControl lb_From;
        private DevExpress.XtraEditors.DateEdit dtpDenngay;
        private DevExpress.XtraEditors.LabelControl lb_To;
        private DevExpress.XtraEditors.SimpleButton btnThunho;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnprint;
        private DevExpress.XtraEditors.SimpleButton btnXuatfile;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraGrid.Columns.GridColumn KHACHHANG;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU1;
        private DevExpress.XtraGrid.Columns.GridColumn MONEYVISA;
        private DevExpress.XtraGrid.Columns.GridColumn MONEYCASH;
        private DevExpress.XtraGrid.Columns.GridColumn MONEYINNERCARD;
        private DevExpress.XtraGrid.Columns.GridColumn MONEYVOUCHER;
        private DevExpress.XtraGrid.Columns.GridColumn CHECKIN;
        private DevExpress.XtraGrid.Columns.GridColumn CHECKOUT;
        private DevExpress.XtraGrid.Columns.GridColumn TIENPHONG;
        private DevExpress.XtraGrid.Columns.GridColumn DICHVU;




    }
}