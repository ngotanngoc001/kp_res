﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_CauhinhBanggiaKhachsan : DevExpress.XtraEditors.XtraForm
    {
        public Frm_CauhinhBanggiaKhachsan()
        {
            InitializeComponent();
            LoadDATA();
        }

        private void LoadDATA()
        {
            try
            {
                string SQL = "Select TEN,GIATRI From CAUHINH Where TEN='KS_GIONHANPHONG' or TEN='KS_GIOTRAPHONG' or TEN='KS_GIOGIADEMTU' or TEN='KS_GIOGIADEMDEN' or TEN='KS_LAMTRONGIORA'";
                DataTable dt = clsMain.ReturnDataTable(SQL);
                timeKS_GIONHANPHONG.EditValue = dt.Select("TEN='KS_GIONHANPHONG'")[0]["GIATRI"].ToString();
                timeKS_GIOTRAPHONG.EditValue = dt.Select("TEN='KS_GIOTRAPHONG'")[0]["GIATRI"].ToString();
                timeKS_GIOGIADEMTU.EditValue = dt.Select("TEN='KS_GIOGIADEMTU'")[0]["GIATRI"].ToString();
                timeKS_GIOGIADEMDEN.EditValue = dt.Select("TEN='KS_GIOGIADEMDEN'")[0]["GIATRI"].ToString();
                txtKS_LAMTRONGIORA.Text = dt.Select("TEN='KS_LAMTRONGIORA'")[0]["GIATRI"].ToString();
            }
            catch
            {
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {

            string sSQL = "";
            sSQL += "Delete CAUHINH" + "\n";
            sSQL += "Where TEN='KS_GIONHANPHONG'" + "\n";
            sSQL += "Insert into CAUHINH (TEN,GIATRI,GHICHU)" + "\n";
            sSQL += "Values ( ";
            sSQL += clsMain.SQLString("KS_GIONHANPHONG") + ",";
            sSQL += clsMain.SQLString(timeKS_GIONHANPHONG.Text) + ",";
            sSQL += clsMain.SQLStringUnicode("Giờ nhận phòng khách sạn") + ")" + "\n";

            sSQL += "Delete CAUHINH" + "\n";
            sSQL += "Where TEN='KS_GIOTRAPHONG'" + "\n";
            sSQL += "Insert into CAUHINH (TEN,GIATRI,GHICHU)" + "\n";
            sSQL += "Values ( ";
            sSQL += clsMain.SQLString("KS_GIOTRAPHONG") + ",";
            sSQL += clsMain.SQLString(timeKS_GIOTRAPHONG.Text) + ",";
            sSQL += clsMain.SQLStringUnicode("Giờ trả phòng khách sạn") + ")" + "\n";

            sSQL += "Delete CAUHINH" + "\n";
            sSQL += "Where TEN='KS_GIOGIADEMTU'" + "\n";
            sSQL += "Insert into CAUHINH (TEN,GIATRI,GHICHU)" + "\n";
            sSQL += "Values ( ";
            sSQL += clsMain.SQLString("KS_GIOGIADEMTU") + ",";
            sSQL += clsMain.SQLString(timeKS_GIOGIADEMTU.Text) + ",";
            sSQL += clsMain.SQLStringUnicode("Giờ bắt đầu tính giá đêm khách sạn") + ")" + "\n";

            sSQL += "Delete CAUHINH" + "\n";
            sSQL += "Where TEN='KS_GIOGIADEMDEN'" + "\n";
            sSQL += "Insert into CAUHINH (TEN,GIATRI,GHICHU)" + "\n";
            sSQL += "Values ( ";
            sSQL += clsMain.SQLString("KS_GIOGIADEMDEN") + ",";
            sSQL += clsMain.SQLString(timeKS_GIOGIADEMDEN.Text) + ",";
            sSQL += clsMain.SQLStringUnicode("Giờ kết thúc tính giá đêm khách sạn") + ")" + "\n";

            sSQL += "Delete CAUHINH" + "\n";
            sSQL += "Where TEN='KS_LAMTRONGIORA'" + "\n";
            sSQL += "Insert into CAUHINH (TEN,GIATRI,GHICHU)" + "\n";
            sSQL += "Values ( ";
            sSQL += clsMain.SQLString("KS_LAMTRONGIORA") + ",";
            sSQL += clsMain.SQLString(txtKS_LAMTRONGIORA.Text) + ",";
            sSQL += clsMain.SQLStringUnicode("Làm tròn giờ ra") + ")" + "\n";


            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}