﻿using KP_RES;
namespace KP_RES
{
    partial class Frm_BanHang_KhachSan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_BanHang_KhachSan));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode4 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode5 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            this.pnShowHang = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.pnTable_Mon = new DevExpress.XtraEditors.XtraScrollableControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.lbPageTable = new DevExpress.XtraEditors.LabelControl();
            this.btn_NextRight = new DevExpress.XtraEditors.SimpleButton();
            this.btn_NextToLeft = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl37 = new DevExpress.XtraEditors.PanelControl();
            this.lbKhuVuc = new DevExpress.XtraEditors.LabelControl();
            this.lbNameTable = new DevExpress.XtraEditors.LabelControl();
            this.lbNameMon = new DevExpress.XtraEditors.LabelControl();
            this.lbListTable = new DevExpress.XtraEditors.LabelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pnKV_Mon = new DevExpress.XtraEditors.PanelControl();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.btn_UpKV = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl13 = new DevExpress.XtraEditors.PanelControl();
            this.btn_DowKV = new DevExpress.XtraEditors.SimpleButton();
            this.pnMain = new DevExpress.XtraEditors.PanelControl();
            this.pn_listProduct = new DevExpress.XtraEditors.PanelControl();
            this.gr_SelectProduct = new DevExpress.XtraGrid.GridControl();
            this.gv_SelectProduct = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SOTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.GIABAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Thanh_Tien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_BEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IS_COMBO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnUpDowGv = new DevExpress.XtraEditors.PanelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.lb_RowsCount = new DevExpress.XtraEditors.LabelControl();
            this.lb_STT = new DevExpress.XtraEditors.LabelControl();
            this.btn_up = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Dow = new DevExpress.XtraEditors.SimpleButton();
            this.btn_DowLast = new DevExpress.XtraEditors.SimpleButton();
            this.btn_UpFirst = new DevExpress.XtraEditors.SimpleButton();
            this.pnBtnDeleteGv = new DevExpress.XtraEditors.PanelControl();
            this.panelControl54 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl64 = new DevExpress.XtraEditors.PanelControl();
            this.btn_tru = new DevExpress.XtraEditors.SimpleButton();
            this.btn_XoaTatCa = new DevExpress.XtraEditors.SimpleButton();
            this.btn_cong = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btn_xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btn9 = new DevExpress.XtraEditors.SimpleButton();
            this.btn8 = new DevExpress.XtraEditors.SimpleButton();
            this.btn7 = new DevExpress.XtraEditors.SimpleButton();
            this.btn6 = new DevExpress.XtraEditors.SimpleButton();
            this.btn5 = new DevExpress.XtraEditors.SimpleButton();
            this.btn4 = new DevExpress.XtraEditors.SimpleButton();
            this.btn3 = new DevExpress.XtraEditors.SimpleButton();
            this.btn2 = new DevExpress.XtraEditors.SimpleButton();
            this.btn1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn0 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl65 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl66 = new DevExpress.XtraEditors.PanelControl();
            this.lbSoluong = new DevExpress.XtraEditors.LabelControl();
            this.lb_soluong = new DevExpress.XtraEditors.LabelControl();
            this.panelControl20 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl63 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl67 = new DevExpress.XtraEditors.PanelControl();
            this.lbDichvu = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.lb_clicktiengio = new DevExpress.XtraEditors.LabelControl();
            this.panelControl62 = new DevExpress.XtraEditors.PanelControl();
            this.lbTatol = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl61 = new DevExpress.XtraEditors.PanelControl();
            this.lbGiamGia = new DevExpress.XtraEditors.LabelControl();
            this.panelControl60 = new DevExpress.XtraEditors.PanelControl();
            this.lbPhuThu = new DevExpress.XtraEditors.LabelControl();
            this.labelControl51 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl59 = new DevExpress.XtraEditors.PanelControl();
            this.lbTiengio = new DevExpress.XtraEditors.LabelControl();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.lbTableTatol = new DevExpress.XtraEditors.LabelControl();
            this.pnNghiepVu_ThuNgan = new DevExpress.XtraEditors.PanelControl();
            this.pnThuNgan = new DevExpress.XtraEditors.PanelControl();
            this.btnKhachrangoai = new DevExpress.XtraEditors.SimpleButton();
            this.btnNhanphong = new DevExpress.XtraEditors.SimpleButton();
            this.btnXacNhan = new DevExpress.XtraEditors.SimpleButton();
            this.btnGiaBan2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnTachPhong = new DevExpress.XtraEditors.SimpleButton();
            this.btn_OrderSent = new DevExpress.XtraEditors.SimpleButton();
            this.btn_SoDoBan = new DevExpress.XtraEditors.SimpleButton();
            this.btn_TamTinh = new DevExpress.XtraEditors.SimpleButton();
            this.btn_GhepBan = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Payment = new DevExpress.XtraEditors.SimpleButton();
            this.btn_YeuCauThem = new DevExpress.XtraEditors.SimpleButton();
            this.btn_logout = new DevExpress.XtraEditors.SimpleButton();
            this.pnNghiepVu = new DevExpress.XtraEditors.PanelControl();
            this.btn_InlaiOrder = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ExitTable = new DevExpress.XtraEditors.SimpleButton();
            this.btn_InLaiHoaDon = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ExitDishe = new DevExpress.XtraEditors.SimpleButton();
            this.btn_DatBan = new DevExpress.XtraEditors.SimpleButton();
            this.btn_LayLaiHoaDon = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoKet = new DevExpress.XtraEditors.SimpleButton();
            this.btnKetKa = new DevExpress.XtraEditors.SimpleButton();
            this.btnChuyenBan = new DevExpress.XtraEditors.SimpleButton();
            this.pnDacThu = new DevExpress.XtraEditors.PanelControl();
            this.btnVesinh = new DevExpress.XtraEditors.SimpleButton();
            this.btnKhoaphong = new DevExpress.XtraEditors.SimpleButton();
            this.btnDanhsachkhach = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.btnDoigio_nhanphong = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.btn_DatThu = new DevExpress.XtraEditors.SimpleButton();
            this.btn_NghiepVu = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ThuNgan = new DevExpress.XtraEditors.SimpleButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pnHuymon = new DevExpress.XtraEditors.PanelControl();
            this.pn_GroupTable = new DevExpress.XtraEditors.PanelControl();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.gr_divide_Table_1 = new DevExpress.XtraGrid.GridControl();
            this.gv_divide_Table_1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._SSTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this._TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this._SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this._GIABAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this._Thanh_Tien = new DevExpress.XtraGrid.Columns.GridColumn();
            this._MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this._GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this._MA_BEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnUpDow = new DevExpress.XtraEditors.PanelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btn_OkThrough = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ToLeftAll = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ToRightAll = new DevExpress.XtraEditors.SimpleButton();
            this.btn_UpDivide = new DevExpress.XtraEditors.SimpleButton();
            this.btn_DowDivide = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ESCThroughtTable = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ToRight = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Toleft = new DevExpress.XtraEditors.SimpleButton();
            this.pn_TableTow = new DevExpress.XtraEditors.PanelControl();
            this.gr_divide_Table_2 = new DevExpress.XtraGrid.GridControl();
            this.gv_divide_Table_2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.@__STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__GIABAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__Thanh_Tien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__MA_BEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnInforTablecheck = new DevExpress.XtraEditors.PanelControl();
            this.panelControl14 = new DevExpress.XtraEditors.PanelControl();
            this.txtTenKhacHang = new DevExpress.XtraEditors.TextEdit();
            this.txtmaBanGoc = new DevExpress.XtraEditors.TextEdit();
            this.txtmaKVBanGoc = new DevExpress.XtraEditors.TextEdit();
            this.panelControl18 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.pn_TableShare = new DevExpress.XtraEditors.PanelControl();
            this.txtTenKhachHangGhep = new DevExpress.XtraEditors.TextEdit();
            this.txtmaBanGhep = new DevExpress.XtraEditors.TextEdit();
            this.txtmaKVBanGhep = new DevExpress.XtraEditors.TextEdit();
            this.pnYeucauthem = new DevExpress.XtraEditors.PanelControl();
            this.panelControl19 = new DevExpress.XtraEditors.PanelControl();
            this.pnYeuCau = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl22 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton23 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton24 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.us_keyboard3 = new KP_RES.Us_keyboard();
            this.panelControl21 = new DevExpress.XtraEditors.PanelControl();
            this.txtYeuCauThem = new DevExpress.XtraEditors.TextEdit();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl24 = new DevExpress.XtraEditors.PanelControl();
            this.lbkvYeuCauThem = new DevExpress.XtraEditors.LabelControl();
            this.lbTableYeuCauThem = new DevExpress.XtraEditors.LabelControl();
            this.lbMonYeuCauThem = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl16 = new DevExpress.XtraEditors.PanelControl();
            this.pnInlaihoadon = new DevExpress.XtraEditors.PanelControl();
            this.panelControl53 = new DevExpress.XtraEditors.PanelControl();
            this.gr_billPaymented = new DevExpress.XtraGrid.GridControl();
            this.gvBillPaymented = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SSTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HOADON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYTAO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_KHUVUC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_BAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.HUY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.LAYLAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.panelControl52 = new DevExpress.XtraEditors.PanelControl();
            this.btnDowBill = new DevExpress.XtraEditors.SimpleButton();
            this.btnDowLastBill = new DevExpress.XtraEditors.SimpleButton();
            this.btnThoatReprint = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.lbTongSoRow = new DevExpress.XtraEditors.LabelControl();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.btn_upbill = new DevExpress.XtraEditors.SimpleButton();
            this.btn_UpTopBill = new DevExpress.XtraEditors.SimpleButton();
            this.pnWapper = new DevExpress.XtraEditors.PanelControl();
            this.pnlNhanPhong = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.XOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rbtnXoa = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.STT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENKH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CMND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rcbbCMND = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.NGAYSINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIOITINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rcbbGIOITINH = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.QUOCTICH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIENTHOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIACHI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EMAIL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_KH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Themkhach = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl17 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtDaThanhToan = new DevExpress.XtraEditors.TextEdit();
            this.dtpNgayvao = new DevExpress.XtraEditors.DateEdit();
            this.dtpNgayra = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnTimkhach = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.lbKhuvuc_Nhanphong = new DevExpress.XtraEditors.LabelControl();
            this.lbPhong_Nhanphong = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl68 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl69 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.pnHeader = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lb_Quay = new DevExpress.XtraEditors.LabelControl();
            this.lb_Kho = new DevExpress.XtraEditors.LabelControl();
            this.lbgiay = new DevExpress.XtraEditors.LabelControl();
            this.lbCaBan = new DevExpress.XtraEditors.LabelControl();
            this.lbGio = new DevExpress.XtraEditors.LabelControl();
            this.lb_NhanVien = new DevExpress.XtraEditors.LabelControl();
            this.lbtimeby = new DevExpress.XtraEditors.LabelControl();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pnShowHang)).BeginInit();
            this.pnShowHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl37)).BeginInit();
            this.panelControl37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnKV_Mon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).BeginInit();
            this.panelControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnMain)).BeginInit();
            this.pnMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_listProduct)).BeginInit();
            this.pn_listProduct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_SelectProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_SelectProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDowGv)).BeginInit();
            this.pnUpDowGv.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnBtnDeleteGv)).BeginInit();
            this.pnBtnDeleteGv.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl54)).BeginInit();
            this.panelControl54.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl64)).BeginInit();
            this.panelControl64.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl65)).BeginInit();
            this.panelControl65.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl66)).BeginInit();
            this.panelControl66.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl20)).BeginInit();
            this.panelControl20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl63)).BeginInit();
            this.panelControl63.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl67)).BeginInit();
            this.panelControl67.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl62)).BeginInit();
            this.panelControl62.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl61)).BeginInit();
            this.panelControl61.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl60)).BeginInit();
            this.panelControl60.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl59)).BeginInit();
            this.panelControl59.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnNghiepVu_ThuNgan)).BeginInit();
            this.pnNghiepVu_ThuNgan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnThuNgan)).BeginInit();
            this.pnThuNgan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnNghiepVu)).BeginInit();
            this.pnNghiepVu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnDacThu)).BeginInit();
            this.pnDacThu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnHuymon)).BeginInit();
            this.pnHuymon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_GroupTable)).BeginInit();
            this.pn_GroupTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            this.panelControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_divide_Table_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_divide_Table_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDow)).BeginInit();
            this.pnUpDow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_TableTow)).BeginInit();
            this.pn_TableTow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_divide_Table_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_divide_Table_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnInforTablecheck)).BeginInit();
            this.pnInforTablecheck.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).BeginInit();
            this.panelControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenKhacHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmaBanGoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmaKVBanGoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).BeginInit();
            this.panelControl18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_TableShare)).BeginInit();
            this.pn_TableShare.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenKhachHangGhep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmaBanGhep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmaKVBanGhep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnYeucauthem)).BeginInit();
            this.pnYeucauthem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).BeginInit();
            this.panelControl19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).BeginInit();
            this.panelControl22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl21)).BeginInit();
            this.panelControl21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtYeuCauThem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl24)).BeginInit();
            this.panelControl24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnInlaihoadon)).BeginInit();
            this.pnInlaihoadon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl53)).BeginInit();
            this.panelControl53.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_billPaymented)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBillPaymented)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl52)).BeginInit();
            this.panelControl52.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnWapper)).BeginInit();
            this.pnWapper.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlNhanPhong)).BeginInit();
            this.pnlNhanPhong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbtnXoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbbCMND)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbbGIOITINH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).BeginInit();
            this.panelControl17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDaThanhToan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayvao.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayvao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayra.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl68)).BeginInit();
            this.panelControl68.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnHeader)).BeginInit();
            this.pnHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnShowHang
            // 
            this.pnShowHang.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnShowHang.Controls.Add(this.panelControl2);
            this.pnShowHang.Controls.Add(this.panelControl4);
            this.pnShowHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnShowHang.Location = new System.Drawing.Point(0, 0);
            this.pnShowHang.Name = "pnShowHang";
            this.pnShowHang.Size = new System.Drawing.Size(674, 698);
            this.pnShowHang.TabIndex = 1;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.pnTable_Mon);
            this.panelControl2.Controls.Add(this.panelControl9);
            this.panelControl2.Controls.Add(this.panelControl7);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(539, 698);
            this.panelControl2.TabIndex = 5;
            // 
            // pnTable_Mon
            // 
            this.pnTable_Mon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnTable_Mon.Location = new System.Drawing.Point(2, 47);
            this.pnTable_Mon.Name = "pnTable_Mon";
            this.pnTable_Mon.Size = new System.Drawing.Size(535, 604);
            this.pnTable_Mon.TabIndex = 26;
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.labelControl3);
            this.panelControl9.Controls.Add(this.labelControl4);
            this.panelControl9.Controls.Add(this.lbPageTable);
            this.panelControl9.Controls.Add(this.btn_NextRight);
            this.panelControl9.Controls.Add(this.btn_NextToLeft);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl9.Location = new System.Drawing.Point(2, 651);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(535, 45);
            this.panelControl9.TabIndex = 1;
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl3.Location = new System.Drawing.Point(262, 17);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(6, 13);
            this.labelControl3.TabIndex = 10;
            this.labelControl3.Text = "/";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl4.Location = new System.Drawing.Point(280, 17);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(7, 13);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "5";
            // 
            // lbPageTable
            // 
            this.lbPageTable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lbPageTable.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbPageTable.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbPageTable.Location = new System.Drawing.Point(243, 17);
            this.lbPageTable.Name = "lbPageTable";
            this.lbPageTable.Size = new System.Drawing.Size(7, 13);
            this.lbPageTable.TabIndex = 8;
            this.lbPageTable.Text = "1";
            // 
            // btn_NextRight
            // 
            this.btn_NextRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_NextRight.Image = ((System.Drawing.Image)(resources.GetObject("btn_NextRight.Image")));
            this.btn_NextRight.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_NextRight.Location = new System.Drawing.Point(402, 2);
            this.btn_NextRight.Name = "btn_NextRight";
            this.btn_NextRight.Size = new System.Drawing.Size(131, 41);
            this.btn_NextRight.TabIndex = 1;
            this.btn_NextRight.Text = "simpleButton16";
            this.btn_NextRight.Click += new System.EventHandler(this.btn_NextRight_Click);
            // 
            // btn_NextToLeft
            // 
            this.btn_NextToLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_NextToLeft.Image = ((System.Drawing.Image)(resources.GetObject("btn_NextToLeft.Image")));
            this.btn_NextToLeft.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_NextToLeft.Location = new System.Drawing.Point(2, 2);
            this.btn_NextToLeft.Name = "btn_NextToLeft";
            this.btn_NextToLeft.Size = new System.Drawing.Size(131, 41);
            this.btn_NextToLeft.TabIndex = 0;
            this.btn_NextToLeft.Text = "simpleButton15";
            this.btn_NextToLeft.Click += new System.EventHandler(this.btn_NextLeft_Click);
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.panelControl37);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(2, 2);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(535, 45);
            this.panelControl7.TabIndex = 0;
            // 
            // panelControl37
            // 
            this.panelControl37.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl37.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl37.Controls.Add(this.lbKhuVuc);
            this.panelControl37.Controls.Add(this.lbNameTable);
            this.panelControl37.Controls.Add(this.lbNameMon);
            this.panelControl37.Controls.Add(this.lbListTable);
            this.panelControl37.Location = new System.Drawing.Point(2, 12);
            this.panelControl37.Name = "panelControl37";
            this.panelControl37.Size = new System.Drawing.Size(521, 31);
            this.panelControl37.TabIndex = 0;
            // 
            // lbKhuVuc
            // 
            this.lbKhuVuc.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbKhuVuc.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbKhuVuc.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbKhuVuc.Location = new System.Drawing.Point(209, 0);
            this.lbKhuVuc.Name = "lbKhuVuc";
            this.lbKhuVuc.Size = new System.Drawing.Size(130, 19);
            this.lbKhuVuc.TabIndex = 0;
            this.lbKhuVuc.Text = "Khu Vực 1         >";
            // 
            // lbNameTable
            // 
            this.lbNameTable.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbNameTable.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbNameTable.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbNameTable.Location = new System.Drawing.Point(339, 0);
            this.lbNameTable.Name = "lbNameTable";
            this.lbNameTable.Size = new System.Drawing.Size(82, 19);
            this.lbNameTable.TabIndex = 1;
            this.lbNameTable.Text = "    Bàn 1  >";
            // 
            // lbNameMon
            // 
            this.lbNameMon.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbNameMon.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbNameMon.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbNameMon.Location = new System.Drawing.Point(421, 0);
            this.lbNameMon.Name = "lbNameMon";
            this.lbNameMon.Size = new System.Drawing.Size(100, 19);
            this.lbNameMon.TabIndex = 2;
            this.lbNameMon.Text = "     Lẩu Bò Tái";
            // 
            // lbListTable
            // 
            this.lbListTable.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbListTable.Location = new System.Drawing.Point(0, 0);
            this.lbListTable.Name = "lbListTable";
            this.lbListTable.Size = new System.Drawing.Size(93, 19);
            this.lbListTable.TabIndex = 3;
            this.lbListTable.Text = "Sơ Đồ Phòng";
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.pnKV_Mon);
            this.panelControl4.Controls.Add(this.panelControl12);
            this.panelControl4.Controls.Add(this.panelControl13);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl4.Location = new System.Drawing.Point(539, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(135, 698);
            this.panelControl4.TabIndex = 4;
            // 
            // pnKV_Mon
            // 
            this.pnKV_Mon.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnKV_Mon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnKV_Mon.Location = new System.Drawing.Point(2, 47);
            this.pnKV_Mon.Name = "pnKV_Mon";
            this.pnKV_Mon.Size = new System.Drawing.Size(131, 604);
            this.pnKV_Mon.TabIndex = 2;
            // 
            // panelControl12
            // 
            this.panelControl12.Controls.Add(this.btn_UpKV);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl12.Location = new System.Drawing.Point(2, 2);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(131, 45);
            this.panelControl12.TabIndex = 2;
            // 
            // btn_UpKV
            // 
            this.btn_UpKV.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_UpKV.Image = ((System.Drawing.Image)(resources.GetObject("btn_UpKV.Image")));
            this.btn_UpKV.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_UpKV.Location = new System.Drawing.Point(2, 2);
            this.btn_UpKV.Name = "btn_UpKV";
            this.btn_UpKV.Size = new System.Drawing.Size(127, 41);
            this.btn_UpKV.TabIndex = 0;
            this.btn_UpKV.Text = "simpleButton17";
            this.btn_UpKV.Click += new System.EventHandler(this.btn_UpKV_Click);
            // 
            // panelControl13
            // 
            this.panelControl13.Controls.Add(this.btn_DowKV);
            this.panelControl13.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl13.Location = new System.Drawing.Point(2, 651);
            this.panelControl13.Name = "panelControl13";
            this.panelControl13.Size = new System.Drawing.Size(131, 45);
            this.panelControl13.TabIndex = 3;
            // 
            // btn_DowKV
            // 
            this.btn_DowKV.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_DowKV.Image = ((System.Drawing.Image)(resources.GetObject("btn_DowKV.Image")));
            this.btn_DowKV.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_DowKV.Location = new System.Drawing.Point(2, 2);
            this.btn_DowKV.Name = "btn_DowKV";
            this.btn_DowKV.Size = new System.Drawing.Size(127, 41);
            this.btn_DowKV.TabIndex = 1;
            this.btn_DowKV.Text = "simpleButton18";
            this.btn_DowKV.Click += new System.EventHandler(this.btn_DowKV_Click);
            // 
            // pnMain
            // 
            this.pnMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnMain.Controls.Add(this.pn_listProduct);
            this.pnMain.Controls.Add(this.pnNghiepVu_ThuNgan);
            this.pnMain.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnMain.Location = new System.Drawing.Point(674, 0);
            this.pnMain.Name = "pnMain";
            this.pnMain.Size = new System.Drawing.Size(350, 698);
            this.pnMain.TabIndex = 2;
            // 
            // pn_listProduct
            // 
            this.pn_listProduct.Controls.Add(this.gr_SelectProduct);
            this.pn_listProduct.Controls.Add(this.pnUpDowGv);
            this.pn_listProduct.Controls.Add(this.pnBtnDeleteGv);
            this.pn_listProduct.Controls.Add(this.panelControl20);
            this.pn_listProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_listProduct.Location = new System.Drawing.Point(0, 0);
            this.pn_listProduct.Name = "pn_listProduct";
            this.pn_listProduct.Size = new System.Drawing.Size(350, 440);
            this.pn_listProduct.TabIndex = 0;
            // 
            // gr_SelectProduct
            // 
            this.gr_SelectProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gr_SelectProduct.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gr_SelectProduct.Location = new System.Drawing.Point(2, 2);
            this.gr_SelectProduct.MainView = this.gv_SelectProduct;
            this.gr_SelectProduct.Name = "gr_SelectProduct";
            this.gr_SelectProduct.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1});
            this.gr_SelectProduct.Size = new System.Drawing.Size(313, 193);
            this.gr_SelectProduct.TabIndex = 19;
            this.gr_SelectProduct.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv_SelectProduct});
            this.gr_SelectProduct.Leave += new System.EventHandler(this.gr_SelectProduct_Leave);
            // 
            // gv_SelectProduct
            // 
            this.gv_SelectProduct.ColumnPanelRowHeight = 30;
            this.gv_SelectProduct.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SOTT,
            this.TEN_HANGHOA,
            this.SOLUONG,
            this.GIABAN,
            this.Thanh_Tien,
            this.MA_HANGHOA,
            this.GHICHU,
            this.MA_BEP,
            this.THUE,
            this.IS_COMBO});
            this.gv_SelectProduct.GridControl = this.gr_SelectProduct;
            this.gv_SelectProduct.Name = "gv_SelectProduct";
            this.gv_SelectProduct.OptionsView.ShowGroupPanel = false;
            this.gv_SelectProduct.OptionsView.ShowIndicator = false;
            this.gv_SelectProduct.RowHeight = 30;
            this.gv_SelectProduct.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gv_SelectProduct.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gv_SelectProduct_RowCellClick);
            this.gv_SelectProduct.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gv_SelectProduct_CustomDrawCell);
            // 
            // SOTT
            // 
            this.SOTT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.SOTT.AppearanceCell.Options.UseFont = true;
            this.SOTT.AppearanceCell.Options.UseTextOptions = true;
            this.SOTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOTT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.SOTT.AppearanceHeader.Options.UseFont = true;
            this.SOTT.AppearanceHeader.Options.UseTextOptions = true;
            this.SOTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOTT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOTT.Caption = "TT";
            this.SOTT.FieldName = "SOTT";
            this.SOTT.Name = "SOTT";
            this.SOTT.OptionsColumn.AllowEdit = false;
            this.SOTT.OptionsColumn.AllowFocus = false;
            this.SOTT.Visible = true;
            this.SOTT.VisibleIndex = 0;
            this.SOTT.Width = 22;
            // 
            // TEN_HANGHOA
            // 
            this.TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_HANGHOA.Caption = "Tên Hàng Hóa";
            this.TEN_HANGHOA.FieldName = "TEN_HANGHOA";
            this.TEN_HANGHOA.Name = "TEN_HANGHOA";
            this.TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this.TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this.TEN_HANGHOA.Visible = true;
            this.TEN_HANGHOA.VisibleIndex = 1;
            this.TEN_HANGHOA.Width = 122;
            // 
            // SOLUONG
            // 
            this.SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.SOLUONG.AppearanceCell.Options.UseFont = true;
            this.SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.Caption = "SL";
            this.SOLUONG.ColumnEdit = this.repositoryItemCalcEdit1;
            this.SOLUONG.FieldName = "SOLUONG";
            this.SOLUONG.Name = "SOLUONG";
            this.SOLUONG.OptionsColumn.AllowEdit = false;
            this.SOLUONG.OptionsColumn.AllowFocus = false;
            this.SOLUONG.Visible = true;
            this.SOLUONG.VisibleIndex = 2;
            this.SOLUONG.Width = 39;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // GIABAN
            // 
            this.GIABAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GIABAN.AppearanceCell.Options.UseFont = true;
            this.GIABAN.AppearanceCell.Options.UseTextOptions = true;
            this.GIABAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIABAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.GIABAN.AppearanceHeader.Options.UseFont = true;
            this.GIABAN.AppearanceHeader.Options.UseTextOptions = true;
            this.GIABAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIABAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN.Caption = "Đơn giá";
            this.GIABAN.DisplayFormat.FormatString = "{0:#,###0}";
            this.GIABAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GIABAN.FieldName = "GIABAN";
            this.GIABAN.Name = "GIABAN";
            this.GIABAN.OptionsColumn.AllowEdit = false;
            this.GIABAN.OptionsColumn.AllowFocus = false;
            this.GIABAN.Visible = true;
            this.GIABAN.VisibleIndex = 3;
            this.GIABAN.Width = 66;
            // 
            // Thanh_Tien
            // 
            this.Thanh_Tien.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Thanh_Tien.AppearanceCell.Options.UseFont = true;
            this.Thanh_Tien.AppearanceCell.Options.UseTextOptions = true;
            this.Thanh_Tien.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Thanh_Tien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.Thanh_Tien.AppearanceHeader.Options.UseFont = true;
            this.Thanh_Tien.AppearanceHeader.Options.UseTextOptions = true;
            this.Thanh_Tien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Thanh_Tien.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Thanh_Tien.Caption = "Thành Tiền";
            this.Thanh_Tien.DisplayFormat.FormatString = "{0:#,###0}";
            this.Thanh_Tien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.Thanh_Tien.FieldName = "Thanh_Tien";
            this.Thanh_Tien.Name = "Thanh_Tien";
            this.Thanh_Tien.OptionsColumn.AllowEdit = false;
            this.Thanh_Tien.OptionsColumn.AllowFocus = false;
            this.Thanh_Tien.Visible = true;
            this.Thanh_Tien.VisibleIndex = 4;
            this.Thanh_Tien.Width = 90;
            // 
            // MA_HANGHOA
            // 
            this.MA_HANGHOA.Caption = "Mã Hàng Hóa";
            this.MA_HANGHOA.FieldName = "MA_HANGHOA";
            this.MA_HANGHOA.Name = "MA_HANGHOA";
            this.MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this.MA_HANGHOA.OptionsColumn.AllowFocus = false;
            // 
            // GHICHU
            // 
            this.GHICHU.Caption = "Ghi Chú";
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.Name = "GHICHU";
            // 
            // MA_BEP
            // 
            this.MA_BEP.Caption = "Máy In Bếp";
            this.MA_BEP.FieldName = "MA_BEP";
            this.MA_BEP.Name = "MA_BEP";
            // 
            // THUE
            // 
            this.THUE.Caption = "VAT";
            this.THUE.FieldName = "THUE";
            this.THUE.Name = "THUE";
            // 
            // IS_COMBO
            // 
            this.IS_COMBO.Caption = "IS_COMBO";
            this.IS_COMBO.FieldName = "IS_COMBO";
            this.IS_COMBO.Name = "IS_COMBO";
            // 
            // pnUpDowGv
            // 
            this.pnUpDowGv.Controls.Add(this.labelControl17);
            this.pnUpDowGv.Controls.Add(this.lb_RowsCount);
            this.pnUpDowGv.Controls.Add(this.lb_STT);
            this.pnUpDowGv.Controls.Add(this.btn_up);
            this.pnUpDowGv.Controls.Add(this.btn_Dow);
            this.pnUpDowGv.Controls.Add(this.btn_DowLast);
            this.pnUpDowGv.Controls.Add(this.btn_UpFirst);
            this.pnUpDowGv.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnUpDowGv.Location = new System.Drawing.Point(315, 2);
            this.pnUpDowGv.Name = "pnUpDowGv";
            this.pnUpDowGv.Size = new System.Drawing.Size(33, 193);
            this.pnUpDowGv.TabIndex = 17;
            // 
            // labelControl17
            // 
            this.labelControl17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl17.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl17.Location = new System.Drawing.Point(7, 80);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(20, 13);
            this.labelControl17.TabIndex = 7;
            this.labelControl17.Text = "----";
            // 
            // lb_RowsCount
            // 
            this.lb_RowsCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_RowsCount.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_RowsCount.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_RowsCount.Location = new System.Drawing.Point(9, 94);
            this.lb_RowsCount.Name = "lb_RowsCount";
            this.lb_RowsCount.Size = new System.Drawing.Size(14, 13);
            this.lb_RowsCount.TabIndex = 6;
            this.lb_RowsCount.Text = "00";
            // 
            // lb_STT
            // 
            this.lb_STT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_STT.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_STT.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_STT.Location = new System.Drawing.Point(9, 68);
            this.lb_STT.Name = "lb_STT";
            this.lb_STT.Size = new System.Drawing.Size(14, 13);
            this.lb_STT.TabIndex = 5;
            this.lb_STT.Text = "00";
            // 
            // btn_up
            // 
            this.btn_up.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_up.Image = ((System.Drawing.Image)(resources.GetObject("btn_up.Image")));
            this.btn_up.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_up.Location = new System.Drawing.Point(2, 42);
            this.btn_up.Name = "btn_up";
            this.btn_up.Size = new System.Drawing.Size(29, 40);
            this.btn_up.TabIndex = 4;
            this.btn_up.Text = "simpleButton7";
            this.btn_up.Click += new System.EventHandler(this.btn_up_Click);
            // 
            // btn_Dow
            // 
            this.btn_Dow.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_Dow.Image = ((System.Drawing.Image)(resources.GetObject("btn_Dow.Image")));
            this.btn_Dow.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_Dow.Location = new System.Drawing.Point(2, 111);
            this.btn_Dow.Name = "btn_Dow";
            this.btn_Dow.Size = new System.Drawing.Size(29, 40);
            this.btn_Dow.TabIndex = 3;
            this.btn_Dow.Click += new System.EventHandler(this.btn_Dow_Click);
            // 
            // btn_DowLast
            // 
            this.btn_DowLast.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_DowLast.Image = ((System.Drawing.Image)(resources.GetObject("btn_DowLast.Image")));
            this.btn_DowLast.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_DowLast.Location = new System.Drawing.Point(2, 151);
            this.btn_DowLast.Name = "btn_DowLast";
            this.btn_DowLast.Size = new System.Drawing.Size(29, 40);
            this.btn_DowLast.TabIndex = 2;
            this.btn_DowLast.Text = "simpleButton4";
            this.btn_DowLast.Click += new System.EventHandler(this.btn_DowLast_Click);
            // 
            // btn_UpFirst
            // 
            this.btn_UpFirst.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_UpFirst.Image = ((System.Drawing.Image)(resources.GetObject("btn_UpFirst.Image")));
            this.btn_UpFirst.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_UpFirst.Location = new System.Drawing.Point(2, 2);
            this.btn_UpFirst.Name = "btn_UpFirst";
            this.btn_UpFirst.Size = new System.Drawing.Size(29, 40);
            this.btn_UpFirst.TabIndex = 0;
            this.btn_UpFirst.Text = "simpleButton1";
            this.btn_UpFirst.Click += new System.EventHandler(this.btn_UpFirst_Click);
            // 
            // pnBtnDeleteGv
            // 
            this.pnBtnDeleteGv.Controls.Add(this.panelControl54);
            this.pnBtnDeleteGv.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBtnDeleteGv.Location = new System.Drawing.Point(2, 195);
            this.pnBtnDeleteGv.Name = "pnBtnDeleteGv";
            this.pnBtnDeleteGv.Size = new System.Drawing.Size(346, 151);
            this.pnBtnDeleteGv.TabIndex = 21;
            // 
            // panelControl54
            // 
            this.panelControl54.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl54.Controls.Add(this.panelControl64);
            this.panelControl54.Controls.Add(this.panelControl65);
            this.panelControl54.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl54.Location = new System.Drawing.Point(2, 2);
            this.panelControl54.Name = "panelControl54";
            this.panelControl54.Size = new System.Drawing.Size(342, 147);
            this.panelControl54.TabIndex = 20;
            // 
            // panelControl64
            // 
            this.panelControl64.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl64.Controls.Add(this.btn_tru);
            this.panelControl64.Controls.Add(this.btn_XoaTatCa);
            this.panelControl64.Controls.Add(this.btn_cong);
            this.panelControl64.Controls.Add(this.btnOK);
            this.panelControl64.Controls.Add(this.btn_xoa);
            this.panelControl64.Controls.Add(this.btn9);
            this.panelControl64.Controls.Add(this.btn8);
            this.panelControl64.Controls.Add(this.btn7);
            this.panelControl64.Controls.Add(this.btn6);
            this.panelControl64.Controls.Add(this.btn5);
            this.panelControl64.Controls.Add(this.btn4);
            this.panelControl64.Controls.Add(this.btn3);
            this.panelControl64.Controls.Add(this.btn2);
            this.panelControl64.Controls.Add(this.btn1);
            this.panelControl64.Controls.Add(this.btn0);
            this.panelControl64.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl64.Location = new System.Drawing.Point(0, 24);
            this.panelControl64.Name = "panelControl64";
            this.panelControl64.Size = new System.Drawing.Size(342, 123);
            this.panelControl64.TabIndex = 22;
            // 
            // btn_tru
            // 
            this.btn_tru.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn_tru.Appearance.Options.UseFont = true;
            this.btn_tru.Location = new System.Drawing.Point(189, 43);
            this.btn_tru.Name = "btn_tru";
            this.btn_tru.Size = new System.Drawing.Size(55, 36);
            this.btn_tru.TabIndex = 1;
            this.btn_tru.Text = "-";
            this.btn_tru.Click += new System.EventHandler(this.btn_tru_Click);
            // 
            // btn_XoaTatCa
            // 
            this.btn_XoaTatCa.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_XoaTatCa.Appearance.Options.UseFont = true;
            this.btn_XoaTatCa.Location = new System.Drawing.Point(250, 83);
            this.btn_XoaTatCa.Name = "btn_XoaTatCa";
            this.btn_XoaTatCa.Size = new System.Drawing.Size(86, 36);
            this.btn_XoaTatCa.TabIndex = 0;
            this.btn_XoaTatCa.Text = "Xóa tất cả";
            this.btn_XoaTatCa.Click += new System.EventHandler(this.btn_XoaTatCa_Click);
            // 
            // btn_cong
            // 
            this.btn_cong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn_cong.Appearance.Options.UseFont = true;
            this.btn_cong.Location = new System.Drawing.Point(189, 3);
            this.btn_cong.Name = "btn_cong";
            this.btn_cong.Size = new System.Drawing.Size(55, 36);
            this.btn_cong.TabIndex = 3;
            this.btn_cong.Text = "+";
            this.btn_cong.Click += new System.EventHandler(this.btn_cong_Click);
            // 
            // btnOK
            // 
            this.btnOK.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnOK.Appearance.Options.UseFont = true;
            this.btnOK.Location = new System.Drawing.Point(250, 3);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(86, 36);
            this.btnOK.TabIndex = 26;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btn_xoa
            // 
            this.btn_xoa.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_xoa.Appearance.Options.UseFont = true;
            this.btn_xoa.Location = new System.Drawing.Point(250, 43);
            this.btn_xoa.Name = "btn_xoa";
            this.btn_xoa.Size = new System.Drawing.Size(86, 36);
            this.btn_xoa.TabIndex = 2;
            this.btn_xoa.Text = "Xóa";
            this.btn_xoa.Click += new System.EventHandler(this.btn_xoa_Click);
            // 
            // btn9
            // 
            this.btn9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn9.Appearance.Options.UseFont = true;
            this.btn9.Location = new System.Drawing.Point(127, 3);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(55, 36);
            this.btn9.TabIndex = 25;
            this.btn9.Text = "9";
            this.btn9.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn8
            // 
            this.btn8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn8.Appearance.Options.UseFont = true;
            this.btn8.Location = new System.Drawing.Point(66, 3);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(55, 36);
            this.btn8.TabIndex = 24;
            this.btn8.Text = "8";
            this.btn8.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn7
            // 
            this.btn7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn7.Appearance.Options.UseFont = true;
            this.btn7.Location = new System.Drawing.Point(5, 3);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(55, 36);
            this.btn7.TabIndex = 23;
            this.btn7.Text = "7";
            this.btn7.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn6
            // 
            this.btn6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn6.Appearance.Options.UseFont = true;
            this.btn6.Location = new System.Drawing.Point(127, 43);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(55, 36);
            this.btn6.TabIndex = 22;
            this.btn6.Text = "6";
            this.btn6.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn5
            // 
            this.btn5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn5.Appearance.Options.UseFont = true;
            this.btn5.Location = new System.Drawing.Point(66, 43);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(55, 36);
            this.btn5.TabIndex = 21;
            this.btn5.Text = "5";
            this.btn5.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn4
            // 
            this.btn4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn4.Appearance.Options.UseFont = true;
            this.btn4.Location = new System.Drawing.Point(5, 43);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(55, 36);
            this.btn4.TabIndex = 20;
            this.btn4.Text = "4";
            this.btn4.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn3
            // 
            this.btn3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn3.Appearance.Options.UseFont = true;
            this.btn3.Location = new System.Drawing.Point(127, 83);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(55, 36);
            this.btn3.TabIndex = 19;
            this.btn3.Text = "3";
            this.btn3.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn2
            // 
            this.btn2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn2.Appearance.Options.UseFont = true;
            this.btn2.Location = new System.Drawing.Point(66, 83);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(55, 36);
            this.btn2.TabIndex = 18;
            this.btn2.Text = "2";
            this.btn2.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn1
            // 
            this.btn1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn1.Appearance.Options.UseFont = true;
            this.btn1.Location = new System.Drawing.Point(5, 83);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(55, 36);
            this.btn1.TabIndex = 17;
            this.btn1.Text = "1";
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn0
            // 
            this.btn0.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn0.Appearance.Options.UseFont = true;
            this.btn0.Location = new System.Drawing.Point(189, 83);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(55, 36);
            this.btn0.TabIndex = 16;
            this.btn0.Text = "0";
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // panelControl65
            // 
            this.panelControl65.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl65.Controls.Add(this.panelControl66);
            this.panelControl65.Controls.Add(this.lb_soluong);
            this.panelControl65.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl65.Location = new System.Drawing.Point(0, 0);
            this.panelControl65.Name = "panelControl65";
            this.panelControl65.Size = new System.Drawing.Size(342, 24);
            this.panelControl65.TabIndex = 21;
            // 
            // panelControl66
            // 
            this.panelControl66.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl66.Controls.Add(this.lbSoluong);
            this.panelControl66.Location = new System.Drawing.Point(125, 5);
            this.panelControl66.Name = "panelControl66";
            this.panelControl66.Size = new System.Drawing.Size(170, 15);
            this.panelControl66.TabIndex = 18;
            // 
            // lbSoluong
            // 
            this.lbSoluong.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbSoluong.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbSoluong.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbSoluong.Location = new System.Drawing.Point(163, 0);
            this.lbSoluong.Name = "lbSoluong";
            this.lbSoluong.Size = new System.Drawing.Size(7, 13);
            this.lbSoluong.TabIndex = 7;
            this.lbSoluong.Text = "0";
            // 
            // lb_soluong
            // 
            this.lb_soluong.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_soluong.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_soluong.Location = new System.Drawing.Point(14, 5);
            this.lb_soluong.Name = "lb_soluong";
            this.lb_soluong.Size = new System.Drawing.Size(55, 13);
            this.lb_soluong.TabIndex = 14;
            this.lb_soluong.Text = "Số lượng :";
            // 
            // panelControl20
            // 
            this.panelControl20.Controls.Add(this.panelControl63);
            this.panelControl20.Controls.Add(this.lbTableTatol);
            this.panelControl20.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl20.Location = new System.Drawing.Point(2, 346);
            this.panelControl20.Name = "panelControl20";
            this.panelControl20.Size = new System.Drawing.Size(346, 92);
            this.panelControl20.TabIndex = 20;
            // 
            // panelControl63
            // 
            this.panelControl63.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl63.Controls.Add(this.panelControl67);
            this.panelControl63.Controls.Add(this.labelControl5);
            this.panelControl63.Controls.Add(this.lb_clicktiengio);
            this.panelControl63.Controls.Add(this.panelControl62);
            this.panelControl63.Controls.Add(this.labelControl13);
            this.panelControl63.Controls.Add(this.panelControl61);
            this.panelControl63.Controls.Add(this.panelControl60);
            this.panelControl63.Controls.Add(this.labelControl51);
            this.panelControl63.Controls.Add(this.panelControl59);
            this.panelControl63.Controls.Add(this.labelControl52);
            this.panelControl63.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl63.Location = new System.Drawing.Point(146, 2);
            this.panelControl63.Name = "panelControl63";
            this.panelControl63.Size = new System.Drawing.Size(198, 88);
            this.panelControl63.TabIndex = 19;
            // 
            // panelControl67
            // 
            this.panelControl67.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl67.Controls.Add(this.lbDichvu);
            this.panelControl67.Location = new System.Drawing.Point(97, 21);
            this.panelControl67.Name = "panelControl67";
            this.panelControl67.Size = new System.Drawing.Size(93, 15);
            this.panelControl67.TabIndex = 21;
            // 
            // lbDichvu
            // 
            this.lbDichvu.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbDichvu.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbDichvu.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbDichvu.Location = new System.Drawing.Point(86, 0);
            this.lbDichvu.Name = "lbDichvu";
            this.lbDichvu.Size = new System.Drawing.Size(7, 13);
            this.lbDichvu.TabIndex = 7;
            this.lbDichvu.Text = "0";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl5.Location = new System.Drawing.Point(3, 21);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(44, 13);
            this.labelControl5.TabIndex = 20;
            this.labelControl5.Text = "Dịch vụ:";
            // 
            // lb_clicktiengio
            // 
            this.lb_clicktiengio.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_clicktiengio.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_clicktiengio.Location = new System.Drawing.Point(3, 4);
            this.lb_clicktiengio.Name = "lb_clicktiengio";
            this.lb_clicktiengio.Size = new System.Drawing.Size(65, 13);
            this.lb_clicktiengio.TabIndex = 10;
            this.lb_clicktiengio.Text = "Tiền phòng:";
            // 
            // panelControl62
            // 
            this.panelControl62.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl62.Controls.Add(this.lbTatol);
            this.panelControl62.Location = new System.Drawing.Point(97, 70);
            this.panelControl62.Name = "panelControl62";
            this.panelControl62.Size = new System.Drawing.Size(93, 15);
            this.panelControl62.TabIndex = 19;
            // 
            // lbTatol
            // 
            this.lbTatol.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbTatol.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbTatol.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbTatol.Location = new System.Drawing.Point(86, 0);
            this.lbTatol.Name = "lbTatol";
            this.lbTatol.Size = new System.Drawing.Size(7, 13);
            this.lbTatol.TabIndex = 2;
            this.lbTatol.Text = "0";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl13.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl13.Location = new System.Drawing.Point(3, 70);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(61, 13);
            this.labelControl13.TabIndex = 0;
            this.labelControl13.Text = "Tổng cộng:";
            // 
            // panelControl61
            // 
            this.panelControl61.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl61.Controls.Add(this.lbGiamGia);
            this.panelControl61.Location = new System.Drawing.Point(97, 54);
            this.panelControl61.Name = "panelControl61";
            this.panelControl61.Size = new System.Drawing.Size(93, 15);
            this.panelControl61.TabIndex = 19;
            // 
            // lbGiamGia
            // 
            this.lbGiamGia.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbGiamGia.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbGiamGia.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbGiamGia.Location = new System.Drawing.Point(86, 0);
            this.lbGiamGia.Name = "lbGiamGia";
            this.lbGiamGia.Size = new System.Drawing.Size(7, 13);
            this.lbGiamGia.TabIndex = 5;
            this.lbGiamGia.Text = "0";
            // 
            // panelControl60
            // 
            this.panelControl60.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl60.Controls.Add(this.lbPhuThu);
            this.panelControl60.Location = new System.Drawing.Point(97, 37);
            this.panelControl60.Name = "panelControl60";
            this.panelControl60.Size = new System.Drawing.Size(93, 15);
            this.panelControl60.TabIndex = 19;
            // 
            // lbPhuThu
            // 
            this.lbPhuThu.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbPhuThu.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbPhuThu.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbPhuThu.Location = new System.Drawing.Point(86, 0);
            this.lbPhuThu.Name = "lbPhuThu";
            this.lbPhuThu.Size = new System.Drawing.Size(7, 13);
            this.lbPhuThu.TabIndex = 7;
            this.lbPhuThu.Text = "0";
            // 
            // labelControl51
            // 
            this.labelControl51.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl51.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl51.Location = new System.Drawing.Point(3, 54);
            this.labelControl51.Name = "labelControl51";
            this.labelControl51.Size = new System.Drawing.Size(63, 13);
            this.labelControl51.TabIndex = 4;
            this.labelControl51.Text = "Chiết khấu:";
            // 
            // panelControl59
            // 
            this.panelControl59.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl59.Controls.Add(this.lbTiengio);
            this.panelControl59.Location = new System.Drawing.Point(97, 4);
            this.panelControl59.Name = "panelControl59";
            this.panelControl59.Size = new System.Drawing.Size(93, 15);
            this.panelControl59.TabIndex = 18;
            // 
            // lbTiengio
            // 
            this.lbTiengio.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbTiengio.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbTiengio.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbTiengio.Location = new System.Drawing.Point(86, 0);
            this.lbTiengio.Name = "lbTiengio";
            this.lbTiengio.Size = new System.Drawing.Size(7, 13);
            this.lbTiengio.TabIndex = 11;
            this.lbTiengio.Text = "0";
            // 
            // labelControl52
            // 
            this.labelControl52.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl52.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl52.Location = new System.Drawing.Point(3, 37);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(46, 13);
            this.labelControl52.TabIndex = 6;
            this.labelControl52.Text = "Phụ thu:";
            // 
            // lbTableTatol
            // 
            this.lbTableTatol.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbTableTatol.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbTableTatol.Location = new System.Drawing.Point(8, 14);
            this.lbTableTatol.Name = "lbTableTatol";
            this.lbTableTatol.Size = new System.Drawing.Size(0, 13);
            this.lbTableTatol.TabIndex = 1;
            // 
            // pnNghiepVu_ThuNgan
            // 
            this.pnNghiepVu_ThuNgan.Controls.Add(this.pnNghiepVu);
            this.pnNghiepVu_ThuNgan.Controls.Add(this.pnThuNgan);
            this.pnNghiepVu_ThuNgan.Controls.Add(this.pnDacThu);
            this.pnNghiepVu_ThuNgan.Controls.Add(this.panelControl8);
            this.pnNghiepVu_ThuNgan.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnNghiepVu_ThuNgan.Location = new System.Drawing.Point(0, 440);
            this.pnNghiepVu_ThuNgan.Name = "pnNghiepVu_ThuNgan";
            this.pnNghiepVu_ThuNgan.Size = new System.Drawing.Size(350, 258);
            this.pnNghiepVu_ThuNgan.TabIndex = 1;
            // 
            // pnThuNgan
            // 
            this.pnThuNgan.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnThuNgan.Controls.Add(this.btnKhachrangoai);
            this.pnThuNgan.Controls.Add(this.btnNhanphong);
            this.pnThuNgan.Controls.Add(this.btnXacNhan);
            this.pnThuNgan.Controls.Add(this.btnGiaBan2);
            this.pnThuNgan.Controls.Add(this.btnTachPhong);
            this.pnThuNgan.Controls.Add(this.btn_OrderSent);
            this.pnThuNgan.Controls.Add(this.btn_SoDoBan);
            this.pnThuNgan.Controls.Add(this.btn_TamTinh);
            this.pnThuNgan.Controls.Add(this.btn_GhepBan);
            this.pnThuNgan.Controls.Add(this.btn_Payment);
            this.pnThuNgan.Controls.Add(this.btn_YeuCauThem);
            this.pnThuNgan.Controls.Add(this.btn_logout);
            this.pnThuNgan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnThuNgan.Location = new System.Drawing.Point(2, 47);
            this.pnThuNgan.Name = "pnThuNgan";
            this.pnThuNgan.Size = new System.Drawing.Size(346, 209);
            this.pnThuNgan.TabIndex = 20;
            // 
            // btnKhachrangoai
            // 
            this.btnKhachrangoai.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnKhachrangoai.Appearance.Options.UseFont = true;
            this.btnKhachrangoai.Appearance.Options.UseTextOptions = true;
            this.btnKhachrangoai.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnKhachrangoai.Location = new System.Drawing.Point(232, 6);
            this.btnKhachrangoai.Name = "btnKhachrangoai";
            this.btnKhachrangoai.Size = new System.Drawing.Size(100, 62);
            this.btnKhachrangoai.TabIndex = 22;
            this.btnKhachrangoai.Text = "Khách Ra Ngoài";
            this.btnKhachrangoai.Click += new System.EventHandler(this.btnKhachrangoai_Click);
            // 
            // btnNhanphong
            // 
            this.btnNhanphong.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnNhanphong.Appearance.Options.UseFont = true;
            this.btnNhanphong.Location = new System.Drawing.Point(8, 6);
            this.btnNhanphong.Name = "btnNhanphong";
            this.btnNhanphong.Size = new System.Drawing.Size(100, 62);
            this.btnNhanphong.TabIndex = 21;
            this.btnNhanphong.Text = "Nhận Phòng";
            this.btnNhanphong.Click += new System.EventHandler(this.btnNhanphong_Click);
            // 
            // btnXacNhan
            // 
            this.btnXacNhan.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnXacNhan.Appearance.Options.UseFont = true;
            this.btnXacNhan.Location = new System.Drawing.Point(120, 6);
            this.btnXacNhan.Name = "btnXacNhan";
            this.btnXacNhan.Size = new System.Drawing.Size(100, 62);
            this.btnXacNhan.TabIndex = 21;
            this.btnXacNhan.Text = "Xác Nhận";
            this.btnXacNhan.Click += new System.EventHandler(this.btnXacNhan_Click);
            // 
            // btnGiaBan2
            // 
            this.btnGiaBan2.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.btnGiaBan2.Appearance.Options.UseFont = true;
            this.btnGiaBan2.Appearance.Options.UseTextOptions = true;
            this.btnGiaBan2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnGiaBan2.Location = new System.Drawing.Point(127, 212);
            this.btnGiaBan2.Name = "btnGiaBan2";
            this.btnGiaBan2.Size = new System.Drawing.Size(100, 32);
            this.btnGiaBan2.TabIndex = 12;
            this.btnGiaBan2.Text = "Bán giá 2";
            this.btnGiaBan2.Visible = false;
            this.btnGiaBan2.Click += new System.EventHandler(this.btnGiaBan2_Click);
            // 
            // btnTachPhong
            // 
            this.btnTachPhong.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnTachPhong.Appearance.Options.UseFont = true;
            this.btnTachPhong.Location = new System.Drawing.Point(149, 211);
            this.btnTachPhong.Name = "btnTachPhong";
            this.btnTachPhong.Size = new System.Drawing.Size(100, 32);
            this.btnTachPhong.TabIndex = 20;
            this.btnTachPhong.Text = "Tách Phòng";
            this.btnTachPhong.Visible = false;
            this.btnTachPhong.Click += new System.EventHandler(this.btnTachPhong_Click);
            // 
            // btn_OrderSent
            // 
            this.btn_OrderSent.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_OrderSent.Appearance.Options.UseFont = true;
            this.btn_OrderSent.Location = new System.Drawing.Point(120, 74);
            this.btn_OrderSent.Name = "btn_OrderSent";
            this.btn_OrderSent.Size = new System.Drawing.Size(100, 62);
            this.btn_OrderSent.TabIndex = 19;
            this.btn_OrderSent.Text = "Gửi Order";
            this.btn_OrderSent.Click += new System.EventHandler(this.btn_OrderSent_Click);
            // 
            // btn_SoDoBan
            // 
            this.btn_SoDoBan.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_SoDoBan.Appearance.Options.UseFont = true;
            this.btn_SoDoBan.Location = new System.Drawing.Point(8, 74);
            this.btn_SoDoBan.Name = "btn_SoDoBan";
            this.btn_SoDoBan.Size = new System.Drawing.Size(100, 62);
            this.btn_SoDoBan.TabIndex = 18;
            this.btn_SoDoBan.Text = "Mở Phòng";
            this.btn_SoDoBan.Click += new System.EventHandler(this.btn_SoDoBan_Click);
            // 
            // btn_TamTinh
            // 
            this.btn_TamTinh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_TamTinh.Appearance.Options.UseFont = true;
            this.btn_TamTinh.Location = new System.Drawing.Point(120, 142);
            this.btn_TamTinh.Name = "btn_TamTinh";
            this.btn_TamTinh.Size = new System.Drawing.Size(100, 62);
            this.btn_TamTinh.TabIndex = 17;
            this.btn_TamTinh.Text = "In Tạm Tính";
            this.btn_TamTinh.Click += new System.EventHandler(this.btn_TamTinh_Click);
            // 
            // btn_GhepBan
            // 
            this.btn_GhepBan.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_GhepBan.Appearance.Options.UseFont = true;
            this.btn_GhepBan.Appearance.Options.UseTextOptions = true;
            this.btn_GhepBan.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btn_GhepBan.Location = new System.Drawing.Point(149, 211);
            this.btn_GhepBan.Name = "btn_GhepBan";
            this.btn_GhepBan.Size = new System.Drawing.Size(100, 32);
            this.btn_GhepBan.TabIndex = 16;
            this.btn_GhepBan.Text = "Ghép Phòng";
            this.btn_GhepBan.Visible = false;
            this.btn_GhepBan.Click += new System.EventHandler(this.btn_GhepBan_Click);
            // 
            // btn_Payment
            // 
            this.btn_Payment.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_Payment.Appearance.Options.UseFont = true;
            this.btn_Payment.Location = new System.Drawing.Point(8, 142);
            this.btn_Payment.Name = "btn_Payment";
            this.btn_Payment.Size = new System.Drawing.Size(100, 62);
            this.btn_Payment.TabIndex = 14;
            this.btn_Payment.Text = "F1\r\nThanh Toán";
            this.btn_Payment.Click += new System.EventHandler(this.btn_ThanhToan_Click);
            // 
            // btn_YeuCauThem
            // 
            this.btn_YeuCauThem.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_YeuCauThem.Appearance.Options.UseFont = true;
            this.btn_YeuCauThem.Location = new System.Drawing.Point(232, 74);
            this.btn_YeuCauThem.Name = "btn_YeuCauThem";
            this.btn_YeuCauThem.Size = new System.Drawing.Size(100, 62);
            this.btn_YeuCauThem.TabIndex = 12;
            this.btn_YeuCauThem.Text = "Yêu Cầu Thêm\r\n";
            this.btn_YeuCauThem.Click += new System.EventHandler(this.btn_YeuCauThem_Click);
            // 
            // btn_logout
            // 
            this.btn_logout.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_logout.Appearance.Options.UseFont = true;
            this.btn_logout.Location = new System.Drawing.Point(232, 142);
            this.btn_logout.Name = "btn_logout";
            this.btn_logout.Size = new System.Drawing.Size(100, 62);
            this.btn_logout.TabIndex = 20;
            this.btn_logout.Text = "Đóng";
            this.btn_logout.Click += new System.EventHandler(this.btn_logout_Click);
            // 
            // pnNghiepVu
            // 
            this.pnNghiepVu.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnNghiepVu.Controls.Add(this.btn_InlaiOrder);
            this.pnNghiepVu.Controls.Add(this.btn_ExitTable);
            this.pnNghiepVu.Controls.Add(this.btn_InLaiHoaDon);
            this.pnNghiepVu.Controls.Add(this.btn_ExitDishe);
            this.pnNghiepVu.Controls.Add(this.btn_DatBan);
            this.pnNghiepVu.Controls.Add(this.btn_LayLaiHoaDon);
            this.pnNghiepVu.Controls.Add(this.btnMoKet);
            this.pnNghiepVu.Controls.Add(this.btnKetKa);
            this.pnNghiepVu.Controls.Add(this.btnChuyenBan);
            this.pnNghiepVu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnNghiepVu.Location = new System.Drawing.Point(2, 47);
            this.pnNghiepVu.Name = "pnNghiepVu";
            this.pnNghiepVu.Size = new System.Drawing.Size(346, 209);
            this.pnNghiepVu.TabIndex = 20;
            // 
            // btn_InlaiOrder
            // 
            this.btn_InlaiOrder.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_InlaiOrder.Appearance.Options.UseFont = true;
            this.btn_InlaiOrder.Location = new System.Drawing.Point(8, 142);
            this.btn_InlaiOrder.Name = "btn_InlaiOrder";
            this.btn_InlaiOrder.Size = new System.Drawing.Size(100, 62);
            this.btn_InlaiOrder.TabIndex = 29;
            this.btn_InlaiOrder.Text = "In Lại\r\nOrder";
            this.btn_InlaiOrder.Click += new System.EventHandler(this.btn_InlaiOrder_Click);
            // 
            // btn_ExitTable
            // 
            this.btn_ExitTable.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_ExitTable.Appearance.Options.UseFont = true;
            this.btn_ExitTable.Location = new System.Drawing.Point(121, 142);
            this.btn_ExitTable.Name = "btn_ExitTable";
            this.btn_ExitTable.Size = new System.Drawing.Size(100, 62);
            this.btn_ExitTable.TabIndex = 28;
            this.btn_ExitTable.Text = "Hủy Phòng";
            this.btn_ExitTable.Click += new System.EventHandler(this.btn_HuyBan_Click);
            // 
            // btn_InLaiHoaDon
            // 
            this.btn_InLaiHoaDon.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_InLaiHoaDon.Appearance.Options.UseFont = true;
            this.btn_InLaiHoaDon.Location = new System.Drawing.Point(8, 5);
            this.btn_InLaiHoaDon.Name = "btn_InLaiHoaDon";
            this.btn_InLaiHoaDon.Size = new System.Drawing.Size(100, 62);
            this.btn_InLaiHoaDon.TabIndex = 27;
            this.btn_InLaiHoaDon.Text = "In Lại\r\nHóa Đơn";
            this.btn_InLaiHoaDon.Click += new System.EventHandler(this.btn_InLaiHoaDon_Click);
            // 
            // btn_ExitDishe
            // 
            this.btn_ExitDishe.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_ExitDishe.Appearance.Options.UseFont = true;
            this.btn_ExitDishe.Location = new System.Drawing.Point(234, 142);
            this.btn_ExitDishe.Name = "btn_ExitDishe";
            this.btn_ExitDishe.Size = new System.Drawing.Size(100, 62);
            this.btn_ExitDishe.TabIndex = 26;
            this.btn_ExitDishe.Text = "Hủy Dịch Vụ";
            this.btn_ExitDishe.Click += new System.EventHandler(this.btn_HuyMon_Click);
            // 
            // btn_DatBan
            // 
            this.btn_DatBan.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_DatBan.Appearance.Options.UseFont = true;
            this.btn_DatBan.Location = new System.Drawing.Point(121, 5);
            this.btn_DatBan.Name = "btn_DatBan";
            this.btn_DatBan.Size = new System.Drawing.Size(100, 62);
            this.btn_DatBan.TabIndex = 25;
            this.btn_DatBan.Text = "Đặt Phòng";
            this.btn_DatBan.Click += new System.EventHandler(this.btn_DatBan_Click);
            // 
            // btn_LayLaiHoaDon
            // 
            this.btn_LayLaiHoaDon.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_LayLaiHoaDon.Appearance.Options.UseFont = true;
            this.btn_LayLaiHoaDon.Location = new System.Drawing.Point(8, 73);
            this.btn_LayLaiHoaDon.Name = "btn_LayLaiHoaDon";
            this.btn_LayLaiHoaDon.Size = new System.Drawing.Size(100, 63);
            this.btn_LayLaiHoaDon.TabIndex = 24;
            this.btn_LayLaiHoaDon.Text = "Lấy Lại \r\nHóa Đơn";
            this.btn_LayLaiHoaDon.Click += new System.EventHandler(this.btn_LayLaiHoaDon_Click);
            // 
            // btnMoKet
            // 
            this.btnMoKet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnMoKet.Appearance.Options.UseFont = true;
            this.btnMoKet.Location = new System.Drawing.Point(234, 5);
            this.btnMoKet.Name = "btnMoKet";
            this.btnMoKet.Size = new System.Drawing.Size(100, 62);
            this.btnMoKet.TabIndex = 22;
            this.btnMoKet.Text = "Mở Két";
            this.btnMoKet.Click += new System.EventHandler(this.btnMoKet_Click);
            // 
            // btnKetKa
            // 
            this.btnKetKa.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnKetKa.Appearance.Options.UseFont = true;
            this.btnKetKa.Location = new System.Drawing.Point(234, 74);
            this.btnKetKa.Name = "btnKetKa";
            this.btnKetKa.Size = new System.Drawing.Size(100, 62);
            this.btnKetKa.TabIndex = 21;
            this.btnKetKa.Text = "Kết Ca";
            this.btnKetKa.Click += new System.EventHandler(this.btnKetKa_Click);
            // 
            // btnChuyenBan
            // 
            this.btnChuyenBan.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnChuyenBan.Appearance.Options.UseFont = true;
            this.btnChuyenBan.Location = new System.Drawing.Point(121, 74);
            this.btnChuyenBan.Name = "btnChuyenBan";
            this.btnChuyenBan.Size = new System.Drawing.Size(100, 62);
            this.btnChuyenBan.TabIndex = 13;
            this.btnChuyenBan.Text = "Chuyển Phòng";
            this.btnChuyenBan.Click += new System.EventHandler(this.btn_ChuyenBan_Click);
            // 
            // pnDacThu
            // 
            this.pnDacThu.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnDacThu.Controls.Add(this.btnVesinh);
            this.pnDacThu.Controls.Add(this.btnKhoaphong);
            this.pnDacThu.Controls.Add(this.btnDanhsachkhach);
            this.pnDacThu.Controls.Add(this.simpleButton5);
            this.pnDacThu.Controls.Add(this.simpleButton6);
            this.pnDacThu.Controls.Add(this.btnDoigio_nhanphong);
            this.pnDacThu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnDacThu.Location = new System.Drawing.Point(2, 47);
            this.pnDacThu.Name = "pnDacThu";
            this.pnDacThu.Size = new System.Drawing.Size(346, 209);
            this.pnDacThu.TabIndex = 21;
            // 
            // btnVesinh
            // 
            this.btnVesinh.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.btnVesinh.Appearance.Options.UseFont = true;
            this.btnVesinh.Appearance.Options.UseTextOptions = true;
            this.btnVesinh.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnVesinh.Location = new System.Drawing.Point(8, 74);
            this.btnVesinh.Name = "btnVesinh";
            this.btnVesinh.Size = new System.Drawing.Size(100, 62);
            this.btnVesinh.TabIndex = 25;
            this.btnVesinh.Text = "Vệ sinh xong";
            this.btnVesinh.Click += new System.EventHandler(this.btnVesinh_Click);
            // 
            // btnKhoaphong
            // 
            this.btnKhoaphong.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.btnKhoaphong.Appearance.Options.UseFont = true;
            this.btnKhoaphong.Appearance.Options.UseTextOptions = true;
            this.btnKhoaphong.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnKhoaphong.Location = new System.Drawing.Point(232, 6);
            this.btnKhoaphong.Name = "btnKhoaphong";
            this.btnKhoaphong.Size = new System.Drawing.Size(100, 62);
            this.btnKhoaphong.TabIndex = 24;
            this.btnKhoaphong.Text = "Khóa phòng";
            this.btnKhoaphong.Click += new System.EventHandler(this.btnKhoaphong_Click);
            // 
            // btnDanhsachkhach
            // 
            this.btnDanhsachkhach.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnDanhsachkhach.Appearance.Options.UseFont = true;
            this.btnDanhsachkhach.Appearance.Options.UseTextOptions = true;
            this.btnDanhsachkhach.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnDanhsachkhach.Location = new System.Drawing.Point(8, 5);
            this.btnDanhsachkhach.Name = "btnDanhsachkhach";
            this.btnDanhsachkhach.Size = new System.Drawing.Size(100, 62);
            this.btnDanhsachkhach.TabIndex = 23;
            this.btnDanhsachkhach.Text = "Hồ Sơ Khách";
            this.btnDanhsachkhach.Click += new System.EventHandler(this.btnDanhsachkhach_Click);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.Appearance.Options.UseTextOptions = true;
            this.simpleButton5.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleButton5.Location = new System.Drawing.Point(8, 146);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(100, 62);
            this.simpleButton5.TabIndex = 12;
            this.simpleButton5.Text = "Bán giá 2";
            this.simpleButton5.Visible = false;
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.Location = new System.Drawing.Point(114, 142);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(100, 62);
            this.simpleButton6.TabIndex = 20;
            this.simpleButton6.Text = "Tách Phòng";
            this.simpleButton6.Visible = false;
            // 
            // btnDoigio_nhanphong
            // 
            this.btnDoigio_nhanphong.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnDoigio_nhanphong.Appearance.Options.UseFont = true;
            this.btnDoigio_nhanphong.Appearance.Options.UseTextOptions = true;
            this.btnDoigio_nhanphong.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnDoigio_nhanphong.Location = new System.Drawing.Point(119, 6);
            this.btnDoigio_nhanphong.Name = "btnDoigio_nhanphong";
            this.btnDoigio_nhanphong.Size = new System.Drawing.Size(100, 62);
            this.btnDoigio_nhanphong.TabIndex = 16;
            this.btnDoigio_nhanphong.Text = "Đổi giờ \r\nnhận phòng";
            this.btnDoigio_nhanphong.Click += new System.EventHandler(this.btnDoigio_nhanphong_Click);
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.btn_DatThu);
            this.panelControl8.Controls.Add(this.btn_NghiepVu);
            this.panelControl8.Controls.Add(this.btn_ThuNgan);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl8.Location = new System.Drawing.Point(2, 2);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(346, 45);
            this.panelControl8.TabIndex = 2;
            // 
            // btn_DatThu
            // 
            this.btn_DatThu.AllowFocus = false;
            this.btn_DatThu.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.btn_DatThu.Appearance.Options.UseFont = true;
            this.btn_DatThu.Location = new System.Drawing.Point(242, 0);
            this.btn_DatThu.Name = "btn_DatThu";
            this.btn_DatThu.Size = new System.Drawing.Size(104, 44);
            this.btn_DatThu.TabIndex = 2;
            this.btn_DatThu.Text = "Đặc thù";
            this.btn_DatThu.Click += new System.EventHandler(this.btn_DatThu_Click);
            // 
            // btn_NghiepVu
            // 
            this.btn_NghiepVu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_NghiepVu.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.btn_NghiepVu.Appearance.Options.UseFont = true;
            this.btn_NghiepVu.Location = new System.Drawing.Point(123, 0);
            this.btn_NghiepVu.Name = "btn_NghiepVu";
            this.btn_NghiepVu.Size = new System.Drawing.Size(119, 44);
            this.btn_NghiepVu.TabIndex = 1;
            this.btn_NghiepVu.Text = "Nghiệp vụ";
            this.btn_NghiepVu.Click += new System.EventHandler(this.btn_NghiepVu_Click);
            // 
            // btn_ThuNgan
            // 
            this.btn_ThuNgan.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_ThuNgan.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.btn_ThuNgan.Appearance.Options.UseFont = true;
            this.btn_ThuNgan.Location = new System.Drawing.Point(1, 1);
            this.btn_ThuNgan.Name = "btn_ThuNgan";
            this.btn_ThuNgan.Size = new System.Drawing.Size(122, 44);
            this.btn_ThuNgan.TabIndex = 0;
            this.btn_ThuNgan.Text = "Thu ngân";
            this.btn_ThuNgan.Click += new System.EventHandler(this.btn_ThuNgan_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pnHuymon
            // 
            this.pnHuymon.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnHuymon.Controls.Add(this.pn_GroupTable);
            this.pnHuymon.Controls.Add(this.pnInforTablecheck);
            this.pnHuymon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnHuymon.Location = new System.Drawing.Point(0, 0);
            this.pnHuymon.Name = "pnHuymon";
            this.pnHuymon.Size = new System.Drawing.Size(674, 698);
            this.pnHuymon.TabIndex = 0;
            // 
            // pn_GroupTable
            // 
            this.pn_GroupTable.Controls.Add(this.panelControl15);
            this.pn_GroupTable.Controls.Add(this.pnUpDow);
            this.pn_GroupTable.Controls.Add(this.pn_TableTow);
            this.pn_GroupTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_GroupTable.Location = new System.Drawing.Point(0, 143);
            this.pn_GroupTable.Name = "pn_GroupTable";
            this.pn_GroupTable.Size = new System.Drawing.Size(674, 555);
            this.pn_GroupTable.TabIndex = 3;
            // 
            // panelControl15
            // 
            this.panelControl15.Controls.Add(this.gr_divide_Table_1);
            this.panelControl15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl15.Location = new System.Drawing.Point(2, 2);
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(356, 551);
            this.panelControl15.TabIndex = 0;
            // 
            // gr_divide_Table_1
            // 
            this.gr_divide_Table_1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.RelationName = "Level1";
            this.gr_divide_Table_1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gr_divide_Table_1.Location = new System.Drawing.Point(2, 2);
            this.gr_divide_Table_1.MainView = this.gv_divide_Table_1;
            this.gr_divide_Table_1.Name = "gr_divide_Table_1";
            this.gr_divide_Table_1.Size = new System.Drawing.Size(352, 547);
            this.gr_divide_Table_1.TabIndex = 20;
            this.gr_divide_Table_1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv_divide_Table_1});
            // 
            // gv_divide_Table_1
            // 
            this.gv_divide_Table_1.ColumnPanelRowHeight = 30;
            this.gv_divide_Table_1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._SSTT,
            this._TEN_HANGHOA,
            this._SOLUONG,
            this._GIABAN,
            this._Thanh_Tien,
            this._MA_HANGHOA,
            this._GHICHU,
            this._MA_BEP});
            this.gv_divide_Table_1.GridControl = this.gr_divide_Table_1;
            this.gv_divide_Table_1.Name = "gv_divide_Table_1";
            this.gv_divide_Table_1.OptionsView.ShowGroupPanel = false;
            this.gv_divide_Table_1.OptionsView.ShowIndicator = false;
            this.gv_divide_Table_1.RowHeight = 35;
            this.gv_divide_Table_1.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gv_divide_Table_1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gv_divide_Table_1_CustomDrawCell);
            // 
            // _SSTT
            // 
            this._SSTT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._SSTT.AppearanceCell.Options.UseFont = true;
            this._SSTT.AppearanceCell.Options.UseTextOptions = true;
            this._SSTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._SSTT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._SSTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this._SSTT.AppearanceHeader.Options.UseFont = true;
            this._SSTT.AppearanceHeader.Options.UseTextOptions = true;
            this._SSTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._SSTT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._SSTT.Caption = "TT";
            this._SSTT.FieldName = "_SSTT";
            this._SSTT.Name = "_SSTT";
            this._SSTT.OptionsColumn.AllowEdit = false;
            this._SSTT.OptionsColumn.AllowFocus = false;
            this._SSTT.Visible = true;
            this._SSTT.VisibleIndex = 0;
            this._SSTT.Width = 34;
            // 
            // _TEN_HANGHOA
            // 
            this._TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this._TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this._TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this._TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this._TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._TEN_HANGHOA.Caption = "Tên Hàng Hóa";
            this._TEN_HANGHOA.FieldName = "_TEN_HANGHOA";
            this._TEN_HANGHOA.Name = "_TEN_HANGHOA";
            this._TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this._TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this._TEN_HANGHOA.Visible = true;
            this._TEN_HANGHOA.VisibleIndex = 1;
            this._TEN_HANGHOA.Width = 116;
            // 
            // _SOLUONG
            // 
            this._SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._SOLUONG.AppearanceCell.Options.UseFont = true;
            this._SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this._SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this._SOLUONG.AppearanceHeader.Options.UseFont = true;
            this._SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this._SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._SOLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._SOLUONG.Caption = "SL";
            this._SOLUONG.FieldName = "_SOLUONG";
            this._SOLUONG.Name = "_SOLUONG";
            this._SOLUONG.OptionsColumn.AllowEdit = false;
            this._SOLUONG.OptionsColumn.AllowFocus = false;
            this._SOLUONG.Visible = true;
            this._SOLUONG.VisibleIndex = 2;
            this._SOLUONG.Width = 51;
            // 
            // _GIABAN
            // 
            this._GIABAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._GIABAN.AppearanceCell.Options.UseFont = true;
            this._GIABAN.AppearanceCell.Options.UseTextOptions = true;
            this._GIABAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._GIABAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._GIABAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this._GIABAN.AppearanceHeader.Options.UseFont = true;
            this._GIABAN.AppearanceHeader.Options.UseTextOptions = true;
            this._GIABAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._GIABAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._GIABAN.Caption = "Đơn giá";
            this._GIABAN.DisplayFormat.FormatString = "{0:#,###0}";
            this._GIABAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this._GIABAN.FieldName = "_GIABAN";
            this._GIABAN.Name = "_GIABAN";
            this._GIABAN.OptionsColumn.AllowEdit = false;
            this._GIABAN.OptionsColumn.AllowFocus = false;
            this._GIABAN.Visible = true;
            this._GIABAN.VisibleIndex = 3;
            this._GIABAN.Width = 96;
            // 
            // _Thanh_Tien
            // 
            this._Thanh_Tien.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._Thanh_Tien.AppearanceCell.Options.UseFont = true;
            this._Thanh_Tien.AppearanceCell.Options.UseTextOptions = true;
            this._Thanh_Tien.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._Thanh_Tien.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._Thanh_Tien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this._Thanh_Tien.AppearanceHeader.Options.UseFont = true;
            this._Thanh_Tien.AppearanceHeader.Options.UseTextOptions = true;
            this._Thanh_Tien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._Thanh_Tien.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._Thanh_Tien.Caption = "Thành Tiền";
            this._Thanh_Tien.DisplayFormat.FormatString = "{0:#,###0}";
            this._Thanh_Tien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this._Thanh_Tien.FieldName = "_Thanh_Tien";
            this._Thanh_Tien.Name = "_Thanh_Tien";
            this._Thanh_Tien.OptionsColumn.AllowEdit = false;
            this._Thanh_Tien.OptionsColumn.AllowFocus = false;
            this._Thanh_Tien.Visible = true;
            this._Thanh_Tien.VisibleIndex = 4;
            this._Thanh_Tien.Width = 104;
            // 
            // _MA_HANGHOA
            // 
            this._MA_HANGHOA.Caption = "Mã Hàng Hóa";
            this._MA_HANGHOA.FieldName = "_MA_HANGHOA";
            this._MA_HANGHOA.Name = "_MA_HANGHOA";
            this._MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this._MA_HANGHOA.OptionsColumn.AllowFocus = false;
            // 
            // _GHICHU
            // 
            this._GHICHU.Caption = "Ghi Chú";
            this._GHICHU.FieldName = "_GHICHU";
            this._GHICHU.Name = "_GHICHU";
            // 
            // _MA_BEP
            // 
            this._MA_BEP.Caption = "May in bep";
            this._MA_BEP.FieldName = "_MA_BEP";
            this._MA_BEP.Name = "_MA_BEP";
            // 
            // pnUpDow
            // 
            this.pnUpDow.Controls.Add(this.labelControl2);
            this.pnUpDow.Controls.Add(this.btn_OkThrough);
            this.pnUpDow.Controls.Add(this.btn_ToLeftAll);
            this.pnUpDow.Controls.Add(this.btn_ToRightAll);
            this.pnUpDow.Controls.Add(this.btn_UpDivide);
            this.pnUpDow.Controls.Add(this.btn_DowDivide);
            this.pnUpDow.Controls.Add(this.btn_ESCThroughtTable);
            this.pnUpDow.Controls.Add(this.btn_ToRight);
            this.pnUpDow.Controls.Add(this.btn_Toleft);
            this.pnUpDow.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnUpDow.Location = new System.Drawing.Point(358, 2);
            this.pnUpDow.Name = "pnUpDow";
            this.pnUpDow.Size = new System.Drawing.Size(105, 551);
            this.pnUpDow.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl2.Location = new System.Drawing.Point(3, 267);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(100, 13);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "--------------------";
            // 
            // btn_OkThrough
            // 
            this.btn_OkThrough.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn_OkThrough.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btn_OkThrough.Appearance.Options.UseFont = true;
            this.btn_OkThrough.Appearance.Options.UseForeColor = true;
            this.btn_OkThrough.Location = new System.Drawing.Point(7, 64);
            this.btn_OkThrough.Name = "btn_OkThrough";
            this.btn_OkThrough.Size = new System.Drawing.Size(93, 50);
            this.btn_OkThrough.TabIndex = 0;
            this.btn_OkThrough.Text = "OK";
            this.btn_OkThrough.Click += new System.EventHandler(this.btn_OkThrough_Click);
            // 
            // btn_ToLeftAll
            // 
            this.btn_ToLeftAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_ToLeftAll.Image = ((System.Drawing.Image)(resources.GetObject("btn_ToLeftAll.Image")));
            this.btn_ToLeftAll.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_ToLeftAll.Location = new System.Drawing.Point(7, 376);
            this.btn_ToLeftAll.Name = "btn_ToLeftAll";
            this.btn_ToLeftAll.Size = new System.Drawing.Size(93, 50);
            this.btn_ToLeftAll.TabIndex = 4;
            this.btn_ToLeftAll.Text = "<<";
            this.btn_ToLeftAll.Click += new System.EventHandler(this.btn_ToLeftAll_Click);
            // 
            // btn_ToRightAll
            // 
            this.btn_ToRightAll.Image = ((System.Drawing.Image)(resources.GetObject("btn_ToRightAll.Image")));
            this.btn_ToRightAll.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_ToRightAll.Location = new System.Drawing.Point(7, 122);
            this.btn_ToRightAll.Name = "btn_ToRightAll";
            this.btn_ToRightAll.Size = new System.Drawing.Size(93, 50);
            this.btn_ToRightAll.TabIndex = 1;
            this.btn_ToRightAll.Text = ">>";
            this.btn_ToRightAll.Click += new System.EventHandler(this.btn_ToRightAll_Click);
            // 
            // btn_UpDivide
            // 
            this.btn_UpDivide.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_UpDivide.Image = ((System.Drawing.Image)(resources.GetObject("btn_UpDivide.Image")));
            this.btn_UpDivide.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_UpDivide.Location = new System.Drawing.Point(2, 2);
            this.btn_UpDivide.Name = "btn_UpDivide";
            this.btn_UpDivide.Size = new System.Drawing.Size(101, 54);
            this.btn_UpDivide.TabIndex = 11;
            this.btn_UpDivide.Text = "simpleButton7";
            this.btn_UpDivide.Click += new System.EventHandler(this.btn_UpDivide_Click);
            // 
            // btn_DowDivide
            // 
            this.btn_DowDivide.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_DowDivide.Image = ((System.Drawing.Image)(resources.GetObject("btn_DowDivide.Image")));
            this.btn_DowDivide.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_DowDivide.Location = new System.Drawing.Point(2, 495);
            this.btn_DowDivide.Name = "btn_DowDivide";
            this.btn_DowDivide.Size = new System.Drawing.Size(101, 54);
            this.btn_DowDivide.TabIndex = 10;
            this.btn_DowDivide.Text = "simpleButton6";
            this.btn_DowDivide.Click += new System.EventHandler(this.btn_DowDivide_Click);
            // 
            // btn_ESCThroughtTable
            // 
            this.btn_ESCThroughtTable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_ESCThroughtTable.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn_ESCThroughtTable.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btn_ESCThroughtTable.Appearance.Options.UseFont = true;
            this.btn_ESCThroughtTable.Appearance.Options.UseForeColor = true;
            this.btn_ESCThroughtTable.Location = new System.Drawing.Point(7, 434);
            this.btn_ESCThroughtTable.Name = "btn_ESCThroughtTable";
            this.btn_ESCThroughtTable.Size = new System.Drawing.Size(93, 50);
            this.btn_ESCThroughtTable.TabIndex = 5;
            this.btn_ESCThroughtTable.Text = "ESC";
            this.btn_ESCThroughtTable.Click += new System.EventHandler(this.btn_ESCThroughtTable_Click);
            // 
            // btn_ToRight
            // 
            this.btn_ToRight.Image = ((System.Drawing.Image)(resources.GetObject("btn_ToRight.Image")));
            this.btn_ToRight.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_ToRight.Location = new System.Drawing.Point(7, 178);
            this.btn_ToRight.Name = "btn_ToRight";
            this.btn_ToRight.Size = new System.Drawing.Size(93, 50);
            this.btn_ToRight.TabIndex = 2;
            this.btn_ToRight.Text = ">";
            this.btn_ToRight.Click += new System.EventHandler(this.btn_ToRight_Click);
            // 
            // btn_Toleft
            // 
            this.btn_Toleft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Toleft.Image = ((System.Drawing.Image)(resources.GetObject("btn_Toleft.Image")));
            this.btn_Toleft.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_Toleft.Location = new System.Drawing.Point(7, 318);
            this.btn_Toleft.Name = "btn_Toleft";
            this.btn_Toleft.Size = new System.Drawing.Size(93, 50);
            this.btn_Toleft.TabIndex = 3;
            this.btn_Toleft.Text = "<";
            this.btn_Toleft.Click += new System.EventHandler(this.btn_Toleft_Click);
            // 
            // pn_TableTow
            // 
            this.pn_TableTow.Controls.Add(this.gr_divide_Table_2);
            this.pn_TableTow.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn_TableTow.Location = new System.Drawing.Point(463, 2);
            this.pn_TableTow.Name = "pn_TableTow";
            this.pn_TableTow.Size = new System.Drawing.Size(209, 551);
            this.pn_TableTow.TabIndex = 1;
            // 
            // gr_divide_Table_2
            // 
            this.gr_divide_Table_2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode3.RelationName = "Level1";
            this.gr_divide_Table_2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode3});
            this.gr_divide_Table_2.Location = new System.Drawing.Point(2, 2);
            this.gr_divide_Table_2.MainView = this.gv_divide_Table_2;
            this.gr_divide_Table_2.Name = "gr_divide_Table_2";
            this.gr_divide_Table_2.Size = new System.Drawing.Size(205, 547);
            this.gr_divide_Table_2.TabIndex = 21;
            this.gr_divide_Table_2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv_divide_Table_2});
            // 
            // gv_divide_Table_2
            // 
            this.gv_divide_Table_2.ColumnPanelRowHeight = 30;
            this.gv_divide_Table_2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.@__STT,
            this.@__TEN_HANGHOA,
            this.@__SOLUONG,
            this.@__GIABAN,
            this.@__Thanh_Tien,
            this.@__MA_HANGHOA,
            this.@__GHICHU,
            this.@__MA_BEP});
            this.gv_divide_Table_2.GridControl = this.gr_divide_Table_2;
            this.gv_divide_Table_2.Name = "gv_divide_Table_2";
            this.gv_divide_Table_2.OptionsView.ShowGroupPanel = false;
            this.gv_divide_Table_2.OptionsView.ShowIndicator = false;
            this.gv_divide_Table_2.RowHeight = 35;
            this.gv_divide_Table_2.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gv_divide_Table_2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gv_divide_Table_2_CustomDrawCell);
            // 
            // __STT
            // 
            this.@__STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.@__STT.AppearanceCell.Options.UseFont = true;
            this.@__STT.AppearanceCell.Options.UseTextOptions = true;
            this.@__STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__STT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.@__STT.AppearanceHeader.Options.UseFont = true;
            this.@__STT.AppearanceHeader.Options.UseTextOptions = true;
            this.@__STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__STT.Caption = "TT";
            this.@__STT.FieldName = "__STT";
            this.@__STT.Name = "__STT";
            this.@__STT.OptionsColumn.AllowEdit = false;
            this.@__STT.OptionsColumn.AllowFocus = false;
            this.@__STT.Visible = true;
            this.@__STT.VisibleIndex = 0;
            this.@__STT.Width = 34;
            // 
            // __TEN_HANGHOA
            // 
            this.@__TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.@__TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.@__TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.@__TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.@__TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.@__TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__TEN_HANGHOA.Caption = "Tên Hàng Hóa";
            this.@__TEN_HANGHOA.FieldName = "__TEN_HANGHOA";
            this.@__TEN_HANGHOA.Name = "__TEN_HANGHOA";
            this.@__TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this.@__TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this.@__TEN_HANGHOA.Visible = true;
            this.@__TEN_HANGHOA.VisibleIndex = 1;
            this.@__TEN_HANGHOA.Width = 116;
            // 
            // __SOLUONG
            // 
            this.@__SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.@__SOLUONG.AppearanceCell.Options.UseFont = true;
            this.@__SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.@__SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.@__SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.@__SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.@__SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__SOLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__SOLUONG.Caption = "SL";
            this.@__SOLUONG.FieldName = "__SOLUONG";
            this.@__SOLUONG.Name = "__SOLUONG";
            this.@__SOLUONG.OptionsColumn.AllowEdit = false;
            this.@__SOLUONG.OptionsColumn.AllowFocus = false;
            this.@__SOLUONG.Visible = true;
            this.@__SOLUONG.VisibleIndex = 2;
            this.@__SOLUONG.Width = 51;
            // 
            // __GIABAN
            // 
            this.@__GIABAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.@__GIABAN.AppearanceCell.Options.UseFont = true;
            this.@__GIABAN.AppearanceCell.Options.UseTextOptions = true;
            this.@__GIABAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__GIABAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__GIABAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.@__GIABAN.AppearanceHeader.Options.UseFont = true;
            this.@__GIABAN.AppearanceHeader.Options.UseTextOptions = true;
            this.@__GIABAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__GIABAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__GIABAN.Caption = "Đơn giá";
            this.@__GIABAN.DisplayFormat.FormatString = "{0:#,###0}";
            this.@__GIABAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.@__GIABAN.FieldName = "__GIABAN";
            this.@__GIABAN.Name = "__GIABAN";
            this.@__GIABAN.OptionsColumn.AllowEdit = false;
            this.@__GIABAN.OptionsColumn.AllowFocus = false;
            this.@__GIABAN.Visible = true;
            this.@__GIABAN.VisibleIndex = 3;
            this.@__GIABAN.Width = 96;
            // 
            // __Thanh_Tien
            // 
            this.@__Thanh_Tien.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.@__Thanh_Tien.AppearanceCell.Options.UseFont = true;
            this.@__Thanh_Tien.AppearanceCell.Options.UseTextOptions = true;
            this.@__Thanh_Tien.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__Thanh_Tien.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__Thanh_Tien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.@__Thanh_Tien.AppearanceHeader.Options.UseFont = true;
            this.@__Thanh_Tien.AppearanceHeader.Options.UseTextOptions = true;
            this.@__Thanh_Tien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__Thanh_Tien.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__Thanh_Tien.Caption = "Thành Tiền";
            this.@__Thanh_Tien.DisplayFormat.FormatString = "{0:#,###0}";
            this.@__Thanh_Tien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.@__Thanh_Tien.FieldName = "__Thanh_Tien";
            this.@__Thanh_Tien.Name = "__Thanh_Tien";
            this.@__Thanh_Tien.OptionsColumn.AllowEdit = false;
            this.@__Thanh_Tien.OptionsColumn.AllowFocus = false;
            this.@__Thanh_Tien.Visible = true;
            this.@__Thanh_Tien.VisibleIndex = 4;
            this.@__Thanh_Tien.Width = 104;
            // 
            // __MA_HANGHOA
            // 
            this.@__MA_HANGHOA.Caption = "Mã Hàng Hóa";
            this.@__MA_HANGHOA.FieldName = "__MA_HANGHOA";
            this.@__MA_HANGHOA.Name = "__MA_HANGHOA";
            this.@__MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this.@__MA_HANGHOA.OptionsColumn.AllowFocus = false;
            // 
            // __GHICHU
            // 
            this.@__GHICHU.Caption = "Ghi Chú";
            this.@__GHICHU.FieldName = "__GHICHU";
            this.@__GHICHU.Name = "__GHICHU";
            // 
            // __MA_BEP
            // 
            this.@__MA_BEP.Caption = "MÃ BẾP";
            this.@__MA_BEP.FieldName = "__MA_BEP";
            this.@__MA_BEP.Name = "__MA_BEP";
            // 
            // pnInforTablecheck
            // 
            this.pnInforTablecheck.Controls.Add(this.panelControl14);
            this.pnInforTablecheck.Controls.Add(this.panelControl18);
            this.pnInforTablecheck.Controls.Add(this.pn_TableShare);
            this.pnInforTablecheck.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnInforTablecheck.Location = new System.Drawing.Point(0, 0);
            this.pnInforTablecheck.Name = "pnInforTablecheck";
            this.pnInforTablecheck.Size = new System.Drawing.Size(674, 143);
            this.pnInforTablecheck.TabIndex = 2;
            // 
            // panelControl14
            // 
            this.panelControl14.Controls.Add(this.txtTenKhacHang);
            this.panelControl14.Controls.Add(this.txtmaBanGoc);
            this.panelControl14.Controls.Add(this.txtmaKVBanGoc);
            this.panelControl14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl14.Location = new System.Drawing.Point(2, 2);
            this.panelControl14.Name = "panelControl14";
            this.panelControl14.Size = new System.Drawing.Size(356, 139);
            this.panelControl14.TabIndex = 0;
            // 
            // txtTenKhacHang
            // 
            this.txtTenKhacHang.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTenKhacHang.Enabled = false;
            this.txtTenKhacHang.Location = new System.Drawing.Point(7, 95);
            this.txtTenKhacHang.Name = "txtTenKhacHang";
            this.txtTenKhacHang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtTenKhacHang.Properties.Appearance.Options.UseFont = true;
            this.txtTenKhacHang.Size = new System.Drawing.Size(344, 30);
            this.txtTenKhacHang.TabIndex = 5;
            // 
            // txtmaBanGoc
            // 
            this.txtmaBanGoc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtmaBanGoc.Enabled = false;
            this.txtmaBanGoc.Location = new System.Drawing.Point(7, 57);
            this.txtmaBanGoc.Name = "txtmaBanGoc";
            this.txtmaBanGoc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtmaBanGoc.Properties.Appearance.Options.UseFont = true;
            this.txtmaBanGoc.Size = new System.Drawing.Size(344, 30);
            this.txtmaBanGoc.TabIndex = 4;
            // 
            // txtmaKVBanGoc
            // 
            this.txtmaKVBanGoc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtmaKVBanGoc.Enabled = false;
            this.txtmaKVBanGoc.Location = new System.Drawing.Point(7, 13);
            this.txtmaKVBanGoc.Name = "txtmaKVBanGoc";
            this.txtmaKVBanGoc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtmaKVBanGoc.Properties.Appearance.Options.UseFont = true;
            this.txtmaKVBanGoc.Size = new System.Drawing.Size(344, 30);
            this.txtmaKVBanGoc.TabIndex = 3;
            // 
            // panelControl18
            // 
            this.panelControl18.Controls.Add(this.labelControl10);
            this.panelControl18.Controls.Add(this.labelControl9);
            this.panelControl18.Controls.Add(this.labelControl8);
            this.panelControl18.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl18.Location = new System.Drawing.Point(358, 2);
            this.panelControl18.Name = "panelControl18";
            this.panelControl18.Size = new System.Drawing.Size(105, 139);
            this.panelControl18.TabIndex = 2;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl10.Location = new System.Drawing.Point(20, 101);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(59, 19);
            this.labelControl10.TabIndex = 2;
            this.labelControl10.Text = "Tên KH";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl9.Location = new System.Drawing.Point(16, 20);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(66, 19);
            this.labelControl9.TabIndex = 1;
            this.labelControl9.Text = "Khu Vực";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl8.Location = new System.Drawing.Point(23, 61);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(51, 19);
            this.labelControl8.TabIndex = 0;
            this.labelControl8.Text = "Phòng";
            // 
            // pn_TableShare
            // 
            this.pn_TableShare.Controls.Add(this.txtTenKhachHangGhep);
            this.pn_TableShare.Controls.Add(this.txtmaBanGhep);
            this.pn_TableShare.Controls.Add(this.txtmaKVBanGhep);
            this.pn_TableShare.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn_TableShare.Location = new System.Drawing.Point(463, 2);
            this.pn_TableShare.Name = "pn_TableShare";
            this.pn_TableShare.Size = new System.Drawing.Size(209, 139);
            this.pn_TableShare.TabIndex = 1;
            // 
            // txtTenKhachHangGhep
            // 
            this.txtTenKhachHangGhep.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTenKhachHangGhep.Enabled = false;
            this.txtTenKhachHangGhep.Location = new System.Drawing.Point(7, 94);
            this.txtTenKhachHangGhep.Name = "txtTenKhachHangGhep";
            this.txtTenKhachHangGhep.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtTenKhachHangGhep.Properties.Appearance.Options.UseFont = true;
            this.txtTenKhachHangGhep.Size = new System.Drawing.Size(197, 30);
            this.txtTenKhachHangGhep.TabIndex = 2;
            // 
            // txtmaBanGhep
            // 
            this.txtmaBanGhep.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtmaBanGhep.Enabled = false;
            this.txtmaBanGhep.Location = new System.Drawing.Point(7, 56);
            this.txtmaBanGhep.Name = "txtmaBanGhep";
            this.txtmaBanGhep.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtmaBanGhep.Properties.Appearance.Options.UseFont = true;
            this.txtmaBanGhep.Size = new System.Drawing.Size(197, 30);
            this.txtmaBanGhep.TabIndex = 1;
            // 
            // txtmaKVBanGhep
            // 
            this.txtmaKVBanGhep.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtmaKVBanGhep.Enabled = false;
            this.txtmaKVBanGhep.Location = new System.Drawing.Point(7, 12);
            this.txtmaKVBanGhep.Name = "txtmaKVBanGhep";
            this.txtmaKVBanGhep.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtmaKVBanGhep.Properties.Appearance.Options.UseFont = true;
            this.txtmaKVBanGhep.Size = new System.Drawing.Size(197, 30);
            this.txtmaKVBanGhep.TabIndex = 0;
            // 
            // pnYeucauthem
            // 
            this.pnYeucauthem.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnYeucauthem.Controls.Add(this.panelControl19);
            this.pnYeucauthem.Controls.Add(this.panelControl5);
            this.pnYeucauthem.Controls.Add(this.panelControl21);
            this.pnYeucauthem.Controls.Add(this.panelControl3);
            this.pnYeucauthem.Controls.Add(this.panelControl16);
            this.pnYeucauthem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnYeucauthem.Location = new System.Drawing.Point(0, 0);
            this.pnYeucauthem.Name = "pnYeucauthem";
            this.pnYeucauthem.Size = new System.Drawing.Size(674, 698);
            this.pnYeucauthem.TabIndex = 8;
            // 
            // panelControl19
            // 
            this.panelControl19.Controls.Add(this.pnYeuCau);
            this.panelControl19.Controls.Add(this.panelControl22);
            this.panelControl19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl19.Location = new System.Drawing.Point(0, 112);
            this.panelControl19.Name = "panelControl19";
            this.panelControl19.Size = new System.Drawing.Size(661, 366);
            this.panelControl19.TabIndex = 3;
            // 
            // pnYeuCau
            // 
            this.pnYeuCau.AutoScroll = true;
            this.pnYeuCau.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnYeuCau.Location = new System.Drawing.Point(2, 2);
            this.pnYeuCau.Name = "pnYeuCau";
            this.pnYeuCau.Size = new System.Drawing.Size(624, 362);
            this.pnYeuCau.TabIndex = 1;
            // 
            // panelControl22
            // 
            this.panelControl22.Controls.Add(this.simpleButton23);
            this.panelControl22.Controls.Add(this.simpleButton24);
            this.panelControl22.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl22.Location = new System.Drawing.Point(626, 2);
            this.panelControl22.Name = "panelControl22";
            this.panelControl22.Size = new System.Drawing.Size(33, 362);
            this.panelControl22.TabIndex = 0;
            // 
            // simpleButton23
            // 
            this.simpleButton23.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.simpleButton23.Image = global::KP_RES.Properties.Resources.up11_26;
            this.simpleButton23.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton23.Location = new System.Drawing.Point(2, 289);
            this.simpleButton23.Name = "simpleButton23";
            this.simpleButton23.Size = new System.Drawing.Size(29, 71);
            this.simpleButton23.TabIndex = 7;
            this.simpleButton23.Text = ">";
            this.simpleButton23.Click += new System.EventHandler(this.simpleButton23_Click);
            // 
            // simpleButton24
            // 
            this.simpleButton24.Dock = System.Windows.Forms.DockStyle.Top;
            this.simpleButton24.Image = global::KP_RES.Properties.Resources.up22_26;
            this.simpleButton24.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton24.Location = new System.Drawing.Point(2, 2);
            this.simpleButton24.Name = "simpleButton24";
            this.simpleButton24.Size = new System.Drawing.Size(29, 71);
            this.simpleButton24.TabIndex = 8;
            this.simpleButton24.Text = "<";
            this.simpleButton24.Click += new System.EventHandler(this.simpleButton24_Click);
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.us_keyboard3);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl5.Location = new System.Drawing.Point(0, 478);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(661, 220);
            this.panelControl5.TabIndex = 80;
            // 
            // us_keyboard3
            // 
            this.us_keyboard3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.us_keyboard3.Location = new System.Drawing.Point(5, 4);
            this.us_keyboard3.Name = "us_keyboard3";
            this.us_keyboard3.Size = new System.Drawing.Size(650, 223);
            this.us_keyboard3.TabIndex = 0;
            // 
            // panelControl21
            // 
            this.panelControl21.Controls.Add(this.txtYeuCauThem);
            this.panelControl21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl21.Location = new System.Drawing.Point(0, 45);
            this.panelControl21.Name = "panelControl21";
            this.panelControl21.Size = new System.Drawing.Size(661, 67);
            this.panelControl21.TabIndex = 0;
            // 
            // txtYeuCauThem
            // 
            this.txtYeuCauThem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtYeuCauThem.Location = new System.Drawing.Point(15, 13);
            this.txtYeuCauThem.Name = "txtYeuCauThem";
            this.txtYeuCauThem.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.txtYeuCauThem.Properties.Appearance.Options.UseFont = true;
            this.txtYeuCauThem.Size = new System.Drawing.Size(636, 40);
            this.txtYeuCauThem.TabIndex = 1;
            this.txtYeuCauThem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtYeuCauThem_KeyDown);
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.panelControl24);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(661, 45);
            this.panelControl3.TabIndex = 81;
            // 
            // panelControl24
            // 
            this.panelControl24.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl24.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl24.Controls.Add(this.lbkvYeuCauThem);
            this.panelControl24.Controls.Add(this.lbTableYeuCauThem);
            this.panelControl24.Controls.Add(this.lbMonYeuCauThem);
            this.panelControl24.Controls.Add(this.labelControl18);
            this.panelControl24.Location = new System.Drawing.Point(2, 12);
            this.panelControl24.Name = "panelControl24";
            this.panelControl24.Size = new System.Drawing.Size(647, 31);
            this.panelControl24.TabIndex = 0;
            // 
            // lbkvYeuCauThem
            // 
            this.lbkvYeuCauThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbkvYeuCauThem.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbkvYeuCauThem.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbkvYeuCauThem.Location = new System.Drawing.Point(335, 0);
            this.lbkvYeuCauThem.Name = "lbkvYeuCauThem";
            this.lbkvYeuCauThem.Size = new System.Drawing.Size(130, 19);
            this.lbkvYeuCauThem.TabIndex = 0;
            this.lbkvYeuCauThem.Text = "Khu Vực 1         >";
            // 
            // lbTableYeuCauThem
            // 
            this.lbTableYeuCauThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbTableYeuCauThem.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbTableYeuCauThem.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbTableYeuCauThem.Location = new System.Drawing.Point(465, 0);
            this.lbTableYeuCauThem.Name = "lbTableYeuCauThem";
            this.lbTableYeuCauThem.Size = new System.Drawing.Size(82, 19);
            this.lbTableYeuCauThem.TabIndex = 1;
            this.lbTableYeuCauThem.Text = "    Bàn 1  >";
            // 
            // lbMonYeuCauThem
            // 
            this.lbMonYeuCauThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbMonYeuCauThem.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbMonYeuCauThem.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbMonYeuCauThem.Location = new System.Drawing.Point(547, 0);
            this.lbMonYeuCauThem.Name = "lbMonYeuCauThem";
            this.lbMonYeuCauThem.Size = new System.Drawing.Size(100, 19);
            this.lbMonYeuCauThem.TabIndex = 2;
            this.lbMonYeuCauThem.Text = "     Lẩu Bò Tái";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl18.Location = new System.Drawing.Point(0, 0);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(111, 19);
            this.labelControl18.TabIndex = 3;
            this.labelControl18.Text = "Yêu Cầu Thêm:";
            // 
            // panelControl16
            // 
            this.panelControl16.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl16.Location = new System.Drawing.Point(661, 0);
            this.panelControl16.Name = "panelControl16";
            this.panelControl16.Size = new System.Drawing.Size(13, 698);
            this.panelControl16.TabIndex = 0;
            // 
            // pnInlaihoadon
            // 
            this.pnInlaihoadon.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnInlaihoadon.Controls.Add(this.panelControl53);
            this.pnInlaihoadon.Controls.Add(this.panelControl52);
            this.pnInlaihoadon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnInlaihoadon.Location = new System.Drawing.Point(0, 0);
            this.pnInlaihoadon.Name = "pnInlaihoadon";
            this.pnInlaihoadon.Size = new System.Drawing.Size(674, 698);
            this.pnInlaihoadon.TabIndex = 2;
            // 
            // panelControl53
            // 
            this.panelControl53.Controls.Add(this.gr_billPaymented);
            this.panelControl53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl53.Location = new System.Drawing.Point(0, 0);
            this.panelControl53.Name = "panelControl53";
            this.panelControl53.Size = new System.Drawing.Size(634, 698);
            this.panelControl53.TabIndex = 2;
            // 
            // gr_billPaymented
            // 
            this.gr_billPaymented.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gr_billPaymented.Location = new System.Drawing.Point(2, 2);
            this.gr_billPaymented.MainView = this.gvBillPaymented;
            this.gr_billPaymented.Name = "gr_billPaymented";
            this.gr_billPaymented.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit2,
            this.repositoryItemButtonEdit3,
            this.repositoryItemButtonEdit4});
            this.gr_billPaymented.Size = new System.Drawing.Size(630, 694);
            this.gr_billPaymented.TabIndex = 3;
            this.gr_billPaymented.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvBillPaymented});
            // 
            // gvBillPaymented
            // 
            this.gvBillPaymented.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.gvBillPaymented.Appearance.FocusedRow.Options.UseFont = true;
            this.gvBillPaymented.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.gvBillPaymented.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvBillPaymented.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.75F);
            this.gvBillPaymented.Appearance.Row.Options.UseFont = true;
            this.gvBillPaymented.ColumnPanelRowHeight = 45;
            this.gvBillPaymented.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SSTT,
            this.MA_HOADON,
            this.NGAYTAO,
            this.TEN_KHUVUC,
            this.TONGTIEN,
            this.TEN_BAN,
            this.IN,
            this.HUY,
            this.LAYLAI});
            this.gvBillPaymented.GridControl = this.gr_billPaymented;
            this.gvBillPaymented.Name = "gvBillPaymented";
            this.gvBillPaymented.OptionsView.EnableAppearanceEvenRow = true;
            this.gvBillPaymented.OptionsView.ShowGroupPanel = false;
            this.gvBillPaymented.OptionsView.ShowIndicator = false;
            this.gvBillPaymented.RowHeight = 45;
            this.gvBillPaymented.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gvBillPaymented.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvBillPaymented_RowCellClick);
            this.gvBillPaymented.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gvBillPaymented_CustomDrawCell);
            // 
            // SSTT
            // 
            this.SSTT.AppearanceCell.Options.UseTextOptions = true;
            this.SSTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SSTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SSTT.AppearanceHeader.Options.UseFont = true;
            this.SSTT.AppearanceHeader.Options.UseTextOptions = true;
            this.SSTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SSTT.Caption = "STT";
            this.SSTT.FieldName = "SSTT";
            this.SSTT.MinWidth = 40;
            this.SSTT.Name = "SSTT";
            this.SSTT.OptionsColumn.AllowEdit = false;
            this.SSTT.OptionsColumn.AllowFocus = false;
            this.SSTT.OptionsFilter.AllowAutoFilter = false;
            this.SSTT.OptionsFilter.AllowFilter = false;
            this.SSTT.Visible = true;
            this.SSTT.VisibleIndex = 0;
            this.SSTT.Width = 40;
            // 
            // MA_HOADON
            // 
            this.MA_HOADON.AppearanceCell.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_HOADON.AppearanceHeader.Options.UseFont = true;
            this.MA_HOADON.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.Caption = "Mã Hóa Đơn";
            this.MA_HOADON.FieldName = "MA_HOADON";
            this.MA_HOADON.MaxWidth = 150;
            this.MA_HOADON.MinWidth = 135;
            this.MA_HOADON.Name = "MA_HOADON";
            this.MA_HOADON.OptionsColumn.AllowEdit = false;
            this.MA_HOADON.OptionsColumn.AllowFocus = false;
            this.MA_HOADON.Visible = true;
            this.MA_HOADON.VisibleIndex = 1;
            this.MA_HOADON.Width = 135;
            // 
            // NGAYTAO
            // 
            this.NGAYTAO.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYTAO.AppearanceHeader.Options.UseFont = true;
            this.NGAYTAO.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.Caption = "Ngày Tạo";
            this.NGAYTAO.DisplayFormat.FormatString = "{0:dd/MM/yyyy HH:mm}";
            this.NGAYTAO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYTAO.FieldName = "NGAYTAO";
            this.NGAYTAO.MaxWidth = 150;
            this.NGAYTAO.MinWidth = 135;
            this.NGAYTAO.Name = "NGAYTAO";
            this.NGAYTAO.OptionsColumn.AllowEdit = false;
            this.NGAYTAO.OptionsColumn.AllowFocus = false;
            this.NGAYTAO.Visible = true;
            this.NGAYTAO.VisibleIndex = 4;
            this.NGAYTAO.Width = 135;
            // 
            // TEN_KHUVUC
            // 
            this.TEN_KHUVUC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_KHUVUC.AppearanceHeader.Options.UseFont = true;
            this.TEN_KHUVUC.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_KHUVUC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_KHUVUC.Caption = "Lầu";
            this.TEN_KHUVUC.FieldName = "TEN_KHUVUC";
            this.TEN_KHUVUC.MinWidth = 70;
            this.TEN_KHUVUC.Name = "TEN_KHUVUC";
            this.TEN_KHUVUC.OptionsColumn.AllowEdit = false;
            this.TEN_KHUVUC.OptionsColumn.AllowFocus = false;
            this.TEN_KHUVUC.OptionsFilter.AllowAutoFilter = false;
            this.TEN_KHUVUC.OptionsFilter.AllowFilter = false;
            this.TEN_KHUVUC.Visible = true;
            this.TEN_KHUVUC.VisibleIndex = 2;
            this.TEN_KHUVUC.Width = 70;
            // 
            // TONGTIEN
            // 
            this.TONGTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TONGTIEN.AppearanceHeader.Options.UseFont = true;
            this.TONGTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGTIEN.Caption = "Tổng Tiền";
            this.TONGTIEN.DisplayFormat.FormatString = "N0";
            this.TONGTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TONGTIEN.FieldName = "TONGTIEN";
            this.TONGTIEN.MaxWidth = 120;
            this.TONGTIEN.MinWidth = 70;
            this.TONGTIEN.Name = "TONGTIEN";
            this.TONGTIEN.OptionsColumn.AllowEdit = false;
            this.TONGTIEN.OptionsColumn.AllowFocus = false;
            this.TONGTIEN.Visible = true;
            this.TONGTIEN.VisibleIndex = 5;
            this.TONGTIEN.Width = 70;
            // 
            // TEN_BAN
            // 
            this.TEN_BAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_BAN.AppearanceHeader.Options.UseFont = true;
            this.TEN_BAN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_BAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_BAN.Caption = "Phòng";
            this.TEN_BAN.FieldName = "TEN_BAN";
            this.TEN_BAN.MinWidth = 70;
            this.TEN_BAN.Name = "TEN_BAN";
            this.TEN_BAN.OptionsColumn.AllowEdit = false;
            this.TEN_BAN.OptionsColumn.AllowFocus = false;
            this.TEN_BAN.OptionsFilter.AllowAutoFilter = false;
            this.TEN_BAN.OptionsFilter.AllowFilter = false;
            this.TEN_BAN.Visible = true;
            this.TEN_BAN.VisibleIndex = 3;
            this.TEN_BAN.Width = 70;
            // 
            // IN
            // 
            this.IN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IN.AppearanceHeader.Options.UseFont = true;
            this.IN.AppearanceHeader.Options.UseTextOptions = true;
            this.IN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.IN.Caption = "In";
            this.IN.ColumnEdit = this.repositoryItemButtonEdit2;
            this.IN.MaxWidth = 100;
            this.IN.MinWidth = 70;
            this.IN.Name = "IN";
            this.IN.OptionsColumn.AllowEdit = false;
            this.IN.OptionsColumn.AllowFocus = false;
            this.IN.OptionsColumn.FixedWidth = true;
            this.IN.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.IN.Visible = true;
            this.IN.VisibleIndex = 6;
            this.IN.Width = 70;
            // 
            // repositoryItemButtonEdit2
            // 
            this.repositoryItemButtonEdit2.AutoHeight = false;
            this.repositoryItemButtonEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit2.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
            this.repositoryItemButtonEdit2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // HUY
            // 
            this.HUY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HUY.AppearanceHeader.Options.UseFont = true;
            this.HUY.AppearanceHeader.Options.UseTextOptions = true;
            this.HUY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HUY.Caption = "Hủy";
            this.HUY.ColumnEdit = this.repositoryItemButtonEdit3;
            this.HUY.MaxWidth = 100;
            this.HUY.MinWidth = 70;
            this.HUY.Name = "HUY";
            this.HUY.OptionsColumn.AllowEdit = false;
            this.HUY.OptionsColumn.AllowFocus = false;
            this.HUY.OptionsColumn.FixedWidth = true;
            this.HUY.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.HUY.Visible = true;
            this.HUY.VisibleIndex = 7;
            this.HUY.Width = 70;
            // 
            // repositoryItemButtonEdit3
            // 
            this.repositoryItemButtonEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit3.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.repositoryItemButtonEdit3.Name = "repositoryItemButtonEdit3";
            this.repositoryItemButtonEdit3.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // LAYLAI
            // 
            this.LAYLAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LAYLAI.AppearanceHeader.Options.UseFont = true;
            this.LAYLAI.AppearanceHeader.Options.UseTextOptions = true;
            this.LAYLAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LAYLAI.Caption = "Lấy lại HĐ";
            this.LAYLAI.ColumnEdit = this.repositoryItemButtonEdit4;
            this.LAYLAI.MaxWidth = 100;
            this.LAYLAI.MinWidth = 70;
            this.LAYLAI.Name = "LAYLAI";
            this.LAYLAI.OptionsColumn.AllowEdit = false;
            this.LAYLAI.OptionsColumn.AllowFocus = false;
            this.LAYLAI.OptionsColumn.FixedWidth = true;
            this.LAYLAI.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.LAYLAI.Visible = true;
            this.LAYLAI.VisibleIndex = 8;
            this.LAYLAI.Width = 70;
            // 
            // repositoryItemButtonEdit4
            // 
            this.repositoryItemButtonEdit4.AutoHeight = false;
            this.repositoryItemButtonEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit4.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", null, null, true)});
            this.repositoryItemButtonEdit4.Name = "repositoryItemButtonEdit4";
            this.repositoryItemButtonEdit4.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // panelControl52
            // 
            this.panelControl52.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl52.Controls.Add(this.btnDowBill);
            this.panelControl52.Controls.Add(this.btnDowLastBill);
            this.panelControl52.Controls.Add(this.btnThoatReprint);
            this.panelControl52.Controls.Add(this.labelControl43);
            this.panelControl52.Controls.Add(this.lbTongSoRow);
            this.panelControl52.Controls.Add(this.labelControl45);
            this.panelControl52.Controls.Add(this.btn_upbill);
            this.panelControl52.Controls.Add(this.btn_UpTopBill);
            this.panelControl52.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl52.Location = new System.Drawing.Point(634, 0);
            this.panelControl52.Name = "panelControl52";
            this.panelControl52.Size = new System.Drawing.Size(40, 698);
            this.panelControl52.TabIndex = 1;
            // 
            // btnDowBill
            // 
            this.btnDowBill.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnDowBill.Image = ((System.Drawing.Image)(resources.GetObject("btnDowBill.Image")));
            this.btnDowBill.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDowBill.Location = new System.Drawing.Point(0, 458);
            this.btnDowBill.Name = "btnDowBill";
            this.btnDowBill.Size = new System.Drawing.Size(40, 80);
            this.btnDowBill.TabIndex = 10;
            this.btnDowBill.Click += new System.EventHandler(this.btnDowBill_Click);
            // 
            // btnDowLastBill
            // 
            this.btnDowLastBill.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnDowLastBill.Image = ((System.Drawing.Image)(resources.GetObject("btnDowLastBill.Image")));
            this.btnDowLastBill.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDowLastBill.Location = new System.Drawing.Point(0, 538);
            this.btnDowLastBill.Name = "btnDowLastBill";
            this.btnDowLastBill.Size = new System.Drawing.Size(40, 80);
            this.btnDowLastBill.TabIndex = 9;
            this.btnDowLastBill.Click += new System.EventHandler(this.btnDowLastBill_Click);
            // 
            // btnThoatReprint
            // 
            this.btnThoatReprint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnThoatReprint.Appearance.Options.UseFont = true;
            this.btnThoatReprint.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnThoatReprint.Image = ((System.Drawing.Image)(resources.GetObject("btnThoatReprint.Image")));
            this.btnThoatReprint.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnThoatReprint.Location = new System.Drawing.Point(0, 618);
            this.btnThoatReprint.Name = "btnThoatReprint";
            this.btnThoatReprint.Size = new System.Drawing.Size(40, 80);
            this.btnThoatReprint.TabIndex = 30;
            this.btnThoatReprint.Text = "Đóng";
            this.btnThoatReprint.Visible = false;
            this.btnThoatReprint.Click += new System.EventHandler(this.btnThoatReprint_Click);
            // 
            // labelControl43
            // 
            this.labelControl43.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl43.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl43.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl43.Location = new System.Drawing.Point(9, 323);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(20, 13);
            this.labelControl43.TabIndex = 14;
            this.labelControl43.Text = "----";
            // 
            // lbTongSoRow
            // 
            this.lbTongSoRow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTongSoRow.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbTongSoRow.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbTongSoRow.Location = new System.Drawing.Point(12, 337);
            this.lbTongSoRow.Name = "lbTongSoRow";
            this.lbTongSoRow.Size = new System.Drawing.Size(14, 13);
            this.lbTongSoRow.TabIndex = 13;
            this.lbTongSoRow.Text = "00";
            // 
            // labelControl45
            // 
            this.labelControl45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl45.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl45.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl45.Location = new System.Drawing.Point(12, 311);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(14, 13);
            this.labelControl45.TabIndex = 12;
            this.labelControl45.Text = "00";
            // 
            // btn_upbill
            // 
            this.btn_upbill.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_upbill.Image = ((System.Drawing.Image)(resources.GetObject("btn_upbill.Image")));
            this.btn_upbill.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_upbill.Location = new System.Drawing.Point(0, 80);
            this.btn_upbill.Name = "btn_upbill";
            this.btn_upbill.Size = new System.Drawing.Size(40, 80);
            this.btn_upbill.TabIndex = 11;
            this.btn_upbill.Click += new System.EventHandler(this.btn_upbill_Click);
            // 
            // btn_UpTopBill
            // 
            this.btn_UpTopBill.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_UpTopBill.Image = ((System.Drawing.Image)(resources.GetObject("btn_UpTopBill.Image")));
            this.btn_UpTopBill.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_UpTopBill.Location = new System.Drawing.Point(0, 0);
            this.btn_UpTopBill.Name = "btn_UpTopBill";
            this.btn_UpTopBill.Size = new System.Drawing.Size(40, 80);
            this.btn_UpTopBill.TabIndex = 8;
            this.btn_UpTopBill.Click += new System.EventHandler(this.btn_UpTopBill_Click);
            // 
            // pnWapper
            // 
            this.pnWapper.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnWapper.Controls.Add(this.pnlNhanPhong);
            this.pnWapper.Controls.Add(this.pnInlaihoadon);
            this.pnWapper.Controls.Add(this.pnShowHang);
            this.pnWapper.Controls.Add(this.pnYeucauthem);
            this.pnWapper.Controls.Add(this.pnHuymon);
            this.pnWapper.Controls.Add(this.pnMain);
            this.pnWapper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnWapper.Location = new System.Drawing.Point(0, 70);
            this.pnWapper.Name = "pnWapper";
            this.pnWapper.Size = new System.Drawing.Size(1024, 698);
            this.pnWapper.TabIndex = 24;
            // 
            // pnlNhanPhong
            // 
            this.pnlNhanPhong.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlNhanPhong.Controls.Add(this.gridControl1);
            this.pnlNhanPhong.Controls.Add(this.gridControl2);
            this.pnlNhanPhong.Controls.Add(this.panelControl17);
            this.pnlNhanPhong.Controls.Add(this.panelControl6);
            this.pnlNhanPhong.Controls.Add(this.panelControl68);
            this.pnlNhanPhong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlNhanPhong.Location = new System.Drawing.Point(0, 0);
            this.pnlNhanPhong.Name = "pnlNhanPhong";
            this.pnlNhanPhong.Size = new System.Drawing.Size(674, 698);
            this.pnlNhanPhong.TabIndex = 2;
            // 
            // gridControl1
            // 
            this.gridControl1.AllowDrop = true;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode4.RelationName = "Level1";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode4});
            this.gridControl1.Location = new System.Drawing.Point(0, 45);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rbtnXoa,
            this.rcbbCMND,
            this.rcbbGIOITINH});
            this.gridControl1.Size = new System.Drawing.Size(635, 308);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 11F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 50;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.XOA,
            this.STT1,
            this.TENKH,
            this.CMND,
            this.NGAYSINH,
            this.GIOITINH,
            this.QUOCTICH,
            this.DIENTHOAI,
            this.DIACHI,
            this.EMAIL,
            this.MA_KH});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // XOA
            // 
            this.XOA.AppearanceCell.Options.UseTextOptions = true;
            this.XOA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.XOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.XOA.AppearanceHeader.Options.UseFont = true;
            this.XOA.AppearanceHeader.Options.UseTextOptions = true;
            this.XOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.XOA.Caption = "Xóa";
            this.XOA.ColumnEdit = this.rbtnXoa;
            this.XOA.FieldName = "Xoa";
            this.XOA.MinWidth = 50;
            this.XOA.Name = "XOA";
            this.XOA.OptionsColumn.AllowMove = false;
            this.XOA.OptionsColumn.FixedWidth = true;
            this.XOA.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.XOA.Visible = true;
            this.XOA.VisibleIndex = 0;
            this.XOA.Width = 50;
            // 
            // rbtnXoa
            // 
            this.rbtnXoa.Appearance.Options.UseTextOptions = true;
            this.rbtnXoa.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rbtnXoa.AutoHeight = false;
            this.rbtnXoa.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("rbtnXoa.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "", null, null, true)});
            this.rbtnXoa.Name = "rbtnXoa";
            this.rbtnXoa.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.rbtnXoa.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.rbtnXoa_ButtonClick);
            // 
            // STT1
            // 
            this.STT1.AppearanceCell.Options.UseTextOptions = true;
            this.STT1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT1.AppearanceHeader.Options.UseFont = true;
            this.STT1.AppearanceHeader.Options.UseTextOptions = true;
            this.STT1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT1.Caption = "TT";
            this.STT1.Name = "STT1";
            this.STT1.OptionsColumn.AllowEdit = false;
            this.STT1.OptionsColumn.AllowFocus = false;
            this.STT1.OptionsColumn.AllowMove = false;
            this.STT1.OptionsColumn.FixedWidth = true;
            this.STT1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT1.Visible = true;
            this.STT1.VisibleIndex = 1;
            this.STT1.Width = 30;
            // 
            // TENKH
            // 
            this.TENKH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENKH.AppearanceHeader.Options.UseFont = true;
            this.TENKH.AppearanceHeader.Options.UseTextOptions = true;
            this.TENKH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENKH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENKH.Caption = "Tên";
            this.TENKH.FieldName = "TENKH";
            this.TENKH.Name = "TENKH";
            this.TENKH.OptionsColumn.AllowMove = false;
            this.TENKH.OptionsColumn.FixedWidth = true;
            this.TENKH.Visible = true;
            this.TENKH.VisibleIndex = 3;
            this.TENKH.Width = 170;
            // 
            // CMND
            // 
            this.CMND.AppearanceCell.Options.UseTextOptions = true;
            this.CMND.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CMND.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CMND.AppearanceHeader.Options.UseFont = true;
            this.CMND.AppearanceHeader.Options.UseTextOptions = true;
            this.CMND.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CMND.Caption = "CMND ";
            this.CMND.ColumnEdit = this.rcbbCMND;
            this.CMND.FieldName = "CMND";
            this.CMND.Name = "CMND";
            this.CMND.OptionsColumn.FixedWidth = true;
            this.CMND.Visible = true;
            this.CMND.VisibleIndex = 2;
            this.CMND.Width = 114;
            // 
            // rcbbCMND
            // 
            this.rcbbCMND.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.rcbbCMND.Appearance.Options.UseFont = true;
            this.rcbbCMND.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 11F);
            this.rcbbCMND.AppearanceDropDown.Options.UseFont = true;
            this.rcbbCMND.AutoHeight = false;
            this.rcbbCMND.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rcbbCMND.DropDownItemHeight = 30;
            this.rcbbCMND.Name = "rcbbCMND";
            this.rcbbCMND.EditValueChanged += new System.EventHandler(this.rcbbCMND_EditValueChanged);
            this.rcbbCMND.Leave += new System.EventHandler(this.rcbbCMND_Leave);
            // 
            // NGAYSINH
            // 
            this.NGAYSINH.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYSINH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYSINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYSINH.AppearanceHeader.Options.UseFont = true;
            this.NGAYSINH.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYSINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYSINH.Caption = "Ngày sinh";
            this.NGAYSINH.FieldName = "NGAYSINH";
            this.NGAYSINH.Name = "NGAYSINH";
            this.NGAYSINH.OptionsColumn.FixedWidth = true;
            this.NGAYSINH.Visible = true;
            this.NGAYSINH.VisibleIndex = 5;
            this.NGAYSINH.Width = 100;
            // 
            // GIOITINH
            // 
            this.GIOITINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIOITINH.AppearanceHeader.Options.UseFont = true;
            this.GIOITINH.AppearanceHeader.Options.UseTextOptions = true;
            this.GIOITINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIOITINH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIOITINH.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GIOITINH.Caption = "Giới tính";
            this.GIOITINH.ColumnEdit = this.rcbbGIOITINH;
            this.GIOITINH.FieldName = "GIOITINH";
            this.GIOITINH.Name = "GIOITINH";
            this.GIOITINH.OptionsColumn.AllowMove = false;
            this.GIOITINH.OptionsColumn.FixedWidth = true;
            this.GIOITINH.Visible = true;
            this.GIOITINH.VisibleIndex = 6;
            this.GIOITINH.Width = 60;
            // 
            // rcbbGIOITINH
            // 
            this.rcbbGIOITINH.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.rcbbGIOITINH.Appearance.Options.UseFont = true;
            this.rcbbGIOITINH.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 11F);
            this.rcbbGIOITINH.AppearanceDropDown.Options.UseFont = true;
            this.rcbbGIOITINH.AutoHeight = false;
            this.rcbbGIOITINH.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rcbbGIOITINH.DropDownItemHeight = 30;
            this.rcbbGIOITINH.Items.AddRange(new object[] {
            "Nam",
            "Nữ"});
            this.rcbbGIOITINH.Name = "rcbbGIOITINH";
            // 
            // QUOCTICH
            // 
            this.QUOCTICH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.QUOCTICH.AppearanceHeader.Options.UseFont = true;
            this.QUOCTICH.AppearanceHeader.Options.UseTextOptions = true;
            this.QUOCTICH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.QUOCTICH.Caption = "Quốc tịch ";
            this.QUOCTICH.FieldName = "QUOCTICH";
            this.QUOCTICH.Name = "QUOCTICH";
            this.QUOCTICH.OptionsColumn.AllowMove = false;
            this.QUOCTICH.OptionsColumn.FixedWidth = true;
            this.QUOCTICH.Visible = true;
            this.QUOCTICH.VisibleIndex = 4;
            this.QUOCTICH.Width = 90;
            // 
            // DIENTHOAI
            // 
            this.DIENTHOAI.AppearanceCell.Options.UseTextOptions = true;
            this.DIENTHOAI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.DIENTHOAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DIENTHOAI.AppearanceHeader.Options.UseFont = true;
            this.DIENTHOAI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIENTHOAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIENTHOAI.Caption = "Điện thoại";
            this.DIENTHOAI.FieldName = "DIENTHOAI";
            this.DIENTHOAI.Name = "DIENTHOAI";
            this.DIENTHOAI.OptionsColumn.FixedWidth = true;
            this.DIENTHOAI.Visible = true;
            this.DIENTHOAI.VisibleIndex = 7;
            this.DIENTHOAI.Width = 100;
            // 
            // DIACHI
            // 
            this.DIACHI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DIACHI.AppearanceHeader.Options.UseFont = true;
            this.DIACHI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIACHI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIACHI.Caption = "Địa chỉ";
            this.DIACHI.FieldName = "DIACHI";
            this.DIACHI.MinWidth = 120;
            this.DIACHI.Name = "DIACHI";
            this.DIACHI.Visible = true;
            this.DIACHI.VisibleIndex = 8;
            this.DIACHI.Width = 120;
            // 
            // EMAIL
            // 
            this.EMAIL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.EMAIL.AppearanceHeader.Options.UseFont = true;
            this.EMAIL.AppearanceHeader.Options.UseTextOptions = true;
            this.EMAIL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.EMAIL.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.EMAIL.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.EMAIL.Caption = "Email";
            this.EMAIL.FieldName = "EMAIL";
            this.EMAIL.MinWidth = 120;
            this.EMAIL.Name = "EMAIL";
            this.EMAIL.OptionsColumn.AllowMove = false;
            this.EMAIL.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.EMAIL.Width = 120;
            // 
            // MA_KH
            // 
            this.MA_KH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MA_KH.AppearanceHeader.Options.UseFont = true;
            this.MA_KH.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_KH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_KH.Caption = "MA_KH";
            this.MA_KH.FieldName = "MA_KH";
            this.MA_KH.Name = "MA_KH";
            this.MA_KH.OptionsColumn.AllowEdit = false;
            this.MA_KH.OptionsColumn.AllowFocus = false;
            this.MA_KH.OptionsColumn.FixedWidth = true;
            this.MA_KH.Width = 102;
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            gridLevelNode5.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode5});
            this.gridControl2.Location = new System.Drawing.Point(0, 353);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1});
            this.gridControl2.Size = new System.Drawing.Size(635, 282);
            this.gridControl2.TabIndex = 6;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            this.gridControl2.Visible = false;
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 11F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 50;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Themkhach,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            // 
            // Themkhach
            // 
            this.Themkhach.AppearanceCell.Options.UseTextOptions = true;
            this.Themkhach.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Themkhach.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.Themkhach.AppearanceHeader.Options.UseFont = true;
            this.Themkhach.AppearanceHeader.Options.UseTextOptions = true;
            this.Themkhach.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Themkhach.Caption = "Thêm";
            this.Themkhach.ColumnEdit = this.repositoryItemButtonEdit1;
            this.Themkhach.FieldName = "Xoa";
            this.Themkhach.MinWidth = 50;
            this.Themkhach.Name = "Themkhach";
            this.Themkhach.OptionsColumn.AllowEdit = false;
            this.Themkhach.OptionsColumn.AllowFocus = false;
            this.Themkhach.OptionsColumn.AllowMove = false;
            this.Themkhach.OptionsColumn.FixedWidth = true;
            this.Themkhach.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.Themkhach.Visible = true;
            this.Themkhach.VisibleIndex = 0;
            this.Themkhach.Width = 57;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.Appearance.Options.UseTextOptions = true;
            this.repositoryItemButtonEdit1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit1.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, "", null, null, true)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn3.Caption = "Tên";
            this.gridColumn3.FieldName = "TEN";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.AllowMove = false;
            this.gridColumn3.OptionsColumn.FixedWidth = true;
            this.gridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 170;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "CMND ";
            this.gridColumn4.FieldName = "CMND";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.FixedWidth = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            this.gridColumn4.Width = 114;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn5.AppearanceHeader.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "Ngày sinh";
            this.gridColumn5.FieldName = "NGAYSINH";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.FixedWidth = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 100;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn6.Caption = "Giới tính";
            this.gridColumn6.FieldName = "GIOITINH";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.AllowMove = false;
            this.gridColumn6.OptionsColumn.FixedWidth = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            this.gridColumn6.Width = 60;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "Quốc tịch ";
            this.gridColumn7.FieldName = "QUOCTICH";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.AllowMove = false;
            this.gridColumn7.OptionsColumn.FixedWidth = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 3;
            this.gridColumn7.Width = 90;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn8.AppearanceHeader.Options.UseFont = true;
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "Điện thoại";
            this.gridColumn8.FieldName = "DIENTHOAI";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.FixedWidth = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 6;
            this.gridColumn8.Width = 100;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn9.AppearanceHeader.Options.UseFont = true;
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.Caption = "Địa chỉ";
            this.gridColumn9.FieldName = "DIACHI";
            this.gridColumn9.MinWidth = 120;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 7;
            this.gridColumn9.Width = 120;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn10.AppearanceHeader.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn10.Caption = "Email";
            this.gridColumn10.FieldName = "EMAIL";
            this.gridColumn10.MinWidth = 120;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.AllowMove = false;
            this.gridColumn10.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn10.Width = 120;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn11.AppearanceHeader.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "MA_KH";
            this.gridColumn11.FieldName = "MA_KH";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.FixedWidth = true;
            this.gridColumn11.Width = 102;
            // 
            // panelControl17
            // 
            this.panelControl17.Controls.Add(this.labelControl1);
            this.panelControl17.Controls.Add(this.txtDaThanhToan);
            this.panelControl17.Controls.Add(this.dtpNgayvao);
            this.panelControl17.Controls.Add(this.dtpNgayra);
            this.panelControl17.Controls.Add(this.label2);
            this.panelControl17.Controls.Add(this.label1);
            this.panelControl17.Controls.Add(this.btnTimkhach);
            this.panelControl17.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl17.Location = new System.Drawing.Point(0, 635);
            this.panelControl17.Name = "panelControl17";
            this.panelControl17.Size = new System.Drawing.Size(635, 63);
            this.panelControl17.TabIndex = 7;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Location = new System.Drawing.Point(2, 36);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(59, 18);
            this.labelControl1.TabIndex = 25;
            this.labelControl1.Text = "Đặt cọc";
            // 
            // txtDaThanhToan
            // 
            this.txtDaThanhToan.EditValue = "0";
            this.txtDaThanhToan.EnterMoveNextControl = true;
            this.txtDaThanhToan.Location = new System.Drawing.Point(106, 33);
            this.txtDaThanhToan.Name = "txtDaThanhToan";
            this.txtDaThanhToan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDaThanhToan.Properties.Appearance.Options.UseFont = true;
            this.txtDaThanhToan.Properties.Appearance.Options.UseTextOptions = true;
            this.txtDaThanhToan.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtDaThanhToan.Properties.DisplayFormat.FormatString = "N0";
            this.txtDaThanhToan.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtDaThanhToan.Properties.EditFormat.FormatString = "N0";
            this.txtDaThanhToan.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtDaThanhToan.Properties.Mask.EditMask = "N0";
            this.txtDaThanhToan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtDaThanhToan.Size = new System.Drawing.Size(388, 24);
            this.txtDaThanhToan.TabIndex = 26;
            // 
            // dtpNgayvao
            // 
            this.dtpNgayvao.EditValue = null;
            this.dtpNgayvao.EnterMoveNextControl = true;
            this.dtpNgayvao.Location = new System.Drawing.Point(106, 3);
            this.dtpNgayvao.Name = "dtpNgayvao";
            this.dtpNgayvao.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayvao.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayvao.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayvao.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayvao.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayvao.Properties.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.dtpNgayvao.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayvao.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayvao.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayvao.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayvao.Properties.ReadOnly = true;
            this.dtpNgayvao.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayvao.Size = new System.Drawing.Size(170, 24);
            this.dtpNgayvao.TabIndex = 24;
            // 
            // dtpNgayra
            // 
            this.dtpNgayra.EditValue = null;
            this.dtpNgayra.EnterMoveNextControl = true;
            this.dtpNgayra.Location = new System.Drawing.Point(366, 4);
            this.dtpNgayra.Name = "dtpNgayra";
            this.dtpNgayra.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayra.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayra.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayra.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayra.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayra.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayra.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayra.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayra.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayra.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayra.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayra.Size = new System.Drawing.Size(128, 24);
            this.dtpNgayra.TabIndex = 23;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(278, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 18);
            this.label2.TabIndex = 21;
            this.label2.Text = "Trả phòng";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(2, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 18);
            this.label1.TabIndex = 19;
            this.label1.Text = "Nhận phòng";
            // 
            // btnTimkhach
            // 
            this.btnTimkhach.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnTimkhach.Appearance.Options.UseFont = true;
            this.btnTimkhach.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnTimkhach.Location = new System.Drawing.Point(536, 2);
            this.btnTimkhach.Name = "btnTimkhach";
            this.btnTimkhach.Size = new System.Drawing.Size(97, 59);
            this.btnTimkhach.TabIndex = 18;
            this.btnTimkhach.Text = "Tìm khách";
            this.btnTimkhach.Click += new System.EventHandler(this.btnTimkhach_Click);
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.panelControl10);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl6.Location = new System.Drawing.Point(0, 0);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(635, 45);
            this.panelControl6.TabIndex = 5;
            // 
            // panelControl10
            // 
            this.panelControl10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl10.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl10.Controls.Add(this.lbKhuvuc_Nhanphong);
            this.panelControl10.Controls.Add(this.lbPhong_Nhanphong);
            this.panelControl10.Controls.Add(this.labelControl16);
            this.panelControl10.Location = new System.Drawing.Point(2, 12);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(621, 31);
            this.panelControl10.TabIndex = 0;
            // 
            // lbKhuvuc_Nhanphong
            // 
            this.lbKhuvuc_Nhanphong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbKhuvuc_Nhanphong.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbKhuvuc_Nhanphong.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbKhuvuc_Nhanphong.Location = new System.Drawing.Point(409, 0);
            this.lbKhuvuc_Nhanphong.Name = "lbKhuvuc_Nhanphong";
            this.lbKhuvuc_Nhanphong.Size = new System.Drawing.Size(130, 19);
            this.lbKhuvuc_Nhanphong.TabIndex = 0;
            this.lbKhuvuc_Nhanphong.Text = "Khu Vực 1         >";
            // 
            // lbPhong_Nhanphong
            // 
            this.lbPhong_Nhanphong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbPhong_Nhanphong.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbPhong_Nhanphong.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbPhong_Nhanphong.Location = new System.Drawing.Point(539, 0);
            this.lbPhong_Nhanphong.Name = "lbPhong_Nhanphong";
            this.lbPhong_Nhanphong.Size = new System.Drawing.Size(82, 19);
            this.lbPhong_Nhanphong.TabIndex = 1;
            this.lbPhong_Nhanphong.Text = "    Bàn 1  >";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl16.Location = new System.Drawing.Point(0, 0);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(128, 19);
            this.labelControl16.TabIndex = 3;
            this.labelControl16.Text = "Danh Sách Khách:";
            // 
            // panelControl68
            // 
            this.panelControl68.Controls.Add(this.panelControl69);
            this.panelControl68.Controls.Add(this.btnXuongit);
            this.panelControl68.Controls.Add(this.btnXuongnhieu);
            this.panelControl68.Controls.Add(this.btnLenit);
            this.panelControl68.Controls.Add(this.btnLennhieu);
            this.panelControl68.Controls.Add(this.btnBanphim);
            this.panelControl68.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl68.Location = new System.Drawing.Point(635, 0);
            this.panelControl68.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl68.Name = "panelControl68";
            this.panelControl68.Size = new System.Drawing.Size(39, 698);
            this.panelControl68.TabIndex = 14;
            // 
            // panelControl69
            // 
            this.panelControl69.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl69.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl69.Location = new System.Drawing.Point(2, 162);
            this.panelControl69.Name = "panelControl69";
            this.panelControl69.Size = new System.Drawing.Size(35, 294);
            this.panelControl69.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(2, 456);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(2, 536);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(2, 82);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(2, 2);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 616);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // pnHeader
            // 
            this.pnHeader.Controls.Add(this.panelControl1);
            this.pnHeader.Controls.Add(this.panelControl11);
            this.pnHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnHeader.Location = new System.Drawing.Point(0, 0);
            this.pnHeader.Name = "pnHeader";
            this.pnHeader.Size = new System.Drawing.Size(1024, 70);
            this.pnHeader.TabIndex = 25;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.lb_Quay);
            this.panelControl1.Controls.Add(this.lb_Kho);
            this.panelControl1.Controls.Add(this.lbgiay);
            this.panelControl1.Controls.Add(this.lbCaBan);
            this.panelControl1.Controls.Add(this.lbGio);
            this.panelControl1.Controls.Add(this.lb_NhanVien);
            this.panelControl1.Controls.Add(this.lbtimeby);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(198, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(824, 66);
            this.panelControl1.TabIndex = 3;
            // 
            // lb_Quay
            // 
            this.lb_Quay.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_Quay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_Quay.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_Quay.Location = new System.Drawing.Point(15, 5);
            this.lb_Quay.Name = "lb_Quay";
            this.lb_Quay.Size = new System.Drawing.Size(41, 19);
            this.lb_Quay.TabIndex = 14;
            this.lb_Quay.Text = "Quầy";
            // 
            // lb_Kho
            // 
            this.lb_Kho.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_Kho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_Kho.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_Kho.Location = new System.Drawing.Point(15, 34);
            this.lb_Kho.Name = "lb_Kho";
            this.lb_Kho.Size = new System.Drawing.Size(41, 19);
            this.lb_Kho.TabIndex = 15;
            this.lb_Kho.Text = "Quầy";
            // 
            // lbgiay
            // 
            this.lbgiay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbgiay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbgiay.Location = new System.Drawing.Point(783, 27);
            this.lbgiay.Name = "lbgiay";
            this.lbgiay.Size = new System.Drawing.Size(18, 19);
            this.lbgiay.TabIndex = 19;
            this.lbgiay.Text = "00";
            // 
            // lbCaBan
            // 
            this.lbCaBan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCaBan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbCaBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbCaBan.Location = new System.Drawing.Point(716, 5);
            this.lbCaBan.Name = "lbCaBan";
            this.lbCaBan.Size = new System.Drawing.Size(94, 19);
            this.lbCaBan.TabIndex = 17;
            this.lbCaBan.Text = "21-12-2012";
            // 
            // lbGio
            // 
            this.lbGio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGio.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGio.Location = new System.Drawing.Point(731, 34);
            this.lbGio.Name = "lbGio";
            this.lbGio.Size = new System.Drawing.Size(46, 19);
            this.lbGio.TabIndex = 20;
            this.lbGio.Text = "00:00";
            // 
            // lb_NhanVien
            // 
            this.lb_NhanVien.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lb_NhanVien.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_NhanVien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_NhanVien.Location = new System.Drawing.Point(357, 5);
            this.lb_NhanVien.Name = "lb_NhanVien";
            this.lb_NhanVien.Size = new System.Drawing.Size(89, 19);
            this.lb_NhanVien.TabIndex = 18;
            this.lb_NhanVien.Text = "Nhân Viên:";
            // 
            // lbtimeby
            // 
            this.lbtimeby.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbtimeby.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbtimeby.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbtimeby.Location = new System.Drawing.Point(357, 38);
            this.lbtimeby.Name = "lbtimeby";
            this.lbtimeby.Size = new System.Drawing.Size(21, 19);
            this.lbtimeby.TabIndex = 16;
            this.lbtimeby.Text = "Ca";
            // 
            // panelControl11
            // 
            this.panelControl11.Controls.Add(this.pictureBox1);
            this.panelControl11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl11.Location = new System.Drawing.Point(2, 2);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(196, 66);
            this.panelControl11.TabIndex = 21;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(8, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(177, 41);
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // Frm_BanHang_KhachSan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.pnWapper);
            this.Controls.Add(this.pnHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "Frm_BanHang_KhachSan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_BanHang";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_BanHang_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pnShowHang)).EndInit();
            this.pnShowHang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.panelControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl37)).EndInit();
            this.panelControl37.ResumeLayout(false);
            this.panelControl37.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnKV_Mon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).EndInit();
            this.panelControl13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnMain)).EndInit();
            this.pnMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_listProduct)).EndInit();
            this.pn_listProduct.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_SelectProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_SelectProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDowGv)).EndInit();
            this.pnUpDowGv.ResumeLayout(false);
            this.pnUpDowGv.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnBtnDeleteGv)).EndInit();
            this.pnBtnDeleteGv.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl54)).EndInit();
            this.panelControl54.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl64)).EndInit();
            this.panelControl64.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl65)).EndInit();
            this.panelControl65.ResumeLayout(false);
            this.panelControl65.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl66)).EndInit();
            this.panelControl66.ResumeLayout(false);
            this.panelControl66.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl20)).EndInit();
            this.panelControl20.ResumeLayout(false);
            this.panelControl20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl63)).EndInit();
            this.panelControl63.ResumeLayout(false);
            this.panelControl63.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl67)).EndInit();
            this.panelControl67.ResumeLayout(false);
            this.panelControl67.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl62)).EndInit();
            this.panelControl62.ResumeLayout(false);
            this.panelControl62.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl61)).EndInit();
            this.panelControl61.ResumeLayout(false);
            this.panelControl61.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl60)).EndInit();
            this.panelControl60.ResumeLayout(false);
            this.panelControl60.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl59)).EndInit();
            this.panelControl59.ResumeLayout(false);
            this.panelControl59.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnNghiepVu_ThuNgan)).EndInit();
            this.pnNghiepVu_ThuNgan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnThuNgan)).EndInit();
            this.pnThuNgan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnNghiepVu)).EndInit();
            this.pnNghiepVu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnDacThu)).EndInit();
            this.pnDacThu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnHuymon)).EndInit();
            this.pnHuymon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_GroupTable)).EndInit();
            this.pn_GroupTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            this.panelControl15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_divide_Table_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_divide_Table_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDow)).EndInit();
            this.pnUpDow.ResumeLayout(false);
            this.pnUpDow.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_TableTow)).EndInit();
            this.pn_TableTow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_divide_Table_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_divide_Table_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnInforTablecheck)).EndInit();
            this.pnInforTablecheck.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).EndInit();
            this.panelControl14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTenKhacHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmaBanGoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmaKVBanGoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).EndInit();
            this.panelControl18.ResumeLayout(false);
            this.panelControl18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_TableShare)).EndInit();
            this.pn_TableShare.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTenKhachHangGhep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmaBanGhep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmaKVBanGhep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnYeucauthem)).EndInit();
            this.pnYeucauthem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).EndInit();
            this.panelControl19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).EndInit();
            this.panelControl22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl21)).EndInit();
            this.panelControl21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtYeuCauThem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl24)).EndInit();
            this.panelControl24.ResumeLayout(false);
            this.panelControl24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnInlaihoadon)).EndInit();
            this.pnInlaihoadon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl53)).EndInit();
            this.panelControl53.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_billPaymented)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBillPaymented)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl52)).EndInit();
            this.panelControl52.ResumeLayout(false);
            this.panelControl52.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnWapper)).EndInit();
            this.pnWapper.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlNhanPhong)).EndInit();
            this.pnlNhanPhong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbtnXoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbbCMND)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbbGIOITINH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).EndInit();
            this.panelControl17.ResumeLayout(false);
            this.panelControl17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDaThanhToan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayvao.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayvao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayra.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            this.panelControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl68)).EndInit();
            this.panelControl68.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnHeader)).EndInit();
            this.pnHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnShowHang;
        private DevExpress.XtraEditors.PanelControl pnMain;
        private DevExpress.XtraEditors.PanelControl pn_listProduct;
        private DevExpress.XtraEditors.PanelControl pnUpDowGv;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl lb_RowsCount;
        private DevExpress.XtraEditors.LabelControl lb_STT;
        private DevExpress.XtraEditors.SimpleButton btn_up;
        private DevExpress.XtraEditors.SimpleButton btn_Dow;
        private DevExpress.XtraEditors.SimpleButton btn_DowLast;
        private DevExpress.XtraEditors.SimpleButton btn_UpFirst;
        private DevExpress.XtraGrid.GridControl gr_SelectProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView gv_SelectProduct;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn GIABAN;
        private DevExpress.XtraGrid.Columns.GridColumn Thanh_Tien;
        private DevExpress.XtraGrid.Columns.GridColumn SOTT;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA;
        private DevExpress.XtraEditors.PanelControl pnNghiepVu_ThuNgan;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.SimpleButton btn_NghiepVu;
        private DevExpress.XtraEditors.SimpleButton btn_ThuNgan;
        private DevExpress.XtraEditors.SimpleButton btn_OrderSent;
        private DevExpress.XtraEditors.SimpleButton btn_SoDoBan;
        private DevExpress.XtraEditors.SimpleButton btn_TamTinh;
        private DevExpress.XtraEditors.SimpleButton btn_GhepBan;
        private DevExpress.XtraEditors.SimpleButton btn_Payment;
        private DevExpress.XtraEditors.SimpleButton btnChuyenBan;
        private DevExpress.XtraEditors.SimpleButton btn_YeuCauThem;
        private DevExpress.XtraEditors.PanelControl pnThuNgan;
        private DevExpress.XtraEditors.PanelControl pnNghiepVu;
        private DevExpress.XtraEditors.SimpleButton btn_ExitTable;
        private DevExpress.XtraEditors.SimpleButton btn_InLaiHoaDon;
        private DevExpress.XtraEditors.SimpleButton btn_ExitDishe;
        private DevExpress.XtraEditors.SimpleButton btn_DatBan;
        private DevExpress.XtraEditors.SimpleButton btn_LayLaiHoaDon;
        private DevExpress.XtraEditors.SimpleButton btnMoKet;
        private DevExpress.XtraEditors.SimpleButton btnKetKa;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.LabelControl lbKhuVuc;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl lbPageTable;
        private DevExpress.XtraEditors.SimpleButton btn_NextRight;
        private DevExpress.XtraEditors.SimpleButton btn_NextToLeft;
        private DevExpress.XtraEditors.SimpleButton btn_DowKV;
        private DevExpress.XtraEditors.SimpleButton btn_UpKV;
        private DevExpress.XtraEditors.PanelControl pnHuymon;
        private DevExpress.XtraEditors.PanelControl pn_GroupTable;
        private DevExpress.XtraEditors.PanelControl pnUpDow;
        private DevExpress.XtraEditors.PanelControl pn_TableTow;
        private DevExpress.XtraEditors.PanelControl panelControl15;
        private DevExpress.XtraEditors.PanelControl pnInforTablecheck;
        private DevExpress.XtraEditors.SimpleButton btn_OkThrough;
        private DevExpress.XtraEditors.PanelControl panelControl18;
        private DevExpress.XtraEditors.PanelControl pn_TableShare;
        private DevExpress.XtraEditors.PanelControl panelControl14;
        private DevExpress.XtraEditors.TextEdit txtTenKhacHang;
        private DevExpress.XtraEditors.TextEdit txtmaBanGoc;
        private DevExpress.XtraEditors.TextEdit txtmaKVBanGoc;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtTenKhachHangGhep;
        private DevExpress.XtraEditors.TextEdit txtmaBanGhep;
        private DevExpress.XtraEditors.TextEdit txtmaKVBanGhep;
        private DevExpress.XtraEditors.SimpleButton btn_ESCThroughtTable;
        private DevExpress.XtraEditors.SimpleButton btn_ToLeftAll;
        private DevExpress.XtraEditors.SimpleButton btn_Toleft;
        private DevExpress.XtraEditors.SimpleButton btn_ToRight;
        private DevExpress.XtraEditors.SimpleButton btn_ToRightAll;
        private DevExpress.XtraEditors.SimpleButton btn_UpDivide;
        private DevExpress.XtraEditors.SimpleButton btn_DowDivide;
        private DevExpress.XtraEditors.PanelControl pnYeucauthem;
        private DevExpress.XtraEditors.PanelControl panelControl19;
        private DevExpress.XtraEditors.TextEdit txtYeuCauThem;
        private DevExpress.XtraEditors.PanelControl panelControl16;
        private DevExpress.XtraEditors.PanelControl panelControl21;
        private DevExpress.XtraEditors.PanelControl panelControl22;
        private DevExpress.XtraEditors.SimpleButton simpleButton23;
        private DevExpress.XtraEditors.SimpleButton simpleButton24;
        private DevExpress.XtraEditors.PanelControl panelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl lbNameTable;
        private DevExpress.XtraEditors.LabelControl lbListTable;
        private DevExpress.XtraEditors.LabelControl lbNameMon;
        private DevExpress.XtraEditors.PanelControl pnKV_Mon;
        private DevExpress.XtraEditors.SimpleButton btn_logout;
        private DevExpress.XtraEditors.LabelControl lbTableTatol;
        private DevExpress.XtraEditors.LabelControl lbTatol;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;
        private DevExpress.XtraEditors.PanelControl panelControl37;
        private DevExpress.XtraGrid.GridControl gr_divide_Table_1;
        private DevExpress.XtraGrid.Views.Grid.GridView gv_divide_Table_1;
        private DevExpress.XtraGrid.Columns.GridColumn _SSTT;
        private DevExpress.XtraGrid.Columns.GridColumn _TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn _SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn _GIABAN;
        private DevExpress.XtraGrid.Columns.GridColumn _Thanh_Tien;
        private DevExpress.XtraGrid.Columns.GridColumn _MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn _GHICHU;
        private DevExpress.XtraGrid.GridControl gr_divide_Table_2;
        private DevExpress.XtraGrid.Views.Grid.GridView gv_divide_Table_2;
        private DevExpress.XtraGrid.Columns.GridColumn __STT;
        private DevExpress.XtraGrid.Columns.GridColumn __TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn __SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn __GIABAN;
        private DevExpress.XtraGrid.Columns.GridColumn __Thanh_Tien;
        private DevExpress.XtraGrid.Columns.GridColumn __MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn __GHICHU;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraGrid.Columns.GridColumn MA_BEP;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl pnInlaihoadon;
        private DevExpress.XtraEditors.PanelControl panelControl52;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.LabelControl lbTongSoRow;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.SimpleButton btn_upbill;
        private DevExpress.XtraEditors.SimpleButton btnDowBill;
        private DevExpress.XtraEditors.SimpleButton btnDowLastBill;
        private DevExpress.XtraEditors.SimpleButton btn_UpTopBill;
        private DevExpress.XtraEditors.PanelControl panelControl53;
        private System.Windows.Forms.FlowLayoutPanel pnYeuCau;
        private DevExpress.XtraGrid.Columns.GridColumn _MA_BEP;
        private DevExpress.XtraGrid.Columns.GridColumn THUE;
        private DevExpress.XtraGrid.Columns.GridColumn __MA_BEP;
        private DevExpress.XtraEditors.LabelControl lbGiamGia;
        private DevExpress.XtraEditors.LabelControl labelControl51;
        private DevExpress.XtraEditors.LabelControl lbPhuThu;
        private DevExpress.XtraEditors.LabelControl labelControl52;
        private DevExpress.XtraEditors.PanelControl pnWapper;
        private DevExpress.XtraEditors.LabelControl lbTiengio;
        private DevExpress.XtraEditors.LabelControl lb_clicktiengio;
        private DevExpress.XtraEditors.SimpleButton btnGiaBan2;
        private DevExpress.XtraEditors.SimpleButton btnTachPhong;
        private DevExpress.XtraGrid.Columns.GridColumn IS_COMBO;
        private DevExpress.XtraEditors.PanelControl panelControl63;
        private DevExpress.XtraEditors.PanelControl panelControl62;
        private DevExpress.XtraEditors.PanelControl panelControl61;
        private DevExpress.XtraEditors.PanelControl panelControl60;
        private DevExpress.XtraEditors.PanelControl panelControl59;
        private DevExpress.XtraEditors.SimpleButton btnThoatReprint;
        private DevExpress.XtraEditors.PanelControl pnBtnDeleteGv;
        private DevExpress.XtraEditors.PanelControl panelControl54;
        private DevExpress.XtraEditors.PanelControl panelControl64;
        private DevExpress.XtraEditors.SimpleButton btn_tru;
        private DevExpress.XtraEditors.SimpleButton btn_XoaTatCa;
        private DevExpress.XtraEditors.SimpleButton btn_cong;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btn_xoa;
        private DevExpress.XtraEditors.SimpleButton btn9;
        private DevExpress.XtraEditors.SimpleButton btn8;
        private DevExpress.XtraEditors.SimpleButton btn7;
        private DevExpress.XtraEditors.SimpleButton btn6;
        private DevExpress.XtraEditors.SimpleButton btn5;
        private DevExpress.XtraEditors.SimpleButton btn4;
        private DevExpress.XtraEditors.SimpleButton btn3;
        private DevExpress.XtraEditors.SimpleButton btn2;
        private DevExpress.XtraEditors.SimpleButton btn1;
        private DevExpress.XtraEditors.SimpleButton btn0;
        private DevExpress.XtraEditors.PanelControl panelControl65;
        private DevExpress.XtraEditors.PanelControl panelControl66;
        private DevExpress.XtraEditors.LabelControl lbSoluong;
        private DevExpress.XtraEditors.LabelControl lb_soluong;
        private Us_keyboard us_keyboard3;
        private DevExpress.XtraEditors.SimpleButton btnNhanphong;
        private DevExpress.XtraEditors.PanelControl pnlNhanPhong;
        private DevExpress.XtraEditors.SimpleButton btnXacNhan;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT1;
        private DevExpress.XtraGrid.Columns.GridColumn MA_KH;
        private DevExpress.XtraGrid.Columns.GridColumn GIOITINH;
        private DevExpress.XtraGrid.Columns.GridColumn EMAIL;
        private DevExpress.XtraGrid.Columns.GridColumn TENKH;
        private DevExpress.XtraGrid.Columns.GridColumn XOA;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit rbtnXoa;
        private DevExpress.XtraGrid.Columns.GridColumn QUOCTICH;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYSINH;
        private DevExpress.XtraGrid.Columns.GridColumn CMND;
        private DevExpress.XtraGrid.Columns.GridColumn DIACHI;
        private DevExpress.XtraGrid.Columns.GridColumn DIENTHOAI;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox rcbbCMND;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox rcbbGIOITINH;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraEditors.PanelControl panelControl68;
        private DevExpress.XtraEditors.PanelControl panelControl69;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.PanelControl panelControl67;
        private DevExpress.XtraEditors.LabelControl lbDichvu;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton btnKhachrangoai;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.LabelControl lbKhuvuc_Nhanphong;
        private DevExpress.XtraEditors.LabelControl lbPhong_Nhanphong;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl24;
        private DevExpress.XtraEditors.LabelControl lbkvYeuCauThem;
        private DevExpress.XtraEditors.LabelControl lbTableYeuCauThem;
        private DevExpress.XtraEditors.LabelControl lbMonYeuCauThem;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn Themkhach;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.PanelControl panelControl17;
        private DevExpress.XtraEditors.SimpleButton btnTimkhach;
        private DevExpress.XtraEditors.SimpleButton btn_InlaiOrder;
        private DevExpress.XtraGrid.GridControl gr_billPaymented;
        private DevExpress.XtraGrid.Views.Grid.GridView gvBillPaymented;
        private DevExpress.XtraGrid.Columns.GridColumn SSTT;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HOADON;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYTAO;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_KHUVUC;
        private DevExpress.XtraGrid.Columns.GridColumn TONGTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_BAN;
        private DevExpress.XtraGrid.Columns.GridColumn IN;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn HUY;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn LAYLAI;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit4;
        private DevExpress.XtraEditors.SimpleButton btn_DatThu;
        private DevExpress.XtraEditors.PanelControl pnHeader;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl lb_Quay;
        private DevExpress.XtraEditors.LabelControl lb_Kho;
        private DevExpress.XtraEditors.LabelControl lbgiay;
        private DevExpress.XtraEditors.LabelControl lbCaBan;
        private DevExpress.XtraEditors.LabelControl lbGio;
        private DevExpress.XtraEditors.LabelControl lb_NhanVien;
        private DevExpress.XtraEditors.LabelControl lbtimeby;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.PanelControl panelControl13;
        private DevExpress.XtraEditors.PanelControl pnDacThu;
        private DevExpress.XtraEditors.SimpleButton btnDanhsachkhach;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton btnDoigio_nhanphong;
        private DevExpress.XtraEditors.XtraScrollableControl pnTable_Mon;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.DateEdit dtpNgayra;
        private DevExpress.XtraEditors.DateEdit dtpNgayvao;
        private DevExpress.XtraEditors.SimpleButton btnKhoaphong;
        private DevExpress.XtraEditors.SimpleButton btnVesinh;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtDaThanhToan;
    }
}