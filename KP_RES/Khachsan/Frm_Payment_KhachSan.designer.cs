﻿namespace KP_RES
{
    partial class Frm_Payment_KhachSan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Payment_KhachSan));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            this.pnlGridviewActionMain = new DevExpress.XtraEditors.PanelControl();
            this.pnlInforBuyProduct = new DevExpress.XtraEditors.PanelControl();
            this.pnlGridview = new DevExpress.XtraEditors.PanelControl();
            this.gcListProduct = new DevExpress.XtraGrid.GridControl();
            this.gvListProduct = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ProductName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Quantum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.Price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TheMoney = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_BEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Discount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcListGIO = new DevExpress.XtraGrid.GridControl();
            this.gvListGIO = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT_GIO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnUpDownGridview = new DevExpress.XtraEditors.PanelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.lbRowsCount = new DevExpress.XtraEditors.LabelControl();
            this.lbSTT = new DevExpress.XtraEditors.LabelControl();
            this.btnUpGridview = new DevExpress.XtraEditors.SimpleButton();
            this.btnDownGridview = new DevExpress.XtraEditors.SimpleButton();
            this.btnLastGridview = new DevExpress.XtraEditors.SimpleButton();
            this.btnFirstGridview = new DevExpress.XtraEditors.SimpleButton();
            this.pnlInforMoneyAndCustomer = new DevExpress.XtraEditors.PanelControl();
            this.pnlInforCustomer = new DevExpress.XtraEditors.PanelControl();
            this.pnlInnerCustomerShow = new DevExpress.XtraEditors.PanelControl();
            this.lbMoneySurplus = new DevExpress.XtraEditors.LabelControl();
            this.lbIdInnerCustomerShow = new DevExpress.XtraEditors.LabelControl();
            this.lbNameInnerCustomer = new DevExpress.XtraEditors.LabelControl();
            this.pnlDearCustomerShow = new DevExpress.XtraEditors.PanelControl();
            this.lbMark = new DevExpress.XtraEditors.LabelControl();
            this.lbIDDearCustomerShow = new DevExpress.XtraEditors.LabelControl();
            this.lbNameDearCustomer = new DevExpress.XtraEditors.LabelControl();
            this.pnlInforMoney = new DevExpress.XtraEditors.PanelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.lbTheMoney = new DevExpress.XtraEditors.LabelControl();
            this.lbTotalMoney = new DevExpress.XtraEditors.LabelControl();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.lb_clicktiengio = new DevExpress.XtraEditors.LabelControl();
            this.lbHourMoney = new DevExpress.XtraEditors.LabelControl();
            this.pnlActionCashier = new DevExpress.XtraEditors.PanelControl();
            this.pnlPayment = new DevExpress.XtraEditors.PanelControl();
            this.btnCongNo = new DevExpress.XtraEditors.SimpleButton();
            this.btnGiaohang = new DevExpress.XtraEditors.SimpleButton();
            this.btnVisa = new DevExpress.XtraEditors.SimpleButton();
            this.btnVoucher = new DevExpress.XtraEditors.SimpleButton();
            this.btnServiceMoney = new DevExpress.XtraEditors.SimpleButton();
            this.btnRetypePayment = new DevExpress.XtraEditors.SimpleButton();
            this.btnDiscount = new DevExpress.XtraEditors.SimpleButton();
            this.btnCardImtimateCustomer = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancelPayment = new DevExpress.XtraEditors.SimpleButton();
            this.btnNotPrintBill = new DevExpress.XtraEditors.SimpleButton();
            this.btnCardInternalPayment = new DevExpress.XtraEditors.SimpleButton();
            this.btnOpenSafe = new DevExpress.XtraEditors.SimpleButton();
            this.pnlPaymentButton = new DevExpress.XtraEditors.PanelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.pnlSpace = new DevExpress.XtraEditors.PanelControl();
            this.pnlPaymentShowFill = new DevExpress.XtraEditors.PanelControl();
            this.pnlShowInforPayment = new DevExpress.XtraEditors.PanelControl();
            this.pnlInputVoucher = new DevExpress.XtraEditors.PanelControl();
            this.txtInputVoucher = new DevExpress.XtraEditors.TextEdit();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.pnlInputGetMoney = new DevExpress.XtraEditors.PanelControl();
            this.lbcurrencyGetMoney = new DevExpress.XtraEditors.LabelControl();
            this.txtInputMoney = new DevExpress.XtraEditors.TextEdit();
            this.lbPayment = new DevExpress.XtraEditors.LabelControl();
            this.pnlInnerCustomer = new DevExpress.XtraEditors.PanelControl();
            this.txtInnerCustomer = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.pnlServiceMoney = new DevExpress.XtraEditors.PanelControl();
            this.lbcurrencyService = new DevExpress.XtraEditors.LabelControl();
            this.txtServiceMoney = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl55 = new DevExpress.XtraEditors.PanelControl();
            this.lbMoneyGetFirst = new DevExpress.XtraEditors.LabelControl();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl51 = new DevExpress.XtraEditors.PanelControl();
            this.lbServiceMoney = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.pnlThird = new DevExpress.XtraEditors.PanelControl();
            this.lbThird = new DevExpress.XtraEditors.LabelControl();
            this.lbThirdMoney = new DevExpress.XtraEditors.LabelControl();
            this.pnlSecond = new DevExpress.XtraEditors.PanelControl();
            this.lbSecond = new DevExpress.XtraEditors.LabelControl();
            this.lbSecondMoney = new DevExpress.XtraEditors.LabelControl();
            this.pnlFirst = new DevExpress.XtraEditors.PanelControl();
            this.lbFirst = new DevExpress.XtraEditors.LabelControl();
            this.lbFirstMoney = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl27 = new DevExpress.XtraEditors.PanelControl();
            this.lbDiscountPayment = new DevExpress.XtraEditors.LabelControl();
            this.lbDiscount = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.pnlMoneyReply = new DevExpress.XtraEditors.PanelControl();
            this.lbReplyCustomer = new DevExpress.XtraEditors.LabelControl();
            this.lbVNDReply = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl29 = new DevExpress.XtraEditors.PanelControl();
            this.lbTotalMoneyAll = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl30 = new DevExpress.XtraEditors.PanelControl();
            this.lbGetCustomer = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl31 = new DevExpress.XtraEditors.PanelControl();
            this.lbTotalMoneyPayment = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.pnlInputDiscount = new DevExpress.XtraEditors.PanelControl();
            this.txtInputDiscount = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.pnlDearCustomer = new DevExpress.XtraEditors.PanelControl();
            this.txtDearCustomer = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.pnlVisa = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.txtCountry = new DevExpress.XtraEditors.TextEdit();
            this.txtNameVisa = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.btnRefeshIdCode = new DevExpress.XtraEditors.SimpleButton();
            this.txtCwCvc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtIdCodeCreate = new DevExpress.XtraEditors.TextEdit();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.txtCity = new DevExpress.XtraEditors.TextEdit();
            this.txtIdCodeInput = new DevExpress.XtraEditors.TextEdit();
            this.txtAddress = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.txtYear = new DevExpress.XtraEditors.TextEdit();
            this.txtMonth = new DevExpress.XtraEditors.TextEdit();
            this.txtMoneyVisa = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtIdCardVisa = new DevExpress.XtraEditors.TextEdit();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnPaymentVisa = new DevExpress.XtraEditors.SimpleButton();
            this.bntCancelVisa = new DevExpress.XtraEditors.SimpleButton();
            this.pnlHeaderVisa = new DevExpress.XtraEditors.PanelControl();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.pnlBottomPayment = new DevExpress.XtraEditors.PanelControl();
            this.pnlKeyboardAndShowForeignCurrency = new DevExpress.XtraEditors.PanelControl();
            this.pnlShowForeignCurrency = new DevExpress.XtraEditors.PanelControl();
            this.lbExchangeRate = new DevExpress.XtraEditors.LabelControl();
            this.lbKindMoney = new DevExpress.XtraEditors.LabelControl();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.pnlKeyboardNumber = new DevExpress.XtraEditors.PanelControl();
            this.btnMoveRightNumber = new DevExpress.XtraEditors.SimpleButton();
            this.btn1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPaymentNumber = new DevExpress.XtraEditors.SimpleButton();
            this.btnTranslateNumber = new DevExpress.XtraEditors.SimpleButton();
            this.btn000 = new DevExpress.XtraEditors.SimpleButton();
            this.btn00 = new DevExpress.XtraEditors.SimpleButton();
            this.btn0 = new DevExpress.XtraEditors.SimpleButton();
            this.btnOkNumber = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteNumber = new DevExpress.XtraEditors.SimpleButton();
            this.btn6 = new DevExpress.XtraEditors.SimpleButton();
            this.btn9 = new DevExpress.XtraEditors.SimpleButton();
            this.btn8 = new DevExpress.XtraEditors.SimpleButton();
            this.btn7 = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoveLeftNumber = new DevExpress.XtraEditors.SimpleButton();
            this.btn5 = new DevExpress.XtraEditors.SimpleButton();
            this.btn4 = new DevExpress.XtraEditors.SimpleButton();
            this.btn3 = new DevExpress.XtraEditors.SimpleButton();
            this.btn2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnEscNumber = new DevExpress.XtraEditors.SimpleButton();
            this.pnlKeyBoardText = new DevExpress.XtraEditors.PanelControl();
            this.btnP = new DevExpress.XtraEditors.SimpleButton();
            this.btnZ = new DevExpress.XtraEditors.SimpleButton();
            this.btnY = new DevExpress.XtraEditors.SimpleButton();
            this.btnPaymentText = new DevExpress.XtraEditors.SimpleButton();
            this.btnShift = new DevExpress.XtraEditors.SimpleButton();
            this.btnX = new DevExpress.XtraEditors.SimpleButton();
            this.btnW = new DevExpress.XtraEditors.SimpleButton();
            this.btnV = new DevExpress.XtraEditors.SimpleButton();
            this.btnU = new DevExpress.XtraEditors.SimpleButton();
            this.btnT = new DevExpress.XtraEditors.SimpleButton();
            this.btnS = new DevExpress.XtraEditors.SimpleButton();
            this.btnF = new DevExpress.XtraEditors.SimpleButton();
            this.btnE = new DevExpress.XtraEditors.SimpleButton();
            this.btnD = new DevExpress.XtraEditors.SimpleButton();
            this.btnC = new DevExpress.XtraEditors.SimpleButton();
            this.btnB = new DevExpress.XtraEditors.SimpleButton();
            this.btnA = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoveRigthText = new DevExpress.XtraEditors.SimpleButton();
            this.btnM = new DevExpress.XtraEditors.SimpleButton();
            this.btnTranslateText = new DevExpress.XtraEditors.SimpleButton();
            this.btnR = new DevExpress.XtraEditors.SimpleButton();
            this.btnQ = new DevExpress.XtraEditors.SimpleButton();
            this.btnOkText = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteText = new DevExpress.XtraEditors.SimpleButton();
            this.btnL = new DevExpress.XtraEditors.SimpleButton();
            this.btnI = new DevExpress.XtraEditors.SimpleButton();
            this.btnH = new DevExpress.XtraEditors.SimpleButton();
            this.btnG = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoveLeftText = new DevExpress.XtraEditors.SimpleButton();
            this.btnK = new DevExpress.XtraEditors.SimpleButton();
            this.btnJ = new DevExpress.XtraEditors.SimpleButton();
            this.btnO = new DevExpress.XtraEditors.SimpleButton();
            this.btnN = new DevExpress.XtraEditors.SimpleButton();
            this.btnEscText = new DevExpress.XtraEditors.SimpleButton();
            this.pnlKeyboardMoney = new DevExpress.XtraEditors.PanelControl();
            this.btn500000 = new DevExpress.XtraEditors.SimpleButton();
            this.btn200000 = new DevExpress.XtraEditors.SimpleButton();
            this.btn100000 = new DevExpress.XtraEditors.SimpleButton();
            this.btn50000 = new DevExpress.XtraEditors.SimpleButton();
            this.btn20000 = new DevExpress.XtraEditors.SimpleButton();
            this.btn10000 = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoveRightMoney = new DevExpress.XtraEditors.SimpleButton();
            this.btnPaymentMoney = new DevExpress.XtraEditors.SimpleButton();
            this.btnTranslateMoney = new DevExpress.XtraEditors.SimpleButton();
            this.btnOkMoney = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteMoney = new DevExpress.XtraEditors.SimpleButton();
            this.btn2000 = new DevExpress.XtraEditors.SimpleButton();
            this.btn1000 = new DevExpress.XtraEditors.SimpleButton();
            this.btn500 = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoveLeftMoney = new DevExpress.XtraEditors.SimpleButton();
            this.btn5000 = new DevExpress.XtraEditors.SimpleButton();
            this.btnEcsMoney = new DevExpress.XtraEditors.SimpleButton();
            this.pnlUpDownForeignCurrency = new DevExpress.XtraEditors.PanelControl();
            this.btnDownForeignCurrency = new DevExpress.XtraEditors.SimpleButton();
            this.btnUpForeignCurrency = new DevExpress.XtraEditors.SimpleButton();
            this.pnlForeignCurrency = new DevExpress.XtraEditors.PanelControl();
            this.gcForeignCurrency = new DevExpress.XtraGrid.GridControl();
            this.gvListForiegnCurrency = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TYGIA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PHUTHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnlHeaderCurrentcy = new DevExpress.XtraEditors.PanelControl();
            this.lbCurrentcy = new DevExpress.XtraEditors.LabelControl();
            this.pnlHeaderShowPayment = new DevExpress.XtraEditors.PanelControl();
            this.lbInforName = new DevExpress.XtraEditors.LabelControl();
            this.pnTime = new DevExpress.XtraEditors.PanelControl();
            this.gc_time = new DevExpress.XtraGrid.GridControl();
            this.gv_time = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.THOIGIANVAO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THOIGIANRA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BLOCKKARAOKE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOPHUT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOBLOCK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIABLOCK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THANHTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pnLogo = new DevExpress.XtraEditors.PanelControl();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl25 = new DevExpress.XtraEditors.PanelControl();
            this.lb_Kho = new DevExpress.XtraEditors.LabelControl();
            this.lb_Quay = new DevExpress.XtraEditors.LabelControl();
            this.lbgiay = new DevExpress.XtraEditors.LabelControl();
            this.lbCaBan = new DevExpress.XtraEditors.LabelControl();
            this.lbGio = new DevExpress.XtraEditors.LabelControl();
            this.lb_NhanVien = new DevExpress.XtraEditors.LabelControl();
            this.lbtimeby = new DevExpress.XtraEditors.LabelControl();
            this.panelControl56 = new DevExpress.XtraEditors.PanelControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelControl57 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl58 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.pnlGridviewActionMain)).BeginInit();
            this.pnlGridviewActionMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInforBuyProduct)).BeginInit();
            this.pnlInforBuyProduct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlGridview)).BeginInit();
            this.pnlGridview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcListProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcListGIO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListGIO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDownGridview)).BeginInit();
            this.pnUpDownGridview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInforMoneyAndCustomer)).BeginInit();
            this.pnlInforMoneyAndCustomer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInforCustomer)).BeginInit();
            this.pnlInforCustomer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInnerCustomerShow)).BeginInit();
            this.pnlInnerCustomerShow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDearCustomerShow)).BeginInit();
            this.pnlDearCustomerShow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInforMoney)).BeginInit();
            this.pnlInforMoney.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlActionCashier)).BeginInit();
            this.pnlActionCashier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPayment)).BeginInit();
            this.pnlPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPaymentButton)).BeginInit();
            this.pnlPaymentButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSpace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPaymentShowFill)).BeginInit();
            this.pnlPaymentShowFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlShowInforPayment)).BeginInit();
            this.pnlShowInforPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInputVoucher)).BeginInit();
            this.pnlInputVoucher.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputVoucher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInputGetMoney)).BeginInit();
            this.pnlInputGetMoney.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputMoney.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInnerCustomer)).BeginInit();
            this.pnlInnerCustomer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInnerCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlServiceMoney)).BeginInit();
            this.pnlServiceMoney.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceMoney.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl55)).BeginInit();
            this.panelControl55.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl51)).BeginInit();
            this.panelControl51.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlThird)).BeginInit();
            this.pnlThird.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSecond)).BeginInit();
            this.pnlSecond.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlFirst)).BeginInit();
            this.pnlFirst.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl27)).BeginInit();
            this.panelControl27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMoneyReply)).BeginInit();
            this.pnlMoneyReply.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl29)).BeginInit();
            this.panelControl29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl30)).BeginInit();
            this.panelControl30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl31)).BeginInit();
            this.panelControl31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInputDiscount)).BeginInit();
            this.pnlInputDiscount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputDiscount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDearCustomer)).BeginInit();
            this.pnlDearCustomer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDearCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlVisa)).BeginInit();
            this.pnlVisa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameVisa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCwCvc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdCodeCreate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdCodeInput.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMoneyVisa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdCardVisa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHeaderVisa)).BeginInit();
            this.pnlHeaderVisa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBottomPayment)).BeginInit();
            this.pnlBottomPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKeyboardAndShowForeignCurrency)).BeginInit();
            this.pnlKeyboardAndShowForeignCurrency.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlShowForeignCurrency)).BeginInit();
            this.pnlShowForeignCurrency.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKeyboardNumber)).BeginInit();
            this.pnlKeyboardNumber.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKeyBoardText)).BeginInit();
            this.pnlKeyBoardText.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKeyboardMoney)).BeginInit();
            this.pnlKeyboardMoney.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlUpDownForeignCurrency)).BeginInit();
            this.pnlUpDownForeignCurrency.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlForeignCurrency)).BeginInit();
            this.pnlForeignCurrency.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcForeignCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListForiegnCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHeaderCurrentcy)).BeginInit();
            this.pnlHeaderCurrentcy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHeaderShowPayment)).BeginInit();
            this.pnlHeaderShowPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnTime)).BeginInit();
            this.pnTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc_time)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_time)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnLogo)).BeginInit();
            this.pnLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl25)).BeginInit();
            this.panelControl25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl56)).BeginInit();
            this.panelControl56.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl57)).BeginInit();
            this.panelControl57.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl58)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlGridviewActionMain
            // 
            this.pnlGridviewActionMain.Controls.Add(this.pnlInforBuyProduct);
            this.pnlGridviewActionMain.Controls.Add(this.pnlActionCashier);
            this.pnlGridviewActionMain.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlGridviewActionMain.Location = new System.Drawing.Point(763, 70);
            this.pnlGridviewActionMain.Name = "pnlGridviewActionMain";
            this.pnlGridviewActionMain.Size = new System.Drawing.Size(350, 702);
            this.pnlGridviewActionMain.TabIndex = 3;
            // 
            // pnlInforBuyProduct
            // 
            this.pnlInforBuyProduct.Controls.Add(this.pnlGridview);
            this.pnlInforBuyProduct.Controls.Add(this.pnUpDownGridview);
            this.pnlInforBuyProduct.Controls.Add(this.pnlInforMoneyAndCustomer);
            this.pnlInforBuyProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlInforBuyProduct.Location = new System.Drawing.Point(2, 2);
            this.pnlInforBuyProduct.Name = "pnlInforBuyProduct";
            this.pnlInforBuyProduct.Size = new System.Drawing.Size(346, 400);
            this.pnlInforBuyProduct.TabIndex = 4;
            // 
            // pnlGridview
            // 
            this.pnlGridview.Controls.Add(this.gcListProduct);
            this.pnlGridview.Controls.Add(this.gcListGIO);
            this.pnlGridview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGridview.Location = new System.Drawing.Point(2, 2);
            this.pnlGridview.Name = "pnlGridview";
            this.pnlGridview.Size = new System.Drawing.Size(303, 328);
            this.pnlGridview.TabIndex = 2;
            // 
            // gcListProduct
            // 
            this.gcListProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gcListProduct.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gcListProduct.Location = new System.Drawing.Point(2, 140);
            this.gcListProduct.MainView = this.gvListProduct;
            this.gcListProduct.Name = "gcListProduct";
            this.gcListProduct.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1});
            this.gcListProduct.Size = new System.Drawing.Size(299, 186);
            this.gcListProduct.TabIndex = 20;
            this.gcListProduct.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvListProduct});
            this.gcListProduct.Leave += new System.EventHandler(this.gcListProduct_Leave);
            // 
            // gvListProduct
            // 
            this.gvListProduct.Appearance.EvenRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gvListProduct.Appearance.EvenRow.Options.UseFont = true;
            this.gvListProduct.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gvListProduct.Appearance.FocusedRow.Options.UseFont = true;
            this.gvListProduct.Appearance.SelectedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gvListProduct.Appearance.SelectedRow.Options.UseFont = true;
            this.gvListProduct.ColumnPanelRowHeight = 30;
            this.gvListProduct.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.ProductName,
            this.Quantum,
            this.Price,
            this.TheMoney,
            this.MA_HANGHOA,
            this.GHICHU,
            this.MA_BEP,
            this.THUE,
            this.Discount});
            this.gvListProduct.GridControl = this.gcListProduct;
            this.gvListProduct.Name = "gvListProduct";
            this.gvListProduct.OptionsView.ShowGroupPanel = false;
            this.gvListProduct.OptionsView.ShowIndicator = false;
            this.gvListProduct.RowHeight = 35;
            this.gvListProduct.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gvListProduct.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvListProduct_RowCellClick);
            this.gvListProduct.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gvListProduct_CustomDrawCell);
            this.gvListProduct.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvListProduct_FocusedRowChanged);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "TT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 34;
            // 
            // ProductName
            // 
            this.ProductName.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.ProductName.AppearanceCell.Options.UseFont = true;
            this.ProductName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.ProductName.AppearanceHeader.Options.UseFont = true;
            this.ProductName.AppearanceHeader.Options.UseTextOptions = true;
            this.ProductName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ProductName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ProductName.Caption = "Tên Hàng Hóa";
            this.ProductName.FieldName = "TEN_HANGHOA";
            this.ProductName.Name = "ProductName";
            this.ProductName.OptionsColumn.AllowEdit = false;
            this.ProductName.OptionsColumn.AllowFocus = false;
            this.ProductName.Visible = true;
            this.ProductName.VisibleIndex = 1;
            this.ProductName.Width = 116;
            // 
            // Quantum
            // 
            this.Quantum.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Quantum.AppearanceCell.Options.UseFont = true;
            this.Quantum.AppearanceCell.Options.UseTextOptions = true;
            this.Quantum.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Quantum.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Quantum.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.Quantum.AppearanceHeader.Options.UseFont = true;
            this.Quantum.AppearanceHeader.Options.UseTextOptions = true;
            this.Quantum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Quantum.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Quantum.Caption = "SL";
            this.Quantum.ColumnEdit = this.repositoryItemCalcEdit1;
            this.Quantum.FieldName = "SOLUONG";
            this.Quantum.Name = "Quantum";
            this.Quantum.OptionsColumn.AllowEdit = false;
            this.Quantum.OptionsColumn.AllowFocus = false;
            this.Quantum.Visible = true;
            this.Quantum.VisibleIndex = 2;
            this.Quantum.Width = 51;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // Price
            // 
            this.Price.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Price.AppearanceCell.Options.UseFont = true;
            this.Price.AppearanceCell.Options.UseTextOptions = true;
            this.Price.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Price.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Price.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.Price.AppearanceHeader.Options.UseFont = true;
            this.Price.AppearanceHeader.Options.UseTextOptions = true;
            this.Price.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Price.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Price.Caption = "Đơn Giá";
            this.Price.DisplayFormat.FormatString = "{0:#,###0}";
            this.Price.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.Price.FieldName = "GIABAN";
            this.Price.Name = "Price";
            this.Price.OptionsColumn.AllowEdit = false;
            this.Price.OptionsColumn.AllowFocus = false;
            this.Price.Visible = true;
            this.Price.VisibleIndex = 3;
            this.Price.Width = 96;
            // 
            // TheMoney
            // 
            this.TheMoney.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TheMoney.AppearanceCell.Options.UseFont = true;
            this.TheMoney.AppearanceCell.Options.UseTextOptions = true;
            this.TheMoney.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TheMoney.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TheMoney.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.TheMoney.AppearanceHeader.Options.UseFont = true;
            this.TheMoney.AppearanceHeader.Options.UseTextOptions = true;
            this.TheMoney.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TheMoney.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TheMoney.Caption = "Thành Tiền";
            this.TheMoney.DisplayFormat.FormatString = "{0:#,###0}";
            this.TheMoney.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TheMoney.FieldName = "Thanh_Tien";
            this.TheMoney.Name = "TheMoney";
            this.TheMoney.OptionsColumn.AllowEdit = false;
            this.TheMoney.OptionsColumn.AllowFocus = false;
            this.TheMoney.Visible = true;
            this.TheMoney.VisibleIndex = 4;
            this.TheMoney.Width = 104;
            // 
            // MA_HANGHOA
            // 
            this.MA_HANGHOA.Caption = "Mã Hàng Hóa";
            this.MA_HANGHOA.FieldName = "MA_HANGHOA";
            this.MA_HANGHOA.Name = "MA_HANGHOA";
            this.MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this.MA_HANGHOA.OptionsColumn.AllowFocus = false;
            // 
            // GHICHU
            // 
            this.GHICHU.Caption = "Ghi Chú";
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.Name = "GHICHU";
            // 
            // MA_BEP
            // 
            this.MA_BEP.Caption = "Máy In Bếp";
            this.MA_BEP.FieldName = "MA_BEP";
            this.MA_BEP.Name = "MA_BEP";
            // 
            // THUE
            // 
            this.THUE.Caption = "VAT";
            this.THUE.FieldName = "THUE";
            this.THUE.Name = "THUE";
            // 
            // Discount
            // 
            this.Discount.Caption = "Giảm Giá";
            this.Discount.FieldName = "GiamGia";
            this.Discount.Name = "Discount";
            // 
            // gcListGIO
            // 
            this.gcListGIO.Dock = System.Windows.Forms.DockStyle.Top;
            gridLevelNode2.RelationName = "Level1";
            this.gcListGIO.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gcListGIO.Location = new System.Drawing.Point(2, 2);
            this.gcListGIO.MainView = this.gvListGIO;
            this.gcListGIO.Name = "gcListGIO";
            this.gcListGIO.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit2});
            this.gcListGIO.Size = new System.Drawing.Size(299, 138);
            this.gcListGIO.TabIndex = 21;
            this.gcListGIO.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvListGIO});
            // 
            // gvListGIO
            // 
            this.gvListGIO.Appearance.EvenRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gvListGIO.Appearance.EvenRow.Options.UseFont = true;
            this.gvListGIO.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gvListGIO.Appearance.FocusedRow.Options.UseFont = true;
            this.gvListGIO.Appearance.SelectedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gvListGIO.Appearance.SelectedRow.Options.UseFont = true;
            this.gvListGIO.ColumnPanelRowHeight = 30;
            this.gvListGIO.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT_GIO,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10});
            this.gvListGIO.GridControl = this.gcListGIO;
            this.gvListGIO.Name = "gvListGIO";
            this.gvListGIO.OptionsView.ShowGroupPanel = false;
            this.gvListGIO.OptionsView.ShowIndicator = false;
            this.gvListGIO.RowHeight = 35;
            this.gvListGIO.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gvListGIO.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gvListGIO_CustomDrawCell);
            // 
            // STT_GIO
            // 
            this.STT_GIO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.STT_GIO.AppearanceCell.Options.UseFont = true;
            this.STT_GIO.AppearanceCell.Options.UseTextOptions = true;
            this.STT_GIO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT_GIO.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT_GIO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.STT_GIO.AppearanceHeader.Options.UseFont = true;
            this.STT_GIO.AppearanceHeader.Options.UseTextOptions = true;
            this.STT_GIO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT_GIO.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT_GIO.Caption = "TT";
            this.STT_GIO.Name = "STT_GIO";
            this.STT_GIO.OptionsColumn.AllowEdit = false;
            this.STT_GIO.OptionsColumn.AllowFocus = false;
            this.STT_GIO.Visible = true;
            this.STT_GIO.VisibleIndex = 0;
            this.STT_GIO.Width = 34;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceCell.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn2.Caption = "Tiền Phòng";
            this.gridColumn2.FieldName = "TEN_HANGHOA";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 116;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceCell.Options.UseFont = true;
            this.gridColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn3.Caption = "SL";
            this.gridColumn3.ColumnEdit = this.repositoryItemCalcEdit2;
            this.gridColumn3.FieldName = "SOLUONG";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 51;
            // 
            // repositoryItemCalcEdit2
            // 
            this.repositoryItemCalcEdit2.AutoHeight = false;
            this.repositoryItemCalcEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit2.Name = "repositoryItemCalcEdit2";
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn4.AppearanceCell.Options.UseFont = true;
            this.gridColumn4.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn4.Caption = "Đơn Giá";
            this.gridColumn4.DisplayFormat.FormatString = "{0:#,###0}";
            this.gridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn4.FieldName = "GIABAN";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 96;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn5.AppearanceCell.Options.UseFont = true;
            this.gridColumn5.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.gridColumn5.AppearanceHeader.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn5.Caption = "Thành Tiền";
            this.gridColumn5.DisplayFormat.FormatString = "{0:#,###0}";
            this.gridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn5.FieldName = "Thanh_Tien";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 104;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Mã Hàng Hóa";
            this.gridColumn6.FieldName = "MA_HANGHOA";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Ghi Chú";
            this.gridColumn7.FieldName = "GHICHU";
            this.gridColumn7.Name = "gridColumn7";
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Máy In Bếp";
            this.gridColumn8.FieldName = "MA_BEP";
            this.gridColumn8.Name = "gridColumn8";
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "VAT";
            this.gridColumn9.FieldName = "THUE";
            this.gridColumn9.Name = "gridColumn9";
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Giảm Giá";
            this.gridColumn10.FieldName = "GiamGia";
            this.gridColumn10.Name = "gridColumn10";
            // 
            // pnUpDownGridview
            // 
            this.pnUpDownGridview.Controls.Add(this.labelControl17);
            this.pnUpDownGridview.Controls.Add(this.lbRowsCount);
            this.pnUpDownGridview.Controls.Add(this.lbSTT);
            this.pnUpDownGridview.Controls.Add(this.btnUpGridview);
            this.pnUpDownGridview.Controls.Add(this.btnDownGridview);
            this.pnUpDownGridview.Controls.Add(this.btnLastGridview);
            this.pnUpDownGridview.Controls.Add(this.btnFirstGridview);
            this.pnUpDownGridview.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnUpDownGridview.Location = new System.Drawing.Point(305, 2);
            this.pnUpDownGridview.Name = "pnUpDownGridview";
            this.pnUpDownGridview.Size = new System.Drawing.Size(39, 328);
            this.pnUpDownGridview.TabIndex = 21;
            this.pnUpDownGridview.Visible = false;
            // 
            // labelControl17
            // 
            this.labelControl17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl17.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl17.Location = new System.Drawing.Point(12, 148);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(20, 13);
            this.labelControl17.TabIndex = 7;
            this.labelControl17.Text = "----";
            // 
            // lbRowsCount
            // 
            this.lbRowsCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbRowsCount.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbRowsCount.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbRowsCount.Location = new System.Drawing.Point(13, 162);
            this.lbRowsCount.Name = "lbRowsCount";
            this.lbRowsCount.Size = new System.Drawing.Size(14, 13);
            this.lbRowsCount.TabIndex = 6;
            this.lbRowsCount.Text = "00";
            // 
            // lbSTT
            // 
            this.lbSTT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSTT.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbSTT.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbSTT.Location = new System.Drawing.Point(13, 136);
            this.lbSTT.Name = "lbSTT";
            this.lbSTT.Size = new System.Drawing.Size(14, 13);
            this.lbSTT.TabIndex = 5;
            this.lbSTT.Text = "00";
            // 
            // btnUpGridview
            // 
            this.btnUpGridview.Image = ((System.Drawing.Image)(resources.GetObject("btnUpGridview.Image")));
            this.btnUpGridview.Location = new System.Drawing.Point(4, 71);
            this.btnUpGridview.Name = "btnUpGridview";
            this.btnUpGridview.Size = new System.Drawing.Size(31, 53);
            this.btnUpGridview.TabIndex = 4;
            this.btnUpGridview.Text = "simpleButton7";
            this.btnUpGridview.Click += new System.EventHandler(this.btnUpGridview_Click);
            // 
            // btnDownGridview
            // 
            this.btnDownGridview.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDownGridview.Image = ((System.Drawing.Image)(resources.GetObject("btnDownGridview.Image")));
            this.btnDownGridview.Location = new System.Drawing.Point(4, 203);
            this.btnDownGridview.Name = "btnDownGridview";
            this.btnDownGridview.Size = new System.Drawing.Size(31, 53);
            this.btnDownGridview.TabIndex = 3;
            this.btnDownGridview.Click += new System.EventHandler(this.btnDownGridview_Click);
            // 
            // btnLastGridview
            // 
            this.btnLastGridview.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLastGridview.Image = ((System.Drawing.Image)(resources.GetObject("btnLastGridview.Image")));
            this.btnLastGridview.Location = new System.Drawing.Point(4, 265);
            this.btnLastGridview.Name = "btnLastGridview";
            this.btnLastGridview.Size = new System.Drawing.Size(31, 53);
            this.btnLastGridview.TabIndex = 2;
            this.btnLastGridview.Text = "simpleButton4";
            this.btnLastGridview.Click += new System.EventHandler(this.btnLastGridview_Click);
            // 
            // btnFirstGridview
            // 
            this.btnFirstGridview.Image = ((System.Drawing.Image)(resources.GetObject("btnFirstGridview.Image")));
            this.btnFirstGridview.Location = new System.Drawing.Point(4, 9);
            this.btnFirstGridview.Name = "btnFirstGridview";
            this.btnFirstGridview.Size = new System.Drawing.Size(31, 53);
            this.btnFirstGridview.TabIndex = 0;
            this.btnFirstGridview.Text = "simpleButton1";
            this.btnFirstGridview.Click += new System.EventHandler(this.btnFirstGridview_Click);
            // 
            // pnlInforMoneyAndCustomer
            // 
            this.pnlInforMoneyAndCustomer.Controls.Add(this.pnlInforCustomer);
            this.pnlInforMoneyAndCustomer.Controls.Add(this.pnlInforMoney);
            this.pnlInforMoneyAndCustomer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlInforMoneyAndCustomer.Location = new System.Drawing.Point(2, 330);
            this.pnlInforMoneyAndCustomer.Name = "pnlInforMoneyAndCustomer";
            this.pnlInforMoneyAndCustomer.Size = new System.Drawing.Size(342, 68);
            this.pnlInforMoneyAndCustomer.TabIndex = 0;
            // 
            // pnlInforCustomer
            // 
            this.pnlInforCustomer.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlInforCustomer.Controls.Add(this.pnlInnerCustomerShow);
            this.pnlInforCustomer.Controls.Add(this.pnlDearCustomerShow);
            this.pnlInforCustomer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlInforCustomer.Location = new System.Drawing.Point(2, 2);
            this.pnlInforCustomer.Name = "pnlInforCustomer";
            this.pnlInforCustomer.Size = new System.Drawing.Size(189, 64);
            this.pnlInforCustomer.TabIndex = 16;
            // 
            // pnlInnerCustomerShow
            // 
            this.pnlInnerCustomerShow.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlInnerCustomerShow.Controls.Add(this.lbMoneySurplus);
            this.pnlInnerCustomerShow.Controls.Add(this.lbIdInnerCustomerShow);
            this.pnlInnerCustomerShow.Controls.Add(this.lbNameInnerCustomer);
            this.pnlInnerCustomerShow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlInnerCustomerShow.Location = new System.Drawing.Point(0, 32);
            this.pnlInnerCustomerShow.Name = "pnlInnerCustomerShow";
            this.pnlInnerCustomerShow.Size = new System.Drawing.Size(189, 32);
            this.pnlInnerCustomerShow.TabIndex = 1;
            this.pnlInnerCustomerShow.Visible = false;
            // 
            // lbMoneySurplus
            // 
            this.lbMoneySurplus.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbMoneySurplus.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbMoneySurplus.Location = new System.Drawing.Point(101, 15);
            this.lbMoneySurplus.Name = "lbMoneySurplus";
            this.lbMoneySurplus.Size = new System.Drawing.Size(31, 13);
            this.lbMoneySurplus.TabIndex = 23;
            this.lbMoneySurplus.Text = "số dư";
            this.lbMoneySurplus.SizeChanged += new System.EventHandler(this.lbMoneySurplus_SizeChanged);
            // 
            // lbIdInnerCustomerShow
            // 
            this.lbIdInnerCustomerShow.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbIdInnerCustomerShow.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbIdInnerCustomerShow.Location = new System.Drawing.Point(3, 18);
            this.lbIdInnerCustomerShow.Name = "lbIdInnerCustomerShow";
            this.lbIdInnerCustomerShow.Size = new System.Drawing.Size(0, 13);
            this.lbIdInnerCustomerShow.TabIndex = 21;
            // 
            // lbNameInnerCustomer
            // 
            this.lbNameInnerCustomer.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbNameInnerCustomer.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbNameInnerCustomer.Location = new System.Drawing.Point(3, 3);
            this.lbNameInnerCustomer.Name = "lbNameInnerCustomer";
            this.lbNameInnerCustomer.Size = new System.Drawing.Size(76, 13);
            this.lbNameInnerCustomer.TabIndex = 19;
            this.lbNameInnerCustomer.Text = "Lệnh Hồ Xuân";
            // 
            // pnlDearCustomerShow
            // 
            this.pnlDearCustomerShow.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlDearCustomerShow.Controls.Add(this.lbMark);
            this.pnlDearCustomerShow.Controls.Add(this.lbIDDearCustomerShow);
            this.pnlDearCustomerShow.Controls.Add(this.lbNameDearCustomer);
            this.pnlDearCustomerShow.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDearCustomerShow.Location = new System.Drawing.Point(0, 0);
            this.pnlDearCustomerShow.Name = "pnlDearCustomerShow";
            this.pnlDearCustomerShow.Size = new System.Drawing.Size(189, 32);
            this.pnlDearCustomerShow.TabIndex = 0;
            this.pnlDearCustomerShow.Visible = false;
            // 
            // lbMark
            // 
            this.lbMark.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbMark.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbMark.Location = new System.Drawing.Point(98, 16);
            this.lbMark.Name = "lbMark";
            this.lbMark.Size = new System.Drawing.Size(30, 13);
            this.lbMark.TabIndex = 18;
            this.lbMark.Text = "Điểm";
            this.lbMark.SizeChanged += new System.EventHandler(this.lbMark_SizeChanged);
            // 
            // lbIDDearCustomerShow
            // 
            this.lbIDDearCustomerShow.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbIDDearCustomerShow.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbIDDearCustomerShow.Location = new System.Drawing.Point(3, 18);
            this.lbIDDearCustomerShow.Name = "lbIDDearCustomerShow";
            this.lbIDDearCustomerShow.Size = new System.Drawing.Size(0, 13);
            this.lbIDDearCustomerShow.TabIndex = 16;
            // 
            // lbNameDearCustomer
            // 
            this.lbNameDearCustomer.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbNameDearCustomer.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbNameDearCustomer.Location = new System.Drawing.Point(3, 3);
            this.lbNameDearCustomer.Name = "lbNameDearCustomer";
            this.lbNameDearCustomer.Size = new System.Drawing.Size(76, 13);
            this.lbNameDearCustomer.TabIndex = 14;
            this.lbNameDearCustomer.Text = "Lệnh Hồ Xuân";
            // 
            // pnlInforMoney
            // 
            this.pnlInforMoney.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlInforMoney.Controls.Add(this.labelControl13);
            this.pnlInforMoney.Controls.Add(this.lbTheMoney);
            this.pnlInforMoney.Controls.Add(this.lbTotalMoney);
            this.pnlInforMoney.Controls.Add(this.labelControl52);
            this.pnlInforMoney.Controls.Add(this.lb_clicktiengio);
            this.pnlInforMoney.Controls.Add(this.lbHourMoney);
            this.pnlInforMoney.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlInforMoney.Location = new System.Drawing.Point(191, 2);
            this.pnlInforMoney.Name = "pnlInforMoney";
            this.pnlInforMoney.Size = new System.Drawing.Size(149, 64);
            this.pnlInforMoney.TabIndex = 15;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl13.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl13.Location = new System.Drawing.Point(3, 45);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(61, 13);
            this.labelControl13.TabIndex = 8;
            this.labelControl13.Text = "Tổng cộng:";
            // 
            // lbTheMoney
            // 
            this.lbTheMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbTheMoney.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbTheMoney.Location = new System.Drawing.Point(112, 26);
            this.lbTheMoney.Name = "lbTheMoney";
            this.lbTheMoney.Size = new System.Drawing.Size(7, 13);
            this.lbTheMoney.TabIndex = 14;
            this.lbTheMoney.Text = "0";
            this.lbTheMoney.TextChanged += new System.EventHandler(this.lbTheMoney_TextChanged);
            // 
            // lbTotalMoney
            // 
            this.lbTotalMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbTotalMoney.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbTotalMoney.Location = new System.Drawing.Point(112, 45);
            this.lbTotalMoney.Name = "lbTotalMoney";
            this.lbTotalMoney.Size = new System.Drawing.Size(7, 13);
            this.lbTotalMoney.TabIndex = 9;
            this.lbTotalMoney.Text = "0";
            this.lbTotalMoney.TextChanged += new System.EventHandler(this.lbTotalMoney_TextChanged);
            // 
            // labelControl52
            // 
            this.labelControl52.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl52.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl52.Location = new System.Drawing.Point(3, 26);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(44, 13);
            this.labelControl52.TabIndex = 13;
            this.labelControl52.Text = "Dịch vụ:";
            // 
            // lb_clicktiengio
            // 
            this.lb_clicktiengio.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_clicktiengio.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_clicktiengio.Location = new System.Drawing.Point(3, 7);
            this.lb_clicktiengio.Name = "lb_clicktiengio";
            this.lb_clicktiengio.Size = new System.Drawing.Size(65, 13);
            this.lb_clicktiengio.TabIndex = 11;
            this.lb_clicktiengio.Text = "Tiền phòng:";
            this.lb_clicktiengio.Click += new System.EventHandler(this.lb_clicktiengio_Click);
            // 
            // lbHourMoney
            // 
            this.lbHourMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbHourMoney.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbHourMoney.Location = new System.Drawing.Point(112, 7);
            this.lbHourMoney.Name = "lbHourMoney";
            this.lbHourMoney.Size = new System.Drawing.Size(7, 13);
            this.lbHourMoney.TabIndex = 12;
            this.lbHourMoney.Tag = "0";
            this.lbHourMoney.Text = "0";
            this.lbHourMoney.TextChanged += new System.EventHandler(this.lbHourMoney_TextChanged);
            // 
            // pnlActionCashier
            // 
            this.pnlActionCashier.Controls.Add(this.pnlPayment);
            this.pnlActionCashier.Controls.Add(this.pnlPaymentButton);
            this.pnlActionCashier.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlActionCashier.Location = new System.Drawing.Point(2, 402);
            this.pnlActionCashier.Name = "pnlActionCashier";
            this.pnlActionCashier.Size = new System.Drawing.Size(346, 298);
            this.pnlActionCashier.TabIndex = 3;
            // 
            // pnlPayment
            // 
            this.pnlPayment.Controls.Add(this.btnCongNo);
            this.pnlPayment.Controls.Add(this.btnGiaohang);
            this.pnlPayment.Controls.Add(this.btnVisa);
            this.pnlPayment.Controls.Add(this.btnVoucher);
            this.pnlPayment.Controls.Add(this.btnServiceMoney);
            this.pnlPayment.Controls.Add(this.btnRetypePayment);
            this.pnlPayment.Controls.Add(this.btnDiscount);
            this.pnlPayment.Controls.Add(this.btnCardImtimateCustomer);
            this.pnlPayment.Controls.Add(this.btnCancelPayment);
            this.pnlPayment.Controls.Add(this.btnNotPrintBill);
            this.pnlPayment.Controls.Add(this.btnCardInternalPayment);
            this.pnlPayment.Controls.Add(this.btnOpenSafe);
            this.pnlPayment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPayment.Location = new System.Drawing.Point(2, 43);
            this.pnlPayment.Name = "pnlPayment";
            this.pnlPayment.Size = new System.Drawing.Size(342, 253);
            this.pnlPayment.TabIndex = 20;
            // 
            // btnCongNo
            // 
            this.btnCongNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnCongNo.Appearance.Options.UseFont = true;
            this.btnCongNo.Location = new System.Drawing.Point(172, 172);
            this.btnCongNo.Name = "btnCongNo";
            this.btnCongNo.Size = new System.Drawing.Size(80, 75);
            this.btnCongNo.TabIndex = 36;
            this.btnCongNo.Text = "Công Nợ";
            // 
            // btnGiaohang
            // 
            this.btnGiaohang.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            this.btnGiaohang.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnGiaohang.Appearance.Options.UseBackColor = true;
            this.btnGiaohang.Appearance.Options.UseFont = true;
            this.btnGiaohang.Location = new System.Drawing.Point(89, 172);
            this.btnGiaohang.Name = "btnGiaohang";
            this.btnGiaohang.Size = new System.Drawing.Size(80, 75);
            this.btnGiaohang.TabIndex = 35;
            this.btnGiaohang.Text = "Giao Hàng";
            // 
            // btnVisa
            // 
            this.btnVisa.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            this.btnVisa.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnVisa.Appearance.Options.UseBackColor = true;
            this.btnVisa.Appearance.Options.UseFont = true;
            this.btnVisa.Location = new System.Drawing.Point(172, 89);
            this.btnVisa.Name = "btnVisa";
            this.btnVisa.Size = new System.Drawing.Size(80, 75);
            this.btnVisa.TabIndex = 32;
            this.btnVisa.Text = "Thẻ Visa";
            this.btnVisa.Click += new System.EventHandler(this.btnVisa_Click);
            // 
            // btnVoucher
            // 
            this.btnVoucher.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            this.btnVoucher.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnVoucher.Appearance.Options.UseBackColor = true;
            this.btnVoucher.Appearance.Options.UseFont = true;
            this.btnVoucher.Location = new System.Drawing.Point(172, 6);
            this.btnVoucher.Name = "btnVoucher";
            this.btnVoucher.Size = new System.Drawing.Size(80, 75);
            this.btnVoucher.TabIndex = 31;
            this.btnVoucher.Text = "Voucher";
            this.btnVoucher.Click += new System.EventHandler(this.btnVoucher_Click);
            // 
            // btnServiceMoney
            // 
            this.btnServiceMoney.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            this.btnServiceMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnServiceMoney.Appearance.Options.UseBackColor = true;
            this.btnServiceMoney.Appearance.Options.UseFont = true;
            this.btnServiceMoney.Location = new System.Drawing.Point(6, 6);
            this.btnServiceMoney.Name = "btnServiceMoney";
            this.btnServiceMoney.Size = new System.Drawing.Size(80, 75);
            this.btnServiceMoney.TabIndex = 29;
            this.btnServiceMoney.Text = "Phí Phục Vụ";
            this.btnServiceMoney.Click += new System.EventHandler(this.btnServiceMoney_Click);
            // 
            // btnRetypePayment
            // 
            this.btnRetypePayment.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnRetypePayment.Appearance.Options.UseFont = true;
            this.btnRetypePayment.Location = new System.Drawing.Point(6, 172);
            this.btnRetypePayment.Name = "btnRetypePayment";
            this.btnRetypePayment.Size = new System.Drawing.Size(80, 75);
            this.btnRetypePayment.TabIndex = 28;
            this.btnRetypePayment.Text = "Nhập Lại\r\n Tiền Trả";
            this.btnRetypePayment.Click += new System.EventHandler(this.btnRetypePayment_Click);
            // 
            // btnDiscount
            // 
            this.btnDiscount.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnDiscount.Appearance.Options.UseFont = true;
            this.btnDiscount.Location = new System.Drawing.Point(6, 89);
            this.btnDiscount.Name = "btnDiscount";
            this.btnDiscount.Size = new System.Drawing.Size(80, 75);
            this.btnDiscount.TabIndex = 26;
            this.btnDiscount.Text = "Chiết Khấu";
            this.btnDiscount.Click += new System.EventHandler(this.btnDiscount_Click);
            // 
            // btnCardImtimateCustomer
            // 
            this.btnCardImtimateCustomer.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            this.btnCardImtimateCustomer.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnCardImtimateCustomer.Appearance.Options.UseBackColor = true;
            this.btnCardImtimateCustomer.Appearance.Options.UseFont = true;
            this.btnCardImtimateCustomer.Location = new System.Drawing.Point(89, 89);
            this.btnCardImtimateCustomer.Name = "btnCardImtimateCustomer";
            this.btnCardImtimateCustomer.Size = new System.Drawing.Size(80, 75);
            this.btnCardImtimateCustomer.TabIndex = 25;
            this.btnCardImtimateCustomer.Text = "KHTT";
            this.btnCardImtimateCustomer.Click += new System.EventHandler(this.btnCardImtimateCustomer_Click);
            // 
            // btnCancelPayment
            // 
            this.btnCancelPayment.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnCancelPayment.Appearance.Options.UseFont = true;
            this.btnCancelPayment.Location = new System.Drawing.Point(255, 172);
            this.btnCancelPayment.Name = "btnCancelPayment";
            this.btnCancelPayment.Size = new System.Drawing.Size(80, 75);
            this.btnCancelPayment.TabIndex = 23;
            this.btnCancelPayment.Text = "ESC - Hủy\r\nThanh Toán";
            this.btnCancelPayment.Click += new System.EventHandler(this.btnCancelPayment_Click);
            // 
            // btnNotPrintBill
            // 
            this.btnNotPrintBill.Appearance.Font = new System.Drawing.Font("Tahoma", 8.2F, System.Drawing.FontStyle.Bold);
            this.btnNotPrintBill.Appearance.Options.UseFont = true;
            this.btnNotPrintBill.Location = new System.Drawing.Point(255, 6);
            this.btnNotPrintBill.Name = "btnNotPrintBill";
            this.btnNotPrintBill.Size = new System.Drawing.Size(80, 75);
            this.btnNotPrintBill.TabIndex = 22;
            this.btnNotPrintBill.Text = "Không In \r\nHóa Đơn\r\n";
            this.btnNotPrintBill.Click += new System.EventHandler(this.btnNotPrintBill_Click);
            // 
            // btnCardInternalPayment
            // 
            this.btnCardInternalPayment.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            this.btnCardInternalPayment.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnCardInternalPayment.Appearance.Options.UseBackColor = true;
            this.btnCardInternalPayment.Appearance.Options.UseFont = true;
            this.btnCardInternalPayment.Location = new System.Drawing.Point(89, 6);
            this.btnCardInternalPayment.Name = "btnCardInternalPayment";
            this.btnCardInternalPayment.Size = new System.Drawing.Size(80, 75);
            this.btnCardInternalPayment.TabIndex = 21;
            this.btnCardInternalPayment.Text = "Thẻ TTNB";
            this.btnCardInternalPayment.Click += new System.EventHandler(this.btnCardInternalPayment_Click);
            // 
            // btnOpenSafe
            // 
            this.btnOpenSafe.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnOpenSafe.Appearance.Options.UseFont = true;
            this.btnOpenSafe.Location = new System.Drawing.Point(255, 89);
            this.btnOpenSafe.Name = "btnOpenSafe";
            this.btnOpenSafe.Size = new System.Drawing.Size(80, 75);
            this.btnOpenSafe.TabIndex = 20;
            this.btnOpenSafe.Text = "Mở Két";
            this.btnOpenSafe.Click += new System.EventHandler(this.btnOpenSafe_Click);
            // 
            // pnlPaymentButton
            // 
            this.pnlPaymentButton.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            this.pnlPaymentButton.Appearance.Options.UseBackColor = true;
            this.pnlPaymentButton.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlPaymentButton.Controls.Add(this.labelControl7);
            this.pnlPaymentButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlPaymentButton.Location = new System.Drawing.Point(2, 2);
            this.pnlPaymentButton.Name = "pnlPaymentButton";
            this.pnlPaymentButton.Size = new System.Drawing.Size(342, 41);
            this.pnlPaymentButton.TabIndex = 4;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl7.Location = new System.Drawing.Point(121, 9);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(122, 24);
            this.labelControl7.TabIndex = 15;
            this.labelControl7.Text = "THANH TOÁN";
            // 
            // pnlSpace
            // 
            this.pnlSpace.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlSpace.Location = new System.Drawing.Point(732, 70);
            this.pnlSpace.Name = "pnlSpace";
            this.pnlSpace.Size = new System.Drawing.Size(31, 702);
            this.pnlSpace.TabIndex = 2;
            // 
            // pnlPaymentShowFill
            // 
            this.pnlPaymentShowFill.Controls.Add(this.pnlShowInforPayment);
            this.pnlPaymentShowFill.Controls.Add(this.pnlVisa);
            this.pnlPaymentShowFill.Controls.Add(this.pnlBottomPayment);
            this.pnlPaymentShowFill.Controls.Add(this.pnlHeaderShowPayment);
            this.pnlPaymentShowFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPaymentShowFill.Location = new System.Drawing.Point(0, 70);
            this.pnlPaymentShowFill.Name = "pnlPaymentShowFill";
            this.pnlPaymentShowFill.Size = new System.Drawing.Size(732, 702);
            this.pnlPaymentShowFill.TabIndex = 4;
            // 
            // pnlShowInforPayment
            // 
            this.pnlShowInforPayment.Controls.Add(this.pnlInputVoucher);
            this.pnlShowInforPayment.Controls.Add(this.pnlInputGetMoney);
            this.pnlShowInforPayment.Controls.Add(this.pnlInnerCustomer);
            this.pnlShowInforPayment.Controls.Add(this.pnlServiceMoney);
            this.pnlShowInforPayment.Controls.Add(this.panelControl55);
            this.pnlShowInforPayment.Controls.Add(this.panelControl51);
            this.pnlShowInforPayment.Controls.Add(this.pnlThird);
            this.pnlShowInforPayment.Controls.Add(this.pnlSecond);
            this.pnlShowInforPayment.Controls.Add(this.pnlFirst);
            this.pnlShowInforPayment.Controls.Add(this.labelControl22);
            this.pnlShowInforPayment.Controls.Add(this.panelControl27);
            this.pnlShowInforPayment.Controls.Add(this.pnlMoneyReply);
            this.pnlShowInforPayment.Controls.Add(this.panelControl29);
            this.pnlShowInforPayment.Controls.Add(this.panelControl30);
            this.pnlShowInforPayment.Controls.Add(this.panelControl31);
            this.pnlShowInforPayment.Controls.Add(this.labelControl30);
            this.pnlShowInforPayment.Controls.Add(this.pnlInputDiscount);
            this.pnlShowInforPayment.Controls.Add(this.pnlDearCustomer);
            this.pnlShowInforPayment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlShowInforPayment.Location = new System.Drawing.Point(2, 37);
            this.pnlShowInforPayment.Name = "pnlShowInforPayment";
            this.pnlShowInforPayment.Size = new System.Drawing.Size(728, 365);
            this.pnlShowInforPayment.TabIndex = 36;
            // 
            // pnlInputVoucher
            // 
            this.pnlInputVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlInputVoucher.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlInputVoucher.Controls.Add(this.txtInputVoucher);
            this.pnlInputVoucher.Controls.Add(this.labelControl35);
            this.pnlInputVoucher.Location = new System.Drawing.Point(53, 316);
            this.pnlInputVoucher.Name = "pnlInputVoucher";
            this.pnlInputVoucher.Size = new System.Drawing.Size(629, 47);
            this.pnlInputVoucher.TabIndex = 48;
            this.pnlInputVoucher.Visible = false;
            // 
            // txtInputVoucher
            // 
            this.txtInputVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInputVoucher.EditValue = "0";
            this.txtInputVoucher.Location = new System.Drawing.Point(269, 7);
            this.txtInputVoucher.Name = "txtInputVoucher";
            this.txtInputVoucher.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.txtInputVoucher.Properties.Appearance.Options.UseFont = true;
            this.txtInputVoucher.Properties.Appearance.Options.UseTextOptions = true;
            this.txtInputVoucher.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtInputVoucher.Properties.Mask.EditMask = "N0";
            this.txtInputVoucher.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtInputVoucher.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtInputVoucher.Size = new System.Drawing.Size(310, 32);
            this.txtInputVoucher.TabIndex = 45;
            this.txtInputVoucher.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInputVoucher_KeyPress);
            // 
            // labelControl35
            // 
            this.labelControl35.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl35.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl35.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl35.Location = new System.Drawing.Point(5, 13);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(113, 19);
            this.labelControl35.TabIndex = 44;
            this.labelControl35.Text = "Nhập Voucher";
            // 
            // pnlInputGetMoney
            // 
            this.pnlInputGetMoney.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlInputGetMoney.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlInputGetMoney.Controls.Add(this.lbcurrencyGetMoney);
            this.pnlInputGetMoney.Controls.Add(this.txtInputMoney);
            this.pnlInputGetMoney.Controls.Add(this.lbPayment);
            this.pnlInputGetMoney.Location = new System.Drawing.Point(53, 315);
            this.pnlInputGetMoney.Name = "pnlInputGetMoney";
            this.pnlInputGetMoney.Size = new System.Drawing.Size(629, 47);
            this.pnlInputGetMoney.TabIndex = 46;
            // 
            // lbcurrencyGetMoney
            // 
            this.lbcurrencyGetMoney.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbcurrencyGetMoney.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbcurrencyGetMoney.Location = new System.Drawing.Point(589, 15);
            this.lbcurrencyGetMoney.Name = "lbcurrencyGetMoney";
            this.lbcurrencyGetMoney.Size = new System.Drawing.Size(43, 19);
            this.lbcurrencyGetMoney.TabIndex = 46;
            this.lbcurrencyGetMoney.Text = "  VND";
            // 
            // txtInputMoney
            // 
            this.txtInputMoney.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInputMoney.EditValue = "0";
            this.txtInputMoney.Location = new System.Drawing.Point(229, 7);
            this.txtInputMoney.Name = "txtInputMoney";
            this.txtInputMoney.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.txtInputMoney.Properties.Appearance.Options.UseFont = true;
            this.txtInputMoney.Properties.Appearance.Options.UseTextOptions = true;
            this.txtInputMoney.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtInputMoney.Properties.Mask.EditMask = "N0";
            this.txtInputMoney.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtInputMoney.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtInputMoney.Size = new System.Drawing.Size(350, 32);
            this.txtInputMoney.TabIndex = 45;
            this.txtInputMoney.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInputMoney_KeyPress);
            // 
            // lbPayment
            // 
            this.lbPayment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbPayment.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPayment.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lbPayment.Location = new System.Drawing.Point(5, 13);
            this.lbPayment.Name = "lbPayment";
            this.lbPayment.Size = new System.Drawing.Size(108, 19);
            this.lbPayment.TabIndex = 44;
            this.lbPayment.Text = "Nhập tiền trả";
            // 
            // pnlInnerCustomer
            // 
            this.pnlInnerCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlInnerCustomer.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlInnerCustomer.Controls.Add(this.txtInnerCustomer);
            this.pnlInnerCustomer.Controls.Add(this.labelControl5);
            this.pnlInnerCustomer.Location = new System.Drawing.Point(53, 315);
            this.pnlInnerCustomer.Name = "pnlInnerCustomer";
            this.pnlInnerCustomer.Size = new System.Drawing.Size(629, 47);
            this.pnlInnerCustomer.TabIndex = 50;
            // 
            // txtInnerCustomer
            // 
            this.txtInnerCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInnerCustomer.EditValue = "";
            this.txtInnerCustomer.Location = new System.Drawing.Point(174, 7);
            this.txtInnerCustomer.Name = "txtInnerCustomer";
            this.txtInnerCustomer.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.txtInnerCustomer.Properties.Appearance.Options.UseFont = true;
            this.txtInnerCustomer.Properties.Appearance.Options.UseTextOptions = true;
            this.txtInnerCustomer.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtInnerCustomer.Size = new System.Drawing.Size(405, 32);
            this.txtInnerCustomer.TabIndex = 45;
            this.txtInnerCustomer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInnerCustomer_KeyPress);
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl5.Location = new System.Drawing.Point(5, 13);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(127, 19);
            this.labelControl5.TabIndex = 44;
            this.labelControl5.Text = "Nhập mã TTNB ";
            // 
            // pnlServiceMoney
            // 
            this.pnlServiceMoney.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlServiceMoney.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlServiceMoney.Controls.Add(this.lbcurrencyService);
            this.pnlServiceMoney.Controls.Add(this.txtServiceMoney);
            this.pnlServiceMoney.Controls.Add(this.labelControl6);
            this.pnlServiceMoney.Location = new System.Drawing.Point(53, 315);
            this.pnlServiceMoney.Name = "pnlServiceMoney";
            this.pnlServiceMoney.Size = new System.Drawing.Size(629, 47);
            this.pnlServiceMoney.TabIndex = 48;
            this.pnlServiceMoney.Visible = false;
            // 
            // lbcurrencyService
            // 
            this.lbcurrencyService.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbcurrencyService.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbcurrencyService.Location = new System.Drawing.Point(588, 15);
            this.lbcurrencyService.Name = "lbcurrencyService";
            this.lbcurrencyService.Size = new System.Drawing.Size(43, 19);
            this.lbcurrencyService.TabIndex = 46;
            this.lbcurrencyService.Text = "  VND";
            // 
            // txtServiceMoney
            // 
            this.txtServiceMoney.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtServiceMoney.EditValue = "0";
            this.txtServiceMoney.Location = new System.Drawing.Point(174, 7);
            this.txtServiceMoney.Name = "txtServiceMoney";
            this.txtServiceMoney.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.txtServiceMoney.Properties.Appearance.Options.UseFont = true;
            this.txtServiceMoney.Properties.Appearance.Options.UseTextOptions = true;
            this.txtServiceMoney.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtServiceMoney.Properties.Mask.EditMask = "N0";
            this.txtServiceMoney.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtServiceMoney.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtServiceMoney.Size = new System.Drawing.Size(405, 32);
            this.txtServiceMoney.TabIndex = 45;
            this.txtServiceMoney.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtServiceMoney_KeyPress);
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl6.Location = new System.Drawing.Point(5, 13);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(146, 19);
            this.labelControl6.TabIndex = 44;
            this.labelControl6.Text = "Nhập tiền phục vụ";
            // 
            // panelControl55
            // 
            this.panelControl55.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl55.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl55.Controls.Add(this.lbMoneyGetFirst);
            this.panelControl55.Controls.Add(this.labelControl48);
            this.panelControl55.Controls.Add(this.labelControl50);
            this.panelControl55.Location = new System.Drawing.Point(52, 88);
            this.panelControl55.Name = "panelControl55";
            this.panelControl55.Size = new System.Drawing.Size(623, 23);
            this.panelControl55.TabIndex = 49;
            // 
            // lbMoneyGetFirst
            // 
            this.lbMoneyGetFirst.Appearance.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMoneyGetFirst.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lbMoneyGetFirst.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbMoneyGetFirst.Location = new System.Drawing.Point(566, 0);
            this.lbMoneyGetFirst.Name = "lbMoneyGetFirst";
            this.lbMoneyGetFirst.Size = new System.Drawing.Size(10, 22);
            this.lbMoneyGetFirst.TabIndex = 0;
            this.lbMoneyGetFirst.Tag = "0";
            this.lbMoneyGetFirst.Text = "0";
            this.lbMoneyGetFirst.TextChanged += new System.EventHandler(this.lbMoneyGetFirst_TextChanged);
            // 
            // labelControl48
            // 
            this.labelControl48.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl48.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl48.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelControl48.Location = new System.Drawing.Point(576, 0);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(47, 19);
            this.labelControl48.TabIndex = 1;
            this.labelControl48.Text = "   VND";
            // 
            // labelControl50
            // 
            this.labelControl50.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl50.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl50.Location = new System.Drawing.Point(5, 1);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(124, 19);
            this.labelControl50.TabIndex = 0;
            this.labelControl50.Text = "Tiền đặt cọc     ";
            // 
            // panelControl51
            // 
            this.panelControl51.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl51.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl51.Controls.Add(this.lbServiceMoney);
            this.panelControl51.Controls.Add(this.labelControl11);
            this.panelControl51.Controls.Add(this.labelControl47);
            this.panelControl51.Location = new System.Drawing.Point(51, 61);
            this.panelControl51.Name = "panelControl51";
            this.panelControl51.Size = new System.Drawing.Size(623, 23);
            this.panelControl51.TabIndex = 48;
            // 
            // lbServiceMoney
            // 
            this.lbServiceMoney.Appearance.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbServiceMoney.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lbServiceMoney.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbServiceMoney.Location = new System.Drawing.Point(566, 0);
            this.lbServiceMoney.Name = "lbServiceMoney";
            this.lbServiceMoney.Size = new System.Drawing.Size(10, 22);
            this.lbServiceMoney.TabIndex = 0;
            this.lbServiceMoney.Tag = "0";
            this.lbServiceMoney.Text = "0";
            this.lbServiceMoney.TextChanged += new System.EventHandler(this.lbMoneyService_TextChanged);
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl11.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelControl11.Location = new System.Drawing.Point(576, 0);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(47, 19);
            this.labelControl11.TabIndex = 1;
            this.labelControl11.Text = "   VND";
            // 
            // labelControl47
            // 
            this.labelControl47.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl47.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl47.Location = new System.Drawing.Point(5, 1);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(128, 19);
            this.labelControl47.TabIndex = 0;
            this.labelControl47.Text = "Phí phục vụ       ";
            // 
            // pnlThird
            // 
            this.pnlThird.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlThird.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlThird.Controls.Add(this.lbThird);
            this.pnlThird.Controls.Add(this.lbThirdMoney);
            this.pnlThird.Location = new System.Drawing.Point(50, 157);
            this.pnlThird.Name = "pnlThird";
            this.pnlThird.Size = new System.Drawing.Size(625, 25);
            this.pnlThird.TabIndex = 37;
            this.pnlThird.Visible = false;
            // 
            // lbThird
            // 
            this.lbThird.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbThird.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbThird.Location = new System.Drawing.Point(5, 1);
            this.lbThird.Name = "lbThird";
            this.lbThird.Size = new System.Drawing.Size(0, 19);
            this.lbThird.TabIndex = 0;
            // 
            // lbThirdMoney
            // 
            this.lbThirdMoney.Appearance.Font = new System.Drawing.Font("Times New Roman", 14.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbThirdMoney.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbThirdMoney.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbThirdMoney.Location = new System.Drawing.Point(625, 0);
            this.lbThirdMoney.Name = "lbThirdMoney";
            this.lbThirdMoney.Size = new System.Drawing.Size(0, 22);
            this.lbThirdMoney.TabIndex = 0;
            this.lbThirdMoney.TextChanged += new System.EventHandler(this.lbThirdMoney_TextChanged);
            // 
            // pnlSecond
            // 
            this.pnlSecond.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlSecond.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlSecond.Controls.Add(this.lbSecond);
            this.pnlSecond.Controls.Add(this.lbSecondMoney);
            this.pnlSecond.Location = new System.Drawing.Point(50, 186);
            this.pnlSecond.Name = "pnlSecond";
            this.pnlSecond.Size = new System.Drawing.Size(625, 25);
            this.pnlSecond.TabIndex = 43;
            this.pnlSecond.Visible = false;
            // 
            // lbSecond
            // 
            this.lbSecond.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSecond.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbSecond.Location = new System.Drawing.Point(5, 1);
            this.lbSecond.Name = "lbSecond";
            this.lbSecond.Size = new System.Drawing.Size(0, 19);
            this.lbSecond.TabIndex = 0;
            // 
            // lbSecondMoney
            // 
            this.lbSecondMoney.Appearance.Font = new System.Drawing.Font("Times New Roman", 14.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSecondMoney.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbSecondMoney.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbSecondMoney.Location = new System.Drawing.Point(625, 0);
            this.lbSecondMoney.Name = "lbSecondMoney";
            this.lbSecondMoney.Size = new System.Drawing.Size(0, 22);
            this.lbSecondMoney.TabIndex = 0;
            this.lbSecondMoney.TextChanged += new System.EventHandler(this.lbSecondMoney_TextChanged);
            // 
            // pnlFirst
            // 
            this.pnlFirst.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlFirst.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlFirst.Controls.Add(this.lbFirst);
            this.pnlFirst.Controls.Add(this.lbFirstMoney);
            this.pnlFirst.Location = new System.Drawing.Point(50, 215);
            this.pnlFirst.Name = "pnlFirst";
            this.pnlFirst.Size = new System.Drawing.Size(625, 25);
            this.pnlFirst.TabIndex = 38;
            this.pnlFirst.Visible = false;
            // 
            // lbFirst
            // 
            this.lbFirst.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFirst.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbFirst.Location = new System.Drawing.Point(5, 1);
            this.lbFirst.Name = "lbFirst";
            this.lbFirst.Size = new System.Drawing.Size(0, 19);
            this.lbFirst.TabIndex = 0;
            // 
            // lbFirstMoney
            // 
            this.lbFirstMoney.Appearance.Font = new System.Drawing.Font("Times New Roman", 14.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFirstMoney.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbFirstMoney.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbFirstMoney.Location = new System.Drawing.Point(625, 0);
            this.lbFirstMoney.Name = "lbFirstMoney";
            this.lbFirstMoney.Size = new System.Drawing.Size(0, 22);
            this.lbFirstMoney.TabIndex = 0;
            this.lbFirstMoney.TextChanged += new System.EventHandler(this.lbFirstMoney_TextChanged);
            // 
            // labelControl22
            // 
            this.labelControl22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl22.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.labelControl22.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl22.Location = new System.Drawing.Point(53, 246);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(622, 1);
            this.labelControl22.TabIndex = 41;
            // 
            // panelControl27
            // 
            this.panelControl27.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl27.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl27.Controls.Add(this.lbDiscountPayment);
            this.panelControl27.Controls.Add(this.lbDiscount);
            this.panelControl27.Controls.Add(this.labelControl24);
            this.panelControl27.Location = new System.Drawing.Point(51, 35);
            this.panelControl27.Name = "panelControl27";
            this.panelControl27.Size = new System.Drawing.Size(624, 23);
            this.panelControl27.TabIndex = 39;
            // 
            // lbDiscountPayment
            // 
            this.lbDiscountPayment.Appearance.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDiscountPayment.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lbDiscountPayment.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbDiscountPayment.Location = new System.Drawing.Point(567, 0);
            this.lbDiscountPayment.Name = "lbDiscountPayment";
            this.lbDiscountPayment.Size = new System.Drawing.Size(10, 22);
            this.lbDiscountPayment.TabIndex = 0;
            this.lbDiscountPayment.Tag = "0";
            this.lbDiscountPayment.Text = "0";
            this.lbDiscountPayment.TextChanged += new System.EventHandler(this.lbDiscountPayment_TextChanged);
            // 
            // lbDiscount
            // 
            this.lbDiscount.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDiscount.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbDiscount.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbDiscount.Location = new System.Drawing.Point(577, 0);
            this.lbDiscount.Name = "lbDiscount";
            this.lbDiscount.Size = new System.Drawing.Size(47, 19);
            this.lbDiscount.TabIndex = 1;
            this.lbDiscount.Text = "   VND";
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl24.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl24.Location = new System.Drawing.Point(5, 1);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(129, 19);
            this.labelControl24.TabIndex = 0;
            this.labelControl24.Text = "Chiết Khấu        ";
            // 
            // pnlMoneyReply
            // 
            this.pnlMoneyReply.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlMoneyReply.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlMoneyReply.Controls.Add(this.lbReplyCustomer);
            this.pnlMoneyReply.Controls.Add(this.lbVNDReply);
            this.pnlMoneyReply.Controls.Add(this.labelControl25);
            this.pnlMoneyReply.Location = new System.Drawing.Point(50, 284);
            this.pnlMoneyReply.Name = "pnlMoneyReply";
            this.pnlMoneyReply.Size = new System.Drawing.Size(625, 25);
            this.pnlMoneyReply.TabIndex = 34;
            // 
            // lbReplyCustomer
            // 
            this.lbReplyCustomer.Appearance.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbReplyCustomer.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lbReplyCustomer.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbReplyCustomer.Location = new System.Drawing.Point(571, 0);
            this.lbReplyCustomer.Name = "lbReplyCustomer";
            this.lbReplyCustomer.Size = new System.Drawing.Size(11, 24);
            this.lbReplyCustomer.TabIndex = 0;
            this.lbReplyCustomer.Tag = "0";
            this.lbReplyCustomer.Text = "0";
            // 
            // lbVNDReply
            // 
            this.lbVNDReply.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVNDReply.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbVNDReply.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbVNDReply.Location = new System.Drawing.Point(582, 0);
            this.lbVNDReply.Name = "lbVNDReply";
            this.lbVNDReply.Size = new System.Drawing.Size(43, 19);
            this.lbVNDReply.TabIndex = 1;
            this.lbVNDReply.Text = "  VND";
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl25.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl25.Location = new System.Drawing.Point(5, 3);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(120, 19);
            this.labelControl25.TabIndex = 0;
            this.labelControl25.Text = "Tiền trả lại      ";
            // 
            // panelControl29
            // 
            this.panelControl29.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl29.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl29.Controls.Add(this.lbTotalMoneyAll);
            this.panelControl29.Controls.Add(this.labelControl18);
            this.panelControl29.Controls.Add(this.labelControl26);
            this.panelControl29.Location = new System.Drawing.Point(51, 124);
            this.panelControl29.Name = "panelControl29";
            this.panelControl29.Size = new System.Drawing.Size(624, 29);
            this.panelControl29.TabIndex = 35;
            // 
            // lbTotalMoneyAll
            // 
            this.lbTotalMoneyAll.Appearance.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalMoneyAll.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lbTotalMoneyAll.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbTotalMoneyAll.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbTotalMoneyAll.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbTotalMoneyAll.Location = new System.Drawing.Point(566, 0);
            this.lbTotalMoneyAll.Name = "lbTotalMoneyAll";
            this.lbTotalMoneyAll.Size = new System.Drawing.Size(11, 24);
            this.lbTotalMoneyAll.TabIndex = 0;
            this.lbTotalMoneyAll.Tag = "0";
            this.lbTotalMoneyAll.Text = "0";
            this.lbTotalMoneyAll.TextChanged += new System.EventHandler(this.lbTotalMoneyAll_TextChanged);
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl18.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl18.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelControl18.Location = new System.Drawing.Point(577, 0);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(47, 19);
            this.labelControl18.TabIndex = 1;
            this.labelControl18.Text = "   VND";
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl26.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.labelControl26.Location = new System.Drawing.Point(5, 1);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(116, 19);
            this.labelControl26.TabIndex = 0;
            this.labelControl26.Text = "TC thanh toán";
            // 
            // panelControl30
            // 
            this.panelControl30.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl30.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl30.Controls.Add(this.lbGetCustomer);
            this.panelControl30.Controls.Add(this.labelControl2);
            this.panelControl30.Controls.Add(this.labelControl28);
            this.panelControl30.Location = new System.Drawing.Point(50, 255);
            this.panelControl30.Name = "panelControl30";
            this.panelControl30.Size = new System.Drawing.Size(625, 26);
            this.panelControl30.TabIndex = 36;
            // 
            // lbGetCustomer
            // 
            this.lbGetCustomer.Appearance.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGetCustomer.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lbGetCustomer.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbGetCustomer.Location = new System.Drawing.Point(571, 0);
            this.lbGetCustomer.Name = "lbGetCustomer";
            this.lbGetCustomer.Size = new System.Drawing.Size(11, 24);
            this.lbGetCustomer.TabIndex = 0;
            this.lbGetCustomer.Tag = "0";
            this.lbGetCustomer.Text = "0";
            this.lbGetCustomer.TextChanged += new System.EventHandler(this.lbGetCustomer_TextChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelControl2.Location = new System.Drawing.Point(582, 0);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(43, 19);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "  VND";
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl28.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl28.Location = new System.Drawing.Point(5, 1);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(118, 19);
            this.labelControl28.TabIndex = 0;
            this.labelControl28.Text = "Tiền khách trả";
            // 
            // panelControl31
            // 
            this.panelControl31.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl31.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl31.Controls.Add(this.lbTotalMoneyPayment);
            this.panelControl31.Controls.Add(this.labelControl20);
            this.panelControl31.Controls.Add(this.labelControl29);
            this.panelControl31.Location = new System.Drawing.Point(51, 7);
            this.panelControl31.Name = "panelControl31";
            this.panelControl31.Size = new System.Drawing.Size(624, 25);
            this.panelControl31.TabIndex = 33;
            // 
            // lbTotalMoneyPayment
            // 
            this.lbTotalMoneyPayment.Appearance.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalMoneyPayment.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lbTotalMoneyPayment.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbTotalMoneyPayment.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbTotalMoneyPayment.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbTotalMoneyPayment.Location = new System.Drawing.Point(567, 0);
            this.lbTotalMoneyPayment.Name = "lbTotalMoneyPayment";
            this.lbTotalMoneyPayment.Size = new System.Drawing.Size(10, 22);
            this.lbTotalMoneyPayment.TabIndex = 0;
            this.lbTotalMoneyPayment.Tag = "0";
            this.lbTotalMoneyPayment.Text = "0";
            this.lbTotalMoneyPayment.TextChanged += new System.EventHandler(this.lbTotalMoneyPayment_TextChanged);
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl20.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl20.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelControl20.Location = new System.Drawing.Point(577, 0);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(47, 19);
            this.labelControl20.TabIndex = 1;
            this.labelControl20.Text = "   VND";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl29.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl29.Location = new System.Drawing.Point(5, 3);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(143, 19);
            this.labelControl29.TabIndex = 0;
            this.labelControl29.Text = "Tổng Thanh Toán";
            // 
            // labelControl30
            // 
            this.labelControl30.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl30.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.labelControl30.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl30.Location = new System.Drawing.Point(52, 119);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(622, 1);
            this.labelControl30.TabIndex = 32;
            // 
            // pnlInputDiscount
            // 
            this.pnlInputDiscount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlInputDiscount.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlInputDiscount.Controls.Add(this.txtInputDiscount);
            this.pnlInputDiscount.Controls.Add(this.labelControl3);
            this.pnlInputDiscount.Location = new System.Drawing.Point(53, 315);
            this.pnlInputDiscount.Name = "pnlInputDiscount";
            this.pnlInputDiscount.Size = new System.Drawing.Size(629, 47);
            this.pnlInputDiscount.TabIndex = 47;
            this.pnlInputDiscount.Visible = false;
            // 
            // txtInputDiscount
            // 
            this.txtInputDiscount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInputDiscount.EditValue = "0";
            this.txtInputDiscount.Location = new System.Drawing.Point(269, 7);
            this.txtInputDiscount.Name = "txtInputDiscount";
            this.txtInputDiscount.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.txtInputDiscount.Properties.Appearance.Options.UseFont = true;
            this.txtInputDiscount.Properties.Appearance.Options.UseTextOptions = true;
            this.txtInputDiscount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtInputDiscount.Properties.Mask.EditMask = "N0";
            this.txtInputDiscount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtInputDiscount.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtInputDiscount.Size = new System.Drawing.Size(310, 32);
            this.txtInputDiscount.TabIndex = 45;
            this.txtInputDiscount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInputDiscount_KeyPress);
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl3.Location = new System.Drawing.Point(5, 13);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(132, 19);
            this.labelControl3.TabIndex = 44;
            this.labelControl3.Text = "Nhập chiết khấu";
            // 
            // pnlDearCustomer
            // 
            this.pnlDearCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlDearCustomer.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlDearCustomer.Controls.Add(this.txtDearCustomer);
            this.pnlDearCustomer.Controls.Add(this.labelControl4);
            this.pnlDearCustomer.Location = new System.Drawing.Point(53, 315);
            this.pnlDearCustomer.Name = "pnlDearCustomer";
            this.pnlDearCustomer.Size = new System.Drawing.Size(629, 47);
            this.pnlDearCustomer.TabIndex = 47;
            // 
            // txtDearCustomer
            // 
            this.txtDearCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDearCustomer.EditValue = "";
            this.txtDearCustomer.Location = new System.Drawing.Point(175, 7);
            this.txtDearCustomer.Name = "txtDearCustomer";
            this.txtDearCustomer.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.txtDearCustomer.Properties.Appearance.Options.UseFont = true;
            this.txtDearCustomer.Properties.Appearance.Options.UseTextOptions = true;
            this.txtDearCustomer.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtDearCustomer.Size = new System.Drawing.Size(404, 32);
            this.txtDearCustomer.TabIndex = 45;
            this.txtDearCustomer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDearCustomer_KeyPress);
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl4.Location = new System.Drawing.Point(5, 13);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(127, 19);
            this.labelControl4.TabIndex = 44;
            this.labelControl4.Text = "Nhập mã KHTT ";
            // 
            // pnlVisa
            // 
            this.pnlVisa.Controls.Add(this.panelControl2);
            this.pnlVisa.Controls.Add(this.panelControl1);
            this.pnlVisa.Controls.Add(this.pnlHeaderVisa);
            this.pnlVisa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlVisa.Location = new System.Drawing.Point(2, 37);
            this.pnlVisa.Name = "pnlVisa";
            this.pnlVisa.Size = new System.Drawing.Size(728, 365);
            this.pnlVisa.TabIndex = 51;
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.panelControl4);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Location = new System.Drawing.Point(5, 60);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(720, 260);
            this.panelControl2.TabIndex = 25;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.txtCountry);
            this.panelControl4.Controls.Add(this.txtNameVisa);
            this.panelControl4.Controls.Add(this.labelControl9);
            this.panelControl4.Controls.Add(this.btnRefeshIdCode);
            this.panelControl4.Controls.Add(this.txtCwCvc);
            this.panelControl4.Controls.Add(this.labelControl12);
            this.panelControl4.Controls.Add(this.txtIdCodeCreate);
            this.panelControl4.Controls.Add(this.labelControl42);
            this.panelControl4.Controls.Add(this.txtCity);
            this.panelControl4.Controls.Add(this.txtIdCodeInput);
            this.panelControl4.Controls.Add(this.txtAddress);
            this.panelControl4.Controls.Add(this.labelControl14);
            this.panelControl4.Controls.Add(this.labelControl21);
            this.panelControl4.Controls.Add(this.labelControl38);
            this.panelControl4.Controls.Add(this.labelControl23);
            this.panelControl4.Location = new System.Drawing.Point(5, 98);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(629, 149);
            this.panelControl4.TabIndex = 106;
            // 
            // txtCountry
            // 
            this.txtCountry.Location = new System.Drawing.Point(103, 113);
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtCountry.Properties.Appearance.Options.UseFont = true;
            this.txtCountry.Size = new System.Drawing.Size(195, 26);
            this.txtCountry.TabIndex = 121;
            // 
            // txtNameVisa
            // 
            this.txtNameVisa.Location = new System.Drawing.Point(102, 34);
            this.txtNameVisa.Name = "txtNameVisa";
            this.txtNameVisa.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtNameVisa.Properties.Appearance.Options.UseFont = true;
            this.txtNameVisa.Size = new System.Drawing.Size(195, 26);
            this.txtNameVisa.TabIndex = 120;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl9.Location = new System.Drawing.Point(2, 37);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(97, 19);
            this.labelControl9.TabIndex = 119;
            this.labelControl9.Text = "Tên Chủ Thẻ ";
            // 
            // btnRefeshIdCode
            // 
            this.btnRefeshIdCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnRefeshIdCode.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnRefeshIdCode.Location = new System.Drawing.Point(588, 116);
            this.btnRefeshIdCode.Name = "btnRefeshIdCode";
            this.btnRefeshIdCode.Size = new System.Drawing.Size(35, 26);
            this.btnRefeshIdCode.TabIndex = 118;
            this.btnRefeshIdCode.Click += new System.EventHandler(this.btnRefeshIdCode_Click);
            // 
            // txtCwCvc
            // 
            this.txtCwCvc.Location = new System.Drawing.Point(102, 75);
            this.txtCwCvc.Name = "txtCwCvc";
            this.txtCwCvc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtCwCvc.Properties.Appearance.Options.UseFont = true;
            this.txtCwCvc.Size = new System.Drawing.Size(195, 26);
            this.txtCwCvc.TabIndex = 116;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl12.Location = new System.Drawing.Point(2, 78);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(78, 19);
            this.labelControl12.TabIndex = 115;
            this.labelControl12.Text = "CW, CVC2 ";
            // 
            // txtIdCodeCreate
            // 
            this.txtIdCodeCreate.EditValue = "";
            this.txtIdCodeCreate.Location = new System.Drawing.Point(493, 116);
            this.txtIdCodeCreate.Name = "txtIdCodeCreate";
            this.txtIdCodeCreate.Properties.Appearance.Font = new System.Drawing.Font("Lucida Handwriting", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdCodeCreate.Properties.Appearance.Options.UseFont = true;
            this.txtIdCodeCreate.Properties.ReadOnly = true;
            this.txtIdCodeCreate.Size = new System.Drawing.Size(90, 26);
            this.txtIdCodeCreate.TabIndex = 114;
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl42.Location = new System.Drawing.Point(2, 4);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(133, 19);
            this.labelControl42.TabIndex = 112;
            this.labelControl42.Text = "Thông Tin Thêm";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(397, 75);
            this.txtCity.Name = "txtCity";
            this.txtCity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtCity.Properties.Appearance.Options.UseFont = true;
            this.txtCity.Size = new System.Drawing.Size(226, 26);
            this.txtCity.TabIndex = 111;
            // 
            // txtIdCodeInput
            // 
            this.txtIdCodeInput.Location = new System.Drawing.Point(397, 116);
            this.txtIdCodeInput.Name = "txtIdCodeInput";
            this.txtIdCodeInput.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtIdCodeInput.Properties.Appearance.Options.UseFont = true;
            this.txtIdCodeInput.Size = new System.Drawing.Size(90, 26);
            this.txtIdCodeInput.TabIndex = 110;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(397, 34);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtAddress.Properties.Appearance.Options.UseFont = true;
            this.txtAddress.Size = new System.Drawing.Size(226, 26);
            this.txtAddress.TabIndex = 109;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Location = new System.Drawing.Point(309, 119);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(86, 19);
            this.labelControl14.TabIndex = 108;
            this.labelControl14.Text = "Mã An Toàn";
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Location = new System.Drawing.Point(309, 78);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(57, 19);
            this.labelControl21.TabIndex = 107;
            this.labelControl21.Text = "Tỉnh/TP";
            // 
            // labelControl38
            // 
            this.labelControl38.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl38.Location = new System.Drawing.Point(2, 119);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(70, 19);
            this.labelControl38.TabIndex = 105;
            this.labelControl38.Text = "Quốc Gia ";
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl23.Location = new System.Drawing.Point(309, 37);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(56, 19);
            this.labelControl23.TabIndex = 106;
            this.labelControl23.Text = "Địa Chỉ ";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.labelControl31);
            this.panelControl3.Controls.Add(this.txtYear);
            this.panelControl3.Controls.Add(this.txtMonth);
            this.panelControl3.Controls.Add(this.txtMoneyVisa);
            this.panelControl3.Controls.Add(this.labelControl19);
            this.panelControl3.Controls.Add(this.labelControl1);
            this.panelControl3.Controls.Add(this.txtIdCardVisa);
            this.panelControl3.Controls.Add(this.labelControl34);
            this.panelControl3.Controls.Add(this.labelControl10);
            this.panelControl3.Controls.Add(this.labelControl8);
            this.panelControl3.Location = new System.Drawing.Point(5, 4);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(629, 89);
            this.panelControl3.TabIndex = 105;
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl31.Location = new System.Drawing.Point(485, 28);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(6, 19);
            this.labelControl31.TabIndex = 114;
            this.labelControl31.Text = "/";
            // 
            // txtYear
            // 
            this.txtYear.Location = new System.Drawing.Point(498, 26);
            this.txtYear.Name = "txtYear";
            this.txtYear.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtYear.Properties.Appearance.Options.UseFont = true;
            this.txtYear.Properties.Appearance.Options.UseTextOptions = true;
            this.txtYear.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtYear.Properties.Mask.EditMask = "d";
            this.txtYear.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtYear.Properties.MaxLength = 2;
            this.txtYear.Size = new System.Drawing.Size(45, 26);
            this.txtYear.TabIndex = 113;
            // 
            // txtMonth
            // 
            this.txtMonth.Location = new System.Drawing.Point(434, 27);
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtMonth.Properties.Appearance.Options.UseFont = true;
            this.txtMonth.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMonth.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMonth.Properties.Mask.EditMask = "d";
            this.txtMonth.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtMonth.Properties.MaxLength = 2;
            this.txtMonth.Size = new System.Drawing.Size(45, 26);
            this.txtMonth.TabIndex = 112;
            // 
            // txtMoneyVisa
            // 
            this.txtMoneyVisa.EditValue = "0";
            this.txtMoneyVisa.Location = new System.Drawing.Point(102, 58);
            this.txtMoneyVisa.Name = "txtMoneyVisa";
            this.txtMoneyVisa.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtMoneyVisa.Properties.Appearance.Options.UseFont = true;
            this.txtMoneyVisa.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMoneyVisa.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtMoneyVisa.Properties.Mask.EditMask = "N0";
            this.txtMoneyVisa.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtMoneyVisa.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtMoneyVisa.Size = new System.Drawing.Size(477, 26);
            this.txtMoneyVisa.TabIndex = 111;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl19.Location = new System.Drawing.Point(580, 60);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(43, 19);
            this.labelControl19.TabIndex = 110;
            this.labelControl19.Text = "  VND";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl1.Location = new System.Drawing.Point(11, 59);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(75, 19);
            this.labelControl1.TabIndex = 108;
            this.labelControl1.Text = "Số Tiền(*)";
            // 
            // txtIdCardVisa
            // 
            this.txtIdCardVisa.Location = new System.Drawing.Point(102, 27);
            this.txtIdCardVisa.Name = "txtIdCardVisa";
            this.txtIdCardVisa.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtIdCardVisa.Properties.Appearance.Options.UseFont = true;
            this.txtIdCardVisa.Size = new System.Drawing.Size(195, 26);
            this.txtIdCardVisa.TabIndex = 105;
            // 
            // labelControl34
            // 
            this.labelControl34.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl34.Location = new System.Drawing.Point(11, 4);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(137, 17);
            this.labelControl34.TabIndex = 103;
            this.labelControl34.Text = "Thông Tin Bắt Buộc";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl10.Location = new System.Drawing.Point(309, 31);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(124, 19);
            this.labelControl10.TabIndex = 102;
            this.labelControl10.Text = "Ngày Hết Hạn (*)";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl8.Location = new System.Drawing.Point(10, 31);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(76, 19);
            this.labelControl8.TabIndex = 101;
            this.labelControl8.Text = "Số Thẻ (*)";
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnPaymentVisa);
            this.panelControl1.Controls.Add(this.bntCancelVisa);
            this.panelControl1.Location = new System.Drawing.Point(52, 319);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(626, 44);
            this.panelControl1.TabIndex = 24;
            // 
            // btnPaymentVisa
            // 
            this.btnPaymentVisa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPaymentVisa.Appearance.Font = new System.Drawing.Font("Tahoma", 14F);
            this.btnPaymentVisa.Appearance.Options.UseFont = true;
            this.btnPaymentVisa.Location = new System.Drawing.Point(61, 3);
            this.btnPaymentVisa.Name = "btnPaymentVisa";
            this.btnPaymentVisa.Size = new System.Drawing.Size(156, 36);
            this.btnPaymentVisa.TabIndex = 4;
            this.btnPaymentVisa.Text = "Giao Dịch";
            this.btnPaymentVisa.Click += new System.EventHandler(this.btnPaymentVisa_Click);
            // 
            // bntCancelVisa
            // 
            this.bntCancelVisa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.bntCancelVisa.Appearance.Font = new System.Drawing.Font("Tahoma", 14F);
            this.bntCancelVisa.Appearance.Options.UseFont = true;
            this.bntCancelVisa.Location = new System.Drawing.Point(232, 3);
            this.bntCancelVisa.Name = "bntCancelVisa";
            this.bntCancelVisa.Size = new System.Drawing.Size(156, 36);
            this.bntCancelVisa.TabIndex = 5;
            this.bntCancelVisa.Text = "Hủy Giao Dịch";
            this.bntCancelVisa.Click += new System.EventHandler(this.bntCancelVisa_Click);
            // 
            // pnlHeaderVisa
            // 
            this.pnlHeaderVisa.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlHeaderVisa.Controls.Add(this.labelControl33);
            this.pnlHeaderVisa.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeaderVisa.Location = new System.Drawing.Point(2, 2);
            this.pnlHeaderVisa.Name = "pnlHeaderVisa";
            this.pnlHeaderVisa.Size = new System.Drawing.Size(724, 52);
            this.pnlHeaderVisa.TabIndex = 10;
            // 
            // labelControl33
            // 
            this.labelControl33.Appearance.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Bold);
            this.labelControl33.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl33.Location = new System.Drawing.Point(18, 8);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(539, 27);
            this.labelControl33.TabIndex = 1;
            this.labelControl33.Text = "Thanh Toán Bằng Thẻ Tín Dụng && Ghi Nợ Quốc Tế";
            // 
            // pnlBottomPayment
            // 
            this.pnlBottomPayment.Controls.Add(this.pnlKeyboardAndShowForeignCurrency);
            this.pnlBottomPayment.Controls.Add(this.pnlUpDownForeignCurrency);
            this.pnlBottomPayment.Controls.Add(this.pnlForeignCurrency);
            this.pnlBottomPayment.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottomPayment.Location = new System.Drawing.Point(2, 402);
            this.pnlBottomPayment.Name = "pnlBottomPayment";
            this.pnlBottomPayment.Size = new System.Drawing.Size(728, 298);
            this.pnlBottomPayment.TabIndex = 48;
            // 
            // pnlKeyboardAndShowForeignCurrency
            // 
            this.pnlKeyboardAndShowForeignCurrency.Controls.Add(this.pnlShowForeignCurrency);
            this.pnlKeyboardAndShowForeignCurrency.Controls.Add(this.pnlKeyboardNumber);
            this.pnlKeyboardAndShowForeignCurrency.Controls.Add(this.pnlKeyBoardText);
            this.pnlKeyboardAndShowForeignCurrency.Controls.Add(this.pnlKeyboardMoney);
            this.pnlKeyboardAndShowForeignCurrency.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlKeyboardAndShowForeignCurrency.Location = new System.Drawing.Point(320, 2);
            this.pnlKeyboardAndShowForeignCurrency.Name = "pnlKeyboardAndShowForeignCurrency";
            this.pnlKeyboardAndShowForeignCurrency.Size = new System.Drawing.Size(406, 294);
            this.pnlKeyboardAndShowForeignCurrency.TabIndex = 4;
            // 
            // pnlShowForeignCurrency
            // 
            this.pnlShowForeignCurrency.Controls.Add(this.lbExchangeRate);
            this.pnlShowForeignCurrency.Controls.Add(this.lbKindMoney);
            this.pnlShowForeignCurrency.Controls.Add(this.labelControl32);
            this.pnlShowForeignCurrency.Controls.Add(this.labelControl27);
            this.pnlShowForeignCurrency.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlShowForeignCurrency.Location = new System.Drawing.Point(2, 258);
            this.pnlShowForeignCurrency.Name = "pnlShowForeignCurrency";
            this.pnlShowForeignCurrency.Size = new System.Drawing.Size(402, 34);
            this.pnlShowForeignCurrency.TabIndex = 67;
            // 
            // lbExchangeRate
            // 
            this.lbExchangeRate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lbExchangeRate.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lbExchangeRate.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbExchangeRate.Location = new System.Drawing.Point(241, 8);
            this.lbExchangeRate.Name = "lbExchangeRate";
            this.lbExchangeRate.Size = new System.Drawing.Size(8, 16);
            this.lbExchangeRate.TabIndex = 11;
            this.lbExchangeRate.Text = "1";
            // 
            // lbKindMoney
            // 
            this.lbKindMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lbKindMoney.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbKindMoney.Location = new System.Drawing.Point(95, 8);
            this.lbKindMoney.Name = "lbKindMoney";
            this.lbKindMoney.Size = new System.Drawing.Size(27, 16);
            this.lbKindMoney.TabIndex = 10;
            this.lbKindMoney.Text = "VNĐ";
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl32.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl32.Location = new System.Drawing.Point(23, 8);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(66, 17);
            this.labelControl32.TabIndex = 9;
            this.labelControl32.Text = "Loại Tiền:";
            // 
            // labelControl27
            // 
            this.labelControl27.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl27.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl27.Location = new System.Drawing.Point(186, 8);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(51, 17);
            this.labelControl27.TabIndex = 8;
            this.labelControl27.Text = "Tỷ Giá :";
            // 
            // pnlKeyboardNumber
            // 
            this.pnlKeyboardNumber.Controls.Add(this.btnMoveRightNumber);
            this.pnlKeyboardNumber.Controls.Add(this.btn1);
            this.pnlKeyboardNumber.Controls.Add(this.btnPaymentNumber);
            this.pnlKeyboardNumber.Controls.Add(this.btnTranslateNumber);
            this.pnlKeyboardNumber.Controls.Add(this.btn000);
            this.pnlKeyboardNumber.Controls.Add(this.btn00);
            this.pnlKeyboardNumber.Controls.Add(this.btn0);
            this.pnlKeyboardNumber.Controls.Add(this.btnOkNumber);
            this.pnlKeyboardNumber.Controls.Add(this.btnDeleteNumber);
            this.pnlKeyboardNumber.Controls.Add(this.btn6);
            this.pnlKeyboardNumber.Controls.Add(this.btn9);
            this.pnlKeyboardNumber.Controls.Add(this.btn8);
            this.pnlKeyboardNumber.Controls.Add(this.btn7);
            this.pnlKeyboardNumber.Controls.Add(this.btnMoveLeftNumber);
            this.pnlKeyboardNumber.Controls.Add(this.btn5);
            this.pnlKeyboardNumber.Controls.Add(this.btn4);
            this.pnlKeyboardNumber.Controls.Add(this.btn3);
            this.pnlKeyboardNumber.Controls.Add(this.btn2);
            this.pnlKeyboardNumber.Controls.Add(this.btnEscNumber);
            this.pnlKeyboardNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlKeyboardNumber.Location = new System.Drawing.Point(2, 2);
            this.pnlKeyboardNumber.Name = "pnlKeyboardNumber";
            this.pnlKeyboardNumber.Size = new System.Drawing.Size(402, 290);
            this.pnlKeyboardNumber.TabIndex = 68;
            // 
            // btnMoveRightNumber
            // 
            this.btnMoveRightNumber.AllowFocus = false;
            this.btnMoveRightNumber.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveRightNumber.Appearance.Options.UseFont = true;
            this.btnMoveRightNumber.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveRightNumber.Image")));
            this.btnMoveRightNumber.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveRightNumber.Location = new System.Drawing.Point(272, 5);
            this.btnMoveRightNumber.Name = "btnMoveRightNumber";
            this.btnMoveRightNumber.Size = new System.Drawing.Size(60, 60);
            this.btnMoveRightNumber.TabIndex = 85;
            this.btnMoveRightNumber.Text = "--->";
            this.btnMoveRightNumber.Click += new System.EventHandler(this.btnMoveRightNumber_Click);
            // 
            // btn1
            // 
            this.btn1.AllowFocus = false;
            this.btn1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.Appearance.Options.UseFont = true;
            this.btn1.Location = new System.Drawing.Point(12, 131);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(60, 60);
            this.btn1.TabIndex = 84;
            this.btn1.Text = "1";
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btnPaymentNumber
            // 
            this.btnPaymentNumber.AllowFocus = false;
            this.btnPaymentNumber.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPaymentNumber.Appearance.Options.UseFont = true;
            this.btnPaymentNumber.Location = new System.Drawing.Point(272, 134);
            this.btnPaymentNumber.Name = "btnPaymentNumber";
            this.btnPaymentNumber.Size = new System.Drawing.Size(124, 116);
            this.btnPaymentNumber.TabIndex = 83;
            this.btnPaymentNumber.Text = "F2    \r\nIn Hóa Đơn\r\n";
            this.btnPaymentNumber.Click += new System.EventHandler(this.btnPaymentNumber_Click);
            // 
            // btnTranslateNumber
            // 
            this.btnTranslateNumber.Appearance.BorderColor = System.Drawing.Color.Black;
            this.btnTranslateNumber.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTranslateNumber.Appearance.Options.UseBorderColor = true;
            this.btnTranslateNumber.Appearance.Options.UseFont = true;
            this.btnTranslateNumber.Location = new System.Drawing.Point(12, 193);
            this.btnTranslateNumber.Name = "btnTranslateNumber";
            this.btnTranslateNumber.Size = new System.Drawing.Size(60, 60);
            this.btnTranslateNumber.TabIndex = 82;
            this.btnTranslateNumber.Text = "ABC";
            this.btnTranslateNumber.Click += new System.EventHandler(this.btnTranslateNumber_Click);
            // 
            // btn000
            // 
            this.btn000.AllowFocus = false;
            this.btn000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn000.Appearance.Options.UseFont = true;
            this.btn000.Location = new System.Drawing.Point(207, 193);
            this.btn000.Name = "btn000";
            this.btn000.Size = new System.Drawing.Size(60, 60);
            this.btn000.TabIndex = 81;
            this.btn000.Text = "000";
            this.btn000.Click += new System.EventHandler(this.btn000_Click);
            // 
            // btn00
            // 
            this.btn00.AllowFocus = false;
            this.btn00.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn00.Appearance.Options.UseFont = true;
            this.btn00.Location = new System.Drawing.Point(141, 193);
            this.btn00.Name = "btn00";
            this.btn00.Size = new System.Drawing.Size(60, 60);
            this.btn00.TabIndex = 80;
            this.btn00.Text = "00";
            this.btn00.Click += new System.EventHandler(this.btn00_Click);
            // 
            // btn0
            // 
            this.btn0.AllowFocus = false;
            this.btn0.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn0.Appearance.Options.UseFont = true;
            this.btn0.Location = new System.Drawing.Point(77, 193);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(60, 60);
            this.btn0.TabIndex = 78;
            this.btn0.Text = "0";
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btnOkNumber
            // 
            this.btnOkNumber.AllowFocus = false;
            this.btnOkNumber.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOkNumber.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btnOkNumber.Appearance.Options.UseFont = true;
            this.btnOkNumber.Appearance.Options.UseForeColor = true;
            this.btnOkNumber.Location = new System.Drawing.Point(272, 68);
            this.btnOkNumber.Name = "btnOkNumber";
            this.btnOkNumber.Size = new System.Drawing.Size(124, 60);
            this.btnOkNumber.TabIndex = 66;
            this.btnOkNumber.Text = "OK";
            this.btnOkNumber.Click += new System.EventHandler(this.btnOkNumber_Click);
            // 
            // btnDeleteNumber
            // 
            this.btnDeleteNumber.AllowFocus = false;
            this.btnDeleteNumber.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteNumber.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btnDeleteNumber.Appearance.Options.UseFont = true;
            this.btnDeleteNumber.Appearance.Options.UseForeColor = true;
            this.btnDeleteNumber.Location = new System.Drawing.Point(336, 5);
            this.btnDeleteNumber.Name = "btnDeleteNumber";
            this.btnDeleteNumber.Size = new System.Drawing.Size(60, 60);
            this.btnDeleteNumber.TabIndex = 79;
            this.btnDeleteNumber.Text = "Del";
            this.btnDeleteNumber.Click += new System.EventHandler(this.btnDeleteNumber_Click);
            // 
            // btn6
            // 
            this.btn6.AllowFocus = false;
            this.btn6.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn6.Appearance.Options.UseFont = true;
            this.btn6.Location = new System.Drawing.Point(141, 68);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(60, 60);
            this.btn6.TabIndex = 74;
            this.btn6.Text = "6";
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            // 
            // btn9
            // 
            this.btn9.AllowFocus = false;
            this.btn9.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn9.Appearance.Options.UseFont = true;
            this.btn9.Location = new System.Drawing.Point(141, 5);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(60, 60);
            this.btn9.TabIndex = 77;
            this.btn9.Text = "9";
            this.btn9.Click += new System.EventHandler(this.btn9_Click);
            // 
            // btn8
            // 
            this.btn8.AllowFocus = false;
            this.btn8.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn8.Appearance.Options.UseFont = true;
            this.btn8.Location = new System.Drawing.Point(77, 5);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(60, 60);
            this.btn8.TabIndex = 76;
            this.btn8.Text = "8";
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            // 
            // btn7
            // 
            this.btn7.AllowFocus = false;
            this.btn7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn7.Appearance.Options.UseFont = true;
            this.btn7.Location = new System.Drawing.Point(12, 5);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(60, 60);
            this.btn7.TabIndex = 75;
            this.btn7.Text = "7";
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // btnMoveLeftNumber
            // 
            this.btnMoveLeftNumber.AllowFocus = false;
            this.btnMoveLeftNumber.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveLeftNumber.Appearance.Options.UseFont = true;
            this.btnMoveLeftNumber.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveLeftNumber.Image")));
            this.btnMoveLeftNumber.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveLeftNumber.Location = new System.Drawing.Point(207, 4);
            this.btnMoveLeftNumber.Name = "btnMoveLeftNumber";
            this.btnMoveLeftNumber.Size = new System.Drawing.Size(60, 60);
            this.btnMoveLeftNumber.TabIndex = 73;
            this.btnMoveLeftNumber.Click += new System.EventHandler(this.btnMoveLeftNumber_Click);
            // 
            // btn5
            // 
            this.btn5.AllowFocus = false;
            this.btn5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5.Appearance.Options.UseFont = true;
            this.btn5.Location = new System.Drawing.Point(77, 68);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(60, 60);
            this.btn5.TabIndex = 72;
            this.btn5.Text = "5";
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btn4
            // 
            this.btn4.AllowFocus = false;
            this.btn4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4.Appearance.Options.UseFont = true;
            this.btn4.Location = new System.Drawing.Point(12, 68);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(60, 60);
            this.btn4.TabIndex = 71;
            this.btn4.Text = "4";
            this.btn4.Click += new System.EventHandler(this.btn4_Click);
            // 
            // btn3
            // 
            this.btn3.AllowFocus = false;
            this.btn3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn3.Appearance.Options.UseFont = true;
            this.btn3.Location = new System.Drawing.Point(141, 131);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(60, 60);
            this.btn3.TabIndex = 70;
            this.btn3.Text = "3";
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btn2
            // 
            this.btn2.AllowFocus = false;
            this.btn2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2.Appearance.Options.UseFont = true;
            this.btn2.Location = new System.Drawing.Point(77, 131);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(60, 60);
            this.btn2.TabIndex = 69;
            this.btn2.Text = "2";
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btnEscNumber
            // 
            this.btnEscNumber.AllowFocus = false;
            this.btnEscNumber.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEscNumber.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnEscNumber.Appearance.Options.UseFont = true;
            this.btnEscNumber.Appearance.Options.UseForeColor = true;
            this.btnEscNumber.Location = new System.Drawing.Point(207, 68);
            this.btnEscNumber.Name = "btnEscNumber";
            this.btnEscNumber.Size = new System.Drawing.Size(60, 123);
            this.btnEscNumber.TabIndex = 67;
            this.btnEscNumber.Text = "ESC";
            this.btnEscNumber.Click += new System.EventHandler(this.btnEscNumber_Click);
            // 
            // pnlKeyBoardText
            // 
            this.pnlKeyBoardText.Controls.Add(this.btnP);
            this.pnlKeyBoardText.Controls.Add(this.btnZ);
            this.pnlKeyBoardText.Controls.Add(this.btnY);
            this.pnlKeyBoardText.Controls.Add(this.btnPaymentText);
            this.pnlKeyBoardText.Controls.Add(this.btnShift);
            this.pnlKeyBoardText.Controls.Add(this.btnX);
            this.pnlKeyBoardText.Controls.Add(this.btnW);
            this.pnlKeyBoardText.Controls.Add(this.btnV);
            this.pnlKeyBoardText.Controls.Add(this.btnU);
            this.pnlKeyBoardText.Controls.Add(this.btnT);
            this.pnlKeyBoardText.Controls.Add(this.btnS);
            this.pnlKeyBoardText.Controls.Add(this.btnF);
            this.pnlKeyBoardText.Controls.Add(this.btnE);
            this.pnlKeyBoardText.Controls.Add(this.btnD);
            this.pnlKeyBoardText.Controls.Add(this.btnC);
            this.pnlKeyBoardText.Controls.Add(this.btnB);
            this.pnlKeyBoardText.Controls.Add(this.btnA);
            this.pnlKeyBoardText.Controls.Add(this.btnMoveRigthText);
            this.pnlKeyBoardText.Controls.Add(this.btnM);
            this.pnlKeyBoardText.Controls.Add(this.btnTranslateText);
            this.pnlKeyBoardText.Controls.Add(this.btnR);
            this.pnlKeyBoardText.Controls.Add(this.btnQ);
            this.pnlKeyBoardText.Controls.Add(this.btnOkText);
            this.pnlKeyBoardText.Controls.Add(this.btnDeleteText);
            this.pnlKeyBoardText.Controls.Add(this.btnL);
            this.pnlKeyBoardText.Controls.Add(this.btnI);
            this.pnlKeyBoardText.Controls.Add(this.btnH);
            this.pnlKeyBoardText.Controls.Add(this.btnG);
            this.pnlKeyBoardText.Controls.Add(this.btnMoveLeftText);
            this.pnlKeyBoardText.Controls.Add(this.btnK);
            this.pnlKeyBoardText.Controls.Add(this.btnJ);
            this.pnlKeyBoardText.Controls.Add(this.btnO);
            this.pnlKeyBoardText.Controls.Add(this.btnN);
            this.pnlKeyBoardText.Controls.Add(this.btnEscText);
            this.pnlKeyBoardText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlKeyBoardText.Location = new System.Drawing.Point(2, 2);
            this.pnlKeyBoardText.Name = "pnlKeyBoardText";
            this.pnlKeyBoardText.Size = new System.Drawing.Size(402, 290);
            this.pnlKeyBoardText.TabIndex = 92;
            // 
            // btnP
            // 
            this.btnP.AllowFocus = false;
            this.btnP.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnP.Appearance.Options.UseFont = true;
            this.btnP.Location = new System.Drawing.Point(365, 5);
            this.btnP.Name = "btnP";
            this.btnP.Size = new System.Drawing.Size(38, 45);
            this.btnP.TabIndex = 78;
            this.btnP.Text = "P";
            this.btnP.Click += new System.EventHandler(this.btnP_Click);
            // 
            // btnZ
            // 
            this.btnZ.AllowFocus = false;
            this.btnZ.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnZ.Appearance.Options.UseFont = true;
            this.btnZ.Location = new System.Drawing.Point(5, 105);
            this.btnZ.Name = "btnZ";
            this.btnZ.Size = new System.Drawing.Size(38, 45);
            this.btnZ.TabIndex = 103;
            this.btnZ.Text = "Z";
            this.btnZ.Click += new System.EventHandler(this.btnZ_Click);
            // 
            // btnY
            // 
            this.btnY.AllowFocus = false;
            this.btnY.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnY.Appearance.Options.UseFont = true;
            this.btnY.Location = new System.Drawing.Point(205, 5);
            this.btnY.Name = "btnY";
            this.btnY.Size = new System.Drawing.Size(38, 45);
            this.btnY.TabIndex = 102;
            this.btnY.Text = "Y";
            this.btnY.Click += new System.EventHandler(this.btnY_Click);
            // 
            // btnPaymentText
            // 
            this.btnPaymentText.AllowFocus = false;
            this.btnPaymentText.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPaymentText.Appearance.Options.UseFont = true;
            this.btnPaymentText.Location = new System.Drawing.Point(285, 156);
            this.btnPaymentText.Name = "btnPaymentText";
            this.btnPaymentText.Size = new System.Drawing.Size(118, 97);
            this.btnPaymentText.TabIndex = 83;
            this.btnPaymentText.Text = "F2    \r\nIn Hóa Đơn";
            this.btnPaymentText.Click += new System.EventHandler(this.btnPaymentText_Click);
            // 
            // btnShift
            // 
            this.btnShift.AllowFocus = false;
            this.btnShift.Appearance.BackColor = System.Drawing.Color.GreenYellow;
            this.btnShift.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShift.Appearance.Options.UseBackColor = true;
            this.btnShift.Appearance.Options.UseFont = true;
            this.btnShift.Location = new System.Drawing.Point(285, 105);
            this.btnShift.Name = "btnShift";
            this.btnShift.Size = new System.Drawing.Size(118, 45);
            this.btnShift.TabIndex = 101;
            this.btnShift.Text = "Shift";
            this.btnShift.Click += new System.EventHandler(this.btnShift_Click);
            // 
            // btnX
            // 
            this.btnX.AllowFocus = false;
            this.btnX.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnX.Appearance.Options.UseFont = true;
            this.btnX.Location = new System.Drawing.Point(45, 105);
            this.btnX.Name = "btnX";
            this.btnX.Size = new System.Drawing.Size(38, 45);
            this.btnX.TabIndex = 100;
            this.btnX.Text = "X";
            this.btnX.Click += new System.EventHandler(this.btnX_Click);
            // 
            // btnW
            // 
            this.btnW.AllowFocus = false;
            this.btnW.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnW.Appearance.Options.UseFont = true;
            this.btnW.Location = new System.Drawing.Point(45, 5);
            this.btnW.Name = "btnW";
            this.btnW.Size = new System.Drawing.Size(38, 45);
            this.btnW.TabIndex = 96;
            this.btnW.Text = "W";
            this.btnW.Click += new System.EventHandler(this.btnW_Click);
            // 
            // btnV
            // 
            this.btnV.AllowFocus = false;
            this.btnV.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnV.Appearance.Options.UseFont = true;
            this.btnV.Location = new System.Drawing.Point(125, 105);
            this.btnV.Name = "btnV";
            this.btnV.Size = new System.Drawing.Size(38, 45);
            this.btnV.TabIndex = 95;
            this.btnV.Text = "V";
            this.btnV.Click += new System.EventHandler(this.btnV_Click);
            // 
            // btnU
            // 
            this.btnU.AllowFocus = false;
            this.btnU.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnU.Appearance.Options.UseFont = true;
            this.btnU.Location = new System.Drawing.Point(245, 5);
            this.btnU.Name = "btnU";
            this.btnU.Size = new System.Drawing.Size(38, 45);
            this.btnU.TabIndex = 94;
            this.btnU.Text = "U";
            this.btnU.Click += new System.EventHandler(this.btnU_Click);
            // 
            // btnT
            // 
            this.btnT.AllowFocus = false;
            this.btnT.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnT.Appearance.Options.UseFont = true;
            this.btnT.Location = new System.Drawing.Point(165, 5);
            this.btnT.Name = "btnT";
            this.btnT.Size = new System.Drawing.Size(38, 45);
            this.btnT.TabIndex = 93;
            this.btnT.Text = "T";
            this.btnT.Click += new System.EventHandler(this.btnT_Click);
            // 
            // btnS
            // 
            this.btnS.AllowFocus = false;
            this.btnS.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnS.Appearance.Options.UseFont = true;
            this.btnS.Location = new System.Drawing.Point(65, 55);
            this.btnS.Name = "btnS";
            this.btnS.Size = new System.Drawing.Size(38, 45);
            this.btnS.TabIndex = 92;
            this.btnS.Text = "S";
            this.btnS.Click += new System.EventHandler(this.btnS_Click);
            // 
            // btnF
            // 
            this.btnF.AllowFocus = false;
            this.btnF.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnF.Appearance.Options.UseFont = true;
            this.btnF.Location = new System.Drawing.Point(145, 55);
            this.btnF.Name = "btnF";
            this.btnF.Size = new System.Drawing.Size(38, 45);
            this.btnF.TabIndex = 91;
            this.btnF.Text = "F";
            this.btnF.Click += new System.EventHandler(this.btnF_Click);
            // 
            // btnE
            // 
            this.btnE.AllowFocus = false;
            this.btnE.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnE.Appearance.Options.UseFont = true;
            this.btnE.Location = new System.Drawing.Point(85, 5);
            this.btnE.Name = "btnE";
            this.btnE.Size = new System.Drawing.Size(38, 45);
            this.btnE.TabIndex = 90;
            this.btnE.Text = "E";
            this.btnE.Click += new System.EventHandler(this.btnE_Click);
            // 
            // btnD
            // 
            this.btnD.AllowFocus = false;
            this.btnD.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnD.Appearance.Options.UseFont = true;
            this.btnD.Location = new System.Drawing.Point(105, 55);
            this.btnD.Name = "btnD";
            this.btnD.Size = new System.Drawing.Size(38, 45);
            this.btnD.TabIndex = 89;
            this.btnD.Text = "D";
            this.btnD.Click += new System.EventHandler(this.btnD_Click);
            // 
            // btnC
            // 
            this.btnC.AllowFocus = false;
            this.btnC.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnC.Appearance.Options.UseFont = true;
            this.btnC.Location = new System.Drawing.Point(85, 105);
            this.btnC.Name = "btnC";
            this.btnC.Size = new System.Drawing.Size(38, 45);
            this.btnC.TabIndex = 88;
            this.btnC.Text = "C";
            this.btnC.Click += new System.EventHandler(this.btnC_Click);
            // 
            // btnB
            // 
            this.btnB.AllowFocus = false;
            this.btnB.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnB.Appearance.Options.UseFont = true;
            this.btnB.Location = new System.Drawing.Point(165, 105);
            this.btnB.Name = "btnB";
            this.btnB.Size = new System.Drawing.Size(38, 45);
            this.btnB.TabIndex = 87;
            this.btnB.Text = "B";
            this.btnB.Click += new System.EventHandler(this.btnB_Click);
            // 
            // btnA
            // 
            this.btnA.AllowFocus = false;
            this.btnA.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA.Appearance.Options.UseFont = true;
            this.btnA.Location = new System.Drawing.Point(25, 55);
            this.btnA.Name = "btnA";
            this.btnA.Size = new System.Drawing.Size(38, 45);
            this.btnA.TabIndex = 86;
            this.btnA.Text = "A";
            this.btnA.Click += new System.EventHandler(this.btnA_Click);
            // 
            // btnMoveRigthText
            // 
            this.btnMoveRigthText.AllowFocus = false;
            this.btnMoveRigthText.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveRigthText.Appearance.Options.UseFont = true;
            this.btnMoveRigthText.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveRigthText.Image")));
            this.btnMoveRigthText.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveRigthText.Location = new System.Drawing.Point(45, 156);
            this.btnMoveRigthText.Name = "btnMoveRigthText";
            this.btnMoveRigthText.Size = new System.Drawing.Size(38, 45);
            this.btnMoveRigthText.TabIndex = 85;
            this.btnMoveRigthText.Click += new System.EventHandler(this.btnMoveRigthText_Click);
            // 
            // btnM
            // 
            this.btnM.AllowFocus = false;
            this.btnM.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnM.Appearance.Options.UseFont = true;
            this.btnM.Location = new System.Drawing.Point(245, 105);
            this.btnM.Name = "btnM";
            this.btnM.Size = new System.Drawing.Size(38, 45);
            this.btnM.TabIndex = 84;
            this.btnM.Text = "M";
            this.btnM.Click += new System.EventHandler(this.btnM_Click);
            // 
            // btnTranslateText
            // 
            this.btnTranslateText.Appearance.BorderColor = System.Drawing.Color.Black;
            this.btnTranslateText.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTranslateText.Appearance.Options.UseBorderColor = true;
            this.btnTranslateText.Appearance.Options.UseFont = true;
            this.btnTranslateText.Location = new System.Drawing.Point(5, 207);
            this.btnTranslateText.Name = "btnTranslateText";
            this.btnTranslateText.Size = new System.Drawing.Size(78, 45);
            this.btnTranslateText.TabIndex = 82;
            this.btnTranslateText.Text = "$$$";
            this.btnTranslateText.Click += new System.EventHandler(this.btnTranslateText_Click);
            // 
            // btnR
            // 
            this.btnR.AllowFocus = false;
            this.btnR.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnR.Appearance.Options.UseFont = true;
            this.btnR.Location = new System.Drawing.Point(125, 5);
            this.btnR.Name = "btnR";
            this.btnR.Size = new System.Drawing.Size(38, 45);
            this.btnR.TabIndex = 81;
            this.btnR.Text = "R";
            this.btnR.Click += new System.EventHandler(this.btnR_Click);
            // 
            // btnQ
            // 
            this.btnQ.AllowFocus = false;
            this.btnQ.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQ.Appearance.Options.UseFont = true;
            this.btnQ.Location = new System.Drawing.Point(5, 5);
            this.btnQ.Name = "btnQ";
            this.btnQ.Size = new System.Drawing.Size(38, 45);
            this.btnQ.TabIndex = 80;
            this.btnQ.Text = "Q";
            this.btnQ.Click += new System.EventHandler(this.btnQ_Click);
            // 
            // btnOkText
            // 
            this.btnOkText.AllowFocus = false;
            this.btnOkText.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOkText.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btnOkText.Appearance.Options.UseFont = true;
            this.btnOkText.Appearance.Options.UseForeColor = true;
            this.btnOkText.Location = new System.Drawing.Point(205, 156);
            this.btnOkText.Name = "btnOkText";
            this.btnOkText.Size = new System.Drawing.Size(78, 96);
            this.btnOkText.TabIndex = 66;
            this.btnOkText.Text = "OK";
            this.btnOkText.Click += new System.EventHandler(this.btnOkText_Click);
            // 
            // btnDeleteText
            // 
            this.btnDeleteText.AllowFocus = false;
            this.btnDeleteText.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteText.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btnDeleteText.Appearance.Options.UseFont = true;
            this.btnDeleteText.Appearance.Options.UseForeColor = true;
            this.btnDeleteText.Location = new System.Drawing.Point(87, 156);
            this.btnDeleteText.Name = "btnDeleteText";
            this.btnDeleteText.Size = new System.Drawing.Size(116, 45);
            this.btnDeleteText.TabIndex = 79;
            this.btnDeleteText.Text = "Del";
            this.btnDeleteText.Click += new System.EventHandler(this.btnDeleteText_Click);
            // 
            // btnL
            // 
            this.btnL.AllowFocus = false;
            this.btnL.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnL.Appearance.Options.UseFont = true;
            this.btnL.Location = new System.Drawing.Point(345, 55);
            this.btnL.Name = "btnL";
            this.btnL.Size = new System.Drawing.Size(38, 45);
            this.btnL.TabIndex = 74;
            this.btnL.Text = "L";
            this.btnL.Click += new System.EventHandler(this.btnL_Click);
            // 
            // btnI
            // 
            this.btnI.AllowFocus = false;
            this.btnI.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnI.Appearance.Options.UseFont = true;
            this.btnI.Location = new System.Drawing.Point(285, 5);
            this.btnI.Name = "btnI";
            this.btnI.Size = new System.Drawing.Size(38, 45);
            this.btnI.TabIndex = 77;
            this.btnI.Text = "I";
            this.btnI.Click += new System.EventHandler(this.btnI_Click);
            // 
            // btnH
            // 
            this.btnH.AllowFocus = false;
            this.btnH.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnH.Appearance.Options.UseFont = true;
            this.btnH.Location = new System.Drawing.Point(225, 55);
            this.btnH.Name = "btnH";
            this.btnH.Size = new System.Drawing.Size(38, 45);
            this.btnH.TabIndex = 76;
            this.btnH.Text = "H";
            this.btnH.Click += new System.EventHandler(this.btnH_Click);
            // 
            // btnG
            // 
            this.btnG.AllowFocus = false;
            this.btnG.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnG.Appearance.Options.UseFont = true;
            this.btnG.Location = new System.Drawing.Point(185, 55);
            this.btnG.Name = "btnG";
            this.btnG.Size = new System.Drawing.Size(38, 45);
            this.btnG.TabIndex = 75;
            this.btnG.Text = "G";
            this.btnG.Click += new System.EventHandler(this.btnG_Click);
            // 
            // btnMoveLeftText
            // 
            this.btnMoveLeftText.AllowFocus = false;
            this.btnMoveLeftText.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveLeftText.Appearance.Options.UseFont = true;
            this.btnMoveLeftText.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveLeftText.Image")));
            this.btnMoveLeftText.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveLeftText.Location = new System.Drawing.Point(5, 156);
            this.btnMoveLeftText.Name = "btnMoveLeftText";
            this.btnMoveLeftText.Size = new System.Drawing.Size(38, 45);
            this.btnMoveLeftText.TabIndex = 73;
            this.btnMoveLeftText.Click += new System.EventHandler(this.btnMoveLeftText_Click);
            // 
            // btnK
            // 
            this.btnK.AllowFocus = false;
            this.btnK.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnK.Appearance.Options.UseFont = true;
            this.btnK.Location = new System.Drawing.Point(305, 55);
            this.btnK.Name = "btnK";
            this.btnK.Size = new System.Drawing.Size(38, 45);
            this.btnK.TabIndex = 72;
            this.btnK.Text = "K";
            this.btnK.Click += new System.EventHandler(this.btnK_Click);
            // 
            // btnJ
            // 
            this.btnJ.AllowFocus = false;
            this.btnJ.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnJ.Appearance.Options.UseFont = true;
            this.btnJ.Location = new System.Drawing.Point(265, 55);
            this.btnJ.Name = "btnJ";
            this.btnJ.Size = new System.Drawing.Size(38, 45);
            this.btnJ.TabIndex = 71;
            this.btnJ.Text = "J";
            this.btnJ.Click += new System.EventHandler(this.btnJ_Click);
            // 
            // btnO
            // 
            this.btnO.AllowFocus = false;
            this.btnO.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnO.Appearance.Options.UseFont = true;
            this.btnO.Location = new System.Drawing.Point(325, 5);
            this.btnO.Name = "btnO";
            this.btnO.Size = new System.Drawing.Size(38, 45);
            this.btnO.TabIndex = 70;
            this.btnO.Text = "O";
            this.btnO.Click += new System.EventHandler(this.btnO_Click);
            // 
            // btnN
            // 
            this.btnN.AllowFocus = false;
            this.btnN.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnN.Appearance.Options.UseFont = true;
            this.btnN.Location = new System.Drawing.Point(205, 105);
            this.btnN.Name = "btnN";
            this.btnN.Size = new System.Drawing.Size(38, 45);
            this.btnN.TabIndex = 69;
            this.btnN.Text = "N";
            this.btnN.Click += new System.EventHandler(this.btnN_Click);
            // 
            // btnEscText
            // 
            this.btnEscText.AllowFocus = false;
            this.btnEscText.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEscText.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnEscText.Appearance.Options.UseFont = true;
            this.btnEscText.Appearance.Options.UseForeColor = true;
            this.btnEscText.Location = new System.Drawing.Point(85, 207);
            this.btnEscText.Name = "btnEscText";
            this.btnEscText.Size = new System.Drawing.Size(116, 45);
            this.btnEscText.TabIndex = 67;
            this.btnEscText.Text = "ESC";
            this.btnEscText.Click += new System.EventHandler(this.btnEscText_Click);
            // 
            // pnlKeyboardMoney
            // 
            this.pnlKeyboardMoney.Controls.Add(this.btn500000);
            this.pnlKeyboardMoney.Controls.Add(this.btn200000);
            this.pnlKeyboardMoney.Controls.Add(this.btn100000);
            this.pnlKeyboardMoney.Controls.Add(this.btn50000);
            this.pnlKeyboardMoney.Controls.Add(this.btn20000);
            this.pnlKeyboardMoney.Controls.Add(this.btn10000);
            this.pnlKeyboardMoney.Controls.Add(this.btnMoveRightMoney);
            this.pnlKeyboardMoney.Controls.Add(this.btnPaymentMoney);
            this.pnlKeyboardMoney.Controls.Add(this.btnTranslateMoney);
            this.pnlKeyboardMoney.Controls.Add(this.btnOkMoney);
            this.pnlKeyboardMoney.Controls.Add(this.btnDeleteMoney);
            this.pnlKeyboardMoney.Controls.Add(this.btn2000);
            this.pnlKeyboardMoney.Controls.Add(this.btn1000);
            this.pnlKeyboardMoney.Controls.Add(this.btn500);
            this.pnlKeyboardMoney.Controls.Add(this.btnMoveLeftMoney);
            this.pnlKeyboardMoney.Controls.Add(this.btn5000);
            this.pnlKeyboardMoney.Controls.Add(this.btnEcsMoney);
            this.pnlKeyboardMoney.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlKeyboardMoney.Location = new System.Drawing.Point(2, 2);
            this.pnlKeyboardMoney.Name = "pnlKeyboardMoney";
            this.pnlKeyboardMoney.Size = new System.Drawing.Size(402, 290);
            this.pnlKeyboardMoney.TabIndex = 92;
            // 
            // btn500000
            // 
            this.btn500000.AllowFocus = false;
            this.btn500000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn500000.Appearance.Options.UseFont = true;
            this.btn500000.Location = new System.Drawing.Point(333, 67);
            this.btn500000.Name = "btn500000";
            this.btn500000.Size = new System.Drawing.Size(70, 60);
            this.btn500000.TabIndex = 91;
            this.btn500000.Text = "500,000";
            this.btn500000.Click += new System.EventHandler(this.btn500000_Click);
            // 
            // btn200000
            // 
            this.btn200000.AllowFocus = false;
            this.btn200000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn200000.Appearance.Options.UseFont = true;
            this.btn200000.Location = new System.Drawing.Point(251, 67);
            this.btn200000.Name = "btn200000";
            this.btn200000.Size = new System.Drawing.Size(70, 60);
            this.btn200000.TabIndex = 90;
            this.btn200000.Text = "200,000";
            this.btn200000.Click += new System.EventHandler(this.btn200000_Click);
            // 
            // btn100000
            // 
            this.btn100000.AllowFocus = false;
            this.btn100000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn100000.Appearance.Options.UseFont = true;
            this.btn100000.Location = new System.Drawing.Point(169, 67);
            this.btn100000.Name = "btn100000";
            this.btn100000.Size = new System.Drawing.Size(70, 60);
            this.btn100000.TabIndex = 89;
            this.btn100000.Text = "100,000";
            this.btn100000.Click += new System.EventHandler(this.btn100000_Click);
            // 
            // btn50000
            // 
            this.btn50000.AllowFocus = false;
            this.btn50000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn50000.Appearance.Options.UseFont = true;
            this.btn50000.Location = new System.Drawing.Point(87, 67);
            this.btn50000.Name = "btn50000";
            this.btn50000.Size = new System.Drawing.Size(70, 60);
            this.btn50000.TabIndex = 88;
            this.btn50000.Text = "50,000";
            this.btn50000.Click += new System.EventHandler(this.btn50000_Click);
            // 
            // btn20000
            // 
            this.btn20000.AllowFocus = false;
            this.btn20000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn20000.Appearance.Options.UseFont = true;
            this.btn20000.Location = new System.Drawing.Point(5, 67);
            this.btn20000.Name = "btn20000";
            this.btn20000.Size = new System.Drawing.Size(70, 60);
            this.btn20000.TabIndex = 87;
            this.btn20000.Text = "20,000";
            this.btn20000.Click += new System.EventHandler(this.btn20000_Click);
            // 
            // btn10000
            // 
            this.btn10000.AllowFocus = false;
            this.btn10000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn10000.Appearance.Options.UseFont = true;
            this.btn10000.Location = new System.Drawing.Point(333, 5);
            this.btn10000.Name = "btn10000";
            this.btn10000.Size = new System.Drawing.Size(70, 60);
            this.btn10000.TabIndex = 86;
            this.btn10000.Text = "10,000";
            this.btn10000.Click += new System.EventHandler(this.btn10000_Click);
            // 
            // btnMoveRightMoney
            // 
            this.btnMoveRightMoney.AllowFocus = false;
            this.btnMoveRightMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveRightMoney.Appearance.Options.UseFont = true;
            this.btnMoveRightMoney.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveRightMoney.Image")));
            this.btnMoveRightMoney.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveRightMoney.Location = new System.Drawing.Point(87, 130);
            this.btnMoveRightMoney.Name = "btnMoveRightMoney";
            this.btnMoveRightMoney.Size = new System.Drawing.Size(70, 60);
            this.btnMoveRightMoney.TabIndex = 85;
            this.btnMoveRightMoney.Text = "--->";
            this.btnMoveRightMoney.Click += new System.EventHandler(this.btnMoveRightMoney_Click);
            // 
            // btnPaymentMoney
            // 
            this.btnPaymentMoney.AllowFocus = false;
            this.btnPaymentMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPaymentMoney.Appearance.Options.UseFont = true;
            this.btnPaymentMoney.Location = new System.Drawing.Point(251, 130);
            this.btnPaymentMoney.Name = "btnPaymentMoney";
            this.btnPaymentMoney.Size = new System.Drawing.Size(152, 122);
            this.btnPaymentMoney.TabIndex = 83;
            this.btnPaymentMoney.Text = "F2    \r\n In Hóa Đơn";
            this.btnPaymentMoney.Click += new System.EventHandler(this.btnPaymentMoney_Click);
            // 
            // btnTranslateMoney
            // 
            this.btnTranslateMoney.Appearance.BorderColor = System.Drawing.Color.Black;
            this.btnTranslateMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTranslateMoney.Appearance.Options.UseBorderColor = true;
            this.btnTranslateMoney.Appearance.Options.UseFont = true;
            this.btnTranslateMoney.Location = new System.Drawing.Point(4, 193);
            this.btnTranslateMoney.Name = "btnTranslateMoney";
            this.btnTranslateMoney.Size = new System.Drawing.Size(70, 60);
            this.btnTranslateMoney.TabIndex = 82;
            this.btnTranslateMoney.Text = "123";
            this.btnTranslateMoney.Click += new System.EventHandler(this.btnTranslateMoney_Click);
            // 
            // btnOkMoney
            // 
            this.btnOkMoney.AllowFocus = false;
            this.btnOkMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOkMoney.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btnOkMoney.Appearance.Options.UseFont = true;
            this.btnOkMoney.Appearance.Options.UseForeColor = true;
            this.btnOkMoney.Location = new System.Drawing.Point(169, 193);
            this.btnOkMoney.Name = "btnOkMoney";
            this.btnOkMoney.Size = new System.Drawing.Size(70, 60);
            this.btnOkMoney.TabIndex = 66;
            this.btnOkMoney.Text = "OK";
            this.btnOkMoney.Click += new System.EventHandler(this.btnOkMoney_Click);
            // 
            // btnDeleteMoney
            // 
            this.btnDeleteMoney.AllowFocus = false;
            this.btnDeleteMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteMoney.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btnDeleteMoney.Appearance.Options.UseFont = true;
            this.btnDeleteMoney.Appearance.Options.UseForeColor = true;
            this.btnDeleteMoney.Location = new System.Drawing.Point(169, 130);
            this.btnDeleteMoney.Name = "btnDeleteMoney";
            this.btnDeleteMoney.Size = new System.Drawing.Size(70, 60);
            this.btnDeleteMoney.TabIndex = 79;
            this.btnDeleteMoney.Text = "Del";
            this.btnDeleteMoney.Click += new System.EventHandler(this.btnDeleteMoney_Click);
            // 
            // btn2000
            // 
            this.btn2000.AllowFocus = false;
            this.btn2000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2000.Appearance.Options.UseFont = true;
            this.btn2000.Location = new System.Drawing.Point(169, 5);
            this.btn2000.Name = "btn2000";
            this.btn2000.Size = new System.Drawing.Size(70, 60);
            this.btn2000.TabIndex = 77;
            this.btn2000.Text = "2,000";
            this.btn2000.Click += new System.EventHandler(this.btn2000_Click);
            // 
            // btn1000
            // 
            this.btn1000.AllowFocus = false;
            this.btn1000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1000.Appearance.Options.UseFont = true;
            this.btn1000.Location = new System.Drawing.Point(87, 5);
            this.btn1000.Name = "btn1000";
            this.btn1000.Size = new System.Drawing.Size(70, 60);
            this.btn1000.TabIndex = 76;
            this.btn1000.Text = "1,000";
            this.btn1000.Click += new System.EventHandler(this.btn1000_Click);
            // 
            // btn500
            // 
            this.btn500.AllowFocus = false;
            this.btn500.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn500.Appearance.Options.UseFont = true;
            this.btn500.Location = new System.Drawing.Point(5, 4);
            this.btn500.Name = "btn500";
            this.btn500.Size = new System.Drawing.Size(70, 60);
            this.btn500.TabIndex = 75;
            this.btn500.Text = "500";
            this.btn500.Click += new System.EventHandler(this.btn500_Click);
            // 
            // btnMoveLeftMoney
            // 
            this.btnMoveLeftMoney.AllowFocus = false;
            this.btnMoveLeftMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveLeftMoney.Appearance.Options.UseFont = true;
            this.btnMoveLeftMoney.BackgroundImage = global::KP_RES.Properties.Resources.advance_back_32;
            this.btnMoveLeftMoney.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveLeftMoney.Image")));
            this.btnMoveLeftMoney.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveLeftMoney.Location = new System.Drawing.Point(5, 130);
            this.btnMoveLeftMoney.Name = "btnMoveLeftMoney";
            this.btnMoveLeftMoney.Size = new System.Drawing.Size(70, 60);
            this.btnMoveLeftMoney.TabIndex = 73;
            this.btnMoveLeftMoney.Text = "<---";
            this.btnMoveLeftMoney.Click += new System.EventHandler(this.btnMoveLeftMoney_Click);
            // 
            // btn5000
            // 
            this.btn5000.AllowFocus = false;
            this.btn5000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5000.Appearance.Options.UseFont = true;
            this.btn5000.Location = new System.Drawing.Point(251, 5);
            this.btn5000.Name = "btn5000";
            this.btn5000.Size = new System.Drawing.Size(70, 60);
            this.btn5000.TabIndex = 71;
            this.btn5000.Text = "5,000";
            this.btn5000.Click += new System.EventHandler(this.btn5000_Click);
            // 
            // btnEcsMoney
            // 
            this.btnEcsMoney.AllowFocus = false;
            this.btnEcsMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEcsMoney.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnEcsMoney.Appearance.Options.UseFont = true;
            this.btnEcsMoney.Appearance.Options.UseForeColor = true;
            this.btnEcsMoney.Location = new System.Drawing.Point(87, 193);
            this.btnEcsMoney.Name = "btnEcsMoney";
            this.btnEcsMoney.Size = new System.Drawing.Size(70, 60);
            this.btnEcsMoney.TabIndex = 67;
            this.btnEcsMoney.Text = "ESC";
            this.btnEcsMoney.Click += new System.EventHandler(this.btnEcsMoney_Click);
            // 
            // pnlUpDownForeignCurrency
            // 
            this.pnlUpDownForeignCurrency.Controls.Add(this.btnDownForeignCurrency);
            this.pnlUpDownForeignCurrency.Controls.Add(this.btnUpForeignCurrency);
            this.pnlUpDownForeignCurrency.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlUpDownForeignCurrency.Location = new System.Drawing.Point(282, 2);
            this.pnlUpDownForeignCurrency.Name = "pnlUpDownForeignCurrency";
            this.pnlUpDownForeignCurrency.Size = new System.Drawing.Size(38, 294);
            this.pnlUpDownForeignCurrency.TabIndex = 3;
            // 
            // btnDownForeignCurrency
            // 
            this.btnDownForeignCurrency.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDownForeignCurrency.Image = ((System.Drawing.Image)(resources.GetObject("btnDownForeignCurrency.Image")));
            this.btnDownForeignCurrency.Location = new System.Drawing.Point(3, 230);
            this.btnDownForeignCurrency.Name = "btnDownForeignCurrency";
            this.btnDownForeignCurrency.Size = new System.Drawing.Size(31, 53);
            this.btnDownForeignCurrency.TabIndex = 8;
            this.btnDownForeignCurrency.Text = "simpleButton4";
            this.btnDownForeignCurrency.Click += new System.EventHandler(this.btnDownForeignCurrency_Click);
            // 
            // btnUpForeignCurrency
            // 
            this.btnUpForeignCurrency.Image = ((System.Drawing.Image)(resources.GetObject("btnUpForeignCurrency.Image")));
            this.btnUpForeignCurrency.Location = new System.Drawing.Point(3, 6);
            this.btnUpForeignCurrency.Name = "btnUpForeignCurrency";
            this.btnUpForeignCurrency.Size = new System.Drawing.Size(31, 53);
            this.btnUpForeignCurrency.TabIndex = 7;
            this.btnUpForeignCurrency.Text = "simpleButton1";
            this.btnUpForeignCurrency.Click += new System.EventHandler(this.btnUpForeignCurrency_Click);
            // 
            // pnlForeignCurrency
            // 
            this.pnlForeignCurrency.Controls.Add(this.gcForeignCurrency);
            this.pnlForeignCurrency.Controls.Add(this.pnlHeaderCurrentcy);
            this.pnlForeignCurrency.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlForeignCurrency.Location = new System.Drawing.Point(2, 2);
            this.pnlForeignCurrency.Name = "pnlForeignCurrency";
            this.pnlForeignCurrency.Size = new System.Drawing.Size(280, 294);
            this.pnlForeignCurrency.TabIndex = 1;
            // 
            // gcForeignCurrency
            // 
            this.gcForeignCurrency.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode3.RelationName = "Level1";
            this.gcForeignCurrency.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode3});
            this.gcForeignCurrency.Location = new System.Drawing.Point(2, 40);
            this.gcForeignCurrency.MainView = this.gvListForiegnCurrency;
            this.gcForeignCurrency.Name = "gcForeignCurrency";
            this.gcForeignCurrency.Size = new System.Drawing.Size(276, 252);
            this.gcForeignCurrency.TabIndex = 6;
            this.gcForeignCurrency.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvListForiegnCurrency});
            // 
            // gvListForiegnCurrency
            // 
            this.gvListForiegnCurrency.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT1,
            this.MA,
            this.TEN,
            this.TYGIA,
            this.PHUTHU});
            this.gvListForiegnCurrency.GridControl = this.gcForeignCurrency;
            this.gvListForiegnCurrency.Name = "gvListForiegnCurrency";
            this.gvListForiegnCurrency.OptionsView.ShowGroupPanel = false;
            this.gvListForiegnCurrency.RowHeight = 43;
            this.gvListForiegnCurrency.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gvListForiegnCurrency.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gvListForiegnCurrency_CustomDrawCell);
            this.gvListForiegnCurrency.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvListForiegnCurrency_FocusedRowChanged);
            // 
            // STT1
            // 
            this.STT1.AppearanceCell.Options.UseTextOptions = true;
            this.STT1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.STT1.AppearanceHeader.Options.UseFont = true;
            this.STT1.AppearanceHeader.Options.UseTextOptions = true;
            this.STT1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT1.Caption = "STT";
            this.STT1.Name = "STT1";
            this.STT1.OptionsColumn.AllowEdit = false;
            this.STT1.OptionsColumn.AllowFocus = false;
            this.STT1.Visible = true;
            this.STT1.VisibleIndex = 0;
            this.STT1.Width = 26;
            // 
            // MA
            // 
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.Caption = "Mã Nguyên Tệ";
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            // 
            // TEN
            // 
            this.TEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TEN.AppearanceCell.Options.UseFont = true;
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.Caption = "Nguyên Tệ";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowEdit = false;
            this.TEN.OptionsColumn.AllowFocus = false;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 1;
            this.TEN.Width = 55;
            // 
            // TYGIA
            // 
            this.TYGIA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TYGIA.AppearanceCell.Options.UseFont = true;
            this.TYGIA.AppearanceCell.Options.UseTextOptions = true;
            this.TYGIA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TYGIA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TYGIA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TYGIA.AppearanceHeader.Options.UseFont = true;
            this.TYGIA.AppearanceHeader.Options.UseTextOptions = true;
            this.TYGIA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TYGIA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TYGIA.Caption = "Tỷ Giá";
            this.TYGIA.DisplayFormat.FormatString = "N0";
            this.TYGIA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TYGIA.FieldName = "TYGIA";
            this.TYGIA.Name = "TYGIA";
            this.TYGIA.OptionsColumn.AllowEdit = false;
            this.TYGIA.OptionsColumn.AllowFocus = false;
            this.TYGIA.Visible = true;
            this.TYGIA.VisibleIndex = 2;
            this.TYGIA.Width = 46;
            // 
            // PHUTHU
            // 
            this.PHUTHU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.PHUTHU.AppearanceCell.Options.UseFont = true;
            this.PHUTHU.AppearanceCell.Options.UseTextOptions = true;
            this.PHUTHU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PHUTHU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PHUTHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PHUTHU.AppearanceHeader.Options.UseFont = true;
            this.PHUTHU.AppearanceHeader.Options.UseTextOptions = true;
            this.PHUTHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PHUTHU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PHUTHU.Caption = "% Phụ Thu";
            this.PHUTHU.FieldName = "PHUTHU";
            this.PHUTHU.Name = "PHUTHU";
            this.PHUTHU.OptionsColumn.AllowEdit = false;
            this.PHUTHU.OptionsColumn.AllowFocus = false;
            this.PHUTHU.Width = 52;
            // 
            // pnlHeaderCurrentcy
            // 
            this.pnlHeaderCurrentcy.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlHeaderCurrentcy.Controls.Add(this.lbCurrentcy);
            this.pnlHeaderCurrentcy.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeaderCurrentcy.Location = new System.Drawing.Point(2, 2);
            this.pnlHeaderCurrentcy.Name = "pnlHeaderCurrentcy";
            this.pnlHeaderCurrentcy.Size = new System.Drawing.Size(276, 38);
            this.pnlHeaderCurrentcy.TabIndex = 7;
            // 
            // lbCurrentcy
            // 
            this.lbCurrentcy.Appearance.Font = new System.Drawing.Font("Tahoma", 14F);
            this.lbCurrentcy.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbCurrentcy.Location = new System.Drawing.Point(102, 8);
            this.lbCurrentcy.Name = "lbCurrentcy";
            this.lbCurrentcy.Size = new System.Drawing.Size(85, 23);
            this.lbCurrentcy.TabIndex = 16;
            this.lbCurrentcy.Text = "NGOẠI TỆ";
            // 
            // pnlHeaderShowPayment
            // 
            this.pnlHeaderShowPayment.Controls.Add(this.lbInforName);
            this.pnlHeaderShowPayment.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeaderShowPayment.Location = new System.Drawing.Point(2, 2);
            this.pnlHeaderShowPayment.Name = "pnlHeaderShowPayment";
            this.pnlHeaderShowPayment.Size = new System.Drawing.Size(728, 35);
            this.pnlHeaderShowPayment.TabIndex = 34;
            // 
            // lbInforName
            // 
            this.lbInforName.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbInforName.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbInforName.Location = new System.Drawing.Point(5, 7);
            this.lbInforName.Name = "lbInforName";
            this.lbInforName.Size = new System.Drawing.Size(129, 19);
            this.lbInforName.TabIndex = 1;
            this.lbInforName.Text = "thông tin phòng";
            // 
            // pnTime
            // 
            this.pnTime.Controls.Add(this.gc_time);
            this.pnTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnTime.Location = new System.Drawing.Point(0, 70);
            this.pnTime.Name = "pnTime";
            this.pnTime.Size = new System.Drawing.Size(732, 702);
            this.pnTime.TabIndex = 52;
            // 
            // gc_time
            // 
            this.gc_time.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc_time.Location = new System.Drawing.Point(2, 2);
            this.gc_time.MainView = this.gv_time;
            this.gc_time.Name = "gc_time";
            this.gc_time.Size = new System.Drawing.Size(728, 698);
            this.gc_time.TabIndex = 1;
            this.gc_time.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv_time});
            // 
            // gv_time
            // 
            this.gv_time.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 20F, System.Drawing.FontStyle.Bold);
            this.gv_time.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Blue;
            this.gv_time.Appearance.FooterPanel.Options.UseFont = true;
            this.gv_time.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gv_time.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gv_time.Appearance.GroupFooter.Options.UseFont = true;
            this.gv_time.ColumnPanelRowHeight = 50;
            this.gv_time.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.THOIGIANVAO,
            this.THOIGIANRA,
            this.BLOCKKARAOKE,
            this.SOPHUT,
            this.SOBLOCK,
            this.GIABLOCK,
            this.THANHTIEN});
            this.gv_time.FooterPanelHeight = 50;
            this.gv_time.GridControl = this.gc_time;
            this.gv_time.Name = "gv_time";
            this.gv_time.OptionsView.ShowFooter = true;
            this.gv_time.OptionsView.ShowGroupPanel = false;
            this.gv_time.RowHeight = 50;
            // 
            // THOIGIANVAO
            // 
            this.THOIGIANVAO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.THOIGIANVAO.AppearanceCell.Options.UseFont = true;
            this.THOIGIANVAO.AppearanceCell.Options.UseTextOptions = true;
            this.THOIGIANVAO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THOIGIANVAO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THOIGIANVAO.AppearanceHeader.Options.UseFont = true;
            this.THOIGIANVAO.AppearanceHeader.Options.UseTextOptions = true;
            this.THOIGIANVAO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THOIGIANVAO.Caption = "VÀO";
            this.THOIGIANVAO.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.THOIGIANVAO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.THOIGIANVAO.FieldName = "THOIGIANVAO";
            this.THOIGIANVAO.Name = "THOIGIANVAO";
            this.THOIGIANVAO.OptionsColumn.AllowEdit = false;
            this.THOIGIANVAO.OptionsColumn.AllowFocus = false;
            this.THOIGIANVAO.Visible = true;
            this.THOIGIANVAO.VisibleIndex = 0;
            this.THOIGIANVAO.Width = 114;
            // 
            // THOIGIANRA
            // 
            this.THOIGIANRA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.THOIGIANRA.AppearanceCell.Options.UseFont = true;
            this.THOIGIANRA.AppearanceCell.Options.UseTextOptions = true;
            this.THOIGIANRA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THOIGIANRA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THOIGIANRA.AppearanceHeader.Options.UseFont = true;
            this.THOIGIANRA.AppearanceHeader.Options.UseTextOptions = true;
            this.THOIGIANRA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THOIGIANRA.Caption = "RA";
            this.THOIGIANRA.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.THOIGIANRA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.THOIGIANRA.FieldName = "THOIGIANRA";
            this.THOIGIANRA.Name = "THOIGIANRA";
            this.THOIGIANRA.OptionsColumn.AllowEdit = false;
            this.THOIGIANRA.OptionsColumn.AllowFocus = false;
            this.THOIGIANRA.Visible = true;
            this.THOIGIANRA.VisibleIndex = 1;
            this.THOIGIANRA.Width = 109;
            // 
            // BLOCKKARAOKE
            // 
            this.BLOCKKARAOKE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.BLOCKKARAOKE.AppearanceCell.Options.UseFont = true;
            this.BLOCKKARAOKE.AppearanceCell.Options.UseTextOptions = true;
            this.BLOCKKARAOKE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BLOCKKARAOKE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.BLOCKKARAOKE.AppearanceHeader.Options.UseFont = true;
            this.BLOCKKARAOKE.AppearanceHeader.Options.UseTextOptions = true;
            this.BLOCKKARAOKE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BLOCKKARAOKE.Caption = "BLOCK";
            this.BLOCKKARAOKE.FieldName = "BLOCKKARAOKE";
            this.BLOCKKARAOKE.Name = "BLOCKKARAOKE";
            this.BLOCKKARAOKE.OptionsColumn.AllowEdit = false;
            this.BLOCKKARAOKE.OptionsColumn.AllowFocus = false;
            this.BLOCKKARAOKE.Visible = true;
            this.BLOCKKARAOKE.VisibleIndex = 2;
            this.BLOCKKARAOKE.Width = 50;
            // 
            // SOPHUT
            // 
            this.SOPHUT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SOPHUT.AppearanceCell.Options.UseFont = true;
            this.SOPHUT.AppearanceCell.Options.UseTextOptions = true;
            this.SOPHUT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOPHUT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOPHUT.AppearanceHeader.Options.UseFont = true;
            this.SOPHUT.AppearanceHeader.Options.UseTextOptions = true;
            this.SOPHUT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOPHUT.Caption = "PHÚT";
            this.SOPHUT.FieldName = "SOPHUT";
            this.SOPHUT.Name = "SOPHUT";
            this.SOPHUT.OptionsColumn.AllowEdit = false;
            this.SOPHUT.OptionsColumn.AllowFocus = false;
            this.SOPHUT.Visible = true;
            this.SOPHUT.VisibleIndex = 3;
            this.SOPHUT.Width = 55;
            // 
            // SOBLOCK
            // 
            this.SOBLOCK.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SOBLOCK.AppearanceCell.Options.UseFont = true;
            this.SOBLOCK.AppearanceCell.Options.UseTextOptions = true;
            this.SOBLOCK.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOBLOCK.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOBLOCK.AppearanceHeader.Options.UseFont = true;
            this.SOBLOCK.AppearanceHeader.Options.UseTextOptions = true;
            this.SOBLOCK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOBLOCK.Caption = "SỐ BLOCK";
            this.SOBLOCK.FieldName = "SOBLOCK";
            this.SOBLOCK.Name = "SOBLOCK";
            this.SOBLOCK.OptionsColumn.AllowEdit = false;
            this.SOBLOCK.OptionsColumn.AllowFocus = false;
            this.SOBLOCK.Visible = true;
            this.SOBLOCK.VisibleIndex = 4;
            this.SOBLOCK.Width = 77;
            // 
            // GIABLOCK
            // 
            this.GIABLOCK.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.GIABLOCK.AppearanceCell.Options.UseFont = true;
            this.GIABLOCK.AppearanceCell.Options.UseTextOptions = true;
            this.GIABLOCK.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIABLOCK.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIABLOCK.AppearanceHeader.Options.UseFont = true;
            this.GIABLOCK.AppearanceHeader.Options.UseTextOptions = true;
            this.GIABLOCK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIABLOCK.Caption = "GIÁ";
            this.GIABLOCK.DisplayFormat.FormatString = "{0:#,###0}";
            this.GIABLOCK.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GIABLOCK.FieldName = "GIABLOCK";
            this.GIABLOCK.Name = "GIABLOCK";
            this.GIABLOCK.OptionsColumn.AllowEdit = false;
            this.GIABLOCK.OptionsColumn.AllowFocus = false;
            this.GIABLOCK.Visible = true;
            this.GIABLOCK.VisibleIndex = 5;
            this.GIABLOCK.Width = 85;
            // 
            // THANHTIEN
            // 
            this.THANHTIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.THANHTIEN.AppearanceCell.Options.UseFont = true;
            this.THANHTIEN.AppearanceCell.Options.UseTextOptions = true;
            this.THANHTIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.THANHTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THANHTIEN.AppearanceHeader.Options.UseFont = true;
            this.THANHTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.THANHTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THANHTIEN.Caption = "THÀNH TIỀN";
            this.THANHTIEN.DisplayFormat.FormatString = "{0:#,###0}";
            this.THANHTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.THANHTIEN.FieldName = "THANHTIEN";
            this.THANHTIEN.Name = "THANHTIEN";
            this.THANHTIEN.OptionsColumn.AllowEdit = false;
            this.THANHTIEN.OptionsColumn.AllowFocus = false;
            this.THANHTIEN.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "THANHTIEN", "{0:#,###0}")});
            this.THANHTIEN.Visible = true;
            this.THANHTIEN.VisibleIndex = 6;
            this.THANHTIEN.Width = 131;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pnLogo
            // 
            this.pnLogo.Controls.Add(this.panelControl12);
            this.pnLogo.Controls.Add(this.panelControl57);
            this.pnLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnLogo.Location = new System.Drawing.Point(0, 0);
            this.pnLogo.Name = "pnLogo";
            this.pnLogo.Size = new System.Drawing.Size(1113, 70);
            this.pnLogo.TabIndex = 53;
            // 
            // panelControl12
            // 
            this.panelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl12.Controls.Add(this.panelControl6);
            this.panelControl12.Controls.Add(this.panelControl56);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl12.Location = new System.Drawing.Point(2, 5);
            this.panelControl12.Margin = new System.Windows.Forms.Padding(6);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(1109, 63);
            this.panelControl12.TabIndex = 3;
            // 
            // panelControl6
            // 
            this.panelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.panelControl6.Controls.Add(this.panelControl25);
            this.panelControl6.Controls.Add(this.lbgiay);
            this.panelControl6.Controls.Add(this.lbCaBan);
            this.panelControl6.Controls.Add(this.lbGio);
            this.panelControl6.Controls.Add(this.lb_NhanVien);
            this.panelControl6.Controls.Add(this.lbtimeby);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(196, 0);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(913, 63);
            this.panelControl6.TabIndex = 23;
            // 
            // panelControl25
            // 
            this.panelControl25.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl25.Controls.Add(this.lb_Kho);
            this.panelControl25.Controls.Add(this.lb_Quay);
            this.panelControl25.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl25.Location = new System.Drawing.Point(2, 2);
            this.panelControl25.Name = "panelControl25";
            this.panelControl25.Size = new System.Drawing.Size(349, 59);
            this.panelControl25.TabIndex = 3;
            // 
            // lb_Kho
            // 
            this.lb_Kho.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lb_Kho.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_Kho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_Kho.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_Kho.Location = new System.Drawing.Point(13, 34);
            this.lb_Kho.Name = "lb_Kho";
            this.lb_Kho.Size = new System.Drawing.Size(41, 19);
            this.lb_Kho.TabIndex = 15;
            this.lb_Kho.Text = "Quầy";
            // 
            // lb_Quay
            // 
            this.lb_Quay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lb_Quay.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_Quay.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.lb_Quay.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_Quay.Location = new System.Drawing.Point(13, 5);
            this.lb_Quay.Name = "lb_Quay";
            this.lb_Quay.Size = new System.Drawing.Size(52, 24);
            this.lb_Quay.TabIndex = 14;
            this.lb_Quay.Text = "Quầy";
            // 
            // lbgiay
            // 
            this.lbgiay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbgiay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbgiay.Location = new System.Drawing.Point(872, 27);
            this.lbgiay.Name = "lbgiay";
            this.lbgiay.Size = new System.Drawing.Size(18, 19);
            this.lbgiay.TabIndex = 19;
            this.lbgiay.Text = "00";
            // 
            // lbCaBan
            // 
            this.lbCaBan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCaBan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbCaBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbCaBan.Location = new System.Drawing.Point(805, 5);
            this.lbCaBan.Name = "lbCaBan";
            this.lbCaBan.Size = new System.Drawing.Size(94, 19);
            this.lbCaBan.TabIndex = 17;
            this.lbCaBan.Text = "21-12-2012";
            // 
            // lbGio
            // 
            this.lbGio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGio.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGio.Location = new System.Drawing.Point(820, 34);
            this.lbGio.Name = "lbGio";
            this.lbGio.Size = new System.Drawing.Size(46, 19);
            this.lbGio.TabIndex = 20;
            this.lbGio.Text = "00:00";
            // 
            // lb_NhanVien
            // 
            this.lb_NhanVien.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lb_NhanVien.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_NhanVien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_NhanVien.Location = new System.Drawing.Point(401, 5);
            this.lb_NhanVien.Name = "lb_NhanVien";
            this.lb_NhanVien.Size = new System.Drawing.Size(89, 19);
            this.lb_NhanVien.TabIndex = 18;
            this.lb_NhanVien.Text = "Nhân Viên:";
            // 
            // lbtimeby
            // 
            this.lbtimeby.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbtimeby.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbtimeby.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbtimeby.Location = new System.Drawing.Point(401, 38);
            this.lbtimeby.Name = "lbtimeby";
            this.lbtimeby.Size = new System.Drawing.Size(21, 19);
            this.lbtimeby.TabIndex = 16;
            this.lbtimeby.Text = "Ca";
            // 
            // panelControl56
            // 
            this.panelControl56.Controls.Add(this.panel2);
            this.panelControl56.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl56.Location = new System.Drawing.Point(0, 0);
            this.panelControl56.Name = "panelControl56";
            this.panelControl56.Size = new System.Drawing.Size(196, 63);
            this.panelControl56.TabIndex = 21;
            // 
            // panel2
            // 
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(192, 59);
            this.panel2.TabIndex = 22;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(6, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(177, 41);
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // panelControl57
            // 
            this.panelControl57.Appearance.BackColor = System.Drawing.Color.DarkGray;
            this.panelControl57.Appearance.ForeColor = System.Drawing.Color.Black;
            this.panelControl57.Appearance.Options.UseBackColor = true;
            this.panelControl57.Appearance.Options.UseForeColor = true;
            this.panelControl57.Controls.Add(this.panelControl58);
            this.panelControl57.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl57.Location = new System.Drawing.Point(2, 2);
            this.panelControl57.Name = "panelControl57";
            this.panelControl57.Size = new System.Drawing.Size(1109, 3);
            this.panelControl57.TabIndex = 3;
            // 
            // panelControl58
            // 
            this.panelControl58.Appearance.BackColor = System.Drawing.Color.DarkGray;
            this.panelControl58.Appearance.ForeColor = System.Drawing.Color.Black;
            this.panelControl58.Appearance.Options.UseBackColor = true;
            this.panelControl58.Appearance.Options.UseForeColor = true;
            this.panelControl58.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl58.Location = new System.Drawing.Point(2, 2);
            this.panelControl58.Name = "panelControl58";
            this.panelControl58.Size = new System.Drawing.Size(1105, 0);
            this.panelControl58.TabIndex = 4;
            // 
            // Frm_Payment_KhachSan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1113, 772);
            this.Controls.Add(this.pnlPaymentShowFill);
            this.Controls.Add(this.pnTime);
            this.Controls.Add(this.pnlSpace);
            this.Controls.Add(this.pnlGridviewActionMain);
            this.Controls.Add(this.pnLogo);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "Frm_Payment_KhachSan";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Payment";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Payment_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Payment_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pnlGridviewActionMain)).EndInit();
            this.pnlGridviewActionMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlInforBuyProduct)).EndInit();
            this.pnlInforBuyProduct.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlGridview)).EndInit();
            this.pnlGridview.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcListProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcListGIO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListGIO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDownGridview)).EndInit();
            this.pnUpDownGridview.ResumeLayout(false);
            this.pnUpDownGridview.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInforMoneyAndCustomer)).EndInit();
            this.pnlInforMoneyAndCustomer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlInforCustomer)).EndInit();
            this.pnlInforCustomer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlInnerCustomerShow)).EndInit();
            this.pnlInnerCustomerShow.ResumeLayout(false);
            this.pnlInnerCustomerShow.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDearCustomerShow)).EndInit();
            this.pnlDearCustomerShow.ResumeLayout(false);
            this.pnlDearCustomerShow.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInforMoney)).EndInit();
            this.pnlInforMoney.ResumeLayout(false);
            this.pnlInforMoney.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlActionCashier)).EndInit();
            this.pnlActionCashier.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlPayment)).EndInit();
            this.pnlPayment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlPaymentButton)).EndInit();
            this.pnlPaymentButton.ResumeLayout(false);
            this.pnlPaymentButton.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSpace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPaymentShowFill)).EndInit();
            this.pnlPaymentShowFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlShowInforPayment)).EndInit();
            this.pnlShowInforPayment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlInputVoucher)).EndInit();
            this.pnlInputVoucher.ResumeLayout(false);
            this.pnlInputVoucher.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputVoucher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInputGetMoney)).EndInit();
            this.pnlInputGetMoney.ResumeLayout(false);
            this.pnlInputGetMoney.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputMoney.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInnerCustomer)).EndInit();
            this.pnlInnerCustomer.ResumeLayout(false);
            this.pnlInnerCustomer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInnerCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlServiceMoney)).EndInit();
            this.pnlServiceMoney.ResumeLayout(false);
            this.pnlServiceMoney.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceMoney.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl55)).EndInit();
            this.panelControl55.ResumeLayout(false);
            this.panelControl55.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl51)).EndInit();
            this.panelControl51.ResumeLayout(false);
            this.panelControl51.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlThird)).EndInit();
            this.pnlThird.ResumeLayout(false);
            this.pnlThird.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSecond)).EndInit();
            this.pnlSecond.ResumeLayout(false);
            this.pnlSecond.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlFirst)).EndInit();
            this.pnlFirst.ResumeLayout(false);
            this.pnlFirst.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl27)).EndInit();
            this.panelControl27.ResumeLayout(false);
            this.panelControl27.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMoneyReply)).EndInit();
            this.pnlMoneyReply.ResumeLayout(false);
            this.pnlMoneyReply.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl29)).EndInit();
            this.panelControl29.ResumeLayout(false);
            this.panelControl29.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl30)).EndInit();
            this.panelControl30.ResumeLayout(false);
            this.panelControl30.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl31)).EndInit();
            this.panelControl31.ResumeLayout(false);
            this.panelControl31.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInputDiscount)).EndInit();
            this.pnlInputDiscount.ResumeLayout(false);
            this.pnlInputDiscount.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputDiscount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDearCustomer)).EndInit();
            this.pnlDearCustomer.ResumeLayout(false);
            this.pnlDearCustomer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDearCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlVisa)).EndInit();
            this.pnlVisa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameVisa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCwCvc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdCodeCreate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdCodeInput.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMoneyVisa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdCardVisa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlHeaderVisa)).EndInit();
            this.pnlHeaderVisa.ResumeLayout(false);
            this.pnlHeaderVisa.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBottomPayment)).EndInit();
            this.pnlBottomPayment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlKeyboardAndShowForeignCurrency)).EndInit();
            this.pnlKeyboardAndShowForeignCurrency.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlShowForeignCurrency)).EndInit();
            this.pnlShowForeignCurrency.ResumeLayout(false);
            this.pnlShowForeignCurrency.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKeyboardNumber)).EndInit();
            this.pnlKeyboardNumber.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlKeyBoardText)).EndInit();
            this.pnlKeyBoardText.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlKeyboardMoney)).EndInit();
            this.pnlKeyboardMoney.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlUpDownForeignCurrency)).EndInit();
            this.pnlUpDownForeignCurrency.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlForeignCurrency)).EndInit();
            this.pnlForeignCurrency.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcForeignCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListForiegnCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHeaderCurrentcy)).EndInit();
            this.pnlHeaderCurrentcy.ResumeLayout(false);
            this.pnlHeaderCurrentcy.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHeaderShowPayment)).EndInit();
            this.pnlHeaderShowPayment.ResumeLayout(false);
            this.pnlHeaderShowPayment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnTime)).EndInit();
            this.pnTime.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc_time)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_time)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnLogo)).EndInit();
            this.pnLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl25)).EndInit();
            this.panelControl25.ResumeLayout(false);
            this.panelControl25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl56)).EndInit();
            this.panelControl56.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl57)).EndInit();
            this.panelControl57.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl58)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnlGridviewActionMain;
        private DevExpress.XtraEditors.PanelControl pnlSpace;
        private DevExpress.XtraEditors.PanelControl pnlPaymentShowFill;
        private DevExpress.XtraEditors.PanelControl pnlHeaderShowPayment;
        private DevExpress.XtraEditors.LabelControl lbInforName;
        private DevExpress.XtraEditors.PanelControl pnlShowInforPayment;
        private DevExpress.XtraEditors.PanelControl pnlInputGetMoney;
        private DevExpress.XtraEditors.TextEdit txtInputMoney;
        private DevExpress.XtraEditors.LabelControl lbPayment;
        private DevExpress.XtraEditors.PanelControl pnlInputDiscount;
        private DevExpress.XtraEditors.TextEdit txtInputDiscount;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.PanelControl pnlThird;
        private DevExpress.XtraEditors.LabelControl lbThird;
        private DevExpress.XtraEditors.LabelControl lbThirdMoney;
        private DevExpress.XtraEditors.PanelControl pnlSecond;
        private DevExpress.XtraEditors.LabelControl lbSecond;
        private DevExpress.XtraEditors.LabelControl lbSecondMoney;
        private DevExpress.XtraEditors.PanelControl pnlFirst;
        private DevExpress.XtraEditors.LabelControl lbFirst;
        private DevExpress.XtraEditors.LabelControl lbFirstMoney;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.PanelControl panelControl27;
        private DevExpress.XtraEditors.LabelControl lbDiscountPayment;
        private DevExpress.XtraEditors.LabelControl lbDiscount;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.PanelControl pnlMoneyReply;
        private DevExpress.XtraEditors.LabelControl lbReplyCustomer;
        private DevExpress.XtraEditors.LabelControl lbVNDReply;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.PanelControl panelControl29;
        private DevExpress.XtraEditors.LabelControl lbTotalMoneyAll;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.PanelControl panelControl30;
        private DevExpress.XtraEditors.LabelControl lbGetCustomer;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.PanelControl panelControl31;
        private DevExpress.XtraEditors.LabelControl lbTotalMoneyPayment;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.PanelControl pnlBottomPayment;
        private DevExpress.XtraEditors.PanelControl pnlForeignCurrency;
        private DevExpress.XtraGrid.GridControl gcForeignCurrency;
        private DevExpress.XtraGrid.Views.Grid.GridView gvListForiegnCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn STT1;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraGrid.Columns.GridColumn TYGIA;
        private DevExpress.XtraGrid.Columns.GridColumn PHUTHU;
        private DevExpress.XtraEditors.PanelControl pnlUpDownForeignCurrency;
        private DevExpress.XtraEditors.SimpleButton btnUpForeignCurrency;
      
        private DevExpress.XtraEditors.PanelControl pnlKeyboardAndShowForeignCurrency;
        private DevExpress.XtraEditors.PanelControl pnlKeyboardNumber;
        private DevExpress.XtraEditors.SimpleButton btnMoveRightNumber;
        private DevExpress.XtraEditors.SimpleButton btn1;
        private DevExpress.XtraEditors.SimpleButton btnPaymentNumber;
        private DevExpress.XtraEditors.SimpleButton btnTranslateNumber;
        private DevExpress.XtraEditors.SimpleButton btn000;
        private DevExpress.XtraEditors.SimpleButton btn00;
        private DevExpress.XtraEditors.SimpleButton btn0;
        private DevExpress.XtraEditors.SimpleButton btnOkNumber;
        private DevExpress.XtraEditors.SimpleButton btnDeleteNumber;
        private DevExpress.XtraEditors.SimpleButton btn6;
        private DevExpress.XtraEditors.SimpleButton btn9;
        private DevExpress.XtraEditors.SimpleButton btn8;
        private DevExpress.XtraEditors.SimpleButton btn7;
        private DevExpress.XtraEditors.SimpleButton btnMoveLeftNumber;
        private DevExpress.XtraEditors.SimpleButton btn5;
        private DevExpress.XtraEditors.SimpleButton btn4;
        private DevExpress.XtraEditors.SimpleButton btn3;
        private DevExpress.XtraEditors.SimpleButton btn2;
        private DevExpress.XtraEditors.SimpleButton btnEscNumber;
        private DevExpress.XtraEditors.PanelControl pnlShowForeignCurrency;
        private DevExpress.XtraEditors.LabelControl lbExchangeRate;
        private DevExpress.XtraEditors.LabelControl lbKindMoney;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.PanelControl pnlActionCashier;
        private DevExpress.XtraEditors.PanelControl pnlPayment;
        private DevExpress.XtraEditors.SimpleButton btnRetypePayment;
        private DevExpress.XtraEditors.SimpleButton btnDiscount;
        private DevExpress.XtraEditors.SimpleButton btnCardImtimateCustomer;
        private DevExpress.XtraEditors.SimpleButton btnCancelPayment;
        private DevExpress.XtraEditors.SimpleButton btnNotPrintBill;
        private DevExpress.XtraEditors.SimpleButton btnCardInternalPayment;
        private DevExpress.XtraEditors.SimpleButton btnOpenSafe;
        private DevExpress.XtraEditors.PanelControl pnlPaymentButton;
        private DevExpress.XtraEditors.PanelControl pnlInforBuyProduct;
        private DevExpress.XtraEditors.PanelControl pnlGridview;
        private DevExpress.XtraGrid.GridControl gcListProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView gvListProduct;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn ProductName;
        private DevExpress.XtraGrid.Columns.GridColumn Quantum;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn Price;
        private DevExpress.XtraGrid.Columns.GridColumn TheMoney;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;
        private DevExpress.XtraGrid.Columns.GridColumn MA_BEP;
        private DevExpress.XtraGrid.Columns.GridColumn THUE;
        private DevExpress.XtraEditors.PanelControl pnlInforMoneyAndCustomer;
        private DevExpress.XtraEditors.LabelControl lbTheMoney;
        private DevExpress.XtraEditors.LabelControl labelControl52;
        private DevExpress.XtraEditors.LabelControl lbHourMoney;
        private DevExpress.XtraEditors.LabelControl lb_clicktiengio;
        private DevExpress.XtraEditors.LabelControl lbTotalMoney;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.PanelControl panelControl55;
        private DevExpress.XtraEditors.LabelControl lbMoneyGetFirst;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private DevExpress.XtraEditors.PanelControl panelControl51;
        private DevExpress.XtraEditors.LabelControl lbServiceMoney;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.PanelControl pnlKeyBoardText;
        private DevExpress.XtraEditors.SimpleButton btnF;
        private DevExpress.XtraEditors.SimpleButton btnE;
        private DevExpress.XtraEditors.SimpleButton btnD;
        private DevExpress.XtraEditors.SimpleButton btnC;
        private DevExpress.XtraEditors.SimpleButton btnB;
        private DevExpress.XtraEditors.SimpleButton btnA;
        private DevExpress.XtraEditors.SimpleButton btnMoveRigthText;
        private DevExpress.XtraEditors.SimpleButton btnM;
        private DevExpress.XtraEditors.SimpleButton btnPaymentText;
        private DevExpress.XtraEditors.SimpleButton btnTranslateText;
        private DevExpress.XtraEditors.SimpleButton btnR;
        private DevExpress.XtraEditors.SimpleButton btnQ;
        private DevExpress.XtraEditors.SimpleButton btnP;
        private DevExpress.XtraEditors.SimpleButton btnOkText;
        private DevExpress.XtraEditors.SimpleButton btnDeleteText;
        private DevExpress.XtraEditors.SimpleButton btnL;
        private DevExpress.XtraEditors.SimpleButton btnI;
        private DevExpress.XtraEditors.SimpleButton btnH;
        private DevExpress.XtraEditors.SimpleButton btnG;
        private DevExpress.XtraEditors.SimpleButton btnMoveLeftText;
        private DevExpress.XtraEditors.SimpleButton btnK;
        private DevExpress.XtraEditors.SimpleButton btnJ;
        private DevExpress.XtraEditors.SimpleButton btnO;
        private DevExpress.XtraEditors.SimpleButton btnN;
        private DevExpress.XtraEditors.SimpleButton btnEscText;
        private DevExpress.XtraEditors.PanelControl pnlKeyboardMoney;
        private DevExpress.XtraEditors.SimpleButton btn500000;
        private DevExpress.XtraEditors.SimpleButton btn200000;
        private DevExpress.XtraEditors.SimpleButton btn100000;
        private DevExpress.XtraEditors.SimpleButton btn50000;
        private DevExpress.XtraEditors.SimpleButton btn20000;
        private DevExpress.XtraEditors.SimpleButton btn10000;
        private DevExpress.XtraEditors.SimpleButton btnMoveRightMoney;
        private DevExpress.XtraEditors.SimpleButton btnPaymentMoney;
        private DevExpress.XtraEditors.SimpleButton btnTranslateMoney;
        private DevExpress.XtraEditors.SimpleButton btnOkMoney;
        private DevExpress.XtraEditors.SimpleButton btnDeleteMoney;
        private DevExpress.XtraEditors.SimpleButton btn2000;
        private DevExpress.XtraEditors.SimpleButton btn1000;
        private DevExpress.XtraEditors.SimpleButton btn500;
        private DevExpress.XtraEditors.SimpleButton btnMoveLeftMoney;
        private DevExpress.XtraEditors.SimpleButton btn5000;
        private DevExpress.XtraEditors.SimpleButton btnEcsMoney;
        private DevExpress.XtraEditors.SimpleButton btnS;
        private DevExpress.XtraEditors.SimpleButton btnW;
        private DevExpress.XtraEditors.SimpleButton btnV;
        private DevExpress.XtraEditors.SimpleButton btnU;
        private DevExpress.XtraEditors.SimpleButton btnT;
        private DevExpress.XtraEditors.SimpleButton btnZ;
        private DevExpress.XtraEditors.SimpleButton btnY;
        private DevExpress.XtraEditors.SimpleButton btnShift;
        private DevExpress.XtraEditors.SimpleButton btnX;
        private DevExpress.XtraEditors.PanelControl pnlDearCustomer;
        private DevExpress.XtraEditors.TextEdit txtDearCustomer;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.PanelControl pnlInnerCustomer;
        private DevExpress.XtraEditors.TextEdit txtInnerCustomer;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.SimpleButton btnDownForeignCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn Discount;
        private DevExpress.XtraEditors.SimpleButton btnServiceMoney;
        private DevExpress.XtraEditors.PanelControl pnlServiceMoney;
        private DevExpress.XtraEditors.TextEdit txtServiceMoney;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.PanelControl pnlInforMoney;
        private DevExpress.XtraEditors.PanelControl pnlInforCustomer;
        private DevExpress.XtraEditors.PanelControl pnlInnerCustomerShow;
        private DevExpress.XtraEditors.PanelControl pnlDearCustomerShow;
        private DevExpress.XtraEditors.LabelControl lbIDDearCustomerShow;
        private DevExpress.XtraEditors.LabelControl lbNameDearCustomer;
        private DevExpress.XtraEditors.LabelControl lbMark;
        private DevExpress.XtraEditors.LabelControl lbMoneySurplus;
        private DevExpress.XtraEditors.LabelControl lbIdInnerCustomerShow;
        private DevExpress.XtraEditors.LabelControl lbNameInnerCustomer;
        private DevExpress.XtraEditors.SimpleButton btnVisa;
        private DevExpress.XtraEditors.SimpleButton btnVoucher;
        private DevExpress.XtraEditors.PanelControl pnUpDownGridview;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl lbRowsCount;
        private DevExpress.XtraEditors.LabelControl lbSTT;
        private DevExpress.XtraEditors.SimpleButton btnUpGridview;
        private DevExpress.XtraEditors.SimpleButton btnDownGridview;
        private DevExpress.XtraEditors.SimpleButton btnLastGridview;
        private DevExpress.XtraEditors.SimpleButton btnFirstGridview;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.PanelControl pnlHeaderCurrentcy;
        private DevExpress.XtraEditors.LabelControl lbCurrentcy;
        private DevExpress.XtraEditors.LabelControl lbcurrencyGetMoney;
        private DevExpress.XtraEditors.LabelControl lbcurrencyService;
        private DevExpress.XtraEditors.PanelControl pnlVisa;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.TextEdit txtCountry;
        private DevExpress.XtraEditors.TextEdit txtNameVisa;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.SimpleButton btnRefeshIdCode;
        private DevExpress.XtraEditors.TextEdit txtCwCvc;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txtIdCodeCreate;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.TextEdit txtCity;
        private DevExpress.XtraEditors.TextEdit txtIdCodeInput;
        private DevExpress.XtraEditors.TextEdit txtAddress;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.TextEdit txtYear;
        private DevExpress.XtraEditors.TextEdit txtMonth;
        private DevExpress.XtraEditors.TextEdit txtMoneyVisa;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtIdCardVisa;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnPaymentVisa;
        private DevExpress.XtraEditors.SimpleButton bntCancelVisa;
        private DevExpress.XtraEditors.PanelControl pnlHeaderVisa;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.PanelControl pnlInputVoucher;
        private DevExpress.XtraEditors.TextEdit txtInputVoucher;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.PanelControl pnTime;
        private DevExpress.XtraGrid.GridControl gc_time;
        private DevExpress.XtraGrid.Views.Grid.GridView gv_time;
        private DevExpress.XtraGrid.Columns.GridColumn THOIGIANVAO;
        private DevExpress.XtraGrid.Columns.GridColumn THOIGIANRA;
        private DevExpress.XtraGrid.Columns.GridColumn BLOCKKARAOKE;
        private DevExpress.XtraGrid.Columns.GridColumn SOPHUT;
        private DevExpress.XtraGrid.Columns.GridColumn SOBLOCK;
        private DevExpress.XtraGrid.Columns.GridColumn GIABLOCK;
        private DevExpress.XtraGrid.Columns.GridColumn THANHTIEN;
        private DevExpress.XtraEditors.PanelControl pnLogo;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.PanelControl panelControl56;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.PanelControl panelControl57;
        private DevExpress.XtraEditors.PanelControl panelControl58;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.PanelControl panelControl25;
        private DevExpress.XtraEditors.LabelControl lb_Kho;
        private DevExpress.XtraEditors.LabelControl lb_Quay;
        private DevExpress.XtraEditors.LabelControl lbgiay;
        private DevExpress.XtraEditors.LabelControl lbCaBan;
        private DevExpress.XtraEditors.LabelControl lbGio;
        private DevExpress.XtraEditors.LabelControl lb_NhanVien;
        private DevExpress.XtraEditors.LabelControl lbtimeby;
        private DevExpress.XtraGrid.GridControl gcListGIO;
        private DevExpress.XtraGrid.Views.Grid.GridView gvListGIO;
        private DevExpress.XtraGrid.Columns.GridColumn STT_GIO;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraEditors.SimpleButton btnCongNo;
        private DevExpress.XtraEditors.SimpleButton btnGiaohang;
    }
}