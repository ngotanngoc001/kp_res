﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Threading;
using KP_UserManagement;
using KP_RES.Class;
using System.IO;
using KP_Terminal;
using KP_RES;
using DevExpress.XtraEditors.Controls;
using KP_Report;

namespace KP_RES
{
    public partial class Frm_BanHang_KhachSan : DevExpress.XtraEditors.XtraForm
    {
        public int funtions = 1;// Funtion 1 Sơ đồ bàn, Funtion 2 Open Table, 3 yêu cầu thêm, 4 send Order,5 join Table , 6 divide, 7 Payments, 8 buy id Product, 9 Check Table, 10 ket ca,200 Nhận phòng
        public int sendOrder = 0;
        public string maBan = "";
        public string maBanGhep = "";
        public string idBillJoin = "";
        bool Flag_soluong = false; //true:so luong tren lưới dc bắt đầu lại,False:sô lượng đang tạo +thêm chuỗi.
        public string maMon;
        public float xGIABAN;
        public DataTable dt = new DataTable("HANGHOA");
        public DataTable dtBan = new DataTable();
        public DataTable dtKhuVuc = new DataTable();
        public DataTable dtNhomhang = new DataTable();
        public DataTable dtMon = new DataTable();
        public DataTable dtTiengio = new DataTable();
        public DataTable dtkc = new DataTable();
        DataTable dtHangOrder = new DataTable();//Những mặt hàng đã order
        public delegate void GetString(DataTable dtshow, int ctrol, int iFocusRow);
        public GetString MyGetData;
        public int contrDelegate = 0;
        public int iFocusRow = 0;
        public static BindingSource bsource = new BindingSource();
        public int tatol_CurrenPages;
        public int tatol_cPageMon;
        public int cPageKV = 1;
        public int cPageDish = 1;
        public int CurrenPages = 1;
        public int SizePages = 30;
        public int cPageMon = 1;
        public int sizeMon = 30;
        public int maKV = 1;
        public int maNH = 1;
        public DataTable divide_Table1 = new DataTable();
        public DataTable divide_Table2 = new DataTable();
        public int setBgrk = 0;
        bool testID = false; // bien ktra trùng tên khi addrow hàng hóa
        public int fastFoot = 0;

        public Frm_BanHang_KhachSan()
        {
            InitializeComponent();
            LoadPermission();
            lb_Quay.Text = cls_ConfigCashier.nameCashier;
            lb_Kho.Text = cls_ConfigCashier.nameWarehouse;
            lbCaBan.Text = DateTime.Now.ToString("dd-MM-yyyy");
            lb_NhanVien.Text = clsGlobal.gsNameOfUserLogin;
            lbtimeby.Text = cls_ConfigCashier.sTenCB + ": " + cls_ConfigCashier.sGioBD + " --> " + cls_ConfigCashier.sGioKT;
            lbNameTable.Text = "";
            lbNameMon.Text = "";

            AddColumnDatatable();
            SetBackgroudOnClick(btn_ThuNgan);
            SetDefautlBackgroud(btn_NghiepVu);
            SetDefautlBackgroud(btn_DatThu);
            pnThuNgan.BringToFront();
            pnShowHang.BringToFront();
            pnBtnDeleteGv.Visible = false;
            pnUpDowGv.Visible = false;

            GetKhuVuc();
            GetSoDoBan();
        }

        private void Frm_BanHang_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                if (funtions == 1 && maBan != "")
                {
                    btn_ThanhToan_Click(sender, e);
                }
            }
            else if (e.KeyCode == Keys.Escape)
            {
                btn_logout_Click(null, null);
            }
        }

        private void LoadPermission()
        {
            //30/08/2016 Thong them cau hinh co dung chuc nang in tam tinh thay khong
            string sIntamtinh = clsUserManagement.CheckCauhinh("INTAMTINH");
            bool bIntamtinh = false;
            if (sIntamtinh != "Error")
            {
                bIntamtinh = sIntamtinh == "1" ? true : false;
            }
            btn_Payment.Enabled = clsUserManagement.AllowAdd("63");
            btn_TamTinh.Enabled = bIntamtinh && clsUserManagement.AllowAdd("63");
            btn_InLaiHoaDon.Enabled = clsUserManagement.AllowEdit("63");
            btnDoigio_nhanphong.Enabled = clsUserManagement.AllowEdit("63");
            btn_LayLaiHoaDon.Enabled = clsUserManagement.AllowView("25101801");
            btn_ExitTable.Enabled = clsUserManagement.AllowDelete("63");
            btn_ExitDishe.Enabled = clsUserManagement.AllowDelete("63");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            string hour, minute, second;
            second = DateTime.Now.Second.ToString();
            hour = DateTime.Now.Hour.ToString();
            minute = DateTime.Now.Minute.ToString();
            if (second.Length < 2)
                lbgiay.Text = "0" + second;
            else
                lbgiay.Text = second;
            if (minute.Length < 2)
                lbGio.Text = hour + ":0" + minute;
            else
                lbGio.Text = hour + ":" + minute;

            timer1.Enabled = true;
        }

        private void AddColumnDatatable()
        {
            dt.Columns.Add("MA_HANGHOA", typeof(Int32));
            dt.Columns.Add("TEN_HANGHOA", Type.GetType("System.String"));
            dt.Columns.Add("SOLUONG", Type.GetType("System.Double"));
            dt.Columns.Add("GIABAN", Type.GetType("System.Double"));
            dt.Columns.Add("GHICHU", Type.GetType("System.String"));
            dt.Columns.Add("MA_BEP", typeof(Int32));
            dt.Columns.Add("THUE", Type.GetType("System.Double"));
            dt.Columns.Add("Thanh_Tien", Type.GetType("System.Double"));

            divide_Table1.Columns.Add("_MA_HANGHOA", typeof(Int32));
            divide_Table1.Columns.Add("_TEN_HANGHOA", Type.GetType("System.String"));
            divide_Table1.Columns.Add("_SOLUONG", Type.GetType("System.Double"));
            divide_Table1.Columns.Add("_GIABAN", Type.GetType("System.Double"));
            divide_Table1.Columns.Add("__MA_BEP", typeof(Int32));
            divide_Table1.Columns.Add("_Thanh_Tien", Type.GetType("System.Double"));

            divide_Table2.Columns.Add("__MA_HANGHOA", typeof(Int32));
            divide_Table2.Columns.Add("__TEN_HANGHOA", Type.GetType("System.String"));
            divide_Table2.Columns.Add("__SOLUONG", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("__GIABAN", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("__MA_BEP", typeof(Int32));
            divide_Table2.Columns.Add("__Thanh_Tien", Type.GetType("System.Double"));
        }

        private void SetBackgroudOnClick(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.YellowGreen;
            _btn.Appearance.BackColor2 = Color.YellowGreen;
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private void SetDefautlBackgroud(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.White;
            if (_btn.Appearance.Options.UseBackColor == true)
                _btn.Appearance.Options.UseBackColor = false;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == false)
                _btn.LookAndFeel.UseDefaultLookAndFeel = true;
        }

        private void GetKhuVuc()
        {
            try
            {
                dtKhuVuc.Rows.Clear();
                string SQLKV = string.Format("Exec SelectKhuVuc_KhachSan @PageNumber={0},@PageSize={1}, @maCH={2}", cPageKV, 7, int.Parse(cls_ConfigCashier.idShop));
                dtKhuVuc = clsMain.ReturnDataTable(SQLKV);
                while (pnKV_Mon.Controls.Count > 0)
                {
                    pnKV_Mon.Controls[0].Dispose();
                }
                pnKV_Mon.Controls.Clear();
                int locateY = 2;
                int locateX = 3;
                for (int i = 0; i < dtKhuVuc.Rows.Count; i++)
                {
                    SimpleButton btnKV = new SimpleButton();
                    btnKV.Name = dtKhuVuc.Rows[i]["MA_KHUVUC"].ToString();
                    if (i == 0)
                    {
                        maKV = int.Parse(dtKhuVuc.Rows[0]["MA_KHUVUC"].ToString());
                        lbKhuVuc.Text = "" + dtKhuVuc.Rows[0]["TEN_KHUVUC"].ToString() + ">";
                        SetBackgroudOnClick(btnKV);
                    }
                    btnKV.Text = dtKhuVuc.Rows[i]["TEN_KHUVUC"].ToString();
                    try
                    {
                        byte[] tam = new byte[((byte[])dtKhuVuc.Rows[i]["HINHANH"]).LongLength];
                        tam = (byte[])dtKhuVuc.Rows[i]["HINHANH"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        btnKV.Image = bm;
                    }
                    catch
                    {
                    }
                    btnKV.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btnKV.ImageLocation = ImageLocation.TopCenter;
                    btnKV.RightToLeft = RightToLeft.No;
                    btnKV.Size = new Size(125, 80);
                    if (i != 0)
                    {
                        locateY += int.Parse(btnKV.Location.Y.ToString()) + 85;
                    }
                    btnKV.Location = new Point(locateX, locateY);
                    btnKV.Click += new EventHandler(btnKV_Click);
                    pnKV_Mon.Controls.Add(btnKV);
                }
            }
            catch
            {
            }
        }

        private void GetSoDoBan()
        {
            pnTable_Mon.Controls.Clear();
            string sql = string.Format("Exec SelectTable @PageNumber={0},@maKV={1}", CurrenPages, maKV);
            dtBan = clsMain.ReturnDataTable(sql);
            while (pnTable_Mon.Controls.Count > 0)
            {
                pnTable_Mon.Controls[0].Dispose();
            }
            for (int i = 0; i < dtBan.Rows.Count; i++)
            {
                SimpleButton btnBan_Mon = new SimpleButton();
                btnBan_Mon.Name = dtBan.Rows[i]["MA_BAN"].ToString();

                if (dtBan.Rows[i]["SOGHE"].ToString() != "0")
                {
                    btnBan_Mon.Text = dtBan.Rows[i]["TEN_BAN"].ToString() + "\n" + dtBan.Rows[i]["SOGHE"].ToString() + "  Giường";
                }
                else
                {
                    btnBan_Mon.Text = dtBan.Rows[i]["TEN_BAN"].ToString();
                }
                try
                {
                    byte[] tam = new byte[((byte[])dtBan.Rows[i]["HINHANH"]).LongLength];
                    tam = (byte[])dtBan.Rows[i]["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    btnBan_Mon.Image = bm;
                }
                catch
                {
                }
                btnBan_Mon.Width = int.Parse(dtBan.Rows[i]["Width"].ToString());
                btnBan_Mon.Height = int.Parse(dtBan.Rows[i]["Height"].ToString());
                btnBan_Mon.Location = new Point(int.Parse(dtBan.Rows[i]["LocaX"].ToString()), int.Parse(dtBan.Rows[i]["LocaY"].ToString()));

                try
                {
                    if (bool.Parse(dtBan.Rows[i]["ISBANDAT"].ToString()) == true)
                    {
                        SetBackgroudOnClick2(btnBan_Mon);
                    }
                }
                catch
                {
                }
                try
                {
                    if (bool.Parse(dtBan.Rows[i]["ISMO_BAN"].ToString()) == true)
                    {
                        SetBackgroudOnClick1(btnBan_Mon);
                        DataTable dataInPut = getGioVaoBan(btnBan_Mon.Name.ToString());
                        if (dataInPut.Rows.Count > 0)
                        {
                            btnBan_Mon.Text += "\n Nhận phòng: " + "\n" + cls_KP_RES.DateToString_Full_NoSecond((DateTime)dataInPut.Rows[0][0]);
                        }
                        string idHD = GetMaHoaDon(btnBan_Mon.Name.ToString());
                        DataTable tb = DemSoBanTrongBill(idHD);
                        if (tb.Rows.Count > 1)
                        {
                            btnBan_Mon.Text += " \n GB " + idHD.Substring(idHD.IndexOf("B") + 1, 4);
                        }
                    }
                }
                catch
                {
                }
                try
                {
                    if (bool.Parse(dtBan.Rows[i]["SEE_PAYMENTS"].ToString()) == true)
                    {
                        SetBackgroudOnClick3(btnBan_Mon);
                    }
                }
                catch
                {
                    dtBan.Rows[i]["SEE_PAYMENTS"] = 0;
                    clsMain.ExecuteSQL("update ban set SEE_PAYMENTS=0 where SEE_PAYMENTS is null");
                }
                try
                {
                    btnBan_Mon.Tag = "0";
                    if (bool.Parse(dtBan.Rows[i]["KHACHRANGOAI"].ToString()) == true)
                    {
                        SetBackgroudOnClick4(btnBan_Mon);
                        btnBan_Mon.Text += "\n Ra ngoài: " + "\n" + string.Format("{0:dd/MM/yyyy HH:mm}", DateTime.Parse(dtBan.Rows[i]["THOIGIANRANGOAI"].ToString()));
                        btnBan_Mon.Tag = "1";
                    }
                }
                catch
                {
                }
                try
                {
                    if (int.Parse(dtBan.Rows[i]["SUACHUA"].ToString()) == 1)
                    {
                        SetBackgroudOnClick5(btnBan_Mon);
                        btnBan_Mon.Text += "\n Đang sửa chữa";
                    }
                }
                catch
                {
                }
                try
                {
                    if (int.Parse(dtBan.Rows[i]["VESINH"].ToString()) == 1)
                    {
                        SetBackgroudOnClick6(btnBan_Mon);
                        btnBan_Mon.Text += "\n Đang dọn phòng";
                    }
                }
                catch
                {
                }

                btnBan_Mon.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btnBan_Mon.ImageLocation = ImageLocation.TopCenter;
                btnBan_Mon.RightToLeft = RightToLeft.No;
                btnBan_Mon.Click += new EventHandler(btnBan_Mon_Click);
                pnTable_Mon.Controls.Add(btnBan_Mon);
            }
        }

        private void SetBackgroudOnClick1(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.FromArgb(229, 137, 45);
            _btn.Appearance.BackColor2 = Color.FromArgb(229, 137, 45);
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private void SetBackgroudOnClick2(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.FromArgb(0, 173, 239);
            _btn.Appearance.BackColor2 = Color.FromArgb(0, 173, 239);
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private void SetBackgroudOnClick3(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.FromArgb(214, 223, 35);
            _btn.Appearance.BackColor2 = Color.FromArgb(214, 223, 35);
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private void SetBackgroudOnClick4(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.FromArgb(202, 32, 217);
            _btn.Appearance.BackColor2 = Color.FromArgb(202, 32, 217);
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private void SetBackgroudOnClick5(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.FromArgb(68, 44, 41);
            _btn.Appearance.BackColor2 = Color.FromArgb(68, 44, 41);
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private void SetBackgroudOnClick6(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.FromArgb(79, 121, 66);
            _btn.Appearance.BackColor2 = Color.FromArgb(79, 121, 66);
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private string GetMaHoaDon(string idTable)
        {
            string sSQL = "";
            sSQL += "Select a.MA_HOADON" + "\n";
            sSQL += "From CT_MOBAN_KHACHSAN a,HOADON_KHACHSAN b,BAN c" + "\n";
            sSQL += "where a.MA_HOADON = b.MA_HOADON And a.MA_BAN=c.MA_BAN  And ISNULL(b.ISBANDAT,0) = 0" + "\n";
            sSQL += "And ISMO_BAN=1 And b.ISPAYMENT = 0 And a.MA_BAN = " + clsMain.SQLString(idTable) + "\n";
            sSQL += "Order by a.MA_HOADON desc";
            DataTable dmBAN = clsMain.ReturnDataTable(sSQL);
            if (dmBAN.Rows.Count > 0)
            {
                return dmBAN.Rows[0]["MA_HOADON"].ToString();
            }
            else
            {
                sSQL = "";
                sSQL += "Select a.MA_HOADON " + "\n";
                sSQL += "From CT_MOBAN_KHACHSAN as a,HOADON_KHACHSAN as b,BAN c" + "\n";
                sSQL += "Where a.MA_HOADON = b.MA_HOADON And a.MA_BAN=c.MA_BAN And b.ISBANDAT=1" + "\n";
                sSQL += "And ISMO_BAN=1 And b.ISPAYMENT = 0 And a.MA_BAN = " + clsMain.SQLString(idTable) + "\n";
                sSQL += "Order by CHECKIN ASC";
                dmBAN = clsMain.ReturnDataTable(sSQL);
                if (dmBAN.Rows.Count > 0)
                    return dmBAN.Rows[0]["MA_HOADON"].ToString();
                else
                    return "";
            }
        }

        private DataTable getGioVaoBan(string maBan)
        {
            string sSQL = string.Empty;
            sSQL += "EXEC SP_ThongtinPhong_Select @MABAN=" + clsMain.SQLString(maBan);
            DataTable dtTime = clsMain.ReturnDataTable(sSQL);
            return dtTime;
        }

        private DataTable DemSoBanTrongBill(string idBill)
        {
            DataTable dttabless;
            string sql = "select MA_BAN from CT_MOBAN_KHACHSAN where MA_HOADON ='" + idBill + "'";
            dttabless = clsMain.ReturnDataTable(sql);
            return dttabless;
        }

        private void btn_UpKV_Click(object sender, EventArgs e)
        {
            if (funtions == 1)
            {
                if (cPageKV > 1)
                {
                    cPageKV -= 1;
                    GetKhuVuc();
                    GetSoDoBan();
                }
            }
            if (funtions == 2)
            {
                if (cPageDish > 1)
                {
                    cPageDish -= 1;
                    GetNhomHang();
                    GetListMon();
                }
            }
        }

        private void btn_DowKV_Click(object sender, EventArgs e)
        {
            if (funtions == 1)
            {
                if (cPageKV < 20)
                {
                    cPageKV += 1;
                    GetKhuVuc();
                    GetSoDoBan();
                }
            }
            if (funtions == 2)
            {
                if (cPageDish < 20)
                {
                    cPageDish += 1;
                    GetNhomHang();
                    GetListMon();
                }
            }
        }

        private void btn_NextLeft_Click(object sender, EventArgs e)
        {
            if (funtions == 1)
            {
                if (CurrenPages == 1)
                    return;
                CurrenPages = CurrenPages - 1;
                lbPageTable.Text = CurrenPages.ToString();
                GetSoDoBan();
            }
            if (funtions == 2)
            {
                if (cPageMon == 1)
                    return;
                cPageMon = cPageMon - 1;
                lbPageTable.Text = cPageMon.ToString();
                GetListMon();
            }
        }

        private void btn_NextRight_Click(object sender, EventArgs e)
        {
            if (funtions == 1)
            {
                if (CurrenPages == 5)
                    return;
                CurrenPages = CurrenPages + 1;
                lbPageTable.Text = CurrenPages.ToString();
                GetSoDoBan();
            }
            if (funtions == 2)
            {
                if (cPageMon == 20)
                    return;
                cPageMon = cPageMon + 1;
                lbPageTable.Text = cPageMon.ToString();
                GetListMon();
            }
        }

        private void GetNhomHang()
        {
            string SQLMON = string.Format("exec SelectDish @PageNumber={0},@PageSize={1},@maQUAY={2}", cPageDish, 7, cls_ConfigCashier.idCashier);
            dtNhomhang = clsMain.ReturnDataTable(SQLMON);
            while (pnKV_Mon.Controls.Count > 0)
            {
                pnKV_Mon.Controls[0].Dispose();
            }
            pnKV_Mon.Controls.Clear();
            int locateY = 2;
            int locateX = 3;
            for (int i = 0; i < dtNhomhang.Rows.Count; i++)
            {
                SimpleButton btnKV = new SimpleButton();
                btnKV.Name = dtNhomhang.Rows[i]["MA_NHOMHANG"].ToString();
                if (i == 0)
                {
                    maNH = int.Parse(dtNhomhang.Rows[0]["MA_NHOMHANG"].ToString());
                    SetBackgroudOnClick(btnKV);
                }
                btnKV.Text = "" + dtNhomhang.Rows[i]["TEN_NHOMHANG"].ToString();
                try
                {
                    byte[] tam = new byte[((byte[])dtNhomhang.Rows[i]["HINHANH"]).LongLength];
                    tam = (byte[])dtNhomhang.Rows[i]["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    btnKV.Image = bm;
                }
                catch
                {
                }
                btnKV.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btnKV.ImageLocation = ImageLocation.TopCenter;
                btnKV.RightToLeft = RightToLeft.No;
                btnKV.Size = new Size(125, 80);
                if (i != 0)
                {
                    locateY += int.Parse(btnKV.Location.Y.ToString()) + 85;
                }
                btnKV.Location = new Point(locateX, locateY);
                btnKV.Click += new EventHandler(btnKV_Click);
                pnKV_Mon.Controls.Add(btnKV);
            }
        }

        private void GetListMon()
        {
            int width = (pnTable_Mon.Width / 110);
            int height = (pnTable_Mon.Height / 80);
            sizeMon = width * height;
            string sql = string.Format("Exec SelectMON @PageNumber={0},@PageSize={1}, @maNH={2}, @maKV={3},@maCH={4}", cPageMon, sizeMon, maNH, maKV, cls_ConfigCashier.idShop);
            dtMon = clsMain.ReturnDataTable(sql);
            while (pnTable_Mon.Controls.Count > 0)
            {
                pnTable_Mon.Controls[0].Dispose();
            }
            pnTable_Mon.Controls.Clear();
            int count = (pnTable_Mon.Width / 120) + 1;
            int line = 0;
            int locateY = 2;
            int locateX = 2;
            int x = 0;
            int y = 0;
            for (int i = 0; i < dtMon.Rows.Count; i++)
            {
                line++;
                SimpleButton btnBan_Mon = new SimpleButton();
                btnBan_Mon.Name = dtMon.Rows[i]["MA_HANGHOA"].ToString();
                btnBan_Mon.Text = dtMon.Rows[i]["TEN_HANGHOA"].ToString() + "\n" + "Giá:" + string.Format("{0:0,0 }", double.Parse(dtMon.Rows[i]["GIABAN"].ToString())) + "";
                btnBan_Mon.Tag = dtMon.Rows[i]["TEN_HANGHOA"].ToString() + " - " + "Giá:" + string.Format("{0:0,0 }", double.Parse(dtMon.Rows[i]["GIABAN"].ToString())) + "";
                try
                {
                    byte[] tam = new byte[((byte[])dtMon.Rows[i]["HINHANH"]).LongLength];
                    tam = (byte[])dtMon.Rows[i]["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    btnBan_Mon.Image = bm;
                }
                catch
                {
                }
                btnBan_Mon.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btnBan_Mon.ImageLocation = ImageLocation.TopCenter;
                btnBan_Mon.RightToLeft = RightToLeft.No;
                btnBan_Mon.Size = new Size(110, 80);
                if (line == count)
                {
                    locateY += int.Parse(btnBan_Mon.Location.Y.ToString()) + 85;
                    locateX = 2;
                    line = 1;
                }
                else
                {
                    if (i != 0)
                    {
                        if (this.Width < 1100)
                        {
                            locateX += int.Parse(btnBan_Mon.Location.X.ToString()) + 132;
                        }
                        else
                        {
                            locateX += int.Parse(btnBan_Mon.Location.X.ToString()) + 124;
                        }
                    }
                }
                btnBan_Mon.Location = new Point(locateX, locateY);
                btnBan_Mon.Click += new EventHandler(btnBan_Mon_Click);
                pnTable_Mon.Controls.Add(btnBan_Mon);
            }
        }

        private void btnKV_Click(object sender, EventArgs e)
        {
            CurrenPages = 1;
            cPageMon = 1;
            lbPageTable.Text = CurrenPages.ToString();
            foreach (SimpleButton btnKV in pnKV_Mon.Controls)
            {
                if (btnKV.Name == this.ActiveControl.Name)
                {
                    SetBackgroudOnClick(btnKV);
                    if (funtions == 1)
                    {
                        funtions = 1;
                        lbNameMon.Text = "";
                        lbNameTable.Text = "";
                        lbKhuVuc.Text = btnKV.Text + "  >  ";
                        maBan = "";
                        maKV = int.Parse(btnKV.Name.ToString());
                        GetSoDoBan();
                    }

                    //if (funtions == 1 || selectDishes == 2)
                    //{
                    //    // set funtion = 1 have to load Table
                    //    funtions = 1;
                    //    lbNameMon.Text = "";
                    //    lbNameTable.Text = "";
                    //    lbKhuVuc.Text = btnKV.Text + "  >  ";
                    //    maBan = "";
                    //    maKV = int.Parse(btnKV.Name.ToString());
                    //    GetSoDoBan();
                    //    if (selectDishes == 2)
                    //        funtions = 9;
                    //}

                    if (funtions == 6)
                    {
                        funtions = 1;
                        lbNameMon.Text = "";
                        lbNameTable.Text = "";
                        lbKhuVuc.Text = btnKV.Text + "  >  ";
                        maBan = "";
                        maKV = int.Parse(btnKV.Name.ToString());
                        GetSoDoBan();
                        funtions = 6;
                    }
                    if (funtions == 11)
                    {
                        funtions = 1;
                        lbNameMon.Text = "";
                        lbNameTable.Text = "";
                        lbKhuVuc.Text = btnKV.Text + "  >  ";
                        maKV = int.Parse(btnKV.Name.ToString());
                        GetSoDoBan();
                        funtions = 11;
                    }

                    if (funtions == 2 || selectDishes == 1 || fastFoot == 2)
                    {
                        // set funtion = 2 have to Laoding Dishes
                        funtions = 2;
                        maNH = int.Parse(btnKV.Name.ToString());
                        GetListMon();
                        if (selectDishes == 1)
                            funtions = 9;
                        if (fastFoot == 2)
                            funtions = 8;
                    }
                }
                else
                {
                    SetDefautlBackgroud(btnKV);
                }
            }
        }

        private void btnBan_Mon_Click(object sender, EventArgs e)
        {
            try
            {
                pnBtnDeleteGv.Visible = false;
                foreach (SimpleButton btnBan_Mon in pnTable_Mon.Controls)
                {
                    if (btnBan_Mon.Name == this.ActiveControl.Name)
                    {
                        SetBackgroudOnClick(btnBan_Mon);
                        if (funtions == 1)
                        {
                            SetBackgroudOnClick(btn_ThuNgan);
                            SetDefautlBackgroud(btn_NghiepVu);
                            SetDefautlBackgroud(btn_DatThu);
                            pnThuNgan.BringToFront();
                            sendOrder = 0;
                            maBan = btnBan_Mon.Name;

                            // 27/07 khang tìm loại phòng theo MA_BAN
                            string sqlLoaiphong = "";
                            DataTable dtLoaiphong;
                            string sTenloaiphong = "";
                            sqlLoaiphong = "Select MALOAIPHONG, TENLOAIPHONG,SUACHUA From KS_LOAIPHONG LP, BAN B Where LP.MALOAIPHONG=B.MA_LOAIPHONG and B.MA_BAN=" + btnBan_Mon.Name;
                            dtLoaiphong = clsMain.ReturnDataTable(sqlLoaiphong);
                            sTenloaiphong = dtLoaiphong.Rows[0]["TENLOAIPHONG"].ToString();

                            //Khóa phòng 
                            if (dtLoaiphong.Rows[0]["SUACHUA"].ToString() == "1")
                            {
                                btnKhoaphong .Text = "Bỏ khóa phòng";
                                btnKhoaphong.Tag = "1";
                            }
                            else
                            {
                                btnKhoaphong.Text = "Khóa phòng";
                                btnKhoaphong.Tag = "0";
                            }

                            //25/09/2018- Xuân-Hiển thị tên khách hàng.
                            string idBill1 = GetMaHoaDon(maBan);
                            string sqlTenkh = "";
                            DataTable dtTenkh;
                            string sTenkh = "";
                            if (idBill1 != "")
                            {
                                sqlTenkh = "SELECT TEN FROM dbo.KS_KHACHHANG kh, dbo.CT_TTKHACHHANG_KHACHSAN ctkh WHERE ctkh.MA_KH=kh.MA_KH AND ctkh.MA_HOADON='" + idBill1 + "' AND ctkh.MA_BAN=" + btnBan_Mon.Name;
                                dtTenkh = clsMain.ReturnDataTable(sqlTenkh);
                                sTenkh = dtTenkh.Rows[0]["TEN"].ToString();

                            }

                            if (btnBan_Mon.Text.IndexOf("\n") > 0)
                            {
                                lbNameTable.Text = btnBan_Mon.Text.Substring(0, btnBan_Mon.Text.IndexOf("\n"));
                                lbNameTable.Text += " (" + sTenloaiphong + ") ";
                                lbNameTable.Tag = dtLoaiphong.Rows[0]["MALOAIPHONG"].ToString();
                                lbTableTatol.Text = sTenkh + "\n" + btnBan_Mon.Text.Substring(0, btnBan_Mon.Text.IndexOf("\n"));
                              
                            }
                            else
                            {
                                lbNameTable.Text = btnBan_Mon.Text;
                                lbNameTable.Text += " (" + sTenloaiphong + ") ";
                                lbNameTable.Tag = dtLoaiphong.Rows[0]["MALOAIPHONG"].ToString();
                                lbTableTatol.Text = sTenkh + "\n" + btnBan_Mon.Text;
                              
                            }
                            lbNameMon.Text = "";
                            //Tính giờ
                            DataTable dataInPut = getGioVaoBan(maBan);
                            if (dataInPut.Rows.Count > 0)
                            {
                                if (dataInPut.Rows[0]["CHECKIN"].ToString() != "")
                                {
                                    string _ngayden = string.Format("{0:yyyyMMdd HH:mm:ss}", DateTime.Parse(dataInPut.Rows[0]["CHECKIN"].ToString()));
                                    string _ngaydi = string.Format("{0:yyyyMMdd HH:mm:ss}", DateTime.Parse(dataInPut.Rows[0]["CHECKOUT"].ToString()));
                                    _checkin = DateTime.Parse(dataInPut.Rows[0]["CHECKIN"].ToString());
                                    _checkout = DateTime.Parse(dataInPut.Rows[0]["CHECKOUT"].ToString());
                                    string sSQL = string.Format("Exec TinhTienphong_Khachsan3 @NGAYDEN='{0}',@NGAYDI='{1}',@LOAIPHONG='{2}',@SONGUOI='{3}'", string.Format("{0:dd/MM/yyyy HH:mm:ss}", DateTime.Parse(dataInPut.Rows[0]["CHECKIN"].ToString())), string.Format("{0:dd/MM/yyyy HH:mm:ss}", DateTime.Parse(dataInPut.Rows[0]["CHECKOUT"].ToString())), dataInPut.Rows[0]["MA_LOAIPHONG"].ToString(), dataInPut.Rows[0]["SONGUOI"].ToString());
                                    dtTiengio = clsMain.ReturnDataTable(sSQL);
                                    if (dtTiengio.Rows.Count > 0)
                                    {
                                        lbTiengio.Text = string.Format("{0:#,###0}", float.Parse(dtTiengio.Rows[0]["GIA"].ToString()));
                                    }
                                    else
                                        lbTiengio.Text = "0";
                                }
                            }
                            else
                            {
                                lbTiengio.Text = "0";
                            }
                            //load hang hoa 
                            string maHOADON = ""; ;
                            if (CheckOpenTable(maBan) == true)
                            {
                                maHOADON = GetMaHoaDon(maBan);
                                selectTable = 1;
                                lbTatol.Text = lbDichvu.Text = "0";
                                GetDishOfTable(maHOADON);
                            }
                            else
                            {
                                lbTatol.Text = lbDichvu.Text = "0";
                                if (gv_SelectProduct.RowCount > 0)
                                {
                                    dt.Rows.Clear();
                                    dtHangOrder.Rows.Clear();
                                    gr_SelectProduct.RefreshDataSource();
                                }
                            }
                            //Set mau nut
                            if (maHOADON != "")
                            {
                                DataTable dtcheck = clsMain.ReturnDataTable("select MA_BAN from CT_MOBAN_KHACHSAN where MA_HOADON='" + maHOADON + "'");
                                if (dtcheck.Rows.Count > 1)
                                {
                                    for (int i = 0; i < dtcheck.Rows.Count; i++)
                                    {
                                        foreach (SimpleButton btnTable in pnTable_Mon.Controls)
                                            if (btnTable.Name == dtcheck.Rows[i]["MA_BAN"].ToString())
                                                SetBackgroudOnClick(btnTable);
                                    }
                                }
                            }
                            //Khách ra / vào phòng
                            if (btnBan_Mon.Tag == "1")
                            {
                                btnKhachrangoai.Text = "Khách Vào Phòng";
                            }
                            else
                            {
                                btnKhachrangoai.Text = "Khách Ra Ngoài";
                            }
                        }
                        //danh sach mon
                        if (funtions == 2)
                        {
                            sendOrder = 1;
                            if (selectTable == 1)
                            {
                                selectTable = 0;
                                dt.Rows.Clear();
                                gr_SelectProduct.RefreshDataSource();
                            }
                            maMon = btnBan_Mon.Name;
                            lbNameMon.Text = " >   " + btnBan_Mon.Text;
                            GetDishGoToBuy(maMon, maKV);
                        }
                        //ghep ban
                        if (funtions == 5)
                        {
                            maBanGhep = btnBan_Mon.Name;
                            string idHD = GetMaHoaDon(maBan);
                            string idTableJoin = GetMaHoaDon(maBanGhep);
                            if (maBan == maBanGhep || idHD == idTableJoin)
                            {
                                return;
                            }
                            funtions = 1;
                            lbNameMon.Text = " > " + btnBan_Mon.Text;
                            SetJoinTable(maBan, maBanGhep);
                            GetDishOfTable(idHD);

                            SetDefautlBackgroud(btn_GhepBan);
                            maBanGhep = "";
                            lbListTable.Text = "Sơ đồ phòng";
                            btn_GhepBan.Text = "Ghép phòng";
                            SetDefautlBackgroud(btn_GhepBan);
                            GetSoDoBan();
                        }
                        //tach ban
                        if (funtions == 6)
                        {
                            maBanGhep = btnBan_Mon.Name;
                            txtmaKVBanGhep.Text = lbKhuVuc.Text;
                            txtmaBanGhep.Text = btnBan_Mon.Text;
                            if (dt.Rows.Count > 0 && maBanGhep != "")
                            {
                                List<DM_ListDisheTableOne> lisHangHoa = new List<DM_ListDisheTableOne>();
                                //add gridview
                                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                                {
                                    DM_ListDisheTableOne dmHangHoa = new DM_ListDisheTableOne();
                                    dmHangHoa._MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                                    dmHangHoa._TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                                    dmHangHoa._SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                                    dmHangHoa._GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                                    dmHangHoa._Thanh_Tien = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"]).ToString());
                                    dmHangHoa._GHICHU = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"]).ToString();
                                    lisHangHoa.Add(dmHangHoa);
                                }
                                convert cvrt = new convert();
                                divide_Table1 = cvrt.ConvertToDataTable(lisHangHoa);
                                gr_divide_Table_1.DataSource = divide_Table1;
                                gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.RowCount - 1;
                                //
                                pnHuymon.BringToFront();
                                //Inbox Table
                                int w = (pn_GroupTable.Width / 2) - pnUpDow.Width;
                                pn_TableTow.Width = w + 30;
                                pn_TableShare.Width = w + 30;
                            }
                            else
                            {
                            }
                        }
                        //Chọn bàn chuyển tới khi Chuyển bàn
                        if (funtions == 11)
                        {
                            maBanGhep = btnBan_Mon.Name;
                            if (maBanGhep == maBan)
                            {
                                return;
                            }
                            if (CheckOpenTable(maBanGhep))
                            {
                                return;
                            }
                            if (ktrasuachua(maBanGhep) == true)
                            {
                                XtraMessageBox.Show("Phòng đang tạm khóa!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                            txtmaKVBanGhep.Text = lbKhuVuc.Text;
                            txtmaBanGhep.Text = btnBan_Mon.Text;
                            string mabep = "";
                            string bangoc = "";
                            string banchuyen = "";
                            string sSQL = "";
                            sSQL = "";
                            sSQL += "Select TEN_BAN" + "\n";
                            sSQL += "From BAN" + "\n";
                            sSQL += "Where MA_BAN=" + clsMain.SQLString(maBan);
                            DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                            bangoc = dt1.Rows[0]["TEN_BAN"].ToString();

                            sSQL = "";
                            sSQL += "Select TEN_BAN" + "\n";
                            sSQL += "From BAN" + "\n";
                            sSQL += "Where MA_BAN=" + clsMain.SQLString(maBanGhep);
                            dt1 = clsMain.ReturnDataTable(sSQL);
                            banchuyen = dt1.Rows[0]["TEN_BAN"].ToString();

                            if (CheckOpenTable(maBanGhep))
                            {
                                string maHD = GetMaHoaDon(maBan);
                                string maHD_banghep = GetMaHoaDon(maBanGhep);
                                sSQL = "";
                                sSQL += "Select MA_HANGHOA as MA,SOLUONG,GIABAN,THUE,TRANGTHAI" + "\n";
                                sSQL += "From CT_HOADON_KHACHSAN" + "\n";
                                sSQL += "Where MA_HOADON =" + clsMain.SQLString(maHD);
                                dt1 = clsMain.ReturnDataTable(sSQL);
                                int n = dt1.Rows.Count;
                                for (int i = 0; i < n; i++)
                                {
                                    if (!ChecBishInTable(maHD_banghep, dt1.Rows[i]["MA"].ToString()))
                                    {
                                        sSQL = "";
                                        sSQL += "Insert Into CT_HOADON_KHACHSAN(MA_HOADON,MA_HANGHOA,SOLUONG,GIABAN,THUE,TRANGTHAI)" + "\n";
                                        sSQL += "Values(";
                                        sSQL += clsMain.SQLString(maHD_banghep) + ",";
                                        sSQL += clsMain.SQLString(dt1.Rows[i]["MA"].ToString()) + ",";
                                        sSQL += clsMain.SQLString(dt1.Rows[i]["SOLUONG"].ToString()) + ",";
                                        sSQL += clsMain.SQLString(dt1.Rows[i]["GIABAN"].ToString()) + ",";
                                        sSQL += clsMain.SQLString(dt1.Rows[i]["THUE"].ToString()) + ",";
                                        sSQL += clsMain.SQLBit(bool.Parse(dt1.Rows[i]["TRANGTHAI"].ToString())) + ")";
                                        clsMain.ExecuteSQL(sSQL);
                                    }
                                    else
                                    {
                                        sSQL = "";
                                        sSQL += "UPDATE CT_HOADON_KHACHSAN SET" + "\n";
                                        sSQL += "SOLUONG +=" + clsMain.SQLString(dt1.Rows[i]["SOLUONG"].ToString()) + "\n";
                                        sSQL += "WHERE MA_HOADON =" + clsMain.SQLString(maHD_banghep) + "\n";
                                        sSQL += "And MA_HANGHOA =" + clsMain.SQLString(dt1.Rows[i]["MA"].ToString());
                                        clsMain.ExecuteSQL(sSQL);
                                    }
                                    CanceTable(maBan);
                                    ExitDivideTable();
                                }
                            }
                            else
                            {
                                string sql = "Update BAN SET ISMO_BAN=1 where MA_BAN =" + int.Parse(maBanGhep) + "";
                                clsMain.ExecuteSQL(sql);
                                string maHD = GetMaHoaDon(maBan);
                                string sqlCTMOBAN = " Update CT_MOBAN_KHACHSAN SET MA_BAN =" + int.Parse(maBanGhep) + " where MA_HOADON='" + maHD + "'";
                                clsMain.ExecuteSQL(sqlCTMOBAN);
                                string sqlCTKHACHHANG = " Update CT_TTKHACHHANG_KHACHSAN SET MA_BAN =" + int.Parse(maBanGhep) + " where MA_HOADON='" + maHD + "'";
                                clsMain.ExecuteSQL(sqlCTKHACHHANG);
                                string sqlpm = "Update BAN SET ISMO_BAN=0 where MA_BAN =" + int.Parse(maBan) + "";
                                clsMain.ExecuteSQL(sqlpm);
                                ExitDivideTable();
                                divide_Table1.Rows.Clear();
                                divide_Table2.Rows.Clear();
                                foreach (SimpleButton btn in pnNghiepVu.Controls)
                                {
                                    btn.Enabled = true;
                                }
                                foreach (SimpleButton btn in pnThuNgan.Controls)
                                {
                                    btn.Enabled = true;
                                }
                            }
                            if (cls_ConfigCashier.sInbep_Chuyenban == "1")
                            {
                                sSQL = "Select MAYINBEP From DM_BEP Where MA_CUAHANG=" + clsMain.SQLString(cls_ConfigCashier.idShop);
                                DataTable dtbep = clsMain.ReturnDataTable(sSQL);
                                string print = "";
                                int m = dtbep.Rows.Count;
                                for (int i = 0; i < m; i++)
                                {
                                    print = dtbep.Rows[i]["MAYINBEP"].ToString();
                                    DataTable dt = new DataTable();
                                    dt.Columns.Add("TITLE");
                                    dt.Columns.Add("BANGOC");
                                    dt.Columns.Add("BANCHUYEN");
                                    dt.Columns.Add("GIUA");
                                    dt.Rows.Add("Chuyển Bàn", bangoc, banchuyen, "chuyển qua");
                                    KP_Report.Frm_Report1 frm = new KP_Report.Frm_Report1();
                                    frm.ReportName = "rpt_Chuyenban";
                                    frm.DataSource = dt;
                                    frm.WindowState = FormWindowState.Minimized;
                                    frm.PrinterName = print;
                                    frm.IsPrint = true;
                                    frm.ShowDialog();
                                    frm.Dispose();
                                }
                            }
                            return;
                        }
                    }
                    else
                    {
                        // Set backgroud  color Table IS OPENed
                        if (funtions == 1 || funtions == 5)
                        {
                            SetDefautlBackgroud(btnBan_Mon);
                            for (int i = 0; i < dtBan.Rows.Count; i++)
                            {
                                if (bool.Parse(dtBan.Rows[i]["ISBANDAT"].ToString()) == true && btnBan_Mon.Name == dtBan.Rows[i]["MA_BAN"].ToString())
                                {
                                    SetBackgroudOnClick2(btnBan_Mon);
                                }
                                if (bool.Parse(dtBan.Rows[i]["ISMO_BAN"].ToString()) == true && btnBan_Mon.Name == dtBan.Rows[i]["MA_BAN"].ToString())
                                {
                                    SetBackgroudOnClick1(btnBan_Mon);
                                }
                                if (bool.Parse(dtBan.Rows[i]["SEE_PAYMENTS"].ToString()) == true && btnBan_Mon.Name == dtBan.Rows[i]["MA_BAN"].ToString())
                                {
                                    SetBackgroudOnClick3(btnBan_Mon);
                                }
                                if (bool.Parse(dtBan.Rows[i]["KHACHRANGOAI"].ToString()) == true && btnBan_Mon.Name == dtBan.Rows[i]["MA_BAN"].ToString())
                                {
                                    SetBackgroudOnClick4(btnBan_Mon);
                                }
                                if (int.Parse(dtBan.Rows[i]["SUACHUA"].ToString()) == 1 && btnBan_Mon.Name == dtBan.Rows[i]["MA_BAN"].ToString())
                                {
                                    SetBackgroudOnClick5(btnBan_Mon);
                                }
                                if (int.Parse(dtBan.Rows[i]["VESINH"].ToString()) == 1 && btnBan_Mon.Name == dtBan.Rows[i]["MA_BAN"].ToString())
                                {
                                    SetBackgroudOnClick6(btnBan_Mon);
                                }
                            }
                        }
                        else
                            SetDefautlBackgroud(btnBan_Mon);
                    }
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK);
            }
        }

        private void ExitDivideTable()
        {
            sendOrder = 0;
            funtions = 1;
            exitReprint = false;
            btn_SoDoBan.Text = "Mở Phòng";
            lbListTable.Text = "Danh sách phòng:";
            btnTachPhong.Text = "Tách Phòng";
            btnChuyenBan.Text = "Chuyển Phòng";
            maBan = "";
            maBanGhep = "";
            txtmaBanGhep.Text = "";
            txtmaKVBanGhep.Text = "";
            lbTiengio.Text = "0";
            divide_Table1.Rows.Clear();
            divide_Table2.Rows.Clear();
            SetDefaultBackgroundBtnInPn(pnThuNgan);
            SetDefaultBackgroundBtnInPn(pnNghiepVu);
            foreach (SimpleButton btn in pnThuNgan.Controls)
            {
                btn.Visible = true;
            }
            foreach (SimpleButton btn in pnNghiepVu.Controls)
            {
                btn.Visible = true;
            }
            pnShowHang.BringToFront();
            GetKhuVuc();
            GetSoDoBan();
            string maHD = GetMaHoaDon(maBan);
            GetDishOfTable(maHD);
        }

        public bool CheckOpenTable(string idTable)
        {
            string sql = "select ISMO_BAN from BAN where MA_BAN =" + clsMain.SQLString(idTable) + "";
            DataTable checkTable = clsMain.ReturnDataTable(sql);
            if (checkTable.Rows.Count > 0 && bool.Parse(checkTable.Rows[0]["ISMO_BAN"].ToString()))
            {
                return true;
            }
            else
                return false;
        }

        private void GetDishOfTable(string maHoaDon)
        {
            dt.Rows.Clear();
            string sSQL = "";
            sSQL += "Select cthd.MA_HANGHOA, hh.TEN_HANGHOA, cthd.SOLUONG,  cthd.GIABAN,  'a' as GHICHU,hh.MA_BEP, cthd.THUE, 0 as Thanh_Tien, hd.GIAMGIA, hd.PHIPHUCVU ";
            sSQL += "From CT_HOADON_KHACHSAN as cthd inner join HOADON_KHACHSAN hd on cthd.MA_HOADON = hd.MA_HOADON inner join HANGHOA as hh on cthd.MA_HANGHOA=hh.MA_HANGHOA where cthd.MA_HOADON ='" + maHoaDon + "'";
            dt = clsMain.ReturnDataTable(sSQL);
            dtHangOrder = clsMain.ReturnDataTable(sSQL);
            gr_SelectProduct.DataSource = dt;
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["PHIPHUCVU"].ToString() != "")
                    lbPhuThu.Text = string.Format("{0:#,###0}", float.Parse(dt.Rows[0]["PHIPHUCVU"].ToString()));
                if (dt.Rows[0]["GIAMGIA"].ToString() != "")
                    lbGiamGia.Text = string.Format("{0:#,###0}", float.Parse(dt.Rows[0]["GIAMGIA"].ToString()));
            }
            if (gv_SelectProduct.RowCount > 0)
            {
                TotalMoney();
            }
            else
            {
                lbDichvu.Text = "0";
                lbTatol.Text = lbTiengio.Text;
            }
            gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.RowCount - 1;
        }

        private void TotalMoney()
        {
            string sqlc = " select GIATRI from CAUHINH where TEN='GIABANTRUOCTHUE' ";
            DataTable checkVAT = clsMain.ReturnDataTable(sqlc);
            bool kt = false;
            if (checkVAT.Rows[0]["GIATRI"].ToString() == "1")
                kt = true;

            double tatol = 0;
            for (int i = 0; i < gv_SelectProduct.RowCount; i++)
            {
                double thanhTien = 0, vat = 0;
                double sl = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                double gia = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                thanhTien = sl * gia;
                if (kt == true)
                    vat = (double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["THUE"]).ToString()) / 100) * thanhTien;
                tatol += thanhTien + vat;
                gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"], thanhTien + vat);
            }
            lbDichvu.Text = string.Format("{0:0,0}", tatol);

            tatol = tatol + double.Parse(lbTiengio.Text);
            lbTatol.Text = string.Format("{0:0,0}", tatol);
        }

        private void btn_DowLast_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.RowCount - 1;
                btn_UpFirst.Enabled = true;
                btn_up.Enabled = true;
                btn_Dow.Enabled = false;
                btn_DowLast.Enabled = false;
            }
            catch
            {
            }
        }

        private void btn_Dow_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.FocusedRowHandle + 1;
                btn_up.Enabled = true;
                btn_UpFirst.Enabled = true;
                if (gv_SelectProduct.FocusedRowHandle == gv_SelectProduct.RowCount - 1)
                {
                    btn_Dow.Enabled = false;
                    btn_DowLast.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btn_up_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.FocusedRowHandle - 1;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
                if (gv_SelectProduct.FocusedRowHandle == 0)
                {
                    btn_up.Enabled = false;
                    btn_UpFirst.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btn_UpFirst_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = 0;
                btn_UpFirst.Enabled = false;
                btn_up.Enabled = false;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
            }
            catch
            {
            }
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            try
            {
                if (Flag_soluong)
                {
                    lbSoluong.Text = btn1.Text;
                    Flag_soluong = false;
                }
                else
                {
                    if (lbSoluong.Text.Length < 3)
                        lbSoluong.Text += btn0.Text;
                }
                int soluong = int.Parse(lbSoluong.Text);
                gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"], soluong);
                TotalMoney();
            }
            catch
            {
            }
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            try
            {
                SimpleButton btn = (SimpleButton)sender;
                if (Flag_soluong)
                {
                    lbSoluong.Text = btn.Text;
                    Flag_soluong = false;
                }
                else
                {
                    if (lbSoluong.Text.Length < 3)
                        lbSoluong.Text += btn.Text;
                }

                int soluong = int.Parse(lbSoluong.Text);
                gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"], soluong);
                TotalMoney();
            }
            catch
            {
            }
        }

        private void btn_xoa_Click(object sender, EventArgs e)
        {
            gv_SelectProduct.DeleteSelectedRows();
            TotalMoney();
            if (gv_SelectProduct.RowCount <= 0)
                pnBtnDeleteGv.Visible = false;
            else
                lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
        }

        private void btn_XoaTatCa_Click(object sender, EventArgs e)
        {
            dt.Rows.Clear();
            DataTable s = new DataTable();
            s = dt;
            pnBtnDeleteGv.Visible = false;
        }

        private void btn_tru_Click(object sender, EventArgs e)
        {
            try
            {
                int soluong = int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                if (soluong > 1)
                    gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"], soluong - 1);
                TotalMoney();
            }
            catch
            {
            }
        }

        private void btn_cong_Click(object sender, EventArgs e)
        {
            try
            {
                int soluong = int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"], soluong + 1);
                TotalMoney();
            }
            catch
            {
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            pnBtnDeleteGv.Visible = false;
        }

        private void gv_SelectProduct_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == SOTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
            if (gv_SelectProduct.RowCount > 0)
            {
                lb_RowsCount.Text = gv_SelectProduct.RowCount.ToString();
                lb_STT.Text = (gv_SelectProduct.FocusedRowHandle + 1).ToString();
            }
            if (1 < gv_SelectProduct.FocusedRowHandle + 1 && gv_SelectProduct.FocusedRowHandle + 1 <= gv_SelectProduct.RowCount - 1)
            {
                btn_up.Enabled = true;
                btn_UpFirst.Enabled = true;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
            }
        }

        private void gv_SelectProduct_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (funtions == 10)
                return;
            XuLyGv();
            maMon = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();

            #region Kiểm tra hàng là thời giá hay ko,nếu đúng thì ẩn button Giá bán 2 đi
            xGIABAN = float.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["GIABAN"]).ToString());
            string sSQL = "";
            sSQL = "select ISNULL(SUAGIA,0) AS SUAGIA from HANGHOA where MA_HANGHOA = " + maMon;
            DataTable dtcheck = clsMain.ReturnDataTable(sSQL);
            if (dtcheck.Rows.Count > 0)
            {
                int Apdungbanggia = int.Parse(clsMain.ReturnDataTable("select GIATRI from CAUHINH where TEN='APDUNGBANGGIATHEOKHUVUC'").Rows[0][0].ToString());
                if (Apdungbanggia == 1)//Nếu áp dụng bảng giá thì cho ẩn nút bán giá 2 đi
                    btnGiaBan2.Tag = 1;
                else if (bool.Parse(dtcheck.Rows[0]["SUAGIA"].ToString()) || funtions == 1)
                    btnGiaBan2.Tag = 1;
                else
                {
                    bool flag = false;
                    int n = dtHangOrder.Rows.Count;
                    for (int i = 0; i < n; i++)
                    {
                        if (dtHangOrder.Rows[i]["MA_HANGHOA"].ToString() == gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString())
                        {
                            flag = true;
                            break;
                        }
                    }
                    //flag=true:hàng đã order rồi,không được đổi giá.
                    if (flag)
                        btnGiaBan2.Tag = 1;
                    else
                    {
                        btnGiaBan2.Tag = 0;
                        #region Kiểm tra hàng đang ở giá nào,nếu giá 1 thì button.Name= Bán giá 2 ; nếu giá 2 thì button.Name= Bán giá 1 ;
                        sSQL = "select GIABAN1,GIABAN2 from HANGHOA where MA_HANGHOA = " + maMon + "";
                        DataTable dtgiaban = clsMain.ReturnDataTable(sSQL);
                        if (float.Parse(dtgiaban.Rows[0]["GIABAN1"].ToString()) == xGIABAN)
                        {
                            btnGiaBan2.Text = "Bán giá 2";
                        }
                        else
                        {
                            btnGiaBan2.Text = "Bán giá 1";
                        }
                    }
                        #endregion
                }
            }
            #endregion

            if (funtions == 8)
            {
                if (pnBtnDeleteGv.Visible == false)
                    pnBtnDeleteGv.Visible = true;

            }
            lb_STT.Text = gv_SelectProduct.FocusedRowHandle.ToString();
            if (sendOrder != 1)
                return;
            if (pnBtnDeleteGv.Visible == false)
                pnBtnDeleteGv.Visible = true;
            Flag_soluong = true;
            lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
            string yeucau = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["GHICHU"]).ToString();
            if (yeucau != "a")
                txtYeuCauThem.Text = yeucau;
            else
                txtYeuCauThem.Text = "";
        }

        private void gr_SelectProduct_Leave(object sender, EventArgs e)
        {
            if (btn_xoa.Focused || btn_cong.Focused || btn_tru.Focused || gr_SelectProduct.Focused || btn_XoaTatCa.Focused || btn_Dow.Focused || btn_up.Focused || btn_UpFirst.Focused || btn_DowLast.Focused)
            {
                return;
            }
            pnUpDowGv.Visible = false;
            gr_SelectProduct.RefreshDataSource();
        }

        private void XuLyGv()
        {
            if (pnBtnDeleteGv.Visible)
            {
                if (gv_SelectProduct.RowCount > 5)
                {
                    pnUpDowGv.Visible = true;
                    gr_SelectProduct.RefreshDataSource();
                }
                else
                {
                    pnUpDowGv.Visible = false;
                    gr_SelectProduct.RefreshDataSource();
                }
            }
            else
            {
                if (gv_SelectProduct.RowCount > 8)
                {
                    pnUpDowGv.Visible = true;
                    gr_SelectProduct.RefreshDataSource();
                }
                else
                {
                    pnUpDowGv.Visible = false;
                    gr_SelectProduct.RefreshDataSource();
                }
            }
        }

        private void btn_ThuNgan_Click(object sender, EventArgs e)
        {
            if (funtions == 9 || exitReprint == true || funtions == 10)
                return;

            SetBackgroudOnClick(btn_ThuNgan);
            SetDefautlBackgroud(btn_NghiepVu);
            SetDefautlBackgroud(btn_DatThu);
            pnThuNgan.BringToFront();
        }

        private void btn_NghiepVu_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
                return;
            SetBackgroudOnClick(btn_NghiepVu);
            SetDefautlBackgroud(btn_ThuNgan);
            SetDefautlBackgroud(btn_DatThu);
            pnNghiepVu.BringToFront();
        }

        private void btn_DatThu_Click(object sender, EventArgs e)
        {
            if (funtions != 1 || exitReprint == true)
                return;
            SetBackgroudOnClick(btn_DatThu);
            SetDefautlBackgroud(btn_ThuNgan);
            SetDefautlBackgroud(btn_NghiepVu);
            pnDacThu.BringToFront();
        }

        private void btn_logout_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnKhachrangoai_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
            {
                return;
            }
            //Update khach da ra ngoai
            string maHoaDon = GetMaHoaDon(maBan);
            string sSQL = "";
            sSQL += " select a.MA_BAN ,a.MA_HOADON,b.KHACHRANGOAI" + "\n";
            sSQL += " from CT_MOBAN_KHACHSAN  a" + "\n";
            sSQL += " inner join BAN b on a.MA_BAN =b.MA_BAN " + "\n";
            sSQL += " where a.MA_HOADON=" + clsMain.SQLString(maHoaDon) + "\n";
            DataTable dtcheck = clsMain.ReturnDataTable(sSQL);
            sSQL = "";
            if (dtcheck.Rows.Count > 0)
            {
                for (int i = 0; i < dtcheck.Rows.Count; i++)
                {
                    if (bool.Parse(dtcheck.Rows[i]["KHACHRANGOAI"].ToString()) == true)
                    {
                        sSQL += " Update BAN set KHACHRANGOAI = 0,THOIGIANRANGOAI=GETDATE() Where MA_BAN = " + dtcheck.Rows[i]["MA_BAN"].ToString() + "\n";
                    }
                    else
                    {
                        sSQL += " Update BAN set KHACHRANGOAI = 1,THOIGIANRANGOAI=GETDATE() Where MA_BAN = " + dtcheck.Rows[i]["MA_BAN"].ToString() + "\n";
                    }
                }
            }
            clsMain.ExecuteSQL(sSQL);
            //
            ExitDivideTable();
        }

        private void btn_TamTinh_Click(object sender, EventArgs e)
        {
            pnBtnDeleteGv.Visible = false;
            if (funtions != 1)
                return;
            if (maBan == "")
                return;
            if (gv_SelectProduct.RowCount <= 0 && decimal.Parse(lbTiengio.Text) <= 0)
                return;
            //Lấy mã hóa đơn
            string idBill = GetMaHoaDon(maBan);
            //Lấy tiền đặt cọc
            decimal Tiendatcoc = 0;
            DataTable dtMoney = clsMain.ReturnDataTable("Select TIENDATCOC from HOADON_KHACHSAN where MA_HOADON='" + idBill + "' and ISNULL(TIENDATCOC,0) != 0");
            if (dtMoney.Rows.Count > 0)
                Tiendatcoc = decimal.Parse(dtMoney.Rows[0]["TIENDATCOC"].ToString());
            else
                Tiendatcoc = 0;
            //Lấy danh sách hàng hóa
            DataTable dt = (DataTable)gr_SelectProduct.DataSource;
            //gọi form thanh toán
            Frm_Payment_KhachSan_Tamtinh frm = new Frm_Payment_KhachSan_Tamtinh(maBan, idBill, dt, Tiendatcoc, decimal.Parse(lbTiengio.Text), dtTiengio, _checkin, _checkout);
            frm.ShowDialog();
            frm.Dispose();
            //
            ExitDivideTable();
        }

        private void btn_ThanhToan_Click(object sender, EventArgs e)
        {
            pnBtnDeleteGv.Visible = false;
            if (funtions != 1)
                return;
            if (maBan == "")
                return;
            if (gv_SelectProduct.RowCount <= 0 && decimal.Parse(lbTiengio.Text) <= 0)
                return;
            //Lấy mã hóa đơn
            string idBill = GetMaHoaDon(maBan);
            //Lấy tiền đặt cọc
            decimal Tiendatcoc = 0;
            //13/10/2018 lấy tiền đặt cọc từ ks_datphong qua
            string sql = "";
            sql += " UPDATE hoadon_khachsan    " + "\n";
            sql += " SET hoadon_khachsan.TIENDATCOC = ks_datphong.DATHANHTOAN     " + "\n";
            sql += " from hoadon_khachsan  " + "\n";
            sql += " INNER JOIN ks_datphong  ON hoadon_khachsan.MA_HOADON = ks_datphong.HOADON  " + "\n";
            sql += " where hoadon_khachsan.MA_HOADON =  " + clsMain .SQLString (idBill )+ "\n";
            sql += " And ks_datphong.DATHANHTOAN >0 " + "\n";
            clsMain.ExecuteSQL(sql);
            DataTable dtMoney = clsMain.ReturnDataTable("Select TIENDATCOC from HOADON_KHACHSAN where MA_HOADON='" + idBill + "' and ISNULL(TIENDATCOC,0) != 0");
            if (dtMoney.Rows.Count > 0)
                Tiendatcoc = decimal.Parse(dtMoney.Rows[0]["TIENDATCOC"].ToString());
            else
                Tiendatcoc = 0;
            //Lấy danh sách hàng hóa
            DataTable dt = (DataTable)gr_SelectProduct.DataSource;
            //gọi form thanh toán
            Frm_Payment_KhachSan frm = new Frm_Payment_KhachSan(maBan, idBill, dt, Tiendatcoc, decimal.Parse(lbTiengio.Text), dtTiengio, _checkin, _checkout);
            frm.ShowDialog();
            frm.Dispose();
            //
            ExitDivideTable();
        }

        private void btnDanhsachkhach_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
                return;
            Frm_Danhsachkhach frm = new Frm_Danhsachkhach();
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnMoKet_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
                return;
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }
            clsKP_Terminal.OpenCashDrawer(false);
        }

        private void btnKetKa_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
                return;
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check = false;
                Frm_ConfigFuntion frm1 = new Frm_ConfigFuntion();
                frm1.ShowDialog();
                check = frm1.kt;
                frm1.Dispose();
                if (!check)
                {
                    return;
                }
            }
            Frm_KetCaKhachSan frm = new Frm_KetCaKhachSan();
            frm.Width = this.Width;
            frm.Height = this.Height;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btn_InlaiOrder_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
                return;
            if (maBan == "")
                return;
            if (gv_SelectProduct.RowCount <= 0)
                return;

            string maHDB = GetMaHoaDon(maBan);
            string nameTable = clsMain.ReturnDataTable("Select TEN_BAN From BAN Where MA_BAN = " + clsMain.SQLString(maBan) + "").Rows[0][0].ToString();

            string solan = "";
            string sSQL = "";
            sSQL += "Select LANIN From CT_INLAI Where MA_HOADON=" + clsMain.SQLString(maHDB);
            DataTable dtinlai = clsMain.ReturnDataTable(sSQL);
            if (dtinlai.Rows.Count <= 0)
            {
                sSQL = "";
                sSQL += "INSERT INTO CT_INLAI(MA_HOADON,LANIN)" + "\n";
                sSQL += "Values(";
                sSQL += clsMain.SQLString(maHDB) + ",";
                sSQL += clsMain.SQLString(1.ToString()) + ")";
                clsMain.ExecuteSQL(sSQL);
                solan = "1";
            }
            else
            {
                solan = (int.Parse(dtinlai.Rows[0]["LANIN"].ToString()) + 1).ToString();
                sSQL = "";
                sSQL += "UPDATE CT_INLAI SET" + "\n";
                sSQL += "LANIN = " + clsMain.SQLString(solan) + "\n";
                sSQL += "WHERE MA_HOADON=" + clsMain.SQLString(maHDB);
                clsMain.ExecuteSQL(sSQL);
            }

            while (gv_SelectProduct.RowCount > 0)
            {
                List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                int j = 0;
                string maBEP = "";
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    DM_HangHoa dmHangHoa = new DM_HangHoa();
                    maBEP = gv_SelectProduct.GetRowCellValue(0, gv_SelectProduct.Columns["MA_BEP"]).ToString();
                    dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                    if (maBEP == gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_BEP"]).ToString())
                    {
                        dmHangHoa.Ma_HoaDon = maHDB;
                        dmHangHoa.Ten_HoaDon = "Order Bếp In Lại Lần" + " " + solan;
                        dmHangHoa.TEN_BAN = nameTable;
                        dmHangHoa.MaQuay = cls_ConfigCashier.nameCashier;
                        dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                        dmHangHoa.STT = (++j).ToString();
                        dmHangHoa.MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                        dmHangHoa.TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                        dmHangHoa.SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                        dmHangHoa.THUE = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["THUE"]).ToString());
                        dmHangHoa.GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                        string repuest = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"]).ToString();
                        if (repuest == "a")
                            dmHangHoa.GHICHU = "";
                        else
                            dmHangHoa.GHICHU = repuest;

                        //Kiểm tra hàng hóa có in bếp ko
                        string sqlinbep = " SELECT ISNULL(IS_INBEP,0) AS IS_INBEP FROM HANGHOA WHERE MA_HANGHOA = " + dmHangHoa.MA_HANGHOA;
                        DataTable dtinbep = clsMain.ReturnDataTable(sqlinbep);
                        if (bool.Parse(dtinbep.Rows[0]["IS_INBEP"].ToString()) == true)
                            listDM_HangHoa.Add(dmHangHoa);

                        // Delete product selected 
                        if (i != 0)
                        {
                            gv_SelectProduct.DeleteRow(i);
                            i--;
                        }
                    }
                }

                string print = GetIdKitchen(maBEP);
                if (print == null)
                    XtraMessageBox.Show("Chưa chọn máy in cho mặt hàng này !", "Thông báo", MessageBoxButtons.OK);
                gv_SelectProduct.DeleteRow(0);// Delete rows(0)
                // Printer Order 
                convert cvrt = new convert();
                DataTable Orderbill = cvrt.ConvertToDataTable(listDM_HangHoa);
                if (Orderbill.Rows.Count > 0)
                {
                    Frm_Report1 frm = new Frm_Report1();
                    frm.ReportName = "rpt_OrderKaraoke";
                    frm.DataSource = Orderbill;
                    frm.WindowState = FormWindowState.Minimized;
                    frm.PrinterName = print;
                    frm.IsPrint = true;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                //
            }
            //
            ExitDivideTable();
        }

        private void btn_HuyBan_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
                return;
            if (maBan == "")
                return;
            if (CheckOpenTable(maBan) == false)
                return;
            if (XtraMessageBox.Show("Xác nhận bạn muốn hủy phòng ? ", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.Yes)
                return;
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion frm = new Frm_ConfigFuntion();
                frm.ShowDialog();
                check = frm.kt;
                frm.Dispose();
                if (check == false)
                    return;
            }
          
            string sTongtiengio = lbTiengio.Text.Replace(",", "");
            string sTongcong = lbTatol.Text.Replace(",", "");
            string sDichvu = lbDichvu.Text.Replace(",", "");
            string maHoaDon = GetMaHoaDon(maBan);
            DataTable table = GetTableInBill(maHoaDon);
            string sSQL = "";
            int n = table.Rows.Count;
            if (n > 0)
            {
                sSQL = "";
                sSQL += "EXEC SP_InsertHuyPhong " + clsMain.SQLString(maHoaDon) + "," + clsMain.SQLString(table.Rows[0]["MA_BAN"].ToString()) + "," + clsMain.SQLString(clsGlobal.gsUserID) + "," + clsMain.SQLString(sTongtiengio) + "," + clsMain.SQLString(sTongcong) + "," + clsMain.SQLString(sDichvu) + "";
                clsMain.ExecuteSQL(sSQL);

                sSQL = "";
                sSQL += "Update KS_DATPHONG  SET TRANGTHAI=0 Where TRANGTHAI=1 AND maphong =" + clsMain.SQLString(table.Rows[0]["MA_BAN"].ToString()) + " AND HOADON=" + clsMain.SQLString(maHoaDon) + "\n";
                sSQL += "Delete From CT_HOADON_KHACHSAN Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + "\n";
                sSQL += "Delete From CT_TTKHACHHANG_KHACHSAN Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + "\n";
                sSQL += "Delete From CT_MOBAN_KHACHSAN Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + "\n";
                sSQL += "Delete From HOADON_KHACHSAN Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + "\n";
                sSQL += "Delete From DINHLUONG_DIEUCHINH Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + "\n";
                for (int i = 0; i < n; i++)
                {
                    sSQL += "Update BAN SET ISMO_BAN=0, SEE_PAYMENTS = 0, KHACHRANGOAI = 0 Where MA_BAN =" + clsMain.SQLString(table.Rows[i]["MA_BAN"].ToString()) + "\n";

                    sSQL += "Delete From DISPLAY_ORDER Where MA_BAN=" + clsMain.SQLString(table.Rows[i]["MA_BAN"].ToString()) + "\n";
                }

               

                clsMain.ExecuteSQL(sSQL);
            }
            //
            ExitDivideTable();
        }

        private void SetBackgroundBtnInPn(PanelControl pn1, PanelControl pn2)
        {
            foreach (SimpleButton bt in pn2.Controls)
            {
                SetDefautlBackgroud(bt);
            }
            foreach (SimpleButton btn in pn1.Controls)
            {
                if (btn.Name == this.ActiveControl.Name)
                {
                    SetBackgroudOnClick(btn);
                }
                else
                {
                    SetDefautlBackgroud(btn);
                }
            }
        }

        private void SetDefaultBackgroundBtnInPn(PanelControl pn2)
        {
            foreach (SimpleButton bt in pn2.Controls)
            {
                SetDefautlBackgroud(bt);
            }
        }

        private void Guiyeucauthem()
        {
            for (int i = 0; i < gv_SelectProduct.RowCount; i++)
            {
                int maHH = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                if (maHH == int.Parse(maMon))
                    gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"], txtYeuCauThem.Text);
            }
            txtYeuCauThem.Text = "";
            // Return List Dishes 
            funtions = 2;
            btn_SoDoBan.Text = "Sơ đồ phòng";
            lbListTable.Text = "Danh sách món:";
            pnShowHang.BringToFront();
            GetNhomHang();
            GetListMon();
            SetDefaultBackgroundBtnInPn(pnThuNgan);
            SetDefaultBackgroundBtnInPn(pnNghiepVu);
            foreach (SimpleButton btn in pnKV_Mon.Controls)
            {
                if (btn.Name == maNH.ToString())
                {
                    SetBackgroudOnClick(btn);
                }
            }
        }
        
        public int selectTable = 0;
        public int selectDishes = 0;
        DateTime _checkin;
        DateTime _checkout;

        private void GetDishGoToBuy(string maHangHoa, int idKV)
        {
            try
            {
                string sql = string.Format("Exec SelectByMON @MAHANG={0},@MAVACH='{1}',@maKV={2},@maCH={3}", maHangHoa, maHangHoa, idKV, cls_ConfigCashier.idShop);
                DataTable dtHH = clsMain.ReturnDataTable(sql); //[SelectByMON]
                int check = 0;
                DM_HangHoa hanghoa = new DM_HangHoa();
                //giá bán thực của hàng hóa.
                decimal Giaban_Cur = 0;
                bool flag_Exist = false; // flag_Exist=false: mặt hàng này chưa bán và ngược lại

                if (dtHH.Rows.Count >= 1)
                {
                    if (bool.Parse(dtHH.Rows[0]["SUAGIA"].ToString()))
                    {
                        if (gv_SelectProduct.RowCount > 0)
                        {
                            for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                            {
                                if (gv_SelectProduct.GetRowCellValue(i, "MA_HANGHOA").ToString() == maHangHoa)
                                {
                                    flag_Exist = true;
                                    break;
                                }
                            }
                        }
                        if (!flag_Exist)
                        {
                            Frm_Chontheothoigia frm = new Frm_Chontheothoigia();
                            frm.ShowDialog();
                            Giaban_Cur = frm.tmp_Thoigia;//Lấy giá theo thời giá người dùng nhập vào

                            if (Giaban_Cur == 0)//chưa nhập thời giá thì return
                                return;

                            btnGiaBan2.Tag = 1;
                        }
                    }
                    else
                    {
                        Giaban_Cur = decimal.Parse(dtHH.Rows[0]["GIABAN"].ToString());
                        btnGiaBan2.Tag = 1;
                        btnGiaBan2.Text = "Bán giá 2";
                    }
                    hanghoa.MA_HANGHOA = dtHH.Rows[0]["MA_HANGHOA"].ToString();
                    hanghoa.TEN_HANGHOA = dtHH.Rows[0]["TEN_HANGHOA"].ToString();
                    hanghoa.SOLUONG = 1;
                    hanghoa.MA_BEP = dtHH.Rows[0]["MA_BEP"].ToString();
                    hanghoa.GIABAN = Giaban_Cur;
                    hanghoa.GHICHU = dtHH.Rows[0]["GHICHU"].ToString();
                    hanghoa.THUE = double.Parse(dtHH.Rows[0]["THUE"].ToString());
                    check = 1;
                }

                if (check == 0)
                    throw new Exception("Hàng hóa này không tồn tại trong hệ thống !");

                if (dt.Rows.Count < 1)
                {
                    testID = false;
                }
                else
                    testID = true;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["MA_HANGHOA"].ToString() == hanghoa.MA_HANGHOA.ToString())
                    {
                        if (funtions == 8)
                            dt.Rows[i]["SOLUONG"] = double.Parse(dt.Rows[i]["SOLUONG"].ToString()) + double.Parse(hanghoa.SOLUONG.ToString());
                        else
                            dt.Rows[i]["SOLUONG"] = double.Parse(dt.Rows[i]["SOLUONG"].ToString()) + 1;

                        testID = true;
                        break;
                    }
                    else
                        testID = false;
                }

                if (testID == false)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = int.Parse(hanghoa.MA_HANGHOA);
                    dr[1] = hanghoa.TEN_HANGHOA;
                    dr[2] = hanghoa.SOLUONG;
                    dr[3] = hanghoa.GIABAN;
                    dr[4] = hanghoa.GHICHU;
                    dr[5] = hanghoa.MA_BEP;
                    dr[6] = hanghoa.THUE;
                    dt.Rows.Add(dr);
                }
                bsource.DataSource = dt;
                gr_SelectProduct.DataSource = bsource;
                gr_SelectProduct.RefreshDataSource();
                if (gv_SelectProduct.RowCount > 0)
                    TotalMoney();
                gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.RowCount - 1;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK);
            }
        }

        private void SetJoinTable(string idTable, string idJoinTable)
        {
            string _idDillJoin = GetMaHoaDon(idJoinTable);
            string maHOADON = GetMaHoaDon(idTable);
            if (maHOADON == null || (_idDillJoin == null && maHOADON == null))
            {
                throw new Exception("Phòng gốc phải là phòng đã mở trước khi ghép !");
            }
            if (_idDillJoin == null && maHOADON != "")
            {
                // update in table columns ISBAN_MO
                string sql = "Update BAN SET ISMO_BAN=1 where MA_BAN =" + int.Parse(idJoinTable);
                clsMain.ExecuteSQL(sql);
                string sqlCTMOBAN = "SET DATEFORMAT DMY Insert into CT_MOBAN_KHACHSAN (MA_HOADON, MA_BAN)";
                sqlCTMOBAN += " values(" + "'" + maHOADON + "'," + int.Parse(idJoinTable) + ")";
                clsMain.ExecuteSQL(sqlCTMOBAN);

                for (int i = 0; i < dtBan.Rows.Count; i++)
                {
                    if (dtBan.Rows[i]["MA_BAN"].ToString() == idJoinTable)
                    {
                        dtBan.Rows[i]["ISMO_BAN"] = 1;
                    }
                }
                return;
            }
            else
            {
                string sqlCT_MOBAN = "update CT_MOBAN_KHACHSAN set MA_HOADON ='" + maHOADON + "' where MA_HOADON ='" + _idDillJoin + "'";
                clsMain.ExecuteSQL(sqlCT_MOBAN);
                string sqlSelect = " select cthd.MA_HANGHOA, hh.TEN_HANGHOA, cthd.SOLUONG, cthd.GIABAN";
                sqlSelect += " from CT_HOADON_KHACHSAN as cthd inner join HANGHOA as hh on cthd.MA_HANGHOA=hh.MA_HANGHOA where cthd.MA_HOADON ='" + _idDillJoin + "'";
                DataTable disheJoinTable = clsMain.ReturnDataTable(sqlSelect);
                if (disheJoinTable.Rows.Count > 0)
                {
                    bool kt = true;
                    for (int i = 0; i < disheJoinTable.Rows.Count; i++)
                    {
                        if (ChecBishInTable(maHOADON, disheJoinTable.Rows[i]["MA_HANGHOA"].ToString()) == false)
                        {
                            string sqls = "SET DATEFORMAT DMY Insert into CT_HOADON_KHACHSAN ( MA_HOADON, MA_HANGHOA, SOLUONG, GIABAN)";
                            sqls += " values(" + "'" + maHOADON + "'," + int.Parse(disheJoinTable.Rows[i]["MA_HANGHOA"].ToString()) + "," + disheJoinTable.Rows[i]["SOLUONG"] + "," + disheJoinTable.Rows[i]["GIABAN"] + ")";
                            clsMain.ExecuteSQL(sqls);
                        }
                        else
                        {
                            string sqlup = "UPDATE CT_HOADON_KHACHSAN SET SOLUONG +=" + disheJoinTable.Rows[i]["SOLUONG"] + "  WHERE MA_HOADON ='" + maHOADON + "' and MA_HANGHOA =" + int.Parse(disheJoinTable.Rows[i]["MA_HANGHOA"].ToString()) + "";
                            clsMain.ExecuteSQL(sqlup);
                        }
                    }
                    if (kt == true)
                    {
                        string deleteCT = "Delete FROM CT_HOADON_KHACHSAN  where MA_HOADON ='" + _idDillJoin + "'";
                        clsMain.ExecuteSQL(deleteCT);
                    }
                }
                string deleteBill = "SET DATEFORMAT DMY  Delete FROM HOADON_KHACHSAN  where MA_HOADON ='" + _idDillJoin + "'";
                clsMain.ExecuteSQL(deleteBill);
            }
        }

        private void CanceTable(string idTable)
        {
            try
            {
                string maHD = GetMaHoaDon(idTable);
                string sqlCTHD = " Delete CT_HOADON_KHACHSAN where MA_HOADON='" + maHD + "'";
                clsMain.ExecuteSQL(sqlCTHD);
                string sqlCTMOBAN = " Delete CT_MOBAN_KHACHSAN where MA_HOADON='" + maHD + "'";
                clsMain.ExecuteSQL(sqlCTMOBAN);
                string sqlHD = " Delete HOADON_KHACHSAN where MA_HOADON ='" + maHD + "'";
                clsMain.ExecuteSQL(sqlHD);
                string sqlpm = "update BAN SET ISMO_BAN=0 where MA_BAN =" + int.Parse(idTable) + "";
                clsMain.ExecuteSQL(sqlpm);
            }
            catch
            {
            }
        }

        private void CheckedIsTable()
        {
            try
            {
                string time1 = Convert.ToDateTime(DateTime.Now.ToShortDateString()).ToString("dd/MM/yyyy");
                DateTime now = DateTime.Parse(time1);
                DataTable dttt = clsMain.ReturnDataTable("SET DATEFORMAT DMY select mb.MA_BAN, convert(varchar(25), hd.CHECKIN , 120) as GIOVAO, convert(varchar(10),hd.GIOVAO , 101) as NgayDat from CT_MOBAN_KHACHSAN mb inner join HOADON_KHACHSAN hd on mb.MA_HOADON=hd.MA_HOADON  where ISBANDAT =1");
                if (dttt.Rows.Count <= 0)
                    return;
                for (int i = 0; i < dttt.Rows.Count; i++)
                {
                    string dd = dttt.Rows[i]["GIOVAO"].ToString();
                    if (dttt.Rows[i]["GIOVAO"].ToString() != "")
                    {
                        string sso = dttt.Rows[i]["NgayDat"].ToString();
                        DateTime x = DateTime.Parse(sso);
                        int outline = DateTime.Compare(now, x);
                        if ((int.Parse(dttt.Rows[i]["GIOVAO"].ToString().Substring(11, 2)) - int.Parse(DateTime.Now.Hour.ToString()) <= 5) && outline == 0)
                        {
                            string sql = "SET DATEFORMAT DMY Update BAN SET ISBANDAT = 1 where MA_BAN =" + int.Parse(dttt.Rows[i]["MA_BAN"].ToString());
                            clsMain.ExecuteSQL(sql);
                        }
                    }
                }
            }
            catch
            {
            }
        }

        // check table is tables check when open table
        private bool CheckHaveISBANDAT(int idTable)
        {
            string sqlIS = "Select ISBANDAT from BAN where MA_BAN =" + idTable + " and ISBANDAT = 1";
            DataTable tableIS = clsMain.ReturnDataTable(sqlIS);
            if (tableIS.Rows.Count > 0)
                return true;
            return false;
        }

        string Madatphong_nhap = "";

        private void btnNhanphong_Click(object sender, EventArgs e)
        {
            if (funtions == 1 && maBan != "")
            {
                if (ktrasuachua(maBan) == true)
                {
                    XtraMessageBox.Show("Phòng đang tạm khóa!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                //Nếu phòng đã có người đặt thì phải nhập mã đặt phòng
                string sql = "";
                sql += "select MADATPHONG ,MAPHONG ,NGAYNHANPHONG ,NGAYTRAPHONG ,NGUOIDAT ,DIENTHOAI ,MA_LOAIPHONG  ,ISMO_BAN   " + "\n";
                sql += "from KS_DATPHONG  " + "\n";
                sql += "INNER JOIN BAN ON BAN .MA_BAN = KS_DATPHONG .MAPHONG  " + "\n";
                sql += "where  KS_DATPHONG.maphong=" + clsMain.SQLString(maBan) + "\n";
                sql += "and  KS_DATPHONG.ngaynhanphong=" + clsMain.SQLString(cls_KP_RES.DateToString_Date(clsMain.GetServerDate())) + "\n";
                sql += "and  KS_DATPHONG.TRANGTHAI=1 " + "\n";
                sql += "and  BAN.ISMO_BAN=0 " + "\n";
                DataTable dtTmp = clsMain.ReturnDataTable(sql);
                if (dtTmp.Rows.Count >0)
                {
                    Madatphong_nhap = "";
                    Frm_Madatphong frm= new Frm_Madatphong ();
                    frm.sMa_Datphong = dtTmp.Rows[0]["MADATPHONG"].ToString();
                    frm.ShowDialog ();
                    Madatphong_nhap = frm.sMa_Datphong_Nhap;
                    frm.Dispose ();
                    if (Madatphong_nhap == "")
                    {
                        return;
                    }
                }

                funtions = 200;
                btnNhanphong.Text = "Sơ đồ phòng";
                lbKhuvuc_Nhanphong.Text = lbKhuVuc.Text;
                lbPhong_Nhanphong.Text = lbNameTable.Text;
                pnlNhanPhong.BringToFront();
                LoadDanhSachKhach();
                if (((DataTable)gridControl1.DataSource ).Rows.Count == 0 && dtTmp.Rows .Count >0)
                {
                    LoadDanhSachKhach_Datphong(Madatphong_nhap);
                }
                // 31/07 khang lấy giờ ra dự tính
                string maHoaDon = GetMaHoaDon(maBan);
                string sql2 = "";
                sql2 = "Select GIORA_DUTINH, CHECKIN from HOADON_KHACHSAN where MA_HOADON=" + clsMain.SQLString(maHoaDon);
                DataTable dt = clsMain.ReturnDataTable(sql2);
                if (dt.Rows.Count != 0)
                {
                    dtpNgayra.EditValue = DateTime.Parse(dt.Rows[0]["GIORA_DUTINH"].ToString());
                    dtpNgayvao.EditValue = DateTime.Parse(dt.Rows[0]["CHECKIN"].ToString());
                }
                else
                {
                    dtpNgayra.EditValue = clsMain.GetServerDate().AddDays(1);
                    dtpNgayvao.EditValue = clsMain.GetServerDate();
                }
                if (maHoaDon != "")
                {
                    sql2 = "";
                    sql2 = "Select * from KS_DATPHONG where TRANGTHAI=1 AND HOADON=" + clsMain.SQLString(maHoaDon);
                    dt = clsMain.ReturnDataTable(sql2);
                    if (dt.Rows.Count != 0)
                    {
                        txtDaThanhToan.EditValue = int.Parse(dt.Rows[0]["DATHANHTOAN"].ToString().Replace(",", ""));
                    }
                    else
                    {
                        txtDaThanhToan.EditValue = int.Parse("0");
                    }
                }
                //
                SetDefaultBackgroundBtnInPn(pnThuNgan);
                SetDefaultBackgroundBtnInPn(pnNghiepVu);
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    if (btn.Name == btnNhanphong.Name || btn.Name == btnXacNhan.Name)
                    {
                        btn.Visible = true;
                    }
                    else
                    {
                        btn.Visible = false;
                    }
                }
                return;
            }
            else 
            {
                funtions = 1;
                btnNhanphong.Text = "Nhận Phòng";
                btnGiaBan2.Tag = 1;
                pnShowHang.BringToFront();
                GetKhuVuc();
                GetSoDoBan();
                SetDefaultBackgroundBtnInPn(pnThuNgan);
                SetDefaultBackgroundBtnInPn(pnNghiepVu);
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    btn.Visible = true;
                }
            }
        }

        DataTable dtTEN = new DataTable();
        private void LoadDanhSachKhach()
        {
            DataTable DTSOUCRE = new DataTable();
            string maHD_KS = GetMaHoaDon(maBan);
            string sSQL = "";

            sSQL += "select a.MA_HOADON ,a.MA_BAN,a.MA_KH ,c.TEN as TENKH,c.NGAYSINH,case c.GIOITINH when 0 then N'Nữ' else N'Nam' end as  GIOITINH,c.DIACHI,c.DIENTHOAI,c.EMAIL,c.CMND,c.QUOCTICH,b.CHECKIN" + "\n";
            sSQL += "from CT_TTKHACHHANG_KHACHSAN a " + "\n";
            sSQL += "inner join HOADON_KHACHSAN b on a.MA_HOADON =b.MA_HOADON " + "\n";
            sSQL += "inner join KS_KHACHHANG C on a.MA_KH  =c.MA_KH  " + "\n";
            sSQL += "where a.MA_HOADON=" + clsMain.SQLString(maHD_KS);
            DTSOUCRE = clsMain.ReturnDataTable(sSQL);
            gridControl1.DataSource = DTSOUCRE;

            sSQL = "";
            sSQL += "Select MA_KH,TEN,NGAYSINH,case GIOITINH when 0 then N'Nữ' else N'Nam' end as  GIOITINH,DIACHI,DIENTHOAI,EMAIL,CMND,QUOCTICH" + "\n";
            sSQL += "From KS_KHACHHANG";
            dtTEN = clsMain.ReturnDataTable(sSQL);
            gridControl2.DataSource = dtTEN.Copy();
            gridControl2.Visible = false;

            ComboBoxItemCollection itemrcbbCMND = rcbbCMND.Items;
            itemrcbbCMND.BeginUpdate();
            try
            {
                itemrcbbCMND.Clear();
                foreach (DataRow dr in dtTEN.Rows)
                {
                    itemrcbbCMND.Add(dr["CMND"].ToString());
                }
            }
            finally
            {
                itemrcbbCMND.EndUpdate();
            }
        }

        private void LoadDanhSachKhach_Datphong(string madatphong)
        {
            DataTable DTSOUCRE = new DataTable();
            string sSQL = "";
            sSQL += "Select KS_KHACHHANG.MA_KH,KS_KHACHHANG.TEN as TENKH,KS_KHACHHANG.NGAYSINH,case KS_KHACHHANG.GIOITINH when 0 then N'Nữ' else N'Nam' end as  GIOITINH,KS_KHACHHANG.DIACHI,KS_KHACHHANG.DIENTHOAI,KS_KHACHHANG.EMAIL,KS_KHACHHANG.CMND,KS_KHACHHANG.QUOCTICH" + "\n";
            sSQL += "From KS_KHACHHANG" + "\n";
            sSQL += "Inner Join KS_DATPHONG on KS_KHACHHANG.CMND=KS_DATPHONG.CMND" + "\n";
            sSQL += "Where  KS_DATPHONG.MADATPHONG= "+clsMain .SQLString (madatphong);
            DTSOUCRE = clsMain.ReturnDataTable(sSQL);
            gridControl1.DataSource = DTSOUCRE;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT1 & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void rcbbCMND_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DevExpress.XtraEditors.ComboBoxEdit editor = (sender as DevExpress.XtraEditors.ComboBoxEdit);
                if (editor.SelectedIndex != -1)
                {
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "MA_KH", dtTEN.Rows[editor.SelectedIndex]["MA_KH"].ToString());
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "GIOITINH", dtTEN.Rows[editor.SelectedIndex]["GIOITINH"].ToString());
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TENKH", dtTEN.Rows[editor.SelectedIndex]["TEN"].ToString());
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "QUOCTICH", dtTEN.Rows[editor.SelectedIndex]["QUOCTICH"].ToString());
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "NGAYSINH", dtTEN.Rows[editor.SelectedIndex]["NGAYSINH"].ToString());
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "EMAIL", dtTEN.Rows[editor.SelectedIndex]["EMAIL"].ToString());
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "DIACHI", dtTEN.Rows[editor.SelectedIndex]["DIACHI"].ToString());
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "DIENTHOAI", dtTEN.Rows[editor.SelectedIndex]["DIENTHOAI"].ToString());
                }
                else
                {
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "GIOITINH", "Nam");
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TENKH", "");
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "QUOCTICH", "Việt Nam");
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "NGAYSINH", "");
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "EMAIL", "");
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "DIACHI", "");
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "DIENTHOAI", "");
                }
            }
            catch
            {
            }
        }

        private void rcbbCMND_Leave(object sender, EventArgs e)
        {
            try
            {
                DevExpress.XtraEditors.ComboBoxEdit editor = (sender as DevExpress.XtraEditors.ComboBoxEdit);
                if (editor.SelectedIndex != -1)
                {
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "MA_KH", dtTEN.Rows[editor.SelectedIndex]["MA_KH"].ToString());
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "GIOITINH", dtTEN.Rows[editor.SelectedIndex]["GIOITINH"].ToString());
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TENKH", dtTEN.Rows[editor.SelectedIndex]["TEN"].ToString());
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "QUOCTICH", dtTEN.Rows[editor.SelectedIndex]["QUOCTICH"].ToString());
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "NGAYSINH", dtTEN.Rows[editor.SelectedIndex]["NGAYSINH"].ToString());
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "EMAIL", dtTEN.Rows[editor.SelectedIndex]["EMAIL"].ToString());
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "DIACHI", dtTEN.Rows[editor.SelectedIndex]["DIACHI"].ToString());
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "DIENTHOAI", dtTEN.Rows[editor.SelectedIndex]["DIENTHOAI"].ToString());
                }
                else
                {
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "GIOITINH", "Nam");
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TENKH", "");
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "QUOCTICH", "Việt Nam");
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "NGAYSINH", "");
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "EMAIL", "");
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "DIACHI", "");
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "DIENTHOAI", "");
                }
            }
            catch
            {
            }
        }

        private void rbtnXoa_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            try
            {
                DataTable dttmp = (DataTable)gridControl1.DataSource;
                if (dttmp.Rows.Count > 1)
                {
                    dttmp.Rows.RemoveAt(gridView1.FocusedRowHandle);
                }
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnXacNhan_Click(object sender, EventArgs e)
        {
            if (funtions != 200)
                return;
            if (!CheckInput())
                return;
            String sSQL = string.Empty;
            string maHoaDon = GetMaHoaDon(maBan);

            //Luu lai danh sach khach hang
            sSQL = string.Empty;
            DataTable dttmp = (DataTable)gridControl1.DataSource;
            foreach (DataRow dr in dttmp.Rows)
            {
                DataTable dt = clsMain.ReturnDataTable("select MA_KH,CMND,TEN from KS_KHACHHANG where CMND=" + clsMain.SQLString(dr["CMND"].ToString()));
                if (dt.Rows.Count > 0)
                {
                    sSQL += "Update KS_KHACHHANG SET " + "\n";
                    sSQL += "TEN=" + clsMain.SQLStringUnicode(dr["TENKH"].ToString()) + ",";
                    sSQL += "NGAYSINH=" + clsMain.SQLString(dr["NGAYSINH"].ToString()) + ",";
                    sSQL += "GIOITINH=" + clsMain.SQLString(dr["GIOITINH"].ToString().ToLower() == "nam" ? "1" : "0") + ",";
                    sSQL += "DIACHI=" + clsMain.SQLStringUnicode(dr["DIACHI"].ToString()) + ",";
                    sSQL += "DIENTHOAI=" + clsMain.SQLString(dr["DIENTHOAI"].ToString()) + ",";
                    sSQL += "EMAIL=" + clsMain.SQLString(dr["EMAIL"].ToString()) + ",";
                    sSQL += "QUOCTICH=" + clsMain.SQLStringUnicode(dr["QUOCTICH"].ToString()) + ",";
                    sSQL += "NGAYTAO=" + "GETDATE()" + ",";
                    sSQL += "NGUOITAO=" + clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + "\n";
                    sSQL += "where MA_KH=" + clsMain.SQLString(dt.Rows[0]["MA_KH"].ToString()) + "\n";
                    sSQL += "\n";
                }
                else
                {
                    sSQL += "Insert Into KS_KHACHHANG(TEN,NGAYSINH,GIOITINH,DIACHI,DIENTHOAI,EMAIL,CMND,QUOCTICH,NGAYTAO,NGUOITAO)" + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLStringUnicode(dr["TENKH"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dr["NGAYSINH"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dr["GIOITINH"].ToString().ToLower() == "nam" ? "1" : "0") + ",";
                    sSQL += clsMain.SQLStringUnicode(dr["DIACHI"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dr["DIENTHOAI"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dr["EMAIL"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dr["CMND"].ToString()) + ",";
                    sSQL += clsMain.SQLStringUnicode(dr["QUOCTICH"].ToString()) + ",";
                    sSQL += "GETDATE()" + ",";
                    sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ")";
                    sSQL += "\n";
                }
            }
            try
            {
                clsMain.ExecuteSQL(sSQL);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(" Có lổi 1." + "\n" + ex.ToString(), "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            //Chua mo phong thi mo phong truoc
            sSQL = string.Empty;
            if (!CheckOpenTable(maBan))
            {
                maHoaDon = GetNewCodeBill();
                sSQL += "Update BAN SET ISMO_BAN=1 where MA_BAN =" + clsMain.SQLString(maBan);
                sSQL += "\n";

                sSQL += "Insert into HOADON_KHACHSAN(MA_HOADON, MANHANVIEN,NGAYTAO, CHECKIN, MA_CUAHANG,MA_KHO,MA_QUAY, ISPAYMENT, ISBANDAT, GIORA_DUTINH)" + "\n";
                sSQL += "Values(";
                sSQL += clsMain.SQLString(maHoaDon) + ",";
                sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                sSQL += "GETDATE()" + ",";
                sSQL += "GETDATE()" + ",";
                sSQL += clsMain.SQLString(cls_ConfigCashier.idShop) + ",";
                sSQL += clsMain.SQLString(cls_ConfigCashier.idWarehouse) + ",";
                sSQL += clsMain.SQLString(cls_ConfigCashier.idCashier) + ",";
                sSQL += clsMain.SQLString("0") + ",";
                sSQL += clsMain.SQLString("0") + ",";
                sSQL += clsMain.SQLString(dtpNgayra.Text) + ")";
                sSQL += "\n";

                sSQL += "Insert into CT_MOBAN_KHACHSAN(MA_HOADON, MA_BAN)" + "\n";
                sSQL += "Values(";
                sSQL += clsMain.SQLString(maHoaDon) + ",";
                sSQL += clsMain.SQLString(maBan) + ")";
                sSQL += "\n";

                if (Madatphong_nhap != "")
                {
                    sSQL += "Update KS_DATPHONG Set " + "\n";
                    sSQL += "HOADON=" + clsMain.SQLString(maHoaDon) + "\n";
                    sSQL += "Where MADATPHONG=" + clsMain.SQLString(Madatphong_nhap) + "\n";
                }
                else
                {
                    String MaBarCode = CreateBarcode();
                    string sql = "" + "\n";
                    sql = "select MA,	TEN	,GIATRI,	GHICHU " + "\n";
                    sql += " from CAUHINH" + "\n";
                    sql += " where TEN in ('KS_GIONHANPHONG','KS_GIOTRAPHONG')" + "\n";
                    DataTable dt = clsMain.ReturnDataTable(sql);
                    string KS_GIONHANPHONG = dt.Select("TEN='KS_GIONHANPHONG'")[0]["GIATRI"].ToString();
                    string KS_GIOTRAPHONG = dt.Select("TEN='KS_GIOTRAPHONG'")[0]["GIATRI"].ToString();
                    float iTongtien = 0;
                    string sSQL1 = string.Format("Exec TinhTienphong_Khachsan3 @NGAYDEN='{0}',@NGAYDI='{1}',@LOAIPHONG='{2}',@SONGUOI='{3}'", cls_KP_RES.DateToString_Date(clsMain.GetServerDate()) + " " + KS_GIONHANPHONG, dtpNgayra.Text + " " + KS_GIOTRAPHONG, lbNameTable.Tag.ToString(), "1");
                    dt = clsMain.ReturnDataTable(sSQL1);
                    iTongtien = iTongtien + float.Parse(dt.Rows[0]["GIA"].ToString());


                    sSQL += "INSERT INTO KS_DATPHONG(MADATPHONG, MAPHONG, NGAYNHANPHONG, NGAYTRAPHONG, NGUOIDAT, DIENTHOAI, EMAIL, TONGTIEN, DATHANHTOAN,TRANGTHAI,HOADON, NGUOITAO, NGAYTAO)" + "\n";
                    sSQL += "VALUES( ";
                    sSQL += clsMain.SQLString(MaBarCode) + ",";
                    sSQL += clsMain.SQLString(maBan) + ",";
                    sSQL += clsMain.SQLString(dtpNgayvao.Text) + ",";
                    sSQL += clsMain.SQLString(dtpNgayra.Text) + ",";
                    sSQL += clsMain.SQLStringUnicode(dttmp.Rows[0]["TENKH"].ToString()) + ",";
                    sSQL += clsMain.SQLStringUnicode(dttmp.Rows[0]["DIENTHOAI"].ToString()) + ",";
                    sSQL += clsMain.SQLStringUnicode(dttmp.Rows[0]["EMAIL"].ToString()) + ",";
                    sSQL += clsMain.SQLString(iTongtien.ToString()) + ",";
                    sSQL += clsMain.SQLString(txtDaThanhToan.Text.Replace(",", "")) + ",";
                    sSQL += clsMain.SQLString("1") + ",";
                    sSQL += clsMain.SQLString(maHoaDon) + ",";
                    sSQL += clsMain.SQLString(clsGlobal.gsUserID) + ",";
                    sSQL += "GETDATE())" + "\n";
                }
               

                try
                {
                    clsMain.ExecuteSQL(sSQL);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(" Có lổi 2." + "\n" + ex.ToString(), "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            // 31/07 khang phòng đã thuê-> update ngày dự định trả phòng
            else
            {
                string sqlUpdate_Ngayra = "Update HOADON_KHACHSAN set GIORA_DUTINH=" + clsMain.SQLString(dtpNgayra.Text) + " where MA_HOADON =" + clsMain.SQLString(maHoaDon);
                try
                {
                    clsMain.ExecuteSQL(sqlUpdate_Ngayra);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(" Có lổi 2." + "\n" + ex.ToString(), "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                string sqlUpdate_Datcoc = "Update KS_DATPHONG set DATHANHTOAN=" + clsMain.SQLString(txtDaThanhToan.Text.Replace(",", "")) + ",NGUOISUA=" + clsMain.SQLString(clsGlobal.gsUserID) + ",NGAYSUA=GETDATE()" + "  where HOADON =" + clsMain.SQLString(maHoaDon);
                try
                {
                    clsMain.ExecuteSQL(sqlUpdate_Datcoc);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(" Có lổi 2.2" + "\n" + ex.ToString(), "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

            //Luu khach vao phong
            sSQL = string.Empty;
            sSQL += "delete from CT_TTKHACHHANG_KHACHSAN where MA_HOADON=" + clsMain.SQLString(maHoaDon) + " and MA_BAN=" + clsMain.SQLString(maBan) + "\n";
            foreach (DataRow dr in dttmp.Rows)
            {
                DataTable dt = clsMain.ReturnDataTable("select MA_KH,CMND,TEN from KS_KHACHHANG where CMND=" + clsMain.SQLString(dr["CMND"].ToString()));
                sSQL += "Insert Into CT_TTKHACHHANG_KHACHSAN(MA_HOADON,MA_BAN,MA_KH)" + "\n";
                sSQL += "Values(";
                sSQL += clsMain.SQLString(maHoaDon) + ",";
                sSQL += clsMain.SQLString(maBan) + ",";
                sSQL += clsMain.SQLString(dt.Rows[0]["MA_KH"].ToString()) + ")";
                sSQL += "\n";
            }
            try
            {
                clsMain.ExecuteSQL(sSQL);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(" Có lổi 3." + "\n" + ex.ToString(), "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            funtions = 1;
            btnNhanphong.Text = "Nhận Phòng";
            btn_SoDoBan.Text = "Mở Phòng";
            lbListTable.Text = "Danh sách phòng:";
            pnShowHang.BringToFront();
            GetKhuVuc();
            GetSoDoBan();
            foreach (SimpleButton btn in pnThuNgan.Controls)
            {
                btn.Visible = true;
            }
        }

        private bool CheckInput()
        {
            DataView dv = (DataView)gridView1.DataSource;
            DataTable dttmp = dv.ToTable();
            dttmp.DefaultView.Sort = "CMND asc";
            dttmp = dttmp.DefaultView.ToTable();
            string tenhk = "";
            string cmnd = "";

            if (dttmp.Rows.Count > 0)
            {
                foreach (DataRow dr in dttmp.Rows)
                {
                    if (dr["TENKH"].ToString() == "" && dr["CMND"].ToString() == "")
                    {
                        break;
                    }
                    if (dr["TENKH"].ToString() == "" || dr["CMND"].ToString() == "")
                    {
                        XtraMessageBox.Show("Chưa nhập đầy đủ thông tin khách hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                    if (dr["TENKH"].ToString() == tenhk && dr["CMND"].ToString() == cmnd)
                    {
                        //DevExpress.XtraEditors.XtraMessageBox.Show("Trùng thông tin khách hàng.Kiểm tra lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //return false;
                    }
                    else
                    {
                        tenhk = dr["TENKH"].ToString();
                        cmnd = dr["CMND"].ToString();
                    }
                }

                //kiem tra ngay ra du kiem co trung voi lich dat khong
                DateTime dtiTu = (DateTime)dtpNgayvao.EditValue;
                DateTime dtiDen = (DateTime)dtpNgayra.EditValue;
                do
                {
                    string sql = "";
                    if (Madatphong_nhap  != "")
                    {
                        sql = "select * from KS_DATPHONG where trangthai=1 and (hoadon ='' or hoadon is null) and  maphong=" + clsMain.SQLString(maBan) + " and NGAYNHANPHONG<=" + clsMain.SQLString(cls_KP_RES.DateToString_Date(dtiTu)) + " and NGAYTRAPHONG>" + clsMain.SQLString(cls_KP_RES.DateToString_Date(dtiTu)) + " and MADATPHONG <>" + clsMain.SQLString(Madatphong_nhap);
                    }
                    else
                    {
                        sql = "select * from KS_DATPHONG where trangthai=1 and (hoadon ='' or hoadon is null) and  maphong=" + clsMain.SQLString(maBan) + " and NGAYNHANPHONG<=" + clsMain.SQLString(cls_KP_RES.DateToString_Date(dtiTu)) + " and NGAYTRAPHONG>" + clsMain.SQLString(cls_KP_RES.DateToString_Date(dtiTu));
                    }

                    if (clsMain.ReturnDataTable(sql).Rows.Count > 0)
                    {
                        XtraMessageBox.Show("Ngày lưu trú không đúng. Đã có người đặt trước", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dtpNgayra.Focus();
                        return false;
                    }
                    dtiTu = dtiTu.AddDays(1);
                }
                while (dtiTu < dtiDen);
                //
                return true;
            }
            else
            {
                XtraMessageBox.Show("Chưa nhập thông tin khách hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }

        private string GetNewCodeBill()
        {
            string _mahoadon = "";
            string ngay = cls_ConfigCashier.idCashier + DateTime.Now.Day.ToString("00") + DateTime.Now.Month.ToString("00") + DateTime.Now.Year.ToString("0000");
            DataTable dtMA = clsMain.ReturnDataTable("select [dbo].[fc_NewcodeHoaDon_KhachSan] ('" + ngay + "')");
            _mahoadon = dtMA.Rows[0][0].ToString();
            return _mahoadon;
        }

        private String CreateBarcode()
        {
            try
            {
                String MaBarCode = "";
                int newbarcode = 0;
                do
                {
                    newbarcode = 0;

                    Random rd = new Random(Convert.ToInt32(DateTime.Now.Ticks % 0x7FFFFFFF));
                    MaBarCode = string.Format("{0:0000000}", rd.Next(0, 9999999));
                    String sSQL1 = "";
                    sSQL1 += "SELECT MADATPHONG" + "\n";
                    sSQL1 += "FROM KS_DATPHONG" + "\n";
                    sSQL1 += "WHERE MADATPHONG = " + clsMain.SQLString(MaBarCode) + "\n";

                    if (clsMain.ReturnDataTable(sSQL1).Rows.Count > 0)
                        newbarcode = 1;
                }
                while (newbarcode == 1);

                return MaBarCode;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        private void btn_SoDoBan_Click(object sender, EventArgs e)
        {
            if (CheckOpenTable(maBan))
            {
                if (funtions == 10)
                    while (gv_SelectProduct.RowCount > 0)
                        gv_SelectProduct.DeleteRow(0);
                if (funtions == 8 || fastFoot != 0 || funtions == 6 || funtions == 200)
                    return;
                if (funtions == 1 && maBan != "")
                {
                    funtions = 2;
                    btn_SoDoBan.Text = "Sơ đồ phòng";
                    lbListTable.Text = "Danh sách món:";
                    pnShowHang.BringToFront();
                    GetNhomHang();
                    GetListMon();
                    SetDefaultBackgroundBtnInPn(pnThuNgan);
                    SetDefaultBackgroundBtnInPn(pnNghiepVu);
                    foreach (SimpleButton btn in pnThuNgan.Controls)
                    {
                        if (btn.Name == btn_SoDoBan.Name || btn.Name == btn_OrderSent.Name || btn.Name == btn_YeuCauThem.Name)
                        {
                            btn.Visible = true;
                        }
                        else
                        {
                            btn.Visible = false;
                        }
                    }
                    return;
                }
                if (funtions != 1)
                {
                    funtions = 1;
                    btn_SoDoBan.Text = "Mở Phòng";
                    lbListTable.Text = "Danh sách phòng:";
                    btnGiaBan2.Tag = 1;
                    pnShowHang.BringToFront();
                    GetKhuVuc();
                    GetSoDoBan();
                    SetDefaultBackgroundBtnInPn(pnThuNgan);
                    SetDefaultBackgroundBtnInPn(pnNghiepVu);
                    foreach (SimpleButton btn in pnThuNgan.Controls)
                    {
                        btn.Visible = true;
                    }
                }
            }
        }

        private void btn_OrderSent_Click(object sender, EventArgs e)
        {
            if (funtions == 8 || fastFoot != 0 || funtions == 6)
                return;
            if (sendOrder != 1)
                return;
            if (string.IsNullOrEmpty(clsGlobal.gsMaNVOfUserLogin))
            {
                XtraMessageBox.Show("Mã nhân viên rỗng,vui lòng đăng xuất và đăng nhập lại !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                pnBtnDeleteGv.Visible = false;
                int _if = 0;
                string maHDB = GetMaHoaDon(maBan);
                string nameTable = clsMain.ReturnDataTable("Select TEN_BAN From BAN Where MA_BAN = " + clsMain.SQLString(maBan) + "").Rows[0][0].ToString();
                while (gv_SelectProduct.RowCount > 0)
                {
                    List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                    _if = 1;
                    int j = 0;
                    string maBEP = "";
                    for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                    {
                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        maBEP = gv_SelectProduct.GetRowCellValue(0, gv_SelectProduct.Columns["MA_BEP"]).ToString();
                        dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                        if (maBEP == gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_BEP"]).ToString())
                        {
                            dmHangHoa.Ma_HoaDon = maHDB;
                            dmHangHoa.Ten_HoaDon = "Order Bếp";
                            dmHangHoa.TEN_BAN = nameTable;
                            dmHangHoa.MaQuay = cls_ConfigCashier.nameCashier;
                            dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                            dmHangHoa.STT = (++j).ToString();
                            dmHangHoa.MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                            dmHangHoa.TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                            dmHangHoa.SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                            dmHangHoa.THUE = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["THUE"]).ToString());
                            dmHangHoa.GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                            string repuest = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"]).ToString();
                            if (repuest == "a")
                                dmHangHoa.GHICHU = "";
                            else
                                dmHangHoa.GHICHU = repuest;
                            //Kiểm tra hàng hóa có in bếp ko
                            string sqlinbep = " SELECT ISNULL(IS_INBEP,0) AS IS_INBEP FROM HANGHOA WHERE MA_HANGHOA = " + dmHangHoa.MA_HANGHOA;
                            DataTable dtinbep = clsMain.ReturnDataTable(sqlinbep);
                            if (bool.Parse(dtinbep.Rows[0]["IS_INBEP"].ToString()) == true)
                                listDM_HangHoa.Add(dmHangHoa);

                            if (maHDB != null)
                            {
                                if (ChecBishInTable(maHDB, dmHangHoa.MA_HANGHOA) == false)
                                {
                                    string sql = "Insert into CT_HOADON_KHACHSAN( MA_HOADON, MA_HANGHOA,  SOLUONG, GIABAN, THUE)";
                                    sql += " values(" + "'" + maHDB + "'," + int.Parse(dmHangHoa.MA_HANGHOA) + "," + dmHangHoa.SOLUONG + "," + dmHangHoa.GIABAN + "," + dmHangHoa.THUE + ")";
                                    clsMain.ExecuteSQL(sql);
                                }
                                else
                                {
                                    string sql = "UPDATE CT_HOADON_KHACHSAN SET SOLUONG +=" + dmHangHoa.SOLUONG + "  WHERE MA_HOADON ='" + maHDB + "' and MA_HANGHOA =" + int.Parse(dmHangHoa.MA_HANGHOA) + "";
                                    clsMain.ExecuteSQL(sql);
                                }
                                string sqlOder = "Insert into DISPLAY_ORDER (MA_HANGHOA, MA_BAN, GIOORDER, SOLUONG, YEUCAU)";
                                sqlOder += " values(" + "" + int.Parse(dmHangHoa.MA_HANGHOA) + "," + int.Parse(maBan) + ", GETDATE()," + dmHangHoa.SOLUONG + ", N'" + dmHangHoa.GHICHU + "')";
                                clsMain.ExecuteSQL(sqlOder);
                            }
                            else
                                throw new Exception("Không thể lập hóa đơn cho phòng này");
                            // Delete product selected 
                            if (i != 0)
                            {
                                gv_SelectProduct.DeleteRow(i);
                                i--;
                            }
                        }
                    }
                    string print = GetIdKitchen(maBEP);
                    if (print == null)
                        XtraMessageBox.Show("Chưa chọn máy in cho mặt hàng này !", "Thông Báo ", MessageBoxButtons.OK);
                    gv_SelectProduct.DeleteRow(0);// Delete rows(0)
                    // Printer Order 
                    convert cvrt = new convert();
                    DataTable Orderbill = cvrt.ConvertToDataTable(listDM_HangHoa);
                    if (Orderbill.Rows.Count > 0)
                    {
                        KP_Report.Frm_Report1 frm = new KP_Report.Frm_Report1();
                        frm.ReportName = "rpt_OrderKaraoke";
                        frm.DataSource = Orderbill;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.PrinterName = print;
                        frm.IsPrint = true;
                        frm.ShowDialog();
                        frm.Dispose();
                    }
                }
                if (_if == 1)
                {
                    dt.Rows.Clear();
                    gr_SelectProduct.RefreshDataSource();
                    GetDishOfTable(GetMaHoaDon(maBan));
                }
                maMon = "";
                // return Lish table 
                sendOrder = 0;
                lbNameMon.Text = "";
                funtions = 1;
                btn_SoDoBan.Text = "Mở Phòng";
                lbListTable.Text = "Danh sách phòng:";
                pnShowHang.BringToFront();
                GetKhuVuc();
                GetSoDoBan();
                SetDefaultBackgroundBtnInPn(pnThuNgan);
                SetDefaultBackgroundBtnInPn(pnNghiepVu);
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    btn.Visible = true;
                }
                string mHD = GetMaHoaDon(maBan);
                if (funtions == 1 && mHD != null)
                {
                    DataTable dtcheck = clsMain.ReturnDataTable("select MA_BAN from CT_MOBAN_KHACHSAN where MA_HOADON='" + mHD + "'");
                    if (dtcheck.Rows.Count > 1)
                    {
                        for (int i = 0; i < dtcheck.Rows.Count; i++)
                        {
                            foreach (SimpleButton btnTable in pnTable_Mon.Controls)
                                if (btnTable.Name == dtcheck.Rows[i]["MA_BAN"].ToString())
                                    SetBackgroudOnClick(btnTable);
                        }
                    }
                }
            }
            catch (Exception es)
            {
                XtraMessageBox.Show(es.Message, "Thông Báo", MessageBoxButtons.OK);
            }
        }

        private bool ChecBishInTable(string _maHOADON, string maHH)
        {
            string sSQL = "";
            sSQL += "Select MA_CTHOADON" + "\n";
            sSQL += "From CT_HOADON_KHACHSAN" + "\n";
            sSQL += "Where MA_HOADON =" + clsMain.SQLString(_maHOADON) + "\n";
            sSQL += "And MA_HANGHOA =" + clsMain.SQLString(maHH);
            DataTable dish = clsMain.ReturnDataTable(sSQL);
            if (dish.Rows.Count > 0)
                return true;
            return false;
        }

        private string GetIdKitchen(string idKitchen)
        {
            string printKitchen = null;
            string sql = "select MAYINBEP from DM_BEP where MA_BEP ='" + idKitchen + "'";
            DataTable kitchen = clsMain.ReturnDataTable(sql);
            if (kitchen.Rows.Count > 0)
            {
                printKitchen = kitchen.Rows[0]["MAYINBEP"].ToString();
                return printKitchen;
            }
            else
                return printKitchen;
        }

        private void btn_YeuCauThem_Click(object sender, EventArgs e)
        {
            if (maMon == null || sendOrder != 1 || funtions == 6 || fastFoot != 0)
                return;
            SetBackgroundBtnInPn(pnThuNgan, pnNghiepVu);
            pnYeucauthem.BringToFront();
            SelectProduct();
            us_keyboard3.Left = panelControl5.Width / 2 - us_keyboard3.Width / 2;
            us_keyboard3.Top = panelControl5.Height / 2 - us_keyboard3.Height / 2;
            us_keyboard3.SetTextbox(txtYeuCauThem);
            txtYeuCauThem.Focus();
            lbkvYeuCauThem.Text = lbKhuVuc.Text;
            lbTableYeuCauThem.Text = lbNameTable.Text;
            lbMonYeuCauThem.Text = lbNameMon.Text;
            int size = panelControl5.Width;
        }

        private void SelectProduct()
        {
            string sql = "SELECT  * FROM YEUCAUTHEM ORDER BY  YEUCAU ";
            DataTable requestAdd = clsMain.ReturnDataTable(sql);
            AddButtonYeuCauThem(requestAdd);
        }

        private void AddButtonYeuCauThem(DataTable table)
        {
            pnYeuCau.Controls.Clear();
            int line = 0;
            int locateY = 7;
            int locateX = 10;
            if (table.Rows.Count <= 0)
                return;
            for (int i = 0; i < table.Rows.Count; i++)
            {
                line++;
                SimpleButton btn = new SimpleButton();
                btn.Name = table.Rows[i]["id"].ToString();
                btn.Text = table.Rows[i]["YEUCAU"].ToString();
                btn.Click += new EventHandler(btn_Click);
                btn.RightToLeft = RightToLeft.No;
                btn.Size = new Size(130, 70);
                if (line == 8)
                {
                    locateY += int.Parse(btn.Location.Y.ToString()) + 80;
                    locateX = 10;
                    line = 1;
                }
                else
                {
                    if (i != 0)
                        locateX += int.Parse(btn.Location.X.ToString()) + 140;
                }
                btn.Location = new Point(locateX, locateY);
                pnYeuCau.Controls.Add(btn);
            }
        }

        private void btn_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            txtYeuCauThem.Text += btn.Text + "," + " ";
            txtYeuCauThem.Focus();
            int textLength = txtYeuCauThem.Text.Length;
            txtYeuCauThem.SelectionStart = textLength;
            txtYeuCauThem.SelectionLength = 0;

        }

        private void simpleButton24_Click(object sender, EventArgs e)
        {
            try
            {
                pnYeuCau.AutoScrollPosition = new Point(0, pnYeuCau.VerticalScroll.Value - pnYeuCau.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void simpleButton23_Click(object sender, EventArgs e)
        {
            try
            {
                pnYeuCau.AutoScrollPosition = new Point(0, pnYeuCau.VerticalScroll.Value + pnYeuCau.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void txtYeuCauThem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    int maHH = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                    if (maHH == int.Parse(maMon))
                    {
                        gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"], txtYeuCauThem.Text);
                        break;
                    }
                }
                txtYeuCauThem.Text = "";
                pnShowHang.BringToFront();
                SetDefaultBackgroundBtnInPn(pnThuNgan);
                SetDefaultBackgroundBtnInPn(pnNghiepVu);
            }
        }

        private void btn_GhepBan_Click(object sender, EventArgs e)
        {
            if (funtions == 5)
            {
                btn_GhepBan.Text = "Ghép Phòng";
                SetDefautlBackgroud(btn_GhepBan);
                funtions = 1;
                return;
            }
            if (funtions == 8 || fastFoot != 0)
                return;
            if (funtions == 1 && maBan != "")
            {
                string maHOADON = GetMaHoaDon(maBan);
                if (maHOADON == null)
                    return;
                funtions = 5;
                btn_GhepBan.Text = "  Hủy \nGhép Phòng";
                lbListTable.Text = "Ghép Phòng =>";
                SetBackgroundBtnInPn(pnThuNgan, pnNghiepVu);
            }
        }

        private void btn_ChuyenBan_Click(object sender, EventArgs e)
        {
            if (funtions == 11)
            {
                btnChuyenBan.Text = "Chuyển Phòng";
                SetDefautlBackgroud(btnChuyenBan);
                maBanGhep = "";
                funtions = 1;
                foreach (SimpleButton btn in pnNghiepVu.Controls)
                {
                    if (btn.Name != btnChuyenBan.Name)
                    {
                        btn.Enabled = true;
                    }
                }
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    if (btn.Name != btnChuyenBan.Name)
                    {
                        btn.Enabled = true;
                    }
                }
                return;
            }
            if (funtions != 1 || fastFoot != 0)
                return;
            if (funtions == 1)
            {
                if (maBan == "")
                    return;
                if (GetMaHoaDon(maBan) == "")
                    return;
                btnChuyenBan.Text = "Hủy\nChuyển Phòng";
                SetBackgroundBtnInPn(pnNghiepVu, pnThuNgan);
                txtmaKVBanGoc.Text = lbKhuVuc.Text;
                txtmaBanGoc.Text = lbNameTable.Text;
                funtions = 11;
                foreach (SimpleButton btn in pnNghiepVu.Controls)
                {
                    if (btn.Name != btnChuyenBan.Name)
                    {
                        btn.Enabled = false;
                    }
                }
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    if (btn.Name != btnChuyenBan.Name)
                    {
                        btn.Enabled = false;
                    }
                }
            }
        }

        private void btn_InLaiHoaDon_Click(object sender, EventArgs e)
        {
            pnBtnDeleteGv.Visible = false;
            if (funtions == 9 || funtions == 10)
                return;
            if (exitReprint == true)
            {
                SetDefautlBackgroud(btn_InLaiHoaDon);
                btnThoatReprint_Click(null, null);
            }
            else
            {
                if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
                {
                    bool check;
                    Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                    fr.ShowDialog();
                    check = fr.kt;
                    fr.Dispose();
                    if (check == false)
                        return;
                }

                string sSQL = "exec SP_DANHSACHHOADON_KHACHSAN";
                DataTable dts = clsMain.ReturnDataTable(sSQL);
                gr_billPaymented.DataSource = dts;
                gr_billPaymented.RefreshDataSource();

                SetBackgroudOnClick(btn_InLaiHoaDon);
                IN.Visible = true;
                HUY.Visible = false;
                LAYLAI.Visible = false;
                pnInlaihoadon.BringToFront();

                foreach (SimpleButton btn in pnNghiepVu.Controls)
                {
                    if (btn.Name != btn_InLaiHoaDon.Name)
                    {
                        btn.Visible = false;
                    }
                }
                exitReprint = true;
            }
        }

        private void gvBillPaymented_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == SSTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
                lbTongSoRow.Text = gvBillPaymented.RowCount.ToString("00");
                labelControl45.Text = (gvBillPaymented.FocusedRowHandle + 1).ToString("00");
            }
        }

        public string idBill = "";
        private void gvBillPaymented_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            try
            {
                idBill = gvBillPaymented.GetRowCellValue(gvBillPaymented.FocusedRowHandle, gvBillPaymented.Columns["MA_HOADON"]).ToString();
                GetTienGio(idBill);
                GetDishOfTable(idBill);

                string sSQL;
                if (e.Column == IN)
                {
                    sSQL = "";
                    sSQL += "Insert into HOADON_INLAI_KHACHSAN(MA_HOADON,NHANVIEN_INLAI,GIO_INLAI)" + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(idBill) + ",";
                    sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                    sSQL += "GETDATE())";
                    clsMain.ExecuteSQL(sSQL);

                    sSQL = "";
                    sSQL += "Select ROW_NUMBER() over (order by (select 1)) as STT,N'PHIẾU THANH TOÁN \n (IN LẠI)' as Ten_HoaDon,hd.MA_HOADON ,hd.NGAYINHOADON as ngayHD,hd.GIAITHICH,hd.CHECKIN,hd.CHECKOUT,hd.TONGTIENPHONG AS TienGio,hd.TONGTIEN AS Tong_Cong,hd.GIAMGIA as Giam_Gia,hd.TIEN_KHACHTRA,hd.TIEN_TRAKHACH,ISNULL(hd.PHIPHUCVU,0) as PHIPHUCVU,ISNULL(hd.TIENDATCOC,0) as TIENDATCOC," + "\n";
                    sSQL += "(Select TENNHANVIEN From DM_NHANVIEN where MANHANVIEN=hd.MANVINHOADON ) as TenNV,(Select TEN_QUAY From QUAY where MA_QUAY=hd.MA_QUAY ) as Ma_Quay,(Select TOP 1 TEN_BAN From BAN where MA_BAN IN (SELECT MA_BAN FROM CT_MOBAN_KHACHSAN  WHERE MA_HOADON =hd .MA_HOADON ) )  as TEN_BAN," + "\n";
                    sSQL += "B.MA_CTHOADON,B.GIABAN,B.SOLUONG,(B.SOLUONG * B.GIABAN) as Thanh_Tien,C.TEN_HANGHOA" + "\n";
                    sSQL += "From HOADON_KHACHSAN hd" + "\n";
                    sSQL += "LEFT JOIN CT_HOADON_KHACHSAN  B ON  hd.MA_HOADON =B.MA_HOADON " + "\n";
                    sSQL += "LEFT JOIN HANGHOA  C ON  B.MA_HANGHOA  =C.MA_HANGHOA   " + "\n";
                    sSQL += "Where hd.MA_HOADON =" + clsMain.SQLString(idBill);
                    DataTable dtReport = clsMain.ReturnDataTable(sSQL);

                    Frm_Report1 frm = new Frm_Report1();
                    frm.ReportName = "rpt_inhoadonKhachSan";
                    frm.DataSource = dtReport;
                    frm.WindowState = FormWindowState.Minimized;
                    frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                    frm.IsPrint = true;
                    frm.ShowDialog();
                    frm.Dispose();
                    dt.Rows.Clear();
                    idBill = "";
                    lbTiengio.Text = lbDichvu.Text = lbPhuThu.Text = lbGiamGia.Text = lbTatol.Text = "0";
                    return;

                }
            }
            catch
            {
            }
        }

        private void GetTienGio(string maHoaDon)
        {
            string sSQL = "";
            sSQL += "Select TONGTIENPHONG from HOADON_KHACHSAN where MA_HOADON=" + clsMain.SQLString(maHoaDon);
            DataTable dtTemp = clsMain.ReturnDataTable(sSQL);

            if (dtTemp.Rows.Count > 0)
            {
                if (dtTemp.Rows[0]["TONGTIENPHONG"].ToString() != "")
                    lbTiengio.Text = string.Format("{0:#,###0}", float.Parse(dtTemp.Rows[0]["TONGTIENPHONG"].ToString()));
            }
            lbTatol.Text = lbTiengio.Text;
        }

        private void btn_upbill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = gvBillPaymented.FocusedRowHandle - 1;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
                if (gvBillPaymented.FocusedRowHandle == 0)
                {
                    btn_up.Enabled = false;
                    btn_UpFirst.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btn_UpTopBill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = 0;
                btn_UpFirst.Enabled = false;
                btn_up.Enabled = false;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnDowBill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = gvBillPaymented.FocusedRowHandle + 1;
                btn_up.Enabled = true;
                btn_UpFirst.Enabled = true;

                if (gvBillPaymented.FocusedRowHandle == gvBillPaymented.RowCount - 1)
                {
                    btn_Dow.Enabled = false;
                    btn_DowLast.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnDowLastBill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = gvBillPaymented.RowCount - 1;
                btn_UpFirst.Enabled = true;
                btn_up.Enabled = true;
                btn_Dow.Enabled = false;
                btn_DowLast.Enabled = false;
            }
            catch
            {
            }
        }

        public bool exitReprint = false;
        private void btnThoatReprint_Click(object sender, EventArgs e)
        {
            pnInlaihoadon.SendToBack();
            while (gvBillPaymented.RowCount > 0)
                gvBillPaymented.DeleteRow(0);
            while (gv_SelectProduct.RowCount > 0)
                gv_SelectProduct.DeleteRow(0);
            foreach (SimpleButton btn in pnNghiepVu.Controls)
            {
                btn.Visible = true;
            }
            foreach (SimpleButton btn in pnThuNgan.Controls)
            {
                btn.Visible = true;
            }
            exitReprint = false;
            dt.Clear();
            idBill = "";
            lbTiengio.Text = lbDichvu.Text = lbPhuThu.Text = lbGiamGia.Text = lbTatol.Text = "0";
        }

        private void btn_LayLaiHoaDon_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
                return;
            if (exitReprint == true)
            {
                SetDefautlBackgroud(btn_LayLaiHoaDon);
                btnThoatReprint_Click(null, null);
            }
            else
            {
                if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
                {
                    bool check;
                    Frm_ConfigFuntion frm = new Frm_ConfigFuntion();
                    frm.ShowDialog();
                    check = frm.kt;
                    frm.Dispose();
                    if (check == false)
                        return;
                }

                SetBackgroudOnClick(btn_LayLaiHoaDon);
                if (idBill != "" && exitReprint == true)
                {
                    string sqlInserter = "SET DATEFORMAT DMY Update HOADON_KHACHSAN set ISPAYMENT=0, GIAMGIA=" + 0 + ", TIEN_TRAKHACH =" + 0 + ", TIEN_KHACHTRA =" + 0 + ", TONGTIEN=" + 0 + "  where MA_HOADON='" + idBill + "'";
                    clsMain.ExecuteSQL(sqlInserter);
                    DataTable dtTables = clsMain.ReturnDataTable("Select MA_BAN From CT_MOBAN_KHACHSAN where MA_HOADON ='" + idBill + "'");
                    if (dtTables.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtTables.Rows.Count; i++)
                        {
                            string sql = "Update BAN SET ISMO_BAN=1 where MA_BAN =" + int.Parse(dtTables.Rows[i]["MA_BAN"].ToString());
                            clsMain.ExecuteSQL(sql);
                        }
                    }
                    string sqlTruTon = string.Format("Exec SP_TRUKHO_DINHLUONG @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 0);
                    if (clsMain.ExecuteSQL(sqlTruTon))
                    {

                    }
                    if (clsMain.ExecuteSQL(string.Format("Exec SP_TRUKHO_HOADON @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 0)))
                    {

                    }
                    //Finish
                    dt.Rows.Clear();
                    lbTiengio.Text = lbDichvu.Text = lbPhuThu.Text = lbGiamGia.Text = lbTatol.Text = "0";
                    XtraMessageBox.Show("Đã lấy lại hóa đơn thành công !");
                    pnInlaihoadon.SendToBack();
                    while (gvBillPaymented.RowCount > 0)
                        gvBillPaymented.DeleteRow(0);
                    while (gv_SelectProduct.RowCount > 0)
                        gv_SelectProduct.DeleteRow(0);
                    foreach (SimpleButton btn in pnNghiepVu.Controls)
                    {
                        btn.Enabled = true;
                    }
                    exitReprint = false;
                    idBill = "";
                    return;
                }
                try
                {
                    pnInlaihoadon.BringToFront();
                    string sql = " select top 20 hd.MA_HOADON, hd.NGAYTAO, isnull(hd.TIENDATCOC,0) as TIENDATCOC, hd.TONGTIEN  from HOADON_KHACHSAN hd inner join CT_MOBAN_KHACHSAN ctmb on hd.MA_HOADON = ctmb.MA_HOADON Left join BAN b on ctmb.MA_BAN = b.MA_BAN left join KHUVUC kv on kv.MA_KHUVUC = b.MA_KHUVUC where ISPAYMENT = 1 and kv.MODE = 1  order by NGAYTAO desc";
                    DataTable dts = clsMain.ReturnDataTable(sql);
                    gr_billPaymented.DataSource = dts;
                    gr_billPaymented.RefreshDataSource();

                    foreach (SimpleButton btn in pnNghiepVu.Controls)
                    {
                        if (btn.Name != btn_LayLaiHoaDon.Name)
                        {
                            btn.Visible = false;
                        }
                    }
                    exitReprint = true;
                }
                catch
                {
                }

            }
        }

        private void btnGiaBan2_Click(object sender, EventArgs e)
        {
            if (gv_SelectProduct.RowCount <= 0 || maMon == null)
                return;
            if (btnGiaBan2.Tag.ToString() == "1")
                return;

            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }

            #region Kiểm tra hàng hóa là thời giá hay ko,nếu đúng thì return
            string sSQL = "select SUAGIA from HANGHOA where MA_HANGHOA = " + maMon + "";
            DataTable dtcheck = clsMain.ReturnDataTable(sSQL);
            if (dtcheck.Rows.Count > 0)
            {
                if (bool.Parse(dtcheck.Rows[0]["SUAGIA"].ToString()))
                {
                    MessageBox.Show("Đây là hàng hóa theo thời giá", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            #endregion

            if (btnGiaBan2.Text == "Bán giá 1")
            {
                sSQL = "select GIABAN1 as GIABAN from HANGHOA where MA_HANGHOA = " + maMon + "";
                btnGiaBan2.Text = "Bán giá 2";
            }
            else
            {
                sSQL = "select GIABAN2 as GIABAN from HANGHOA where MA_HANGHOA = " + maMon + "";
                btnGiaBan2.Text = "Bán giá 1";
            }

            DataTable dtGia = clsMain.ReturnDataTable(sSQL);
            if (dtGia.Rows.Count > 0)
            {
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    if (gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString() == maMon)
                    {
                        gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"], dtGia.Rows[0][0].ToString());
                    }
                }
                if (gv_SelectProduct.RowCount > 0)
                    TotalMoney();
                gr_SelectProduct.RefreshDataSource();
            }
        }

        private void btn_DatBan_Click(object sender, EventArgs e)
        {
            //if (fastFoot != 0 || funtions == 10)
            //    return;
            //SetBackgroundBtnInPn(pnNghiepVu, pnThuNgan);
            //if (funtions == 9)
            //{
            //    // Check Find Dishes
            //    selectDishes = 0;
            //    pnDatphong.BringToFront();
            //    return;
            //}
            //else
            //    funtions = 9;
            ////Check Find Tables 
            //selectTable = 1;
            //dt.Rows.Clear();
            //gr_SelectProduct.RefreshDataSource();
            //pnDatphong.BringToFront();
            //txtTenKhacHang.Focus();
            //us_keyboard2.SetTextbox(txtDBTenKH);
            ////set time 
            //for (int i = 0; i <= 23; i++)
            //    cbHH.Properties.Items.Add(i);
            //cbHH.Text = "00";
            ////
            ////Set mins
            //for (int j = 0; j <= 59; j++)
            //    cbMM.Properties.Items.Add(j);
            //cbMM.Text = "00";
            ////
            ////set secsion 
            //for (int t = 1; t <= 24; t++)
            //    cbSS.Properties.Items.Add(t);
            //cbSS.Text = "00";
            //txtngay.Text = clsMain.GetServerDate().ToShortDateString();

            if (funtions != 1)
                return;
            Frm_DatPhong_Khachsan frm = new Frm_DatPhong_Khachsan();
            frm.ShowDialog();
            frm.Dispose();
        }

        private void txtNhapTienTra_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != ',')
                e.Handled = true;
        }

        private void btn_SelectDishes_Click(object sender, EventArgs e)
        {
            funtions = 2;
            pnShowHang.BringToFront();
            btn_SoDoBan.Text = "Mở Phòng";
            lbListTable.Text = "Danh sách phòng:";
            GetNhomHang();
            GetListMon();
            foreach (SimpleButton btn in pnKV_Mon.Controls)
            {
                if (btn.Name == maNH.ToString())
                {
                    SetBackgroudOnClick(btn);
                }
            }
            funtions = 9;
            selectDishes = 1;
        }

        private void btn_HuyMon_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
                return;
            if (maBan == "")
                return;
            if (CheckOpenTable(maBan) == false)
                return;
            if (exitReprint == true)
            {
                SetDefautlBackgroud(btn_ExitDishe);
                ExitDivideTable();
            }
            else
            {
                if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
                {
                    bool check;
                    Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                    fr.ShowDialog();
                    check = fr.kt;
                    fr.Dispose();
                    if (check == false)
                        return;
                }

                SetBackgroudOnClick(btn_ExitDishe);
                pnInforTablecheck.Visible = false;
                pnHuymon.BringToFront();
                int w = (pn_GroupTable.Width - 110) / 2;
                panelControl15.Width = w;
                pn_TableTow.Width = w;
                List<DM_ListDisheTableOne> lisHangHoa = new List<DM_ListDisheTableOne>();
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    DM_ListDisheTableOne dmHangHoa = new DM_ListDisheTableOne();
                    dmHangHoa._MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                    dmHangHoa._TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                    dmHangHoa._SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                    dmHangHoa._GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                    dmHangHoa._MA_BEP = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_BEP"]).ToString());
                    dmHangHoa._Thanh_Tien = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"]).ToString());
                    dmHangHoa._GHICHU = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"]).ToString();
                    lisHangHoa.Add(dmHangHoa);
                }
                convert cvrt = new convert();
                divide_Table1 = cvrt.ConvertToDataTable(lisHangHoa);
                gr_divide_Table_1.DataSource = divide_Table1;
                gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.RowCount - 1;

                foreach (SimpleButton btn in pnNghiepVu.Controls)
                {
                    if (btn.Name == btn_ExitDishe.Name)
                    {
                        btn.Visible = true;
                    }
                    else
                    {
                        btn.Visible = false;
                    }
                }
                exitReprint = true;
            }
        }

        private void btnTachPhong_Click(object sender, EventArgs e)
        {
            if (funtions != 1 || fastFoot != 0)
                return;
            if (funtions == 1)
            {
                // Check Table Opened or table paymented
                if (maBan == "")
                    return;
                if (GetMaHoaDon(maBan) == null)
                    return;
                SetBackgroundBtnInPn(pnThuNgan, pnNghiepVu);
                txtmaKVBanGoc.Text = lbKhuVuc.Text;
                txtmaBanGoc.Text = lbNameTable.Text;
                funtions = 6;
                if (dt.Rows.Count > 0 && maBanGhep != "")
                {
                    List<DM_ListDisheTableOne> lisHangHoa = new List<DM_ListDisheTableOne>();
                    //add gridview
                    for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                    {
                        DM_ListDisheTableOne dmHangHoa = new DM_ListDisheTableOne();
                        dmHangHoa._MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                        dmHangHoa._TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                        dmHangHoa._SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                        dmHangHoa._GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                        dmHangHoa._Thanh_Tien = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"]).ToString());
                        dmHangHoa._GHICHU = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"]).ToString();
                        lisHangHoa.Add(dmHangHoa);
                    }
                    convert cvrt = new convert();
                    divide_Table1 = cvrt.ConvertToDataTable(lisHangHoa);
                    gr_divide_Table_1.DataSource = divide_Table1;
                    gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.RowCount - 1;
                    //
                    pnHuymon.BringToFront();
                    //Inbox Table
                    int w = (pn_GroupTable.Width / 2) - pnUpDow.Width;
                    pn_TableTow.Width = w + 30;
                    pn_TableShare.Width = w + 30;
                }
                else
                {
                }
            }
        }

        private void btn_OkThrough_Click(object sender, EventArgs e)
        {
            try
            {
                if (gv_divide_Table_2.RowCount <= 0)
                    return;
                if (funtions == 1)
                {
                    if (funtions == 1 && maBan != "")
                    {
                        String sSQL = "";
                        if (CheckOpenTable(maBan) == false)
                            return;
                        string maHoaDon = GetMaHoaDon(maBan);
                        for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                        {
                            string idDisher = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                            int soLuong = int.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());

                            sSQL = "";
                            sSQL += "EXEC SP_InsertHuyDichVu " + clsMain.SQLString(maHoaDon) + ",";
                            sSQL += clsMain.SQLString(maBan) + ",";
                            sSQL += clsMain.SQLString(idDisher) + ",";
                            sSQL += clsMain.SQLString(soLuong.ToString()) + ",";
                            sSQL += clsMain.SQLString(clsGlobal.gsUserID);
                            clsMain.ExecuteSQL(sSQL);

                            string sqlCTHD = "Update CT_HOADON_KHACHSAN set SOLUONG = SOLUONG - " + soLuong + " where MA_HOADON ='" + maHoaDon + "' and MA_HANGHOA='" + idDisher + "'";
                            clsMain.ExecuteSQL(sqlCTHD);
                            string sqlDelete = "Delete from CT_HOADON_KHACHSAN where SOLUONG = 0 and  MA_HOADON ='" + maHoaDon + "' and MA_HANGHOA='" + idDisher + "'";
                            clsMain.ExecuteSQL(sqlDelete);
                        }
                        string sTen_ban = "";
                        sSQL = "";
                        sSQL += "Select TEN_BAN From BAN Where MA_BAN=" + clsMain.SQLString(maBan) + "\n";
                        DataTable dtb = clsMain.ReturnDataTable(sSQL);
                        if (dtb.Rows.Count > 0)
                        {
                            sTen_ban = dtb.Rows[0]["TEN_BAN"].ToString();
                        }
                        while (gv_divide_Table_2.RowCount > 0)
                        {
                            List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                            int j = 0;
                            string maBEP = "";
                            for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                            {
                                DM_HangHoa dmHangHoa = new DM_HangHoa();
                                maBEP = gv_divide_Table_2.GetRowCellValue(0, gv_divide_Table_2.Columns["__MA_BEP"]).ToString();
                                if (maBEP == gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_BEP"]).ToString())
                                {
                                    dmHangHoa.Ma_HoaDon = maHoaDon;
                                    dmHangHoa.Ten_HoaDon = "Hủy Order Bếp";
                                    dmHangHoa.MaBan = maBan;
                                    dmHangHoa.TEN_BAN = sTen_ban;
                                    dmHangHoa.Ngay = string.Format("{0:dd/MM/yyyy hh:mm:ss}", DateTime.Now);
                                    dmHangHoa.MaQuay = cls_ConfigCashier.nameCashier;
                                    dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                                    dmHangHoa.STT = (++j).ToString();
                                    dmHangHoa.MA_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                                    dmHangHoa.TEN_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__TEN_HANGHOA"]).ToString();
                                    dmHangHoa.SOLUONG = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                                    //Kiểm tra hàng hóa có in bếp ko
                                    string sql = " SELECT ISNULL(IS_INBEP,0) AS IS_INBEP FROM HANGHOA WHERE MA_HANGHOA = " + dmHangHoa.MA_HANGHOA;
                                    DataTable dtinbep = clsMain.ReturnDataTable(sql);
                                    if (bool.Parse(dtinbep.Rows[0]["IS_INBEP"].ToString()) == true)
                                        listDM_HangHoa.Add(dmHangHoa);
                                    if (i != 0)
                                    {
                                        gv_divide_Table_2.DeleteRow(i);
                                        i--;
                                    }
                                    DeleteOrder(int.Parse(dmHangHoa.MA_HANGHOA), int.Parse(maBan));
                                }
                            }
                            gv_divide_Table_2.DeleteRow(0);// Delete rows(0)
                            string print = GetIdKitchen(maBEP);
                            if (print == null)
                                XtraMessageBox.Show("Chưa chọn máy in cho mặt hàng này !", "Thông Báo ", MessageBoxButtons.OK);
                            // Printer Order 
                            convert cvrt = new convert();
                            DataTable Orderbill = cvrt.ConvertToDataTable(listDM_HangHoa);
                            if (Orderbill.Rows.Count > 0)
                            {
                                KP_Report.Frm_Report1 frm = new KP_Report.Frm_Report1();
                                frm.ReportName = "rpt_OrderKaraoke";
                                frm.DataSource = Orderbill;
                                frm.WindowState = FormWindowState.Minimized;
                                frm.PrinterName = print;
                                frm.IsPrint = true;
                                frm.ShowDialog();
                                frm.Dispose();
                            }
                        }
                    }
                    ExitDivideTable();
                    return;
                }
                // if you throw table1 to table 2
                if (gv_divide_Table_1.RowCount <= 0)
                {
                    if (CheckOpenTable(maBanGhep) == false)
                    {
                        string sql = "Update BAN SET ISMO_BAN=1 where MA_BAN =" + int.Parse(maBanGhep) + "";
                        clsMain.ExecuteSQL(sql);
                        string maHD = GetMaHoaDon(maBan);
                        string sqlCTMOBAN = " Update CT_MOBAN_KHACHSAN SET MA_BAN =" + int.Parse(maBanGhep) + " where MA_HOADON='" + maHD + "'";
                        clsMain.ExecuteSQL(sqlCTMOBAN);
                        string sqlpm = "Update BAN SET ISMO_BAN=0 where MA_BAN =" + int.Parse(maBan) + "";
                        clsMain.ExecuteSQL(sqlpm);
                        ExitDivideTable();
                        divide_Table1.Rows.Clear();
                        divide_Table2.Rows.Clear();
                        return;
                    }
                    else
                    {
                        CanceTable(maBan);
                        string maHD = GetMaHoaDon(maBanGhep);
                        for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                        {
                            DM_ListDisheTableTwo dmHangHoa = new DM_ListDisheTableTwo();
                            dmHangHoa.__MA_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                            dmHangHoa.__SOLUONG = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                            dmHangHoa.__GIABAN = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__GIABAN"]).ToString());

                            if (maHD != null)
                            {
                                if (ChecBishInTable(maHD, dmHangHoa.__MA_HANGHOA) == false)
                                {
                                    string sql = "SET DATEFORMAT DMY Insert into CT_HOADON_KHACHSAN( MA_HOADON, MA_HANGHOA,  SOLUONG, GIABAN)";
                                    sql += " values(" + "'" + maHD + "'," + int.Parse(dmHangHoa.__MA_HANGHOA) + "," + dmHangHoa.__SOLUONG + "," + dmHangHoa.__GIABAN + ")";
                                    clsMain.ExecuteSQL(sql);
                                }
                                else
                                {
                                    string sql = "UPDATE CT_HOADON_KHACHSAN SET SOLUONG +=" + dmHangHoa.__SOLUONG + "  WHERE MA_HOADON ='" + maHD + "' and MA_HANGHOA =" + int.Parse(dmHangHoa.__MA_HANGHOA) + "";
                                    clsMain.ExecuteSQL(sql);
                                }
                            }
                            else
                                throw new Exception("Lỗi update hàng hóa !");
                        }
                        ExitDivideTable();
                        divide_Table1.Rows.Clear();
                        divide_Table2.Rows.Clear();
                        return;
                    }
                }
                else
                {
                    // if you divide table1 to table 2
                    if (CheckOpenTable(maBanGhep) == false)
                    {
                        // is Open table 
                        string sql = "Update BAN SET ISMO_BAN=1 where MA_BAN =" + int.Parse(maBanGhep);
                        clsMain.ExecuteSQL(sql);
                        string maHoaDon = GetNewCodeBill();
                        string sqlInserter = "SET DATEFORMAT DMY Insert into HOADON_KHACHSAN(MA_HOADON, NGAYTAO, CHECKIN)";
                        sqlInserter += " values(" + "'" + maHoaDon + "',CONVERT(datetime, GetDate(), 121),CONVERT(datetime, GetDate(), 121))";
                        clsMain.ExecuteSQL(sqlInserter);
                        string sqlCTMOBAN = "SET DATEFORMAT DMY Insert into CT_MOBAN_KHACHSAN(MA_HOADON, MA_BAN)";
                        sqlCTMOBAN += " values(" + "'" + maHoaDon + "'," + int.Parse(maBanGhep) + ")";
                        clsMain.ExecuteSQL(sqlCTMOBAN);
                        //  
                        for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                        {
                            DM_HangHoa dmHangHoa = new DM_HangHoa();
                            dmHangHoa.MA_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                            dmHangHoa.SOLUONG = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                            dmHangHoa.GIABAN = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__GIABAN"]).ToString());

                            if (maHoaDon != null)
                            {
                                if (ChecBishInTable(maHoaDon, dmHangHoa.MA_HANGHOA) == false)
                                {
                                    string sqlc = "SET DATEFORMAT DMY Insert into CT_HOADON_KHACHSAN( MA_HOADON, MA_HANGHOA,  SOLUONG, GIABAN)";
                                    sqlc += " values(" + "'" + maHoaDon + "'," + int.Parse(dmHangHoa.MA_HANGHOA) + "," + dmHangHoa.SOLUONG + "," + dmHangHoa.GIABAN + ")";
                                    clsMain.ExecuteSQL(sqlc);
                                }
                                else
                                {
                                    string sqlt = "UPDATE CT_HOADON_KHACHSAN SET SOLUONG +=" + dmHangHoa.SOLUONG + "  WHERE MA_HOADON ='" + maHoaDon + "' and MA_HANGHOA =" + int.Parse(dmHangHoa.MA_HANGHOA) + "";
                                    clsMain.ExecuteSQL(sqlt);
                                }
                            }
                            else
                                throw new Exception("Tách phòng lỗi !");
                        }
                    }
                    else
                    {
                        for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                        {
                            DM_HangHoa dmHangHoa = new DM_HangHoa();
                            dmHangHoa.MA_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                            dmHangHoa.SOLUONG = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                            dmHangHoa.GIABAN = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__GIABAN"]).ToString());
                            string maHDB = GetMaHoaDon(maBanGhep);
                            if (maHDB != null)
                            {
                                if (ChecBishInTable(maHDB, dmHangHoa.MA_HANGHOA) == false)
                                {
                                    string sqlc = "SET DATEFORMAT DMY Insert into CT_HOADON_KHACHSAN( MA_HOADON, MA_HANGHOA,  SOLUONG, GIABAN)";
                                    sqlc += " values(" + "'" + maHDB + "'," + int.Parse(dmHangHoa.MA_HANGHOA) + "," + dmHangHoa.SOLUONG + "," + dmHangHoa.GIABAN + ")";
                                    clsMain.ExecuteSQL(sqlc);
                                }
                                else
                                {
                                    string sqlt = "UPDATE CT_HOADON_KHACHSAN SET SOLUONG +=" + dmHangHoa.SOLUONG + "  WHERE MA_HOADON ='" + maHDB + "' and MA_HANGHOA =" + int.Parse(dmHangHoa.MA_HANGHOA) + "";
                                    clsMain.ExecuteSQL(sqlt);
                                }
                            }
                            else
                                throw new Exception("Tách phòng lỗi !");
                        }
                    }
                    //ReUpdate table 1
                    string idBill1 = GetMaHoaDon(maBan);
                    string sqldelte = "Delete CT_HOADON_KHACHSAN where MA_HOADON ='" + idBill1 + "'";
                    clsMain.ExecuteSQL(sqldelte);
                    for (int i = 0; i < gv_divide_Table_1.RowCount; i++)
                    {
                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        dmHangHoa.MA_HANGHOA = gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString();
                        dmHangHoa.SOLUONG = decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                        dmHangHoa.GIABAN = decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_GIABAN"]).ToString());

                        if (idBill1 != null)
                        {
                            if (ChecBishInTable(idBill1, dmHangHoa.MA_HANGHOA) == false)
                            {
                                string sqlc = "SET DATEFORMAT DMY Insert into CT_HOADON_KHACHSAN( MA_HOADON, MA_HANGHOA,  SOLUONG, GIABAN)";
                                sqlc += " values(" + "'" + idBill1 + "'," + int.Parse(dmHangHoa.MA_HANGHOA) + "," + dmHangHoa.SOLUONG + "," + dmHangHoa.GIABAN + ")";
                                clsMain.ExecuteSQL(sqlc);
                            }
                            else
                            {
                                string sqlt = "UPDATE CT_HOADON_KHACHSAN SET SOLUONG +=" + dmHangHoa.SOLUONG + "  WHERE MA_HOADON ='" + idBill1 + "' and MA_HANGHOA =" + int.Parse(dmHangHoa.MA_HANGHOA) + "";
                                clsMain.ExecuteSQL(sqlt);
                            }
                        }
                        else
                            throw new Exception("Tách phòng lỗi !");
                    }
                }
                divide_Table1.Rows.Clear();
                divide_Table2.Rows.Clear();
                ExitDivideTable();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK);
            }
        }

        private void DeleteOrder(int maHH, int maBan)
        {
            string sql = "Delete From DISPLAY_ORDER where MA_HANGHOA=" + maHH + " and MA_BAN=" + maBan + "";
            clsMain.ReturnDataTable(sql);
        }

        private void btn_ESCThroughtTable_Click(object sender, EventArgs e)
        {
            try
            {
                ExitDivideTable();
            }
            catch
            {
            }
        }

        private void gv_divide_Table_1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == _SSTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }

            if (gv_SelectProduct.RowCount <= 0)
                lbTatol.Text = lbDichvu.Text = "0";
            if (gv_SelectProduct.RowCount > 0)
            {
                lb_RowsCount.Text = gv_SelectProduct.RowCount.ToString();
                lb_STT.Text = (gv_SelectProduct.FocusedRowHandle + 1).ToString();
            }
            if (gv_SelectProduct.RowCount > 0 && gv_SelectProduct.FocusedRowHandle == gv_SelectProduct.RowCount - 1)
            {
                btn_Dow.Enabled = false;
                btn_DowLast.Enabled = false;
            }
            else
            {
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
            }
            if (gv_SelectProduct.RowCount > 0 && gv_SelectProduct.FocusedRowHandle == 0)
            {
                btn_UpFirst.Enabled = false;
                btn_up.Enabled = false;
            }
            else
            {
                btn_UpFirst.Enabled = true;
                btn_up.Enabled = true;
            }
        }

        private void gv_divide_Table_2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == __STT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
        }

        private void btn_DowDivide_Click(object sender, EventArgs e)
        {
            try
            {
                gv_divide_Table_1.Focus();
                gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.FocusedRowHandle + 1;
                btn_UpDivide.Enabled = true;
                if (gv_divide_Table_1.FocusedRowHandle == gv_divide_Table_1.RowCount - 1)
                {
                    btn_DowDivide.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btn_UpDivide_Click(object sender, EventArgs e)
        {
            try
            {
                gv_divide_Table_1.Focus();
                gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.FocusedRowHandle - 1;
                btn_DowDivide.Enabled = true;
                if (gv_divide_Table_1.FocusedRowHandle == 0)
                {
                    btn_UpDivide.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btn_ToRightAll_Click(object sender, EventArgs e)
        {
            try
            {
                if (funtions == 1)
                {
                    while (gv_divide_Table_1.RowCount > 0)
                        ThrowTable1ToTable2();
                    return;
                }
                if (maBanGhep == "")
                {
                    pnShowHang.BringToFront();
                }
                while (gv_divide_Table_1.RowCount > 0)
                    ThrowTable1ToTable2();
            }
            catch
            { }
        }

        private void btn_ToRight_Click(object sender, EventArgs e)
        {
            try
            {
                if (funtions == 1)
                {
                    ThrowTable1ToTable2();
                    return;
                }
                if (maBanGhep == "")
                {
                    pnShowHang.BringToFront();
                }
                ThrowTable1ToTable2();
            }
            catch
            { }
        }

        private void ThrowTable1ToTable2()
        {
            if (gv_divide_Table_1.RowCount <= 0)
                return;
            bool checkProductOnGv = false;
            //add gridview
            if (divide_Table2.Rows.Count > 0)
            {
                for (int i = 0; i < divide_Table2.Rows.Count; i++)
                {
                    string maHH = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString();
                    if (divide_Table2.Rows[i]["__MA_HANGHOA"].ToString() == maHH)
                    {
                        divide_Table2.Rows[i]["__SOLUONG"] = decimal.Parse(divide_Table2.Rows[i]["__SOLUONG"].ToString()) + 1;
                        checkProductOnGv = true;
                    }
                }
            }
            if (checkProductOnGv == false)
            {
                DataRow dr = divide_Table2.NewRow();
                dr[0] = int.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString());
                dr[1] = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_TEN_HANGHOA"]).ToString();
                dr[2] = 1;
                dr[3] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_GIABAN"]).ToString());
                dr[4] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_MA_BEP"]).ToString());
                dr[5] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_Thanh_Tien"]).ToString());
                divide_Table2.Rows.Add(dr);
            }
            gr_divide_Table_2.DataSource = divide_Table2;
            gv_divide_Table_2.FocusedRowHandle = gv_divide_Table_2.RowCount - 1;
            if (decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString()) == 1)
                gv_divide_Table_1.DeleteSelectedRows();
            else
            {
                int sl = int.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                gv_divide_Table_1.SetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"], sl - 1);
            }
        }

        private void btn_ToLeftAll_Click(object sender, EventArgs e)
        {
            try
            {
                if (funtions == 1)
                {
                    while (gv_divide_Table_2.RowCount > 0)
                        Throw_Table2ToTable1();
                    return;
                }
                if (maBanGhep == "")
                {
                    pnShowHang.BringToFront();
                }
                while (gv_divide_Table_2.RowCount > 0)
                    Throw_Table2ToTable1();
            }
            catch
            { }
        }

        private void btn_Toleft_Click(object sender, EventArgs e)
        {
            try
            {
                if (funtions == 1)
                {
                    Throw_Table2ToTable1();
                    return;
                }
                if (maBanGhep == "")
                {
                    pnShowHang.BringToFront();
                }
                Throw_Table2ToTable1();
            }
            catch
            { }
        }

        private void Throw_Table2ToTable1()
        {
            if (gv_divide_Table_2.RowCount <= 0)
                return;
            bool checkProductOnGv = false;
            //add gridview
            if (divide_Table1.Rows.Count > 0)
            {
                for (int i = 0; i < divide_Table1.Rows.Count; i++)
                {
                    string maHH = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                    if (divide_Table1.Rows[i]["_MA_HANGHOA"].ToString() == maHH)
                    {
                        divide_Table1.Rows[i]["_SOLUONG"] = decimal.Parse(divide_Table1.Rows[i]["_SOLUONG"].ToString()) + 1;
                        checkProductOnGv = true;
                    }
                }
            }
            if (checkProductOnGv == false)
            {
                DataRow dr = divide_Table1.NewRow();
                dr[0] = int.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString());
                dr[1] = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__TEN_HANGHOA"]).ToString();
                dr[2] = 1;
                dr[3] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__GIABAN"]).ToString());
                dr[4] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__MA_BEP"]).ToString());
                dr[5] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__Thanh_Tien"]).ToString());
                divide_Table1.Rows.Add(dr);
            }
            gr_divide_Table_1.DataSource = divide_Table1;
            gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.RowCount - 1;
            if (decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString()) == 1)
                gv_divide_Table_2.DeleteSelectedRows();
            else
            {
                int sl = int.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                gv_divide_Table_2.SetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"], sl - 1);
            }
        }

        private void btn_BrowTable_Click(object sender, EventArgs e)
        {
            pnShowHang.BringToFront();
            foreach (SimpleButton btn in pnKV_Mon.Controls)
            {
                if (btn.Name == maKV.ToString())
                {
                    SetBackgroudOnClick(btn);
                }
            }
            foreach (SimpleButton btnb in pnTable_Mon.Controls)
            {
                SetDefautlBackgroud(btnb);
                if (CheckHaveISBANDAT(int.Parse(maBan)) == true)
                {
                    SetBackgroudOnClick2(btnb);
                }
                if (CheckOpenTable(btnb.Name) == true)
                {
                    SetBackgroudOnClick1(btnb);
                }
            }
        }

        private DataTable GetTableInBill(string idBill)
        {
            string sql = "select MA_BAN from CT_MOBAN_KHACHSAN where MA_HOADON='" + idBill + "'";
            DataTable retTables = clsMain.ReturnDataTable(sql);
            return retTables;
        }

        private void btnTimkhach_Click(object sender, EventArgs e)
        {
            gridControl2.Visible = !gridControl2.Visible;
            gridView2.ActiveFilter.Clear();
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (e.Column == Themkhach && gridView2.FocusedRowHandle >= 0)
            {
                DataView dv = (DataView)gridView1.DataSource;
                DataTable dt = dv.ToTable();
                dt.Rows.Add(null, null, gridView2.GetFocusedRowCellValue("MA_KH").ToString(), gridView2.GetFocusedRowCellValue("TEN").ToString(), gridView2.GetFocusedRowCellValue("NGAYSINH").ToString(), gridView2.GetFocusedRowCellValue("GIOITINH").ToString(), gridView2.GetFocusedRowCellValue("DIACHI").ToString(), gridView2.GetFocusedRowCellValue("DIENTHOAI").ToString(), gridView2.GetFocusedRowCellValue("EMAIL").ToString(), gridView2.GetFocusedRowCellValue("CMND").ToString(), gridView2.GetFocusedRowCellValue("QUOCTICH").ToString(), null);
                gridControl1.DataSource = dt;

            }
        }

        private void btnDoigio_nhanphong_Click(object sender, EventArgs e)
        {
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }
            if (CheckOpenTable(maBan))
            {
                if (funtions == 10)
                    while (gv_SelectProduct.RowCount > 0)
                        gv_SelectProduct.DeleteRow(0);
                if (funtions == 8 || fastFoot != 0 || funtions == 6 || funtions == 200)
                    return;
                string maHD = GetMaHoaDon(maBan);
                DataTable dataInPut = getGioVaoBan(maBan);
                if (dataInPut.Rows.Count > 0)
                {
                    Frm_Doigio_Nhanphong frm = new Frm_Doigio_Nhanphong(cls_KP_RES.DateToString_Full_NoSecond((DateTime)dataInPut.Rows[0][0]), maHD);
                    frm.ShowDialog(this);
                    frm.Dispose();
                    ExitDivideTable();
                }
            }
        }

        private void btnKhoaphong_Click(object sender, EventArgs e)
        {
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }

            if (CheckOpenTable(maBan))
            {
                return;
            }

            if (ktrasuachua(maBan ) == true)
            {

                clsMain.ExecuteSQL("UPDATE ban SET SUACHUA=0 WHERE MA_BAN=" + maBan);
                XtraMessageBox.Show("Bỏ khóa phòng thành công!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnKhoaphong.Text = "Khóa phòng";
            }
            else
            {

                clsMain.ExecuteSQL("UPDATE ban SET SUACHUA=1 WHERE MA_BAN=" + maBan);
                XtraMessageBox.Show("Khóa phòng thành công!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnKhoaphong.Text = "Bỏ khóa phòng";
            }
            GetKhuVuc();
            GetSoDoBan();
        }

        private bool ktrasuachua(string smaban)
        {
            string sql = "Select SUACHUA from BAN where MA_BAN=" + smaban;
            DataTable dt = clsMain.ReturnDataTable(sql);
            string sSUACHUA = dt.Rows[0]["SUACHUA"].ToString();
            if (sSUACHUA == "1")
            {
                return true;
            }
            return false;
        }

        private void btnVesinh_Click(object sender, EventArgs e)
        {
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }

            if (CheckOpenTable(maBan))
            {
                return;
            }

            clsMain.ExecuteSQL("UPDATE ban SET VESINH=0 WHERE MA_BAN=" + maBan);
            XtraMessageBox.Show("Đã dọn phòng !", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetKhuVuc();
            GetSoDoBan();
        }
    }
}