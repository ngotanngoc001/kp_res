﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES
{
    public partial class Frm_KS_Lapbanggia : DevExpress.XtraEditors.XtraForm
    {
        DataTable dtBanggia = new DataTable();
        string sma = "";
        int igrid = 1;

        public Frm_KS_Lapbanggia()
        {
            InitializeComponent();
            LoadBanggia();
            LoadPermission();
        }

        private void Frm_KS_Lapbanggia_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void Frm_KS_Lapbanggia_Load(object sender, EventArgs e)
        {
            pnlGiagio.Width = pnlhuthuvaosom.Width = panelControl8.Width / 3;
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                if (igrid == 1)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
                else if (igrid == 3)
                {
                    gridView3.Focus();
                    gridView3.FocusedRowHandle = gridView3.RowCount - 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
                else
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                if (igrid == 1)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                    {
                        btnXuongit.Enabled = false;
                        btnXuongnhieu.Enabled = false;
                    }
                }
                else if (igrid == 3)
                {
                    gridView3.Focus();
                    gridView3.FocusedRowHandle = gridView3.FocusedRowHandle + 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    if (gridView3.FocusedRowHandle == gridView3.RowCount - 1)
                    {
                        btnXuongit.Enabled = false;
                        btnXuongnhieu.Enabled = false;
                    }
                }
                else
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                    {
                        btnXuongit.Enabled = false;
                        btnXuongnhieu.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                if (igrid == 1)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                    if (gridView1.FocusedRowHandle == 0)
                    {
                        btnLenit.Enabled = false;
                        btnLennhieu.Enabled = false;
                    }
                }
                else if (igrid == 3)
                {
                    gridView3.Focus();
                    gridView3.FocusedRowHandle = gridView3.FocusedRowHandle - 1;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                    if (gridView3.FocusedRowHandle == 0)
                    {
                        btnLenit.Enabled = false;
                        btnLennhieu.Enabled = false;
                    }
                }
                else
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                    if (gridView2.FocusedRowHandle == 0)
                    {
                        btnLenit.Enabled = false;
                        btnLennhieu.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                if (igrid == 1)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }
                else if (igrid == 3)
                {
                    gridView3.Focus();
                    gridView3.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }
                else
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTEN.Properties.ReadOnly = true;
                txtGHICHU.Properties.ReadOnly = true;
                chkSUDUNG.Properties.ReadOnly = true;
                txtGIANGAY.Properties.ReadOnly = true;
                txtGIATUAN.Properties.ReadOnly = true;
                txtGIATHANG.Properties.ReadOnly = true;
                txtQUADEM.Properties.ReadOnly = true;
                txtPHUTHUTHEMNGUOI.Properties.ReadOnly = true;
                gridView2.OptionsBehavior.ReadOnly = true;
                gridView1.OptionsBehavior.ReadOnly = true;
                gridView3.OptionsBehavior.ReadOnly = true;
            }
            else//mở
            {
                txtTEN.Properties.ReadOnly = false;
                txtGHICHU.Properties.ReadOnly = false;
                chkSUDUNG.Properties.ReadOnly = false;
                txtGIANGAY.Properties.ReadOnly = false;
                txtGIATUAN.Properties.ReadOnly = false;
                txtGIATHANG.Properties.ReadOnly = false;
                txtQUADEM.Properties.ReadOnly = false;
                txtPHUTHUTHEMNGUOI.Properties.ReadOnly = false;
                gridView2.OptionsBehavior.ReadOnly = false;
                gridView1.OptionsBehavior.ReadOnly = false;
                gridView3.OptionsBehavior.ReadOnly = false;
            }
        }

        private void SetRongControl()
        {
            sma = "";
            txtTEN.Text = "";
            txtGHICHU.Text = "";
            chkSUDUNG.Checked = true;
            txtGIANGAY.EditValue = 0;
            txtGIATUAN.EditValue = 0;
            txtGIATHANG.EditValue = 0;
            txtQUADEM.EditValue = 0;
            txtPHUTHUTHEMNGUOI.EditValue = 0;

            DataTable DTSOUCRE = new DataTable();
            DTSOUCRE.Columns.Add("SOGIO", typeof(string));
            DTSOUCRE.Columns.Add("GIA", typeof(string));
            DTSOUCRE.Rows.Add(null, null);

            gridControl1.DataSource = DTSOUCRE.Copy();
            gridControl2.DataSource = DTSOUCRE.Copy();
            gridControl3.DataSource = DTSOUCRE.Copy();
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadBanggia();
        }

        private void LoadBanggia()
        {
            try
            {
                SetRongControl();
                KhoaMoControl(true);

                String sSQL = "";
                sSQL += "SELECT MABANGGIA AS MA,TENBANGGIA AS TEN, GHICHU, SUDUNG" + "\n";
                sSQL += "FROM BANGGIA" + "\n";
                sSQL += "WHERE MODE = 2" + "\n";
                dtBanggia = clsMain.ReturnDataTable(sSQL);

                fpnlBanggia.Controls.Clear();
                foreach (DataRow dr in dtBanggia.Rows)
                {
                    SimpleButton btn = new SimpleButton();
                    btn.Width = 110;
                    btn.Height = 80;
                    btn.Font = fpnlBanggia.Font;
                    btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btn.ImageLocation = ImageLocation.TopCenter;
                    btn.Image = global::KP_RES.Properties.Resources.display_26;
                    btn.Name = "H" + dr["MA"].ToString();
                    btn.Text = dr["TEN"].ToString();
                    if (btn.Text.Length > 20)
                    {
                        btn.Width = 110 * 2;
                    }
                    btn.Click += new EventHandler(BangGia_Click);
                    fpnlBanggia.Controls.Add(btn);
                }
            }
            catch
            {
            }
        }

        private void BangGia_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in fpnlBanggia.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }
            sma = btn.Name.Substring(1, btn.Name.Length - 1);
            txtTEN.Text = dtBanggia.Select("MA=" + clsMain.SQLString(sma))[0]["TEN"].ToString();
            txtGHICHU.Text = dtBanggia.Select("MA=" + clsMain.SQLString(sma))[0]["GHICHU"].ToString();
            chkSUDUNG.Checked = bool.Parse(dtBanggia.Select("MA=" + clsMain.SQLString(sma))[0]["SUDUNG"].ToString());
            LoaddataGridView1();
        }

        private void LoaddataGridView1()
        {
            try
            {
                String sSQL = "";
                DataTable dt = new DataTable();
                sSQL += "SELECT MABANGGIA, NGAY, TUAN, THANG, QUADEM, PHUTHUTHEMNGUOI" + "\n";
                sSQL += "FROM KS_BANGGIANGAYDEM" + "\n";
                sSQL += "WHERE MABANGGIA = " + clsMain.SQLString(sma);
                dt = clsMain.ReturnDataTable(sSQL);
                txtGIANGAY.EditValue = float.Parse(dt.Rows[0]["NGAY"].ToString());
                txtGIATUAN.EditValue = float.Parse(dt.Rows[0]["TUAN"].ToString());
                txtGIATHANG.EditValue = float.Parse(dt.Rows[0]["THANG"].ToString());
                txtPHUTHUTHEMNGUOI.EditValue = float.Parse(dt.Rows[0]["PHUTHUTHEMNGUOI"].ToString());
                txtQUADEM.EditValue = float.Parse(dt.Rows[0]["QUADEM"].ToString());

                sSQL = "SELECT MABANGGIA,SOGIO,GIA" + "\n";
                sSQL += "FROM KS_BANGGIAGIO" + "\n";
                sSQL += "WHERE MABANGGIA = " + clsMain.SQLString(sma);
                dt = clsMain.ReturnDataTable(sSQL);
                gridControl2.DataSource = dt;

                sSQL = "SELECT MABANGGIA,SOGIO,GIA" + "\n";
                sSQL += "FROM KS_BANGGIAVAOSOM" + "\n";
                sSQL += "WHERE MABANGGIA = " + clsMain.SQLString(sma);
                dt = clsMain.ReturnDataTable(sSQL);
                gridControl3.DataSource = dt;

                sSQL = "SELECT MABANGGIA,SOGIO,GIA" + "\n";
                sSQL += "FROM KS_BANGGIAOQUAGIO" + "\n";
                sSQL += "WHERE MABANGGIA = " + clsMain.SQLString(sma);
                dt = clsMain.ReturnDataTable(sSQL);
                gridControl1.DataSource = dt;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtTEN.Focus();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để sửa
                return;

            KhoaMoControl(false);
            txtTEN.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để xóa
                return;

            if (!CheckDelete())
                return;

            if (XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;

            String sSQL = "";
            sSQL += "DELETE FROM KS_BANGGIAOQUAGIO" + "\n";
            sSQL += "WHERE MABANGGIA = " + clsMain.SQLString(sma) + "\n";

            sSQL += "DELETE FROM KS_BANGGIAVAOSOM" + "\n";
            sSQL += "WHERE MABANGGIA = " + clsMain.SQLString(sma) + "\n";

            sSQL += "DELETE FROM KS_BANGGIAGIO" + "\n";
            sSQL += "WHERE MABANGGIA = " + clsMain.SQLString(sma) + "\n";

            sSQL += "DELETE FROM KS_BANGGIANGAYDEM" + "\n";
            sSQL += "WHERE MABANGGIA = " + clsMain.SQLString(sma) + "\n";

            sSQL += "DELETE FROM BANGGIA" + "\n";
            sSQL += "WHERE MABANGGIA = " + clsMain.SQLString(sma) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
                LoadBanggia();
            else
                XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;

            String sSQL = "";
            if (sma == "" && btnThem.Enabled)
            {
                int sMabanggia = 1 + int.Parse(clsMain.ReturnDataTable("SELECT IDENT_CURRENT( 'BANGGIA' )").Rows[0][0].ToString());
                sSQL += "INSERT INTO BANGGIA(TENBANGGIA, GHICHU, SUDUNG, MODE)" + "\n";
                sSQL += "VALUES( ";
                sSQL += clsMain.SQLStringUnicode(txtTEN.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtGHICHU.Text) + ",";
                sSQL += clsMain.SQLBit(chkSUDUNG.Checked) + ",";
                sSQL += "2" + ")" + "\n";

                sSQL += "INSERT INTO KS_BANGGIANGAYDEM(MABANGGIA, NGAY, TUAN, THANG, PHUTHUTHEMNGUOI, QUADEM)" + "\n";
                sSQL += "VALUES( ";
                sSQL += clsMain.SQLString(sMabanggia.ToString()) + ",";
                sSQL += clsMain.SQLString(txtGIANGAY.Text.Replace(",", "")) + ",";
                sSQL += clsMain.SQLString(txtGIATUAN.Text.Replace(",", "")) + ",";
                sSQL += clsMain.SQLString(txtGIATHANG.Text.Replace(",", "")) + ",";
                sSQL += clsMain.SQLString(txtPHUTHUTHEMNGUOI.Text.Replace(",", "")) + ",";
                sSQL += clsMain.SQLString(txtQUADEM.Text.Replace(",", "")) + ")" + "\n";

                for (int i = 0; i < gridView2.RowCount - 1; i++)
                {
                    if (gridView2.GetRowCellValue(i, "SOGIO").ToString() != "" && gridView2.GetRowCellValue(i, "GIA").ToString() != "")
                    {
                        sSQL += "DELETE FROM KS_BANGGIAGIO" + "\n";
                        sSQL += "WHERE MABANGGIA = " + clsMain.SQLString(sMabanggia.ToString()) + "\n";
                        sSQL += "AND SOGIO = " + clsMain.SQLString(gridView2.GetRowCellValue(i, "SOGIO").ToString().Replace(",", "")) + "\n";

                        sSQL += "INSERT INTO KS_BANGGIAGIO(MABANGGIA, SOGIO, GIA)" + "\n";
                        sSQL += "VALUES( ";
                        sSQL += clsMain.SQLString(sMabanggia.ToString()) + ",";
                        sSQL += clsMain.SQLString(gridView2.GetRowCellValue(i, "SOGIO").ToString().Replace(",", "")) + ",";
                        sSQL += clsMain.SQLString(gridView2.GetRowCellValue(i, "GIA").ToString().Replace(",", "")) + ")" + "\n";
                    }
                }

                for (int i = 0; i < gridView3.RowCount - 1; i++)
                {
                    if (gridView3.GetRowCellValue(i, "SOGIO").ToString() != "" && gridView3.GetRowCellValue(i, "GIA").ToString() != "")
                    {
                        sSQL += "DELETE FROM KS_BANGGIAVAOSOM" + "\n";
                        sSQL += "WHERE MABANGGIA = " + clsMain.SQLString(sMabanggia.ToString()) + "\n";
                        sSQL += "And SOGIO = " + clsMain.SQLString(gridView3.GetRowCellValue(i, "SOGIO").ToString().Replace(",", "")) + "\n";

                        sSQL += "INSERT INTO KS_BANGGIAVAOSOM(MABANGGIA, SOGIO, GIA)" + "\n";
                        sSQL += "VALUES( ";
                        sSQL += clsMain.SQLString(sMabanggia.ToString()) + ",";
                        sSQL += clsMain.SQLString(gridView3.GetRowCellValue(i, "SOGIO").ToString().Replace(",", "")) + ",";
                        sSQL += clsMain.SQLString(gridView3.GetRowCellValue(i, "GIA").ToString().Replace(",", "")) + ")" + "\n";
                    }
                }

                for (int i = 0; i < gridView1.RowCount - 1; i++)
                {
                    if (gridView1.GetRowCellValue(i, "SOGIO").ToString() != "" && gridView1.GetRowCellValue(i, "GIA").ToString() != "")
                    {
                        sSQL += "DELETE FROM KS_BANGGIAOQUAGIO" + "\n";
                        sSQL += "WHERE MABANGGIA = " + clsMain.SQLString(sMabanggia.ToString()) + "\n";
                        sSQL += "AND SOGIO = " + clsMain.SQLString(gridView1.GetRowCellValue(i, "SOGIO").ToString().Replace(",", "")) + "\n";

                        sSQL += "INSERT INTO KS_BANGGIAOQUAGIO(MABANGGIA, SOGIO, GIA)" + "\n";
                        sSQL += "VALUES( ";
                        sSQL += clsMain.SQLString(sMabanggia.ToString()) + ",";
                        sSQL += clsMain.SQLString(gridView1.GetRowCellValue(i, "SOGIO").ToString().Replace(",", "")) + ",";
                        sSQL += clsMain.SQLString(gridView1.GetRowCellValue(i, "GIA").ToString().Replace(",", "")) + ")" + "\n";
                    }
                }
            }
            else if ((sma != "" && btnSua.Enabled))
            {
                sSQL += "UPDATE BANGGIA " + "\n";
                sSQL += "SET TENBANGGIA =" + clsMain.SQLStringUnicode(txtTEN.Text) + "," + "\n";
                sSQL += "GHICHU = " + clsMain.SQLStringUnicode(txtGHICHU.Text) + "," + "\n";
                sSQL += "SUDUNG = " + clsMain.SQLBit(chkSUDUNG.Checked) + "," + "\n";
                sSQL += "MODE = 2 " + "\n";
                sSQL += "WHERE MABANGGIA =" + clsMain.SQLString(sma) + "\n";

                sSQL += "UPDATE KS_BANGGIANGAYDEM " + "\n";
                sSQL += "SET NGAY =" + clsMain.SQLString(txtGIANGAY.Text.Replace(",", "")) + "," + "\n";
                sSQL += "TUAN =" + clsMain.SQLString(txtGIATUAN.Text.Replace(",", "")) + "," + "\n";
                sSQL += "THANG =" + clsMain.SQLString(txtGIATHANG.Text.Replace(",", "")) + "," + "\n";
                sSQL += "PHUTHUTHEMNGUOI =" + clsMain.SQLString(txtPHUTHUTHEMNGUOI.Text.Replace(",", "")) + "," + "\n";
                sSQL += "QUADEM =" + clsMain.SQLString(txtQUADEM.Text.Replace(",", "")) + "\n";
                sSQL += "WHERE MABANGGIA =" + clsMain.SQLString(sma) + "\n";

                sSQL += "DELETE FROM KS_BANGGIAGIO" + "\n";
                sSQL += "WHERE MABANGGIA = " + clsMain.SQLString(sma) + "\n";
                for (int i = 0; i < gridView2.RowCount - 1; i++)
                {
                    if (gridView2.GetRowCellValue(i, "SOGIO").ToString() != "" && gridView2.GetRowCellValue(i, "GIA").ToString() != "")
                    {
                        sSQL += "INSERT INTO KS_BANGGIAGIO(MABANGGIA, SOGIO, GIA)" + "\n";
                        sSQL += "VALUES( ";
                        sSQL += clsMain.SQLString(sma) + ",";
                        sSQL += clsMain.SQLString(gridView2.GetRowCellValue(i, "SOGIO").ToString().Replace(",", "")) + ",";
                        sSQL += clsMain.SQLString(gridView2.GetRowCellValue(i, "GIA").ToString().Replace(",", "")) + ")" + "\n";
                    }
                }

                sSQL += "DELETE FROM KS_BANGGIAVAOSOM" + "\n";
                sSQL += "WHERE MABANGGIA = " + clsMain.SQLString(sma) + "\n";
                for (int i = 0; i < gridView3.RowCount - 1; i++)
                {
                    if (gridView3.GetRowCellValue(i, "SOGIO").ToString() != "" && gridView3.GetRowCellValue(i, "GIA").ToString() != "")
                    {
                        sSQL += "INSERT INTO KS_BANGGIAVAOSOM(MABANGGIA, SOGIO, GIA)" + "\n";
                        sSQL += "VALUES( ";
                        sSQL += clsMain.SQLString(sma) + ",";
                        sSQL += clsMain.SQLString(gridView3.GetRowCellValue(i, "SOGIO").ToString().Replace(",", "")) + ",";
                        sSQL += clsMain.SQLString(gridView3.GetRowCellValue(i, "GIA").ToString().Replace(",", "")) + ")" + "\n";
                    }
                }

                sSQL += "DELETE FROM KS_BANGGIAOQUAGIO" + "\n";
                sSQL += "WHERE MABANGGIA = " + clsMain.SQLString(sma) + "\n";
                for (int i = 0; i < gridView1.RowCount - 1; i++)
                {
                    if (gridView1.GetRowCellValue(i, "SOGIO").ToString() != "" && gridView1.GetRowCellValue(i, "GIA").ToString() != "")
                    {
                        sSQL += "INSERT INTO KS_BANGGIAOQUAGIO(MABANGGIA, SOGIO, GIA)" + "\n";
                        sSQL += "VALUES( ";
                        sSQL += clsMain.SQLString(sma) + ",";
                        sSQL += clsMain.SQLString(gridView1.GetRowCellValue(i, "SOGIO").ToString().Replace(",", "")) + ",";
                        sSQL += clsMain.SQLString(gridView1.GetRowCellValue(i, "GIA").ToString().Replace(",", "")) + ")" + "\n";
                    }
                }
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoadBanggia();
                btnThem.Focus();
            }
            else
                XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private Boolean CheckInput()
        {
            if (txtTEN.Text == "")
            {
                XtraMessageBox.Show("Nhập " + lblTEN.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTEN.Focus();
                return false;
            }
            else if (txtGIANGAY.Text == "0")
            {
                XtraMessageBox.Show("Chưa nhập giá thuê ngày", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtGIANGAY.Focus();
                return false;
            }
            return true;
        }

        private Boolean CheckDelete()
        {
            if (clsMain.ReturnDataTable("SELECT * FROM APDUNGBANGGIA WHERE THUHAI = " + clsMain.SQLString(sma) + " Or THUBA=" + clsMain.SQLString(sma) + " Or THUTU=" + clsMain.SQLString(sma) + " Or THUNAM=" + clsMain.SQLString(sma) + " Or THUSAU=" + clsMain.SQLString(sma) + " Or THUBAY=" + clsMain.SQLString(sma) + " Or CHUNHAT=" + clsMain.SQLString(sma)).Rows.Count > 0)
            {
                XtraMessageBox.Show("Bảng giá đã sử dụng. Không được xóa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("0906201502");
            btnSua.Enabled = clsUserManagement.AllowEdit("0906201502");
            btnXoa.Enabled = clsUserManagement.AllowDelete("0906201502");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            igrid = 2;
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            btnLenit.Enabled = true;
            btnLennhieu.Enabled = true;
            btnXuongit.Enabled = true;
            btnXuongnhieu.Enabled = true;
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            igrid = 1;
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;

            btnLenit.Enabled = true;
            btnLennhieu.Enabled = true;
            btnXuongit.Enabled = true;
            btnXuongnhieu.Enabled = true;
        }

        private void gridView3_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            igrid = 3;
            if (gridView3.RowCount == 0 || gridView3.FocusedRowHandle < 0)
                return;

            btnLenit.Enabled = true;
            btnLennhieu.Enabled = true;
            btnXuongit.Enabled = true;
            btnXuongnhieu.Enabled = true;
        }
    }
}