﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using KP_RES.Class;
using System.IO;
using KP_Report;

namespace KP_RES
{
    public partial class Frm_Payment_KhachSan : DevExpress.XtraEditors.XtraForm
    {
        #region varriable declaretion
        TextEdit txtInput=new TextEdit();//txt get input
        bool bShift = true;//true is upperText,false is lower text
        bool bText = false;//true is permit input,false is not permit
        bool bPrintBill = true;//true print bill.false is not print bill
        bool bVisaCard = true; //true is not payment visa,false were payment visa
        bool bVoucher = true; //true is not payment Voucher,false were payment Voucher
        string _sIdRoomAndTable = "";
        string _sIdBill = "";
        decimal dMoneyVisa = 0;//total payment = visa
        decimal dMoneyVoucher = 0;//total payment = voucher
        decimal dMoneyInnerCard = 0;//total payment = inner card
        decimal dMoneyCash = 0;//total payment = Cash
        public string soThe = "0";
        public string ngayThe = "0";
        decimal _dMoneygetFirst = 0;
        decimal _dMoneyHour = 0;
        DataTable dtGridviewProduct=new DataTable();//data list product sale 
        DataTable _dtMoneyHour = new DataTable();//data money hour 
        DataTable dtTienGio = new DataTable();//Luoi tien gio
        DateTime _dHourInput;
        DateTime _dHourOutput;
        int set = 1;//1: tiền giờ, 2: giao diện thanh toán
        
        #endregion

        #region Event Handling

        #region Form Main
        public Frm_Payment_KhachSan(string sIdRoomAndTable, string sIdBill, DataTable dtGridview, decimal dMoneygetFirst, decimal dMoneyHour, DataTable dtMoneyHour, DateTime dHourInput, DateTime dHourOutput)
        {
            InitializeComponent();
            dtGridviewProduct = dtGridview;
            _sIdRoomAndTable = sIdRoomAndTable;
            _sIdBill = sIdBill;
            _dMoneygetFirst = dMoneygetFirst;
            _dMoneyHour = dMoneyHour;
            _dtMoneyHour = dtMoneyHour;
            _dHourInput = dHourInput;
            _dHourOutput =  dHourOutput;
        }

        private void Payment_Load(object sender, EventArgs e)
        {
            LoadDataPrepare();
            LoadForiegnCurrency();
            LoadKeyboardMoney();
            LoadKeyboardNumber();
            LoadKeyboardText();
        }

        #endregion

        #region Click Event

        #region Click Action

        private void btnOpenSafe_Click(object sender, EventArgs e)
        {
            KP_Terminal.clsKP_Terminal.OpenCashDrawer(false);
        }

        private void InputGetMoneyFocus()
        {
            bText = false;
            txtInput = txtInputMoney;
            pnlInputVoucher.Visible = false;
            pnlInputDiscount.Visible = false;
            pnlInputGetMoney.Visible = true;
            pnlDearCustomer.Visible = false;
            pnlInnerCustomer.Visible = false;
            pnlServiceMoney.Visible = false;
            txtInput.Text = "0";
            txtInput.SelectionStart = 1;
            txtInput.Focus();
        }

        private void btnServiceMoney_Click(object sender, EventArgs e)
        {
            pnlShowInforPayment.BringToFront();
            bText = false;

            txtInput = txtServiceMoney;
            pnlInputVoucher.Visible = false;
            pnlInputDiscount.Visible = false;
            pnlInputGetMoney.Visible = false;
            pnlDearCustomer.Visible = false;
            pnlInnerCustomer.Visible = false;
            pnlServiceMoney.Visible = true;
            txtInput.Text = "0";
            txtInput.Focus();
            txtInput.SelectionStart = 1;
        }

        private void btnCancelPayment_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void btnDiscount_Click(object sender, EventArgs e)
        {
            //bDiscountOrVoucher = true;
            pnlShowInforPayment.BringToFront();
            bText = false;
            txtInput = txtInputDiscount;
            pnlInputVoucher.Visible = false;
            pnlInputDiscount.Visible = true;
            pnlInputGetMoney.Visible = false;
            pnlDearCustomer.Visible = false;
            pnlInnerCustomer.Visible = false;
            pnlServiceMoney.Visible = false;
            txtInput.Text = "0";
            txtInput.Focus();
            txtInput.SelectionStart = 1;

        }

        private void btnRetypePayment_Click(object sender, EventArgs e)
        {
            pnlShowInforPayment.BringToFront();
            bText = false;
            
            txtInput = txtInputMoney;
            pnlInputVoucher.Visible = false;
            pnlInputDiscount.Visible = false;
            pnlInputGetMoney.Visible = true;
            pnlDearCustomer.Visible = false;
            pnlInnerCustomer.Visible = false;
            pnlServiceMoney.Visible = false;
            txtInput.Text = "0";
            txtInput.SelectionStart = 1;
            txtInput.Focus();
            ResetInputMoney();
        }

        private void btnCardImtimateCustomer_Click(object sender, EventArgs e)
        {
            if (!clsUserManagement.CheckActive("4"))
            {
                XtraMessageBox.Show("Bạn không dùng chức năng  khách hàng thân thiết", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            pnlShowInforPayment.BringToFront();
            bText = true;
            txtInput = txtDearCustomer;
            pnlInputVoucher.Visible = false;
            pnlInputDiscount.Visible = false;
            pnlInputGetMoney.Visible = false;
            pnlDearCustomer.Visible = true;
            pnlInnerCustomer.Visible = false;
            pnlServiceMoney.Visible = false;
            txtInput.Text = lbIDDearCustomerShow.Text;
            txtInput.Focus();
            txtInput.SelectionStart = txtInput.Text.Length;
        }

        private void btnCardInternalPayment_Click(object sender, EventArgs e)
        {
            if (!clsUserManagement.CheckActive("8"))
            {
                XtraMessageBox.Show("Bạn không dùng chức năng  khách hàng thanh toán nội bộ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            decimal dMustGetMoney = decimal.Parse(lbTotalMoneyAll.Tag.ToString()) - decimal.Parse(lbGetCustomer.Text.Replace(",", ""));
            if (dMustGetMoney <= 0)
                return;

            pnlShowInforPayment.BringToFront();            
            bText = true;
            txtInput = txtInnerCustomer;
            pnlInputVoucher.Visible = false;
            pnlInputDiscount.Visible = false;
            pnlInputGetMoney.Visible = false;
            pnlDearCustomer.Visible = false;
            pnlInnerCustomer.Visible = true;
            pnlServiceMoney.Visible = false;
            txtInput.Text = lbIdInnerCustomerShow.Text;
            txtInput.Focus();
            txtInput.SelectionStart = txtInput.Text.Length;
        }

        private void btnNotPrintBill_Click(object sender, EventArgs e)
        {
            bPrintBill = false;
            PrintBill(2); // 2: ko in hóa đơn            
        }

        private void btnVoucher_Click(object sender, EventArgs e)
        {
           // bDiscountOrVoucher = false;
            if (bVoucher)
            {
                pnlShowInforPayment.BringToFront();
                bText = false;

                txtInput = txtInputVoucher;
                pnlInputVoucher.Visible = true;
                pnlInputDiscount.Visible = false;
                pnlInputGetMoney.Visible = false;
                pnlDearCustomer.Visible = false;
                pnlInnerCustomer.Visible = false;
                pnlServiceMoney.Visible = false;
                txtInput.Text = "0";
                txtInput.Focus();
                txtInput.SelectionStart = 1;
            }
        }

        #endregion
        
        #region click keyboard
      

        #region Text Click

        private void btnTranslateText_Click(object sender, EventArgs e)
        {
            
            pnlKeyboardMoney.BringToFront();
            if (txtInput.SelectedText ==txtInput.Text)
            {
                txtInput.Focus();
            }
            else
            {
                int iPosition = txtInput.SelectionStart;
                txtInput.Focus();
                txtInput.SelectionStart = txtInput.Text.Length;
                txtInput.SelectionStart = iPosition;
            }
        }

        private void btnShift_Click(object sender, EventArgs e)
        {
            bShift = !bShift;
            if (bShift)
            {
                btnShift.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            }
            else
            {
                btnShift.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            }
        }

        private void btnOkText_Click(object sender, EventArgs e)
        {
            OkTextBox();
        }

        private void btnA_Click(object sender, EventArgs e)
        {
            InputKeyboardText("A");
        }

        private void btnQ_Click(object sender, EventArgs e)
        {
            InputKeyboardText("Q");
        }

        private void btnW_Click(object sender, EventArgs e)
        {
            InputKeyboardText("W");
        }

        private void btnE_Click(object sender, EventArgs e)
        {
            InputKeyboardText("E");
        }

        private void btnR_Click(object sender, EventArgs e)
        {
            InputKeyboardText("R");
        }

        private void btnT_Click(object sender, EventArgs e)
        {
            InputKeyboardText("T");
        }

        private void btnY_Click(object sender, EventArgs e)
        {
            InputKeyboardText("Y");
        }

        private void btnU_Click(object sender, EventArgs e)
        {
            InputKeyboardText("U");
        }

        private void btnI_Click(object sender, EventArgs e)
        {
            InputKeyboardText("I");
        }

        private void btnO_Click(object sender, EventArgs e)
        {
            InputKeyboardText("O");
        }

        private void btnP_Click(object sender, EventArgs e)
        {
            InputKeyboardText("P");
        }

        private void btnS_Click(object sender, EventArgs e)
        {
            InputKeyboardText("S");
        }

        private void btnD_Click(object sender, EventArgs e)
        {
            InputKeyboardText("D");
        }

        private void btnF_Click(object sender, EventArgs e)
        {
            InputKeyboardText("F");
        }

        private void btnG_Click(object sender, EventArgs e)
        {
            InputKeyboardText("G");
        }

        private void btnH_Click(object sender, EventArgs e)
        {
            InputKeyboardText("H");
        }

        private void btnJ_Click(object sender, EventArgs e)
        {
            InputKeyboardText("J");
        }

        private void btnK_Click(object sender, EventArgs e)
        {
            InputKeyboardText("K");
        }

        private void btnL_Click(object sender, EventArgs e)
        {
            InputKeyboardText("L");
        }

        private void btnM_Click(object sender, EventArgs e)
        {
            InputKeyboardText("M");
        }

        private void btnX_Click(object sender, EventArgs e)
        {
            InputKeyboardText("X");
        }

        private void btnC_Click(object sender, EventArgs e)
        {
            InputKeyboardText("C");
        }

        private void btnV_Click(object sender, EventArgs e)
        {
            InputKeyboardText("V");
        }

        private void btnB_Click(object sender, EventArgs e)
        {
            InputKeyboardText("B");
        }

        private void btnN_Click(object sender, EventArgs e)
        {
            InputKeyboardText("N");
        }

        private void btnDeleteText_Click(object sender, EventArgs e)
        {
            DeleteTextBox();
        }

        private void btnPaymentText_Click(object sender, EventArgs e)
        {
            bPrintBill = true;
            PrintBill(1); //1 in hóa đơn
        }

        private void btnZ_Click(object sender, EventArgs e)
        {
            InputKeyboardText("Z");
        }

        private void btnEscText_Click(object sender, EventArgs e)
        {
            EscapeTextbox();
        }

        private void btnMoveRigthText_Click(object sender, EventArgs e)
        {
            MoveToRight();
        }

        private void btnMoveLeftText_Click(object sender, EventArgs e)
        {
            MoveToLeft();
        }


        #endregion
              
        #region money click

        private void btnPaymentMoney_Click(object sender, EventArgs e)
        {
            bPrintBill = true;
            PrintBill(1);//1 in hóa đơn
        }

        private void btnTranslateMoney_Click(object sender, EventArgs e)
        {
            pnlKeyboardNumber.BringToFront();
            if (txtInput.SelectedText == txtInput.Text)
            {
                txtInput.Focus();
            }
            else
            {
                int iPosition = txtInput.SelectionStart;
                txtInput.Focus();
                txtInput.SelectionStart = txtInput.Text.Length;
                txtInput.SelectionStart = iPosition;
            }
        }

        private void btn1000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(1000);
        }

        private void btn2000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(2000);
        }

        private void btn5000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(5000);
        }

        private void btn10000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(10000);
        }

        private void btn20000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(20000);
        }

        private void btn50000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(50000);
        }

        private void btn100000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(100000);
        }

        private void btn200000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(200000);
        }

        private void btn500000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(500000);
        }

        private void btnMoveLeftMoney_Click(object sender, EventArgs e)
        {
            MoveToLeft();
        }

        private void btnMoveRightMoney_Click(object sender, EventArgs e)
        {
            MoveToRight();
        }

        private void btnDeleteMoney_Click(object sender, EventArgs e)
        {
            DeleteTextBox();
        }

        private void btnOkMoney_Click(object sender, EventArgs e)
        {
            OkTextBox();
        }

        private void btn500_Click(object sender, EventArgs e)
        {
            PlusInputMoney(500);
        }

        private void btnEcsMoney_Click(object sender, EventArgs e)
        {
            EscapeTextbox();
        }


        #endregion

        #region number click
        private string GetNewCodeBill()
        {
            string _mahoadon = "";
            string ngay = cls_ConfigCashier.idCashier + DateTime.Now.Day.ToString("00") + DateTime.Now.Month.ToString("00") + DateTime.Now.Year.ToString("0000");
            DataTable dtMA = clsMain.ReturnDataTable("select [dbo].[fc_NewcodeHoaDon] ('" + ngay + "')");
            _mahoadon = dtMA.Rows[0][0].ToString();
            return _mahoadon;
        }

        public int checkPrinter = 0;
        private void btnPaymentNumber_Click(object sender, EventArgs e)
        {
            bPrintBill = true;
            PrintBill(1); // 1: in hóa đơn
        }

        private void btnTranslateNumber_Click(object sender, EventArgs e)
        {
            pnlKeyBoardText.BringToFront();
            txtInput.SelectionStart = txtInput.Text.Length;
            if (txtInput.SelectedText == txtInput.Text)
            {
                txtInput.Focus();
            }
            else
            {
                int iPosition = txtInput.SelectionStart;
                
                txtInput.Focus();
                txtInput.SelectionStart = iPosition;
            }
            
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("7");
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("8");
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("9");
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("4");
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("5");
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("6");
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("1");
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("2");
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("3");
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("0");
        }

        private void btn00_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("00");
        }

        private void btn000_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("000");
        }

        private void btnMoveLeftNumber_Click(object sender, EventArgs e)
        {
            MoveToLeft();
        }

        private void btnMoveRightNumber_Click(object sender, EventArgs e)
        {
            MoveToRight();
        }

        private void btnDeleteNumber_Click(object sender, EventArgs e)
        {
            DeleteTextBox();
        }

        private void btnOkNumber_Click(object sender, EventArgs e)
        {
            OkTextBox();
        }

        private void btnEscNumber_Click(object sender, EventArgs e)
        {
            EscapeTextbox();
        }

        #endregion

        #endregion

        #region for gridview

        private void lb_clicktiengio_Click(object sender, EventArgs e)
        {
            if (set == 1)
            {
                set = 2;
                gc_time.DataSource = _dtMoneyHour;
                pnTime.BringToFront();
            }
            else if (set == 2)
            {
                set = 1;
                pnlPaymentShowFill.BringToFront();
            }
        }

        private void gvListGIO_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT_GIO && e.RowHandle >= 0)
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void gvListProduct_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT && e.RowHandle >= 0)
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void gvListForiegnCurrency_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            lbKindMoney.Text = gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString();
            lbcurrencyService.Text = lbcurrencyGetMoney.Text = gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString();
            lbExchangeRate.Text = gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString();
        }
        private void btnUpForeignCurrency_Click(object sender, EventArgs e)
        {
            if (gvListForiegnCurrency.FocusedRowHandle > 0)
            {
                gvListForiegnCurrency.FocusedRowHandle = gvListForiegnCurrency.FocusedRowHandle - 1;
            }
        }

        private void gvListForiegnCurrency_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT1 && e.RowHandle >= 0)
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnDownForeignCurrency_Click(object sender, EventArgs e)
        {
            if (gvListForiegnCurrency.FocusedRowHandle < gvListForiegnCurrency.RowCount - 1)
            {
                gvListForiegnCurrency.FocusedRowHandle = gvListForiegnCurrency.FocusedRowHandle + 1;
            }
        }

        private void gcListProduct_Leave(object sender, EventArgs e)
        {
            if (!btnFirstGridview.Focused && !btnLastGridview.Focused && !btnUpGridview.Focused && !btnDownGridview.Focused)
                pnUpDownGridview.Visible = false;
        }

        private void gvListProduct_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (dtGridviewProduct.Rows.Count >= 7)
            {

                pnUpDownGridview.Visible = true;
            }
        }

        private void btnFirstGridview_Click(object sender, EventArgs e)
        {
            gvListProduct.Focus();
            gvListProduct.FocusedRowHandle = 0;
        }

        private void btnLastGridview_Click(object sender, EventArgs e)
        {
            gvListProduct.Focus();
            gvListProduct.FocusedRowHandle = gvListProduct.RowCount - 1;
        }

        private void btnUpGridview_Click(object sender, EventArgs e)
        {
            if (gvListProduct.FocusedRowHandle > 0)
            {
                gvListProduct.Focus();
                gvListProduct.FocusedRowHandle--;
            }
        }

        private void btnDownGridview_Click(object sender, EventArgs e)
        {
            if (gvListProduct.FocusedRowHandle < gvListProduct.RowCount - 1)
            {
                gvListProduct.Focus();
                gvListProduct.FocusedRowHandle++;
            }
        }

        private void gvListProduct_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            lbRowsCount.Text = gvListProduct.RowCount.ToString();
            lbSTT.Text = (int.Parse(gvListProduct.FocusedRowHandle.ToString()) + 1).ToString();
            if (lbSTT.Text == "1")
            {
                btnFirstGridview.Enabled = btnUpGridview.Enabled = false;
                btnLastGridview.Enabled = btnDownGridview.Enabled = true;
            }
            else
                if (lbSTT.Text == lbRowsCount.Text)
                {
                    btnFirstGridview.Enabled = btnUpGridview.Enabled = true;
                    btnLastGridview.Enabled = btnDownGridview.Enabled = false;
                }
                else
                {
                    btnFirstGridview.Enabled = btnUpGridview.Enabled = true;
                    btnLastGridview.Enabled = btnDownGridview.Enabled = true;
                }
        }

        #endregion

        #region text change

        private void lbTotalMoneyAll_TextChanged(object sender, EventArgs e)
        {
            decimal fReplyCustomer = decimal.Parse(lbGetCustomer.Text.Replace(",", "")) - decimal.Parse(lbTotalMoneyAll.Text.Replace(",", ""));
            lbReplyCustomer.Tag = fReplyCustomer;
            lbReplyCustomer.Text = string.Format("{0:#,###0#0}", fReplyCustomer);
        }

        private void lbTotalMoneyPayment_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalMoneyPayment();
        }

        private void lbDiscountPayment_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalMoneyPayment();
        }

        private void lbMoneyService_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalMoneyPayment();
        }

        private void lbMoneyGetFirst_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalMoneyPayment();
        }

        private void lbGetCustomer_TextChanged(object sender, EventArgs e)
        {
            decimal fReplyCustomer = decimal.Parse(lbGetCustomer.Text.Replace(",", "")) - decimal.Parse(lbTotalMoneyAll.Text.Replace(",", ""));
            lbReplyCustomer.Tag = fReplyCustomer;
            lbReplyCustomer.Text = string.Format("{0:#,###0#0}", fReplyCustomer);
        }

        private void lbFirstMoney_TextChanged(object sender, EventArgs e)
        {
            lbGetCustomer.Text = string.Format("{0:#,###0#0}", lbFirstMoney.Tag);
        }

        private void lbSecondMoney_TextChanged(object sender, EventArgs e)
        {
            lbGetCustomer.Text = string.Format("{0:#,###0#0}", (decimal.Parse(lbFirstMoney.Tag.ToString()) + decimal.Parse(lbSecondMoney.Tag.ToString())));
        }

        private void lbThirdMoney_TextChanged(object sender, EventArgs e)
        {
            lbGetCustomer.Text = string.Format("{0:#,###0#0}", (decimal.Parse(lbFirstMoney.Tag.ToString()) + decimal.Parse(lbSecondMoney.Tag.ToString())) + decimal.Parse(lbThirdMoney.Tag.ToString()));
        }

        private void lbTheMoney_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalGridview();
        }

        private void lbHourMoney_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalGridview();
        }

        private void lbTotalMoney_TextChanged(object sender, EventArgs e)
        {
            lbTotalMoneyPayment.Tag = lbTotalMoney.Tag;
            lbTotalMoneyPayment.Text = lbTotalMoney.Text;
        }


        private void lbMoneySurplus_SizeChanged(object sender, EventArgs e)
        {
            lbMoneySurplus.Location = new Point(pnlInnerCustomerShow.Width - lbMoneySurplus.Width - 3, 18);
        }

        private void lbMark_SizeChanged(object sender, EventArgs e)
        {
            lbMark.Location = new Point(pnlDearCustomerShow.Width - lbMark.Width - 3, 18);
        }

       

        #endregion

        #region  keypress

        private void txtInputVoucher_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void Payment_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.F2)
            {
                bPrintBill = true;
                PrintBill(1);               
            }
            int iPosition = txtInput.SelectionStart;
            int iLenght = txtInput.Text.Length;
            if (e.KeyCode == Keys.Delete)
            {
                if (iLenght > 2)
                {
                    if (iPosition == 0)
                    {
                        if (txtInput.Text[1] == ',')
                            txtInput.Text = txtInput.Text.Remove(0, 2);
                        else
                            txtInput.Text = txtInput.Text.Remove(0, 1);

                        txtInput.SelectionStart = 0;
                        e.Handled = true;
                    }
                }
            }
            else if (e.KeyCode == Keys.Back)
            {

                if (iPosition == 1)
                {
                    txtInput.Text = txtInput.Text.Remove(iPosition - 1, 1);
                    txtInput.SelectionStart = iPosition - 1;
                }
                else if (iPosition == 2)
                {
                    if (txtInput.Text[1] == ',')
                    {
                        txtInput.Text = txtInput.Text.Remove(iPosition - 2, 2);
                        txtInput.SelectionStart = iPosition - 2;
                    }
                }
            }
        }

        private void txtInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtInput.Text.Length > 15)
            {
                XtraMessageBox.Show("Bạn không được nhập quá 15 ký tự", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
            }
        }

        private void txtServiceMoney_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void txtInnerCustomer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void txtInputMoney_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void txtDearCustomer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void txtInputDiscount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }


        #endregion

        #region Visa event

        private void btnVisa_Click(object sender, EventArgs e)
        {
            if (bVisaCard)
            {
                pnlVisa.BringToFront();
                ResetControlValue(pnlVisa);
                if (txtIdCodeCreate.Text == "")
                {
                    txtIdCodeCreate.Text = CreateIdCode();
                }
            }
        }

        private void bntCancelVisa_Click(object sender, EventArgs e)
        {
            pnlShowInforPayment.BringToFront();
            ResetControlValue(pnlVisa);
            InputGetMoneyFocus();
        }

        private void btnRefeshIdCode_Click(object sender, EventArgs e)
        {

            txtIdCodeCreate.Text = CreateIdCode();
        }

        private bool CheckInputVisa()
        {
            if (txtIdCardVisa.Text == "")
            {
                XtraMessageBox.Show("Bạn Chưa Nhập Số Thẻ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtIdCardVisa.Focus();
                return false;
            }
            if (txtMoneyVisa.Text == "0")
            {
                XtraMessageBox.Show("Bạn Chưa Nhập Số Tiền ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMoneyVisa.Focus();
                return false;
            }
            return true;

        }

        private void btnPaymentVisa_Click(object sender, EventArgs e)
        {
            if (!CheckInputVisa())
            {
                return;
            }
            if (txtIdCodeInput.Text != "")
            {
                if (txtIdCodeInput.Text != txtIdCodeCreate.Text)
                {
                    XtraMessageBox.Show("Mã An Toàn Không Đúng", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtIdCodeInput.Text = "";
                    txtIdCodeInput.Focus();
                    return;
                }
            }

            dMoneyVisa = decimal.Parse(txtMoneyVisa.Text.Replace(",", ""));
            
            if (!pnlFirst.Visible)
            {

                lbFirst.Text = "Tiền Thẻ Visa: " + string.Format("{0:#,###0#0}", dMoneyVisa) + " VND -->";
                lbFirstMoney.Tag = dMoneyVisa;
                lbFirstMoney.Text = string.Format("{0:#,###0#0}", dMoneyVisa) + "  VND";
                pnlFirst.Visible = true;
            }
            else
                if (!pnlSecond.Visible)
                {
                    lbSecond.Text = "Tiền Thẻ Visa: " + string.Format("{0:#,###0#0}", dMoneyVisa) + " VND -->";
                    lbSecondMoney.Tag = dMoneyVisa;
                    lbSecondMoney.Text = string.Format("{0:#,###0#0}", dMoneyVisa) + "  VND";
                    pnlSecond.Visible = true;

                }
                else
                    if (!pnlThird.Visible)
                    {
                        lbThird.Text = "Tiền Thẻ Visa: " + string.Format("{0:#,###0#0}", dMoneyVisa) + " VND -->";
                        lbThirdMoney.Tag = dMoneyVisa;
                        lbThirdMoney.Text = string.Format("{0:#,###0#0}", dMoneyVisa) + "  VND";
                        pnlThird.Visible = true;
                    }
                    else
                    {
                        XtraMessageBox.Show("Tiền Trả Không được nhập quá 3 lần,bạn hãy nhập lại từ đầu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ResetInputMoney();
                    }
            soThe = txtIdCardVisa.Text;
            ngayThe = txtMonth.Text + " - " + txtYear.Text;
            
            InputGetMoneyFocus();
            pnlShowInforPayment.BringToFront();
            ResetControlValue(pnlVisa);
            InputGetMoneyFocus();
            bVisaCard = false;
        }

        private void txtIdCardVisa_GotFocus(object sender, EventArgs e)
        {
            bText = false;
            txtInput = txtIdCardVisa;
        }

        private void txtMoneyVisa_GotFocus(object sender, EventArgs e)
        {
            bText = false;
            txtInput = txtMoneyVisa;
        }

        private void txtMonth_GotFocus(object sender, EventArgs e)
        {
            bText = false;
            txtInput = txtMonth;
        }

        private void txtYear_GotFocus(object sender, EventArgs e)
        {
            bText = false;
            txtInput = txtYear;
        }

        private void txtNameVisa_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtNameVisa;
        }

        private void txtAddress_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtAddress;
        }

        private void txtCwCvc_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtCwCvc;
        }

        private void txtCity_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtCity;
        }

        private void txtIdCodeInput_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtIdCodeInput;
        }

        private void txtCountry_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtCountry;
        }

        #endregion

        #endregion

        #region Orther Event

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            string hour, minute, second;
            second = DateTime.Now.Second.ToString();
            hour = DateTime.Now.Hour.ToString();
            minute = DateTime.Now.Minute.ToString();
            if (second.Length < 2)
                lbgiay.Text = "0" + second;
            else
                lbgiay.Text = second;
            if (minute.Length < 2)
                lbGio.Text = hour + ":0" + minute;
            else
                lbGio.Text = hour + ":" + minute;

            timer1.Enabled = true;
        }

        #endregion

        #endregion

        #region Load Data

        private void LoadDataPrepare()
        {
            LoadComboVisa();
            bText = false;
            bShift = true;
            DataTable dtRoom = clsMain.ReturnDataTable("SELECT TEN_BAN FROM BAN WHERE MA_BAN = " + _sIdRoomAndTable);
            if (dtRoom.Rows.Count > 0)
                lbInforName.Text = dtRoom.Rows[0][0].ToString() + " -Nhận phòng:" + String.Format("{0:dd/MM/yyyy HH:mm}", _dHourInput) + " -Trả phòng:" + String.Format("{0:dd/MM/yyyy HH:mm}", _dHourOutput);
            if (_dtMoneyHour.Rows.Count > 0 && _dtMoneyHour.Columns.Count > 1)
            {
                dtTienGio = dtGridviewProduct.Copy();
                dtTienGio.Clear();
                if (_dtMoneyHour.Rows[0]["TIENGIO_TT"].ToString() != "" && _dtMoneyHour.Rows[0]["TIENGIO_TT"].ToString() != "0")
                {
                    dtTienGio.Rows.Add("1", "Tiền giờ", _dtMoneyHour.Rows[0]["SOGIO_TT"].ToString(), _dtMoneyHour.Rows[0]["TIENGIO_TT"].ToString(), "", "1", "1", _dtMoneyHour.Rows[0]["TIENGIO_TT"].ToString(), "1", "1");
                }
                if (_dtMoneyHour.Rows[0]["TIENDEM_TT"].ToString() != "" && _dtMoneyHour.Rows[0]["TIENDEM_TT"].ToString() != "0")
                {
                    dtTienGio.Rows.Add("2", "Qua đêm", _dtMoneyHour.Rows[0]["SODEM_TT"].ToString(), _dtMoneyHour.Rows[0]["TIENDEM_TT"].ToString(), "", "2", "2", _dtMoneyHour.Rows[0]["TIENDEM_TT"].ToString(), "2", "2");
                }
                if (_dtMoneyHour.Rows[0]["TIENNGAY_TT"].ToString() != "" && _dtMoneyHour.Rows[0]["TIENNGAY_TT"].ToString() != "0")
                {
                    dtTienGio.Rows.Add("3", "Ngày đêm", _dtMoneyHour.Rows[0]["SONGAY_TT"].ToString(), _dtMoneyHour.Rows[0]["GIA_1NGAY"].ToString(), "", "3", "3", _dtMoneyHour.Rows[0]["TIENNGAY_TT"].ToString(), "3", "3");
                }
                if (_dtMoneyHour.Rows[0]["TIENGIO_VAOSOM_TT"].ToString() != "" && _dtMoneyHour.Rows[0]["TIENGIO_VAOSOM_TT"].ToString() != "0")
                {
                    dtTienGio.Rows.Add("4", "Vào sớm", _dtMoneyHour.Rows[0]["SOGIO_VAOSOM_TT"].ToString(), _dtMoneyHour.Rows[0]["TIENGIO_VAOSOM_TT"].ToString(), "", "4", "4", _dtMoneyHour.Rows[0]["TIENGIO_VAOSOM_TT"].ToString(), "4", "4");
                }
                if (_dtMoneyHour.Rows[0]["TIENGIO_RATRE_TT"].ToString() != ""&&_dtMoneyHour.Rows[0]["TIENGIO_RATRE_TT"].ToString() != "0")
                {
                    dtTienGio.Rows.Add("5", "Ra trể", _dtMoneyHour.Rows[0]["SOGIO_RATRE_TT"].ToString(), _dtMoneyHour.Rows[0]["TIENGIO_RATRE_TT"].ToString(), "", "5", "5", _dtMoneyHour.Rows[0]["TIENGIO_RATRE_TT"].ToString(), "5", "5");
                }
                if (_dtMoneyHour.Rows[0]["TIENNGUOI"].ToString() != "" && _dtMoneyHour.Rows[0]["TIENNGUOI"].ToString() != "0")
                {
                    dtTienGio.Rows.Add("6", "Thêm người", _dtMoneyHour.Rows[0]["SONGUOI"].ToString(), _dtMoneyHour.Rows[0]["TIEN_1NGUOI"].ToString(), "", "6", "6", _dtMoneyHour.Rows[0]["TIENNGUOI"].ToString(), "6", "6");
                }

                gcListGIO.DataSource = dtTienGio;
            }
            gcListProduct.DataSource = dtGridviewProduct;
            pnlForeignCurrency.Size = new Size((pnlBottomPayment.Width / 100) * 35, 294);
            txtInput = txtInputMoney;
            txtInput.Focus();
            txtInput.SelectionStart = 1;
            pnlServiceMoney.Visible = pnlInnerCustomer.Visible = pnlDearCustomer.Visible = pnlInputDiscount.Visible = pnlInputVoucher.Visible = false;
            pnlInputGetMoney.Visible = true;
            pnlKeyboardMoney.BringToFront();
            pnlShowInforPayment.BringToFront();
            txtInput.KeyPress += new KeyPressEventHandler(txtInput_KeyPress);

            //thông tin kho,quầy,ca bán
            timer1.Start();
            lb_Quay.Text = cls_ConfigCashier.nameCashier;
            lb_Kho.Text = cls_ConfigCashier.nameWarehouse;
            lb_NhanVien.Text = clsUserManagement.ReturnNameOfUserLogin();
            lbCaBan.Text = clsMain.GetServerDate().ToString("dd-MM-yyyy");
            lbtimeby.Text = cls_ConfigCashier.sTenCB + ": " + cls_ConfigCashier.sGioBD + " --> " + cls_ConfigCashier.sGioKT;

            //tính tổng tiền
            decimal fTotalMoney = 0;
            if (dtGridviewProduct.Rows.Count > 0)
            {
                for (int i = 0; i < dtGridviewProduct.Rows.Count; i++)
                {
                    fTotalMoney += decimal.Parse(dtGridviewProduct.Rows[i]["Thanh_Tien"].ToString());
                }
            }
            lbTheMoney.Tag = fTotalMoney;
            lbTheMoney.Text = string.Format("{0:#,###0}", fTotalMoney);
            lbMoneyGetFirst.Tag = _dMoneygetFirst;
            lbMoneyGetFirst.Text = string.Format("{0:#,###0}", _dMoneygetFirst);
            lbHourMoney.Tag = _dMoneyHour;
            lbHourMoney.Text = string.Format("{0:#,###0}", _dMoneyHour);
            lbTheMoney.Location = new Point(pnlInforMoney.Width - lbTheMoney.Width-4, 26);
            lbHourMoney.Location = new Point(pnlInforMoney.Width - lbHourMoney.Width-4, 7);
            lbTotalMoney.Location = new Point(pnlInforMoney.Width - lbTotalMoney.Width-4, 45);
            lbCurrentcy.Location = new Point(pnlHeaderCurrentcy.Width/2-lbCurrentcy.Width/2, 8);
           
            lbcurrencyService.Location = new Point(pnlServiceMoney.Width - lbcurrencyService.Width+2 , 15);
            lbcurrencyGetMoney.Location = new Point(pnlInputGetMoney.Width - lbcurrencyGetMoney.Width +2, 15);
            int iWidth = pnlMoneyReply.Width + pnlMoneyReply.Location.X - lbVNDReply.Width - pnlKeyboardAndShowForeignCurrency.Location.X;
            txtInnerCustomer.Size = txtDearCustomer.Size = txtInputMoney.Size = txtServiceMoney.Size = txtInputDiscount.Size=txtInputVoucher.Size = new Size(iWidth, 32);
            txtInnerCustomer.Location = txtDearCustomer.Location = txtInputMoney.Location = txtServiceMoney.Location = txtInputDiscount.Location=txtInputVoucher.Location = new Point(pnlKeyboardAndShowForeignCurrency.Location.X - pnlInputDiscount.Location.X, 7);

            txtInput.Text = lbTotalMoneyAll.Text;
            txtInput.SelectAll();
            lbReplyCustomer.Text = "0";
        }

        private void LoadForiegnCurrency()
        {
            DataTable dtForiegnCurrency = clsMain.ReturnDataTable("exec GETTYGIA");
            gcForeignCurrency.DataSource = dtForiegnCurrency;
            if (dtForiegnCurrency.Rows.Count > 0)
            {
                lbKindMoney.Text = dtForiegnCurrency.Rows[0]["MA"].ToString();
                lbExchangeRate.Text = dtForiegnCurrency.Rows[0]["TYGIA"].ToString();
            }
        }

        private void LoadKeyboardMoney()
        {
           
            int iWidth = (pnlKeyboardMoney.Width - 5) / 5;
            int iSurplusWidth = (pnlKeyboardMoney.Width - 5) % 5;
            int iHeight = (pnlKeyboardMoney.Height - 5) / 4;
            int iSurplusHeight = (pnlKeyboardMoney.Height - 5) % 4;
            int iAlignWidth = (10 + iSurplusWidth) / 2;
            int iAlignHeight = (10 + iSurplusHeight) / 2;
            //set size for button
            btnMoveLeftMoney.Size = btnMoveRightMoney.Size = btnDeleteMoney.Size = btnTranslateMoney.Size = btnEcsMoney.Size = btnOkMoney.Size = btn500.Size = btn1000.Size = btn2000.Size = btn5000.Size = btn10000.Size = btn20000.Size = btn50000.Size = btn100000.Size = btn200000.Size = btn500000.Size = new Size(iWidth - 5, iHeight - 5);
            btnPaymentMoney.Size = new Size(iWidth * 2 - 5, iHeight * 2 - 5);
            //set location for button
            //dòng 1
            btn500.Location = new Point(iAlignWidth, iAlignHeight);
            btn1000.Location = new Point(btn500.Location.X + iWidth, btn500.Location.Y);
            btn2000.Location = new Point(btn1000.Location.X + iWidth, btn500.Location.Y);
            btn5000.Location = new Point(btn2000.Location.X + iWidth, btn500.Location.Y);
            btn10000.Location = new Point(btn5000.Location.X + iWidth, btn500.Location.Y);
            //dòng 2
            btn20000.Location = new Point(iAlignWidth, btn500.Location.Y + iHeight);
            btn50000.Location = new Point(btn20000.Location.X + iWidth, btn20000.Location.Y);
            btn100000.Location = new Point(btn50000.Location.X + iWidth, btn20000.Location.Y);
            btn200000.Location = new Point(btn100000.Location.X + iWidth, btn20000.Location.Y);
            btn500000.Location = new Point(btn200000.Location.X + iWidth, btn20000.Location.Y);
            //dòng 3
            btnMoveLeftMoney.Location = new Point(iAlignWidth, btn20000.Location.Y + iHeight);
            btnMoveRightMoney.Location = new Point(btnMoveLeftMoney.Location.X + iWidth, btnMoveLeftMoney.Location.Y);
            btnDeleteMoney.Location = new Point(btnMoveRightMoney.Location.X + iWidth, btnMoveLeftMoney.Location.Y);
            btnPaymentMoney.Location = new Point(btnDeleteMoney.Location.X + iWidth, btnMoveLeftMoney.Location.Y);

            //dòng 4
            btnTranslateMoney.Location = new Point(iAlignWidth, btnMoveLeftMoney.Location.Y + iHeight);
            btnEcsMoney.Location = new Point(btnTranslateMoney.Location.X + iWidth, btnTranslateMoney.Location.Y);
            btnOkMoney.Location = new Point(btnEcsMoney.Location.X + iWidth, btnTranslateMoney.Location.Y);

        }

        private void LoadKeyboardNumber()
        {
           
            int iWidth = (pnlKeyboardNumber.Width - 5) / 6;
            int iSurplusWidth = (pnlKeyboardNumber.Width - 5) % 6;
            int iHeight = (pnlKeyboardNumber.Height - 5) / 4;
            int iSurplusHeight = (pnlKeyboardNumber.Height - 5) % 4;
            int iAlignWidth = (10 + iSurplusWidth) / 2;
            int iAlignHeight = (10 + iSurplusHeight) / 2;
            //set size for button
            btnDeleteNumber.Size=btnMoveLeftNumber.Size=btnMoveRightNumber.Size= btn0.Size = btn00.Size = btn000.Size = btn1.Size = btn2.Size = btn3.Size = btn4.Size = btn5.Size = btn6.Size = btn7.Size = btn8.Size = btn9.Size = btnTranslateNumber.Size =   new Size(iWidth - 5, iHeight - 5);
            btnPaymentNumber.Size = new Size(iWidth * 2 - 5, iHeight * 2 - 5);
            btnEscNumber.Size = new Size(iWidth - 5, iHeight * 2 - 5);
            btnOkNumber.Size = new Size(iWidth * 2 - 5, iHeight - 5);

            //set location for button
            //dòng 1
            btn7.Location = new Point(iAlignWidth, iAlignHeight);
            btn8.Location = new Point(btn7.Location.X + iWidth, btn7.Location.Y);
            btn9.Location = new Point(btn8.Location.X + iWidth, btn7.Location.Y);
            btnMoveLeftNumber.Location = new Point(btn9.Location.X + iWidth, btn7.Location.Y);
            btnMoveRightNumber.Location = new Point(btnMoveLeftNumber.Location.X + iWidth, btn7.Location.Y);
            btnDeleteNumber.Location = new Point(btnMoveRightNumber.Location.X + iWidth, btn7.Location.Y);
            //dòng 2
            btn4.Location = new Point(iAlignWidth, btn7.Location.Y + iHeight);
            btn5.Location = new Point(btn4.Location.X + iWidth, btn4.Location.Y);
            btn6.Location = new Point(btn5.Location.X + iWidth, btn4.Location.Y);
            btnEscNumber.Location = new Point(btn6.Location.X + iWidth, btn4.Location.Y);
            btnOkNumber.Location = new Point(btnEscNumber.Location.X + iWidth, btn4.Location.Y);
            //dòng 3
            btn1.Location = new Point(iAlignWidth, btn4.Location.Y + iHeight);
            btn2.Location = new Point(btn1.Location.X + iWidth, btn1.Location.Y);
            btn3.Location = new Point(btn2.Location.X + iWidth, btn1.Location.Y);
            btnPaymentNumber.Location = new Point(btnOkNumber.Location.X, btn1.Location.Y);

            //dòng 4
            btnTranslateNumber.Location = new Point(iAlignWidth, btn1.Location.Y + iHeight);
            btn0.Location = new Point(btnTranslateNumber.Location.X + iWidth, btnTranslateMoney.Location.Y);
            btn00.Location = new Point(btn0.Location.X + iWidth, btnTranslateMoney.Location.Y);
            btn000.Location = new Point(btn00.Location.X + iWidth, btnTranslateMoney.Location.Y);

        }

        private void LoadKeyboardText()
        {
            
            int iWidth = (pnlKeyBoardText.Width - 5) / 10;
            int iSurplusWidth = (pnlKeyBoardText.Width - 5) % 10;
            int iHeight = (pnlKeyBoardText.Height - 5) / 5;
            int iSurplusHeight = (pnlKeyBoardText.Height - 5) % 5;
            int iAlignWidth = (10 + iSurplusWidth) / 2;
            int iAlignHeight = (10 + iSurplusHeight) / 2;
            //set size for button
            btnQ.Size = btnW.Size = btnE.Size = btnR.Size = btnT.Size = btnY.Size = btnU.Size = btnI.Size = btnO.Size = btnP.Size = btnA.Size = btnS.Size = btnD.Size = btnF.Size = btnG.Size = btnH.Size = btnJ.Size = btnK.Size = btnL.Size = btnZ.Size = btnX.Size = btnC.Size = btnV.Size = btnB.Size = btnN.Size = btnM.Size = btnMoveLeftText.Size=btnMoveRigthText.Size= new Size(iWidth - 5, iHeight - 5);
            btnTranslateText.Size=new Size(iWidth*2-5,iHeight-5);
            btnShift.Size= btnEscText.Size  =btnDeleteText.Size = new Size(iWidth * 3 - 5, iHeight - 5);
            btnPaymentText.Size = new Size(iWidth * 3 - 5, iHeight * 2 - 5);
            btnOkText.Size = new Size(iWidth * 2 - 5, iHeight*2 - 5);

            //set location for button
            //dòng 1
            btnQ.Location = new Point(iAlignWidth, iAlignHeight);
            btnW.Location = new Point(btnQ.Location.X + iWidth, btnQ.Location.Y);
            btnE.Location = new Point(btnW.Location.X + iWidth, btnQ.Location.Y);
            btnR.Location = new Point(btnE.Location.X + iWidth, btnQ.Location.Y);
            btnT.Location = new Point(btnR.Location.X + iWidth, btnQ.Location.Y);
            btnY.Location = new Point(btnT.Location.X + iWidth, btnQ.Location.Y);
            btnU.Location = new Point(btnY.Location.X + iWidth, btnQ.Location.Y);
            btnI.Location = new Point(btnU.Location.X + iWidth, btnQ.Location.Y);
            btnO.Location = new Point(btnI.Location.X + iWidth, btnQ.Location.Y);
            btnP.Location = new Point(btnO.Location.X + iWidth, btnQ.Location.Y);
            //dòng 2
            btnA.Location = new Point(iAlignWidth+iWidth/2, btnQ.Location.Y+iHeight);
            btnS.Location = new Point(btnA.Location.X + iWidth, btnA.Location.Y);
            btnD.Location = new Point(btnS.Location.X + iWidth, btnA.Location.Y);
            btnF.Location = new Point(btnD.Location.X + iWidth, btnA.Location.Y);
            btnG.Location = new Point(btnF.Location.X + iWidth, btnA.Location.Y);
            btnH.Location = new Point(btnG.Location.X + iWidth, btnA.Location.Y);
            btnJ.Location = new Point(btnH.Location.X + iWidth, btnA.Location.Y);
            btnK.Location = new Point(btnJ.Location.X + iWidth, btnA.Location.Y);
            btnL.Location = new Point(btnK.Location.X + iWidth, btnA.Location.Y);
            //dòng 3
            btnZ.Location = new Point(iAlignWidth, btnA.Location.Y + iHeight);
            btnX.Location = new Point(btnZ.Location.X + iWidth, btnZ.Location.Y);
            btnC.Location = new Point(btnX.Location.X + iWidth, btnZ.Location.Y);
            btnV.Location = new Point(btnC.Location.X + iWidth, btnZ.Location.Y);
            btnB.Location = new Point(btnV.Location.X + iWidth, btnZ.Location.Y);
            btnN.Location = new Point(btnB.Location.X + iWidth, btnZ.Location.Y);
            btnM.Location = new Point(btnN.Location.X + iWidth, btnZ.Location.Y);
            btnShift.Location = new Point(btnM.Location.X + iWidth, btnZ.Location.Y);
           

            //dòng 4
            btnMoveLeftText.Location = new Point(iAlignWidth, btnZ.Location.Y + iHeight);
            btnMoveRigthText.Location = new Point(btnMoveLeftText.Location.X + iWidth, btnMoveLeftText.Location.Y);
            btnDeleteText.Location = new Point(btnMoveRigthText.Location.X + iWidth, btnMoveLeftText.Location.Y);
            btnOkText.Location = new Point(btnDeleteText.Location.X + iWidth*3, btnMoveLeftText.Location.Y);
            btnPaymentText.Location = new Point(btnOkText.Location.X + iWidth*2, btnMoveLeftText.Location.Y);
            //dòng 5
            btnTranslateText.Location = new Point(iAlignWidth, btnMoveLeftText.Location.Y + iHeight);
            btnEscText.Location = new Point(btnTranslateText.Location.X + iWidth*2, btnTranslateText.Location.Y);
        }

        private void LoadComboVisa()
        {
            txtIdCardVisa.GotFocus += new EventHandler(txtIdCardVisa_GotFocus);
            txtMoneyVisa.GotFocus += new EventHandler(txtMoneyVisa_GotFocus);
            txtNameVisa.GotFocus += new EventHandler(txtNameVisa_GotFocus);
            txtAddress.GotFocus += new EventHandler(txtAddress_GotFocus);
            txtCwCvc.GotFocus += new EventHandler(txtCwCvc_GotFocus);
            txtCity.GotFocus += new EventHandler(txtCity_GotFocus);
            txtIdCodeInput.GotFocus += new EventHandler(txtIdCodeInput_GotFocus);
            //txtMoneyVisa.GotFocus += new EventHandler(txtMoneyVisa_GotFocus);
            txtMonth.GotFocus += new EventHandler(txtMonth_GotFocus);
            txtYear.GotFocus += new EventHandler(txtYear_GotFocus);
            txtCountry.GotFocus += new EventHandler(txtCountry_GotFocus);
        }

        #endregion

        #region Function

        private void PlusInputMoney(int iMoney)
        {
            if (!bText)
            {
                if (txtInput.Text.Length > 15)
                {
                    int iPositon = txtInput.SelectionStart;
                    XtraMessageBox.Show("Bạn không được nhập quá 15 ký tự", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtInput.SelectionStart = txtInput.Text.Length;
                    txtInput.SelectionStart = iPositon;
                    return;
                }
                if (txtInput.SelectedText==txtInput.Text)
                {
                    txtInput.Text = "0";
                }
                decimal iTotal = decimal.Parse(txtInput.Text.Replace(",", "")) + iMoney;
                txtInput.Text = iTotal.ToString();
                txtInput.SelectionStart = txtInput.Text.Length;
            }
        }//input keyboard for money

        private void MoveToLeft()
        {
            int iPosition = txtInput.SelectionStart;
            if (iPosition > 1)
            {
                if (txtInput.Text[iPosition - 1].ToString() == ",")
                {
                    txtInput.SelectionStart = iPosition - 2;
                }
                else
                {
                    txtInput.SelectionStart = iPosition - 1;
                }
            }
            else
            {
                txtInput.SelectionStart = 0;

            }
        }

        private void MoveToRight()
        {
            int iPosition = txtInput.SelectionStart;
            if (iPosition < txtInput.Text.Length - 1)
            {
                if (txtInput.Text[iPosition + 1].ToString() == ",")
                {
                    txtInput.SelectionStart = iPosition + 2;
                }
                else
                {
                    txtInput.SelectionStart = iPosition + 1;
                }
            }
            else
            {
                txtInput.SelectionStart = txtInput.Text.Length;

            }
        }

        private void DeleteTextBox()
        {
           
            int iPosition = txtInput.SelectionStart;
            int iLenght = txtInput.Text.Length;
            if (iPosition > 1)
            {
                if (txtInput.Text[iPosition - 1].ToString() == ",")
                {
                    txtInput.Text = txtInput.Text.Remove(iPosition - 2, 1);

                    txtInput.SelectionStart = iPosition - 2;

                }
                else
                {
                    txtInput.Text = txtInput.Text.Remove(iPosition - 1, 1);
                    if (iLenght - txtInput.Text.Length == 1)
                    {
                        txtInput.SelectionStart = iPosition - 1;
                    }
                    else
                    {
                        txtInput.SelectionStart = iPosition - 2;
                    }
                }
            }
            else
                if (iPosition == 1)
                {
                    if (iLenght >= 2)
                    {
                        txtInput.Text = txtInput.Text.Remove(iPosition - 1, 1);
                        txtInput.SelectionStart = iPosition - 1;
                        if (txtInput.Text == "0")
                            txtInput.SelectionStart = 1;
                    }
                    else
                    {
                        txtInput.Text = "0";
                        txtInput.SelectionStart = 1;
                    }
                }
        }

        private void OkTextBox()
        {
            if (txtInput.Name=="txtInputMoney")
            {
                if (int.Parse(txtInput.Text.Replace(",", "")) == 0)
                    return;
                if (!pnlFirst.Visible)
                {
                    decimal MoneyInputAfterChange = decimal.Parse(txtInput.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                    lbFirst.Text = "Tiền Mặt: " + txtInput.Text + " " + gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString() + "  -->";
                    lbFirstMoney.Tag = MoneyInputAfterChange;
                    lbFirstMoney.Text = string.Format("{0:#,###0#0}", MoneyInputAfterChange) + "  VND";
                    txtInput.Text = "0";
                    txtInput.SelectionStart = 1;
                    pnlFirst.Visible = true;
                }
                else
                    if (!pnlSecond.Visible)
                    {
                        decimal MoneyInputAfterChange = decimal.Parse(txtInput.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                        lbSecond.Text = "Tiền Mặt: " + txtInput.Text + " " + gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString() + "  -->";
                        lbSecondMoney.Tag = MoneyInputAfterChange;
                        lbSecondMoney.Text = string.Format("{0:#,###0#0}", MoneyInputAfterChange) + "  VND";
                        txtInput.Text = "0";
                        txtInput.SelectionStart = 1;
                        pnlSecond.Visible = true;
                    }
                    else
                        if (!pnlThird.Visible)
                        {
                            decimal MoneyInputAfterChange = decimal.Parse(txtInput.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                            lbThird.Text = "Tiền Mặt: " + txtInput.Text + " " + gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString() + "  -->";
                            lbThirdMoney.Tag = MoneyInputAfterChange;
                            lbThirdMoney.Text = string.Format("{0:#,###0#0}", MoneyInputAfterChange) + "  VND";
                            txtInput.Text = "0";
                            txtInput.SelectionStart = 1;
                            pnlThird.Visible = true;
                        }
                        else
                        {
                            XtraMessageBox.Show("Tiền Trả Không được nhập quá 3 lần,bạn hãy nhập lại từ đầu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ResetInputMoney();
                        }
            }
            else
                if (txtInput.Name == "txtInputDiscount")
                {
                    if (int.Parse(txtInput.Text.Replace(",", "")) == 0)
                        return;
                    if (decimal.Parse(txtInputDiscount.Text.Replace(",", "")) <= 100)
                    {
                        lbDiscountPayment.Tag = (decimal.Parse(lbTotalMoneyPayment.Tag.ToString()) * decimal.Parse(txtInputDiscount.Text.Replace(",", ""))) / 100;
                        lbDiscountPayment.Text = txtInputDiscount.Text;
                        lbDiscount.Text = "        %";
                    }
                    else
                    {
                        lbDiscountPayment.Tag = decimal.Parse(txtInputDiscount.Text.Replace(",", ""));
                        lbDiscountPayment.Text = string.Format("{0:#,###0#0}", decimal.Parse(lbDiscountPayment.Tag.ToString()));
                        lbDiscount.Text = "   VND";
                    }


                    txtInput.Text = "0";
                    InputGetMoneyFocus();
                }
                else if (txtInput.Name == "txtServiceMoney")
                {
                    if (int.Parse(txtInput.Text.Replace(",", "")) == 0)
                        return;
                    lbServiceMoney.Tag = decimal.Parse(txtServiceMoney.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                    lbServiceMoney.Text = string.Format("{0:#,###0#0}", decimal.Parse(lbServiceMoney.Tag.ToString()));
                    txtInput.Text = "0";
                    InputGetMoneyFocus();
                }
                else if (txtInput.Name == "txtDearCustomer")
                {
                    if (txtInput.Text == "")
                    {
                        lbIDDearCustomerShow.Text=lbMark.Text= lbNameDearCustomer.Text = "";
                        pnlDearCustomerShow.Visible = false;
                        return;
                    }
                    if (cls_KHTT.CheckKhachHangThanThiet(txtInput.Text))
                    {
                        lbIDDearCustomerShow.Text = txtInput.Text;
                        DataTable dtCustomer = clsMain.ReturnDataTable("select khtt.SoDiem,ncc.TEN from khachhang_thanthiet khtt inner join NHACUNGCAP ncc on khtt.Ma_KHTT=ncc.MA where MAKHTT='" + lbIDDearCustomerShow.Text + "'");
                        lbNameDearCustomer.Text = dtCustomer.Rows[0]["TEN"].ToString();
                        lbMark.Text = dtCustomer.Rows[0]["SoDiem"].ToString();
                        pnlDearCustomerShow.Visible = true;
                        InputGetMoneyFocus();
                    }
                    else
                    {
                        XtraMessageBox.Show("Mã KHTT không đúng,bạn hãy kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtInput.Focus();
                    }
                }
                else if (txtInput.Name == "txtInnerCustomer")
                {
                    if (txtInput.Text == "")
                    {
                        return;
                    }
                    if (cls_KHTT.CheckThanhToanNoiBo(txtInput.Text))
                    {

                        dMoneyInnerCard = cls_KHTT.ReturnMoneyMustPay(txtInput.Text, decimal.Parse(lbTotalMoneyAll.Tag.ToString()) - decimal.Parse(lbGetCustomer.Text.Replace(",", "")));
                        if (dMoneyInnerCard == 0)
                        {
                            return;
                        }
                        if (lbIdInnerCustomerShow.Text != "")
                            return;
                        lbIdInnerCustomerShow.Text = txtInput.Text;
                        DataTable dtCustomer = clsMain.ReturnDataTable("select khttnb.SoDuTaiKhoan,ncc.TEN from khachhang_thanhtoan_noibo khttnb inner join NHACUNGCAP ncc on khttnb.Ma_KHTT=ncc.MA where MAKHTTNB='" + lbIdInnerCustomerShow.Text + "'");
                        lbNameInnerCustomer.Text = dtCustomer.Rows[0]["TEN"].ToString();
                        lbMoneySurplus.Text = string.Format("{0:#,###0#0}", decimal.Parse(dtCustomer.Rows[0]["SoDuTaiKhoan"].ToString())) + " VND";
                        pnlInnerCustomerShow.Visible = true;
                        if (!pnlFirst.Visible)
                        {

                            lbFirst.Text = "Tiền Thẻ TTNB: " + string.Format("{0:#,###0#0}", dMoneyInnerCard) + " VND -->";
                            lbFirstMoney.Tag = dMoneyInnerCard;
                            lbFirstMoney.Text = string.Format("{0:#,###0#0}", dMoneyInnerCard) + "  VND";
                            pnlFirst.Visible = true;
                        }
                        else
                            if (!pnlSecond.Visible)
                            {
                                lbSecond.Text = "Tiền Thẻ TTNB: " + string.Format("{0:#,###0#0}", dMoneyInnerCard) + " VND -->";
                                lbSecondMoney.Tag = dMoneyInnerCard;
                                lbSecondMoney.Text = string.Format("{0:#,###0#0}", dMoneyInnerCard) + "  VND";
                                pnlSecond.Visible = true;

                            }
                            else
                                if (!pnlThird.Visible)
                                {
                                    lbThird.Text = "Tiền Thẻ TTNB: " + string.Format("{0:#,###0#0}", dMoneyInnerCard) + " VND -->";
                                    lbThirdMoney.Tag = dMoneyInnerCard;
                                    lbThirdMoney.Text = string.Format("{0:#,###0#0}", dMoneyInnerCard) + "  VND";
                                    pnlThird.Visible = true;
                                }
                                else
                                {
                                    XtraMessageBox.Show("Tiển trả không được nhập quá 3 lần, bạn hãy nhập lại từ đầu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    ResetInputMoney();
                                }
                        InputGetMoneyFocus();
                    }
                    else
                    {
                        XtraMessageBox.Show("Mã KHTT không đúng, bạn hãy kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtInput.Focus();
                    }
                }           
                else
                    if (txtInput.Name == "txtInputVoucher")
                     {
                         if (decimal.Parse(txtInput.Text.Replace(",", "")) == 0)
                             return;
                        if(decimal.Parse(txtInput.Text.Replace(",", ""))>decimal.Parse( lbTotalMoneyAll.Tag.ToString()))
                        {
                            XtraMessageBox.Show("Thông Báo","Số tiền Voucher không được lớn hơn tổng tiền Thanh Toán",MessageBoxButtons.OK,MessageBoxIcon.Information);
                            txtInput.Focus();
                            return;
                        }
                         bVoucher = false;
                         dMoneyVoucher =decimal.Parse( txtInput.Text.Replace(",", ""));
                         if (!pnlFirst.Visible)
                         {
                             decimal MoneyInputAfterChange = decimal.Parse(txtInput.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                             lbFirst.Text = "Voucher: " + txtInput.Text + " " + gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString() + "  -->";
                             lbFirstMoney.Tag = MoneyInputAfterChange;
                             lbFirstMoney.Text = string.Format("{0:#,###0#0}", MoneyInputAfterChange) + "  VND";
                             txtInput.Text = "0";
                             txtInput.SelectionStart = 1;
                             pnlFirst.Visible = true;
                         }
                         else
                             if (!pnlSecond.Visible)
                             {
                                 decimal MoneyInputAfterChange = decimal.Parse(txtInput.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                                 lbSecond.Text = "Voucher:" + txtInput.Text + " " + gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString() + "  -->";
                                 lbSecondMoney.Tag = MoneyInputAfterChange;
                                 lbSecondMoney.Text = string.Format("{0:#,###0#0}", MoneyInputAfterChange) + "  VND";
                                 txtInput.Text = "0";
                                 txtInput.SelectionStart = 1;
                                 pnlSecond.Visible = true;
                             }
                             else
                                 if (!pnlThird.Visible)
                                 {
                                     decimal MoneyInputAfterChange = decimal.Parse(txtInput.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                                     lbThird.Text = "Voucher: " + txtInput.Text + " " + gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString() + "  -->";
                                     lbThirdMoney.Tag = MoneyInputAfterChange;
                                     lbThirdMoney.Text = string.Format("{0:#,###0#0}", MoneyInputAfterChange) + "  VND";
                                     txtInput.Text = "0";
                                     txtInput.SelectionStart = 1;
                                     pnlThird.Visible = true;
                                 }
                                 else
                                 {
                                     XtraMessageBox.Show("Tiền Trả Không được nhập quá 3 lần,bạn hãy nhập lại từ đầu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                     ResetInputMoney();
                                 }
                         //txtInput.Text = "0";
                         InputGetMoneyFocus();
                     }
        }

        private void EscapeTextbox()
        {
            if (txtInput.Name=="txtInputMoney")
            {
                txtInputMoney.Text = "0";
                txtInputMoney.Focus();
                txtInputMoney.SelectionStart = 1;
            }
            else
                if (txtInput.Name == "txtInputDiscount")
                {
                    txtInputDiscount.Text = "0";
                    txtInputDiscount.Focus();
                    txtInputDiscount.SelectionStart = 1;
                }
                else
                if (txtInput.Name == "txtInputVoucher")
                {
                    txtInputVoucher.Text = "0";
                    txtInputVoucher.Focus();
                    txtInputVoucher.SelectionStart = 1;
                }
                else if (txtInput.Name == "txtDearCustomer")
                {
                    txtDearCustomer.Text = "";
                    txtDearCustomer.Focus();
                    txtDearCustomer.SelectionStart =0;
                }
                else if (txtInput.Name == "txtInnerCustomer")
                {
                    txtInnerCustomer.Text = "";
                    txtInnerCustomer.Focus();
                    txtInnerCustomer.SelectionStart = 0;
                }
                
        }

        private void InputKeyboardNumber(string sInput)
        {
            if (txtInput.Text.Length >= 15)
            {
                int iPositon = txtInput.SelectionStart;
                XtraMessageBox.Show("Bạn không được nhập quá 15 ký tự", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtInput.SelectionStart = txtInput.Text.Length;
                txtInput.SelectionStart = iPositon;
                return;
            }

            if (txtInput.Text == "0")
            {
                if (sInput == "0" || sInput == "00" || sInput == "000")
                    return;
            }
            if (txtInput.SelectedText==txtInput.Text)
            {
                txtInput.Text = "0";
            }
            int iLenght, iPosition;
            iLenght = txtInput.Text.Length;
            iPosition = txtInput.SelectionStart;
            if (iPosition == 0)
            {
                if (sInput == "0" || sInput == "00" || sInput == "000")
                    return;
            }

            if(txtInput.Text == "0")
            {
                txtInput.Text = sInput;
            }
            else
            {
                txtInput.Text = txtInput.Text.Insert(iPosition, sInput);
            }
            if (sInput.Length == 1)
            {
                if (txtInput.Text.Length - iLenght == 1)
                {
                    txtInput.SelectionStart = iPosition + 1;
                }
                else
                {
                    txtInput.SelectionStart = iPosition + 2;
                }
            }
            else
                if (sInput.Length == 2)
                {
                    if (txtInput.Text.Length - iLenght == 2)
                    {
                        txtInput.SelectionStart = iPosition + 2;
                    }
                    else
                    {
                        txtInput.SelectionStart = iPosition + 3;
                    }
                }
                else
                    if (sInput.Length == 3)
                    {
                        if (txtInput.Text.Length - iLenght == 3)
                        {
                            txtInput.SelectionStart = iPosition + 3;
                        }
                        else
                        {
                            txtInput.SelectionStart = iPosition + 4;
                        }
                    }


        }//input keyboard for number

        private void InputKeyboardText(string sInput)
        {
            if (bText)
            {
                if (txtInput.Text.Length > 15)
                {
                    int iPositon = txtInput.SelectionStart;
                    XtraMessageBox.Show("Bạn không được nhập quá 15 ký tự", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtInput.SelectionStart = txtInput.Text.Length;
                    txtInput.SelectionStart = iPositon;
                    return;
                }
                if (bShift)
                    sInput = sInput.ToUpper();
                else
                    sInput = sInput.ToLower();
                int iPosition = txtInput.SelectionStart;
                txtInput.Text = txtInput.Text.Insert(iPosition, sInput);
                txtInput.SelectionStart = iPosition + 1;
            }

        }//input keyboard for Text

        private void UpdateTotalMoneyPayment()
        {
            decimal fTotal = decimal.Parse(lbTotalMoneyPayment.Tag.ToString()) + decimal.Parse(lbServiceMoney.Tag.ToString()) - decimal.Parse(lbDiscountPayment.Tag.ToString()) - decimal.Parse(lbMoneyGetFirst.Tag.ToString());
            lbTotalMoneyAll.Tag = fTotal;
            lbTotalMoneyAll.Text = string.Format("{0:#,###0#0}", fTotal);
            //txtInputMoney.Text = lbTotalMoneyAll.Text;
        }

        private void UpdateTotalGridview()
        {
            decimal fTotal = decimal.Parse(lbTheMoney.Tag.ToString()) + decimal.Parse(lbHourMoney.Tag.ToString());
            lbTotalMoney.Tag=fTotal;
            lbTotalMoney.Text = string.Format("{0:#,###0#0}", fTotal);
            //txtInputMoney.Text = lbTotalMoneyAll.Text;
        }

        private string CreateIdCode()
        {
            string sRandom = "";
            int[] sRandomArr = new int[70];
            int iCount = 0;
            for (int i = 48; i <= 57; i++)
            {
                sRandomArr[iCount] = i;
                iCount++;
            }
            for (int i = 65; i <= 90; i++)
            {
                sRandomArr[iCount] = i;
                iCount++;
            }
            for (int i = 97; i <= 122; i++)
            {
                sRandomArr[iCount] = i;
                iCount++;
            }
            Random rd = new Random();
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);

            return sRandom;

        }
        /// <summary>
        /// Saver information 
        /// </summary>

        private void DeleteOrder(int maHH, int maBan)
        {
            string sql = "Delete From DISPLAY_ORDER where MA_HANGHOA = " + maHH + " and MA_BAN = " + maBan;
            clsMain.ReturnDataTable(sql);
        }

        private void PrintBill(int flag)
        {
            if (pnlInputGetMoney.Visible)
            {
                if (!pnlFirst.Visible)
                {
                    OkTextBox();
                }
            }
            if (decimal.Parse(lbReplyCustomer.Tag.ToString()) < 0)
            {
                XtraMessageBox.Show("Chưa thanh toán hết tiền, yêu cầu nhập số tiền thanh toán", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dMoneyVoucher > decimal.Parse(lbTotalMoneyAll.Tag.ToString()))
            {
                dMoneyCash = decimal.Parse(lbTotalMoneyAll.Tag.ToString()) - dMoneyVoucher;
                dMoneyVisa = 0;
                dMoneyInnerCard = 0;

            }
            else
                if ((dMoneyVoucher + dMoneyVisa) > decimal.Parse(lbTotalMoneyAll.Tag.ToString()))
                {
                    dMoneyCash = decimal.Parse(lbTotalMoneyAll.Tag.ToString()) - (dMoneyVoucher + dMoneyVisa);
                    dMoneyInnerCard = 0;
                }
                else
                {
                    dMoneyCash = decimal.Parse(lbTotalMoneyAll.Tag.ToString()) - (dMoneyVoucher + dMoneyVisa + dMoneyInnerCard);
                }
            if (lbIdInnerCustomerShow.Text != "")
            {
                cls_KHTT.UpdateInnerCustomer(lbIdInnerCustomerShow.Text, dMoneyInnerCard);
            }
            if (lbIDDearCustomerShow.Text != "")
            {
                cls_KHTT.CongDiemKhachHangThanThiet(lbIDDearCustomerShow.Text, decimal.Parse(lbTotalMoneyAll.Tag.ToString()));
            }
            //Payments
            string nameTable = clsMain.ReturnDataTable("select TEN_BAN from BAN where MA_BAN = " + _sIdRoomAndTable + "").Rows[0][0].ToString();
            List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
            if (dtGridviewProduct.Rows.Count > 0)
            {
                for (int i = 0; i < dtGridviewProduct.Rows.Count; i++)
                {
                    DM_HangHoa dmHangHoa = new DM_HangHoa();
                    dmHangHoa.STT = (i + 1).ToString();
                    dmHangHoa.Ten_HoaDon = "PHIẾU THANH TOÁN";
                    dmHangHoa.MA_HANGHOA = dtGridviewProduct.Rows[i]["MA_HANGHOA"].ToString();
                    dmHangHoa.TEN_HANGHOA = dtGridviewProduct.Rows[i]["TEN_HANGHOA"].ToString();
                    dmHangHoa.SOLUONG = decimal.Parse(dtGridviewProduct.Rows[i]["SOLUONG"].ToString());
                    dmHangHoa.GIABAN = decimal.Parse(dtGridviewProduct.Rows[i]["GIABAN"].ToString());
                    dmHangHoa.Thanh_Tien = decimal.Parse(dtGridviewProduct.Rows[i]["Thanh_Tien"].ToString());
                    dmHangHoa.Tong_Cong = lbTotalMoneyAll.Text;
                    dmHangHoa.Tien_TraKhach = lbReplyCustomer.Text;
                    if (lbGetCustomer.Text != "0")
                        dmHangHoa.Tien_KhachTra = lbGetCustomer.Text;
                    else
                    {
                        dmHangHoa.Tien_KhachTra = txtInputMoney.Text;
                        dmHangHoa.Tien_TraKhach = (double.Parse(txtInputMoney.Text.ToString()) - double.Parse(lbTotalMoneyAll.Text.ToString())).ToString();
                        lbReplyCustomer.Text = dmHangHoa.Tien_TraKhach;
                    }
                    dmHangHoa.Phiphucvu = lbServiceMoney.Text;
                    dmHangHoa.Tiendatcoc = lbMoneyGetFirst.Text;
                    dmHangHoa.Giam_Gia = lbDiscountPayment.Text;
                    dmHangHoa.Ma_HoaDon = _sIdBill;
                    dmHangHoa.Ma_Quay = cls_ConfigCashier.nameCashier;
                    dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                    dmHangHoa.ngayHD = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                    dmHangHoa.MaBan = _sIdRoomAndTable;
                    dmHangHoa.TEN_BAN = nameTable;
                    dmHangHoa.TienGio = lbHourMoney.Text;
                    if (_dtMoneyHour.Rows.Count > 0)
                    {
                        dmHangHoa.CheckIn = _dHourInput;
                        dmHangHoa.CheckOut = _dHourOutput;
                    }
                    else //tránh trường hợp dtTiengio ko có dòng nào sẽ bị lỗi, để thời gian mặc định
                    {
                        //dmHangHoa.Giovao = Convert.ToDateTime("01/01/1900 12:00:00 AM");
                        //dmHangHoa.Giora = Convert.ToDateTime("01/01/1900 12:00:00 AM");
                    }
                    listDM_HangHoa.Add(dmHangHoa);
                    //Delete dislay order
                    DeleteOrder(int.Parse(dmHangHoa.MA_HANGHOA), int.Parse(_sIdRoomAndTable));
                }
            }
            else
            {
                DM_HangHoa dmHangHoa = new DM_HangHoa();
                dmHangHoa.Ten_HoaDon = "PHIẾU THANH TOÁN";
                dmHangHoa.Tong_Cong = lbTotalMoneyAll.Text;
                dmHangHoa.Tien_TraKhach = lbReplyCustomer.Text;
                if (lbGetCustomer.Text != "0")
                    dmHangHoa.Tien_KhachTra = lbGetCustomer.Text;
                else
                {
                    dmHangHoa.Tien_KhachTra = txtInputMoney.Text;
                    dmHangHoa.Tien_TraKhach = (double.Parse(txtInputMoney.Text.ToString()) - double.Parse(lbTotalMoneyAll.Text.ToString())).ToString();
                    lbReplyCustomer.Text = dmHangHoa.Tien_TraKhach;
                }
                dmHangHoa.Phiphucvu = lbServiceMoney.Text;
                dmHangHoa.Tiendatcoc = lbMoneyGetFirst.Text;
                dmHangHoa.Giam_Gia = lbDiscountPayment.Text;
                dmHangHoa.Ma_HoaDon = _sIdBill;
                dmHangHoa.Ma_Quay = cls_ConfigCashier.nameCashier;
                dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                dmHangHoa.ngayHD = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                dmHangHoa.MaBan = _sIdRoomAndTable;
                dmHangHoa.TEN_BAN = nameTable;
                dmHangHoa.TienGio = lbHourMoney.Text;
                if (_dtMoneyHour.Rows.Count > 0)
                {
                    dmHangHoa.CheckIn = _dHourInput;
                    dmHangHoa.CheckOut = _dHourOutput;
                }
                else //tránh trường hợp dtTiengio ko có dòng nào sẽ bị lỗi, để thời gian mặc định
                {
                    //dmHangHoa.Giovao = Convert.ToDateTime("01/01/1900 12:00:00 AM");
                    //dmHangHoa.Giora = Convert.ToDateTime("01/01/1900 12:00:00 AM");
                }
                listDM_HangHoa.Add(dmHangHoa);
            }
            convert cvrt = new convert();
            DataTable bill = cvrt.ConvertToDataTable(listDM_HangHoa);
            //Thêm thông tin thời gian thuê phòng
            string sGiai_Thich_Tien_Phong = "";
            if (dtTienGio.Rows.Count > 0)
            {
                for (int i = 0; i < dtTienGio.Rows.Count; i++)
                {
                    sGiai_Thich_Tien_Phong = sGiai_Thich_Tien_Phong + "-" + dtTienGio.Rows[i]["TEN_HANGHOA"].ToString() + ": " + dtTienGio.Rows[i]["SOLUONG"].ToString() + " -> " + int.Parse ( dtTienGio.Rows[i]["Thanh_Tien"].ToString()).ToString ("N0") + "\n";
                }
            }
            DataColumn dcGiaiThich = new DataColumn();
            dcGiaiThich.ColumnName = "GIAITHICH";
            dcGiaiThich.DefaultValue = sGiai_Thich_Tien_Phong;
            bill.Columns.Add(dcGiaiThich);
            //
            Frm_Report1 frm = new Frm_Report1();
            frm.ReportName = "rpt_inhoadonKhachSan";
            frm.DataSource = bill;
            frm.WindowState = FormWindowState.Minimized;
            frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
            if (flag == 1)
            {
                frm.IsPrint = true;
                frm.ShowDialog();
            }
            else
            {
                frm.IsPrint = false;
            }
            frm.Dispose();
            txtInputMoney.Text = "";
            int thu = int.Parse(lbTotalMoneyAll.Text.Replace(",", ""));
            string sSQL = string.Empty;
            sSQL += "Update HOADON_KHACHSAN Set" + "\n";
            sSQL += "ISPAYMENT=1,";
            sSQL += "GIAMGIA=" + lbDiscountPayment.Text.Replace(",", "") + ",";
            sSQL += "TIEN_TRAKHACH =" + lbReplyCustomer.Text.Replace(",", "") + ",";
            sSQL += "TIEN_KHACHTRA =" + lbGetCustomer.Text.Replace(",", "") + ",";
            sSQL += "TONGTIEN=" + lbTotalMoneyAll.Text.Replace(",", "") + ",";
            sSQL += "TONGTIENPHONG=" + lbHourMoney.Text.Replace(",", "") + ",";
            sSQL += "CHECKOUT='" + string.Format("{0:dd/MM/yyyy HH:mm:ss}",_dHourOutput) + "',";
            sSQL += "PHIPHUCVU=" + float.Parse(lbServiceMoney.Text) + ",";
            sSQL += "SOTHETT='" + soThe + "',";
            sSQL += "NGAYTHETT='" + ngayThe + "',";
            sSQL += "TIENTRATHETT=" + dMoneyVisa + ",";
            sSQL += "MONEYVISA=" + dMoneyVisa + ",";
            sSQL += "MONEYINNERCARD=" + dMoneyInnerCard + ",";
            sSQL += "MONEYVOUCHER=" + dMoneyVoucher + ",";
            sSQL += "MONEYCASH=" + dMoneyCash + ",";
            sSQL += "MANVINHOADON=" + clsGlobal.gsMaNVOfUserLogin + ",";
            sSQL += "NGAYINHOADON = GETDATE()" + ",";
            sSQL += "GIAITHICH = " + clsMain.SQLStringUnicode(sGiai_Thich_Tien_Phong) + "\n";
            sSQL += "Where MA_HOADON='" + _sIdBill + "'";
            clsMain.ExecuteSQL(sSQL);
            DataTable dtTables = clsMain.ReturnDataTable("Select MA_BAN From CT_MOBAN_KHACHSAN where MA_HOADON ='" + _sIdBill + "'");
            if (dtTables.Rows.Count > 0)
            {
                for (int i = 0; i < dtTables.Rows.Count; i++)
                {
                    string sql = "Update BAN SET ISMO_BAN = 0,KHACHRANGOAI=0, SEE_PAYMENTS = 0,VESINH=1 where MA_BAN =" + dtTables.Rows[i]["MA_BAN"].ToString() + "\n";
                    sql += "Update KS_DATPHONG  SET TRANGTHAI=2 Where TRANGTHAI=1 AND maphong =" + clsMain.SQLString(dtTables.Rows[i]["MA_BAN"].ToString()) + " AND HOADON=" + clsMain.SQLString(_sIdBill) + "\n";
                    clsMain.ExecuteSQL(sql);
                }
            }
            clsMain.ExecuteSQL(string.Format("Exec SP_TRUKHO_DINHLUONG @MA_HOADON='{0}', @IS_PAYMENT={1}", _sIdBill, 1));
            clsMain.ExecuteSQL(string.Format("Exec SP_TRUKHO_HOADON @MA_HOADON='{0}', @IS_PAYMENT={1}", _sIdBill, 1));
           
            this.Close();
        }
        #endregion

        #region reset control,data

        private void ResetInputMoney()
        {
            bVoucher = bVisaCard = true;
            lbThirdMoney.Tag = lbSecondMoney.Tag = lbFirstMoney.Tag = 0;
            lbFirst.Text = lbFirstMoney.Text = lbSecond.Text = lbSecondMoney.Text = lbThird.Text = lbThirdMoney.Text = string.Empty;
            pnlFirst.Visible = pnlSecond.Visible = pnlThird.Visible = false;
           
            lbNameInnerCustomer.Text = lbIdInnerCustomerShow.Text = lbMoneySurplus .Text= "";
            pnlInnerCustomerShow.Visible = false;
            txtInputMoney.Text = "0";
            txtInputMoney.SelectionStart = 1;
            txtDearCustomer.Text = txtInnerCustomer.Text = "";
            dMoneyInnerCard = 0;
            dMoneyVisa = 0;
            dMoneyVoucher = 0;
            dMoneyCash = 0;
        }

        public static void ResetControlValue(Control parent)
        {

            foreach (Control c in parent.Controls)
            {
                if (c.Controls.Count > 0)
                {
                    switch (c.GetType().ToString())
                    {
                        case "DevExpress.XtraEditors.TextEdit":
                            ((TextEdit)c).Text = "";
                            break;
                        case "DevExpress.XtraEditors.DateEdit":
                            ((DateEdit)c).EditValue = clsMain.GetServerDate();
                            break;
                        case "DevExpress.XtraEditors.CheckEdit":
                            ((CheckEdit)c).Checked = true;
                            break;
                        case "DevExpress.XtraEditors.LookupEdit":
                            ((LookUpEdit)(c)).EditValue = ((LookUpEdit)c).Properties.GetDataSourceValue(((LookUpEdit)c).Properties.ValueMember, 0);
                            break;
                        case "DevExpress.XtraEditors.GridLookUpEdit":
                            ((GridLookUpEdit)(c)).EditValue = ((GridLookUpEdit)c).Properties.GetKeyValue(0);
                            break;
                        default:
                            ResetControlValue(c);
                            break;
                    }
                }
                else
                {
                    switch (c.GetType().ToString())
                    {
                        case "DevExpress.XtraEditors.CheckEdit":
                            ((CheckEdit)c).Checked = true;
                            break;

                        default:
                            break;
                    }
                }
            }
        }

        #endregion

      
      
    }
}