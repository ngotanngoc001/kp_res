﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;
using KP_RES.Class;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_DSHoaDon_Khachsan : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt;

        public Frm_DSHoaDon_Khachsan()
        {
            InitializeComponent();
            dtpTungay.EditValue = clsGlobal.gdServerDate;
            dtpDenngay.EditValue = clsGlobal.gdServerDate;
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show(_kocodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                FILL.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
                FILL.Visible = true ;
            }
        }

        private void btnChitiet_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle < 0)
                    throw new Exception(_chuachonhddexem);

                Frm_BCChitietHoaDon_Khachsan frm = new Frm_BCChitietHoaDon_Khachsan(gridView1.GetFocusedRowCellValue(MA_HOADON).ToString());
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.WindowState = FormWindowState.Maximized;
                frm.ShowDialog();
                frm.Dispose();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    string sSQL = "";
                    
                    sSQL = "";
                    sSQL += "Select ROW_NUMBER() over (order by (select 1)) as STT,'' as TenNV ,0 as CHIETKHAU,";
                    sSQL += "cthd.GIABAN, (cthd.SOLUONG * cthd.GIABAN) - (cthd.SOLUONG * cthd.GIABAN) * 0 / 100 as Thanh_Tien, hd.NGAYTAO, ISNULL(hd.CHECKOUT,hd.NGAYTAO) as ngayHD,hh.TEN_HANGHOA, cthd.SOLUONG,";
                    sSQL += "hd.PHIPHUCVU as Phiphucvu,hd.MA_HOADON,TEN_QUAY as MA_QUAY, ISNULL(hd.TIENDATCOC,0) as TIENDATCOC,";
                    sSQL += "case when ISNULL(hd.MANVINHOADON,'')='' Then (hd.MANHANVIEN) Else (hd.MANVINHOADON) End as MANHANVIEN,";
                    sSQL += "hd.GIAMGIA as Giam_Gia1, hd.TONGTIEN as Tong_Cong, hd.TONGTIENPHONG as TienGio, hd.CHECKIN, hd.CHECKOUT, hd.GIAITHICH , hd.TIEN_KHACHTRA, hd.TIEN_TRAKHACH, hd.TONGTIEN, N'PHIẾU THANH TOÁN \n (IN LẠI)' as Ten_HoaDon,'' AS TenNhanVien,'' AS GhiChu,C.TEN AS TENKH,ISNULL(C.MA,'') AS MA_KHACHHANG,ISNULL(C.DIACHI,'') AS DIACHIGIAOHANG,ISNULL(C.DIENTHOAI,'') AS DIENTHOAI_KH" + "\n";
                    sSQL += "From HANGHOA hh inner join CT_HOADON_KHACHSAN cthd on hh.MA_HANGHOA = cthd.MA_HANGHOA inner join HOADON_KHACHSAN hd on cthd.MA_HOADON = hd.MA_HOADON" + "\n";
                    sSQL += "inner join QUAY q on hd.MA_QUAY = q.MA_QUAY LEFT JOIN KHTT C ON hd.MA_KHACHHANG = C.MA " + "\n";
                    sSQL += "Where hd.MA_HOADON =" + clsMain.SQLString(gridView1.GetFocusedRowCellValue(MA_HOADON).ToString());

                    DataTable dtReport = clsMain.ReturnDataTable(sSQL);
                    dtReport.Columns.Add("Giam_Gia", typeof(String));

                    string TenNV = "";
                    if (dtReport.Rows.Count > 0)
                    {
                        sSQL = "Select TENNHANVIEN From DM_NHANVIEN Where MANHANVIEN=" + clsMain.SQLString(dtReport.Rows[0]["MANHANVIEN"].ToString());
                        DataTable dtTenNV = clsMain.ReturnDataTable(sSQL);
                        if (dtTenNV.Rows.Count > 0)
                            TenNV = dtTenNV.Rows[0]["TENNHANVIEN"].ToString();
                    }

                    sSQL = "Select TEN_BAN From CT_MOBAN_KHACHSAN ct,BAN b Where ct.MA_BAN=b.MA_BAN And ct.MA_HOADON=" + clsMain.SQLString(gridView1.GetFocusedRowCellValue(MA_HOADON).ToString());
                    DataTable dtTenban = clsMain.ReturnDataTable(sSQL);
                    if (dtTenban.Rows.Count > 0)
                    {
                        dtReport.Columns.Add("TEN_BAN", typeof(String));

                        int n = dtReport.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            dtReport.Rows[i]["TEN_BAN"] = dtTenban.Rows[0]["TEN_BAN"].ToString();
                            dtReport.Rows[i]["TenNV"] = TenNV;
                            if (double.Parse(dtReport.Rows[i]["Giam_Gia1"].ToString()) > 100)
                                dtReport.Rows[i]["Giam_Gia"] = string.Format("{0:#,###0}", dtReport.Rows[i]["Giam_Gia1"]);
                            else
                                dtReport.Rows[i]["Giam_Gia"] = dtReport.Rows[i]["Giam_Gia1"].ToString();
                        }
                        Frm_BCInPhieu frm = new Frm_BCInPhieu();
                        frm.WindowState = FormWindowState.Maximized;
                        frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        frm.dtSource = dtReport;
                        if (dtReport.Rows[0]["MA_KHACHHANG"].ToString() == "")
                        {
                            if (cls_KP_RES.Mode == 8)
                                frm.Mode = 100;
                            else
                                frm.Mode = 113;
                        }
                        else
                        {
                            if (cls_KP_RES.Mode == 8)
                                frm.Mode = 101;
                            else
                                frm.Mode = 98;
                        }

                        frm.ShowDialog();
                        frm.Dispose();
                    }
                    else
                    {
                        int n = dtReport.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            dtReport.Rows[i]["TenNV"] = TenNV;
                            if (double.Parse(dtReport.Rows[i]["Giam_Gia1"].ToString()) > 100)
                                dtReport.Rows[i]["Giam_Gia"] = string.Format("{0:#,###0}", dtReport.Rows[i]["Giam_Gia1"]);
                            else
                                dtReport.Rows[i]["Giam_Gia"] = dtReport.Rows[i]["Giam_Gia1"].ToString() + "%";
                        }
                        Frm_BCInPhieu frm = new Frm_BCInPhieu();
                        frm.WindowState = FormWindowState.Maximized;
                        frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        frm.dtSource = dtReport;
                        if (dtReport.Rows[0]["MA_KHACHHANG"].ToString() == "")
                        {
                            if (cls_KP_RES.Mode == 8)
                                frm.Mode = 100;
                            else
                                frm.Mode = 113;
                        }
                        else
                        {
                            if (cls_KP_RES.Mode == 8)
                                frm.Mode = 101;
                            else
                                frm.Mode = 99;
                        }

                        frm.ShowDialog();
                        frm.Dispose();
                    }
                }
                else
                {
                    XtraMessageBox.Show(_kocodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception EX)
            {
                XtraMessageBox.Show(EX.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally { this.Cursor = Cursors.Default; }   
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
                panelControl7.Visible = false;
                panelControl8.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
                panelControl7.Visible = true;
                panelControl8.Visible = true;
            }
            this.Refresh();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        private void LoadcboDATA()
        {
            cboDATA.Properties.DataSource = null;
            cboDATA.Properties.Columns.Clear();
            cboDATA.ItemIndex = -1;
            if (optGroup.SelectedIndex == 0)//cửa hàng
            {
                dt = clsMain.ReturnDataTable("select MA_CUAHANG as MA,TEN_CUAHANG as TEN from CUAHANG where SUDUNG=1 order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên cửa hàng" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = _cuahang;
            }
            else if (optGroup.SelectedIndex == 1)//kho
            {
                dt = clsMain.ReturnDataTable("select MA_KHO as MA,TEN_KHO as TEN from KHO where SUDUNG=1 order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên kho" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = _kho;
            }
            else if (optGroup.SelectedIndex == 2)//quầy
            {
                dt = clsMain.ReturnDataTable("select MA_QUAY as MA,TEN_QUAY as TEN from QUAY where SUDUNG=1 order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên quầy" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = _quay;
            }
            else if (optGroup.SelectedIndex == 3)//nhóm hàng
            {
                dt = clsMain.ReturnDataTable("select MA_NHOMHANG as MA,TEN_NHOMHANG as TEN from NHOMHANG where SUDUNG=1 order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên nhóm" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = _nhomhang;
            }
            else if (optGroup.SelectedIndex == 4)//hàng hóa
            {
                dt = clsMain.ReturnDataTable("select MA_HANGHOA as MA,TEN_HANGHOA as TEN from HANGHOA where SUDUNG=1 order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên mặt hàng" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = _mathang;
            }
            else if (optGroup.SelectedIndex == 5)//nhân viên
            {
                dt = clsMain.ReturnDataTable("select MANHANVIEN as MA,TENNHANVIEN as TEN from DM_NHANVIEN where SUDUNG=1 order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên nhân viên" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = _nhanvien;
            }
            cboDATA.Properties.AutoSearchColumnIndex = 1;
        }

        private void taocolumncholookupedit(String[] Caption, String[] FieldName, bool[] visible, DataTable dt)
        {
            if (Caption.Length > 1 & dt.Rows.Count > 0)
            {
                for (int i = 0; i < Caption.Length; i++)
                {
                    LookUpColumnInfo info = new LookUpColumnInfo();
                    info.FieldName = FieldName[i].TrimEnd();
                    info.Caption = Caption[i].TrimEnd();
                    info.Visible = visible[i];
                    info.Alignment = DevExpress.Utils.HorzAlignment.Default;
                    cboDATA.Properties.Columns.Add(info);
                }
                cboDATA.Properties.BestFitMode = BestFitMode.None;
                cboDATA.Properties.SearchMode = SearchMode.AutoFilter;
                cboDATA.Properties.DisplayMember = FieldName[1];
                cboDATA.Properties.ValueMember = FieldName[0];
                if (visible[0] == true)
                {
                    cboDATA.Properties.Columns[0].Width = 100;
                    cboDATA.Properties.Columns[1].Width = 500;
                }
                else
                {
                    cboDATA.Properties.Columns[1].Width = 100;
                    cboDATA.Properties.Columns[2].Width = 500;
                }
                if (dt.Rows.Count > 0)
                    cboDATA.EditValue = int.Parse(dt.Rows[0][0].ToString());
            }
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            string sql = string.Empty;
            try
            {
                if (cboDATA.EditValue == null || cboDATA.EditValue == DBNull.Value)
                    throw new Exception(_chuachondtcanxem);
               
                if (optGroup.SelectedIndex == 0)//Cửa hàng
                {
                    sql += "EXEC SP_BAOCAO_HOADON_KHACHSAN @TUNGAY=" + clsMain.SQLString(dtpTungay.Text + " 00:00:00") + ", @DENNGAY=" + clsMain.SQLString(dtpDenngay.Text + " 23:59:59") + ",@DIEUKIEN=" + clsMain.SQLString(cboDATA.EditValue.ToString()) + ",@TYPE=" + clsMain.SQLString("CH");
                }
                else if (optGroup.SelectedIndex == 1)//Kho
                {
                    sql += "EXEC SP_BAOCAO_HOADON_KHACHSAN @TUNGAY=" + clsMain.SQLString(dtpTungay.Text + " 00:00:00") + ", @DENNGAY=" + clsMain.SQLString(dtpDenngay.Text + " 23:59:59") + ",@DIEUKIEN=" + clsMain.SQLString(cboDATA.EditValue.ToString()) + ",@TYPE=" + clsMain.SQLString("KO");
                }
                else if (optGroup.SelectedIndex == 2)//Quầy
                {
                    sql += "EXEC SP_BAOCAO_HOADON_KHACHSAN @TUNGAY=" + clsMain.SQLString(dtpTungay.Text + " 00:00:00") + ", @DENNGAY=" + clsMain.SQLString(dtpDenngay.Text + " 23:59:59") + ",@DIEUKIEN=" + clsMain.SQLString(cboDATA.EditValue.ToString()) + ",@TYPE=" + clsMain.SQLString("QA");
                }
                else if (optGroup.SelectedIndex == 3)//Nhóm hàng
                {
                    sql += "EXEC SP_BAOCAO_HOADON_KHACHSAN @TUNGAY=" + clsMain.SQLString(dtpTungay.Text + " 00:00:00") + ", @DENNGAY=" + clsMain.SQLString(dtpDenngay.Text + " 23:59:59") + ",@DIEUKIEN=" + clsMain.SQLString(cboDATA.EditValue.ToString()) + ",@TYPE=" + clsMain.SQLString("NH");
                }
                else if (optGroup.SelectedIndex == 4)//Hàng hóa
                {
                    sql += "EXEC SP_BAOCAO_HOADON_KHACHSAN @TUNGAY=" + clsMain.SQLString(dtpTungay.Text + " 00:00:00") + ", @DENNGAY=" + clsMain.SQLString(dtpDenngay.Text + " 23:59:59") + ",@DIEUKIEN=" + clsMain.SQLString(cboDATA.EditValue.ToString()) + ",@TYPE=" + clsMain.SQLString("HH");
                }
                else if (optGroup.SelectedIndex == 5)//Nhân viên
                {
                    sql += "EXEC SP_BAOCAO_HOADON_KHACHSAN @TUNGAY=" + clsMain.SQLString(dtpTungay.Text + " 00:00:00") + ", @DENNGAY=" + clsMain.SQLString(dtpDenngay.Text + " 23:59:59") + ",@DIEUKIEN=" + clsMain.SQLString(cboDATA.EditValue.ToString()) + ",@TYPE=" + clsMain.SQLString("NV");
                }
                else if (optGroup.SelectedIndex == 6)//Hóa đơn giảm giá
                {
                    sql += "EXEC SP_BAOCAO_HOADON_KHACHSAN @TUNGAY=" + clsMain.SQLString(dtpTungay.Text + " 00:00:00") + ", @DENNGAY=" + clsMain.SQLString(dtpDenngay.Text + " 23:59:59") + ",@DIEUKIEN=" + clsMain.SQLString(cboDATA.EditValue.ToString()) + ",@TYPE=" + clsMain.SQLString("GG");
                }
               
                dt = clsMain.ReturnDataTable(sql);
                gridControl1.DataSource = dt;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnprint_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    DataColumn a = new DataColumn();
                    a.ColumnName = "FILTER";
                    a.DataType = System.Type.GetType("System.String");
                    a.DefaultValue = optGroup.Properties.Items[optGroup.SelectedIndex].Description + " : " + cboDATA.Text;
                    if (ContainColumn("FILTER", dt) == false)
                        dt.Columns.Add(a);
                    DataColumn a1 = new DataColumn();
                    a1.ColumnName = "TUNGAY";
                    a1.DefaultValue = string.Format("{0:dd/MM/yyyy}", dtpTungay.EditValue);
                    if (ContainColumn("TUNGAY", dt) == false)
                        dt.Columns.Add(a1);
                    DataColumn a2 = new DataColumn();
                    a2.ColumnName = "DENNGAY";
                    a2.DefaultValue = string.Format("{0:dd/MM/yyyy}", dtpDenngay.EditValue);
                    if (ContainColumn("DENNGAY", dt) == false)
                        dt.Columns.Add(a2);


                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = dt;
                    frm.Mode = 114;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show(_kocodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception EX)
            {
                XtraMessageBox.Show(EX.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally { this.Cursor = Cursors.Default; }   
        }
       
        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        string _thongbao = "";
        string _cuahang = "";
        string _kho = "";
        string _quay = "";
        string _nhomhang = "";
        string _mathang = ""; 
        string _nhanvien = "";
        string _kocodulieu = "";
        string _chuachondtcanxem = "";
        string _chuachonhddexem = "";
        private void Frm_DSHoaDon_Load(object sender, EventArgs e)
        {   
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            this.Text = rm.GetString("danhsachhoadon", culture);
            gridView1.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView1.Columns["MA_HOADON"].Caption = rm.GetString("mahoadon", culture);
            gridView1.Columns["TenNV"].Caption = rm.GetString("nhanvienban", culture);
            gridView1.Columns["NGAYTAO"].Caption = rm.GetString("ngayban", culture);
            gridView1.Columns["TONGTIEN"].Caption = rm.GetString("tongtien", culture);
            gridView1.GroupPanelText = rm.GetString("keothacotvaodaydenhom", culture);
            lb_From.Text = rm.GetString("tungay", culture);
            lb_To.Text = rm.GetString("denngay", culture);
            btnTimkiem.Text = rm.GetString("xem", culture);
            _chuachondtcanxem = rm.GetString("chuachondtcanxem", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _cuahang = rm.GetString("cuahang", culture);
            _kho = rm.GetString("kho", culture);
            _quay = rm.GetString("quay", culture);
            _nhomhang = rm.GetString("nhomhang", culture);
            _mathang = rm.GetString("mathang", culture);
            _nhanvien = rm.GetString("nhanvien", culture);
            _kocodulieu = rm.GetString("kocodulieu", culture);
            _chuachonhddexem = rm.GetString("chuachonhddexem", culture);
            optGroup.Properties.Items[0].Description = rm.GetString("theocuahang", culture);
            optGroup.Properties.Items[1].Description = rm.GetString("theokho", culture);
            optGroup.Properties.Items[2].Description = rm.GetString("theoquay", culture);
            optGroup.Properties.Items[3].Description = rm.GetString("theonhomhang", culture);
            optGroup.Properties.Items[4].Description = rm.GetString("theohanghoa", culture);
            optGroup.Properties.Items[5].Description = rm.GetString("theonhanvien", culture);
            optGroup.Properties.Items[6].Description = rm.GetString("hoadongiamgia", culture);

            LoadcboDATA();
        }
    }
}