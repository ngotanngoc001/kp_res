﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Data.SqlClient;

namespace KP_RES 
{
    public partial class Frm_LoaiTroChoi : DevExpress.XtraEditors.XtraForm
    {
        public Frm_LoaiTroChoi()
        {
            InitializeComponent();
            LoadCombo();
            LoaddataGridView();
            LoaddataGridView1(smaLOAITROCHOI);
            LoadPermission();
            btnChonLOAITROCHOI_Click(null ,null );
        }

        private void Frm_LoaiTroChoi_Load(object sender, EventArgs e)
        {
            if (fpnlLOAITROCHOI.Controls.Count > 0)
            {
                LOAITROCHOI_Click(fpnlLOAITROCHOI.Controls[0], null);
            }
            foreach (Control ctl in pnlLOAITROCHOI.Controls)
            {
                ctl.Click += new EventHandler(ctl_Click);
            }
            foreach (Control ctl in pnlTROCHOI.Controls)
            {
                ctl.Click += new EventHandler(ctl_Click);
            }
        }

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                txt.SelectAll();
            }
            catch
            {
            }
        }

        private void Frm_LoaiTroChoi_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        string smaLOAITROCHOI = "";
        string smaTROCHOI = "";
       
        private void btnLenLOAITROCHOI_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlLOAITROCHOI.AutoScrollPosition = new Point(0, fpnlLOAITROCHOI.VerticalScroll.Value - fpnlLOAITROCHOI.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnXuongLOAITROCHOI_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlLOAITROCHOI.AutoScrollPosition = new Point(0, fpnlLOAITROCHOI.VerticalScroll.Value + fpnlLOAITROCHOI.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnLenTROCHOI_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlTROCHOI.AutoScrollPosition = new Point(0, fpnlTROCHOI.VerticalScroll.Value - fpnlTROCHOI.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnXuongTROCHOI_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlTROCHOI.AutoScrollPosition = new Point(0, fpnlTROCHOI.VerticalScroll.Value + fpnlTROCHOI.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
            LoaddataGridView1(smaLOAITROCHOI);
            if (fpnlLOAITROCHOI.Controls.Count > 0)
            {
                LOAITROCHOI_Click(fpnlLOAITROCHOI.Controls[0], null);
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnChonLOAITROCHOI_Click(object sender, EventArgs e)
        {
            pnlLOAITROCHOI.Visible = true;
            pnlLOAITROCHOI.Dock = DockStyle.Fill;
            pnlTROCHOI.Visible = false;
            btnChonLOAITROCHOI.ForeColor = Color.Red;
            btnChonTROCHOI.ForeColor = btnCapnhat.ForeColor;
        }

        private void btnChonTROCHOI_Click(object sender, EventArgs e)
        {
            pnlTROCHOI.Visible = true;
            pnlTROCHOI.Dock = DockStyle.Fill;
            pnlLOAITROCHOI.Visible = false;
            btnChonTROCHOI.ForeColor = Color.Red;
            btnChonLOAITROCHOI.ForeColor = btnCapnhat.ForeColor;
        }

        private void picLOAITROCHOI_Click(object sender, EventArgs e)
        {
            if (picLOAITROCHOI.Enabled)
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = Application .StartupPath +"\\Images";
                openFileDialog1.Filter = "Image Files|*.jpg;*.bmp;*.png;*.gif|All Files|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.Title = "Chọn ảnh";
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        picLOAITROCHOI.ImageLocation = openFileDialog1.FileName;
                        picLOAITROCHOI.BackgroundImage = null; 
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {
                        picLOAITROCHOI.Image = null;
                        picLOAITROCHOI.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26; 
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void picTROCHOI_Click(object sender, EventArgs e)
        {
            if (picTROCHOI.Enabled)
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = Application.StartupPath + "\\Images";
                openFileDialog1.Filter = "Image Files|*.jpg;*.bmp;*.png;*.gif|All Files|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.Title = "Chọn ảnh";
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        picTROCHOI.ImageLocation = openFileDialog1.FileName;
                        picTROCHOI.BackgroundImage = null ; 
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {
                        picTROCHOI.Image = null;
                        picTROCHOI.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26; 
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void btnThemLOAITROCHOI_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtTENLOAITROCHOI.Focus();
        }

        private void btnSuaLOAITROCHOI_Click(object sender, EventArgs e)
        {
            if (smaLOAITROCHOI == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl(false);
            txtTENLOAITROCHOI.Focus();
        }

        private void btnXoaLOAITROCHOI_Click(object sender, EventArgs e)
        {
            if (smaLOAITROCHOI == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From LOAITROCHOI" + "\n";
            sSQL += "Where MA_LOAITROCHOI=" + clsMain.SQLString(smaLOAITROCHOI) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
                LoadCombo();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLuuLOAITROCHOI_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(picLOAITROCHOI.Image, typeof(Byte[]));
            SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
            SqlCommand SqlComMain = null; 
            string sSQL = "";
            if (smaLOAITROCHOI == "" && btnThemLOAITROCHOI.Enabled)
            {
                sSQL += "Insert into LOAITROCHOI (TEN_LOAITROCHOI,GHICHU,SUDUNG,THUCDON,STT,HINHANH) Values (@TEN_LOAITROCHOI,@GHICHU,@SUDUNG,@THUCDON,@STT,@HINHANH)" + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);
                SqlCom.Parameters.Add(new SqlParameter("@TEN_LOAITROCHOI", (object)txtTENLOAITROCHOI.Text ));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txtGHICHULOAITROCHOI.Text ));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSUDUNGLOAITROCHOI.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@THUCDON", (object)chkTHUCDONLOAITROCHOI.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@STT", (object)txtSTTLOAITROCHOI.Text));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlComMain = SqlCom;
            }
            else if ((smaLOAITROCHOI != "" && btnSuaLOAITROCHOI.Enabled))
            {
                sSQL += "Update LOAITROCHOI Set TEN_LOAITROCHOI=@TEN_LOAITROCHOI,GHICHU=@GHICHU,SUDUNG=@SUDUNG,THUCDON=@THUCDON,STT=@STT,HINHANH=@HINHANH " + "\n";
                sSQL += "Where MA_LOAITROCHOI=" + clsMain.SQLString(smaLOAITROCHOI) + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);
                SqlCom.Parameters.Add(new SqlParameter("@TEN_LOAITROCHOI", (object)txtTENLOAITROCHOI.Text));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txtGHICHULOAITROCHOI.Text));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSUDUNGLOAITROCHOI.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@THUCDON", (object)chkTHUCDONLOAITROCHOI.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@STT", (object)txtSTTLOAITROCHOI.Text));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlComMain = SqlCom;
            }
            else
            {
                SqlComMain = null;
            }

            Boolean bRunSQL = true;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                CN.Open();
                SqlComMain.ExecuteNonQuery();
                CN.Close();
                this.Cursor = Cursors.Default;
                bRunSQL = true;
            }
            catch
            {
                bRunSQL = false;
            }

            if (bRunSQL)
            {
                LoaddataGridView();
                LoadCombo();
                btnThemLOAITROCHOI.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnThemTROCHOI_Click(object sender, EventArgs e)
        {
            SetRongControl1();
            KhoaMoControl1(false);
            cboLOAITROCHOI.Focus();
        }

        private void btnSuaTROCHOI_Click(object sender, EventArgs e)
        {
            if (smaTROCHOI == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl1(false);
            cboLOAITROCHOI.Focus();
        }

        private void btnXoaTROCHOI_Click(object sender, EventArgs e)
        {
            if (smaTROCHOI == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From TROCHOI" + "\n";
            sSQL += "Where MA_TROCHOI=" + clsMain.SQLString(smaTROCHOI) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView1(smaLOAITROCHOI);
                LoadCombo();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLuuTROCHOI_Click(object sender, EventArgs e)
        {
            if (!CheckInput1())
            {
                return;
            }
            Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(picTROCHOI.Image, typeof(Byte[]));
            SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
            SqlCommand SqlComMain = null;
            string sSQL = "";
            if (smaTROCHOI == "" && btnThemTROCHOI.Enabled)
            {
                sSQL += "Insert into TROCHOI (TEN_TROCHOI,HINHANH,THUE,GIANHAP,GIABAN1,GIABAN2,GHICHU,MAVACH,SUDUNG,SUAGIA,TONKHO,TONTOITHIEU,MA_THANHPHAN,SOLUONG,MA_LOAITROCHOI,MA_DONVITINH,STT,MAYIN) Values (@TEN_TROCHOI,@HINHANH,@THUE,@GIANHAP,@GIABAN1,@GIABAN2,@GHICHU,@MAVACH,@SUDUNG,@SUAGIA,@TONKHO,@TONTOITHIEU,@MA_THANHPHAN,@SOLUONG,@MA_LOAITROCHOI,@MA_DONVITINH,@STT,@MAYIN)" + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);

                SqlCom.Parameters.Add(new SqlParameter("@TEN_TROCHOI", (object)txtTENTROCHOI.Text));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlCom.Parameters.Add(new SqlParameter("@THUE", (object)cboThue.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIANHAP", (object)txtGIAMUA.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN1", (object)txtGIABAN1.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN2", (object)txtGIABAN2.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txtGHICHUTROCHOI.Text));
                SqlCom.Parameters.Add(new SqlParameter("@MAVACH", (object)txtBlock.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSUDUNGTROCHOI.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@SUAGIA", (object)chkTinhgio.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@TONKHO", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@TONTOITHIEU", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@MA_THANHPHAN", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@SOLUONG", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@MA_LOAITROCHOI", (object)cboLOAITROCHOI.EditValue));
                SqlCom.Parameters.Add(new SqlParameter("@MA_DONVITINH", (object)cboDonvitinh.EditValue));
                SqlCom.Parameters.Add(new SqlParameter("@STT", (object)txtSTTTROCHOI.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@MAYIN", (object)cboMayin.EditValue));
          
                SqlComMain = SqlCom;
            }
            else if ((smaTROCHOI != "" && btnSuaTROCHOI.Enabled))
            {
                sSQL += "Update TROCHOI Set TEN_TROCHOI=@TEN_TROCHOI,HINHANH=@HINHANH,THUE=@THUE,GIANHAP=@GIANHAP,GIABAN1=@GIABAN1,GIABAN2=@GIABAN2,GHICHU=@GHICHU,MAVACH=@MAVACH,SUDUNG=@SUDUNG,SUAGIA=@SUAGIA,TONKHO=@TONKHO,TONTOITHIEU=@TONTOITHIEU,MA_THANHPHAN=@MA_THANHPHAN,SOLUONG=@SOLUONG,MA_LOAITROCHOI=@MA_LOAITROCHOI,MA_DONVITINH=@MA_DONVITINH,STT=@STT,MAYIN=@MAYIN " + "\n";
                sSQL += "Where MA_TROCHOI=" + clsMain.SQLString(smaTROCHOI) + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);

                SqlCom.Parameters.Add(new SqlParameter("@TEN_TROCHOI", (object)txtTENTROCHOI.Text));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlCom.Parameters.Add(new SqlParameter("@THUE", (object)cboThue.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIANHAP", (object)txtGIAMUA.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN1", (object)txtGIABAN1.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN2", (object)txtGIABAN2.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txtGHICHUTROCHOI.Text));
                SqlCom.Parameters.Add(new SqlParameter("@MAVACH", (object)txtBlock.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSUDUNGTROCHOI.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@SUAGIA", (object)chkTinhgio.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@TONKHO", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@TONTOITHIEU", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@MA_THANHPHAN", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@SOLUONG", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@MA_LOAITROCHOI", (object)cboLOAITROCHOI.EditValue));
                SqlCom.Parameters.Add(new SqlParameter("@MA_DONVITINH", (object)cboDonvitinh.EditValue));
                SqlCom.Parameters.Add(new SqlParameter("@STT", (object)txtSTTTROCHOI.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@MAYIN", (object)cboMayin.EditValue));

                SqlComMain = SqlCom;
            }
            else
            {
                SqlComMain = null;
            }

            Boolean bRunSQL = true;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                CN.Open();
                SqlComMain.ExecuteNonQuery();
                CN.Close();
                this.Cursor = Cursors.Default;
                bRunSQL = true;
            }
            catch
            {
                bRunSQL = false;
            }

            if (bRunSQL)
            {
                LoaddataGridView1(smaLOAITROCHOI);
                LoadCombo();
                btnThemTROCHOI.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LOAITROCHOI_Click(object sender, EventArgs e)
        {
            btnChonLOAITROCHOI_Click(null, null);

            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in fpnlLOAITROCHOI.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }

            LoaddataGridView1(btn.Name.Substring(1, btn.Name.Length - 1));

            smaLOAITROCHOI = btn.Name.Substring(1, btn.Name.Length - 1);
            txtTENLOAITROCHOI.Text = dtLOAITROCHOI.Select("MA=" + clsMain.SQLString(smaLOAITROCHOI))[0]["TEN"].ToString();
            txtGHICHULOAITROCHOI.Text = dtLOAITROCHOI.Select("MA=" + clsMain.SQLString(smaLOAITROCHOI))[0]["GHICHU"].ToString();
            txtSTTLOAITROCHOI.Text = dtLOAITROCHOI.Select("MA=" + clsMain.SQLString(smaLOAITROCHOI))[0]["STT"].ToString();
            chkSUDUNGLOAITROCHOI.Checked = bool.Parse(dtLOAITROCHOI.Select("MA=" + clsMain.SQLString(smaLOAITROCHOI))[0]["SUDUNG"].ToString());
            chkTHUCDONLOAITROCHOI.Checked = bool.Parse(dtLOAITROCHOI.Select("MA=" + clsMain.SQLString(smaLOAITROCHOI))[0]["THUCDON"].ToString());
            try
            {
                byte[] tam = new byte[((byte[])dtLOAITROCHOI.Select("MA=" + clsMain.SQLString(smaLOAITROCHOI))[0]["HINHANH"]).LongLength];
                tam = (byte[])dtLOAITROCHOI.Select("MA=" + clsMain.SQLString(smaLOAITROCHOI))[0]["HINHANH"];
                MemoryStream ms = new MemoryStream(tam);
                Bitmap bm = new Bitmap(ms);
                picLOAITROCHOI.Image = bm;
                picLOAITROCHOI.BackgroundImage = null; 
            }
            catch
            {
                picLOAITROCHOI.Image = null;
                picLOAITROCHOI.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26; 
            }
        }

        private void TROCHOI_Click(object sender, EventArgs e)
        {
            btnChonTROCHOI_Click(null, null);

            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in fpnlTROCHOI.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }
           
            smaTROCHOI = btn.Name.Substring(1, btn.Name.Length - 1);
            cboLOAITROCHOI.EditValue =int.Parse ( dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["MA_LOAITROCHOI"].ToString());
            cboDonvitinh.EditValue = int.Parse(dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["MA_DONVITINH"].ToString());
            cboThue.EditValue = int.Parse(dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["THUE"].ToString());
            txtTENTROCHOI.Text = dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["TEN"].ToString();
            txtGHICHUTROCHOI.Text = dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["GHICHU"].ToString();
            txtSTTTROCHOI.Text = dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["STT"].ToString();
            chkSUDUNGTROCHOI.Checked = bool .Parse ( dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["SUDUNG"].ToString());
            chkTinhgio.Checked = bool.Parse(dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["SUAGIA"].ToString());
            txtGIAMUA.EditValue = int.Parse(dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["GIANHAP"].ToString());
            txtBlock.EditValue = dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["MAVACH"].ToString()==""? 0: int.Parse(dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["MAVACH"].ToString());
            txtGIABAN1.EditValue = int.Parse(dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["GIABAN1"].ToString());
            txtGIABAN2.EditValue = int.Parse(dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["GIABAN2"].ToString());
            cboMayin.EditValue = int.Parse(dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["MAYIN"].ToString());
            try
            {
                byte[] tam = new byte[((byte[])dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["HINHANH"]).LongLength];
                tam = (byte[])dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["HINHANH"];
                MemoryStream ms = new MemoryStream(tam);
                Bitmap bm = new Bitmap(ms);
                picTROCHOI.Image = bm;
                picTROCHOI.BackgroundImage = null; 
            }
            catch
            {
                picTROCHOI.Image = null;
                picTROCHOI.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26; 
            }
        }

        DataTable dtLOAITROCHOI = new DataTable();
        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
        
            string sSQL = "";
            sSQL += "Select MA_LOAITROCHOI As MA,TEN_LOAITROCHOI As TEN,GHICHU,SUDUNG,THUCDON,STT,HINHANH" + "\n";
            sSQL += "From LOAITROCHOI" + "\n";
            sSQL += "Order by SUDUNG DESC,STT,TEN_LOAITROCHOI" + "\n";
            dtLOAITROCHOI  = clsMain.ReturnDataTable(sSQL);
            fpnlLOAITROCHOI.Controls.Clear();
            fpnlTROCHOI.Controls.Clear();
            foreach (DataRow dr in dtLOAITROCHOI.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 125;
                btn.Height = 80;
                btn.Font = fpnlTROCHOI.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                try
                {
                    btn.ImageLocation = ImageLocation.TopCenter;
                    byte[] tam = new byte[((byte[])dr["HINHANH"]).LongLength];
                    tam = (byte[])dr["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    btn.Image = bm;
                }
                catch
                {
                }
                btn.Name = "N" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(LOAITROCHOI_Click);
                fpnlLOAITROCHOI.Controls.Add(btn);
            }
            if (fpnlLOAITROCHOI.VerticalScroll.Visible)
            {
                panelControl3.Width = 152;
            }
            else
            {
                panelControl3.Width = 135;
            }
        }

        DataTable dtTROCHOI = new DataTable();
        private void LoaddataGridView1(string maLOAITROCHOI)
        {
            SetRongControl1();
            KhoaMoControl1(true);

            string sSQL = "";
            sSQL += "Select  MA_TROCHOI As MA,TEN_TROCHOI As TEN,HINHANH,THUE,GIANHAP,GIABAN1,GIABAN2,GHICHU,MAVACH,SUDUNG,SUAGIA,TONKHO,TONTOITHIEU,MA_THANHPHAN,SOLUONG,MA_LOAITROCHOI,MA_DONVITINH,STT,MAYIN" + "\n";
            sSQL += "From TROCHOI" + "\n";
            sSQL += "Where MA_LOAITROCHOI=" + clsMain.SQLString(maLOAITROCHOI) + "\n";
            sSQL += "Order by SUDUNG DESC, STT,TEN_TROCHOI" + "\n";
            dtTROCHOI = clsMain.ReturnDataTable(sSQL);
            fpnlTROCHOI.Controls.Clear();
            foreach (DataRow dr in dtTROCHOI.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 110;
                btn.Height = 80;
                btn.Font = fpnlTROCHOI.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                try
                {
                    btn.ImageLocation = ImageLocation.TopCenter;
                    byte[] tam = new byte[((byte[])dr["HINHANH"]).LongLength];
                    tam = (byte[])dr["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    btn.Image = bm;
                }
                catch
                {
                }
                btn.Name = "H" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString() + "\n" + String.Format("{0:#,###0}", dr["GIABAN1"]);
                btn.Click += new EventHandler(TROCHOI_Click);
                fpnlTROCHOI.Controls.Add(btn);
            }
        }

        private void LoadCombo()
        {
            string sSQL = "";
            DataTable dt = new DataTable();
            sSQL += "Select MA_LOAITROCHOI As MA,TEN_LOAITROCHOI As TEN" + "\n";
            sSQL += "From LOAITROCHOI" + "\n";
            sSQL += "Where SUDUNG=1 ";
            sSQL += "Order by  TEN_LOAITROCHOI ";
            dt = clsMain.ReturnDataTable(sSQL);
            cboLOAITROCHOI.Properties.DataSource = dt;
            cboLOAITROCHOI.EditValue = cboLOAITROCHOI.Properties.GetDataSourceValue(cboLOAITROCHOI.Properties.ValueMember, 0);

            sSQL = "";
            sSQL += "Select MA_DONVITINH As MA,TEN_DONVITINH As TEN" + "\n";
            sSQL += "From DONVITINH" + "\n";
            sSQL += "Where SUDUNG=1 ";
            sSQL += "Order by  TEN_DONVITINH ";
            dt = clsMain.ReturnDataTable(sSQL);
            cboDonvitinh.Properties.DataSource = dt.Copy ();
            cboDonvitinh.EditValue = cboDonvitinh.Properties.GetDataSourceValue(cboDonvitinh.Properties.ValueMember, 0);

            dt.Clear();
            dt.Rows.Add("0", "Máy in vé");
            dt.Rows.Add("1", "Máy in bill");
            dt.Rows.Add("2", "Máy in mã vạch");
            cboMayin.Properties.DataSource = dt;
            cboMayin.EditValue = 0;
        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTENLOAITROCHOI.Properties.ReadOnly = true;
                txtGHICHULOAITROCHOI.Properties.ReadOnly = true;
                txtSTTLOAITROCHOI.Properties.ReadOnly = true;
                chkSUDUNGLOAITROCHOI.Properties.ReadOnly = true;
                chkTHUCDONLOAITROCHOI.Properties.ReadOnly = true;
                picLOAITROCHOI.Enabled = false;
            }
            else//mở
            {
                txtTENLOAITROCHOI.Properties.ReadOnly = false;
                txtGHICHULOAITROCHOI.Properties.ReadOnly = false;
                txtSTTLOAITROCHOI.Properties.ReadOnly = false;
                chkSUDUNGLOAITROCHOI.Properties.ReadOnly = false;
                chkTHUCDONLOAITROCHOI.Properties.ReadOnly = false;
                picLOAITROCHOI.Enabled = true ;
            }
        }

        private void KhoaMoControl1(bool tf)
        {
            if (tf == true)//khóa
            {
                cboLOAITROCHOI.Properties.ReadOnly = true;
                cboDonvitinh.Properties.ReadOnly = true;
                txtTENTROCHOI.Properties.ReadOnly = true;
                txtGHICHUTROCHOI.Properties.ReadOnly = true;
                txtSTTTROCHOI.Properties.ReadOnly = true;
                chkSUDUNGTROCHOI.Properties.ReadOnly = true;
                chkTinhgio.Properties.ReadOnly = true;
                picTROCHOI.Enabled = false;
                cboThue.Properties.ReadOnly = true;
                txtGIAMUA.Properties.ReadOnly = true;
                txtBlock.Properties.ReadOnly = true;
                txtGIABAN1.Properties.ReadOnly = true;
                txtGIABAN2.Properties.ReadOnly = true;
            }
            else//mở
            {
                cboLOAITROCHOI.Properties.ReadOnly = false;
                cboDonvitinh.Properties.ReadOnly = false;
                txtTENTROCHOI.Properties.ReadOnly = false;
                txtGHICHUTROCHOI.Properties.ReadOnly = false;
                txtSTTTROCHOI.Properties.ReadOnly = false;
                chkSUDUNGTROCHOI.Properties.ReadOnly = false;
                chkTinhgio.Properties.ReadOnly = false;
                picTROCHOI.Enabled = true;
                cboThue.Properties.ReadOnly = false;
                txtGIAMUA.Properties.ReadOnly = false;
                txtBlock.Properties.ReadOnly = false;
                txtGIABAN1.Properties.ReadOnly = false;
                txtGIABAN2.Properties.ReadOnly = false;
            }
        }

        private void SetRongControl()
        {
            smaLOAITROCHOI = "";
            txtTENLOAITROCHOI.Text = "";
            txtGHICHULOAITROCHOI.Text = "";
            txtSTTLOAITROCHOI.Text = "1";
            chkSUDUNGLOAITROCHOI.Checked = true;
            chkTHUCDONLOAITROCHOI.Checked = true;
            picLOAITROCHOI.Image = null;
            picLOAITROCHOI.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26; 
        }

        private void SetRongControl1()
        {
            smaTROCHOI = "";
            cboLOAITROCHOI.EditValue = (smaLOAITROCHOI != "") ? int.Parse(smaLOAITROCHOI) : cboLOAITROCHOI.Properties.GetDataSourceValue(cboLOAITROCHOI.Properties.ValueMember, 0);
            cboDonvitinh.EditValue = cboDonvitinh.Properties.GetDataSourceValue(cboDonvitinh.Properties.ValueMember, 0);
            cboThue.EditValue = 0;
            txtTENTROCHOI.Text = "";
            txtGHICHUTROCHOI.Text = "";
            txtSTTTROCHOI.EditValue = 1;
            chkSUDUNGTROCHOI.Checked = true;
            chkTinhgio.Checked = false;
            picTROCHOI.Image = null;
            picTROCHOI.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26; 
            txtGIAMUA.EditValue = 0;
            txtBlock.EditValue = 0;
            txtGIABAN1.EditValue = 0;
            txtGIABAN2.EditValue = 0;
        }

        private void LoadPermission()
        {
            btnThemLOAITROCHOI.Enabled = clsUserManagement.AllowAdd("60");
            btnSuaLOAITROCHOI.Enabled = clsUserManagement.AllowEdit("60");
            btnXoaLOAITROCHOI.Enabled = clsUserManagement.AllowDelete("60");
            btnLuuLOAITROCHOI.Enabled = btnThemLOAITROCHOI.Enabled || btnSuaLOAITROCHOI.Enabled;

            btnThemTROCHOI.Enabled = clsUserManagement.AllowAdd("60");
            btnSuaTROCHOI.Enabled = clsUserManagement.AllowEdit("60");
            btnXoaTROCHOI.Enabled = clsUserManagement.AllowDelete("60");
            btnLuuTROCHOI.Enabled = btnThemTROCHOI.Enabled || btnSuaTROCHOI.Enabled;
        }

        private Boolean CheckInput()
        {
            if (txtTENLOAITROCHOI.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblTENLOAITROCHOI.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTENLOAITROCHOI.Focus();
                return false;
            }
            return true;
        }

        private Boolean CheckInput1()
        {
            if (txtTENTROCHOI.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblTENTROCHOI.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTENTROCHOI.Focus();
                return false;
            }
            if (chkTinhgio.Checked && txtBlock .Text =="")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập thời gian của 1 block giờ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtBlock.Focus();
                return false;
            }
            return true;
        }
    }
}