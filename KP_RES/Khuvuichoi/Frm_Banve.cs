﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Data.SqlClient;
using KP_RES.Class;
using KP_Report;
using KP_RES;
using System.Threading;

namespace KP_RES 
{
    public partial class Frm_Banve : DevExpress.XtraEditors.XtraForm
    {
        int iMode = 0;//0-Dong,1-Ban ve,2-In lai ve,3-Huy ve,4-Ket ca,5-Soat ve,6-Tinh tien ve theo thoi gian
        string smaLOAITROCHOI = "";
        string smaTROCHOI = "";
        DataTable dtHanghoa = new DataTable();
        int iGiaban = 1;

        public Frm_Banve()
        {
            InitializeComponent();
            timer1.Start();
            lb_KhoQuay.Text = cls_ConfigCashier.nameCashier + "    " + cls_ConfigCashier.nameWarehouse;
            lbCaBan.Text = "Ngày: " + DateTime.Now.ToString("dd-MM-yyyy");
            lb_NhanVien.Text ="Nhân viên: " +clsGlobal.gsNameOfUserLogin;
            lbtimeby.Text = cls_ConfigCashier.sTenCB +": " + cls_ConfigCashier.sGioBD +" --> " + cls_ConfigCashier.sGioKT ;
            LoaddataGridView();
            LoaddataGridView1(smaLOAITROCHOI);
            LoadTiente();
            LoadPermission();
        }

        private void LoadPermission()
        {
            btnHuyve.Enabled = clsUserManagement.AllowDelete("61");
            btnInlaive.Enabled = clsUserManagement.AllowDelete("61");
        }

        private void Frm_Banve_Load(object sender, EventArgs e)
        {
            try
            {
                LOAITROCHOI_Click(fpnlLOAITROCHOI.Controls[0], null);
                dtHanghoa.Columns.Add(new DataColumn("MA_HANGHOA", typeof(Int32)));
                dtHanghoa.Columns.Add(new DataColumn("TEN_HANGHOA", typeof(string)));
                dtHanghoa.Columns.Add(new DataColumn("SOLUONG", typeof(Int32)));
                dtHanghoa.Columns.Add(new DataColumn("GIABAN", typeof(Int32)));
                dtHanghoa.Columns.Add(new DataColumn("Thanh_Tien", typeof(Int32)));
            }
            catch
            {
            }
        }

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                txt.SelectAll();
            }
            catch
            {
            }
        }

        private void btnLenLOAITROCHOI_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlLOAITROCHOI.AutoScrollPosition = new Point(0, fpnlLOAITROCHOI.VerticalScroll.Value - fpnlLOAITROCHOI.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnXuongLOAITROCHOI_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlLOAITROCHOI.AutoScrollPosition = new Point(0, fpnlLOAITROCHOI.VerticalScroll.Value + fpnlLOAITROCHOI.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnLenTROCHOI_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlTROCHOI.AutoScrollPosition = new Point(0, fpnlTROCHOI.VerticalScroll.Value - fpnlTROCHOI.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnXuongTROCHOI_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlTROCHOI.AutoScrollPosition = new Point(0, fpnlTROCHOI.VerticalScroll.Value + fpnlTROCHOI.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void LOAITROCHOI_Click(object sender, EventArgs e)
        {
            pnBtnDeleteGv.Visible = false;
            SimpleButton btn = (SimpleButton)sender;
            foreach (SimpleButton btn1 in fpnlLOAITROCHOI.Controls)
            {
                if (btn1.Name == btn.Name)
                {
                    cls_KP_RES.SetBackgroudOnClick(btn1);
                }
                else
                {
                    cls_KP_RES.SetDefautlBackgroud(btn1);
                }
            }

            LoaddataGridView1(btn.Name.Substring(1, btn.Name.Length - 1));
            smaLOAITROCHOI = btn.Name.Substring(1, btn.Name.Length - 1);
        }

        private void TROCHOI_Click(object sender, EventArgs e)
        {
            pnBtnDeleteGv.Visible = false;
            SimpleButton btn = (SimpleButton)sender;
            foreach (SimpleButton btn1 in fpnlTROCHOI.Controls)
            {
                if (btn1.Name == btn.Name)
                {
                    cls_KP_RES.SetBackgroudOnClick(btn1);
                }
                else
                {
                    cls_KP_RES.SetDefautlBackgroud(btn1);
                }
            }

            smaTROCHOI = btn.Name.Substring(1, btn.Name.Length - 1);

            try
            {
                if (dtHanghoa.Select("MA_HANGHOA=" + clsMain.SQLString(smaTROCHOI)).Length <= 0)
                {
                    if ((bool)dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["SUAGIA"])
                    {

                        dtHanghoa.Rows.Add(dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["MA"], dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["TEN"], 1,0, 0);
                    }
                    else
                    {
                        if (iGiaban == 1)
                        {
                            dtHanghoa.Rows.Add(dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["MA"], dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["TEN"], 1, dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["GIABAN1"], dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["GIABAN1"]);
                        }
                        else
                        {
                            dtHanghoa.Rows.Add(dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["MA"], dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["TEN"], 1, dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["GIABAN2"], dtTROCHOI.Select("MA=" + clsMain.SQLString(smaTROCHOI))[0]["GIABAN2"]);
                        }
                    }
                }
                else
                {
                    dtHanghoa.Rows[dtHanghoa.Rows.IndexOf(dtHanghoa.Select("MA_HANGHOA=" + clsMain.SQLString(smaTROCHOI))[0])]["SOLUONG"] = (int)dtHanghoa.Rows[dtHanghoa.Rows.IndexOf(dtHanghoa.Select("MA_HANGHOA=" + clsMain.SQLString(smaTROCHOI))[0])]["SOLUONG"] + 1;
                    dtHanghoa.Rows[dtHanghoa.Rows.IndexOf(dtHanghoa.Select("MA_HANGHOA=" + clsMain.SQLString(smaTROCHOI))[0])]["Thanh_Tien"] = (int)dtHanghoa.Rows[dtHanghoa.Rows.IndexOf(dtHanghoa.Select("MA_HANGHOA=" + clsMain.SQLString(smaTROCHOI))[0])]["SOLUONG"] * (int)dtHanghoa.Rows[dtHanghoa.Rows.IndexOf(dtHanghoa.Select("MA_HANGHOA=" + clsMain.SQLString(smaTROCHOI))[0])]["GIABAN"];
                }
                gr_SelectProduct.DataSource = dtHanghoa.Copy();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString ());
            }
        }

        private void btn_XoaTatCa_Click(object sender, EventArgs e)
        {
            try
            {
                dtHanghoa.Clear();
                gr_SelectProduct.DataSource = dtHanghoa.Copy();
                if (gv_SelectProduct.RowCount > 10)
                {
                    pnUpDowGv.Visible = true;
                }
                else
                {
                    pnUpDowGv.Visible = false;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString());
            }
        }

        private void btn_xoa_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtHanghoa.Rows.Count <= 0)
                    return;
                dtHanghoa.Rows.Remove(dtHanghoa.Select("MA_HANGHOA=" + clsMain.SQLString(gv_SelectProduct.GetFocusedRowCellValue("MA_HANGHOA").ToString()))[0]);
                gr_SelectProduct.DataSource = dtHanghoa.Copy();

                if (gv_SelectProduct.RowCount > 10)
                {
                    pnUpDowGv.Visible = true;
                }
                else
                {
                    pnUpDowGv.Visible = false;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString());
            }
        }

        private void btn_cong_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtHanghoa.Rows.Count <= 0)
                    return;
                dtHanghoa.Rows[dtHanghoa.Rows.IndexOf(dtHanghoa.Select("MA_HANGHOA=" + clsMain.SQLString(gv_SelectProduct.GetFocusedRowCellValue("MA_HANGHOA").ToString()))[0])]["SOLUONG"] = (int)dtHanghoa.Rows[dtHanghoa.Rows.IndexOf(dtHanghoa.Select("MA_HANGHOA=" + clsMain.SQLString(gv_SelectProduct.GetFocusedRowCellValue("MA_HANGHOA").ToString()))[0])]["SOLUONG"] + 1;
                dtHanghoa.Rows[dtHanghoa.Rows.IndexOf(dtHanghoa.Select("MA_HANGHOA=" + clsMain.SQLString(gv_SelectProduct.GetFocusedRowCellValue("MA_HANGHOA").ToString()))[0])]["Thanh_Tien"] = (int)dtHanghoa.Rows[dtHanghoa.Rows.IndexOf(dtHanghoa.Select("MA_HANGHOA=" + clsMain.SQLString(gv_SelectProduct.GetFocusedRowCellValue("MA_HANGHOA").ToString()))[0])]["SOLUONG"] * (int)dtHanghoa.Rows[dtHanghoa.Rows.IndexOf(dtHanghoa.Select("MA_HANGHOA=" + clsMain.SQLString(gv_SelectProduct.GetFocusedRowCellValue("MA_HANGHOA").ToString()))[0])]["GIABAN"];
                int FocusedRowHandle = gv_SelectProduct.FocusedRowHandle ;
                gr_SelectProduct.DataSource = dtHanghoa.Copy();
                gv_SelectProduct.FocusedRowHandle = FocusedRowHandle;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString());
            }
        }

        private void btn_tru_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtHanghoa.Rows.Count <= 0 || (int)dtHanghoa.Rows[dtHanghoa.Rows.IndexOf(dtHanghoa.Select("MA_HANGHOA=" + clsMain.SQLString(gv_SelectProduct.GetFocusedRowCellValue("MA_HANGHOA").ToString()))[0])]["SOLUONG"]==1)
                    return;
                dtHanghoa.Rows[dtHanghoa.Rows.IndexOf(dtHanghoa.Select("MA_HANGHOA=" + clsMain.SQLString(gv_SelectProduct.GetFocusedRowCellValue("MA_HANGHOA").ToString()))[0])]["SOLUONG"] = (int)dtHanghoa.Rows[dtHanghoa.Rows.IndexOf(dtHanghoa.Select("MA_HANGHOA=" + clsMain.SQLString(gv_SelectProduct.GetFocusedRowCellValue("MA_HANGHOA").ToString()))[0])]["SOLUONG"] - 1;
                dtHanghoa.Rows[dtHanghoa.Rows.IndexOf(dtHanghoa.Select("MA_HANGHOA=" + clsMain.SQLString(gv_SelectProduct.GetFocusedRowCellValue("MA_HANGHOA").ToString()))[0])]["Thanh_Tien"] = (int)dtHanghoa.Rows[dtHanghoa.Rows.IndexOf(dtHanghoa.Select("MA_HANGHOA=" + clsMain.SQLString(gv_SelectProduct.GetFocusedRowCellValue("MA_HANGHOA").ToString()))[0])]["SOLUONG"] * (int)dtHanghoa.Rows[dtHanghoa.Rows.IndexOf(dtHanghoa.Select("MA_HANGHOA=" + clsMain.SQLString(gv_SelectProduct.GetFocusedRowCellValue("MA_HANGHOA").ToString()))[0])]["GIABAN"];
                int FocusedRowHandle = gv_SelectProduct.FocusedRowHandle;
                gr_SelectProduct.DataSource = dtHanghoa.Copy();
                gv_SelectProduct.FocusedRowHandle = FocusedRowHandle;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString());
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string hour, minute, second;
            second = DateTime.Now.Second.ToString();
            hour = DateTime.Now.Hour.ToString();
            minute = DateTime.Now.Minute.ToString();
            if (second.Length < 2)
                lbgiay.Text = "0" + second;
            else
                lbgiay.Text = second;
            if (minute.Length < 2)
                lbGio.Text = hour + ":0" + minute;
            else
                lbGio.Text = hour + ":" + minute;
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            if (iMode == 0 )//Dong
            {
                this.Close();
            }
        }

        private void btnbanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnMoket_Click(object sender, EventArgs e)
        {
            bool check = false;
            Frm_ConfigFuntion frm = new Frm_ConfigFuntion();
            frm.ShowDialog();
            check = frm.kt;
            frm.Dispose();
            if (!check)
            {
                return;
            }
            KP_Terminal.clsKP_Terminal.OpenCashDrawer(false);
        }

        private void btn_UpFirst_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = 0;
                btn_UpFirst.Enabled = false;
                btn_up.Enabled = false;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
            }
            catch
            {
            }
        }

        private void btn_up_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.FocusedRowHandle - 1;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
                if (gv_SelectProduct.FocusedRowHandle == 0)
                {
                    btn_up.Enabled = false;
                    btn_UpFirst.Enabled = false;

                }
            }
            catch
            {
            }
        }

        private void btn_Dow_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.FocusedRowHandle + 1;
                btn_up.Enabled = true;
                btn_UpFirst.Enabled = true;


                if (gv_SelectProduct.FocusedRowHandle == gv_SelectProduct.RowCount - 1)
                {
                    btn_Dow.Enabled = false;
                    btn_DowLast.Enabled = false;
                }
            }
            catch
            {

            }
        }

        private void btn_DowLast_Click(object sender, EventArgs e)
        {
            try
            {

                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.RowCount - 1;
                btn_UpFirst.Enabled = true;
                btn_up.Enabled = true;
                btn_Dow.Enabled = false;
                btn_DowLast.Enabled = false;
            }
            catch
            {
            }
        }
        
        private void gv_SelectProduct_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == SOTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
            if (gv_SelectProduct.RowCount > 0)
            {
                lb_RowsCount.Text = gv_SelectProduct.RowCount.ToString();
                lb_STT.Text = (gv_SelectProduct.FocusedRowHandle + 1).ToString();
            }
            if (1 < gv_SelectProduct.FocusedRowHandle + 1 && gv_SelectProduct.FocusedRowHandle + 1 <= gv_SelectProduct.RowCount - 1)
            {
                btn_up.Enabled = true;
                btn_UpFirst.Enabled = true;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
            }

            if (gv_SelectProduct.RowCount > 10)
            {
                pnUpDowGv.Visible = true;
            }
            else
            {
                pnUpDowGv.Visible = false ;
            }

            txtTienve.EditValue = int.Parse(Thanh_Tien.SummaryText.Replace(",", ""));
        }

        private void gv_SelectProduct_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            pnBtnDeleteGv.Visible = true;
        }

        private void gv_SelectProduct_LostFocus(object sender, EventArgs e)
        {
            if (btn_xoa.Focused || btn_cong.Focused || btn_tru.Focused || btn_XoaTatCa.Focused || btn_Dow.Focused || btn_up.Focused || btn_UpFirst.Focused || btn_DowLast.Focused)
            {
                return;
            }
            pnBtnDeleteGv.Visible = false;
        }

        DataTable dtLOAITROCHOI = new DataTable();
        private void LoaddataGridView()
        {
            string sSQL = "";
            sSQL += "Select MA_LOAITROCHOI As MA,TEN_LOAITROCHOI As TEN,GHICHU,SUDUNG,THUCDON,STT,HINHANH" + "\n";
            sSQL += "From LOAITROCHOI" + "\n";
            sSQL += "Where SUDUNG=1" + "\n";
            sSQL += "Order by STT,TEN_LOAITROCHOI" + "\n";
            dtLOAITROCHOI  = clsMain.ReturnDataTable(sSQL);
            fpnlLOAITROCHOI.Controls.Clear();
            fpnlTROCHOI.Controls.Clear();
            foreach (DataRow dr in dtLOAITROCHOI.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 125;
                btn.Height = 80;
                btn.Font = fpnlTROCHOI.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                try
                {
                    btn.ImageLocation = ImageLocation.TopCenter;
                    byte[] tam = new byte[((byte[])dr["HINHANH"]).LongLength];
                    tam = (byte[])dr["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    btn.Image = bm;
                }
                catch
                {
                }
                btn.Name = "N" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(LOAITROCHOI_Click);
                fpnlLOAITROCHOI.Controls.Add(btn);
            }
            if (fpnlLOAITROCHOI.Controls .Count >7)
            {
                pnlLoaitrochoi.Width = 152;
            }
            else
            {
                pnlLoaitrochoi.Width = 135;
            }
        }

        DataTable dtTROCHOI = new DataTable();
        private void LoaddataGridView1(string maLOAITROCHOI)
        {
            string sSQL = "";
            sSQL += "Select  MA_TROCHOI As MA,TEN_TROCHOI As TEN,HINHANH,THUE,GIANHAP,GIABAN1,GIABAN2,GHICHU,MAVACH,SUDUNG,SUAGIA,TONKHO,TONTOITHIEU,MA_THANHPHAN,SOLUONG,MA_LOAITROCHOI,MA_DONVITINH,STT" + "\n";
            sSQL += "From TROCHOI" + "\n";
            sSQL += "Where MA_LOAITROCHOI=" + clsMain.SQLString(maLOAITROCHOI) + "\n";
            sSQL += "And SUDUNG=1" + "\n";
            sSQL += "Order by STT,TEN_TROCHOI" + "\n";
            dtTROCHOI = clsMain.ReturnDataTable(sSQL);
            fpnlTROCHOI.Controls.Clear();
            foreach (DataRow dr in dtTROCHOI.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 110;
                btn.Height = 80;
                btn.Font = fpnlTROCHOI.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.Margin = new Padding(3,3,10,3);
                try
                {
                    btn.ImageLocation = ImageLocation.TopCenter;
                    byte[] tam = new byte[((byte[])dr["HINHANH"]).LongLength];
                    tam = (byte[])dr["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    btn.Image = bm;
                }
                catch
                {
                }
                btn.Name = "H" + dr["MA"].ToString();
                if (iGiaban == 1)
                {
                    btn.Text = dr["TEN"].ToString() + "\n" + String.Format("{0:#,###0}", dr["GIABAN1"]);
                }
                else
                {
                    btn.Text = dr["TEN"].ToString() + "\n" + String.Format("{0:#,###0}", dr["GIABAN2"]);
                }
                btn.Click += new EventHandler(TROCHOI_Click);
                fpnlTROCHOI.Controls.Add(btn);
            }
        }

        private void LoadTiente()
        {
            string sSQL = "";
            sSQL += "Select MA_TIENTE,TEN_TIENTE,TYGIA" + "\n";
            sSQL += "From TIENTE" + "\n";
            sSQL += "Where SUDUNG=1" + "\n";
            sSQL += "Order by STT,TEN_TIENTE" + "\n";
            DataTable dt  = clsMain.ReturnDataTable(sSQL);
            grdTiente.DataSource = dt;
        }

        private void grvtiente_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == STTTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
        }

        private void grvtiente_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            pnlQuydoi.Visible = true;
            lblTienquydoi.Text = "   Quy đổi \n" + grvtiente.GetFocusedRowCellValue("MA_TIENTE").ToString() + " -> VNĐ";
            txtTienquydoi.EditValue = (int)txtTienKhachTra.EditValue * int.Parse ( grvtiente.GetFocusedRowCellValue("TYGIA").ToString ());
        }

        private void txtTienquydoi_EditValueChanged(object sender, EventArgs e)
        {
            txtTienThoiLai.EditValue = (int)txtTienquydoi.EditValue- (int)txtTienve.EditValue;
        }

        private void btnLentiente_Click(object sender, EventArgs e)
        {
            try
            {
                grvtiente.Focus();
                grvtiente.FocusedRowHandle = grvtiente.FocusedRowHandle - 1;
                btnLentiente.Enabled = true;
                btnXuongtiente.Enabled = true;
                if (grvtiente.FocusedRowHandle == 0)
                {
                    btnLentiente.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnXuongtiente_Click(object sender, EventArgs e)
        {
            try
            {
                grvtiente.Focus();
                grvtiente.FocusedRowHandle = grvtiente.FocusedRowHandle + 1;
                btnXuongtiente.Enabled = true;
                btnLentiente.Enabled = true;
                if (grvtiente.FocusedRowHandle == grvtiente.RowCount - 1)
                {
                    btnXuongtiente.Enabled = false;
                }
            }
            catch
            {

            }
        }

        private Boolean CheckInput()
        {
         
            return true;
        }

        private void btnSoatve_Click(object sender, EventArgs e)
        {
            if (iMode == 0 || iMode == 5)//Soat ve
            {
                if (iMode == 0)
                {
                    cls_KP_RES.SetBackgroudOnClick(btnSoatve);
                    iMode = 5;
                }
                else
                {
                    txtMave.Text = "";
                    txtTrangthai.Text = "";
                    txtThongtinve.Text = "";
                    cls_KP_RES.SetDefautlBackgroud(btnSoatve);
                    iMode = 0;
                }
                pnlLoaddanhmuc.Visible = !pnlLoaddanhmuc.Visible;
                pnlSoatve.Visible = !pnlSoatve.Visible;
            }
        }

        private void txtMave_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string sSQL = "";
                sSQL = "Select A.MA_VEBAN,A.MA_TROCHOI,A.MA_NHANVIEN,A.MA_QUAY,A.GIABAN,A.NGAYBAN,B.TEN_TROCHOI,B.MAYIN,C.TENNHANVIEN" + "\n";
                sSQL += "From VETROCHOI A,TROCHOI B,DM_NHANVIEN C" + "\n";
                sSQL += "Where A.MA_TROCHOI = B.MA_TROCHOI" + "\n";
                sSQL += "And A.MA_NHANVIEN = C.MANHANVIEN" + "\n";
                sSQL += "And A.MA_VEBAN = " + clsMain.SQLString(txtMave .Text ) + "\n";
                sSQL += "And A.HUY = 0" + "\n";
                
                DataTable dt = clsMain.ReturnDataTable(sSQL);

                if (dt.Rows.Count > 0)
                {
                    txtTrangthai.Text = "Vé hợp lệ";
                    txtTrangthai.BackColor = Color.Blue;
                    txtTrangthai.ForeColor = Color.White;
                    txtThongtinve.Text = "Mã vé : " + dt.Rows[0]["MA_VEBAN"].ToString() + "\n\r\n\r";
                    txtThongtinve.Text += "\nTrò chơi : " + dt.Rows[0]["TEN_TROCHOI"].ToString() + "\n\r\n\r";
                    txtThongtinve.Text += "\nGiá vé : " + dt.Rows[0]["GIABAN"].ToString() + "\n\r\n\r";
                    txtThongtinve.Text += "\nNgày : " + ((DateTime)dt.Rows[0]["NGAYBAN"]).Day + "/" + ((DateTime)dt.Rows[0]["NGAYBAN"]).Month + "/" + ((DateTime)dt.Rows[0]["NGAYBAN"]).Year + " " + ((DateTime)dt.Rows[0]["NGAYBAN"]).Hour + ":" + ((DateTime)dt.Rows[0]["NGAYBAN"]).Minute + "\n\r\n\r";
                    txtThongtinve.Text += "\nNhân viên : " + dt.Rows[0]["TENNHANVIEN"].ToString() + "\n\r";
                }
                else
                {
                    txtTrangthai.Text = "Vé không hợp lệ";
                    txtTrangthai.BackColor = Color.Red;
                    txtTrangthai.ForeColor = Color.Black;
                    txtThongtinve.Text = "";
                }
                txtMave.Text = "";
                txtMave.Focus();
            }
        }

        private void btnKetca_Click(object sender, EventArgs e)
        {
            if (iMode == 0 || iMode == 4)//Ket ca
            {
                if (iMode == 0)
                {
                    bool check = false;
                    Frm_ConfigFuntion frm = new Frm_ConfigFuntion();
                    frm.ShowDialog();
                    check = frm.kt;
                    frm.Dispose();
                    if (!check)
                    {
                        return;
                    }
                    cls_KP_RES.SetBackgroudOnClick(btnKetca);
                    iMode = 4;
                }
                else
                {
                    cls_KP_RES.SetDefautlBackgroud(btnKetca);
                    iMode = 0;
                }
                string sSQL = "";
                sSQL = "Select MA_TROCHOI,(Select TEN_TROCHOI From TROCHOI Where TROCHOI.MA_TROCHOI=VETROCHOI.MA_TROCHOI)As TEN_TROCHOI,MA_NHANVIEN,(Select TENNHANVIEN  From DM_NHANVIEN  Where DM_NHANVIEN.MANHANVIEN =VETROCHOI.MA_NHANVIEN)As TEN_NHANVIEN, COUNT(MA_TROCHOI) AS SL, GIABAN, SUM(GIABAN) AS THANHTIEN" + "\n";
                sSQL += "From VETROCHOI " + "\n";
                sSQL += "Where MA_NHANVIEN ="+ clsMain .SQLString (clsUserManagement .ReturnMaNVOfUserLogin()) + "\n";
                sSQL += "And NGAYBAN Between '" + cls_KP_RES.DateToString_Date(clsMain.GetServerDate()) + " 00:00:00'" + " And '" + cls_KP_RES.DateToString_Date(clsMain.GetServerDate()) + " 23:59:59'" + "\n";
                sSQL += "And HUY = 0" + "\n";
                sSQL += "Group by MA_TROCHOI, GIABAN,MA_NHANVIEN"  + "\n";
                sSQL += "Order by MA_TROCHOI";
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                grdKetca.DataSource = dt;

                pnlLoaddanhmuc.Visible = !pnlLoaddanhmuc.Visible;
                pnlKetca.Visible = !pnlKetca.Visible;
            }
        }
        
        private void grvKetca_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == STTKC)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
            txtKetca.EditValue = int.Parse(THANHTIEN.SummaryText.Replace(",", ""));
        }

        private void btnInketca_Click(object sender, EventArgs e)
        {
            cls_PrinterEndSales printSales = new cls_PrinterEndSales();
            printSales.maCaBan = "";
            printSales.tenCaBan = cls_ConfigCashier.sTenCB;
            printSales.maNV = clsGlobal.gsMaNVOfUserLogin;
            printSales.tenNV = clsGlobal.gsNameOfUserLogin;
            printSales.maQuay = cls_ConfigCashier.idCashier;
            printSales.tenQuay = cls_ConfigCashier.nameCashier;
            printSales.giamgia = double.Parse("0");
            printSales.tongTienBan = double.Parse(txtKetca.Text.Trim(new Char[] { 'V', 'N', 'Đ' }));
            printSales.tongTienTra = double.Parse("0".Trim(new Char[] { 'V', 'N', 'Đ' })); ;
            printSales.tongTien = double.Parse(txtKetca.Text.Trim(new Char[] { 'V', 'N', 'Đ' }));
            List<cls_PrinterEndSales> endSales = new List<cls_PrinterEndSales>();
            endSales.Add(printSales);
            convert cvrt = new convert();
            DataTable bill = cvrt.ConvertToDataTable(endSales);
            Frm_Report1 frm = new Frm_Report1();
            frm.ReportName = "rpt_ketca";
            frm.DataSource = bill;
            frm.WindowState = FormWindowState.Minimized;
            frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
            frm.IsPrint = true;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnInlaive_Click(object sender, EventArgs e)
        {
            if (iMode == 0 || iMode == 2)//In lại vé
            {
                if (iMode == 0)
                {
                    bool check = false;
                    Frm_ConfigFuntion frm = new Frm_ConfigFuntion();
                    frm.ShowDialog();
                    check = frm.kt;
                    frm.Dispose();
                    if (!check)
                    {
                        return;
                    }
                    cls_KP_RES.SetBackgroudOnClick(btnInlaive);
                    iMode = 2;
                }
                else
                {
                    cls_KP_RES.SetDefautlBackgroud(btnInlaive);
                    iMode = 0;
                }
                string sSQL = "";
                sSQL = "Select A.MA_VEBAN,A.MA_TROCHOI,A.MA_NHANVIEN,A.MA_QUAY,A.GIABAN,A.NGAYBAN,B.TEN_TROCHOI,B.MAYIN,C.TENNHANVIEN" + "\n";
                sSQL += "From VETROCHOI A,TROCHOI B,DM_NHANVIEN C" + "\n";
                sSQL += "Where A.MA_TROCHOI = B.MA_TROCHOI" + "\n";
                sSQL += "And A.MA_NHANVIEN = C.MANHANVIEN" + "\n";
                sSQL += "And A.HUY = 0" + "\n";
                sSQL += "And A.NGAYBAN Between '" + cls_KP_RES.DateToString_Date(clsMain.GetServerDate()) + " 00:00:00'" + " And '" + cls_KP_RES.DateToString_Date(clsMain.GetServerDate()) + " 23:59:59'" + "\n";
                sSQL += "Order by A.NGAYBAN Desc";
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                grdInlaive.DataSource = dt;

                pnlLoaddanhmuc.Visible = !pnlLoaddanhmuc.Visible;
                pnlInlaive.Visible = !pnlInlaive.Visible;
            }
        }

        private void grvInlaive_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == STT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
        }

        private void grvInlaive_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (e.Column == INVE)
            {
                string sSQL = "";
                sSQL = "Select A.MA_VEBAN,A.MA_TROCHOI,A.MA_NHANVIEN,A.MA_QUAY,A.GIABAN,A.NGAYBAN,B.TEN_TROCHOI,B.MAYIN,C.TENNHANVIEN" + "\n";
                sSQL += "From VETROCHOI A,TROCHOI B,DM_NHANVIEN C" + "\n";
                sSQL += "Where A.MA_TROCHOI = B.MA_TROCHOI" + "\n";
                sSQL += "And A.MA_NHANVIEN = C.MANHANVIEN" + "\n";
                sSQL += "And A.MA_VEBAN = " + clsMain.SQLString(grvInlaive.GetRowCellValue(grvInlaive.FocusedRowHandle, "MA_VEBAN").ToString()) + "\n";

                DataTable dt = clsMain.ReturnDataTable(sSQL);

                Frm_Report1 frm = new Frm_Report1();
                if (dt.Rows[0]["MAYIN"].ToString() == "0")//VÉ
                {
                    frm.ReportName = "rptVe";
                }
                else if (dt.Rows[0]["MAYIN"].ToString() == "1")//BILL
                {
                    frm.ReportName = "rptVeKhuvuichoi";
                }
                else//MA VACH
                {
                    frm.ReportName = "rptVeKhuvuichoi";
                }

                frm.DataSource = dt;
                frm.WindowState = FormWindowState.Minimized;
                if (dt.Rows[0]["MAYIN"].ToString() == "0")
                {
                    frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinve;
                }
                else if (dt.Rows[0]["MAYIN"].ToString() == "1")
                {
                    frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                }
                else
                {
                    frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinmavach;
                }
                frm.IsPrint = true;
                frm.ShowDialog();
                frm.Dispose();
            }
        }

        private void btnHuyve_Click(object sender, EventArgs e)
        {
            if (iMode == 0 || iMode == 3)//huy ve
            {
                if (iMode == 0)
                {
                    bool check = false;
                    Frm_ConfigFuntion frm = new Frm_ConfigFuntion();
                    frm.ShowDialog();
                    check = frm.kt;
                    frm.Dispose();
                    if (!check)
                    {
                        return;
                    }
                    cls_KP_RES.SetBackgroudOnClick(btnHuyve);
                    iMode = 3;
                }
                else
                {
                    txtMA_VEBAN.Text = "";
                    txtLYDO.Text = "";
                    cls_KP_RES.SetDefautlBackgroud(btnHuyve);
                    iMode = 0;
                }
                pnlLoaddanhmuc.Visible = !pnlLoaddanhmuc.Visible;
                pnlHuyve.Visible = !pnlHuyve.Visible;
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            if (txtLYDO.Text == "" || txtMA_VEBAN .Text =="")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Phải nhập đầy đủ thông tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (txtMA_VEBAN.Text == "")
                {
                    txtMA_VEBAN.Focus();
                }
                else
                {
                    txtLYDO.Focus();
                }
                return;
            }
            string sSQL = "";
            sSQL = "Select A.MA_VEBAN,A.MA_TROCHOI,A.MA_NHANVIEN,A.MA_QUAY,A.GIABAN,A.NGAYBAN,B.TEN_TROCHOI,B.MAYIN,C.TENNHANVIEN,A.MA_KHACHHANG" + "\n";
            sSQL += "From VETROCHOI A,TROCHOI B,DM_NHANVIEN C" + "\n";
            sSQL += "Where A.MA_TROCHOI = B.MA_TROCHOI" + "\n";
            sSQL += "And A.MA_NHANVIEN = C.MANHANVIEN" + "\n";
            sSQL += "And A.MA_VEBAN = " + clsMain.SQLString(txtMA_VEBAN.Text) + "\n";
            sSQL += "And A.NGAYBAN >= " + clsMain.SQLString( cls_KP_RES.DateToString_Date(clsGlobal.gdServerDate)) + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Mã vé không chính xác. Kiểm tra lại mã vé", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMA_VEBAN.Text = "";
                txtMA_VEBAN.Focus();
            }
            else
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?" + "\n" + "Mã vé : " + txtMA_VEBAN.Text + "\n" + "Loại vé : " + dt.Rows[0]["TEN_TROCHOI"].ToString (), "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (dt.Rows[0]["MA_KHACHHANG"].ToString() != "" && dt.Rows[0]["MA_KHACHHANG"].ToString() != "0")
                    {
                        if (clsUserManagement.CheckActive("4"))
                        {
                            cls_KHTT.TruDiemKhachHangThanThiet(dt.Rows[0]["MA_KHACHHANG"].ToString(), decimal.Parse(dt.Rows[0]["GIABAN"].ToString()));//KHTT
                        }
                    }
                    sSQL = "";
                    sSQL += "Update VETROCHOI Set " + "\n";
                    sSQL += "HUY=" + clsMain.SQLString("1") + "," + "\n";
                    sSQL += "NGUOIHUY=" + clsMain.SQLString(clsUserManagement.ReturnUserLogin()) + "," + "\n";
                    sSQL += "LYDO=" + clsMain.SQLStringUnicode(txtLYDO.Text) + "\n";
                    sSQL += "Where MA_VEBAN=" + clsMain.SQLString(txtMA_VEBAN.Text ) + "\n";
                    if (clsMain.ExecuteSQL(sSQL))
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Đã hủy vé", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtMA_VEBAN.Text = "";
                        txtMA_VEBAN.Focus();
                    }
                }
            }
        }

        private void btnTinhtienvetheothoigian_Click(object sender, EventArgs e)
        {
            if (iMode == 0 || iMode == 6)//huy ve
            {
                if (iMode == 0)
                {
                    cls_KP_RES.SetBackgroudOnClick(btnTinhtienvetheothoigian);
                    iMode = 6;
                }
                else
                {
                    txtMaveTT.Text = "";
                    txtTrochoi.Text = "";
                    txtThoigianquydoi.Text = "";
                    txtGiovao.Text = "";
                    txtGiora.Text = "";
                    txtThoigian.Text = "";
                    txtGiave.Text = "";
                    txtTongtien.Text = "";
                    cls_KP_RES.SetDefautlBackgroud(btnTinhtienvetheothoigian);
                    iMode = 0;
                }
                pnlLoaddanhmuc.Visible = !pnlLoaddanhmuc.Visible;
                pnlTinhtiengio.Visible = !pnlTinhtiengio.Visible;
            }
        }

        private void txtMaveTT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string sSQL = "";
                if (iGiaban == 1)
                {
                    sSQL = "Select A.MA_VEBAN,A.MA_TROCHOI,A.MA_NHANVIEN,A.MA_QUAY,A.GIABAN,A.NGAYBAN,B.TEN_TROCHOI,B.SUAGIA,B.MAVACH,B.MAYIN,B.GIABAN1,C.TENNHANVIEN" + "\n";
                }
                else
                {
                    sSQL = "Select A.MA_VEBAN,A.MA_TROCHOI,A.MA_NHANVIEN,A.MA_QUAY,A.GIABAN,A.NGAYBAN,B.TEN_TROCHOI,B.SUAGIA,B.MAVACH,B.MAYIN,B.GIABAN2,C.TENNHANVIEN" + "\n";
                }
                sSQL += "From VETROCHOI A,TROCHOI B,DM_NHANVIEN C" + "\n";
                sSQL += "Where A.MA_TROCHOI = B.MA_TROCHOI" + "\n";
                sSQL += "And A.MA_NHANVIEN = C.MANHANVIEN" + "\n";
                sSQL += "And B.SUAGIA = " + clsMain.SQLString("1") + "\n";
                sSQL += "And A.MA_VEBAN = " + clsMain.SQLString(txtMaveTT.Text) + "\n";

                DataTable dt = clsMain.ReturnDataTable(sSQL);

                if (dt.Rows.Count > 0)
                {
                    DateTime dtNow = clsMain.GetServerDate();
                    txtThoigianquydoi.Text = dt.Rows[0]["MAVACH"].ToString()+ " Phút = 1 Block";
                    txtMaveTT.Tag = dt.Rows[0]["MA_VEBAN"].ToString();
                    txtTrochoi.Tag = dt.Rows[0]["MA_TROCHOI"].ToString();
                    txtTrochoi.Text = dt.Rows[0]["TEN_TROCHOI"].ToString();
                    txtGiovao.Text = ((DateTime)dt.Rows[0]["NGAYBAN"]).Day.ToString("00") + "/" + ((DateTime)dt.Rows[0]["NGAYBAN"]).Month.ToString("00") + "/" + ((DateTime)dt.Rows[0]["NGAYBAN"]).Year.ToString("0000") + "   " + ((DateTime)dt.Rows[0]["NGAYBAN"]).Hour.ToString("00") + ":" + ((DateTime)dt.Rows[0]["NGAYBAN"]).Minute.ToString("00");
                    txtGiora.Text = dtNow.Day.ToString("00") + "/" + dtNow.Month.ToString("00") + "/" + dtNow.Year.ToString("0000") + "   " + dtNow.Hour.ToString("00") + ":" + dtNow.Minute.ToString("00");

                    DateTime Giovao = DateTime.ParseExact(txtGiovao.Text.Replace("   ", " "), "dd/MM/yyyy HH:mm", null);
                    DateTime Giora = DateTime.ParseExact(txtGiora.Text.Replace("   ", " "), "dd/MM/yyyy HH:mm", null);

                    int thoigian = ((Giora - Giovao).Days * 1440) + ((Giora - Giovao).Hours * 60) + (Giora - Giovao).Minutes;
                  
                    int block = thoigian / int.Parse(dt.Rows[0]["MAVACH"].ToString().Replace(",", ""));
                    if (thoigian % int.Parse(dt.Rows[0]["MAVACH"].ToString().Replace(",", "")) > 0)
                    {
                        block++;
                    }
                    
                    txtThoigian.EditValue = thoigian.ToString() + " Phút = " + block.ToString() + " Block";
                    if (iGiaban == 1)
                    {
                        txtGiave.EditValue = int.Parse(dt.Rows[0]["GIABAN1"].ToString());
                    }
                    else
                    {
                        txtGiave.EditValue = int.Parse(dt.Rows[0]["GIABAN2"].ToString());
                    }
                    txtTongtien.EditValue = (int)txtGiave.EditValue * block;
                    txtTongtien.Tag = txtTongtien.EditValue;
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Mã vé không chính xác. Kiểm tra lại mã vé", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtMaveTT.Text = "";
                    txtMaveTT.Focus();
                }
            }
        }

        private void btnThanhtoan_Click(object sender, EventArgs e)
        {
            if (txtTongtien.Text != "")
            {
                txtTienve.EditValue = txtTongtien.EditValue;
                dtHanghoa.Clear();
                dtHanghoa.Rows.Add(txtTrochoi.Tag, txtTrochoi.Text, 1, txtTongtien.EditValue, txtTongtien.EditValue);
                gr_SelectProduct.DataSource = dtHanghoa.Copy();
                btnTinhtienvetheothoigian_Click(null, null);
                //btnBanve_Click(null, null);

                if (gv_SelectProduct.RowCount <= 0)
                {
                    return;
                }
                DataView dv = (DataView)gv_SelectProduct.DataSource;
                DataTable dt = dv.ToTable();
                bool bInve = false;
                Payment_Khuvuichoi frm = new Payment_Khuvuichoi(dt, txtTrochoi.Tag.ToString(), txtTongtien.EditValue.ToString(), txtMaveTT.Tag.ToString());
                frm.Width = this.Width;
                frm.Height = this.Height;
                frm.ShowDialog(this);
                bInve = frm.kt;
                frm.Dispose();

                if (bInve)
                {
                    txtTienve.EditValue = 0;
                    txtTienKhachTra.EditValue = 0;
                    runkhtt = true;
                    runttnb = true;
                    btn_XoaTatCa_Click(null, null);
                    btnBanve_Click(null, null);
                }
            }
        }

        private void btnBanve_Click(object sender, EventArgs e)
        {
            //if (iMode == 0 || iMode == 1)//ban ve
            //{
            //    bFirst = true;
            //    if (iMode == 0)
            //    {
            //        if (gv_SelectProduct.RowCount == 0)
            //        {
            //            return;
            //        }
            //        cls_KP_RES.SetBackgroudOnClick(btnBanve);
            //        iMode = 1;
            //    }
            //    else
            //    {
            //        pnlQuydoi.Visible = false;
            //        txtMakhachhang.Text = "";
            //        cls_KP_RES.SetDefautlBackgroud(btnBanve);
            //        iMode = 0;
            //    }
            //    pnlLoaddanhmuc.Visible = !pnlLoaddanhmuc.Visible;
            //    pnlBanve.Visible = !pnlBanve.Visible;
            //}

            if (gv_SelectProduct.RowCount <= 0)
            {
                return;
            }
            DataView dv = (DataView)gv_SelectProduct.DataSource;
            DataTable dt = dv.ToTable();
            bool bInve = false;
            Payment_Khuvuichoi frm = new Payment_Khuvuichoi(dt);
            frm.Width = this.Width;
            frm.Height = this.Height;
            frm.ShowDialog(this);
            bInve = frm.kt;
            frm.Dispose();

            if (bInve)
            {
                txtTienve.EditValue = 0;
                txtTienKhachTra.EditValue = 0;
                runkhtt = true;
                runttnb = true;
                btn_XoaTatCa_Click(null, null);
                btnBanve_Click(null, null);
            }
        }
        
        bool runkhtt = true;
        bool runttnb = true;
        private void btnInve_Click(object sender, EventArgs e)
        {
            if ((int)txtTienThoiLai.EditValue < 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chưa thanh toán hết không thể in vé", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            
            if(txtMakhachhang.Text != "")
            {
                if (!btnKHTT.LookAndFeel.UseDefaultLookAndFeel)
                {
                    if (clsUserManagement.CheckActive("4"))
                    {
                        if (!cls_KHTT.CheckKhachHangThanThiet(txtMakhachhang.Text))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Mã khách hàng không đúng. Kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtMakhachhang.Focus();
                            return;
                        }
                        else
                        {
                            if (runkhtt)
                            {
                                cls_KHTT.CongDiemKhachHangThanThiet(txtMakhachhang.Text, decimal.Parse(txtTienve.EditValue.ToString()));//KHTT
                                runkhtt = false;
                            }
                        }
                    }
                }
                if (!btnTTTNB.LookAndFeel.UseDefaultLookAndFeel)
                {
                    if (clsUserManagement.CheckActive("8"))
                    {
                        if (!cls_KHTT.CheckThanhToanNoiBo(txtMakhachhang.Text))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Mã khách hàng không đúng. Kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtMakhachhang.Focus();
                            return;
                        }
                        else
                        {
                            if (runttnb)
                            {
                                decimal TienTT = cls_KHTT.ThanhToanNoiBo(txtMakhachhang.Text, decimal.Parse(txtTienve.EditValue.ToString()));
                                runttnb = false;
                                if (TienTT != decimal.Parse(txtTienve.EditValue.ToString()))
                                {
                                    txtTienKhachTra.EditValue = int.Parse(TienTT.ToString());
                                    return;
                                }
                            }
                        }
                    }
                }
            }
           
            foreach (DataRow dr in dtHanghoa.Rows)
            {
                for (int i = 1; i <= (int)dr["SOLUONG"]; i++)
                {
                    string sSQL = "";
                    DataTable dt = new DataTable();
                    string MA_VEBAN = "";
                    if (dr["MA_HANGHOA"].ToString() == txtTrochoi.Tag.ToString() && dr["GIABAN"].ToString() == txtTongtien.Tag.ToString())
                    {
                        MA_VEBAN = txtMaveTT.Tag.ToString();
                        sSQL = "";
                        sSQL += "Update VETROCHOI Set " + "\n";
                        sSQL += "GIABAN=" + clsMain.SQLString(dr["GIABAN"].ToString()) + "\n";
                        sSQL += "Where MA_VEBAN=" + clsMain.SQLString(MA_VEBAN) + "\n";
                    }
                    else
                    {
                        MA_VEBAN = cls_ConfigCashier.idCashier + DateTime.Today.Day.ToString("00") + DateTime.Today.Month.ToString("00") + DateTime.Today.Year.ToString("00");
                        dt = clsMain.ReturnDataTable("Select MA_VEBAN From VETROCHOI Where MA_VEBAN Like '" + MA_VEBAN + "%'" + "\n Order by MA_VEBAN DESC");
                        if (dt.Rows.Count <= 0)
                        {
                            MA_VEBAN = MA_VEBAN + "B0001";
                        }
                        else
                        {
                            MA_VEBAN = MA_VEBAN + "B" + (int.Parse(dt.Rows[0][0].ToString().Substring(dt.Rows[0][0].ToString().LastIndexOf("B") + 1, 4)) + 1).ToString("0000");
                        }
                        sSQL = "";
                        sSQL += "Insert into VETROCHOI(MA_VEBAN,MA_TROCHOI,MA_NHANVIEN,MA_QUAY,MA_KHACHHANG,GIABAN)" + "\n";
                        sSQL += "Values ( ";
                        sSQL += clsMain.SQLString(MA_VEBAN) + ",";
                        sSQL += clsMain.SQLString(dr["MA_HANGHOA"].ToString()) + ",";
                        sSQL += clsMain.SQLString(clsUserManagement.ReturnMaNVOfUserLogin()) + ",";
                        sSQL += clsMain.SQLString(cls_ConfigCashier.idCashier) + ",";
                        sSQL += clsMain.SQLString(txtMakhachhang.Text) + ",";
                        sSQL += clsMain.SQLString(dr["GIABAN"].ToString()) + ")";
                    }

                    this.Cursor = Cursors.WaitCursor;
                    Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
                    this.Cursor = Cursors.Default;

                    if (bRunSQL)
                    {
                        sSQL = "";
                        sSQL = "Select A.MA_VEBAN,A.MA_TROCHOI,A.MA_NHANVIEN,A.MA_QUAY,A.GIABAN,A.NGAYBAN,B.TEN_TROCHOI,B.MAYIN,C.TENNHANVIEN" + "\n";
                        sSQL += "From VETROCHOI A,TROCHOI B,DM_NHANVIEN C" + "\n";
                        sSQL += "Where A.MA_TROCHOI = B.MA_TROCHOI" + "\n";
                        sSQL += "And A.MA_NHANVIEN = C.MANHANVIEN" + "\n";
                        sSQL += "And A.MA_VEBAN = " + clsMain.SQLString(MA_VEBAN) + "\n";

                        dt = clsMain.ReturnDataTable(sSQL);

                        Frm_Report1 frm = new Frm_Report1();
                        if (dt.Rows[0]["MAYIN"].ToString() == "0")//VÉ
                        {
                            frm.ReportName = "rptVe";
                        }
                        else if (dt.Rows[0]["MAYIN"].ToString() == "1")//BILL
                        {
                            frm.ReportName = "rptVeKhuvuichoi";
                        }
                        else//MA VACH
                        {
                            frm.ReportName = "rptVeKhuvuichoi";
                        }

                        frm.DataSource = dt;
                        frm.WindowState = FormWindowState.Minimized;
                        if (dt.Rows[0]["MAYIN"].ToString() == "0")
                        {
                            frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinve;
                        }
                        else if (dt.Rows[0]["MAYIN"].ToString() == "1")
                        {
                            frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                        }
                        else
                        {
                            frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinmavach;
                        }
                        frm.IsPrint = true;
                        frm.ShowDialog();
                        frm.Dispose();
                    }
                    else
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            txtTienve.EditValue = 0;
            txtTienKhachTra.EditValue = 0;
            runkhtt = true;
            runttnb = true;
            btn_XoaTatCa_Click(null, null);
            btnBanve_Click(null, null);
            if (!btnKHTT.LookAndFeel.UseDefaultLookAndFeel)
            {
                btnKHTT_Click(null, null);
            }
            if (!btnTTTNB.LookAndFeel.UseDefaultLookAndFeel)
            {
                btnTTTNB_Click(null, null);
            }
        }

        private void txtTienve_EditValueChanged(object sender, EventArgs e)
        {
            if (iMode == 0 || iMode == 6)//Bình thường - Tính tiền vé theo thời gian
            {
                txtTienKhachTra.EditValue = txtTienve.EditValue;
            }
            else if (iMode == 1)//bán vé
            {
                txtTienThoiLai.EditValue = (int)txtTienKhachTra.EditValue - (int)txtTienve.EditValue;
            }
        }

        private void btnNhaplaitienkhachtra_Click(object sender, EventArgs e)
        {
            txtTienve.EditValue = int.Parse(Thanh_Tien.SummaryText.Replace(",", ""));
            txtTienKhachTra.EditValue = 0;
        }

        private void txtTienKhachTra_EditValueChanged(object sender, EventArgs e)
        {
            txtTienThoiLai.EditValue = (int)txtTienKhachTra.EditValue - (int)txtTienve.EditValue;
            if (pnlQuydoi.Visible)
            {
                txtTienquydoi.EditValue = (int)txtTienKhachTra.EditValue * int.Parse(grvtiente.GetFocusedRowCellValue("TYGIA").ToString());
            }
        }

        bool bFirst = true;

        private void btn5t_Click(object sender, EventArgs e)
        {
            if (bFirst)
            {
                txtTienKhachTra.EditValue = 500;
                bFirst = false;
            }
            else
            {
                txtTienKhachTra.EditValue = (int)txtTienKhachTra.EditValue + 500;
            }
        }

        private void btn1k_Click(object sender, EventArgs e)
        {
            if (bFirst)
            {
                txtTienKhachTra.EditValue = 1000;
                bFirst = false;
            }
            else
            {
                txtTienKhachTra.EditValue = (int)txtTienKhachTra.EditValue + 1000;
            }
        }

        private void btn2k_Click(object sender, EventArgs e)
        {
            if (bFirst)
            {
                txtTienKhachTra.EditValue = 2000;
                bFirst = false;
            }
            else
            {
                txtTienKhachTra.EditValue = (int)txtTienKhachTra.EditValue + 2000;
            }
        }

        private void btn5k_Click(object sender, EventArgs e)
        {
            if (bFirst)
            {
                txtTienKhachTra.EditValue = 5000;
                bFirst = false;
            }
            else
            {
                txtTienKhachTra.EditValue = (int)txtTienKhachTra.EditValue + 5000;
            }
        }

        private void btn10k_Click(object sender, EventArgs e)
        {
            if (bFirst)
            {
                txtTienKhachTra.EditValue = 10000;
                bFirst = false;
            }
            else
            {
                txtTienKhachTra.EditValue = (int)txtTienKhachTra.EditValue + 10000;
            }
        }

        private void btn20k_Click(object sender, EventArgs e)
        {
            if (bFirst)
            {
                txtTienKhachTra.EditValue = 20000;
                bFirst = false;
            }
            else
            {
                txtTienKhachTra.EditValue = (int)txtTienKhachTra.EditValue + 20000;
            }
        }

        private void btn50k_Click(object sender, EventArgs e)
        {
            if (bFirst)
            {
                txtTienKhachTra.EditValue = 50000;
                bFirst = false;
            }
            else
            {
                txtTienKhachTra.EditValue = (int)txtTienKhachTra.EditValue + 50000;
            }
        }

        private void btn100k_Click(object sender, EventArgs e)
        {
            if (bFirst)
            {
                txtTienKhachTra.EditValue = 100000;
                bFirst = false;
            }
            else
            {
                txtTienKhachTra.EditValue = (int)txtTienKhachTra.EditValue + 100000;
            }
        }

        private void btn200k_Click(object sender, EventArgs e)
        {
            if (bFirst)
            {
                txtTienKhachTra.EditValue = 200000;
                bFirst = false;
            }
            else
            {
                txtTienKhachTra.EditValue = (int)txtTienKhachTra.EditValue + 200000;
            }
        }

        private void btn500k_Click(object sender, EventArgs e)
        {
            if (bFirst)
            {
                txtTienKhachTra.EditValue = 500000;
                bFirst = false;
            }
            else
            {
                txtTienKhachTra.EditValue = (int)txtTienKhachTra.EditValue + 500000;
            }
        }

        private void btnKHTT_Click(object sender, EventArgs e)
        {
            if (btnKHTT.LookAndFeel.UseDefaultLookAndFeel)
            {
                cls_KP_RES.SetBackgroudOnClick(btnKHTT);
                pnlMakhachhang.Visible = true;
                txtMakhachhang.Focus();
            }
            else
            {
                cls_KP_RES.SetDefautlBackgroud(btnKHTT);
                if (btnTTTNB.LookAndFeel.UseDefaultLookAndFeel)
                {
                    pnlMakhachhang.Visible = false ;
                    txtMakhachhang.Text = "";
                }
            }
        }

        private void btnTTTNB_Click(object sender, EventArgs e)
        {
            if (btnTTTNB.LookAndFeel.UseDefaultLookAndFeel)
            {
                cls_KP_RES.SetBackgroudOnClick(btnTTTNB);
                pnlMakhachhang.Visible = true;
                txtMakhachhang.Focus();
            }
            else
            {
                cls_KP_RES.SetDefautlBackgroud(btnTTTNB);
                if (btnKHTT.LookAndFeel.UseDefaultLookAndFeel)
                {
                    pnlMakhachhang.Visible = false;
                    txtMakhachhang.Text = "";
                }
            }
        }

        private void btnBanggia_Click(object sender, EventArgs e)
        {
            bool check = false;
            Frm_ConfigFuntion frm = new Frm_ConfigFuntion();
            frm.ShowDialog();
            check = frm.kt;
            frm.Dispose();
            if (!check)
            {
                return;
            }
            if (iGiaban == 1)
            {
                iGiaban = 2;
                btnBanggia.Text = "Giá bán :"+"\nGIÁ BÁN 2";
            }
            else
            {
                iGiaban = 1;
                btnBanggia.Text = "Giá bán :"+"\nGIÁ BÁN 1";
            }
            try
            {
                LOAITROCHOI_Click(fpnlLOAITROCHOI.Controls[0], null);
            }
            catch
            {
            }
        }
    }
}