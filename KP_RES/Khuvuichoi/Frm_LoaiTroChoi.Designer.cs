﻿namespace KP_RES 
{
    partial class Frm_LoaiTroChoi 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_LoaiTroChoi));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.fpnlTROCHOI = new System.Windows.Forms.FlowLayoutPanel();
            this.btnXuongHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.fpnlLOAITROCHOI = new System.Windows.Forms.FlowLayoutPanel();
            this.btnXuongNhomhang = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenNhomhang = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pnlTROCHOI = new DevExpress.XtraEditors.PanelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.txtBlock = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lblSOLUONG = new DevExpress.XtraEditors.LabelControl();
            this.chkTinhgio = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.cboMayin = new DevExpress.XtraEditors.LookUpEdit();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.txtGIABAN2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtGIABAN1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtGIAMUA = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.chkSUDUNGTROCHOI = new DevExpress.XtraEditors.CheckEdit();
            this.btnLuuTROCHOI = new DevExpress.XtraEditors.SimpleButton();
            this.btnSuaTROCHOI = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaTROCHOI = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemTROCHOI = new DevExpress.XtraEditors.SimpleButton();
            this.cboLOAITROCHOI = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.picTROCHOI = new System.Windows.Forms.PictureBox();
            this.txtGHICHUTROCHOI = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lblTENTROCHOI = new DevExpress.XtraEditors.LabelControl();
            this.txtTENTROCHOI = new DevExpress.XtraEditors.TextEdit();
            this.txtSTTTROCHOI = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.cboDonvitinh = new DevExpress.XtraEditors.LookUpEdit();
            this.cboThue = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.pnlLOAITROCHOI = new DevExpress.XtraEditors.PanelControl();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.picLOAITROCHOI = new System.Windows.Forms.PictureBox();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lblTENLOAITROCHOI = new DevExpress.XtraEditors.LabelControl();
            this.btnLuuLOAITROCHOI = new DevExpress.XtraEditors.SimpleButton();
            this.txtTENLOAITROCHOI = new DevExpress.XtraEditors.TextEdit();
            this.btnSuaLOAITROCHOI = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaLOAITROCHOI = new DevExpress.XtraEditors.SimpleButton();
            this.lblGHICHU = new DevExpress.XtraEditors.LabelControl();
            this.btnThemLOAITROCHOI = new DevExpress.XtraEditors.SimpleButton();
            this.chkSUDUNGLOAITROCHOI = new DevExpress.XtraEditors.CheckEdit();
            this.chkTHUCDONLOAITROCHOI = new DevExpress.XtraEditors.CheckEdit();
            this.txtSTTLOAITROCHOI = new DevExpress.XtraEditors.TextEdit();
            this.txtGHICHULOAITROCHOI = new DevExpress.XtraEditors.TextEdit();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.btnChonLOAITROCHOI = new DevExpress.XtraEditors.SimpleButton();
            this.btnChonTROCHOI = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTROCHOI)).BeginInit();
            this.pnlTROCHOI.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBlock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTinhgio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboMayin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIAMUA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGTROCHOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLOAITROCHOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picTROCHOI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUTROCHOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENTROCHOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTTTROCHOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDonvitinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboThue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlLOAITROCHOI)).BeginInit();
            this.pnlLOAITROCHOI.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLOAITROCHOI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENLOAITROCHOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGLOAITROCHOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTHUCDONLOAITROCHOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTTLOAITROCHOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHULOAITROCHOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Controls.Add(this.panelControl4);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 742);
            this.panelControl1.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.fpnlTROCHOI);
            this.panelControl2.Controls.Add(this.btnXuongHangHoa);
            this.panelControl2.Controls.Add(this.btnLenHangHoa);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(531, 738);
            this.panelControl2.TabIndex = 0;
            // 
            // fpnlTROCHOI
            // 
            this.fpnlTROCHOI.AutoScroll = true;
            this.fpnlTROCHOI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpnlTROCHOI.Font = new System.Drawing.Font("Tahoma", 12F);
            this.fpnlTROCHOI.Location = new System.Drawing.Point(2, 37);
            this.fpnlTROCHOI.Name = "fpnlTROCHOI";
            this.fpnlTROCHOI.Size = new System.Drawing.Size(527, 664);
            this.fpnlTROCHOI.TabIndex = 18;
            // 
            // btnXuongHangHoa
            // 
            this.btnXuongHangHoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongHangHoa.Appearance.Options.UseFont = true;
            this.btnXuongHangHoa.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongHangHoa.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongHangHoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongHangHoa.Location = new System.Drawing.Point(2, 701);
            this.btnXuongHangHoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongHangHoa.Name = "btnXuongHangHoa";
            this.btnXuongHangHoa.Size = new System.Drawing.Size(527, 35);
            this.btnXuongHangHoa.TabIndex = 17;
            // 
            // btnLenHangHoa
            // 
            this.btnLenHangHoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenHangHoa.Appearance.Options.UseFont = true;
            this.btnLenHangHoa.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenHangHoa.Image = ((System.Drawing.Image)(resources.GetObject("btnLenHangHoa.Image")));
            this.btnLenHangHoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenHangHoa.Location = new System.Drawing.Point(2, 2);
            this.btnLenHangHoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnLenHangHoa.Name = "btnLenHangHoa";
            this.btnLenHangHoa.Size = new System.Drawing.Size(527, 35);
            this.btnLenHangHoa.TabIndex = 16;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.fpnlLOAITROCHOI);
            this.panelControl3.Controls.Add(this.btnXuongNhomhang);
            this.panelControl3.Controls.Add(this.btnLenNhomhang);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl3.Location = new System.Drawing.Point(533, 2);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(135, 738);
            this.panelControl3.TabIndex = 1;
            // 
            // fpnlLOAITROCHOI
            // 
            this.fpnlLOAITROCHOI.AutoScroll = true;
            this.fpnlLOAITROCHOI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpnlLOAITROCHOI.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fpnlLOAITROCHOI.Location = new System.Drawing.Point(2, 37);
            this.fpnlLOAITROCHOI.Margin = new System.Windows.Forms.Padding(0);
            this.fpnlLOAITROCHOI.Name = "fpnlLOAITROCHOI";
            this.fpnlLOAITROCHOI.Size = new System.Drawing.Size(131, 664);
            this.fpnlLOAITROCHOI.TabIndex = 17;
            // 
            // btnXuongNhomhang
            // 
            this.btnXuongNhomhang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongNhomhang.Appearance.Options.UseFont = true;
            this.btnXuongNhomhang.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongNhomhang.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongNhomhang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongNhomhang.Location = new System.Drawing.Point(2, 701);
            this.btnXuongNhomhang.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongNhomhang.Name = "btnXuongNhomhang";
            this.btnXuongNhomhang.Size = new System.Drawing.Size(131, 35);
            this.btnXuongNhomhang.TabIndex = 16;
            // 
            // btnLenNhomhang
            // 
            this.btnLenNhomhang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenNhomhang.Appearance.Options.UseFont = true;
            this.btnLenNhomhang.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenNhomhang.Image = ((System.Drawing.Image)(resources.GetObject("btnLenNhomhang.Image")));
            this.btnLenNhomhang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenNhomhang.Location = new System.Drawing.Point(2, 2);
            this.btnLenNhomhang.Margin = new System.Windows.Forms.Padding(4);
            this.btnLenNhomhang.Name = "btnLenNhomhang";
            this.btnLenNhomhang.Size = new System.Drawing.Size(131, 35);
            this.btnLenNhomhang.TabIndex = 15;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.pnlTROCHOI);
            this.panelControl4.Controls.Add(this.panelControl7);
            this.panelControl4.Controls.Add(this.pnlLOAITROCHOI);
            this.panelControl4.Controls.Add(this.panelControl12);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl4.Location = new System.Drawing.Point(668, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(350, 738);
            this.panelControl4.TabIndex = 0;
            // 
            // pnlTROCHOI
            // 
            this.pnlTROCHOI.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlTROCHOI.Controls.Add(this.panelControl6);
            this.pnlTROCHOI.Controls.Add(this.panelControl5);
            this.pnlTROCHOI.Controls.Add(this.txtBlock);
            this.pnlTROCHOI.Controls.Add(this.labelControl2);
            this.pnlTROCHOI.Controls.Add(this.lblSOLUONG);
            this.pnlTROCHOI.Controls.Add(this.chkTinhgio);
            this.pnlTROCHOI.Controls.Add(this.labelControl8);
            this.pnlTROCHOI.Controls.Add(this.cboMayin);
            this.pnlTROCHOI.Controls.Add(this.panelControl10);
            this.pnlTROCHOI.Controls.Add(this.txtGIABAN2);
            this.pnlTROCHOI.Controls.Add(this.labelControl13);
            this.pnlTROCHOI.Controls.Add(this.txtGIABAN1);
            this.pnlTROCHOI.Controls.Add(this.labelControl12);
            this.pnlTROCHOI.Controls.Add(this.txtGIAMUA);
            this.pnlTROCHOI.Controls.Add(this.labelControl11);
            this.pnlTROCHOI.Controls.Add(this.labelControl10);
            this.pnlTROCHOI.Controls.Add(this.chkSUDUNGTROCHOI);
            this.pnlTROCHOI.Controls.Add(this.btnLuuTROCHOI);
            this.pnlTROCHOI.Controls.Add(this.btnSuaTROCHOI);
            this.pnlTROCHOI.Controls.Add(this.btnXoaTROCHOI);
            this.pnlTROCHOI.Controls.Add(this.btnThemTROCHOI);
            this.pnlTROCHOI.Controls.Add(this.cboLOAITROCHOI);
            this.pnlTROCHOI.Controls.Add(this.labelControl5);
            this.pnlTROCHOI.Controls.Add(this.panelControl9);
            this.pnlTROCHOI.Controls.Add(this.txtGHICHUTROCHOI);
            this.pnlTROCHOI.Controls.Add(this.labelControl1);
            this.pnlTROCHOI.Controls.Add(this.lblTENTROCHOI);
            this.pnlTROCHOI.Controls.Add(this.txtTENTROCHOI);
            this.pnlTROCHOI.Controls.Add(this.txtSTTTROCHOI);
            this.pnlTROCHOI.Controls.Add(this.labelControl4);
            this.pnlTROCHOI.Controls.Add(this.lblTEN);
            this.pnlTROCHOI.Controls.Add(this.cboDonvitinh);
            this.pnlTROCHOI.Controls.Add(this.cboThue);
            this.pnlTROCHOI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTROCHOI.Location = new System.Drawing.Point(2, 203);
            this.pnlTROCHOI.Name = "pnlTROCHOI";
            this.pnlTROCHOI.Size = new System.Drawing.Size(346, 498);
            this.pnlTROCHOI.TabIndex = 0;
            // 
            // panelControl5
            // 
            this.panelControl5.Location = new System.Drawing.Point(1, 252);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(344, 2);
            this.panelControl5.TabIndex = 27;
            // 
            // txtBlock
            // 
            this.txtBlock.EnterMoveNextControl = true;
            this.txtBlock.Location = new System.Drawing.Point(78, 282);
            this.txtBlock.Name = "txtBlock";
            this.txtBlock.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBlock.Properties.Appearance.Options.UseFont = true;
            this.txtBlock.Properties.Appearance.Options.UseTextOptions = true;
            this.txtBlock.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtBlock.Properties.DisplayFormat.FormatString = "N0";
            this.txtBlock.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtBlock.Properties.EditFormat.FormatString = "N0";
            this.txtBlock.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtBlock.Properties.Mask.EditMask = "N0";
            this.txtBlock.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtBlock.Size = new System.Drawing.Size(226, 26);
            this.txtBlock.TabIndex = 19;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(311, 285);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(32, 19);
            this.labelControl2.TabIndex = 20;
            this.labelControl2.Text = "Phút";
            // 
            // lblSOLUONG
            // 
            this.lblSOLUONG.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSOLUONG.Location = new System.Drawing.Point(3, 285);
            this.lblSOLUONG.Margin = new System.Windows.Forms.Padding(4);
            this.lblSOLUONG.Name = "lblSOLUONG";
            this.lblSOLUONG.Size = new System.Drawing.Size(68, 19);
            this.lblSOLUONG.TabIndex = 18;
            this.lblSOLUONG.Text = "1 Block =";
            // 
            // chkTinhgio
            // 
            this.chkTinhgio.EnterMoveNextControl = true;
            this.chkTinhgio.Location = new System.Drawing.Point(76, 252);
            this.chkTinhgio.Name = "chkTinhgio";
            this.chkTinhgio.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTinhgio.Properties.Appearance.Options.UseFont = true;
            this.chkTinhgio.Properties.Caption = "Vé tính theo giờ";
            this.chkTinhgio.Size = new System.Drawing.Size(147, 24);
            this.chkTinhgio.TabIndex = 17;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(3, 223);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(46, 19);
            this.labelControl8.TabIndex = 15;
            this.labelControl8.Text = "Máy in";
            // 
            // cboMayin
            // 
            this.cboMayin.EnterMoveNextControl = true;
            this.cboMayin.Location = new System.Drawing.Point(78, 220);
            this.cboMayin.Name = "cboMayin";
            this.cboMayin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMayin.Properties.Appearance.Options.UseFont = true;
            this.cboMayin.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMayin.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboMayin.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboMayin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboMayin.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboMayin.Properties.DisplayMember = "TEN";
            this.cboMayin.Properties.DropDownItemHeight = 40;
            this.cboMayin.Properties.NullText = "";
            this.cboMayin.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboMayin.Properties.ShowHeader = false;
            this.cboMayin.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboMayin.Properties.ValueMember = "MA";
            this.cboMayin.Size = new System.Drawing.Size(265, 26);
            this.cboMayin.TabIndex = 16;
            // 
            // panelControl10
            // 
            this.panelControl10.Location = new System.Drawing.Point(0, 154);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(344, 2);
            this.panelControl10.TabIndex = 10;
            // 
            // txtGIABAN2
            // 
            this.txtGIABAN2.EnterMoveNextControl = true;
            this.txtGIABAN2.Location = new System.Drawing.Point(78, 188);
            this.txtGIABAN2.Name = "txtGIABAN2";
            this.txtGIABAN2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGIABAN2.Properties.Appearance.Options.UseFont = true;
            this.txtGIABAN2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGIABAN2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGIABAN2.Properties.DisplayFormat.FormatString = "N0";
            this.txtGIABAN2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN2.Properties.EditFormat.FormatString = "N0";
            this.txtGIABAN2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN2.Properties.Mask.EditMask = "N0";
            this.txtGIABAN2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGIABAN2.Size = new System.Drawing.Size(265, 26);
            this.txtGIABAN2.TabIndex = 14;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Location = new System.Drawing.Point(3, 191);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(68, 19);
            this.labelControl13.TabIndex = 13;
            this.labelControl13.Text = "Giá bán 2";
            // 
            // txtGIABAN1
            // 
            this.txtGIABAN1.EnterMoveNextControl = true;
            this.txtGIABAN1.Location = new System.Drawing.Point(78, 159);
            this.txtGIABAN1.Name = "txtGIABAN1";
            this.txtGIABAN1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGIABAN1.Properties.Appearance.Options.UseFont = true;
            this.txtGIABAN1.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGIABAN1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGIABAN1.Properties.DisplayFormat.FormatString = "N0";
            this.txtGIABAN1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN1.Properties.EditFormat.FormatString = "N0";
            this.txtGIABAN1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN1.Properties.Mask.EditMask = "N0";
            this.txtGIABAN1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGIABAN1.Size = new System.Drawing.Size(265, 26);
            this.txtGIABAN1.TabIndex = 12;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(3, 162);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(68, 19);
            this.labelControl12.TabIndex = 11;
            this.labelControl12.Text = "Giá bán 1";
            // 
            // txtGIAMUA
            // 
            this.txtGIAMUA.EnterMoveNextControl = true;
            this.txtGIAMUA.Location = new System.Drawing.Point(78, 188);
            this.txtGIAMUA.Name = "txtGIAMUA";
            this.txtGIAMUA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGIAMUA.Properties.Appearance.Options.UseFont = true;
            this.txtGIAMUA.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGIAMUA.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGIAMUA.Properties.DisplayFormat.FormatString = "N0";
            this.txtGIAMUA.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIAMUA.Properties.EditFormat.FormatString = "N0";
            this.txtGIAMUA.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIAMUA.Properties.Mask.EditMask = "N0";
            this.txtGIAMUA.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGIAMUA.Size = new System.Drawing.Size(265, 26);
            this.txtGIAMUA.TabIndex = 14;
            this.txtGIAMUA.Visible = false;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(3, 191);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(59, 19);
            this.labelControl11.TabIndex = 13;
            this.labelControl11.Text = "Giá mua";
            this.labelControl11.Visible = false;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(3, 162);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(36, 19);
            this.labelControl10.TabIndex = 11;
            this.labelControl10.Text = "Thuế";
            this.labelControl10.Visible = false;
            // 
            // chkSUDUNGTROCHOI
            // 
            this.chkSUDUNGTROCHOI.EnterMoveNextControl = true;
            this.chkSUDUNGTROCHOI.Location = new System.Drawing.Point(76, 321);
            this.chkSUDUNGTROCHOI.Name = "chkSUDUNGTROCHOI";
            this.chkSUDUNGTROCHOI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNGTROCHOI.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNGTROCHOI.Properties.Caption = "Sử dụng";
            this.chkSUDUNGTROCHOI.Size = new System.Drawing.Size(92, 24);
            this.chkSUDUNGTROCHOI.TabIndex = 21;
            // 
            // btnLuuTROCHOI
            // 
            this.btnLuuTROCHOI.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuTROCHOI.Appearance.Options.UseFont = true;
            this.btnLuuTROCHOI.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuuTROCHOI.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuuTROCHOI.Location = new System.Drawing.Point(6, 352);
            this.btnLuuTROCHOI.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuuTROCHOI.Name = "btnLuuTROCHOI";
            this.btnLuuTROCHOI.Size = new System.Drawing.Size(80, 35);
            this.btnLuuTROCHOI.TabIndex = 22;
            this.btnLuuTROCHOI.Text = "&Lưu";
            this.btnLuuTROCHOI.Click += new System.EventHandler(this.btnLuuTROCHOI_Click);
            // 
            // btnSuaTROCHOI
            // 
            this.btnSuaTROCHOI.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaTROCHOI.Appearance.Options.UseFont = true;
            this.btnSuaTROCHOI.Image = ((System.Drawing.Image)(resources.GetObject("btnSuaTROCHOI.Image")));
            this.btnSuaTROCHOI.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSuaTROCHOI.Location = new System.Drawing.Point(172, 352);
            this.btnSuaTROCHOI.Margin = new System.Windows.Forms.Padding(4);
            this.btnSuaTROCHOI.Name = "btnSuaTROCHOI";
            this.btnSuaTROCHOI.Size = new System.Drawing.Size(80, 35);
            this.btnSuaTROCHOI.TabIndex = 24;
            this.btnSuaTROCHOI.Text = "&Sửa";
            this.btnSuaTROCHOI.Click += new System.EventHandler(this.btnSuaTROCHOI_Click);
            // 
            // btnXoaTROCHOI
            // 
            this.btnXoaTROCHOI.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaTROCHOI.Appearance.Options.UseFont = true;
            this.btnXoaTROCHOI.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoaTROCHOI.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoaTROCHOI.Location = new System.Drawing.Point(255, 352);
            this.btnXoaTROCHOI.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoaTROCHOI.Name = "btnXoaTROCHOI";
            this.btnXoaTROCHOI.Size = new System.Drawing.Size(80, 35);
            this.btnXoaTROCHOI.TabIndex = 25;
            this.btnXoaTROCHOI.Text = "&Xóa";
            this.btnXoaTROCHOI.Click += new System.EventHandler(this.btnXoaTROCHOI_Click);
            // 
            // btnThemTROCHOI
            // 
            this.btnThemTROCHOI.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemTROCHOI.Appearance.Options.UseFont = true;
            this.btnThemTROCHOI.Image = ((System.Drawing.Image)(resources.GetObject("btnThemTROCHOI.Image")));
            this.btnThemTROCHOI.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThemTROCHOI.Location = new System.Drawing.Point(89, 352);
            this.btnThemTROCHOI.Margin = new System.Windows.Forms.Padding(4);
            this.btnThemTROCHOI.Name = "btnThemTROCHOI";
            this.btnThemTROCHOI.Size = new System.Drawing.Size(80, 35);
            this.btnThemTROCHOI.TabIndex = 23;
            this.btnThemTROCHOI.Text = "&Thêm";
            this.btnThemTROCHOI.Click += new System.EventHandler(this.btnThemTROCHOI_Click);
            // 
            // cboLOAITROCHOI
            // 
            this.cboLOAITROCHOI.EnterMoveNextControl = true;
            this.cboLOAITROCHOI.Location = new System.Drawing.Point(61, 6);
            this.cboLOAITROCHOI.Name = "cboLOAITROCHOI";
            this.cboLOAITROCHOI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLOAITROCHOI.Properties.Appearance.Options.UseFont = true;
            this.cboLOAITROCHOI.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLOAITROCHOI.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboLOAITROCHOI.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboLOAITROCHOI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboLOAITROCHOI.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboLOAITROCHOI.Properties.DisplayMember = "TEN";
            this.cboLOAITROCHOI.Properties.DropDownItemHeight = 40;
            this.cboLOAITROCHOI.Properties.NullText = "";
            this.cboLOAITROCHOI.Properties.ShowHeader = false;
            this.cboLOAITROCHOI.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboLOAITROCHOI.Properties.ValueMember = "MA";
            this.cboLOAITROCHOI.Size = new System.Drawing.Size(282, 26);
            this.cboLOAITROCHOI.TabIndex = 1;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(3, 9);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(29, 19);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "Loại";
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.picTROCHOI);
            this.panelControl9.Location = new System.Drawing.Point(225, 34);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(118, 88);
            this.panelControl9.TabIndex = 26;
            // 
            // picTROCHOI
            // 
            this.picTROCHOI.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            this.picTROCHOI.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picTROCHOI.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picTROCHOI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picTROCHOI.Location = new System.Drawing.Point(2, 2);
            this.picTROCHOI.Name = "picTROCHOI";
            this.picTROCHOI.Size = new System.Drawing.Size(114, 84);
            this.picTROCHOI.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picTROCHOI.TabIndex = 23;
            this.picTROCHOI.TabStop = false;
            this.picTROCHOI.Click += new System.EventHandler(this.picTROCHOI_Click);
            // 
            // txtGHICHUTROCHOI
            // 
            this.txtGHICHUTROCHOI.EnterMoveNextControl = true;
            this.txtGHICHUTROCHOI.Location = new System.Drawing.Point(61, 125);
            this.txtGHICHUTROCHOI.Name = "txtGHICHUTROCHOI";
            this.txtGHICHUTROCHOI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHUTROCHOI.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHUTROCHOI.Size = new System.Drawing.Size(282, 26);
            this.txtGHICHUTROCHOI.TabIndex = 9;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(3, 96);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(29, 19);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "STT";
            // 
            // lblTENTROCHOI
            // 
            this.lblTENTROCHOI.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTENTROCHOI.Location = new System.Drawing.Point(3, 38);
            this.lblTENTROCHOI.Margin = new System.Windows.Forms.Padding(4);
            this.lblTENTROCHOI.Name = "lblTENTROCHOI";
            this.lblTENTROCHOI.Size = new System.Drawing.Size(27, 19);
            this.lblTENTROCHOI.TabIndex = 2;
            this.lblTENTROCHOI.Text = "Tên";
            // 
            // txtTENTROCHOI
            // 
            this.txtTENTROCHOI.EnterMoveNextControl = true;
            this.txtTENTROCHOI.Location = new System.Drawing.Point(61, 35);
            this.txtTENTROCHOI.Name = "txtTENTROCHOI";
            this.txtTENTROCHOI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTENTROCHOI.Properties.Appearance.Options.UseFont = true;
            this.txtTENTROCHOI.Size = new System.Drawing.Size(162, 26);
            this.txtTENTROCHOI.TabIndex = 3;
            // 
            // txtSTTTROCHOI
            // 
            this.txtSTTTROCHOI.EnterMoveNextControl = true;
            this.txtSTTTROCHOI.Location = new System.Drawing.Point(61, 93);
            this.txtSTTTROCHOI.Name = "txtSTTTROCHOI";
            this.txtSTTTROCHOI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSTTTROCHOI.Properties.Appearance.Options.UseFont = true;
            this.txtSTTTROCHOI.Properties.Mask.EditMask = "d";
            this.txtSTTTROCHOI.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSTTTROCHOI.Size = new System.Drawing.Size(162, 26);
            this.txtSTTTROCHOI.TabIndex = 7;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(3, 128);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(54, 19);
            this.labelControl4.TabIndex = 8;
            this.labelControl4.Text = "Ghi chú";
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(3, 67);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(31, 19);
            this.lblTEN.TabIndex = 4;
            this.lblTEN.Text = "ĐVT";
            // 
            // cboDonvitinh
            // 
            this.cboDonvitinh.EnterMoveNextControl = true;
            this.cboDonvitinh.Location = new System.Drawing.Point(61, 64);
            this.cboDonvitinh.Name = "cboDonvitinh";
            this.cboDonvitinh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDonvitinh.Properties.Appearance.Options.UseFont = true;
            this.cboDonvitinh.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDonvitinh.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboDonvitinh.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboDonvitinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboDonvitinh.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboDonvitinh.Properties.DisplayMember = "TEN";
            this.cboDonvitinh.Properties.DropDownItemHeight = 40;
            this.cboDonvitinh.Properties.NullText = "";
            this.cboDonvitinh.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboDonvitinh.Properties.ShowHeader = false;
            this.cboDonvitinh.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboDonvitinh.Properties.ValueMember = "MA";
            this.cboDonvitinh.Size = new System.Drawing.Size(162, 26);
            this.cboDonvitinh.TabIndex = 5;
            // 
            // cboThue
            // 
            this.cboThue.EnterMoveNextControl = true;
            this.cboThue.Location = new System.Drawing.Point(78, 159);
            this.cboThue.Name = "cboThue";
            this.cboThue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboThue.Properties.Appearance.Options.UseFont = true;
            this.cboThue.Properties.Appearance.Options.UseTextOptions = true;
            this.cboThue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cboThue.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cboThue.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboThue.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.cboThue.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cboThue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboThue.Properties.DropDownItemHeight = 40;
            this.cboThue.Properties.Items.AddRange(new object[] {
            "0",
            "5",
            "10"});
            this.cboThue.Size = new System.Drawing.Size(265, 26);
            this.cboThue.TabIndex = 12;
            this.cboThue.Visible = false;
            // 
            // panelControl7
            // 
            this.panelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl7.Controls.Add(this.btnCapnhat);
            this.panelControl7.Controls.Add(this.btnBanphim);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl7.Location = new System.Drawing.Point(2, 701);
            this.panelControl7.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(346, 35);
            this.panelControl7.TabIndex = 17;
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(0, 0);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(4);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(173, 35);
            this.btnCapnhat.TabIndex = 1;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(173, 0);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(4);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(173, 35);
            this.btnBanphim.TabIndex = 10;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // pnlLOAITROCHOI
            // 
            this.pnlLOAITROCHOI.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlLOAITROCHOI.Controls.Add(this.panelControl8);
            this.pnlLOAITROCHOI.Controls.Add(this.labelControl3);
            this.pnlLOAITROCHOI.Controls.Add(this.lblTENLOAITROCHOI);
            this.pnlLOAITROCHOI.Controls.Add(this.btnLuuLOAITROCHOI);
            this.pnlLOAITROCHOI.Controls.Add(this.txtTENLOAITROCHOI);
            this.pnlLOAITROCHOI.Controls.Add(this.btnSuaLOAITROCHOI);
            this.pnlLOAITROCHOI.Controls.Add(this.btnXoaLOAITROCHOI);
            this.pnlLOAITROCHOI.Controls.Add(this.lblGHICHU);
            this.pnlLOAITROCHOI.Controls.Add(this.btnThemLOAITROCHOI);
            this.pnlLOAITROCHOI.Controls.Add(this.chkSUDUNGLOAITROCHOI);
            this.pnlLOAITROCHOI.Controls.Add(this.chkTHUCDONLOAITROCHOI);
            this.pnlLOAITROCHOI.Controls.Add(this.txtSTTLOAITROCHOI);
            this.pnlLOAITROCHOI.Controls.Add(this.txtGHICHULOAITROCHOI);
            this.pnlLOAITROCHOI.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlLOAITROCHOI.Location = new System.Drawing.Point(2, 37);
            this.pnlLOAITROCHOI.Name = "pnlLOAITROCHOI";
            this.pnlLOAITROCHOI.Size = new System.Drawing.Size(346, 166);
            this.pnlLOAITROCHOI.TabIndex = 0;
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.picLOAITROCHOI);
            this.panelControl8.Location = new System.Drawing.Point(225, 5);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(118, 88);
            this.panelControl8.TabIndex = 12;
            // 
            // picLOAITROCHOI
            // 
            this.picLOAITROCHOI.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            this.picLOAITROCHOI.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picLOAITROCHOI.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picLOAITROCHOI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picLOAITROCHOI.Location = new System.Drawing.Point(2, 2);
            this.picLOAITROCHOI.Name = "picLOAITROCHOI";
            this.picLOAITROCHOI.Size = new System.Drawing.Size(114, 84);
            this.picLOAITROCHOI.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picLOAITROCHOI.TabIndex = 23;
            this.picLOAITROCHOI.TabStop = false;
            this.picLOAITROCHOI.Click += new System.EventHandler(this.picLOAITROCHOI_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(3, 37);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(29, 19);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "STT";
            // 
            // lblTENLOAITROCHOI
            // 
            this.lblTENLOAITROCHOI.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTENLOAITROCHOI.Location = new System.Drawing.Point(3, 8);
            this.lblTENLOAITROCHOI.Margin = new System.Windows.Forms.Padding(4);
            this.lblTENLOAITROCHOI.Name = "lblTENLOAITROCHOI";
            this.lblTENLOAITROCHOI.Size = new System.Drawing.Size(27, 19);
            this.lblTENLOAITROCHOI.TabIndex = 0;
            this.lblTENLOAITROCHOI.Text = "Tên";
            // 
            // btnLuuLOAITROCHOI
            // 
            this.btnLuuLOAITROCHOI.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuLOAITROCHOI.Appearance.Options.UseFont = true;
            this.btnLuuLOAITROCHOI.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuuLOAITROCHOI.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuuLOAITROCHOI.Location = new System.Drawing.Point(6, 126);
            this.btnLuuLOAITROCHOI.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuuLOAITROCHOI.Name = "btnLuuLOAITROCHOI";
            this.btnLuuLOAITROCHOI.Size = new System.Drawing.Size(80, 35);
            this.btnLuuLOAITROCHOI.TabIndex = 8;
            this.btnLuuLOAITROCHOI.Text = "&Lưu";
            this.btnLuuLOAITROCHOI.Click += new System.EventHandler(this.btnLuuLOAITROCHOI_Click);
            // 
            // txtTENLOAITROCHOI
            // 
            this.txtTENLOAITROCHOI.EnterMoveNextControl = true;
            this.txtTENLOAITROCHOI.Location = new System.Drawing.Point(61, 5);
            this.txtTENLOAITROCHOI.Name = "txtTENLOAITROCHOI";
            this.txtTENLOAITROCHOI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTENLOAITROCHOI.Properties.Appearance.Options.UseFont = true;
            this.txtTENLOAITROCHOI.Size = new System.Drawing.Size(162, 26);
            this.txtTENLOAITROCHOI.TabIndex = 1;
            // 
            // btnSuaLOAITROCHOI
            // 
            this.btnSuaLOAITROCHOI.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaLOAITROCHOI.Appearance.Options.UseFont = true;
            this.btnSuaLOAITROCHOI.Image = ((System.Drawing.Image)(resources.GetObject("btnSuaLOAITROCHOI.Image")));
            this.btnSuaLOAITROCHOI.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSuaLOAITROCHOI.Location = new System.Drawing.Point(172, 126);
            this.btnSuaLOAITROCHOI.Margin = new System.Windows.Forms.Padding(4);
            this.btnSuaLOAITROCHOI.Name = "btnSuaLOAITROCHOI";
            this.btnSuaLOAITROCHOI.Size = new System.Drawing.Size(80, 35);
            this.btnSuaLOAITROCHOI.TabIndex = 10;
            this.btnSuaLOAITROCHOI.Text = "&Sửa";
            this.btnSuaLOAITROCHOI.Click += new System.EventHandler(this.btnSuaLOAITROCHOI_Click);
            // 
            // btnXoaLOAITROCHOI
            // 
            this.btnXoaLOAITROCHOI.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaLOAITROCHOI.Appearance.Options.UseFont = true;
            this.btnXoaLOAITROCHOI.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoaLOAITROCHOI.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoaLOAITROCHOI.Location = new System.Drawing.Point(255, 126);
            this.btnXoaLOAITROCHOI.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoaLOAITROCHOI.Name = "btnXoaLOAITROCHOI";
            this.btnXoaLOAITROCHOI.Size = new System.Drawing.Size(80, 35);
            this.btnXoaLOAITROCHOI.TabIndex = 11;
            this.btnXoaLOAITROCHOI.Text = "&Xóa";
            this.btnXoaLOAITROCHOI.Click += new System.EventHandler(this.btnXoaLOAITROCHOI_Click);
            // 
            // lblGHICHU
            // 
            this.lblGHICHU.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGHICHU.Location = new System.Drawing.Point(3, 66);
            this.lblGHICHU.Margin = new System.Windows.Forms.Padding(4);
            this.lblGHICHU.Name = "lblGHICHU";
            this.lblGHICHU.Size = new System.Drawing.Size(54, 19);
            this.lblGHICHU.TabIndex = 4;
            this.lblGHICHU.Text = "Ghi chú";
            // 
            // btnThemLOAITROCHOI
            // 
            this.btnThemLOAITROCHOI.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemLOAITROCHOI.Appearance.Options.UseFont = true;
            this.btnThemLOAITROCHOI.Image = ((System.Drawing.Image)(resources.GetObject("btnThemLOAITROCHOI.Image")));
            this.btnThemLOAITROCHOI.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThemLOAITROCHOI.Location = new System.Drawing.Point(89, 126);
            this.btnThemLOAITROCHOI.Margin = new System.Windows.Forms.Padding(4);
            this.btnThemLOAITROCHOI.Name = "btnThemLOAITROCHOI";
            this.btnThemLOAITROCHOI.Size = new System.Drawing.Size(80, 35);
            this.btnThemLOAITROCHOI.TabIndex = 9;
            this.btnThemLOAITROCHOI.Text = "&Thêm";
            this.btnThemLOAITROCHOI.Click += new System.EventHandler(this.btnThemLOAITROCHOI_Click);
            // 
            // chkSUDUNGLOAITROCHOI
            // 
            this.chkSUDUNGLOAITROCHOI.EnterMoveNextControl = true;
            this.chkSUDUNGLOAITROCHOI.Location = new System.Drawing.Point(59, 95);
            this.chkSUDUNGLOAITROCHOI.Name = "chkSUDUNGLOAITROCHOI";
            this.chkSUDUNGLOAITROCHOI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNGLOAITROCHOI.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNGLOAITROCHOI.Properties.Caption = "Sử dụng";
            this.chkSUDUNGLOAITROCHOI.Size = new System.Drawing.Size(92, 24);
            this.chkSUDUNGLOAITROCHOI.TabIndex = 6;
            // 
            // chkTHUCDONLOAITROCHOI
            // 
            this.chkTHUCDONLOAITROCHOI.EnterMoveNextControl = true;
            this.chkTHUCDONLOAITROCHOI.Location = new System.Drawing.Point(157, 95);
            this.chkTHUCDONLOAITROCHOI.Name = "chkTHUCDONLOAITROCHOI";
            this.chkTHUCDONLOAITROCHOI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTHUCDONLOAITROCHOI.Properties.Appearance.Options.UseFont = true;
            this.chkTHUCDONLOAITROCHOI.Properties.Caption = "Thực đơn";
            this.chkTHUCDONLOAITROCHOI.Size = new System.Drawing.Size(92, 24);
            this.chkTHUCDONLOAITROCHOI.TabIndex = 7;
            this.chkTHUCDONLOAITROCHOI.Visible = false;
            // 
            // txtSTTLOAITROCHOI
            // 
            this.txtSTTLOAITROCHOI.EnterMoveNextControl = true;
            this.txtSTTLOAITROCHOI.Location = new System.Drawing.Point(61, 34);
            this.txtSTTLOAITROCHOI.Name = "txtSTTLOAITROCHOI";
            this.txtSTTLOAITROCHOI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSTTLOAITROCHOI.Properties.Appearance.Options.UseFont = true;
            this.txtSTTLOAITROCHOI.Properties.Mask.BeepOnError = true;
            this.txtSTTLOAITROCHOI.Properties.Mask.EditMask = "d";
            this.txtSTTLOAITROCHOI.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSTTLOAITROCHOI.Size = new System.Drawing.Size(162, 26);
            this.txtSTTLOAITROCHOI.TabIndex = 3;
            // 
            // txtGHICHULOAITROCHOI
            // 
            this.txtGHICHULOAITROCHOI.EnterMoveNextControl = true;
            this.txtGHICHULOAITROCHOI.Location = new System.Drawing.Point(61, 63);
            this.txtGHICHULOAITROCHOI.Name = "txtGHICHULOAITROCHOI";
            this.txtGHICHULOAITROCHOI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHULOAITROCHOI.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHULOAITROCHOI.Size = new System.Drawing.Size(162, 26);
            this.txtGHICHULOAITROCHOI.TabIndex = 5;
            // 
            // panelControl12
            // 
            this.panelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl12.Controls.Add(this.btnChonLOAITROCHOI);
            this.panelControl12.Controls.Add(this.btnChonTROCHOI);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl12.Location = new System.Drawing.Point(2, 2);
            this.panelControl12.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(346, 35);
            this.panelControl12.TabIndex = 18;
            // 
            // btnChonLOAITROCHOI
            // 
            this.btnChonLOAITROCHOI.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonLOAITROCHOI.Appearance.Options.UseFont = true;
            this.btnChonLOAITROCHOI.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnChonLOAITROCHOI.Image = global::KP_RES.Properties.Resources.list_26;
            this.btnChonLOAITROCHOI.Location = new System.Drawing.Point(0, 0);
            this.btnChonLOAITROCHOI.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonLOAITROCHOI.Name = "btnChonLOAITROCHOI";
            this.btnChonLOAITROCHOI.Size = new System.Drawing.Size(173, 35);
            this.btnChonLOAITROCHOI.TabIndex = 1;
            this.btnChonLOAITROCHOI.Text = "Loại trò chơi";
            this.btnChonLOAITROCHOI.Click += new System.EventHandler(this.btnChonLOAITROCHOI_Click);
            // 
            // btnChonTROCHOI
            // 
            this.btnChonTROCHOI.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonTROCHOI.Appearance.Options.UseFont = true;
            this.btnChonTROCHOI.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnChonTROCHOI.Image = global::KP_RES.Properties.Resources.monkey_26;
            this.btnChonTROCHOI.Location = new System.Drawing.Point(173, 0);
            this.btnChonTROCHOI.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonTROCHOI.Name = "btnChonTROCHOI";
            this.btnChonTROCHOI.Size = new System.Drawing.Size(173, 35);
            this.btnChonTROCHOI.TabIndex = 10;
            this.btnChonTROCHOI.Text = "Trò chơi";
            this.btnChonTROCHOI.Click += new System.EventHandler(this.btnChonTROCHOI_Click);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton5.Location = new System.Drawing.Point(65, 32);
            this.simpleButton5.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(80, 35);
            this.simpleButton5.TabIndex = 10;
            this.simpleButton5.Text = "&Lưu";
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton6.Location = new System.Drawing.Point(73, 84);
            this.simpleButton6.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(80, 35);
            this.simpleButton6.TabIndex = 11;
            this.simpleButton6.Text = "&Lưu";
            // 
            // simpleButton7
            // 
            this.simpleButton7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton7.Appearance.Options.UseFont = true;
            this.simpleButton7.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton7.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton7.Location = new System.Drawing.Point(81, 92);
            this.simpleButton7.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(80, 35);
            this.simpleButton7.TabIndex = 12;
            this.simpleButton7.Text = "&Lưu";
            // 
            // simpleButton8
            // 
            this.simpleButton8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton8.Appearance.Options.UseFont = true;
            this.simpleButton8.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton8.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton8.Location = new System.Drawing.Point(89, 100);
            this.simpleButton8.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(80, 35);
            this.simpleButton8.TabIndex = 13;
            this.simpleButton8.Text = "&Lưu";
            // 
            // simpleButton9
            // 
            this.simpleButton9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton9.Appearance.Options.UseFont = true;
            this.simpleButton9.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton9.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton9.Location = new System.Drawing.Point(114, 150);
            this.simpleButton9.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(80, 35);
            this.simpleButton9.TabIndex = 14;
            this.simpleButton9.Text = "&Lưu";
            // 
            // panelControl6
            // 
            this.panelControl6.Location = new System.Drawing.Point(1, 316);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(344, 2);
            this.panelControl6.TabIndex = 28;
            // 
            // Frm_LoaiTroChoi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 742);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_LoaiTroChoi";
            this.Text = "Loại trò chơi - Trò chơi";
            this.Load += new System.EventHandler(this.Frm_LoaiTroChoi_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_LoaiTroChoi_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlTROCHOI)).EndInit();
            this.pnlTROCHOI.ResumeLayout(false);
            this.pnlTROCHOI.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBlock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTinhgio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboMayin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIAMUA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGTROCHOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLOAITROCHOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picTROCHOI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUTROCHOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENTROCHOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTTTROCHOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDonvitinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboThue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlLOAITROCHOI)).EndInit();
            this.pnlLOAITROCHOI.ResumeLayout(false);
            this.pnlLOAITROCHOI.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLOAITROCHOI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENLOAITROCHOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGLOAITROCHOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTHUCDONLOAITROCHOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTTLOAITROCHOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHULOAITROCHOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnXuongNhomhang;
        private DevExpress.XtraEditors.SimpleButton btnLenNhomhang;
        private System.Windows.Forms.FlowLayoutPanel fpnlTROCHOI;
        private DevExpress.XtraEditors.SimpleButton btnXuongHangHoa;
        private DevExpress.XtraEditors.SimpleButton btnLenHangHoa;
        private System.Windows.Forms.FlowLayoutPanel fpnlLOAITROCHOI;
        private DevExpress.XtraEditors.CheckEdit chkTHUCDONLOAITROCHOI;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNGLOAITROCHOI;
        private DevExpress.XtraEditors.LabelControl lblGHICHU;
        private DevExpress.XtraEditors.TextEdit txtGHICHULOAITROCHOI;
        private DevExpress.XtraEditors.LabelControl lblTENLOAITROCHOI;
        private DevExpress.XtraEditors.TextEdit txtTENLOAITROCHOI;
        private DevExpress.XtraEditors.SimpleButton btnLuuLOAITROCHOI;
        private DevExpress.XtraEditors.SimpleButton btnSuaLOAITROCHOI;
        private DevExpress.XtraEditors.SimpleButton btnXoaLOAITROCHOI;
        private DevExpress.XtraEditors.SimpleButton btnThemLOAITROCHOI;
        private DevExpress.XtraEditors.PanelControl pnlTROCHOI;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.PanelControl pnlLOAITROCHOI;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.TextEdit txtSTTLOAITROCHOI;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private System.Windows.Forms.PictureBox picLOAITROCHOI;
        private DevExpress.XtraEditors.LookUpEdit cboLOAITROCHOI;
        private DevExpress.XtraEditors.LookUpEdit cboDonvitinh;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private System.Windows.Forms.PictureBox picTROCHOI;
        private DevExpress.XtraEditors.TextEdit txtGHICHUTROCHOI;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblTENTROCHOI;
        private DevExpress.XtraEditors.TextEdit txtTENTROCHOI;
        private DevExpress.XtraEditors.TextEdit txtSTTTROCHOI;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.SimpleButton btnLuuTROCHOI;
        private DevExpress.XtraEditors.SimpleButton btnSuaTROCHOI;
        private DevExpress.XtraEditors.SimpleButton btnXoaTROCHOI;
        private DevExpress.XtraEditors.SimpleButton btnThemTROCHOI;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNGTROCHOI;
        private DevExpress.XtraEditors.TextEdit txtGIAMUA;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtGIABAN1;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txtGIABAN2;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.ComboBoxEdit cboThue;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.SimpleButton btnChonLOAITROCHOI;
        private DevExpress.XtraEditors.SimpleButton btnChonTROCHOI;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LookUpEdit cboMayin;
        private DevExpress.XtraEditors.CheckEdit chkTinhgio;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl lblSOLUONG;
        private DevExpress.XtraEditors.TextEdit txtBlock;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl6;




    }
}