﻿namespace KP_RES 
{
    partial class Frm_Banve 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Banve));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode4 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pnlTinhtiengio = new DevExpress.XtraEditors.PanelControl();
            this.btnThanhtoan = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl48 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl49 = new DevExpress.XtraEditors.PanelControl();
            this.txtTongtien = new DevExpress.XtraEditors.TextEdit();
            this.panelControl50 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl36 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl43 = new DevExpress.XtraEditors.PanelControl();
            this.txtThoigian = new DevExpress.XtraEditors.TextEdit();
            this.panelControl44 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl21 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl28 = new DevExpress.XtraEditors.PanelControl();
            this.txtGiora = new DevExpress.XtraEditors.TextEdit();
            this.panelControl29 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl37 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl38 = new DevExpress.XtraEditors.PanelControl();
            this.txtGiovao = new DevExpress.XtraEditors.TextEdit();
            this.panelControl39 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl45 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl46 = new DevExpress.XtraEditors.PanelControl();
            this.txtGiave = new DevExpress.XtraEditors.TextEdit();
            this.panelControl47 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl52 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl54 = new DevExpress.XtraEditors.PanelControl();
            this.txtThoigianquydoi = new DevExpress.XtraEditors.TextEdit();
            this.panelControl55 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl51 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl533 = new DevExpress.XtraEditors.PanelControl();
            this.txtTrochoi = new DevExpress.XtraEditors.TextEdit();
            this.panelControl53 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl40 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl41 = new DevExpress.XtraEditors.PanelControl();
            this.txtMaveTT = new DevExpress.XtraEditors.TextEdit();
            this.panelControl42 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.pnlBanve = new DevExpress.XtraEditors.PanelControl();
            this.panelControl58 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl57 = new DevExpress.XtraEditors.PanelControl();
            this.grdTiente = new DevExpress.XtraGrid.GridControl();
            this.grvtiente = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STTTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_TIENTE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_TIENTE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TYGIA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl59 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongtiente = new DevExpress.XtraEditors.SimpleButton();
            this.btnLentiente = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl62 = new DevExpress.XtraEditors.PanelControl();
            this.btnNhaplaitienkhachtra = new DevExpress.XtraEditors.SimpleButton();
            this.btnTTTNB = new DevExpress.XtraEditors.SimpleButton();
            this.btnInve = new DevExpress.XtraEditors.SimpleButton();
            this.btnKHTT = new DevExpress.XtraEditors.SimpleButton();
            this.pnlMakhachhang = new DevExpress.XtraEditors.PanelControl();
            this.panelControl60 = new DevExpress.XtraEditors.PanelControl();
            this.txtMakhachhang = new DevExpress.XtraEditors.TextEdit();
            this.panelControl61 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl16 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl17 = new DevExpress.XtraEditors.PanelControl();
            this.txtTienThoiLai = new DevExpress.XtraEditors.TextEdit();
            this.panelControl18 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.pnlQuydoi = new DevExpress.XtraEditors.PanelControl();
            this.panelControl63 = new DevExpress.XtraEditors.PanelControl();
            this.txtTienquydoi = new DevExpress.XtraEditors.TextEdit();
            this.panelControl64 = new DevExpress.XtraEditors.PanelControl();
            this.lblTienquydoi = new DevExpress.XtraEditors.LabelControl();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn5t = new DevExpress.XtraEditors.SimpleButton();
            this.btn1k = new DevExpress.XtraEditors.SimpleButton();
            this.btn2k = new DevExpress.XtraEditors.SimpleButton();
            this.btn5k = new DevExpress.XtraEditors.SimpleButton();
            this.btn10k = new DevExpress.XtraEditors.SimpleButton();
            this.btn20k = new DevExpress.XtraEditors.SimpleButton();
            this.btn50k = new DevExpress.XtraEditors.SimpleButton();
            this.btn100k = new DevExpress.XtraEditors.SimpleButton();
            this.btn200k = new DevExpress.XtraEditors.SimpleButton();
            this.btn500k = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl19 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.txtTienKhachTra = new DevExpress.XtraEditors.TextEdit();
            this.panelControl14 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.txtTienve = new DevExpress.XtraEditors.TextEdit();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.pnlSoatve = new DevExpress.XtraEditors.PanelControl();
            this.panelControl22 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl26 = new DevExpress.XtraEditors.PanelControl();
            this.txtThongtinve = new DevExpress.XtraEditors.MemoEdit();
            this.panelControl27 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl30 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl31 = new DevExpress.XtraEditors.PanelControl();
            this.txtTrangthai = new DevExpress.XtraEditors.TextEdit();
            this.panelControl32 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl33 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl34 = new DevExpress.XtraEditors.PanelControl();
            this.txtMave = new DevExpress.XtraEditors.TextEdit();
            this.panelControl35 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.pnlKetca = new DevExpress.XtraEditors.PanelControl();
            this.grdKetca = new DevExpress.XtraGrid.GridControl();
            this.grvKetca = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STTKC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THANHTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl23 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl24 = new DevExpress.XtraEditors.PanelControl();
            this.txtKetca = new DevExpress.XtraEditors.TextEdit();
            this.panelControl25 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl20 = new DevExpress.XtraEditors.PanelControl();
            this.btnInketca = new DevExpress.XtraEditors.SimpleButton();
            this.pnlHuyve = new DevExpress.XtraEditors.PanelControl();
            this.btnHuy = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.txtLYDO = new DevExpress.XtraEditors.MemoEdit();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.txtMA_VEBAN = new DevExpress.XtraEditors.TextEdit();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.pnlInlaive = new DevExpress.XtraEditors.PanelControl();
            this.grdInlaive = new DevExpress.XtraGrid.GridControl();
            this.grvInlaive = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_VEBAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_TROCHOI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENNHANVIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYBAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIABAN1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.INVE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.pnlLoaddanhmuc = new DevExpress.XtraEditors.PanelControl();
            this.pnlTrochoi = new DevExpress.XtraEditors.PanelControl();
            this.fpnlTROCHOI = new System.Windows.Forms.FlowLayoutPanel();
            this.btnXuongHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.pnlLoaitrochoi = new DevExpress.XtraEditors.PanelControl();
            this.fpnlLOAITROCHOI = new System.Windows.Forms.FlowLayoutPanel();
            this.btnXuongNhomhang = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenNhomhang = new DevExpress.XtraEditors.SimpleButton();
            this.pnlNghiepvu = new DevExpress.XtraEditors.PanelControl();
            this.pn_listProduct = new DevExpress.XtraEditors.PanelControl();
            this.gr_SelectProduct = new DevExpress.XtraGrid.GridControl();
            this.gv_SelectProduct = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SOTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.GIABAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Thanh_Tien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_BEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnUpDowGv = new DevExpress.XtraEditors.PanelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.lb_RowsCount = new DevExpress.XtraEditors.LabelControl();
            this.lb_STT = new DevExpress.XtraEditors.LabelControl();
            this.btn_up = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Dow = new DevExpress.XtraEditors.SimpleButton();
            this.btn_DowLast = new DevExpress.XtraEditors.SimpleButton();
            this.btn_UpFirst = new DevExpress.XtraEditors.SimpleButton();
            this.pnBtnDeleteGv = new DevExpress.XtraEditors.PanelControl();
            this.btn_cong = new DevExpress.XtraEditors.SimpleButton();
            this.btn_xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btn_tru = new DevExpress.XtraEditors.SimpleButton();
            this.btn_XoaTatCa = new DevExpress.XtraEditors.SimpleButton();
            this.pnThuNgan = new DevExpress.XtraEditors.PanelControl();
            this.btnBanggia = new DevExpress.XtraEditors.SimpleButton();
            this.btnbanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanve = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoket = new DevExpress.XtraEditors.SimpleButton();
            this.btnKetca = new DevExpress.XtraEditors.SimpleButton();
            this.btnSoatve = new DevExpress.XtraEditors.SimpleButton();
            this.btnTinhtienvetheothoigian = new DevExpress.XtraEditors.SimpleButton();
            this.btnInlaive = new DevExpress.XtraEditors.SimpleButton();
            this.btnThoat = new DevExpress.XtraEditors.SimpleButton();
            this.btnHuyve = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl13 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl65 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl66 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl67 = new DevExpress.XtraEditors.PanelControl();
            this.lb_KhoQuay = new DevExpress.XtraEditors.LabelControl();
            this.lbgiay = new DevExpress.XtraEditors.LabelControl();
            this.lbCaBan = new DevExpress.XtraEditors.LabelControl();
            this.lbGio = new DevExpress.XtraEditors.LabelControl();
            this.lb_NhanVien = new DevExpress.XtraEditors.LabelControl();
            this.lbtimeby = new DevExpress.XtraEditors.LabelControl();
            this.panelControl68 = new DevExpress.XtraEditors.PanelControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelControl69 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl70 = new DevExpress.XtraEditors.PanelControl();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTinhtiengio)).BeginInit();
            this.pnlTinhtiengio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl48)).BeginInit();
            this.panelControl48.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl49)).BeginInit();
            this.panelControl49.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongtien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl50)).BeginInit();
            this.panelControl50.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl36)).BeginInit();
            this.panelControl36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl43)).BeginInit();
            this.panelControl43.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtThoigian.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl44)).BeginInit();
            this.panelControl44.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl21)).BeginInit();
            this.panelControl21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl28)).BeginInit();
            this.panelControl28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiora.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl29)).BeginInit();
            this.panelControl29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl37)).BeginInit();
            this.panelControl37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl38)).BeginInit();
            this.panelControl38.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiovao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl39)).BeginInit();
            this.panelControl39.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl45)).BeginInit();
            this.panelControl45.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl46)).BeginInit();
            this.panelControl46.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiave.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl47)).BeginInit();
            this.panelControl47.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl52)).BeginInit();
            this.panelControl52.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl54)).BeginInit();
            this.panelControl54.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtThoigianquydoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl55)).BeginInit();
            this.panelControl55.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl51)).BeginInit();
            this.panelControl51.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl533)).BeginInit();
            this.panelControl533.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrochoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl53)).BeginInit();
            this.panelControl53.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl40)).BeginInit();
            this.panelControl40.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl41)).BeginInit();
            this.panelControl41.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaveTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl42)).BeginInit();
            this.panelControl42.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBanve)).BeginInit();
            this.pnlBanve.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl58)).BeginInit();
            this.panelControl58.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl57)).BeginInit();
            this.panelControl57.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdTiente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvtiente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl59)).BeginInit();
            this.panelControl59.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl62)).BeginInit();
            this.panelControl62.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMakhachhang)).BeginInit();
            this.pnlMakhachhang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl60)).BeginInit();
            this.panelControl60.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMakhachhang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl61)).BeginInit();
            this.panelControl61.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).BeginInit();
            this.panelControl16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).BeginInit();
            this.panelControl17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienThoiLai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).BeginInit();
            this.panelControl18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlQuydoi)).BeginInit();
            this.pnlQuydoi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl63)).BeginInit();
            this.panelControl63.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienquydoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl64)).BeginInit();
            this.panelControl64.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            this.panelControl15.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienKhachTra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).BeginInit();
            this.panelControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienve.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSoatve)).BeginInit();
            this.pnlSoatve.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).BeginInit();
            this.panelControl22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl26)).BeginInit();
            this.panelControl26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtThongtinve.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl27)).BeginInit();
            this.panelControl27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl30)).BeginInit();
            this.panelControl30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl31)).BeginInit();
            this.panelControl31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrangthai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl32)).BeginInit();
            this.panelControl32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl33)).BeginInit();
            this.panelControl33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl34)).BeginInit();
            this.panelControl34.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMave.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl35)).BeginInit();
            this.panelControl35.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKetca)).BeginInit();
            this.pnlKetca.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdKetca)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvKetca)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl23)).BeginInit();
            this.panelControl23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl24)).BeginInit();
            this.panelControl24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtKetca.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl25)).BeginInit();
            this.panelControl25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl20)).BeginInit();
            this.panelControl20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHuyve)).BeginInit();
            this.pnlHuyve.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLYDO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMA_VEBAN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInlaive)).BeginInit();
            this.pnlInlaive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdInlaive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvInlaive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlLoaddanhmuc)).BeginInit();
            this.pnlLoaddanhmuc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTrochoi)).BeginInit();
            this.pnlTrochoi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlLoaitrochoi)).BeginInit();
            this.pnlLoaitrochoi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlNghiepvu)).BeginInit();
            this.pnlNghiepvu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_listProduct)).BeginInit();
            this.pn_listProduct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_SelectProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_SelectProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDowGv)).BeginInit();
            this.pnUpDowGv.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnBtnDeleteGv)).BeginInit();
            this.pnBtnDeleteGv.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnThuNgan)).BeginInit();
            this.pnThuNgan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).BeginInit();
            this.panelControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl65)).BeginInit();
            this.panelControl65.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl66)).BeginInit();
            this.panelControl66.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl67)).BeginInit();
            this.panelControl67.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl68)).BeginInit();
            this.panelControl68.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl69)).BeginInit();
            this.panelControl69.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl70)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pnlTinhtiengio);
            this.panelControl1.Controls.Add(this.pnlBanve);
            this.panelControl1.Controls.Add(this.pnlSoatve);
            this.panelControl1.Controls.Add(this.pnlKetca);
            this.panelControl1.Controls.Add(this.pnlHuyve);
            this.panelControl1.Controls.Add(this.pnlInlaive);
            this.panelControl1.Controls.Add(this.pnlLoaddanhmuc);
            this.panelControl1.Controls.Add(this.pnlNghiepvu);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 70);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1036, 616);
            this.panelControl1.TabIndex = 0;
            // 
            // pnlTinhtiengio
            // 
            this.pnlTinhtiengio.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlTinhtiengio.Controls.Add(this.btnThanhtoan);
            this.pnlTinhtiengio.Controls.Add(this.panelControl48);
            this.pnlTinhtiengio.Controls.Add(this.panelControl36);
            this.pnlTinhtiengio.Controls.Add(this.panelControl21);
            this.pnlTinhtiengio.Controls.Add(this.panelControl37);
            this.pnlTinhtiengio.Controls.Add(this.panelControl45);
            this.pnlTinhtiengio.Controls.Add(this.panelControl52);
            this.pnlTinhtiengio.Controls.Add(this.panelControl51);
            this.pnlTinhtiengio.Controls.Add(this.panelControl40);
            this.pnlTinhtiengio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTinhtiengio.Location = new System.Drawing.Point(2, 2);
            this.pnlTinhtiengio.Name = "pnlTinhtiengio";
            this.pnlTinhtiengio.Size = new System.Drawing.Size(682, 612);
            this.pnlTinhtiengio.TabIndex = 8;
            this.pnlTinhtiengio.Visible = false;
            // 
            // btnThanhtoan
            // 
            this.btnThanhtoan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnThanhtoan.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnThanhtoan.Appearance.Options.UseFont = true;
            this.btnThanhtoan.Location = new System.Drawing.Point(570, 471);
            this.btnThanhtoan.Name = "btnThanhtoan";
            this.btnThanhtoan.Size = new System.Drawing.Size(110, 44);
            this.btnThanhtoan.TabIndex = 43;
            this.btnThanhtoan.Text = "Thanh toán";
            this.btnThanhtoan.Click += new System.EventHandler(this.btnThanhtoan_Click);
            // 
            // panelControl48
            // 
            this.panelControl48.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl48.Controls.Add(this.panelControl49);
            this.panelControl48.Controls.Add(this.panelControl50);
            this.panelControl48.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl48.Location = new System.Drawing.Point(0, 416);
            this.panelControl48.Name = "panelControl48";
            this.panelControl48.Size = new System.Drawing.Size(682, 52);
            this.panelControl48.TabIndex = 42;
            // 
            // panelControl49
            // 
            this.panelControl49.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl49.Controls.Add(this.txtTongtien);
            this.panelControl49.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl49.Location = new System.Drawing.Point(156, 0);
            this.panelControl49.Name = "panelControl49";
            this.panelControl49.Size = new System.Drawing.Size(526, 52);
            this.panelControl49.TabIndex = 18;
            // 
            // txtTongtien
            // 
            this.txtTongtien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTongtien.EnterMoveNextControl = true;
            this.txtTongtien.Location = new System.Drawing.Point(0, 0);
            this.txtTongtien.Name = "txtTongtien";
            this.txtTongtien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongtien.Properties.Appearance.Options.UseFont = true;
            this.txtTongtien.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTongtien.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTongtien.Properties.DisplayFormat.FormatString = "N0";
            this.txtTongtien.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTongtien.Properties.EditFormat.FormatString = "N0";
            this.txtTongtien.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTongtien.Properties.ReadOnly = true;
            this.txtTongtien.Size = new System.Drawing.Size(526, 48);
            this.txtTongtien.TabIndex = 4;
            this.txtTongtien.Tag = "1";
            // 
            // panelControl50
            // 
            this.panelControl50.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl50.Controls.Add(this.labelControl16);
            this.panelControl50.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl50.Location = new System.Drawing.Point(0, 0);
            this.panelControl50.Name = "panelControl50";
            this.panelControl50.Size = new System.Drawing.Size(156, 52);
            this.panelControl50.TabIndex = 17;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl16.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl16.Location = new System.Drawing.Point(10, 18);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(78, 19);
            this.labelControl16.TabIndex = 15;
            this.labelControl16.Text = "Tổng tiền";
            // 
            // panelControl36
            // 
            this.panelControl36.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl36.Controls.Add(this.panelControl43);
            this.panelControl36.Controls.Add(this.panelControl44);
            this.panelControl36.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl36.Location = new System.Drawing.Point(0, 364);
            this.panelControl36.Name = "panelControl36";
            this.panelControl36.Size = new System.Drawing.Size(682, 52);
            this.panelControl36.TabIndex = 40;
            // 
            // panelControl43
            // 
            this.panelControl43.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl43.Controls.Add(this.txtThoigian);
            this.panelControl43.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl43.Location = new System.Drawing.Point(156, 0);
            this.panelControl43.Name = "panelControl43";
            this.panelControl43.Size = new System.Drawing.Size(526, 52);
            this.panelControl43.TabIndex = 18;
            // 
            // txtThoigian
            // 
            this.txtThoigian.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtThoigian.EnterMoveNextControl = true;
            this.txtThoigian.Location = new System.Drawing.Point(0, 0);
            this.txtThoigian.Name = "txtThoigian";
            this.txtThoigian.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThoigian.Properties.Appearance.Options.UseFont = true;
            this.txtThoigian.Properties.Appearance.Options.UseTextOptions = true;
            this.txtThoigian.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtThoigian.Properties.ReadOnly = true;
            this.txtThoigian.Size = new System.Drawing.Size(526, 48);
            this.txtThoigian.TabIndex = 4;
            // 
            // panelControl44
            // 
            this.panelControl44.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl44.Controls.Add(this.labelControl14);
            this.panelControl44.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl44.Location = new System.Drawing.Point(0, 0);
            this.panelControl44.Name = "panelControl44";
            this.panelControl44.Size = new System.Drawing.Size(156, 52);
            this.panelControl44.TabIndex = 17;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl14.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl14.Location = new System.Drawing.Point(10, 18);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(76, 19);
            this.labelControl14.TabIndex = 15;
            this.labelControl14.Text = "Thời gian";
            // 
            // panelControl21
            // 
            this.panelControl21.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl21.Controls.Add(this.panelControl28);
            this.panelControl21.Controls.Add(this.panelControl29);
            this.panelControl21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl21.Location = new System.Drawing.Point(0, 312);
            this.panelControl21.Name = "panelControl21";
            this.panelControl21.Size = new System.Drawing.Size(682, 52);
            this.panelControl21.TabIndex = 39;
            // 
            // panelControl28
            // 
            this.panelControl28.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl28.Controls.Add(this.txtGiora);
            this.panelControl28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl28.Location = new System.Drawing.Point(156, 0);
            this.panelControl28.Name = "panelControl28";
            this.panelControl28.Size = new System.Drawing.Size(526, 52);
            this.panelControl28.TabIndex = 18;
            // 
            // txtGiora
            // 
            this.txtGiora.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGiora.EnterMoveNextControl = true;
            this.txtGiora.Location = new System.Drawing.Point(0, 0);
            this.txtGiora.Name = "txtGiora";
            this.txtGiora.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiora.Properties.Appearance.Options.UseFont = true;
            this.txtGiora.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGiora.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGiora.Properties.ReadOnly = true;
            this.txtGiora.Size = new System.Drawing.Size(526, 48);
            this.txtGiora.TabIndex = 4;
            // 
            // panelControl29
            // 
            this.panelControl29.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl29.Controls.Add(this.labelControl11);
            this.panelControl29.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl29.Location = new System.Drawing.Point(0, 0);
            this.panelControl29.Name = "panelControl29";
            this.panelControl29.Size = new System.Drawing.Size(156, 52);
            this.panelControl29.TabIndex = 17;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl11.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl11.Location = new System.Drawing.Point(10, 18);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(49, 19);
            this.labelControl11.TabIndex = 15;
            this.labelControl11.Text = "Giờ ra";
            // 
            // panelControl37
            // 
            this.panelControl37.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl37.Controls.Add(this.panelControl38);
            this.panelControl37.Controls.Add(this.panelControl39);
            this.panelControl37.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl37.Location = new System.Drawing.Point(0, 260);
            this.panelControl37.Name = "panelControl37";
            this.panelControl37.Size = new System.Drawing.Size(682, 52);
            this.panelControl37.TabIndex = 38;
            // 
            // panelControl38
            // 
            this.panelControl38.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl38.Controls.Add(this.txtGiovao);
            this.panelControl38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl38.Location = new System.Drawing.Point(156, 0);
            this.panelControl38.Name = "panelControl38";
            this.panelControl38.Size = new System.Drawing.Size(526, 52);
            this.panelControl38.TabIndex = 18;
            // 
            // txtGiovao
            // 
            this.txtGiovao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGiovao.EnterMoveNextControl = true;
            this.txtGiovao.Location = new System.Drawing.Point(0, 0);
            this.txtGiovao.Name = "txtGiovao";
            this.txtGiovao.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiovao.Properties.Appearance.Options.UseFont = true;
            this.txtGiovao.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGiovao.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGiovao.Properties.ReadOnly = true;
            this.txtGiovao.Size = new System.Drawing.Size(526, 48);
            this.txtGiovao.TabIndex = 4;
            // 
            // panelControl39
            // 
            this.panelControl39.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl39.Controls.Add(this.labelControl12);
            this.panelControl39.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl39.Location = new System.Drawing.Point(0, 0);
            this.panelControl39.Name = "panelControl39";
            this.panelControl39.Size = new System.Drawing.Size(156, 52);
            this.panelControl39.TabIndex = 17;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl12.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl12.Location = new System.Drawing.Point(10, 18);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(61, 19);
            this.labelControl12.TabIndex = 15;
            this.labelControl12.Text = "Giờ vào";
            // 
            // panelControl45
            // 
            this.panelControl45.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl45.Controls.Add(this.panelControl46);
            this.panelControl45.Controls.Add(this.panelControl47);
            this.panelControl45.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl45.Location = new System.Drawing.Point(0, 156);
            this.panelControl45.Name = "panelControl45";
            this.panelControl45.Size = new System.Drawing.Size(682, 104);
            this.panelControl45.TabIndex = 41;
            // 
            // panelControl46
            // 
            this.panelControl46.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl46.Controls.Add(this.txtGiave);
            this.panelControl46.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl46.Location = new System.Drawing.Point(156, 0);
            this.panelControl46.Name = "panelControl46";
            this.panelControl46.Size = new System.Drawing.Size(526, 104);
            this.panelControl46.TabIndex = 18;
            // 
            // txtGiave
            // 
            this.txtGiave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGiave.EnterMoveNextControl = true;
            this.txtGiave.Location = new System.Drawing.Point(0, 0);
            this.txtGiave.Name = "txtGiave";
            this.txtGiave.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiave.Properties.Appearance.Options.UseFont = true;
            this.txtGiave.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGiave.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGiave.Properties.DisplayFormat.FormatString = "N0";
            this.txtGiave.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGiave.Properties.EditFormat.FormatString = "N0";
            this.txtGiave.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGiave.Properties.ReadOnly = true;
            this.txtGiave.Size = new System.Drawing.Size(526, 48);
            this.txtGiave.TabIndex = 4;
            // 
            // panelControl47
            // 
            this.panelControl47.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl47.Controls.Add(this.labelControl15);
            this.panelControl47.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl47.Location = new System.Drawing.Point(0, 0);
            this.panelControl47.Name = "panelControl47";
            this.panelControl47.Size = new System.Drawing.Size(156, 104);
            this.panelControl47.TabIndex = 17;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl15.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl15.Location = new System.Drawing.Point(10, 18);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(104, 19);
            this.labelControl15.TabIndex = 15;
            this.labelControl15.Text = "Giá vé/Block";
            // 
            // panelControl52
            // 
            this.panelControl52.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl52.Controls.Add(this.panelControl54);
            this.panelControl52.Controls.Add(this.panelControl55);
            this.panelControl52.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl52.Location = new System.Drawing.Point(0, 104);
            this.panelControl52.Name = "panelControl52";
            this.panelControl52.Size = new System.Drawing.Size(682, 52);
            this.panelControl52.TabIndex = 45;
            // 
            // panelControl54
            // 
            this.panelControl54.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl54.Controls.Add(this.txtThoigianquydoi);
            this.panelControl54.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl54.Location = new System.Drawing.Point(156, 0);
            this.panelControl54.Name = "panelControl54";
            this.panelControl54.Size = new System.Drawing.Size(526, 52);
            this.panelControl54.TabIndex = 18;
            // 
            // txtThoigianquydoi
            // 
            this.txtThoigianquydoi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtThoigianquydoi.EnterMoveNextControl = true;
            this.txtThoigianquydoi.Location = new System.Drawing.Point(0, 0);
            this.txtThoigianquydoi.Name = "txtThoigianquydoi";
            this.txtThoigianquydoi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThoigianquydoi.Properties.Appearance.Options.UseFont = true;
            this.txtThoigianquydoi.Properties.Appearance.Options.UseTextOptions = true;
            this.txtThoigianquydoi.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtThoigianquydoi.Properties.DisplayFormat.FormatString = "N0";
            this.txtThoigianquydoi.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtThoigianquydoi.Properties.EditFormat.FormatString = "N0";
            this.txtThoigianquydoi.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtThoigianquydoi.Properties.ReadOnly = true;
            this.txtThoigianquydoi.Size = new System.Drawing.Size(526, 48);
            this.txtThoigianquydoi.TabIndex = 4;
            // 
            // panelControl55
            // 
            this.panelControl55.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl55.Controls.Add(this.labelControl19);
            this.panelControl55.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl55.Location = new System.Drawing.Point(0, 0);
            this.panelControl55.Name = "panelControl55";
            this.panelControl55.Size = new System.Drawing.Size(156, 52);
            this.panelControl55.TabIndex = 17;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl19.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl19.Location = new System.Drawing.Point(10, 18);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(140, 19);
            this.labelControl19.TabIndex = 15;
            this.labelControl19.Text = "Thời gian quy đổi";
            // 
            // panelControl51
            // 
            this.panelControl51.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl51.Controls.Add(this.panelControl533);
            this.panelControl51.Controls.Add(this.panelControl53);
            this.panelControl51.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl51.Location = new System.Drawing.Point(0, 52);
            this.panelControl51.Name = "panelControl51";
            this.panelControl51.Size = new System.Drawing.Size(682, 52);
            this.panelControl51.TabIndex = 44;
            // 
            // panelControl533
            // 
            this.panelControl533.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl533.Controls.Add(this.txtTrochoi);
            this.panelControl533.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl533.Location = new System.Drawing.Point(156, 0);
            this.panelControl533.Name = "panelControl533";
            this.panelControl533.Size = new System.Drawing.Size(526, 52);
            this.panelControl533.TabIndex = 18;
            // 
            // txtTrochoi
            // 
            this.txtTrochoi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTrochoi.Location = new System.Drawing.Point(0, 0);
            this.txtTrochoi.Name = "txtTrochoi";
            this.txtTrochoi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrochoi.Properties.Appearance.Options.UseFont = true;
            this.txtTrochoi.Properties.ReadOnly = true;
            this.txtTrochoi.Size = new System.Drawing.Size(526, 48);
            this.txtTrochoi.TabIndex = 4;
            this.txtTrochoi.Tag = "0";
            // 
            // panelControl53
            // 
            this.panelControl53.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl53.Controls.Add(this.labelControl18);
            this.panelControl53.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl53.Location = new System.Drawing.Point(0, 0);
            this.panelControl53.Name = "panelControl53";
            this.panelControl53.Size = new System.Drawing.Size(156, 52);
            this.panelControl53.TabIndex = 17;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl18.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl18.Location = new System.Drawing.Point(10, 18);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(66, 19);
            this.labelControl18.TabIndex = 15;
            this.labelControl18.Text = "Trò chơi";
            // 
            // panelControl40
            // 
            this.panelControl40.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl40.Controls.Add(this.panelControl41);
            this.panelControl40.Controls.Add(this.panelControl42);
            this.panelControl40.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl40.Location = new System.Drawing.Point(0, 0);
            this.panelControl40.Name = "panelControl40";
            this.panelControl40.Size = new System.Drawing.Size(682, 52);
            this.panelControl40.TabIndex = 37;
            // 
            // panelControl41
            // 
            this.panelControl41.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl41.Controls.Add(this.txtMaveTT);
            this.panelControl41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl41.Location = new System.Drawing.Point(156, 0);
            this.panelControl41.Name = "panelControl41";
            this.panelControl41.Size = new System.Drawing.Size(526, 52);
            this.panelControl41.TabIndex = 18;
            // 
            // txtMaveTT
            // 
            this.txtMaveTT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMaveTT.Location = new System.Drawing.Point(0, 0);
            this.txtMaveTT.Name = "txtMaveTT";
            this.txtMaveTT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaveTT.Properties.Appearance.Options.UseFont = true;
            this.txtMaveTT.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMaveTT.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMaveTT.Size = new System.Drawing.Size(526, 48);
            this.txtMaveTT.TabIndex = 4;
            this.txtMaveTT.Tag = "0";
            this.txtMaveTT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMaveTT_KeyDown);
            // 
            // panelControl42
            // 
            this.panelControl42.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl42.Controls.Add(this.labelControl13);
            this.panelControl42.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl42.Location = new System.Drawing.Point(0, 0);
            this.panelControl42.Name = "panelControl42";
            this.panelControl42.Size = new System.Drawing.Size(156, 52);
            this.panelControl42.TabIndex = 17;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl13.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl13.Location = new System.Drawing.Point(10, 18);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(48, 19);
            this.labelControl13.TabIndex = 15;
            this.labelControl13.Text = "Mã vé";
            // 
            // pnlBanve
            // 
            this.pnlBanve.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlBanve.Controls.Add(this.panelControl58);
            this.pnlBanve.Controls.Add(this.pnlMakhachhang);
            this.pnlBanve.Controls.Add(this.panelControl16);
            this.pnlBanve.Controls.Add(this.pnlQuydoi);
            this.pnlBanve.Controls.Add(this.panelControl15);
            this.pnlBanve.Controls.Add(this.panelControl11);
            this.pnlBanve.Controls.Add(this.panelControl8);
            this.pnlBanve.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBanve.Location = new System.Drawing.Point(2, 2);
            this.pnlBanve.Name = "pnlBanve";
            this.pnlBanve.Size = new System.Drawing.Size(682, 612);
            this.pnlBanve.TabIndex = 3;
            this.pnlBanve.Visible = false;
            // 
            // panelControl58
            // 
            this.panelControl58.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl58.Controls.Add(this.panelControl57);
            this.panelControl58.Controls.Add(this.panelControl62);
            this.panelControl58.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl58.Location = new System.Drawing.Point(0, 413);
            this.panelControl58.Name = "panelControl58";
            this.panelControl58.Size = new System.Drawing.Size(682, 199);
            this.panelControl58.TabIndex = 44;
            // 
            // panelControl57
            // 
            this.panelControl57.Controls.Add(this.grdTiente);
            this.panelControl57.Controls.Add(this.panelControl59);
            this.panelControl57.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl57.Location = new System.Drawing.Point(0, 0);
            this.panelControl57.Name = "panelControl57";
            this.panelControl57.Size = new System.Drawing.Size(447, 199);
            this.panelControl57.TabIndex = 45;
            // 
            // grdTiente
            // 
            this.grdTiente.AllowDrop = true;
            this.grdTiente.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.grdTiente.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.grdTiente.Location = new System.Drawing.Point(2, 2);
            this.grdTiente.MainView = this.grvtiente;
            this.grdTiente.Name = "grdTiente";
            this.grdTiente.Size = new System.Drawing.Size(408, 195);
            this.grdTiente.TabIndex = 43;
            this.grdTiente.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvtiente});
            // 
            // grvtiente
            // 
            this.grvtiente.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.grvtiente.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Maroon;
            this.grvtiente.Appearance.FocusedRow.Options.UseFont = true;
            this.grvtiente.Appearance.FocusedRow.Options.UseForeColor = true;
            this.grvtiente.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvtiente.Appearance.FooterPanel.Options.UseFont = true;
            this.grvtiente.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grvtiente.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grvtiente.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvtiente.Appearance.GroupFooter.Options.UseFont = true;
            this.grvtiente.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grvtiente.Appearance.Row.Options.UseFont = true;
            this.grvtiente.ColumnPanelRowHeight = 30;
            this.grvtiente.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STTTT,
            this.MA_TIENTE,
            this.TEN_TIENTE,
            this.TYGIA});
            this.grvtiente.FooterPanelHeight = 30;
            this.grvtiente.GridControl = this.grdTiente;
            this.grvtiente.Name = "grvtiente";
            this.grvtiente.OptionsNavigation.EnterMoveNextColumn = true;
            this.grvtiente.OptionsView.EnableAppearanceEvenRow = true;
            this.grvtiente.OptionsView.ShowGroupPanel = false;
            this.grvtiente.OptionsView.ShowIndicator = false;
            this.grvtiente.RowHeight = 35;
            this.grvtiente.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.None;
            this.grvtiente.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.grvtiente.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.grvtiente.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.grvtiente_RowCellClick);
            this.grvtiente.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.grvtiente_CustomDrawCell);
            // 
            // STTTT
            // 
            this.STTTT.AppearanceCell.Options.UseTextOptions = true;
            this.STTTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.STTTT.AppearanceHeader.Options.UseFont = true;
            this.STTTT.AppearanceHeader.Options.UseTextOptions = true;
            this.STTTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTTT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STTTT.Caption = "STT";
            this.STTTT.Name = "STTTT";
            this.STTTT.OptionsColumn.AllowEdit = false;
            this.STTTT.OptionsColumn.AllowFocus = false;
            this.STTTT.OptionsColumn.AllowMove = false;
            this.STTTT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STTTT.Visible = true;
            this.STTTT.VisibleIndex = 0;
            this.STTTT.Width = 43;
            // 
            // MA_TIENTE
            // 
            this.MA_TIENTE.AppearanceCell.Options.UseTextOptions = true;
            this.MA_TIENTE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.MA_TIENTE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.MA_TIENTE.AppearanceHeader.Options.UseFont = true;
            this.MA_TIENTE.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_TIENTE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_TIENTE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_TIENTE.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA_TIENTE.Caption = "Mã tiền tệ";
            this.MA_TIENTE.FieldName = "MA_TIENTE";
            this.MA_TIENTE.Name = "MA_TIENTE";
            this.MA_TIENTE.OptionsColumn.AllowEdit = false;
            this.MA_TIENTE.OptionsColumn.AllowFocus = false;
            this.MA_TIENTE.OptionsColumn.AllowMove = false;
            this.MA_TIENTE.Visible = true;
            this.MA_TIENTE.VisibleIndex = 1;
            this.MA_TIENTE.Width = 100;
            // 
            // TEN_TIENTE
            // 
            this.TEN_TIENTE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TEN_TIENTE.AppearanceHeader.Options.UseFont = true;
            this.TEN_TIENTE.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_TIENTE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_TIENTE.Caption = "Tên tiền tệ";
            this.TEN_TIENTE.FieldName = "TEN_TIENTE";
            this.TEN_TIENTE.Name = "TEN_TIENTE";
            this.TEN_TIENTE.OptionsColumn.AllowEdit = false;
            this.TEN_TIENTE.OptionsColumn.AllowFocus = false;
            this.TEN_TIENTE.Visible = true;
            this.TEN_TIENTE.VisibleIndex = 2;
            this.TEN_TIENTE.Width = 180;
            // 
            // TYGIA
            // 
            this.TYGIA.AppearanceCell.Options.UseTextOptions = true;
            this.TYGIA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TYGIA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TYGIA.AppearanceHeader.Options.UseFont = true;
            this.TYGIA.AppearanceHeader.Options.UseTextOptions = true;
            this.TYGIA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TYGIA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TYGIA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TYGIA.Caption = "Tỷ giá";
            this.TYGIA.DisplayFormat.FormatString = "N0";
            this.TYGIA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TYGIA.FieldName = "TYGIA";
            this.TYGIA.Name = "TYGIA";
            this.TYGIA.OptionsColumn.AllowEdit = false;
            this.TYGIA.OptionsColumn.AllowFocus = false;
            this.TYGIA.OptionsColumn.AllowMove = false;
            this.TYGIA.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.TYGIA.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DONGIA", "{0:#,###0}")});
            this.TYGIA.Visible = true;
            this.TYGIA.VisibleIndex = 3;
            this.TYGIA.Width = 100;
            // 
            // panelControl59
            // 
            this.panelControl59.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl59.Controls.Add(this.btnXuongtiente);
            this.panelControl59.Controls.Add(this.btnLentiente);
            this.panelControl59.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl59.Location = new System.Drawing.Point(410, 2);
            this.panelControl59.Name = "panelControl59";
            this.panelControl59.Size = new System.Drawing.Size(35, 195);
            this.panelControl59.TabIndex = 42;
            // 
            // btnXuongtiente
            // 
            this.btnXuongtiente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXuongtiente.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongtiente.Image")));
            this.btnXuongtiente.Location = new System.Drawing.Point(2, 128);
            this.btnXuongtiente.Name = "btnXuongtiente";
            this.btnXuongtiente.Size = new System.Drawing.Size(32, 67);
            this.btnXuongtiente.TabIndex = 6;
            this.btnXuongtiente.Click += new System.EventHandler(this.btnXuongtiente_Click);
            // 
            // btnLentiente
            // 
            this.btnLentiente.Image = ((System.Drawing.Image)(resources.GetObject("btnLentiente.Image")));
            this.btnLentiente.Location = new System.Drawing.Point(2, 1);
            this.btnLentiente.Name = "btnLentiente";
            this.btnLentiente.Size = new System.Drawing.Size(31, 67);
            this.btnLentiente.TabIndex = 5;
            this.btnLentiente.Text = "simpleButton7";
            this.btnLentiente.Click += new System.EventHandler(this.btnLentiente_Click);
            // 
            // panelControl62
            // 
            this.panelControl62.Controls.Add(this.btnNhaplaitienkhachtra);
            this.panelControl62.Controls.Add(this.btnTTTNB);
            this.panelControl62.Controls.Add(this.btnInve);
            this.panelControl62.Controls.Add(this.btnKHTT);
            this.panelControl62.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl62.Location = new System.Drawing.Point(447, 0);
            this.panelControl62.Name = "panelControl62";
            this.panelControl62.Size = new System.Drawing.Size(235, 199);
            this.panelControl62.TabIndex = 44;
            // 
            // btnNhaplaitienkhachtra
            // 
            this.btnNhaplaitienkhachtra.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNhaplaitienkhachtra.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnNhaplaitienkhachtra.Appearance.Options.UseFont = true;
            this.btnNhaplaitienkhachtra.Appearance.Options.UseTextOptions = true;
            this.btnNhaplaitienkhachtra.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnNhaplaitienkhachtra.Location = new System.Drawing.Point(122, 70);
            this.btnNhaplaitienkhachtra.Name = "btnNhaplaitienkhachtra";
            this.btnNhaplaitienkhachtra.Size = new System.Drawing.Size(110, 127);
            this.btnNhaplaitienkhachtra.TabIndex = 36;
            this.btnNhaplaitienkhachtra.Text = "Nhập lại tiền khách trả";
            this.btnNhaplaitienkhachtra.Click += new System.EventHandler(this.btnNhaplaitienkhachtra_Click);
            // 
            // btnTTTNB
            // 
            this.btnTTTNB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTTTNB.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnTTTNB.Appearance.Options.UseFont = true;
            this.btnTTTNB.Appearance.Options.UseTextOptions = true;
            this.btnTTTNB.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnTTTNB.Location = new System.Drawing.Point(122, 2);
            this.btnTTTNB.Name = "btnTTTNB";
            this.btnTTTNB.Size = new System.Drawing.Size(110, 62);
            this.btnTTTNB.TabIndex = 38;
            this.btnTTTNB.Text = "Thẻ thanh toán nội bộ";
            this.btnTTTNB.Click += new System.EventHandler(this.btnTTTNB_Click);
            // 
            // btnInve
            // 
            this.btnInve.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInve.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnInve.Appearance.Options.UseFont = true;
            this.btnInve.Appearance.Options.UseTextOptions = true;
            this.btnInve.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnInve.Location = new System.Drawing.Point(6, 70);
            this.btnInve.Name = "btnInve";
            this.btnInve.Size = new System.Drawing.Size(110, 127);
            this.btnInve.TabIndex = 11;
            this.btnInve.Text = "In vé";
            this.btnInve.Click += new System.EventHandler(this.btnInve_Click);
            // 
            // btnKHTT
            // 
            this.btnKHTT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKHTT.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnKHTT.Appearance.Options.UseFont = true;
            this.btnKHTT.Appearance.Options.UseTextOptions = true;
            this.btnKHTT.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnKHTT.Location = new System.Drawing.Point(6, 2);
            this.btnKHTT.Name = "btnKHTT";
            this.btnKHTT.Size = new System.Drawing.Size(110, 62);
            this.btnKHTT.TabIndex = 37;
            this.btnKHTT.Text = "Khách hàng thân thiết";
            this.btnKHTT.Click += new System.EventHandler(this.btnKHTT_Click);
            // 
            // pnlMakhachhang
            // 
            this.pnlMakhachhang.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlMakhachhang.Controls.Add(this.panelControl60);
            this.pnlMakhachhang.Controls.Add(this.panelControl61);
            this.pnlMakhachhang.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMakhachhang.Location = new System.Drawing.Point(0, 348);
            this.pnlMakhachhang.Name = "pnlMakhachhang";
            this.pnlMakhachhang.Size = new System.Drawing.Size(682, 52);
            this.pnlMakhachhang.TabIndex = 42;
            this.pnlMakhachhang.Visible = false;
            // 
            // panelControl60
            // 
            this.panelControl60.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl60.Controls.Add(this.txtMakhachhang);
            this.panelControl60.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl60.Location = new System.Drawing.Point(138, 0);
            this.panelControl60.Name = "panelControl60";
            this.panelControl60.Size = new System.Drawing.Size(544, 52);
            this.panelControl60.TabIndex = 18;
            // 
            // txtMakhachhang
            // 
            this.txtMakhachhang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMakhachhang.Location = new System.Drawing.Point(0, 0);
            this.txtMakhachhang.Name = "txtMakhachhang";
            this.txtMakhachhang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMakhachhang.Properties.Appearance.Options.UseFont = true;
            this.txtMakhachhang.Properties.Mask.EditMask = "d";
            this.txtMakhachhang.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtMakhachhang.Size = new System.Drawing.Size(544, 48);
            this.txtMakhachhang.TabIndex = 4;
            this.txtMakhachhang.Tag = "0";
            // 
            // panelControl61
            // 
            this.panelControl61.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl61.Controls.Add(this.labelControl21);
            this.panelControl61.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl61.Location = new System.Drawing.Point(0, 0);
            this.panelControl61.Name = "panelControl61";
            this.panelControl61.Size = new System.Drawing.Size(138, 52);
            this.panelControl61.TabIndex = 17;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl21.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl21.Location = new System.Drawing.Point(10, 18);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(122, 19);
            this.labelControl21.TabIndex = 15;
            this.labelControl21.Text = "Mã khách hàng";
            // 
            // panelControl16
            // 
            this.panelControl16.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl16.Controls.Add(this.panelControl17);
            this.panelControl16.Controls.Add(this.panelControl18);
            this.panelControl16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl16.Location = new System.Drawing.Point(0, 296);
            this.panelControl16.Name = "panelControl16";
            this.panelControl16.Size = new System.Drawing.Size(682, 52);
            this.panelControl16.TabIndex = 40;
            // 
            // panelControl17
            // 
            this.panelControl17.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl17.Controls.Add(this.txtTienThoiLai);
            this.panelControl17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl17.Location = new System.Drawing.Point(138, 0);
            this.panelControl17.Name = "panelControl17";
            this.panelControl17.Size = new System.Drawing.Size(544, 52);
            this.panelControl17.TabIndex = 18;
            // 
            // txtTienThoiLai
            // 
            this.txtTienThoiLai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTienThoiLai.EditValue = "0";
            this.txtTienThoiLai.EnterMoveNextControl = true;
            this.txtTienThoiLai.Location = new System.Drawing.Point(0, 0);
            this.txtTienThoiLai.Name = "txtTienThoiLai";
            this.txtTienThoiLai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThoiLai.Properties.Appearance.Options.UseFont = true;
            this.txtTienThoiLai.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienThoiLai.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienThoiLai.Properties.DisplayFormat.FormatString = "N0";
            this.txtTienThoiLai.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTienThoiLai.Properties.EditFormat.FormatString = "N0";
            this.txtTienThoiLai.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTienThoiLai.Properties.Mask.EditMask = "N0";
            this.txtTienThoiLai.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienThoiLai.Properties.ReadOnly = true;
            this.txtTienThoiLai.Size = new System.Drawing.Size(544, 48);
            this.txtTienThoiLai.TabIndex = 4;
            // 
            // panelControl18
            // 
            this.panelControl18.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl18.Controls.Add(this.labelControl8);
            this.panelControl18.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl18.Location = new System.Drawing.Point(0, 0);
            this.panelControl18.Name = "panelControl18";
            this.panelControl18.Size = new System.Drawing.Size(138, 52);
            this.panelControl18.TabIndex = 17;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl8.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl8.Location = new System.Drawing.Point(10, 18);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(98, 19);
            this.labelControl8.TabIndex = 15;
            this.labelControl8.Text = "Tiền thối lại";
            // 
            // pnlQuydoi
            // 
            this.pnlQuydoi.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlQuydoi.Controls.Add(this.panelControl63);
            this.pnlQuydoi.Controls.Add(this.panelControl64);
            this.pnlQuydoi.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlQuydoi.Location = new System.Drawing.Point(0, 244);
            this.pnlQuydoi.Name = "pnlQuydoi";
            this.pnlQuydoi.Size = new System.Drawing.Size(682, 52);
            this.pnlQuydoi.TabIndex = 45;
            this.pnlQuydoi.Visible = false;
            // 
            // panelControl63
            // 
            this.panelControl63.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl63.Controls.Add(this.txtTienquydoi);
            this.panelControl63.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl63.Location = new System.Drawing.Point(138, 0);
            this.panelControl63.Name = "panelControl63";
            this.panelControl63.Size = new System.Drawing.Size(544, 52);
            this.panelControl63.TabIndex = 18;
            // 
            // txtTienquydoi
            // 
            this.txtTienquydoi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTienquydoi.EditValue = "0";
            this.txtTienquydoi.EnterMoveNextControl = true;
            this.txtTienquydoi.Location = new System.Drawing.Point(0, 0);
            this.txtTienquydoi.Name = "txtTienquydoi";
            this.txtTienquydoi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienquydoi.Properties.Appearance.Options.UseFont = true;
            this.txtTienquydoi.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienquydoi.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienquydoi.Properties.DisplayFormat.FormatString = "N0";
            this.txtTienquydoi.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTienquydoi.Properties.EditFormat.FormatString = "N0";
            this.txtTienquydoi.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTienquydoi.Properties.Mask.EditMask = "N0";
            this.txtTienquydoi.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienquydoi.Properties.ReadOnly = true;
            this.txtTienquydoi.Size = new System.Drawing.Size(544, 48);
            this.txtTienquydoi.TabIndex = 4;
            this.txtTienquydoi.EditValueChanged += new System.EventHandler(this.txtTienquydoi_EditValueChanged);
            // 
            // panelControl64
            // 
            this.panelControl64.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl64.Controls.Add(this.lblTienquydoi);
            this.panelControl64.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl64.Location = new System.Drawing.Point(0, 0);
            this.panelControl64.Name = "panelControl64";
            this.panelControl64.Size = new System.Drawing.Size(138, 52);
            this.panelControl64.TabIndex = 17;
            // 
            // lblTienquydoi
            // 
            this.lblTienquydoi.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblTienquydoi.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lblTienquydoi.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lblTienquydoi.Location = new System.Drawing.Point(10, 7);
            this.lblTienquydoi.Name = "lblTienquydoi";
            this.lblTienquydoi.Size = new System.Drawing.Size(240, 19);
            this.lblTienquydoi.TabIndex = 15;
            this.lblTienquydoi.Text = "Quy đổi thành Việt Nam Đồng";
            // 
            // panelControl15
            // 
            this.panelControl15.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl15.Controls.Add(this.flowLayoutPanel1);
            this.panelControl15.Controls.Add(this.panelControl19);
            this.panelControl15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl15.Location = new System.Drawing.Point(0, 104);
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(682, 140);
            this.panelControl15.TabIndex = 39;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn5t);
            this.flowLayoutPanel1.Controls.Add(this.btn1k);
            this.flowLayoutPanel1.Controls.Add(this.btn2k);
            this.flowLayoutPanel1.Controls.Add(this.btn5k);
            this.flowLayoutPanel1.Controls.Add(this.btn10k);
            this.flowLayoutPanel1.Controls.Add(this.btn20k);
            this.flowLayoutPanel1.Controls.Add(this.btn50k);
            this.flowLayoutPanel1.Controls.Add(this.btn100k);
            this.flowLayoutPanel1.Controls.Add(this.btn200k);
            this.flowLayoutPanel1.Controls.Add(this.btn500k);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(138, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(544, 140);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn5t
            // 
            this.btn5t.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn5t.Appearance.Options.UseFont = true;
            this.btn5t.Location = new System.Drawing.Point(3, 3);
            this.btn5t.Name = "btn5t";
            this.btn5t.Size = new System.Drawing.Size(96, 60);
            this.btn5t.TabIndex = 35;
            this.btn5t.Text = "500";
            this.btn5t.Click += new System.EventHandler(this.btn5t_Click);
            // 
            // btn1k
            // 
            this.btn1k.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn1k.Appearance.Options.UseFont = true;
            this.btn1k.Location = new System.Drawing.Point(105, 3);
            this.btn1k.Name = "btn1k";
            this.btn1k.Size = new System.Drawing.Size(96, 60);
            this.btn1k.TabIndex = 32;
            this.btn1k.Text = "1,000";
            this.btn1k.Click += new System.EventHandler(this.btn1k_Click);
            // 
            // btn2k
            // 
            this.btn2k.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn2k.Appearance.Options.UseFont = true;
            this.btn2k.Location = new System.Drawing.Point(207, 3);
            this.btn2k.Name = "btn2k";
            this.btn2k.Size = new System.Drawing.Size(96, 60);
            this.btn2k.TabIndex = 33;
            this.btn2k.Text = "2,000";
            this.btn2k.Click += new System.EventHandler(this.btn2k_Click);
            // 
            // btn5k
            // 
            this.btn5k.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn5k.Appearance.Options.UseFont = true;
            this.btn5k.Location = new System.Drawing.Point(309, 3);
            this.btn5k.Name = "btn5k";
            this.btn5k.Size = new System.Drawing.Size(96, 60);
            this.btn5k.TabIndex = 34;
            this.btn5k.Text = "5,000";
            this.btn5k.Click += new System.EventHandler(this.btn5k_Click);
            // 
            // btn10k
            // 
            this.btn10k.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn10k.Appearance.Options.UseFont = true;
            this.btn10k.Location = new System.Drawing.Point(411, 3);
            this.btn10k.Name = "btn10k";
            this.btn10k.Size = new System.Drawing.Size(96, 60);
            this.btn10k.TabIndex = 26;
            this.btn10k.Text = "10,000";
            this.btn10k.Click += new System.EventHandler(this.btn10k_Click);
            // 
            // btn20k
            // 
            this.btn20k.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn20k.Appearance.Options.UseFont = true;
            this.btn20k.Location = new System.Drawing.Point(3, 69);
            this.btn20k.Name = "btn20k";
            this.btn20k.Size = new System.Drawing.Size(96, 60);
            this.btn20k.TabIndex = 27;
            this.btn20k.Text = "20,000";
            this.btn20k.Click += new System.EventHandler(this.btn20k_Click);
            // 
            // btn50k
            // 
            this.btn50k.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn50k.Appearance.Options.UseFont = true;
            this.btn50k.Location = new System.Drawing.Point(105, 69);
            this.btn50k.Name = "btn50k";
            this.btn50k.Size = new System.Drawing.Size(96, 60);
            this.btn50k.TabIndex = 28;
            this.btn50k.Text = "50,000";
            this.btn50k.Click += new System.EventHandler(this.btn50k_Click);
            // 
            // btn100k
            // 
            this.btn100k.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn100k.Appearance.Options.UseFont = true;
            this.btn100k.Location = new System.Drawing.Point(207, 69);
            this.btn100k.Name = "btn100k";
            this.btn100k.Size = new System.Drawing.Size(96, 60);
            this.btn100k.TabIndex = 29;
            this.btn100k.Text = "100,000";
            this.btn100k.Click += new System.EventHandler(this.btn100k_Click);
            // 
            // btn200k
            // 
            this.btn200k.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn200k.Appearance.Options.UseFont = true;
            this.btn200k.Location = new System.Drawing.Point(309, 69);
            this.btn200k.Name = "btn200k";
            this.btn200k.Size = new System.Drawing.Size(96, 60);
            this.btn200k.TabIndex = 30;
            this.btn200k.Text = "200,000";
            this.btn200k.Click += new System.EventHandler(this.btn200k_Click);
            // 
            // btn500k
            // 
            this.btn500k.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn500k.Appearance.Options.UseFont = true;
            this.btn500k.Location = new System.Drawing.Point(411, 69);
            this.btn500k.Name = "btn500k";
            this.btn500k.Size = new System.Drawing.Size(96, 60);
            this.btn500k.TabIndex = 31;
            this.btn500k.Text = "500,000";
            this.btn500k.Click += new System.EventHandler(this.btn500k_Click);
            // 
            // panelControl19
            // 
            this.panelControl19.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl19.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl19.Location = new System.Drawing.Point(0, 0);
            this.panelControl19.Name = "panelControl19";
            this.panelControl19.Size = new System.Drawing.Size(138, 140);
            this.panelControl19.TabIndex = 18;
            // 
            // panelControl11
            // 
            this.panelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl11.Controls.Add(this.panelControl12);
            this.panelControl11.Controls.Add(this.panelControl14);
            this.panelControl11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl11.Location = new System.Drawing.Point(0, 52);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(682, 52);
            this.panelControl11.TabIndex = 38;
            // 
            // panelControl12
            // 
            this.panelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl12.Controls.Add(this.txtTienKhachTra);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl12.Location = new System.Drawing.Point(138, 0);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(544, 52);
            this.panelControl12.TabIndex = 18;
            // 
            // txtTienKhachTra
            // 
            this.txtTienKhachTra.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTienKhachTra.EditValue = "0";
            this.txtTienKhachTra.EnterMoveNextControl = true;
            this.txtTienKhachTra.Location = new System.Drawing.Point(0, 0);
            this.txtTienKhachTra.Name = "txtTienKhachTra";
            this.txtTienKhachTra.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienKhachTra.Properties.Appearance.Options.UseFont = true;
            this.txtTienKhachTra.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienKhachTra.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienKhachTra.Properties.DisplayFormat.FormatString = "N0";
            this.txtTienKhachTra.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTienKhachTra.Properties.EditFormat.FormatString = "N0";
            this.txtTienKhachTra.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTienKhachTra.Properties.Mask.EditMask = "N0";
            this.txtTienKhachTra.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienKhachTra.Size = new System.Drawing.Size(544, 48);
            this.txtTienKhachTra.TabIndex = 4;
            this.txtTienKhachTra.EditValueChanged += new System.EventHandler(this.txtTienKhachTra_EditValueChanged);
            // 
            // panelControl14
            // 
            this.panelControl14.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl14.Controls.Add(this.labelControl7);
            this.panelControl14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl14.Location = new System.Drawing.Point(0, 0);
            this.panelControl14.Name = "panelControl14";
            this.panelControl14.Size = new System.Drawing.Size(138, 52);
            this.panelControl14.TabIndex = 17;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl7.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl7.Location = new System.Drawing.Point(10, 18);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(118, 19);
            this.labelControl7.TabIndex = 15;
            this.labelControl7.Text = "Tiền khách trả";
            // 
            // panelControl8
            // 
            this.panelControl8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl8.Controls.Add(this.panelControl9);
            this.panelControl8.Controls.Add(this.panelControl10);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl8.Location = new System.Drawing.Point(0, 0);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(682, 52);
            this.panelControl8.TabIndex = 37;
            // 
            // panelControl9
            // 
            this.panelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl9.Controls.Add(this.txtTienve);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl9.Location = new System.Drawing.Point(138, 0);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(544, 52);
            this.panelControl9.TabIndex = 18;
            // 
            // txtTienve
            // 
            this.txtTienve.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTienve.EditValue = "0";
            this.txtTienve.EnterMoveNextControl = true;
            this.txtTienve.Location = new System.Drawing.Point(0, 0);
            this.txtTienve.Name = "txtTienve";
            this.txtTienve.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienve.Properties.Appearance.Options.UseFont = true;
            this.txtTienve.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienve.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienve.Properties.DisplayFormat.FormatString = "N0";
            this.txtTienve.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTienve.Properties.EditFormat.FormatString = "N0";
            this.txtTienve.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTienve.Properties.Mask.EditMask = "N0";
            this.txtTienve.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienve.Properties.ReadOnly = true;
            this.txtTienve.Size = new System.Drawing.Size(544, 48);
            this.txtTienve.TabIndex = 4;
            this.txtTienve.EditValueChanged += new System.EventHandler(this.txtTienve_EditValueChanged);
            // 
            // panelControl10
            // 
            this.panelControl10.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl10.Controls.Add(this.labelControl6);
            this.panelControl10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl10.Location = new System.Drawing.Point(0, 0);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(138, 52);
            this.panelControl10.TabIndex = 17;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl6.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl6.Location = new System.Drawing.Point(10, 18);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(78, 19);
            this.labelControl6.TabIndex = 15;
            this.labelControl6.Text = "Tổng tiền";
            // 
            // pnlSoatve
            // 
            this.pnlSoatve.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlSoatve.Controls.Add(this.panelControl22);
            this.pnlSoatve.Controls.Add(this.panelControl30);
            this.pnlSoatve.Controls.Add(this.panelControl33);
            this.pnlSoatve.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSoatve.Location = new System.Drawing.Point(2, 2);
            this.pnlSoatve.Name = "pnlSoatve";
            this.pnlSoatve.Size = new System.Drawing.Size(682, 612);
            this.pnlSoatve.TabIndex = 7;
            this.pnlSoatve.Visible = false;
            // 
            // panelControl22
            // 
            this.panelControl22.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl22.Controls.Add(this.panelControl26);
            this.panelControl22.Controls.Add(this.panelControl27);
            this.panelControl22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl22.Location = new System.Drawing.Point(0, 104);
            this.panelControl22.Name = "panelControl22";
            this.panelControl22.Size = new System.Drawing.Size(682, 508);
            this.panelControl22.TabIndex = 40;
            // 
            // panelControl26
            // 
            this.panelControl26.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl26.Controls.Add(this.txtThongtinve);
            this.panelControl26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl26.Location = new System.Drawing.Point(138, 0);
            this.panelControl26.Name = "panelControl26";
            this.panelControl26.Size = new System.Drawing.Size(544, 508);
            this.panelControl26.TabIndex = 18;
            // 
            // txtThongtinve
            // 
            this.txtThongtinve.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtThongtinve.EditValue = "";
            this.txtThongtinve.Location = new System.Drawing.Point(0, 0);
            this.txtThongtinve.Name = "txtThongtinve";
            this.txtThongtinve.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThongtinve.Properties.Appearance.Options.UseFont = true;
            this.txtThongtinve.Properties.ReadOnly = true;
            this.txtThongtinve.Size = new System.Drawing.Size(544, 508);
            this.txtThongtinve.TabIndex = 4;
            // 
            // panelControl27
            // 
            this.panelControl27.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl27.Controls.Add(this.labelControl1);
            this.panelControl27.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl27.Location = new System.Drawing.Point(0, 0);
            this.panelControl27.Name = "panelControl27";
            this.panelControl27.Size = new System.Drawing.Size(138, 508);
            this.panelControl27.TabIndex = 17;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl1.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl1.Location = new System.Drawing.Point(10, 18);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(102, 19);
            this.labelControl1.TabIndex = 15;
            this.labelControl1.Text = "Thông tin vé";
            // 
            // panelControl30
            // 
            this.panelControl30.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl30.Controls.Add(this.panelControl31);
            this.panelControl30.Controls.Add(this.panelControl32);
            this.panelControl30.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl30.Location = new System.Drawing.Point(0, 52);
            this.panelControl30.Name = "panelControl30";
            this.panelControl30.Size = new System.Drawing.Size(682, 52);
            this.panelControl30.TabIndex = 38;
            // 
            // panelControl31
            // 
            this.panelControl31.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl31.Controls.Add(this.txtTrangthai);
            this.panelControl31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl31.Location = new System.Drawing.Point(138, 0);
            this.panelControl31.Name = "panelControl31";
            this.panelControl31.Size = new System.Drawing.Size(544, 52);
            this.panelControl31.TabIndex = 18;
            // 
            // txtTrangthai
            // 
            this.txtTrangthai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTrangthai.EnterMoveNextControl = true;
            this.txtTrangthai.Location = new System.Drawing.Point(0, 0);
            this.txtTrangthai.Name = "txtTrangthai";
            this.txtTrangthai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrangthai.Properties.Appearance.Options.UseFont = true;
            this.txtTrangthai.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTrangthai.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrangthai.Properties.ReadOnly = true;
            this.txtTrangthai.Size = new System.Drawing.Size(544, 48);
            this.txtTrangthai.TabIndex = 4;
            // 
            // panelControl32
            // 
            this.panelControl32.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl32.Controls.Add(this.labelControl4);
            this.panelControl32.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl32.Location = new System.Drawing.Point(0, 0);
            this.panelControl32.Name = "panelControl32";
            this.panelControl32.Size = new System.Drawing.Size(138, 52);
            this.panelControl32.TabIndex = 17;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl4.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl4.Location = new System.Drawing.Point(10, 18);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(85, 19);
            this.labelControl4.TabIndex = 15;
            this.labelControl4.Text = "Trạng thái";
            // 
            // panelControl33
            // 
            this.panelControl33.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl33.Controls.Add(this.panelControl34);
            this.panelControl33.Controls.Add(this.panelControl35);
            this.panelControl33.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl33.Location = new System.Drawing.Point(0, 0);
            this.panelControl33.Name = "panelControl33";
            this.panelControl33.Size = new System.Drawing.Size(682, 52);
            this.panelControl33.TabIndex = 37;
            // 
            // panelControl34
            // 
            this.panelControl34.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl34.Controls.Add(this.txtMave);
            this.panelControl34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl34.Location = new System.Drawing.Point(138, 0);
            this.panelControl34.Name = "panelControl34";
            this.panelControl34.Size = new System.Drawing.Size(544, 52);
            this.panelControl34.TabIndex = 18;
            // 
            // txtMave
            // 
            this.txtMave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMave.Location = new System.Drawing.Point(0, 0);
            this.txtMave.Name = "txtMave";
            this.txtMave.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMave.Properties.Appearance.Options.UseFont = true;
            this.txtMave.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMave.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMave.Size = new System.Drawing.Size(544, 48);
            this.txtMave.TabIndex = 4;
            this.txtMave.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMave_KeyDown);
            // 
            // panelControl35
            // 
            this.panelControl35.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl35.Controls.Add(this.labelControl10);
            this.panelControl35.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl35.Location = new System.Drawing.Point(0, 0);
            this.panelControl35.Name = "panelControl35";
            this.panelControl35.Size = new System.Drawing.Size(138, 52);
            this.panelControl35.TabIndex = 17;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl10.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl10.Location = new System.Drawing.Point(10, 18);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(48, 19);
            this.labelControl10.TabIndex = 15;
            this.labelControl10.Text = "Mã vé";
            // 
            // pnlKetca
            // 
            this.pnlKetca.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlKetca.Controls.Add(this.grdKetca);
            this.pnlKetca.Controls.Add(this.panelControl23);
            this.pnlKetca.Controls.Add(this.panelControl20);
            this.pnlKetca.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlKetca.Location = new System.Drawing.Point(2, 2);
            this.pnlKetca.Name = "pnlKetca";
            this.pnlKetca.Size = new System.Drawing.Size(682, 612);
            this.pnlKetca.TabIndex = 6;
            this.pnlKetca.Visible = false;
            // 
            // grdKetca
            // 
            this.grdKetca.AllowDrop = true;
            this.grdKetca.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.RelationName = "Level1";
            this.grdKetca.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.grdKetca.Location = new System.Drawing.Point(0, 0);
            this.grdKetca.MainView = this.grvKetca;
            this.grdKetca.Name = "grdKetca";
            this.grdKetca.Size = new System.Drawing.Size(682, 508);
            this.grdKetca.TabIndex = 6;
            this.grdKetca.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvKetca});
            // 
            // grvKetca
            // 
            this.grvKetca.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.grvKetca.Appearance.FocusedRow.Options.UseFont = true;
            this.grvKetca.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvKetca.Appearance.FooterPanel.Options.UseFont = true;
            this.grvKetca.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grvKetca.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grvKetca.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvKetca.Appearance.GroupFooter.Options.UseFont = true;
            this.grvKetca.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grvKetca.Appearance.Row.Options.UseFont = true;
            this.grvKetca.ColumnPanelRowHeight = 30;
            this.grvKetca.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STTKC,
            this.gridColumn3,
            this.SL,
            this.gridColumn6,
            this.THANHTIEN,
            this.gridColumn4});
            this.grvKetca.FooterPanelHeight = 30;
            this.grvKetca.GridControl = this.grdKetca;
            this.grvKetca.Name = "grvKetca";
            this.grvKetca.OptionsNavigation.EnterMoveNextColumn = true;
            this.grvKetca.OptionsView.EnableAppearanceEvenRow = true;
            this.grvKetca.OptionsView.ShowGroupPanel = false;
            this.grvKetca.OptionsView.ShowIndicator = false;
            this.grvKetca.RowHeight = 35;
            this.grvKetca.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.grvKetca.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.grvKetca_CustomDrawCell);
            // 
            // STTKC
            // 
            this.STTKC.AppearanceCell.Options.UseTextOptions = true;
            this.STTKC.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTKC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.STTKC.AppearanceHeader.Options.UseFont = true;
            this.STTKC.AppearanceHeader.Options.UseTextOptions = true;
            this.STTKC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTKC.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STTKC.Caption = "STT";
            this.STTKC.Name = "STTKC";
            this.STTKC.OptionsColumn.AllowEdit = false;
            this.STTKC.OptionsColumn.AllowFocus = false;
            this.STTKC.OptionsColumn.AllowMove = false;
            this.STTKC.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STTKC.Visible = true;
            this.STTKC.VisibleIndex = 0;
            this.STTKC.Width = 45;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn3.Caption = "Trò chơi";
            this.gridColumn3.FieldName = "TEN_TROCHOI";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.AllowMove = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 150;
            // 
            // SL
            // 
            this.SL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.SL.AppearanceHeader.Options.UseFont = true;
            this.SL.AppearanceHeader.Options.UseTextOptions = true;
            this.SL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SL.Caption = "Số lượng";
            this.SL.FieldName = "SL";
            this.SL.Name = "SL";
            this.SL.OptionsColumn.AllowEdit = false;
            this.SL.OptionsColumn.AllowFocus = false;
            this.SL.Visible = true;
            this.SL.VisibleIndex = 2;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn6.Caption = "Giá vé";
            this.gridColumn6.DisplayFormat.FormatString = "N0";
            this.gridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn6.FieldName = "GIABAN";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.AllowMove = false;
            this.gridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn6.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DONGIA", "{0:#,###0}")});
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 3;
            this.gridColumn6.Width = 150;
            // 
            // THANHTIEN
            // 
            this.THANHTIEN.AppearanceCell.Options.UseTextOptions = true;
            this.THANHTIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.THANHTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.THANHTIEN.AppearanceHeader.Options.UseFont = true;
            this.THANHTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.THANHTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THANHTIEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THANHTIEN.Caption = "Thành tiền";
            this.THANHTIEN.DisplayFormat.FormatString = "N0";
            this.THANHTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.THANHTIEN.FieldName = "THANHTIEN";
            this.THANHTIEN.Name = "THANHTIEN";
            this.THANHTIEN.OptionsColumn.AllowEdit = false;
            this.THANHTIEN.OptionsColumn.AllowFocus = false;
            this.THANHTIEN.OptionsColumn.AllowMove = false;
            this.THANHTIEN.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.THANHTIEN.Visible = true;
            this.THANHTIEN.VisibleIndex = 4;
            this.THANHTIEN.Width = 150;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "Nhân viên";
            this.gridColumn4.FieldName = "TEN_NHANVIEN";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.AllowMove = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 5;
            this.gridColumn4.Width = 200;
            // 
            // panelControl23
            // 
            this.panelControl23.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl23.Controls.Add(this.panelControl24);
            this.panelControl23.Controls.Add(this.panelControl25);
            this.panelControl23.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl23.Location = new System.Drawing.Point(0, 508);
            this.panelControl23.Name = "panelControl23";
            this.panelControl23.Size = new System.Drawing.Size(682, 52);
            this.panelControl23.TabIndex = 42;
            // 
            // panelControl24
            // 
            this.panelControl24.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl24.Controls.Add(this.txtKetca);
            this.panelControl24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl24.Location = new System.Drawing.Point(156, 0);
            this.panelControl24.Name = "panelControl24";
            this.panelControl24.Size = new System.Drawing.Size(526, 52);
            this.panelControl24.TabIndex = 18;
            // 
            // txtKetca
            // 
            this.txtKetca.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtKetca.EnterMoveNextControl = true;
            this.txtKetca.Location = new System.Drawing.Point(0, 0);
            this.txtKetca.Name = "txtKetca";
            this.txtKetca.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKetca.Properties.Appearance.Options.UseFont = true;
            this.txtKetca.Properties.Appearance.Options.UseTextOptions = true;
            this.txtKetca.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtKetca.Properties.DisplayFormat.FormatString = "N0";
            this.txtKetca.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtKetca.Properties.EditFormat.FormatString = "N0";
            this.txtKetca.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtKetca.Properties.Mask.EditMask = "N0";
            this.txtKetca.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtKetca.Properties.ReadOnly = true;
            this.txtKetca.Size = new System.Drawing.Size(526, 48);
            this.txtKetca.TabIndex = 4;
            // 
            // panelControl25
            // 
            this.panelControl25.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl25.Controls.Add(this.labelControl3);
            this.panelControl25.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl25.Location = new System.Drawing.Point(0, 0);
            this.panelControl25.Name = "panelControl25";
            this.panelControl25.Size = new System.Drawing.Size(156, 52);
            this.panelControl25.TabIndex = 17;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl3.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl3.Location = new System.Drawing.Point(10, 18);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(137, 19);
            this.labelControl3.TabIndex = 15;
            this.labelControl3.Text = "Tổng tiền bán vé";
            // 
            // panelControl20
            // 
            this.panelControl20.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl20.Controls.Add(this.btnInketca);
            this.panelControl20.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl20.Location = new System.Drawing.Point(0, 560);
            this.panelControl20.Name = "panelControl20";
            this.panelControl20.Size = new System.Drawing.Size(682, 52);
            this.panelControl20.TabIndex = 41;
            // 
            // btnInketca
            // 
            this.btnInketca.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInketca.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnInketca.Appearance.Options.UseFont = true;
            this.btnInketca.Location = new System.Drawing.Point(570, 3);
            this.btnInketca.Name = "btnInketca";
            this.btnInketca.Size = new System.Drawing.Size(110, 44);
            this.btnInketca.TabIndex = 19;
            this.btnInketca.Text = "In kết ca";
            this.btnInketca.Click += new System.EventHandler(this.btnInketca_Click);
            // 
            // pnlHuyve
            // 
            this.pnlHuyve.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlHuyve.Controls.Add(this.btnHuy);
            this.pnlHuyve.Controls.Add(this.panelControl5);
            this.pnlHuyve.Controls.Add(this.panelControl2);
            this.pnlHuyve.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlHuyve.Location = new System.Drawing.Point(2, 2);
            this.pnlHuyve.Name = "pnlHuyve";
            this.pnlHuyve.Size = new System.Drawing.Size(682, 612);
            this.pnlHuyve.TabIndex = 5;
            this.pnlHuyve.Visible = false;
            // 
            // btnHuy
            // 
            this.btnHuy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHuy.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnHuy.Appearance.Options.UseFont = true;
            this.btnHuy.Location = new System.Drawing.Point(570, 253);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(110, 44);
            this.btnHuy.TabIndex = 18;
            this.btnHuy.Text = "Hủy";
            this.btnHuy.Click += new System.EventHandler(this.btnHuy_Click);
            // 
            // panelControl5
            // 
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl5.Controls.Add(this.panelControl6);
            this.panelControl5.Controls.Add(this.panelControl7);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl5.Location = new System.Drawing.Point(0, 52);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(682, 195);
            this.panelControl5.TabIndex = 17;
            // 
            // panelControl6
            // 
            this.panelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl6.Controls.Add(this.txtLYDO);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(78, 0);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(604, 195);
            this.panelControl6.TabIndex = 18;
            // 
            // txtLYDO
            // 
            this.txtLYDO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLYDO.Location = new System.Drawing.Point(0, 0);
            this.txtLYDO.Name = "txtLYDO";
            this.txtLYDO.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLYDO.Properties.Appearance.Options.UseFont = true;
            this.txtLYDO.Size = new System.Drawing.Size(604, 195);
            this.txtLYDO.TabIndex = 4;
            // 
            // panelControl7
            // 
            this.panelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl7.Controls.Add(this.labelControl5);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl7.Location = new System.Drawing.Point(0, 0);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(78, 195);
            this.panelControl7.TabIndex = 17;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl5.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl5.Location = new System.Drawing.Point(10, 18);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(43, 19);
            this.labelControl5.TabIndex = 15;
            this.labelControl5.Text = "Lý do";
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.panelControl4);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(682, 52);
            this.panelControl2.TabIndex = 16;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Controls.Add(this.txtMA_VEBAN);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(78, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(604, 52);
            this.panelControl4.TabIndex = 18;
            // 
            // txtMA_VEBAN
            // 
            this.txtMA_VEBAN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMA_VEBAN.EnterMoveNextControl = true;
            this.txtMA_VEBAN.Location = new System.Drawing.Point(0, 0);
            this.txtMA_VEBAN.Name = "txtMA_VEBAN";
            this.txtMA_VEBAN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMA_VEBAN.Properties.Appearance.Options.UseFont = true;
            this.txtMA_VEBAN.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMA_VEBAN.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMA_VEBAN.Size = new System.Drawing.Size(604, 48);
            this.txtMA_VEBAN.TabIndex = 4;
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.labelControl2);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(78, 52);
            this.panelControl3.TabIndex = 17;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl2.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelControl2.Location = new System.Drawing.Point(10, 18);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(48, 19);
            this.labelControl2.TabIndex = 15;
            this.labelControl2.Text = "Mã vé";
            // 
            // pnlInlaive
            // 
            this.pnlInlaive.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlInlaive.Controls.Add(this.grdInlaive);
            this.pnlInlaive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlInlaive.Location = new System.Drawing.Point(2, 2);
            this.pnlInlaive.Name = "pnlInlaive";
            this.pnlInlaive.Size = new System.Drawing.Size(682, 612);
            this.pnlInlaive.TabIndex = 4;
            this.pnlInlaive.Visible = false;
            // 
            // grdInlaive
            // 
            this.grdInlaive.AllowDrop = true;
            this.grdInlaive.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode3.RelationName = "Level1";
            this.grdInlaive.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode3});
            this.grdInlaive.Location = new System.Drawing.Point(0, 0);
            this.grdInlaive.MainView = this.grvInlaive;
            this.grdInlaive.Name = "grdInlaive";
            this.grdInlaive.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1});
            this.grdInlaive.Size = new System.Drawing.Size(682, 612);
            this.grdInlaive.TabIndex = 6;
            this.grdInlaive.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvInlaive});
            // 
            // grvInlaive
            // 
            this.grvInlaive.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.grvInlaive.Appearance.FocusedRow.Options.UseFont = true;
            this.grvInlaive.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvInlaive.Appearance.FooterPanel.Options.UseFont = true;
            this.grvInlaive.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grvInlaive.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grvInlaive.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvInlaive.Appearance.GroupFooter.Options.UseFont = true;
            this.grvInlaive.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grvInlaive.Appearance.Row.Options.UseFont = true;
            this.grvInlaive.ColumnPanelRowHeight = 30;
            this.grvInlaive.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MA_VEBAN,
            this.TEN_TROCHOI,
            this.TENNHANVIEN,
            this.NGAYBAN,
            this.GIABAN1,
            this.INVE});
            this.grvInlaive.FooterPanelHeight = 30;
            this.grvInlaive.GridControl = this.grdInlaive;
            this.grvInlaive.Name = "grvInlaive";
            this.grvInlaive.OptionsNavigation.EnterMoveNextColumn = true;
            this.grvInlaive.OptionsView.EnableAppearanceEvenRow = true;
            this.grvInlaive.OptionsView.ShowFooter = true;
            this.grvInlaive.OptionsView.ShowGroupPanel = false;
            this.grvInlaive.OptionsView.ShowIndicator = false;
            this.grvInlaive.RowHeight = 35;
            this.grvInlaive.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.grvInlaive.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.grvInlaive_RowCellClick);
            this.grvInlaive.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.grvInlaive_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MA_VEBAN", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 45;
            // 
            // MA_VEBAN
            // 
            this.MA_VEBAN.AppearanceCell.Options.UseTextOptions = true;
            this.MA_VEBAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_VEBAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.MA_VEBAN.AppearanceHeader.Options.UseFont = true;
            this.MA_VEBAN.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_VEBAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_VEBAN.Caption = "Mã vé";
            this.MA_VEBAN.FieldName = "MA_VEBAN";
            this.MA_VEBAN.MinWidth = 120;
            this.MA_VEBAN.Name = "MA_VEBAN";
            this.MA_VEBAN.OptionsColumn.AllowEdit = false;
            this.MA_VEBAN.OptionsColumn.AllowFocus = false;
            this.MA_VEBAN.Visible = true;
            this.MA_VEBAN.VisibleIndex = 1;
            this.MA_VEBAN.Width = 150;
            // 
            // TEN_TROCHOI
            // 
            this.TEN_TROCHOI.AppearanceCell.Options.UseTextOptions = true;
            this.TEN_TROCHOI.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.TEN_TROCHOI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TEN_TROCHOI.AppearanceHeader.Options.UseFont = true;
            this.TEN_TROCHOI.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_TROCHOI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_TROCHOI.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_TROCHOI.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TEN_TROCHOI.Caption = "Trò chơi";
            this.TEN_TROCHOI.FieldName = "TEN_TROCHOI";
            this.TEN_TROCHOI.Name = "TEN_TROCHOI";
            this.TEN_TROCHOI.OptionsColumn.AllowEdit = false;
            this.TEN_TROCHOI.OptionsColumn.AllowFocus = false;
            this.TEN_TROCHOI.OptionsColumn.AllowMove = false;
            this.TEN_TROCHOI.Visible = true;
            this.TEN_TROCHOI.VisibleIndex = 2;
            this.TEN_TROCHOI.Width = 150;
            // 
            // TENNHANVIEN
            // 
            this.TENNHANVIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TENNHANVIEN.AppearanceHeader.Options.UseFont = true;
            this.TENNHANVIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TENNHANVIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENNHANVIEN.Caption = "Nhân viên";
            this.TENNHANVIEN.FieldName = "TENNHANVIEN";
            this.TENNHANVIEN.Name = "TENNHANVIEN";
            this.TENNHANVIEN.OptionsColumn.AllowEdit = false;
            this.TENNHANVIEN.OptionsColumn.AllowFocus = false;
            this.TENNHANVIEN.OptionsColumn.AllowMove = false;
            this.TENNHANVIEN.Visible = true;
            this.TENNHANVIEN.VisibleIndex = 3;
            this.TENNHANVIEN.Width = 200;
            // 
            // NGAYBAN
            // 
            this.NGAYBAN.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYBAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYBAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.NGAYBAN.AppearanceHeader.Options.UseFont = true;
            this.NGAYBAN.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYBAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYBAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYBAN.Caption = "Ngày bán ";
            this.NGAYBAN.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.NGAYBAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYBAN.FieldName = "NGAYBAN";
            this.NGAYBAN.Name = "NGAYBAN";
            this.NGAYBAN.OptionsColumn.AllowEdit = false;
            this.NGAYBAN.OptionsColumn.AllowFocus = false;
            this.NGAYBAN.OptionsColumn.AllowMove = false;
            this.NGAYBAN.Visible = true;
            this.NGAYBAN.VisibleIndex = 4;
            this.NGAYBAN.Width = 150;
            // 
            // GIABAN1
            // 
            this.GIABAN1.AppearanceCell.Options.UseTextOptions = true;
            this.GIABAN1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIABAN1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GIABAN1.AppearanceHeader.Options.UseFont = true;
            this.GIABAN1.AppearanceHeader.Options.UseTextOptions = true;
            this.GIABAN1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIABAN1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GIABAN1.Caption = "Giá vé";
            this.GIABAN1.DisplayFormat.FormatString = "N0";
            this.GIABAN1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GIABAN1.FieldName = "GIABAN";
            this.GIABAN1.Name = "GIABAN1";
            this.GIABAN1.OptionsColumn.AllowFocus = false;
            this.GIABAN1.OptionsColumn.AllowMove = false;
            this.GIABAN1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.GIABAN1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "GIABAN", "{0:#,###0}")});
            this.GIABAN1.Visible = true;
            this.GIABAN1.VisibleIndex = 5;
            this.GIABAN1.Width = 150;
            // 
            // INVE
            // 
            this.INVE.AppearanceCell.Options.UseTextOptions = true;
            this.INVE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.INVE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.INVE.AppearanceHeader.Options.UseFont = true;
            this.INVE.AppearanceHeader.Options.UseTextOptions = true;
            this.INVE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.INVE.Caption = "In lại";
            this.INVE.ColumnEdit = this.repositoryItemButtonEdit1;
            this.INVE.FieldName = "Xoa";
            this.INVE.MinWidth = 60;
            this.INVE.Name = "INVE";
            this.INVE.OptionsColumn.AllowEdit = false;
            this.INVE.OptionsColumn.AllowMove = false;
            this.INVE.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.INVE.Visible = true;
            this.INVE.VisibleIndex = 6;
            this.INVE.Width = 100;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit1.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // pnlLoaddanhmuc
            // 
            this.pnlLoaddanhmuc.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlLoaddanhmuc.Controls.Add(this.pnlTrochoi);
            this.pnlLoaddanhmuc.Controls.Add(this.pnlLoaitrochoi);
            this.pnlLoaddanhmuc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLoaddanhmuc.Location = new System.Drawing.Point(2, 2);
            this.pnlLoaddanhmuc.Name = "pnlLoaddanhmuc";
            this.pnlLoaddanhmuc.Size = new System.Drawing.Size(682, 612);
            this.pnlLoaddanhmuc.TabIndex = 2;
            // 
            // pnlTrochoi
            // 
            this.pnlTrochoi.Controls.Add(this.fpnlTROCHOI);
            this.pnlTrochoi.Controls.Add(this.btnXuongHangHoa);
            this.pnlTrochoi.Controls.Add(this.btnLenHangHoa);
            this.pnlTrochoi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTrochoi.Location = new System.Drawing.Point(0, 0);
            this.pnlTrochoi.Name = "pnlTrochoi";
            this.pnlTrochoi.Size = new System.Drawing.Size(547, 612);
            this.pnlTrochoi.TabIndex = 0;
            // 
            // fpnlTROCHOI
            // 
            this.fpnlTROCHOI.AutoScroll = true;
            this.fpnlTROCHOI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpnlTROCHOI.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.fpnlTROCHOI.Location = new System.Drawing.Point(2, 37);
            this.fpnlTROCHOI.Name = "fpnlTROCHOI";
            this.fpnlTROCHOI.Size = new System.Drawing.Size(543, 538);
            this.fpnlTROCHOI.TabIndex = 18;
            // 
            // btnXuongHangHoa
            // 
            this.btnXuongHangHoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongHangHoa.Appearance.Options.UseFont = true;
            this.btnXuongHangHoa.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongHangHoa.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongHangHoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongHangHoa.Location = new System.Drawing.Point(2, 575);
            this.btnXuongHangHoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongHangHoa.Name = "btnXuongHangHoa";
            this.btnXuongHangHoa.Size = new System.Drawing.Size(543, 35);
            this.btnXuongHangHoa.TabIndex = 17;
            this.btnXuongHangHoa.Click += new System.EventHandler(this.btnXuongLOAITROCHOI_Click);
            // 
            // btnLenHangHoa
            // 
            this.btnLenHangHoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenHangHoa.Appearance.Options.UseFont = true;
            this.btnLenHangHoa.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenHangHoa.Image = ((System.Drawing.Image)(resources.GetObject("btnLenHangHoa.Image")));
            this.btnLenHangHoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenHangHoa.Location = new System.Drawing.Point(2, 2);
            this.btnLenHangHoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnLenHangHoa.Name = "btnLenHangHoa";
            this.btnLenHangHoa.Size = new System.Drawing.Size(543, 35);
            this.btnLenHangHoa.TabIndex = 16;
            this.btnLenHangHoa.Click += new System.EventHandler(this.btnLenTROCHOI_Click);
            // 
            // pnlLoaitrochoi
            // 
            this.pnlLoaitrochoi.Controls.Add(this.fpnlLOAITROCHOI);
            this.pnlLoaitrochoi.Controls.Add(this.btnXuongNhomhang);
            this.pnlLoaitrochoi.Controls.Add(this.btnLenNhomhang);
            this.pnlLoaitrochoi.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlLoaitrochoi.Location = new System.Drawing.Point(547, 0);
            this.pnlLoaitrochoi.Margin = new System.Windows.Forms.Padding(0);
            this.pnlLoaitrochoi.Name = "pnlLoaitrochoi";
            this.pnlLoaitrochoi.Size = new System.Drawing.Size(135, 612);
            this.pnlLoaitrochoi.TabIndex = 1;
            // 
            // fpnlLOAITROCHOI
            // 
            this.fpnlLOAITROCHOI.AutoScroll = true;
            this.fpnlLOAITROCHOI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpnlLOAITROCHOI.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.fpnlLOAITROCHOI.Location = new System.Drawing.Point(2, 37);
            this.fpnlLOAITROCHOI.Margin = new System.Windows.Forms.Padding(0);
            this.fpnlLOAITROCHOI.Name = "fpnlLOAITROCHOI";
            this.fpnlLOAITROCHOI.Size = new System.Drawing.Size(131, 538);
            this.fpnlLOAITROCHOI.TabIndex = 17;
            // 
            // btnXuongNhomhang
            // 
            this.btnXuongNhomhang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongNhomhang.Appearance.Options.UseFont = true;
            this.btnXuongNhomhang.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongNhomhang.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongNhomhang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongNhomhang.Location = new System.Drawing.Point(2, 575);
            this.btnXuongNhomhang.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongNhomhang.Name = "btnXuongNhomhang";
            this.btnXuongNhomhang.Size = new System.Drawing.Size(131, 35);
            this.btnXuongNhomhang.TabIndex = 16;
            this.btnXuongNhomhang.Click += new System.EventHandler(this.btnXuongTROCHOI_Click);
            // 
            // btnLenNhomhang
            // 
            this.btnLenNhomhang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenNhomhang.Appearance.Options.UseFont = true;
            this.btnLenNhomhang.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenNhomhang.Image = ((System.Drawing.Image)(resources.GetObject("btnLenNhomhang.Image")));
            this.btnLenNhomhang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenNhomhang.Location = new System.Drawing.Point(2, 2);
            this.btnLenNhomhang.Margin = new System.Windows.Forms.Padding(4);
            this.btnLenNhomhang.Name = "btnLenNhomhang";
            this.btnLenNhomhang.Size = new System.Drawing.Size(131, 35);
            this.btnLenNhomhang.TabIndex = 15;
            this.btnLenNhomhang.Click += new System.EventHandler(this.btnLenLOAITROCHOI_Click);
            // 
            // pnlNghiepvu
            // 
            this.pnlNghiepvu.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlNghiepvu.Controls.Add(this.pn_listProduct);
            this.pnlNghiepvu.Controls.Add(this.pnThuNgan);
            this.pnlNghiepvu.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlNghiepvu.Location = new System.Drawing.Point(684, 2);
            this.pnlNghiepvu.Name = "pnlNghiepvu";
            this.pnlNghiepvu.Size = new System.Drawing.Size(350, 612);
            this.pnlNghiepvu.TabIndex = 0;
            // 
            // pn_listProduct
            // 
            this.pn_listProduct.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pn_listProduct.Controls.Add(this.gr_SelectProduct);
            this.pn_listProduct.Controls.Add(this.pnUpDowGv);
            this.pn_listProduct.Controls.Add(this.pnBtnDeleteGv);
            this.pn_listProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_listProduct.Location = new System.Drawing.Point(0, 0);
            this.pn_listProduct.Name = "pn_listProduct";
            this.pn_listProduct.Size = new System.Drawing.Size(350, 344);
            this.pn_listProduct.TabIndex = 0;
            // 
            // gr_SelectProduct
            // 
            this.gr_SelectProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode4.RelationName = "Level1";
            this.gr_SelectProduct.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode4});
            this.gr_SelectProduct.Location = new System.Drawing.Point(0, 0);
            this.gr_SelectProduct.MainView = this.gv_SelectProduct;
            this.gr_SelectProduct.Name = "gr_SelectProduct";
            this.gr_SelectProduct.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1});
            this.gr_SelectProduct.Size = new System.Drawing.Size(315, 296);
            this.gr_SelectProduct.TabIndex = 19;
            this.gr_SelectProduct.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv_SelectProduct});
            // 
            // gv_SelectProduct
            // 
            this.gv_SelectProduct.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gv_SelectProduct.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Maroon;
            this.gv_SelectProduct.Appearance.FocusedRow.Options.UseFont = true;
            this.gv_SelectProduct.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gv_SelectProduct.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gv_SelectProduct.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Red;
            this.gv_SelectProduct.Appearance.FooterPanel.Options.UseFont = true;
            this.gv_SelectProduct.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gv_SelectProduct.ColumnPanelRowHeight = 30;
            this.gv_SelectProduct.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SOTT,
            this.TEN_HANGHOA,
            this.SOLUONG,
            this.GIABAN,
            this.Thanh_Tien,
            this.MA_HANGHOA,
            this.GHICHU,
            this.MA_BEP,
            this.THUE});
            this.gv_SelectProduct.GridControl = this.gr_SelectProduct;
            this.gv_SelectProduct.Name = "gv_SelectProduct";
            this.gv_SelectProduct.OptionsView.EnableAppearanceEvenRow = true;
            this.gv_SelectProduct.OptionsView.ShowFooter = true;
            this.gv_SelectProduct.OptionsView.ShowGroupPanel = false;
            this.gv_SelectProduct.OptionsView.ShowIndicator = false;
            this.gv_SelectProduct.RowHeight = 35;
            this.gv_SelectProduct.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gv_SelectProduct.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gv_SelectProduct_RowCellClick);
            this.gv_SelectProduct.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gv_SelectProduct_CustomDrawCell);
            this.gv_SelectProduct.LostFocus += new System.EventHandler(this.gv_SelectProduct_LostFocus);
            // 
            // SOTT
            // 
            this.SOTT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.SOTT.AppearanceCell.Options.UseFont = true;
            this.SOTT.AppearanceCell.Options.UseTextOptions = true;
            this.SOTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOTT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.SOTT.AppearanceHeader.Options.UseFont = true;
            this.SOTT.AppearanceHeader.Options.UseTextOptions = true;
            this.SOTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOTT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOTT.Caption = "TT";
            this.SOTT.FieldName = "SOTT";
            this.SOTT.Name = "SOTT";
            this.SOTT.OptionsColumn.AllowEdit = false;
            this.SOTT.OptionsColumn.AllowFocus = false;
            this.SOTT.OptionsColumn.FixedWidth = true;
            this.SOTT.Visible = true;
            this.SOTT.VisibleIndex = 0;
            this.SOTT.Width = 25;
            // 
            // TEN_HANGHOA
            // 
            this.TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_HANGHOA.Caption = "Tên";
            this.TEN_HANGHOA.FieldName = "TEN_HANGHOA";
            this.TEN_HANGHOA.Name = "TEN_HANGHOA";
            this.TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this.TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this.TEN_HANGHOA.Visible = true;
            this.TEN_HANGHOA.VisibleIndex = 1;
            this.TEN_HANGHOA.Width = 113;
            // 
            // SOLUONG
            // 
            this.SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.SOLUONG.AppearanceCell.Options.UseFont = true;
            this.SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.Caption = "SL";
            this.SOLUONG.ColumnEdit = this.repositoryItemCalcEdit1;
            this.SOLUONG.FieldName = "SOLUONG";
            this.SOLUONG.Name = "SOLUONG";
            this.SOLUONG.OptionsColumn.AllowEdit = false;
            this.SOLUONG.OptionsColumn.AllowFocus = false;
            this.SOLUONG.OptionsColumn.FixedWidth = true;
            this.SOLUONG.Visible = true;
            this.SOLUONG.VisibleIndex = 2;
            this.SOLUONG.Width = 30;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // GIABAN
            // 
            this.GIABAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GIABAN.AppearanceCell.Options.UseFont = true;
            this.GIABAN.AppearanceCell.Options.UseTextOptions = true;
            this.GIABAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIABAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.GIABAN.AppearanceHeader.Options.UseFont = true;
            this.GIABAN.AppearanceHeader.Options.UseTextOptions = true;
            this.GIABAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIABAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN.Caption = "Đơn giá";
            this.GIABAN.DisplayFormat.FormatString = "{0:#,###0}";
            this.GIABAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GIABAN.FieldName = "GIABAN";
            this.GIABAN.Name = "GIABAN";
            this.GIABAN.OptionsColumn.AllowEdit = false;
            this.GIABAN.OptionsColumn.AllowFocus = false;
            this.GIABAN.OptionsColumn.FixedWidth = true;
            this.GIABAN.Visible = true;
            this.GIABAN.VisibleIndex = 3;
            this.GIABAN.Width = 70;
            // 
            // Thanh_Tien
            // 
            this.Thanh_Tien.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Thanh_Tien.AppearanceCell.Options.UseFont = true;
            this.Thanh_Tien.AppearanceCell.Options.UseTextOptions = true;
            this.Thanh_Tien.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Thanh_Tien.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Thanh_Tien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.Thanh_Tien.AppearanceHeader.Options.UseFont = true;
            this.Thanh_Tien.AppearanceHeader.Options.UseTextOptions = true;
            this.Thanh_Tien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Thanh_Tien.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Thanh_Tien.Caption = "Thành tiền";
            this.Thanh_Tien.DisplayFormat.FormatString = "{0:#,###0}";
            this.Thanh_Tien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.Thanh_Tien.FieldName = "Thanh_Tien";
            this.Thanh_Tien.Name = "Thanh_Tien";
            this.Thanh_Tien.OptionsColumn.AllowEdit = false;
            this.Thanh_Tien.OptionsColumn.AllowFocus = false;
            this.Thanh_Tien.OptionsColumn.FixedWidth = true;
            this.Thanh_Tien.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Thanh_Tien", "{0:#,###0}")});
            this.Thanh_Tien.Visible = true;
            this.Thanh_Tien.VisibleIndex = 4;
            this.Thanh_Tien.Width = 80;
            // 
            // MA_HANGHOA
            // 
            this.MA_HANGHOA.Caption = "Mã Hàng Hóa";
            this.MA_HANGHOA.FieldName = "MA_HANGHOA";
            this.MA_HANGHOA.Name = "MA_HANGHOA";
            this.MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this.MA_HANGHOA.OptionsColumn.AllowFocus = false;
            // 
            // GHICHU
            // 
            this.GHICHU.Caption = "Ghi Chú";
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.Name = "GHICHU";
            // 
            // MA_BEP
            // 
            this.MA_BEP.Caption = "Máy In Bếp";
            this.MA_BEP.FieldName = "MA_BEP";
            this.MA_BEP.Name = "MA_BEP";
            // 
            // THUE
            // 
            this.THUE.Caption = "VAT";
            this.THUE.FieldName = "THUE";
            this.THUE.Name = "THUE";
            // 
            // pnUpDowGv
            // 
            this.pnUpDowGv.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnUpDowGv.Controls.Add(this.labelControl17);
            this.pnUpDowGv.Controls.Add(this.lb_RowsCount);
            this.pnUpDowGv.Controls.Add(this.lb_STT);
            this.pnUpDowGv.Controls.Add(this.btn_up);
            this.pnUpDowGv.Controls.Add(this.btn_Dow);
            this.pnUpDowGv.Controls.Add(this.btn_DowLast);
            this.pnUpDowGv.Controls.Add(this.btn_UpFirst);
            this.pnUpDowGv.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnUpDowGv.Location = new System.Drawing.Point(315, 0);
            this.pnUpDowGv.Name = "pnUpDowGv";
            this.pnUpDowGv.Size = new System.Drawing.Size(35, 296);
            this.pnUpDowGv.TabIndex = 17;
            this.pnUpDowGv.Visible = false;
            // 
            // labelControl17
            // 
            this.labelControl17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl17.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl17.Location = new System.Drawing.Point(7, 153);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(20, 13);
            this.labelControl17.TabIndex = 7;
            this.labelControl17.Text = "----";
            // 
            // lb_RowsCount
            // 
            this.lb_RowsCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_RowsCount.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_RowsCount.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_RowsCount.Location = new System.Drawing.Point(10, 172);
            this.lb_RowsCount.Name = "lb_RowsCount";
            this.lb_RowsCount.Size = new System.Drawing.Size(14, 13);
            this.lb_RowsCount.TabIndex = 6;
            this.lb_RowsCount.Text = "00";
            // 
            // lb_STT
            // 
            this.lb_STT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_STT.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_STT.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_STT.Location = new System.Drawing.Point(10, 134);
            this.lb_STT.Name = "lb_STT";
            this.lb_STT.Size = new System.Drawing.Size(14, 13);
            this.lb_STT.TabIndex = 5;
            this.lb_STT.Text = "00";
            // 
            // btn_up
            // 
            this.btn_up.Image = ((System.Drawing.Image)(resources.GetObject("btn_up.Image")));
            this.btn_up.Location = new System.Drawing.Point(2, 73);
            this.btn_up.Name = "btn_up";
            this.btn_up.Size = new System.Drawing.Size(31, 67);
            this.btn_up.TabIndex = 4;
            this.btn_up.Text = "simpleButton7";
            this.btn_up.Click += new System.EventHandler(this.btn_up_Click);
            // 
            // btn_Dow
            // 
            this.btn_Dow.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Dow.Image = ((System.Drawing.Image)(resources.GetObject("btn_Dow.Image")));
            this.btn_Dow.Location = new System.Drawing.Point(2, 155);
            this.btn_Dow.Name = "btn_Dow";
            this.btn_Dow.Size = new System.Drawing.Size(31, 67);
            this.btn_Dow.TabIndex = 3;
            this.btn_Dow.Click += new System.EventHandler(this.btn_Dow_Click);
            // 
            // btn_DowLast
            // 
            this.btn_DowLast.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_DowLast.Image = ((System.Drawing.Image)(resources.GetObject("btn_DowLast.Image")));
            this.btn_DowLast.Location = new System.Drawing.Point(2, 228);
            this.btn_DowLast.Name = "btn_DowLast";
            this.btn_DowLast.Size = new System.Drawing.Size(31, 67);
            this.btn_DowLast.TabIndex = 2;
            this.btn_DowLast.Text = "simpleButton4";
            this.btn_DowLast.Click += new System.EventHandler(this.btn_DowLast_Click);
            // 
            // btn_UpFirst
            // 
            this.btn_UpFirst.Image = ((System.Drawing.Image)(resources.GetObject("btn_UpFirst.Image")));
            this.btn_UpFirst.Location = new System.Drawing.Point(2, 0);
            this.btn_UpFirst.Name = "btn_UpFirst";
            this.btn_UpFirst.Size = new System.Drawing.Size(31, 67);
            this.btn_UpFirst.TabIndex = 0;
            this.btn_UpFirst.Text = "simpleButton1";
            this.btn_UpFirst.Click += new System.EventHandler(this.btn_UpFirst_Click);
            // 
            // pnBtnDeleteGv
            // 
            this.pnBtnDeleteGv.Controls.Add(this.btn_cong);
            this.pnBtnDeleteGv.Controls.Add(this.btn_xoa);
            this.pnBtnDeleteGv.Controls.Add(this.btn_tru);
            this.pnBtnDeleteGv.Controls.Add(this.btn_XoaTatCa);
            this.pnBtnDeleteGv.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBtnDeleteGv.Location = new System.Drawing.Point(0, 296);
            this.pnBtnDeleteGv.Name = "pnBtnDeleteGv";
            this.pnBtnDeleteGv.Size = new System.Drawing.Size(350, 48);
            this.pnBtnDeleteGv.TabIndex = 18;
            this.pnBtnDeleteGv.Visible = false;
            // 
            // btn_cong
            // 
            this.btn_cong.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btn_cong.Appearance.Options.UseFont = true;
            this.btn_cong.Location = new System.Drawing.Point(4, 6);
            this.btn_cong.Name = "btn_cong";
            this.btn_cong.Size = new System.Drawing.Size(75, 35);
            this.btn_cong.TabIndex = 3;
            this.btn_cong.Text = "+";
            this.btn_cong.Click += new System.EventHandler(this.btn_cong_Click);
            // 
            // btn_xoa
            // 
            this.btn_xoa.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_xoa.Appearance.Options.UseFont = true;
            this.btn_xoa.Location = new System.Drawing.Point(170, 6);
            this.btn_xoa.Name = "btn_xoa";
            this.btn_xoa.Size = new System.Drawing.Size(75, 35);
            this.btn_xoa.TabIndex = 2;
            this.btn_xoa.Text = "Xóa";
            this.btn_xoa.Click += new System.EventHandler(this.btn_xoa_Click);
            // 
            // btn_tru
            // 
            this.btn_tru.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btn_tru.Appearance.Options.UseFont = true;
            this.btn_tru.Location = new System.Drawing.Point(87, 6);
            this.btn_tru.Name = "btn_tru";
            this.btn_tru.Size = new System.Drawing.Size(75, 35);
            this.btn_tru.TabIndex = 1;
            this.btn_tru.Text = "-";
            this.btn_tru.Click += new System.EventHandler(this.btn_tru_Click);
            // 
            // btn_XoaTatCa
            // 
            this.btn_XoaTatCa.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_XoaTatCa.Appearance.Options.UseFont = true;
            this.btn_XoaTatCa.Location = new System.Drawing.Point(253, 6);
            this.btn_XoaTatCa.Name = "btn_XoaTatCa";
            this.btn_XoaTatCa.Size = new System.Drawing.Size(93, 35);
            this.btn_XoaTatCa.TabIndex = 0;
            this.btn_XoaTatCa.Text = "Xóa tất cả";
            this.btn_XoaTatCa.Click += new System.EventHandler(this.btn_XoaTatCa_Click);
            // 
            // pnThuNgan
            // 
            this.pnThuNgan.Controls.Add(this.btnBanggia);
            this.pnThuNgan.Controls.Add(this.btnbanphim);
            this.pnThuNgan.Controls.Add(this.btnBanve);
            this.pnThuNgan.Controls.Add(this.btnMoket);
            this.pnThuNgan.Controls.Add(this.btnKetca);
            this.pnThuNgan.Controls.Add(this.btnSoatve);
            this.pnThuNgan.Controls.Add(this.btnTinhtienvetheothoigian);
            this.pnThuNgan.Controls.Add(this.btnInlaive);
            this.pnThuNgan.Controls.Add(this.btnThoat);
            this.pnThuNgan.Controls.Add(this.btnHuyve);
            this.pnThuNgan.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnThuNgan.Location = new System.Drawing.Point(0, 344);
            this.pnThuNgan.Margin = new System.Windows.Forms.Padding(0);
            this.pnThuNgan.Name = "pnThuNgan";
            this.pnThuNgan.Size = new System.Drawing.Size(350, 268);
            this.pnThuNgan.TabIndex = 20;
            // 
            // btnBanggia
            // 
            this.btnBanggia.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnBanggia.Appearance.Options.UseFont = true;
            this.btnBanggia.Appearance.Options.UseTextOptions = true;
            this.btnBanggia.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnBanggia.Location = new System.Drawing.Point(4, 205);
            this.btnBanggia.Name = "btnBanggia";
            this.btnBanggia.Size = new System.Drawing.Size(110, 62);
            this.btnBanggia.TabIndex = 25;
            this.btnBanggia.Text = "Giá bán :\r\nGIÁ BÁN 1";
            this.btnBanggia.Click += new System.EventHandler(this.btnBanggia_Click);
            // 
            // btnbanphim
            // 
            this.btnbanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnbanphim.Appearance.Options.UseFont = true;
            this.btnbanphim.Location = new System.Drawing.Point(236, 137);
            this.btnbanphim.Name = "btnbanphim";
            this.btnbanphim.Size = new System.Drawing.Size(110, 62);
            this.btnbanphim.TabIndex = 24;
            this.btnbanphim.Text = "Bàn phím";
            this.btnbanphim.Click += new System.EventHandler(this.btnbanphim_Click);
            // 
            // btnBanve
            // 
            this.btnBanve.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnBanve.Appearance.Options.UseFont = true;
            this.btnBanve.Location = new System.Drawing.Point(4, 2);
            this.btnBanve.Name = "btnBanve";
            this.btnBanve.Size = new System.Drawing.Size(110, 130);
            this.btnBanve.TabIndex = 23;
            this.btnBanve.Text = "Bán vé";
            this.btnBanve.Click += new System.EventHandler(this.btnBanve_Click);
            // 
            // btnMoket
            // 
            this.btnMoket.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnMoket.Appearance.Options.UseFont = true;
            this.btnMoket.Location = new System.Drawing.Point(120, 137);
            this.btnMoket.Name = "btnMoket";
            this.btnMoket.Size = new System.Drawing.Size(110, 62);
            this.btnMoket.TabIndex = 22;
            this.btnMoket.Text = "Mở két";
            this.btnMoket.Click += new System.EventHandler(this.btnMoket_Click);
            // 
            // btnKetca
            // 
            this.btnKetca.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnKetca.Appearance.Options.UseFont = true;
            this.btnKetca.Location = new System.Drawing.Point(4, 137);
            this.btnKetca.Name = "btnKetca";
            this.btnKetca.Size = new System.Drawing.Size(110, 62);
            this.btnKetca.TabIndex = 21;
            this.btnKetca.Text = "Kết ca";
            this.btnKetca.Click += new System.EventHandler(this.btnKetca_Click);
            // 
            // btnSoatve
            // 
            this.btnSoatve.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnSoatve.Appearance.Options.UseFont = true;
            this.btnSoatve.Location = new System.Drawing.Point(120, 70);
            this.btnSoatve.Name = "btnSoatve";
            this.btnSoatve.Size = new System.Drawing.Size(110, 62);
            this.btnSoatve.TabIndex = 19;
            this.btnSoatve.Text = "Soát vé";
            this.btnSoatve.Click += new System.EventHandler(this.btnSoatve_Click);
            // 
            // btnTinhtienvetheothoigian
            // 
            this.btnTinhtienvetheothoigian.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnTinhtienvetheothoigian.Appearance.Options.UseFont = true;
            this.btnTinhtienvetheothoigian.Appearance.Options.UseTextOptions = true;
            this.btnTinhtienvetheothoigian.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnTinhtienvetheothoigian.Location = new System.Drawing.Point(236, 70);
            this.btnTinhtienvetheothoigian.Name = "btnTinhtienvetheothoigian";
            this.btnTinhtienvetheothoigian.Size = new System.Drawing.Size(110, 62);
            this.btnTinhtienvetheothoigian.TabIndex = 18;
            this.btnTinhtienvetheothoigian.Text = "Tính tiền vé theo thời gian";
            this.btnTinhtienvetheothoigian.Click += new System.EventHandler(this.btnTinhtienvetheothoigian_Click);
            // 
            // btnInlaive
            // 
            this.btnInlaive.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnInlaive.Appearance.Options.UseFont = true;
            this.btnInlaive.Location = new System.Drawing.Point(236, 2);
            this.btnInlaive.Name = "btnInlaive";
            this.btnInlaive.Size = new System.Drawing.Size(110, 62);
            this.btnInlaive.TabIndex = 17;
            this.btnInlaive.Text = "In lại vé";
            this.btnInlaive.Click += new System.EventHandler(this.btnInlaive_Click);
            // 
            // btnThoat
            // 
            this.btnThoat.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnThoat.Appearance.Options.UseFont = true;
            this.btnThoat.Location = new System.Drawing.Point(120, 205);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(226, 62);
            this.btnThoat.TabIndex = 15;
            this.btnThoat.Text = "Đóng";
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // btnHuyve
            // 
            this.btnHuyve.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnHuyve.Appearance.Options.UseFont = true;
            this.btnHuyve.Location = new System.Drawing.Point(120, 2);
            this.btnHuyve.Name = "btnHuyve";
            this.btnHuyve.Size = new System.Drawing.Size(110, 62);
            this.btnHuyve.TabIndex = 12;
            this.btnHuyve.Text = "Hủy vé";
            this.btnHuyve.Click += new System.EventHandler(this.btnHuyve_Click);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton5.Location = new System.Drawing.Point(65, 32);
            this.simpleButton5.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(80, 35);
            this.simpleButton5.TabIndex = 10;
            this.simpleButton5.Text = "&Lưu";
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton6.Location = new System.Drawing.Point(73, 84);
            this.simpleButton6.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(80, 35);
            this.simpleButton6.TabIndex = 11;
            this.simpleButton6.Text = "&Lưu";
            // 
            // simpleButton7
            // 
            this.simpleButton7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton7.Appearance.Options.UseFont = true;
            this.simpleButton7.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton7.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton7.Location = new System.Drawing.Point(81, 92);
            this.simpleButton7.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(80, 35);
            this.simpleButton7.TabIndex = 12;
            this.simpleButton7.Text = "&Lưu";
            // 
            // simpleButton8
            // 
            this.simpleButton8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton8.Appearance.Options.UseFont = true;
            this.simpleButton8.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton8.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton8.Location = new System.Drawing.Point(89, 100);
            this.simpleButton8.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(80, 35);
            this.simpleButton8.TabIndex = 13;
            this.simpleButton8.Text = "&Lưu";
            // 
            // simpleButton9
            // 
            this.simpleButton9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton9.Appearance.Options.UseFont = true;
            this.simpleButton9.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton9.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton9.Location = new System.Drawing.Point(114, 150);
            this.simpleButton9.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(80, 35);
            this.simpleButton9.TabIndex = 14;
            this.simpleButton9.Text = "&Lưu";
            // 
            // panelControl13
            // 
            this.panelControl13.Controls.Add(this.panelControl65);
            this.panelControl13.Controls.Add(this.panelControl69);
            this.panelControl13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl13.Location = new System.Drawing.Point(0, 0);
            this.panelControl13.Name = "panelControl13";
            this.panelControl13.Size = new System.Drawing.Size(1036, 70);
            this.panelControl13.TabIndex = 1;
            // 
            // panelControl65
            // 
            this.panelControl65.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.panelControl65.Controls.Add(this.panelControl66);
            this.panelControl65.Controls.Add(this.panelControl68);
            this.panelControl65.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl65.Location = new System.Drawing.Point(2, 5);
            this.panelControl65.Margin = new System.Windows.Forms.Padding(6);
            this.panelControl65.Name = "panelControl65";
            this.panelControl65.Size = new System.Drawing.Size(1032, 63);
            this.panelControl65.TabIndex = 3;
            // 
            // panelControl66
            // 
            this.panelControl66.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.panelControl66.Controls.Add(this.panelControl67);
            this.panelControl66.Controls.Add(this.lbgiay);
            this.panelControl66.Controls.Add(this.lbCaBan);
            this.panelControl66.Controls.Add(this.lbGio);
            this.panelControl66.Controls.Add(this.lb_NhanVien);
            this.panelControl66.Controls.Add(this.lbtimeby);
            this.panelControl66.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl66.Location = new System.Drawing.Point(198, 2);
            this.panelControl66.Name = "panelControl66";
            this.panelControl66.Size = new System.Drawing.Size(832, 59);
            this.panelControl66.TabIndex = 3;
            // 
            // panelControl67
            // 
            this.panelControl67.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl67.Controls.Add(this.lb_KhoQuay);
            this.panelControl67.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl67.Location = new System.Drawing.Point(2, 2);
            this.panelControl67.Name = "panelControl67";
            this.panelControl67.Size = new System.Drawing.Size(337, 55);
            this.panelControl67.TabIndex = 3;
            // 
            // lb_KhoQuay
            // 
            this.lb_KhoQuay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lb_KhoQuay.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_KhoQuay.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.lb_KhoQuay.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_KhoQuay.Location = new System.Drawing.Point(28, 15);
            this.lb_KhoQuay.Name = "lb_KhoQuay";
            this.lb_KhoQuay.Size = new System.Drawing.Size(52, 24);
            this.lb_KhoQuay.TabIndex = 14;
            this.lb_KhoQuay.Text = "Quầy";
            // 
            // lbgiay
            // 
            this.lbgiay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbgiay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbgiay.Location = new System.Drawing.Point(791, 27);
            this.lbgiay.Name = "lbgiay";
            this.lbgiay.Size = new System.Drawing.Size(18, 19);
            this.lbgiay.TabIndex = 19;
            this.lbgiay.Text = "00";
            // 
            // lbCaBan
            // 
            this.lbCaBan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCaBan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbCaBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbCaBan.Location = new System.Drawing.Point(679, 7);
            this.lbCaBan.Name = "lbCaBan";
            this.lbCaBan.Size = new System.Drawing.Size(39, 19);
            this.lbCaBan.TabIndex = 17;
            this.lbCaBan.Text = "ngay";
            // 
            // lbGio
            // 
            this.lbGio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGio.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGio.Location = new System.Drawing.Point(739, 36);
            this.lbGio.Name = "lbGio";
            this.lbGio.Size = new System.Drawing.Size(46, 19);
            this.lbGio.TabIndex = 20;
            this.lbGio.Text = "00:00";
            // 
            // lb_NhanVien
            // 
            this.lb_NhanVien.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lb_NhanVien.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_NhanVien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_NhanVien.Location = new System.Drawing.Point(354, 5);
            this.lb_NhanVien.Name = "lb_NhanVien";
            this.lb_NhanVien.Size = new System.Drawing.Size(89, 19);
            this.lb_NhanVien.TabIndex = 18;
            this.lb_NhanVien.Text = "Nhân Viên:";
            // 
            // lbtimeby
            // 
            this.lbtimeby.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbtimeby.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbtimeby.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbtimeby.Location = new System.Drawing.Point(354, 34);
            this.lbtimeby.Name = "lbtimeby";
            this.lbtimeby.Size = new System.Drawing.Size(21, 19);
            this.lbtimeby.TabIndex = 16;
            this.lbtimeby.Text = "Ca";
            // 
            // panelControl68
            // 
            this.panelControl68.Controls.Add(this.panel2);
            this.panelControl68.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl68.Location = new System.Drawing.Point(2, 2);
            this.panelControl68.Name = "panelControl68";
            this.panelControl68.Size = new System.Drawing.Size(196, 59);
            this.panelControl68.TabIndex = 21;
            // 
            // panel2
            // 
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(192, 55);
            this.panel2.TabIndex = 22;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(6, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(177, 41);
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // panelControl69
            // 
            this.panelControl69.Appearance.BackColor = System.Drawing.Color.DarkGray;
            this.panelControl69.Appearance.ForeColor = System.Drawing.Color.Black;
            this.panelControl69.Appearance.Options.UseBackColor = true;
            this.panelControl69.Appearance.Options.UseForeColor = true;
            this.panelControl69.Controls.Add(this.panelControl70);
            this.panelControl69.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl69.Location = new System.Drawing.Point(2, 2);
            this.panelControl69.Name = "panelControl69";
            this.panelControl69.Size = new System.Drawing.Size(1032, 3);
            this.panelControl69.TabIndex = 3;
            // 
            // panelControl70
            // 
            this.panelControl70.Appearance.BackColor = System.Drawing.Color.DarkGray;
            this.panelControl70.Appearance.ForeColor = System.Drawing.Color.Black;
            this.panelControl70.Appearance.Options.UseBackColor = true;
            this.panelControl70.Appearance.Options.UseForeColor = true;
            this.panelControl70.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl70.Location = new System.Drawing.Point(2, 2);
            this.panelControl70.Name = "panelControl70";
            this.panelControl70.Size = new System.Drawing.Size(1028, 0);
            this.panelControl70.TabIndex = 4;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Frm_Banve
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 686);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl13);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Banve";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Loại trò chơi - Trò chơi";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frm_Banve_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlTinhtiengio)).EndInit();
            this.pnlTinhtiengio.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl48)).EndInit();
            this.panelControl48.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl49)).EndInit();
            this.panelControl49.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTongtien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl50)).EndInit();
            this.panelControl50.ResumeLayout(false);
            this.panelControl50.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl36)).EndInit();
            this.panelControl36.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl43)).EndInit();
            this.panelControl43.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtThoigian.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl44)).EndInit();
            this.panelControl44.ResumeLayout(false);
            this.panelControl44.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl21)).EndInit();
            this.panelControl21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl28)).EndInit();
            this.panelControl28.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtGiora.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl29)).EndInit();
            this.panelControl29.ResumeLayout(false);
            this.panelControl29.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl37)).EndInit();
            this.panelControl37.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl38)).EndInit();
            this.panelControl38.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtGiovao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl39)).EndInit();
            this.panelControl39.ResumeLayout(false);
            this.panelControl39.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl45)).EndInit();
            this.panelControl45.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl46)).EndInit();
            this.panelControl46.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtGiave.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl47)).EndInit();
            this.panelControl47.ResumeLayout(false);
            this.panelControl47.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl52)).EndInit();
            this.panelControl52.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl54)).EndInit();
            this.panelControl54.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtThoigianquydoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl55)).EndInit();
            this.panelControl55.ResumeLayout(false);
            this.panelControl55.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl51)).EndInit();
            this.panelControl51.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl533)).EndInit();
            this.panelControl533.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTrochoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl53)).EndInit();
            this.panelControl53.ResumeLayout(false);
            this.panelControl53.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl40)).EndInit();
            this.panelControl40.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl41)).EndInit();
            this.panelControl41.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMaveTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl42)).EndInit();
            this.panelControl42.ResumeLayout(false);
            this.panelControl42.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBanve)).EndInit();
            this.pnlBanve.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl58)).EndInit();
            this.panelControl58.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl57)).EndInit();
            this.panelControl57.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdTiente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvtiente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl59)).EndInit();
            this.panelControl59.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl62)).EndInit();
            this.panelControl62.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlMakhachhang)).EndInit();
            this.pnlMakhachhang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl60)).EndInit();
            this.panelControl60.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMakhachhang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl61)).EndInit();
            this.panelControl61.ResumeLayout(false);
            this.panelControl61.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).EndInit();
            this.panelControl16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).EndInit();
            this.panelControl17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTienThoiLai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).EndInit();
            this.panelControl18.ResumeLayout(false);
            this.panelControl18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlQuydoi)).EndInit();
            this.pnlQuydoi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl63)).EndInit();
            this.panelControl63.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTienquydoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl64)).EndInit();
            this.panelControl64.ResumeLayout(false);
            this.panelControl64.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            this.panelControl15.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTienKhachTra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).EndInit();
            this.panelControl14.ResumeLayout(false);
            this.panelControl14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTienve.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            this.panelControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSoatve)).EndInit();
            this.pnlSoatve.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).EndInit();
            this.panelControl22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl26)).EndInit();
            this.panelControl26.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtThongtinve.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl27)).EndInit();
            this.panelControl27.ResumeLayout(false);
            this.panelControl27.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl30)).EndInit();
            this.panelControl30.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl31)).EndInit();
            this.panelControl31.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTrangthai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl32)).EndInit();
            this.panelControl32.ResumeLayout(false);
            this.panelControl32.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl33)).EndInit();
            this.panelControl33.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl34)).EndInit();
            this.panelControl34.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMave.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl35)).EndInit();
            this.panelControl35.ResumeLayout(false);
            this.panelControl35.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKetca)).EndInit();
            this.pnlKetca.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdKetca)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvKetca)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl23)).EndInit();
            this.panelControl23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl24)).EndInit();
            this.panelControl24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtKetca.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl25)).EndInit();
            this.panelControl25.ResumeLayout(false);
            this.panelControl25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl20)).EndInit();
            this.panelControl20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlHuyve)).EndInit();
            this.pnlHuyve.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtLYDO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.panelControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMA_VEBAN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInlaive)).EndInit();
            this.pnlInlaive.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdInlaive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvInlaive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlLoaddanhmuc)).EndInit();
            this.pnlLoaddanhmuc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlTrochoi)).EndInit();
            this.pnlTrochoi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlLoaitrochoi)).EndInit();
            this.pnlLoaitrochoi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlNghiepvu)).EndInit();
            this.pnlNghiepvu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_listProduct)).EndInit();
            this.pn_listProduct.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_SelectProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_SelectProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDowGv)).EndInit();
            this.pnUpDowGv.ResumeLayout(false);
            this.pnUpDowGv.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnBtnDeleteGv)).EndInit();
            this.pnBtnDeleteGv.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnThuNgan)).EndInit();
            this.pnThuNgan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).EndInit();
            this.panelControl13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl65)).EndInit();
            this.panelControl65.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl66)).EndInit();
            this.panelControl66.ResumeLayout(false);
            this.panelControl66.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl67)).EndInit();
            this.panelControl67.ResumeLayout(false);
            this.panelControl67.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl68)).EndInit();
            this.panelControl68.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl69)).EndInit();
            this.panelControl69.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl70)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl pnlNghiepvu;
        private DevExpress.XtraEditors.PanelControl pnlLoaitrochoi;
        private DevExpress.XtraEditors.PanelControl pnlTrochoi;
        private DevExpress.XtraEditors.SimpleButton btnXuongNhomhang;
        private DevExpress.XtraEditors.SimpleButton btnLenNhomhang;
        private System.Windows.Forms.FlowLayoutPanel fpnlTROCHOI;
        private DevExpress.XtraEditors.SimpleButton btnXuongHangHoa;
        private DevExpress.XtraEditors.SimpleButton btnLenHangHoa;
        private System.Windows.Forms.FlowLayoutPanel fpnlLOAITROCHOI;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.PanelControl panelControl13;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.PanelControl pn_listProduct;
        private DevExpress.XtraGrid.GridControl gr_SelectProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView gv_SelectProduct;
        private DevExpress.XtraGrid.Columns.GridColumn SOTT;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn SOLUONG;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn GIABAN;
        private DevExpress.XtraGrid.Columns.GridColumn Thanh_Tien;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;
        private DevExpress.XtraGrid.Columns.GridColumn MA_BEP;
        private DevExpress.XtraGrid.Columns.GridColumn THUE;
        private DevExpress.XtraEditors.PanelControl pnUpDowGv;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl lb_RowsCount;
        private DevExpress.XtraEditors.LabelControl lb_STT;
        private DevExpress.XtraEditors.SimpleButton btn_up;
        private DevExpress.XtraEditors.SimpleButton btn_Dow;
        private DevExpress.XtraEditors.SimpleButton btn_DowLast;
        private DevExpress.XtraEditors.SimpleButton btn_UpFirst;
        private DevExpress.XtraEditors.PanelControl pnBtnDeleteGv;
        private DevExpress.XtraEditors.SimpleButton btn_cong;
        private DevExpress.XtraEditors.SimpleButton btn_xoa;
        private DevExpress.XtraEditors.SimpleButton btn_tru;
        private DevExpress.XtraEditors.SimpleButton btn_XoaTatCa;
        private DevExpress.XtraEditors.PanelControl pnThuNgan;
        private DevExpress.XtraEditors.SimpleButton btnSoatve;
        private DevExpress.XtraEditors.SimpleButton btnTinhtienvetheothoigian;
        private DevExpress.XtraEditors.SimpleButton btnInlaive;
        private DevExpress.XtraEditors.SimpleButton btnThoat;
        private DevExpress.XtraEditors.SimpleButton btnHuyve;
        private DevExpress.XtraEditors.SimpleButton btnInve;
        private DevExpress.XtraEditors.SimpleButton btnMoket;
        private DevExpress.XtraEditors.SimpleButton btnKetca;
        private DevExpress.XtraEditors.SimpleButton btnBanve;
        private DevExpress.XtraEditors.SimpleButton btn5t;
        private DevExpress.XtraEditors.SimpleButton btn5k;
        private DevExpress.XtraEditors.SimpleButton btn2k;
        private DevExpress.XtraEditors.SimpleButton btn1k;
        private DevExpress.XtraEditors.SimpleButton btn500k;
        private DevExpress.XtraEditors.SimpleButton btn200k;
        private DevExpress.XtraEditors.SimpleButton btn100k;
        private DevExpress.XtraEditors.SimpleButton btn50k;
        private DevExpress.XtraEditors.SimpleButton btn20k;
        private DevExpress.XtraEditors.SimpleButton btn10k;
        private DevExpress.XtraEditors.SimpleButton btnNhaplaitienkhachtra;
        private DevExpress.XtraEditors.PanelControl pnlLoaddanhmuc;
        private DevExpress.XtraEditors.PanelControl pnlBanve;
        private DevExpress.XtraEditors.PanelControl pnlInlaive;
        private DevExpress.XtraGrid.GridControl grdInlaive;
        private DevExpress.XtraGrid.Views.Grid.GridView grvInlaive;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_TROCHOI;
        private DevExpress.XtraGrid.Columns.GridColumn GIABAN1;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYBAN;
        private DevExpress.XtraGrid.Columns.GridColumn INVE;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn MA_VEBAN;
        private DevExpress.XtraGrid.Columns.GridColumn TENNHANVIEN;
        private DevExpress.XtraEditors.PanelControl pnlHuyve;
        private DevExpress.XtraEditors.SimpleButton btnbanphim;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton btnHuy;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.MemoEdit txtLYDO;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtMA_VEBAN;
        private DevExpress.XtraEditors.PanelControl panelControl16;
        private DevExpress.XtraEditors.PanelControl panelControl17;
        private DevExpress.XtraEditors.TextEdit txtTienThoiLai;
        private DevExpress.XtraEditors.PanelControl panelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.PanelControl panelControl15;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.TextEdit txtTienKhachTra;
        private DevExpress.XtraEditors.PanelControl panelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.TextEdit txtTienve;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.PanelControl panelControl19;
        private DevExpress.XtraEditors.PanelControl pnlKetca;
        private DevExpress.XtraGrid.GridControl grdKetca;
        private DevExpress.XtraGrid.Views.Grid.GridView grvKetca;
        private DevExpress.XtraGrid.Columns.GridColumn STTKC;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn SL;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn THANHTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.PanelControl panelControl23;
        private DevExpress.XtraEditors.PanelControl panelControl24;
        private DevExpress.XtraEditors.TextEdit txtKetca;
        private DevExpress.XtraEditors.PanelControl panelControl25;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl20;
        private DevExpress.XtraEditors.SimpleButton btnInketca;
        private DevExpress.XtraEditors.PanelControl pnlSoatve;
        private DevExpress.XtraEditors.PanelControl panelControl22;
        private DevExpress.XtraEditors.PanelControl panelControl26;
        private DevExpress.XtraEditors.MemoEdit txtThongtinve;
        private DevExpress.XtraEditors.PanelControl panelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl30;
        private DevExpress.XtraEditors.PanelControl panelControl31;
        private DevExpress.XtraEditors.TextEdit txtTrangthai;
        private DevExpress.XtraEditors.PanelControl panelControl32;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl33;
        private DevExpress.XtraEditors.PanelControl panelControl34;
        private DevExpress.XtraEditors.TextEdit txtMave;
        private DevExpress.XtraEditors.PanelControl panelControl35;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.PanelControl pnlTinhtiengio;
        private DevExpress.XtraEditors.PanelControl panelControl48;
        private DevExpress.XtraEditors.PanelControl panelControl49;
        private DevExpress.XtraEditors.TextEdit txtTongtien;
        private DevExpress.XtraEditors.PanelControl panelControl50;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.PanelControl panelControl45;
        private DevExpress.XtraEditors.PanelControl panelControl46;
        private DevExpress.XtraEditors.TextEdit txtGiave;
        private DevExpress.XtraEditors.PanelControl panelControl47;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.PanelControl panelControl36;
        private DevExpress.XtraEditors.PanelControl panelControl43;
        private DevExpress.XtraEditors.TextEdit txtThoigian;
        private DevExpress.XtraEditors.PanelControl panelControl44;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.PanelControl panelControl21;
        private DevExpress.XtraEditors.PanelControl panelControl28;
        private DevExpress.XtraEditors.TextEdit txtGiora;
        private DevExpress.XtraEditors.PanelControl panelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.PanelControl panelControl37;
        private DevExpress.XtraEditors.PanelControl panelControl38;
        private DevExpress.XtraEditors.TextEdit txtGiovao;
        private DevExpress.XtraEditors.PanelControl panelControl39;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.PanelControl panelControl40;
        private DevExpress.XtraEditors.PanelControl panelControl41;
        private DevExpress.XtraEditors.TextEdit txtMaveTT;
        private DevExpress.XtraEditors.PanelControl panelControl42;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.SimpleButton btnThanhtoan;
        private DevExpress.XtraEditors.PanelControl panelControl51;
        private DevExpress.XtraEditors.PanelControl panelControl533;
        private DevExpress.XtraEditors.TextEdit txtTrochoi;
        private DevExpress.XtraEditors.PanelControl panelControl53;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.PanelControl panelControl52;
        private DevExpress.XtraEditors.PanelControl panelControl54;
        private DevExpress.XtraEditors.TextEdit txtThoigianquydoi;
        private DevExpress.XtraEditors.PanelControl panelControl55;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.PanelControl pnlMakhachhang;
        private DevExpress.XtraEditors.PanelControl panelControl60;
        private DevExpress.XtraEditors.TextEdit txtMakhachhang;
        private DevExpress.XtraEditors.PanelControl panelControl61;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.SimpleButton btnTTTNB;
        private DevExpress.XtraEditors.SimpleButton btnKHTT;
        private DevExpress.XtraEditors.PanelControl panelControl58;
        private DevExpress.XtraEditors.PanelControl panelControl57;
        private DevExpress.XtraEditors.PanelControl panelControl62;
        private DevExpress.XtraGrid.GridControl grdTiente;
        private DevExpress.XtraGrid.Views.Grid.GridView grvtiente;
        private DevExpress.XtraGrid.Columns.GridColumn STTTT;
        private DevExpress.XtraGrid.Columns.GridColumn MA_TIENTE;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_TIENTE;
        private DevExpress.XtraGrid.Columns.GridColumn TYGIA;
        private DevExpress.XtraEditors.PanelControl panelControl59;
        private DevExpress.XtraEditors.SimpleButton btnXuongtiente;
        private DevExpress.XtraEditors.SimpleButton btnLentiente;
        private DevExpress.XtraEditors.PanelControl pnlQuydoi;
        private DevExpress.XtraEditors.PanelControl panelControl63;
        private DevExpress.XtraEditors.TextEdit txtTienquydoi;
        private DevExpress.XtraEditors.PanelControl panelControl64;
        private DevExpress.XtraEditors.LabelControl lblTienquydoi;
        private DevExpress.XtraEditors.PanelControl panelControl65;
        private DevExpress.XtraEditors.PanelControl panelControl66;
        private DevExpress.XtraEditors.PanelControl panelControl67;
        private DevExpress.XtraEditors.LabelControl lb_KhoQuay;
        private DevExpress.XtraEditors.LabelControl lbgiay;
        private DevExpress.XtraEditors.LabelControl lbCaBan;
        private DevExpress.XtraEditors.LabelControl lbGio;
        private DevExpress.XtraEditors.LabelControl lb_NhanVien;
        private DevExpress.XtraEditors.LabelControl lbtimeby;
        private DevExpress.XtraEditors.PanelControl panelControl68;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.PanelControl panelControl69;
        private DevExpress.XtraEditors.PanelControl panelControl70;
        private DevExpress.XtraEditors.SimpleButton btnBanggia;




    }
}