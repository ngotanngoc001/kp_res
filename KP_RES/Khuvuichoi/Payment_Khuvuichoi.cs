﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using KP_RES.Class;
using System.IO;
using KP_Report;

namespace KP_RES 
{
    public partial class Payment_Khuvuichoi : DevExpress.XtraEditors.XtraForm
    {
        // check 
        public bool kt = false;

        #region varriable declaretion
        TextEdit txtInput=new TextEdit();//txt get input
        bool bShift = true;//true is upperText,false is lower text
        bool bText = false;//true is permit input,false is not permit
        bool bPrintBill = true;//true print bill.false is not print bill
        bool bVisaCard = true; //true is not payment visa,false were payment visa
        bool bVoucher = true; //true is not payment Voucher,false were payment Voucher
        string _sIdRoomAndTable = "";
        string sTenBan = "";
        string _sIdBill = "";
        string idKHTT = "";//ID khách hàng thân thiết
        string idVoucher = "";//ID Voucher
        decimal dMoneyVisa = 0;//total payment = visa
        decimal dMoneyVoucher = 0;//total payment = voucher
        decimal dMoneyInnerCard = 0;//total payment = inner card
        decimal dMoneyCash = 0;//total payment = Cash
        public string soThe = "0";
        public string ngayThe = "0";
        public string GH_diachi = "";
        public string GH_MaKH = "";
        public string GH_TenKH = "";
        public string GH_Dienthoai = "";
        public bool GH_chk_diachikhac = false;
        decimal _dMoneygetFirst = 0;
        decimal _dMoneyHour = 0;
        decimal per_Discount = 0;
        decimal per_Extra = 0;
        DataTable dtGridviewProduct=new DataTable();//data list product sale 
        
        public int fastFoot;
        

        string idTrochoi="";
        string idtongtiengio="";
        string MaveTT = "";

        
        #endregion

        #region Event Handling

        #region Form Main
        public Payment_Khuvuichoi(DataTable dtGridview)
        {
            InitializeComponent();
            dtGridviewProduct = dtGridview;
        }
        public Payment_Khuvuichoi(DataTable dtGridview, string _idTrochoi, string _idtongtiengio, string _MaveTT)
        {
            InitializeComponent();
            dtGridviewProduct = dtGridview;
            idTrochoi = _idTrochoi;
            idtongtiengio = _idtongtiengio;
            MaveTT = _MaveTT;
        }

        private void Payment_Load(object sender, EventArgs e)
        {
            LoadDataPrepare();
            LoadForiegnCurrency();
            LoadKeyboardMoney();
            LoadKeyboardNumber();
            LoadKeyboardText();
        }

        #endregion

        #region Click Event

        #region Click Action

        private void btnOpenSafe_Click(object sender, EventArgs e)
        {
            bool check;
            Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
            fr.ShowDialog();
            check = fr.kt;
            fr.Dispose();
            if (check == false)
                return;
            KP_Terminal.clsKP_Terminal.OpenCashDrawer(false );
        }

        private void InputGetMoneyFocus()
        {
            bText = false;
            txtInput = txtInputMoney;
            pnlInputVoucher.Visible = false;
            pnlInputDiscount.Visible = false;
            pnlInputGetMoney.Visible = true;
            pnlDearCustomer.Visible = false;
            pnlInnerCustomer.Visible = false;
            pnlServiceMoney.Visible = false;
            txtInput.Text = "0";
            txtInput.SelectionStart = 1;
            txtInput.Focus();
        }

        private void btnServiceMoney_Click(object sender, EventArgs e)
        {
            //pnlShowInforPayment.BringToFront();
            //bText = false;

            //txtInput = txtServiceMoney;
            //pnlInputVoucher.Visible = false;
            //pnlInputDiscount.Visible = false;
            //pnlInputGetMoney.Visible = false;
            //pnlDearCustomer.Visible = false;
            //pnlInnerCustomer.Visible = false;
            //pnlServiceMoney.Visible = true;
            //txtInput.Text = "0";
            //txtInput.Focus();
            //txtInput.SelectionStart = 1;
        }

        private void btnCancelPayment_Click(object sender, EventArgs e)
        {
            kt = false;
            this.Close();
            this.Dispose();
        }

        private void btnDiscount_Click(object sender, EventArgs e)
        {
            ////bDiscountOrVoucher = true;
            //pnlShowInforPayment.BringToFront();
            //bText = false;
            //txtInput = txtInputDiscount;
            //pnlInputVoucher.Visible = false;
            //pnlInputDiscount.Visible = true;
            //pnlInputGetMoney.Visible = false;
            //pnlDearCustomer.Visible = false;
            //pnlInnerCustomer.Visible = false;
            //pnlServiceMoney.Visible = false;
            //txtInput.Text = "0";
            //txtInput.Focus();
            //txtInput.SelectionStart = 1;

        }

        private void btnRetypePayment_Click(object sender, EventArgs e)
        {
            pnlShowInforPayment.BringToFront();
            bText = false;
            
            txtInput = txtInputMoney;
            pnlInputVoucher.Visible = false;
            pnlInputDiscount.Visible = false;
            pnlInputGetMoney.Visible = true;
            pnlDearCustomer.Visible = false;
            pnlInnerCustomer.Visible = false;
            pnlServiceMoney.Visible = false;
            txtInput.Text = "0";
            txtInput.SelectionStart = 1;
            txtInput.Focus();
            ResetInputMoney();
        }

        private void btnCardImtimateCustomer_Click(object sender, EventArgs e)
        {
            if (!clsUserManagement.CheckActive("4"))
            {
                XtraMessageBox.Show("Bạn không dùng chức năng  khách hàng thân thiết", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            pnlShowInforPayment.BringToFront();
            bText = true;
            txtInput = txtDearCustomer;
            pnlInputVoucher.Visible = false;
            pnlInputDiscount.Visible = false;
            pnlInputGetMoney.Visible = false;
            pnlDearCustomer.Visible = true;
            pnlInnerCustomer.Visible = false;
            pnlServiceMoney.Visible = false;
            txtInput.Text = lbIDDearCustomerShow.Text;
            txtInput.Focus();
            txtInput.SelectionStart = txtInput.Text.Length;
        }

        private void btnCardInternalPayment_Click(object sender, EventArgs e)
        {
            if (!clsUserManagement.CheckActive("8"))
            {
                XtraMessageBox.Show("Bạn không dùng chức năng  khách hàng thanh toán nội bộ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            decimal dMustGetMoney = decimal.Parse(lbTotalMoneyAll.Tag.ToString()) - decimal.Parse(lbGetCustomer.Text.Replace(",", ""));
            if (dMustGetMoney <= 0)
                return;

            pnlShowInforPayment.BringToFront();            
            bText = true;
            txtInput = txtInnerCustomer;
            pnlInputVoucher.Visible = false;
            pnlInputDiscount.Visible = false;
            pnlInputGetMoney.Visible = false;
            pnlDearCustomer.Visible = false;
            pnlInnerCustomer.Visible = true;
            pnlServiceMoney.Visible = false;
            txtInput.Text = lbIdInnerCustomerShow.Text;
            txtInput.Focus();
            txtInput.SelectionStart = txtInput.Text.Length;
        }

        private void btnNotPrintBill_Click(object sender, EventArgs e)
        {
            //bPrintBill = false;
            //PrintBill();
            
        }

        private void btnVoucher_Click(object sender, EventArgs e)
        {
           // bDiscountOrVoucher = false;
            if (bVoucher)
            {
                pnlShowInforPayment.BringToFront();
                bText = true;

                txtInput = txtInputVoucher;
                pnlInputVoucher.Visible = true;
                pnlInputDiscount.Visible = false;
                pnlInputGetMoney.Visible = false;
                pnlDearCustomer.Visible = false;
                pnlInnerCustomer.Visible = false;
                pnlServiceMoney.Visible = false;
                //txtInput.Text = "0";
                txtInput.Focus();
                txtInput.SelectionStart = 1;
            }
        }

        #endregion
        
        #region click keyboard
      

        #region Text Click

        private void btnTranslateText_Click(object sender, EventArgs e)
        {
            
            pnlKeyboardMoney.BringToFront();
            if (txtInput.SelectedText ==txtInput.Text)
            {
                txtInput.Focus();
            }
            else
            {
                int iPosition = txtInput.SelectionStart;
                txtInput.Focus();
                txtInput.SelectionStart = txtInput.Text.Length;
                txtInput.SelectionStart = iPosition;
            }
        }

        private void btnShift_Click(object sender, EventArgs e)
        {
            bShift = !bShift;
            if (bShift)
            {
                btnShift.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            }
            else
            {
                btnShift.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            }
        }

        private void btnOkText_Click(object sender, EventArgs e)
        {
            OkTextBox();
        }

        private void btnA_Click(object sender, EventArgs e)
        {
            InputKeyboardText("A");
        }

        private void btnQ_Click(object sender, EventArgs e)
        {
            InputKeyboardText("Q");
        }

        private void btnW_Click(object sender, EventArgs e)
        {
            InputKeyboardText("W");
        }

        private void btnE_Click(object sender, EventArgs e)
        {
            InputKeyboardText("E");
        }

        private void btnR_Click(object sender, EventArgs e)
        {
            InputKeyboardText("R");
        }

        private void btnT_Click(object sender, EventArgs e)
        {
            InputKeyboardText("T");
        }

        private void btnY_Click(object sender, EventArgs e)
        {
            InputKeyboardText("Y");
        }

        private void btnU_Click(object sender, EventArgs e)
        {
            InputKeyboardText("U");
        }

        private void btnI_Click(object sender, EventArgs e)
        {
            InputKeyboardText("I");
        }

        private void btnO_Click(object sender, EventArgs e)
        {
            InputKeyboardText("O");
        }

        private void btnP_Click(object sender, EventArgs e)
        {
            InputKeyboardText("P");
        }

        private void btnS_Click(object sender, EventArgs e)
        {
            InputKeyboardText("S");
        }

        private void btnD_Click(object sender, EventArgs e)
        {
            InputKeyboardText("D");
        }

        private void btnF_Click(object sender, EventArgs e)
        {
            InputKeyboardText("F");
        }

        private void btnG_Click(object sender, EventArgs e)
        {
            InputKeyboardText("G");
        }

        private void btnH_Click(object sender, EventArgs e)
        {
            InputKeyboardText("H");
        }

        private void btnJ_Click(object sender, EventArgs e)
        {
            InputKeyboardText("J");
        }

        private void btnK_Click(object sender, EventArgs e)
        {
            InputKeyboardText("K");
        }

        private void btnL_Click(object sender, EventArgs e)
        {
            InputKeyboardText("L");
        }

        private void btnM_Click(object sender, EventArgs e)
        {
            InputKeyboardText("M");
        }

        private void btnX_Click(object sender, EventArgs e)
        {
            InputKeyboardText("X");
        }

        private void btnC_Click(object sender, EventArgs e)
        {
            InputKeyboardText("C");
        }

        private void btnV_Click(object sender, EventArgs e)
        {
            InputKeyboardText("V");
        }

        private void btnB_Click(object sender, EventArgs e)
        {
            InputKeyboardText("B");
        }

        private void btnN_Click(object sender, EventArgs e)
        {
            InputKeyboardText("N");
        }

        private void btnDeleteText_Click(object sender, EventArgs e)
        {
            DeleteTextBox();
        }

        private void btnPaymentText_Click(object sender, EventArgs e)
        {
            bPrintBill = true;
            PrintBill();
                 
        }

        private void btnZ_Click(object sender, EventArgs e)
        {
            InputKeyboardText("Z");
        }

        private void btnEscText_Click(object sender, EventArgs e)
        {
            EscapeTextbox();
        }

        private void btnMoveRigthText_Click(object sender, EventArgs e)
        {
            MoveToRight();
        }

        private void btnMoveLeftText_Click(object sender, EventArgs e)
        {
            MoveToLeft();
        }


        #endregion
              
        #region money click

        private void btnPaymentMoney_Click(object sender, EventArgs e)
        {
            bPrintBill = true;
            PrintBill();  
        }

        private void btnTranslateMoney_Click(object sender, EventArgs e)
        {
            pnlKeyboardNumber.BringToFront();
            if (txtInput.SelectedText == txtInput.Text)
            {
                txtInput.Focus();
            }
            else
            {
                int iPosition = txtInput.SelectionStart;
                txtInput.Focus();
                txtInput.SelectionStart = txtInput.Text.Length;
                txtInput.SelectionStart = iPosition;
            }
        }

        private void btn1000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(1000);
        }

        private void btn2000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(2000);
        }

        private void btn5000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(5000);
        }

        private void btn10000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(10000);
        }

        private void btn20000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(20000);
        }

        private void btn50000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(50000);
        }

        private void btn100000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(100000);
        }

        private void btn200000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(200000);
        }

        private void btn500000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(500000);
        }

        private void btnMoveLeftMoney_Click(object sender, EventArgs e)
        {
            MoveToLeft();
        }

        private void btnMoveRightMoney_Click(object sender, EventArgs e)
        {
            MoveToRight();
        }

        private void btnDeleteMoney_Click(object sender, EventArgs e)
        {
            DeleteTextBox();
        }

        private void btnOkMoney_Click(object sender, EventArgs e)
        {
            OkTextBox();
        }

        private void btn500_Click(object sender, EventArgs e)
        {
            PlusInputMoney(500);
        }

        private void btnEcsMoney_Click(object sender, EventArgs e)
        {
            EscapeTextbox();
        }


        #endregion

        #region number click
        private string GetNewCodeBill()
        {
            string _mahoadon = "";
            string ngay = cls_ConfigCashier.idCashier + DateTime.Now.Day.ToString("00") + DateTime.Now.Month.ToString("00") + DateTime.Now.Year.ToString("0000");
            DataTable dtMA = clsMain.ReturnDataTable("select [dbo].[fc_NewcodeHoaDon] ('" + ngay + "')");
            _mahoadon = dtMA.Rows[0][0].ToString();
            return _mahoadon;
        }

        public int checkPrinter = 0;
        private void btnPaymentNumber_Click(object sender, EventArgs e)
        {
            bPrintBill = true;
            PrintBill();
             
        }

        private void btnTranslateNumber_Click(object sender, EventArgs e)
        {
            pnlKeyBoardText.BringToFront();
            txtInput.SelectionStart = txtInput.Text.Length;
            if (txtInput.SelectedText == txtInput.Text)
            {
                txtInput.Focus();
            }
            else
            {
                int iPosition = txtInput.SelectionStart;
                
                txtInput.Focus();
                txtInput.SelectionStart = iPosition;
            }
            
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("7");
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("8");
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("9");
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("4");
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("5");
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("6");
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("1");
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("2");
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("3");
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("0");
        }

        private void btn00_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("00");
        }

        private void btn000_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("000");
        }

        private void btnMoveLeftNumber_Click(object sender, EventArgs e)
        {
            MoveToLeft();
        }

        private void btnMoveRightNumber_Click(object sender, EventArgs e)
        {
            MoveToRight();
        }

        private void btnDeleteNumber_Click(object sender, EventArgs e)
        {
            DeleteTextBox();
        }

        private void btnOkNumber_Click(object sender, EventArgs e)
        {
            OkTextBox();
        }

        private void btnEscNumber_Click(object sender, EventArgs e)
        {
            EscapeTextbox();
        }

        #endregion

        #endregion

        #region for gridview

        private void gvListProduct_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT && e.RowHandle >= 0)
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void gvListForiegnCurrency_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            lbKindMoney.Text = gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString();
            lbcurrencyService.Text = lbcurrencyGetMoney.Text = gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString();
            lbExchangeRate.Text = gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString();

            if (gvListForiegnCurrency.FocusedRowHandle == 0)
            {
                btnUpForeignCurrency.Enabled = false;
                btnDownForeignCurrency.Enabled = true;
            }
            else
                if (gvListForiegnCurrency.FocusedRowHandle == gvListForiegnCurrency.RowCount - 1)
                {
                    btnUpForeignCurrency.Enabled = true;
                    btnDownForeignCurrency.Enabled = false;
                }
                else
                {
                    btnUpForeignCurrency.Enabled = true;
                    btnDownForeignCurrency.Enabled = true;
                }
        }
        private void btnUpForeignCurrency_Click(object sender, EventArgs e)
        {
            if (gvListForiegnCurrency.FocusedRowHandle > 0)
            {
                gvListForiegnCurrency.FocusedRowHandle = gvListForiegnCurrency.FocusedRowHandle - 1;
            }
        }

        private void gvListForiegnCurrency_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT1 && e.RowHandle >= 0)
                e.DisplayText = (e.RowHandle + 1).ToString();
        }
        private void btnDownForeignCurrency_Click(object sender, EventArgs e)
        {
            if (gvListForiegnCurrency.FocusedRowHandle < gvListForiegnCurrency.RowCount - 1)
            {
                gvListForiegnCurrency.FocusedRowHandle = gvListForiegnCurrency.FocusedRowHandle + 1;
            }
        }

        private void gcListProduct_Leave(object sender, EventArgs e)
        {
            if (!btnFirstGridview.Focused && !btnLastGridview.Focused && !btnUpGridview.Focused && !btnDownGridview.Focused)
                pnUpDownGridview.Visible = false;
        }

        private void gvListProduct_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (dtGridviewProduct.Rows.Count >= 7)
            {

                pnUpDownGridview.Visible = true;
            }
        }

        private void btnFirstGridview_Click(object sender, EventArgs e)
        {
            gvListProduct.Focus();
            gvListProduct.FocusedRowHandle = 0;
        }

        private void btnLastGridview_Click(object sender, EventArgs e)
        {
            gvListProduct.Focus();
            gvListProduct.FocusedRowHandle = gvListProduct.RowCount - 1;
        }

        private void btnUpGridview_Click(object sender, EventArgs e)
        {
            if (gvListProduct.FocusedRowHandle > 0)
            {
                gvListProduct.Focus();
                gvListProduct.FocusedRowHandle--;
            }
        }

        private void btnDownGridview_Click(object sender, EventArgs e)
        {
            if (gvListProduct.FocusedRowHandle < gvListProduct.RowCount - 1)
            {
                gvListProduct.Focus();
                gvListProduct.FocusedRowHandle++;
            }
        }

        private void gvListProduct_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            lbRowsCount.Text = gvListProduct.RowCount.ToString();
            lbSTT.Text = (int.Parse(gvListProduct.FocusedRowHandle.ToString()) + 1).ToString();
            if (lbSTT.Text == "1")
            {
                btnFirstGridview.Enabled = btnUpGridview.Enabled = false;
                btnLastGridview.Enabled = btnDownGridview.Enabled = true;
            }
            else
                if (lbSTT.Text == lbRowsCount.Text)
                {
                    btnFirstGridview.Enabled = btnUpGridview.Enabled = true;
                    btnLastGridview.Enabled = btnDownGridview.Enabled = false;
                }
                else
                {
                    btnFirstGridview.Enabled = btnUpGridview.Enabled = true;
                    btnLastGridview.Enabled = btnDownGridview.Enabled = true;
                }
        }

        #endregion

        #region text change

        private void lbTotalMoneyAll_TextChanged(object sender, EventArgs e)
        {
            decimal fReplyCustomer = decimal.Parse(lbGetCustomer.Text.Replace(",", "")) - decimal.Parse(lbTotalMoneyAll.Text.Replace(",", ""));
            lbReplyCustomer.Tag = fReplyCustomer;
            lbReplyCustomer.Text = string.Format("{0:#,###0}", fReplyCustomer);
        }

        private void lbTotalMoneyPayment_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalMoneyPayment();
        }

        private void lbDiscountPayment_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalMoneyPayment();
        }

        private void lbMoneyService_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalMoneyPayment();
        }

        private void lbMoneyGetFirst_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalMoneyPayment();
        }

        private void lbGetCustomer_TextChanged(object sender, EventArgs e)
        {
            //decimal fReplyCustomer = decimal.Parse(lbGetCustomer.Text.Replace(",", "")) - decimal.Parse(lbTotalMoneyAll.Text.Replace(",", ""));
            //lbReplyCustomer.Tag = fReplyCustomer;
            //lbReplyCustomer.Text = string.Format("{0:#,###0}", fReplyCustomer);
        }

        private void lbFirstMoney_TextChanged(object sender, EventArgs e)
        {
            lbGetCustomer.Text = string.Format("{0:#,###0}", lbFirstMoney.Tag);
           
            decimal fReplyCustomer = decimal.Parse(lbGetCustomer.Text.Replace(",", "")) - decimal.Parse(lbTotalMoneyAll.Text.Replace(",", ""));
            lbReplyCustomer.Tag = fReplyCustomer;
            lbReplyCustomer.Text = string.Format("{0:#,###0}", fReplyCustomer);
        }

        private void lbSecondMoney_TextChanged(object sender, EventArgs e)
        {
            lbGetCustomer.Text = string.Format("{0:#,###0}", (decimal.Parse(lbFirstMoney.Tag.ToString()) + decimal.Parse(lbSecondMoney.Tag.ToString())));

            decimal fReplyCustomer = decimal.Parse(lbGetCustomer.Text.Replace(",", "")) - decimal.Parse(lbTotalMoneyAll.Text.Replace(",", ""));
            lbReplyCustomer.Tag = fReplyCustomer;
            lbReplyCustomer.Text = string.Format("{0:#,###0}", fReplyCustomer);
        }

        private void lbThirdMoney_TextChanged(object sender, EventArgs e)
        {
            lbGetCustomer.Text = string.Format("{0:#,###0}", (decimal.Parse(lbFirstMoney.Tag.ToString()) + decimal.Parse(lbSecondMoney.Tag.ToString())) + decimal.Parse(lbThirdMoney.Tag.ToString()));

            decimal fReplyCustomer = decimal.Parse(lbGetCustomer.Text.Replace(",", "")) - decimal.Parse(lbTotalMoneyAll.Text.Replace(",", ""));
            lbReplyCustomer.Tag = fReplyCustomer;
            lbReplyCustomer.Text = string.Format("{0:#,###0}", fReplyCustomer);
        }

        private void lbTheMoney_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalGridview();
        }

        private void lbHourMoney_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalGridview();
        }

        private void lbTotalMoney_TextChanged(object sender, EventArgs e)
        {
            lbTotalMoneyPayment.Tag = lbTotalMoney.Tag;
            lbTotalMoneyPayment.Text = lbTotalMoney.Text;
        }


        private void lbMoneySurplus_SizeChanged(object sender, EventArgs e)
        {
            lbMoneySurplus.Location = new Point(pnlInnerCustomerShow.Width - lbMoneySurplus.Width - 3, 18);
        }

        private void lbMark_SizeChanged(object sender, EventArgs e)
        {
            lbMark.Location = new Point(pnlDearCustomerShow.Width - lbMark.Width - 3, 18);
        }

       

        #endregion

        #region  keypress


        private void Payment_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.F2)
            {
                bPrintBill = true;
                PrintBill();
               
            }
            int iPosition = txtInput.SelectionStart;
            int iLenght = txtInput.Text.Length;
            if (e.KeyCode == Keys.Delete)
            {

                if (iLenght > 2)
                {
                    if (iPosition == 0)
                    {
                        if (txtInput.Text[1] == ',')
                            txtInput.Text = txtInput.Text.Remove(0, 2);
                        else
                            txtInput.Text = txtInput.Text.Remove(0, 1);

                        txtInput.SelectionStart = 0;
                        e.Handled = true;
                    }
                }
            }
            else if (e.KeyCode == Keys.Back)
            {

                if (iPosition == 1)
                {
                    txtInput.Text = txtInput.Text.Remove(iPosition - 1, 1);
                    txtInput.SelectionStart = iPosition - 1;
                }
                else if (iPosition == 2)
                {
                    if (txtInput.Text[1] == ',')
                    {
                        txtInput.Text = txtInput.Text.Remove(iPosition - 2, 2);
                        txtInput.SelectionStart = iPosition - 2;
                    }
                }
            }
        }

        private void txtInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtInput.Text.Length > 15)
            {
                XtraMessageBox.Show("Bạn không được nhập quá 15 ký tự", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
            }
        }

        private void txtServiceMoney_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void txtInnerCustomer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void txtInputMoney_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void txtDearCustomer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void txtInputDiscount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }


        #endregion

        #region Visa event

        private void btnVisa_Click(object sender, EventArgs e)
        {
            if (bVisaCard)
            {
                pnlVisa.BringToFront();
                ResetControlValue(pnlVisa);
                if (txtIdCodeCreate.Text == "")
                {
                    txtIdCodeCreate.Text = CreateIdCode();
                }
            }
        }

        private void bntCancelVisa_Click(object sender, EventArgs e)
        {
            pnlShowInforPayment.BringToFront();
            ResetControlValue(pnlVisa);
            InputGetMoneyFocus();
        }

        private void btnRefeshIdCode_Click(object sender, EventArgs e)
        {

            txtIdCodeCreate.Text = CreateIdCode();
        }

        private bool CheckInputVisa()
        {
            if (txtIdCardVisa.Text == "")
            {
                XtraMessageBox.Show("Bạn Chưa Nhập Số Thẻ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtIdCardVisa.Focus();
                return false;
            }
            if (txtMoneyVisa.Text == "0")
            {
                XtraMessageBox.Show("Bạn Chưa Nhập Số Tiền ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMoneyVisa.Focus();
                return false;
            }
            return true;

        }

        private void btnPaymentVisa_Click(object sender, EventArgs e)
        {
            if (!CheckInputVisa())
            {
                return;
            }
            if (txtIdCodeInput.Text != "")
            {
                if (txtIdCodeInput.Text != txtIdCodeCreate.Text)
                {
                    XtraMessageBox.Show("Mã An Toàn Không Đúng", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtIdCodeInput.Text = "";
                    txtIdCodeInput.Focus();
                    return;
                }
            }

            dMoneyVisa = decimal.Parse(txtMoneyVisa.Text.Replace(",", ""));
            
            if (!pnlFirst.Visible)
            {

                lbFirst.Text = "Tiền Thẻ Visa: " + string.Format("{0:#,###0}", dMoneyVisa) + " VND -->";
                lbFirstMoney.Tag = dMoneyVisa;
                lbFirstMoney.Text = string.Format("{0:#,###0}", dMoneyVisa) + "  VND";
                pnlFirst.Visible = true;
            }
            else
                if (!pnlSecond.Visible)
                {
                    lbSecond.Text = "Tiền Thẻ Visa: " + string.Format("{0:#,###0}", dMoneyVisa) + " VND -->";
                    lbSecondMoney.Tag = dMoneyVisa;
                    lbSecondMoney.Text = string.Format("{0:#,###0}", dMoneyVisa) + "  VND";
                    pnlSecond.Visible = true;

                }
                else
                    if (!pnlThird.Visible)
                    {
                        lbThird.Text = "Tiền Thẻ Visa: " + string.Format("{0:#,###0}", dMoneyVisa) + " VND -->";
                        lbThirdMoney.Tag = dMoneyVisa;
                        lbThirdMoney.Text = string.Format("{0:#,###0}", dMoneyVisa) + "  VND";
                        pnlThird.Visible = true;
                    }
                    else
                    {
                        XtraMessageBox.Show("Tiền Trả Không được nhập quá 3 lần,bạn hãy nhập lại từ đầu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ResetInputMoney();
                    }
            soThe = txtIdCardVisa.Text;
            ngayThe = txtMonth.Text + " - " + txtYear.Text;
            
            InputGetMoneyFocus();
            pnlShowInforPayment.BringToFront();
            ResetControlValue(pnlVisa);
            InputGetMoneyFocus();
            bVisaCard = false;
        }

        private void txtIdCardVisa_GotFocus(object sender, EventArgs e)
        {
            bText = false;
            txtInput = txtIdCardVisa;
        }

        private void txtMoneyVisa_GotFocus(object sender, EventArgs e)
        {
            bText = false;
            txtInput = txtMoneyVisa;
        }

        private void txtMonth_GotFocus(object sender, EventArgs e)
        {
            bText = false;
            txtInput = txtMonth;
        }

        private void txtYear_GotFocus(object sender, EventArgs e)
        {
            bText = false;
            txtInput = txtYear;
        }

        private void txtNameVisa_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtNameVisa;
        }

        private void txtAddress_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtAddress;
        }

        private void txtCwCvc_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtCwCvc;
        }

        private void txtCity_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtCity;
        }

        private void txtIdCodeInput_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtIdCodeInput;
        }

        private void txtCountry_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtCountry;
        }

        #endregion

        #endregion

        #region Orther Event

        private void timer1_Tick(object sender, EventArgs e)
        {
            string sHour, sMinute, sSecond;
            sSecond = clsMain.GetServerDate().Second.ToString();
            sHour = clsMain.GetServerDate().Hour.ToString();
            sMinute = clsMain.GetServerDate().Minute.ToString();
            if (sSecond.Length < 2)
                lbgiay.Text = "0" + sSecond;
            else
                lbgiay.Text = sSecond;
            if (sMinute.Length < 2)
                lbGio.Text = sHour + ":0" + sMinute;
            else
                lbGio.Text = sHour + ":" + sMinute;
        }

        #endregion

        #endregion

        #region Load Data

        private void LoadDataPrepare()
        {
            LoadComboVisa();
            bText = false;
            bShift = true;
            lbInforName.Text = sTenBan ==""? _sIdRoomAndTable:sTenBan ;
            gcListProduct.DataSource = dtGridviewProduct;
            pnlForeignCurrency.Size = new Size((pnlBottomPayment.Width / 100) * 35, 294);
            txtInput = txtInputMoney;
            txtInput.Focus();
            txtInput.SelectionStart = 1;
            pnlServiceMoney.Visible = pnlInnerCustomer.Visible = pnlDearCustomer.Visible = pnlInputDiscount.Visible = pnlInputVoucher.Visible = false;
            pnlInputGetMoney.Visible = true;
            pnlKeyboardMoney.BringToFront();
            pnlShowInforPayment.BringToFront();
            txtInput.KeyPress += new KeyPressEventHandler(txtInput_KeyPress);
            
            //thông tin kho,quầy,ca bán
            timer1.Start();
            lb_KhoQuay.Text = cls_ConfigCashier.nameCashier + "    " + cls_ConfigCashier.nameWarehouse;
            lb_NhanVien.Text = clsUserManagement.ReturnNameOfUserLogin();
            lbCaBan.Text = "Ngày: " + clsMain.GetServerDate().ToString("dd-MM-yyyy");
            lbtimeby.Text = cls_ConfigCashier.sTenCB + ": " + cls_ConfigCashier.sGioBD + " --> " + cls_ConfigCashier.sGioKT;

            //tính tổng tiền
            decimal fTotalMoney = 0;
            if (dtGridviewProduct.Rows.Count > 0)
            {
                for (int i = 0; i < dtGridviewProduct.Rows.Count; i++)
                {
                    fTotalMoney += decimal.Parse(dtGridviewProduct.Rows[i]["Thanh_Tien"].ToString());
                }
            }
            lbTheMoney.Tag = fTotalMoney;
            lbTheMoney.Text = string.Format("{0:#,###0}", fTotalMoney);
            lbMoneyGetFirst.Tag = _dMoneygetFirst;
            lbMoneyGetFirst.Text = string.Format("{0:#,###0}", _dMoneygetFirst);
            lbHourMoney.Tag = _dMoneyHour;
            lbHourMoney.Text = string.Format("{0:#,###0}", _dMoneyHour);
            lbTheMoney.Location = new Point(pnlInforMoney.Width - lbTheMoney.Width-4, 7);
            lbHourMoney.Location = new Point(pnlInforMoney.Width - lbHourMoney.Width-4, 26);
            lbTotalMoney.Location = new Point(pnlInforMoney.Width - lbTotalMoney.Width-4, 45);
          //  lbCurrentcy.Location = new Point(pnlHeaderCurrentcy.Width/2-lbCurrentcy.Width/2, 8);
           
            lbcurrencyService.Location = new Point(pnlServiceMoney.Width - lbcurrencyService.Width+2 , 15);
            lbcurrencyGetMoney.Location = new Point(pnlInputGetMoney.Width - lbcurrencyGetMoney.Width +2, 15);
            int iWidth = pnlMoneyReply.Width + pnlMoneyReply.Location.X - lbVNDReply.Width - pnlKeyboardAndShowForeignCurrency.Location.X;
            txtInnerCustomer.Size = txtDearCustomer.Size = txtInputMoney.Size = txtServiceMoney.Size = txtInputDiscount.Size=txtInputVoucher.Size = new Size(iWidth, 32);
            txtInnerCustomer.Location = txtDearCustomer.Location = txtInputMoney.Location = txtServiceMoney.Location = txtInputDiscount.Location=txtInputVoucher.Location = new Point(pnlKeyboardAndShowForeignCurrency.Location.X - pnlInputDiscount.Location.X, 7);

            txtInput.Text = lbTotalMoneyAll.Text;
            txtInput.SelectAll();
            lbReplyCustomer.Text = "0";
        }

        private void LoadForiegnCurrency()
        {
            DataTable dtForiegnCurrency = clsMain.ReturnDataTable("exec GETTYGIA");
            gcForeignCurrency.DataSource = dtForiegnCurrency;
            if (dtForiegnCurrency.Rows.Count > 0)
            {
                lbKindMoney.Text = dtForiegnCurrency.Rows[0]["MA"].ToString();
                lbExchangeRate.Text = dtForiegnCurrency.Rows[0]["TYGIA"].ToString();
            }
        }

        private void LoadKeyboardMoney()
        {
           
            int iWidth = (pnlKeyboardMoney.Width - 5) / 5;
            int iSurplusWidth = (pnlKeyboardMoney.Width - 5) % 5;
            int iHeight = (pnlKeyboardMoney.Height - 5) / 4;
            int iSurplusHeight = (pnlKeyboardMoney.Height - 5) % 4;
            int iAlignWidth = (10 + iSurplusWidth) / 2;
            int iAlignHeight = (10 + iSurplusHeight) / 2;
            //set size for button
            btnMoveLeftMoney.Size = btnMoveRightMoney.Size = btnDeleteMoney.Size = btnTranslateMoney.Size = btnEcsMoney.Size = btnOkMoney.Size = btn500.Size = btn1000.Size = btn2000.Size = btn5000.Size = btn10000.Size = btn20000.Size = btn50000.Size = btn100000.Size = btn200000.Size = btn500000.Size = new Size(iWidth - 5, iHeight - 5);
            btnPaymentMoney.Size = new Size(iWidth * 2 - 5, iHeight * 2 - 5);
            //set location for button
            //dòng 1
            btn500.Location = new Point(iAlignWidth, iAlignHeight);
            btn1000.Location = new Point(btn500.Location.X + iWidth, btn500.Location.Y);
            btn2000.Location = new Point(btn1000.Location.X + iWidth, btn500.Location.Y);
            btn5000.Location = new Point(btn2000.Location.X + iWidth, btn500.Location.Y);
            btn10000.Location = new Point(btn5000.Location.X + iWidth, btn500.Location.Y);
            //dòng 2
            btn20000.Location = new Point(iAlignWidth, btn500.Location.Y + iHeight);
            btn50000.Location = new Point(btn20000.Location.X + iWidth, btn20000.Location.Y);
            btn100000.Location = new Point(btn50000.Location.X + iWidth, btn20000.Location.Y);
            btn200000.Location = new Point(btn100000.Location.X + iWidth, btn20000.Location.Y);
            btn500000.Location = new Point(btn200000.Location.X + iWidth, btn20000.Location.Y);
            //dòng 3
            btnMoveLeftMoney.Location = new Point(iAlignWidth, btn20000.Location.Y + iHeight);
            btnMoveRightMoney.Location = new Point(btnMoveLeftMoney.Location.X + iWidth, btnMoveLeftMoney.Location.Y);
            btnDeleteMoney.Location = new Point(btnMoveRightMoney.Location.X + iWidth, btnMoveLeftMoney.Location.Y);
            btnPaymentMoney.Location = new Point(btnDeleteMoney.Location.X + iWidth, btnMoveLeftMoney.Location.Y);

            //dòng 4
            btnTranslateMoney.Location = new Point(iAlignWidth, btnMoveLeftMoney.Location.Y + iHeight);
            btnEcsMoney.Location = new Point(btnTranslateMoney.Location.X + iWidth, btnTranslateMoney.Location.Y);
            btnOkMoney.Location = new Point(btnEcsMoney.Location.X + iWidth, btnTranslateMoney.Location.Y);

        }

        private void LoadKeyboardNumber()
        {
           
            int iWidth = (pnlKeyboardNumber.Width - 5) / 6;
            int iSurplusWidth = (pnlKeyboardNumber.Width - 5) % 6;
            int iHeight = (pnlKeyboardNumber.Height - 5) / 4;
            int iSurplusHeight = (pnlKeyboardNumber.Height - 5) % 4;
            int iAlignWidth = (10 + iSurplusWidth) / 2;
            int iAlignHeight = (10 + iSurplusHeight) / 2;
            //set size for button
            btnDeleteNumber.Size=btnMoveLeftNumber.Size=btnMoveRightNumber.Size= btn0.Size = btn00.Size = btn000.Size = btn1.Size = btn2.Size = btn3.Size = btn4.Size = btn5.Size = btn6.Size = btn7.Size = btn8.Size = btn9.Size = btnTranslateNumber.Size =   new Size(iWidth - 5, iHeight - 5);
            btnPaymentNumber.Size = new Size(iWidth * 2 - 5, iHeight * 2 - 5);
            btnEscNumber.Size = new Size(iWidth - 5, iHeight * 2 - 5);
            btnOkNumber.Size = new Size(iWidth * 2 - 5, iHeight - 5);

            //set location for button
            //dòng 1
            btn7.Location = new Point(iAlignWidth, iAlignHeight);
            btn8.Location = new Point(btn7.Location.X + iWidth, btn7.Location.Y);
            btn9.Location = new Point(btn8.Location.X + iWidth, btn7.Location.Y);
            btnMoveLeftNumber.Location = new Point(btn9.Location.X + iWidth, btn7.Location.Y);
            btnMoveRightNumber.Location = new Point(btnMoveLeftNumber.Location.X + iWidth, btn7.Location.Y);
            btnDeleteNumber.Location = new Point(btnMoveRightNumber.Location.X + iWidth, btn7.Location.Y);
            //dòng 2
            btn4.Location = new Point(iAlignWidth, btn7.Location.Y + iHeight);
            btn5.Location = new Point(btn4.Location.X + iWidth, btn4.Location.Y);
            btn6.Location = new Point(btn5.Location.X + iWidth, btn4.Location.Y);
            btnEscNumber.Location = new Point(btn6.Location.X + iWidth, btn4.Location.Y);
            btnOkNumber.Location = new Point(btnEscNumber.Location.X + iWidth, btn4.Location.Y);
            //dòng 3
            btn1.Location = new Point(iAlignWidth, btn4.Location.Y + iHeight);
            btn2.Location = new Point(btn1.Location.X + iWidth, btn1.Location.Y);
            btn3.Location = new Point(btn2.Location.X + iWidth, btn1.Location.Y);
            btnPaymentNumber.Location = new Point(btnOkNumber.Location.X, btn1.Location.Y);

            //dòng 4
            btnTranslateNumber.Location = new Point(iAlignWidth, btn1.Location.Y + iHeight);
            btn0.Location = new Point(btnTranslateNumber.Location.X + iWidth, btnTranslateMoney.Location.Y);
            btn00.Location = new Point(btn0.Location.X + iWidth, btnTranslateMoney.Location.Y);
            btn000.Location = new Point(btn00.Location.X + iWidth, btnTranslateMoney.Location.Y);

        }

        private void LoadKeyboardText()
        {
            
            int iWidth = (pnlKeyBoardText.Width - 5) / 10;
            int iSurplusWidth = (pnlKeyBoardText.Width - 5) % 10;
            int iHeight = (pnlKeyBoardText.Height - 5) / 5;
            int iSurplusHeight = (pnlKeyBoardText.Height - 5) % 5;
            int iAlignWidth = (10 + iSurplusWidth) / 2;
            int iAlignHeight = (10 + iSurplusHeight) / 2;
            //set size for button
            btnQ.Size = btnW.Size = btnE.Size = btnR.Size = btnT.Size = btnY.Size = btnU.Size = btnI.Size = btnO.Size = btnP.Size = btnA.Size = btnS.Size = btnD.Size = btnF.Size = btnG.Size = btnH.Size = btnJ.Size = btnK.Size = btnL.Size = btnZ.Size = btnX.Size = btnC.Size = btnV.Size = btnB.Size = btnN.Size = btnM.Size = btnMoveLeftText.Size=btnMoveRigthText.Size= new Size(iWidth - 5, iHeight - 5);
            btnTranslateText.Size=new Size(iWidth*2-5,iHeight-5);
            btnShift.Size= btnEscText.Size  =btnDeleteText.Size = new Size(iWidth * 3 - 5, iHeight - 5);
            btnPaymentText.Size = new Size(iWidth * 3 - 5, iHeight * 2 - 5);
            btnOkText.Size = new Size(iWidth * 2 - 5, iHeight*2 - 5);

            //set location for button
            //dòng 1
            btnQ.Location = new Point(iAlignWidth, iAlignHeight);
            btnW.Location = new Point(btnQ.Location.X + iWidth, btnQ.Location.Y);
            btnE.Location = new Point(btnW.Location.X + iWidth, btnQ.Location.Y);
            btnR.Location = new Point(btnE.Location.X + iWidth, btnQ.Location.Y);
            btnT.Location = new Point(btnR.Location.X + iWidth, btnQ.Location.Y);
            btnY.Location = new Point(btnT.Location.X + iWidth, btnQ.Location.Y);
            btnU.Location = new Point(btnY.Location.X + iWidth, btnQ.Location.Y);
            btnI.Location = new Point(btnU.Location.X + iWidth, btnQ.Location.Y);
            btnO.Location = new Point(btnI.Location.X + iWidth, btnQ.Location.Y);
            btnP.Location = new Point(btnO.Location.X + iWidth, btnQ.Location.Y);
            //dòng 2
            btnA.Location = new Point(iAlignWidth+iWidth/2, btnQ.Location.Y+iHeight);
            btnS.Location = new Point(btnA.Location.X + iWidth, btnA.Location.Y);
            btnD.Location = new Point(btnS.Location.X + iWidth, btnA.Location.Y);
            btnF.Location = new Point(btnD.Location.X + iWidth, btnA.Location.Y);
            btnG.Location = new Point(btnF.Location.X + iWidth, btnA.Location.Y);
            btnH.Location = new Point(btnG.Location.X + iWidth, btnA.Location.Y);
            btnJ.Location = new Point(btnH.Location.X + iWidth, btnA.Location.Y);
            btnK.Location = new Point(btnJ.Location.X + iWidth, btnA.Location.Y);
            btnL.Location = new Point(btnK.Location.X + iWidth, btnA.Location.Y);
            //dòng 3
            btnZ.Location = new Point(iAlignWidth, btnA.Location.Y + iHeight);
            btnX.Location = new Point(btnZ.Location.X + iWidth, btnZ.Location.Y);
            btnC.Location = new Point(btnX.Location.X + iWidth, btnZ.Location.Y);
            btnV.Location = new Point(btnC.Location.X + iWidth, btnZ.Location.Y);
            btnB.Location = new Point(btnV.Location.X + iWidth, btnZ.Location.Y);
            btnN.Location = new Point(btnB.Location.X + iWidth, btnZ.Location.Y);
            btnM.Location = new Point(btnN.Location.X + iWidth, btnZ.Location.Y);
            btnShift.Location = new Point(btnM.Location.X + iWidth, btnZ.Location.Y);
           

            //dòng 4
            btnMoveLeftText.Location = new Point(iAlignWidth, btnZ.Location.Y + iHeight);
            btnMoveRigthText.Location = new Point(btnMoveLeftText.Location.X + iWidth, btnMoveLeftText.Location.Y);
            btnDeleteText.Location = new Point(btnMoveRigthText.Location.X + iWidth, btnMoveLeftText.Location.Y);
            btnOkText.Location = new Point(btnDeleteText.Location.X + iWidth*3, btnMoveLeftText.Location.Y);
            btnPaymentText.Location = new Point(btnOkText.Location.X + iWidth*2, btnMoveLeftText.Location.Y);
            //dòng 5
            btnTranslateText.Location = new Point(iAlignWidth, btnMoveLeftText.Location.Y + iHeight);
            btnEscText.Location = new Point(btnTranslateText.Location.X + iWidth*2, btnTranslateText.Location.Y);
        }

        private void LoadComboVisa()
        {
            txtIdCardVisa.GotFocus += new EventHandler(txtIdCardVisa_GotFocus);
            txtMoneyVisa.GotFocus += new EventHandler(txtMoneyVisa_GotFocus);
            txtNameVisa.GotFocus += new EventHandler(txtNameVisa_GotFocus);
            txtAddress.GotFocus += new EventHandler(txtAddress_GotFocus);
            txtCwCvc.GotFocus += new EventHandler(txtCwCvc_GotFocus);
            txtCity.GotFocus += new EventHandler(txtCity_GotFocus);
            txtIdCodeInput.GotFocus += new EventHandler(txtIdCodeInput_GotFocus);
            //txtMoneyVisa.GotFocus += new EventHandler(txtMoneyVisa_GotFocus);
            txtMonth.GotFocus += new EventHandler(txtMonth_GotFocus);
            txtYear.GotFocus += new EventHandler(txtYear_GotFocus);
            txtCountry.GotFocus += new EventHandler(txtCountry_GotFocus);
        }

        #endregion

        #region Function

        private void PlusInputMoney(int iMoney)
        {
            if (!bText)
            {
                if (txtInput.Text.Length > 15)
                {
                    int iPositon = txtInput.SelectionStart;
                    XtraMessageBox.Show("Bạn không được nhập quá 15 ký tự", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtInput.SelectionStart = txtInput.Text.Length;
                    txtInput.SelectionStart = iPositon;
                    return;
                }
                if (txtInput.SelectedText==txtInput.Text)
                {
                    txtInput.Text = "0";
                }
                decimal iTotal = decimal.Parse(txtInput.Text.Replace(",", "")) + iMoney;
                txtInput.Text = iTotal.ToString();
                txtInput.SelectionStart = txtInput.Text.Length;
            }
        }//input keyboard for money

        private void MoveToLeft()
        {
            int iPosition = txtInput.SelectionStart;
            if (iPosition > 1)
            {
                if (txtInput.Text[iPosition - 1].ToString() == ",")
                {
                    txtInput.SelectionStart = iPosition - 2;
                }
                else
                {
                    txtInput.SelectionStart = iPosition - 1;
                }
            }
            else
            {
                txtInput.SelectionStart = 0;

            }
        }

        private void MoveToRight()
        {
            int iPosition = txtInput.SelectionStart;
            if (iPosition < txtInput.Text.Length - 1)
            {
                if (txtInput.Text[iPosition + 1].ToString() == ",")
                {
                    txtInput.SelectionStart = iPosition + 2;
                }
                else
                {
                    txtInput.SelectionStart = iPosition + 1;
                }
            }
            else
            {
                txtInput.SelectionStart = txtInput.Text.Length;

            }
        }

        private void DeleteTextBox()
        {
           
            int iPosition = txtInput.SelectionStart;
            int iLenght = txtInput.Text.Length;
            if (iPosition > 1)
            {
                if (txtInput.Text[iPosition - 1].ToString() == ",")
                {
                    txtInput.Text = txtInput.Text.Remove(iPosition - 2, 1);

                    txtInput.SelectionStart = iPosition - 2;

                }
                else
                {
                    txtInput.Text = txtInput.Text.Remove(iPosition - 1, 1);
                    if (iLenght - txtInput.Text.Length == 1)
                    {
                        txtInput.SelectionStart = iPosition - 1;
                    }
                    else
                    {
                        txtInput.SelectionStart = iPosition - 2;
                    }
                }
            }
            else
                if (iPosition == 1)
                {
                    if (iLenght >= 2)
                    {
                        txtInput.Text = txtInput.Text.Remove(iPosition - 1, 1);
                        txtInput.SelectionStart = iPosition - 1;
                        if (txtInput.Text == "0")
                            txtInput.SelectionStart = 1;
                    }
                    else
                    {
                        txtInput.Text = "0";
                        txtInput.SelectionStart = 1;
                    }
                }
        }

        private void OkTextBox()
        {
            if (txtInput.Name == "txtInputMoney")
            {
                if (int.Parse(txtInput.Text.Replace(",", "")) == 0)
                {
                    if (float.Parse(lbTotalMoneyAll.Text) <= 0)
                    {
                        decimal MoneyInputAfterChange = decimal.Parse(txtInput.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                        lbFirst.Text = "Tiền Mặt: " + txtInput.Text + " " + gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString() + "  -->";
                        lbFirstMoney.Tag = MoneyInputAfterChange;
                        lbFirstMoney.Text = string.Format("{0:#,###0}", MoneyInputAfterChange) + "  VND";
                        txtInput.Text = "0";
                        txtInput.SelectionStart = 1;
                        pnlFirst.Visible = true;
                    }
                    else
                        return;
                }
                else if (!pnlFirst.Visible)
                {
                    decimal MoneyInputAfterChange = decimal.Parse(txtInput.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                    lbFirst.Text = "Tiền Mặt: " + txtInput.Text + " " + gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString() + "  -->";
                    lbFirstMoney.Tag = MoneyInputAfterChange;
                    lbFirstMoney.Text = string.Format("{0:#,###0}", MoneyInputAfterChange) + "  VND";
                    txtInput.Text = "0";
                    txtInput.SelectionStart = 1;
                    pnlFirst.Visible = true;
                }
                else
                    if (!pnlSecond.Visible)
                    {
                        decimal MoneyInputAfterChange = decimal.Parse(txtInput.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                        lbSecond.Text = "Tiền Mặt: " + txtInput.Text + " " + gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString() + "  -->";
                        lbSecondMoney.Tag = MoneyInputAfterChange;
                        lbSecondMoney.Text = string.Format("{0:#,###0}", MoneyInputAfterChange) + "  VND";
                        txtInput.Text = "0";
                        txtInput.SelectionStart = 1;
                        pnlSecond.Visible = true;
                    }
                    else
                        if (!pnlThird.Visible)
                        {
                            decimal MoneyInputAfterChange = decimal.Parse(txtInput.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                            lbThird.Text = "Tiền Mặt: " + txtInput.Text + " " + gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString() + "  -->";
                            lbThirdMoney.Tag = MoneyInputAfterChange;
                            lbThirdMoney.Text = string.Format("{0:#,###0}", MoneyInputAfterChange) + "  VND";
                            txtInput.Text = "0";
                            txtInput.SelectionStart = 1;
                            pnlThird.Visible = true;
                        }
                        else
                        {
                            XtraMessageBox.Show("Tiền Trả Không được nhập quá 3 lần,bạn hãy nhập lại từ đầu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ResetInputMoney();
                        }
            }
            else
                if (txtInput.Name == "txtInputDiscount")
                {
                    if (int.Parse(txtInput.Text.Replace(",", "")) == 0)
                        return;
                    if (decimal.Parse(txtInputDiscount.Text.Replace(",", "")) <= 100)
                    {
                        lbDiscountPayment.Tag = (decimal.Parse(lbTotalMoneyPayment.Tag.ToString()) * decimal.Parse(txtInputDiscount.Text.Replace(",", ""))) / 100;
                        lbDiscountPayment.Text = txtInputDiscount.Text;
                        lbDiscount.Text = "        %";
                        per_Discount = decimal.Parse(txtInputDiscount.Text.Replace(",", ""));
                    }
                    else
                    {
                        if (decimal.Parse(txtInputDiscount.Text.Replace(",", "")) > decimal.Parse(lbTotalMoneyPayment.Tag.ToString()))
                        {
                            XtraMessageBox.Show("Lớn hơn số tiền khách trả !","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                            txtInput.Text = "0";
                            return;
                        }
                        lbDiscountPayment.Tag = decimal.Parse(txtInputDiscount.Text.Replace(",", ""));
                        lbDiscountPayment.Text = string.Format("{0:#,###0}", decimal.Parse(lbDiscountPayment.Tag.ToString()));
                        lbDiscount.Text = "   VND";
                        per_Discount = decimal.Parse(txtInputDiscount.Text.Replace(",", "")) * 100 / decimal.Parse(lbTotalMoneyPayment.Tag.ToString());
                    }


                    txtInput.Text = "0";
                    InputGetMoneyFocus();
                }
                else if (txtInput.Name == "txtServiceMoney")
                {
                    if (int.Parse(txtInput.Text.Replace(",", "")) == 0)
                        return;
                    lbServiceMoney.Tag = decimal.Parse(txtServiceMoney.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                    lbServiceMoney.Text = string.Format("{0:#,###0}", decimal.Parse(lbServiceMoney.Tag.ToString()));
                    txtInput.Text = "0";
                    per_Extra = decimal.Parse(lbServiceMoney.Tag.ToString()) * 100 / decimal.Parse(lbTotalMoneyPayment.Tag.ToString());
                    InputGetMoneyFocus();
                }
                else if (txtInput.Name == "txtDearCustomer")
                {
                    if (txtInput.Text == "")
                    {
                        lbIDDearCustomerShow.Text = lbMark.Text = lbNameDearCustomer.Text = "";
                        pnlDearCustomerShow.Visible = false;
                        return;
                    }
                    if (cls_KHTT.CheckKhachHangThanThiet(txtInput.Text))
                    {
                        lbIDDearCustomerShow.Text = txtInput.Text;
                        idKHTT = txtInput.Text;
                        DataTable dtCustomer = clsMain.ReturnDataTable("select khtt.SoDiem,ncc.TEN from khachhang_thanthiet khtt inner join NHACUNGCAP ncc on khtt.Ma_KHTT=ncc.MA where MAKHTT='" + lbIDDearCustomerShow.Text + "'");
                        lbNameDearCustomer.Text = dtCustomer.Rows[0]["TEN"].ToString();
                        lbMark.Text = dtCustomer.Rows[0]["SoDiem"].ToString();
                        pnlDearCustomerShow.Visible = true;
                        InputGetMoneyFocus();
                    }
                    else
                    {
                        XtraMessageBox.Show("Mã KHTT không đúng,bạn hãy kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtInput.Focus();
                    }
                }
                else if (txtInput.Name == "txtInnerCustomer")
                {
                    if (txtInput.Text == "")
                    {
                        return;
                    }
                    if (cls_KHTT.CheckThanhToanNoiBo(txtInput.Text))
                    {

                        dMoneyInnerCard = cls_KHTT.ReturnMoneyMustPay(txtInput.Text, decimal.Parse(lbTotalMoneyAll.Tag.ToString()) - decimal.Parse(lbGetCustomer.Text.Replace(",", "")));
                        if (dMoneyInnerCard == 0)
                        {
                            return;
                        }
                        if (lbIdInnerCustomerShow.Text != "")
                            return;
                        lbIdInnerCustomerShow.Text = txtInput.Text;
                        DataTable dtCustomer = clsMain.ReturnDataTable("select khttnb.SoDuTaiKhoan,ncc.TEN from khachhang_thanhtoan_noibo khttnb inner join NHACUNGCAP ncc on khttnb.Ma_KHTT=ncc.MA where MAKHTTNB='" + lbIdInnerCustomerShow.Text + "'");
                        lbNameInnerCustomer.Text = dtCustomer.Rows[0]["TEN"].ToString();
                        lbMoneySurplus.Text = string.Format("{0:#,###0}", decimal.Parse(dtCustomer.Rows[0]["SoDuTaiKhoan"].ToString())) + " VND";
                        pnlInnerCustomerShow.Visible = true;
                        if (!pnlFirst.Visible)
                        {

                            lbFirst.Text = "Tiền Thẻ TTNB: " + string.Format("{0:#,###0}", dMoneyInnerCard) + " VND -->";
                            lbFirstMoney.Tag = dMoneyInnerCard;
                            lbFirstMoney.Text = string.Format("{0:#,###0}", dMoneyInnerCard) + "  VND";
                            pnlFirst.Visible = true;
                        }
                        else
                            if (!pnlSecond.Visible)
                            {
                                lbSecond.Text = "Tiền Thẻ TTNB: " + string.Format("{0:#,###0}", dMoneyInnerCard) + " VND -->";
                                lbSecondMoney.Tag = dMoneyInnerCard;
                                lbSecondMoney.Text = string.Format("{0:#,###0}", dMoneyInnerCard) + "  VND";
                                pnlSecond.Visible = true;

                            }
                            else
                                if (!pnlThird.Visible)
                                {
                                    lbThird.Text = "Tiền Thẻ TTNB: " + string.Format("{0:#,###0}", dMoneyInnerCard) + " VND -->";
                                    lbThirdMoney.Tag = dMoneyInnerCard;
                                    lbThirdMoney.Text = string.Format("{0:#,###0}", dMoneyInnerCard) + "  VND";
                                    pnlThird.Visible = true;
                                }
                                else
                                {
                                    XtraMessageBox.Show("Tiền trả không được nhập quá 3 lần,bạn hãy nhập lại từ đầu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    ResetInputMoney();
                                }
                        InputGetMoneyFocus();
                    }
                    else
                    {
                        XtraMessageBox.Show("Mã KHTT không dúng,bạn hãy kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtInput.Focus();
                    }
                }
                else if (txtInput.Name == "txtInputVoucher")
                {
                    if (txtInput.Text == "")
                    {
                        return;
                    }
                    else if (cls_KHTT.CheckTheVoucher(txtInput.Text) == 1)
                    {
                        XtraMessageBox.Show("Mã thẻ Voucher không tồn tại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (cls_KHTT.CheckTheVoucher(txtInput.Text) == 2)
                    {
                        XtraMessageBox.Show("Mã thẻ Voucher này đã sử dụng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (cls_KHTT.CheckTheVoucher(txtInput.Text) == 3)
                    {
                        XtraMessageBox.Show("Mã thẻ Voucher chưa được Active", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (cls_KHTT.CheckTheVoucher(txtInput.Text) == 4)
                    {
                        XtraMessageBox.Show("Mã thẻ Voucher này đã hết hạn sử dụng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (cls_KHTT.CheckTheVoucher(txtInput.Text) == 0)
                    {
                        string sSQL = "";
                        sSQL += "Select VALUE_PERCENT,VALUE_AMOUNT From EVOUCHER Where ID =" + clsMain.SQLString(txtInput.Text);
                        DataTable dt = clsMain.ReturnDataTable(sSQL);
                        if (int.Parse(dt.Rows[0]["VALUE_PERCENT"].ToString()) != 0)
                        {
                            bVoucher = false;
                            idVoucher = txtInput.Text;
                            dMoneyVoucher = decimal.Parse(dt.Rows[0]["VALUE_PERCENT"].ToString().Replace(",", "")) * decimal.Parse(lbTotalMoneyAll.Tag.ToString()) / 100;
                            if (!pnlFirst.Visible)
                            {
                                lbFirst.Text = "Voucher: Giảm " + dt.Rows[0]["VALUE_PERCENT"].ToString() + " %" + "  -->";
                                lbFirstMoney.Tag = dMoneyVoucher;
                                lbFirstMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                txtInput.Text = "0";
                                txtInput.SelectionStart = 1;
                                pnlFirst.Visible = true;
                            }
                            else
                                if (!pnlSecond.Visible)
                                {
                                    lbSecond.Text = "Voucher: Giảm " + dt.Rows[0]["VALUE_PERCENT"].ToString() + " %" + "  -->";
                                    lbSecondMoney.Tag = dMoneyVoucher;
                                    lbSecondMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                    txtInput.Text = "0";
                                    txtInput.SelectionStart = 1;
                                    pnlSecond.Visible = true;
                                }
                                else
                                    if (!pnlThird.Visible)
                                    {
                                        lbThird.Text = "Voucher: Giảm " + dt.Rows[0]["VALUE_PERCENT"].ToString() + " %" + "  -->";
                                        lbThirdMoney.Tag = dMoneyVoucher;
                                        lbThirdMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                        txtInput.Text = "0";
                                        txtInput.SelectionStart = 1;
                                        pnlThird.Visible = true;
                                    }
                                    else
                                    {
                                        XtraMessageBox.Show("Tiền Trả Không được nhập quá 3 lần,bạn hãy nhập lại từ đầu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        ResetInputMoney();
                                    }
                            //txtInput.Text = "0";
                            InputGetMoneyFocus();
                        }
                        else
                        {
                            bVoucher = false;
                            idVoucher = txtInput.Text;
                            dMoneyVoucher = decimal.Parse(dt.Rows[0]["VALUE_AMOUNT"].ToString().Replace(",", ""));
                            if (dMoneyVoucher > decimal.Parse(lbTotalMoneyAll.Tag.ToString()))
                                dMoneyVoucher = decimal.Parse(lbTotalMoneyAll.Tag.ToString());
                            if (!pnlFirst.Visible)
                            {
                                lbFirst.Text = "Voucher: Giảm " + string.Format("{0:#,###0}", decimal.Parse(dt.Rows[0]["VALUE_AMOUNT"].ToString())) + " VND" + "  -->";
                                lbFirstMoney.Tag = dMoneyVoucher;
                                lbFirstMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                txtInput.Text = "0";
                                txtInput.SelectionStart = 1;
                                pnlFirst.Visible = true;
                            }
                            else
                                if (!pnlSecond.Visible)
                                {
                                    lbSecond.Text = "Voucher: Giảm " + string.Format("{0:#,###0}", decimal.Parse(dt.Rows[0]["VALUE_AMOUNT"].ToString())) + " VND" + "  -->";
                                    lbSecondMoney.Tag = dMoneyVoucher;
                                    lbSecondMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                    txtInput.Text = "0";
                                    txtInput.SelectionStart = 1;
                                    pnlSecond.Visible = true;
                                }
                                else
                                    if (!pnlThird.Visible)
                                    {
                                        lbThirdMoney.Text = "Voucher: Giảm " + string.Format("{0:#,###0}", decimal.Parse(dt.Rows[0]["VALUE_AMOUNT"].ToString())) + " VND" + "  -->";
                                        lbThirdMoney.Tag = dMoneyVoucher;
                                        lbThirdMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                        txtInput.Text = "0";
                                        txtInput.SelectionStart = 1;
                                        pnlThird.Visible = true;
                                    }
                                    else
                                    {
                                        XtraMessageBox.Show("Tiền Trả Không được nhập quá 3 lần,bạn hãy nhập lại từ đầu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        ResetInputMoney();
                                    }
                            //txtInput.Text = "0";
                            InputGetMoneyFocus();
                        }  
                    }
                }
        }

        private void EscapeTextbox()
        {
            if (txtInput.Name=="txtInputMoney")
            {
                txtInputMoney.Text = "0";
                txtInputMoney.Focus();
                txtInputMoney.SelectionStart = 1;
            }
            else
                if (txtInput.Name == "txtInputDiscount")
                {
                    txtInputDiscount.Text = "0";
                    txtInputDiscount.Focus();
                    txtInputDiscount.SelectionStart = 1;
                }
                else
                if (txtInput.Name == "txtInputVoucher")
                {
                    txtInputVoucher.Text = "0";
                    txtInputVoucher.Focus();
                    txtInputVoucher.SelectionStart = 1;
                }
                else if (txtInput.Name == "txtDearCustomer")
                {
                    txtDearCustomer.Text = "";
                    txtDearCustomer.Focus();
                    txtDearCustomer.SelectionStart =0;
                }
                else if (txtInput.Name == "txtInnerCustomer")
                {
                    txtInnerCustomer.Text = "";
                    txtInnerCustomer.Focus();
                    txtInnerCustomer.SelectionStart = 0;
                }
                
        }

        private void InputKeyboardNumber(string sInput)
        {
            if (txtInput.Text.Length >= 15)
            {
                int iPositon = txtInput.SelectionStart;
                XtraMessageBox.Show("Bạn không được nhập quá 15 ký tự", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtInput.SelectionStart = txtInput.Text.Length;
                txtInput.SelectionStart = iPositon;
                return;
            }

            if (txtInput.Text == "0")
            {
                if (sInput == "0" || sInput == "00" || sInput == "000")
                    return;
            }
            if (txtInput.SelectedText==txtInput.Text)
            {
                txtInput.Text = "0";
            }
            int iLenght, iPosition;
            iLenght = txtInput.Text.Length;
            iPosition = txtInput.SelectionStart;
            if (iPosition == 0)
            {
                if (sInput == "0" || sInput == "00" || sInput == "000")
                    return;
            }

            if(txtInput.Text == "0")
            {
                txtInput.Text = sInput;
            }
            else
            {
                txtInput.Text = txtInput.Text.Insert(iPosition, sInput);
            }
            if (sInput.Length == 1)
            {
                if (txtInput.Text.Length - iLenght == 1)
                {
                    txtInput.SelectionStart = iPosition + 1;
                }
                else
                {
                    txtInput.SelectionStart = iPosition + 2;
                }
            }
            else
                if (sInput.Length == 2)
                {
                    if (txtInput.Text.Length - iLenght == 2)
                    {
                        txtInput.SelectionStart = iPosition + 2;
                    }
                    else
                    {
                        txtInput.SelectionStart = iPosition + 3;
                    }
                }
                else
                    if (sInput.Length == 3)
                    {
                        if (txtInput.Text.Length - iLenght == 3)
                        {
                            txtInput.SelectionStart = iPosition + 3;
                        }
                        else
                        {
                            txtInput.SelectionStart = iPosition + 4;
                        }
                    }


        }//input keyboard for number

        private void InputKeyboardText(string sInput)
        {
            if (bText)
            {
                if (txtInput.Text.Length > 15)
                {
                    int iPositon = txtInput.SelectionStart;
                    XtraMessageBox.Show("Bạn không được nhập quá 15 ký tự", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtInput.SelectionStart = txtInput.Text.Length;
                    txtInput.SelectionStart = iPositon;
                    return;
                }
                if (bShift)
                    sInput = sInput.ToUpper();
                else
                    sInput = sInput.ToLower();
                int iPosition = txtInput.SelectionStart;
                txtInput.Text = txtInput.Text.Insert(iPosition, sInput);
                txtInput.SelectionStart = iPosition + 1;
            }

        }//input keyboard for Text

        private void UpdateTotalMoneyPayment()
        {
            decimal fTotal = decimal.Parse(lbTotalMoneyPayment.Tag.ToString()) + decimal.Parse(lbServiceMoney.Tag.ToString()) - decimal.Parse(lbDiscountPayment.Tag.ToString()) - decimal.Parse(lbMoneyGetFirst.Tag.ToString());
            lbTotalMoneyAll.Tag = fTotal;
            lbTotalMoneyAll.Text = string.Format("{0:#,###0}", fTotal);
            //txtInputMoney.Text = lbTotalMoneyAll.Text;
        }

        private void UpdateTotalGridview()
        {
            decimal fTotal = decimal.Parse(lbTheMoney.Tag.ToString()) + decimal.Parse(lbHourMoney.Tag.ToString());
            lbTotalMoney.Tag=fTotal;
            lbTotalMoney.Text = string.Format("{0:#,###0}", fTotal);
            //txtInputMoney.Text = lbTotalMoneyAll.Text;
        }

        private void NotPrintBill()
        {
            //if (txtInputDiscount.Text.ToString() == "0" && lbGetCustomer.Text == "0")
            //{
            //    XtraMessageBox.Show("Chưa nhập tiền trả!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}
            // double moneyPCard = double.Parse(lbMoneyCard.Text.TrimEnd(new Char[] { 'V', 'N', 'Đ', 'D' }));
            //double numberCard =             
            if (_sIdRoomAndTable == "") //Payment id product and fastfoot
            {
                List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                string idBill = GetNewCodeBill();
                int thu = int.Parse(lbTotalMoneyAll.Text.Replace(",", ""));
                string sqlInserter = "SET DATEFORMAT DMY Insert into HOADON(MA_HOADON, MANHANVIEN, GIAMGIA, PHIPHUCVU, TIEN_TRAKHACH, TIEN_KHACHTRA, TONGTIEN, NGAYTAO, ISPAYMENT, MA_CUAHANG, MA_KHO, MA_QUAY,SOTHETT,NGAYTHETT,TIENTRATHETT,MONEYVISA,MONEYINNERCARD,MONEYVOUCHER,MONEYCASH)";
                sqlInserter += " values(" + "'" + idBill + "'," + int.Parse(KP_UserManagement.clsGlobal.gsMaNVOfUserLogin) + "," + float.Parse(lbDiscountPayment.Text.Replace(",", "")) + "," + float.Parse(lbServiceMoney.Text) + ",";
                sqlInserter += float.Parse(lbReplyCustomer.Text.Replace(",", "")) + "," + float.Parse(lbGetCustomer.Text.Replace(",", "")) + "," + float.Parse(lbTotalMoneyAll.Text.Replace(",", "")) + ",'" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "', " + 1 + ", '" + cls_ConfigCashier.idShop + "', '" + cls_ConfigCashier.idWarehouse + "','" + cls_ConfigCashier.idCashier + "','" + soThe + "','" + ngayThe + "'," + dMoneyVisa + "," + dMoneyVisa + "," + dMoneyInnerCard + "," + dMoneyVoucher + "," + dMoneyCash + ")";
                clsMain.ExecuteSQL(sqlInserter);
                for (int i = 0; i < gvListProduct.RowCount; i++)
                {
                    DM_HangHoa dmHangHoa = new DM_HangHoa();
                    dmHangHoa.STT = (i + 1).ToString();
                    dmHangHoa.Ten_HoaDon = "PHIẾU THANH TOÁN";
                    dmHangHoa.MA_HANGHOA = gvListProduct.GetRowCellValue(i, gvListProduct.Columns["MA_HANGHOA"]).ToString();
                    dmHangHoa.TEN_HANGHOA = gvListProduct.GetRowCellValue(i, gvListProduct.Columns["TEN_HANGHOA"]).ToString();
                    dmHangHoa.SOLUONG = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["SOLUONG"]).ToString());
                    dmHangHoa.GIABAN = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["GIABAN"]).ToString());
                    dmHangHoa.THUE = double.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["THUE"]).ToString());
                    dmHangHoa.Thanh_Tien = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["Thanh_Tien"]).ToString());
                    dmHangHoa.Tong_Cong = lbTotalMoneyAll.Text;
                    dmHangHoa.Tien_TraKhach = lbReplyCustomer.Text;
                    if (lbGetCustomer.Text != "0")
                        dmHangHoa.Tien_KhachTra = lbGetCustomer.Text;
                    else
                    {
                        dmHangHoa.Tien_KhachTra = txtInputMoney.Text;
                        dmHangHoa.Tien_TraKhach = (double.Parse(txtInputMoney.Text.ToString()) - double.Parse(lbTotalMoneyAll.Text.ToString())).ToString();
                        lbReplyCustomer.Text = dmHangHoa.Tien_TraKhach;
                    }
                    dmHangHoa.Phiphucvu = lbServiceMoney.Text;
                    dmHangHoa.Tiendatcoc = lbMoneyGetFirst.Text;
                    if (double.Parse(lbDiscountPayment.Text) >= 100)
                        dmHangHoa.Giam_Gia = lbDiscountPayment.Text;
                    else
                        dmHangHoa.Giam_Gia = lbDiscountPayment.Text + "%"; //lbDiscountPayment
                    dmHangHoa.Ma_HoaDon = idBill;
                    dmHangHoa.Ma_Quay = cls_ConfigCashier.nameCashier;
                    dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                    dmHangHoa.ngayHD = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                    listDM_HangHoa.Add(dmHangHoa);
                    string sql = "SET DATEFORMAT DMY Insert into CT_HOADON( MA_HOADON, MA_HANGHOA,  SOLUONG, GIABAN, THUE)";
                    sql += " values(" + "'" + idBill + "'," + int.Parse(dmHangHoa.MA_HANGHOA) + "," + dmHangHoa.SOLUONG + "," + dmHangHoa.GIABAN + "," + dmHangHoa.THUE + ")";
                    clsMain.ExecuteSQL(sql);
                }
                if (checkPrinter == 0)
                {
                    //convert cvrt = new convert();
                    //// printer bill
                    //DataTable bill = cvrt.ConvertToDataTable(listDM_HangHoa);
                    //KP_ReportBill.Frm_Report1 frm = new KP_ReportBill.Frm_Report1();
                    //frm.ReportName = "rpt_inhoadon";
                    //frm.DataSource = bill;
                    //frm.WindowState = FormWindowState.Minimized;
                    //frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                    //frm.IsPrint = true;
                    //frm.ShowDialog();
                    //frm.Dispose();
                }//               

                if (fastFoot == 1)
                {

                }
                if (fastFoot == 2)
                {
                    //GetListDish();
                    //GetListMon();
                    //dt.Rows.Clear();                   
                }
                //
                string sqlTruTon = string.Format("Exec SP_TRUKHO_DINHLUONG @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 1);
                if (clsMain.ExecuteSQL(sqlTruTon))
                {

                }
                if (clsMain.ExecuteSQL(string.Format("Exec SP_TRUKHO_HOADON @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 1)))
                { }
                return;
            }
            if (_sIdRoomAndTable != "")// Payment have table
            {
                string nameTable = clsMain.ReturnDataTable("select TEN_BAN from BAN where MA_BAN = " + _sIdRoomAndTable + "").Rows[0][0].ToString();
                List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                string idBill = _sIdBill;
                for (int i = 0; i < gvListProduct.RowCount; i++)
                {
                    DM_HangHoa dmHangHoa = new DM_HangHoa();
                    dmHangHoa.STT = (i + 1).ToString();
                    dmHangHoa.Ten_HoaDon = "PHIẾU THANH TOÁN";
                    dmHangHoa.MA_HANGHOA = gvListProduct.GetRowCellValue(i, gvListProduct.Columns["MA_HANGHOA"]).ToString();
                    dmHangHoa.TEN_HANGHOA = gvListProduct.GetRowCellValue(i, gvListProduct.Columns["TEN_HANGHOA"]).ToString();
                    dmHangHoa.SOLUONG = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["SOLUONG"]).ToString());
                    dmHangHoa.GIABAN = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["GIABAN"]).ToString());
                    dmHangHoa.Thanh_Tien = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["Thanh_Tien"]).ToString());
                    dmHangHoa.Tong_Cong = lbTotalMoneyAll.Text;
                    dmHangHoa.Tien_TraKhach = lbReplyCustomer.Text;
                    dmHangHoa.TEN_BAN = nameTable;
                    if (lbGetCustomer.Text != "0")
                        dmHangHoa.Tien_KhachTra = lbGetCustomer.Text;
                    else
                    {
                        dmHangHoa.Tien_KhachTra = txtInputMoney.Text;
                        dmHangHoa.Tien_TraKhach = (double.Parse(txtInputMoney.Text.ToString()) - double.Parse(lbTotalMoneyAll.Text.ToString())).ToString();
                        lbReplyCustomer.Text = dmHangHoa.Tien_TraKhach;
                    }
                    dmHangHoa.Phiphucvu = lbServiceMoney.Text;
                    dmHangHoa.Tiendatcoc = lbMoneyGetFirst.Text;
                    if (double.Parse(lbDiscountPayment.Text) >= 100)
                        dmHangHoa.Giam_Gia = lbDiscountPayment.Text;
                    else
                        dmHangHoa.Giam_Gia = lbDiscountPayment.Text + "%"; //lbDiscountPayment
                    dmHangHoa.Ma_HoaDon = idBill;
                    dmHangHoa.Ma_Quay = cls_ConfigCashier.nameCashier;
                    dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                    dmHangHoa.ngayHD = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                    listDM_HangHoa.Add(dmHangHoa);
                    //Delete dislay order
                    //DeleteOrder(int.Parse(dmHangHoa.MA_HANGHOA), int.Parse(maBan));
                }
                if (checkPrinter == 0)
                {
                //    convert cvrt = new convert();
                //    DataTable bill = cvrt.ConvertToDataTable(listDM_HangHoa);
                //    KP_ReportBill.Frm_Report1 frm = new KP_ReportBill.Frm_Report1();
                //    frm.ReportName = "rpt_inhoadon";
                //    frm.DataSource = bill;
                //    frm.WindowState = FormWindowState.Minimized;
                //    frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                //    frm.IsPrint = true;
                //    frm.ShowDialog();
                //    frm.Dispose();
                }
                txtInputMoney.Text = "";
                int thu = int.Parse(lbTotalMoneyAll.Text.Replace(",", ""));
                //Update Payment Bill                 
                // string maHoaDon = CheckPayMentTable(maBan);
                //string sqlCTMOBAN = "SET DATEFORMAT DMY update CT_MOBAN set GIORA='" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "' where MA_HOADON='" + maHoaDon + "'";
                //clsMain.ExecuteSQL(sqlCTMOBAN);
                string sqlInserter = "SET DATEFORMAT DMY Update HOADON set MA_CUAHANG='" + cls_ConfigCashier.idShop + "', MA_KHO='" + cls_ConfigCashier.idWarehouse + "', MA_QUAY='" + cls_ConfigCashier.idCashier + "',  ISPAYMENT=1, GIAMGIA=" + int.Parse(lbDiscountPayment.Text.Replace(",", "")) + ", TIEN_TRAKHACH =" + int.Parse(lbReplyCustomer.Text.Replace(",", "")) + ", TIEN_KHACHTRA =" + int.Parse(lbGetCustomer.Text.Replace(",", "")) + ", TONGTIEN=" + int.Parse(lbTotalMoneyAll.Text.Replace(",", "")) + ", GIORA='" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "', PHIPHUCVU=" + float.Parse(lbServiceMoney.Text) + "";
                sqlInserter += ",SOTHETT='" + soThe + "',NGAYTHETT='" + ngayThe + "',TIENTRATHETT=" + dMoneyVisa + ",MONEYVISA=" + dMoneyVisa + ",MONEYINNERCARD=" + dMoneyInnerCard + ",MONEYVOUCHER=" + dMoneyVoucher + ",MONEYCASH=" + dMoneyCash + " where MA_HOADON='" + idBill + "'";
                clsMain.ExecuteSQL(sqlInserter);
                soThe = "0";
                ngayThe = "0";
                DataTable dtTables = clsMain.ReturnDataTable("Select MA_BAN From CT_MOBAN where MA_HOADON ='" + idBill + "'");
                if (dtTables.Rows.Count > 0)
                {
                    for (int i = 0; i < dtTables.Rows.Count; i++)
                    {
                        string sql = "Update BAN SET ISMO_BAN=0, SEE_PAYMENTS = 0 where MA_BAN =" + int.Parse(dtTables.Rows[i]["MA_BAN"].ToString());
                        clsMain.ExecuteSQL(sql);
                    }
                }

                // show list table               
                string sqlTruTon = string.Format("Exec SP_TRUKHO_DINHLUONG @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 1);
                if (clsMain.ExecuteSQL(sqlTruTon))
                {

                }
                if (clsMain.ExecuteSQL(string.Format("Exec SP_TRUKHO_HOADON @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 1)))
                { }
            }
        }

        private string CreateIdCode()
        {
            string sRandom = "";
            int[] sRandomArr = new int[70];
            int iCount = 0;
            for (int i = 48; i <= 57; i++)
            {
                sRandomArr[iCount] = i;
                iCount++;
            }
            for (int i = 65; i <= 90; i++)
            {
                sRandomArr[iCount] = i;
                iCount++;
            }
            for (int i = 97; i <= 122; i++)
            {
                sRandomArr[iCount] = i;
                iCount++;
            }
            Random rd = new Random();
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);

            return sRandom;

        }
        /// <summary>
        /// Saver information 
        /// </summary>
        private void PrintAndSaveBill()
        {
            string sSQL = "";
            foreach (DataRow dr in dtGridviewProduct.Rows)
            {
                for (int i = 1; i <= (int)dr["SOLUONG"]; i++)
                {
                    DataTable dt = new DataTable();
                    string MA_VEBAN = "";
                    if (dr["MA_HANGHOA"].ToString() == idTrochoi && dr["GIABAN"].ToString() == idtongtiengio)
                    {
                        MA_VEBAN = MaveTT;
                        sSQL = "";
                        sSQL += "Update VETROCHOI Set " + "\n";
                        sSQL += "GIABAN=" + clsMain.SQLString(dr["GIABAN"].ToString()) + "\n";
                        sSQL += "Where MA_VEBAN=" + clsMain.SQLString(MA_VEBAN) + "\n";
                    }
                    else
                    {
                        MA_VEBAN = cls_ConfigCashier.idCashier + DateTime.Today.Day.ToString("00") + DateTime.Today.Month.ToString("00") + DateTime.Today.Year.ToString("00");
                        dt = clsMain.ReturnDataTable("Select MA_VEBAN From VETROCHOI Where MA_VEBAN Like '" + MA_VEBAN + "%'" + "\n Order by MA_VEBAN DESC");
                        if (dt.Rows.Count <= 0)
                        {
                            MA_VEBAN = MA_VEBAN + "B0001";
                        }
                        else
                        {
                            MA_VEBAN = MA_VEBAN + "B" + (int.Parse(dt.Rows[0][0].ToString().Substring(dt.Rows[0][0].ToString().LastIndexOf("B") + 1, 4)) + 1).ToString("0000");
                        }
                        sSQL = "";
                        sSQL += "Insert into VETROCHOI(MA_VEBAN,MA_TROCHOI,MA_NHANVIEN,MA_QUAY,MA_KHACHHANG,GIABAN)" + "\n";
                        sSQL += "Values ( ";
                        sSQL += clsMain.SQLString(MA_VEBAN) + ",";
                        sSQL += clsMain.SQLString(dr["MA_HANGHOA"].ToString()) + ",";
                        sSQL += clsMain.SQLString(clsUserManagement.ReturnMaNVOfUserLogin()) + ",";
                        sSQL += clsMain.SQLString(cls_ConfigCashier.idCashier) + ",";
                        sSQL += clsMain.SQLString(idKHTT) + ",";
                        sSQL += clsMain.SQLString(dr["GIABAN"].ToString()) + ")";
                    }

                    this.Cursor = Cursors.WaitCursor;
                    Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
                    this.Cursor = Cursors.Default;

                    if (bRunSQL)
                    {
                        sSQL = "";
                        sSQL = "Select A.MA_VEBAN,A.MA_TROCHOI,A.MA_NHANVIEN,A.MA_QUAY,A.GIABAN,A.NGAYBAN,B.TEN_TROCHOI,B.MAYIN,C.TENNHANVIEN" + "\n";
                        sSQL += "From VETROCHOI A,TROCHOI B,DM_NHANVIEN C" + "\n";
                        sSQL += "Where A.MA_TROCHOI = B.MA_TROCHOI" + "\n";
                        sSQL += "And A.MA_NHANVIEN = C.MANHANVIEN" + "\n";
                        sSQL += "And A.MA_VEBAN = " + clsMain.SQLString(MA_VEBAN) + "\n";

                        dt = clsMain.ReturnDataTable(sSQL);

                        Frm_Report1 frm = new Frm_Report1();
                        if (dt.Rows[0]["MAYIN"].ToString() == "0")//VÉ
                        {
                            frm.ReportName = "rptVe";
                        }
                        else if (dt.Rows[0]["MAYIN"].ToString() == "1")//BILL
                        {
                            frm.ReportName = "rptVeKhuvuichoi";
                        }
                        else//MA VACH
                        {
                            frm.ReportName = "rptVeKhuvuichoi";
                        }

                        frm.DataSource = dt;
                        frm.WindowState = FormWindowState.Minimized;
                        if (dt.Rows[0]["MAYIN"].ToString() == "0")
                        {
                            frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinve;
                        }
                        else if (dt.Rows[0]["MAYIN"].ToString() == "1")
                        {
                            frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                        }
                        else
                        {
                            frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinmavach;
                        }
                        frm.IsPrint = true;
                        frm.ShowDialog();
                        frm.Dispose();
                    }
                    else
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }

            if (idVoucher != "")
            {
                sSQL = "Update EVOUCHER Set STATUS=1 Where ID=" + clsMain.SQLString(idVoucher);
                clsMain.ExecuteSQL(sSQL);
            }
            return;
        }
        private void PrintBill()
        {
            if (pnlInputGetMoney.Visible)
            {
                if (!pnlFirst.Visible)
                {
                    OkTextBox();
                }
            }
            if (decimal.Parse(lbReplyCustomer.Tag.ToString()) < 0)
            {
                XtraMessageBox.Show("Chưa thanh toán hết tiền,yêu cầu nhập số tiền thanh toán", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dMoneyVoucher >decimal.Parse( lbTotalMoneyAll.Tag.ToString()))
            {
                dMoneyCash =  decimal.Parse(lbTotalMoneyAll.Tag.ToString())-dMoneyVoucher ;
                dMoneyVisa = 0;
                dMoneyInnerCard = 0;
                
            }
            else
                if ((dMoneyVoucher + dMoneyVisa) > decimal.Parse(lbTotalMoneyAll.Tag.ToString()))
                {
                    dMoneyCash = decimal.Parse(lbTotalMoneyAll.Tag.ToString()) - (dMoneyVoucher + dMoneyVisa);
                    dMoneyInnerCard = 0;
                }
                else
                {
                    dMoneyCash = decimal.Parse(lbTotalMoneyAll.Tag.ToString()) - (dMoneyVoucher + dMoneyVisa + dMoneyInnerCard);
                }
            if (lbIdInnerCustomerShow.Text != "")
            {
                cls_KHTT.UpdateInnerCustomer(lbIdInnerCustomerShow.Text, dMoneyInnerCard);
            }
            if (lbIDDearCustomerShow.Text != "")
            {
                cls_KHTT.CongDiemKhachHangThanThiet(lbIDDearCustomerShow.Text, decimal.Parse(lbTotalMoneyAll.Tag.ToString()));
            }

            if (bPrintBill)
            {
                PrintAndSaveBill();
            }

            kt = true;
            this.Close();          
        }
        #endregion

        #region reset control,data

        private void ResetInputMoney()
        {
            bVoucher = bVisaCard = true;
            lbThirdMoney.Tag = lbSecondMoney.Tag = lbFirstMoney.Tag = 0;
            lbFirst.Text = lbFirstMoney.Text = lbSecond.Text = lbSecondMoney.Text = lbThird.Text = lbThirdMoney.Text = string.Empty;
            pnlFirst.Visible = pnlSecond.Visible = pnlThird.Visible = false;
           
            lbNameInnerCustomer.Text = lbIdInnerCustomerShow.Text = lbMoneySurplus .Text= "";
            pnlInnerCustomerShow.Visible = false;
            txtInputMoney.Text = "0";
            txtInputMoney.SelectionStart = 1;
            txtDearCustomer.Text = txtInnerCustomer.Text = "";
            idVoucher = "";
            dMoneyInnerCard = 0;
            dMoneyVisa = 0;
            dMoneyVoucher = 0;
            dMoneyCash = 0;
        }

        public static void ResetControlValue(Control parent)
        {

            foreach (Control c in parent.Controls)
            {
                if (c.Controls.Count > 0)
                {
                    switch (c.GetType().ToString())
                    {
                        case "DevExpress.XtraEditors.TextEdit":
                            ((TextEdit)c).Text = "";
                            break;
                        case "DevExpress.XtraEditors.DateEdit":
                            ((DateEdit)c).EditValue = clsMain.GetServerDate();
                            break;
                        case "DevExpress.XtraEditors.CheckEdit":
                            ((CheckEdit)c).Checked = true;
                            break;
                        case "DevExpress.XtraEditors.LookupEdit":
                            ((LookUpEdit)(c)).EditValue = ((LookUpEdit)c).Properties.GetDataSourceValue(((LookUpEdit)c).Properties.ValueMember, 0);
                            break;
                        case "DevExpress.XtraEditors.GridLookUpEdit":
                            ((GridLookUpEdit)(c)).EditValue = ((GridLookUpEdit)c).Properties.GetKeyValue(0);
                            break;
                        default:
                            ResetControlValue(c);
                            break;
                    }
                }
                else
                {
                    switch (c.GetType().ToString())
                    {
                        case "DevExpress.XtraEditors.CheckEdit":
                            ((CheckEdit)c).Checked = true;
                            break;

                        default:
                            break;
                    }
                }
            }
        }

        #endregion

        private void txtInputVoucher_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void btnGiaohang_Click(object sender, EventArgs e)
        {
            //if (GH_diachi != "")
            //{
            //    Frm_Giaohang frm = new Frm_Giaohang(GH_MaKH, GH_chk_diachikhac, GH_diachi);
            //    frm.ShowDialog();
            //    GH_diachi = frm.sdiachi;
            //    GH_MaKH = frm.sMaKH;
            //    GH_TenKH = frm.sTenKH;
            //    GH_Dienthoai = frm.sDienthoai;
            //    GH_chk_diachikhac = frm.chkdiachikhac;
            //}
            //else
            //{
            //    Frm_Giaohang frm = new Frm_Giaohang();
            //    frm.ShowDialog();
            //    GH_diachi = frm.sdiachi;
            //    GH_MaKH = frm.sMaKH;
            //    GH_TenKH = frm.sTenKH;
            //    GH_Dienthoai = frm.sDienthoai;
            //    GH_chk_diachikhac = frm.chkdiachikhac;
            //}
        }

        private void pnlShowInforPayment_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}