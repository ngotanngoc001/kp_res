﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;
using KP_RES.Class;

namespace KP_RES 
{
    public partial class Frm_DSHoaDon_Karaoke : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt;

        public Frm_DSHoaDon_Karaoke()
        {
            InitializeComponent();
            dtpTungay.EditValue = clsGlobal.gdServerDate;
            dtpDenngay.EditValue = clsGlobal.gdServerDate;
        }

        private void Frm_DSHoaDon_Load(object sender, EventArgs e)
        {
            LoadcboDATA();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                FILL.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
                FILL.Visible = true ;
            }
        }

        private void btnChitiet_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle < 0)
                    throw new Exception("Chưa chọn hóa đơn bán nào cần xem");

                Frm_BCChitietHoaDon_Karaoke frm = new Frm_BCChitietHoaDon_Karaoke(gridView1.GetFocusedRowCellValue(MA_HOADON).ToString());
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.WindowState = FormWindowState.Maximized;
                frm.ShowDialog();
                frm.Dispose();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                    string sql = "Exec SP_INHOADON_DSHoaDon_Karaoke @Mahoadon='" + gridView1.GetFocusedRowCellValue(MA_HOADON).ToString() + "'";
                    DataTable dt2 = clsMain.ReturnDataTable(sql);

                    string sqlKaraoke = string.Empty;
                    sqlKaraoke = "SELECT NGAYTAO, MA_HOADON, B.MA_BAN, B.TEN_BAN as TENPHONG, THOIGIANVAO, THOIGIANRA, BLOCKKARAOKE, SOPHUT, SOBLOCK, GIABLOCK, THANHTIEN" + "\n";
                    sqlKaraoke += "FROM CT_TIENGIOKARAOKE CT LEFT JOIN BAN B ON CT.MA_PHONG = B.MA_BAN" + "\n";
                    sqlKaraoke += "WHERE MA_HOADON = '" + gridView1.GetFocusedRowCellValue(MA_HOADON).ToString() + "'";
                    DataTable dtKaraoke = clsMain.ReturnDataTable(sqlKaraoke);

                    int n = dt2.Rows.Count;
                    for (int i = 0; i < n; i++)
                    {
                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        dmHangHoa.STT = (i + 1).ToString();
                        dmHangHoa.Ten_HoaDon = "HÓA ĐƠN BÁN LẺ";
                        dmHangHoa.MA_HANGHOA = dt2.Rows[i]["MA_HANGHOA"].ToString();
                        dmHangHoa.TEN_HANGHOA = dt2.Rows[i]["TEN_HANGHOA"].ToString();
                        dmHangHoa.SOLUONG = decimal.Parse(dt2.Rows[i]["SOLUONG"].ToString());
                        dmHangHoa.GIABAN = decimal.Parse(dt2.Rows[i]["GIABAN"].ToString());
                        //dmHangHoa.THUE = double.Parse(dt2.Rows[i]["THUE"].ToString());
                        dmHangHoa.Thanh_Tien = decimal.Parse(dt2.Rows[i]["Thanh_Tien"].ToString());
                        dmHangHoa.Tong_Cong = string.Format("{0:#,###0}", dt2.Rows[i]["Tong_Cong"]);
                        dmHangHoa.Tien_TraKhach = string.Format("{0:#,###0}", dt2.Rows[i]["Tien_TraKhach"]);
                        dmHangHoa.Tien_KhachTra = string.Format("{0:#,###0}", dt2.Rows[i]["Tien_KhachTra"]);
                        dmHangHoa.Phiphucvu = string.Format("{0:#,###0}", dt2.Rows[i]["Phiphucvu"]);
                        dmHangHoa.Giam_Gia = string.Format("{0:#,###0}", dt2.Rows[i]["GIAMGIA"]);
                        dmHangHoa.Ma_HoaDon = dt2.Rows[i]["MA_HOADON"].ToString();
                        dmHangHoa.TienGio = string.Format("{0:#,###0}", dt2.Rows[i]["TIENGIO"]);
                        dmHangHoa.Ma_Quay = dt2.Rows[i]["TEN_QUAY"].ToString();
                        dmHangHoa.TenNV = dt2.Rows[i]["TENNV"].ToString();
                        dmHangHoa.ngayHD =string.Format("{0:dd/MM/yyyy HH:mm:ss}", dt2.Rows[i]["NGAYHD"]);
                        dmHangHoa.MaBan = dtKaraoke.Rows[0]["MA_BAN"].ToString();
                        dmHangHoa.Giovao = Convert.ToDateTime(dtKaraoke.Rows[0]["THOIGIANVAO"]);
                        dmHangHoa.Giora = Convert.ToDateTime(dtKaraoke.Rows[0]["THOIGIANRA"]);
                        listDM_HangHoa.Add(dmHangHoa);
                    }
                    
                    convert cvrt = new convert();
                    // printer bill
                    DataTable bill = cvrt.ConvertToDataTable(listDM_HangHoa);
                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

                    frm.dtSource = bill;
                    frm.Mode = 119;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception EX)
            {
                XtraMessageBox.Show(EX.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally { this.Cursor = Cursors.Default; }   
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
                panelControl7.Visible = false;
                panelControl8.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
                panelControl7.Visible = true;
                panelControl8.Visible = true;
            }
            this.Refresh();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            panelControl9.Visible = true;
            if (optGroup.SelectedIndex == 6)//Hóa đơn giảm giá
            {
                panelControl9.Visible = false;
            }
            LoadcboDATA();
        }

        private void LoadcboDATA()
        {
            cboDATA.Properties.DataSource = null;
            cboDATA.Properties.Columns.Clear();
            cboDATA.ItemIndex = -1;
            if (optGroup.SelectedIndex == 0)//cửa hàng
            {
                dt = clsMain.ReturnDataTable("select MA_CUAHANG as MA,TEN_CUAHANG as TEN from CUAHANG where SUDUNG=1 order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên cửa hàng" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Cửa hàng";
            }
            else if (optGroup.SelectedIndex == 1)//kho
            {
                dt = clsMain.ReturnDataTable("select MA_KHO as MA,TEN_KHO as TEN from KHO where SUDUNG=1 order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên kho" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Kho";
            }
            else if (optGroup.SelectedIndex == 2)//quầy
            {
                dt = clsMain.ReturnDataTable("select MA_QUAY as MA,TEN_QUAY as TEN from QUAY where SUDUNG=1 order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên quầy" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Quầy";
            }
            else if (optGroup.SelectedIndex == 3)//nhóm hàng
            {
                dt = clsMain.ReturnDataTable("select MA_NHOMHANG as MA,TEN_NHOMHANG as TEN from NHOMHANG where SUDUNG=1 order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên nhóm" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Nhóm hàng";
            }
            else if (optGroup.SelectedIndex == 4)//hàng hóa
            {
                dt = clsMain.ReturnDataTable("select MA_HANGHOA as MA,TEN_HANGHOA as TEN from HANGHOA where SUDUNG=1 order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên mặt hàng" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Mặt hàng";
            }
            else if (optGroup.SelectedIndex == 5)//nhân viên
            {
                dt = clsMain.ReturnDataTable("select MANHANVIEN as MA,TENNHANVIEN as TEN from DM_NHANVIEN where SUDUNG=1 order by TEN");
                cboDATA.Properties.DataSource = dt;
                taocolumncholookupedit(new string[] { "Mã", "Tên nhân viên" }, new string[] { "MA", "TEN" }, new bool[] { true, true }, dt);
                lblData.Text = "Nhân viên";
            }
            cboDATA.Properties.AutoSearchColumnIndex = 1;
        }

        private void taocolumncholookupedit(String[] Caption, String[] FieldName, bool[] visible, DataTable dt)
        {
            if (Caption.Length > 1 & dt.Rows.Count > 0)
            {
                for (int i = 0; i < Caption.Length; i++)
                {
                    LookUpColumnInfo info = new LookUpColumnInfo();
                    info.FieldName = FieldName[i].TrimEnd();
                    info.Caption = Caption[i].TrimEnd();
                    info.Visible = visible[i];
                    info.Alignment = DevExpress.Utils.HorzAlignment.Default;
                    cboDATA.Properties.Columns.Add(info);
                }
                cboDATA.Properties.BestFitMode = BestFitMode.None;
                cboDATA.Properties.SearchMode = SearchMode.AutoFilter;
                cboDATA.Properties.DisplayMember = FieldName[1];
                cboDATA.Properties.ValueMember = FieldName[0];
                if (visible[0] == true)
                {
                    cboDATA.Properties.Columns[0].Width = 100;
                    cboDATA.Properties.Columns[1].Width = 500;
                }
                else
                {
                    cboDATA.Properties.Columns[1].Width = 100;
                    cboDATA.Properties.Columns[2].Width = 500;
                }
                if (dt.Rows.Count > 0)
                    cboDATA.EditValue = int.Parse(dt.Rows[0][0].ToString());
            }
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            string sql = string.Empty;
            try
            {
                if (cboDATA.EditValue == null || cboDATA.EditValue == DBNull.Value)
                    throw new Exception("Chưa chọn đối tượng cần xem");

                sql += " SELECT DISTINCT HD.MA_HOADON, HD.NGAYTAO, HD.TONGTIEN,( SELECT TENNHANVIEN FROM DM_NHANVIEN WHERE MANHANVIEN=HD.MANHANVIEN) AS TenNV, HD.TONGTIEN AS MONEYCASH ";
                sql += " FROM HOADON_KARAOKE HD  INNER JOIN CT_HOADON_KARAOKE CTHD ON HD.MA_HOADON = CTHD.MA_HOADON INNER JOIN HANGHOA HH ON CTHD.MA_HANGHOA = HH.MA_HANGHOA ";
                sql += " WHERE Convert(datetime,CAST(HD.NGAYTAO as Date),103) between Convert(datetime,'" + Convert.ToDateTime(dtpTungay.EditValue).ToString("dd/MM/yyyy") + "',103) and Convert(datetime,'" + Convert.ToDateTime(dtpDenngay.EditValue).ToString("dd/MM/yyyy") + "',103)";
                sql += " AND  HD.ISPAYMENT='1'";

                if (optGroup.SelectedIndex == 0)//Cửa hàng
                {
                    sql += " AND  HD.MA_CUAHANG='" + cboDATA.EditValue.ToString() + "'";
                }
                else if (optGroup.SelectedIndex == 1)//Kho
                {
                    sql += " AND  HD.MA_KHO='" + cboDATA.EditValue.ToString() + "'";
                }
                else if (optGroup.SelectedIndex == 2)//Quầy
                {
                    sql += " AND  HD.MA_QUAY='" + cboDATA.EditValue.ToString() + "'";
                }
                else if (optGroup.SelectedIndex == 3)//Nhóm hàng
                {
                    sql += " AND  HH.MA_NHOMHANG='" + cboDATA.EditValue.ToString() + "'";
                }
                else if (optGroup.SelectedIndex == 4)//Hàng hóa
                {
                    sql += " AND  CTHD.MA_HANGHOA='" + cboDATA.EditValue.ToString() + "'";
                }
                else if (optGroup.SelectedIndex == 5)//Nhân viên
                {
                    sql += " AND  HD.MANHANVIEN='" + cboDATA.EditValue.ToString() + "'";
                }
                else if (optGroup.SelectedIndex == 6)//Hóa đơn giảm giá
                {
                    sql += " AND  HD.GIAMGIA<>0";
                }
                sql += "\n ORDER BY HD.MA_HOADON";
                
                dt = clsMain.ReturnDataTable(sql);
                gridControl1.DataSource = dt;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnprint_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    DataColumn a = new DataColumn();
                    a.ColumnName = "FILTER";
                    a.DataType = System.Type.GetType("System.String");
                    a.DefaultValue = optGroup.Properties.Items[optGroup.SelectedIndex].Description + " : " + cboDATA.Text;
                    if (ContainColumn("FILTER", dt) == false)
                        dt.Columns.Add(a);
                    DataColumn a1 = new DataColumn();
                    a1.ColumnName = "TUNGAY";
                    a1.DefaultValue = string.Format("{0:dd/MM/yyyy}", dtpTungay.EditValue);
                    if (ContainColumn("TUNGAY", dt) == false)
                        dt.Columns.Add(a1);
                    DataColumn a2 = new DataColumn();
                    a2.ColumnName = "DENNGAY";
                    a2.DefaultValue = string.Format("{0:dd/MM/yyyy}", dtpDenngay.EditValue);
                    if (ContainColumn("DENNGAY", dt) == false)
                        dt.Columns.Add(a2);


                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = dt;
                    frm.Mode = 46;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show("Không có dữ liệu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception EX)
            {
                XtraMessageBox.Show(EX.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally { this.Cursor = Cursors.Default; }   
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}