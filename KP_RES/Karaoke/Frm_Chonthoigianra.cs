﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES
{
    public partial class Frm_Chonthoigianra : DevExpress.XtraEditors.XtraForm
    {
        public DateTime giovao;
        public DateTime giora;

        public Frm_Chonthoigianra()
        {
            InitializeComponent();
            
        }

        string _thongbao = "";
        private void Frm_Chonthoigianra_Load(object sender, EventArgs e)
        {
            string sSQL = "Select GIATRI from CAUHINH where TEN = 'CAUHINHGIOBIDA'";
            DataTable myDT = clsMain.ReturnDataTable(sSQL);
            int iGiatri = int.Parse(myDT.Rows[0]["GIATRI"].ToString());
            if (iGiatri == 0)
            {
                dtpGiora.Enabled = false;
                dtpGiora.ForeColor = Color.Black;
                dtpGiovao.Enabled = false;
                dtpGiovao.ForeColor = Color.Black;
            }
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            this.Text = rm.GetString("nhapthoigian", culture);
           
            _thongbao = rm.GetString("thongbao", culture);
            labelControl2.Text = rm.GetString("giobatdau", culture);
            labelControl1.Text = rm.GetString("gioketthuc", culture);
            dtpGiovao.EditValue = giovao;
            dtpGiora.EditValue = clsMain.GetServerDate();
        }

        private void btnDongy_Click(object sender, EventArgs e)
        {
            try
            {
                if (checkinput())
                {
                    giovao = Convert.ToDateTime(dtpGiovao.EditValue);
                    giora = Convert.ToDateTime(dtpGiora.EditValue);
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK);
            }
        }

        private bool checkinput()
        {
            if (DateTime.Compare(Convert.ToDateTime(dtpGiovao.EditValue), Convert.ToDateTime(dtpGiora.EditValue)) > 0)
            {
                XtraMessageBox.Show("Giờ vào phải nhỏ hơn giờ ra!", _thongbao, MessageBoxButtons.OK);
                dtpGiovao.Focus();
                return false;
            }
            else
                return true;
        }
    }
}