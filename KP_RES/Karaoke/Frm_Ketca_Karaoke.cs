﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using KP_Report;
using System.Globalization;
using System.Resources;

namespace KP_RES
{
    public partial class Frm_Ketca_Karaoke : DevExpress.XtraEditors.XtraForm
    {
        string _thongbao = "";
        string _dongtkthanhcong = "";
        string _xacnhanbmhtkc = "";
        public string s_mahanghoa = "";
        public string s_tenhanghoa = "";
        public string s_soluong = "";
        public string s_phuthu = "";
        DataTable dtkc;

        public Frm_Ketca_Karaoke()
        {
            InitializeComponent();
            if (clsMain.GetServerDate().Hour<=4)
            {
                dtpTuNgay.EditValue = clsMain.GetServerDate().AddDays(-1);
                dtpDenNgay.EditValue = clsMain.GetServerDate();
                txtTuTime.Text = "06:00";
                txtDenTime.Text = "04:00";
            }
            else
            {
                dtpTuNgay.EditValue = clsMain.GetServerDate();
                dtpDenNgay.EditValue = clsMain.GetServerDate();
                txtTuTime.Text = "05:00";
                txtDenTime.Text = "23:59";
            }
           
        }

        private void Frm_Combo_Option_Load(object sender, EventArgs e)
        {
            string sql = "EXEC selectKetca_Karaoke @MANHANVIEN=" + clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin);
            dtkc = clsMain.ReturnDataTable(sql);
            gr_SelectProduct.DataSource = dtkc;
            gr_SelectProduct.RefreshDataSource();
            if (gv_SelectProduct.RowCount > 0)
            {
                TotalMoney();
                lblTienGio.Text = string.Format("{0:#,###0}", double.Parse(dtkc.Rows[0]["TIENGIO"].ToString()));
                lbGiamGia.Text = string.Format("{0:#,###0}", double.Parse(dtkc.Rows[0]["GIAMGIA"].ToString()));
                lbPhuThu.Text = string.Format("{0:#,###0}", double.Parse(dtkc.Rows[0]["PHIPHUCVU"].ToString()));      
                lb_Tientamung.Text = string.Format("{0:#,###0}", double.Parse(clsUserManagement.ReturnTienTamUng()));
                lb_tongcong.Text = string.Format("{0:#,###0}", double.Parse(lbTotal.Text.Replace(",", "")) + double.Parse(lblTienGio.Text.Replace(",", "")) + double.Parse(lbPhuThu.Text.Replace(",", "")) - double.Parse(lbGiamGia.Text.Replace(",", "")));
                lb_Tongtiennop.Text = string.Format("{0:#,###0}", double.Parse(lb_tongcong.Text.Replace(",", "")) + double.Parse(lb_Tientamung.Text.Replace(",", "")));
            }
            else
                lbTotal.Text = "0";

            lb_NhanVien.Text = clsGlobal.gsNameOfUserLogin;
            lbtimeby.Text = cls_ConfigCashier.sTenCB ;

            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            this.Text = rm.GetString("ketca", culture);
            title.Text = rm.GetString("ketca", culture);
            labelControl4.Text = rm.GetString("ngayketca", culture);
            labelControl1.Text = rm.GetString("nhanvien", culture);
            labelControl2.Text = rm.GetString("calamviec", culture);
            labelControl3.Text = rm.GetString("tiengio", culture);
            gv_SelectProduct.Columns["SOTT"].Caption = rm.GetString("stt", culture);
            gv_SelectProduct.Columns["TEN_HANGHOA"].Caption = rm.GetString("tenhanghoa", culture);
            gv_SelectProduct.Columns["SOLUONG"].Caption = rm.GetString("soluong", culture);
            gv_SelectProduct.Columns["GIABAN"].Caption = rm.GetString("dongia", culture);
            gv_SelectProduct.Columns["Thanh_Tien"].Caption = rm.GetString("thanhtien", culture);
            btnHoantatketca.Text = rm.GetString("hoantatketca", culture);
            btnPrint.Text = rm.GetString("inketca1", culture);
            lblInHangHoa.Text = rm.GetString("indshanghoa", culture);
            btnThoat.Text = rm.GetString("thoat", culture);
            lb_Phuthu.Text = rm.GetString("phuthu", culture);
            lb_Giamgia.Text = rm.GetString("giamgia", culture);
            lb_tongcong1.Text = rm.GetString("tongcong", culture);
            lb_tientamung1.Text = rm.GetString("tientamung", culture);
            lb_tiennop.Text = rm.GetString("tongtiennop", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _dongtkthanhcong = rm.GetString("dongtkthanhcong", culture);
            _xacnhanbmhtkc = rm.GetString("xacnhanbmhtkc", culture);
            btnTimkiem.Text = rm.GetString("timkiem", culture);

            btnTimkiem_Click(null, null);
        }

        private void TotalMoney()
        {
            string sSQL = "Select GIATRI From CAUHINH Where TEN='GIABANTRUOCTHUE' ";
            DataTable checkVAT = clsMain.ReturnDataTable(sSQL);
            bool kt = false;
            if (checkVAT.Rows[0]["GIATRI"].ToString() == "1")
                kt = true;

            double total = 0;
            try
            {
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    double thanhTien = 0, vat = 0;
                    double sl = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                    double gia = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                    thanhTien = sl * gia;
                    if (kt == true)
                        vat = (double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["THUE"]).ToString()) / 100) * thanhTien;
                    total += thanhTien + vat;
                    gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"], thanhTien + vat);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            lbTotal.Text = string.Format("{0:#,###0}", total);
        }

        private void gv_SelectProduct_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == SOTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }

        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gv_SelectProduct.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gv_SelectProduct.FocusedRowHandle == gv_SelectProduct.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnHoantatketca_Click(object sender, EventArgs e)
        {
            if (DevExpress.XtraEditors.XtraMessageBox.Show(_xacnhanbmhtkc, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                string sSQL = "Select GIATRI From CAUHINH where TEN='KICHHOATUSER'";
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString() == "1")
                        clsUserManagement.DongTaiKhoan(clsGlobal.gsUserID, false);
                    else
                        clsUserManagement.DongTaiKhoan(clsGlobal.gsUserID, true);
                }

                DevExpress.XtraEditors.XtraMessageBox.Show(_dongtkthanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Application.Restart();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            cls_PrinterEndSales printSales = new cls_PrinterEndSales();
            printSales.maCaBan = "";
            printSales.tenCaBan = cls_ConfigCashier.sTenCB;
            printSales.maNV = clsGlobal.gsMaNVOfUserLogin;
            printSales.tenNV = clsGlobal.gsNameOfUserLogin;
            printSales.maQuay = cls_ConfigCashier.idCashier;
            printSales.tenQuay = cls_ConfigCashier.nameCashier;
            printSales.giamgia = double.Parse(lbGiamGia.Text);
            printSales.phuthu = double.Parse(lbPhuThu.Text);
            printSales.TienTamUng = double.Parse(lb_Tientamung.Text.Replace(",", ""));
            printSales.tongTienBan = double.Parse(lbTotal.Text.Replace(",", ""));
            printSales.tongTien = double.Parse(lb_Tongtiennop.Text.Replace(",", ""));

            if (dtkc.Rows.Count > 0)
            {
                printSales.tiengio = double.Parse(dtkc.Rows[0]["TIENGIO"].ToString());
                printSales.tienmat = double.Parse(dtkc.Rows[0]["MONEYCASH"].ToString()) + double.Parse(lb_Tientamung.Text.Replace(",", ""));
                printSales.tienthe = double.Parse(dtkc.Rows[0]["MONEYVISA"].ToString());
                printSales.voucher = double.Parse(dtkc.Rows[0]["MONEYVOUCHER"].ToString());
                printSales.TTNB = double.Parse(dtkc.Rows[0]["MONEYINNERCARD"].ToString());
            }

            List<cls_PrinterEndSales> endSales = new List<cls_PrinterEndSales>();
            endSales.Add(printSales);
            convert cvrt = new convert();
            DataTable bill = cvrt.ConvertToDataTable(endSales);

            DataColumn myDC1 = new DataColumn();
            myDC1.ColumnName = "ngayHD";
            myDC1.DataType = System.Type.GetType("System.String");
            myDC1.DefaultValue = String.Format("{0:dd/MM/yyyy}", dtpTuNgay.EditValue) + " " + txtTuTime.Text + " - " + String.Format("{0:dd/MM/yyyy}", dtpDenNgay.EditValue) + " " + txtDenTime.Text;

            if (ContainColumn("ngayHD", bill) == false)
                bill.Columns.Add(myDC1);

            Frm_Report1 frm = new Frm_Report1();
            if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                frm.ReportName = "rpt_ketcaKaraoke_58";
            else
                frm.ReportName = "rpt_ketcaKaraoke";
            frm.DataSource = bill;
            frm.WindowState = FormWindowState.Minimized;
            frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
            frm.IsPrint = true;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnInHangHoa_Click(object sender, EventArgs e)
        {
            DataView myDV = (DataView)gv_SelectProduct.DataSource;
            DataTable myDT = myDV.ToTable();

            DataColumn myDC = new DataColumn();
            myDC.ColumnName = "TenNV";
            myDC.DataType = System.Type.GetType("System.String");
            myDC.DefaultValue = lb_NhanVien.Text;
            DataColumn myDC1 = new DataColumn();
            myDC1.ColumnName = "ngayHD";
            myDC1.DataType = System.Type.GetType("System.String");
            myDC1.DefaultValue = String.Format("{0:dd/MM/yyyy}", dtpTuNgay.EditValue);

            if (ContainColumn("TenNV", myDT) == false)
                myDT.Columns.Add(myDC);
            if (ContainColumn("ngayHD", myDT) == false)
                myDT.Columns.Add(myDC1);

            Frm_Report1 frm = new Frm_Report1();
            if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                frm.ReportName = "rptKetCaHangHoa_58";
            else
                frm.ReportName = "rptKetCaHangHoa";
            frm.DataSource = myDT;
            frm.WindowState = FormWindowState.Minimized;
            frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
            frm.IsPrint = true;
            frm.ShowDialog();
            frm.Dispose();
        }

        private Boolean ContainColumn(String columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
                return true;
            else
                return false;
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            String sSQL = "";
            sSQL += "EXEC SelectKetca_TheoGio ";
            sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
            sSQL += clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", dtpTuNgay.EditValue) + " " + txtTuTime.Text) + ",";
            sSQL += clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", dtpDenNgay.EditValue) + " " + txtDenTime.Text);

            dtkc = clsMain.ReturnDataTable(sSQL);
            gr_SelectProduct.DataSource = dtkc;
            gr_SelectProduct.RefreshDataSource();
            if (gv_SelectProduct.RowCount > 0)
            {
                TotalMoney();
                lblTienGio.Text = string.Format("{0:#,###0}", double.Parse(dtkc.Rows[0]["TIENGIO"].ToString()));
                lbGiamGia.Text = string.Format("{0:#,###0}", double.Parse(dtkc.Rows[0]["GIAMGIA"].ToString()));
                lbPhuThu.Text = string.Format("{0:#,###0}", double.Parse(dtkc.Rows[0]["PHIPHUCVU"].ToString()));
                lb_Tientamung.Text = string.Format("{0:#,###0}", int.Parse(clsUserManagement.ReturnTienTamUng()));
                lb_tongcong.Text = string.Format("{0:#,###0}", double.Parse(lbTotal.Text.Replace(",", "")) + double.Parse(lblTienGio.Text.Replace(",", "")) + double.Parse(lbPhuThu.Text.Replace(",", "")) - double.Parse(lbGiamGia.Text.Replace(",", "")));
                lb_Tongtiennop.Text = string.Format("{0:#,###0}", double.Parse(lb_tongcong.Text.Replace(",", "")) + double.Parse(lb_Tientamung.Text.Replace(",", "")));
            }
            else
            {
                lbTotal.Text = "0";
                lblTienGio.Text = "0";
                lb_tongcong.Text = "0";
                lb_Tongtiennop.Text = "0";
            }

            lb_NhanVien.Text = clsGlobal.gsNameOfUserLogin;
            lbtimeby.Text = cls_ConfigCashier.sTenCB;
        }
    }
}