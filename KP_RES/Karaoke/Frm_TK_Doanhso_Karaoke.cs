﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraCharts;
using System.Globalization;

namespace KP_RES 
{
    public partial class Frm_TK_Doanhso_Karaoke : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt = new DataTable();
        int m_next = 0;
        DateTime date;

        public Frm_TK_Doanhso_Karaoke()
        {
            InitializeComponent();
        }

        private void Frm_TK_Doanhso_Load(object sender, EventArgs e)
        {
            loadCombo();
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            chart.Series.Clear();
            string sql = string.Empty;
            if (optGroup.SelectedIndex == 0)
            {
                int nam = int.Parse(cboNam.EditValue.ToString())-1;
                sql  = "SELECT SUM(TONGTIEN) as [Doanh Thu],(N'Tháng '+cast(MONTH(NGAYTAO) as varchar(2))) as [Mem] \n";
                sql += "FROM HOADON_KARAOKE WHERE ISPAYMENT=1 and NGAYTAO>'" + nam + "1231' and NGAYTAO<=GETDATE() \n";
                sql += "GROUP BY MONTH(NGAYTAO)";
            }
            else if (optGroup.SelectedIndex == 1)
            {
                string m_cur = string.Empty;        //tháng hiện tại
                string _m_next = m_next.ToString("00"); //tháng tiếp theo
                if (m_next == 1)
                    m_cur = "12";
                else
                    m_cur = (m_next-1).ToString("00");

                sql  = "SELECT SUM(TONGTIEN) as [Doanh Thu],(N'Tuần '+cast(datepart(ww,ngaytao)as varchar(2))) as [Mem]\n";
                sql += "FROM HOADON_KARAOKE where ISPAYMENT=1 and NGAYTAO>='" + cboNam.EditValue + m_cur + "01' and NGAYTAO<'" + cboNam.EditValue + _m_next + "01'\n";
                sql += "GROUP BY datepart(ww,ngaytao)";
            }
            else if (optGroup.SelectedIndex == 2)
            {
                string[] arr = new string[2];
                arr = cboTuan.EditValue.ToString().Split(' ');
                int sotuan = int.Parse(arr[1]);
                int dinm = date.AddDays(-1).Day;

                string tungay = string.Empty;
                string denngay = string.Empty;
                int i;
                for (i = 1; i < (dinm+1); i++)
                {
                    if (sotuan == GetWeekOfMonth(date.AddMonths(-1).AddDays(i-1)))
                    {
                        tungay = i.ToString("00"); 
                        break;
                    }
                }
                for (i = int.Parse(tungay); i < (dinm + 1); i++)
                {
                    if (i == dinm)
                    {
                        denngay = i.ToString("00"); 
                        break;
                    }
                    else if (sotuan < GetWeekOfMonth(date.AddMonths(-1).AddDays(i-1)))
                    {
                        denngay = (i - 1).ToString("00");
                        break;
                    }
                }
                
                string m_cur = string.Empty;
                if (m_next == 1)
                    m_cur = "12";
                else
                    m_cur = (m_next - 1).ToString("00");

                sql  = "SELECT SUM(TONGTIEN) as [Doanh Thu],(N'Ngày '+cast(DAY(NGAYTAO)as varchar(2))) as [Mem] \n";
                sql += "FROM HOADON_KARAOKE WHERE ISPAYMENT=1 and NGAYTAO>='" + cboNam.EditValue + m_cur + tungay + "' and NGAYTAO<='" + cboNam.EditValue + m_cur + denngay + "' \n";
                sql += "GROUP BY DAY(NGAYTAO)";
            }
            DataTable dt = clsMain.ReturnDataTable(sql);

            if (optGroup.SelectedIndex == 1)
            {
                if (dt.Rows.Count > 0)
                {
                    int n = dt.Rows.Count;
                    for (int i = 0; i < n; i++)
                    {
                        dt.Rows[i]["Mem"] = "Tuần "+(i + 1).ToString();
                    }
                }
            }
            chart.DataSource = dt;
            Series series = new Series("Series1", ViewType.Bar);
            chart.Series.Add(series);
            series.LegendText = "Doanh thu";
            series.ArgumentDataMember = "Mem";
            series.ValueDataMembers.AddRange("Doanh Thu");
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
            }
            this.Refresh();
        }

        private void optGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (optGroup.SelectedIndex == 0)
            {
                lbThang.Visible = false;
                cboThang.Visible = false;

                lbTuan.Visible = false;
                cboTuan.Visible = false;
            }
            else if(optGroup.SelectedIndex == 1)
            {
                lbThang.Visible = true;
                cboThang.Visible = true;

                lbTuan.Visible = false;
                cboTuan.Visible = false;
            }
            else if (optGroup.SelectedIndex == 2)
            {
                lbThang.Visible = true;
                cboThang.Visible = true;

                lbTuan.Visible = true;
                cboTuan.Visible = true;
            }
        }

        private void loadCombo()
        {
            cboNam.Properties.Items.Add(clsGlobal.gdServerDate.Year.ToString());
            cboNam.Properties.Items.Add(clsGlobal.gdServerDate.AddYears(-1).Year.ToString());
            cboNam.Properties.Items.Add(clsGlobal.gdServerDate.AddYears(-2).Year.ToString());
            cboNam.EditValue = clsGlobal.gdServerDate.Year.ToString();

            cboThang.EditValue = "Tháng " + clsGlobal.gdServerDate.Month.ToString();
        }

        public static int GetWeekOfMonth(DateTime date)
        {
            DateTime beginningOfMonth = new DateTime(date.Year, date.Month, 1);

            while (date.Date.AddDays(1).DayOfWeek != CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek)
                date = date.AddDays(1);

            return (int)Math.Truncate((double)date.Subtract(beginningOfMonth).TotalDays / 7f) + 1;
        }

        private void cboNam_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboThang.EditValue.ToString() != "")
            {
                cboTuan.Properties.Items.Clear();
                string[] arr = new string[2];
                arr = cboThang.EditValue.ToString().Split(' ');
                if (int.Parse(arr[1]) == 12)
                    m_next = 1;
                else
                    m_next = int.Parse(arr[1]) + 1;

                date = new DateTime(int.Parse(cboNam.EditValue.ToString()), m_next, 1);
                int num_Week = GetWeekOfMonth(date.AddDays(-1));
                for (int i = 0; i < num_Week; i++)
                {
                    cboTuan.Properties.Items.Add("Tuần " + (i + 1).ToString());
                }
                cboTuan.EditValue = "Tuần 1";
            }
        }

        private void cboThang_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboThang.EditValue.ToString() != "")
            {
                cboTuan.Properties.Items.Clear();
                string[] arr = new string[2];
                arr = cboThang.EditValue.ToString().Split(' ');
                if (int.Parse(arr[1]) == 12)
                    m_next = 1;
                else
                    m_next = int.Parse(arr[1]) + 1;

                date = new DateTime(int.Parse(cboNam.EditValue.ToString()), m_next, 1);
                int num_Week = GetWeekOfMonth(date.AddDays(-1));
                for (int i = 0; i < num_Week; i++)
                {
                    cboTuan.Properties.Items.Add("Tuần " + (i + 1).ToString());
                }
                cboTuan.EditValue = "Tuần 1";
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            try
            {
                ChartControl chart = (ChartControl)panelControl5.Controls[0];
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                chart.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                chart.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private static byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            DataColumn dc2 = new DataColumn();
            dc2.ColumnName = "TEMP";
            dc2.DefaultValue = "Temp";
            dt.Columns.Add(dc2);
            dt.Rows.Add("TEMP");

            Bitmap bmp = new Bitmap(panelControl5.ClientRectangle.Width, panelControl5.ClientRectangle.Height);
            panelControl5.DrawToBitmap(bmp, panelControl5.ClientRectangle);

            DataColumn colIMAGE = new DataColumn("IMAGE"); //Create the column.
            colIMAGE.DataType = System.Type.GetType("System.Byte[]"); //Type byte[] to store image bytes.
            colIMAGE.AllowDBNull = true;
            colIMAGE.Caption = "IMAGE";
            colIMAGE.DefaultValue = ImageToByteArray(bmp);
            dt.Columns.Add(colIMAGE);

            DataColumn dc1 = new DataColumn();
            dc1.ColumnName = "NGAY";
            if (cboThang.Visible)
            {
                dc1.DefaultValue = cboThang.Text + "/" + cboNam.Text;
            }
            else
            {
                dc1.DefaultValue = cboNam.Text;
            }

            dt.Columns.Add(dc1);


            if (optGroup.SelectedIndex == 0)
            {
                Frm_BCInPhieu frm = new Frm_BCInPhieu();
                frm.WindowState = FormWindowState.Maximized;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.dtSource = dt;
                frm.Mode = 57;
                frm.ShowDialog();
                frm.Dispose();

            }
            else
            {
                Frm_BCInPhieu frm = new Frm_BCInPhieu();
                frm.WindowState = FormWindowState.Maximized;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.dtSource = dt;
                frm.Mode = 58;
                frm.ShowDialog();
                frm.Dispose();

            }
        }
    }
}