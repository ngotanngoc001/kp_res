﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Threading;
using KP_UserManagement;
using KP_RES.Class;
using System.IO;
using KP_Terminal;
using KP_RES;

namespace KP_RES
{
    public partial class Frm_BanHang_Karaoke : DevExpress.XtraEditors.XtraForm
    {
        public int funtions = 1;//Funtion 1 Sơ đồ bàn, Funtion 2 Open Table, 3 yêu cầu thêm, 4 send Order,5 join Table , 6 divide, 7 Payments, 8 buy id Product, 9Check Table, 10 ket ca.,11 Chuyển bàn.
        public int sendOrder = 0;
        public string maBan = "";
        public string maBanGhep = "";
        public string idBillJoin = "";
        bool Flag_soluong = false; //true:so luong tren lưới dc bắt đầu lại,False:sô lượng đang tạo +thêm chuỗi.
        public string maMon;
        public float xGIABAN;
        public DataTable dtLuoi = new DataTable("HANGHOA"); 
        public DataTable dtBan = new DataTable();
        public DataTable dtKV = new DataTable();
        public DataTable dtDish = new DataTable();
        public DataTable dtMon = new DataTable();
        public DataTable dtTiengio = new DataTable();
        public DataTable dtkc = new DataTable();
        DataTable dtHangOrder = new DataTable();//Những mặt hàng đã order
        public delegate void GetString(DataTable dtshow, int ctrol, int iFocusRow);
        public GetString MyGetData;
        public int contrDelegate = 0;
        public int iFocusRow = 0;
        public static BindingSource bsource = new BindingSource();
        public int tatol_cPageKV;
        public int tatol_cPageDish;
        public int tatol_CurrenPages;
        public int tatol_cPageMon;  
        public int cPageKV = 1;
        public int cPageNH = 1;
        public int cPageBan=1;
        public int SizePages = 30;
        public int cPageMon = 1;
        public int sizeMon = 30;
        public int maKV = 1;
        public int maNH = 1;      

        public DataTable divide_Table1 = new DataTable();
        public DataTable divide_Table2 = new DataTable();
        public int setBgrk = 0;
        // bien ktra trùng tên khi addrow hàng hóa
        bool testID = false;
        //karaoke
        DateTime giovao;
        DateTime giora;
        string GhiChu = "";

        public Frm_BanHang_Karaoke()
        {
            InitializeComponent();
            LoadPermission();
            AddColumnDatatable();           
            lb_Quay.Text = cls_ConfigCashier.nameCashier;
            lb_Kho.Text = cls_ConfigCashier.nameWarehouse;
            lbNameTable.Text = "";
            lbNameMon.Text = "";
            lbCaBan.Text = DateTime.Now.ToString("dd-MM-yyyy");
            lb_NhanVien.Text = clsGlobal.gsNameOfUserLogin;
            lbtimeby.Text = cls_ConfigCashier.sTenCB + ": " + cls_ConfigCashier.sGioBD + " --> " + cls_ConfigCashier.sGioKT;
        }

        private void Frm_BanHang_Karaoke_Load(object sender, EventArgs e)
        {
            SetBackgroudOnClick(btn_ThuNgan);
            SetDefautlBackgroud(btn_NghiepVu);
            pnThuNgan.BringToFront();
            pnShowHang.BringToFront();
            pnBtnDeleteGv.Visible = false;
            pnUpDowGv.Visible = false;
            btnComboOption.Visible = false;
            GetKhuVuc();
            GetSoDoBan();
        }

        private void Frm_BanHang_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                if (funtions == 1 && maBan != "" && gv_SelectProduct.RowCount > 0)
                {
                    btn_Payment_Click(sender, e);
                }
            }
            else if (e.KeyCode == Keys.Escape)
            {
                btn_logout_Click(null, null);
            }
        }

        private void LoadPermission()
        {
            ////30/08/2016 Thong them cau hinh co dung chuc nang in tam tinh thay khong
            //string sIntamtinh = clsUserManagement.CheckCauhinh("INTAMTINH");
            //bool bIntamtinh = false;
            //if (sIntamtinh != "Error")
            //{
            //    bIntamtinh = sIntamtinh == "1" ? true : false;
            //}
            //btn_ThanhToan.Enabled = clsUserManagement.AllowAdd("23");
            //btn_TamTinh.Enabled = bIntamtinh && clsUserManagement.AllowAdd("23");
            //btn_InLaiHoaDon.Enabled = clsUserManagement.AllowEdit("23");
            //btn_LayLaiHoaDon.Enabled = clsUserManagement.AllowDelete("23");
            //btn_HuyBan.Enabled = clsUserManagement.AllowDelete("23");
            //btn_HuyMon.Enabled = clsUserManagement.AllowDelete("23");
        }

        private void AddColumnDatatable()
        {
            dtLuoi.Columns.Add("MA_HANGHOA", typeof(Int32));
            dtLuoi.Columns.Add("TEN_HANGHOA", Type.GetType("System.String"));
            dtLuoi.Columns.Add("SOLUONG", Type.GetType("System.Double"));
            dtLuoi.Columns.Add("GIABAN", Type.GetType("System.Double"));
            dtLuoi.Columns.Add("GHICHU", Type.GetType("System.String"));
            dtLuoi.Columns.Add("MA_BEP", typeof(Int32));
            dtLuoi.Columns.Add("THUE", Type.GetType("System.Double"));
            dtLuoi.Columns.Add("Thanh_Tien", Type.GetType("System.Double"));
            dtLuoi.Columns.Add("TRANGTHAI", typeof(bool));
            dtLuoi.Columns.Add("IS_COMBO", typeof(bool));
            dtLuoi.Columns.Add("IS_INBEP", typeof(bool));
            dtLuoi.Columns.Add("IS_KHUYENMAI", typeof(bool));
            dtLuoi.Columns.Add("CHIETKHAU", Type.GetType("System.Double"));

            divide_Table1.Columns.Add("_MA_HANGHOA", typeof(Int32));
            divide_Table1.Columns.Add("_TEN_HANGHOA", Type.GetType("System.String"));
            divide_Table1.Columns.Add("_SOLUONG", Type.GetType("System.Double"));
            divide_Table1.Columns.Add("_GIABAN", Type.GetType("System.Double"));
            divide_Table1.Columns.Add("__MA_BEP", typeof(Int32));
            divide_Table1.Columns.Add("_Thanh_Tien", Type.GetType("System.Double"));

            divide_Table2.Columns.Add("__MA_HANGHOA", typeof(Int32));
            divide_Table2.Columns.Add("__TEN_HANGHOA", Type.GetType("System.String"));
            divide_Table2.Columns.Add("__SOLUONG", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("__GIABAN", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("__MA_BEP", typeof(Int32));
            divide_Table2.Columns.Add("__Thanh_Tien", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("__IS_COMBO", typeof(bool));
        }

        private void SetBackgroudOnClick(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.YellowGreen;
            _btn.Appearance.BackColor2 = Color.YellowGreen;
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;

        }

        private void SetDefautlBackgroud(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.White;
            if (_btn.Appearance.Options.UseBackColor == true)
                _btn.Appearance.Options.UseBackColor = false;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == false)
                _btn.LookAndFeel.UseDefaultLookAndFeel = true;
        }

        private void GetKhuVuc()
        {
            try
            {
                DataTable dtKhuVuc = new DataTable();
                dtKhuVuc.Rows.Clear();
                string SQLKV = string.Format("Exec SelectKhuVuc_Karaoke @PageNumber={0},@PageSize={1}, @maCH={2}", cPageKV, 7, int.Parse(cls_ConfigCashier.idShop));
                dtKhuVuc = clsMain.ReturnDataTable(SQLKV);

                while (pnKV_Mon.Controls.Count > 0)
                {
                    pnKV_Mon.Controls[0].Dispose();
                }
                pnKV_Mon.Controls.Clear();
                int locateY = 2;
                int locateX = 3;
                for (int i = 0; i < dtKhuVuc.Rows.Count; i++)
                {
                    SimpleButton btnKV = new SimpleButton();
                    btnKV.Name = dtKhuVuc.Rows[i]["MA_KHUVUC"].ToString();
                    if (i == 0)
                    {
                        maKV = int.Parse(dtKhuVuc.Rows[0]["MA_KHUVUC"].ToString());
                        lbKhuVuc.Text = "" + dtKhuVuc.Rows[0]["TEN_KHUVUC"].ToString() + ">";
                        SetBackgroudOnClick(btnKV);
                    }
                    btnKV.Text = dtKhuVuc.Rows[i]["TEN_KHUVUC"].ToString();
                    try
                    {
                        byte[] tam = new byte[((byte[])dtKhuVuc.Rows[i]["HINHANH"]).LongLength];
                        tam = (byte[])dtKhuVuc.Rows[i]["HINHANH"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        btnKV.Image = bm;
                    }
                    catch
                    {
                    }
                    btnKV.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btnKV.ImageLocation = ImageLocation.TopCenter;
                    btnKV.RightToLeft = RightToLeft.No;
                    btnKV.Size = new Size(125, 80);
                    if (i != 0)
                    {
                        locateY += int.Parse(btnKV.Location.Y.ToString()) + 85;
                    }
                    btnKV.Location = new Point(locateX, locateY);
                    btnKV.Click += new EventHandler(btnKV_Click);
                    pnKV_Mon.Controls.Add(btnKV);
                }
            }
            catch
            {
            }
        }

        private void GetSoDoBan()
        {
            try
            {
                dtBan.Clear();
                string sql = string.Format("Exec SelectTable @PageNumber={0},@maKV={1}", cPageBan, maKV);
                dtBan = clsMain.ReturnDataTable(sql);

                while (pnTable_Mon.Controls.Count > 0)
                {
                    pnTable_Mon.Controls[0].Dispose();
                }
                pnTable_Mon.Controls.Clear();
                for (int i = 0; i < dtBan.Rows.Count; i++)
                {
                    SimpleButton btnBan_Mon = new SimpleButton();
                    btnBan_Mon.Name = dtBan.Rows[i]["MA_BAN"].ToString();
                    if (dtBan.Rows[i]["SOGHE"].ToString() != "0")
                    {
                        btnBan_Mon.Text = dtBan.Rows[i]["TEN_BAN"].ToString() + " - " + dtBan.Rows[i]["SOGHE"].ToString() + "  Ghế";
                    }
                    else
                    {
                        btnBan_Mon.Text = dtBan.Rows[i]["TEN_BAN"].ToString();
                    }
                    try
                    {
                        byte[] tam = new byte[((byte[])dtBan.Rows[i]["HINHANH"]).LongLength];
                        tam = (byte[])dtBan.Rows[i]["HINHANH"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        btnBan_Mon.Image = bm;
                    }
                    catch
                    {
                    }
                    btnBan_Mon.Width = int.Parse(dtBan.Rows[i]["Width"].ToString());
                    btnBan_Mon.Height = int.Parse(dtBan.Rows[i]["Height"].ToString());
                    btnBan_Mon.Location = new Point(int.Parse(dtBan.Rows[i]["LocaX"].ToString()), int.Parse(dtBan.Rows[i]["LocaY"].ToString()));

                    try
                    {
                        if (bool.Parse(dtBan.Rows[i]["ISBANDAT"].ToString()) == true)
                        {
                            SetBackgroudOnClick2(btnBan_Mon);
                        }
                    }
                    catch
                    {
                    }
                    try
                    {
                        if (bool.Parse(dtBan.Rows[i]["ISMO_BAN"].ToString()) == true)
                        {
                            SetBackgroudOnClick1(btnBan_Mon);
                            DataTable dataInPut = getGioVaoBan(btnBan_Mon.Name.ToString());
                            if (dataInPut.Rows.Count > 0)
                            {
                                btnBan_Mon.Text += "\n Giờ vào: " + "\n" + cls_KP_RES.DateToString_Full_NoSecond((DateTime)dataInPut.Rows[0][1]);
                            }
                            string idHD = GetMaHoaDon(btnBan_Mon.Name.ToString());
                            DataTable tb = DemSoBanTrongBill(idHD);
                            if (tb.Rows.Count > 1)
                            {
                                btnBan_Mon.Text += " \n GB " + idHD.Substring(idHD.IndexOf("B") + 1, 4);
                            }
                        }
                    }
                    catch
                    {
                    }
                    try
                    {
                        if (bool.Parse(dtBan.Rows[i]["SEE_PAYMENTS"].ToString()) == true)
                        {
                            SetBackgroudOnClick3(btnBan_Mon);
                        }
                    }
                    catch
                    {
                        dtBan.Rows[i]["SEE_PAYMENTS"] = 0;
                        clsMain.ExecuteSQL("update ban set SEE_PAYMENTS=0 where SEE_PAYMENTS is null");
                    }

                    btnBan_Mon.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btnBan_Mon.ImageLocation = ImageLocation.TopCenter;
                    btnBan_Mon.RightToLeft = RightToLeft.No;
                    btnBan_Mon.Click += new EventHandler(btnBan_Mon_Click);
                    pnTable_Mon.Controls.Add(btnBan_Mon);
                }
            }
            catch
            {
            }
        }

        private void SetBackgroudOnClick2(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.FromArgb(0, 173, 239);
            _btn.Appearance.BackColor2 = Color.FromArgb(0, 173, 239);
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;

        }

        private void SetBackgroudOnClick1(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.FromArgb(229, 137, 45);
            _btn.Appearance.BackColor2 = Color.FromArgb(229, 137, 45);
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;

        }

        private void SetBackgroudOnClick3(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.FromArgb(214, 223, 35);
            _btn.Appearance.BackColor2 = Color.FromArgb(214, 223, 35);
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;

        }

        private DataTable getGioVaoBan(string maBan)
        {
            string ssl = " select CONVERT(varchar(8),hd.GIOVAO,108) as GIOVAO,  GIOVAO AS GIOVAOKARAOKE From HOADON_KARAOKE hd inner join CT_MOBAN_KARAOKE ctmb on hd.MA_HOADON = ctmb.MA_HOADON where ctmb.MA_BAN = " + int.Parse(maBan) + " and ISNULL(hd.ISPAYMENT,0) = 0 ";
            DataTable dtTime = clsMain.ReturnDataTable(ssl);
            return dtTime;
        }

        private string GetMaHoaDon(string idTable)
        {
            string sSQL = "";
            sSQL += "Select a.MA_HOADON" + "\n";
            sSQL += "From CT_MOBAN_KARAOKE a,HOADON_KARAOKE b,BAN c" + "\n";
            sSQL += "where a.MA_HOADON = b.MA_HOADON And a.MA_BAN=c.MA_BAN  And ISNULL(b.ISBANDAT,0) = 0" + "\n";
            sSQL += "And ISMO_BAN=1 And b.ISPAYMENT = 0 And a.MA_BAN = " + clsMain.SQLString(idTable) + "\n";
            sSQL += "Order by a.MA_HOADON desc";
            DataTable dmBAN = clsMain.ReturnDataTable(sSQL);
            if (dmBAN.Rows.Count > 0)
            {
                return dmBAN.Rows[0]["MA_HOADON"].ToString();
            }
            else
            {
                sSQL = "";
                sSQL += "Select a.MA_HOADON " + "\n";
                sSQL += "From CT_MOBAN_KARAOKE as a,HOADON_KARAOKE as b,BAN c" + "\n";
                sSQL += "Where a.MA_HOADON = b.MA_HOADON And a.MA_BAN=c.MA_BAN And b.ISBANDAT=1" + "\n";
                sSQL += "And ISMO_BAN=1 And b.ISPAYMENT = 0 And a.MA_BAN = " + clsMain.SQLString(idTable) + "\n";
                sSQL += "Order by GIOVAO ASC";
                dmBAN = clsMain.ReturnDataTable(sSQL);
                if (dmBAN.Rows.Count > 0)
                    return dmBAN.Rows[0]["MA_HOADON"].ToString();
                else
                    return "";
            }
        }

        private DataTable DemSoBanTrongBill(string idBill)
        {
            DataTable dttabless;
            string sql = "select MA_BAN from CT_MOBAN_KARAOKE where MA_HOADON ='" + idBill + "'";
            dttabless = clsMain.ReturnDataTable(sql);
            return dttabless;
        }

        private void btn_UpKV_Click(object sender, EventArgs e)
        {
            if (funtions == 1 || funtions == 11 || funtions == 6)
            {
                if (cPageKV > 1)
                {
                    cPageKV -= 1;
                    GetKhuVuc();
                    GetSoDoBan();
                }
            }
            if (funtions == 2)
            {
                if (cPageNH > 1)
                {
                    cPageNH -= 1;
                    GetNhomhang();
                    GetListMon();
                }
            }
        }

        private void btn_DowKV_Click(object sender, EventArgs e)
        {
            if (funtions == 1 || funtions == 11 || funtions == 6)
            {
                if (cPageKV < 20)
                {
                    cPageKV += 1;
                    GetKhuVuc();
                    GetSoDoBan();
                }
            }
            if (funtions == 2)
            {
                if (cPageNH < 20)
                {
                    cPageNH += 1;
                    GetNhomhang();
                    GetListMon();
                }
            }
        }

        private void btn_NextToRight_Click(object sender, EventArgs e)
        {
            if (funtions == 1)
            {
                if (cPageBan == 5)
                    return;
                cPageBan = cPageBan + 1;
                lbPageTable.Text = cPageBan.ToString();
                GetSoDoBan();
            }
            if (funtions == 2)
            {
                if (cPageMon == 20)
                    return;
                cPageMon = cPageMon + 1;
                lbPageTable.Text = cPageMon.ToString();
                GetListMon();
            }
        }

        private void btn_NextToLeft_Click(object sender, EventArgs e)
        {
            if (funtions == 1)
            {
                if (cPageBan == 1)
                    return;
                cPageBan = cPageBan - 1;
                lbPageTable.Text = cPageBan.ToString();
                GetSoDoBan();
            }
            if (funtions == 2)
            {
                if (cPageMon == 1)
                    return;
                cPageMon = cPageMon - 1;
                lbPageTable.Text = cPageMon.ToString();
                GetListMon();
            }
        }

        private void GetNhomhang()
        {
            try
            {
                DataTable dtNhomhang = new DataTable();
                dtNhomhang.Rows.Clear();
                string SQLMON = string.Format("exec SelectDish @PageNumber={0},@PageSize={1},@maQUAY={2}", cPageNH, 7, cls_ConfigCashier.idCashier);
                dtNhomhang = clsMain.ReturnDataTable(SQLMON);

                while (pnKV_Mon.Controls.Count > 0)
                {
                    pnKV_Mon.Controls[0].Dispose();
                }
                pnKV_Mon.Controls.Clear();
                int locateY = 2;
                int locateX = 3;
                for (int i = 0; i < dtNhomhang.Rows.Count; i++)
                {
                    SimpleButton btnKV = new SimpleButton();
                    btnKV.Name = dtNhomhang.Rows[i]["MA_NHOMHANG"].ToString();
                    if (i == 0)
                    {
                        maNH = int.Parse(dtNhomhang.Rows[0]["MA_NHOMHANG"].ToString());
                        SetBackgroudOnClick(btnKV);
                    }
                    btnKV.Text = "" + dtNhomhang.Rows[i]["TEN_NHOMHANG"].ToString();
                    try
                    {
                        byte[] tam = new byte[((byte[])dtNhomhang.Rows[i]["HINHANH"]).LongLength];
                        tam = (byte[])dtNhomhang.Rows[i]["HINHANH"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        btnKV.Image = bm;
                    }
                    catch
                    {
                    }
                    btnKV.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btnKV.ImageLocation = ImageLocation.TopCenter;
                    btnKV.RightToLeft = RightToLeft.No;
                    btnKV.Size = new Size(125, 80);
                    if (i != 0)
                    {
                        locateY += int.Parse(btnKV.Location.Y.ToString()) + 85;
                    }
                    btnKV.Location = new Point(locateX, locateY);
                    btnKV.Click += new EventHandler(btnKV_Click);
                    pnKV_Mon.Controls.Add(btnKV);
                }
            }
            catch
            {
            }
        }

        private void GetListMon()
        {
            DataTable dtMon = new DataTable();
            dtMon.Clear();
            int width = (pnTable_Mon.Width / 110);
            int height = (pnTable_Mon.Height / 80);
            int sizeMon = width * height;
            string sql = string.Format("Exec SelectMON @PageNumber={0},@PageSize={1}, @maNH={2}, @maKV={3},@maCH={4}", cPageMon, sizeMon, maNH, maKV, cls_ConfigCashier.idShop);
            dtMon = clsMain.ReturnDataTable(sql);

            while (pnTable_Mon.Controls.Count > 0)
            {
                pnTable_Mon.Controls[0].Dispose();
            }
            pnTable_Mon.Controls.Clear();
            int count = (pnTable_Mon.Width / 120) + 1;
            int line = 0;
            int locateY = 2;
            int locateX = 2;
            int x = 0;
            int y = 0;
            for (int i = 0; i < dtMon.Rows.Count; i++)
            {
                line++;
                SimpleButton btnBan_Mon = new SimpleButton();
                btnBan_Mon.Name = dtMon.Rows[i]["MA_HANGHOA"].ToString();
                btnBan_Mon.Text = dtMon.Rows[i]["TEN_HANGHOA"].ToString() + "\n" + "Giá:" + string.Format("{0:0,0 }", double.Parse(dtMon.Rows[i]["GIABAN"].ToString())) + "";
                btnBan_Mon.Tag = dtMon.Rows[i]["TEN_HANGHOA"].ToString() + " - " + "Giá:" + string.Format("{0:0,0 }", double.Parse(dtMon.Rows[i]["GIABAN"].ToString())) + "";
                try
                {
                    byte[] tam = new byte[((byte[])dtMon.Rows[i]["HINHANH"]).LongLength];
                    tam = (byte[])dtMon.Rows[i]["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    btnBan_Mon.Image = bm;
                }
                catch
                {
                }
                btnBan_Mon.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btnBan_Mon.ImageLocation = ImageLocation.TopCenter;
                btnBan_Mon.RightToLeft = RightToLeft.No;
                btnBan_Mon.Size = new Size(110, 80);
                if (line == count)
                {
                    locateY += int.Parse(btnBan_Mon.Location.Y.ToString()) + 85;
                    locateX = 2;
                    line = 1;
                }
                else
                {
                    if (i != 0)
                    {
                        if (this.Width < 1100)
                        {
                            locateX += int.Parse(btnBan_Mon.Location.X.ToString()) + 132;
                        }
                        else
                        {
                            locateX += int.Parse(btnBan_Mon.Location.X.ToString()) + 124;
                        }
                    }
                }
                btnBan_Mon.Location = new Point(locateX, locateY);
                btnBan_Mon.Click += new EventHandler(btnBan_Mon_Click);
                pnTable_Mon.Controls.Add(btnBan_Mon);
            }
        }

        private void btnKV_Click(object sender, EventArgs e)
        {
            cPageBan = 1;
            cPageMon = 1;
            lbPageTable.Text = cPageBan.ToString();
            btnComboOption.Visible = false;
            foreach (SimpleButton btnKV in pnKV_Mon.Controls)
            {
                if (btnKV.Name == this.ActiveControl.Name)
                {
                    SetBackgroudOnClick(btnKV);
                    if (funtions == 1)
                    {
                        lbNameMon.Text = "";
                        lbNameTable.Text = "";
                        lbKhuVuc.Text = btnKV.Text + "  >  ";
                        maBan = "";
                        maKV = int.Parse(btnKV.Name.ToString());
                        GetSoDoBan();
                    }
                    if (funtions == 2)
                    {
                        maNH = int.Parse(btnKV.Name.ToString());
                        GetListMon();
                    }
                    if (funtions == 11)
                    {
                        funtions = 1;
                        lbNameMon.Text = "";
                        lbNameTable.Text = "";
                        lbKhuVuc.Text = btnKV.Text + "  >  ";
                        maKV = int.Parse(btnKV.Name.ToString());
                        GetSoDoBan();
                        funtions = 11;
                    }
                    if (selectDishes == 2)
                    {
                        funtions = 1;
                        lbNameMon.Text = "";
                        lbNameTable.Text = "";
                        lbKhuVuc.Text = btnKV.Text + "  >  ";
                        maBan = "";
                        maKV = int.Parse(btnKV.Name.ToString());
                        GetSoDoBan();
                        funtions = 9;
                           
                    }
                    if (selectDishes == 1)
                    {
                        funtions = 2;
                        maNH = int.Parse(btnKV.Name.ToString());
                        GetListMon();
                        funtions = 9;
                    }
                }
                else
                {
                    SetDefautlBackgroud(btnKV);
                }
            }
        }

        private void btnBan_Mon_Click(object sender, EventArgs e)
        {
            try
            {
                if (funtions == 9)
                {
                    if (selectDishes == 0)
                    {
                        XtraMessageBox.Show("Enter vào đây rồi ngon qua ta !");
                        return;
                    }
                    foreach (SimpleButton btnBan in pnTable_Mon.Controls)
                    {
                        if (btnBan.Name == this.ActiveControl.Name)
                        {
                            if (selectDishes == 1)
                            {
                                sendOrder = 1;
                                maMon = btnBan.Name;
                                if (selectTable == 1)
                                {
                                    selectTable = 0;
                                    dtLuoi.Rows.Clear();
                                    gr_SelectProduct.RefreshDataSource();
                                }
                                GetDishGoToBuy(maMon, 0);
                                selectDishes = 0;
                            }
                            else
                            {
                                if (selectDishes == 2)
                                {
                                    maBan = btnBan.Name;
                                    if (CheckOpenTable(maBan) == false)
                                    {
                                        if (CheckHaveISBANDAT(int.Parse(maBan)) == true)// research find table check
                                        {
                                            if (SelectTablesCheckt(maBan) == true)// kiểm tra time of this table have ok or no 
                                                SelectCheckTable(maBan); // kiểm tra bàn này đã chọn đặt chưa ?
                                            else
                                            {
                                                return;
                                            }
                                        }
                                        else
                                            SelectCheckTable(maBan);
                                    }
                                    else
                                    {
                                        if (XacNhanBanDat(maBan) == true)
                                        {
                                            SelectCheckTable(maBan);
                                        }
                                        else
                                        {
                                            return;
                                        }
                                    }
                                    selectDishes = 0;
                                }
                            }
                        }
                    }
                    //// select Table                    
                    pnCheckTable.BringToFront();
                    us_keyboard2.SetTextbox(txtDBTenKH);
                    txtTenKhacHang.Focus();
                    return;
                }

                btnComboOption.Visible = false;
                pnBtnDeleteGv.Visible = false;
                foreach (SimpleButton btnBan_Mon in pnTable_Mon.Controls)
                {
                    if (btnBan_Mon.Name == this.ActiveControl.Name)
                    {
                        //Tính giờ Karaoke
                        DataTable dataInPut = getGioVaoBan(btnBan_Mon.Name.ToString());
                        DataTable dtmakv = clsMain.ReturnDataTable("Select MA_KHUVUC From BAN Where MA_BAN = " + btnBan_Mon.Name);
                        if (dataInPut.Rows.Count > 0)
                        {
                            if (dataInPut.Rows[0]["GIOVAOKARAOKE"].ToString() != "")
                            {
                                giovao = Convert.ToDateTime(dataInPut.Rows[0]["GIOVAOKARAOKE"].ToString());
                                giora = Convert.ToDateTime(clsMain.ReturnDataTable("SELECT GETDATE()").Rows[0][0].ToString());
                                string sqltiengio = string.Format("SET DATEFORMAT MDY exec TinhTienGio_Karaoke @Giovao='{0:MM/dd/yyyy HH:mm:ss}', @Giora='{1:MM/dd/yyyy HH:mm:ss}', @Khuvuc={2}", giovao, giora, dtmakv.Rows[0]["MA_KHUVUC"].ToString());
                                dtTiengio = clsMain.ReturnDataTable(sqltiengio);
                                DataTable dtSet = clsMain.ReturnDataTable("SET DATEFORMAT DMY");
                                if (dtTiengio.Rows.Count > 0)
                                {
                                    if (float.Parse(dtTiengio.Rows[0]["TONGCONG"].ToString()) > 0)// tránh trường hợp đặt phòng trước giờ vào sẽ lớn làm tiền giờ âm
                                        lbTiengio.Text = string.Format("{0:#,###0}", float.Parse(dtTiengio.Rows[0]["TONGCONG"].ToString()));
                                }
                                else
                                {
                                    lbTiengio.Text = "0";
                                }
                            }
                        }
                        else
                        {
                            lbTiengio.Text = "0";
                        }
                        //
                        SetBackgroudOnClick(btnBan_Mon);
                        if (funtions == 1)
                        {
                            SetBackgroudOnClick(btn_ThuNgan);
                            SetDefautlBackgroud(btn_NghiepVu);
                            pnThuNgan.BringToFront();
                            sendOrder = 0;
                            maBan = btnBan_Mon.Name;
                            if (btnBan_Mon.Text.IndexOf("\n") > 0)
                            {
                                lbNameTable.Text = btnBan_Mon.Text.Substring(0, btnBan_Mon.Text.IndexOf("\n"));
                                lbTableTatol.Text = btnBan_Mon.Text.Substring(0, btnBan_Mon.Text.IndexOf("\n"));
                            }
                            else
                            {
                                lbNameTable.Text = btnBan_Mon.Text;
                                lbTableTatol.Text = btnBan_Mon.Text;
                            }
                            lbNameMon.Text = "";
                            lbTatol.Text = "0";
                            if (CheckOpenTable(maBan) == true)
                            {
                                selectTable = 1;
                                string maHOADON = GetMaHoaDon(maBan);
                                lbTatol.Text = "0";
                                GetDishOfTable(maHOADON);
                            }
                            else
                            {
                                dtLuoi.Rows.Clear();
                                gr_SelectProduct.DataSource = dtLuoi;
                            }
                        }
                        if (funtions == 2)
                        {
                            sendOrder = 1;
                            if (selectTable == 1)
                            {
                                selectTable = 0;
                                dtLuoi.Rows.Clear();
                                gr_SelectProduct.DataSource = dtLuoi;
                            }
                            maMon = btnBan_Mon.Name;
                            lbNameMon.Text = " >   " + btnBan_Mon.Tag;
                            GetDishGoToBuy(maMon, maKV);
                        }
                        if (funtions == 5)
                        {
                            maBanGhep = btnBan_Mon.Name;
                            string idHD = GetMaHoaDon(maBan);
                            string idTableJoin = GetMaHoaDon(maBanGhep);
                            if (maBan == maBanGhep || idHD == idTableJoin)
                            {
                                return;
                            }
                            funtions = 1;
                            lbNameMon.Text = " > " + btnBan_Mon.Text;
                            SetJoinTable(maBan, maBanGhep);
                            GetDishOfTable(idHD);
                            maBanGhep = "";
                            lbListTable.Text = "Sơ đồ phòng";
                            GetSoDoBan();
                        }
                        if (funtions == 6)
                        {
                            maBanGhep = btnBan_Mon.Name;
                            txtmaKVBanGhep.Text = lbKhuVuc.Text;
                            txtmaBanGhep.Text = btnBan_Mon.Text;
                            if (dtLuoi.Rows.Count > 0 && maBanGhep != "")
                            {
                                List<DM_ListDisheTableOne> lisHangHoa = new List<DM_ListDisheTableOne>();
                                //add gridview
                                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                                {
                                    DM_ListDisheTableOne dmHangHoa = new DM_ListDisheTableOne();
                                    dmHangHoa._MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                                    dmHangHoa._TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                                    dmHangHoa._SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                                    dmHangHoa._GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                                    dmHangHoa._Thanh_Tien = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"]).ToString());
                                    dmHangHoa._GHICHU = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"]).ToString();
                                    lisHangHoa.Add(dmHangHoa);
                                }
                                convert cvrt = new convert();
                                divide_Table1 = cvrt.ConvertToDataTable(lisHangHoa);
                                gr_divide_Table_1.DataSource = divide_Table1;
                                gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.RowCount - 1;
                                //
                                pnHuymon.BringToFront();
                                //Inbox Table
                                int w = (pn_GroupTable.Width / 2) - pnUpDow.Width;
                                pn_TableTow.Width = w + 30;
                                pn_TableShare.Width = w + 30;
                                labelControl12.Location = new Point((pnHdCheckTable.Width / 2) - (labelControl12.Width / 2), labelControl12.Location.Y);
                            }
                            else
                            {
                            }
                        }
                        if (funtions == 11)
                        {
                            maBanGhep = btnBan_Mon.Name;
                            if (maBanGhep == maBan)
                            {
                                return;
                            }
                            txtmaKVBanGhep.Text = lbKhuVuc.Text;
                            txtmaBanGhep.Text = btnBan_Mon.Text;

                            if (CheckOpenTable(maBanGhep))
                            {
                                string maHD = GetMaHoaDon(maBan);
                                string maHD_banghep = GetMaHoaDon(maBanGhep);
                                string sSQL = "";
                                sSQL += "Select MA_HANGHOA as MA,SOLUONG,GIABAN,THUE,TRANGTHAI" + "\n";
                                sSQL += "From CT_HOADON_KARAOKE" + "\n";
                                sSQL += "Where MA_HOADON =" + clsMain.SQLString(maHD);
                                DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                                int n = dt1.Rows.Count;
                                for (int i = 0; i < n; i++)
                                {
                                    if (!ChecBishInTable(maHD_banghep, dt1.Rows[i]["MA"].ToString()))
                                    {
                                        sSQL = "";
                                        sSQL += "Insert Into CT_HOADON_KARAOKE(MA_HOADON,MA_HANGHOA,SOLUONG,GIABAN,THUE,TRANGTHAI)" + "\n";
                                        sSQL += "Values(";
                                        sSQL += clsMain.SQLString(maHD_banghep) + ",";
                                        sSQL += clsMain.SQLString(dt1.Rows[i]["MA"].ToString()) + ",";
                                        sSQL += clsMain.SQLString(dt1.Rows[i]["SOLUONG"].ToString()) + ",";
                                        sSQL += clsMain.SQLString(dt1.Rows[i]["GIABAN"].ToString()) + ",";
                                        sSQL += clsMain.SQLString(dt1.Rows[i]["THUE"].ToString()) + ",";
                                        sSQL += clsMain.SQLBit(bool.Parse(dt1.Rows[i]["TRANGTHAI"].ToString())) + ")";
                                        clsMain.ExecuteSQL(sSQL);
                                    }
                                    else
                                    {
                                        sSQL = "";
                                        sSQL += "UPDATE CT_HOADON_KARAOKE SET" + "\n";
                                        sSQL += "SOLUONG +=" + clsMain.SQLString(dt1.Rows[i]["SOLUONG"].ToString()) + "\n";
                                        sSQL += "WHERE MA_HOADON =" + clsMain.SQLString(maHD_banghep) + "\n";
                                        sSQL += "And MA_HANGHOA =" + clsMain.SQLString(dt1.Rows[i]["MA"].ToString());
                                        clsMain.ExecuteSQL(sSQL);
                                    }
                                    CanceTable(maBan);
                                    ExitDivideTable();
                                }

                            }
                            else
                            {
                                string sql = "Update BAN SET ISMO_BAN=1 where MA_BAN =" + int.Parse(maBanGhep) + "";
                                clsMain.ExecuteSQL(sql);
                                string maHD = GetMaHoaDon(maBan);
                                string sqlCTMOBAN = " Update CT_MOBAN_KARAOKE SET MA_BAN =" + int.Parse(maBanGhep) + " where MA_HOADON='" + maHD + "'";
                                clsMain.ExecuteSQL(sqlCTMOBAN);
                                string sqlpm = "Update BAN SET ISMO_BAN=0 where MA_BAN =" + int.Parse(maBan) + "";
                                clsMain.ExecuteSQL(sqlpm);
                                ExitDivideTable();
                                divide_Table1.Rows.Clear();
                                divide_Table2.Rows.Clear();
                                return;
                            }
                        }
                    }
                    else
                    {
                        SetDefautlBackgroud(btnBan_Mon);
                        if (funtions == 1)
                        {
                            SetDefautlBackgroud(btnBan_Mon);
                            for (int i = 0; i < dtBan.Rows.Count; i++)
                            {
                                if (bool.Parse(dtBan.Rows[i]["ISBANDAT"].ToString()) == true && btnBan_Mon.Name == dtBan.Rows[i]["MA_BAN"].ToString())
                                {
                                    SetBackgroudOnClick2(btnBan_Mon);
                                }
                                if (bool.Parse(dtBan.Rows[i]["ISMO_BAN"].ToString()) == true && btnBan_Mon.Name == dtBan.Rows[i]["MA_BAN"].ToString())
                                {
                                    SetBackgroudOnClick1(btnBan_Mon);
                                }
                                if (bool.Parse(dtBan.Rows[i]["SEE_PAYMENTS"].ToString()) == true && btnBan_Mon.Name == dtBan.Rows[i]["MA_BAN"].ToString())
                                {
                                    SetBackgroudOnClick3(btnBan_Mon);
                                }
                            }
                        }
                    }
                }
               
                if (funtions == 1)
                {
                    string mHD = GetMaHoaDon(maBan);
                    if (mHD != null && mHD != "")
                    {
                        DataTable dtcheck = clsMain.ReturnDataTable("select MA_BAN from CT_MOBAN_KARAOKE where MA_HOADON='" + mHD + "'");
                        if (dtcheck.Rows.Count > 1)
                        {
                            for (int i = 0; i < dtcheck.Rows.Count; i++)
                            {
                                foreach (SimpleButton btnTable in pnTable_Mon.Controls)
                                {
                                    if (btnTable.Name == dtcheck.Rows[i]["MA_BAN"].ToString())
                                    {
                                        SetBackgroudOnClick(btnTable);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "THÔNG BÁO", MessageBoxButtons.OK);
            }
        }

        public bool CheckOpenTable(string idTable)
        {
            string sql = "select ISMO_BAN from BAN where MA_BAN =" + int.Parse(idTable) + "";
            DataTable checkTable = clsMain.ReturnDataTable(sql);
            if (checkTable.Rows.Count > 0 && bool.Parse(checkTable.Rows[0]["ISMO_BAN"].ToString()) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void GetDishOfTable(string maHoaDon)
        {
            dtLuoi.Rows.Clear();
            string sSQL = "";
            sSQL += "Select cthd.MA_HANGHOA, hh.TEN_HANGHOA, cthd.SOLUONG,  cthd.GIABAN,  'a' as GHICHU, hh.MA_BEP, cthd.THUE, ";
            sSQL += "0 as Thanh_Tien, ISNULL(TRANGTHAI,0) AS TRANGTHAI,ISNULL(TRANGTHAI,0) as IS_COMBO,hh.IS_INBEP,'' as IS_KHUYENMAI, ISNULL(cthd.CHIETKHAU,0) AS CHIETKHAU, ";
            sSQL += "hd.GIAMGIA, hd.PHIPHUCVU, (select SUM(THANHTIEN) from CT_TIENGIOKARAOKE cttg where cttg.MA_HOADON = cthd.MA_HOADON) as TienGio ";
            sSQL += "From CT_HOADON_KARAOKE as cthd inner join HOADON_KARAOKE hd on cthd.MA_HOADON = hd.MA_HOADON inner join HANGHOA as hh on cthd.MA_HANGHOA=hh.MA_HANGHOA where cthd.MA_HOADON ='" + maHoaDon + "'";
            dtLuoi = clsMain.ReturnDataTable(sSQL);
            dtHangOrder = dtLuoi.Copy();
            gr_SelectProduct.DataSource = dtLuoi;
            if (dtLuoi.Rows.Count > 0)
            {
                if (dtLuoi.Rows[0]["PHIPHUCVU"].ToString() != "")
                    lbPhuThu.Text = string.Format("{0:#,###0}", float.Parse(dtLuoi.Rows[0]["PHIPHUCVU"].ToString()));
                if (dtLuoi.Rows[0]["GIAMGIA"].ToString() != "")
                    lbGiamGia.Text = string.Format("{0:#,###0}", float.Parse(dtLuoi.Rows[0]["GIAMGIA"].ToString()));
                if (dtLuoi.Rows[0]["TienGio"].ToString() != "")
                    lbTiengio.Text = string.Format("{0:#,###0}", float.Parse(dtLuoi.Rows[0]["TienGio"].ToString()));
            }
            if (gv_SelectProduct.RowCount > 0)
            {
                TotalMoney();
            }
            else
                lbTatol.Text = "0";
            gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.RowCount - 1;
        }

        private void GetDishGoToBuy(string maHangHoa, int idKV)
        {
            try
            {
                string sql = string.Format("Exec SelectByMON @MAHANG={0},@MAVACH='{1}',@maKV={2},@maCH={3}", maHangHoa, maHangHoa, idKV, cls_ConfigCashier.idShop);
                DataTable dtHH = clsMain.ReturnDataTable(sql);
                if (dtHH.Rows.Count < 1)
                {
                    return;
                }
                DM_HangHoa hanghoa = new DM_HangHoa();
                decimal Giaban_Cur = 0; //giá bán thực của hàng hóa.
                bool flag_Exist = false; // flag_Exist=false: mặt hàng này chưa bán và ngược lại

                if (dtHH.Rows.Count >= 1)
                {
                    if (bool.Parse(dtHH.Rows[0]["SUAGIA"].ToString()))
                    {
                        if (gv_SelectProduct.RowCount > 0)
                        {
                            for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                            {
                                if (gv_SelectProduct.GetRowCellValue(i, "MA_HANGHOA").ToString() == maHangHoa)
                                {
                                    flag_Exist = true;
                                    break;
                                }
                            }
                        }
                        if (!flag_Exist)
                        {
                            Frm_Chontheothoigia frm = new Frm_Chontheothoigia();
                            frm.ShowDialog();
                            Giaban_Cur = frm.tmp_Thoigia;//Lấy giá theo thời giá người dùng nhập vào
                            if (Giaban_Cur == 0)//chưa nhập thời giá thì return
                                return;
                            btnGiaBan2.Tag = 1;
                        }
                    }
                    else
                    {
                        Giaban_Cur = decimal.Parse(dtHH.Rows[0]["GIABAN"].ToString());
                        btnGiaBan2.Tag = 1;
                        btnGiaBan2.Text = "Bán giá 2";
                    }
                    hanghoa.MA_HANGHOA = dtHH.Rows[0]["MA_HANGHOA"].ToString();
                    hanghoa.TEN_HANGHOA = dtHH.Rows[0]["TEN_HANGHOA"].ToString();
                    hanghoa.SOLUONG = 1;
                    hanghoa.MA_BEP = dtHH.Rows[0]["MA_BEP"].ToString();
                    hanghoa.GIABAN = Giaban_Cur;
                    hanghoa.GHICHU = dtHH.Rows[0]["GHICHU"].ToString();
                    hanghoa.THUE = double.Parse(dtHH.Rows[0]["THUE"].ToString());
                }
                if (dtLuoi.Rows.Count < 1)
                {
                    testID = false;
                }
                else
                {
                    testID = true;
                }
                for (int i = 0; i < dtLuoi.Rows.Count; i++)
                {
                    if (dtLuoi.Rows[i]["MA_HANGHOA"].ToString() == hanghoa.MA_HANGHOA.ToString())
                    {
                        var dtCombo = GetProductInCombo(hanghoa.MA_HANGHOA);
                        if (dtCombo.Rows.Count > 0)
                        {
                            DataRow dr = dtLuoi.NewRow();
                            dr[0] = hanghoa.MA_HANGHOA;
                            dr[1] = hanghoa.TEN_HANGHOA;
                            dr[2] = hanghoa.SOLUONG;
                            dr[3] = hanghoa.GIABAN;
                            dr[4] = hanghoa.GHICHU;
                            dr[5] = hanghoa.MA_BEP;
                            dr[6] = hanghoa.THUE;
                            dr["CHIETKHAU"] = 0;
                            dr["TRANGTHAI"] = false;//hàng hóa bình thường,được xóa hay thêm giảm
                            dr["IS_COMBO"] = false;//hàng hóa bình thường
                            dr["IS_INBEP"] = bool.Parse(dtHH.Rows[0]["IS_INBEP"].ToString());
                            dr["IS_KHUYENMAI"] = false;
                            dtLuoi.Rows.Add(dr);
                            for (int j = 0; j < dtCombo.Rows.Count; j++)
                            {
                                DataRow drr = dtLuoi.NewRow();
                                drr[0] = int.Parse(dtCombo.Rows[j]["MA_HANGHOA"].ToString());
                                drr[1] = dtCombo.Rows[j]["TEN_HANGHOA"].ToString();
                                drr[2] = dtCombo.Rows[j]["SOLUONG"].ToString();
                                drr[3] = 0;
                                drr[4] = "cb";
                                drr[5] = dtCombo.Rows[j]["MA_BEP"].ToString();
                                drr[6] = 0;
                                drr["CHIETKHAU"] = 0;
                                drr["TRANGTHAI"] = true;//hàng hóa thuộc Combo,không được xóa hay thêm giảm
                                drr["IS_COMBO"] = true;//hàng hóa thuộc Combo
                                drr["IS_INBEP"] = bool.Parse(dtHH.Rows[0]["IS_INBEP"].ToString());
                                drr["IS_KHUYENMAI"] = false;
                                dtLuoi.Rows.Add(drr);
                            }
                            iFocusRow = i;
                            testID = true;
                            break;
                        }
                        else
                        {
                            if (funtions == 8)
                                dtLuoi.Rows[i]["SOLUONG"] = double.Parse(dtLuoi.Rows[i]["SOLUONG"].ToString()) + double.Parse(hanghoa.SOLUONG.ToString());
                            else
                                dtLuoi.Rows[i]["SOLUONG"] = double.Parse(dtLuoi.Rows[i]["SOLUONG"].ToString()) + 1;

                            testID = true;
                            break;
                        }
                    }
                    else
                        testID = false;
                }

                if (testID == false)
                {
                    DataRow dr = dtLuoi.NewRow();
                    dr[0] = int.Parse(hanghoa.MA_HANGHOA);
                    dr[1] = hanghoa.TEN_HANGHOA;
                    dr[2] = hanghoa.SOLUONG;
                    dr[3] = hanghoa.GIABAN;
                    dr[4] = hanghoa.GHICHU;
                    dr[5] = hanghoa.MA_BEP;
                    dr[6] = hanghoa.THUE;
                    dr["CHIETKHAU"] = 0;
                    dr["TRANGTHAI"] = false;//hàng hóa bình thường,được xóa hay thêm giảm
                    dr["IS_COMBO"] = false;//hàng hóa bình thường
                    dr["IS_INBEP"] = bool.Parse(dtHH.Rows[0]["IS_INBEP"].ToString());
                    dr["IS_KHUYENMAI"] = false;
                    dtLuoi.Rows.Add(dr);
                    // Combo 
                    var dtCombo = GetProductInCombo(hanghoa.MA_HANGHOA);
                    int iNumberCombo = 0;
                    iNumberCombo = dtCombo.Rows.Count;
                    if (dtCombo.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtCombo.Rows.Count; i++)
                        {
                            DataRow drr = dtLuoi.NewRow();
                            drr[0] = int.Parse(dtCombo.Rows[i]["MA_HANGHOA"].ToString());
                            drr[1] = dtCombo.Rows[i]["TEN_HANGHOA"].ToString();
                            drr[2] = dtCombo.Rows[i]["SOLUONG"].ToString();
                            drr[3] = 0;
                            drr[4] = "cb";
                            drr[5] = dtCombo.Rows[i]["MA_BEP"].ToString();
                            drr[6] = 0;
                            drr["CHIETKHAU"] = 0;
                            drr["TRANGTHAI"] = true;//hàng hóa thuộc Combo,không được xóa hay thêm giảm
                            drr["IS_COMBO"] = true;//hàng hóa thuộc Combo
                            drr["IS_INBEP"] = bool.Parse(dtHH.Rows[0]["IS_INBEP"].ToString());
                            drr["IS_KHUYENMAI"] = false;
                            dtLuoi.Rows.Add(drr);
                        }
                    }
                }
                bsource.DataSource = dtLuoi;
                gr_SelectProduct.DataSource = bsource;
                gr_SelectProduct.RefreshDataSource();
                if (gv_SelectProduct.RowCount > 0)
                    TotalMoney();
                gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.RowCount - 1;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK);
            }
        }
        private DataTable GetProductInCombo(string maHangHoa)
        {
            string sSQL = "";
            sSQL += "Select cb.MA_COMBO, hh.MA_HANGHOA, hh.TEN_HANGHOA, cb.SOLUONG,HH.MA_BEP" + "\n";
            sSQL += "From COMBO cb inner join HANGHOA hh on cb.MA_HANGHOA = hh.MA_HANGHOA" + "\n";
            sSQL += "Where MA_COMBO = " + maHangHoa + "";
            var dtCombo = clsMain.ReturnDataTable(sSQL);
            return dtCombo;
        }
        private void TotalMoney()
        {
            string sqlc = " select GIATRI from CAUHINH where TEN='GIABANTRUOCTHUE' ";
            DataTable checkVAT = clsMain.ReturnDataTable(sqlc);
            bool kt = false;
            if (checkVAT.Rows[0]["GIATRI"].ToString() == "1")
                kt = true;

            double tatol = 0;
            for (int i = 0; i < gv_SelectProduct.RowCount; i++)
            {
                double thanhTien = 0, vat = 0;
                double sl = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                double gia = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                thanhTien = sl * gia;
                if (kt == true)
                    vat = (double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["THUE"]).ToString()) / 100) * thanhTien;
                tatol += thanhTien + vat;
                gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"], thanhTien + vat);
            }
            tatol = tatol + double.Parse(lbTiengio.Text);
            lbTatol.Text = string.Format("{0:#,###0}", tatol);
            lbPhuThu.Text = string.Format("{0:#,###0}", (float.Parse(lbTatol.Text.Replace(",", "")) - float.Parse(lbTiengio.Text.Replace(",", ""))));
        }

        private void btn_DowLast_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.RowCount - 1;
                btn_UpFirst.Enabled = true;
                btn_up.Enabled = true;
                btn_Dow.Enabled = false;
                btn_DowLast.Enabled = false;
                lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
            }
            catch
            {
            }
        }

        private void btn_Dow_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.FocusedRowHandle + 1;
                btn_up.Enabled = true;
                btn_UpFirst.Enabled = true;
                lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
                if (gv_SelectProduct.FocusedRowHandle == gv_SelectProduct.RowCount - 1)
                {
                    btn_Dow.Enabled = false;
                    btn_DowLast.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btn_up_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.FocusedRowHandle - 1;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
                lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
                if (gv_SelectProduct.FocusedRowHandle == 0)
                {
                    btn_up.Enabled = false;
                    btn_UpFirst.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btn_UpFirst_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = 0;
                btn_UpFirst.Enabled = false;
                btn_up.Enabled = false;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
                lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
            }
            catch
            {
            }
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            try
            {
                if (!bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
                {
                    if (!Flag_soluong)
                    {
                        if (lbSoluong.Text.Length < 3)
                            lbSoluong.Text += btn0.Text;
                    }
                    int soluong = int.Parse(lbSoluong.Text);
                    gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"], soluong);
                    TotalMoney();
                }
            }
            catch
            {
            }
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            try
            {
                SimpleButton btn = (SimpleButton)sender;
                if (!bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
                {
                    if (Flag_soluong)
                    {
                        lbSoluong.Text = btn.Text;
                        Flag_soluong = false;
                    }
                    else
                    {
                        if (lbSoluong.Text.Length < 3)
                            lbSoluong.Text += btn.Text;
                    }

                    int soluong = int.Parse(lbSoluong.Text);
                    gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"], soluong);
                    TotalMoney();
                }
            }
            catch
            {
            }
        }

        private void btn_tru_Click(object sender, EventArgs e)
        {
            try
            {
                if (!bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
                {
                    int soluong = int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                    if (soluong > 1)
                    {
                        Flag_soluong = true;
                        gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"], soluong - 1);
                        lbSoluong.Text = (soluong - 1).ToString();
                    }
                    TotalMoney();
                }
            }
            catch
            {
            }
        }

        private void btn_cong_Click(object sender, EventArgs e)
        {
            try
            {
                if (!bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
                {
                    int soluong = int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                    if (soluong < 999)
                    {
                        Flag_soluong = true;
                        gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"], soluong + 1);
                        lbSoluong.Text = (soluong + 1).ToString();
                        TotalMoney();
                    }
                }
            }
            catch
            {
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            pnBtnDeleteGv.Visible = false;
            btnComboOption.Visible = false;
        }

        private void btn_xoa_Click(object sender, EventArgs e)
        {
            if (!bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
            {
                DataTable dtCombo = GetProductInCombo(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                if (dtCombo.Rows.Count > 0)
                {
                    int n = dtCombo.Rows.Count;
                    for (int i = 0; i < n; i++)
                    {
                        gv_SelectProduct.DeleteRow(gv_SelectProduct.FocusedRowHandle + 1);
                    }
                }
                gv_SelectProduct.DeleteSelectedRows();
                btnComboOption.Visible = false;
                TotalMoney();
                if (gv_SelectProduct.RowCount <= 0)
                    pnBtnDeleteGv.Visible = false;
                else
                    lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
            }
        }

        private void btn_XoaTatCa_Click(object sender, EventArgs e)
        {
            dtLuoi.Rows.Clear();
            DataTable s = new DataTable();
            s = dtLuoi;
            //sSQL_DL = "";
            //sSQL_Yeucauthem = "";
            TotalMoney();
            pnBtnDeleteGv.Visible = false;
            btnComboOption.Visible = false;
        }

        private void gv_SelectProduct_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == SOTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
            if (gv_SelectProduct.RowCount > 0)
            {
                lb_RowsCount.Text = gv_SelectProduct.RowCount.ToString();
                lb_STT.Text = (gv_SelectProduct.FocusedRowHandle + 1).ToString();
            }
            if (1 < gv_SelectProduct.FocusedRowHandle + 1 && gv_SelectProduct.FocusedRowHandle + 1 <= gv_SelectProduct.RowCount - 1)
            {
                btn_up.Enabled = true;
                btn_UpFirst.Enabled = true;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
            }
        }

        private void gv_SelectProduct_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (funtions == 10)
                return;
            XuLyGv();
            maMon = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
            if (bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
                btnComboOption.Visible = true;
            else
                btnComboOption.Visible = false;

            string yeucau = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["GHICHU"]).ToString();
            if (yeucau != "a")
                txtYeuCauThem.Text = yeucau;
            else
                txtYeuCauThem.Text = "";
            #region Kiểm tra hàng là thời giá hay ko,nếu đúng thì ẩn button Giá bán 2 đi
            xGIABAN = float.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["GIABAN"]).ToString());
            string sSQL = "";
            sSQL = "select ISNULL(SUAGIA,0) AS SUAGIA from HANGHOA where MA_HANGHOA = " + maMon;
            DataTable dtcheck = clsMain.ReturnDataTable(sSQL);
            if (dtcheck.Rows.Count > 0)
            {
                int Apdungbanggia = int.Parse(clsMain.ReturnDataTable("select GIATRI from CAUHINH where TEN='APDUNGBANGGIATHEOKHUVUC'").Rows[0][0].ToString());
                if (Apdungbanggia == 1)//Nếu áp dụng bảng giá thì cho ẩn nút bán giá 2 đi
                    btnGiaBan2.Tag = 1;
                else if (bool.Parse(dtcheck.Rows[0]["SUAGIA"].ToString()) || funtions == 1)
                    btnGiaBan2.Tag = 1;
                else
                {
                    bool flag = false;
                    int n = dtHangOrder.Rows.Count;
                    for (int i = 0; i < n; i++)
                    {
                        if (dtHangOrder.Rows[i]["MA_HANGHOA"].ToString() == gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString())
                        {
                            flag = true;
                            break;
                        }
                    }
                    //flag=true:hàng đã order rồi,không được đổi giá.
                    if (flag)
                        btnGiaBan2.Tag = 1;
                    else
                    {
                        btnGiaBan2.Tag = 0;
                        #region Kiểm tra hàng đang ở giá nào,nếu giá 1 thì button.Name= Bán giá 2 ; nếu giá 2 thì button.Name= Bán giá 1 ;
                        sSQL = "select GIABAN1,GIABAN2 from HANGHOA where MA_HANGHOA = " + maMon + "";
                        DataTable dtgiaban = clsMain.ReturnDataTable(sSQL);
                        if (float.Parse(dtgiaban.Rows[0]["GIABAN1"].ToString()) == xGIABAN)
                        {
                            btnGiaBan2.Text = "Bán giá 2";
                        }
                        else
                        {
                            btnGiaBan2.Text = "Bán giá 1";
                        }
                    }
                        #endregion
                }
            }
            #endregion
            if (funtions == 8)
            {
                if (pnBtnDeleteGv.Visible == false)
                    pnBtnDeleteGv.Visible = true;

            }
            lb_STT.Text = gv_SelectProduct.FocusedRowHandle.ToString();
            if (sendOrder != 1)
                return;
            if (pnBtnDeleteGv.Visible == false)
                pnBtnDeleteGv.Visible = true;
            Flag_soluong = true;
            lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
            yeucau = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["GHICHU"]).ToString();
            if (yeucau != "a")
                txtYeuCauThem.Text = yeucau;
            else
                txtYeuCauThem.Text = "";
        }

        private void XuLyGv()
        {
            if (pnBtnDeleteGv.Visible)
            {
                if (gv_SelectProduct.RowCount > 5)
                {
                    pnUpDowGv.Visible = true;
                }
                else
                {
                    pnUpDowGv.Visible = false;
                }
            }
            else
            {
                if (gv_SelectProduct.RowCount > 8)
                {
                    pnUpDowGv.Visible = true;
                }
                else
                {
                    pnUpDowGv.Visible = false;
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            string hour, minute, second;
            second = DateTime.Now.Second.ToString();
            hour = DateTime.Now.Hour.ToString();
            minute = DateTime.Now.Minute.ToString();
            if (second.Length < 2)
                lbgiay.Text = "0" + second;
            else
                lbgiay.Text = second;
            if (minute.Length < 2)
                lbGio.Text = hour + ":0" + minute;
            else
                lbGio.Text = hour + ":" + minute;
            timer1.Enabled = true;
        }

        private void btn_ThuNgan_Click(object sender, EventArgs e)
        {
            if (funtions == 9 || exitReprint == true)
                return;
            SetBackgroudOnClick(btn_ThuNgan);
            SetDefautlBackgroud(btn_NghiepVu);
            pnThuNgan.BringToFront();
        }

        private void btn_NghiepVu_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
                return;
            SetBackgroudOnClick(btn_NghiepVu);
            SetDefautlBackgroud(btn_ThuNgan);
            pnNghiepVu.BringToFront();
        }

        private void btn_logout_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnMoKet_Click(object sender, EventArgs e)
        {
            KP_Terminal.clsKP_Terminal.OpenCashDrawer(false);
        }

        private void btnKetKa_Click(object sender, EventArgs e)
        {
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check = false;
                Frm_ConfigFuntion frm1 = new Frm_ConfigFuntion();
                frm1.ShowDialog();
                check = frm1.kt;
                frm1.Dispose();
                if (!check)
                {
                    return;
                }
            }
            Frm_Ketca_Karaoke frm = new Frm_Ketca_Karaoke();
            frm.Width = this.Width;
            frm.Height = this.Height;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btn_HuyBan_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
                return;
            if (maBan == "")
                return;
            if (CheckOpenTable(maBan) == false)
                return;
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion frm = new Frm_ConfigFuntion();
                frm.ShowDialog();
                check = frm.kt;
                frm.Dispose();
                if (check == false)
                    return;
            }

            string maHoaDon = GetMaHoaDon(maBan);
            DataTable table = GetTableInBill(maHoaDon);
            if (table.Rows.Count == 1)
            {
                string sSQL = "";
                sSQL += "EXEC SP_InsertHuyBan_Karaoke " + clsMain.SQLString(maHoaDon) + "," + clsMain.SQLString(table.Rows[0]["MA_BAN"].ToString()) + "," + clsMain.SQLString(clsGlobal.gsUserID) + "";
                clsMain.ExecuteSQL(sSQL);

                string sqlCTHD = "Delete from CT_HOADON_KARAOKE where MA_HOADON ='" + maHoaDon + "'";
                clsMain.ExecuteSQL(sqlCTHD);
                string sqlCTMOBAN = "Delete from CT_MOBAN_KARAOKE where MA_HOADON ='" + maHoaDon + "' and MA_BAN='" + maBan + "'";
                clsMain.ExecuteSQL(sqlCTMOBAN);
                string sqlInserter = "Delete from HOADON_KARAOKE where MA_HOADON ='" + maHoaDon + "'";
                clsMain.ExecuteSQL(sqlInserter);
                string sql = "Update BAN SET ISMO_BAN=0, SEE_PAYMENTS = 0 where MA_BAN =" + int.Parse(maBan);
                clsMain.ExecuteSQL(sql);
                string sqlOrder = "Delete from DISPLAY_ORDER where MA_BAN=" + int.Parse(maBan) + "";
            }
            else
            {
                string sqlCTMB = "Delete from CT_MOBAN_KARAOKE where MA_HOADON ='" + maHoaDon + "' and MA_BAN='" + maBan + "'";
                clsMain.ExecuteSQL(sqlCTMB);
                string sql = "Update BAN SET ISMO_BAN=0, SEE_PAYMENTS = 0 where MA_BAN =" + int.Parse(maBan);
                clsMain.ExecuteSQL(sql);
            }
            maBan = "";
            dtLuoi.Rows.Clear();
            gr_SelectProduct.RefreshDataSource();
            GetSoDoBan();
        }

        private DataTable GetTableInBill(string idBill)
        {
            string sql = "select MA_BAN from CT_MOBAN_KARAOKE where MA_HOADON='" + idBill + "'";
            DataTable retTables = clsMain.ReturnDataTable(sql);
            return retTables;
        }

        private void btn_SoDoBan_Click(object sender, EventArgs e)
        {
            if (funtions == 10)
                while (gv_SelectProduct.RowCount > 0)
                    gv_SelectProduct.DeleteRow(0);
            if (funtions == 8 || fastFoot != 0 || funtions == 6)
                return;
            if (funtions == 1 && maBan != "")
            {
                // string idTableCheck = null;
                if (CheckHaveISBANDAT(int.Parse(maBan)) == true)
                {
                    Frm_DatphongKaraoke frm = new Frm_DatphongKaraoke();
                    frm.ShowDialog();
                    idCheckTable = frm.idTable;
                    frm.Dispose();
                    if (idCheckTable == null)
                        return;
                    string sql = "Update HOADON_KARAOKE SET ISBANDAT = 0, GIOVAO = CONVERT(datetime, GetDate(), 121), NGAYTAO = GetDate()  where MA_HOADON ='" + idCheckTable + "'";
                    clsMain.ExecuteSQL(sql);
                    DataTable dtTable = clsMain.ReturnDataTable("select  MA_BAN from CT_MOBAN_KARAOKE where MA_HOADON = '" + idCheckTable + "'");
                    if (dtTable.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtTable.Rows.Count; i++)
                        {
                            string sqlT = "Update BAN SET ISMO_BAN=1, ISBANDAT = 0 where MA_BAN =" + int.Parse(dtTable.Rows[i]["MA_BAN"].ToString());
                            clsMain.ExecuteSQL(sqlT);
                        }
                    }
                }
                funtions = 2;
                btn_SoDoBan.Text = "Sơ đồ Phòng";
                lbListTable.Text = "Danh sách món:";
                pnShowHang.BringToFront();
                GetNhomhang();
                GetListMon();
                SetDefaultBackgroundBtnInPn(pnThuNgan);
                SetDefaultBackgroundBtnInPn(pnNghiepVu);
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    if (btn.Name == btn_SoDoBan.Name || btn.Name == btn_OrderSent.Name || btn.Name == btn_YeuCauThem.Name)
                    {
                        btn.Visible = true;
                    }
                    else
                    {
                        btn.Visible = false;
                    }
                }
                foreach (SimpleButton btn in pnKV_Mon.Controls)
                {
                    if (btn.Name == maNH.ToString())
                    {
                        SetBackgroudOnClick(btn);
                    }
                }
                return;
            }
            if (funtions != 1)
            {
                funtions = 1;
                btn_SoDoBan.Text = "Mở phòng";
                lbListTable.Text = "Danh sách phòng:";
                btnGiaBan2.Tag = 1;
                pnShowHang.BringToFront();
                GetKhuVuc();
                GetSoDoBan();
                SetDefaultBackgroundBtnInPn(pnThuNgan);
                SetDefaultBackgroundBtnInPn(pnNghiepVu);
                foreach (SimpleButton btn in pnKV_Mon.Controls)
                {
                    if (btn.Name == maKV.ToString())
                    {
                        SetBackgroudOnClick(btn);
                    }
                }
                foreach (SimpleButton btnb in pnTable_Mon.Controls)
                {
                    if (btnb.Name == maBan.ToString())
                    {
                        SetBackgroudOnClick(btnb);
                    }
                }
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    btn.Visible = true;
                }
            }
        }
        
        private void SetJoinTable(string idTable, string idJoinTable)
        {
            string _idDillJoin = GetMaHoaDon(idJoinTable);
            string maHOADON = GetMaHoaDon(idTable);
            if (maHOADON == null || (_idDillJoin == null && maHOADON == null))
            {
                throw new Exception("Phòng gốc phải là phòng đã mở trước khi ghép !");
            }
            if (_idDillJoin == null && maHOADON != "")
            {
                // update in table columns ISBAN_MO
                string sql = "Update BAN SET ISMO_BAN=1 where MA_BAN =" + int.Parse(idJoinTable);
                clsMain.ExecuteSQL(sql);
                string sqlCTMOBAN = "SET DATEFORMAT DMY Insert into CT_MOBAN_KARAOKE (MA_HOADON, MA_BAN)";
                sqlCTMOBAN += " values(" + "'" + maHOADON + "'," + int.Parse(idJoinTable) + ")";
                clsMain.ExecuteSQL(sqlCTMOBAN);

                for (int i = 0; i < dtBan.Rows.Count; i++)
                {
                    if (dtBan.Rows[i]["MA_BAN"].ToString() == idJoinTable)
                    {
                        dtBan.Rows[i]["ISMO_BAN"] = 1;
                    }
                }
                return;
            }
            else
            {
                string sqlCT_MOBAN = "update CT_MOBAN_KARAOKE set MA_HOADON ='" + maHOADON + "' where MA_HOADON ='" + _idDillJoin + "'";
                clsMain.ExecuteSQL(sqlCT_MOBAN);
                string sqlSelect = " select cthd.MA_HANGHOA, hh.TEN_HANGHOA, cthd.SOLUONG, cthd.GIABAN";
                sqlSelect += " from CT_HOADON_KARAOKE as cthd inner join HANGHOA as hh on cthd.MA_HANGHOA=hh.MA_HANGHOA where cthd.MA_HOADON ='" + _idDillJoin + "'";
                DataTable disheJoinTable = clsMain.ReturnDataTable(sqlSelect);
                if (disheJoinTable.Rows.Count > 0)
                {
                    bool kt = true;
                    for (int i = 0; i < disheJoinTable.Rows.Count; i++)
                    {
                        if (ChecBishInTable(maHOADON, disheJoinTable.Rows[i]["MA_HANGHOA"].ToString()) == false)
                        {
                            string sqls = "SET DATEFORMAT DMY Insert into CT_HOADON_KARAOKE ( MA_HOADON, MA_HANGHOA, SOLUONG, GIABAN)";
                            sqls += " values(" + "'" + maHOADON + "'," + int.Parse(disheJoinTable.Rows[i]["MA_HANGHOA"].ToString()) + "," + disheJoinTable.Rows[i]["SOLUONG"] + "," + disheJoinTable.Rows[i]["GIABAN"] + ")";
                            clsMain.ExecuteSQL(sqls);
                        }
                        else
                        {
                            string sqlup = "UPDATE CT_HOADON_KARAOKE SET SOLUONG +=" + disheJoinTable.Rows[i]["SOLUONG"] + "  WHERE MA_HOADON ='" + maHOADON + "' and MA_HANGHOA =" + int.Parse(disheJoinTable.Rows[i]["MA_HANGHOA"].ToString()) + "";
                            clsMain.ExecuteSQL(sqlup);
                        }
                    }
                    if (kt == true)
                    {
                        string deleteCT = "Delete FROM CT_HOADON_KARAOKE  where MA_HOADON ='" + _idDillJoin + "'";
                        clsMain.ExecuteSQL(deleteCT);
                    }
                }
                string deleteBill = "SET DATEFORMAT DMY  Delete FROM HOADON_KARAOKE  where MA_HOADON ='" + _idDillJoin + "'";
                clsMain.ExecuteSQL(deleteBill);
            }
        }
    
        // Select check Table have in flow funtion 9
        private void SelectCheckTable(string idTable)
        {
            //Checked table select 
            foreach (SimpleButton btnBan in flCheckTable.Controls)
            {
                if (btnBan.Name == this.ActiveControl.Name)
                {
                    if (btnBan.Name == maBan)
                        throw new Exception("Phòng này đã được chọn !");
                }
            }
            //Select Table
            string sql = "select MA_BAN, TEN_BAN, SOGHE, HINHANH from BAN where MA_BAN = "+idTable+"";
            DataTable dtCheckTable = clsMain.ReturnDataTable(sql);
            SimpleButton btnTables = new SimpleButton();
            btnTables.Name = dtCheckTable.Rows[0]["MA_BAN"].ToString();
            btnTables.Text = dtCheckTable.Rows[0]["TEN_BAN"].ToString() + " - " + dtCheckTable.Rows[0]["SOGHE"].ToString() + "  Người";
            btnTables.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            btnTables.ImageLocation = ImageLocation.TopCenter;
            try
            {
              byte[] tam = new byte[((byte[])dtCheckTable.Rows[0]["HINHANH"]).LongLength];
                     tam = (byte[])dtCheckTable.Rows[0]["HINHANH"];
                     MemoryStream ms = new MemoryStream(tam);
                     Bitmap bm = new Bitmap(ms);
                     btnTables.Image = bm;
            }
            catch
            {
            }
            btnTables.RightToLeft = RightToLeft.No;
            btnTables.Size = new Size(110, 80);
            btnTables.Click += new EventHandler(btnTables_Click);
            flCheckTable.Controls.Add(btnTables);
        }
        private void btnTables_Click(object sender, EventArgs e)
        {
            if (funtions == 9)
            {
                foreach (SimpleButton btnTables in flCheckTable.Controls)
                {
                    if (btnTables.Name == this.ActiveControl.Name)
                    {
                        idCheckTable = btnTables.Name ;
                        SetBackgroudOnClick(btnTables);
                    }
                    else
                        SetDefautlBackgroud(btnTables);
                }
            }
        }
        public bool XacNhanBanDat(string idTable)
        {
            DateTime now;
            string idBill = GetMaHoaDon(idTable);
            DataTable timeIn = clsMain.ReturnDataTable("Select GIOVAO from HOADON_KARAOKE where MA_HOADON ='" + idBill + "'");
            if(timeIn.Rows.Count > 0)
                now = DateTime.Parse(timeIn.Rows[0]["GIOVAO"].ToString().Substring(0, 10));
             else
            throw new Exception("Thời gian vào của phòng này không xác định !");             
            DateTime x = DateTime.Parse(txtngay.EditValue.ToString().Substring(0, 10));
            int outline = DateTime.Compare(now, x);
            if (outline == 1)
            {
                XtraMessageBox.Show(" Ngày đặt phòng không hợp lệ !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            else
            {
                if(cbHH.Text!="")
                if ((int.Parse(cbHH.Text) - int.Parse(clsMain.GetServerDate().Hour.ToString())) <= 5 && outline == 0)
                {
                    XtraMessageBox.Show("Thời gian đến không hợp lệ. Vui lòng chọn phòng khác !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
            }
            return true;
        }
        // select table isbandat
        private bool SelectTablesCheckt(string idTable) 
        {
            DateTime now;
            DataTable timeIn = clsMain.ReturnDataTable("Select HD.GIOVAO from HOADON_KARAOKE HD inner join CT_MOBAN_KARAOKE BM on HD.MA_HOADON = BM.MA_HOADON where BM.MA_BAN = " + idTable + " and HD.ISBANDAT = 1 ");
            if (timeIn.Rows.Count > 0)
            {
                for (int i = 0; i < timeIn.Rows.Count; i++)
                {
                    now = DateTime.Parse(timeIn.Rows[i]["GIOVAO"].ToString().Substring(0, 10));
                    DateTime x = DateTime.Parse(txtngay.EditValue.ToString().Substring(0, 10));
                    int outline = DateTime.Compare(now, x);
                    if (outline == 0)
                    {
                        XtraMessageBox.Show(" Phòng này đã có người đặt vui lòng chọn phòng khác !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }                    
                }
            }
            return true;
                   
        }
        public int selectTable = 0;
        public int selectDishes = 0;
       

        private bool ChecBishInTable(string _maHOADON, string maHH)
        {
            string sSQL = "";
            sSQL += "Select MA_CTHOADON" + "\n";
            sSQL += "From CT_HOADON_KARAOKE" + "\n";
            sSQL += "Where MA_HOADON =" + clsMain.SQLString(_maHOADON) + "\n";
            sSQL += "And MA_HANGHOA =" + clsMain.SQLString(maHH);
            DataTable dish = clsMain.ReturnDataTable(sSQL);
            if (dish.Rows.Count > 0)
                return true;
            return false;
        }

      
        private void SelectProduct()
        {
            string sql = "SELECT TOP 21  *   FROM YEUCAUTHEM ";
            sql += "  WHERE  id  not in ( SELECT TOP " + "0" + "  id  FROM YEUCAUTHEM  where SUDUNG =0 ORDER BY  YEUCAU  DESC ) ";
            sql += " AND  SUDUNG =0 ORDER BY  YEUCAU DESC  ";
            DataTable requestAdd = clsMain.ReturnDataTable(sql);
            AddButtonYeuCauThem(requestAdd);
        }
        // Loading yêu câu thêm
        private void AddButtonYeuCauThem(DataTable table) 
        {
            pnYeuCau.Controls.Clear();
            int line = 0;
            int locateY = 7;
            int locateX = 10;
            if (table.Rows.Count <= 0)
                return;
            for (int i = 0; i < table.Rows.Count; i++)
            {
                line++;
                SimpleButton btn = new SimpleButton();
                btn.Name = table.Rows[i]["id"].ToString();
                btn.Text = table.Rows[i]["YEUCAU"].ToString();
                btn.Click += new EventHandler(btn_Click);
                btn.RightToLeft = RightToLeft.No;
                btn.Size = new Size(130, 70);
                if (line == 8)
                {
                    locateY += int.Parse(btn.Location.Y.ToString()) + 80;
                    locateX = 10;
                    line = 1;
                }
                else
                {
                    if (i != 0)
                        locateX += int.Parse(btn.Location.X.ToString()) + 140;
                }
                btn.Location = new Point(locateX, locateY);
                pnYeuCau.Controls.Add(btn);
            }
        }
        
        private void SetTexBoxBtn( FlowLayoutPanel pn) 
        {
            foreach (SimpleButton btn in pn.Controls)
            {
                if (btn.Name == this.ActiveControl.Name)
                    txtYeuCauThem.Text += btn.Text + "," + " ";
                
            }
            txtYeuCauThem.Focus();
        }
        private void btn_Click(object sender, EventArgs e)
        {
            SetTexBoxBtn(pnYeuCau);
            int textLength = txtYeuCauThem.Text.Length;
            txtYeuCauThem.SelectionStart = textLength;
            txtYeuCauThem.SelectionLength = 0;
        }
       
       
        private void SetBackgroundBtnInPn(PanelControl pn1, PanelControl pn2)
        {
            foreach (SimpleButton bt in pn2.Controls)
            {
                SetDefautlBackgroud(bt);
            }            
            foreach (SimpleButton btn in pn1.Controls)
            {
                if (btn.Name == this.ActiveControl.Name)
                {
                    SetBackgroudOnClick(btn);
                }
                else
                {
                    SetDefautlBackgroud(btn);
                }
            }
        }
        private void SetDefaultBackgroundBtnInPn( PanelControl pn2)
        {
            foreach (SimpleButton bt in pn2.Controls)
            {
                SetDefautlBackgroud(bt);
            }
        }

       
        private string GetNewCodeBill()
        {
            string _mahoadon="";
            string ngay = cls_ConfigCashier.idCashier + DateTime.Now.Day.ToString("00") + DateTime.Now.Month.ToString("00") + DateTime.Now.Year.ToString("0000");
            DataTable dtMA = clsMain.ReturnDataTable("select [dbo].[fc_NewcodeHoaDon_Karaoke] ('"+ ngay + "')");
            _mahoadon = dtMA.Rows[0][0].ToString();
            return _mahoadon;
        }

      

        private void btn_hien_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btn_YeuCauThem_Click(object sender, EventArgs e)
        {
            if (maMon == null || sendOrder != 1 || funtions == 6 || fastFoot != 0)
                return;
            SetBackgroundBtnInPn(pnThuNgan, pnNghiepVu);
           
            pnYeucauthem.BringToFront();
            us_keyboard3.Left = panelControl5.Width / 2 - us_keyboard3.Width / 2;
            us_keyboard3.Top = panelControl5.Height / 2 - us_keyboard3.Height / 2;
            us_keyboard3.SetTextbox(txtYeuCauThem);
            lbkvYeuCauThem.Text = lbKhuVuc.Text;
            lbTableYeuCauThem.Text = lbNameTable.Text;
            lbMonYeuCauThem.Text = lbNameMon.Text;
            txtYeuCauThem.Focus();
            SelectProduct();
            int size = panelControl5.Width;
        }
        
        private string GetIdKitchen(string idKitchen)
        {
            string printKitchen=null;
            string sql = "select MAYINBEP from DM_BEP where MA_BEP ='"+ idKitchen +"'";
            DataTable kitchen = clsMain.ReturnDataTable(sql);
            if (kitchen.Rows.Count > 0)
            {
                printKitchen= kitchen.Rows[0]["MAYINBEP"].ToString();
                return printKitchen;
            }
            else
                return printKitchen;
        }
        private void btn_OrderSent_Click(object sender, EventArgs e)
        {
            if (funtions == 8 || fastFoot != 0 || funtions == 6)
                return;
            if (sendOrder != 1)
                return;
            try
            {
                pnBtnDeleteGv.Visible = false;
                btnComboOption.Visible = false;
                if(CheckOpenTable(maBan) == false )
                {
                    string sql = "Update BAN SET ISMO_BAN=1 where MA_BAN ="+ int.Parse(maBan);
                    clsMain.ExecuteSQL(sql);
                    string maHoaDon =GetNewCodeBill();
                    string sqlInserter = "SET DATEFORMAT DMY Insert into HOADON_KARAOKE(MA_HOADON, MANHANVIEN, NGAYTAO, GIOVAO, ISPAYMENT, ISBANDAT)";
                    sqlInserter += " values(" + "'" + maHoaDon + "'," + int.Parse(KP_UserManagement.clsGlobal.gsMaNVOfUserLogin) + ", CONVERT(datetime, GetDate(), 121),CONVERT(datetime, GetDate(), 121),0,0)";
                    clsMain.ExecuteSQL(sqlInserter);
                    string sqlCTMOBAN = "SET DATEFORMAT DMY Insert into CT_MOBAN_KARAOKE(MA_HOADON, MA_BAN)";
                    sqlCTMOBAN += " values(" + "'" + maHoaDon + "'," + int.Parse(maBan) + ")";
                    clsMain.ExecuteSQL(sqlCTMOBAN);
                }
                int _if = 0;
                string maHDB = GetMaHoaDon(maBan);
                string sSQL = "Select TEN_BAN, NGAYTAO" + "\n";
                sSQL += "From BAN A, HOADON_KARAOKE B, CT_MOBAN_KARAOKE C" + "\n";
                sSQL += "Where B.MA_HOADON = C.MA_HOADON" + "\n";
                sSQL += "AND A.MA_BAN = C.MA_BAN" + "\n";
                sSQL += "AND C.MA_HOADON = '" + maHDB + "' \n";
                DataTable myDT = clsMain.ReturnDataTable(sSQL);
                string sMaban = myDT.Rows[0]["TEN_BAN"].ToString();
                string sNgaytao = myDT.Rows[0]["NGAYTAO"].ToString();
                while (gv_SelectProduct.RowCount > 0)
                {
                    List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                    _if = 1;
                    int j = 0;
                    string maBEP="";
                    for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                    {
                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        maBEP = gv_SelectProduct.GetRowCellValue(0, gv_SelectProduct.Columns["MA_BEP"]).ToString();
                        if (maBEP == gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_BEP"]).ToString())
                        {
                            dmHangHoa.TEN_BAN = sMaban;
                            dmHangHoa.Ngay = sNgaytao;
                            dmHangHoa.Ma_HoaDon = maHDB;
                            dmHangHoa.Ten_HoaDon = "Order Bếp";
                            dmHangHoa.MaQuay = cls_ConfigCashier.nameCashier;
                            dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                            dmHangHoa.MaBan = maBan;
                            dmHangHoa.STT = (++j).ToString();
                            dmHangHoa.MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                            dmHangHoa.TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                            dmHangHoa.SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                            dmHangHoa.THUE = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["THUE"]).ToString());
                            dmHangHoa.GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                            string repuest= gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"]).ToString();
                            if (repuest == "a")
                                dmHangHoa.GHICHU = "";
                            else
                                dmHangHoa.GHICHU = repuest;
                            //Kiểm tra hàng hóa có in bếp ko
                            string sqlinbep = " SELECT ISNULL(IS_INBEP,0) AS IS_INBEP FROM HANGHOA WHERE MA_HANGHOA = " + dmHangHoa.MA_HANGHOA;
                            DataTable dtinbep = clsMain.ReturnDataTable(sqlinbep);
                            if (bool.Parse(dtinbep.Rows[0]["IS_INBEP"].ToString()) == true)
                                listDM_HangHoa.Add(dmHangHoa);

                            if (maHDB != null)
                            {
                                if (ChecBishInTable(maHDB, dmHangHoa.MA_HANGHOA) == false)
                                {
                                    string sql = "SET DATEFORMAT DMY Insert into CT_HOADON_KARAOKE( MA_HOADON, MA_HANGHOA,  SOLUONG, GIABAN, THUE)";
                                    sql += " values(" + "'" + maHDB + "'," + int.Parse(dmHangHoa.MA_HANGHOA) + "," + dmHangHoa.SOLUONG + "," + dmHangHoa.GIABAN + "," + dmHangHoa.THUE + ")";
                                    clsMain.ExecuteSQL(sql);
                                }
                                else
                                {
                                    string sql = "UPDATE CT_HOADON_KARAOKE SET SOLUONG +=" + dmHangHoa.SOLUONG + "  WHERE MA_HOADON ='" + maHDB + "' and MA_HANGHOA =" + int.Parse(dmHangHoa.MA_HANGHOA) + "";
                                    clsMain.ExecuteSQL(sql);                                    
                                }
                                string sqlOder = "SET DATEFORMAT DMY Insert into DISPLAY_ORDER (MA_HANGHOA, MA_BAN, GIOORDER, SOLUONG, YEUCAU)";
                                sqlOder += " values(" + "" + int.Parse(dmHangHoa.MA_HANGHOA) + "," + int.Parse(maBan) + ", GETDATE()," + dmHangHoa.SOLUONG + ", N'" + dmHangHoa.GHICHU + "')";
                                clsMain.ExecuteSQL(sqlOder);
                            }
                            else
                                throw new Exception("Không thể lập hóa đơn cho phòng này");
                            // Delete product selected 
                            if (i != 0)
                            {
                                gv_SelectProduct.DeleteRow(i);
                                i--;
                            }                            
                        }                     
                    }                   
                    string print= GetIdKitchen(maBEP);
                    if (print == null)
                        XtraMessageBox.Show("Chưa chọn máy in cho mặt hàng này !","Thông Báo ",MessageBoxButtons.OK);
                    gv_SelectProduct.DeleteRow(0);// Delete rows(0)
                    // Printer Order 
                    convert cvrt = new convert();
                    DataTable Orderbill = cvrt.ConvertToDataTable(listDM_HangHoa);
                    if (Orderbill.Rows.Count > 0)
                    {
                        KP_Report.Frm_Report1 frm = new KP_Report.Frm_Report1();
                        frm.ReportName = "rpt_OrderKaraoke";
                        frm.DataSource = Orderbill;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.PrinterName = print;
                        frm.IsPrint = true;
                        frm.ShowDialog();
                        frm.Dispose();
                    }
                }               
                if (_if == 1)
                {
                    dtLuoi.Rows.Clear();
                    gr_SelectProduct.RefreshDataSource();
                    GetDishOfTable(GetMaHoaDon(maBan));                    
                }
                maMon = "";
                // return Lish table 
                sendOrder = 0;
                lbNameMon.Text = "";
                funtions = 1;
                btn_SoDoBan.Text = "Mở phòng";
                lbListTable.Text = "Danh sách phòng:";
                pnShowHang.BringToFront();
                GetKhuVuc();
                GetSoDoBan();
                SetDefaultBackgroundBtnInPn(pnThuNgan);
                SetDefaultBackgroundBtnInPn(pnNghiepVu);
                foreach (SimpleButton btn in pnKV_Mon.Controls)
                {
                    if (btn.Name == maKV.ToString())
                    {
                        SetBackgroudOnClick(btn);
                    }
                }
                foreach (SimpleButton btnb in pnTable_Mon.Controls)
                {
                    if (btnb.Name == maBan.ToString())
                    {
                        SetBackgroudOnClick(btnb);
                    }
                }    
                string mHD = GetMaHoaDon(maBan);
                if (funtions == 1 && mHD != null)
                {
                    DataTable dtcheck = clsMain.ReturnDataTable("select MA_BAN from CT_MOBAN_KARAOKE where MA_HOADON='" + mHD + "'");
                    if (dtcheck.Rows.Count > 1)
                    {
                        for (int i = 0; i < dtcheck.Rows.Count; i++)
                        {
                            foreach (SimpleButton btnTable in pnTable_Mon.Controls)
                                if (btnTable.Name == dtcheck.Rows[i]["MA_BAN"].ToString())
                                    SetBackgroudOnClick(btnTable);
                        }
                    }
                }
				foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    btn.Visible = true;
                }
            }
            catch (Exception es)
            {
                XtraMessageBox.Show(es.Message, "Thông Báo", MessageBoxButtons.OK);
            }
        }
    
        public string tongtiengkhachtra = "0";
     
        private bool CheckProductInTable(string idTable)
        {
            string idBill = GetMaHoaDon(idTable);
            string sql = "select * from CT_HOADON_KARAOKE where MA_HOADON ='" + idBill + "'";
            DataTable check = clsMain.ReturnDataTable(sql);
            if (check.Rows.Count > 0)
                return true;
              return false;
        }
     
        private void btn_Payment_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
                return;
            if (maBan == "")
                return;
            if (gv_SelectProduct.RowCount <= 0)
                return;
            //Lấy mã hóa don
            string idBill = GetMaHoaDon(maBan);
            if (idBill == "")
                return;
            if (giovao.Year < 1000)
                return;
            Frm_Chonthoigianra frmgiora = new Frm_Chonthoigianra();
            frmgiora.giovao = giovao;
            frmgiora.ShowDialog();
            giora = frmgiora.giora;
            giovao = frmgiora.giovao;
            frmgiora.Dispose();
            clsMain.ExecuteSQL("SET DATEFORMAT DMY Update HOADON_KARAOKE set GIOVAO='" + String.Format("{0:dd/MM/yyyy HH:mm:ss}", giovao) + "' , GIORA='" + String.Format("{0:dd/MM/yyyy HH:mm:ss}", giora) + "' where MA_HOADON='" + idBill + "'");
            DataTable dtmakv = clsMain.ReturnDataTable("Select MA_KHUVUC From BAN Where MA_BAN = " + maBan);
            string sqltiengio = string.Format("SET DATEFORMAT DMY exec TinhTienGio_Karaoke @Giovao='{0:dd/MM/yyyy HH:mm:ss}', @Giora='{1:dd/MM/yyyy HH:mm:ss}', @Khuvuc={2}", giovao, giora, dtmakv.Rows[0]["MA_KHUVUC"].ToString());
            dtTiengio = clsMain.ReturnDataTable(sqltiengio);
            if (dtTiengio.Rows.Count > 0)
            {
                if (float.Parse(dtTiengio.Rows[0]["TONGCONG"].ToString()) > 0)// tránh trường hợp đặt phòng trước giờ vào sẽ lớn làm tiền giờ âm
                    lbTiengio.Text = string.Format("{0:#,###0}", float.Parse(dtTiengio.Rows[0]["TONGCONG"].ToString()));
            }
            else
                lbTiengio.Text = "0";


            //try
            //{
            //    payment();
            //}
            //catch
            //{
            //}
            if (gv_SelectProduct.RowCount <= 0 || funtions == 6)
                return;

            //Lấy tiền đặt cọc
            decimal Tiendatcoc = 0;
            DataTable dtMoney = clsMain.ReturnDataTable("Select TIENDATCOC from HOADON_KARAOKE where MA_HOADON='" + idBill + "' and ISNULL(TIENDATCOC,0) != 0");
            if (dtMoney.Rows.Count > 0)
                Tiendatcoc = decimal.Parse(dtMoney.Rows[0]["TIENDATCOC"].ToString());
            else
                Tiendatcoc = 0;

            btnComboOption.Visible = false;
            pnBtnDeleteGv.Visible = false;
            //gọi form thanh toán
            DataTable dt = (DataTable)gr_SelectProduct.DataSource;
            Frm_Payment_Karaoke frm = new Frm_Payment_Karaoke(maBan, idBill, dt, Tiendatcoc, decimal.Parse(lbTiengio.Text), dtTiengio, giovao, giora);
            frm.ShowDialog();
            frm.Dispose();
            // show list table
            dt.Rows.Clear();
            pnThuNgan.BringToFront();
            pnShowHang.BringToFront();
            btn_NghiepVu.Enabled = true;
            btn_ThuNgan.Enabled = true;
            btn_TitleThanhToan.SendToBack();
            SetBackgroudOnClick(btn_ThuNgan);
            funtions = 1;
            btn_SoDoBan.Text = "Mở phòng";
            lbListTable.Text = "Danh sách phòng:";
            pnShowHang.BringToFront();
            GetKhuVuc();
            GetSoDoBan();
            SetDefaultBackgroundBtnInPn(pnThuNgan);
            SetDefaultBackgroundBtnInPn(pnNghiepVu);
            foreach (SimpleButton btn in pnKV_Mon.Controls)
            {
                if (btn.Name == maKV.ToString())
                {
                    SetBackgroudOnClick(btn);
                }
            }
            lbTatol.Text = "0";
            lbTiengio.Text = "0";
            lbTableTatol.Text = "";
        }
      
        private TextEdit _textbox;
        public void SetTextbox(TextEdit edit)
        {
            _textbox = edit;
        }
     
        #region  Set text box key boarchar 

        private void txtYeuCauThem_Click(object sender, EventArgs e)
        {
            SetTextbox(txtYeuCauThem);
        }
        private void btn_OKChar_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < gv_SelectProduct.RowCount; i++)
            {
                int maHH =int.Parse( gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                if (maHH == int.Parse(maMon))
                    gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"], txtYeuCauThem.Text);
            }
            txtYeuCauThem.Text = "";
            // Return List Dishes 
            funtions = 2;
            btn_SoDoBan.Text = "Sơ đồ phòng";
            lbListTable.Text = "Danh sách món:";
            pnShowHang.BringToFront();
            GetNhomhang();
            GetListMon();
            SetDefaultBackgroundBtnInPn(pnThuNgan);
            SetDefaultBackgroundBtnInPn(pnNghiepVu);
            foreach (SimpleButton btn in pnKV_Mon.Controls)
            {
                if (btn.Name == maNH.ToString())
                {
                    SetBackgroudOnClick(btn);
                }
            }
        }

        

        private void btn_backchar_Click(object sender, EventArgs e)
        {
            int select = _textbox.SelectionStart;
            if (select != 0)
            {
                _textbox.Focus();
                _textbox.SelectionStart = select - 1;
                _textbox.SelectionLength = 0;
            }
            else
            {
                _textbox.Focus();
                _textbox.SelectionStart = _textbox.Text.Length;
                _textbox.SelectionLength = 0;
            }
        }


        private void btn_ESCChar_Click(object sender, EventArgs e)
        {
            _textbox.Text = "";
            _textbox.Focus();
        }
       
        private void btn_DeleteChar_Click(object sender, EventArgs e)
        {
            int select = _textbox.SelectionStart;
            if (select != 0)
            {
                if (_textbox.SelectionLength < 1)
                {
                    _textbox.Text = _textbox.Text.Remove(_textbox.SelectionStart - 1, 1);
                    _textbox.Focus();
                    _textbox.SelectionStart = select - 1;
                    _textbox.SelectionLength = 0;
                }
                else
                {
                    int length = _textbox.SelectionLength;
                    _textbox.Text = _textbox.Text.Remove(select, length);
                    _textbox.Focus();
                    _textbox.SelectionStart = select;
                    _textbox.SelectionLength = 0;
                }
            }
            else
            {
                _textbox.Focus();
                _textbox.SelectionStart = 0;
                _textbox.SelectionLength = 0;
            }
        }
        #endregion
        
        private void gr_SelectProduct_Leave(object sender, EventArgs e)
        {
            if (btn_xoa.Focused || btn_cong.Focused || btn_tru.Focused || gr_SelectProduct.Focused || btn_XoaTatCa.Focused || btn_Dow.Focused || btn_up.Focused || btn_UpFirst.Focused || btn_DowLast.Focused)
            {
                return;
            }
            //pnBtnDeleteGv.Visible = false;
            pnUpDowGv.Visible = false;
            gr_SelectProduct.RefreshDataSource();
        }

        private void btn_xoa_Leave(object sender, EventArgs e)
        {
            if (btn_cong.Focused || btn_tru.Focused || gr_SelectProduct.Focused)
            {
                return;
            }
            pnBtnDeleteGv.Visible = false;
        }

        private void btn_tru_Leave(object sender, EventArgs e)
        {
            if (btn_xoa.Focused || btn_cong.Focused || gr_SelectProduct.Focused)
            {
                return;
            }
            pnBtnDeleteGv.Visible = false;
        }

        private void btn_cong_Leave(object sender, EventArgs e)
        {
            if (btn_xoa.Focused || btn_tru.Focused || gr_SelectProduct.Focused)
            {
                return;
            }
            pnBtnDeleteGv.Visible = false;
        }
       
        private void btnDivideTable_Click(object sender, EventArgs e)
        {
            if (funtions == 11)
            {
                btnChuyenBan.Text = "Chuyển Phòng";
                SetDefautlBackgroud(btnChuyenBan);
                maBanGhep = "";
                funtions = 1;
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    btn.Visible = true;
                }
                return;
            }
            if (funtions != 1 || fastFoot != 0)
                return;
            if (funtions == 1)
            {
                // Check Table Opened or table paymented
                if (maBan == "")
                    return;
                if (GetMaHoaDon(maBan) == "")
                    return;
                btnChuyenBan.Text = "Hủy\nChuyển Phòng";
                SetBackgroundBtnInPn(pnThuNgan, pnNghiepVu);
                txtmaKVBanGoc.Text = lbKhuVuc.Text;
                txtmaBanGoc.Text = lbNameTable.Text;
                funtions = 11;
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    if (btn.Name == btnChuyenBan.Name)
                    {
                        btn.Visible = true;
                    }
                    else
                    {
                        btn.Visible = false;
                    }
                }
            }   
        }
        private void ExitDivideTable()
        {
            sendOrder = 0;
            funtions = 1;
            btn_SoDoBan.Text = "Mở phòng";
            lbListTable.Text = "Danh sách phòng:";
            btnChuyenBan.Text = "Chuyển Phòng";
            pnShowHang.BringToFront();
            GetKhuVuc();
            GetSoDoBan();
            SetDefaultBackgroundBtnInPn(pnThuNgan);
            SetDefaultBackgroundBtnInPn(pnNghiepVu);
            foreach (SimpleButton btn in pnKV_Mon.Controls)
            {
                if (btn.Name == maKV.ToString())
                {
                    SetBackgroudOnClick(btn);
                }
            }

            //foreach (SimpleButton btnb in pnTable_Mon.Controls)
            //{
            //    if (btnb.Name == maBan.ToString())
            //    {
            //        SetBackgroudOnClick(btnb);
            //    }
            //}
            divide_Table1.Rows.Clear();
            divide_Table2.Rows.Clear();
            string maHD = GetMaHoaDon(maBan);
            GetDishOfTable(maHD);
            maBanGhep = "";
            txtmaBanGhep.Text = "";
            txtmaKVBanGhep.Text = "";
			foreach (SimpleButton btn in pnThuNgan.Controls)
            {
                btn.Visible = true;
            }
            foreach (SimpleButton btn in pnNghiepVu.Controls)
            {
                btn.Visible = true;
            }
        }
        private void btn_ESCThroughtTable_Click(object sender, EventArgs e)
        {
            try
            {
                ExitDivideTable();
            }
            catch
            { 
            }
        }

        private void gv_divide_Table_1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == _SSTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }

            if (gv_SelectProduct.RowCount <= 0)
                lbTatol.Text = "0";
            if (gv_SelectProduct.RowCount > 0)
            {
                lb_RowsCount.Text = gv_SelectProduct.RowCount.ToString();
                lb_STT.Text = (gv_SelectProduct.FocusedRowHandle + 1).ToString();
            }
            if (gv_SelectProduct.RowCount > 0 && gv_SelectProduct.FocusedRowHandle == gv_SelectProduct.RowCount - 1)
            {
                btn_Dow.Enabled = false;
                btn_DowLast.Enabled = false;
            }
            else
            {
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
            }
            if (gv_SelectProduct.RowCount > 0 && gv_SelectProduct.FocusedRowHandle == 0)
            {
                btn_UpFirst.Enabled = false;
                btn_up.Enabled = false;
            }
            else
            {
                btn_UpFirst.Enabled = true;
                btn_up.Enabled = true;
            }
        }

        private void btn_DowDivide_Click(object sender, EventArgs e)
        {
            try
            {
                gv_divide_Table_1.Focus();
                gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.FocusedRowHandle + 1;
                btn_UpDivide.Enabled = true;            
                if (gv_divide_Table_1.FocusedRowHandle == gv_divide_Table_1.RowCount - 1)
                {
                    btn_DowDivide.Enabled = false;                     
                }
            }
            catch
            {
            }
        }

        private void btn_UpDivide_Click(object sender, EventArgs e)
        {
            try
            {
                gv_divide_Table_1.Focus();
                gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.FocusedRowHandle - 1;
                btn_DowDivide.Enabled = true;
                if (gv_divide_Table_1.FocusedRowHandle == 0)
                {
                    btn_UpDivide.Enabled = false;                   
                }
            }
            catch
            {
            }
        }
        
        private void btn_ToRightAll_Click(object sender, EventArgs e)
        {
            try
            {
                if (funtions == 1)
                {
                    while (gv_divide_Table_1.RowCount > 0)
                        ThrowTable1ToTable2();
                    return;
                }
                if (maBanGhep == "")
                {
                    pnShowHang.BringToFront();
                }
                while(gv_divide_Table_1.RowCount > 0)
                ThrowTable1ToTable2();
            }
            catch
            { }
        }
        private void ThrowTable1ToTable2()
        {
            if (gv_divide_Table_1.RowCount <= 0)
                return;
            bool checkProductOnGv = false;
            //add gridview
            if (divide_Table2.Rows.Count > 0)
            {
                for (int i = 0; i < divide_Table2.Rows.Count; i++)
                {
                    string maHH = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString();
                    if (divide_Table2.Rows[i]["__MA_HANGHOA"].ToString() == maHH)
                    {
                        divide_Table2.Rows[i]["__SOLUONG"] = decimal.Parse(divide_Table2.Rows[i]["__SOLUONG"].ToString()) + 1;
                        checkProductOnGv = true;
                    }
                }
            }
            if (checkProductOnGv == false)
            {
                DataRow dr = divide_Table2.NewRow();
                dr[0] = int.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString());
                dr[1] = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_TEN_HANGHOA"]).ToString();
                dr[2] = 1;
                //gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                dr[3] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_GIABAN"]).ToString());
                dr[4] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_MA_BEP"]).ToString());
                dr[5] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_Thanh_Tien"]).ToString());
                divide_Table2.Rows.Add(dr);
            }
            gr_divide_Table_2.DataSource = divide_Table2;
            gv_divide_Table_2.FocusedRowHandle = gv_divide_Table_2.RowCount - 1;
            if (decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString()) == 1)
                gv_divide_Table_1.DeleteSelectedRows();
            else
            {
                int sl = int.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                gv_divide_Table_1.SetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"], sl - 1);
            }
        }
        private void btn_ToRight_Click(object sender, EventArgs e)
        {
            try
            {
                if (funtions == 1)
                {
                    ThrowTable1ToTable2();
                    return;
                }
                if (maBanGhep == "")
                {
                    pnShowHang.BringToFront();
                }
                ThrowTable1ToTable2();
            }
            catch
            { }
        }
        // cance Table 
        private void CanceTable(string idTable)
        {
            try
            {
                string maHD = GetMaHoaDon(idTable);
                string sqlCTHD = " Delete CT_HOADON_KARAOKE where MA_HOADON='" + maHD + "'";
                clsMain.ExecuteSQL(sqlCTHD);
                string sqlCTMOBAN = " Delete CT_MOBAN_KARAOKE where MA_HOADON='" + maHD + "'";
                clsMain.ExecuteSQL(sqlCTMOBAN);
                string sqlHD = " Delete HOADON_KARAOKE where MA_HOADON ='" + maHD + "'";
                clsMain.ExecuteSQL(sqlHD);
                string sqlpm = "update BAN SET ISMO_BAN=0 where MA_BAN =" + int.Parse(idTable) + "";
                clsMain.ExecuteSQL(sqlpm);
            }
            catch
            { 
            }
        }

        private void btn_OkThrough_Click(object sender, EventArgs e)
        {
            try
            {  // 
                if (gv_divide_Table_2.RowCount <= 0)
                    return;
               if (funtions == 1)
                {
                    //--------------------------------
                    if (funtions == 1 && maBan != "")
                    {
                        if (CheckOpenTable(maBan) == false)
                            return;
                        string maHoaDon = GetMaHoaDon(maBan);
                        for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                        {
                            string idDisher = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                            int soLuong = int.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());

                            string sSQL = "";
                            sSQL += "EXEC SP_InsertHuyMon_Karaoke " + clsMain.SQLString(maHoaDon) + ",";
                            sSQL += clsMain.SQLString(maBan) + ",";
                            sSQL += clsMain.SQLString(idDisher) + ",";
                            sSQL += clsMain.SQLString(soLuong.ToString()) + ",";
                            sSQL += clsMain.SQLString(clsGlobal.gsUserID);
                            clsMain.ExecuteSQL(sSQL);

                            string sqlCTHD = "Update CT_HOADON_KARAOKE set SOLUONG = SOLUONG - " + soLuong + " where MA_HOADON ='" + maHoaDon + "' and MA_HANGHOA='" + idDisher + "'";
                            clsMain.ExecuteSQL(sqlCTHD);
                            string sqlDelete = "Delete from CT_HOADON_KARAOKE where SOLUONG = 0 and  MA_HOADON ='" + maHoaDon + "' and MA_HANGHOA='" + idDisher + "'";
                            clsMain.ExecuteSQL(sqlDelete);
                        }
                         while (gv_divide_Table_2.RowCount > 0)
                         {
                             List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                             int j = 0;
                             string maBEP = "";
                             for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                             {
                                 DM_HangHoa dmHangHoa = new DM_HangHoa();
                                 maBEP =gv_divide_Table_2.GetRowCellValue(0, gv_divide_Table_2.Columns["__MA_BEP"]).ToString();
                                 if(maBEP == gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_BEP"]).ToString())
                                 {
                                     dmHangHoa.Ma_HoaDon = maHoaDon;
                                     dmHangHoa.Ten_HoaDon = "Hủy Order Bếp";
                                     dmHangHoa.MaBan = maBan;
                                     dmHangHoa.MaQuay = cls_ConfigCashier.nameCashier;
                                     dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                                     //
                                     dmHangHoa.STT = (++j).ToString();
                                     dmHangHoa.MA_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                                     dmHangHoa.TEN_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__TEN_HANGHOA"]).ToString();
                                     dmHangHoa.SOLUONG = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                                     //Kiểm tra hàng hóa có in bếp ko
                                     string sql = " SELECT ISNULL(IS_INBEP,0) AS IS_INBEP FROM HANGHOA WHERE MA_HANGHOA = " + dmHangHoa.MA_HANGHOA;
                                     DataTable dtinbep = clsMain.ReturnDataTable(sql);
                                     if (bool.Parse(dtinbep.Rows[0]["IS_INBEP"].ToString()) == true)
                                        listDM_HangHoa.Add(dmHangHoa);
                                     if (i != 0)
                                     {
                                         gv_divide_Table_2.DeleteRow(i);
                                         i--;
                                     }
                                     DeleteOrder(int.Parse(dmHangHoa.MA_HANGHOA), int.Parse(maBan));
                                 }
                             }
                             gv_divide_Table_2.DeleteRow(0);// Delete rows(0)
                             //----------------
                             string print = GetIdKitchen(maBEP); 
                             if (print == null)
                                 XtraMessageBox.Show("Chưa chọn máy in cho mặt hàng này !", "Thông Báo ", MessageBoxButtons.OK);                            
                             // Printer Order 
                             convert cvrt = new convert();
                             DataTable Orderbill = cvrt.ConvertToDataTable(listDM_HangHoa );
                             if (Orderbill.Rows.Count > 0)
                             {
                                 KP_Report.Frm_Report1 frm = new KP_Report.Frm_Report1();
                                 frm.ReportName = "rpt_OrderKaraoke";
                                 frm.DataSource = Orderbill;
                                 frm.WindowState = FormWindowState.Minimized;
                                 frm.PrinterName = print;
                                 frm.IsPrint = true;
                                 frm.ShowDialog();
                                 frm.Dispose();
                             }
                         }                        
                    }
                    ExitDivideTable();
                    return;
                }
                // if you throw table1 to table 2
                if (gv_divide_Table_1.RowCount <= 0)
                {
                    if (CheckOpenTable(maBanGhep) == false)
                    {
                        string sql = "Update BAN SET ISMO_BAN=1 where MA_BAN =" + int.Parse(maBanGhep) + "";
                        clsMain.ExecuteSQL(sql);
                        string maHD = GetMaHoaDon(maBan);
                        string sqlCTMOBAN = " Update CT_MOBAN_KARAOKE SET MA_BAN =" + int.Parse(maBanGhep) + " where MA_HOADON='" + maHD + "'";
                        clsMain.ExecuteSQL(sqlCTMOBAN);
                        string sqlpm = "Update BAN SET ISMO_BAN=0 where MA_BAN =" + int.Parse(maBan) + "";
                        clsMain.ExecuteSQL(sqlpm);
                        ExitDivideTable();
                        divide_Table1.Rows.Clear();
                        divide_Table2.Rows.Clear();
                        return;
                    }
                    else
                    {
                        CanceTable(maBan);
                        string maHD = GetMaHoaDon(maBanGhep);
                        for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                        {
                            DM_ListDisheTableTwo dmHangHoa = new DM_ListDisheTableTwo();
                            dmHangHoa.__MA_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                            dmHangHoa.__SOLUONG = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                            dmHangHoa.__GIABAN = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__GIABAN"]).ToString());

                            if (maHD != null)
                            {
                                if (ChecBishInTable(maHD, dmHangHoa.__MA_HANGHOA) == false)
                                {
                                    string sql = "SET DATEFORMAT DMY Insert into CT_HOADON_KARAOKE( MA_HOADON, MA_HANGHOA,  SOLUONG, GIABAN)";
                                    sql += " values(" + "'" + maHD + "'," + int.Parse(dmHangHoa.__MA_HANGHOA) + "," + dmHangHoa.__SOLUONG + "," + dmHangHoa.__GIABAN + ")";
                                    clsMain.ExecuteSQL(sql);
                                }
                                else
                                {
                                    string sql = "UPDATE CT_HOADON_KARAOKE SET SOLUONG +=" + dmHangHoa.__SOLUONG + "  WHERE MA_HOADON ='" + maHD + "' and MA_HANGHOA =" + int.Parse(dmHangHoa.__MA_HANGHOA) + "";
                                    clsMain.ExecuteSQL(sql);
                                }
                            }
                            else
                                throw new Exception("Lỗi update hàng hóa !");
                        }
                        ExitDivideTable();
                        divide_Table1.Rows.Clear();
                        divide_Table2.Rows.Clear();
                        return;
                    }
                }
                else
                {
                    // if you divide table1 to table 2
                    if (CheckOpenTable(maBanGhep) == false)
                    {
                        // is Open table 
                        string sql = "Update BAN SET ISMO_BAN=1 where MA_BAN =" + int.Parse(maBanGhep);
                        clsMain.ExecuteSQL(sql);
                        string maHoaDon = GetNewCodeBill();
                        string sqlInserter = "SET DATEFORMAT DMY Insert into HOADON_KARAOKE(MA_HOADON, NGAYTAO, GIOVAO, ISPAYMENT, ISBANDAT, MANHANVIEN)";
                        sqlInserter += " values(" + "'" + maHoaDon + "',CONVERT(datetime, GetDate(), 121),CONVERT(datetime, GetDate(), 121),0,0,'" + KP_UserManagement.clsGlobal.gsMaNVOfUserLogin+ "')";
                        clsMain.ExecuteSQL(sqlInserter);
                        string sqlCTMOBAN = "SET DATEFORMAT DMY Insert into CT_MOBAN_KARAOKE(MA_HOADON, MA_BAN)";
                        sqlCTMOBAN += " values(" + "'" + maHoaDon + "'," + int.Parse(maBanGhep) + ")";
                        clsMain.ExecuteSQL(sqlCTMOBAN);          
                        //  
                        for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                        {
                            DM_HangHoa dmHangHoa = new DM_HangHoa();
                            dmHangHoa.MA_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                            dmHangHoa.SOLUONG = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                            dmHangHoa.GIABAN = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__GIABAN"]).ToString());

                            if (maHoaDon != null)
                            {
                                if (ChecBishInTable(maHoaDon, dmHangHoa.MA_HANGHOA) == false)
                                {
                                    string sqlc = "SET DATEFORMAT DMY Insert into CT_HOADON_KARAOKE( MA_HOADON, MA_HANGHOA,  SOLUONG, GIABAN)";
                                    sqlc += " values(" + "'" + maHoaDon + "'," + int.Parse(dmHangHoa.MA_HANGHOA) + "," + dmHangHoa.SOLUONG + "," + dmHangHoa.GIABAN + ")";
                                    clsMain.ExecuteSQL(sqlc);
                                }
                                else
                                {
                                    string sqlt = "UPDATE CT_HOADON_KARAOKE SET SOLUONG +=" + dmHangHoa.SOLUONG + "  WHERE MA_HOADON ='" + maHoaDon + "' and MA_HANGHOA =" + int.Parse(dmHangHoa.MA_HANGHOA) + "";
                                    clsMain.ExecuteSQL(sqlt);                                   
                                }
                            }
                            else
                                throw new Exception("Tách phòng lỗi !");
                        }                        
                    }
                    else
                    {
                        for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                        {
                            DM_HangHoa dmHangHoa = new DM_HangHoa();
                            dmHangHoa.MA_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                            dmHangHoa.SOLUONG = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                            dmHangHoa.GIABAN = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__GIABAN"]).ToString());
                            string maHDB = GetMaHoaDon(maBanGhep);
                            if (maHDB != null)
                            {
                                if (ChecBishInTable(maHDB, dmHangHoa.MA_HANGHOA) == false)
                                {
                                    string sqlc = "SET DATEFORMAT DMY Insert into CT_HOADON_KARAOKE( MA_HOADON, MA_HANGHOA,  SOLUONG, GIABAN)";
                                    sqlc += " values(" + "'" + maHDB + "'," + int.Parse(dmHangHoa.MA_HANGHOA) + "," + dmHangHoa.SOLUONG + "," + dmHangHoa.GIABAN + ")";
                                    clsMain.ExecuteSQL(sqlc);
                                }
                                else
                                {
                                    string sqlt = "UPDATE CT_HOADON_KARAOKE SET SOLUONG +=" + dmHangHoa.SOLUONG + "  WHERE MA_HOADON ='" + maHDB + "' and MA_HANGHOA =" + int.Parse(dmHangHoa.MA_HANGHOA) + "";
                                    clsMain.ExecuteSQL(sqlt);
                                }
                            }
                            else
                                throw new Exception("Tách phòng lỗi !");
                      }        
                    }
                    //ReUpdate table 1
                    string idBill1 = GetMaHoaDon(maBan);
                    string sqldelte = "Delete CT_HOADON_KARAOKE where MA_HOADON ='" + idBill1 + "'";
                    clsMain.ExecuteSQL(sqldelte);
                    for (int i = 0; i < gv_divide_Table_1.RowCount; i++)
                    {
                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        dmHangHoa.MA_HANGHOA = gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString();
                        dmHangHoa.SOLUONG = decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                        dmHangHoa.GIABAN = decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_GIABAN"]).ToString());

                        if (idBill1 != null)
                        {
                            if (ChecBishInTable(idBill1, dmHangHoa.MA_HANGHOA) == false)
                            {
                                string sqlc = "SET DATEFORMAT DMY Insert into CT_HOADON_KARAOKE( MA_HOADON, MA_HANGHOA,  SOLUONG, GIABAN)";
                                sqlc += " values(" + "'" + idBill1 + "'," + int.Parse(dmHangHoa.MA_HANGHOA) + "," + dmHangHoa.SOLUONG + "," + dmHangHoa.GIABAN + ")";
                                clsMain.ExecuteSQL(sqlc);
                            }
                            else
                            {
                                string sqlt = "UPDATE CT_HOADON_KARAOKE SET SOLUONG +=" + dmHangHoa.SOLUONG + "  WHERE MA_HOADON ='" + idBill1 + "' and MA_HANGHOA =" + int.Parse(dmHangHoa.MA_HANGHOA) + "";
                                clsMain.ExecuteSQL(sqlt);
                            }
                        }
                        else
                            throw new Exception("Tách phòng lỗi !");
                    }
                }
                divide_Table1.Rows.Clear();
                divide_Table2.Rows.Clear();
                ExitDivideTable();
            }
            catch (Exception es)
            {
                XtraMessageBox.Show(es.Message, "Thông Báo", MessageBoxButtons.OK);
            }
        }

        private void btn_ToLeftAll_Click(object sender, EventArgs e)
        {
            try
            {
                if (funtions == 1)
                {
                    while (gv_divide_Table_2.RowCount > 0)
                        Throw_Table2ToTable1();
                    return;
                }
                if (maBanGhep == "")
                {
                    pnShowHang.BringToFront();
                }
                while(gv_divide_Table_2.RowCount>0)
                Throw_Table2ToTable1();
            }
            catch
            { }
        }

        private void gv_divide_Table_2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == __STT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
        }

        private void Throw_Table2ToTable1()
        {
            if (gv_divide_Table_2.RowCount <= 0)
                return;
            bool checkProductOnGv = false;
            //add gridview
            if (divide_Table1.Rows.Count > 0)
            {
                for (int i = 0; i < divide_Table1.Rows.Count; i++)
                {
                    string maHH = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                    if (divide_Table1.Rows[i]["_MA_HANGHOA"].ToString() == maHH)
                    {
                        divide_Table1.Rows[i]["_SOLUONG"] = decimal.Parse(divide_Table1.Rows[i]["_SOLUONG"].ToString()) + 1;
                        checkProductOnGv = true;
                    }
                }
            }
            if (checkProductOnGv == false)
            {
                DataRow dr = divide_Table1.NewRow();
                dr[0] = int.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString());
                dr[1] = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__TEN_HANGHOA"]).ToString();
                dr[2] = 1;
                //gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                dr[3] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__GIABAN"]).ToString());
                dr[4] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__MA_BEP"]).ToString());
                dr[5] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__Thanh_Tien"]).ToString());
                divide_Table1.Rows.Add(dr);
            }
            gr_divide_Table_1.DataSource = divide_Table1;
            gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.RowCount - 1;
            if (decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString()) == 1)
                gv_divide_Table_2.DeleteSelectedRows();
            else
            {
                int sl = int.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                gv_divide_Table_2.SetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"], sl - 1);
            }
        }
        private void btn_Toleft_Click(object sender, EventArgs e)
        {
            try
            {
                if (funtions == 1)
                {
                    Throw_Table2ToTable1();
                    return;
                }
                if (maBanGhep == "")
                {
                    pnShowHang.BringToFront();
                }
                Throw_Table2ToTable1();
            }
            catch
            { }
        }

        private void btn_BrowTable_Click(object sender, EventArgs e)
        {
            pnShowHang.BringToFront();
            foreach (SimpleButton btn in pnKV_Mon.Controls)
            {
                if (btn.Name == maKV.ToString())
                {
                    SetBackgroudOnClick(btn);
                }
            }
            foreach (SimpleButton btnb in pnTable_Mon.Controls)
            {
                SetDefautlBackgroud(btnb);
                if (CheckHaveISBANDAT(int.Parse(maBan)) == true)
                {
                    SetBackgroudOnClick2(btnb);
                }
                if (CheckOpenTable(btnb.Name) == true)
                {
                    SetBackgroudOnClick1(btnb);
                }
            }
        }

        private void btn_DatBan_Click(object sender, EventArgs e)
        {
            if (fastFoot != 0 || funtions == 10)
                return;
            SetBackgroundBtnInPn(pnNghiepVu, pnThuNgan);
            if (funtions == 9)
            {
                // Check Find Dishes
                selectDishes = 0;
                pnCheckTable.BringToFront();
                return;
            }
            else
                funtions = 9;
            //Check Find Tables 
            selectTable = 1;
            dtLuoi.Rows.Clear();
            gr_SelectProduct.RefreshDataSource();
            pnCheckTable.BringToFront();
            txtTenKhacHang.Focus();
            us_keyboard2.SetTextbox(txtDBTenKH);
            //set time 
            for (int i = 0; i <= 23; i++)
                cbHH.Properties.Items.Add(i);
            cbHH.Text = "00";
            //
            //Set mins
            for (int j = 0; j <= 59; j++)
                cbMM.Properties.Items.Add(j);
            cbMM.Text = "00";
            //
            //set secsion 
            for (int t = 1; t <= 24; t++)
                cbSS.Properties.Items.Add(t);
            cbSS.Text = "00";
            txtngay.Text = clsMain.GetServerDate().ToShortDateString();
        }

        private void btn_TamTinh_Click(object sender, EventArgs e)
        {
            if (maBan == "" || funtions != 1 || fastFoot != 0 || funtions == 6)
                return;
            else
            {
                List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                string idBill = GetMaHoaDon(maBan);
                if (idBill == "")
                    return;
                //select list table 
                DataTable dtcheck = clsMain.ReturnDataTable("select MA_BAN from CT_MOBAN_KARAOKE where MA_HOADON='" + idBill + "'");
                if (dtcheck.Rows.Count > 0)
                {
                    for (int i = 0; i < dtcheck.Rows.Count; i++)
                    {
                        string updateSeePayments = " Update BAN set SEE_PAYMENTS = 1 Where MA_BAN = " + dtcheck.Rows[i]["MA_BAN"].ToString() + "";
                        clsMain.ExecuteSQL(updateSeePayments);
                    }
                }
                //
                GetSoDoBan();  
                for(int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    DM_HangHoa dmHangHoa = new DM_HangHoa();
                    dmHangHoa.STT = (i + 1).ToString();
                    dmHangHoa.Ten_HoaDon = "PHIẾU TẠM TÍNH";
                    dmHangHoa.MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                    dmHangHoa.TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                    dmHangHoa.SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                    dmHangHoa.GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                    dmHangHoa.Thanh_Tien = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"]).ToString());
                    //dmHangHoa.Tong_Cong = lbTatol.Text.Substring(0, lbTatol.Text.Length - 2); // - 4 cắt bỏ ký tự VND
                    dmHangHoa.Tong_Cong = lbTatol.Text;
                    dmHangHoa.Giam_Gia = "0";
                    dmHangHoa.Tien_KhachTra = "0";
                    dmHangHoa.Tien_TraKhach = "0";
                    dmHangHoa.Phiphucvu = "0";
                    dmHangHoa.Tiendatcoc = "0";
                    dmHangHoa.Ma_HoaDon = idBill;
                    dmHangHoa.Ma_Quay = cls_ConfigCashier.nameCashier;
                    dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                    dmHangHoa.ngayHD = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                    //
                    dmHangHoa.MaBan = maBan;
                    dmHangHoa.TienGio = lbTiengio.Text;
                    if (dtTiengio.Rows.Count > 0)
                    {
                        dmHangHoa.Giovao = giovao;
                        dmHangHoa.Giora = giora;
                    }
                    else //tránh trường hợp dtTiengio ko có dòng nào sẽ bị lỗi, để thời gian mặc định
                    {
                        dmHangHoa.Giovao = Convert.ToDateTime("01/01/1900 12:00:00 AM");
                        dmHangHoa.Giora = Convert.ToDateTime("01/01/1900 12:00:00 AM");
                    }
                    listDM_HangHoa.Add(dmHangHoa);
                }
                convert cvrt = new convert();
                DataTable bill = cvrt.ConvertToDataTable(listDM_HangHoa);
                KP_Report.Frm_Report1 frm = new KP_Report.Frm_Report1();
                frm.ReportName = "rpt_inhoadonKaraoke";
                frm.DataSource = bill;
                frm.WindowState = FormWindowState.Minimized;
                frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                frm.IsPrint = true;
                frm.ShowDialog();
                frm.Dispose();
            }
        }
        public int fastFoot =0;

        private void btn_ExitDishe_Click(object sender, EventArgs e)
        {
            if (fastFoot != 0 || funtions == 10)
                return;         
            // Delete Disher 
            if (funtions == 1 && maBan != "")
            {
                if (CheckOpenTable(maBan) == false)
                    return;
            }
            else
                return;
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion frm = new Frm_ConfigFuntion();
                frm.ShowDialog();
                check = frm.kt;
                frm.Dispose();
                if (check == false)
                    return;
            }
            pnInforTablecheck.Visible = false;
            //btn_BrowTable.Visible = false;
            pnHdCheckTable.Visible = false;
            pnHuymon.BringToFront();
            int w = (pn_GroupTable.Width / 2) - pnUpDow.Width;
            pn_TableTow.Width = w + 30;
            pn_TableShare.Width = w + 30;
            labelControl12.Location = new Point((pnHdCheckTable.Width / 2) - (labelControl12.Width / 2), labelControl12.Location.Y);
           //Set data for divde table 
            List<DM_ListDisheTableOne> lisHangHoa = new List<DM_ListDisheTableOne>();
            //add gridview
            for (int i = 0; i < gv_SelectProduct.RowCount; i++)
            {
                DM_ListDisheTableOne dmHangHoa = new DM_ListDisheTableOne();
                dmHangHoa._MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                dmHangHoa._TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                dmHangHoa._SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                dmHangHoa._GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                dmHangHoa._MA_BEP = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_BEP"]).ToString());
                dmHangHoa._Thanh_Tien = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"]).ToString());
                dmHangHoa._GHICHU = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"]).ToString();
                lisHangHoa.Add(dmHangHoa);
            }
            convert cvrt = new convert();
            divide_Table1 = cvrt.ConvertToDataTable(lisHangHoa);
            gr_divide_Table_1.DataSource = divide_Table1;
            gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.RowCount - 1;
        }

        private void btn_SelectTable_Click(object sender, EventArgs e)
        {
            if (txtngay.Text == "" || cbHH.Text == "")
                return;            
            DateTime now = DateTime.Parse(DateTime.Now.ToShortDateString()); 
            DateTime x = DateTime.Parse(txtngay.EditValue.ToString().Substring(0,10));
            int outline = DateTime.Compare(now, x);
            if (outline == 1)
            {
               XtraMessageBox.Show(" Ngày đặt phòng không hợp lệ !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
               return;
            }
            else
            {
                if (int.Parse(cbHH.Text) < int.Parse(DateTime.Now.Hour.ToString()) && outline == 0)
                {
                    XtraMessageBox.Show("Chưa chọn thời gian đến !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            funtions = 1;
            if (funtions == 1)
            {
                pnShowHang.BringToFront();
                btn_SoDoBan.Text = "Sơ đồ phòng";
                lbListTable.Text = "Danh sách món:";
                GetKhuVuc();
                GetSoDoBan();
                foreach (SimpleButton btn in pnKV_Mon.Controls)
                {
                    if (btn.Name == maKV.ToString())
                    {
                        SetBackgroudOnClick(btn);
                    }
                }
                selectDishes = 2;
                funtions = 9;
            }
        }

        private void btn_SelectDishes_Click(object sender, EventArgs e)
        {
            funtions = 2;
            pnShowHang.BringToFront();
            btn_SoDoBan.Text = "Mở phòng";
            lbListTable.Text = "Danh sách phòng:";
            GetNhomhang();
            GetListMon();
            foreach (SimpleButton btn in pnKV_Mon.Controls)
            {
                if (btn.Name == maNH.ToString())
                {
                    SetBackgroudOnClick(btn);
                }
            }
            funtions = 9;
            selectDishes = 1;
        }

        private void cbHH_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(cbHH);
        }

        private void cbMM_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(cbMM);
        }

        private void cbSS_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(cbSS);
        }

        private void txtDienThoai_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(txtDienThoai);
        }

        private void txtTenKhachHang_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(txtDBTenKH);
        }

        private void txtAddress_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(txtAddress);
        }

        private void txtPartMoneyPayments_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(txtPartMoneyPayments);
        }

        private void btn_InLaiHoaDon_Click(object sender, EventArgs e)
        {
            bool check;
            btnComboOption.Visible = false;
            pnBtnDeleteGv.Visible = false;
            if (funtions == 9 || funtions == 10 )
                return;
            if (exitReprint == true)
            {
                btnThoatReprint_Click(sender, e);
            }
            else
            {
                string sSQL = "";
                try
                {
                    if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
                    {
                        Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                        fr.ShowDialog();
                        check = fr.kt;
                        fr.Dispose();
                        if (check == false)
                            return;
                    }
                    pnReprintBill.BringToFront();
                    sSQL = "EXEC SP_SELECT_HOADONINLAI";
                    DataTable dts = clsMain.ReturnDataTable(sSQL);
                    gr_billPaymented.DataSource = dts;
                    gr_billPaymented.RefreshDataSource();

                    SetBackgroudOnClick(btn_InLaiHoaDon);
                    IN.Visible = true;
                    HUY.Visible = false;
                    LAYLAI.Visible = false;
                    foreach (SimpleButton btn in pnNghiepVu.Controls)
                    {
                        if (btn.Name != "btn_InLaiHoaDon")
                        {
                            btn.Enabled = false;
                        }
                    }
                    foreach (SimpleButton btn in pnThuNgan.Controls)
                    {
                        if (btn.Name != "btn_InLaiHoaDon")
                        {
                            btn.Enabled = false;
                        }
                    }
                    exitReprint = true;
                    //btnLogOff.Text = "Nghiệp Vụ";
                    //btn_logout.Enabled = false;
                }
                catch
                {
                }
            }
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
            {
                funtions = 1;
                btn_SoDoBan.Text = "Mở phòng";
                lbListTable.Text = "Danh sách phòng:";
                pnShowHang.BringToFront();
                GetKhuVuc();
                GetSoDoBan();
                SetDefaultBackgroundBtnInPn(pnThuNgan);
                SetDefaultBackgroundBtnInPn(pnNghiepVu);
                foreach (SimpleButton btn in pnKV_Mon.Controls)
                {
                    if (btn.Name == maKV.ToString())
                    {
                        SetBackgroudOnClick(btn);
                    }
                }
                foreach (SimpleButton btnb in pnTable_Mon.Controls)
                {
                    if (btnb.Name == maBan.ToString())
                    {
                        SetBackgroudOnClick(btnb);
                    }
                }
                dtLuoi.Rows.Clear();
                gr_SelectProduct.RefreshDataSource();
                txtDBTenKH.Text = "";
                txtDienThoai.Text = "";
                txtAddress.Text = "";
                txtPartMoneyPayments.Text = "";
                flCheckTable.Controls.Clear();
            }
        }
        private void btnOKCheckTable_Click(object sender, EventArgs e)
        {
            int dem = 0;
            foreach (SimpleButton btn in flCheckTable.Controls)
            {
                dem++;
            }
            if (funtions != 9 || txtDBTenKH.Text == "" || dem == 0 || txtngay.Text == "")
            {
                XtraMessageBox.Show("Bạn phải nhập đầy đủ thông tin !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtDienThoai.Text == "" || txtAddress.Text == "")
            {
                XtraMessageBox.Show("Chưa nhập thông tin xác nhận !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            string date = txtngay.EditValue.ToString().Substring(0, 10);
            string time = "" + cbHH.Text + ":" + cbMM.Text + ":" + cbSS.Text + "";
            string ngayDat = "" + date + " " + time;
            string ngay = string.Format("{0:dd/MM/yyyy HH:mm:ss}", clsMain.GetServerDate());
            //Creater id Bill
            if (txtPartMoneyPayments.Text == "")
                txtPartMoneyPayments.Text = "0";
            int n;
            bool isNumeric = int.TryParse(txtPartMoneyPayments.Text.Replace(",", String.Empty), out n);
            if (isNumeric == false)
            {
                XtraMessageBox.Show("Tiền đặt cọc là kiểu số !");
                return;
            }
            string maHoaDon = GetNewCodeBill();
            //update Check Table           
            //Update infor Table checked
            if (maHoaDon != null)
            {
                DataTable KH;
                KH = clsMain.ReturnDataTable("Select MA from NHACUNGCAP where DIENTHOAI ='" + txtDienThoai.Text + "' and LOAI =0");
                if (KH.Rows.Count <= 0)
                {
                    string sqlkh = "SET DATEFORMAT DMY Insert into NHACUNGCAP(TEN, DIACHI, DIENTHOAI) ";
                    sqlkh += " values(" + "'" + txtDBTenKH.Text + "','" + txtAddress.Text + "','" + txtDienThoai.Text + "')";
                    clsMain.ExecuteSQL(sqlkh);
                    KH = clsMain.ReturnDataTable("Select MA from NHACUNGCAP where DIENTHOAI ='" + txtDienThoai.Text + "'  and LOAI =0");
                }
                else
                {
                    string sqlkh = "SET DATEFORMAT DMY UPDATE NHACUNGCAP SET TEN='" + txtDBTenKH.Text + "', DIACHI ='" + txtAddress.Text + "' where MA =" + int.Parse(KH.Rows[0]["MA"].ToString()) + "";
                    clsMain.ExecuteSQL(sqlkh);
                }

                string sqlInserter = "SET DATEFORMAT DMY Insert into HOADON_KARAOKE(MA_HOADON, MANHANVIEN, NGAYDAT, TIENDATCOC, GIOVAO, SODIENTHOAI, MA_KHACHHANG, ISBANDAT) ";
                sqlInserter += " values(" + "'" + maHoaDon + "'," + int.Parse(KP_UserManagement.clsGlobal.gsMaNVOfUserLogin) + ", '" + ngay + "'," + float.Parse(txtPartMoneyPayments.Text.Replace(",", String.Empty)) + ",'" + ngayDat + "', '" + txtDienThoai.Text + "'," + KH.Rows[0]["MA"] + "," + 1 + ")";
                clsMain.ExecuteSQL(sqlInserter);
                string nameTable = "";
                int countTable = 0;
                foreach (SimpleButton btn in flCheckTable.Controls)
                {
                    countTable++;
                    nameTable += clsMain.ReturnDataTable("SELECT TEN_BAN FROM BAN WHERE MA_BAN = " + int.Parse(btn.Name.ToString())).Rows[0]["TEN_BAN"].ToString();
                    string sqlCTMOBAN = "SET DATEFORMAT DMY Insert into CT_MOBAN_KARAOKE(MA_HOADON, MA_BAN)";
                    sqlCTMOBAN += " values(" + "'" + maHoaDon + "'," + int.Parse(btn.Name.ToString()) + ")";
                    clsMain.ExecuteSQL(sqlCTMOBAN);
                    string sqlban = "UPDATE BAN SET ISBANDAT = 1 where MA_BAN = " + int.Parse(btn.Name.ToString());
                    clsMain.ExecuteSQL(sqlban);
                }
                string NameDistrict = "";
                DataTable dtKVV = clsMain.ReturnDataTable("select TEN_KHUVUC from KHUVUC where MA_KHUVUC=" + maKV + "");
                if (dtKVV.Rows.Count > 0)
                    NameDistrict = dtKVV.Rows[0]["TEN_KHUVUC"].ToString();

                List<DM_HangHoa> listHH = new List<DM_HangHoa>();
                int j = 1;
                if (gv_SelectProduct.RowCount > 0)
                {
                    for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                    {
                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        dmHangHoa.Ten_HoaDon = "Phiếu xác nhận đặt phòng";
                        dmHangHoa.STT = (j++).ToString();
                        dmHangHoa.Ma_HoaDon = maHoaDon;
                        dmHangHoa.TenKhachHang = txtDBTenKH.Text;
                        dmHangHoa.DienThoai = txtDienThoai.Text;
                        dmHangHoa.GIOVAO = ngayDat;
                        dmHangHoa.MaBan = nameTable;
                        dmHangHoa.TienDatCoc = decimal.Parse(txtPartMoneyPayments.Text);
                        dmHangHoa.KhuVuc = NameDistrict;
                        dmHangHoa.MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                        dmHangHoa.TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                        dmHangHoa.SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                        dmHangHoa.GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                        dmHangHoa.Thanh_Tien = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"]).ToString());
                        dmHangHoa.Ma_Quay = cls_ConfigCashier.nameCashier;
                        dmHangHoa.TenNV = clsGlobal.gsNameOfUserLogin;
                        dmHangHoa.Ngay = ngay;
                        listHH.Add(dmHangHoa);
                        string sqlcthd = "SET DATEFORMAT DMY Insert into CT_HOADON_KARAOKE( MA_HOADON, MA_HANGHOA,  SOLUONG, GIABAN)";
                        sqlcthd += " values(" + "'" + maHoaDon + "'," + int.Parse(dmHangHoa.MA_HANGHOA) + "," + dmHangHoa.SOLUONG + "," + dmHangHoa.GIABAN + ")";
                        clsMain.ExecuteSQL(sqlcthd);
                    }
                }
                else
                {
                    DM_HangHoa dmHangHoa = new DM_HangHoa();
                    dmHangHoa.Ten_HoaDon = "Phiếu xác nhận đặt phòng";
                    dmHangHoa.Ma_HoaDon = maHoaDon;
                    dmHangHoa.TenKhachHang = txtDBTenKH.Text;
                    dmHangHoa.DienThoai = txtDienThoai.Text;
                    dmHangHoa.GIOVAO = ngayDat;
                    dmHangHoa.MaBan = nameTable;
                    dmHangHoa.TienDatCoc = decimal.Parse(txtPartMoneyPayments.Text);
                    dmHangHoa.KhuVuc = NameDistrict;
                    dmHangHoa.Ma_Quay = cls_ConfigCashier.nameCashier;
                    dmHangHoa.TenNV = clsGlobal.gsNameOfUserLogin;
                    dmHangHoa.Ngay = ngay;
                    listHH.Add(dmHangHoa);
                }
                //
                convert cvrt = new convert();
                DataTable TableCheck = cvrt.ConvertToDataTable(listHH);
                KP_Report.Frm_Report1 frm = new KP_Report.Frm_Report1();
                frm.ReportName = "rpt_BillDatPhongKaraoke";
                frm.DataSource = TableCheck;
                frm.WindowState = FormWindowState.Minimized;
                frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                frm.IsPrint = true;
                frm.ShowDialog();
                frm.Dispose();
                XtraMessageBox.Show("Xác nhận đặt phòng thành công !", "Thông báo", MessageBoxButtons.OK);
                CheckedIsTable();
                //Reload Table
                funtions = 1;
                GetSoDoBan();
                funtions = 9;
            }
            // Dispes funtion 9 Check Table
            if (funtions != 1)
            {
                funtions = 1;
                btn_SoDoBan.Text = "Mở phòng";
                lbListTable.Text = "Danh sách phòng:";
                pnShowHang.BringToFront();
                GetKhuVuc();
                GetSoDoBan();
                SetDefaultBackgroundBtnInPn(pnThuNgan);
                SetDefaultBackgroundBtnInPn(pnNghiepVu);
                foreach (SimpleButton btn in pnKV_Mon.Controls)
                {
                    if (btn.Name == maKV.ToString())
                    {
                        SetBackgroudOnClick(btn);
                    }
                }
                foreach (SimpleButton btnb in pnTable_Mon.Controls)
                {
                    if (btnb.Name == maBan.ToString())
                    {
                        SetBackgroudOnClick(btnb);
                    }
                }
                dtLuoi.Rows.Clear();
                gr_SelectProduct.RefreshDataSource();
                txtDBTenKH.Text = "";
                txtDienThoai.Text = "";
                txtAddress.Text = "";
                txtPartMoneyPayments.Text = "";
                flCheckTable.Controls.Clear();                
            }            
        }
        // check table is tables check when open table
        private bool CheckHaveISBANDAT(int idTable)
        {
            string sqlIS = "Select ISBANDAT from BAN where MA_BAN =" + idTable + " and ISBANDAT = 1";
            DataTable tableIS = clsMain.ReturnDataTable(sqlIS);
            if (tableIS.Rows.Count > 0 )                
                return true;
            return false;
        }
        private void CheckedIsTable()
        {
            try
            {
                string time1 = Convert.ToDateTime(DateTime.Now.ToShortDateString()).ToString("dd/MM/yyyy");
                DateTime now = DateTime.Parse(time1);
                DataTable dttt = clsMain.ReturnDataTable("SET DATEFORMAT DMY select mb.MA_BAN, convert(varchar(25), hd.GIOVAO , 120) as GIOVAO, convert(varchar(10),hd.GIOVAO , 101) as NgayDat from CT_MOBAN_KARAOKE mb inner join HOADON_KARAOKE hd on mb.MA_HOADON=hd.MA_HOADON  where ISBANDAT =1");
                if (dttt.Rows.Count <= 0)
                    return;
                for (int i = 0; i < dttt.Rows.Count; i++)
                {
                    string dd = dttt.Rows[i]["GIOVAO"].ToString();
                    if (dttt.Rows[i]["GIOVAO"].ToString() != "")
                    {
                        string sso = dttt.Rows[i]["NgayDat"].ToString();
                        DateTime x = DateTime.Parse(sso);
                        int outline = DateTime.Compare(now, x);
                        if ((int.Parse(dttt.Rows[i]["GIOVAO"].ToString().Substring(11, 2)) - int.Parse(DateTime.Now.Hour.ToString()) <= 5) && outline == 0)
                        {
                            string sql = "SET DATEFORMAT DMY Update BAN SET ISBANDAT = 1 where MA_BAN =" + int.Parse(dttt.Rows[i]["MA_BAN"].ToString());
                            clsMain.ExecuteSQL(sql);
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void btnComboOption_Click(object sender, EventArgs e)
        {
            try
            {
                string co_mahanghoa = "";
                string co_tenhanghoa = "";
                string co_soluong = "";
                string co_phuthu = "";

                if (bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
                {
                    string maCombo = "";
                    int i = 0;
                    for (i = gv_SelectProduct.FocusedRowHandle - 1; i >= 0; i--)
                    {
                        if (!bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
                        {
                            maCombo = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                            break;
                        }
                    }

                    string sSQL = "";
                    sSQL += "Select MA_HANGHOA_OPTION as MA,B.TEN_HANGHOA as TEN,A.SOLUONG,GIATHEM" + "\n";
                    sSQL += "From COMBO_OPTION A,HANGHOA B" + "\n";
                    sSQL += "Where MA_HANGHOA_OPTION=b.MA_HANGHOA" + "\n";
                    sSQL += "AND a.MA_COMBO=" + clsMain.SQLString(maCombo) + "\n";
                    sSQL += "AND a.MA_HANGHOA=" + clsMain.SQLString(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                    DataTable dt = clsMain.ReturnDataTable(sSQL);
                    if (dt.Rows.Count > 0)
                    {
                        Frm_Combo_Option frm = new Frm_Combo_Option(maCombo, gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                        frm.ShowDialog();
                        co_mahanghoa = frm.s_mahanghoa;
                        co_tenhanghoa = frm.s_tenhanghoa;
                        co_soluong = (int.Parse(frm.s_soluong) * int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, "SOLUONG").ToString())).ToString();
                        co_phuthu = frm.s_phuthu;
                        if (co_mahanghoa != "")
                        {
                            float tt = float.Parse(gv_SelectProduct.GetRowCellValue(i, "GIABAN").ToString()) + float.Parse(co_phuthu);
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "MA_HANGHOA", co_mahanghoa);
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "TEN_HANGHOA", co_tenhanghoa);
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "SOLUONG", int.Parse(co_soluong));
                            gv_SelectProduct.SetRowCellValue(i, "GIABAN", tt);
                            gv_SelectProduct.SetRowCellValue(i, "Thanh_Tien", tt * int.Parse(gv_SelectProduct.GetRowCellValue(i, "SOLUONG").ToString()));
                        }
                    }
                    else if (dt.Rows.Count == 0)
                    {
                        sSQL = "";
                        sSQL += "Select a.MA_HANGHOA as MA,B.TEN_HANGHOA as TEN,A.SOLUONG,GIATHEM" + "\n";
                        sSQL += "From COMBO_OPTION A,HANGHOA B" + "\n";
                        sSQL += "Where a.MA_HANGHOA=b.MA_HANGHOA" + "\n";
                        sSQL += "AND a.MA_COMBO=" + clsMain.SQLString(maCombo) + "\n";
                        sSQL += "AND a.MA_HANGHOA_OPTION=" + clsMain.SQLString(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, "MA_HANGHOA").ToString());
                        dt = clsMain.ReturnDataTable(sSQL);
                        if (dt.Rows.Count > 0)
                        {
                            int sl = int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, "SOLUONG").ToString()) / int.Parse(dt.Rows[0]["SOLUONG"].ToString());
                            float tt = float.Parse(gv_SelectProduct.GetRowCellValue(i, "GIABAN").ToString()) - float.Parse(dt.Rows[0]["GIATHEM"].ToString());
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "MA_HANGHOA", dt.Rows[0]["MA"].ToString());
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "TEN_HANGHOA", dt.Rows[0]["TEN"].ToString());
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "SOLUONG", sl);
                            gv_SelectProduct.SetRowCellValue(i, "GIABAN", tt);
                            gv_SelectProduct.SetRowCellValue(i, "Thanh_Tien", tt * int.Parse(gv_SelectProduct.GetRowCellValue(i, "SOLUONG").ToString()));
                        }
                        else
                        {
                            XtraMessageBox.Show("Không có hàng hóa thay thế", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    else
                    {
                        XtraMessageBox.Show("không có hàng hóa thay thế", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                else
                {
                    XtraMessageBox.Show("Hàng hóa không thuộc combo", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch
            {
            }
        }

        public string idCheckTable = "";
        private void btnRemove_Click(object sender, EventArgs e)
        {
            foreach (SimpleButton btnBan in flCheckTable.Controls)
            {
                if (btnBan.Name == idCheckTable)//ActiveControl.Name
                {
                    flCheckTable.Controls.Remove(btnBan);
                    idCheckTable = "";
                }
            }
        }

        private void txtngay_TextChanged(object sender, EventArgs e)
        {
            flCheckTable.Controls.Clear();
        }

        private void gvBillPaymented_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == SSTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
                lbTongSoRow.Text = gvBillPaymented.RowCount.ToString();
                labelControl45.Text = (gvBillPaymented.FocusedRowHandle + 1).ToString();
            }
        }
     
        public string idBill = "";
        private void gvBillPaymented_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            try
            {
                idBill = gvBillPaymented.GetRowCellValue(gvBillPaymented.FocusedRowHandle, gvBillPaymented.Columns["MA_HOADON"]).ToString(); ;
                //GetTableInABill(idBill);
                GetDishOfTable(idBill);

                if (e.Column == IN)
                {
                    string sSQL = "";
                    if (idBill != "" && exitReprint == true)
                    {
                        sSQL = "";
                        sSQL += "Insert into HOADON_INLAI(MA_HOADON,NHANVIEN_INLAI,GIO_INLAI)" + "\n";
                        sSQL += "Values(";
                        sSQL += clsMain.SQLString(idBill) + ",";
                        sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                        sSQL += "GETDATE())";
                        clsMain.ExecuteSQL(sSQL);

                        sSQL = "";
                        sSQL += "Exec SP_HOADONINLAI @MA_HOADON='" + idBill + "'";
                        DataTable dtReport = clsMain.ReturnDataTable(sSQL);
                        KP_Report.Frm_Report1 frm = new KP_Report.Frm_Report1();
                        frm.ReportName = "rpt_inhoadonKaraoke";
                        frm.DataSource = dtReport;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                        frm.IsPrint = true;
                        frm.ShowDialog();
                        frm.Dispose();
                        dtLuoi.Rows.Clear();
                        lbTiengio.Text = lbPhuThu.Text = lbGiamGia.Text = lbTatol.Text = "0";
                        return;
                    }
                }
                else if (e.Column == LAYLAI)
                {

                    string sSQL;
                    if (idBill != "" && exitReprint == true)
                    {
                        //Kiểm tra bàn có đang trống
                        DataTable dtTables = clsMain.ReturnDataTable("Select MA_BAN From CT_MOBAN_KARAOKE where MA_HOADON =" + clsMain.SQLString(idBill));
                        if (dtTables.Rows.Count > 0)
                        {
                            DataTable dtCheckban;
                            for (int i = 0; i < dtTables.Rows.Count; i++)
                            {
                                dtCheckban = clsMain.ReturnDataTable("Select ISMO_BAN From BAN Where MA_BAN =" + clsMain.SQLString(dtTables.Rows[i]["MA_BAN"].ToString()));
                                if (dtCheckban.Rows.Count > 0)
                                {
                                    if (bool.Parse(dtCheckban.Rows[0]["ISMO_BAN"].ToString()))
                                    {
                                        XtraMessageBox.Show("Bàn đang mở không thể lấy lại hóa đơn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        return;
                                    }
                                }
                            }
                        }

                        //nhập lý do
                        Frm_GhiChu frm = new Frm_GhiChu(GhiChu);
                        frm.ShowDialog();
                        GhiChu = frm.sGhiChu;
                        frm.Dispose();
                        if (GhiChu.Trim() == "")
                        {
                            return;
                        }
                        sSQL = "";
                        sSQL += "EXEC SP_InsertDSLayLaiHD_Karaoke " + clsMain.SQLString(idBill) + "," + clsMain.SQLString(dtTables.Rows[0]["MA_BAN"].ToString()) + "," + clsMain.SQLString(clsGlobal.gsUserID) + "," + clsMain.SQLStringUnicode(GhiChu) + "";
                        clsMain.ExecuteSQL(sSQL);

                        //hóa đơn
                        string sqlInserter = "SET DATEFORMAT DMY Update HOADON_KARAOKE set ISPAYMENT=0, GIAMGIA=" + 0 + ", TIEN_TRAKHACH =" + 0 + ", TIEN_KHACHTRA =" + 0 + ", TONGTIEN=" + 0 + "  where MA_HOADON='" + idBill + "'";
                        clsMain.ExecuteSQL(sqlInserter);
                        //Bàn
                        dtTables = clsMain.ReturnDataTable("Select MA_BAN From CT_MOBAN_KARAOKE where MA_HOADON ='" + idBill + "'");
                        if (dtTables.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtTables.Rows.Count; i++)
                            {
                                string sql = "Update BAN SET ISMO_BAN=1 where MA_BAN =" + int.Parse(dtTables.Rows[i]["MA_BAN"].ToString());
                                clsMain.ExecuteSQL(sql);
                            }
                        }
                        //Tiền giờ
                        //clsMain.ExecuteSQL("DELETE FROM CT_TIENGIOKARAOKE WHERE MA_HOADON ='" + idBill + "'");
                        //Kho
                        string sqlTruTon = string.Format("Exec SP_TRUKHO_DINHLUONG @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 0);
                        if (clsMain.ExecuteSQL(sqlTruTon))
                        {

                        }
                        if (clsMain.ExecuteSQL(string.Format("Exec SP_TRUKHO_HOADON @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 0)))
                        {

                        }
                        // KHTT -TTNB
                        sSQL = "Select ISNULL(MA_KHACHHANG,'') AS MA_KHACHHANG,TONGTIEN,GIAMGIA From HOADON Where MA_HOADON =" + clsMain.SQLString(idBill);
                        DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                        if (dt1.Rows.Count > 0)
                        {
                            if (dt1.Rows[0]["MA_KHACHHANG"].ToString() != "")
                            {
                                DataTable dtKH = cls_KHTT.ReturnCauHinhKHTT();
                                if (dtKH.Rows[0]["GIATRI"].ToString() == "1")
                                {
                                    int tem = 100 - int.Parse(dt1.Rows[0]["GIAMGIA"].ToString());
                                    decimal tongtien = decimal.Parse(dt1.Rows[0]["TONGTIEN"].ToString()) / tem * 100;
                                    cls_KHTT.TruDiemKhachHangThanThiet(dt1.Rows[0]["MA_KHACHHANG"].ToString(), tongtien);
                                    cls_KHTT.TruTien_TTTNB(dt1.Rows[0]["MA_KHACHHANG"].ToString(), tongtien, "Lấy lại hóa đơn");
                                }
                                else
                                {
                                    cls_KHTT.TruDiemKhachHangThanThiet(dt1.Rows[0]["MA_KHACHHANG"].ToString(), decimal.Parse(dt1.Rows[0]["TONGTIEN"].ToString()));
                                    cls_KHTT.TruTien_TTTNB(dt1.Rows[0]["MA_KHACHHANG"].ToString(), decimal.Parse(dt1.Rows[0]["TONGTIEN"].ToString()), "Lấy lại hóa đơn");
                                }
                            }
                        }
                        //Công nợ
                        sSQL = "EXEC SP_CN_HUYHOADON '" + idBill + "'";
                        clsMain.ExecuteSQL(sSQL);

                        SetDefautlBackgroud(btn_LayLaiHoaDon);
                        ExitDivideTable();
                        //Finish
                        dtLuoi.Rows.Clear();
                        lbTiengio.Text = lbPhuThu.Text = lbGiamGia.Text = lbTatol.Text = "0";
                        XtraMessageBox.Show("Đã lấy lại hóa đơn thành công !");
                        pnReprintBill.SendToBack();
                        while (gvBillPaymented.RowCount > 0)
                            gvBillPaymented.DeleteRow(0);
                        while (gv_SelectProduct.RowCount > 0)
                            gv_SelectProduct.DeleteRow(0);
                        SetDefautlBackgroud(btn_LayLaiHoaDon);
                        foreach (SimpleButton btn in pnNghiepVu.Controls)
                        {
                            btn.Enabled = true;
                        }

                        exitReprint = false;
                        idBill = "";
                        GetKhuVuc();
                        GetSoDoBan();
                        return;
                    }

                }
            }
            catch { 
            }
        }

        private void btn_upbill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = gvBillPaymented.FocusedRowHandle - 1;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
                if (gvBillPaymented.FocusedRowHandle == 0)
                {
                    btn_up.Enabled = false;
                    btn_UpFirst.Enabled = false;
                }
            }
            catch
            {
            }            
        }

        private void btn_UpTopBill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = 0;
                btn_UpFirst.Enabled = false;
                btn_up.Enabled = false;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnDowBill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = gvBillPaymented.FocusedRowHandle + 1;
                btn_up.Enabled = true;
                btn_UpFirst.Enabled = true;

                if (gvBillPaymented.FocusedRowHandle == gvBillPaymented.RowCount - 1)
                {
                    btn_Dow.Enabled = false;
                    btn_DowLast.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnDowLastBill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = gvBillPaymented.RowCount - 1;
                btn_UpFirst.Enabled = true;
                btn_up.Enabled = true;
                btn_Dow.Enabled = false;
                btn_DowLast.Enabled = false;
            }
            catch
            {
            }
        }
        public bool exitReprint = false;
        private void btnThoatReprint_Click(object sender, EventArgs e)
        {
            pnReprintBill.SendToBack();
            while (gvBillPaymented.RowCount > 0)
                 gvBillPaymented.DeleteRow(0);
            while (gv_SelectProduct.RowCount > 0)
                gv_SelectProduct.DeleteRow(0);
            foreach (SimpleButton btn in pnNghiepVu.Controls)
            {
                btn.Enabled = true;
            }
            foreach (SimpleButton btn in pnThuNgan.Controls)
            {
                btn.Enabled = true;
            }
            exitReprint = false;
            idBill = "";
            lbTiengio.Text = lbPhuThu.Text = lbGiamGia.Text = lbTatol.Text = "0";
            SetDefautlBackgroud(btn_InLaiHoaDon);
            SetDefautlBackgroud(btn_LayLaiHoaDon);
        }

        public int inPutMoney = 0;
        
        private void btn_LayLaiHoaDon_Click(object sender, EventArgs e)
        {
            if (fastFoot != 0 || funtions == 10)
                return;
            if (exitReprint == true)
            {
                btnThoatReprint_Click(sender, e);
            }
            else
            {
                if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
                {
                    bool check;
                    Frm_ConfigFuntion frm = new Frm_ConfigFuntion();
                    frm.ShowDialog();
                    check = frm.kt;
                    frm.Dispose();
                    if (check == false)
                        return;
                }
                try
                {
                    pnReprintBill.BringToFront();
                    string sql = "EXEC SP_SELECT_HOADONINLAI";
                    DataTable dts = clsMain.ReturnDataTable(sql);
                    gr_billPaymented.DataSource = dts;
                    gr_billPaymented.RefreshDataSource();

                    SetBackgroudOnClick(btn_LayLaiHoaDon);
                    LAYLAI.Visible = true;
                    HUY.Visible = false;
                    IN.Visible = false;
                    foreach (SimpleButton btn in pnNghiepVu.Controls)
                    {
                        if (btn.Name != "btn_LayLaiHoaDon")
                        {
                            btn.Enabled = false;
                        }
                    }
                    exitReprint = true;
                }
                catch
                {
                }
            }
        }

        private void txtPartMoneyPayments_KeyUp(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPartMoneyPayments.Text))
            {
                System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
                double valueBefore = double.Parse(txtPartMoneyPayments.Text, System.Globalization.NumberStyles.AllowThousands);
                txtPartMoneyPayments.Text = String.Format(culture, "{0:N0}", valueBefore);
                txtPartMoneyPayments.Select(txtPartMoneyPayments.Text.Length, 0);
            }
        }

        private void DeleteOrder(int maHH, int maBan)
        {
            string sql = "Delete From DISPLAY_ORDER where MA_HANGHOA=" + maHH + " and MA_BAN="+maBan+"";
            clsMain.ReturnDataTable(sql);
        }

        private void simpleButton35_Click(object sender, EventArgs e)
        {
            KP_Terminal.clsKP_Terminal.OpenCashDrawer(false);
        }

        private void Guiyeucauthem()
        {
            for (int i = 0; i < gv_SelectProduct.RowCount; i++)
            {
                int maHH = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                if (maHH == int.Parse(maMon))
                    gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"], txtYeuCauThem.Text);
            }
            txtYeuCauThem.Text = "";
            // Return List Dishes 
            funtions = 2;
            btn_SoDoBan.Text = "Sơ đồ phòng";
            lbListTable.Text = "Danh sách món:";
            pnShowHang.BringToFront();
            GetNhomhang();
            GetListMon();
            SetDefaultBackgroundBtnInPn(pnThuNgan);
            SetDefaultBackgroundBtnInPn(pnNghiepVu);
            foreach (SimpleButton btn in pnKV_Mon.Controls)
            {
                if (btn.Name == maNH.ToString())
                {
                    SetBackgroudOnClick(btn);
                }
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            CheckedIsTable();
        }

        int flag = 1;//1: tiền giờ, 2: sơ đồ phòng
        private void lb_clicktiengio_Click(object sender, EventArgs e)
        {
            if (flag == 1)
            {
                flag = 2;
                gc_time.DataSource = dtTiengio;
                pnTime.BringToFront();
            }
            else if (flag == 2)
            {
                flag = 1;
                pnShowHang.BringToFront();
            }
        }

        private void btnGiaBan2_Click(object sender, EventArgs e)
        {
            if (funtions != 1 || gv_SelectProduct.RowCount <= 0 || maBan == "")
                return;

            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }

            string smaMon = gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
            string maHD = GetMaHoaDon(maBan);

            //Kiểm tra nếu đang dùng bản giá theo khu hay cửa hàng thì return
            string sSQL = "select GIATRI from CAUHINH where TEN='APDUNGBANGGIATHEOKHUVUC'";
            DataTable dtcheck = clsMain.ReturnDataTable(sSQL);
            if (dtcheck.Rows.Count > 0)
            {
                if (dtcheck.Rows[0]["GIATRI"].ToString() != "0")
                {
                    MessageBox.Show("Đang dùng chương trình tính giá theo khu hay cửa hàng. Không áp dụng được giá bán 2", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }

            //Kiểm tra hàng hóa là thời giá hay ko,nếu đúng thì return
            sSQL = "Select ISNULL(SUAGIA,0) AS SUAGIA From HANGHOA Where MA_HANGHOA = " + clsMain.SQLString(smaMon);
            dtcheck = clsMain.ReturnDataTable(sSQL);
            if (dtcheck.Rows.Count > 0)
            {
                if (bool.Parse(dtcheck.Rows[0]["SUAGIA"].ToString()))
                {
                    MessageBox.Show("Đây là hàng hóa theo thời giá", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }

            //lây giá bán 1,2
            sSQL = "select  ISNULL(GIABAN1,0) as GIABAN1,ISNULL(GIABAN2,0) as GIABAN2 from HANGHOA where MA_HANGHOA = " + clsMain.SQLString(smaMon);
            DataTable dtgiaban = clsMain.ReturnDataTable(sSQL);
            if (dtgiaban.Rows.Count > 0)
            {
                if (gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["GIABAN"]).ToString() == dtgiaban.Rows[0]["GIABAN1"].ToString())
                {
                    //gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"], dtgiaban.Rows[0]["GIABAN2"].ToString());
                    sSQL = "";
                    sSQL += "UPDATE CT_HOADON_KARAOKE SET " + "\n";
                    sSQL += "GIABAN =" + clsMain.SQLString(dtgiaban.Rows[0]["GIABAN2"].ToString()) + "\n";
                    sSQL += "WHERE MA_HOADON =" + clsMain.SQLString(maHD) + "\n";
                    sSQL += "AND Isnull(Is_Combo,0)=" + clsMain.SQLBit(false) + "\n";
                    sSQL += "AND MA_HANGHOA =" + clsMain.SQLString(smaMon) + "\n";
                    sSQL += "AND GIABAN =" + clsMain.SQLString(gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["GIABAN"]).ToString()) + "\n";
                    clsMain.ExecuteSQL(sSQL);
                }
                else
                {
                    //gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"], dtgiaban.Rows[0]["GIABAN1"].ToString());
                    sSQL = "";
                    sSQL += "UPDATE CT_HOADON_KARAOKE SET " + "\n";
                    sSQL += "GIABAN =" + clsMain.SQLString(dtgiaban.Rows[0]["GIABAN1"].ToString()) + "\n";
                    sSQL += "WHERE MA_HOADON =" + clsMain.SQLString(maHD) + "\n";
                    sSQL += "AND Isnull(Is_Combo,0)=" + clsMain.SQLBit(false) + "\n";
                    sSQL += "AND MA_HANGHOA =" + clsMain.SQLString(smaMon) + "\n";
                    sSQL += "AND GIABAN =" + clsMain.SQLString(gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["GIABAN"]).ToString()) + "\n";
                    clsMain.ExecuteSQL(sSQL);
                }
                ExitDivideTable();
            }
        }

        private void txtYeuCauThem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    int maHH = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                    if (maHH == int.Parse(maMon))
                    {
                        gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"], txtYeuCauThem.Text);
                        break;
                    }
                }
                txtYeuCauThem.Text = "";
                pnShowHang.BringToFront();
                SetDefaultBackgroundBtnInPn(pnThuNgan);
                SetDefaultBackgroundBtnInPn(pnNghiepVu);
            }
        }
    }
}