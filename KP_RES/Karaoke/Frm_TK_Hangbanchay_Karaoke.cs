﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraCharts;
using System.Globalization;

namespace KP_RES 
{
    public partial class Frm_TK_Hangbanchay_Karaoke : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt;
        public Frm_TK_Hangbanchay_Karaoke()
        {
            InitializeComponent();
            dtpTungay.EditValue = clsGlobal.gdServerDate;
            dtpDenngay.EditValue = clsGlobal.gdServerDate;
        }

        private void Frm_TK_Hangbanchay_Karaoke_Load(object sender, EventArgs e)
        {
            Loadcheckbox();
        }
        private void Loadcheckbox()
        {
            dt = clsMain.ReturnDataTable("SELECT MA_NHOMHANG AS MA,TEN_NHOMHANG AS TEN FROM NHOMHANG WHERE SUDUNG='True' AND THUCDON='True'");
            dt.Rows.Add("0", "Tất cả");

            cboNhom.Properties.DataSource = dt;
            cboNhom.EditValue = int.Parse(dt.Rows[0]["MA"].ToString());    
        }
        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            chart.Series.Clear();
            string sql = string.Empty;
            if (cboNhom.Text == "Tất cả")
            {
                sql += "SELECT TOP 10 COUNT(CT.SOLUONG) AS SL,TEN_HANGHOA \n";
                sql += "FROM CT_HOADON_KARAOKE CT,HANGHOA H,HOADON_KARAOKE HD,NHOMHANG NH \n";
                sql += "WHERE CT.MA_HANGHOA=H.MA_HANGHOA AND HD.MA_HOADON=CT.MA_HOADON AND H.MA_NHOMHANG=NH.MA_NHOMHANG \n";
                sql += "AND ISPAYMENT=1 AND NGAYTAO BETWEEN '" + string.Format("{0:yyyyMMdd}", dtpTungay.EditValue) + "' \n";
                sql += "AND '" + string.Format("{0:yyyyMMdd}", dtpDenngay.EditValue) + " " + DateTime.Now.ToString("HH:mm:ss") + "' \n";
                sql += "GROUP BY TEN_HANGHOA  ORDER BY SL DESC";
            }
            else
            {
                sql += "SELECT TOP 10 COUNT(CT.SOLUONG) AS SL,TEN_HANGHOA \n";
                sql += "FROM CT_HOADON_KARAOKE CT,HANGHOA H,HOADON_KARAOKE HD,NHOMHANG NH \n";
                sql += "WHERE CT.MA_HANGHOA=H.MA_HANGHOA AND HD.MA_HOADON=CT.MA_HOADON AND H.MA_NHOMHANG=NH.MA_NHOMHANG \n";
                sql += "AND NH.MA_NHOMHANG='" + cboNhom.EditValue + "' AND ISPAYMENT=1 \n";
                sql += "AND NGAYTAO BETWEEN '" + string.Format("{0:yyyyMMdd}", dtpTungay.EditValue) + "' \n";
                sql += "AND '" + string.Format("{0:yyyyMMdd}", dtpDenngay.EditValue) + " " + DateTime.Now.ToString("HH:mm:ss") + "' \n";
                sql += "GROUP BY TEN_HANGHOA  ORDER BY SL DESC";
            }
            DataTable dt1 = clsMain.ReturnDataTable(sql);

            
            chart.DataSource = dt1;
            Series series = new Series("Series1", ViewType.Bar);
            chart.Series.Add(series);
            series.LegendText = "Số lượng";
            series.ArgumentDataMember = "TEN_HANGHOA";
            series.ValueDataMembers.AddRange("SL");
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
            }
            this.Refresh();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            try
            {
                ChartControl chart = (ChartControl)panelControl5.Controls[0];
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                chart.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                chart.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private static byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            DataColumn dc2 = new DataColumn();
            dc2.ColumnName = "TEMP";
            dc2.DefaultValue = "Temp";
            dt.Columns.Add(dc2);
            dt.Rows.Add("TEMP");

            Bitmap bmp = new Bitmap(panelControl5.ClientRectangle.Width, panelControl5.ClientRectangle.Height);
            panelControl5.DrawToBitmap(bmp, panelControl5.ClientRectangle);

            DataColumn colIMAGE = new DataColumn("IMAGE"); //Create the column.
            colIMAGE.DataType = System.Type.GetType("System.Byte[]"); //Type byte[] to store image bytes.
            colIMAGE.AllowDBNull = true;
            colIMAGE.Caption = "IMAGE";
            colIMAGE.DefaultValue = ImageToByteArray(bmp);
            dt.Columns.Add(colIMAGE);

            DataColumn dc1 = new DataColumn();
            dc1.ColumnName = "NGAY";
            dc1.DefaultValue = "(Từ " + string.Format("{0: dd/MM/yyyy}", dtpTungay.EditValue) + " đến " + string.Format("{0: dd/MM/yyyy}", dtpDenngay.EditValue) + ")";
            dt.Columns.Add(dc1);

            Frm_BCInPhieu frm = new Frm_BCInPhieu();
            frm.WindowState = FormWindowState.Maximized;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.dtSource = dt;
            frm.Mode = 59;
            frm.ShowDialog();
            frm.Dispose();
        }    


    }
}