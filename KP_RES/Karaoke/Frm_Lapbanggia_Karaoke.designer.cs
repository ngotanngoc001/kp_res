﻿namespace KP_RES 
{
    partial class Frm_Lapbanggia_Karaoke 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Lapbanggia_Karaoke));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.pnTop = new DevExpress.XtraEditors.PanelControl();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.chkSUDUNG = new DevExpress.XtraEditors.CheckEdit();
            this.lblGHICHU = new DevExpress.XtraEditors.LabelControl();
            this.txtGHICHU = new DevExpress.XtraEditors.TextEdit();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.txtTEN = new DevExpress.XtraEditors.TextEdit();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.fpnlBanggia = new System.Windows.Forms.FlowLayoutPanel();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.THOIGIAN1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.time_thoigian1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.GIA1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtGIABAN = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.THOIGIAN2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.time_thoigian2 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.GIA2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THOIGIAN3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.time_thoigian3 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.GIA3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THOIGIAN4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.time_thoigian4 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.lookKHUVUC = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.lookHANGHOA = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnTop)).BeginInit();
            this.pnTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHU.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.time_thoigian1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.time_thoigian2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.time_thoigian3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.time_thoigian4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookKHUVUC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookHANGHOA)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(981, 109);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 581);
            this.panelControl2.TabIndex = 2;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 82);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 417);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 97);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 257);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 337);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 499);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // pnTop
            // 
            this.pnTop.Controls.Add(this.btnLuu);
            this.pnTop.Controls.Add(this.btnSua);
            this.pnTop.Controls.Add(this.btnXoa);
            this.pnTop.Controls.Add(this.btnThem);
            this.pnTop.Controls.Add(this.chkSUDUNG);
            this.pnTop.Controls.Add(this.lblGHICHU);
            this.pnTop.Controls.Add(this.txtGHICHU);
            this.pnTop.Controls.Add(this.lblTEN);
            this.pnTop.Controls.Add(this.txtTEN);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(1020, 109);
            this.pnTop.TabIndex = 3;
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = ((System.Drawing.Image)(resources.GetObject("btnLuu.Image")));
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(40, 68);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 5;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnSua
            // 
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Image = ((System.Drawing.Image)(resources.GetObject("btnSua.Image")));
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(216, 68);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(80, 35);
            this.btnSua.TabIndex = 7;
            this.btnSua.Text = "&Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(304, 68);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(80, 35);
            this.btnXoa.TabIndex = 8;
            this.btnXoa.Text = "&Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = ((System.Drawing.Image)(resources.GetObject("btnThem.Image")));
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(128, 68);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 35);
            this.btnThem.TabIndex = 6;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // chkSUDUNG
            // 
            this.chkSUDUNG.EnterMoveNextControl = true;
            this.chkSUDUNG.Location = new System.Drawing.Point(38, 37);
            this.chkSUDUNG.Name = "chkSUDUNG";
            this.chkSUDUNG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNG.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNG.Properties.Caption = "Sử dụng";
            this.chkSUDUNG.Size = new System.Drawing.Size(92, 24);
            this.chkSUDUNG.TabIndex = 4;
            // 
            // lblGHICHU
            // 
            this.lblGHICHU.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGHICHU.Location = new System.Drawing.Point(295, 8);
            this.lblGHICHU.Margin = new System.Windows.Forms.Padding(4);
            this.lblGHICHU.Name = "lblGHICHU";
            this.lblGHICHU.Size = new System.Drawing.Size(54, 19);
            this.lblGHICHU.TabIndex = 2;
            this.lblGHICHU.Text = "Ghi chú";
            // 
            // txtGHICHU
            // 
            this.txtGHICHU.EnterMoveNextControl = true;
            this.txtGHICHU.Location = new System.Drawing.Point(356, 5);
            this.txtGHICHU.Name = "txtGHICHU";
            this.txtGHICHU.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHU.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHU.Size = new System.Drawing.Size(248, 26);
            this.txtGHICHU.TabIndex = 3;
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(6, 8);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(27, 19);
            this.lblTEN.TabIndex = 0;
            this.lblTEN.Text = "Tên";
            // 
            // txtTEN
            // 
            this.txtTEN.EnterMoveNextControl = true;
            this.txtTEN.Location = new System.Drawing.Point(40, 5);
            this.txtTEN.Name = "txtTEN";
            this.txtTEN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTEN.Properties.Appearance.Options.UseFont = true;
            this.txtTEN.Size = new System.Drawing.Size(248, 26);
            this.txtTEN.TabIndex = 1;
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.fpnlBanggia);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl5.Location = new System.Drawing.Point(0, 109);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(981, 90);
            this.panelControl5.TabIndex = 4;
            // 
            // fpnlBanggia
            // 
            this.fpnlBanggia.AutoScroll = true;
            this.fpnlBanggia.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpnlBanggia.Font = new System.Drawing.Font("Tahoma", 12F);
            this.fpnlBanggia.Location = new System.Drawing.Point(2, 2);
            this.fpnlBanggia.Name = "fpnlBanggia";
            this.fpnlBanggia.Size = new System.Drawing.Size(977, 86);
            this.fpnlBanggia.TabIndex = 19;
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 199);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.lookKHUVUC,
            this.lookHANGHOA,
            this.txtGIABAN,
            this.time_thoigian1,
            this.time_thoigian2,
            this.time_thoigian3,
            this.time_thoigian4});
            this.gridControl2.Size = new System.Drawing.Size(981, 491);
            this.gridControl2.TabIndex = 5;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.THOIGIAN1,
            this.GIA1,
            this.THOIGIAN2,
            this.GIA2,
            this.THOIGIAN3,
            this.GIA3,
            this.THOIGIAN4});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            // 
            // THOIGIAN1
            // 
            this.THOIGIAN1.AppearanceCell.Options.UseTextOptions = true;
            this.THOIGIAN1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THOIGIAN1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.THOIGIAN1.AppearanceHeader.Options.UseFont = true;
            this.THOIGIAN1.AppearanceHeader.Options.UseTextOptions = true;
            this.THOIGIAN1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THOIGIAN1.Caption = "Thời gian 1";
            this.THOIGIAN1.ColumnEdit = this.time_thoigian1;
            this.THOIGIAN1.DisplayFormat.FormatString = "HH:mm:ss";
            this.THOIGIAN1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.THOIGIAN1.FieldName = "THOIGIAN1";
            this.THOIGIAN1.Name = "THOIGIAN1";
            this.THOIGIAN1.OptionsColumn.FixedWidth = true;
            this.THOIGIAN1.Visible = true;
            this.THOIGIAN1.VisibleIndex = 0;
            this.THOIGIAN1.Width = 120;
            // 
            // time_thoigian1
            // 
            this.time_thoigian1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.time_thoigian1.Appearance.Options.UseFont = true;
            this.time_thoigian1.AutoHeight = false;
            this.time_thoigian1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.time_thoigian1.DisplayFormat.FormatString = "HH:mm:ss";
            this.time_thoigian1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.time_thoigian1.EditFormat.FormatString = "HH:mm:ss";
            this.time_thoigian1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.time_thoigian1.Mask.EditMask = "HH:mm:ss";
            this.time_thoigian1.Name = "time_thoigian1";
            this.time_thoigian1.EditValueChanged += new System.EventHandler(this.time_thoigian1_EditValueChanged);
            // 
            // GIA1
            // 
            this.GIA1.AppearanceCell.Options.UseTextOptions = true;
            this.GIA1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIA1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GIA1.AppearanceHeader.Options.UseFont = true;
            this.GIA1.AppearanceHeader.Options.UseTextOptions = true;
            this.GIA1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIA1.Caption = "Giá 1";
            this.GIA1.ColumnEdit = this.txtGIABAN;
            this.GIA1.DisplayFormat.FormatString = "N0";
            this.GIA1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GIA1.FieldName = "GIA1";
            this.GIA1.Name = "GIA1";
            this.GIA1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.GIA1.OptionsColumn.FixedWidth = true;
            this.GIA1.Visible = true;
            this.GIA1.VisibleIndex = 1;
            this.GIA1.Width = 120;
            // 
            // txtGIABAN
            // 
            this.txtGIABAN.AutoHeight = false;
            this.txtGIABAN.DisplayFormat.FormatString = "N0";
            this.txtGIABAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN.EditFormat.FormatString = "N0";
            this.txtGIABAN.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN.Mask.EditMask = "N0";
            this.txtGIABAN.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGIABAN.Name = "txtGIABAN";
            // 
            // THOIGIAN2
            // 
            this.THOIGIAN2.AppearanceCell.Options.UseTextOptions = true;
            this.THOIGIAN2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THOIGIAN2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THOIGIAN2.AppearanceHeader.Options.UseFont = true;
            this.THOIGIAN2.AppearanceHeader.Options.UseTextOptions = true;
            this.THOIGIAN2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THOIGIAN2.Caption = "Thời gian 2";
            this.THOIGIAN2.ColumnEdit = this.time_thoigian2;
            this.THOIGIAN2.DisplayFormat.FormatString = "HH:mm:ss";
            this.THOIGIAN2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.THOIGIAN2.FieldName = "THOIGIAN2";
            this.THOIGIAN2.Name = "THOIGIAN2";
            this.THOIGIAN2.OptionsColumn.FixedWidth = true;
            this.THOIGIAN2.Visible = true;
            this.THOIGIAN2.VisibleIndex = 2;
            this.THOIGIAN2.Width = 120;
            // 
            // time_thoigian2
            // 
            this.time_thoigian2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.time_thoigian2.Appearance.Options.UseFont = true;
            this.time_thoigian2.AutoHeight = false;
            this.time_thoigian2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.time_thoigian2.DisplayFormat.FormatString = "HH:mm:ss";
            this.time_thoigian2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.time_thoigian2.EditFormat.FormatString = "HH:mm:ss";
            this.time_thoigian2.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.time_thoigian2.Mask.EditMask = "HH:mm:ss";
            this.time_thoigian2.Name = "time_thoigian2";
            // 
            // GIA2
            // 
            this.GIA2.AppearanceCell.Options.UseTextOptions = true;
            this.GIA2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIA2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIA2.AppearanceHeader.Options.UseFont = true;
            this.GIA2.AppearanceHeader.Options.UseTextOptions = true;
            this.GIA2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIA2.Caption = "Giá 2";
            this.GIA2.ColumnEdit = this.txtGIABAN;
            this.GIA2.DisplayFormat.FormatString = "N0";
            this.GIA2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GIA2.FieldName = "GIA2";
            this.GIA2.Name = "GIA2";
            this.GIA2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.GIA2.OptionsColumn.FixedWidth = true;
            this.GIA2.Visible = true;
            this.GIA2.VisibleIndex = 3;
            this.GIA2.Width = 120;
            // 
            // THOIGIAN3
            // 
            this.THOIGIAN3.AppearanceCell.Options.UseTextOptions = true;
            this.THOIGIAN3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THOIGIAN3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THOIGIAN3.AppearanceHeader.Options.UseFont = true;
            this.THOIGIAN3.AppearanceHeader.Options.UseTextOptions = true;
            this.THOIGIAN3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THOIGIAN3.Caption = "Thời gian 3";
            this.THOIGIAN3.ColumnEdit = this.time_thoigian3;
            this.THOIGIAN3.DisplayFormat.FormatString = "HH:mm:ss";
            this.THOIGIAN3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.THOIGIAN3.FieldName = "THOIGIAN3";
            this.THOIGIAN3.Name = "THOIGIAN3";
            this.THOIGIAN3.OptionsColumn.FixedWidth = true;
            this.THOIGIAN3.Visible = true;
            this.THOIGIAN3.VisibleIndex = 4;
            this.THOIGIAN3.Width = 120;
            // 
            // time_thoigian3
            // 
            this.time_thoigian3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.time_thoigian3.Appearance.Options.UseFont = true;
            this.time_thoigian3.AutoHeight = false;
            this.time_thoigian3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.time_thoigian3.DisplayFormat.FormatString = "HH:mm:ss";
            this.time_thoigian3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.time_thoigian3.EditFormat.FormatString = "HH:mm:ss";
            this.time_thoigian3.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.time_thoigian3.Mask.EditMask = "HH:mm:ss";
            this.time_thoigian3.Name = "time_thoigian3";
            // 
            // GIA3
            // 
            this.GIA3.AppearanceCell.Options.UseTextOptions = true;
            this.GIA3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIA3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIA3.AppearanceHeader.Options.UseFont = true;
            this.GIA3.AppearanceHeader.Options.UseTextOptions = true;
            this.GIA3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIA3.Caption = "Giá 3";
            this.GIA3.ColumnEdit = this.txtGIABAN;
            this.GIA3.DisplayFormat.FormatString = "N0";
            this.GIA3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GIA3.FieldName = "GIA3";
            this.GIA3.Name = "GIA3";
            this.GIA3.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.GIA3.OptionsColumn.FixedWidth = true;
            this.GIA3.Visible = true;
            this.GIA3.VisibleIndex = 5;
            this.GIA3.Width = 120;
            // 
            // THOIGIAN4
            // 
            this.THOIGIAN4.AppearanceCell.Options.UseTextOptions = true;
            this.THOIGIAN4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THOIGIAN4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THOIGIAN4.AppearanceHeader.Options.UseFont = true;
            this.THOIGIAN4.AppearanceHeader.Options.UseTextOptions = true;
            this.THOIGIAN4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THOIGIAN4.Caption = "Thời gian 4";
            this.THOIGIAN4.ColumnEdit = this.time_thoigian4;
            this.THOIGIAN4.DisplayFormat.FormatString = "HH:mm:ss";
            this.THOIGIAN4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.THOIGIAN4.FieldName = "THOIGIAN4";
            this.THOIGIAN4.MinWidth = 120;
            this.THOIGIAN4.Name = "THOIGIAN4";
            this.THOIGIAN4.OptionsColumn.AllowEdit = false;
            this.THOIGIAN4.OptionsColumn.AllowMove = false;
            this.THOIGIAN4.OptionsColumn.FixedWidth = true;
            this.THOIGIAN4.Visible = true;
            this.THOIGIAN4.VisibleIndex = 6;
            this.THOIGIAN4.Width = 120;
            // 
            // time_thoigian4
            // 
            this.time_thoigian4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.time_thoigian4.Appearance.Options.UseFont = true;
            this.time_thoigian4.AutoHeight = false;
            this.time_thoigian4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.time_thoigian4.DisplayFormat.FormatString = "HH:mm:ss";
            this.time_thoigian4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.time_thoigian4.EditFormat.FormatString = "HH:mm:ss";
            this.time_thoigian4.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.time_thoigian4.Mask.EditMask = "HH:mm:ss";
            this.time_thoigian4.Name = "time_thoigian4";
            // 
            // lookKHUVUC
            // 
            this.lookKHUVUC.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lookKHUVUC.AppearanceDropDown.Options.UseFont = true;
            this.lookKHUVUC.AutoHeight = false;
            this.lookKHUVUC.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookKHUVUC.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA_KHUVUC", "MA_KHUVUC", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN_KHUVUC", "TEN_KHUVUC"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN_CUAHANG", 40, "TEN_CUAHANG")});
            this.lookKHUVUC.DisplayMember = "TEN_KHUVUC";
            this.lookKHUVUC.DropDownItemHeight = 40;
            this.lookKHUVUC.Name = "lookKHUVUC";
            this.lookKHUVUC.ShowHeader = false;
            this.lookKHUVUC.ValueMember = "MA_KHUVUC";
            // 
            // lookHANGHOA
            // 
            this.lookHANGHOA.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lookHANGHOA.AppearanceDropDown.Options.UseFont = true;
            this.lookHANGHOA.AutoHeight = false;
            this.lookHANGHOA.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookHANGHOA.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA_HANGHOA", "MA_HANGHOA", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN_HANGHOA", "TEN_HANGHOA"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN_DONVITINH", 10, "TEN_DONVITINH")});
            this.lookHANGHOA.DisplayMember = "TEN_HANGHOA";
            this.lookHANGHOA.DropDownItemHeight = 40;
            this.lookHANGHOA.Name = "lookHANGHOA";
            this.lookHANGHOA.ShowHeader = false;
            this.lookHANGHOA.ValueMember = "MA_HANGHOA";
            // 
            // Frm_Lapbanggia_Karaoke
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 690);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panelControl5);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.pnTop);
            this.Name = "Frm_Lapbanggia_Karaoke";
            this.Text = "Lập bảng giá giờ";
            this.Load += new System.EventHandler(this.Frm_Lapbanggia_Karaoke_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Lapbanggia_Karaoke_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnTop)).EndInit();
            this.pnTop.ResumeLayout(false);
            this.pnTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHU.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.time_thoigian1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.time_thoigian2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.time_thoigian3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.time_thoigian4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookKHUVUC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookHANGHOA)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl pnTop;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNG;
        private DevExpress.XtraEditors.LabelControl lblGHICHU;
        private DevExpress.XtraEditors.TextEdit txtGHICHU;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.TextEdit txtTEN;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private System.Windows.Forms.FlowLayoutPanel fpnlBanggia;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lookKHUVUC;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lookHANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn THOIGIAN1;
        private DevExpress.XtraGrid.Columns.GridColumn GIA1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtGIABAN;
        private DevExpress.XtraGrid.Columns.GridColumn THOIGIAN2;
        private DevExpress.XtraGrid.Columns.GridColumn GIA2;
        private DevExpress.XtraGrid.Columns.GridColumn THOIGIAN3;
        private DevExpress.XtraGrid.Columns.GridColumn GIA3;
        private DevExpress.XtraGrid.Columns.GridColumn THOIGIAN4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit time_thoigian1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit time_thoigian2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit time_thoigian3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit time_thoigian4;




    }
}