﻿namespace KP_RES
{
    partial class Frm_Chonthoigianra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Chonthoigianra));
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.dtpGiovao = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.dtpGiora = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnDongy = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpGiovao.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpGiovao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpGiora.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpGiora.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.dtpGiovao);
            this.panelControl2.Controls.Add(this.labelControl2);
            this.panelControl2.Controls.Add(this.dtpGiora);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(229, 125);
            this.panelControl2.TabIndex = 0;
            // 
            // dtpGiovao
            // 
            this.dtpGiovao.EditValue = null;
            this.dtpGiovao.EnterMoveNextControl = true;
            this.dtpGiovao.Location = new System.Drawing.Point(12, 36);
            this.dtpGiovao.Name = "dtpGiovao";
            this.dtpGiovao.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGiovao.Properties.Appearance.Options.UseFont = true;
            this.dtpGiovao.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGiovao.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpGiovao.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpGiovao.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpGiovao.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpGiovao.Properties.EditFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.dtpGiovao.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpGiovao.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.dtpGiovao.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtpGiovao.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpGiovao.Size = new System.Drawing.Size(206, 26);
            this.dtpGiovao.TabIndex = 4;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl2.Location = new System.Drawing.Point(12, 17);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(64, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Giờ bắt đầu";
            // 
            // dtpGiora
            // 
            this.dtpGiora.EditValue = null;
            this.dtpGiora.EnterMoveNextControl = true;
            this.dtpGiora.Location = new System.Drawing.Point(13, 96);
            this.dtpGiora.Name = "dtpGiora";
            this.dtpGiora.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGiora.Properties.Appearance.Options.UseFont = true;
            this.dtpGiora.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGiora.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpGiora.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpGiora.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpGiora.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpGiora.Properties.EditFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.dtpGiora.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpGiora.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.dtpGiora.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtpGiora.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpGiora.Size = new System.Drawing.Size(206, 26);
            this.dtpGiora.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Location = new System.Drawing.Point(13, 77);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(68, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Giờ kết thúc";
            // 
            // btnDongy
            // 
            this.btnDongy.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDongy.Appearance.Options.UseFont = true;
            this.btnDongy.Image = global::KP_RES.Properties.Resources.checked_checkbox_26;
            this.btnDongy.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDongy.Location = new System.Drawing.Point(118, 132);
            this.btnDongy.Margin = new System.Windows.Forms.Padding(4);
            this.btnDongy.Name = "btnDongy";
            this.btnDongy.Size = new System.Drawing.Size(100, 40);
            this.btnDongy.TabIndex = 3;
            this.btnDongy.Text = "&OK";
            this.btnDongy.Click += new System.EventHandler(this.btnDongy_Click);
            // 
            // Frm_Chonthoigianra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(231, 176);
            this.ControlBox = false;
            this.Controls.Add(this.btnDongy);
            this.Controls.Add(this.panelControl2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_Chonthoigianra";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Giờ kết thúc";
            this.Load += new System.EventHandler(this.Frm_Chonthoigianra_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpGiovao.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpGiovao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpGiora.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpGiora.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnDongy;
        private DevExpress.XtraEditors.DateEdit dtpGiora;
        private DevExpress.XtraEditors.DateEdit dtpGiovao;
        private DevExpress.XtraEditors.LabelControl labelControl2;
    }
}