﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using DevExpress.XtraEditors.Controls;
using KP_Report;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_BC_InlaiHoadonKaraoke : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt = new DataTable();

        public Frm_BC_InlaiHoadonKaraoke()
        {
            InitializeComponent();
            LoadCombo();
        }
     
        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;
        }

        private void btnTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                string sql = string.Empty;

                sql = "EXEC SelectHoadonInlai_KARAOKE @TUNGAY='" + string.Format("{0:yyyyMMdd}", dtpTungay.EditValue)
                    + "', @DENNGAY='" + string.Format("{0:yyyyMMdd}", dtpDenngay.EditValue) + " 23:59:59',@MANHANVIEN='"
                    + cboNV.EditValue.ToString() + "'";
                dt = clsMain.ReturnDataTable(sql);
                gridControl1.DataSource = dt;
                gridView1.Columns["MA_HOADON"].GroupIndex = 0;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXuatfile_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show(_kocodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                STT.Visible = false;
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.Filter = "Excel (2010) (.xlsx)|*.xlsx |Excel (2003)(.xls)|*.xls";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        switch (fileExtenstion)
                        {
                            case ".xls":
                                gridControl1.ExportToXls(exportFilePath);
                                break;
                            case ".xlsx":
                                gridControl1.ExportToXlsx(exportFilePath);
                                break;
                            default:
                                break;
                        }
                    }
                }
                STT.Visible = true;
            }
        }

        private void gridView1_CustomDrawGroupRow(object sender, DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs e)
        {
            //GridView view = sender as GridView;
            //GridGroupRowInfo info = e.Info as GridGroupRowInfo;
            //if (info.Column.Caption == "Tên nhân viên")
            //{
            //    info.GroupText = info.Column.Caption + ": <color=Blue>" + info.GroupValueText + "</color> ";
            //    info.GroupText += "<color=Red>" + view.GetGroupSummaryText(e.RowHandle) + "</color> ";
            //}
        }

        private void LoadCombo()
        {
            dtpTungay.EditValue = clsGlobal.gdServerDate;
            dtpDenngay.EditValue = clsGlobal.gdServerDate;
            DataTable dt = clsMain.ReturnDataTable("select MANHANVIEN as MA,TENNHANVIEN as TEN from DM_NHANVIEN where SUDUNG=1 order by TEN");
            dt.Rows.Add("0", "Tất cả");
            cboNV.Properties.DataSource = dt;
            cboNV.EditValue = 0;
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
        }

        private void btnXemtruockhiin_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    DataColumn a = new DataColumn();
                    a.ColumnName = "FILTER";
                    a.DataType = System.Type.GetType("System.String");
                    a.DefaultValue = "Nhân viên : " + cboNV.Text;
                    if (ContainColumn("FILTER", dt) == false)
                        dt.Columns.Add(a);
                    DataColumn a1 = new DataColumn();
                    a1.ColumnName = "TUNGAY";
                    a1.DefaultValue = string.Format("{0:dd/MM/yyyy}", dtpTungay.EditValue);
                    if (ContainColumn("TUNGAY", dt) == false)
                        dt.Columns.Add(a1);
                    DataColumn a2 = new DataColumn();
                    a2.ColumnName = "DENNGAY";
                    a2.DefaultValue = string.Format("{0:dd/MM/yyyy}", dtpDenngay.EditValue);
                    if (ContainColumn("DENNGAY", dt) == false)
                        dt.Columns.Add(a2);


                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.dtSource = dt;
                    frm.Mode = 48;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show(_kocodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception EX)
            {
                XtraMessageBox.Show(EX.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally { this.Cursor = Cursors.Default; }
        }

        private void btnThunho_Click(object sender, EventArgs e)
        {
            if (panelControl1.Width == 219)
            {
                panelControl1.Width = 39;
                btnThunho.Image = global::KP_RES.Properties.Resources.right_round_26;
                btnThunho.Dock = DockStyle.Fill;
                panelControl6.Visible = false;
            }
            else
            {
                panelControl1.Width = 219;
                btnThunho.Image = global::KP_RES.Properties.Resources.left_round_26;
                btnThunho.Dock = DockStyle.Bottom;
                panelControl6.Visible = true;
            }
            this.Refresh();
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnChitiet_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle < 0)
                    throw new Exception(_chuachondtcanxem);
                string sql = "select * from CT_TIENGIOKARAOKE where MA_HOADON = '" + gridView1.GetFocusedRowCellValue(MA_HOADON).ToString() + "'";
                DataTable dtKaraoke = clsMain.ReturnDataTable(sql);
                if (dtKaraoke.Rows.Count > 0)//Hóa don Karaoke, có ti?n gi?
                {
                    Frm_BCChitietHoaDon_Karaoke frm = new Frm_BCChitietHoaDon_Karaoke(gridView1.GetFocusedRowCellValue(MA_HOADON).ToString());
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.WindowState = FormWindowState.Maximized;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    Frm_BCChitietHoaDon frm = new Frm_BCChitietHoaDon(gridView1.GetFocusedRowCellValue(MA_HOADON).ToString());
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.WindowState = FormWindowState.Maximized;
                    frm.ShowDialog();
                    frm.Dispose();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnInbill_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount > 0)
                {
                    List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                    string sql = "Exec SP_INHOADON_DSHoaDon_Karaoke @Mahoadon='" + gridView1.GetFocusedRowCellValue(MA_HOADON).ToString() + "'";
                    DataTable dt2 = clsMain.ReturnDataTable(sql);

                    string sqlKaraoke = string.Empty;
                    sqlKaraoke = "SELECT NGAYTAO, MA_HOADON, B.MA_BAN, B.TEN_BAN as TENPHONG, THOIGIANVAO, THOIGIANRA, BLOCKKARAOKE, SOPHUT, SOBLOCK, GIABLOCK, THANHTIEN" + "\n";
                    sqlKaraoke += "FROM CT_TIENGIOKARAOKE CT LEFT JOIN BAN B ON CT.MA_PHONG = B.MA_BAN" + "\n";
                    sqlKaraoke += "WHERE MA_HOADON = '" + gridView1.GetFocusedRowCellValue(MA_HOADON).ToString() + "'";
                    DataTable dtKaraoke = clsMain.ReturnDataTable(sqlKaraoke);

                    int n = dt2.Rows.Count;
                    for (int i = 0; i < n; i++)
                    {
                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        dmHangHoa.STT = (i + 1).ToString();
                        dmHangHoa.Ten_HoaDon = "HÓA ĐƠN BÁN LẺ\n(IN LẠI)";
                        dmHangHoa.MA_HANGHOA = dt2.Rows[i]["MA_HANGHOA"].ToString();
                        dmHangHoa.TEN_HANGHOA = dt2.Rows[i]["TEN_HANGHOA"].ToString();
                        dmHangHoa.SOLUONG = decimal.Parse(dt2.Rows[i]["SOLUONG"].ToString());
                        dmHangHoa.GIABAN = decimal.Parse(dt2.Rows[i]["GIABAN"].ToString());
                        //dmHangHoa.THUE = double.Parse(dt2.Rows[i]["THUE"].ToString());
                        dmHangHoa.Thanh_Tien = decimal.Parse(dt2.Rows[i]["Thanh_Tien"].ToString());
                        dmHangHoa.Tong_Cong = string.Format("{0:#,###0}", dt2.Rows[i]["Tong_Cong"]);
                        dmHangHoa.Tien_TraKhach = string.Format("{0:#,###0}", dt2.Rows[i]["Tien_TraKhach"]);
                        dmHangHoa.Tien_KhachTra = string.Format("{0:#,###0}", dt2.Rows[i]["Tien_KhachTra"]);
                        dmHangHoa.Phiphucvu = string.Format("{0:#,###0}", dt2.Rows[i]["Phiphucvu"]);
                        dmHangHoa.Giam_Gia = string.Format("{0:#,###0}", dt2.Rows[i]["GIAMGIA"]);
                        dmHangHoa.Ma_HoaDon = dt2.Rows[i]["MA_HOADON"].ToString();
                        dmHangHoa.TienGio = string.Format("{0:#,###0}", dt2.Rows[i]["TIENGIO"]);
                        dmHangHoa.Ma_Quay = dt2.Rows[i]["TEN_QUAY"].ToString();
                        dmHangHoa.TenNV = dt2.Rows[i]["TENNV"].ToString();
                        dmHangHoa.ngayHD = string.Format("{0:dd/MM/yyyy HH:mm:ss}", dt2.Rows[i]["NGAYHD"]);
                        dmHangHoa.MaBan = dtKaraoke.Rows[0]["MA_BAN"].ToString();
                        dmHangHoa.Giovao = Convert.ToDateTime(dtKaraoke.Rows[0]["THOIGIANVAO"]);
                        dmHangHoa.Giora = Convert.ToDateTime(dtKaraoke.Rows[0]["THOIGIANRA"]);
                        listDM_HangHoa.Add(dmHangHoa);
                    }

                    convert cvrt = new convert();
                    // printer bill
                    DataTable bill = cvrt.ConvertToDataTable(listDM_HangHoa);
                    Frm_BCInPhieu frm = new Frm_BCInPhieu();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

                    frm.dtSource = bill;
                    frm.Mode = 119;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                else
                {
                    XtraMessageBox.Show("Không có dữ liệu", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception EX)
            {
                XtraMessageBox.Show(EX.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally { this.Cursor = Cursors.Default; }   
        }


        string _thongbao = "";
        string _kocodulieu = "";
        string _chuachondtcanxem = "";
        private void Frm_BC_InlaiHoadon_Load(object sender, EventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            this.Text = rm.GetString("danhsachhoadoninlai", culture);
            gridView1.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView1.Columns["MA_HOADON"].Caption = rm.GetString("hoadon", culture);
            gridView1.Columns["TEN_NHANVIEN"].Caption = rm.GetString("nhanvien", culture);
            gridView1.Columns["NGAYTAO"].Caption = rm.GetString("ngaytao", culture);
            gridView1.Columns["TONGTIEN"].Caption = rm.GetString("tongtien", culture);
            gridView1.Columns["NV_INLAI"].Caption = rm.GetString("nhanvieninlai", culture);
            gridView1.Columns["GIO_INLAI"].Caption = rm.GetString("gioinlai", culture);
            gridView1.GroupPanelText = rm.GetString("keothacotvaodaydenhom", culture);
            labeFrom.Text = rm.GetString("tungay", culture);
            lb_To.Text = rm.GetString("denngay", culture);
            lbNhanVien.Text = rm.GetString("nhanvien", culture);
            btnTimkiem.Text = rm.GetString("xem", culture);
            _chuachondtcanxem = rm.GetString("chuachondtcanxem", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _kocodulieu = rm.GetString("kocodulieu", culture);
        }
    }
}