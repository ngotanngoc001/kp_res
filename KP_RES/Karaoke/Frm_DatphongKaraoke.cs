﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;

namespace KP_RES
{
    public partial class Frm_DatphongKaraoke : DevExpress.XtraEditors.XtraForm
    {
        public string idTable = null;
        public Frm_DatphongKaraoke()
        {
            InitializeComponent();
        }
        private void txtIdTables_Click(object sender, EventArgs e)
        {
            us_keyboard1.SetTextbox(txtIdTables);
        }
        private void txtPhoneNumber_Click(object sender, EventArgs e)
        {
            us_keyboard1.SetTextbox(txtPhoneNumber);
        }
        public string configTables()
        {
            string sql = "select MA_HOADON from HOADON_KARAOKE  ";
            sql += " where MA_HOADON = '" + txtIdTables.Text + "' OR SODIENTHOAI = '" + txtPhoneNumber.Text + "'";
            sql += " Order by GIOVAO";
            DataTable dt = clsMain.ReturnDataTable(sql);
            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0]["MA_HOADON"].ToString();
            }
            else
                return null;      
        }
        private void txtIdTables_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (txtIdTables.Text == "")
                    {
                        XtraMessageBox.Show("Chưa nhập số hóa đơn !", "Thông Báo ", MessageBoxButtons.OK);
                        txtIdTables.Focus();
                        return;
                    }
                    if (configTables() != null)
                    {
                        idTable = configTables();
                        this.Close();
                    }
                }
            }
            catch
            { 
            }
        }

        private void txtPhoneNumber_KeyDown(object sender, KeyEventArgs e)
        {
            txtIdTables_KeyDown(sender, e);
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}