﻿namespace KP_RES 
{
    partial class Frm_KhuvucPhongKaraoke 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_KhuvucPhongKaraoke));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.pal_1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.pal_ban = new System.Windows.Forms.Panel();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.bnt_trangxuong = new DevExpress.XtraEditors.SimpleButton();
            this.lbl_sotrang1 = new DevExpress.XtraEditors.SimpleButton();
            this.pal_sotrang = new DevExpress.XtraEditors.PanelControl();
            this.bnt_tranglen = new DevExpress.XtraEditors.SimpleButton();
            this.lbl_sotrang = new DevExpress.XtraEditors.SimpleButton();
            this.panel = new DevExpress.XtraEditors.PanelControl();
            this.pal_khuvuc = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.bnt_Xuong = new DevExpress.XtraEditors.SimpleButton();
            this.pal_len = new DevExpress.XtraEditors.PanelControl();
            this.bnt_Len = new DevExpress.XtraEditors.SimpleButton();
            this.pnRight = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.tab_Option = new DevExpress.XtraTab.XtraTabControl();
            this.tab_ban = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl17 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl13 = new DevExpress.XtraEditors.PanelControl();
            this.bnt_ban_phai = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_ban_len = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_ban_xuong = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_ban_trai = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl14 = new DevExpress.XtraEditors.PanelControl();
            this.btnLuusodoban = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_macdinh = new DevExpress.XtraEditors.SimpleButton();
            this.pal_themnhanh = new DevExpress.XtraEditors.PanelControl();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.picAnhban = new System.Windows.Forms.PictureBox();
            this.btnLuuthemnhanh = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.cbb_kichthuocban = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txt_soban = new DevExpress.XtraEditors.TextEdit();
            this.txt_sogheban = new DevExpress.XtraEditors.TextEdit();
            this.panel_Sandard = new DevExpress.XtraEditors.PanelControl();
            this.bnt_suaban = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_xoaban = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_luuban = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_themnhanh = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_themban = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.pic_hinhnen = new System.Windows.Forms.PictureBox();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cboKhuvuc = new DevExpress.XtraEditors.LookUpEdit();
            this.chkSudungban = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.cbb_kichthuoc = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txt_ban = new DevExpress.XtraEditors.TextEdit();
            this.txt_soghe = new DevExpress.XtraEditors.TextEdit();
            this.tab_khuvuc = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl16 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.picKhuvuc = new System.Windows.Forms.PictureBox();
            this.cboCUAHANG = new DevExpress.XtraEditors.LookUpEdit();
            this.btnSuaKhuvuc = new DevExpress.XtraEditors.SimpleButton();
            this.lblGHICHU = new DevExpress.XtraEditors.LabelControl();
            this.chkSUDUNG = new DevExpress.XtraEditors.CheckEdit();
            this.txt_ghichu = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.btnThemKhuvuc = new DevExpress.XtraEditors.SimpleButton();
            this.txtSTT = new DevExpress.XtraEditors.TextEdit();
            this.txt_khuvuc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.btnLuuKhuvuc = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaKhuvuc = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl18 = new DevExpress.XtraEditors.PanelControl();
            this.pal_cuahang = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.btnChonKhuvuc = new DevExpress.XtraEditors.SimpleButton();
            this.btnChonBan = new DevExpress.XtraEditors.SimpleButton();
            this.pal_BUTTON = new DevExpress.XtraEditors.PanelControl();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.timer4 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_1)).BeginInit();
            this.pal_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_sotrang)).BeginInit();
            this.pal_sotrang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel)).BeginInit();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_len)).BeginInit();
            this.pal_len.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnRight)).BeginInit();
            this.pnRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tab_Option)).BeginInit();
            this.tab_Option.SuspendLayout();
            this.tab_ban.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).BeginInit();
            this.panelControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).BeginInit();
            this.panelControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_themnhanh)).BeginInit();
            this.pal_themnhanh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAnhban)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbb_kichthuocban.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soban.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_sogheban.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel_Sandard)).BeginInit();
            this.panel_Sandard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_hinhnen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboKhuvuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSudungban.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbb_kichthuoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ban.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soghe.Properties)).BeginInit();
            this.tab_khuvuc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).BeginInit();
            this.panelControl16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            this.panelControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picKhuvuc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCUAHANG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ghichu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_khuvuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).BeginInit();
            this.panelControl18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pal_BUTTON)).BeginInit();
            this.pal_BUTTON.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.panelControl5);
            this.panelControl1.Controls.Add(this.panel);
            this.panelControl1.Controls.Add(this.pnRight);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 741);
            this.panelControl1.TabIndex = 0;
            // 
            // panelControl5
            // 
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl5.Controls.Add(this.pal_1);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(0, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(535, 741);
            this.panelControl5.TabIndex = 4;
            // 
            // pal_1
            // 
            this.pal_1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pal_1.Appearance.Options.UseBackColor = true;
            this.pal_1.Controls.Add(this.panelControl9);
            this.pal_1.Controls.Add(this.panelControl6);
            this.pal_1.Controls.Add(this.pal_sotrang);
            this.pal_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_1.Location = new System.Drawing.Point(0, 0);
            this.pal_1.Name = "pal_1";
            this.pal_1.Size = new System.Drawing.Size(535, 741);
            this.pal_1.TabIndex = 1;
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.pal_ban);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl9.Location = new System.Drawing.Point(2, 37);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(531, 667);
            this.panelControl9.TabIndex = 56;
            // 
            // pal_ban
            // 
            this.pal_ban.BackColor = System.Drawing.Color.Transparent;
            this.pal_ban.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_ban.Font = new System.Drawing.Font("Tahoma", 12F);
            this.pal_ban.Location = new System.Drawing.Point(2, 2);
            this.pal_ban.Name = "pal_ban";
            this.pal_ban.Size = new System.Drawing.Size(527, 663);
            this.pal_ban.TabIndex = 55;
            // 
            // panelControl6
            // 
            this.panelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl6.Controls.Add(this.bnt_trangxuong);
            this.panelControl6.Controls.Add(this.lbl_sotrang1);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl6.Location = new System.Drawing.Point(2, 704);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(531, 35);
            this.panelControl6.TabIndex = 54;
            // 
            // bnt_trangxuong
            // 
            this.bnt_trangxuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_trangxuong.Appearance.Options.UseFont = true;
            this.bnt_trangxuong.Appearance.Options.UseTextOptions = true;
            this.bnt_trangxuong.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bnt_trangxuong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bnt_trangxuong.Image = global::KP_RES.Properties.Resources.up11_26;
            this.bnt_trangxuong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_trangxuong.Location = new System.Drawing.Point(78, 0);
            this.bnt_trangxuong.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_trangxuong.Name = "bnt_trangxuong";
            this.bnt_trangxuong.Size = new System.Drawing.Size(453, 35);
            this.bnt_trangxuong.TabIndex = 50;
            this.bnt_trangxuong.Text = "Khu vực";
            this.bnt_trangxuong.Click += new System.EventHandler(this.bnt_trangxuong_Click);
            // 
            // lbl_sotrang1
            // 
            this.lbl_sotrang1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_sotrang1.Appearance.Options.UseFont = true;
            this.lbl_sotrang1.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbl_sotrang1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.lbl_sotrang1.Location = new System.Drawing.Point(0, 0);
            this.lbl_sotrang1.Margin = new System.Windows.Forms.Padding(4);
            this.lbl_sotrang1.Name = "lbl_sotrang1";
            this.lbl_sotrang1.Size = new System.Drawing.Size(78, 35);
            this.lbl_sotrang1.TabIndex = 49;
            this.lbl_sotrang1.Text = "1/5";
            // 
            // pal_sotrang
            // 
            this.pal_sotrang.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_sotrang.Controls.Add(this.bnt_tranglen);
            this.pal_sotrang.Controls.Add(this.lbl_sotrang);
            this.pal_sotrang.Dock = System.Windows.Forms.DockStyle.Top;
            this.pal_sotrang.Location = new System.Drawing.Point(2, 2);
            this.pal_sotrang.Name = "pal_sotrang";
            this.pal_sotrang.Size = new System.Drawing.Size(531, 35);
            this.pal_sotrang.TabIndex = 52;
            // 
            // bnt_tranglen
            // 
            this.bnt_tranglen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_tranglen.Appearance.Options.UseFont = true;
            this.bnt_tranglen.Appearance.Options.UseTextOptions = true;
            this.bnt_tranglen.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bnt_tranglen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bnt_tranglen.Image = global::KP_RES.Properties.Resources.up22_26;
            this.bnt_tranglen.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_tranglen.Location = new System.Drawing.Point(78, 0);
            this.bnt_tranglen.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_tranglen.Name = "bnt_tranglen";
            this.bnt_tranglen.Size = new System.Drawing.Size(453, 35);
            this.bnt_tranglen.TabIndex = 50;
            this.bnt_tranglen.Text = "Khu vực";
            this.bnt_tranglen.Click += new System.EventHandler(this.bnt_tranglen_Click);
            // 
            // lbl_sotrang
            // 
            this.lbl_sotrang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_sotrang.Appearance.Options.UseFont = true;
            this.lbl_sotrang.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbl_sotrang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.lbl_sotrang.Location = new System.Drawing.Point(0, 0);
            this.lbl_sotrang.Margin = new System.Windows.Forms.Padding(4);
            this.lbl_sotrang.Name = "lbl_sotrang";
            this.lbl_sotrang.Size = new System.Drawing.Size(78, 35);
            this.lbl_sotrang.TabIndex = 49;
            this.lbl_sotrang.Text = "1/5";
            // 
            // panel
            // 
            this.panel.Controls.Add(this.pal_khuvuc);
            this.panel.Controls.Add(this.panelControl7);
            this.panel.Controls.Add(this.panelControl11);
            this.panel.Controls.Add(this.pal_len);
            this.panel.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.panel.Location = new System.Drawing.Point(535, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(135, 741);
            this.panel.TabIndex = 2;
            // 
            // pal_khuvuc
            // 
            this.pal_khuvuc.AutoScroll = true;
            this.pal_khuvuc.BackColor = System.Drawing.Color.Transparent;
            this.pal_khuvuc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_khuvuc.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pal_khuvuc.Location = new System.Drawing.Point(2, 37);
            this.pal_khuvuc.Margin = new System.Windows.Forms.Padding(0);
            this.pal_khuvuc.Name = "pal_khuvuc";
            this.pal_khuvuc.Size = new System.Drawing.Size(131, 667);
            this.pal_khuvuc.TabIndex = 29;
            // 
            // panelControl7
            // 
            this.panelControl7.Location = new System.Drawing.Point(915, 291);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(77, 168);
            this.panelControl7.TabIndex = 1;
            // 
            // panelControl11
            // 
            this.panelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl11.Controls.Add(this.bnt_Xuong);
            this.panelControl11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl11.Location = new System.Drawing.Point(2, 704);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(131, 35);
            this.panelControl11.TabIndex = 28;
            // 
            // bnt_Xuong
            // 
            this.bnt_Xuong.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bnt_Xuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_Xuong.Appearance.Options.UseBackColor = true;
            this.bnt_Xuong.Appearance.Options.UseFont = true;
            this.bnt_Xuong.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bnt_Xuong.Image = global::KP_RES.Properties.Resources.up11_26;
            this.bnt_Xuong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_Xuong.Location = new System.Drawing.Point(0, 0);
            this.bnt_Xuong.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_Xuong.Name = "bnt_Xuong";
            this.bnt_Xuong.Size = new System.Drawing.Size(131, 35);
            this.bnt_Xuong.TabIndex = 2;
            this.bnt_Xuong.Click += new System.EventHandler(this.btn_Xuong_Click);
            // 
            // pal_len
            // 
            this.pal_len.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_len.Controls.Add(this.bnt_Len);
            this.pal_len.Dock = System.Windows.Forms.DockStyle.Top;
            this.pal_len.Location = new System.Drawing.Point(2, 2);
            this.pal_len.Name = "pal_len";
            this.pal_len.Size = new System.Drawing.Size(131, 35);
            this.pal_len.TabIndex = 2;
            // 
            // bnt_Len
            // 
            this.bnt_Len.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bnt_Len.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_Len.Appearance.Options.UseBackColor = true;
            this.bnt_Len.Appearance.Options.UseFont = true;
            this.bnt_Len.Dock = System.Windows.Forms.DockStyle.Top;
            this.bnt_Len.Image = global::KP_RES.Properties.Resources.up22_26;
            this.bnt_Len.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_Len.Location = new System.Drawing.Point(0, 0);
            this.bnt_Len.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_Len.Name = "bnt_Len";
            this.bnt_Len.Size = new System.Drawing.Size(131, 35);
            this.bnt_Len.TabIndex = 26;
            this.bnt_Len.Click += new System.EventHandler(this.bnt_Len_Click);
            // 
            // pnRight
            // 
            this.pnRight.Controls.Add(this.panelControl3);
            this.pnRight.Controls.Add(this.panelControl18);
            this.pnRight.Controls.Add(this.panelControl12);
            this.pnRight.Controls.Add(this.pal_BUTTON);
            this.pnRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnRight.Location = new System.Drawing.Point(670, 0);
            this.pnRight.Name = "pnRight";
            this.pnRight.Size = new System.Drawing.Size(350, 741);
            this.pnRight.TabIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.tab_Option);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 125);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(346, 579);
            this.panelControl3.TabIndex = 18;
            // 
            // tab_Option
            // 
            this.tab_Option.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tab_Option.Location = new System.Drawing.Point(0, 0);
            this.tab_Option.Name = "tab_Option";
            this.tab_Option.SelectedTabPage = this.tab_ban;
            this.tab_Option.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.tab_Option.Size = new System.Drawing.Size(346, 579);
            this.tab_Option.TabIndex = 0;
            this.tab_Option.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tab_khuvuc,
            this.tab_ban});
            // 
            // tab_ban
            // 
            this.tab_ban.Controls.Add(this.panelControl2);
            this.tab_ban.Name = "tab_ban";
            this.tab_ban.Size = new System.Drawing.Size(340, 573);
            this.tab_ban.Text = "xtraTabPage2";
            // 
            // panelControl2
            // 
            this.panelControl2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.panelControl2.Appearance.Options.UseBackColor = true;
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.panelControl17);
            this.panelControl2.Controls.Add(this.panelControl13);
            this.panelControl2.Controls.Add(this.panelControl14);
            this.panelControl2.Controls.Add(this.pal_themnhanh);
            this.panelControl2.Controls.Add(this.panel_Sandard);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(340, 573);
            this.panelControl2.TabIndex = 0;
            // 
            // panelControl17
            // 
            this.panelControl17.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl17.Location = new System.Drawing.Point(0, 432);
            this.panelControl17.Name = "panelControl17";
            this.panelControl17.Size = new System.Drawing.Size(340, 58);
            this.panelControl17.TabIndex = 55;
            // 
            // panelControl13
            // 
            this.panelControl13.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl13.Controls.Add(this.bnt_ban_phai);
            this.panelControl13.Controls.Add(this.bnt_ban_len);
            this.panelControl13.Controls.Add(this.bnt_ban_xuong);
            this.panelControl13.Controls.Add(this.bnt_ban_trai);
            this.panelControl13.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl13.Location = new System.Drawing.Point(0, 490);
            this.panelControl13.Name = "panelControl13";
            this.panelControl13.Size = new System.Drawing.Size(340, 83);
            this.panelControl13.TabIndex = 36;
            // 
            // bnt_ban_phai
            // 
            this.bnt_ban_phai.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bnt_ban_phai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_ban_phai.Appearance.ForeColor = System.Drawing.Color.Black;
            this.bnt_ban_phai.Appearance.Options.UseBackColor = true;
            this.bnt_ban_phai.Appearance.Options.UseFont = true;
            this.bnt_ban_phai.Appearance.Options.UseForeColor = true;
            this.bnt_ban_phai.Image = global::KP_RES.Properties.Resources.right_26;
            this.bnt_ban_phai.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_ban_phai.Location = new System.Drawing.Point(230, 44);
            this.bnt_ban_phai.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_ban_phai.Name = "bnt_ban_phai";
            this.bnt_ban_phai.Size = new System.Drawing.Size(110, 35);
            this.bnt_ban_phai.TabIndex = 34;
            this.bnt_ban_phai.Click += new System.EventHandler(this.bnt_ban_phai_Click);
            this.bnt_ban_phai.MouseDown += new System.Windows.Forms.MouseEventHandler(this.bnt_ban_phai_MouseDown);
            this.bnt_ban_phai.MouseUp += new System.Windows.Forms.MouseEventHandler(this.bnt_ban_phai_MouseUp);
            // 
            // bnt_ban_len
            // 
            this.bnt_ban_len.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bnt_ban_len.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_ban_len.Appearance.ForeColor = System.Drawing.Color.Black;
            this.bnt_ban_len.Appearance.Options.UseBackColor = true;
            this.bnt_ban_len.Appearance.Options.UseFont = true;
            this.bnt_ban_len.Appearance.Options.UseForeColor = true;
            this.bnt_ban_len.Image = global::KP_RES.Properties.Resources.up_26;
            this.bnt_ban_len.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_ban_len.Location = new System.Drawing.Point(117, 5);
            this.bnt_ban_len.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_ban_len.Name = "bnt_ban_len";
            this.bnt_ban_len.Size = new System.Drawing.Size(110, 35);
            this.bnt_ban_len.TabIndex = 32;
            this.bnt_ban_len.Click += new System.EventHandler(this.bnt_ban_len_Click);
            this.bnt_ban_len.MouseDown += new System.Windows.Forms.MouseEventHandler(this.bnt_ban_len_MouseDown);
            this.bnt_ban_len.MouseUp += new System.Windows.Forms.MouseEventHandler(this.bnt_ban_len_MouseUp);
            // 
            // bnt_ban_xuong
            // 
            this.bnt_ban_xuong.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bnt_ban_xuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_ban_xuong.Appearance.ForeColor = System.Drawing.Color.Black;
            this.bnt_ban_xuong.Appearance.Options.UseBackColor = true;
            this.bnt_ban_xuong.Appearance.Options.UseFont = true;
            this.bnt_ban_xuong.Appearance.Options.UseForeColor = true;
            this.bnt_ban_xuong.Image = global::KP_RES.Properties.Resources.down_26;
            this.bnt_ban_xuong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_ban_xuong.Location = new System.Drawing.Point(117, 44);
            this.bnt_ban_xuong.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_ban_xuong.Name = "bnt_ban_xuong";
            this.bnt_ban_xuong.Size = new System.Drawing.Size(110, 35);
            this.bnt_ban_xuong.TabIndex = 35;
            this.bnt_ban_xuong.Click += new System.EventHandler(this.bnt_ban_xuong_Click);
            this.bnt_ban_xuong.MouseDown += new System.Windows.Forms.MouseEventHandler(this.bnt_ban_xuong_MouseDown);
            this.bnt_ban_xuong.MouseUp += new System.Windows.Forms.MouseEventHandler(this.bnt_ban_xuong_MouseUp);
            // 
            // bnt_ban_trai
            // 
            this.bnt_ban_trai.Appearance.BackColor = System.Drawing.Color.Silver;
            this.bnt_ban_trai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_ban_trai.Appearance.ForeColor = System.Drawing.Color.Black;
            this.bnt_ban_trai.Appearance.Options.UseBackColor = true;
            this.bnt_ban_trai.Appearance.Options.UseFont = true;
            this.bnt_ban_trai.Appearance.Options.UseForeColor = true;
            this.bnt_ban_trai.Image = global::KP_RES.Properties.Resources.left_26;
            this.bnt_ban_trai.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_ban_trai.Location = new System.Drawing.Point(3, 44);
            this.bnt_ban_trai.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_ban_trai.Name = "bnt_ban_trai";
            this.bnt_ban_trai.Size = new System.Drawing.Size(110, 35);
            this.bnt_ban_trai.TabIndex = 33;
            this.bnt_ban_trai.Click += new System.EventHandler(this.bnt_ban_trai_Click);
            this.bnt_ban_trai.MouseDown += new System.Windows.Forms.MouseEventHandler(this.bnt_ban_trai_MouseDown);
            this.bnt_ban_trai.MouseUp += new System.Windows.Forms.MouseEventHandler(this.bnt_ban_trai_MouseUp);
            // 
            // panelControl14
            // 
            this.panelControl14.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl14.Controls.Add(this.btnLuusodoban);
            this.panelControl14.Controls.Add(this.bnt_macdinh);
            this.panelControl14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl14.Location = new System.Drawing.Point(0, 387);
            this.panelControl14.Name = "panelControl14";
            this.panelControl14.Size = new System.Drawing.Size(340, 45);
            this.panelControl14.TabIndex = 54;
            // 
            // btnLuusodoban
            // 
            this.btnLuusodoban.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuusodoban.Appearance.Options.UseFont = true;
            this.btnLuusodoban.Image = global::KP_RES.Properties.Resources.display_26;
            this.btnLuusodoban.Location = new System.Drawing.Point(6, 7);
            this.btnLuusodoban.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuusodoban.Name = "btnLuusodoban";
            this.btnLuusodoban.Size = new System.Drawing.Size(163, 35);
            this.btnLuusodoban.TabIndex = 54;
            this.btnLuusodoban.Text = "Lưu sơ đồ phòng";
            this.btnLuusodoban.Click += new System.EventHandler(this.btnLuusodoban_Click);
            // 
            // bnt_macdinh
            // 
            this.bnt_macdinh.Appearance.BackColor = System.Drawing.SystemColors.Highlight;
            this.bnt_macdinh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_macdinh.Appearance.Options.UseBackColor = true;
            this.bnt_macdinh.Appearance.Options.UseFont = true;
            this.bnt_macdinh.Image = global::KP_RES.Properties.Resources.restart_26;
            this.bnt_macdinh.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.bnt_macdinh.Location = new System.Drawing.Point(172, 7);
            this.bnt_macdinh.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_macdinh.Name = "bnt_macdinh";
            this.bnt_macdinh.Size = new System.Drawing.Size(163, 35);
            this.bnt_macdinh.TabIndex = 39;
            this.bnt_macdinh.Text = "Sắp xếp mặc định";
            this.bnt_macdinh.Click += new System.EventHandler(this.bnt_default_Click);
            // 
            // pal_themnhanh
            // 
            this.pal_themnhanh.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_themnhanh.Controls.Add(this.panelControl10);
            this.pal_themnhanh.Controls.Add(this.btnLuuthemnhanh);
            this.pal_themnhanh.Controls.Add(this.labelControl10);
            this.pal_themnhanh.Controls.Add(this.cbb_kichthuocban);
            this.pal_themnhanh.Controls.Add(this.labelControl11);
            this.pal_themnhanh.Controls.Add(this.labelControl12);
            this.pal_themnhanh.Controls.Add(this.txt_soban);
            this.pal_themnhanh.Controls.Add(this.txt_sogheban);
            this.pal_themnhanh.Dock = System.Windows.Forms.DockStyle.Top;
            this.pal_themnhanh.Location = new System.Drawing.Point(0, 246);
            this.pal_themnhanh.Name = "pal_themnhanh";
            this.pal_themnhanh.Size = new System.Drawing.Size(340, 141);
            this.pal_themnhanh.TabIndex = 53;
            this.pal_themnhanh.Visible = false;
            // 
            // panelControl10
            // 
            this.panelControl10.Controls.Add(this.picAnhban);
            this.panelControl10.Location = new System.Drawing.Point(216, 3);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(118, 90);
            this.panelControl10.TabIndex = 39;
            // 
            // picAnhban
            // 
            this.picAnhban.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            this.picAnhban.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picAnhban.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picAnhban.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picAnhban.Location = new System.Drawing.Point(2, 2);
            this.picAnhban.Name = "picAnhban";
            this.picAnhban.Size = new System.Drawing.Size(114, 86);
            this.picAnhban.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picAnhban.TabIndex = 23;
            this.picAnhban.TabStop = false;
            this.picAnhban.Click += new System.EventHandler(this.picAnhban_Click);
            // 
            // btnLuuthemnhanh
            // 
            this.btnLuuthemnhanh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuthemnhanh.Appearance.Options.UseFont = true;
            this.btnLuuthemnhanh.Image = ((System.Drawing.Image)(resources.GetObject("btnLuuthemnhanh.Image")));
            this.btnLuuthemnhanh.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuuthemnhanh.Location = new System.Drawing.Point(84, 100);
            this.btnLuuthemnhanh.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuuthemnhanh.Name = "btnLuuthemnhanh";
            this.btnLuuthemnhanh.Size = new System.Drawing.Size(80, 35);
            this.btnLuuthemnhanh.TabIndex = 38;
            this.btnLuuthemnhanh.Text = "&Lưu";
            this.btnLuuthemnhanh.Click += new System.EventHandler(this.btnLuuthemnhanh_Click);
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(6, 70);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(74, 19);
            this.labelControl10.TabIndex = 36;
            this.labelControl10.Text = "Kích thước";
            // 
            // cbb_kichthuocban
            // 
            this.cbb_kichthuocban.Location = new System.Drawing.Point(84, 67);
            this.cbb_kichthuocban.Name = "cbb_kichthuocban";
            this.cbb_kichthuocban.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.cbb_kichthuocban.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_kichthuocban.Properties.Appearance.Options.UseFont = true;
            this.cbb_kichthuocban.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbb_kichthuocban.Properties.Items.AddRange(new object[] {
            "Kích thước 1",
            "Kích thước 2",
            "Kích thước 3",
            "Kích thước 4"});
            this.cbb_kichthuocban.Properties.ReadOnly = true;
            this.cbb_kichthuocban.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbb_kichthuocban.Size = new System.Drawing.Size(128, 26);
            this.cbb_kichthuocban.TabIndex = 37;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(6, 6);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(68, 19);
            this.labelControl11.TabIndex = 22;
            this.labelControl11.Text = "Số phòng";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(6, 38);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(64, 19);
            this.labelControl12.TabIndex = 24;
            this.labelControl12.Text = "Số người";
            // 
            // txt_soban
            // 
            this.txt_soban.EnterMoveNextControl = true;
            this.txt_soban.Location = new System.Drawing.Point(84, 3);
            this.txt_soban.Name = "txt_soban";
            this.txt_soban.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_soban.Properties.Appearance.Options.UseFont = true;
            this.txt_soban.Properties.MaxLength = 2;
            this.txt_soban.Size = new System.Drawing.Size(128, 26);
            this.txt_soban.TabIndex = 23;
            // 
            // txt_sogheban
            // 
            this.txt_sogheban.EnterMoveNextControl = true;
            this.txt_sogheban.Location = new System.Drawing.Point(84, 35);
            this.txt_sogheban.Name = "txt_sogheban";
            this.txt_sogheban.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_sogheban.Properties.Appearance.Options.UseFont = true;
            this.txt_sogheban.Properties.MaxLength = 2;
            this.txt_sogheban.Size = new System.Drawing.Size(128, 26);
            this.txt_sogheban.TabIndex = 25;
            // 
            // panel_Sandard
            // 
            this.panel_Sandard.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panel_Sandard.Controls.Add(this.bnt_suaban);
            this.panel_Sandard.Controls.Add(this.bnt_xoaban);
            this.panel_Sandard.Controls.Add(this.bnt_luuban);
            this.panel_Sandard.Controls.Add(this.bnt_themnhanh);
            this.panel_Sandard.Controls.Add(this.bnt_themban);
            this.panel_Sandard.Controls.Add(this.panelControl8);
            this.panel_Sandard.Controls.Add(this.labelControl4);
            this.panel_Sandard.Controls.Add(this.cboKhuvuc);
            this.panel_Sandard.Controls.Add(this.chkSudungban);
            this.panel_Sandard.Controls.Add(this.labelControl6);
            this.panel_Sandard.Controls.Add(this.cbb_kichthuoc);
            this.panel_Sandard.Controls.Add(this.labelControl2);
            this.panel_Sandard.Controls.Add(this.labelControl3);
            this.panel_Sandard.Controls.Add(this.txt_ban);
            this.panel_Sandard.Controls.Add(this.txt_soghe);
            this.panel_Sandard.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_Sandard.Location = new System.Drawing.Point(0, 0);
            this.panel_Sandard.Name = "panel_Sandard";
            this.panel_Sandard.Size = new System.Drawing.Size(340, 246);
            this.panel_Sandard.TabIndex = 0;
            // 
            // bnt_suaban
            // 
            this.bnt_suaban.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_suaban.Appearance.Options.UseFont = true;
            this.bnt_suaban.Image = ((System.Drawing.Image)(resources.GetObject("bnt_suaban.Image")));
            this.bnt_suaban.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.bnt_suaban.Location = new System.Drawing.Point(172, 165);
            this.bnt_suaban.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_suaban.Name = "bnt_suaban";
            this.bnt_suaban.Size = new System.Drawing.Size(80, 35);
            this.bnt_suaban.TabIndex = 11;
            this.bnt_suaban.Text = "&Sửa";
            this.bnt_suaban.Click += new System.EventHandler(this.bnt_suaban_Click);
            // 
            // bnt_xoaban
            // 
            this.bnt_xoaban.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_xoaban.Appearance.Options.UseFont = true;
            this.bnt_xoaban.Image = ((System.Drawing.Image)(resources.GetObject("bnt_xoaban.Image")));
            this.bnt_xoaban.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.bnt_xoaban.Location = new System.Drawing.Point(255, 165);
            this.bnt_xoaban.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_xoaban.Name = "bnt_xoaban";
            this.bnt_xoaban.Size = new System.Drawing.Size(80, 35);
            this.bnt_xoaban.TabIndex = 12;
            this.bnt_xoaban.Text = "&Xóa";
            this.bnt_xoaban.Click += new System.EventHandler(this.bnt_xoaban_Click);
            // 
            // bnt_luuban
            // 
            this.bnt_luuban.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_luuban.Appearance.Options.UseFont = true;
            this.bnt_luuban.Image = ((System.Drawing.Image)(resources.GetObject("bnt_luuban.Image")));
            this.bnt_luuban.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.bnt_luuban.Location = new System.Drawing.Point(6, 165);
            this.bnt_luuban.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_luuban.Name = "bnt_luuban";
            this.bnt_luuban.Size = new System.Drawing.Size(80, 35);
            this.bnt_luuban.TabIndex = 9;
            this.bnt_luuban.Text = "&Lưu";
            this.bnt_luuban.Click += new System.EventHandler(this.bnt_luuban_Click);
            // 
            // bnt_themnhanh
            // 
            this.bnt_themnhanh.Appearance.BackColor = System.Drawing.SystemColors.Highlight;
            this.bnt_themnhanh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_themnhanh.Appearance.Options.UseBackColor = true;
            this.bnt_themnhanh.Appearance.Options.UseFont = true;
            this.bnt_themnhanh.Image = global::KP_RES.Properties.Resources.add_list_26;
            this.bnt_themnhanh.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.bnt_themnhanh.Location = new System.Drawing.Point(6, 208);
            this.bnt_themnhanh.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_themnhanh.Name = "bnt_themnhanh";
            this.bnt_themnhanh.Size = new System.Drawing.Size(329, 35);
            this.bnt_themnhanh.TabIndex = 40;
            this.bnt_themnhanh.Text = "Thêm nhiều phòng";
            this.bnt_themnhanh.Click += new System.EventHandler(this.bnt_themnhanh_Click);
            // 
            // bnt_themban
            // 
            this.bnt_themban.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_themban.Appearance.Options.UseFont = true;
            this.bnt_themban.Image = ((System.Drawing.Image)(resources.GetObject("bnt_themban.Image")));
            this.bnt_themban.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.bnt_themban.Location = new System.Drawing.Point(89, 165);
            this.bnt_themban.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_themban.Name = "bnt_themban";
            this.bnt_themban.Size = new System.Drawing.Size(80, 35);
            this.bnt_themban.TabIndex = 10;
            this.bnt_themban.Text = "&Thêm";
            this.bnt_themban.Click += new System.EventHandler(this.bnt_themban_Click);
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.pic_hinhnen);
            this.panelControl8.Location = new System.Drawing.Point(216, 38);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(118, 90);
            this.panelControl8.TabIndex = 13;
            // 
            // pic_hinhnen
            // 
            this.pic_hinhnen.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            this.pic_hinhnen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pic_hinhnen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pic_hinhnen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic_hinhnen.Location = new System.Drawing.Point(2, 2);
            this.pic_hinhnen.Name = "pic_hinhnen";
            this.pic_hinhnen.Size = new System.Drawing.Size(114, 86);
            this.pic_hinhnen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pic_hinhnen.TabIndex = 23;
            this.pic_hinhnen.TabStop = false;
            this.pic_hinhnen.Click += new System.EventHandler(this.pic_hinhnen_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(6, 9);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(57, 19);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "Khu vực";
            // 
            // cboKhuvuc
            // 
            this.cboKhuvuc.EnterMoveNextControl = true;
            this.cboKhuvuc.Location = new System.Drawing.Point(84, 6);
            this.cboKhuvuc.Name = "cboKhuvuc";
            this.cboKhuvuc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboKhuvuc.Properties.Appearance.Options.UseFont = true;
            this.cboKhuvuc.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboKhuvuc.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboKhuvuc.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboKhuvuc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboKhuvuc.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tiền tệ", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboKhuvuc.Properties.DisplayMember = "TEN";
            this.cboKhuvuc.Properties.DropDownItemHeight = 40;
            this.cboKhuvuc.Properties.NullText = "";
            this.cboKhuvuc.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboKhuvuc.Properties.ShowHeader = false;
            this.cboKhuvuc.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboKhuvuc.Properties.ValueMember = "MA";
            this.cboKhuvuc.Size = new System.Drawing.Size(250, 26);
            this.cboKhuvuc.TabIndex = 1;
            // 
            // chkSudungban
            // 
            this.chkSudungban.EditValue = true;
            this.chkSudungban.EnterMoveNextControl = true;
            this.chkSudungban.Location = new System.Drawing.Point(82, 134);
            this.chkSudungban.Name = "chkSudungban";
            this.chkSudungban.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSudungban.Properties.Appearance.Options.UseFont = true;
            this.chkSudungban.Properties.Caption = "Sử dụng";
            this.chkSudungban.Size = new System.Drawing.Size(104, 24);
            this.chkSudungban.TabIndex = 8;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(6, 105);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(74, 19);
            this.labelControl6.TabIndex = 6;
            this.labelControl6.Text = "Kích thước";
            // 
            // cbb_kichthuoc
            // 
            this.cbb_kichthuoc.EnterMoveNextControl = true;
            this.cbb_kichthuoc.Location = new System.Drawing.Point(84, 102);
            this.cbb_kichthuoc.Name = "cbb_kichthuoc";
            this.cbb_kichthuoc.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.cbb_kichthuoc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_kichthuoc.Properties.Appearance.Options.UseFont = true;
            this.cbb_kichthuoc.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cbb_kichthuoc.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbb_kichthuoc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbb_kichthuoc.Properties.DropDownItemHeight = 40;
            this.cbb_kichthuoc.Properties.Items.AddRange(new object[] {
            "Khổ 1",
            "Khổ 2",
            "Khổ 3",
            "Khổ 4"});
            this.cbb_kichthuoc.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbb_kichthuoc.Size = new System.Drawing.Size(128, 26);
            this.cbb_kichthuoc.TabIndex = 7;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(6, 41);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(77, 19);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Tên phòng";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(6, 73);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(64, 19);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Số người";
            // 
            // txt_ban
            // 
            this.txt_ban.EnterMoveNextControl = true;
            this.txt_ban.Location = new System.Drawing.Point(84, 38);
            this.txt_ban.Name = "txt_ban";
            this.txt_ban.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ban.Properties.Appearance.Options.UseFont = true;
            this.txt_ban.Size = new System.Drawing.Size(128, 26);
            this.txt_ban.TabIndex = 3;
            // 
            // txt_soghe
            // 
            this.txt_soghe.EnterMoveNextControl = true;
            this.txt_soghe.Location = new System.Drawing.Point(84, 70);
            this.txt_soghe.Name = "txt_soghe";
            this.txt_soghe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_soghe.Properties.Appearance.Options.UseFont = true;
            this.txt_soghe.Properties.Mask.EditMask = "d";
            this.txt_soghe.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt_soghe.Properties.MaxLength = 3;
            this.txt_soghe.Size = new System.Drawing.Size(128, 26);
            this.txt_soghe.TabIndex = 5;
            // 
            // tab_khuvuc
            // 
            this.tab_khuvuc.Appearance.PageClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.tab_khuvuc.Appearance.PageClient.Options.UseBackColor = true;
            this.tab_khuvuc.Controls.Add(this.panelControl16);
            this.tab_khuvuc.Name = "tab_khuvuc";
            this.tab_khuvuc.Size = new System.Drawing.Size(340, 573);
            this.tab_khuvuc.Text = "xtraTabPage1";
            // 
            // panelControl16
            // 
            this.panelControl16.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.panelControl16.Appearance.Options.UseBackColor = true;
            this.panelControl16.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl16.Controls.Add(this.panelControl15);
            this.panelControl16.Controls.Add(this.cboCUAHANG);
            this.panelControl16.Controls.Add(this.btnSuaKhuvuc);
            this.panelControl16.Controls.Add(this.lblGHICHU);
            this.panelControl16.Controls.Add(this.chkSUDUNG);
            this.panelControl16.Controls.Add(this.txt_ghichu);
            this.panelControl16.Controls.Add(this.labelControl7);
            this.panelControl16.Controls.Add(this.btnThemKhuvuc);
            this.panelControl16.Controls.Add(this.txtSTT);
            this.panelControl16.Controls.Add(this.txt_khuvuc);
            this.panelControl16.Controls.Add(this.labelControl5);
            this.panelControl16.Controls.Add(this.btnLuuKhuvuc);
            this.panelControl16.Controls.Add(this.btnXoaKhuvuc);
            this.panelControl16.Controls.Add(this.labelControl1);
            this.panelControl16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl16.Location = new System.Drawing.Point(0, 0);
            this.panelControl16.Name = "panelControl16";
            this.panelControl16.Size = new System.Drawing.Size(340, 573);
            this.panelControl16.TabIndex = 15;
            // 
            // panelControl15
            // 
            this.panelControl15.Controls.Add(this.picKhuvuc);
            this.panelControl15.Location = new System.Drawing.Point(217, 38);
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(118, 90);
            this.panelControl15.TabIndex = 14;
            // 
            // picKhuvuc
            // 
            this.picKhuvuc.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            this.picKhuvuc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picKhuvuc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picKhuvuc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picKhuvuc.Location = new System.Drawing.Point(2, 2);
            this.picKhuvuc.Name = "picKhuvuc";
            this.picKhuvuc.Size = new System.Drawing.Size(114, 86);
            this.picKhuvuc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picKhuvuc.TabIndex = 23;
            this.picKhuvuc.TabStop = false;
            this.picKhuvuc.Click += new System.EventHandler(this.picKhuvuc_Click);
            // 
            // cboCUAHANG
            // 
            this.cboCUAHANG.EnterMoveNextControl = true;
            this.cboCUAHANG.Location = new System.Drawing.Point(80, 6);
            this.cboCUAHANG.Name = "cboCUAHANG";
            this.cboCUAHANG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCUAHANG.Properties.Appearance.Options.UseFont = true;
            this.cboCUAHANG.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCUAHANG.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboCUAHANG.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboCUAHANG.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboCUAHANG.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tiền tệ", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboCUAHANG.Properties.DisplayMember = "TEN";
            this.cboCUAHANG.Properties.DropDownItemHeight = 40;
            this.cboCUAHANG.Properties.NullText = "";
            this.cboCUAHANG.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboCUAHANG.Properties.ShowHeader = false;
            this.cboCUAHANG.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboCUAHANG.Properties.ValueMember = "MA";
            this.cboCUAHANG.Size = new System.Drawing.Size(255, 26);
            this.cboCUAHANG.TabIndex = 1;
            // 
            // btnSuaKhuvuc
            // 
            this.btnSuaKhuvuc.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaKhuvuc.Appearance.Options.UseFont = true;
            this.btnSuaKhuvuc.Image = ((System.Drawing.Image)(resources.GetObject("btnSuaKhuvuc.Image")));
            this.btnSuaKhuvuc.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSuaKhuvuc.Location = new System.Drawing.Point(173, 165);
            this.btnSuaKhuvuc.Margin = new System.Windows.Forms.Padding(4);
            this.btnSuaKhuvuc.Name = "btnSuaKhuvuc";
            this.btnSuaKhuvuc.Size = new System.Drawing.Size(80, 35);
            this.btnSuaKhuvuc.TabIndex = 11;
            this.btnSuaKhuvuc.Text = "&Sửa";
            this.btnSuaKhuvuc.Click += new System.EventHandler(this.btnSuaKhuvuc_Click);
            // 
            // lblGHICHU
            // 
            this.lblGHICHU.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGHICHU.Location = new System.Drawing.Point(7, 41);
            this.lblGHICHU.Margin = new System.Windows.Forms.Padding(4);
            this.lblGHICHU.Name = "lblGHICHU";
            this.lblGHICHU.Size = new System.Drawing.Size(27, 19);
            this.lblGHICHU.TabIndex = 2;
            this.lblGHICHU.Text = "Tên";
            // 
            // chkSUDUNG
            // 
            this.chkSUDUNG.EditValue = true;
            this.chkSUDUNG.EnterMoveNextControl = true;
            this.chkSUDUNG.Location = new System.Drawing.Point(78, 134);
            this.chkSUDUNG.Name = "chkSUDUNG";
            this.chkSUDUNG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNG.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNG.Properties.Caption = "Sử dụng";
            this.chkSUDUNG.Size = new System.Drawing.Size(99, 24);
            this.chkSUDUNG.TabIndex = 8;
            // 
            // txt_ghichu
            // 
            this.txt_ghichu.EnterMoveNextControl = true;
            this.txt_ghichu.Location = new System.Drawing.Point(80, 102);
            this.txt_ghichu.Name = "txt_ghichu";
            this.txt_ghichu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ghichu.Properties.Appearance.Options.UseFont = true;
            this.txt_ghichu.Size = new System.Drawing.Size(133, 26);
            this.txt_ghichu.TabIndex = 7;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(7, 73);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(29, 19);
            this.labelControl7.TabIndex = 4;
            this.labelControl7.Text = "STT";
            // 
            // btnThemKhuvuc
            // 
            this.btnThemKhuvuc.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemKhuvuc.Appearance.Options.UseFont = true;
            this.btnThemKhuvuc.Image = ((System.Drawing.Image)(resources.GetObject("btnThemKhuvuc.Image")));
            this.btnThemKhuvuc.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThemKhuvuc.Location = new System.Drawing.Point(90, 165);
            this.btnThemKhuvuc.Margin = new System.Windows.Forms.Padding(4);
            this.btnThemKhuvuc.Name = "btnThemKhuvuc";
            this.btnThemKhuvuc.Size = new System.Drawing.Size(80, 35);
            this.btnThemKhuvuc.TabIndex = 10;
            this.btnThemKhuvuc.Text = "&Thêm";
            this.btnThemKhuvuc.Click += new System.EventHandler(this.btnThemKhuvuc_Click);
            // 
            // txtSTT
            // 
            this.txtSTT.EnterMoveNextControl = true;
            this.txtSTT.Location = new System.Drawing.Point(80, 70);
            this.txtSTT.Name = "txtSTT";
            this.txtSTT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSTT.Properties.Appearance.Options.UseFont = true;
            this.txtSTT.Properties.Mask.EditMask = "d";
            this.txtSTT.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSTT.Size = new System.Drawing.Size(133, 26);
            this.txtSTT.TabIndex = 5;
            // 
            // txt_khuvuc
            // 
            this.txt_khuvuc.EnterMoveNextControl = true;
            this.txt_khuvuc.Location = new System.Drawing.Point(80, 38);
            this.txt_khuvuc.Name = "txt_khuvuc";
            this.txt_khuvuc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_khuvuc.Properties.Appearance.Options.UseFont = true;
            this.txt_khuvuc.Size = new System.Drawing.Size(133, 26);
            this.txt_khuvuc.TabIndex = 3;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(7, 9);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(68, 19);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "Cửa hàng";
            // 
            // btnLuuKhuvuc
            // 
            this.btnLuuKhuvuc.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuKhuvuc.Appearance.Options.UseFont = true;
            this.btnLuuKhuvuc.Image = ((System.Drawing.Image)(resources.GetObject("btnLuuKhuvuc.Image")));
            this.btnLuuKhuvuc.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuuKhuvuc.Location = new System.Drawing.Point(7, 165);
            this.btnLuuKhuvuc.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuuKhuvuc.Name = "btnLuuKhuvuc";
            this.btnLuuKhuvuc.Size = new System.Drawing.Size(80, 35);
            this.btnLuuKhuvuc.TabIndex = 9;
            this.btnLuuKhuvuc.Text = "&Lưu";
            this.btnLuuKhuvuc.Click += new System.EventHandler(this.btnLuuKhuvuc_Click);
            // 
            // btnXoaKhuvuc
            // 
            this.btnXoaKhuvuc.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaKhuvuc.Appearance.Options.UseFont = true;
            this.btnXoaKhuvuc.Image = ((System.Drawing.Image)(resources.GetObject("btnXoaKhuvuc.Image")));
            this.btnXoaKhuvuc.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoaKhuvuc.Location = new System.Drawing.Point(256, 165);
            this.btnXoaKhuvuc.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoaKhuvuc.Name = "btnXoaKhuvuc";
            this.btnXoaKhuvuc.Size = new System.Drawing.Size(80, 35);
            this.btnXoaKhuvuc.TabIndex = 12;
            this.btnXoaKhuvuc.Text = "&Xóa";
            this.btnXoaKhuvuc.Click += new System.EventHandler(this.btnXoaKhuvuc_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(7, 105);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(54, 19);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "Ghi chú";
            // 
            // panelControl18
            // 
            this.panelControl18.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl18.Controls.Add(this.pal_cuahang);
            this.panelControl18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl18.Location = new System.Drawing.Point(2, 37);
            this.panelControl18.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl18.Name = "panelControl18";
            this.panelControl18.Size = new System.Drawing.Size(346, 88);
            this.panelControl18.TabIndex = 21;
            // 
            // pal_cuahang
            // 
            this.pal_cuahang.AutoScroll = true;
            this.pal_cuahang.BackColor = System.Drawing.Color.Transparent;
            this.pal_cuahang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pal_cuahang.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pal_cuahang.Location = new System.Drawing.Point(0, 0);
            this.pal_cuahang.Margin = new System.Windows.Forms.Padding(0);
            this.pal_cuahang.Name = "pal_cuahang";
            this.pal_cuahang.Size = new System.Drawing.Size(346, 88);
            this.pal_cuahang.TabIndex = 30;
            // 
            // panelControl12
            // 
            this.panelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl12.Controls.Add(this.btnChonKhuvuc);
            this.panelControl12.Controls.Add(this.btnChonBan);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl12.Location = new System.Drawing.Point(2, 2);
            this.panelControl12.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(346, 35);
            this.panelControl12.TabIndex = 19;
            // 
            // btnChonKhuvuc
            // 
            this.btnChonKhuvuc.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonKhuvuc.Appearance.Options.UseFont = true;
            this.btnChonKhuvuc.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnChonKhuvuc.Image = global::KP_RES.Properties.Resources.control_panel_26;
            this.btnChonKhuvuc.Location = new System.Drawing.Point(0, 0);
            this.btnChonKhuvuc.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonKhuvuc.Name = "btnChonKhuvuc";
            this.btnChonKhuvuc.Size = new System.Drawing.Size(173, 35);
            this.btnChonKhuvuc.TabIndex = 1;
            this.btnChonKhuvuc.Text = "Khu vực";
            this.btnChonKhuvuc.Click += new System.EventHandler(this.btnChonKhuvuc_Click);
            // 
            // btnChonBan
            // 
            this.btnChonBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonBan.Appearance.Options.UseFont = true;
            this.btnChonBan.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnChonBan.Image = global::KP_RES.Properties.Resources.large_icons_26;
            this.btnChonBan.Location = new System.Drawing.Point(173, 0);
            this.btnChonBan.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonBan.Name = "btnChonBan";
            this.btnChonBan.Size = new System.Drawing.Size(173, 35);
            this.btnChonBan.TabIndex = 10;
            this.btnChonBan.Text = "Phòng";
            this.btnChonBan.Click += new System.EventHandler(this.btnChonBan_Click);
            // 
            // pal_BUTTON
            // 
            this.pal_BUTTON.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_BUTTON.Controls.Add(this.btnCapnhat);
            this.pal_BUTTON.Controls.Add(this.btnBanphim);
            this.pal_BUTTON.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pal_BUTTON.Location = new System.Drawing.Point(2, 704);
            this.pal_BUTTON.Name = "pal_BUTTON";
            this.pal_BUTTON.Size = new System.Drawing.Size(346, 35);
            this.pal_BUTTON.TabIndex = 2;
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(0, 0);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(4);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(173, 35);
            this.btnCapnhat.TabIndex = 33;
            this.btnCapnhat.Click += new System.EventHandler(this.bnt_capnhat_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnBanphim.Appearance.Options.UseBackColor = true;
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Appearance.Options.UseForeColor = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(173, 0);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(4);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(173, 35);
            this.btnBanphim.TabIndex = 32;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // timer3
            // 
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // timer4
            // 
            this.timer4.Tick += new System.EventHandler(this.timer4_Tick);
            // 
            // Frm_KhuvucPhongKaraoke
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 741);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_KhuvucPhongKaraoke";
            this.Text = "Khu vực - Phòng";
            this.Load += new System.EventHandler(this.Frm_KhuvucBan_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_KhuvucBan_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_KhuvucBan_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_1)).EndInit();
            this.pal_1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_sotrang)).EndInit();
            this.pal_sotrang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panel)).EndInit();
            this.panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_len)).EndInit();
            this.pal_len.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnRight)).EndInit();
            this.pnRight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tab_Option)).EndInit();
            this.tab_Option.ResumeLayout(false);
            this.tab_ban.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).EndInit();
            this.panelControl13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).EndInit();
            this.panelControl14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_themnhanh)).EndInit();
            this.pal_themnhanh.ResumeLayout(false);
            this.pal_themnhanh.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picAnhban)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbb_kichthuocban.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soban.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_sogheban.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel_Sandard)).EndInit();
            this.panel_Sandard.ResumeLayout(false);
            this.panel_Sandard.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_hinhnen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboKhuvuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSudungban.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbb_kichthuoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ban.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soghe.Properties)).EndInit();
            this.tab_khuvuc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).EndInit();
            this.panelControl16.ResumeLayout(false);
            this.panelControl16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            this.panelControl15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picKhuvuc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCUAHANG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ghichu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_khuvuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).EndInit();
            this.panelControl18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pal_BUTTON)).EndInit();
            this.pal_BUTTON.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl pal_1;
        private DevExpress.XtraEditors.PanelControl pal_BUTTON;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.PanelControl pnRight;
        private DevExpress.XtraEditors.PanelControl panel;
        private DevExpress.XtraEditors.PanelControl pal_len;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraEditors.SimpleButton bnt_Len;
        private DevExpress.XtraEditors.SimpleButton bnt_Xuong;
        private DevExpress.XtraEditors.LabelControl lblGHICHU;
        private DevExpress.XtraEditors.TextEdit txt_khuvuc;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txt_ghichu;
        private DevExpress.XtraEditors.CheckEdit chkSudungban;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txt_soghe;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txt_ban;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNG;
        private DevExpress.XtraEditors.LookUpEdit cboCUAHANG;
        private DevExpress.XtraEditors.SimpleButton bnt_ban_xuong;
        private DevExpress.XtraEditors.SimpleButton bnt_ban_phai;
        private DevExpress.XtraEditors.SimpleButton bnt_ban_trai;
        private DevExpress.XtraEditors.SimpleButton bnt_ban_len;
        private DevExpress.XtraEditors.ComboBoxEdit cbb_kichthuoc;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.SimpleButton bnt_macdinh;
        private DevExpress.XtraEditors.SimpleButton bnt_themnhanh;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Timer timer4;
        private DevExpress.XtraEditors.SimpleButton btnLuuKhuvuc;
        private DevExpress.XtraEditors.SimpleButton btnXoaKhuvuc;
        private DevExpress.XtraEditors.SimpleButton btnThemKhuvuc;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl pal_themnhanh;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.ComboBoxEdit cbb_kichthuocban;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txt_soban;
        private DevExpress.XtraEditors.TextEdit txt_sogheban;
        private DevExpress.XtraEditors.PanelControl panel_Sandard;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.SimpleButton btnChonKhuvuc;
        private DevExpress.XtraEditors.SimpleButton btnChonBan;
        private DevExpress.XtraEditors.PanelControl pal_sotrang;
        private DevExpress.XtraEditors.SimpleButton bnt_tranglen;
        private DevExpress.XtraEditors.SimpleButton lbl_sotrang;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton bnt_trangxuong;
        private DevExpress.XtraEditors.SimpleButton lbl_sotrang1;
        private DevExpress.XtraEditors.SimpleButton btnSuaKhuvuc;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtSTT;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LookUpEdit cboKhuvuc;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private System.Windows.Forms.PictureBox pic_hinhnen;
        private DevExpress.XtraEditors.SimpleButton bnt_suaban;
        private DevExpress.XtraEditors.SimpleButton bnt_xoaban;
        private DevExpress.XtraEditors.SimpleButton bnt_luuban;
        private DevExpress.XtraEditors.SimpleButton bnt_themban;
        private System.Windows.Forms.FlowLayoutPanel pal_khuvuc;
        private System.Windows.Forms.Panel pal_ban;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.SimpleButton btnLuusodoban;
        private DevExpress.XtraEditors.SimpleButton btnLuuthemnhanh;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private System.Windows.Forms.PictureBox picAnhban;
        private DevExpress.XtraEditors.PanelControl panelControl13;
        private DevExpress.XtraEditors.PanelControl panelControl14;
        private DevExpress.XtraEditors.PanelControl panelControl15;
        private System.Windows.Forms.PictureBox picKhuvuc;
        private DevExpress.XtraEditors.PanelControl panelControl16;
        private DevExpress.XtraEditors.PanelControl panelControl17;
        private DevExpress.XtraTab.XtraTabControl tab_Option;
        private DevExpress.XtraTab.XtraTabPage tab_ban;
        private DevExpress.XtraTab.XtraTabPage tab_khuvuc;
        private DevExpress.XtraEditors.PanelControl panelControl18;
        private System.Windows.Forms.FlowLayoutPanel pal_cuahang;




    }
}