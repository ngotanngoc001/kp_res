﻿namespace KP_RES
{
    partial class Frm_BCChitietHoaDon_Karaoke
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STTHH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_DONVITINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DONGIA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THANHTIENTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIENTHUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGCONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txt_mahanghoa = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txt_soluong = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txt_gianhap = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txt_chietkhau = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txtvat = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txt_soserial = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.cmb_tenhanghoa = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.header = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.title = new DevExpress.XtraEditors.LabelControl();
            this.pnlHome = new DevExpress.XtraEditors.PanelControl();
            this.home = new System.Windows.Forms.PictureBox();
            this.pnTiengiokaraoke = new DevExpress.XtraEditors.PanelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYTAO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HOADON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENPHONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THOIGIANVAO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THOIGIANRA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BLOCKKARAOKE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOPHUT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOBLOCK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIABLOCK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THANHTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnHanghoa = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_mahanghoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soluong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_gianhap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_chietkhau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soserial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmb_tenhanghoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.header)).BeginInit();
            this.header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHome)).BeginInit();
            this.pnlHome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.home)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnTiengiokaraoke)).BeginInit();
            this.pnTiengiokaraoke.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnHanghoa)).BeginInit();
            this.pnHanghoa.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.txt_mahanghoa,
            this.txt_soluong,
            this.txt_gianhap,
            this.txt_chietkhau,
            this.txtvat,
            this.txt_soserial,
            this.cmb_tenhanghoa});
            this.gridControl1.Size = new System.Drawing.Size(1185, 240);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STTHH,
            this.MA,
            this.TEN_HANGHOA,
            this.TEN_DONVITINH,
            this.SOLUONG,
            this.DONGIA,
            this.THANHTIENTT,
            this.THUE,
            this.TIENTHUE,
            this.TONGCONG});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.ShowCloseButton = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // STTHH
            // 
            this.STTHH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STTHH.AppearanceCell.Options.UseFont = true;
            this.STTHH.AppearanceCell.Options.UseTextOptions = true;
            this.STTHH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTHH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STTHH.AppearanceHeader.Options.UseFont = true;
            this.STTHH.AppearanceHeader.Options.UseTextOptions = true;
            this.STTHH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTHH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STTHH.Caption = "STT";
            this.STTHH.FieldName = "STTHH";
            this.STTHH.Name = "STTHH";
            this.STTHH.OptionsColumn.AllowEdit = false;
            this.STTHH.OptionsColumn.AllowFocus = false;
            this.STTHH.OptionsColumn.FixedWidth = true;
            this.STTHH.Visible = true;
            this.STTHH.VisibleIndex = 0;
            this.STTHH.Width = 60;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA.AppearanceCell.Options.UseFont = true;
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.Caption = "Mã";
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Visible = true;
            this.MA.VisibleIndex = 1;
            this.MA.Width = 80;
            // 
            // TEN_HANGHOA
            // 
            this.TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_HANGHOA.Caption = "Tên Hàng Hóa";
            this.TEN_HANGHOA.FieldName = "TEN_HANGHOA";
            this.TEN_HANGHOA.Name = "TEN_HANGHOA";
            this.TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this.TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this.TEN_HANGHOA.OptionsColumn.FixedWidth = true;
            this.TEN_HANGHOA.Visible = true;
            this.TEN_HANGHOA.VisibleIndex = 2;
            this.TEN_HANGHOA.Width = 400;
            // 
            // TEN_DONVITINH
            // 
            this.TEN_DONVITINH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_DONVITINH.AppearanceCell.Options.UseFont = true;
            this.TEN_DONVITINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_DONVITINH.AppearanceHeader.Options.UseFont = true;
            this.TEN_DONVITINH.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_DONVITINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_DONVITINH.Caption = "ĐVT";
            this.TEN_DONVITINH.FieldName = "TEN_DONVITINH";
            this.TEN_DONVITINH.Name = "TEN_DONVITINH";
            this.TEN_DONVITINH.OptionsColumn.AllowEdit = false;
            this.TEN_DONVITINH.OptionsColumn.AllowFocus = false;
            this.TEN_DONVITINH.OptionsColumn.FixedWidth = true;
            this.TEN_DONVITINH.Visible = true;
            this.TEN_DONVITINH.VisibleIndex = 3;
            this.TEN_DONVITINH.Width = 86;
            // 
            // SOLUONG
            // 
            this.SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOLUONG.AppearanceCell.Options.UseFont = true;
            this.SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.Caption = "SL";
            this.SOLUONG.DisplayFormat.FormatString = "{0:#,###0}";
            this.SOLUONG.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.SOLUONG.FieldName = "SOLUONG";
            this.SOLUONG.Name = "SOLUONG";
            this.SOLUONG.OptionsColumn.AllowEdit = false;
            this.SOLUONG.OptionsColumn.AllowFocus = false;
            this.SOLUONG.OptionsColumn.FixedWidth = true;
            this.SOLUONG.Visible = true;
            this.SOLUONG.VisibleIndex = 4;
            this.SOLUONG.Width = 64;
            // 
            // DONGIA
            // 
            this.DONGIA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.DONGIA.AppearanceCell.Options.UseFont = true;
            this.DONGIA.AppearanceCell.Options.UseTextOptions = true;
            this.DONGIA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.DONGIA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DONGIA.AppearanceHeader.Options.UseFont = true;
            this.DONGIA.AppearanceHeader.Options.UseTextOptions = true;
            this.DONGIA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DONGIA.Caption = "Đơn giá";
            this.DONGIA.DisplayFormat.FormatString = "{0:#,###0}";
            this.DONGIA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.DONGIA.FieldName = "DONGIA";
            this.DONGIA.Name = "DONGIA";
            this.DONGIA.OptionsColumn.AllowEdit = false;
            this.DONGIA.OptionsColumn.AllowFocus = false;
            this.DONGIA.OptionsColumn.FixedWidth = true;
            this.DONGIA.Visible = true;
            this.DONGIA.VisibleIndex = 5;
            this.DONGIA.Width = 112;
            // 
            // THANHTIENTT
            // 
            this.THANHTIENTT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.THANHTIENTT.AppearanceCell.Options.UseFont = true;
            this.THANHTIENTT.AppearanceCell.Options.UseTextOptions = true;
            this.THANHTIENTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.THANHTIENTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THANHTIENTT.AppearanceHeader.Options.UseFont = true;
            this.THANHTIENTT.AppearanceHeader.Options.UseTextOptions = true;
            this.THANHTIENTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THANHTIENTT.Caption = "Thành tiền";
            this.THANHTIENTT.DisplayFormat.FormatString = "{0:#,###0.00}";
            this.THANHTIENTT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.THANHTIENTT.FieldName = "THANHTIENTT";
            this.THANHTIENTT.Name = "THANHTIENTT";
            this.THANHTIENTT.OptionsColumn.AllowEdit = false;
            this.THANHTIENTT.OptionsColumn.AllowFocus = false;
            this.THANHTIENTT.OptionsColumn.FixedWidth = true;
            this.THANHTIENTT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "THANHTIENTT", "{0:#,###0.00}")});
            this.THANHTIENTT.Visible = true;
            this.THANHTIENTT.VisibleIndex = 6;
            this.THANHTIENTT.Width = 116;
            // 
            // THUE
            // 
            this.THUE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.THUE.AppearanceCell.Options.UseFont = true;
            this.THUE.AppearanceCell.Options.UseTextOptions = true;
            this.THUE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.THUE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THUE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.THUE.AppearanceHeader.Options.UseFont = true;
            this.THUE.AppearanceHeader.Options.UseTextOptions = true;
            this.THUE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THUE.Caption = "VAT";
            this.THUE.DisplayFormat.FormatString = "{0:#,###0}%";
            this.THUE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.THUE.FieldName = "THUE";
            this.THUE.Name = "THUE";
            this.THUE.OptionsColumn.AllowEdit = false;
            this.THUE.OptionsColumn.AllowFocus = false;
            this.THUE.OptionsColumn.FixedWidth = true;
            this.THUE.Visible = true;
            this.THUE.VisibleIndex = 7;
            this.THUE.Width = 60;
            // 
            // TIENTHUE
            // 
            this.TIENTHUE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TIENTHUE.AppearanceCell.Options.UseFont = true;
            this.TIENTHUE.AppearanceCell.Options.UseTextOptions = true;
            this.TIENTHUE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIENTHUE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIENTHUE.AppearanceHeader.Options.UseFont = true;
            this.TIENTHUE.AppearanceHeader.Options.UseTextOptions = true;
            this.TIENTHUE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIENTHUE.Caption = "Tiền VAT";
            this.TIENTHUE.DisplayFormat.FormatString = "{0:#,###0.00}";
            this.TIENTHUE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TIENTHUE.FieldName = "TIENTHUE";
            this.TIENTHUE.Name = "TIENTHUE";
            this.TIENTHUE.OptionsColumn.AllowEdit = false;
            this.TIENTHUE.OptionsColumn.AllowFocus = false;
            this.TIENTHUE.OptionsColumn.FixedWidth = true;
            this.TIENTHUE.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TIENTHUE", "{0:#,###0.00}")});
            this.TIENTHUE.Visible = true;
            this.TIENTHUE.VisibleIndex = 8;
            this.TIENTHUE.Width = 150;
            // 
            // TONGCONG
            // 
            this.TONGCONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TONGCONG.AppearanceCell.Options.UseFont = true;
            this.TONGCONG.AppearanceCell.Options.UseTextOptions = true;
            this.TONGCONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TONGCONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TONGCONG.AppearanceHeader.Options.UseFont = true;
            this.TONGCONG.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGCONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGCONG.Caption = "Tổng cộng";
            this.TONGCONG.DisplayFormat.FormatString = "{0:#,###0.00}";
            this.TONGCONG.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TONGCONG.FieldName = "TONGCONG";
            this.TONGCONG.MinWidth = 150;
            this.TONGCONG.Name = "TONGCONG";
            this.TONGCONG.OptionsColumn.AllowEdit = false;
            this.TONGCONG.OptionsColumn.AllowFocus = false;
            this.TONGCONG.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TONGCONG", "{0:#,###0.00}")});
            this.TONGCONG.Visible = true;
            this.TONGCONG.VisibleIndex = 9;
            this.TONGCONG.Width = 150;
            // 
            // txt_mahanghoa
            // 
            this.txt_mahanghoa.AutoHeight = false;
            this.txt_mahanghoa.Name = "txt_mahanghoa";
            // 
            // txt_soluong
            // 
            this.txt_soluong.AutoHeight = false;
            this.txt_soluong.Name = "txt_soluong";
            // 
            // txt_gianhap
            // 
            this.txt_gianhap.AutoHeight = false;
            this.txt_gianhap.Name = "txt_gianhap";
            // 
            // txt_chietkhau
            // 
            this.txt_chietkhau.AutoHeight = false;
            this.txt_chietkhau.Name = "txt_chietkhau";
            // 
            // txtvat
            // 
            this.txtvat.AutoHeight = false;
            this.txtvat.Name = "txtvat";
            // 
            // txt_soserial
            // 
            this.txt_soserial.AutoHeight = false;
            this.txt_soserial.Name = "txt_soserial";
            // 
            // cmb_tenhanghoa
            // 
            this.cmb_tenhanghoa.AutoHeight = false;
            this.cmb_tenhanghoa.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmb_tenhanghoa.Name = "cmb_tenhanghoa";
            // 
            // header
            // 
            this.header.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.header.Controls.Add(this.panelControl4);
            this.header.Controls.Add(this.panelControl2);
            this.header.Controls.Add(this.pnlHome);
            this.header.Dock = System.Windows.Forms.DockStyle.Top;
            this.header.Location = new System.Drawing.Point(0, 0);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(1189, 52);
            this.header.TabIndex = 48;
            // 
            // panelControl4
            // 
            this.panelControl4.AutoSize = true;
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl4.Controls.Add(this.pictureEdit1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(358, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(831, 52);
            this.panelControl4.TabIndex = 7;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit1.EditValue = global::KP_RES.Properties.Resources.KingPos_for_Restaurant;
            this.pictureEdit1.Location = new System.Drawing.Point(2, 2);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.PictureAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit1.Size = new System.Drawing.Size(827, 48);
            this.pictureEdit1.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.AutoSize = true;
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl2.Controls.Add(this.title);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl2.Location = new System.Drawing.Point(81, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(277, 52);
            this.panelControl2.TabIndex = 5;
            // 
            // title
            // 
            this.title.Appearance.Font = new System.Drawing.Font("Segoe UI", 18F);
            this.title.Location = new System.Drawing.Point(5, 10);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(265, 32);
            this.title.TabIndex = 1;
            this.title.Text = "Chi tiết hóa đơn Karaoke";
            // 
            // pnlHome
            // 
            this.pnlHome.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.pnlHome.Controls.Add(this.home);
            this.pnlHome.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlHome.Location = new System.Drawing.Point(0, 0);
            this.pnlHome.Name = "pnlHome";
            this.pnlHome.Size = new System.Drawing.Size(81, 52);
            this.pnlHome.TabIndex = 6;
            this.pnlHome.Click += new System.EventHandler(this.pnlHome_Click);
            // 
            // home
            // 
            this.home.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.home.Image = global::KP_RES.Properties.Resources.ArrowBlack;
            this.home.Location = new System.Drawing.Point(22, 10);
            this.home.Name = "home";
            this.home.Size = new System.Drawing.Size(35, 32);
            this.home.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.home.TabIndex = 0;
            this.home.TabStop = false;
            this.home.Click += new System.EventHandler(this.home_Click);
            // 
            // pnTiengiokaraoke
            // 
            this.pnTiengiokaraoke.Controls.Add(this.gridControl2);
            this.pnTiengiokaraoke.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTiengiokaraoke.Location = new System.Drawing.Point(0, 52);
            this.pnTiengiokaraoke.Name = "pnTiengiokaraoke";
            this.pnTiengiokaraoke.Size = new System.Drawing.Size(1189, 200);
            this.pnTiengiokaraoke.TabIndex = 49;
            // 
            // gridControl2
            // 
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(2, 2);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1185, 196);
            this.gridControl2.TabIndex = 4;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.NGAYTAO,
            this.MA_HOADON,
            this.TENPHONG,
            this.THOIGIANVAO,
            this.THOIGIANRA,
            this.BLOCKKARAOKE,
            this.SOPHUT,
            this.SOBLOCK,
            this.GIABLOCK,
            this.THANHTIEN});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView2.GroupRowHeight = 30;
            this.gridView2.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "THANHTIEN", null, "- Tổng Cộng : {0:#,###0}")});
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsPrint.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 61;
            // 
            // NGAYTAO
            // 
            this.NGAYTAO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYTAO.AppearanceCell.Options.UseFont = true;
            this.NGAYTAO.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYTAO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYTAO.AppearanceHeader.Options.UseFont = true;
            this.NGAYTAO.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NGAYTAO.Caption = "Ngày";
            this.NGAYTAO.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.NGAYTAO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYTAO.FieldName = "NGAYTAO";
            this.NGAYTAO.Name = "NGAYTAO";
            this.NGAYTAO.OptionsColumn.AllowEdit = false;
            this.NGAYTAO.OptionsColumn.AllowFocus = false;
            this.NGAYTAO.OptionsColumn.FixedWidth = true;
            this.NGAYTAO.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.NGAYTAO.Visible = true;
            this.NGAYTAO.VisibleIndex = 1;
            this.NGAYTAO.Width = 153;
            // 
            // MA_HOADON
            // 
            this.MA_HOADON.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MA_HOADON.AppearanceCell.Options.UseFont = true;
            this.MA_HOADON.AppearanceCell.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA_HOADON.AppearanceHeader.Options.UseFont = true;
            this.MA_HOADON.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA_HOADON.Caption = "Mã Hóa Đơn";
            this.MA_HOADON.FieldName = "MA_HOADON";
            this.MA_HOADON.Name = "MA_HOADON";
            this.MA_HOADON.OptionsColumn.AllowEdit = false;
            this.MA_HOADON.OptionsColumn.AllowFocus = false;
            this.MA_HOADON.OptionsColumn.FixedWidth = true;
            this.MA_HOADON.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.MA_HOADON.Width = 145;
            // 
            // TENPHONG
            // 
            this.TENPHONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TENPHONG.AppearanceCell.Options.UseFont = true;
            this.TENPHONG.AppearanceCell.Options.UseTextOptions = true;
            this.TENPHONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENPHONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENPHONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENPHONG.AppearanceHeader.Options.UseFont = true;
            this.TENPHONG.AppearanceHeader.Options.UseTextOptions = true;
            this.TENPHONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENPHONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENPHONG.Caption = "Phòng";
            this.TENPHONG.FieldName = "TENPHONG";
            this.TENPHONG.Name = "TENPHONG";
            this.TENPHONG.OptionsColumn.AllowEdit = false;
            this.TENPHONG.OptionsColumn.AllowFocus = false;
            this.TENPHONG.OptionsColumn.FixedWidth = true;
            this.TENPHONG.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.TENPHONG.Visible = true;
            this.TENPHONG.VisibleIndex = 2;
            this.TENPHONG.Width = 81;
            // 
            // THOIGIANVAO
            // 
            this.THOIGIANVAO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.THOIGIANVAO.AppearanceCell.Options.UseFont = true;
            this.THOIGIANVAO.AppearanceCell.Options.UseTextOptions = true;
            this.THOIGIANVAO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THOIGIANVAO.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THOIGIANVAO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THOIGIANVAO.AppearanceHeader.Options.UseFont = true;
            this.THOIGIANVAO.AppearanceHeader.Options.UseTextOptions = true;
            this.THOIGIANVAO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THOIGIANVAO.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THOIGIANVAO.Caption = "Giờ Vào";
            this.THOIGIANVAO.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.THOIGIANVAO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.THOIGIANVAO.FieldName = "THOIGIANVAO";
            this.THOIGIANVAO.Name = "THOIGIANVAO";
            this.THOIGIANVAO.OptionsColumn.AllowEdit = false;
            this.THOIGIANVAO.OptionsColumn.AllowFocus = false;
            this.THOIGIANVAO.OptionsColumn.FixedWidth = true;
            this.THOIGIANVAO.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.THOIGIANVAO.Visible = true;
            this.THOIGIANVAO.VisibleIndex = 3;
            this.THOIGIANVAO.Width = 148;
            // 
            // THOIGIANRA
            // 
            this.THOIGIANRA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.THOIGIANRA.AppearanceCell.Options.UseFont = true;
            this.THOIGIANRA.AppearanceCell.Options.UseTextOptions = true;
            this.THOIGIANRA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THOIGIANRA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THOIGIANRA.AppearanceHeader.Options.UseFont = true;
            this.THOIGIANRA.AppearanceHeader.Options.UseTextOptions = true;
            this.THOIGIANRA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THOIGIANRA.Caption = "Giờ Ra";
            this.THOIGIANRA.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.THOIGIANRA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.THOIGIANRA.FieldName = "THOIGIANRA";
            this.THOIGIANRA.Name = "THOIGIANRA";
            this.THOIGIANRA.OptionsColumn.AllowEdit = false;
            this.THOIGIANRA.OptionsColumn.AllowFocus = false;
            this.THOIGIANRA.OptionsColumn.FixedWidth = true;
            this.THOIGIANRA.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.THOIGIANRA.Visible = true;
            this.THOIGIANRA.VisibleIndex = 4;
            this.THOIGIANRA.Width = 159;
            // 
            // BLOCKKARAOKE
            // 
            this.BLOCKKARAOKE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.BLOCKKARAOKE.AppearanceCell.Options.UseFont = true;
            this.BLOCKKARAOKE.AppearanceCell.Options.UseTextOptions = true;
            this.BLOCKKARAOKE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BLOCKKARAOKE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.BLOCKKARAOKE.AppearanceHeader.Options.UseFont = true;
            this.BLOCKKARAOKE.AppearanceHeader.Options.UseTextOptions = true;
            this.BLOCKKARAOKE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BLOCKKARAOKE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.BLOCKKARAOKE.Caption = "Block";
            this.BLOCKKARAOKE.FieldName = "BLOCKKARAOKE";
            this.BLOCKKARAOKE.Name = "BLOCKKARAOKE";
            this.BLOCKKARAOKE.OptionsColumn.AllowEdit = false;
            this.BLOCKKARAOKE.OptionsColumn.AllowFocus = false;
            this.BLOCKKARAOKE.OptionsColumn.FixedWidth = true;
            this.BLOCKKARAOKE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.BLOCKKARAOKE.Visible = true;
            this.BLOCKKARAOKE.VisibleIndex = 5;
            this.BLOCKKARAOKE.Width = 65;
            // 
            // SOPHUT
            // 
            this.SOPHUT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SOPHUT.AppearanceCell.Options.UseFont = true;
            this.SOPHUT.AppearanceCell.Options.UseTextOptions = true;
            this.SOPHUT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOPHUT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOPHUT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOPHUT.AppearanceHeader.Options.UseFont = true;
            this.SOPHUT.AppearanceHeader.Options.UseTextOptions = true;
            this.SOPHUT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOPHUT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOPHUT.Caption = "Số Phút";
            this.SOPHUT.FieldName = "SOPHUT";
            this.SOPHUT.Name = "SOPHUT";
            this.SOPHUT.OptionsColumn.AllowEdit = false;
            this.SOPHUT.OptionsColumn.AllowFocus = false;
            this.SOPHUT.OptionsColumn.FixedWidth = true;
            this.SOPHUT.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.SOPHUT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SOPHUT", "{0:#,###0}")});
            this.SOPHUT.Visible = true;
            this.SOPHUT.VisibleIndex = 6;
            this.SOPHUT.Width = 83;
            // 
            // SOBLOCK
            // 
            this.SOBLOCK.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SOBLOCK.AppearanceCell.Options.UseFont = true;
            this.SOBLOCK.AppearanceCell.Options.UseTextOptions = true;
            this.SOBLOCK.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOBLOCK.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOBLOCK.AppearanceHeader.Options.UseFont = true;
            this.SOBLOCK.AppearanceHeader.Options.UseTextOptions = true;
            this.SOBLOCK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOBLOCK.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOBLOCK.Caption = "Số Block";
            this.SOBLOCK.FieldName = "SOBLOCK";
            this.SOBLOCK.Name = "SOBLOCK";
            this.SOBLOCK.OptionsColumn.AllowEdit = false;
            this.SOBLOCK.OptionsColumn.AllowFocus = false;
            this.SOBLOCK.OptionsColumn.FixedWidth = true;
            this.SOBLOCK.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.SOBLOCK.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SOBLOCK", "{0:#,###0}")});
            this.SOBLOCK.Visible = true;
            this.SOBLOCK.VisibleIndex = 7;
            this.SOBLOCK.Width = 90;
            // 
            // GIABLOCK
            // 
            this.GIABLOCK.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GIABLOCK.AppearanceCell.Options.UseFont = true;
            this.GIABLOCK.AppearanceCell.Options.UseTextOptions = true;
            this.GIABLOCK.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIABLOCK.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GIABLOCK.AppearanceHeader.Options.UseFont = true;
            this.GIABLOCK.AppearanceHeader.Options.UseTextOptions = true;
            this.GIABLOCK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIABLOCK.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABLOCK.Caption = "Giá Block";
            this.GIABLOCK.DisplayFormat.FormatString = "{0:#,###0}";
            this.GIABLOCK.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GIABLOCK.FieldName = "GIABLOCK";
            this.GIABLOCK.Name = "GIABLOCK";
            this.GIABLOCK.OptionsColumn.AllowEdit = false;
            this.GIABLOCK.OptionsColumn.AllowFocus = false;
            this.GIABLOCK.OptionsColumn.FixedWidth = true;
            this.GIABLOCK.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.GIABLOCK.Visible = true;
            this.GIABLOCK.VisibleIndex = 8;
            this.GIABLOCK.Width = 92;
            // 
            // THANHTIEN
            // 
            this.THANHTIEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.THANHTIEN.AppearanceCell.Options.UseFont = true;
            this.THANHTIEN.AppearanceCell.Options.UseTextOptions = true;
            this.THANHTIEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.THANHTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THANHTIEN.AppearanceHeader.Options.UseFont = true;
            this.THANHTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.THANHTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THANHTIEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THANHTIEN.Caption = "Thành Tiền";
            this.THANHTIEN.DisplayFormat.FormatString = "{0:#,###0}";
            this.THANHTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.THANHTIEN.FieldName = "THANHTIEN";
            this.THANHTIEN.MinWidth = 100;
            this.THANHTIEN.Name = "THANHTIEN";
            this.THANHTIEN.OptionsColumn.AllowEdit = false;
            this.THANHTIEN.OptionsColumn.AllowFocus = false;
            this.THANHTIEN.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.THANHTIEN.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "THANHTIEN", "{0:#,###0}")});
            this.THANHTIEN.Visible = true;
            this.THANHTIEN.VisibleIndex = 9;
            this.THANHTIEN.Width = 234;
            // 
            // pnHanghoa
            // 
            this.pnHanghoa.Controls.Add(this.gridControl1);
            this.pnHanghoa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnHanghoa.Location = new System.Drawing.Point(0, 252);
            this.pnHanghoa.Name = "pnHanghoa";
            this.pnHanghoa.Size = new System.Drawing.Size(1189, 244);
            this.pnHanghoa.TabIndex = 50;
            // 
            // Frm_BCChitietHoaDon_Karaoke
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1189, 496);
            this.Controls.Add(this.pnHanghoa);
            this.Controls.Add(this.pnTiengiokaraoke);
            this.Controls.Add(this.header);
            this.Name = "Frm_BCChitietHoaDon_Karaoke";
            this.Text = "Chi Tiết Hóa Đơn Karaoke";
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_mahanghoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soluong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_gianhap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_chietkhau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_soserial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmb_tenhanghoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.header)).EndInit();
            this.header.ResumeLayout(false);
            this.header.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHome)).EndInit();
            this.pnlHome.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.home)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnTiengiokaraoke)).EndInit();
            this.pnTiengiokaraoke.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnHanghoa)).EndInit();
            this.pnHanghoa.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STTHH;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_DONVITINH;
        private DevExpress.XtraGrid.Columns.GridColumn THUE;
        private DevExpress.XtraGrid.Columns.GridColumn SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn TONGCONG;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_mahanghoa;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_soluong;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_gianhap;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_chietkhau;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtvat;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txt_soserial;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox cmb_tenhanghoa;
        private DevExpress.XtraEditors.PanelControl header;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl title;
        private DevExpress.XtraEditors.PanelControl pnlHome;
        private System.Windows.Forms.PictureBox home;
        private DevExpress.XtraGrid.Columns.GridColumn DONGIA;
        private DevExpress.XtraGrid.Columns.GridColumn THANHTIENTT;
        private DevExpress.XtraGrid.Columns.GridColumn TIENTHUE;
        private DevExpress.XtraEditors.PanelControl pnTiengiokaraoke;
        private DevExpress.XtraEditors.PanelControl pnHanghoa;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYTAO;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HOADON;
        private DevExpress.XtraGrid.Columns.GridColumn TENPHONG;
        private DevExpress.XtraGrid.Columns.GridColumn THOIGIANVAO;
        private DevExpress.XtraGrid.Columns.GridColumn THOIGIANRA;
        private DevExpress.XtraGrid.Columns.GridColumn BLOCKKARAOKE;
        private DevExpress.XtraGrid.Columns.GridColumn SOPHUT;
        private DevExpress.XtraGrid.Columns.GridColumn SOBLOCK;
        private DevExpress.XtraGrid.Columns.GridColumn GIABLOCK;
        private DevExpress.XtraGrid.Columns.GridColumn THANHTIEN;
    }
}