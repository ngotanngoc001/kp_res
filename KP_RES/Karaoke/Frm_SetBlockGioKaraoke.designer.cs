﻿namespace KP_RES 
{
    partial class Frm_SetBlockGioKaraoke
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.txtBlock = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lblSOLUONG = new DevExpress.XtraEditors.LabelControl();
            this.pn_thietlapblock = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.ck_tinhgiatheogiaidoan = new DevExpress.XtraEditors.CheckEdit();
            this.ck_giathoidiemvao = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnLUU1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtBlock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_thietlapblock)).BeginInit();
            this.pn_thietlapblock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ck_tinhgiatheogiaidoan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck_giathoidiemvao.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtBlock
            // 
            this.txtBlock.EnterMoveNextControl = true;
            this.txtBlock.Location = new System.Drawing.Point(132, 77);
            this.txtBlock.Name = "txtBlock";
            this.txtBlock.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBlock.Properties.Appearance.Options.UseFont = true;
            this.txtBlock.Properties.Appearance.Options.UseTextOptions = true;
            this.txtBlock.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtBlock.Properties.DisplayFormat.FormatString = "N0";
            this.txtBlock.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtBlock.Properties.EditFormat.FormatString = "N0";
            this.txtBlock.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtBlock.Properties.Mask.EditMask = "N0";
            this.txtBlock.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtBlock.Size = new System.Drawing.Size(226, 26);
            this.txtBlock.TabIndex = 22;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(365, 80);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(32, 19);
            this.labelControl2.TabIndex = 23;
            this.labelControl2.Text = "Phút";
            // 
            // lblSOLUONG
            // 
            this.lblSOLUONG.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSOLUONG.Location = new System.Drawing.Point(57, 80);
            this.lblSOLUONG.Margin = new System.Windows.Forms.Padding(4);
            this.lblSOLUONG.Name = "lblSOLUONG";
            this.lblSOLUONG.Size = new System.Drawing.Size(68, 19);
            this.lblSOLUONG.TabIndex = 21;
            this.lblSOLUONG.Text = "1 Block =";
            // 
            // pn_thietlapblock
            // 
            this.pn_thietlapblock.Controls.Add(this.labelControl1);
            this.pn_thietlapblock.Controls.Add(this.labelControl2);
            this.pn_thietlapblock.Controls.Add(this.txtBlock);
            this.pn_thietlapblock.Controls.Add(this.lblSOLUONG);
            this.pn_thietlapblock.Dock = System.Windows.Forms.DockStyle.Top;
            this.pn_thietlapblock.Location = new System.Drawing.Point(0, 0);
            this.pn_thietlapblock.Name = "pn_thietlapblock";
            this.pn_thietlapblock.Size = new System.Drawing.Size(563, 115);
            this.pn_thietlapblock.TabIndex = 24;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Location = new System.Drawing.Point(13, 40);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(219, 19);
            this.labelControl1.TabIndex = 24;
            this.labelControl1.Text = "Thiết lập Block tính giá giờ";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.ck_tinhgiatheogiaidoan);
            this.panelControl1.Controls.Add(this.ck_giathoidiemvao);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 115);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(563, 137);
            this.panelControl1.TabIndex = 25;
            // 
            // ck_tinhgiatheogiaidoan
            // 
            this.ck_tinhgiatheogiaidoan.EditValue = true;
            this.ck_tinhgiatheogiaidoan.Location = new System.Drawing.Point(55, 93);
            this.ck_tinhgiatheogiaidoan.Name = "ck_tinhgiatheogiaidoan";
            this.ck_tinhgiatheogiaidoan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.ck_tinhgiatheogiaidoan.Properties.Appearance.Options.UseFont = true;
            this.ck_tinhgiatheogiaidoan.Properties.Caption = "Tính giá theo từng mốc thời gian được thiết lập trong bảng giá";
            this.ck_tinhgiatheogiaidoan.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.ck_tinhgiatheogiaidoan.Properties.RadioGroupIndex = 1;
            this.ck_tinhgiatheogiaidoan.Size = new System.Drawing.Size(567, 24);
            this.ck_tinhgiatheogiaidoan.TabIndex = 26;
            // 
            // ck_giathoidiemvao
            // 
            this.ck_giathoidiemvao.Location = new System.Drawing.Point(55, 45);
            this.ck_giathoidiemvao.Name = "ck_giathoidiemvao";
            this.ck_giathoidiemvao.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.ck_giathoidiemvao.Properties.Appearance.Options.UseFont = true;
            this.ck_giathoidiemvao.Properties.Caption = "Tính giá tại thời điểm vào";
            this.ck_giathoidiemvao.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.ck_giathoidiemvao.Properties.RadioGroupIndex = 1;
            this.ck_giathoidiemvao.Size = new System.Drawing.Size(227, 24);
            this.ck_giathoidiemvao.TabIndex = 25;
            this.ck_giathoidiemvao.TabStop = false;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl3.Location = new System.Drawing.Point(13, 13);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(168, 19);
            this.labelControl3.TabIndex = 24;
            this.labelControl3.Text = "Cấu hình tính giá giờ";
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(444, 294);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(110, 35);
            this.btnDong.TabIndex = 27;
            this.btnDong.Text = "&2.Đóng";
            this.btnDong.Visible = false;
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnLUU1
            // 
            this.btnLUU1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLUU1.Appearance.Options.UseFont = true;
            this.btnLUU1.Image = global::KP_RES.Properties.Resources.ok_26;
            this.btnLUU1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLUU1.Location = new System.Drawing.Point(57, 259);
            this.btnLUU1.Margin = new System.Windows.Forms.Padding(4);
            this.btnLUU1.Name = "btnLUU1";
            this.btnLUU1.Size = new System.Drawing.Size(110, 35);
            this.btnLUU1.TabIndex = 26;
            this.btnLUU1.Text = "&1.Đồng ý";
            this.btnLUU1.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // Frm_SetBlockGioKaraoke
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(563, 338);
            this.ControlBox = false;
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.btnLUU1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.pn_thietlapblock);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_SetBlockGioKaraoke";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thiết lập Block tính giá giờ";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_ApdungbanggiaKaraoke_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.txtBlock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_thietlapblock)).EndInit();
            this.pn_thietlapblock.ResumeLayout(false);
            this.pn_thietlapblock.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ck_tinhgiatheogiaidoan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck_giathoidiemvao.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtBlock;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl lblSOLUONG;
        private DevExpress.XtraEditors.PanelControl pn_thietlapblock;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.CheckEdit ck_tinhgiatheogiaidoan;
        private DevExpress.XtraEditors.CheckEdit ck_giathoidiemvao;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.SimpleButton btnLUU1;




    }
}