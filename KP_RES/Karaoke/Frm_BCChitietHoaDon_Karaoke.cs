﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KP_UserManagement;

namespace KP_RES
{
    public partial class Frm_BCChitietHoaDon_Karaoke : Form
    {
        public Frm_BCChitietHoaDon_Karaoke(string ma)
        {
            InitializeComponent();
            bool bGIABANTRUOCTHUE;
            if (clsMain.ReturnDataTable("Select TEN,GIATRI From CAUHINH where TEN='GIABANTRUOCTHUE'").Rows[0]["GIATRI"].ToString() == "1")
                bGIABANTRUOCTHUE = true;
            else
                bGIABANTRUOCTHUE = false;
             
            string sql = "";
            if(bGIABANTRUOCTHUE)
                sql += "select cthd.MA_HANGHOA as MA,TEN_HANGHOA,TEN_DONVITINH,isnull(cthd.THUE,0)as THUE,isnull(cthd.GIABAN*(cthd.THUE/100)*cthd.SOLUONG,0) as TIENTHUE,cthd.SOLUONG,GIABAN as DONGIA ";
            else
                sql += "select cthd.MA_HANGHOA as MA,TEN_HANGHOA,TEN_DONVITINH,isnull(cthd.THUE,0)as THUE,isnull(cthd.GIABAN*(cthd.THUE/(100+cthd.THUE))*cthd.SOLUONG,0) as TIENTHUE,cthd.SOLUONG,GIABAN as DONGIA ";
            
            sql += "from CT_HOADON_KARAOKE cthd,HANGHOA H,DONVITINH DVT where cthd.MA_HANGHOA=H.MA_HANGHOA and DVT.MA_DONVITINH=H.MA_DONVITINH and MA_HOADON='" + ma + "'";
            DataTable dt = clsMain.ReturnDataTable(sql);
            dt.Columns.Add("THANHTIENTT",typeof(float));
            dt.Columns.Add("TONGCONG", typeof(float));
            foreach (DataRow dr in dt.Rows)
            {
                dr["THANHTIENTT"] = float.Parse(dr["SOLUONG"].ToString()) * float.Parse(dr["DONGIA"].ToString());
                if(bGIABANTRUOCTHUE)
                    dr["TONGCONG"] = float.Parse(dr["SOLUONG"].ToString()) * float.Parse(dr["DONGIA"].ToString()) + float.Parse(dr["TIENTHUE"].ToString());
                else
                    dr["TONGCONG"] = float.Parse(dr["SOLUONG"].ToString()) * float.Parse(dr["DONGIA"].ToString());
            }
            gridControl1.DataSource = dt;
            //Đổ nguồn lưới Karaoke
            string sqlKaraoke = string.Empty;
            sqlKaraoke = "SELECT NGAYTAO, MA_HOADON, B.TEN_BAN as TENPHONG, THOIGIANVAO, THOIGIANRA, BLOCKKARAOKE, SOPHUT, SOBLOCK, GIABLOCK, THANHTIEN" + "\n";
            sqlKaraoke += "FROM CT_TIENGIOKARAOKE CT LEFT JOIN BAN B ON CT.MA_PHONG = B.MA_BAN" + "\n";
            sqlKaraoke += "WHERE MA_HOADON = '" + ma + "'";
            DataTable dtKaraoke = clsMain.ReturnDataTable(sqlKaraoke);
            gridControl2.DataSource = dtKaraoke;
        }
        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STTHH & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void home_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pnlHome_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }
    }   
}
