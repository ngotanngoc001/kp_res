﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_Lapbanggia_Karaoke : DevExpress.XtraEditors.XtraForm
    {
        public static BindingSource BS = new BindingSource();
        DataTable dtBanggia = new DataTable();
        string sma = "";

        public Frm_Lapbanggia_Karaoke()
        {
            InitializeComponent();
            LoaddataGridView();
            LoadPermission();
        }

        private void Frm_Lapbanggia_Karaoke_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTEN.Properties.ReadOnly = true;
                txtGHICHU.Properties.ReadOnly = true;
                chkSUDUNG.Properties.ReadOnly = true;
                gridView2.OptionsBehavior.ReadOnly = true;
            }
            else//mở
            {
                txtTEN.Properties.ReadOnly = false;
                txtGHICHU.Properties.ReadOnly = false;
                chkSUDUNG.Properties.ReadOnly = false;
                gridView2.OptionsBehavior.ReadOnly = false;
            }
        }

        private void SetRongControl()
        {
            sma = "";
            txtTEN.Text = "";
            txtGHICHU.Text = "";
            chkSUDUNG.Checked = true;
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void LoaddataGridView()
        {
            try
            {
                SetRongControl();
                KhoaMoControl(true);

                string sSQL = "";
                sSQL += "Select MABANGGIA As MA,TENBANGGIA As TEN,GHICHU,SUDUNG,THOIGIAN1,GIA1,THOIGIAN2,GIA2,THOIGIAN3,GIA3" + "\n";
                sSQL += "From BANGGIAKARAOKE" + "\n";
                dtBanggia = clsMain.ReturnDataTable(sSQL);

                fpnlBanggia.Controls.Clear();
                foreach (DataRow dr in dtBanggia.Rows)
                {
                    SimpleButton btn = new SimpleButton();
                    btn.Width = 110;
                    btn.Height = 80;
                    btn.Font = fpnlBanggia.Font;
                    btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btn.ImageLocation = ImageLocation.TopCenter;
                    btn.Image = global::KP_RES.Properties.Resources.display_26;
                    btn.Name = "H" + dr["MA"].ToString();
                    btn.Text = dr["TEN"].ToString();
                    if (btn.Text.Length > 20)
                    {
                        btn.Width = 110 * 2;
                    }
                    btn.Click += new EventHandler(BangGia_Click);
                    fpnlBanggia.Controls.Add(btn);
                }
            }
            catch
            {
            }
        }

        private void LoaddataGridView1()
        {
            try
            {
                string sSQL = "";
                sSQL += "Select  MABANGGIA,TENBANGGIA,GHICHU,SUDUNG,CONVERT(datetime, THOIGIAN1,103) as THOIGIAN1 ,GIA1,CONVERT(datetime, THOIGIAN2,103) as THOIGIAN2,GIA2,CONVERT(datetime, THOIGIAN3,103)as THOIGIAN3,GIA3" + "\n";
                sSQL += "From BANGGIAKARAOKE" + "\n";
                sSQL += "Where MABANGGIA=" + clsMain.SQLString(sma);
                DataTable dt = clsMain.ReturnDataTable(sSQL);

                DataColumn dcTHOIGIAN4 = new DataColumn();
                dcTHOIGIAN4.ColumnName = "THOIGIAN4";
                DateTime dti = DateTime.Parse(dt.Rows[0]["THOIGIAN1"].ToString());
                dcTHOIGIAN4.DefaultValue = dti.AddSeconds(-1.0).Hour.ToString("00") + ":" + dti.AddSeconds(-1.0).Minute.ToString("00") + ":" + dti.AddSeconds(-1.0).Second.ToString("00");
                dt.Columns.Add(dcTHOIGIAN4);

                BS.DataSource = dt;
                gridControl2.DataSource = BS;
            }
            catch
            {
                gridControl2.DataSource = null;
            }
        }

        private void SetDulieuKhoiTao()
        {
            DataTable DTSOUCRE = new DataTable();
            DTSOUCRE.Columns.Add("THOIGIAN1", typeof(DateTime));
            DTSOUCRE.Columns.Add("GIA1", typeof(float));
            DTSOUCRE.Columns.Add("THOIGIAN2", typeof(DateTime));
            DTSOUCRE.Columns.Add("GIA2", typeof(float));
            DTSOUCRE.Columns.Add("THOIGIAN3", typeof(DateTime));
            DTSOUCRE.Columns.Add("GIA3", typeof(float));
            DTSOUCRE.Columns.Add("THOIGIAN4", typeof(DateTime));
            DTSOUCRE.Rows.Add("00:00:00", 0, "00:00:00", 0, "00:00:00", 0, "00:00:00");
            BS.DataSource = DTSOUCRE;
            gridControl2.DataSource = BS;
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtTEN.Focus();
            SetDulieuKhoiTao();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để sửa
            {
                return;
            }
            KhoaMoControl(false);
            txtTEN.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (!CheckDelete())
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận bạn muốn xóa ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From BANGGIAKARAOKE" + "\n";
            sSQL += "Where MABANGGIA=" + clsMain.SQLString(sma) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
                LoaddataGridView1();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            DataTable dt = (DataTable)BS.DataSource;
            string sSQL = "";
            if (sma == "" && btnThem.Enabled)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    sSQL += "Insert into BANGGIAKARAOKE (TENBANGGIA,GHICHU,SUDUNG,THOIGIAN1,GIA1,THOIGIAN2,GIA2,THOIGIAN3,GIA3)" + "\n";
                    sSQL += "Values ( ";
                    sSQL += clsMain.SQLStringUnicode(txtTEN.Text) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtGHICHU.Text) + ",";
                    sSQL += clsMain.SQLBit(chkSUDUNG.Checked) + ",";
                    sSQL += clsMain.SQLString(string.Format("{0:HH:mm:ss}", Convert.ToDateTime(dr["THOIGIAN1"].ToString()))) + "," + clsMain.SQLString(dr["GIA1"].ToString()) + ",";
                    sSQL += clsMain.SQLString(string.Format("{0:HH:mm:ss}", Convert.ToDateTime(dr["THOIGIAN2"].ToString()))) + "," + clsMain.SQLString(dr["GIA2"].ToString()) + ",";
                    sSQL += clsMain.SQLString(string.Format("{0:HH:mm:ss}", Convert.ToDateTime(dr["THOIGIAN3"].ToString()))) + "," + clsMain.SQLString(dr["GIA3"].ToString());
                    sSQL += ")" + "\n";
                }
            }
            else if ((sma != "" && btnSua.Enabled))
            {
                foreach (DataRow dr in dt.Rows)
                {
                    sSQL += "Update BANGGIAKARAOKE Set " + "\n";
                    sSQL += "TENBANGGIA=" + clsMain.SQLStringUnicode(txtTEN.Text) + "," + "\n";
                    sSQL += "GHICHU=" + clsMain.SQLStringUnicode(txtGHICHU.Text) + "," + "\n";
                    sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNG.Checked) + "," + "\n";
                    sSQL += "THOIGIAN1=" + clsMain.SQLString(string.Format("{0:HH:mm:ss}", Convert.ToDateTime(dr["THOIGIAN1"].ToString()))) + "," + "\n";
                    sSQL += "GIA1=" + clsMain.SQLString(dr["GIA1"].ToString()) + "," + "\n";
                    sSQL += "THOIGIAN2=" + clsMain.SQLString(string.Format("{0:HH:mm:ss}", Convert.ToDateTime(dr["THOIGIAN2"].ToString()))) + "," + "\n";
                    sSQL += "GIA2=" + clsMain.SQLString(dr["GIA2"].ToString()) + "," + "\n";
                    sSQL += "THOIGIAN3=" + clsMain.SQLString(string.Format("{0:HH:mm:ss}", Convert.ToDateTime(dr["THOIGIAN3"].ToString()))) + "," + "\n";
                    sSQL += "GIA3=" + clsMain.SQLString(dr["GIA3"].ToString()) + "\n";
                    sSQL += "Where MABANGGIA=" + clsMain.SQLString(sma) + "\n";
                }
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
       
        private Boolean CheckInput()
        {
            if (txtTEN.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblTEN.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTEN.Focus();
                return false;
            }
            DataTable dt = (DataTable)BS.DataSource;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (float.Parse(dt.Rows[i]["GIA1"].ToString()) == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Chưa nhập giá 1", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                if (float.Parse(dt.Rows[i]["GIA2"].ToString()) == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Chưa nhập giá 2", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                if (float.Parse(dt.Rows[i]["GIA3"].ToString()) == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Chưa nhập giá 3", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                if (int.Parse(string.Format("{0:HHmmss}", Convert.ToDateTime(dt.Rows[i]["THOIGIAN1"].ToString()))) > int.Parse(string.Format("{0:HHmmss}", Convert.ToDateTime(dt.Rows[i]["THOIGIAN2"].ToString()))))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Thời gian 1 không được lớn hơn thời gian 2", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                if (int.Parse(string.Format("{0:HHmmss}", Convert.ToDateTime(dt.Rows[i]["THOIGIAN2"].ToString()))) > int.Parse(string.Format("{0:HHmmss}", Convert.ToDateTime(dt.Rows[i]["THOIGIAN3"].ToString()))))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Thời gian 2 không được lớn hơn thời gian 3", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
            }
            return true;
        }

        private Boolean CheckDelete()
        {
            if (clsMain.ReturnDataTable("Select * From APDUNGBANGGIAKARAOKE Where THUHAI=" + clsMain.SQLString(sma) + " Or THUBA=" + clsMain.SQLString(sma) + " Or THUTU=" + clsMain.SQLString(sma) + " Or THUNAM=" + clsMain.SQLString(sma) + " Or THUSAU=" + clsMain.SQLString(sma) + " Or THUBAY=" + clsMain.SQLString(sma) + " Or CHUNHAT=" + clsMain.SQLString(sma)).Rows.Count > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Bảng giá đã sử dụng. Không được xóa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }

        private void BangGia_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in fpnlBanggia.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }
            sma = btn.Name.Substring(1, btn.Name.Length - 1);
            txtTEN.Text = dtBanggia.Select("MA=" + clsMain.SQLString(sma))[0]["TEN"].ToString();
            txtGHICHU.Text = dtBanggia.Select("MA=" + clsMain.SQLString(sma))[0]["GHICHU"].ToString();
            chkSUDUNG.Checked = bool.Parse(dtBanggia.Select("MA=" + clsMain.SQLString(sma))[0]["SUDUNG"].ToString());
            LoaddataGridView1();
            KhoaMoControl(true);
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("42");
            btnSua.Enabled = clsUserManagement.AllowEdit("42");
            btnXoa.Enabled = clsUserManagement.AllowDelete("42");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void time_thoigian1_EditValueChanged(object sender, EventArgs e)
        {
            int vitri = gridView2.FocusedRowHandle;
            gridView2.FocusedRowHandle = -1;
            gridView2.FocusedRowHandle = vitri;

            DataTable dt = (DataTable)BS.DataSource;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DateTime dti = DateTime.Parse(dt.Rows[i]["THOIGIAN1"].ToString());
                dt.Rows[i]["THOIGIAN4"] = dti.AddSeconds(-1.0).Hour.ToString("00") + ":" + dti.AddSeconds(-1.0).Minute.ToString("00") + ":" + dti.AddSeconds(-1.0).Second.ToString("00");
            }

        }

        private void Frm_Lapbanggia_Karaoke_Load(object sender, EventArgs e)
        {
            foreach (Control ctl in pnTop.Controls)
            {
                if (ctl is TextEdit)
                {
                    ctl.Tag = 0;
                }
                ctl.Click += new EventHandler(ctl_Click);
            }
        }

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                if (txt.Tag.ToString() == "0")
                {
                    txt.SelectAll();
                    txt.Tag = 1;
                }
                else
                {
                    txt.Tag = 0;
                }
            }
            catch
            {
            }
        }
    }
}