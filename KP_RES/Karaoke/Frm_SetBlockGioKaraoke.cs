﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_SetBlockGioKaraoke : DevExpress.XtraEditors.XtraForm
    {
        public Frm_SetBlockGioKaraoke()
        {
            InitializeComponent();
            LoaddataGridView();
        }

        private void Frm_ApdungbanggiaKaraoke_Paint(object sender, PaintEventArgs e)
        {
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private Boolean CheckInput()
        {
            if (txtBlock.Text != "")
            {
                return true;
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chưa nhập số phút cho 1 block", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            //if (ck_giathoidiemvao.Checked == true || ck_tinhgiatheogiaidoan.Checked == true)
            //{
            //    return true;
            //}
            //else
            //{
            //    DevExpress.XtraEditors.XtraMessageBox.Show("Chưa chọn cách tính giá cho Karaoke", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    return false;
            //}
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete CAUHINH" + "\n";
            sSQL += "Where TEN='BLOCKKARAOKE'" + "\n";
            sSQL += "Insert into CAUHINH (TEN,GIATRI,GHICHU)" + "\n";
            sSQL += "Values ( ";
            sSQL += clsMain.SQLString("BLOCKKARAOKE") + ",";
            sSQL += clsMain.SQLString(txtBlock.Text.Replace(",", "")) + ",";
            sSQL += clsMain.SQLStringUnicode("Block Karaoke") + ")" + "\n";

            string cauhinh = "";
            if (ck_giathoidiemvao.Checked == true)
                cauhinh = "1";
            else if (ck_tinhgiatheogiaidoan.Checked == true)
                cauhinh = "2";

            sSQL += "Delete CAUHINH" + "\n";
            sSQL += "Where TEN='TIENGIOKARAOKE'" + "\n";
            sSQL += "Insert into CAUHINH (TEN,GIATRI,GHICHU)" + "\n";
            sSQL += "Values ( ";
            sSQL += clsMain.SQLString("TIENGIOKARAOKE") + ",";
            sSQL += clsMain.SQLString(cauhinh) + ",";
            sSQL += clsMain.SQLStringUnicode("- 1: Tính giá tại thời điểm vào - 2: Tính giá theo từng mốc thời gian được thiết lập trong bảng giá") + ")" + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LoaddataGridView()
        {
            try
            {
                string sSQL = "";

                sSQL = "";
                sSQL += "Select TEN, GIATRI" + "\n";
                sSQL += "From CAUHINH" + "\n";
                sSQL += "Where TEN = 'BLOCKKARAOKE'" + "\n";
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                
                if(dt.Rows.Count > 0)
                    txtBlock.Text = dt.Rows[0]["GIATRI"].ToString();

                string sSQLgia = "";

                sSQLgia = "";
                sSQLgia += "Select TEN, GIATRI" + "\n";
                sSQLgia += "From CAUHINH" + "\n";
                sSQLgia += "Where TEN = 'TIENGIOKARAOKE'" + "\n";
                DataTable dtgia = clsMain.ReturnDataTable(sSQLgia);

                if (dtgia.Rows.Count > 0)
                {
                    if (dtgia.Rows[0]["GIATRI"].ToString() == "1")
                        ck_giathoidiemvao.Checked = true;
                    if (dtgia.Rows[0]["GIATRI"].ToString() == "2")
                        ck_tinhgiatheogiaidoan.Checked = true;
                }
            }
            catch
            {
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}