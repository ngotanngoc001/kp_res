﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES
{
    public partial class Frm_Soluongkhach : DevExpress.XtraEditors.XtraForm
    {
        public bool kt = false;
        bool[] ctltxt;
        public decimal tmp_Thoigia;

        public Frm_Soluongkhach()
        {
            InitializeComponent();
        }

        string _thongbao = "";
        string _bkdnq15kt = "";
        string _cntg = "";
        private void Frm_ConfigFuntion_Load(object sender, EventArgs e)
        {
            int dem = 1;
            foreach (Control ctl in panelControl2.Controls)
            {
                if (ctl is TextEdit)
                {
                    ctl.Tag = dem;
                    dem += 1;
                }
                ctl.Click += new EventHandler(ctl_Click);
            }
            ctltxt = new bool[dem];

            txtGIA.SelectAll();

            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            //this.Text = rm.GetString("nhapgia", culture);
            //lb_nhapgia.Text = rm.GetString("nhapgia", culture);
            btn_thoat.Text = rm.GetString("thoat", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _bkdnq15kt = rm.GetString("bkdnq15kt", culture);
            _cntg = rm.GetString("cntg", culture);
        }

        private void btn_000_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("000");
        }

        private void btn_00_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("00");
        }

        private void btn_0_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("0");
        }

        private void btn_1_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("1");
        }

        private void btn_2_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("2");
        }

        private void btn_3_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("3");
        }

        private void btn_4_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("4");
        }

        private void btn_5_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("5");
        }

        private void btn_6_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("6");
        }

        private void btn_7_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("7");
        }

        private void btn_8_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("8");
        }

        private void btn_9_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("9");
        }

        private void btn_ESC_Click(object sender, EventArgs e)
        {
            txtGIA.EditValue = 0;
        }

        private void btn_Payments_Click(object sender, EventArgs e)
        {
            SendKeys.Send("{Enter}");
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            DeleteTextBox();
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            MoveToLeft();
        }

        private void btnMoveRightNumber_Click(object sender, EventArgs e)
        {
            MoveToRight();
        }

        private void btn_Thoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtConfig_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtGIA.Text == "" || txtGIA.Text == "0")
                {
                    MessageBox.Show(_cntg, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                tmp_Thoigia = Convert.ToDecimal(txtGIA.Text);
                this.Close();
            }
        }

        private void InputKeyboardNumber(string sInput)
        {
            if (txtGIA.Text.Length >= 15)
            {
                int iPositon = txtGIA.SelectionStart;
                XtraMessageBox.Show(_bkdnq15kt, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtGIA.SelectionStart = txtGIA.Text.Length;
                txtGIA.SelectionStart = iPositon;
                return;
            }

            if (txtGIA.Text == "0")
            {
                if (sInput == "0" || sInput == "00" || sInput == "000")
                    return;
            }
            if (txtGIA.SelectedText == txtGIA.Text)
            {
                txtGIA.Text = "0";
            }
            int iLenght, iPosition;
            iLenght = txtGIA.Text.Length;
            iPosition = txtGIA.SelectionStart;
            if (iPosition == 0)
            {
                if (sInput == "0" || sInput == "00" || sInput == "000")
                    return;
            }

            if (txtGIA.Text == "0")
            {
                txtGIA.Text = sInput;
            }
            else
            {
                txtGIA.Text = txtGIA.Text.Insert(iPosition, sInput);
            }
            if (sInput.Length == 1)
            {
                if (txtGIA.Text.Length - iLenght == 1)
                {
                    txtGIA.SelectionStart = iPosition + 1;
                }
                else
                {
                    txtGIA.SelectionStart = iPosition + 2;
                }
            }
            else
                if (sInput.Length == 2)
                {
                    if (txtGIA.Text.Length - iLenght == 2)
                    {
                        txtGIA.SelectionStart = iPosition + 2;
                    }
                    else
                    {
                        txtGIA.SelectionStart = iPosition + 3;
                    }
                }
                else
                    if (sInput.Length == 3)
                    {
                        if (txtGIA.Text.Length - iLenght == 3)
                        {
                            txtGIA.SelectionStart = iPosition + 3;
                        }
                        else
                        {
                            txtGIA.SelectionStart = iPosition + 4;
                        }
                    }


        }

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                if (!ctltxt[int.Parse(txt.Tag.ToString())])
                {
                    txt.SelectAll();
                    ctltxt[int.Parse(txt.Tag.ToString())] = true;
                }
                else
                {
                    ctltxt[int.Parse(txt.Tag.ToString())] = false;
                }
            }
            catch
            {
            }
        }

        private void DeleteTextBox()
        {
            int iPosition = txtGIA.SelectionStart;
            int iLenght = txtGIA.Text.Length;
            if (iPosition > 1)
            {
                if (txtGIA.Text[iPosition - 1].ToString() == ",")
                {
                    txtGIA.Text = txtGIA.Text.Remove(iPosition - 2, 1);

                    txtGIA.SelectionStart = iPosition - 2;

                }
                else
                {
                    txtGIA.Text = txtGIA.Text.Remove(iPosition - 1, 1);
                    if (iLenght - txtGIA.Text.Length == 1)
                    {
                        txtGIA.SelectionStart = iPosition - 1;
                    }
                    else
                    {
                        txtGIA.SelectionStart = iPosition - 2;
                    }
                }
            }
            else if (iPosition == 1)
            {
                if (iLenght >= 2)
                {
                    txtGIA.Text = txtGIA.Text.Remove(iPosition - 1, 1);
                    txtGIA.SelectionStart = iPosition - 1;
                    if (txtGIA.Text == "0")
                        txtGIA.SelectionStart = 1;
                }
                else
                {
                    txtGIA.Text = "0";
                    txtGIA.SelectionStart = 1;
                }
            }
        }

        private void MoveToLeft()
        {
            int iPosition = txtGIA.SelectionStart;
            if (iPosition > 1)
            {
                if (txtGIA.Text[iPosition - 1].ToString() == ",")
                {
                    txtGIA.SelectionStart = iPosition - 2;
                }
                else
                {
                    txtGIA.SelectionStart = iPosition - 1;
                }
            }
            else
            {
                txtGIA.SelectionStart = 0;

            }
        }

        private void MoveToRight()
        {
            int iPosition = txtGIA.SelectionStart;
            if (iPosition < txtGIA.Text.Length - 1)
            {
                if (txtGIA.Text[iPosition + 1].ToString() == ",")
                {
                    txtGIA.SelectionStart = iPosition + 2;
                }
                else
                {
                    txtGIA.SelectionStart = iPosition + 1;
                }
            }
            else
            {
                txtGIA.SelectionStart = txtGIA.Text.Length;

            }
        }
    }
}