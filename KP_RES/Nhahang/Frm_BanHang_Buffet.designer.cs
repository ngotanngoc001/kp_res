﻿namespace KP_RES
{
    partial class Frm_BanHang_Buffet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_BanHang_Buffet));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            this.pnShowHang = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.pnTable_Mon = new DevExpress.XtraEditors.XtraScrollableControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.lbPageTable = new DevExpress.XtraEditors.LabelControl();
            this.btn_NextRight = new DevExpress.XtraEditors.SimpleButton();
            this.btn_NextToLeft = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.pnlBuffet = new DevExpress.XtraEditors.PanelControl();
            this.pnBuffet = new DevExpress.XtraEditors.XtraScrollableControl();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.btn_UpBF = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl16 = new DevExpress.XtraEditors.PanelControl();
            this.btn_DowBF = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pnKV_Mon = new DevExpress.XtraEditors.PanelControl();
            this.panelControl27 = new DevExpress.XtraEditors.PanelControl();
            this.btn_DowKV = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl28 = new DevExpress.XtraEditors.PanelControl();
            this.btn_UpKV = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl37 = new DevExpress.XtraEditors.PanelControl();
            this.lbMon = new DevExpress.XtraEditors.LabelControl();
            this.lbKhuVuc = new DevExpress.XtraEditors.LabelControl();
            this.lbBan = new DevExpress.XtraEditors.LabelControl();
            this.txtID = new DevExpress.XtraEditors.LookUpEdit();
            this.pnMain = new DevExpress.XtraEditors.PanelControl();
            this.pn_listProduct = new DevExpress.XtraEditors.PanelControl();
            this.gr_SelectProduct = new DevExpress.XtraGrid.GridControl();
            this.gv_SelectProduct = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SOTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIABAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CHIETKHAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Thanh_Tien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_BEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IS_COMBO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IS_INBEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IS_KHUYENMAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnUpDowGv = new DevExpress.XtraEditors.PanelControl();
            this.btn_up = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Dow = new DevExpress.XtraEditors.SimpleButton();
            this.btn_DowLast = new DevExpress.XtraEditors.SimpleButton();
            this.btn_UpFirst = new DevExpress.XtraEditors.SimpleButton();
            this.pnBtnDeleteGv = new DevExpress.XtraEditors.PanelControl();
            this.panelControl29 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl33 = new DevExpress.XtraEditors.PanelControl();
            this.btn_tru = new DevExpress.XtraEditors.SimpleButton();
            this.btn_XoaTatCa = new DevExpress.XtraEditors.SimpleButton();
            this.btn_cong = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btn_xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btn9 = new DevExpress.XtraEditors.SimpleButton();
            this.btn8 = new DevExpress.XtraEditors.SimpleButton();
            this.btn7 = new DevExpress.XtraEditors.SimpleButton();
            this.btn6 = new DevExpress.XtraEditors.SimpleButton();
            this.btn5 = new DevExpress.XtraEditors.SimpleButton();
            this.btn4 = new DevExpress.XtraEditors.SimpleButton();
            this.btn3 = new DevExpress.XtraEditors.SimpleButton();
            this.btn2 = new DevExpress.XtraEditors.SimpleButton();
            this.btn1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn0 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl31 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl32 = new DevExpress.XtraEditors.PanelControl();
            this.lbSoluong = new DevExpress.XtraEditors.LabelControl();
            this.lb_soluong = new DevExpress.XtraEditors.LabelControl();
            this.panelControl20 = new DevExpress.XtraEditors.PanelControl();
            this.btnComboOption = new DevExpress.XtraEditors.SimpleButton();
            this.pnlInforMoney = new DevExpress.XtraEditors.PanelControl();
            this.panelControl13 = new DevExpress.XtraEditors.PanelControl();
            this.lbPhuThu = new DevExpress.XtraEditors.LabelControl();
            this.panelControl23 = new DevExpress.XtraEditors.PanelControl();
            this.lbGiamGia = new DevExpress.XtraEditors.LabelControl();
            this.panelControl50 = new DevExpress.XtraEditors.PanelControl();
            this.lbTotal = new DevExpress.XtraEditors.LabelControl();
            this.lb_phuthu = new DevExpress.XtraEditors.LabelControl();
            this.lb_giamgia = new DevExpress.XtraEditors.LabelControl();
            this.lb_tongcong = new DevExpress.XtraEditors.LabelControl();
            this.pnNghiepVu_ThuNgan = new DevExpress.XtraEditors.PanelControl();
            this.pnDacThu = new DevExpress.XtraEditors.PanelControl();
            this.btnDoihinhthucthanhtoan = new DevExpress.XtraEditors.SimpleButton();
            this.btnNangcap = new DevExpress.XtraEditors.SimpleButton();
            this.btnBoKhoaBan = new DevExpress.XtraEditors.SimpleButton();
            this.btnHetmon = new DevExpress.XtraEditors.SimpleButton();
            this.btnBaoCaoBanHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnChongia = new DevExpress.XtraEditors.SimpleButton();
            this.btn_BanGia2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnChietkhau = new DevExpress.XtraEditors.SimpleButton();
            this.btnSuaGia = new DevExpress.XtraEditors.SimpleButton();
            this.pnThuNgan = new DevExpress.XtraEditors.PanelControl();
            this.btn_Dong = new DevExpress.XtraEditors.SimpleButton();
            this.btn_TamTinh = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ThanhToan = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Tachban = new DevExpress.XtraEditors.SimpleButton();
            this.btn_GhepBan = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Chuyenban = new DevExpress.XtraEditors.SimpleButton();
            this.btn_YeuCauThem = new DevExpress.XtraEditors.SimpleButton();
            this.btn_GuiOrder = new DevExpress.XtraEditors.SimpleButton();
            this.btn_SoDoBan = new DevExpress.XtraEditors.SimpleButton();
            this.pnNghiepVu = new DevExpress.XtraEditors.PanelControl();
            this.btn_KetCa = new DevExpress.XtraEditors.SimpleButton();
            this.btn_MoKet = new DevExpress.XtraEditors.SimpleButton();
            this.btn_HuyMon = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.btn_HuyBan = new DevExpress.XtraEditors.SimpleButton();
            this.btn_DatBan = new DevExpress.XtraEditors.SimpleButton();
            this.btn_InlaiOrder = new DevExpress.XtraEditors.SimpleButton();
            this.btn_LayLaiHoaDon = new DevExpress.XtraEditors.SimpleButton();
            this.btn_InLaiHoaDon = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.btn_DatThu = new DevExpress.XtraEditors.SimpleButton();
            this.btn_NghiepVu = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ThuNgan = new DevExpress.XtraEditors.SimpleButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pnTachban = new DevExpress.XtraEditors.PanelControl();
            this.pn_GroupTable = new DevExpress.XtraEditors.PanelControl();
            this.pnUpDow = new DevExpress.XtraEditors.PanelControl();
            this.btn_NhapSL = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btn_OkThrough = new DevExpress.XtraEditors.SimpleButton();
            this.btn_UpDivide = new DevExpress.XtraEditors.SimpleButton();
            this.btn_DowDivide = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ESCThroughtTable = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ToRight = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Toleft = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ToRightAll = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ToLeftAll = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.gr_divide_Table_1 = new DevExpress.XtraGrid.GridControl();
            this.gv_divide_Table_1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._SSTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this._TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this._SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this._GIABAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this._Thanh_Tien = new DevExpress.XtraGrid.Columns.GridColumn();
            this._MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this._GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this._MA_BEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this._IS_COMBO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pn_TableTow = new DevExpress.XtraEditors.PanelControl();
            this.gr_divide_Table_2 = new DevExpress.XtraGrid.GridControl();
            this.gv_divide_Table_2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.@__STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__GIABAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__Thanh_Tien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__MA_BEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__IS_COMBO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnInforTablecheck = new DevExpress.XtraEditors.PanelControl();
            this.panelControl18 = new DevExpress.XtraEditors.PanelControl();
            this.lb_TenKH = new DevExpress.XtraEditors.LabelControl();
            this.lb_KhuVuc = new DevExpress.XtraEditors.LabelControl();
            this.lb_Ban = new DevExpress.XtraEditors.LabelControl();
            this.panelControl14 = new DevExpress.XtraEditors.PanelControl();
            this.txtTenKhacHang = new DevExpress.XtraEditors.TextEdit();
            this.txtmaBanGoc = new DevExpress.XtraEditors.TextEdit();
            this.txtmaKVBanGoc = new DevExpress.XtraEditors.TextEdit();
            this.pn_TableShare = new DevExpress.XtraEditors.PanelControl();
            this.txtTenKhachHangGhep = new DevExpress.XtraEditors.TextEdit();
            this.txtmaBanGhep = new DevExpress.XtraEditors.TextEdit();
            this.txtmaKVBanGhep = new DevExpress.XtraEditors.TextEdit();
            this.pnYeucauthem = new DevExpress.XtraEditors.PanelControl();
            this.panelControl19 = new DevExpress.XtraEditors.PanelControl();
            this.pnYeuCau = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl22 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton23 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton24 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.us_keyboard3 = new KP_RES.Us_keyboard();
            this.panelControl21 = new DevExpress.XtraEditors.PanelControl();
            this.txtYeuCauThem = new DevExpress.XtraEditors.TextEdit();
            this.panelControl17 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.lbMonYeuCauThem = new DevExpress.XtraEditors.LabelControl();
            this.lbTableYeuCauThem = new DevExpress.XtraEditors.LabelControl();
            this.lbkvYeuCauThem = new DevExpress.XtraEditors.LabelControl();
            this.pnDatban = new DevExpress.XtraEditors.PanelControl();
            this.panelControl45 = new DevExpress.XtraEditors.PanelControl();
            this.us_keyboard2 = new KP_RES.Us_keyboard();
            this.panelControl46 = new DevExpress.XtraEditors.PanelControl();
            this.pnInforTable = new DevExpress.XtraEditors.GroupControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtngay = new DevExpress.XtraEditors.DateEdit();
            this.btn_Danhsachdatban = new DevExpress.XtraEditors.SimpleButton();
            this.chkEmail = new DevExpress.XtraEditors.CheckEdit();
            this.txtEmail = new DevExpress.XtraEditors.TextEdit();
            this.btnMoBanNgay = new DevExpress.XtraEditors.SimpleButton();
            this.txtPartMoneyPayments = new DevExpress.XtraEditors.TextEdit();
            this.btnMobansau = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Close = new DevExpress.XtraEditors.SimpleButton();
            this.btnRemove = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl48 = new DevExpress.XtraEditors.PanelControl();
            this.flCheckTable = new System.Windows.Forms.FlowLayoutPanel();
            this.cbMM = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbHH = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.btnSendEmail = new DevExpress.XtraEditors.SimpleButton();
            this.lb_Gio = new DevExpress.XtraEditors.LabelControl();
            this.btn_SelectDishes = new DevExpress.XtraEditors.SimpleButton();
            this.btn_SelectTable = new DevExpress.XtraEditors.SimpleButton();
            this.lb_Diachi = new DevExpress.XtraEditors.LabelControl();
            this.lb_Tiendatcoc = new DevExpress.XtraEditors.LabelControl();
            this.lb_ngay2 = new DevExpress.XtraEditors.LabelControl();
            this.lb_dienthoai2 = new DevExpress.XtraEditors.LabelControl();
            this.lb_TenKH2 = new DevExpress.XtraEditors.LabelControl();
            this.txtAddress = new DevExpress.XtraEditors.TextEdit();
            this.txtDienThoai = new DevExpress.XtraEditors.TextEdit();
            this.txtDBTenKH = new DevExpress.XtraEditors.TextEdit();
            this.pnInlaihoadon = new DevExpress.XtraEditors.PanelControl();
            this.panelControl53 = new DevExpress.XtraEditors.PanelControl();
            this.gr_billPaymented = new DevExpress.XtraGrid.GridControl();
            this.gvBillPaymented = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SSTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HOADON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYTAO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_KHUVUC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_BAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.HUY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.LAYLAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.panelControl52 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.lbTongSoRow = new DevExpress.XtraEditors.LabelControl();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.btn_upbill = new DevExpress.XtraEditors.SimpleButton();
            this.btnDowBill = new DevExpress.XtraEditors.SimpleButton();
            this.btnDowLastBill = new DevExpress.XtraEditors.SimpleButton();
            this.btn_UpTopBill = new DevExpress.XtraEditors.SimpleButton();
            this.pnWapper = new DevExpress.XtraEditors.PanelControl();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.pnHeader = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.lb_Quay = new DevExpress.XtraEditors.LabelControl();
            this.lb_Kho = new DevExpress.XtraEditors.LabelControl();
            this.lbgiay = new DevExpress.XtraEditors.LabelControl();
            this.lbCaBan = new DevExpress.XtraEditors.LabelControl();
            this.lbGio = new DevExpress.XtraEditors.LabelControl();
            this.lb_NhanVien = new DevExpress.XtraEditors.LabelControl();
            this.lbtimeby = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pnShowHang)).BeginInit();
            this.pnShowHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBuffet)).BeginInit();
            this.pnlBuffet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).BeginInit();
            this.panelControl16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnKV_Mon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl27)).BeginInit();
            this.panelControl27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl28)).BeginInit();
            this.panelControl28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl37)).BeginInit();
            this.panelControl37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnMain)).BeginInit();
            this.pnMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_listProduct)).BeginInit();
            this.pn_listProduct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_SelectProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_SelectProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDowGv)).BeginInit();
            this.pnUpDowGv.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnBtnDeleteGv)).BeginInit();
            this.pnBtnDeleteGv.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl29)).BeginInit();
            this.panelControl29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl33)).BeginInit();
            this.panelControl33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl31)).BeginInit();
            this.panelControl31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl32)).BeginInit();
            this.panelControl32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl20)).BeginInit();
            this.panelControl20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInforMoney)).BeginInit();
            this.pnlInforMoney.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).BeginInit();
            this.panelControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl23)).BeginInit();
            this.panelControl23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl50)).BeginInit();
            this.panelControl50.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnNghiepVu_ThuNgan)).BeginInit();
            this.pnNghiepVu_ThuNgan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnDacThu)).BeginInit();
            this.pnDacThu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnThuNgan)).BeginInit();
            this.pnThuNgan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnNghiepVu)).BeginInit();
            this.pnNghiepVu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnTachban)).BeginInit();
            this.pnTachban.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_GroupTable)).BeginInit();
            this.pn_GroupTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDow)).BeginInit();
            this.pnUpDow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            this.panelControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_divide_Table_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_divide_Table_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_TableTow)).BeginInit();
            this.pn_TableTow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_divide_Table_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_divide_Table_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnInforTablecheck)).BeginInit();
            this.pnInforTablecheck.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).BeginInit();
            this.panelControl18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).BeginInit();
            this.panelControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenKhacHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmaBanGoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmaKVBanGoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_TableShare)).BeginInit();
            this.pn_TableShare.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenKhachHangGhep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmaBanGhep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmaKVBanGhep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnYeucauthem)).BeginInit();
            this.pnYeucauthem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).BeginInit();
            this.panelControl19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).BeginInit();
            this.panelControl22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl21)).BeginInit();
            this.panelControl21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtYeuCauThem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).BeginInit();
            this.panelControl17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnDatban)).BeginInit();
            this.pnDatban.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl45)).BeginInit();
            this.panelControl45.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl46)).BeginInit();
            this.panelControl46.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnInforTable)).BeginInit();
            this.pnInforTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtngay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtngay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartMoneyPayments.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl48)).BeginInit();
            this.panelControl48.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbMM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbHH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienThoai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDBTenKH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnInlaihoadon)).BeginInit();
            this.pnInlaihoadon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl53)).BeginInit();
            this.panelControl53.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_billPaymented)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBillPaymented)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl52)).BeginInit();
            this.panelControl52.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnWapper)).BeginInit();
            this.pnWapper.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnHeader)).BeginInit();
            this.pnHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnShowHang
            // 
            this.pnShowHang.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnShowHang.Controls.Add(this.panelControl2);
            this.pnShowHang.Controls.Add(this.panelControl4);
            this.pnShowHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnShowHang.Location = new System.Drawing.Point(0, 0);
            this.pnShowHang.Name = "pnShowHang";
            this.pnShowHang.Size = new System.Drawing.Size(674, 698);
            this.pnShowHang.TabIndex = 1;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.pnTable_Mon);
            this.panelControl2.Controls.Add(this.panelControl9);
            this.panelControl2.Controls.Add(this.panelControl7);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(539, 698);
            this.panelControl2.TabIndex = 5;
            // 
            // pnTable_Mon
            // 
            this.pnTable_Mon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnTable_Mon.Location = new System.Drawing.Point(2, 47);
            this.pnTable_Mon.Name = "pnTable_Mon";
            this.pnTable_Mon.Size = new System.Drawing.Size(535, 604);
            this.pnTable_Mon.TabIndex = 0;
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.labelControl3);
            this.panelControl9.Controls.Add(this.labelControl4);
            this.panelControl9.Controls.Add(this.lbPageTable);
            this.panelControl9.Controls.Add(this.btn_NextRight);
            this.panelControl9.Controls.Add(this.btn_NextToLeft);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl9.Location = new System.Drawing.Point(2, 651);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(535, 45);
            this.panelControl9.TabIndex = 1;
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl3.Location = new System.Drawing.Point(262, 17);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(6, 13);
            this.labelControl3.TabIndex = 10;
            this.labelControl3.Text = "/";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl4.Location = new System.Drawing.Point(280, 17);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(14, 13);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "20";
            // 
            // lbPageTable
            // 
            this.lbPageTable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lbPageTable.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbPageTable.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbPageTable.Location = new System.Drawing.Point(243, 17);
            this.lbPageTable.Name = "lbPageTable";
            this.lbPageTable.Size = new System.Drawing.Size(7, 13);
            this.lbPageTable.TabIndex = 8;
            this.lbPageTable.Text = "1";
            // 
            // btn_NextRight
            // 
            this.btn_NextRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_NextRight.Image = global::KP_RES.Properties.Resources.up33_26;
            this.btn_NextRight.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_NextRight.Location = new System.Drawing.Point(402, 2);
            this.btn_NextRight.Name = "btn_NextRight";
            this.btn_NextRight.Size = new System.Drawing.Size(131, 41);
            this.btn_NextRight.TabIndex = 1;
            this.btn_NextRight.Text = "simpleButton16";
            this.btn_NextRight.Click += new System.EventHandler(this.btn_NextToRight_Click);
            // 
            // btn_NextToLeft
            // 
            this.btn_NextToLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_NextToLeft.Image = global::KP_RES.Properties.Resources.up44_26;
            this.btn_NextToLeft.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_NextToLeft.Location = new System.Drawing.Point(2, 2);
            this.btn_NextToLeft.Name = "btn_NextToLeft";
            this.btn_NextToLeft.Size = new System.Drawing.Size(131, 41);
            this.btn_NextToLeft.TabIndex = 0;
            this.btn_NextToLeft.Text = "simpleButton15";
            this.btn_NextToLeft.Click += new System.EventHandler(this.btn_NextToLeft_Click);
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.pnlBuffet);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(2, 2);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(535, 45);
            this.panelControl7.TabIndex = 0;
            // 
            // pnlBuffet
            // 
            this.pnlBuffet.Controls.Add(this.pnBuffet);
            this.pnlBuffet.Controls.Add(this.panelControl12);
            this.pnlBuffet.Controls.Add(this.panelControl16);
            this.pnlBuffet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBuffet.Location = new System.Drawing.Point(2, 2);
            this.pnlBuffet.Name = "pnlBuffet";
            this.pnlBuffet.Size = new System.Drawing.Size(531, 41);
            this.pnlBuffet.TabIndex = 5;
            // 
            // pnBuffet
            // 
            this.pnBuffet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnBuffet.Location = new System.Drawing.Point(53, 2);
            this.pnBuffet.Name = "pnBuffet";
            this.pnBuffet.Size = new System.Drawing.Size(425, 37);
            this.pnBuffet.TabIndex = 26;
            // 
            // panelControl12
            // 
            this.panelControl12.Controls.Add(this.btn_UpBF);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl12.Location = new System.Drawing.Point(2, 2);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(51, 37);
            this.panelControl12.TabIndex = 4;
            // 
            // btn_UpBF
            // 
            this.btn_UpBF.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_UpBF.Image = global::KP_RES.Properties.Resources.up44_26;
            this.btn_UpBF.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_UpBF.Location = new System.Drawing.Point(2, 2);
            this.btn_UpBF.Name = "btn_UpBF";
            this.btn_UpBF.Size = new System.Drawing.Size(47, 33);
            this.btn_UpBF.TabIndex = 0;
            this.btn_UpBF.Text = "simpleButton17";
            this.btn_UpBF.Click += new System.EventHandler(this.btn_UpBF_Click);
            // 
            // panelControl16
            // 
            this.panelControl16.Controls.Add(this.btn_DowBF);
            this.panelControl16.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl16.Location = new System.Drawing.Point(478, 2);
            this.panelControl16.Name = "panelControl16";
            this.panelControl16.Size = new System.Drawing.Size(51, 37);
            this.panelControl16.TabIndex = 3;
            // 
            // btn_DowBF
            // 
            this.btn_DowBF.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_DowBF.Image = global::KP_RES.Properties.Resources.up33_26;
            this.btn_DowBF.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_DowBF.Location = new System.Drawing.Point(2, 2);
            this.btn_DowBF.Name = "btn_DowBF";
            this.btn_DowBF.Size = new System.Drawing.Size(47, 33);
            this.btn_DowBF.TabIndex = 1;
            this.btn_DowBF.Text = "simpleButton18";
            this.btn_DowBF.Click += new System.EventHandler(this.btn_DowBF_Click);
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.pnKV_Mon);
            this.panelControl4.Controls.Add(this.panelControl27);
            this.panelControl4.Controls.Add(this.panelControl28);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl4.Location = new System.Drawing.Point(539, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(135, 698);
            this.panelControl4.TabIndex = 4;
            // 
            // pnKV_Mon
            // 
            this.pnKV_Mon.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnKV_Mon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnKV_Mon.Location = new System.Drawing.Point(2, 47);
            this.pnKV_Mon.Name = "pnKV_Mon";
            this.pnKV_Mon.Size = new System.Drawing.Size(131, 604);
            this.pnKV_Mon.TabIndex = 2;
            // 
            // panelControl27
            // 
            this.panelControl27.Controls.Add(this.btn_DowKV);
            this.panelControl27.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl27.Location = new System.Drawing.Point(2, 651);
            this.panelControl27.Name = "panelControl27";
            this.panelControl27.Size = new System.Drawing.Size(131, 45);
            this.panelControl27.TabIndex = 3;
            // 
            // btn_DowKV
            // 
            this.btn_DowKV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_DowKV.Image = ((System.Drawing.Image)(resources.GetObject("btn_DowKV.Image")));
            this.btn_DowKV.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_DowKV.Location = new System.Drawing.Point(2, 2);
            this.btn_DowKV.Name = "btn_DowKV";
            this.btn_DowKV.Size = new System.Drawing.Size(127, 41);
            this.btn_DowKV.TabIndex = 1;
            this.btn_DowKV.Text = "simpleButton18";
            this.btn_DowKV.Click += new System.EventHandler(this.btn_DowKV_Click);
            // 
            // panelControl28
            // 
            this.panelControl28.Controls.Add(this.btn_UpKV);
            this.panelControl28.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl28.Location = new System.Drawing.Point(2, 2);
            this.panelControl28.Name = "panelControl28";
            this.panelControl28.Size = new System.Drawing.Size(131, 45);
            this.panelControl28.TabIndex = 4;
            // 
            // btn_UpKV
            // 
            this.btn_UpKV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_UpKV.Image = ((System.Drawing.Image)(resources.GetObject("btn_UpKV.Image")));
            this.btn_UpKV.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_UpKV.Location = new System.Drawing.Point(2, 2);
            this.btn_UpKV.Name = "btn_UpKV";
            this.btn_UpKV.Size = new System.Drawing.Size(127, 41);
            this.btn_UpKV.TabIndex = 0;
            this.btn_UpKV.Text = "simpleButton17";
            this.btn_UpKV.Click += new System.EventHandler(this.btn_UpKV_Click);
            // 
            // panelControl37
            // 
            this.panelControl37.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl37.Controls.Add(this.lbMon);
            this.panelControl37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl37.Location = new System.Drawing.Point(2, 33);
            this.panelControl37.Name = "panelControl37";
            this.panelControl37.Size = new System.Drawing.Size(344, 31);
            this.panelControl37.TabIndex = 0;
            // 
            // lbMon
            // 
            this.lbMon.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbMon.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbMon.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbMon.Location = new System.Drawing.Point(244, 0);
            this.lbMon.Name = "lbMon";
            this.lbMon.Size = new System.Drawing.Size(100, 19);
            this.lbMon.TabIndex = 2;
            this.lbMon.Text = "     Lẩu Bò Tái";
            // 
            // lbKhuVuc
            // 
            this.lbKhuVuc.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbKhuVuc.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbKhuVuc.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbKhuVuc.Location = new System.Drawing.Point(132, 0);
            this.lbKhuVuc.Name = "lbKhuVuc";
            this.lbKhuVuc.Size = new System.Drawing.Size(130, 19);
            this.lbKhuVuc.TabIndex = 0;
            this.lbKhuVuc.Text = "Khu Vực 1         >";
            // 
            // lbBan
            // 
            this.lbBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbBan.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbBan.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbBan.Location = new System.Drawing.Point(262, 0);
            this.lbBan.Name = "lbBan";
            this.lbBan.Size = new System.Drawing.Size(82, 19);
            this.lbBan.TabIndex = 1;
            this.lbBan.Text = "    Bàn 1  >";
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(0, 1);
            this.txtID.Name = "txtID";
            this.txtID.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtID.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.Properties.Appearance.Options.UseFont = true;
            this.txtID.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtID.Properties.AppearanceDropDown.Options.UseFont = true;
            this.txtID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "MA", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.txtID.Properties.DisplayMember = "TEN";
            this.txtID.Properties.DropDownItemHeight = 30;
            this.txtID.Properties.DropDownRows = 15;
            this.txtID.Properties.NullText = "";
            this.txtID.Properties.ShowHeader = false;
            this.txtID.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.txtID.Properties.ValueMember = "MA";
            this.txtID.Size = new System.Drawing.Size(184, 26);
            this.txtID.TabIndex = 5;
            this.txtID.Visible = false;
            this.txtID.Click += new System.EventHandler(this.txtID_Click);
            this.txtID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtID_KeyDown);
            // 
            // pnMain
            // 
            this.pnMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnMain.Controls.Add(this.pn_listProduct);
            this.pnMain.Controls.Add(this.pnNghiepVu_ThuNgan);
            this.pnMain.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnMain.Location = new System.Drawing.Point(674, 0);
            this.pnMain.Name = "pnMain";
            this.pnMain.Size = new System.Drawing.Size(350, 698);
            this.pnMain.TabIndex = 2;
            // 
            // pn_listProduct
            // 
            this.pn_listProduct.Controls.Add(this.gr_SelectProduct);
            this.pn_listProduct.Controls.Add(this.pnUpDowGv);
            this.pn_listProduct.Controls.Add(this.pnBtnDeleteGv);
            this.pn_listProduct.Controls.Add(this.panelControl20);
            this.pn_listProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_listProduct.Location = new System.Drawing.Point(0, 0);
            this.pn_listProduct.Name = "pn_listProduct";
            this.pn_listProduct.Size = new System.Drawing.Size(350, 440);
            this.pn_listProduct.TabIndex = 0;
            // 
            // gr_SelectProduct
            // 
            this.gr_SelectProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gr_SelectProduct.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gr_SelectProduct.Location = new System.Drawing.Point(2, 2);
            this.gr_SelectProduct.MainView = this.gv_SelectProduct;
            this.gr_SelectProduct.Name = "gr_SelectProduct";
            this.gr_SelectProduct.Size = new System.Drawing.Size(313, 217);
            this.gr_SelectProduct.TabIndex = 19;
            this.gr_SelectProduct.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv_SelectProduct});
            // 
            // gv_SelectProduct
            // 
            this.gv_SelectProduct.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.gv_SelectProduct.Appearance.FocusedRow.Options.UseFont = true;
            this.gv_SelectProduct.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Red;
            this.gv_SelectProduct.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gv_SelectProduct.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.75F);
            this.gv_SelectProduct.Appearance.Row.Options.UseFont = true;
            this.gv_SelectProduct.ColumnPanelRowHeight = 43;
            this.gv_SelectProduct.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SOTT,
            this.TEN_HANGHOA,
            this.SOLUONG,
            this.GIABAN,
            this.CHIETKHAU,
            this.Thanh_Tien,
            this.MA_HANGHOA,
            this.GHICHU,
            this.MA_BEP,
            this.THUE,
            this.TRANGTHAI,
            this.IS_COMBO,
            this.IS_INBEP,
            this.IS_KHUYENMAI});
            this.gv_SelectProduct.GridControl = this.gr_SelectProduct;
            this.gv_SelectProduct.Name = "gv_SelectProduct";
            this.gv_SelectProduct.OptionsView.ShowGroupPanel = false;
            this.gv_SelectProduct.OptionsView.ShowIndicator = false;
            this.gv_SelectProduct.RowHeight = 35;
            this.gv_SelectProduct.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gv_SelectProduct.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gv_SelectProduct_RowCellClick);
            this.gv_SelectProduct.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gv_SelectProduct_CustomDrawCell);
            this.gv_SelectProduct.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gv_SelectProduct_FocusedRowChanged);
            // 
            // SOTT
            // 
            this.SOTT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.SOTT.AppearanceCell.Options.UseFont = true;
            this.SOTT.AppearanceCell.Options.UseTextOptions = true;
            this.SOTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOTT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.SOTT.AppearanceHeader.Options.UseFont = true;
            this.SOTT.AppearanceHeader.Options.UseTextOptions = true;
            this.SOTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOTT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOTT.Caption = "TT";
            this.SOTT.FieldName = "SOTT";
            this.SOTT.Name = "SOTT";
            this.SOTT.OptionsColumn.AllowEdit = false;
            this.SOTT.OptionsColumn.AllowFocus = false;
            this.SOTT.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.SOTT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.SOTT.OptionsColumn.FixedWidth = true;
            this.SOTT.OptionsFilter.AllowFilter = false;
            this.SOTT.Width = 37;
            // 
            // TEN_HANGHOA
            // 
            this.TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_HANGHOA.Caption = "Tên Hàng Hóa";
            this.TEN_HANGHOA.FieldName = "TEN_HANGHOA";
            this.TEN_HANGHOA.Name = "TEN_HANGHOA";
            this.TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this.TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this.TEN_HANGHOA.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.TEN_HANGHOA.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TEN_HANGHOA.OptionsFilter.AllowFilter = false;
            this.TEN_HANGHOA.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.TEN_HANGHOA.Visible = true;
            this.TEN_HANGHOA.VisibleIndex = 0;
            this.TEN_HANGHOA.Width = 61;
            // 
            // SOLUONG
            // 
            this.SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.SOLUONG.AppearanceCell.Options.UseFont = true;
            this.SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.Caption = "SL";
            this.SOLUONG.FieldName = "SOLUONG";
            this.SOLUONG.Name = "SOLUONG";
            this.SOLUONG.OptionsColumn.AllowEdit = false;
            this.SOLUONG.OptionsColumn.AllowFocus = false;
            this.SOLUONG.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.SOLUONG.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.SOLUONG.OptionsColumn.FixedWidth = true;
            this.SOLUONG.OptionsFilter.AllowFilter = false;
            this.SOLUONG.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.SOLUONG.Visible = true;
            this.SOLUONG.VisibleIndex = 1;
            this.SOLUONG.Width = 37;
            // 
            // GIABAN
            // 
            this.GIABAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GIABAN.AppearanceCell.Options.UseFont = true;
            this.GIABAN.AppearanceCell.Options.UseTextOptions = true;
            this.GIABAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIABAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.GIABAN.AppearanceHeader.Options.UseFont = true;
            this.GIABAN.AppearanceHeader.Options.UseTextOptions = true;
            this.GIABAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIABAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN.Caption = "Đơn giá";
            this.GIABAN.DisplayFormat.FormatString = "{0:#,###0}";
            this.GIABAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GIABAN.FieldName = "GIABAN";
            this.GIABAN.Name = "GIABAN";
            this.GIABAN.OptionsColumn.AllowEdit = false;
            this.GIABAN.OptionsColumn.AllowFocus = false;
            this.GIABAN.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.GIABAN.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.GIABAN.OptionsColumn.FixedWidth = true;
            this.GIABAN.OptionsFilter.AllowFilter = false;
            this.GIABAN.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.GIABAN.Visible = true;
            this.GIABAN.VisibleIndex = 2;
            this.GIABAN.Width = 60;
            // 
            // CHIETKHAU
            // 
            this.CHIETKHAU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.CHIETKHAU.AppearanceCell.Options.UseFont = true;
            this.CHIETKHAU.AppearanceCell.Options.UseTextOptions = true;
            this.CHIETKHAU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CHIETKHAU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHIETKHAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.CHIETKHAU.AppearanceHeader.Options.UseFont = true;
            this.CHIETKHAU.AppearanceHeader.Options.UseTextOptions = true;
            this.CHIETKHAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHIETKHAU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHIETKHAU.Caption = "CK";
            this.CHIETKHAU.DisplayFormat.FormatString = "{0:#,###0}%";
            this.CHIETKHAU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CHIETKHAU.FieldName = "CHIETKHAU";
            this.CHIETKHAU.Name = "CHIETKHAU";
            this.CHIETKHAU.OptionsColumn.AllowEdit = false;
            this.CHIETKHAU.OptionsColumn.AllowFocus = false;
            this.CHIETKHAU.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.CHIETKHAU.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.CHIETKHAU.OptionsColumn.FixedWidth = true;
            this.CHIETKHAU.OptionsFilter.AllowFilter = false;
            this.CHIETKHAU.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.CHIETKHAU.Visible = true;
            this.CHIETKHAU.VisibleIndex = 3;
            this.CHIETKHAU.Width = 50;
            // 
            // Thanh_Tien
            // 
            this.Thanh_Tien.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Thanh_Tien.AppearanceCell.Options.UseFont = true;
            this.Thanh_Tien.AppearanceCell.Options.UseTextOptions = true;
            this.Thanh_Tien.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Thanh_Tien.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Thanh_Tien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.Thanh_Tien.AppearanceHeader.Options.UseFont = true;
            this.Thanh_Tien.AppearanceHeader.Options.UseTextOptions = true;
            this.Thanh_Tien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Thanh_Tien.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Thanh_Tien.Caption = "Thành Tiền";
            this.Thanh_Tien.DisplayFormat.FormatString = "{0:#,###0}";
            this.Thanh_Tien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.Thanh_Tien.FieldName = "Thanh_Tien";
            this.Thanh_Tien.Name = "Thanh_Tien";
            this.Thanh_Tien.OptionsColumn.AllowEdit = false;
            this.Thanh_Tien.OptionsColumn.AllowFocus = false;
            this.Thanh_Tien.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.Thanh_Tien.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.Thanh_Tien.OptionsColumn.FixedWidth = true;
            this.Thanh_Tien.OptionsFilter.AllowFilter = false;
            this.Thanh_Tien.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.Thanh_Tien.Visible = true;
            this.Thanh_Tien.VisibleIndex = 4;
            this.Thanh_Tien.Width = 78;
            // 
            // MA_HANGHOA
            // 
            this.MA_HANGHOA.Caption = "Mã Hàng Hóa";
            this.MA_HANGHOA.FieldName = "MA_HANGHOA";
            this.MA_HANGHOA.Name = "MA_HANGHOA";
            this.MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this.MA_HANGHOA.OptionsColumn.AllowFocus = false;
            // 
            // GHICHU
            // 
            this.GHICHU.Caption = "Ghi Chú";
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.Name = "GHICHU";
            // 
            // MA_BEP
            // 
            this.MA_BEP.Caption = "Máy In Bếp";
            this.MA_BEP.FieldName = "MA_BEP";
            this.MA_BEP.Name = "MA_BEP";
            // 
            // THUE
            // 
            this.THUE.Caption = "VAT";
            this.THUE.FieldName = "THUE";
            this.THUE.Name = "THUE";
            // 
            // TRANGTHAI
            // 
            this.TRANGTHAI.Caption = "TRANGTHAI";
            this.TRANGTHAI.FieldName = "TRANGTHAI";
            this.TRANGTHAI.Name = "TRANGTHAI";
            this.TRANGTHAI.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI.OptionsColumn.AllowFocus = false;
            // 
            // IS_COMBO
            // 
            this.IS_COMBO.Caption = "IS_COMBO";
            this.IS_COMBO.FieldName = "IS_COMBO";
            this.IS_COMBO.Name = "IS_COMBO";
            // 
            // IS_INBEP
            // 
            this.IS_INBEP.Caption = "IS_INBEP";
            this.IS_INBEP.FieldName = "IS_INBEP";
            this.IS_INBEP.Name = "IS_INBEP";
            // 
            // IS_KHUYENMAI
            // 
            this.IS_KHUYENMAI.Caption = "IS_KHUYENMAI";
            this.IS_KHUYENMAI.FieldName = "IS_KHUYENMAI";
            this.IS_KHUYENMAI.Name = "IS_KHUYENMAI";
            // 
            // pnUpDowGv
            // 
            this.pnUpDowGv.Controls.Add(this.btn_up);
            this.pnUpDowGv.Controls.Add(this.btn_Dow);
            this.pnUpDowGv.Controls.Add(this.btn_DowLast);
            this.pnUpDowGv.Controls.Add(this.btn_UpFirst);
            this.pnUpDowGv.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnUpDowGv.Location = new System.Drawing.Point(315, 2);
            this.pnUpDowGv.Name = "pnUpDowGv";
            this.pnUpDowGv.Size = new System.Drawing.Size(33, 217);
            this.pnUpDowGv.TabIndex = 17;
            // 
            // btn_up
            // 
            this.btn_up.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_up.Image = ((System.Drawing.Image)(resources.GetObject("btn_up.Image")));
            this.btn_up.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_up.Location = new System.Drawing.Point(2, 42);
            this.btn_up.Name = "btn_up";
            this.btn_up.Size = new System.Drawing.Size(29, 45);
            this.btn_up.TabIndex = 4;
            this.btn_up.Text = "simpleButton7";
            this.btn_up.Click += new System.EventHandler(this.btn_up_Click);
            // 
            // btn_Dow
            // 
            this.btn_Dow.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_Dow.Image = ((System.Drawing.Image)(resources.GetObject("btn_Dow.Image")));
            this.btn_Dow.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_Dow.Location = new System.Drawing.Point(2, 130);
            this.btn_Dow.Name = "btn_Dow";
            this.btn_Dow.Size = new System.Drawing.Size(29, 45);
            this.btn_Dow.TabIndex = 3;
            this.btn_Dow.Click += new System.EventHandler(this.btn_Dow_Click);
            // 
            // btn_DowLast
            // 
            this.btn_DowLast.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_DowLast.Image = ((System.Drawing.Image)(resources.GetObject("btn_DowLast.Image")));
            this.btn_DowLast.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_DowLast.Location = new System.Drawing.Point(2, 175);
            this.btn_DowLast.Name = "btn_DowLast";
            this.btn_DowLast.Size = new System.Drawing.Size(29, 40);
            this.btn_DowLast.TabIndex = 2;
            this.btn_DowLast.Text = "simpleButton4";
            this.btn_DowLast.Click += new System.EventHandler(this.btn_DowLast_Click);
            // 
            // btn_UpFirst
            // 
            this.btn_UpFirst.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_UpFirst.Image = ((System.Drawing.Image)(resources.GetObject("btn_UpFirst.Image")));
            this.btn_UpFirst.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_UpFirst.Location = new System.Drawing.Point(2, 2);
            this.btn_UpFirst.Name = "btn_UpFirst";
            this.btn_UpFirst.Size = new System.Drawing.Size(29, 40);
            this.btn_UpFirst.TabIndex = 0;
            this.btn_UpFirst.Text = "simpleButton1";
            this.btn_UpFirst.Click += new System.EventHandler(this.btn_UpFirst_Click);
            // 
            // pnBtnDeleteGv
            // 
            this.pnBtnDeleteGv.Controls.Add(this.panelControl29);
            this.pnBtnDeleteGv.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBtnDeleteGv.Location = new System.Drawing.Point(2, 219);
            this.pnBtnDeleteGv.Name = "pnBtnDeleteGv";
            this.pnBtnDeleteGv.Size = new System.Drawing.Size(346, 151);
            this.pnBtnDeleteGv.TabIndex = 18;
            // 
            // panelControl29
            // 
            this.panelControl29.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl29.Controls.Add(this.panelControl33);
            this.panelControl29.Controls.Add(this.panelControl31);
            this.panelControl29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl29.Location = new System.Drawing.Point(2, 2);
            this.panelControl29.Name = "panelControl29";
            this.panelControl29.Size = new System.Drawing.Size(342, 147);
            this.panelControl29.TabIndex = 20;
            // 
            // panelControl33
            // 
            this.panelControl33.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl33.Controls.Add(this.btn_tru);
            this.panelControl33.Controls.Add(this.btn_XoaTatCa);
            this.panelControl33.Controls.Add(this.btn_cong);
            this.panelControl33.Controls.Add(this.btnOK);
            this.panelControl33.Controls.Add(this.btn_xoa);
            this.panelControl33.Controls.Add(this.btn9);
            this.panelControl33.Controls.Add(this.btn8);
            this.panelControl33.Controls.Add(this.btn7);
            this.panelControl33.Controls.Add(this.btn6);
            this.panelControl33.Controls.Add(this.btn5);
            this.panelControl33.Controls.Add(this.btn4);
            this.panelControl33.Controls.Add(this.btn3);
            this.panelControl33.Controls.Add(this.btn2);
            this.panelControl33.Controls.Add(this.btn1);
            this.panelControl33.Controls.Add(this.btn0);
            this.panelControl33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl33.Location = new System.Drawing.Point(0, 24);
            this.panelControl33.Name = "panelControl33";
            this.panelControl33.Size = new System.Drawing.Size(342, 123);
            this.panelControl33.TabIndex = 22;
            // 
            // btn_tru
            // 
            this.btn_tru.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn_tru.Appearance.Options.UseFont = true;
            this.btn_tru.Location = new System.Drawing.Point(189, 43);
            this.btn_tru.Name = "btn_tru";
            this.btn_tru.Size = new System.Drawing.Size(55, 36);
            this.btn_tru.TabIndex = 1;
            this.btn_tru.Text = "-";
            this.btn_tru.Click += new System.EventHandler(this.btn_tru_Click);
            // 
            // btn_XoaTatCa
            // 
            this.btn_XoaTatCa.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_XoaTatCa.Appearance.Options.UseFont = true;
            this.btn_XoaTatCa.Location = new System.Drawing.Point(250, 83);
            this.btn_XoaTatCa.Name = "btn_XoaTatCa";
            this.btn_XoaTatCa.Size = new System.Drawing.Size(86, 36);
            this.btn_XoaTatCa.TabIndex = 0;
            this.btn_XoaTatCa.Text = "Xóa tất cả";
            this.btn_XoaTatCa.Click += new System.EventHandler(this.btn_XoaTatCa_Click);
            // 
            // btn_cong
            // 
            this.btn_cong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn_cong.Appearance.Options.UseFont = true;
            this.btn_cong.Location = new System.Drawing.Point(189, 3);
            this.btn_cong.Name = "btn_cong";
            this.btn_cong.Size = new System.Drawing.Size(55, 36);
            this.btn_cong.TabIndex = 3;
            this.btn_cong.Text = "+";
            this.btn_cong.Click += new System.EventHandler(this.btn_cong_Click);
            // 
            // btnOK
            // 
            this.btnOK.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnOK.Appearance.Options.UseFont = true;
            this.btnOK.Location = new System.Drawing.Point(250, 3);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(86, 36);
            this.btnOK.TabIndex = 26;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btn_xoa
            // 
            this.btn_xoa.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_xoa.Appearance.Options.UseFont = true;
            this.btn_xoa.Location = new System.Drawing.Point(250, 43);
            this.btn_xoa.Name = "btn_xoa";
            this.btn_xoa.Size = new System.Drawing.Size(86, 36);
            this.btn_xoa.TabIndex = 2;
            this.btn_xoa.Text = "Xóa";
            this.btn_xoa.Click += new System.EventHandler(this.btn_xoa_Click);
            // 
            // btn9
            // 
            this.btn9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn9.Appearance.Options.UseFont = true;
            this.btn9.Location = new System.Drawing.Point(127, 3);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(55, 36);
            this.btn9.TabIndex = 25;
            this.btn9.Text = "9";
            this.btn9.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn8
            // 
            this.btn8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn8.Appearance.Options.UseFont = true;
            this.btn8.Location = new System.Drawing.Point(66, 3);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(55, 36);
            this.btn8.TabIndex = 24;
            this.btn8.Text = "8";
            this.btn8.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn7
            // 
            this.btn7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn7.Appearance.Options.UseFont = true;
            this.btn7.Location = new System.Drawing.Point(5, 3);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(55, 36);
            this.btn7.TabIndex = 23;
            this.btn7.Text = "7";
            this.btn7.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn6
            // 
            this.btn6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn6.Appearance.Options.UseFont = true;
            this.btn6.Location = new System.Drawing.Point(127, 43);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(55, 36);
            this.btn6.TabIndex = 22;
            this.btn6.Text = "6";
            this.btn6.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn5
            // 
            this.btn5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn5.Appearance.Options.UseFont = true;
            this.btn5.Location = new System.Drawing.Point(66, 43);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(55, 36);
            this.btn5.TabIndex = 21;
            this.btn5.Text = "5";
            this.btn5.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn4
            // 
            this.btn4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn4.Appearance.Options.UseFont = true;
            this.btn4.Location = new System.Drawing.Point(5, 43);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(55, 36);
            this.btn4.TabIndex = 20;
            this.btn4.Text = "4";
            this.btn4.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn3
            // 
            this.btn3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn3.Appearance.Options.UseFont = true;
            this.btn3.Location = new System.Drawing.Point(127, 83);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(55, 36);
            this.btn3.TabIndex = 19;
            this.btn3.Text = "3";
            this.btn3.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn2
            // 
            this.btn2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn2.Appearance.Options.UseFont = true;
            this.btn2.Location = new System.Drawing.Point(66, 83);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(55, 36);
            this.btn2.TabIndex = 18;
            this.btn2.Text = "2";
            this.btn2.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn1
            // 
            this.btn1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn1.Appearance.Options.UseFont = true;
            this.btn1.Location = new System.Drawing.Point(5, 83);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(55, 36);
            this.btn1.TabIndex = 17;
            this.btn1.Text = "1";
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn0
            // 
            this.btn0.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn0.Appearance.Options.UseFont = true;
            this.btn0.Location = new System.Drawing.Point(189, 83);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(55, 36);
            this.btn0.TabIndex = 16;
            this.btn0.Text = "0";
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // panelControl31
            // 
            this.panelControl31.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl31.Controls.Add(this.panelControl32);
            this.panelControl31.Controls.Add(this.lb_soluong);
            this.panelControl31.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl31.Location = new System.Drawing.Point(0, 0);
            this.panelControl31.Name = "panelControl31";
            this.panelControl31.Size = new System.Drawing.Size(342, 24);
            this.panelControl31.TabIndex = 21;
            // 
            // panelControl32
            // 
            this.panelControl32.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl32.Controls.Add(this.lbSoluong);
            this.panelControl32.Location = new System.Drawing.Point(125, 5);
            this.panelControl32.Name = "panelControl32";
            this.panelControl32.Size = new System.Drawing.Size(170, 15);
            this.panelControl32.TabIndex = 18;
            // 
            // lbSoluong
            // 
            this.lbSoluong.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbSoluong.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbSoluong.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbSoluong.Location = new System.Drawing.Point(163, 0);
            this.lbSoluong.Name = "lbSoluong";
            this.lbSoluong.Size = new System.Drawing.Size(7, 13);
            this.lbSoluong.TabIndex = 7;
            this.lbSoluong.Text = "0";
            // 
            // lb_soluong
            // 
            this.lb_soluong.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_soluong.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_soluong.Location = new System.Drawing.Point(14, 5);
            this.lb_soluong.Name = "lb_soluong";
            this.lb_soluong.Size = new System.Drawing.Size(55, 13);
            this.lb_soluong.TabIndex = 14;
            this.lb_soluong.Text = "Số lượng :";
            // 
            // panelControl20
            // 
            this.panelControl20.Controls.Add(this.btnComboOption);
            this.panelControl20.Controls.Add(this.pnlInforMoney);
            this.panelControl20.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl20.Location = new System.Drawing.Point(2, 370);
            this.panelControl20.Name = "panelControl20";
            this.panelControl20.Size = new System.Drawing.Size(346, 68);
            this.panelControl20.TabIndex = 20;
            // 
            // btnComboOption
            // 
            this.btnComboOption.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnComboOption.Appearance.Options.UseFont = true;
            this.btnComboOption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnComboOption.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnComboOption.Location = new System.Drawing.Point(2, 2);
            this.btnComboOption.Name = "btnComboOption";
            this.btnComboOption.Size = new System.Drawing.Size(171, 64);
            this.btnComboOption.TabIndex = 27;
            this.btnComboOption.Text = "Tùy chọn  ";
            this.btnComboOption.Visible = false;
            this.btnComboOption.Click += new System.EventHandler(this.btnComboOption_Click);
            // 
            // pnlInforMoney
            // 
            this.pnlInforMoney.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlInforMoney.Controls.Add(this.panelControl13);
            this.pnlInforMoney.Controls.Add(this.panelControl23);
            this.pnlInforMoney.Controls.Add(this.panelControl50);
            this.pnlInforMoney.Controls.Add(this.lb_phuthu);
            this.pnlInforMoney.Controls.Add(this.lb_giamgia);
            this.pnlInforMoney.Controls.Add(this.lb_tongcong);
            this.pnlInforMoney.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlInforMoney.Location = new System.Drawing.Point(173, 2);
            this.pnlInforMoney.Name = "pnlInforMoney";
            this.pnlInforMoney.Size = new System.Drawing.Size(171, 64);
            this.pnlInforMoney.TabIndex = 16;
            // 
            // panelControl13
            // 
            this.panelControl13.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl13.Controls.Add(this.lbPhuThu);
            this.panelControl13.Location = new System.Drawing.Point(70, 4);
            this.panelControl13.Name = "panelControl13";
            this.panelControl13.Size = new System.Drawing.Size(93, 15);
            this.panelControl13.TabIndex = 17;
            // 
            // lbPhuThu
            // 
            this.lbPhuThu.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbPhuThu.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbPhuThu.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbPhuThu.Location = new System.Drawing.Point(86, 0);
            this.lbPhuThu.Name = "lbPhuThu";
            this.lbPhuThu.Size = new System.Drawing.Size(7, 13);
            this.lbPhuThu.TabIndex = 7;
            this.lbPhuThu.Text = "0";
            // 
            // panelControl23
            // 
            this.panelControl23.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl23.Controls.Add(this.lbGiamGia);
            this.panelControl23.Location = new System.Drawing.Point(70, 23);
            this.panelControl23.Name = "panelControl23";
            this.panelControl23.Size = new System.Drawing.Size(93, 15);
            this.panelControl23.TabIndex = 18;
            // 
            // lbGiamGia
            // 
            this.lbGiamGia.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbGiamGia.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbGiamGia.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbGiamGia.Location = new System.Drawing.Point(86, 0);
            this.lbGiamGia.Name = "lbGiamGia";
            this.lbGiamGia.Size = new System.Drawing.Size(7, 13);
            this.lbGiamGia.TabIndex = 5;
            this.lbGiamGia.Text = "0";
            // 
            // panelControl50
            // 
            this.panelControl50.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl50.Controls.Add(this.lbTotal);
            this.panelControl50.Location = new System.Drawing.Point(70, 44);
            this.panelControl50.Name = "panelControl50";
            this.panelControl50.Size = new System.Drawing.Size(93, 15);
            this.panelControl50.TabIndex = 18;
            // 
            // lbTotal
            // 
            this.lbTotal.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbTotal.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbTotal.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbTotal.Location = new System.Drawing.Point(86, 0);
            this.lbTotal.Name = "lbTotal";
            this.lbTotal.Size = new System.Drawing.Size(7, 13);
            this.lbTotal.TabIndex = 2;
            this.lbTotal.Text = "0";
            // 
            // lb_phuthu
            // 
            this.lb_phuthu.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_phuthu.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_phuthu.Location = new System.Drawing.Point(5, 5);
            this.lb_phuthu.Name = "lb_phuthu";
            this.lb_phuthu.Size = new System.Drawing.Size(46, 13);
            this.lb_phuthu.TabIndex = 6;
            this.lb_phuthu.Text = "Phụ thu:";
            // 
            // lb_giamgia
            // 
            this.lb_giamgia.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_giamgia.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_giamgia.Location = new System.Drawing.Point(5, 24);
            this.lb_giamgia.Name = "lb_giamgia";
            this.lb_giamgia.Size = new System.Drawing.Size(52, 13);
            this.lb_giamgia.TabIndex = 4;
            this.lb_giamgia.Text = "Giảm giá:";
            // 
            // lb_tongcong
            // 
            this.lb_tongcong.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_tongcong.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_tongcong.Location = new System.Drawing.Point(5, 46);
            this.lb_tongcong.Name = "lb_tongcong";
            this.lb_tongcong.Size = new System.Drawing.Size(61, 13);
            this.lb_tongcong.TabIndex = 0;
            this.lb_tongcong.Text = "Tổng cộng:";
            // 
            // pnNghiepVu_ThuNgan
            // 
            this.pnNghiepVu_ThuNgan.Controls.Add(this.pnDacThu);
            this.pnNghiepVu_ThuNgan.Controls.Add(this.pnThuNgan);
            this.pnNghiepVu_ThuNgan.Controls.Add(this.pnNghiepVu);
            this.pnNghiepVu_ThuNgan.Controls.Add(this.panelControl8);
            this.pnNghiepVu_ThuNgan.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnNghiepVu_ThuNgan.Location = new System.Drawing.Point(0, 440);
            this.pnNghiepVu_ThuNgan.Name = "pnNghiepVu_ThuNgan";
            this.pnNghiepVu_ThuNgan.Size = new System.Drawing.Size(350, 258);
            this.pnNghiepVu_ThuNgan.TabIndex = 1;
            // 
            // pnDacThu
            // 
            this.pnDacThu.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnDacThu.Controls.Add(this.btnDoihinhthucthanhtoan);
            this.pnDacThu.Controls.Add(this.btnNangcap);
            this.pnDacThu.Controls.Add(this.btnBoKhoaBan);
            this.pnDacThu.Controls.Add(this.btnHetmon);
            this.pnDacThu.Controls.Add(this.btnBaoCaoBanHang);
            this.pnDacThu.Controls.Add(this.btnChongia);
            this.pnDacThu.Controls.Add(this.btn_BanGia2);
            this.pnDacThu.Controls.Add(this.btnChietkhau);
            this.pnDacThu.Controls.Add(this.btnSuaGia);
            this.pnDacThu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnDacThu.Location = new System.Drawing.Point(2, 47);
            this.pnDacThu.Name = "pnDacThu";
            this.pnDacThu.Size = new System.Drawing.Size(346, 209);
            this.pnDacThu.TabIndex = 21;
            // 
            // btnDoihinhthucthanhtoan
            // 
            this.btnDoihinhthucthanhtoan.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnDoihinhthucthanhtoan.Appearance.Options.UseFont = true;
            this.btnDoihinhthucthanhtoan.Appearance.Options.UseTextOptions = true;
            this.btnDoihinhthucthanhtoan.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnDoihinhthucthanhtoan.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnDoihinhthucthanhtoan.Location = new System.Drawing.Point(235, 142);
            this.btnDoihinhthucthanhtoan.Name = "btnDoihinhthucthanhtoan";
            this.btnDoihinhthucthanhtoan.Size = new System.Drawing.Size(100, 62);
            this.btnDoihinhthucthanhtoan.TabIndex = 38;
            this.btnDoihinhthucthanhtoan.Text = "Đổi hình thức thanh toán";
            this.btnDoihinhthucthanhtoan.Click += new System.EventHandler(this.btnDoihinhthucthanhtoan_Click);
            // 
            // btnNangcap
            // 
            this.btnNangcap.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnNangcap.Appearance.Options.UseFont = true;
            this.btnNangcap.Appearance.Options.UseTextOptions = true;
            this.btnNangcap.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnNangcap.Location = new System.Drawing.Point(122, 142);
            this.btnNangcap.Name = "btnNangcap";
            this.btnNangcap.Size = new System.Drawing.Size(100, 62);
            this.btnNangcap.TabIndex = 37;
            this.btnNangcap.Text = "Nâng hạng buffet";
            this.btnNangcap.Click += new System.EventHandler(this.btnNangcap_Click);
            // 
            // btnBoKhoaBan
            // 
            this.btnBoKhoaBan.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnBoKhoaBan.Appearance.Options.UseFont = true;
            this.btnBoKhoaBan.Location = new System.Drawing.Point(11, 142);
            this.btnBoKhoaBan.Name = "btnBoKhoaBan";
            this.btnBoKhoaBan.Size = new System.Drawing.Size(100, 62);
            this.btnBoKhoaBan.TabIndex = 36;
            this.btnBoKhoaBan.Text = "Bỏ khóa bàn";
            this.btnBoKhoaBan.Click += new System.EventHandler(this.btnBoKhoaBan_Click);
            // 
            // btnHetmon
            // 
            this.btnHetmon.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnHetmon.Appearance.Options.UseFont = true;
            this.btnHetmon.Appearance.Options.UseTextOptions = true;
            this.btnHetmon.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnHetmon.Location = new System.Drawing.Point(235, 73);
            this.btnHetmon.Name = "btnHetmon";
            this.btnHetmon.Size = new System.Drawing.Size(100, 62);
            this.btnHetmon.TabIndex = 35;
            this.btnHetmon.Text = "Hết món";
            this.btnHetmon.Click += new System.EventHandler(this.btnHetmon_Click);
            // 
            // btnBaoCaoBanHang
            // 
            this.btnBaoCaoBanHang.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnBaoCaoBanHang.Appearance.Options.UseFont = true;
            this.btnBaoCaoBanHang.Appearance.Options.UseTextOptions = true;
            this.btnBaoCaoBanHang.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnBaoCaoBanHang.Location = new System.Drawing.Point(122, 73);
            this.btnBaoCaoBanHang.Name = "btnBaoCaoBanHang";
            this.btnBaoCaoBanHang.Size = new System.Drawing.Size(100, 62);
            this.btnBaoCaoBanHang.TabIndex = 34;
            this.btnBaoCaoBanHang.Text = "BC Bán hàng";
            this.btnBaoCaoBanHang.Click += new System.EventHandler(this.btnBaoCaoBanHang_Click);
            // 
            // btnChongia
            // 
            this.btnChongia.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnChongia.Appearance.Options.UseFont = true;
            this.btnChongia.Location = new System.Drawing.Point(11, 73);
            this.btnChongia.Name = "btnChongia";
            this.btnChongia.Size = new System.Drawing.Size(100, 62);
            this.btnChongia.TabIndex = 33;
            this.btnChongia.Text = "Chọn giá";
            this.btnChongia.Click += new System.EventHandler(this.btnChongia_Click);
            // 
            // btn_BanGia2
            // 
            this.btn_BanGia2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_BanGia2.Appearance.Options.UseFont = true;
            this.btn_BanGia2.Location = new System.Drawing.Point(235, 6);
            this.btn_BanGia2.Name = "btn_BanGia2";
            this.btn_BanGia2.Size = new System.Drawing.Size(100, 62);
            this.btn_BanGia2.TabIndex = 8;
            this.btn_BanGia2.Text = "Tặng";
            this.btn_BanGia2.Click += new System.EventHandler(this.btnGiaBan2_Click);
            // 
            // btnChietkhau
            // 
            this.btnChietkhau.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnChietkhau.Appearance.Options.UseFont = true;
            this.btnChietkhau.Location = new System.Drawing.Point(122, 6);
            this.btnChietkhau.Name = "btnChietkhau";
            this.btnChietkhau.Size = new System.Drawing.Size(100, 62);
            this.btnChietkhau.TabIndex = 32;
            this.btnChietkhau.Text = "Chiết khấu";
            this.btnChietkhau.Click += new System.EventHandler(this.btnChietkhau_Click);
            // 
            // btnSuaGia
            // 
            this.btnSuaGia.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnSuaGia.Appearance.Options.UseFont = true;
            this.btnSuaGia.Location = new System.Drawing.Point(11, 6);
            this.btnSuaGia.Name = "btnSuaGia";
            this.btnSuaGia.Size = new System.Drawing.Size(100, 62);
            this.btnSuaGia.TabIndex = 31;
            this.btnSuaGia.Text = "Sửa giá";
            this.btnSuaGia.Click += new System.EventHandler(this.btnSuaGia_Click);
            // 
            // pnThuNgan
            // 
            this.pnThuNgan.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnThuNgan.Controls.Add(this.btn_Dong);
            this.pnThuNgan.Controls.Add(this.btn_TamTinh);
            this.pnThuNgan.Controls.Add(this.btn_ThanhToan);
            this.pnThuNgan.Controls.Add(this.btn_Tachban);
            this.pnThuNgan.Controls.Add(this.btn_GhepBan);
            this.pnThuNgan.Controls.Add(this.btn_Chuyenban);
            this.pnThuNgan.Controls.Add(this.btn_YeuCauThem);
            this.pnThuNgan.Controls.Add(this.btn_GuiOrder);
            this.pnThuNgan.Controls.Add(this.btn_SoDoBan);
            this.pnThuNgan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnThuNgan.Location = new System.Drawing.Point(2, 47);
            this.pnThuNgan.Name = "pnThuNgan";
            this.pnThuNgan.Size = new System.Drawing.Size(346, 209);
            this.pnThuNgan.TabIndex = 20;
            // 
            // btn_Dong
            // 
            this.btn_Dong.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_Dong.Appearance.Options.UseFont = true;
            this.btn_Dong.Location = new System.Drawing.Point(235, 142);
            this.btn_Dong.Name = "btn_Dong";
            this.btn_Dong.Size = new System.Drawing.Size(100, 62);
            this.btn_Dong.TabIndex = 20;
            this.btn_Dong.Text = "Đóng";
            this.btn_Dong.Click += new System.EventHandler(this.btn_logout_Click);
            // 
            // btn_TamTinh
            // 
            this.btn_TamTinh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_TamTinh.Appearance.Options.UseFont = true;
            this.btn_TamTinh.Location = new System.Drawing.Point(122, 142);
            this.btn_TamTinh.Name = "btn_TamTinh";
            this.btn_TamTinh.Size = new System.Drawing.Size(100, 62);
            this.btn_TamTinh.TabIndex = 17;
            this.btn_TamTinh.Text = "In Tạm Tính";
            this.btn_TamTinh.Click += new System.EventHandler(this.btn_TamTinh_Click);
            // 
            // btn_ThanhToan
            // 
            this.btn_ThanhToan.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_ThanhToan.Appearance.Options.UseFont = true;
            this.btn_ThanhToan.Location = new System.Drawing.Point(11, 142);
            this.btn_ThanhToan.Name = "btn_ThanhToan";
            this.btn_ThanhToan.Size = new System.Drawing.Size(100, 62);
            this.btn_ThanhToan.TabIndex = 14;
            this.btn_ThanhToan.Text = "F1\r\nThanh Toán";
            this.btn_ThanhToan.Click += new System.EventHandler(this.btn_Payment_Click);
            // 
            // btn_Tachban
            // 
            this.btn_Tachban.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_Tachban.Appearance.Options.UseFont = true;
            this.btn_Tachban.Location = new System.Drawing.Point(235, 73);
            this.btn_Tachban.Name = "btn_Tachban";
            this.btn_Tachban.Size = new System.Drawing.Size(100, 62);
            this.btn_Tachban.TabIndex = 13;
            this.btn_Tachban.Text = "Tách Bàn ";
            this.btn_Tachban.Click += new System.EventHandler(this.btn_TachBan_Click);
            // 
            // btn_GhepBan
            // 
            this.btn_GhepBan.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_GhepBan.Appearance.Options.UseFont = true;
            this.btn_GhepBan.Location = new System.Drawing.Point(122, 73);
            this.btn_GhepBan.Name = "btn_GhepBan";
            this.btn_GhepBan.Size = new System.Drawing.Size(100, 62);
            this.btn_GhepBan.TabIndex = 16;
            this.btn_GhepBan.Text = "Ghép Bàn";
            this.btn_GhepBan.Click += new System.EventHandler(this.btn_GhepBan_Click);
            // 
            // btn_Chuyenban
            // 
            this.btn_Chuyenban.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_Chuyenban.Appearance.Options.UseFont = true;
            this.btn_Chuyenban.Location = new System.Drawing.Point(11, 73);
            this.btn_Chuyenban.Name = "btn_Chuyenban";
            this.btn_Chuyenban.Size = new System.Drawing.Size(100, 62);
            this.btn_Chuyenban.TabIndex = 20;
            this.btn_Chuyenban.Text = "Chuyển Bàn";
            this.btn_Chuyenban.Click += new System.EventHandler(this.btnChuyenban_Click);
            // 
            // btn_YeuCauThem
            // 
            this.btn_YeuCauThem.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_YeuCauThem.Appearance.Options.UseFont = true;
            this.btn_YeuCauThem.Location = new System.Drawing.Point(235, 6);
            this.btn_YeuCauThem.Name = "btn_YeuCauThem";
            this.btn_YeuCauThem.Size = new System.Drawing.Size(100, 62);
            this.btn_YeuCauThem.TabIndex = 12;
            this.btn_YeuCauThem.Text = "Yêu Cầu \r\nThêm";
            this.btn_YeuCauThem.Click += new System.EventHandler(this.btn_YeuCauThem_Click);
            // 
            // btn_GuiOrder
            // 
            this.btn_GuiOrder.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_GuiOrder.Appearance.Options.UseFont = true;
            this.btn_GuiOrder.Location = new System.Drawing.Point(122, 6);
            this.btn_GuiOrder.Name = "btn_GuiOrder";
            this.btn_GuiOrder.Size = new System.Drawing.Size(100, 62);
            this.btn_GuiOrder.TabIndex = 19;
            this.btn_GuiOrder.Text = "Gửi Order";
            this.btn_GuiOrder.Click += new System.EventHandler(this.btn_OrderSent_Click);
            // 
            // btn_SoDoBan
            // 
            this.btn_SoDoBan.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_SoDoBan.Appearance.Options.UseFont = true;
            this.btn_SoDoBan.Location = new System.Drawing.Point(11, 6);
            this.btn_SoDoBan.Name = "btn_SoDoBan";
            this.btn_SoDoBan.Size = new System.Drawing.Size(100, 62);
            this.btn_SoDoBan.TabIndex = 18;
            this.btn_SoDoBan.Text = "Mở Bàn";
            this.btn_SoDoBan.Click += new System.EventHandler(this.btn_SoDoBan_Click);
            // 
            // pnNghiepVu
            // 
            this.pnNghiepVu.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnNghiepVu.Controls.Add(this.btn_KetCa);
            this.pnNghiepVu.Controls.Add(this.btn_MoKet);
            this.pnNghiepVu.Controls.Add(this.btn_HuyMon);
            this.pnNghiepVu.Controls.Add(this.btnThemHangHoa);
            this.pnNghiepVu.Controls.Add(this.btn_HuyBan);
            this.pnNghiepVu.Controls.Add(this.btn_DatBan);
            this.pnNghiepVu.Controls.Add(this.btn_InlaiOrder);
            this.pnNghiepVu.Controls.Add(this.btn_LayLaiHoaDon);
            this.pnNghiepVu.Controls.Add(this.btn_InLaiHoaDon);
            this.pnNghiepVu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnNghiepVu.Location = new System.Drawing.Point(2, 47);
            this.pnNghiepVu.Name = "pnNghiepVu";
            this.pnNghiepVu.Size = new System.Drawing.Size(346, 209);
            this.pnNghiepVu.TabIndex = 20;
            // 
            // btn_KetCa
            // 
            this.btn_KetCa.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_KetCa.Appearance.Options.UseFont = true;
            this.btn_KetCa.Location = new System.Drawing.Point(235, 73);
            this.btn_KetCa.Name = "btn_KetCa";
            this.btn_KetCa.Size = new System.Drawing.Size(100, 62);
            this.btn_KetCa.TabIndex = 21;
            this.btn_KetCa.Text = "Kết ca";
            this.btn_KetCa.Click += new System.EventHandler(this.btnKetKa_Click);
            // 
            // btn_MoKet
            // 
            this.btn_MoKet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_MoKet.Appearance.Options.UseFont = true;
            this.btn_MoKet.Location = new System.Drawing.Point(235, 5);
            this.btn_MoKet.Name = "btn_MoKet";
            this.btn_MoKet.Size = new System.Drawing.Size(100, 62);
            this.btn_MoKet.TabIndex = 22;
            this.btn_MoKet.Text = "Mở két";
            this.btn_MoKet.Click += new System.EventHandler(this.btnMoKet_Click);
            // 
            // btn_HuyMon
            // 
            this.btn_HuyMon.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_HuyMon.Appearance.Options.UseFont = true;
            this.btn_HuyMon.Location = new System.Drawing.Point(122, 142);
            this.btn_HuyMon.Name = "btn_HuyMon";
            this.btn_HuyMon.Size = new System.Drawing.Size(100, 62);
            this.btn_HuyMon.TabIndex = 26;
            this.btn_HuyMon.Text = "Hủy Món";
            this.btn_HuyMon.Click += new System.EventHandler(this.btn_ExitDishe_Click);
            // 
            // btnThemHangHoa
            // 
            this.btnThemHangHoa.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnThemHangHoa.Appearance.Options.UseFont = true;
            this.btnThemHangHoa.Location = new System.Drawing.Point(235, 142);
            this.btnThemHangHoa.Name = "btnThemHangHoa";
            this.btnThemHangHoa.Size = new System.Drawing.Size(100, 62);
            this.btnThemHangHoa.TabIndex = 33;
            this.btnThemHangHoa.Text = "Thêm hàng hóa";
            this.btnThemHangHoa.Click += new System.EventHandler(this.btnThemHangHoa_Click);
            // 
            // btn_HuyBan
            // 
            this.btn_HuyBan.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_HuyBan.Appearance.Options.UseFont = true;
            this.btn_HuyBan.Location = new System.Drawing.Point(122, 73);
            this.btn_HuyBan.Name = "btn_HuyBan";
            this.btn_HuyBan.Size = new System.Drawing.Size(100, 62);
            this.btn_HuyBan.TabIndex = 28;
            this.btn_HuyBan.Text = "Hủy Bàn";
            this.btn_HuyBan.Click += new System.EventHandler(this.btn_HuyBan_Click);
            // 
            // btn_DatBan
            // 
            this.btn_DatBan.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_DatBan.Appearance.Options.UseFont = true;
            this.btn_DatBan.Location = new System.Drawing.Point(122, 5);
            this.btn_DatBan.Name = "btn_DatBan";
            this.btn_DatBan.Size = new System.Drawing.Size(100, 62);
            this.btn_DatBan.TabIndex = 25;
            this.btn_DatBan.Text = "Đặt Bàn ";
            this.btn_DatBan.Click += new System.EventHandler(this.btn_DatBan_Click);
            // 
            // btn_InlaiOrder
            // 
            this.btn_InlaiOrder.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_InlaiOrder.Appearance.Options.UseFont = true;
            this.btn_InlaiOrder.Location = new System.Drawing.Point(11, 142);
            this.btn_InlaiOrder.Name = "btn_InlaiOrder";
            this.btn_InlaiOrder.Size = new System.Drawing.Size(100, 62);
            this.btn_InlaiOrder.TabIndex = 3;
            this.btn_InlaiOrder.Text = "In lại\r\nOrder";
            this.btn_InlaiOrder.Click += new System.EventHandler(this.btn_InlaiOrder_Click);
            // 
            // btn_LayLaiHoaDon
            // 
            this.btn_LayLaiHoaDon.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_LayLaiHoaDon.Appearance.Options.UseFont = true;
            this.btn_LayLaiHoaDon.Location = new System.Drawing.Point(11, 73);
            this.btn_LayLaiHoaDon.Name = "btn_LayLaiHoaDon";
            this.btn_LayLaiHoaDon.Size = new System.Drawing.Size(100, 62);
            this.btn_LayLaiHoaDon.TabIndex = 24;
            this.btn_LayLaiHoaDon.Text = "Lấy Lại \r\nHóa Đơn";
            this.btn_LayLaiHoaDon.Click += new System.EventHandler(this.btn_LayLaiHoaDon_Click);
            // 
            // btn_InLaiHoaDon
            // 
            this.btn_InLaiHoaDon.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_InLaiHoaDon.Appearance.Options.UseFont = true;
            this.btn_InLaiHoaDon.Location = new System.Drawing.Point(11, 5);
            this.btn_InLaiHoaDon.Name = "btn_InLaiHoaDon";
            this.btn_InLaiHoaDon.Size = new System.Drawing.Size(100, 62);
            this.btn_InLaiHoaDon.TabIndex = 27;
            this.btn_InLaiHoaDon.Text = "In lại\r\nhóa đơn";
            this.btn_InLaiHoaDon.Click += new System.EventHandler(this.btn_InLaiHoaDon_Click);
            // 
            // panelControl8
            // 
            this.panelControl8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl8.Controls.Add(this.btn_DatThu);
            this.panelControl8.Controls.Add(this.btn_NghiepVu);
            this.panelControl8.Controls.Add(this.btn_ThuNgan);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl8.Location = new System.Drawing.Point(2, 2);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(346, 45);
            this.panelControl8.TabIndex = 2;
            // 
            // btn_DatThu
            // 
            this.btn_DatThu.AllowFocus = false;
            this.btn_DatThu.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.btn_DatThu.Appearance.Options.UseFont = true;
            this.btn_DatThu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_DatThu.Location = new System.Drawing.Point(242, 0);
            this.btn_DatThu.Name = "btn_DatThu";
            this.btn_DatThu.Size = new System.Drawing.Size(104, 45);
            this.btn_DatThu.TabIndex = 2;
            this.btn_DatThu.Text = "Đặc thù";
            this.btn_DatThu.Click += new System.EventHandler(this.btn_DatThu_Click);
            // 
            // btn_NghiepVu
            // 
            this.btn_NghiepVu.AllowFocus = false;
            this.btn_NghiepVu.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.btn_NghiepVu.Appearance.Options.UseFont = true;
            this.btn_NghiepVu.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_NghiepVu.Location = new System.Drawing.Point(123, 0);
            this.btn_NghiepVu.Name = "btn_NghiepVu";
            this.btn_NghiepVu.Size = new System.Drawing.Size(119, 45);
            this.btn_NghiepVu.TabIndex = 1;
            this.btn_NghiepVu.Text = "NGHIỆP VỤ";
            this.btn_NghiepVu.Click += new System.EventHandler(this.btn_NghiepVu_Click);
            // 
            // btn_ThuNgan
            // 
            this.btn_ThuNgan.AllowFocus = false;
            this.btn_ThuNgan.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.btn_ThuNgan.Appearance.Options.UseFont = true;
            this.btn_ThuNgan.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_ThuNgan.Location = new System.Drawing.Point(0, 0);
            this.btn_ThuNgan.Name = "btn_ThuNgan";
            this.btn_ThuNgan.Size = new System.Drawing.Size(123, 45);
            this.btn_ThuNgan.TabIndex = 0;
            this.btn_ThuNgan.Text = "THU NGÂN";
            this.btn_ThuNgan.Click += new System.EventHandler(this.btn_ThuNgan_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pnTachban
            // 
            this.pnTachban.Controls.Add(this.pn_GroupTable);
            this.pnTachban.Controls.Add(this.pnInforTablecheck);
            this.pnTachban.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnTachban.Location = new System.Drawing.Point(0, 0);
            this.pnTachban.Name = "pnTachban";
            this.pnTachban.Size = new System.Drawing.Size(674, 698);
            this.pnTachban.TabIndex = 0;
            // 
            // pn_GroupTable
            // 
            this.pn_GroupTable.Controls.Add(this.pnUpDow);
            this.pn_GroupTable.Controls.Add(this.panelControl15);
            this.pn_GroupTable.Controls.Add(this.pn_TableTow);
            this.pn_GroupTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_GroupTable.Location = new System.Drawing.Point(2, 145);
            this.pn_GroupTable.Name = "pn_GroupTable";
            this.pn_GroupTable.Size = new System.Drawing.Size(670, 551);
            this.pn_GroupTable.TabIndex = 3;
            // 
            // pnUpDow
            // 
            this.pnUpDow.Controls.Add(this.btn_NhapSL);
            this.pnUpDow.Controls.Add(this.labelControl2);
            this.pnUpDow.Controls.Add(this.btn_OkThrough);
            this.pnUpDow.Controls.Add(this.btn_UpDivide);
            this.pnUpDow.Controls.Add(this.btn_DowDivide);
            this.pnUpDow.Controls.Add(this.btn_ESCThroughtTable);
            this.pnUpDow.Controls.Add(this.btn_ToRight);
            this.pnUpDow.Controls.Add(this.btn_Toleft);
            this.pnUpDow.Controls.Add(this.btn_ToRightAll);
            this.pnUpDow.Controls.Add(this.btn_ToLeftAll);
            this.pnUpDow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnUpDow.Location = new System.Drawing.Point(356, 2);
            this.pnUpDow.Name = "pnUpDow";
            this.pnUpDow.Size = new System.Drawing.Size(103, 547);
            this.pnUpDow.TabIndex = 2;
            // 
            // btn_NhapSL
            // 
            this.btn_NhapSL.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn_NhapSL.Appearance.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.btn_NhapSL.Appearance.Options.UseFont = true;
            this.btn_NhapSL.Appearance.Options.UseForeColor = true;
            this.btn_NhapSL.Location = new System.Drawing.Point(7, 234);
            this.btn_NhapSL.Name = "btn_NhapSL";
            this.btn_NhapSL.Size = new System.Drawing.Size(93, 54);
            this.btn_NhapSL.TabIndex = 13;
            this.btn_NhapSL.Text = "Nhập SL";
            this.btn_NhapSL.Click += new System.EventHandler(this.btn_NhapSL_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl2.Location = new System.Drawing.Point(3, 265);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(100, 13);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "--------------------";
            // 
            // btn_OkThrough
            // 
            this.btn_OkThrough.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn_OkThrough.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btn_OkThrough.Appearance.Options.UseFont = true;
            this.btn_OkThrough.Appearance.Options.UseForeColor = true;
            this.btn_OkThrough.Location = new System.Drawing.Point(7, 64);
            this.btn_OkThrough.Name = "btn_OkThrough";
            this.btn_OkThrough.Size = new System.Drawing.Size(93, 50);
            this.btn_OkThrough.TabIndex = 0;
            this.btn_OkThrough.Text = "OK";
            this.btn_OkThrough.Click += new System.EventHandler(this.btn_OkThrough_Click);
            // 
            // btn_UpDivide
            // 
            this.btn_UpDivide.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_UpDivide.Image = ((System.Drawing.Image)(resources.GetObject("btn_UpDivide.Image")));
            this.btn_UpDivide.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_UpDivide.Location = new System.Drawing.Point(2, 2);
            this.btn_UpDivide.Name = "btn_UpDivide";
            this.btn_UpDivide.Size = new System.Drawing.Size(99, 50);
            this.btn_UpDivide.TabIndex = 11;
            this.btn_UpDivide.Text = "simpleButton7";
            this.btn_UpDivide.Click += new System.EventHandler(this.btn_UpDivide_Click);
            // 
            // btn_DowDivide
            // 
            this.btn_DowDivide.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_DowDivide.Image = ((System.Drawing.Image)(resources.GetObject("btn_DowDivide.Image")));
            this.btn_DowDivide.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_DowDivide.Location = new System.Drawing.Point(2, 495);
            this.btn_DowDivide.Name = "btn_DowDivide";
            this.btn_DowDivide.Size = new System.Drawing.Size(99, 50);
            this.btn_DowDivide.TabIndex = 10;
            this.btn_DowDivide.Text = "simpleButton6";
            this.btn_DowDivide.Click += new System.EventHandler(this.btn_DowDivide_Click);
            // 
            // btn_ESCThroughtTable
            // 
            this.btn_ESCThroughtTable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_ESCThroughtTable.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn_ESCThroughtTable.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btn_ESCThroughtTable.Appearance.Options.UseFont = true;
            this.btn_ESCThroughtTable.Appearance.Options.UseForeColor = true;
            this.btn_ESCThroughtTable.Location = new System.Drawing.Point(7, 430);
            this.btn_ESCThroughtTable.Name = "btn_ESCThroughtTable";
            this.btn_ESCThroughtTable.Size = new System.Drawing.Size(93, 50);
            this.btn_ESCThroughtTable.TabIndex = 5;
            this.btn_ESCThroughtTable.Text = "ESC";
            this.btn_ESCThroughtTable.Click += new System.EventHandler(this.btn_ESCThroughtTable_Click);
            // 
            // btn_ToRight
            // 
            this.btn_ToRight.Image = ((System.Drawing.Image)(resources.GetObject("btn_ToRight.Image")));
            this.btn_ToRight.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_ToRight.Location = new System.Drawing.Point(7, 122);
            this.btn_ToRight.Name = "btn_ToRight";
            this.btn_ToRight.Size = new System.Drawing.Size(93, 50);
            this.btn_ToRight.TabIndex = 2;
            this.btn_ToRight.Text = ">";
            this.btn_ToRight.Click += new System.EventHandler(this.btn_ToRight_Click);
            // 
            // btn_Toleft
            // 
            this.btn_Toleft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Toleft.Image = ((System.Drawing.Image)(resources.GetObject("btn_Toleft.Image")));
            this.btn_Toleft.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_Toleft.Location = new System.Drawing.Point(7, 372);
            this.btn_Toleft.Name = "btn_Toleft";
            this.btn_Toleft.Size = new System.Drawing.Size(93, 50);
            this.btn_Toleft.TabIndex = 3;
            this.btn_Toleft.Text = "<";
            this.btn_Toleft.Click += new System.EventHandler(this.btn_Toleft_Click);
            // 
            // btn_ToRightAll
            // 
            this.btn_ToRightAll.Image = ((System.Drawing.Image)(resources.GetObject("btn_ToRightAll.Image")));
            this.btn_ToRightAll.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_ToRightAll.Location = new System.Drawing.Point(7, 122);
            this.btn_ToRightAll.Name = "btn_ToRightAll";
            this.btn_ToRightAll.Size = new System.Drawing.Size(93, 50);
            this.btn_ToRightAll.TabIndex = 1;
            this.btn_ToRightAll.Text = ">>";
            this.btn_ToRightAll.Visible = false;
            this.btn_ToRightAll.Click += new System.EventHandler(this.btn_ToRightAll_Click);
            // 
            // btn_ToLeftAll
            // 
            this.btn_ToLeftAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_ToLeftAll.Image = ((System.Drawing.Image)(resources.GetObject("btn_ToLeftAll.Image")));
            this.btn_ToLeftAll.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_ToLeftAll.Location = new System.Drawing.Point(7, 372);
            this.btn_ToLeftAll.Name = "btn_ToLeftAll";
            this.btn_ToLeftAll.Size = new System.Drawing.Size(93, 50);
            this.btn_ToLeftAll.TabIndex = 4;
            this.btn_ToLeftAll.Text = "<<";
            this.btn_ToLeftAll.Visible = false;
            this.btn_ToLeftAll.Click += new System.EventHandler(this.btn_ToLeftAll_Click);
            // 
            // panelControl15
            // 
            this.panelControl15.Controls.Add(this.gr_divide_Table_1);
            this.panelControl15.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl15.Location = new System.Drawing.Point(2, 2);
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(354, 547);
            this.panelControl15.TabIndex = 0;
            // 
            // gr_divide_Table_1
            // 
            this.gr_divide_Table_1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.RelationName = "Level1";
            this.gr_divide_Table_1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gr_divide_Table_1.Location = new System.Drawing.Point(2, 2);
            this.gr_divide_Table_1.MainView = this.gv_divide_Table_1;
            this.gr_divide_Table_1.Name = "gr_divide_Table_1";
            this.gr_divide_Table_1.Size = new System.Drawing.Size(350, 543);
            this.gr_divide_Table_1.TabIndex = 20;
            this.gr_divide_Table_1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv_divide_Table_1});
            // 
            // gv_divide_Table_1
            // 
            this.gv_divide_Table_1.ColumnPanelRowHeight = 30;
            this.gv_divide_Table_1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._SSTT,
            this._TEN_HANGHOA,
            this._SOLUONG,
            this._GIABAN,
            this._Thanh_Tien,
            this._MA_HANGHOA,
            this._GHICHU,
            this._MA_BEP,
            this._IS_COMBO});
            this.gv_divide_Table_1.GridControl = this.gr_divide_Table_1;
            this.gv_divide_Table_1.Name = "gv_divide_Table_1";
            this.gv_divide_Table_1.OptionsView.ShowGroupPanel = false;
            this.gv_divide_Table_1.OptionsView.ShowIndicator = false;
            this.gv_divide_Table_1.RowHeight = 35;
            this.gv_divide_Table_1.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gv_divide_Table_1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gv_divide_Table_1_CustomDrawCell);
            // 
            // _SSTT
            // 
            this._SSTT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._SSTT.AppearanceCell.Options.UseFont = true;
            this._SSTT.AppearanceCell.Options.UseTextOptions = true;
            this._SSTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._SSTT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._SSTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this._SSTT.AppearanceHeader.Options.UseFont = true;
            this._SSTT.AppearanceHeader.Options.UseTextOptions = true;
            this._SSTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._SSTT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._SSTT.Caption = "TT";
            this._SSTT.FieldName = "_SSTT";
            this._SSTT.Name = "_SSTT";
            this._SSTT.OptionsColumn.AllowEdit = false;
            this._SSTT.OptionsColumn.AllowFocus = false;
            this._SSTT.Visible = true;
            this._SSTT.VisibleIndex = 0;
            this._SSTT.Width = 34;
            // 
            // _TEN_HANGHOA
            // 
            this._TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this._TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this._TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this._TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this._TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._TEN_HANGHOA.Caption = "Hàng Hóa";
            this._TEN_HANGHOA.FieldName = "_TEN_HANGHOA";
            this._TEN_HANGHOA.Name = "_TEN_HANGHOA";
            this._TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this._TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this._TEN_HANGHOA.Visible = true;
            this._TEN_HANGHOA.VisibleIndex = 1;
            this._TEN_HANGHOA.Width = 116;
            // 
            // _SOLUONG
            // 
            this._SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._SOLUONG.AppearanceCell.Options.UseFont = true;
            this._SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this._SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this._SOLUONG.AppearanceHeader.Options.UseFont = true;
            this._SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this._SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._SOLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._SOLUONG.Caption = "SL";
            this._SOLUONG.FieldName = "_SOLUONG";
            this._SOLUONG.Name = "_SOLUONG";
            this._SOLUONG.OptionsColumn.AllowEdit = false;
            this._SOLUONG.OptionsColumn.AllowFocus = false;
            this._SOLUONG.Visible = true;
            this._SOLUONG.VisibleIndex = 2;
            this._SOLUONG.Width = 51;
            // 
            // _GIABAN
            // 
            this._GIABAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._GIABAN.AppearanceCell.Options.UseFont = true;
            this._GIABAN.AppearanceCell.Options.UseTextOptions = true;
            this._GIABAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._GIABAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._GIABAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this._GIABAN.AppearanceHeader.Options.UseFont = true;
            this._GIABAN.AppearanceHeader.Options.UseTextOptions = true;
            this._GIABAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._GIABAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._GIABAN.Caption = "Đơn giá";
            this._GIABAN.DisplayFormat.FormatString = "{0:#,###0}";
            this._GIABAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this._GIABAN.FieldName = "_GIABAN";
            this._GIABAN.Name = "_GIABAN";
            this._GIABAN.OptionsColumn.AllowEdit = false;
            this._GIABAN.OptionsColumn.AllowFocus = false;
            this._GIABAN.Visible = true;
            this._GIABAN.VisibleIndex = 3;
            this._GIABAN.Width = 96;
            // 
            // _Thanh_Tien
            // 
            this._Thanh_Tien.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._Thanh_Tien.AppearanceCell.Options.UseFont = true;
            this._Thanh_Tien.AppearanceCell.Options.UseTextOptions = true;
            this._Thanh_Tien.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._Thanh_Tien.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._Thanh_Tien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this._Thanh_Tien.AppearanceHeader.Options.UseFont = true;
            this._Thanh_Tien.AppearanceHeader.Options.UseTextOptions = true;
            this._Thanh_Tien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._Thanh_Tien.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._Thanh_Tien.Caption = "T Tiền";
            this._Thanh_Tien.DisplayFormat.FormatString = "{0:#,###0}";
            this._Thanh_Tien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this._Thanh_Tien.FieldName = "_Thanh_Tien";
            this._Thanh_Tien.Name = "_Thanh_Tien";
            this._Thanh_Tien.OptionsColumn.AllowEdit = false;
            this._Thanh_Tien.OptionsColumn.AllowFocus = false;
            this._Thanh_Tien.Visible = true;
            this._Thanh_Tien.VisibleIndex = 4;
            this._Thanh_Tien.Width = 104;
            // 
            // _MA_HANGHOA
            // 
            this._MA_HANGHOA.Caption = "Mã Hàng Hóa";
            this._MA_HANGHOA.FieldName = "_MA_HANGHOA";
            this._MA_HANGHOA.Name = "_MA_HANGHOA";
            this._MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this._MA_HANGHOA.OptionsColumn.AllowFocus = false;
            // 
            // _GHICHU
            // 
            this._GHICHU.Caption = "Ghi Chú";
            this._GHICHU.FieldName = "_GHICHU";
            this._GHICHU.Name = "_GHICHU";
            // 
            // _MA_BEP
            // 
            this._MA_BEP.Caption = "May in bep";
            this._MA_BEP.FieldName = "_MA_BEP";
            this._MA_BEP.Name = "_MA_BEP";
            // 
            // _IS_COMBO
            // 
            this._IS_COMBO.Caption = "_IS_COMBO";
            this._IS_COMBO.FieldName = "_IS_COMBO";
            this._IS_COMBO.Name = "_IS_COMBO";
            // 
            // pn_TableTow
            // 
            this.pn_TableTow.Controls.Add(this.gr_divide_Table_2);
            this.pn_TableTow.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn_TableTow.Location = new System.Drawing.Point(459, 2);
            this.pn_TableTow.Name = "pn_TableTow";
            this.pn_TableTow.Size = new System.Drawing.Size(209, 547);
            this.pn_TableTow.TabIndex = 1;
            // 
            // gr_divide_Table_2
            // 
            this.gr_divide_Table_2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode3.RelationName = "Level1";
            this.gr_divide_Table_2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode3});
            this.gr_divide_Table_2.Location = new System.Drawing.Point(2, 2);
            this.gr_divide_Table_2.MainView = this.gv_divide_Table_2;
            this.gr_divide_Table_2.Name = "gr_divide_Table_2";
            this.gr_divide_Table_2.Size = new System.Drawing.Size(205, 543);
            this.gr_divide_Table_2.TabIndex = 21;
            this.gr_divide_Table_2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv_divide_Table_2});
            // 
            // gv_divide_Table_2
            // 
            this.gv_divide_Table_2.ColumnPanelRowHeight = 30;
            this.gv_divide_Table_2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.@__STT,
            this.@__TEN_HANGHOA,
            this.@__SOLUONG,
            this.@__GIABAN,
            this.@__Thanh_Tien,
            this.@__MA_HANGHOA,
            this.@__GHICHU,
            this.@__MA_BEP,
            this.@__IS_COMBO});
            this.gv_divide_Table_2.GridControl = this.gr_divide_Table_2;
            this.gv_divide_Table_2.Name = "gv_divide_Table_2";
            this.gv_divide_Table_2.OptionsView.ShowGroupPanel = false;
            this.gv_divide_Table_2.OptionsView.ShowIndicator = false;
            this.gv_divide_Table_2.RowHeight = 35;
            this.gv_divide_Table_2.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gv_divide_Table_2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gv_divide_Table_2_CustomDrawCell);
            // 
            // __STT
            // 
            this.@__STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.@__STT.AppearanceCell.Options.UseFont = true;
            this.@__STT.AppearanceCell.Options.UseTextOptions = true;
            this.@__STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__STT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.@__STT.AppearanceHeader.Options.UseFont = true;
            this.@__STT.AppearanceHeader.Options.UseTextOptions = true;
            this.@__STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__STT.Caption = "TT";
            this.@__STT.FieldName = "__STT";
            this.@__STT.Name = "__STT";
            this.@__STT.OptionsColumn.AllowEdit = false;
            this.@__STT.OptionsColumn.AllowFocus = false;
            this.@__STT.Visible = true;
            this.@__STT.VisibleIndex = 0;
            this.@__STT.Width = 34;
            // 
            // __TEN_HANGHOA
            // 
            this.@__TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.@__TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.@__TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.@__TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.@__TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.@__TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__TEN_HANGHOA.Caption = "Hàng Hóa";
            this.@__TEN_HANGHOA.FieldName = "__TEN_HANGHOA";
            this.@__TEN_HANGHOA.Name = "__TEN_HANGHOA";
            this.@__TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this.@__TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this.@__TEN_HANGHOA.Visible = true;
            this.@__TEN_HANGHOA.VisibleIndex = 1;
            this.@__TEN_HANGHOA.Width = 116;
            // 
            // __SOLUONG
            // 
            this.@__SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.@__SOLUONG.AppearanceCell.Options.UseFont = true;
            this.@__SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.@__SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.@__SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.@__SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.@__SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__SOLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__SOLUONG.Caption = "SL";
            this.@__SOLUONG.FieldName = "__SOLUONG";
            this.@__SOLUONG.Name = "__SOLUONG";
            this.@__SOLUONG.OptionsColumn.AllowEdit = false;
            this.@__SOLUONG.OptionsColumn.AllowFocus = false;
            this.@__SOLUONG.Visible = true;
            this.@__SOLUONG.VisibleIndex = 2;
            this.@__SOLUONG.Width = 51;
            // 
            // __GIABAN
            // 
            this.@__GIABAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.@__GIABAN.AppearanceCell.Options.UseFont = true;
            this.@__GIABAN.AppearanceCell.Options.UseTextOptions = true;
            this.@__GIABAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.@__GIABAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__GIABAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.@__GIABAN.AppearanceHeader.Options.UseFont = true;
            this.@__GIABAN.AppearanceHeader.Options.UseTextOptions = true;
            this.@__GIABAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__GIABAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__GIABAN.Caption = "Đơn giá";
            this.@__GIABAN.DisplayFormat.FormatString = "{0:#,###0}";
            this.@__GIABAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.@__GIABAN.FieldName = "__GIABAN";
            this.@__GIABAN.Name = "__GIABAN";
            this.@__GIABAN.OptionsColumn.AllowEdit = false;
            this.@__GIABAN.OptionsColumn.AllowFocus = false;
            this.@__GIABAN.Visible = true;
            this.@__GIABAN.VisibleIndex = 3;
            this.@__GIABAN.Width = 96;
            // 
            // __Thanh_Tien
            // 
            this.@__Thanh_Tien.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.@__Thanh_Tien.AppearanceCell.Options.UseFont = true;
            this.@__Thanh_Tien.AppearanceCell.Options.UseTextOptions = true;
            this.@__Thanh_Tien.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.@__Thanh_Tien.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__Thanh_Tien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.@__Thanh_Tien.AppearanceHeader.Options.UseFont = true;
            this.@__Thanh_Tien.AppearanceHeader.Options.UseTextOptions = true;
            this.@__Thanh_Tien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__Thanh_Tien.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__Thanh_Tien.Caption = "T Tiền";
            this.@__Thanh_Tien.DisplayFormat.FormatString = "{0:#,###0}";
            this.@__Thanh_Tien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.@__Thanh_Tien.FieldName = "__Thanh_Tien";
            this.@__Thanh_Tien.Name = "__Thanh_Tien";
            this.@__Thanh_Tien.OptionsColumn.AllowEdit = false;
            this.@__Thanh_Tien.OptionsColumn.AllowFocus = false;
            this.@__Thanh_Tien.Visible = true;
            this.@__Thanh_Tien.VisibleIndex = 4;
            this.@__Thanh_Tien.Width = 104;
            // 
            // __MA_HANGHOA
            // 
            this.@__MA_HANGHOA.Caption = "Mã Hàng Hóa";
            this.@__MA_HANGHOA.FieldName = "__MA_HANGHOA";
            this.@__MA_HANGHOA.Name = "__MA_HANGHOA";
            this.@__MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this.@__MA_HANGHOA.OptionsColumn.AllowFocus = false;
            // 
            // __GHICHU
            // 
            this.@__GHICHU.Caption = "Ghi Chú";
            this.@__GHICHU.FieldName = "__GHICHU";
            this.@__GHICHU.Name = "__GHICHU";
            // 
            // __MA_BEP
            // 
            this.@__MA_BEP.Caption = "MÃ BẾP";
            this.@__MA_BEP.FieldName = "__MA_BEP";
            this.@__MA_BEP.Name = "__MA_BEP";
            // 
            // __IS_COMBO
            // 
            this.@__IS_COMBO.Caption = "__IS_COMBO";
            this.@__IS_COMBO.FieldName = "__IS_COMBO";
            this.@__IS_COMBO.Name = "__IS_COMBO";
            // 
            // pnInforTablecheck
            // 
            this.pnInforTablecheck.Controls.Add(this.panelControl18);
            this.pnInforTablecheck.Controls.Add(this.panelControl14);
            this.pnInforTablecheck.Controls.Add(this.pn_TableShare);
            this.pnInforTablecheck.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnInforTablecheck.Location = new System.Drawing.Point(2, 2);
            this.pnInforTablecheck.Name = "pnInforTablecheck";
            this.pnInforTablecheck.Size = new System.Drawing.Size(670, 143);
            this.pnInforTablecheck.TabIndex = 2;
            // 
            // panelControl18
            // 
            this.panelControl18.Controls.Add(this.lb_TenKH);
            this.panelControl18.Controls.Add(this.lb_KhuVuc);
            this.panelControl18.Controls.Add(this.lb_Ban);
            this.panelControl18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl18.Location = new System.Drawing.Point(356, 2);
            this.panelControl18.Name = "panelControl18";
            this.panelControl18.Size = new System.Drawing.Size(103, 139);
            this.panelControl18.TabIndex = 2;
            // 
            // lb_TenKH
            // 
            this.lb_TenKH.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lb_TenKH.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_TenKH.Location = new System.Drawing.Point(18, 101);
            this.lb_TenKH.Name = "lb_TenKH";
            this.lb_TenKH.Size = new System.Drawing.Size(59, 19);
            this.lb_TenKH.TabIndex = 2;
            this.lb_TenKH.Text = "Tên KH";
            // 
            // lb_KhuVuc
            // 
            this.lb_KhuVuc.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lb_KhuVuc.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_KhuVuc.Location = new System.Drawing.Point(14, 20);
            this.lb_KhuVuc.Name = "lb_KhuVuc";
            this.lb_KhuVuc.Size = new System.Drawing.Size(66, 19);
            this.lb_KhuVuc.TabIndex = 1;
            this.lb_KhuVuc.Text = "Khu Vực";
            // 
            // lb_Ban
            // 
            this.lb_Ban.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lb_Ban.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_Ban.Location = new System.Drawing.Point(31, 61);
            this.lb_Ban.Name = "lb_Ban";
            this.lb_Ban.Size = new System.Drawing.Size(31, 19);
            this.lb_Ban.TabIndex = 0;
            this.lb_Ban.Text = "Bàn";
            // 
            // panelControl14
            // 
            this.panelControl14.Controls.Add(this.txtTenKhacHang);
            this.panelControl14.Controls.Add(this.txtmaBanGoc);
            this.panelControl14.Controls.Add(this.txtmaKVBanGoc);
            this.panelControl14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl14.Location = new System.Drawing.Point(2, 2);
            this.panelControl14.Name = "panelControl14";
            this.panelControl14.Size = new System.Drawing.Size(354, 139);
            this.panelControl14.TabIndex = 0;
            // 
            // txtTenKhacHang
            // 
            this.txtTenKhacHang.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTenKhacHang.Enabled = false;
            this.txtTenKhacHang.Location = new System.Drawing.Point(7, 95);
            this.txtTenKhacHang.Name = "txtTenKhacHang";
            this.txtTenKhacHang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtTenKhacHang.Properties.Appearance.Options.UseFont = true;
            this.txtTenKhacHang.Size = new System.Drawing.Size(342, 30);
            this.txtTenKhacHang.TabIndex = 5;
            // 
            // txtmaBanGoc
            // 
            this.txtmaBanGoc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtmaBanGoc.Enabled = false;
            this.txtmaBanGoc.Location = new System.Drawing.Point(7, 57);
            this.txtmaBanGoc.Name = "txtmaBanGoc";
            this.txtmaBanGoc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtmaBanGoc.Properties.Appearance.Options.UseFont = true;
            this.txtmaBanGoc.Size = new System.Drawing.Size(342, 30);
            this.txtmaBanGoc.TabIndex = 4;
            // 
            // txtmaKVBanGoc
            // 
            this.txtmaKVBanGoc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtmaKVBanGoc.Enabled = false;
            this.txtmaKVBanGoc.Location = new System.Drawing.Point(7, 13);
            this.txtmaKVBanGoc.Name = "txtmaKVBanGoc";
            this.txtmaKVBanGoc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtmaKVBanGoc.Properties.Appearance.Options.UseFont = true;
            this.txtmaKVBanGoc.Size = new System.Drawing.Size(342, 30);
            this.txtmaKVBanGoc.TabIndex = 3;
            // 
            // pn_TableShare
            // 
            this.pn_TableShare.Controls.Add(this.txtTenKhachHangGhep);
            this.pn_TableShare.Controls.Add(this.txtmaBanGhep);
            this.pn_TableShare.Controls.Add(this.txtmaKVBanGhep);
            this.pn_TableShare.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn_TableShare.Location = new System.Drawing.Point(459, 2);
            this.pn_TableShare.Name = "pn_TableShare";
            this.pn_TableShare.Size = new System.Drawing.Size(209, 139);
            this.pn_TableShare.TabIndex = 1;
            // 
            // txtTenKhachHangGhep
            // 
            this.txtTenKhachHangGhep.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTenKhachHangGhep.Enabled = false;
            this.txtTenKhachHangGhep.Location = new System.Drawing.Point(7, 94);
            this.txtTenKhachHangGhep.Name = "txtTenKhachHangGhep";
            this.txtTenKhachHangGhep.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtTenKhachHangGhep.Properties.Appearance.Options.UseFont = true;
            this.txtTenKhachHangGhep.Size = new System.Drawing.Size(197, 30);
            this.txtTenKhachHangGhep.TabIndex = 2;
            // 
            // txtmaBanGhep
            // 
            this.txtmaBanGhep.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtmaBanGhep.Enabled = false;
            this.txtmaBanGhep.Location = new System.Drawing.Point(7, 56);
            this.txtmaBanGhep.Name = "txtmaBanGhep";
            this.txtmaBanGhep.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtmaBanGhep.Properties.Appearance.Options.UseFont = true;
            this.txtmaBanGhep.Size = new System.Drawing.Size(197, 30);
            this.txtmaBanGhep.TabIndex = 1;
            // 
            // txtmaKVBanGhep
            // 
            this.txtmaKVBanGhep.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtmaKVBanGhep.Enabled = false;
            this.txtmaKVBanGhep.Location = new System.Drawing.Point(7, 12);
            this.txtmaKVBanGhep.Name = "txtmaKVBanGhep";
            this.txtmaKVBanGhep.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtmaKVBanGhep.Properties.Appearance.Options.UseFont = true;
            this.txtmaKVBanGhep.Size = new System.Drawing.Size(197, 30);
            this.txtmaKVBanGhep.TabIndex = 0;
            // 
            // pnYeucauthem
            // 
            this.pnYeucauthem.Controls.Add(this.panelControl19);
            this.pnYeucauthem.Controls.Add(this.panelControl5);
            this.pnYeucauthem.Controls.Add(this.panelControl21);
            this.pnYeucauthem.Controls.Add(this.panelControl17);
            this.pnYeucauthem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnYeucauthem.Location = new System.Drawing.Point(0, 0);
            this.pnYeucauthem.Name = "pnYeucauthem";
            this.pnYeucauthem.Size = new System.Drawing.Size(674, 698);
            this.pnYeucauthem.TabIndex = 8;
            // 
            // panelControl19
            // 
            this.panelControl19.Controls.Add(this.pnYeuCau);
            this.panelControl19.Controls.Add(this.panelControl22);
            this.panelControl19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl19.Location = new System.Drawing.Point(2, 98);
            this.panelControl19.Name = "panelControl19";
            this.panelControl19.Size = new System.Drawing.Size(670, 378);
            this.panelControl19.TabIndex = 3;
            // 
            // pnYeuCau
            // 
            this.pnYeuCau.AutoScroll = true;
            this.pnYeuCau.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnYeuCau.Location = new System.Drawing.Point(2, 2);
            this.pnYeuCau.Name = "pnYeuCau";
            this.pnYeuCau.Size = new System.Drawing.Size(633, 374);
            this.pnYeuCau.TabIndex = 1;
            // 
            // panelControl22
            // 
            this.panelControl22.Controls.Add(this.simpleButton23);
            this.panelControl22.Controls.Add(this.simpleButton24);
            this.panelControl22.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl22.Location = new System.Drawing.Point(635, 2);
            this.panelControl22.Name = "panelControl22";
            this.panelControl22.Size = new System.Drawing.Size(33, 374);
            this.panelControl22.TabIndex = 0;
            // 
            // simpleButton23
            // 
            this.simpleButton23.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.simpleButton23.Image = global::KP_RES.Properties.Resources.up11_26;
            this.simpleButton23.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton23.Location = new System.Drawing.Point(2, 301);
            this.simpleButton23.Name = "simpleButton23";
            this.simpleButton23.Size = new System.Drawing.Size(29, 71);
            this.simpleButton23.TabIndex = 7;
            this.simpleButton23.Text = ">";
            this.simpleButton23.Click += new System.EventHandler(this.btnYCT_Xuong_Click);
            // 
            // simpleButton24
            // 
            this.simpleButton24.Dock = System.Windows.Forms.DockStyle.Top;
            this.simpleButton24.Image = global::KP_RES.Properties.Resources.up22_26;
            this.simpleButton24.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton24.Location = new System.Drawing.Point(2, 2);
            this.simpleButton24.Name = "simpleButton24";
            this.simpleButton24.Size = new System.Drawing.Size(29, 71);
            this.simpleButton24.TabIndex = 8;
            this.simpleButton24.Text = "<";
            this.simpleButton24.Click += new System.EventHandler(this.btnYCT_Len_Click);
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.us_keyboard3);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl5.Location = new System.Drawing.Point(2, 476);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(670, 220);
            this.panelControl5.TabIndex = 80;
            // 
            // us_keyboard3
            // 
            this.us_keyboard3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.us_keyboard3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.us_keyboard3.Location = new System.Drawing.Point(10, 4);
            this.us_keyboard3.Name = "us_keyboard3";
            this.us_keyboard3.Size = new System.Drawing.Size(649, 213);
            this.us_keyboard3.TabIndex = 10;
            // 
            // panelControl21
            // 
            this.panelControl21.Controls.Add(this.txtYeuCauThem);
            this.panelControl21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl21.Location = new System.Drawing.Point(2, 44);
            this.panelControl21.Name = "panelControl21";
            this.panelControl21.Size = new System.Drawing.Size(670, 54);
            this.panelControl21.TabIndex = 0;
            // 
            // txtYeuCauThem
            // 
            this.txtYeuCauThem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtYeuCauThem.Location = new System.Drawing.Point(3, 6);
            this.txtYeuCauThem.Name = "txtYeuCauThem";
            this.txtYeuCauThem.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.txtYeuCauThem.Properties.Appearance.Options.UseFont = true;
            this.txtYeuCauThem.Size = new System.Drawing.Size(665, 40);
            this.txtYeuCauThem.TabIndex = 1;
            this.txtYeuCauThem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtYeuCauThem_KeyDown);
            // 
            // panelControl17
            // 
            this.panelControl17.Controls.Add(this.panelControl11);
            this.panelControl17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl17.Location = new System.Drawing.Point(2, 2);
            this.panelControl17.Name = "panelControl17";
            this.panelControl17.Size = new System.Drawing.Size(670, 42);
            this.panelControl17.TabIndex = 2;
            // 
            // panelControl11
            // 
            this.panelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl11.Controls.Add(this.lbMonYeuCauThem);
            this.panelControl11.Controls.Add(this.lbTableYeuCauThem);
            this.panelControl11.Controls.Add(this.lbkvYeuCauThem);
            this.panelControl11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl11.Location = new System.Drawing.Point(2, 12);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(666, 28);
            this.panelControl11.TabIndex = 3;
            // 
            // lbMonYeuCauThem
            // 
            this.lbMonYeuCauThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbMonYeuCauThem.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbMonYeuCauThem.Location = new System.Drawing.Point(306, 0);
            this.lbMonYeuCauThem.Name = "lbMonYeuCauThem";
            this.lbMonYeuCauThem.Size = new System.Drawing.Size(116, 19);
            this.lbMonYeuCauThem.TabIndex = 2;
            this.lbMonYeuCauThem.Text = "Coffee Sửa Đá";
            // 
            // lbTableYeuCauThem
            // 
            this.lbTableYeuCauThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbTableYeuCauThem.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbTableYeuCauThem.Location = new System.Drawing.Point(192, 0);
            this.lbTableYeuCauThem.Name = "lbTableYeuCauThem";
            this.lbTableYeuCauThem.Size = new System.Drawing.Size(114, 19);
            this.lbTableYeuCauThem.TabIndex = 1;
            this.lbTableYeuCauThem.Text = "Bàn 1           >";
            // 
            // lbkvYeuCauThem
            // 
            this.lbkvYeuCauThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbkvYeuCauThem.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbkvYeuCauThem.Location = new System.Drawing.Point(0, 0);
            this.lbkvYeuCauThem.Name = "lbkvYeuCauThem";
            this.lbkvYeuCauThem.Size = new System.Drawing.Size(192, 19);
            this.lbkvYeuCauThem.TabIndex = 0;
            this.lbkvYeuCauThem.Text = "KV Sân Vườn               >";
            // 
            // pnDatban
            // 
            this.pnDatban.Controls.Add(this.panelControl45);
            this.pnDatban.Controls.Add(this.panelControl46);
            this.pnDatban.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnDatban.Location = new System.Drawing.Point(0, 0);
            this.pnDatban.Name = "pnDatban";
            this.pnDatban.Size = new System.Drawing.Size(674, 698);
            this.pnDatban.TabIndex = 0;
            // 
            // panelControl45
            // 
            this.panelControl45.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl45.Controls.Add(this.us_keyboard2);
            this.panelControl45.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl45.Location = new System.Drawing.Point(2, 477);
            this.panelControl45.Name = "panelControl45";
            this.panelControl45.Size = new System.Drawing.Size(670, 219);
            this.panelControl45.TabIndex = 1;
            // 
            // us_keyboard2
            // 
            this.us_keyboard2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.us_keyboard2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.us_keyboard2.Location = new System.Drawing.Point(-1, 3);
            this.us_keyboard2.Name = "us_keyboard2";
            this.us_keyboard2.Size = new System.Drawing.Size(647, 219);
            this.us_keyboard2.TabIndex = 0;
            // 
            // panelControl46
            // 
            this.panelControl46.Controls.Add(this.pnInforTable);
            this.panelControl46.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl46.Location = new System.Drawing.Point(2, 2);
            this.panelControl46.Name = "panelControl46";
            this.panelControl46.Size = new System.Drawing.Size(670, 694);
            this.panelControl46.TabIndex = 2;
            // 
            // pnInforTable
            // 
            this.pnInforTable.Appearance.BackColor = System.Drawing.Color.Gainsboro;
            this.pnInforTable.Appearance.Options.UseBackColor = true;
            this.pnInforTable.Controls.Add(this.labelControl1);
            this.pnInforTable.Controls.Add(this.txtngay);
            this.pnInforTable.Controls.Add(this.btn_Danhsachdatban);
            this.pnInforTable.Controls.Add(this.chkEmail);
            this.pnInforTable.Controls.Add(this.txtEmail);
            this.pnInforTable.Controls.Add(this.btnMoBanNgay);
            this.pnInforTable.Controls.Add(this.txtPartMoneyPayments);
            this.pnInforTable.Controls.Add(this.btnMobansau);
            this.pnInforTable.Controls.Add(this.btn_Close);
            this.pnInforTable.Controls.Add(this.btnRemove);
            this.pnInforTable.Controls.Add(this.panelControl48);
            this.pnInforTable.Controls.Add(this.cbMM);
            this.pnInforTable.Controls.Add(this.cbHH);
            this.pnInforTable.Controls.Add(this.labelControl38);
            this.pnInforTable.Controls.Add(this.btnSendEmail);
            this.pnInforTable.Controls.Add(this.lb_Gio);
            this.pnInforTable.Controls.Add(this.btn_SelectDishes);
            this.pnInforTable.Controls.Add(this.btn_SelectTable);
            this.pnInforTable.Controls.Add(this.lb_Diachi);
            this.pnInforTable.Controls.Add(this.lb_Tiendatcoc);
            this.pnInforTable.Controls.Add(this.lb_ngay2);
            this.pnInforTable.Controls.Add(this.lb_dienthoai2);
            this.pnInforTable.Controls.Add(this.lb_TenKH2);
            this.pnInforTable.Controls.Add(this.txtAddress);
            this.pnInforTable.Controls.Add(this.txtDienThoai);
            this.pnInforTable.Controls.Add(this.txtDBTenKH);
            this.pnInforTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnInforTable.Location = new System.Drawing.Point(2, 2);
            this.pnInforTable.Name = "pnInforTable";
            this.pnInforTable.Size = new System.Drawing.Size(666, 690);
            this.pnInforTable.TabIndex = 0;
            this.pnInforTable.Text = "Thông Tin Bàn Đặt";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(349, 68);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(24, 13);
            this.labelControl1.TabIndex = 45;
            this.labelControl1.Text = "Email";
            // 
            // txtngay
            // 
            this.txtngay.EditValue = null;
            this.txtngay.EnterMoveNextControl = true;
            this.txtngay.Location = new System.Drawing.Point(97, 93);
            this.txtngay.Name = "txtngay";
            this.txtngay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtngay.Properties.Appearance.Options.UseFont = true;
            this.txtngay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtngay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.txtngay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtngay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.txtngay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtngay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.txtngay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtngay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.txtngay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtngay.Size = new System.Drawing.Size(193, 26);
            this.txtngay.TabIndex = 5;
            // 
            // btn_Danhsachdatban
            // 
            this.btn_Danhsachdatban.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Danhsachdatban.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btn_Danhsachdatban.Appearance.Options.UseFont = true;
            this.btn_Danhsachdatban.Location = new System.Drawing.Point(140, 429);
            this.btn_Danhsachdatban.Name = "btn_Danhsachdatban";
            this.btn_Danhsachdatban.Size = new System.Drawing.Size(124, 35);
            this.btn_Danhsachdatban.TabIndex = 23;
            this.btn_Danhsachdatban.Text = "Danh sách đặt bàn";
            this.btn_Danhsachdatban.Click += new System.EventHandler(this.btn_Danhsachdatban_Click);
            // 
            // chkEmail
            // 
            this.chkEmail.EnterMoveNextControl = true;
            this.chkEmail.Location = new System.Drawing.Point(423, 129);
            this.chkEmail.Name = "chkEmail";
            this.chkEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkEmail.Properties.Appearance.Options.UseFont = true;
            this.chkEmail.Properties.Caption = "  Gửi Email";
            this.chkEmail.Size = new System.Drawing.Size(74, 19);
            this.chkEmail.TabIndex = 44;
            // 
            // txtEmail
            // 
            this.txtEmail.EnterMoveNextControl = true;
            this.txtEmail.Location = new System.Drawing.Point(425, 60);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtEmail.Properties.Appearance.Options.UseFont = true;
            this.txtEmail.Size = new System.Drawing.Size(193, 26);
            this.txtEmail.TabIndex = 9;
            this.txtEmail.Click += new System.EventHandler(this.txtEmail_Click);
            this.txtEmail.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtEmail_Click);
            // 
            // btnMoBanNgay
            // 
            this.btnMoBanNgay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMoBanNgay.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnMoBanNgay.Appearance.Options.UseFont = true;
            this.btnMoBanNgay.Location = new System.Drawing.Point(269, 429);
            this.btnMoBanNgay.Name = "btnMoBanNgay";
            this.btnMoBanNgay.Size = new System.Drawing.Size(119, 35);
            this.btnMoBanNgay.TabIndex = 25;
            this.btnMoBanNgay.Text = "Mở Bàn Ngay";
            this.btnMoBanNgay.Click += new System.EventHandler(this.btnMoBanNgay_Click);
            // 
            // txtPartMoneyPayments
            // 
            this.txtPartMoneyPayments.EditValue = "";
            this.txtPartMoneyPayments.EnterMoveNextControl = true;
            this.txtPartMoneyPayments.Location = new System.Drawing.Point(97, 124);
            this.txtPartMoneyPayments.Name = "txtPartMoneyPayments";
            this.txtPartMoneyPayments.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtPartMoneyPayments.Properties.Appearance.Options.UseFont = true;
            this.txtPartMoneyPayments.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPartMoneyPayments.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtPartMoneyPayments.Properties.DisplayFormat.FormatString = "N0";
            this.txtPartMoneyPayments.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtPartMoneyPayments.Properties.EditFormat.FormatString = "N0";
            this.txtPartMoneyPayments.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtPartMoneyPayments.Properties.Mask.EditMask = "N0";
            this.txtPartMoneyPayments.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPartMoneyPayments.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtPartMoneyPayments.Size = new System.Drawing.Size(193, 26);
            this.txtPartMoneyPayments.TabIndex = 11;
            this.txtPartMoneyPayments.Click += new System.EventHandler(this.txtPartMoneyPayments_Click);
            // 
            // btnMobansau
            // 
            this.btnMobansau.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMobansau.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnMobansau.Appearance.Options.UseFont = true;
            this.btnMobansau.Location = new System.Drawing.Point(397, 430);
            this.btnMobansau.Name = "btnMobansau";
            this.btnMobansau.Size = new System.Drawing.Size(119, 35);
            this.btnMobansau.TabIndex = 27;
            this.btnMobansau.Text = "Mở Bàn Sau";
            this.btnMobansau.Click += new System.EventHandler(this.btnMobansau_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Close.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btn_Close.Appearance.Options.UseFont = true;
            this.btn_Close.Location = new System.Drawing.Point(526, 429);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(95, 35);
            this.btn_Close.TabIndex = 33;
            this.btn_Close.Text = "Đóng";
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemove.Location = new System.Drawing.Point(526, 155);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(95, 35);
            this.btnRemove.TabIndex = 21;
            this.btnRemove.Text = "Hủy Bàn";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // panelControl48
            // 
            this.panelControl48.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl48.Controls.Add(this.flCheckTable);
            this.panelControl48.Location = new System.Drawing.Point(5, 194);
            this.panelControl48.Name = "panelControl48";
            this.panelControl48.Size = new System.Drawing.Size(629, 231);
            this.panelControl48.TabIndex = 31;
            // 
            // flCheckTable
            // 
            this.flCheckTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flCheckTable.Location = new System.Drawing.Point(2, 2);
            this.flCheckTable.Name = "flCheckTable";
            this.flCheckTable.Size = new System.Drawing.Size(625, 227);
            this.flCheckTable.TabIndex = 0;
            // 
            // cbMM
            // 
            this.cbMM.EnterMoveNextControl = true;
            this.cbMM.Location = new System.Drawing.Point(535, 93);
            this.cbMM.Name = "cbMM";
            this.cbMM.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cbMM.Properties.Appearance.Options.UseFont = true;
            this.cbMM.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMM.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbMM.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbMM.Properties.DropDownItemHeight = 25;
            this.cbMM.Properties.DropDownRows = 15;
            this.cbMM.Properties.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50",
            "51",
            "52",
            "53",
            "54",
            "55",
            "56",
            "57",
            "58",
            "59"});
            this.cbMM.Size = new System.Drawing.Size(83, 26);
            this.cbMM.TabIndex = 15;
            this.cbMM.Click += new System.EventHandler(this.cbMM_Click);
            // 
            // cbHH
            // 
            this.cbHH.EnterMoveNextControl = true;
            this.cbHH.Location = new System.Drawing.Point(425, 93);
            this.cbHH.Name = "cbHH";
            this.cbHH.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cbHH.Properties.Appearance.Options.UseFont = true;
            this.cbHH.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHH.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbHH.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbHH.Properties.DropDownItemHeight = 25;
            this.cbHH.Properties.DropDownRows = 15;
            this.cbHH.Properties.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23"});
            this.cbHH.Size = new System.Drawing.Size(84, 26);
            this.cbHH.TabIndex = 13;
            this.cbHH.Click += new System.EventHandler(this.cbHH_Click);
            // 
            // labelControl38
            // 
            this.labelControl38.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl38.Location = new System.Drawing.Point(519, 95);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(6, 19);
            this.labelControl38.TabIndex = 26;
            this.labelControl38.Text = ":";
            // 
            // btnSendEmail
            // 
            this.btnSendEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendEmail.Location = new System.Drawing.Point(397, 156);
            this.btnSendEmail.Name = "btnSendEmail";
            this.btnSendEmail.Size = new System.Drawing.Size(119, 35);
            this.btnSendEmail.TabIndex = 19;
            this.btnSendEmail.Text = "Gửi Email";
            this.btnSendEmail.Click += new System.EventHandler(this.btnSendEmail_Click);
            // 
            // lb_Gio
            // 
            this.lb_Gio.Location = new System.Drawing.Point(349, 99);
            this.lb_Gio.Name = "lb_Gio";
            this.lb_Gio.Size = new System.Drawing.Size(15, 13);
            this.lb_Gio.TabIndex = 24;
            this.lb_Gio.Text = "Giờ";
            // 
            // btn_SelectDishes
            // 
            this.btn_SelectDishes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_SelectDishes.Location = new System.Drawing.Point(269, 156);
            this.btn_SelectDishes.Name = "btn_SelectDishes";
            this.btn_SelectDishes.Size = new System.Drawing.Size(119, 35);
            this.btn_SelectDishes.TabIndex = 18;
            this.btn_SelectDishes.Text = "Chọn Món...";
            this.btn_SelectDishes.Click += new System.EventHandler(this.btn_SelectDishes_Click);
            // 
            // btn_SelectTable
            // 
            this.btn_SelectTable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_SelectTable.Location = new System.Drawing.Point(140, 156);
            this.btn_SelectTable.Name = "btn_SelectTable";
            this.btn_SelectTable.Size = new System.Drawing.Size(119, 35);
            this.btn_SelectTable.TabIndex = 17;
            this.btn_SelectTable.Text = "Chọn Bàn...";
            this.btn_SelectTable.Click += new System.EventHandler(this.btn_SelectTable_Click);
            // 
            // lb_Diachi
            // 
            this.lb_Diachi.Location = new System.Drawing.Point(349, 35);
            this.lb_Diachi.Name = "lb_Diachi";
            this.lb_Diachi.Size = new System.Drawing.Size(34, 13);
            this.lb_Diachi.TabIndex = 15;
            this.lb_Diachi.Text = "Địa Chỉ";
            // 
            // lb_Tiendatcoc
            // 
            this.lb_Tiendatcoc.Location = new System.Drawing.Point(13, 132);
            this.lb_Tiendatcoc.Name = "lb_Tiendatcoc";
            this.lb_Tiendatcoc.Size = new System.Drawing.Size(62, 13);
            this.lb_Tiendatcoc.TabIndex = 14;
            this.lb_Tiendatcoc.Text = "Tiền Đặt Cọc";
            // 
            // lb_ngay2
            // 
            this.lb_ngay2.Location = new System.Drawing.Point(13, 100);
            this.lb_ngay2.Name = "lb_ngay2";
            this.lb_ngay2.Size = new System.Drawing.Size(28, 13);
            this.lb_ngay2.TabIndex = 13;
            this.lb_ngay2.Text = "Ngày ";
            // 
            // lb_dienthoai2
            // 
            this.lb_dienthoai2.Location = new System.Drawing.Point(13, 70);
            this.lb_dienthoai2.Name = "lb_dienthoai2";
            this.lb_dienthoai2.Size = new System.Drawing.Size(51, 13);
            this.lb_dienthoai2.TabIndex = 10;
            this.lb_dienthoai2.Text = "Điện Thoại";
            // 
            // lb_TenKH2
            // 
            this.lb_TenKH2.Location = new System.Drawing.Point(13, 38);
            this.lb_TenKH2.Name = "lb_TenKH2";
            this.lb_TenKH2.Size = new System.Drawing.Size(78, 13);
            this.lb_TenKH2.TabIndex = 9;
            this.lb_TenKH2.Text = "Tên Khách Hàng";
            // 
            // txtAddress
            // 
            this.txtAddress.EnterMoveNextControl = true;
            this.txtAddress.Location = new System.Drawing.Point(425, 26);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtAddress.Properties.Appearance.Options.UseFont = true;
            this.txtAddress.Size = new System.Drawing.Size(193, 26);
            this.txtAddress.TabIndex = 7;
            this.txtAddress.Click += new System.EventHandler(this.txtAddress_Click);
            // 
            // txtDienThoai
            // 
            this.txtDienThoai.EnterMoveNextControl = true;
            this.txtDienThoai.Location = new System.Drawing.Point(97, 60);
            this.txtDienThoai.Name = "txtDienThoai";
            this.txtDienThoai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtDienThoai.Properties.Appearance.Options.UseFont = true;
            this.txtDienThoai.Size = new System.Drawing.Size(193, 26);
            this.txtDienThoai.TabIndex = 3;
            this.txtDienThoai.Click += new System.EventHandler(this.txtDienThoai_Click);
            // 
            // txtDBTenKH
            // 
            this.txtDBTenKH.EnterMoveNextControl = true;
            this.txtDBTenKH.Location = new System.Drawing.Point(97, 27);
            this.txtDBTenKH.Name = "txtDBTenKH";
            this.txtDBTenKH.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtDBTenKH.Properties.Appearance.Options.UseFont = true;
            this.txtDBTenKH.Size = new System.Drawing.Size(193, 26);
            this.txtDBTenKH.TabIndex = 0;
            this.txtDBTenKH.Click += new System.EventHandler(this.txtDBTenKH_Click);
            // 
            // pnInlaihoadon
            // 
            this.pnInlaihoadon.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnInlaihoadon.Controls.Add(this.panelControl53);
            this.pnInlaihoadon.Controls.Add(this.panelControl52);
            this.pnInlaihoadon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnInlaihoadon.Location = new System.Drawing.Point(0, 0);
            this.pnInlaihoadon.Name = "pnInlaihoadon";
            this.pnInlaihoadon.Size = new System.Drawing.Size(674, 698);
            this.pnInlaihoadon.TabIndex = 2;
            // 
            // panelControl53
            // 
            this.panelControl53.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl53.Controls.Add(this.gr_billPaymented);
            this.panelControl53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl53.Location = new System.Drawing.Point(0, 0);
            this.panelControl53.Name = "panelControl53";
            this.panelControl53.Size = new System.Drawing.Size(634, 698);
            this.panelControl53.TabIndex = 2;
            // 
            // gr_billPaymented
            // 
            this.gr_billPaymented.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gr_billPaymented.Location = new System.Drawing.Point(0, 0);
            this.gr_billPaymented.MainView = this.gvBillPaymented;
            this.gr_billPaymented.Name = "gr_billPaymented";
            this.gr_billPaymented.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1,
            this.repositoryItemButtonEdit2,
            this.repositoryItemButtonEdit3});
            this.gr_billPaymented.Size = new System.Drawing.Size(634, 698);
            this.gr_billPaymented.TabIndex = 1;
            this.gr_billPaymented.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvBillPaymented});
            // 
            // gvBillPaymented
            // 
            this.gvBillPaymented.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.gvBillPaymented.Appearance.FocusedRow.Options.UseFont = true;
            this.gvBillPaymented.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.gvBillPaymented.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvBillPaymented.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.75F);
            this.gvBillPaymented.Appearance.Row.Options.UseFont = true;
            this.gvBillPaymented.ColumnPanelRowHeight = 45;
            this.gvBillPaymented.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SSTT,
            this.MA_HOADON,
            this.NGAYTAO,
            this.TEN_KHUVUC,
            this.TONGTIEN,
            this.TEN_BAN,
            this.IN,
            this.HUY,
            this.LAYLAI});
            this.gvBillPaymented.GridControl = this.gr_billPaymented;
            this.gvBillPaymented.Name = "gvBillPaymented";
            this.gvBillPaymented.OptionsView.EnableAppearanceEvenRow = true;
            this.gvBillPaymented.OptionsView.ShowGroupPanel = false;
            this.gvBillPaymented.OptionsView.ShowIndicator = false;
            this.gvBillPaymented.RowHeight = 45;
            this.gvBillPaymented.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gvBillPaymented.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvBillPaymented_RowCellClick);
            this.gvBillPaymented.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gvBillPaymented_CustomDrawCell);
            // 
            // SSTT
            // 
            this.SSTT.AppearanceCell.Options.UseTextOptions = true;
            this.SSTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SSTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SSTT.AppearanceHeader.Options.UseFont = true;
            this.SSTT.AppearanceHeader.Options.UseTextOptions = true;
            this.SSTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SSTT.Caption = "STT";
            this.SSTT.FieldName = "SSTT";
            this.SSTT.MinWidth = 40;
            this.SSTT.Name = "SSTT";
            this.SSTT.OptionsColumn.AllowEdit = false;
            this.SSTT.OptionsColumn.AllowFocus = false;
            this.SSTT.OptionsFilter.AllowAutoFilter = false;
            this.SSTT.OptionsFilter.AllowFilter = false;
            this.SSTT.Visible = true;
            this.SSTT.VisibleIndex = 0;
            this.SSTT.Width = 40;
            // 
            // MA_HOADON
            // 
            this.MA_HOADON.AppearanceCell.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_HOADON.AppearanceHeader.Options.UseFont = true;
            this.MA_HOADON.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.Caption = "Mã Hóa Đơn";
            this.MA_HOADON.FieldName = "MA_HOADON";
            this.MA_HOADON.MaxWidth = 150;
            this.MA_HOADON.MinWidth = 135;
            this.MA_HOADON.Name = "MA_HOADON";
            this.MA_HOADON.OptionsColumn.AllowEdit = false;
            this.MA_HOADON.OptionsColumn.AllowFocus = false;
            this.MA_HOADON.Visible = true;
            this.MA_HOADON.VisibleIndex = 1;
            this.MA_HOADON.Width = 135;
            // 
            // NGAYTAO
            // 
            this.NGAYTAO.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYTAO.AppearanceHeader.Options.UseFont = true;
            this.NGAYTAO.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.Caption = "Ngày Tạo";
            this.NGAYTAO.DisplayFormat.FormatString = "{0:dd/MM/yyyy HH:mm}";
            this.NGAYTAO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYTAO.FieldName = "NGAYTAO";
            this.NGAYTAO.MaxWidth = 150;
            this.NGAYTAO.MinWidth = 135;
            this.NGAYTAO.Name = "NGAYTAO";
            this.NGAYTAO.OptionsColumn.AllowEdit = false;
            this.NGAYTAO.OptionsColumn.AllowFocus = false;
            this.NGAYTAO.Visible = true;
            this.NGAYTAO.VisibleIndex = 4;
            this.NGAYTAO.Width = 135;
            // 
            // TEN_KHUVUC
            // 
            this.TEN_KHUVUC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_KHUVUC.AppearanceHeader.Options.UseFont = true;
            this.TEN_KHUVUC.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_KHUVUC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_KHUVUC.Caption = "Khu vực";
            this.TEN_KHUVUC.FieldName = "TEN_KHUVUC";
            this.TEN_KHUVUC.MinWidth = 70;
            this.TEN_KHUVUC.Name = "TEN_KHUVUC";
            this.TEN_KHUVUC.OptionsColumn.AllowEdit = false;
            this.TEN_KHUVUC.OptionsColumn.AllowFocus = false;
            this.TEN_KHUVUC.OptionsFilter.AllowAutoFilter = false;
            this.TEN_KHUVUC.OptionsFilter.AllowFilter = false;
            this.TEN_KHUVUC.Visible = true;
            this.TEN_KHUVUC.VisibleIndex = 2;
            this.TEN_KHUVUC.Width = 70;
            // 
            // TONGTIEN
            // 
            this.TONGTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TONGTIEN.AppearanceHeader.Options.UseFont = true;
            this.TONGTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGTIEN.Caption = "Tổng Tiền";
            this.TONGTIEN.DisplayFormat.FormatString = "N0";
            this.TONGTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TONGTIEN.FieldName = "TONGTIEN";
            this.TONGTIEN.MaxWidth = 120;
            this.TONGTIEN.MinWidth = 70;
            this.TONGTIEN.Name = "TONGTIEN";
            this.TONGTIEN.OptionsColumn.AllowEdit = false;
            this.TONGTIEN.OptionsColumn.AllowFocus = false;
            this.TONGTIEN.Visible = true;
            this.TONGTIEN.VisibleIndex = 5;
            this.TONGTIEN.Width = 70;
            // 
            // TEN_BAN
            // 
            this.TEN_BAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_BAN.AppearanceHeader.Options.UseFont = true;
            this.TEN_BAN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_BAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_BAN.Caption = "Bàn";
            this.TEN_BAN.FieldName = "TEN_BAN";
            this.TEN_BAN.MinWidth = 70;
            this.TEN_BAN.Name = "TEN_BAN";
            this.TEN_BAN.OptionsColumn.AllowEdit = false;
            this.TEN_BAN.OptionsColumn.AllowFocus = false;
            this.TEN_BAN.OptionsFilter.AllowAutoFilter = false;
            this.TEN_BAN.OptionsFilter.AllowFilter = false;
            this.TEN_BAN.Visible = true;
            this.TEN_BAN.VisibleIndex = 3;
            this.TEN_BAN.Width = 70;
            // 
            // IN
            // 
            this.IN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IN.AppearanceHeader.Options.UseFont = true;
            this.IN.AppearanceHeader.Options.UseTextOptions = true;
            this.IN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.IN.Caption = "In";
            this.IN.ColumnEdit = this.repositoryItemButtonEdit1;
            this.IN.MaxWidth = 100;
            this.IN.MinWidth = 70;
            this.IN.Name = "IN";
            this.IN.OptionsColumn.AllowEdit = false;
            this.IN.OptionsColumn.AllowFocus = false;
            this.IN.OptionsColumn.FixedWidth = true;
            this.IN.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.IN.Visible = true;
            this.IN.VisibleIndex = 6;
            this.IN.Width = 70;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit1.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // HUY
            // 
            this.HUY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HUY.AppearanceHeader.Options.UseFont = true;
            this.HUY.AppearanceHeader.Options.UseTextOptions = true;
            this.HUY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HUY.Caption = "Hủy";
            this.HUY.ColumnEdit = this.repositoryItemButtonEdit2;
            this.HUY.MaxWidth = 100;
            this.HUY.MinWidth = 70;
            this.HUY.Name = "HUY";
            this.HUY.OptionsColumn.AllowEdit = false;
            this.HUY.OptionsColumn.AllowFocus = false;
            this.HUY.OptionsColumn.FixedWidth = true;
            this.HUY.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.HUY.Visible = true;
            this.HUY.VisibleIndex = 7;
            this.HUY.Width = 70;
            // 
            // repositoryItemButtonEdit2
            // 
            this.repositoryItemButtonEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit2.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
            this.repositoryItemButtonEdit2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // LAYLAI
            // 
            this.LAYLAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LAYLAI.AppearanceHeader.Options.UseFont = true;
            this.LAYLAI.AppearanceHeader.Options.UseTextOptions = true;
            this.LAYLAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LAYLAI.Caption = "Lấy lại HĐ";
            this.LAYLAI.ColumnEdit = this.repositoryItemButtonEdit3;
            this.LAYLAI.MaxWidth = 100;
            this.LAYLAI.MinWidth = 70;
            this.LAYLAI.Name = "LAYLAI";
            this.LAYLAI.OptionsColumn.AllowEdit = false;
            this.LAYLAI.OptionsColumn.AllowFocus = false;
            this.LAYLAI.OptionsColumn.FixedWidth = true;
            this.LAYLAI.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.LAYLAI.Visible = true;
            this.LAYLAI.VisibleIndex = 8;
            this.LAYLAI.Width = 70;
            // 
            // repositoryItemButtonEdit3
            // 
            this.repositoryItemButtonEdit3.AutoHeight = false;
            this.repositoryItemButtonEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit3.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", null, null, true)});
            this.repositoryItemButtonEdit3.Name = "repositoryItemButtonEdit3";
            this.repositoryItemButtonEdit3.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // panelControl52
            // 
            this.panelControl52.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl52.Controls.Add(this.labelControl43);
            this.panelControl52.Controls.Add(this.lbTongSoRow);
            this.panelControl52.Controls.Add(this.labelControl45);
            this.panelControl52.Controls.Add(this.btn_upbill);
            this.panelControl52.Controls.Add(this.btnDowBill);
            this.panelControl52.Controls.Add(this.btnDowLastBill);
            this.panelControl52.Controls.Add(this.btn_UpTopBill);
            this.panelControl52.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl52.Location = new System.Drawing.Point(634, 0);
            this.panelControl52.Name = "panelControl52";
            this.panelControl52.Size = new System.Drawing.Size(40, 698);
            this.panelControl52.TabIndex = 1;
            // 
            // labelControl43
            // 
            this.labelControl43.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl43.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl43.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl43.Location = new System.Drawing.Point(10, 323);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(20, 13);
            this.labelControl43.TabIndex = 14;
            this.labelControl43.Text = "----";
            // 
            // lbTongSoRow
            // 
            this.lbTongSoRow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTongSoRow.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbTongSoRow.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbTongSoRow.Location = new System.Drawing.Point(12, 337);
            this.lbTongSoRow.Name = "lbTongSoRow";
            this.lbTongSoRow.Size = new System.Drawing.Size(14, 13);
            this.lbTongSoRow.TabIndex = 13;
            this.lbTongSoRow.Text = "00";
            // 
            // labelControl45
            // 
            this.labelControl45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl45.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl45.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl45.Location = new System.Drawing.Point(12, 311);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(14, 13);
            this.labelControl45.TabIndex = 12;
            this.labelControl45.Text = "00";
            // 
            // btn_upbill
            // 
            this.btn_upbill.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_upbill.Image = ((System.Drawing.Image)(resources.GetObject("btn_upbill.Image")));
            this.btn_upbill.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_upbill.Location = new System.Drawing.Point(0, 80);
            this.btn_upbill.Name = "btn_upbill";
            this.btn_upbill.Size = new System.Drawing.Size(40, 80);
            this.btn_upbill.TabIndex = 11;
            this.btn_upbill.Click += new System.EventHandler(this.btn_upbill_Click);
            // 
            // btnDowBill
            // 
            this.btnDowBill.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnDowBill.Image = ((System.Drawing.Image)(resources.GetObject("btnDowBill.Image")));
            this.btnDowBill.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDowBill.Location = new System.Drawing.Point(0, 538);
            this.btnDowBill.Name = "btnDowBill";
            this.btnDowBill.Size = new System.Drawing.Size(40, 80);
            this.btnDowBill.TabIndex = 10;
            this.btnDowBill.Click += new System.EventHandler(this.btnDowBill_Click);
            // 
            // btnDowLastBill
            // 
            this.btnDowLastBill.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnDowLastBill.Image = ((System.Drawing.Image)(resources.GetObject("btnDowLastBill.Image")));
            this.btnDowLastBill.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDowLastBill.Location = new System.Drawing.Point(0, 618);
            this.btnDowLastBill.Name = "btnDowLastBill";
            this.btnDowLastBill.Size = new System.Drawing.Size(40, 80);
            this.btnDowLastBill.TabIndex = 9;
            this.btnDowLastBill.Text = "simpleButton4";
            this.btnDowLastBill.Click += new System.EventHandler(this.btnDowLastBill_Click);
            // 
            // btn_UpTopBill
            // 
            this.btn_UpTopBill.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_UpTopBill.Image = ((System.Drawing.Image)(resources.GetObject("btn_UpTopBill.Image")));
            this.btn_UpTopBill.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_UpTopBill.Location = new System.Drawing.Point(0, 0);
            this.btn_UpTopBill.Name = "btn_UpTopBill";
            this.btn_UpTopBill.Size = new System.Drawing.Size(40, 80);
            this.btn_UpTopBill.TabIndex = 8;
            this.btn_UpTopBill.Text = "simpleButton1";
            this.btn_UpTopBill.Click += new System.EventHandler(this.btn_UpTopBill_Click);
            // 
            // pnWapper
            // 
            this.pnWapper.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnWapper.Controls.Add(this.pnShowHang);
            this.pnWapper.Controls.Add(this.pnTachban);
            this.pnWapper.Controls.Add(this.pnInlaihoadon);
            this.pnWapper.Controls.Add(this.pnDatban);
            this.pnWapper.Controls.Add(this.pnYeucauthem);
            this.pnWapper.Controls.Add(this.pnMain);
            this.pnWapper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnWapper.Location = new System.Drawing.Point(0, 70);
            this.pnWapper.Name = "pnWapper";
            this.pnWapper.Size = new System.Drawing.Size(1024, 698);
            this.pnWapper.TabIndex = 24;
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 120000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // pnHeader
            // 
            this.pnHeader.Controls.Add(this.panelControl6);
            this.pnHeader.Controls.Add(this.panelControl1);
            this.pnHeader.Controls.Add(this.panelControl10);
            this.pnHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnHeader.Location = new System.Drawing.Point(0, 0);
            this.pnHeader.Name = "pnHeader";
            this.pnHeader.Size = new System.Drawing.Size(1024, 70);
            this.pnHeader.TabIndex = 25;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.lb_Quay);
            this.panelControl6.Controls.Add(this.lb_Kho);
            this.panelControl6.Controls.Add(this.lbgiay);
            this.panelControl6.Controls.Add(this.lbCaBan);
            this.panelControl6.Controls.Add(this.lbGio);
            this.panelControl6.Controls.Add(this.lb_NhanVien);
            this.panelControl6.Controls.Add(this.lbtimeby);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(182, 2);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(492, 66);
            this.panelControl6.TabIndex = 23;
            // 
            // lb_Quay
            // 
            this.lb_Quay.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_Quay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_Quay.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_Quay.Location = new System.Drawing.Point(4, 5);
            this.lb_Quay.Name = "lb_Quay";
            this.lb_Quay.Size = new System.Drawing.Size(41, 19);
            this.lb_Quay.TabIndex = 14;
            this.lb_Quay.Text = "Quầy";
            // 
            // lb_Kho
            // 
            this.lb_Kho.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_Kho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_Kho.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_Kho.Location = new System.Drawing.Point(4, 34);
            this.lb_Kho.Name = "lb_Kho";
            this.lb_Kho.Size = new System.Drawing.Size(41, 19);
            this.lb_Kho.TabIndex = 15;
            this.lb_Kho.Text = "Quầy";
            // 
            // lbgiay
            // 
            this.lbgiay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbgiay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbgiay.Location = new System.Drawing.Point(468, 27);
            this.lbgiay.Name = "lbgiay";
            this.lbgiay.Size = new System.Drawing.Size(18, 19);
            this.lbgiay.TabIndex = 19;
            this.lbgiay.Text = "00";
            // 
            // lbCaBan
            // 
            this.lbCaBan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCaBan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbCaBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbCaBan.Location = new System.Drawing.Point(394, 5);
            this.lbCaBan.Name = "lbCaBan";
            this.lbCaBan.Size = new System.Drawing.Size(94, 19);
            this.lbCaBan.TabIndex = 17;
            this.lbCaBan.Text = "21-12-2012";
            // 
            // lbGio
            // 
            this.lbGio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGio.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGio.Location = new System.Drawing.Point(419, 34);
            this.lbGio.Name = "lbGio";
            this.lbGio.Size = new System.Drawing.Size(46, 19);
            this.lbGio.TabIndex = 20;
            this.lbGio.Text = "00:00";
            // 
            // lb_NhanVien
            // 
            this.lb_NhanVien.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lb_NhanVien.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_NhanVien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_NhanVien.Location = new System.Drawing.Point(144, 5);
            this.lb_NhanVien.Name = "lb_NhanVien";
            this.lb_NhanVien.Size = new System.Drawing.Size(89, 19);
            this.lb_NhanVien.TabIndex = 18;
            this.lb_NhanVien.Text = "Nhân Viên:";
            // 
            // lbtimeby
            // 
            this.lbtimeby.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbtimeby.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbtimeby.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbtimeby.Location = new System.Drawing.Point(144, 34);
            this.lbtimeby.Name = "lbtimeby";
            this.lbtimeby.Size = new System.Drawing.Size(21, 19);
            this.lbtimeby.TabIndex = 16;
            this.lbtimeby.Text = "Ca";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl37);
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl1.Location = new System.Drawing.Point(674, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(348, 66);
            this.panelControl1.TabIndex = 22;
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.lbKhuVuc);
            this.panelControl3.Controls.Add(this.lbBan);
            this.panelControl3.Controls.Add(this.txtID);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(2, 2);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(344, 31);
            this.panelControl3.TabIndex = 1;
            // 
            // panelControl10
            // 
            this.panelControl10.Controls.Add(this.pictureBox1);
            this.panelControl10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl10.Location = new System.Drawing.Point(2, 2);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(180, 66);
            this.panelControl10.TabIndex = 21;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(8, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(177, 41);
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // Frm_BanHang_Buffet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.pnWapper);
            this.Controls.Add(this.pnHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Frm_BanHang_Buffet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_BanHang";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frm_BanHang_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_BanHang_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_BanHang_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pnShowHang)).EndInit();
            this.pnShowHang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.panelControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlBuffet)).EndInit();
            this.pnlBuffet.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).EndInit();
            this.panelControl16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnKV_Mon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl27)).EndInit();
            this.panelControl27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl28)).EndInit();
            this.panelControl28.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl37)).EndInit();
            this.panelControl37.ResumeLayout(false);
            this.panelControl37.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnMain)).EndInit();
            this.pnMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_listProduct)).EndInit();
            this.pn_listProduct.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_SelectProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_SelectProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDowGv)).EndInit();
            this.pnUpDowGv.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnBtnDeleteGv)).EndInit();
            this.pnBtnDeleteGv.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl29)).EndInit();
            this.panelControl29.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl33)).EndInit();
            this.panelControl33.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl31)).EndInit();
            this.panelControl31.ResumeLayout(false);
            this.panelControl31.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl32)).EndInit();
            this.panelControl32.ResumeLayout(false);
            this.panelControl32.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl20)).EndInit();
            this.panelControl20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlInforMoney)).EndInit();
            this.pnlInforMoney.ResumeLayout(false);
            this.pnlInforMoney.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).EndInit();
            this.panelControl13.ResumeLayout(false);
            this.panelControl13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl23)).EndInit();
            this.panelControl23.ResumeLayout(false);
            this.panelControl23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl50)).EndInit();
            this.panelControl50.ResumeLayout(false);
            this.panelControl50.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnNghiepVu_ThuNgan)).EndInit();
            this.pnNghiepVu_ThuNgan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnDacThu)).EndInit();
            this.pnDacThu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnThuNgan)).EndInit();
            this.pnThuNgan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnNghiepVu)).EndInit();
            this.pnNghiepVu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnTachban)).EndInit();
            this.pnTachban.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_GroupTable)).EndInit();
            this.pn_GroupTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDow)).EndInit();
            this.pnUpDow.ResumeLayout(false);
            this.pnUpDow.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            this.panelControl15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_divide_Table_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_divide_Table_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_TableTow)).EndInit();
            this.pn_TableTow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_divide_Table_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_divide_Table_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnInforTablecheck)).EndInit();
            this.pnInforTablecheck.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).EndInit();
            this.panelControl18.ResumeLayout(false);
            this.panelControl18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).EndInit();
            this.panelControl14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTenKhacHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmaBanGoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmaKVBanGoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_TableShare)).EndInit();
            this.pn_TableShare.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTenKhachHangGhep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmaBanGhep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmaKVBanGhep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnYeucauthem)).EndInit();
            this.pnYeucauthem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).EndInit();
            this.panelControl19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).EndInit();
            this.panelControl22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl21)).EndInit();
            this.panelControl21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtYeuCauThem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).EndInit();
            this.panelControl17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            this.panelControl11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnDatban)).EndInit();
            this.pnDatban.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl45)).EndInit();
            this.panelControl45.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl46)).EndInit();
            this.panelControl46.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnInforTable)).EndInit();
            this.pnInforTable.ResumeLayout(false);
            this.pnInforTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtngay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtngay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartMoneyPayments.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl48)).EndInit();
            this.panelControl48.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbMM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbHH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienThoai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDBTenKH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnInlaihoadon)).EndInit();
            this.pnInlaihoadon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl53)).EndInit();
            this.panelControl53.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_billPaymented)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBillPaymented)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl52)).EndInit();
            this.panelControl52.ResumeLayout(false);
            this.panelControl52.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnWapper)).EndInit();
            this.pnWapper.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnHeader)).EndInit();
            this.pnHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnShowHang;
        private DevExpress.XtraEditors.PanelControl pnMain;
        private DevExpress.XtraEditors.PanelControl pn_listProduct;
        private DevExpress.XtraEditors.PanelControl pnUpDowGv;
        private DevExpress.XtraEditors.SimpleButton btn_up;
        private DevExpress.XtraEditors.SimpleButton btn_Dow;
        private DevExpress.XtraEditors.SimpleButton btn_DowLast;
        private DevExpress.XtraEditors.SimpleButton btn_UpFirst;
        private DevExpress.XtraGrid.GridControl gr_SelectProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView gv_SelectProduct;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn GIABAN;
        private DevExpress.XtraGrid.Columns.GridColumn Thanh_Tien;
        private DevExpress.XtraEditors.PanelControl pnBtnDeleteGv;
        private DevExpress.XtraGrid.Columns.GridColumn SOTT;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA;
        private DevExpress.XtraEditors.PanelControl pnNghiepVu_ThuNgan;
        private DevExpress.XtraEditors.SimpleButton btn_cong;
        private DevExpress.XtraEditors.SimpleButton btn_xoa;
        private DevExpress.XtraEditors.SimpleButton btn_tru;
        private DevExpress.XtraEditors.SimpleButton btn_XoaTatCa;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.SimpleButton btn_NghiepVu;
        private DevExpress.XtraEditors.SimpleButton btn_ThuNgan;
        private DevExpress.XtraEditors.SimpleButton btn_GuiOrder;
        private DevExpress.XtraEditors.SimpleButton btn_SoDoBan;
        private DevExpress.XtraEditors.SimpleButton btn_TamTinh;
        private DevExpress.XtraEditors.SimpleButton btn_GhepBan;
        private DevExpress.XtraEditors.SimpleButton btn_ThanhToan;
        private DevExpress.XtraEditors.SimpleButton btn_Tachban;
        private DevExpress.XtraEditors.SimpleButton btn_YeuCauThem;
        private DevExpress.XtraEditors.PanelControl pnThuNgan;
        private DevExpress.XtraEditors.PanelControl pnNghiepVu;
        private DevExpress.XtraEditors.SimpleButton btn_HuyBan;
        private DevExpress.XtraEditors.SimpleButton btn_InLaiHoaDon;
        private DevExpress.XtraEditors.SimpleButton btn_HuyMon;
        private DevExpress.XtraEditors.SimpleButton btn_DatBan;
        private DevExpress.XtraEditors.SimpleButton btn_LayLaiHoaDon;
        private DevExpress.XtraEditors.SimpleButton btn_MoKet;
        private DevExpress.XtraEditors.SimpleButton btn_KetCa;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.LabelControl lbKhuVuc;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl lbPageTable;
        private DevExpress.XtraEditors.SimpleButton btn_NextRight;
        private DevExpress.XtraEditors.SimpleButton btn_NextToLeft;
        private DevExpress.XtraEditors.SimpleButton btn_DowKV;
        private DevExpress.XtraEditors.SimpleButton btn_UpKV;
        private DevExpress.XtraEditors.PanelControl pnTachban;
        private DevExpress.XtraEditors.PanelControl pn_GroupTable;
        private DevExpress.XtraEditors.PanelControl pnUpDow;
        private DevExpress.XtraEditors.PanelControl pn_TableTow;
        private DevExpress.XtraEditors.PanelControl panelControl15;
        private DevExpress.XtraEditors.PanelControl pnInforTablecheck;
        private DevExpress.XtraEditors.SimpleButton btn_OkThrough;
        private DevExpress.XtraEditors.PanelControl panelControl18;
        private DevExpress.XtraEditors.PanelControl pn_TableShare;
        private DevExpress.XtraEditors.PanelControl panelControl14;
        private DevExpress.XtraEditors.TextEdit txtTenKhacHang;
        private DevExpress.XtraEditors.TextEdit txtmaBanGoc;
        private DevExpress.XtraEditors.TextEdit txtmaKVBanGoc;
        private DevExpress.XtraEditors.LabelControl lb_TenKH;
        private DevExpress.XtraEditors.LabelControl lb_KhuVuc;
        private DevExpress.XtraEditors.LabelControl lb_Ban;
        private DevExpress.XtraEditors.TextEdit txtTenKhachHangGhep;
        private DevExpress.XtraEditors.TextEdit txtmaBanGhep;
        private DevExpress.XtraEditors.TextEdit txtmaKVBanGhep;
        private DevExpress.XtraEditors.SimpleButton btn_ESCThroughtTable;
        private DevExpress.XtraEditors.SimpleButton btn_ToLeftAll;
        private DevExpress.XtraEditors.SimpleButton btn_Toleft;
        private DevExpress.XtraEditors.SimpleButton btn_ToRight;
        private DevExpress.XtraEditors.SimpleButton btn_ToRightAll;
        private DevExpress.XtraEditors.SimpleButton btn_UpDivide;
        private DevExpress.XtraEditors.SimpleButton btn_DowDivide;
        private DevExpress.XtraEditors.PanelControl pnYeucauthem;
        private DevExpress.XtraEditors.PanelControl panelControl19;
        private DevExpress.XtraEditors.PanelControl panelControl17;
        private DevExpress.XtraEditors.TextEdit txtYeuCauThem;
        private DevExpress.XtraEditors.PanelControl panelControl21;
        private DevExpress.XtraEditors.LabelControl lbTableYeuCauThem;
        private DevExpress.XtraEditors.LabelControl lbkvYeuCauThem;
        private DevExpress.XtraEditors.PanelControl panelControl22;
        private DevExpress.XtraEditors.SimpleButton simpleButton23;
        private DevExpress.XtraEditors.SimpleButton simpleButton24;
        private DevExpress.XtraEditors.PanelControl panelControl20;
        private DevExpress.XtraEditors.LabelControl lbMonYeuCauThem;
        private DevExpress.XtraEditors.LabelControl lb_tongcong;
        private DevExpress.XtraEditors.LabelControl lbBan;
        private DevExpress.XtraEditors.LabelControl lbMon;
        private DevExpress.XtraEditors.PanelControl pnKV_Mon;
        private DevExpress.XtraEditors.SimpleButton btn_Dong;
        private DevExpress.XtraEditors.LabelControl lbTotal;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;
        private DevExpress.XtraEditors.PanelControl panelControl37;
        private DevExpress.XtraGrid.GridControl gr_divide_Table_1;
        private DevExpress.XtraGrid.Views.Grid.GridView gv_divide_Table_1;
        private DevExpress.XtraGrid.Columns.GridColumn _SSTT;
        private DevExpress.XtraGrid.Columns.GridColumn _TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn _SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn _GIABAN;
        private DevExpress.XtraGrid.Columns.GridColumn _Thanh_Tien;
        private DevExpress.XtraGrid.Columns.GridColumn _MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn _GHICHU;
        private DevExpress.XtraGrid.GridControl gr_divide_Table_2;
        private DevExpress.XtraGrid.Views.Grid.GridView gv_divide_Table_2;
        private DevExpress.XtraGrid.Columns.GridColumn __STT;
        private DevExpress.XtraGrid.Columns.GridColumn __TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn __SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn __GIABAN;
        private DevExpress.XtraGrid.Columns.GridColumn __Thanh_Tien;
        private DevExpress.XtraGrid.Columns.GridColumn __MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn __GHICHU;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraGrid.Columns.GridColumn MA_BEP;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl pnDatban;
        private DevExpress.XtraEditors.PanelControl panelControl46;
        private DevExpress.XtraEditors.GroupControl pnInforTable;
        private DevExpress.XtraEditors.PanelControl panelControl45;
        private DevExpress.XtraEditors.SimpleButton btn_SelectDishes;
        private DevExpress.XtraEditors.SimpleButton btn_SelectTable;
        private DevExpress.XtraEditors.LabelControl lb_Diachi;
        private DevExpress.XtraEditors.LabelControl lb_Tiendatcoc;
        private DevExpress.XtraEditors.LabelControl lb_ngay2;
        private DevExpress.XtraEditors.LabelControl lb_dienthoai2;
        private DevExpress.XtraEditors.LabelControl lb_TenKH2;
        private DevExpress.XtraEditors.TextEdit txtAddress;
        private DevExpress.XtraEditors.TextEdit txtDienThoai;
        private DevExpress.XtraEditors.TextEdit txtDBTenKH;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.SimpleButton btnSendEmail;
        private DevExpress.XtraEditors.LabelControl lb_Gio;
        private DevExpress.XtraEditors.ComboBoxEdit cbMM;
        private DevExpress.XtraEditors.ComboBoxEdit cbHH;
        private DevExpress.XtraEditors.PanelControl panelControl48;
        private System.Windows.Forms.FlowLayoutPanel flCheckTable;
        private DevExpress.XtraEditors.SimpleButton btn_Close;
        private DevExpress.XtraEditors.SimpleButton btnRemove;
        private DevExpress.XtraEditors.SimpleButton btnMobansau;
        private DevExpress.XtraEditors.PanelControl pnInlaihoadon;
        private DevExpress.XtraEditors.PanelControl panelControl52;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.LabelControl lbTongSoRow;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.SimpleButton btn_upbill;
        private DevExpress.XtraEditors.SimpleButton btnDowBill;
        private DevExpress.XtraEditors.SimpleButton btnDowLastBill;
        private DevExpress.XtraEditors.SimpleButton btn_UpTopBill;
        private DevExpress.XtraEditors.PanelControl panelControl53;
        private DevExpress.XtraGrid.GridControl gr_billPaymented;
        private DevExpress.XtraGrid.Views.Grid.GridView gvBillPaymented;
        private DevExpress.XtraGrid.Columns.GridColumn SSTT;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HOADON;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYTAO;
        private DevExpress.XtraGrid.Columns.GridColumn TONGTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_BAN;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_KHUVUC;
        private System.Windows.Forms.FlowLayoutPanel pnYeuCau;
        private DevExpress.XtraEditors.TextEdit txtPartMoneyPayments;
        private DevExpress.XtraEditors.SimpleButton btn_InlaiOrder;
        private DevExpress.XtraGrid.Columns.GridColumn _MA_BEP;
        private DevExpress.XtraGrid.Columns.GridColumn THUE;
        private DevExpress.XtraGrid.Columns.GridColumn __MA_BEP;
        private DevExpress.XtraEditors.LabelControl lbGiamGia;
        private DevExpress.XtraEditors.LabelControl lb_giamgia;
        private DevExpress.XtraEditors.LabelControl lbPhuThu;
        private DevExpress.XtraEditors.LabelControl lb_phuthu;
        private DevExpress.XtraEditors.PanelControl pnWapper;
        private DevExpress.XtraEditors.SimpleButton btn_BanGia2;
        private DevExpress.XtraEditors.SimpleButton btn_DatThu;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI;
        private DevExpress.XtraEditors.PanelControl pnlInforMoney;
        private DevExpress.XtraEditors.PanelControl panelControl50;
        private DevExpress.XtraEditors.PanelControl panelControl23;
        private DevExpress.XtraEditors.PanelControl panelControl13;
        private Us_keyboard us_keyboard3;
        private Us_keyboard us_keyboard2;
        private DevExpress.XtraEditors.PanelControl panelControl28;
        private DevExpress.XtraEditors.PanelControl panelControl27;
        private DevExpress.XtraEditors.SimpleButton btn_Danhsachdatban;
        private DevExpress.XtraEditors.SimpleButton btnMoBanNgay;
        private System.Windows.Forms.Timer timer2;
        private DevExpress.XtraEditors.DateEdit txtngay;
        private DevExpress.XtraGrid.Columns.GridColumn IS_COMBO;
        private DevExpress.XtraEditors.CheckEdit chkEmail;
        private DevExpress.XtraEditors.TextEdit txtEmail;
        private DevExpress.XtraGrid.Columns.GridColumn IS_INBEP;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl29;
        private DevExpress.XtraEditors.PanelControl panelControl31;
        private DevExpress.XtraEditors.PanelControl panelControl32;
        private DevExpress.XtraEditors.LabelControl lbSoluong;
        private DevExpress.XtraEditors.LabelControl lb_soluong;
        private DevExpress.XtraEditors.PanelControl panelControl33;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btn9;
        private DevExpress.XtraEditors.SimpleButton btn8;
        private DevExpress.XtraEditors.SimpleButton btn7;
        private DevExpress.XtraEditors.SimpleButton btn6;
        private DevExpress.XtraEditors.SimpleButton btn5;
        private DevExpress.XtraEditors.SimpleButton btn4;
        private DevExpress.XtraEditors.SimpleButton btn3;
        private DevExpress.XtraEditors.SimpleButton btn2;
        private DevExpress.XtraEditors.SimpleButton btn1;
        private DevExpress.XtraEditors.SimpleButton btn0;
        private DevExpress.XtraEditors.SimpleButton btnComboOption;
        private DevExpress.XtraEditors.SimpleButton btn_Chuyenban;
        private DevExpress.XtraGrid.Columns.GridColumn IS_KHUYENMAI;
        private DevExpress.XtraGrid.Columns.GridColumn _IS_COMBO;
        private DevExpress.XtraGrid.Columns.GridColumn __IS_COMBO;
        private DevExpress.XtraGrid.Columns.GridColumn IN;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn HUY;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn LAYLAI;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit3;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraEditors.PanelControl pnDacThu;
        private DevExpress.XtraEditors.SimpleButton btnSuaGia;
        private DevExpress.XtraEditors.XtraScrollableControl pnTable_Mon;
        private DevExpress.XtraEditors.PanelControl pnHeader;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.LookUpEdit txtID;
        private DevExpress.XtraGrid.Columns.GridColumn CHIETKHAU;
        private DevExpress.XtraEditors.SimpleButton btnChietkhau;
        private DevExpress.XtraEditors.SimpleButton btnThemHangHoa;
        private DevExpress.XtraEditors.SimpleButton btn_NhapSL;
        private DevExpress.XtraEditors.SimpleButton btnChongia;
        private DevExpress.XtraEditors.SimpleButton btnBaoCaoBanHang;
        private DevExpress.XtraEditors.SimpleButton btnHetmon;
        private DevExpress.XtraEditors.SimpleButton btnBoKhoaBan;
        private DevExpress.XtraEditors.PanelControl pnlBuffet;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.SimpleButton btn_UpBF;
        private DevExpress.XtraEditors.PanelControl panelControl16;
        private DevExpress.XtraEditors.SimpleButton btn_DowBF;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.LabelControl lb_Quay;
        private DevExpress.XtraEditors.LabelControl lb_Kho;
        private DevExpress.XtraEditors.LabelControl lbgiay;
        private DevExpress.XtraEditors.LabelControl lbCaBan;
        private DevExpress.XtraEditors.LabelControl lbGio;
        private DevExpress.XtraEditors.LabelControl lb_NhanVien;
        private DevExpress.XtraEditors.LabelControl lbtimeby;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.XtraScrollableControl pnBuffet;
        private DevExpress.XtraEditors.SimpleButton btnNangcap;
        private DevExpress.XtraEditors.SimpleButton btnDoihinhthucthanhtoan;
    }
}