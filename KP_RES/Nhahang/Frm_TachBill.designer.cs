﻿namespace KP_RES
{
    partial class Frm_TachBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_TachBill));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            this.pnTachban = new DevExpress.XtraEditors.PanelControl();
            this.pn_GroupTable = new DevExpress.XtraEditors.PanelControl();
            this.pnUpDow = new DevExpress.XtraEditors.PanelControl();
            this.btn_NhapSL = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btn_OkThrough = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ToLeftAll = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ToRightAll = new DevExpress.XtraEditors.SimpleButton();
            this.btn_UpDivide = new DevExpress.XtraEditors.SimpleButton();
            this.btn_DowDivide = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ESCThroughtTable = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ToRight = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Toleft = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.gr_divide_Table_1 = new DevExpress.XtraGrid.GridControl();
            this.gv_divide_Table_1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._SSTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this._TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this._SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this._GIABAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this._Thanh_Tien = new DevExpress.XtraGrid.Columns.GridColumn();
            this._MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this._GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this._MA_BEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this._IS_COMBO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pn_TableTow = new DevExpress.XtraEditors.PanelControl();
            this.gr_divide_Table_2 = new DevExpress.XtraGrid.GridControl();
            this.gv_divide_Table_2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.@__STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__GIABAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__Thanh_Tien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__MA_BEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.@__IS_COMBO = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.pnTachban)).BeginInit();
            this.pnTachban.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_GroupTable)).BeginInit();
            this.pn_GroupTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDow)).BeginInit();
            this.pnUpDow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            this.panelControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_divide_Table_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_divide_Table_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_TableTow)).BeginInit();
            this.pn_TableTow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_divide_Table_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_divide_Table_2)).BeginInit();
            this.SuspendLayout();
            // 
            // pnTachban
            // 
            this.pnTachban.Controls.Add(this.pn_GroupTable);
            this.pnTachban.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnTachban.Location = new System.Drawing.Point(0, 0);
            this.pnTachban.Name = "pnTachban";
            this.pnTachban.Size = new System.Drawing.Size(916, 675);
            this.pnTachban.TabIndex = 1;
            // 
            // pn_GroupTable
            // 
            this.pn_GroupTable.Controls.Add(this.pnUpDow);
            this.pn_GroupTable.Controls.Add(this.panelControl15);
            this.pn_GroupTable.Controls.Add(this.pn_TableTow);
            this.pn_GroupTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_GroupTable.Location = new System.Drawing.Point(2, 2);
            this.pn_GroupTable.Name = "pn_GroupTable";
            this.pn_GroupTable.Size = new System.Drawing.Size(912, 671);
            this.pn_GroupTable.TabIndex = 3;
            // 
            // pnUpDow
            // 
            this.pnUpDow.Controls.Add(this.btn_NhapSL);
            this.pnUpDow.Controls.Add(this.labelControl2);
            this.pnUpDow.Controls.Add(this.btn_OkThrough);
            this.pnUpDow.Controls.Add(this.btn_ToLeftAll);
            this.pnUpDow.Controls.Add(this.btn_ToRightAll);
            this.pnUpDow.Controls.Add(this.btn_UpDivide);
            this.pnUpDow.Controls.Add(this.btn_DowDivide);
            this.pnUpDow.Controls.Add(this.btn_ESCThroughtTable);
            this.pnUpDow.Controls.Add(this.btn_ToRight);
            this.pnUpDow.Controls.Add(this.btn_Toleft);
            this.pnUpDow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnUpDow.Location = new System.Drawing.Point(402, 2);
            this.pnUpDow.Name = "pnUpDow";
            this.pnUpDow.Size = new System.Drawing.Size(108, 667);
            this.pnUpDow.TabIndex = 2;
            // 
            // btn_NhapSL
            // 
            this.btn_NhapSL.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn_NhapSL.Appearance.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.btn_NhapSL.Appearance.Options.UseFont = true;
            this.btn_NhapSL.Appearance.Options.UseForeColor = true;
            this.btn_NhapSL.Location = new System.Drawing.Point(7, 234);
            this.btn_NhapSL.Name = "btn_NhapSL";
            this.btn_NhapSL.Size = new System.Drawing.Size(93, 54);
            this.btn_NhapSL.TabIndex = 13;
            this.btn_NhapSL.Text = "Nhập SL";
            this.btn_NhapSL.Click += new System.EventHandler(this.btn_NhapSL_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl2.Location = new System.Drawing.Point(4, 325);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(100, 13);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "--------------------";
            // 
            // btn_OkThrough
            // 
            this.btn_OkThrough.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn_OkThrough.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btn_OkThrough.Appearance.Options.UseFont = true;
            this.btn_OkThrough.Appearance.Options.UseForeColor = true;
            this.btn_OkThrough.Location = new System.Drawing.Point(7, 64);
            this.btn_OkThrough.Name = "btn_OkThrough";
            this.btn_OkThrough.Size = new System.Drawing.Size(93, 50);
            this.btn_OkThrough.TabIndex = 0;
            this.btn_OkThrough.Text = "OK";
            this.btn_OkThrough.Click += new System.EventHandler(this.btn_OkThrough_Click);
            // 
            // btn_ToLeftAll
            // 
            this.btn_ToLeftAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_ToLeftAll.Image = ((System.Drawing.Image)(resources.GetObject("btn_ToLeftAll.Image")));
            this.btn_ToLeftAll.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_ToLeftAll.Location = new System.Drawing.Point(8, 492);
            this.btn_ToLeftAll.Name = "btn_ToLeftAll";
            this.btn_ToLeftAll.Size = new System.Drawing.Size(93, 50);
            this.btn_ToLeftAll.TabIndex = 4;
            this.btn_ToLeftAll.Text = "<<";
            this.btn_ToLeftAll.Click += new System.EventHandler(this.btn_ToLeftAll_Click);
            // 
            // btn_ToRightAll
            // 
            this.btn_ToRightAll.Image = ((System.Drawing.Image)(resources.GetObject("btn_ToRightAll.Image")));
            this.btn_ToRightAll.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_ToRightAll.Location = new System.Drawing.Point(7, 122);
            this.btn_ToRightAll.Name = "btn_ToRightAll";
            this.btn_ToRightAll.Size = new System.Drawing.Size(93, 50);
            this.btn_ToRightAll.TabIndex = 1;
            this.btn_ToRightAll.Text = ">>";
            this.btn_ToRightAll.Click += new System.EventHandler(this.btn_ToRightAll_Click);
            // 
            // btn_UpDivide
            // 
            this.btn_UpDivide.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_UpDivide.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btn_UpDivide.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_UpDivide.Location = new System.Drawing.Point(2, 2);
            this.btn_UpDivide.Name = "btn_UpDivide";
            this.btn_UpDivide.Size = new System.Drawing.Size(104, 50);
            this.btn_UpDivide.TabIndex = 11;
            this.btn_UpDivide.Text = "simpleButton7";
            this.btn_UpDivide.Click += new System.EventHandler(this.btn_UpDivide_Click);
            // 
            // btn_DowDivide
            // 
            this.btn_DowDivide.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_DowDivide.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btn_DowDivide.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_DowDivide.Location = new System.Drawing.Point(2, 615);
            this.btn_DowDivide.Name = "btn_DowDivide";
            this.btn_DowDivide.Size = new System.Drawing.Size(104, 50);
            this.btn_DowDivide.TabIndex = 10;
            this.btn_DowDivide.Text = "simpleButton6";
            this.btn_DowDivide.Click += new System.EventHandler(this.btn_DowDivide_Click);
            // 
            // btn_ESCThroughtTable
            // 
            this.btn_ESCThroughtTable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_ESCThroughtTable.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn_ESCThroughtTable.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btn_ESCThroughtTable.Appearance.Options.UseFont = true;
            this.btn_ESCThroughtTable.Appearance.Options.UseForeColor = true;
            this.btn_ESCThroughtTable.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_ESCThroughtTable.Location = new System.Drawing.Point(8, 550);
            this.btn_ESCThroughtTable.Name = "btn_ESCThroughtTable";
            this.btn_ESCThroughtTable.Size = new System.Drawing.Size(93, 50);
            this.btn_ESCThroughtTable.TabIndex = 5;
            this.btn_ESCThroughtTable.Text = "ESC";
            this.btn_ESCThroughtTable.Click += new System.EventHandler(this.btn_ESCThroughtTable_Click);
            // 
            // btn_ToRight
            // 
            this.btn_ToRight.Image = ((System.Drawing.Image)(resources.GetObject("btn_ToRight.Image")));
            this.btn_ToRight.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_ToRight.Location = new System.Drawing.Point(7, 178);
            this.btn_ToRight.Name = "btn_ToRight";
            this.btn_ToRight.Size = new System.Drawing.Size(93, 50);
            this.btn_ToRight.TabIndex = 2;
            this.btn_ToRight.Text = ">";
            this.btn_ToRight.Click += new System.EventHandler(this.btn_ToRight_Click);
            // 
            // btn_Toleft
            // 
            this.btn_Toleft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Toleft.Image = ((System.Drawing.Image)(resources.GetObject("btn_Toleft.Image")));
            this.btn_Toleft.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_Toleft.Location = new System.Drawing.Point(8, 434);
            this.btn_Toleft.Name = "btn_Toleft";
            this.btn_Toleft.Size = new System.Drawing.Size(93, 50);
            this.btn_Toleft.TabIndex = 3;
            this.btn_Toleft.Text = "<";
            this.btn_Toleft.Click += new System.EventHandler(this.btn_Toleft_Click);
            // 
            // panelControl15
            // 
            this.panelControl15.Controls.Add(this.gr_divide_Table_1);
            this.panelControl15.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl15.Location = new System.Drawing.Point(2, 2);
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(400, 667);
            this.panelControl15.TabIndex = 0;
            // 
            // gr_divide_Table_1
            // 
            this.gr_divide_Table_1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gr_divide_Table_1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gr_divide_Table_1.Location = new System.Drawing.Point(2, 2);
            this.gr_divide_Table_1.MainView = this.gv_divide_Table_1;
            this.gr_divide_Table_1.Name = "gr_divide_Table_1";
            this.gr_divide_Table_1.Size = new System.Drawing.Size(396, 663);
            this.gr_divide_Table_1.TabIndex = 21;
            this.gr_divide_Table_1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv_divide_Table_1});
            // 
            // gv_divide_Table_1
            // 
            this.gv_divide_Table_1.ColumnPanelRowHeight = 30;
            this.gv_divide_Table_1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._SSTT,
            this._TEN_HANGHOA,
            this._SOLUONG,
            this._GIABAN,
            this._Thanh_Tien,
            this._MA_HANGHOA,
            this._GHICHU,
            this._MA_BEP,
            this._IS_COMBO});
            this.gv_divide_Table_1.GridControl = this.gr_divide_Table_1;
            this.gv_divide_Table_1.Name = "gv_divide_Table_1";
            this.gv_divide_Table_1.OptionsView.ShowFooter = true;
            this.gv_divide_Table_1.OptionsView.ShowGroupPanel = false;
            this.gv_divide_Table_1.OptionsView.ShowIndicator = false;
            this.gv_divide_Table_1.RowHeight = 35;
            this.gv_divide_Table_1.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gv_divide_Table_1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gv_divide_Table_1_CustomDrawCell);
            // 
            // _SSTT
            // 
            this._SSTT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._SSTT.AppearanceCell.Options.UseFont = true;
            this._SSTT.AppearanceCell.Options.UseTextOptions = true;
            this._SSTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._SSTT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._SSTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this._SSTT.AppearanceHeader.Options.UseFont = true;
            this._SSTT.AppearanceHeader.Options.UseTextOptions = true;
            this._SSTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._SSTT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._SSTT.Caption = "TT";
            this._SSTT.FieldName = "_SSTT";
            this._SSTT.Name = "_SSTT";
            this._SSTT.OptionsColumn.AllowEdit = false;
            this._SSTT.OptionsColumn.AllowFocus = false;
            this._SSTT.Visible = true;
            this._SSTT.VisibleIndex = 0;
            this._SSTT.Width = 34;
            // 
            // _TEN_HANGHOA
            // 
            this._TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this._TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this._TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this._TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this._TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._TEN_HANGHOA.Caption = "Hàng Hóa";
            this._TEN_HANGHOA.FieldName = "_TEN_HANGHOA";
            this._TEN_HANGHOA.Name = "_TEN_HANGHOA";
            this._TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this._TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this._TEN_HANGHOA.Visible = true;
            this._TEN_HANGHOA.VisibleIndex = 1;
            this._TEN_HANGHOA.Width = 116;
            // 
            // _SOLUONG
            // 
            this._SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._SOLUONG.AppearanceCell.Options.UseFont = true;
            this._SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this._SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this._SOLUONG.AppearanceHeader.Options.UseFont = true;
            this._SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this._SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._SOLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._SOLUONG.Caption = "SL";
            this._SOLUONG.FieldName = "_SOLUONG";
            this._SOLUONG.Name = "_SOLUONG";
            this._SOLUONG.OptionsColumn.AllowEdit = false;
            this._SOLUONG.OptionsColumn.AllowFocus = false;
            this._SOLUONG.Visible = true;
            this._SOLUONG.VisibleIndex = 2;
            this._SOLUONG.Width = 51;
            // 
            // _GIABAN
            // 
            this._GIABAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._GIABAN.AppearanceCell.Options.UseFont = true;
            this._GIABAN.AppearanceCell.Options.UseTextOptions = true;
            this._GIABAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._GIABAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._GIABAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this._GIABAN.AppearanceHeader.Options.UseFont = true;
            this._GIABAN.AppearanceHeader.Options.UseTextOptions = true;
            this._GIABAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._GIABAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._GIABAN.Caption = "Đơn giá";
            this._GIABAN.DisplayFormat.FormatString = "{0:#,###0}";
            this._GIABAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this._GIABAN.FieldName = "_GIABAN";
            this._GIABAN.Name = "_GIABAN";
            this._GIABAN.OptionsColumn.AllowEdit = false;
            this._GIABAN.OptionsColumn.AllowFocus = false;
            this._GIABAN.Visible = true;
            this._GIABAN.VisibleIndex = 3;
            this._GIABAN.Width = 96;
            // 
            // _Thanh_Tien
            // 
            this._Thanh_Tien.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._Thanh_Tien.AppearanceCell.Options.UseFont = true;
            this._Thanh_Tien.AppearanceCell.Options.UseTextOptions = true;
            this._Thanh_Tien.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._Thanh_Tien.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._Thanh_Tien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this._Thanh_Tien.AppearanceHeader.Options.UseFont = true;
            this._Thanh_Tien.AppearanceHeader.Options.UseTextOptions = true;
            this._Thanh_Tien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._Thanh_Tien.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._Thanh_Tien.Caption = "T Tiền";
            this._Thanh_Tien.DisplayFormat.FormatString = "{0:#,###0}";
            this._Thanh_Tien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this._Thanh_Tien.FieldName = "_Thanh_Tien";
            this._Thanh_Tien.Name = "_Thanh_Tien";
            this._Thanh_Tien.OptionsColumn.AllowEdit = false;
            this._Thanh_Tien.OptionsColumn.AllowFocus = false;
            this._Thanh_Tien.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "_Thanh_Tien", "{0:#,###0}")});
            this._Thanh_Tien.Visible = true;
            this._Thanh_Tien.VisibleIndex = 4;
            this._Thanh_Tien.Width = 104;
            // 
            // _MA_HANGHOA
            // 
            this._MA_HANGHOA.Caption = "Mã Hàng Hóa";
            this._MA_HANGHOA.FieldName = "_MA_HANGHOA";
            this._MA_HANGHOA.Name = "_MA_HANGHOA";
            this._MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this._MA_HANGHOA.OptionsColumn.AllowFocus = false;
            // 
            // _GHICHU
            // 
            this._GHICHU.Caption = "Ghi Chú";
            this._GHICHU.FieldName = "_GHICHU";
            this._GHICHU.Name = "_GHICHU";
            // 
            // _MA_BEP
            // 
            this._MA_BEP.Caption = "May in bep";
            this._MA_BEP.FieldName = "_MA_BEP";
            this._MA_BEP.Name = "_MA_BEP";
            // 
            // _IS_COMBO
            // 
            this._IS_COMBO.Caption = "_IS_COMBO";
            this._IS_COMBO.FieldName = "_IS_COMBO";
            this._IS_COMBO.Name = "_IS_COMBO";
            // 
            // pn_TableTow
            // 
            this.pn_TableTow.Controls.Add(this.gr_divide_Table_2);
            this.pn_TableTow.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn_TableTow.Location = new System.Drawing.Point(510, 2);
            this.pn_TableTow.Name = "pn_TableTow";
            this.pn_TableTow.Size = new System.Drawing.Size(400, 667);
            this.pn_TableTow.TabIndex = 1;
            // 
            // gr_divide_Table_2
            // 
            this.gr_divide_Table_2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.RelationName = "Level1";
            this.gr_divide_Table_2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gr_divide_Table_2.Location = new System.Drawing.Point(2, 2);
            this.gr_divide_Table_2.MainView = this.gv_divide_Table_2;
            this.gr_divide_Table_2.Name = "gr_divide_Table_2";
            this.gr_divide_Table_2.Size = new System.Drawing.Size(396, 663);
            this.gr_divide_Table_2.TabIndex = 22;
            this.gr_divide_Table_2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv_divide_Table_2});
            // 
            // gv_divide_Table_2
            // 
            this.gv_divide_Table_2.ColumnPanelRowHeight = 30;
            this.gv_divide_Table_2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.@__STT,
            this.@__TEN_HANGHOA,
            this.@__SOLUONG,
            this.@__GIABAN,
            this.@__Thanh_Tien,
            this.@__MA_HANGHOA,
            this.@__GHICHU,
            this.@__MA_BEP,
            this.@__IS_COMBO});
            this.gv_divide_Table_2.GridControl = this.gr_divide_Table_2;
            this.gv_divide_Table_2.Name = "gv_divide_Table_2";
            this.gv_divide_Table_2.OptionsView.ShowFooter = true;
            this.gv_divide_Table_2.OptionsView.ShowGroupPanel = false;
            this.gv_divide_Table_2.OptionsView.ShowIndicator = false;
            this.gv_divide_Table_2.RowHeight = 35;
            this.gv_divide_Table_2.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gv_divide_Table_2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gv_divide_Table_2_CustomDrawCell);
            // 
            // __STT
            // 
            this.@__STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.@__STT.AppearanceCell.Options.UseFont = true;
            this.@__STT.AppearanceCell.Options.UseTextOptions = true;
            this.@__STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__STT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.@__STT.AppearanceHeader.Options.UseFont = true;
            this.@__STT.AppearanceHeader.Options.UseTextOptions = true;
            this.@__STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__STT.Caption = "TT";
            this.@__STT.FieldName = "__STT";
            this.@__STT.Name = "__STT";
            this.@__STT.OptionsColumn.AllowEdit = false;
            this.@__STT.OptionsColumn.AllowFocus = false;
            this.@__STT.Visible = true;
            this.@__STT.VisibleIndex = 0;
            this.@__STT.Width = 34;
            // 
            // __TEN_HANGHOA
            // 
            this.@__TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.@__TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.@__TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.@__TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.@__TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.@__TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__TEN_HANGHOA.Caption = "Hàng Hóa";
            this.@__TEN_HANGHOA.FieldName = "__TEN_HANGHOA";
            this.@__TEN_HANGHOA.Name = "__TEN_HANGHOA";
            this.@__TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this.@__TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this.@__TEN_HANGHOA.Visible = true;
            this.@__TEN_HANGHOA.VisibleIndex = 1;
            this.@__TEN_HANGHOA.Width = 116;
            // 
            // __SOLUONG
            // 
            this.@__SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.@__SOLUONG.AppearanceCell.Options.UseFont = true;
            this.@__SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.@__SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.@__SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.@__SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.@__SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__SOLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__SOLUONG.Caption = "SL";
            this.@__SOLUONG.FieldName = "__SOLUONG";
            this.@__SOLUONG.Name = "__SOLUONG";
            this.@__SOLUONG.OptionsColumn.AllowEdit = false;
            this.@__SOLUONG.OptionsColumn.AllowFocus = false;
            this.@__SOLUONG.Visible = true;
            this.@__SOLUONG.VisibleIndex = 2;
            this.@__SOLUONG.Width = 51;
            // 
            // __GIABAN
            // 
            this.@__GIABAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.@__GIABAN.AppearanceCell.Options.UseFont = true;
            this.@__GIABAN.AppearanceCell.Options.UseTextOptions = true;
            this.@__GIABAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.@__GIABAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__GIABAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.@__GIABAN.AppearanceHeader.Options.UseFont = true;
            this.@__GIABAN.AppearanceHeader.Options.UseTextOptions = true;
            this.@__GIABAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__GIABAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__GIABAN.Caption = "Đơn giá";
            this.@__GIABAN.DisplayFormat.FormatString = "{0:#,###0}";
            this.@__GIABAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.@__GIABAN.FieldName = "__GIABAN";
            this.@__GIABAN.Name = "__GIABAN";
            this.@__GIABAN.OptionsColumn.AllowEdit = false;
            this.@__GIABAN.OptionsColumn.AllowFocus = false;
            this.@__GIABAN.Visible = true;
            this.@__GIABAN.VisibleIndex = 3;
            this.@__GIABAN.Width = 96;
            // 
            // __Thanh_Tien
            // 
            this.@__Thanh_Tien.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.@__Thanh_Tien.AppearanceCell.Options.UseFont = true;
            this.@__Thanh_Tien.AppearanceCell.Options.UseTextOptions = true;
            this.@__Thanh_Tien.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.@__Thanh_Tien.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__Thanh_Tien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.@__Thanh_Tien.AppearanceHeader.Options.UseFont = true;
            this.@__Thanh_Tien.AppearanceHeader.Options.UseTextOptions = true;
            this.@__Thanh_Tien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.@__Thanh_Tien.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.@__Thanh_Tien.Caption = "T Tiền";
            this.@__Thanh_Tien.DisplayFormat.FormatString = "{0:#,###0}";
            this.@__Thanh_Tien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.@__Thanh_Tien.FieldName = "__Thanh_Tien";
            this.@__Thanh_Tien.Name = "__Thanh_Tien";
            this.@__Thanh_Tien.OptionsColumn.AllowEdit = false;
            this.@__Thanh_Tien.OptionsColumn.AllowFocus = false;
            this.@__Thanh_Tien.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "__Thanh_Tien", "{0:#,###0}")});
            this.@__Thanh_Tien.Visible = true;
            this.@__Thanh_Tien.VisibleIndex = 4;
            this.@__Thanh_Tien.Width = 104;
            // 
            // __MA_HANGHOA
            // 
            this.@__MA_HANGHOA.Caption = "Mã Hàng Hóa";
            this.@__MA_HANGHOA.FieldName = "__MA_HANGHOA";
            this.@__MA_HANGHOA.Name = "__MA_HANGHOA";
            this.@__MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this.@__MA_HANGHOA.OptionsColumn.AllowFocus = false;
            // 
            // __GHICHU
            // 
            this.@__GHICHU.Caption = "Ghi Chú";
            this.@__GHICHU.FieldName = "__GHICHU";
            this.@__GHICHU.Name = "__GHICHU";
            // 
            // __MA_BEP
            // 
            this.@__MA_BEP.Caption = "MÃ BẾP";
            this.@__MA_BEP.FieldName = "__MA_BEP";
            this.@__MA_BEP.Name = "__MA_BEP";
            // 
            // __IS_COMBO
            // 
            this.@__IS_COMBO.Caption = "__IS_COMBO";
            this.@__IS_COMBO.FieldName = "__IS_COMBO";
            this.@__IS_COMBO.Name = "__IS_COMBO";
            // 
            // Frm_TachBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(916, 675);
            this.ControlBox = false;
            this.Controls.Add(this.pnTachban);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_TachBill";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TÁCH BILL";
            this.Load += new System.EventHandler(this.Frm_TachBill_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pnTachban)).EndInit();
            this.pnTachban.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_GroupTable)).EndInit();
            this.pn_GroupTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDow)).EndInit();
            this.pnUpDow.ResumeLayout(false);
            this.pnUpDow.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            this.panelControl15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_divide_Table_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_divide_Table_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_TableTow)).EndInit();
            this.pn_TableTow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_divide_Table_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_divide_Table_2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnTachban;
        private DevExpress.XtraEditors.PanelControl pn_GroupTable;
        private DevExpress.XtraEditors.PanelControl pnUpDow;
        private DevExpress.XtraEditors.SimpleButton btn_NhapSL;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton btn_OkThrough;
        private DevExpress.XtraEditors.SimpleButton btn_ToLeftAll;
        private DevExpress.XtraEditors.SimpleButton btn_ToRightAll;
        private DevExpress.XtraEditors.SimpleButton btn_UpDivide;
        private DevExpress.XtraEditors.SimpleButton btn_DowDivide;
        private DevExpress.XtraEditors.SimpleButton btn_ESCThroughtTable;
        private DevExpress.XtraEditors.SimpleButton btn_ToRight;
        private DevExpress.XtraEditors.SimpleButton btn_Toleft;
        private DevExpress.XtraEditors.PanelControl panelControl15;
        private DevExpress.XtraEditors.PanelControl pn_TableTow;
        private DevExpress.XtraGrid.GridControl gr_divide_Table_1;
        private DevExpress.XtraGrid.Views.Grid.GridView gv_divide_Table_1;
        private DevExpress.XtraGrid.Columns.GridColumn _SSTT;
        private DevExpress.XtraGrid.Columns.GridColumn _TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn _SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn _GIABAN;
        private DevExpress.XtraGrid.Columns.GridColumn _Thanh_Tien;
        private DevExpress.XtraGrid.Columns.GridColumn _MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn _GHICHU;
        private DevExpress.XtraGrid.Columns.GridColumn _MA_BEP;
        private DevExpress.XtraGrid.Columns.GridColumn _IS_COMBO;
        private DevExpress.XtraGrid.GridControl gr_divide_Table_2;
        private DevExpress.XtraGrid.Views.Grid.GridView gv_divide_Table_2;
        private DevExpress.XtraGrid.Columns.GridColumn __STT;
        private DevExpress.XtraGrid.Columns.GridColumn __TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn __SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn __GIABAN;
        private DevExpress.XtraGrid.Columns.GridColumn __Thanh_Tien;
        private DevExpress.XtraGrid.Columns.GridColumn __MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn __GHICHU;
        private DevExpress.XtraGrid.Columns.GridColumn __MA_BEP;
        private DevExpress.XtraGrid.Columns.GridColumn __IS_COMBO;

    }
}