﻿namespace KP_RES
{
    partial class Frm_Chontheosoluong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Chontheosoluong));
            this.txtGIA = new DevExpress.XtraEditors.TextEdit();
            this.lb_nhapgia = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btn_00 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoveRightNumber = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Payments = new DevExpress.XtraEditors.SimpleButton();
            this.btn_000 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_thoat = new DevExpress.XtraEditors.SimpleButton();
            this.btn_0 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_delete = new DevExpress.XtraEditors.SimpleButton();
            this.btn_6 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_9 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_8 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_7 = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoveLeftNumber = new DevExpress.XtraEditors.SimpleButton();
            this.btn_5 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_4 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_3 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_2 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ESC = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtSoLuong = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoLuong.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtGIA
            // 
            this.txtGIA.EditValue = "0";
            this.txtGIA.Location = new System.Drawing.Point(12, 96);
            this.txtGIA.Name = "txtGIA";
            this.txtGIA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtGIA.Properties.Appearance.Options.UseFont = true;
            this.txtGIA.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGIA.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGIA.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtGIA.Size = new System.Drawing.Size(368, 30);
            this.txtGIA.TabIndex = 3;
            this.txtGIA.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtConfig_KeyDown);
            // 
            // lb_nhapgia
            // 
            this.lb_nhapgia.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_nhapgia.Location = new System.Drawing.Point(12, 77);
            this.lb_nhapgia.Name = "lb_nhapgia";
            this.lb_nhapgia.Size = new System.Drawing.Size(48, 13);
            this.lb_nhapgia.TabIndex = 2;
            this.lb_nhapgia.Text = "Nhập giá";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btn_00);
            this.panelControl1.Controls.Add(this.btn_1);
            this.panelControl1.Controls.Add(this.btnMoveRightNumber);
            this.panelControl1.Controls.Add(this.btn_Payments);
            this.panelControl1.Controls.Add(this.btn_000);
            this.panelControl1.Controls.Add(this.btn_thoat);
            this.panelControl1.Controls.Add(this.btn_0);
            this.panelControl1.Controls.Add(this.btn_delete);
            this.panelControl1.Controls.Add(this.btn_6);
            this.panelControl1.Controls.Add(this.btn_9);
            this.panelControl1.Controls.Add(this.btn_8);
            this.panelControl1.Controls.Add(this.btn_7);
            this.panelControl1.Controls.Add(this.btnMoveLeftNumber);
            this.panelControl1.Controls.Add(this.btn_5);
            this.panelControl1.Controls.Add(this.btn_4);
            this.panelControl1.Controls.Add(this.btn_3);
            this.panelControl1.Controls.Add(this.btn_2);
            this.panelControl1.Controls.Add(this.btn_ESC);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 66);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(396, 280);
            this.panelControl1.TabIndex = 1;
            // 
            // btn_00
            // 
            this.btn_00.AllowFocus = false;
            this.btn_00.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_00.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_00.Appearance.Options.UseFont = true;
            this.btn_00.Location = new System.Drawing.Point(166, 219);
            this.btn_00.Name = "btn_00";
            this.btn_00.Size = new System.Drawing.Size(58, 55);
            this.btn_00.TabIndex = 68;
            this.btn_00.Text = ".";
            this.btn_00.Click += new System.EventHandler(this.btn_00_Click);
            // 
            // btn_1
            // 
            this.btn_1.AllowFocus = false;
            this.btn_1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_1.Appearance.Options.UseFont = true;
            this.btn_1.Location = new System.Drawing.Point(12, 149);
            this.btn_1.Name = "btn_1";
            this.btn_1.Size = new System.Drawing.Size(58, 55);
            this.btn_1.TabIndex = 67;
            this.btn_1.Text = "1";
            this.btn_1.Click += new System.EventHandler(this.btn_1_Click);
            // 
            // btnMoveRightNumber
            // 
            this.btnMoveRightNumber.AllowFocus = false;
            this.btnMoveRightNumber.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveRightNumber.Appearance.Options.UseFont = true;
            this.btnMoveRightNumber.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveRightNumber.Image")));
            this.btnMoveRightNumber.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveRightNumber.Location = new System.Drawing.Point(322, 79);
            this.btnMoveRightNumber.Name = "btnMoveRightNumber";
            this.btnMoveRightNumber.Size = new System.Drawing.Size(58, 55);
            this.btnMoveRightNumber.TabIndex = 66;
            this.btnMoveRightNumber.Click += new System.EventHandler(this.btnMoveRightNumber_Click);
            // 
            // btn_Payments
            // 
            this.btn_Payments.AllowFocus = false;
            this.btn_Payments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Payments.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Payments.Appearance.Options.UseFont = true;
            this.btn_Payments.Location = new System.Drawing.Point(243, 219);
            this.btn_Payments.Name = "btn_Payments";
            this.btn_Payments.Size = new System.Drawing.Size(137, 55);
            this.btn_Payments.TabIndex = 65;
            this.btn_Payments.Text = "ENTER";
            this.btn_Payments.Click += new System.EventHandler(this.btn_Payments_Click);
            // 
            // btn_000
            // 
            this.btn_000.AllowFocus = false;
            this.btn_000.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_000.Appearance.Options.UseFont = true;
            this.btn_000.Location = new System.Drawing.Point(89, 219);
            this.btn_000.Name = "btn_000";
            this.btn_000.Size = new System.Drawing.Size(58, 55);
            this.btn_000.TabIndex = 63;
            this.btn_000.Text = "000";
            this.btn_000.Click += new System.EventHandler(this.btn_000_Click);
            // 
            // btn_thoat
            // 
            this.btn_thoat.AllowFocus = false;
            this.btn_thoat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_thoat.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_thoat.Appearance.Options.UseFont = true;
            this.btn_thoat.Location = new System.Drawing.Point(243, 149);
            this.btn_thoat.Name = "btn_thoat";
            this.btn_thoat.Size = new System.Drawing.Size(137, 55);
            this.btn_thoat.TabIndex = 62;
            this.btn_thoat.Text = "Thoát";
            this.btn_thoat.Click += new System.EventHandler(this.btn_Thoat_Click);
            // 
            // btn_0
            // 
            this.btn_0.AllowFocus = false;
            this.btn_0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_0.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_0.Appearance.Options.UseFont = true;
            this.btn_0.Location = new System.Drawing.Point(12, 219);
            this.btn_0.Name = "btn_0";
            this.btn_0.Size = new System.Drawing.Size(58, 55);
            this.btn_0.TabIndex = 60;
            this.btn_0.Text = "0";
            this.btn_0.Click += new System.EventHandler(this.btn_0_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.AllowFocus = false;
            this.btn_delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_delete.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_delete.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btn_delete.Appearance.Options.UseFont = true;
            this.btn_delete.Appearance.Options.UseForeColor = true;
            this.btn_delete.Location = new System.Drawing.Point(322, 9);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(58, 55);
            this.btn_delete.TabIndex = 61;
            this.btn_delete.Text = "Del";
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_6
            // 
            this.btn_6.AllowFocus = false;
            this.btn_6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_6.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_6.Appearance.Options.UseFont = true;
            this.btn_6.Location = new System.Drawing.Point(166, 79);
            this.btn_6.Name = "btn_6";
            this.btn_6.Size = new System.Drawing.Size(58, 55);
            this.btn_6.TabIndex = 56;
            this.btn_6.Text = "6";
            this.btn_6.Click += new System.EventHandler(this.btn_6_Click);
            // 
            // btn_9
            // 
            this.btn_9.AllowFocus = false;
            this.btn_9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_9.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_9.Appearance.Options.UseFont = true;
            this.btn_9.Location = new System.Drawing.Point(166, 9);
            this.btn_9.Name = "btn_9";
            this.btn_9.Size = new System.Drawing.Size(58, 55);
            this.btn_9.TabIndex = 59;
            this.btn_9.Text = "9";
            this.btn_9.Click += new System.EventHandler(this.btn_9_Click);
            // 
            // btn_8
            // 
            this.btn_8.AllowFocus = false;
            this.btn_8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_8.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_8.Appearance.Options.UseFont = true;
            this.btn_8.Location = new System.Drawing.Point(89, 9);
            this.btn_8.Name = "btn_8";
            this.btn_8.Size = new System.Drawing.Size(58, 55);
            this.btn_8.TabIndex = 58;
            this.btn_8.Text = "8";
            this.btn_8.Click += new System.EventHandler(this.btn_8_Click);
            // 
            // btn_7
            // 
            this.btn_7.AllowFocus = false;
            this.btn_7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_7.Appearance.Options.UseFont = true;
            this.btn_7.Location = new System.Drawing.Point(12, 9);
            this.btn_7.Name = "btn_7";
            this.btn_7.Size = new System.Drawing.Size(58, 55);
            this.btn_7.TabIndex = 57;
            this.btn_7.Text = "7";
            this.btn_7.Click += new System.EventHandler(this.btn_7_Click);
            // 
            // btnMoveLeftNumber
            // 
            this.btnMoveLeftNumber.AllowFocus = false;
            this.btnMoveLeftNumber.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveLeftNumber.Appearance.Options.UseFont = true;
            this.btnMoveLeftNumber.Image = global::KP_RES.Properties.Resources.advance_back_32;
            this.btnMoveLeftNumber.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveLeftNumber.Location = new System.Drawing.Point(243, 79);
            this.btnMoveLeftNumber.Name = "btnMoveLeftNumber";
            this.btnMoveLeftNumber.Size = new System.Drawing.Size(58, 55);
            this.btnMoveLeftNumber.TabIndex = 55;
            this.btnMoveLeftNumber.Click += new System.EventHandler(this.btn_back_Click);
            // 
            // btn_5
            // 
            this.btn_5.AllowFocus = false;
            this.btn_5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_5.Appearance.Options.UseFont = true;
            this.btn_5.Location = new System.Drawing.Point(89, 79);
            this.btn_5.Name = "btn_5";
            this.btn_5.Size = new System.Drawing.Size(58, 55);
            this.btn_5.TabIndex = 54;
            this.btn_5.Text = "5";
            this.btn_5.Click += new System.EventHandler(this.btn_5_Click);
            // 
            // btn_4
            // 
            this.btn_4.AllowFocus = false;
            this.btn_4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_4.Appearance.Options.UseFont = true;
            this.btn_4.Location = new System.Drawing.Point(12, 79);
            this.btn_4.Name = "btn_4";
            this.btn_4.Size = new System.Drawing.Size(58, 55);
            this.btn_4.TabIndex = 53;
            this.btn_4.Text = "4";
            this.btn_4.Click += new System.EventHandler(this.btn_4_Click);
            // 
            // btn_3
            // 
            this.btn_3.AllowFocus = false;
            this.btn_3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_3.Appearance.Options.UseFont = true;
            this.btn_3.Location = new System.Drawing.Point(166, 149);
            this.btn_3.Name = "btn_3";
            this.btn_3.Size = new System.Drawing.Size(58, 55);
            this.btn_3.TabIndex = 52;
            this.btn_3.Text = "3";
            this.btn_3.Click += new System.EventHandler(this.btn_3_Click);
            // 
            // btn_2
            // 
            this.btn_2.AllowFocus = false;
            this.btn_2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_2.Appearance.Options.UseFont = true;
            this.btn_2.Location = new System.Drawing.Point(89, 149);
            this.btn_2.Name = "btn_2";
            this.btn_2.Size = new System.Drawing.Size(58, 55);
            this.btn_2.TabIndex = 51;
            this.btn_2.Text = "2";
            this.btn_2.Click += new System.EventHandler(this.btn_2_Click);
            // 
            // btn_ESC
            // 
            this.btn_ESC.AllowFocus = false;
            this.btn_ESC.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ESC.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.btn_ESC.Appearance.Options.UseFont = true;
            this.btn_ESC.Appearance.Options.UseForeColor = true;
            this.btn_ESC.Location = new System.Drawing.Point(243, 9);
            this.btn_ESC.Name = "btn_ESC";
            this.btn_ESC.Size = new System.Drawing.Size(58, 55);
            this.btn_ESC.TabIndex = 49;
            this.btn_ESC.Text = "ESC";
            this.btn_ESC.Click += new System.EventHandler(this.btn_ESC_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Controls.Add(this.txtSoLuong);
            this.panelControl2.Controls.Add(this.lb_nhapgia);
            this.panelControl2.Controls.Add(this.txtGIA);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(396, 66);
            this.panelControl2.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Location = new System.Drawing.Point(12, 11);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(44, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Nhập số";
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.EditValue = "1";
            this.txtSoLuong.Location = new System.Drawing.Point(12, 30);
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtSoLuong.Properties.Appearance.Options.UseFont = true;
            this.txtSoLuong.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSoLuong.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSoLuong.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtSoLuong.Size = new System.Drawing.Size(368, 30);
            this.txtSoLuong.TabIndex = 1;
            this.txtSoLuong.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSoLuong_KeyDown);
            // 
            // Frm_Chontheosoluong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 346);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_Chontheosoluong";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nhập giá";
            this.Load += new System.EventHandler(this.Frm_ConfigFuntion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtGIA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoLuong.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtGIA;
        private DevExpress.XtraEditors.LabelControl lb_nhapgia;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btn_Payments;
        private DevExpress.XtraEditors.SimpleButton btn_000;
        private DevExpress.XtraEditors.SimpleButton btn_thoat;
        private DevExpress.XtraEditors.SimpleButton btn_0;
        private DevExpress.XtraEditors.SimpleButton btn_delete;
        private DevExpress.XtraEditors.SimpleButton btn_6;
        private DevExpress.XtraEditors.SimpleButton btn_9;
        private DevExpress.XtraEditors.SimpleButton btn_8;
        private DevExpress.XtraEditors.SimpleButton btn_7;
        private DevExpress.XtraEditors.SimpleButton btnMoveLeftNumber;
        private DevExpress.XtraEditors.SimpleButton btn_5;
        private DevExpress.XtraEditors.SimpleButton btn_4;
        private DevExpress.XtraEditors.SimpleButton btn_3;
        private DevExpress.XtraEditors.SimpleButton btn_2;
        private DevExpress.XtraEditors.SimpleButton btn_ESC;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnMoveRightNumber;
        private DevExpress.XtraEditors.SimpleButton btn_1;
        private DevExpress.XtraEditors.SimpleButton btn_00;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtSoLuong;
    }
}