﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES
{
    public partial class Frm_Danhsachdatban : DevExpress.XtraEditors.XtraForm
    {
        public string idTable = null;
        public static bool flag_Chuyenban = false;
        public static string mahoadon = "";
        
        public void Laydanhsachdatban()
        {
            string sSQL = "";
            DataTable dt = clsMain.ReturnDataTable("Exec SP_Select_Danhsachdatban");
            int n = dt.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                if (bool.Parse(dt.Rows[i]["ISMO_BAN"].ToString()))
                    dt.Rows[i]["TRANGTHAI"] = _moban;
                else
                    dt.Rows[i]["TRANGTHAI"] = _dongban;
            }
            gridControl2.DataSource = dt;
            if (dt.Rows.Count > 0)
            {
                sSQL += "Select b.MA_BAN,TEN_BAN" + "\n";
                sSQL += "From BAN b,CT_MOBAN c" + "\n";
                sSQL += "Where b.MA_BAN=c.MA_BAN And c.MA_HOADON=" + clsMain.SQLString(gridView2.GetFocusedRowCellValue("MA_HOADON").ToString());
                dt = clsMain.ReturnDataTable(sSQL);
                gridControl1.DataSource = dt;
            }
        }
       
       

       
        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            string sSQL = "";
            sSQL += "Select b.MA_BAN,TEN_BAN" + "\n";
            sSQL += "From BAN b,CT_MOBAN c" + "\n";
            sSQL += "Where b.MA_BAN=c.MA_BAN And c.MA_HOADON=" + clsMain.SQLString(gridView2.GetFocusedRowCellValue("MA_HOADON").ToString());
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            gridControl1.DataSource = dt;
        }

        private void btn_TrangThai_Click(object sender, EventArgs e)
        {
            if (gridView1.RowCount <= 0)
                return;
            bool flag = false;
            string sSQL = "";
            DataTable dt;
            //sSQL += "Select ISMO_BAN From BAN Where MA_BAN=" + clsMain.SQLString(gridView1.GetRowCellValue(0, "MA_BAN").ToString());
            //DataTable dt = clsMain.ReturnDataTable(sSQL);
            int n = gridView1.RowCount;
            //sSQL = "";
            //if (bool.Parse(dt.Rows[0]["ISMO_BAN"].ToString()))
            //{
            if(gridView2.GetRowCellValue(gridView2.FocusedRowHandle,"TRANGTHAI").ToString()=="Mở bàn")
            {
                for (int i = 0; i < n; i++)
                {
                    sSQL += "Update BAN Set ISMO_BAN=0 Where MA_BAN=" + clsMain.SQLString(gridView1.GetRowCellValue(i, "MA_BAN").ToString()) + "\n";
                }
            }
            else
            {
                for (int i = 0; i < n; i++)
                {
                    sSQL = "";
                    sSQL += "Select ISMO_BAN From BAN Where MA_BAN=" + clsMain.SQLString(gridView1.GetRowCellValue(i, "MA_BAN").ToString());
                    dt=clsMain.ReturnDataTable(sSQL);
                    if (bool.Parse(dt.Rows[0]["ISMO_BAN"].ToString()))
                    {
                        flag = true;
                        break;
                    }
                }
                if (flag)
                {
                    if (DialogResult.Yes == XtraMessageBox.Show(_cbdsbk, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                    {
                        flag_Chuyenban = true;
                        mahoadon = gridView2.GetFocusedRowCellValue("MA_HOADON").ToString();
                        this.Close();
                    }
                    else
                        return;
                }
                else
                {
                    for (int i = 0; i < n; i++)
                    {
                        sSQL += "Update BAN Set ISMO_BAN=1 Where MA_BAN=" + clsMain.SQLString(gridView1.GetRowCellValue(i, "MA_BAN").ToString()) + "\n";
                    }
                }
            }
            clsMain.ExecuteSQL(sSQL);
            Laydanhsachdatban();
        }

        string _moban = "";
        string _dongban = "";
        string _thongbao = "";
        string _cbdsbk = "";
        public Frm_Danhsachdatban()
        {
            InitializeComponent();
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            btn_TrangThai.Text = rm.GetString("modongban", culture);
            btnDong.Text = rm.GetString("dong", culture);
            groupControl1.Text = rm.GetString("danhsachdb", culture);
            gridView2.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView2.Columns["MA_HOADON"].Caption = rm.GetString("hoadon", culture);
            gridView2.Columns["TIENDATCOC"].Caption = rm.GetString("tiendatcoc", culture);
            gridView2.Columns["TEN"].Caption = rm.GetString("nguoidat", culture);
            gridView2.Columns["GIODAT"].Caption = rm.GetString("giodat", culture);
            gridView2.Columns["SODIENTHOAI"].Caption = rm.GetString("dienthoai", culture);
            gridView2.Columns["TRANGTHAI"].Caption = rm.GetString("trangthai", culture);
            gridView1.Columns["TEN_BAN"].Caption = rm.GetString("dsban", culture);
            _moban = rm.GetString("moban1", culture);
            _dongban = rm.GetString("dongban1", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _cbdsbk = rm.GetString("cbdsbk", culture);

            Laydanhsachdatban();
        }
    }
}