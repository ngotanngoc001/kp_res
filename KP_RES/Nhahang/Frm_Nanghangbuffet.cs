﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_Nanghangbuffet : DevExpress.XtraEditors.XtraForm
    {
        public string sMaHD = "";
        public Frm_Nanghangbuffet()
        {
            InitializeComponent();
        }

        private void Frm_Nanghangbuffet_Load(object sender, EventArgs e)
        {
            LoadOption();
        }

        private void LoadOption()
        {

            string sql = "EXEC SP_GET_BUFFET_UP  "+ clsMain .SQLString (sMaHD );
            DataTable requestAdd = clsMain.ReturnDataTable(sql);
            pnYeuCau.Controls.Clear();
            for (int i = 0; i < requestAdd.Rows.Count; i++)
            {
                SimpleButton btn = new SimpleButton();
                btn.Name = requestAdd.Rows[i]["MA_BUFFET"].ToString();
                btn.Text = requestAdd.Rows[i]["TEN_BUFFET"].ToString();
                btn.Click += new EventHandler(btn_Click);
                btn.Size = new Size(403, 80);
                btn.Image = global::KP_RES.Properties.Resources.vegetarian_food_26;
                btn.Font = btnDong.Font;
                pnYeuCau.Controls.Add(btn);
            }
        }

        private void btn_Click(object sender, EventArgs e)
        {
            foreach (SimpleButton btn in pnYeuCau.Controls)
            {
                if (btn.Name == this.ActiveControl.Name)
                {
                    SetBackgroudOnClick(btn);
                    string sql = "EXEC SP_SET_BUFFET_UP  " + clsMain.SQLString(sMaHD) + " , " + clsMain.SQLString(btn.Name);
                    bool bSQL = clsMain.ExecuteSQL(sql);
                    if (bSQL)
                    {
                        XtraMessageBox.Show("Đã nâng hạng buffet thành công", "Thông báo", MessageBoxButtons.OK,MessageBoxIcon.Information);
                    }
                    else
                    {
                        XtraMessageBox.Show("Nâng hạng buffet không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                this.Close();
            }
        }

        private void SetBackgroudOnClick(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.YellowGreen;
            _btn.Appearance.BackColor2 = Color.YellowGreen;
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnYCT_Len_Click(object sender, EventArgs e)
        {
            try
            {
                pnYeuCau.AutoScrollPosition = new Point(0, pnYeuCau.VerticalScroll.Value - pnYeuCau.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnYCT_Xuong_Click(object sender, EventArgs e)
        {
            try
            {
                pnYeuCau.AutoScrollPosition = new Point(0, pnYeuCau.VerticalScroll.Value + pnYeuCau.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }
    }
}