﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.IO;
using DevExpress.XtraGrid.Views.Grid;
using System.Globalization;
using System.Resources;
namespace KP_RES
{
    public partial class Frm_Manhinhhai_Nhahang : DevExpress.XtraEditors.XtraForm
    {
        
        bool[] listDaDownVideo;

        public Frm_Manhinhhai_Nhahang()
        {
            InitializeComponent();
            LoadData();
            timer2.Enabled = true;
            pn_Right.Visible = false;
            pn_ChenRightRight.Visible = false;
        }

        private void LoadData()
        {
            try
            {
                string sql = "select MAQC,VITRI from QC_QUANGCAO where mode=0 ";
                DataTable dtQC = clsMain.ReturnDataTable(sql);
                if (dtQC.Rows.Count < 0)
                {
                    throw new Exception("Bạn chưa cài đặt quảng cáo,hãy vào cài đặt trước khi dùng!");
                }
                if (dtQC.Rows[0]["VITRI"].ToString() == "0")
                {
                    pn_chenRight.Dock = DockStyle.Right;
                    pn_Right.Dock = DockStyle.Right;
                }
                else
                {
                    pn_chenRight.Dock = DockStyle.Left;
                    pn_Right.Dock = DockStyle.Left;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            try
            {
                timer2.Enabled = false;
                LoadImage();
                LoadDuLieu();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void LoadImage()
        {
            string maQuay = cls_ConfigCashier.idCashier;
            string sqldownvideo = "select h.MAHINH,h.TENHINH from  QUAY q inner join QC_HINHANH_QUAY qh on q.MA_QUAY=qh.MA_QUAY inner join QC_HINHANH h on qh.MAHINH=h.MAHINH inner join QC_QUANGCAO qc on qc.MAQC=h.MAQC where qc.MODE=0 and q.MA_QUAY='" + maQuay + "' ";
            DataTable dtadownvideo = clsMain.ReturnDataTable(sqldownvideo);
            listDaDownVideo = new bool[dtadownvideo.Rows.Count];
            for (int k = 0; k < dtadownvideo.Rows.Count; k++)
            {
                listDaDownVideo[k] = false;//gắn mặc định là chưa down về
            }
            DirectoryInfo directoryVideo = new DirectoryInfo(Application.StartupPath + "\\VideoHaiManHinh");
            FileInfo[] filesVideo = directoryVideo.GetFiles("*.*");//dữ liệu đang nằm trong folder 
            foreach (FileInfo f in filesVideo)
            {
                if (!checkname(dtadownvideo, f.Name))
                {
                    f.Delete();
                }
            }
            for (int i = 0; i < dtadownvideo.Rows.Count; i++)
            {
                if (!listDaDownVideo[i])
                {
                    string sqldadown1 = "select URL from  QC_HINHANH where MAHINH='" + dtadownvideo.Rows[i]["MAHINH"].ToString() + "'";
                    DataTable dtadadown1 = clsMain.ReturnDataTable(sqldadown1);
                    byte[] fileBytes = new byte[((byte[])dtadadown1.Rows[0]["URL"]).LongLength];
                    fileBytes = (byte[])dtadadown1.Rows[0]["URL"];
                    Stream file = File.OpenWrite(Application.StartupPath + "/VideoHaiManHinh/" + dtadownvideo.Rows[i]["TENHINH"].ToString() + "");
                    file.Write(fileBytes, 0, fileBytes.Length);
                    file.Close();
                }
            }
        }

        private bool checkname(DataTable dt, string name)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["TENHINH"].ToString() == name)//folder down về đã có file
                {
                    listDaDownVideo[i] = true;
                    return true;
                }
            }
            return false;
        }

        private void LoadDuLieu()
        {
            try
            {
                if (WindowsMediaPlayer.playlistCollection.getAll().count > 1)
                {
                    WindowsMediaPlayer.playlistCollection.remove(WindowsMediaPlayer.playlistCollection.getAll().Item(0));
                }

                WMPLib.IWMPPlaylist playlist = WindowsMediaPlayer.playlistCollection.newPlaylist("myplaylist");
                WMPLib.IWMPMedia media;
                DirectoryInfo directory = new DirectoryInfo(Application.StartupPath + "\\VideoHaiManHinh");
                FileInfo[] files = directory.GetFiles("*.*");
                if (files.Length > 0) 
                {
                    pnVideo.BringToFront();
                    foreach (FileInfo f in files)
                    {
                        media = WindowsMediaPlayer.newMedia(f.FullName);
                        playlist.appendItem(media);
                    }
                    WindowsMediaPlayer.settings.setMode("loop", true);
                    WindowsMediaPlayer.currentPlaylist = playlist;
                    WindowsMediaPlayer.Ctlcontrols.play();
                    WindowsMediaPlayer.uiMode = "none";
                    WindowsMediaPlayer.stretchToFit = true;
                }
            }
            catch
            {
            }
        }

        public void GetValue(DataTable _dt, int control, int iFocusRow)
        {
            try
            {
                if (_dt.Rows.Count > 0)
                {
                    pn_Right.Visible = true;
                    pn_ChenRightRight.Visible = true;
                    gr_SelectProduct.DataSource = _dt;
                    gv_SelectProduct.FocusedRowHandle = iFocusRow;
                    lbName.Text = _dt.Rows[iFocusRow]["TEN_HANGHOA"].ToString();
                    lbNumber.Text = "1";
                    lbprice.Text = string.Format("{0:#,###0}", decimal.Parse(_dt.Rows[iFocusRow]["GIABAN"].ToString()));

                    double tatol = 0;
                    for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                    {
                        tatol += double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"]).ToString());

                    }
                    int chietkhau = cls_KP_RES.iChietkhau_Manhinhhai;
                    double thanhtien = tatol - tatol * chietkhau / 100;

                    lbTongCong.Text = string.Format("{0:#,###0 }", tatol);
                    lbGiamGia.Text = string.Format("{0:#,###0 }", chietkhau);
                    lbThanhTien.Text = string.Format("{0:#,###0}", thanhtien);
                }
                if (_dt.Rows.Count <= 0)
                {
                    pn_Right.Visible = false;
                    pn_ChenRightRight.Visible = false;
                }
                if (control != 0)
                {
                    cls_KP_RES.iChietkhau_Manhinhhai = 0;
                    this.Close();
                    this.Dispose();
                }
            }
            catch
            {
            }
        }

        private void gv_SelectProduct_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == SOTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
            else
            {
                pn_Right.Visible = false;
            }
        }

        private void gv_SelectProduct_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            GridView View = sender as GridView;
            string category = View.GetRowCellDisplayText(e.RowHandle, View.Columns["GHICHU"]);
            if (category == "cb")
                e.Appearance.Font = new Font(new FontFamily("Arial"), 8);          
        }

        private void Frm_Manhinhhai_Nhahang_Load(object sender, EventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);

            labelControl3.Text = rm.GetString("thanhtien", culture);
            labelControl1.Text = rm.GetString("giamgia", culture);
            labelControl2.Text = rm.GetString("tongcong", culture);
            TEN_HANGHOA.Caption = rm.GetString("tenhanghoa", culture);
            SOLUONG.Caption = rm.GetString("soluong", culture);
            GIABAN.Caption = rm.GetString("dongia", culture);
            Thanh_Tien.Caption = rm.GetString("thanhtien", culture);
        }

      
    }
}