﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Threading;
using KP_UserManagement;
using System.IO;
using KP_Terminal;
using DevExpress.XtraGrid.Views.Grid;
using System.Net.Mail;
using KP_Report;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;
using RestSharp;
using Newtonsoft.Json;

namespace KP_RES
{
    public partial class Frm_Banhang_TraSua : DevExpress.XtraEditors.XtraForm
    {
        string maMon;
        DataTable dt = new DataTable("HANGHOA");
        DataTable dtNH = new DataTable();
        DataTable dtMon = new DataTable();
        int cPageDish = 1;
        int cPageMon = 1;
        int maKV = 1;
        string maNH = "1";
        bool Flag_soluong = false; //true:so luong tren lưới dc bắt đầu lại,False:sô lượng đang tạo +thêm chuỗi.
        public delegate void GetString(DataTable dtshow, int ctrol, int iFocusRow);
        public GetString MyGetData;
        public int contrDelegate = 0;
        public int iFocusRow = 0;
        int sTEM = 1;
        int sTEM_THEMMON = 1;
        int sTT = 1;
        int sMaCB = 1;
        int sMaCB_THEMMON = 1;
        bool sButtonMonThem = false;
        bool isYeuCauThem = false;
        string Kytu_Combo = "   *  ";
        string Kytu_Monthem = "       +  ";
        public Frm_Banhang_TraSua()
        {
            InitializeComponent();
            cls_KP_RES.LoadBangCauHinh();
            LoadPermission();
            lb_Kho.Text = cls_ConfigCashier.nameWarehouse;
            lb_Quay.Text = cls_ConfigCashier.nameCashier;
            lbCaBan.Text = DateTime.Now.ToString("dd-MM-yyyy");
            lb_NhanVien.Text = clsGlobal.gsNameOfUserLogin;
            lbtimeby.Text = cls_ConfigCashier.sTenCB + ": " + cls_ConfigCashier.sGioBD + " --> " + cls_ConfigCashier.sGioKT;
            lbNameMon.Text = "";
            AddColumnDatatable();
        }

        private void Frm_FastFood_Load(object sender, EventArgs e)
        {
            XuLyGv();
            SetBackgroudOnClick(btn_ThuNgan);
            pnThuNgan.BringToFront();
            pnShowHang.BringToFront();
            pnBtnDeleteGv.Visible = false;
            btnComboOption.Visible = false;
            LoadCombo();
            GetNhomhang();
            GetListMon();
        }

        private void Frm_FastFood_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                btn_Payment_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Escape)
            {
                btn_logout_Click(null, null);
            }
        }

        string _thongbao = "";
        string _chuakhaibaodinhluong = "";
        string _daylahhtheothoigia = "";
        string _khongcohhthaythe = "";
        string _hhkhongthuoccombo = "";
        private void Frm_FastFood_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            btn_ThuNgan.Text = rm.GetString("thungan", culture);
            btn_DatThu.Text = rm.GetString("datthu", culture);
            btn_MonThem.Text = rm.GetString("monthem", culture);
            lbCaBan.Text = DateTime.Now.ToString("dd-MM-yyyy");
            btn_Payment.Text = rm.GetString("F1thanhtoan", culture);
            //btnGiaBan2.Text = rm.GetString("giaban2", culture);
            btn_InLaiHoaDon.Text = rm.GetString("inlaihoadon", culture);
            btnKetKa.Text = rm.GetString("ketca", culture);
            btn_HuyHoaDon.Text = rm.GetString("huyhoadon", culture);
            btnMoKet.Text = rm.GetString("moket", culture);
            btn_logout.Text = rm.GetString("dong", culture);
            lb_phuthu.Text = rm.GetString("phuthu", culture);
            lb_giamgia.Text = rm.GetString("giamgia", culture);
            lb_tongcong.Text = rm.GetString("tongcong", culture);
            btnComboOption.Text = rm.GetString("tuychon", culture);
            btnChietkhau.Text = rm.GetString("ck", culture);
            btnSuaGia.Text = rm.GetString("suagia", culture);
            btn_xoa.Text = rm.GetString("xoa", culture);
            btn_XoaTatCa.Text = rm.GetString("xoatatca", culture);
            gv_SelectProduct.Columns["SOTT"].Caption = rm.GetString("stt", culture);
            gv_SelectProduct.Columns["TEN_HANGHOA"].Caption = rm.GetString("tenhanghoa", culture);
            gv_SelectProduct.Columns["SOLUONG"].Caption = rm.GetString("soluong", culture);
            gv_SelectProduct.Columns["GIABAN"].Caption = rm.GetString("dongia", culture);
            gv_SelectProduct.Columns["CHIETKHAU"].Caption = rm.GetString("ck2", culture);
            gv_SelectProduct.Columns["Thanh_Tien"].Caption = rm.GetString("thanhtien2", culture);
            lb_soluong.Text = rm.GetString("soluong1", culture);
            gvBillPaymented.Columns["SSTT"].Caption = rm.GetString("stt", culture);
            gvBillPaymented.Columns["MA_HOADON"].Caption = rm.GetString("mahoadon", culture);
            gvBillPaymented.Columns["TEN_KHUVUC"].Caption = rm.GetString("khuvuc", culture);
            gvBillPaymented.Columns["TEN_BAN"].Caption = rm.GetString("ban", culture);
            gvBillPaymented.Columns["NGAYTAO"].Caption = rm.GetString("ngaytao", culture);
            //gvBillPaymented.Columns["HUY"].Caption = rm.GetString("huy", culture);
            gvBillPaymented.Columns["TONGTIEN"].Caption = rm.GetString("tongtien", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _chuakhaibaodinhluong = rm.GetString("chuakhaibaodinhluong", culture);
            _khongcohhthaythe = rm.GetString("khongcohhthaythe", culture);
            _hhkhongthuoccombo = rm.GetString("hhkhongthuoccombo", culture);
            btnInVeOnline.Text = rm.GetString("InVeOnline", culture);
            btnThemHangHoa.Text = rm.GetString("themhanghoa", culture);
            btn_YeuCauThem.Text = rm.GetString("yeucauthem", culture);
            btnGoikhach.Text = rm.GetString("goikhach", culture);
            btnInbilldoi.Text = rm.GetString("inbilldoi", culture);

        }

        private void LoadPermission()
        {
            btn_Payment.Enabled = clsUserManagement.AllowAdd("25121701");
            btnInVeOnline.Enabled = clsUserManagement.AllowAdd("25121701");
            btn_InLaiHoaDon.Enabled = clsUserManagement.AllowEdit("25121701");
            btn_HuyHoaDon.Enabled = clsUserManagement.AllowDelete("25121701");
            btnKetKa.Enabled = clsUserManagement.AllowView("19031801");
        }

        private void LoadCombo()
        {
            string sSQL = "";
            DataTable dt = new DataTable();
            sSQL = string.Format("Exec SelectHangHoa_Fastfood");
            dt = clsMain.ReturnDataTable(sSQL);

            txtID.Properties.DataSource = dt;
            txtID.EditValue = txtID.Properties.GetDataSourceValue(txtID.Properties.ValueMember, 0);
        }

        private void AddColumnDatatable()
        {
            dt.Columns.Add("MA_HANGHOA", Type.GetType("System.String"));
            dt.Columns.Add("TEN_HANGHOA", Type.GetType("System.String"));
            dt.Columns.Add("SOLUONG", Type.GetType("System.Double"));
            dt.Columns.Add("GIABAN", Type.GetType("System.Double"));
            dt.Columns.Add("GHICHU", Type.GetType("System.String"));
            dt.Columns.Add("MA_BEP", typeof(Int32));
            dt.Columns.Add("THUE", Type.GetType("System.Double"));
            dt.Columns.Add("Thanh_Tien", Type.GetType("System.Double"));
            dt.Columns.Add("TRANGTHAI", typeof(bool));
            dt.Columns.Add("IS_COMBO", typeof(bool));
            dt.Columns.Add("IS_INBEP", typeof(bool));
            dt.Columns.Add("IS_KHUYENMAI", typeof(bool));
            dt.Columns.Add("CHIETKHAU", Type.GetType("System.Double"));
            dt.Columns.Add("MONTHEM", typeof(bool));
            dt.Columns.Add("TEM", typeof(Int32));
            dt.Columns.Add("MACP", Type.GetType("System.String"));
            dt.Columns.Add("STT", Type.GetType("System.String"));
            dt.Columns.Add("MACB", typeof(Int32));
            dt.Columns.Add("INTEM", typeof(bool));
        }

        private void XuLyGv()
        {
            if (pnBtnDeleteGv.Visible)
            {
                if (gv_SelectProduct.RowCount > 5)
                {
                    pnUpDowGv.Visible = true;
                }
                else
                {
                    pnUpDowGv.Visible = false;
                }
            }
            else
            {
                if (gv_SelectProduct.RowCount > 8)
                {
                    pnUpDowGv.Visible = true;
                }
                else
                {
                    pnUpDowGv.Visible = false;
                }
            }
        }

        private void btn_ThuNgan_Click(object sender, EventArgs e)
        {
            if (exitReprint == true)
                return;
            SetBackgroudOnClick(btn_ThuNgan);
            SetDefautlBackgroud(btn_DatThu);
            pnThuNgan.BringToFront();
        }

        private void btn_DatThu_Click(object sender, EventArgs e)
        {
            if (exitReprint == true || sButtonMonThem == true)
                return;
            SetBackgroudOnClick(btn_DatThu);
            SetDefautlBackgroud(btn_ThuNgan);
            pnDacThu.BringToFront();
        }

        private void SetBackgroudOnClick(SimpleButton _btn)
        {
            if (cls_KP_RES.iHinhanh == 1)
            {
                if (_btn.BackgroundImageLayout == ImageLayout.Stretch)
                {
                    return;
                }
            }
            _btn.Appearance.BackColor = Color.YellowGreen;
            _btn.Appearance.BackColor2 = Color.YellowGreen;
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private void SetDefautlBackgroud(SimpleButton _btn)
        {
            if (cls_KP_RES.iHinhanh == 1)
            {
                if (_btn.BackgroundImageLayout == ImageLayout.Stretch)
                {
                    return;
                }
            }
            _btn.Appearance.BackColor = Color.White;
            if (_btn.Appearance.Options.UseBackColor == true)
                _btn.Appearance.Options.UseBackColor = false;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == false)
                _btn.LookAndFeel.UseDefaultLookAndFeel = true;
        }

        private void GetNhomhang()
        {
            try
            {
                dtNH.Rows.Clear();
                string SQLMON = string.Format("exec SelectDish @PageNumber={0},@PageSize={1},@maQUAY={2}", cPageDish, 7, int.Parse(cls_ConfigCashier.idCashier));
                dtNH = clsMain.ReturnDataTable(SQLMON);

                while (pnKV_Mon.Controls.Count > 0)
                {
                    pnKV_Mon.Controls[0].Dispose();
                }
                pnKV_Mon.Controls.Clear();
                int locateY = 2;
                int locateX = 3;
                for (int i = 0; i < dtNH.Rows.Count; i++)
                {
                    SimpleButton btnKV = new SimpleButton();
                    btnKV.Name = dtNH.Rows[i]["MA_NHOMHANG"].ToString();
                    if (i == 0)
                    {
                        maNH = dtNH.Rows[0]["MA_NHOMHANG"].ToString();
                        SetBackgroudOnClick(btnKV);
                    }
                    btnKV.Text = "" + dtNH.Rows[i]["TEN_NHOMHANG"].ToString();
                    try
                    {
                        byte[] tam = new byte[((byte[])dtNH.Rows[i]["HINHANH"]).LongLength];
                        tam = (byte[])dtNH.Rows[i]["HINHANH"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        //btnKV.Image = bm;
                        if (cls_KP_RES.iHinhanh == 1)
                        {
                            btnKV.BackgroundImage = bm;
                            btnKV.BackgroundImageLayout = ImageLayout.Stretch;
                            btnKV.Appearance.BackColor = Color.Transparent;
                            btnKV.Appearance.Options.UseBackColor = true;
                            btnKV.ButtonStyle = BorderStyles.NoBorder;
                            btnKV.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                        }
                        else
                        {
                            btnKV.Image = bm;
                        }
                    }
                    catch
                    {
                    }
                    btnKV.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btnKV.ImageLocation = ImageLocation.TopCenter;
                    btnKV.RightToLeft = RightToLeft.No;
                    btnKV.Size = new Size(125, 80);
                    if (i != 0)
                    {
                        locateY += int.Parse(btnKV.Location.Y.ToString()) + 85;
                    }
                    btnKV.Location = new Point(locateX, locateY);
                    btnKV.Click += new EventHandler(btnKV_Click);
                    pnKV_Mon.Controls.Add(btnKV);
                }
            }
            catch
            {
            }
        }
        
        private void GetListMon()
        {
            try
            {
                dtMon.Clear();
                int width = (pnTable_Mon.Width / 110);
                int height = (pnTable_Mon.Height / 80);
                int sizeMon = width * height;
                string sql = string.Format("Exec SelectMON @PageNumber={0},@PageSize={1}, @maNH={2}, @maKV={3},@maCH={4}", cPageMon, sizeMon, clsMain.SQLString (maNH), maKV, cls_ConfigCashier.idShop);
                dtMon = clsMain.ReturnDataTable(sql);

                while (pnTable_Mon.Controls.Count > 0)
                {
                    pnTable_Mon.Controls[0].Dispose();
                }
                pnTable_Mon.Controls.Clear();
                int count = (pnTable_Mon.Width / 120) + 1;
                int line = 0;
                int locateY = 2;
                int locateX = 2;
                int x = 0;
                int y = 0;
                for (int i = 0; i < dtMon.Rows.Count; i++)
                {
                    line++;
                    SimpleButton btnBan_Mon = new SimpleButton();
                    btnBan_Mon.Name = dtMon.Rows[i]["MA_HANGHOA"].ToString();
                    btnBan_Mon.Text = dtMon.Rows[i]["TEN_HANGHOA"].ToString() + "\n" + "Giá:" + string.Format("{0:0,0 }", double.Parse(dtMon.Rows[i]["GIABAN"].ToString())) + "";
                    btnBan_Mon.Tag = dtMon.Rows[i]["TEN_HANGHOA"].ToString() + " - " + "Giá:" + string.Format("{0:0,0 }", double.Parse(dtMon.Rows[i]["GIABAN"].ToString())) + "";
                    try
                    {
                        byte[] tam = new byte[((byte[])dtMon.Rows[i]["HINHANH"]).LongLength];
                        tam = (byte[])dtMon.Rows[i]["HINHANH"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        //btnBan_Mon.Image = bm;
                        if (cls_KP_RES.iHinhanh == 1)
                        {
                            btnBan_Mon.BackgroundImage = bm;
                            btnBan_Mon.BackgroundImageLayout = ImageLayout.Stretch;
                            btnBan_Mon.Appearance.BackColor = Color.Transparent;
                            btnBan_Mon.Appearance.Options.UseBackColor = true;
                            btnBan_Mon.ButtonStyle = BorderStyles.NoBorder;
                            btnBan_Mon.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                        }
                        else
                        {
                            btnBan_Mon.Image = bm;
                        }
                    }
                    catch
                    {
                    }
                    btnBan_Mon.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btnBan_Mon.ImageLocation = ImageLocation.TopCenter;
                    btnBan_Mon.RightToLeft = RightToLeft.No;

                    if (cls_KP_RES.iHinhanh == 1)
                    {
                        btnBan_Mon.Size = new Size(168, 130);
                        if (this.Width < 1100)
                        {
                            if (line == 4)
                            {
                                locateY += int.Parse(btnBan_Mon.Location.Y.ToString()) + 133;
                                locateX = 2;
                                line = 1;
                            }
                            else
                            {
                                if (i != 0)
                                {
                                    locateX += int.Parse(btnBan_Mon.Location.X.ToString()) + 171;
                                }
                            }
                        }
                        else
                        {
                            if (line == (pnTable_Mon.Width / btnBan_Mon.Width) + 1)
                            {
                                locateY += int.Parse(btnBan_Mon.Location.Y.ToString()) + 133;
                                locateX = 2;
                                line = 1;
                            }
                            else
                            {
                                if (i != 0)
                                {
                                    locateX += int.Parse(btnBan_Mon.Location.X.ToString()) + 171;
                                }
                            }
                        }
                    }
                    else
                    {
                        btnBan_Mon.Size = new Size(110, 80);
                        if (line == count)
                        {
                            locateY += int.Parse(btnBan_Mon.Location.Y.ToString()) + 85;
                            locateX = 2;
                            line = 1;
                        }
                        else
                        {
                            if (i != 0)
                            {
                                if (this.Width < 1100)
                                {
                                    locateX += int.Parse(btnBan_Mon.Location.X.ToString()) + 132;
                                }
                                else
                                {
                                    locateX += int.Parse(btnBan_Mon.Location.X.ToString()) + 124;
                                }
                            }
                        }
                    }


                    btnBan_Mon.Location = new Point(locateX, locateY);
                    btnBan_Mon.Click += new EventHandler(btnBan_Mon_Click);
                    pnTable_Mon.Controls.Add(btnBan_Mon);
                }
            }
            catch
            {
            }
        }

        private void btn_UpKV_Click(object sender, EventArgs e)
        {
            if (cPageDish > 1)
            {
                cPageDish -= 1;
                GetNhomhang();
                GetListMon();
            }
        }

        private void btn_DowKV_Click(object sender, EventArgs e)
        {
            if (cPageDish < 20)
            {
                cPageDish += 1;
                GetNhomhang();
                GetListMon();
            }
        }

        private void btn_NextToLeft_Click(object sender, EventArgs e)
        {
            if (cPageMon > 1)
            {
                cPageMon = cPageMon - 1;
                lbPageTable.Text = cPageMon.ToString();
                GetListMon();
            }
        }

        private void btn_NextToRight_Click(object sender, EventArgs e)
        {
            if (cPageMon < 20)
            {
                cPageMon = cPageMon + 1;
                lbPageTable.Text = cPageMon.ToString();
                GetListMon();
            }
        }

        private void btnKV_Click(object sender, EventArgs e)
        {
            try
            {
                cPageMon = 1;
                lbPageTable.Text = cPageMon.ToString();
                btnComboOption.Visible = false;
                foreach (SimpleButton btnKV in pnKV_Mon.Controls)
                {
                    if (btnKV.Name == this.ActiveControl.Name)
                    {
                        maNH = btnKV.Name.ToString();
                        SetBackgroudOnClick(btnKV);
                        GetListMon();
                    }
                    else
                    {
                        SetDefautlBackgroud(btnKV);
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK);
            }
        }

        private void btnBan_Mon_Click(object sender, EventArgs e)
        {
            try
            {
                btnComboOption.Visible = false;
                pnBtnDeleteGv.Visible = false;
                foreach (SimpleButton btnBan_Mon in pnTable_Mon.Controls)
                {
                    if (btnBan_Mon.Name == this.ActiveControl.Name)
                    {
                        maMon = btnBan_Mon.Name;
                        lbNameMon.Text = " >   " + btnBan_Mon.Tag;
                        sTenMon = btnBan_Mon.Tag.ToString().Substring(0, btnBan_Mon.Tag.ToString().LastIndexOf("-"));
                        SetBackgroudOnClick(btnBan_Mon);
                        GetDishGoToBuy(maMon, maKV);
                    }
                    else
                    {
                        SetDefautlBackgroud(btnBan_Mon);
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK);
            }
        }
        
        private void GetDishGoToBuy(string maHangHoa, int idKV)
        {
            try
            {
                string sql = "";
                DataTable dtHH;
                bool CongCombo = false;
                sql = string.Format("SELECT MA_HANGHOA, TEN_HANGHOA, ISNULL(MONTHEM,0) AS MONTHEM, ISNULL(INTEM,0) AS INTEM FROM HANGHOA WHERE MA_HANGHOA='" + maHangHoa + "'");
                dtHH = clsMain.ReturnDataTable(sql);
                bool sMONTHEM = bool.Parse(dtHH.Rows[0]["MONTHEM"].ToString());
                bool sINTEM = bool.Parse(dtHH.Rows[0]["INTEM"].ToString());

                sql = string.Format("SELECT MA_HANGHOA, TEN_HANGHOA FROM HANGHOA A INNER JOIN NHOMHANG B ON A.MA_NHOMHANG=B.MA_NHOMHANG AND B.MONTHEM=1 WHERE MA_HANGHOA='"+maHangHoa+"'");
                dtHH = clsMain.ReturnDataTable(sql);

                if (dtHH.Rows.Count > 0 && sButtonMonThem == true)
                {
                    string sMaHangHoaChonTrenLuoi = gv_SelectProduct.GetRowCellValue(iFocusRow, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                    string sql_HangHoaChonTrenLuoi = string.Format("SELECT MA_HANGHOA, TEN_HANGHOA, ISNULL(MONTHEM,0) AS MONTHEM FROM HANGHOA WHERE MA_HANGHOA='" + sMaHangHoaChonTrenLuoi + "'");
                    DataTable dtHH_HangHoaChonTrenLuoi = clsMain.ReturnDataTable(sql_HangHoaChonTrenLuoi);

                    bool sMONTHEM_HangHoaChonTrenLuoi = bool.Parse(dtHH_HangHoaChonTrenLuoi.Rows[0]["MONTHEM"].ToString());
                    if (sMONTHEM_HangHoaChonTrenLuoi == true)
                    {
                        sTEM = int.Parse(gv_SelectProduct.GetRowCellValue(iFocusRow, gv_SelectProduct.Columns["TEM"]).ToString());
                        sMaCB = int.Parse(gv_SelectProduct.GetRowCellValue(iFocusRow, gv_SelectProduct.Columns["MACB"]).ToString());
                    }                
                }
                sql = string.Format("Exec SelectByMON_TRASUA @MAHANG='{0}',@MAVACH='{1}',@maKV={2},@maCH={3}", maHangHoa, maHangHoa, idKV, cls_ConfigCashier.idShop);
                dtHH = clsMain.ReturnDataTable(sql);

                DM_HangHoa hanghoakm = new DM_HangHoa();
                if (dtHH.Rows.Count < 1)
                {
                    return;
                }
                //sửa định lượng
                if (bool.Parse(dtHH.Rows[0]["SUADINHLUONG"].ToString()))
                {
                    bool flag_Exist = false; // flag_Exist=false: mặt hàng này chưa bán và ngược lại
                    if (gv_SelectProduct.RowCount > 0)
                    {
                        for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                        {
                            if (gv_SelectProduct.GetRowCellValue(i, "MA_HANGHOA").ToString() == maHangHoa)
                            {
                                flag_Exist = true;
                                break;
                            }
                        }
                    }
                    if (!flag_Exist)
                    {
                        sql = "Select MANGUYENLIEU" + "\n";
                        sql += "From DINHLUONG" + "\n";
                        sql += "Where MATHANHPHAM=" + clsMain.SQLString(dtHH.Rows[0]["MA_HANGHOA"].ToString());
                        DataTable dtDL = clsMain.ReturnDataTable(sql);
                        if (dtDL.Rows.Count > 0)
                        {
                            Frm_Suadinhluong frm = new Frm_Suadinhluong(dtHH.Rows[0]["MA_HANGHOA"].ToString());
                            frm.ShowDialog();
                            frm.Dispose();
                        }
                        else
                        {
                            XtraMessageBox.Show(_chuakhaibaodinhluong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                }
                //sửa giá
                decimal Giaban_Cur = 0;//giá bán thực của hàng hóa.
                decimal SoLuong_Cur = 0;
                if (bool.Parse(dtHH.Rows[0]["SUAGIA"].ToString()))
                {
                    bool flag_Exist = false; // flag_Exist=false: mặt hàng này chưa bán và ngược lại
                    
                    if (!flag_Exist)
                    {
                        Frm_Chontheothoigia frm = new Frm_Chontheothoigia();
						frm.tmp_Thoigia = decimal.Parse(dtHH.Rows[0]["GIABAN"].ToString());
                        frm.ShowDialog();
						if (frm.bThoat == true)
                        {
                            return;
                        }
                        Giaban_Cur = frm.tmp_Thoigia;//Lấy giá theo thời giá người dùng nhập vào
                        SoLuong_Cur = frm.tmp_SoLuong;
                        frm.Dispose();
                    }
                }
                else
                {
                    SoLuong_Cur = 1;
                    Giaban_Cur = decimal.Parse(dtHH.Rows[0]["GIABAN"].ToString());
                }
                //lấy dữ liệu hàng hóa mới chọn
                DM_HangHoa hanghoa = new DM_HangHoa();
                hanghoa.MA_HANGHOA = dtHH.Rows[0]["MA_HANGHOA"].ToString();
                hanghoa.TEN_HANGHOA = dtHH.Rows[0]["TEN_HANGHOA"].ToString();
                hanghoa.SOLUONG = SoLuong_Cur;
                hanghoa.MA_BEP = dtHH.Rows[0]["MA_BEP"].ToString();
                hanghoa.GIABAN = Giaban_Cur;
                hanghoa.GHICHU = dtHH.Rows[0]["GHICHU"].ToString();
                hanghoa.THUE = double.Parse(dtHH.Rows[0]["THUE"].ToString());
                hanghoa.TRANGTHAI = false;
                hanghoa.MONTHEM = bool.Parse(dtHH.Rows[0]["MONTHEM"].ToString());
                hanghoa.IS_COMBO = bool.Parse(dtHH.Rows[0]["IS_COMBO"].ToString());
                hanghoa.INTEM = bool.Parse(dtHH.Rows[0]["INTEM"].ToString());
                //kiem tra co tren luoi chua
                bool testID = false;  // bien ktra trùng tên khi addrow hàng hóa
                if (dt.Rows.Count < 1)
                {
                    testID = false;
                }
                else
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["MA_HANGHOA"].ToString() == hanghoa.MA_HANGHOA && "cb" != dt.Rows[i]["GHICHU"].ToString() && !bool.Parse(dt.Rows[i]["IS_KHUYENMAI"].ToString()))
                        {
                            var dtCombo = GetProductInCombo(hanghoa.MA_HANGHOA);
                            if (dtCombo.Rows.Count > 0)
                            {
                                if (KiemTraCombo(hanghoa.MA_HANGHOA) == false)
                                {
                                    iFocusRow = i;
                                    dt.Rows[i]["SOLUONG"] = double.Parse(dt.Rows[i]["SOLUONG"].ToString()) + 1;
                                    for (int j = 0; j < dtCombo.Rows.Count; j++)
                                    {
                                        double sl = double.Parse(dtCombo.Rows[j]["SOLUONG"].ToString());
                                        sl += double.Parse(dt.Rows[i + j + 1]["SOLUONG"].ToString());
                                        dt.Rows[i + j + 1]["SOLUONG"] = sl;
                                    }
                                    testID = true;
                                    CongCombo = true;
                                    break;
                                }
                                else
                                {
                                    DataRow dr = dt.NewRow();
                                    dr[0] = hanghoa.MA_HANGHOA;
                                    dr[2] = hanghoa.SOLUONG;
                                    dr[3] = hanghoa.GIABAN;
                                    dr[4] = hanghoa.GHICHU;
                                    dr[5] = hanghoa.MA_BEP;
                                    dr[6] = hanghoa.THUE;
                                    dr["CHIETKHAU"] = 0;
                                    dr["TRANGTHAI"] = false;//hàng hóa bình thường,được xóa hay thêm giảm
                                    dr["IS_COMBO"] = false;//hàng hóa bình thường
                                    dr["IS_INBEP"] = bool.Parse(dtHH.Rows[0]["IS_INBEP"].ToString());
                                    dr["IS_KHUYENMAI"] = false;
                                    dr["MONTHEM"] = bool.Parse(dtHH.Rows[0]["MONTHEM"].ToString());
                                    dr["MACB"] = sMaCB;
                                    dr["INTEM"] = bool.Parse(dtHH.Rows[0]["INTEM"].ToString());
                                    if (sButtonMonThem == false)
                                    {
                                        dr[1] = hanghoa.TEN_HANGHOA;

                                        if (hanghoa.IS_COMBO == false)
                                        {
                                            if (sINTEM == true)
                                                dr["TEM"] = sTEM;
                                            else
                                                dr["TEM"] = 0;
                                            dr["MACP"] = "C";
                                            dr["STT"] = sTT;
                                        }
                                        else
                                        {
                                            dr["TEM"] = 0;
                                            dr["MACP"] = "CB";
                                            dr["STT"] = "CB";
                                        }

                                    }
                                    else if (sButtonMonThem == true)
                                    {
                                        dr[1] = Kytu_Monthem + hanghoa.TEN_HANGHOA;
                                        dr["TEM"] = sTEM;
                                        dr["MACP"] = "P";
                                    }
                                    dt.Rows.Add(dr);
                                    for (int j = 0; j < dtCombo.Rows.Count; j++)
                                    {
                                        DataRow drr = dt.NewRow();
                                        string sql_combo = string.Format("SELECT MA_HANGHOA, TEN_HANGHOA, ISNULL(MONTHEM,0) AS MONTHEM, ISNULL(INTEM,0) AS INTEM FROM HANGHOA WHERE MA_HANGHOA='" + dtCombo.Rows[j]["MA_HANGHOA"].ToString() + "'");
                                        DataTable dtHH_combo = clsMain.ReturnDataTable(sql_combo);
                                        bool sMONTHEM_combo = bool.Parse(dtHH_combo.Rows[0]["MONTHEM"].ToString());
                                        bool sINTEM_combo = bool.Parse(dtHH_combo.Rows[0]["INTEM"].ToString());
                                        drr[0] = dtCombo.Rows[j]["MA_HANGHOA"].ToString();
                                        drr[2] = dtCombo.Rows[j]["SOLUONG"].ToString();
                                        drr[3] = 0;
                                        drr[4] = "cb";
                                        drr[5] = dtCombo.Rows[j]["MA_BEP"].ToString();
                                        drr[6] = 0;
                                        drr["CHIETKHAU"] = 0;
                                        drr["TRANGTHAI"] = true;//hàng hóa thuộc Combo,không được xóa hay thêm giảm
                                        drr["IS_COMBO"] = true;//hàng hóa thuộc Combo
                                        drr["IS_INBEP"] = bool.Parse(dtHH.Rows[0]["IS_INBEP"].ToString());
                                        drr["IS_KHUYENMAI"] = false;
                                        drr["MONTHEM"] = bool.Parse(dtCombo.Rows[j]["MONTHEM"].ToString());
                                        drr["INTEM"] = bool.Parse(dtCombo.Rows[j]["INTEM"].ToString());
                                        drr["MACB"] = sMaCB;
                                        if (sButtonMonThem == false)
                                        {
                                            drr[1] = Kytu_Combo + dtCombo.Rows[j]["TEN_HANGHOA"].ToString();
                                            if (sINTEM_combo == true)
                                            {
                                                drr["TEM"] = sTEM;
                                                sTEM += 1;
                                            }
                                            else
                                                drr["TEM"] = 0;
                                            drr["MACP"] = "CBC";
                                            drr["STT"] = sTT;
                                            sTT += 1;
                                        }
                                        else if (sButtonMonThem == true)
                                        {
                                            drr[1] = Kytu_Monthem + hanghoa.TEN_HANGHOA;
                                            drr["TEM"] = sTEM;
                                            drr["MACP"] = "P";
                                        }
                                        dt.Rows.Add(drr);
                                    }
                                    iFocusRow = i;
                                    testID = true;
                                    break;
                                }
                            }
                            else
                            {
                                if (sButtonMonThem == true & int.Parse(dt.Rows[i]["TEM"].ToString()) == sTEM & !bool.Parse(dtHH.Rows[0]["SUAGIA"].ToString())
                                    || sButtonMonThem == false & sMONTHEM == false & sINTEM == false & dt.Rows[i]["MACP"].ToString() == "C" & !bool.Parse(dtHH.Rows[0]["SUAGIA"].ToString())
                                    || sButtonMonThem == false & sMONTHEM == false & sINTEM == false & dt.Rows[i]["MACP"].ToString() == "CBC" & !bool.Parse(dtHH.Rows[0]["SUAGIA"].ToString()))
                                {
                                    iFocusRow = i;
                                    if (dt.Rows[i]["MACP"].ToString() == "P")
                                    {
                                        dt.Rows[i]["SOLUONG"] = double.Parse(dt.Rows[i]["SOLUONG"].ToString()) + LaySoLuongLonNhat(sMaCB);
                                    }
                                    else
                                    {
                                        dt.Rows[i]["SOLUONG"] = double.Parse(dt.Rows[i]["SOLUONG"].ToString()) + 1;
                                    }
                                    testID = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            testID = false;
                        }
                    }
                }
                //Hàng hóa chưa được order trong bàn nên thêm dòng mới
                if (testID == false)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = hanghoa.MA_HANGHOA;
                    dr[2] = hanghoa.SOLUONG;
                    dr[3] = hanghoa.GIABAN;
                    dr[4] = hanghoa.GHICHU;
                    dr[5] = hanghoa.MA_BEP;
                    dr[6] = hanghoa.THUE;
                    dr["CHIETKHAU"] = 0;
                    dr["TRANGTHAI"] = false;//hàng hóa bình thường,được xóa hay thêm giảm
                    dr["IS_COMBO"] = false;//hàng hóa bình thường
                    dr["IS_INBEP"] = bool.Parse(dtHH.Rows[0]["IS_INBEP"].ToString());
                    dr["IS_KHUYENMAI"] = false;
                    dr["MONTHEM"] = bool.Parse(dtHH.Rows[0]["MONTHEM"].ToString());
                    dr["MACB"] = sMaCB;
                    dr["INTEM"] = bool.Parse(dtHH.Rows[0]["INTEM"].ToString());
                    if (sButtonMonThem == false)
                    {
                        dr[1] = hanghoa.TEN_HANGHOA;
                        
                        if (hanghoa.IS_COMBO == false)
                        {
                            if (sINTEM == true)
                                dr["TEM"] = sTEM;
                            else
                                dr["TEM"] = 0;
                            dr["MACP"] = "C";
                            dr["STT"] = sTT;
                        }
                        else
                        {
                            dr["TEM"] = 0;
                            dr["MACP"] = "CB";
                            dr["STT"] = "CB";
                        }
                    }
                    else if (sButtonMonThem == true)
                    {
                        dr[1] = Kytu_Monthem + hanghoa.TEN_HANGHOA;
                        dr["TEM"] = sTEM;
                        dr["MACP"] = "P";
                        dr[2] = hanghoa.SOLUONG * LaySoLuongLonNhat(sMaCB);
                    }
                    dt.Rows.Add(dr);
                    // Combo 
                    var dtCombo = GetProductInCombo(hanghoa.MA_HANGHOA);
                    if (dtCombo.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtCombo.Rows.Count; i++)
                        {
                            DataRow drr = dt.NewRow();
                            string sql_combo = string.Format("SELECT MA_HANGHOA, TEN_HANGHOA, ISNULL(MONTHEM,0) AS MONTHEM, ISNULL(INTEM,0) AS INTEM FROM HANGHOA WHERE MA_HANGHOA='" + dtCombo.Rows[i]["MA_HANGHOA"].ToString() + "'");
                            DataTable dtHH_combo = clsMain.ReturnDataTable(sql_combo);
                            bool sMONTHEM_combo = bool.Parse(dtHH_combo.Rows[0]["MONTHEM"].ToString());
                            bool sINTEM_combo = bool.Parse(dtHH_combo.Rows[0]["INTEM"].ToString());
                            drr[0] = dtCombo.Rows[i]["MA_HANGHOA"].ToString();
                            drr[2] = dtCombo.Rows[i]["SOLUONG"].ToString();
                            drr[3] = 0;
                            drr[4] = "cb";
                            drr[5] = dtCombo.Rows[i]["MA_BEP"].ToString();
                            drr[6] = 0;
                            drr["CHIETKHAU"] = 0;
                            drr["TRANGTHAI"] = true;//hàng hóa thuộc Combo,không được xóa hay thêm giảm
                            drr["IS_COMBO"] = true;//hàng hóa thuộc Combo
                            drr["IS_INBEP"] = bool.Parse(dtHH.Rows[0]["IS_INBEP"].ToString());
                            drr["IS_KHUYENMAI"] = false;
                            drr["MONTHEM"] = bool.Parse(dtCombo.Rows[i]["MONTHEM"].ToString());
                            drr["MACB"] = sMaCB;
                            drr["INTEM"] = bool.Parse(dtCombo.Rows[i]["INTEM"].ToString());
                            if (sButtonMonThem == false)
                            {
                                drr[1] = Kytu_Combo + dtCombo.Rows[i]["TEN_HANGHOA"].ToString();
                                if (sINTEM_combo == true)
                                {
                                    drr["TEM"] = sTEM;
                                    sTEM += 1;
                                }
                                else
                                    drr["TEM"] = 0;
                                drr["MACP"] = "CBC";
                                drr["STT"] = sTT;
                                sTT += 1;
                            }
                            else if (sButtonMonThem == true)
                            {
                                drr[1] = Kytu_Monthem + hanghoa.TEN_HANGHOA;
                                drr["TEM"] = sTEM;
                                drr["MACP"] = "P";
                            }
                            dt.Rows.Add(drr);
                        }
                    }
                    if (sButtonMonThem == false)
                        iFocusRow = dt.Rows.Count - 1 - dtCombo.Rows.Count;
                }

                #region  phần khuyến mãi
                String sSQL = "";

                DataTable dt1 = DanhSachSpKM(maHangHoa);
                if (dt1.Rows.Count > 0)
                {
                    if (dt1.Rows.Count == 1)
                    {
                        if (int.Parse(dt1.Rows[0]["MALOAICTKM"].ToString()) == 1)
                        {
                            sSQL = "";
                            sSQL += "SELECT HANGMUA, SOLUONGHANGMUA, HANGTANG, SOLUONGHANGTANG" + "\n";
                            sSQL += "FROM CT_KHUYENMAI" + "\n";
                            sSQL += "WHERE MAKHUYENMAI = " + clsMain.SQLString(dt1.Rows[0]["MAKHUYENMAI"].ToString()) + "\n";
                            sSQL += "AND HANGMUA = " + clsMain.SQLString(maHangHoa) + "\n";
                            dt1 = clsMain.ReturnDataTable(sSQL);

                            if (int.Parse(dt.Rows[iFocusRow]["SOLUONG"].ToString()) % int.Parse(dt1.Rows[0]["SOLUONGHANGMUA"].ToString()) == 0)
                            {
                                sSQL = "";
                                sSQL += "SELECT MA_HANGHOA, TEN_HANGHOA, MA_BEP, ISNULL(IS_INBEP,0) AS IS_INBEP, ISNULL(MONTHEM,0) AS MONTHEM" + "\n";
                                sSQL += "FROM HANGHOA" + "\n";
                                sSQL += "WHERE MA_HANGHOA = " + clsMain.SQLString(dt1.Rows[0]["HANGTANG"].ToString());
                                DataTable dt2 = clsMain.ReturnDataTable(sSQL);
                                hanghoakm.MA_HANGHOA = dt2.Rows[0]["MA_HANGHOA"].ToString();
                                hanghoakm.TEN_HANGHOA = dt2.Rows[0]["TEN_HANGHOA"].ToString();
                                hanghoakm.SOLUONG = int.Parse(dt.Rows[iFocusRow]["SOLUONG"].ToString()) / int.Parse(dt1.Rows[0]["SOLUONGHANGMUA"].ToString()) * int.Parse(dt1.Rows[0]["SOLUONGHANGTANG"].ToString());
                                hanghoakm.MA_BEP = dt2.Rows[0]["MA_BEP"].ToString();
                                hanghoakm.GIABAN = 0;
                                hanghoakm.GHICHU = "";
                                hanghoakm.THUE = 0;
                                hanghoakm.TRANGTHAI = false;
                                hanghoakm.IS_KHUYENMAI = true;
                                hanghoakm.MONTHEM = bool.Parse(dt2.Rows[0]["MONTHEM"].ToString());
                                //xem hàng khuyến mãi có chưa,nếu có thì cộng sl,nếu chưa thì thêm mới
                                bool flag_km = false;
                                //for (int i = 0; i < dt.Rows.Count; i++)
                                //{
                                //    if (dt.Rows[i]["MA_HANGHOA"].ToString() == hanghoakm.MA_HANGHOA && bool.Parse(dt.Rows[i]["IS_KHUYENMAI"].ToString()))
                                //    {
                                //        dt.Rows[i]["SOLUONG"] = double.Parse(dt.Rows[i]["SOLUONG"].ToString()) + double.Parse(hanghoakm.SOLUONG.ToString());

                                //        flag_km = true;
                                //        break;
                                //    }
                                //    else
                                //        flag_km = false;
                                //}
                                if (!flag_km)
                                {
                                    DataRow dr = dt.NewRow();
                                    string sql_KM = string.Format("SELECT MA_HANGHOA, TEN_HANGHOA, ISNULL(MONTHEM,0) AS MONTHEM FROM HANGHOA WHERE MA_HANGHOA='" + hanghoakm.MA_HANGHOA + "'");
                                    DataTable dtHH_KM = clsMain.ReturnDataTable(sql_KM);
                                    bool sMONTHEM_KM = bool.Parse(dtHH_KM.Rows[0]["MONTHEM"].ToString());
                                    dr[0] = hanghoakm.MA_HANGHOA;                                    
                                    dr[2] = hanghoakm.SOLUONG;
                                    dr[3] = hanghoakm.GIABAN;
                                    dr[4] = hanghoakm.GHICHU;
                                    dr[5] = hanghoakm.MA_BEP;
                                    dr[6] = hanghoakm.THUE;
                                    dr["CHIETKHAU"] = 0;
                                    dr["TRANGTHAI"] = true;
                                    dr["IS_COMBO"] = false;
                                    dr["IS_INBEP"] = bool.Parse(dt2.Rows[0]["IS_INBEP"].ToString());
                                    dr["IS_KHUYENMAI"] = true;
                                    dr["MONTHEM"] = hanghoakm.MONTHEM;
                                    dr["MACB"] = sMaCB;
                                    if (sButtonMonThem == false)
                                    {
                                        //sTEM = int.Parse(hanghoa.MA_HANGHOA);
                                        dr[1] = hanghoakm.TEN_HANGHOA;
                                        if (sMONTHEM_KM == true)
                                        {
                                            dr["TEM"] = sTEM + 1;
                                            sTEM += 1;
                                        }
                                        else
                                            dr["TEM"] = 0;
                                        
                                        dr["MACP"] = "C";
                                        dr["STT"] = sTT + 1;
                                        sTT += 1;
                                       
                                    }
                                    else if (sButtonMonThem == true)
                                    {
                                        dr[1] = Kytu_Monthem + hanghoakm.TEN_HANGHOA;
                                        dr["TEM"] = sTEM;
                                        dr["MACP"] = "P";
                                    }
                                    dt.Rows.Add(dr);
                                }
                            }
                        }
                        else if (int.Parse(dt1.Rows[0]["MALOAICTKM"].ToString()) == 2)
                        {
                            if (!testID)
                            {
                                sSQL = "";
                                sSQL += "SELECT ISNULL(SOTIENKM, 0) AS SOTIENKM, ISNULL(TYLEKM, 0) AS TYLEKM" + "\n";
                                sSQL += "FROM CT_KHUYENMAI" + "\n";
                                sSQL += "WHERE MAKHUYENMAI = " + clsMain.SQLString(dt1.Rows[0]["MAKHUYENMAI"].ToString()) + "\n";
                                sSQL += "AND (NHOMHANG_HANGHOA = " + clsMain.SQLString(dtNH.Rows[0]["MA_NHOMHANG"].ToString()) + "\n";
                                sSQL += "OR NHOMHANG_HANGHOA = " + clsMain.SQLString(maHangHoa) + ")" + "\n";
                                dt1 = clsMain.ReturnDataTable(sSQL);

                                if (dt1.Rows.Count > 0)
                                {
                                    if (float.Parse(dt1.Rows[0]["SOTIENKM"].ToString()) != 0)
                                        dt.Rows[iFocusRow]["GIABAN"] = double.Parse(dt.Rows[iFocusRow]["GIABAN"].ToString()) - double.Parse(dt1.Rows[0]["SOTIENKM"].ToString());
                                    else
                                        dt.Rows[iFocusRow]["GIABAN"] = double.Parse(dt.Rows[iFocusRow]["GIABAN"].ToString()) - (double.Parse(dt.Rows[iFocusRow]["GIABAN"].ToString()) * double.Parse(dt1.Rows[0]["TYLEKM"].ToString()) / 100);
                                }
                            }
                        }
                    }
                    else
                    {
                        sSQL = "SELECT GIATRI FROM CAUHINH WHERE TEN = 'KHUYENMAIUUTIEN'";
                        DataTable dtcauhinh = clsMain.ReturnDataTable(sSQL);
                        if (dtcauhinh.Rows.Count > 0)
                        {
                            string sMaCTKM = "";
                            string sMaKM = "";
                            int max = int.MaxValue;
                            int m = dt1.Rows.Count;
                            for (int i = 0; i < m; i++)
                            {
                                if (dtcauhinh.Rows[0][0].ToString() == dt1.Rows[i]["MALOAICTKM"].ToString())
                                {
                                    if (int.Parse(dt1.Rows[0]["THUTUUUTIEN"].ToString()) < max)
                                    {
                                        sMaCTKM = dtcauhinh.Rows[0][0].ToString();
                                        sMaKM = dt1.Rows[i]["MAKHUYENMAI"].ToString();
                                        max = int.Parse(dt1.Rows[i]["THUTUUUTIEN"].ToString());
                                    }
                                }
                            }
                            if (sMaCTKM != "" && sMaKM != "")
                            {
                                if (sMaCTKM == "1")
                                {
                                    sSQL = "";
                                    sSQL += "SELECT HANGMUA, SOLUONGHANGMUA, HANGTANG, SOLUONGHANGTANG" + "\n";
                                    sSQL += "FROM CT_KHUYENMAI" + "\n";
                                    sSQL += "WHERE MAKHUYENMAI = " + clsMain.SQLString(sMaKM) + "\n";
                                    sSQL += "AND HANGMUA = " + clsMain.SQLString(maHangHoa) + "\n";
                                    dt1 = clsMain.ReturnDataTable(sSQL);

                                    if (int.Parse(dt.Rows[iFocusRow]["SOLUONG"].ToString()) % int.Parse(dt1.Rows[0]["SOLUONGHANGMUA"].ToString()) == 0)
                                    {
                                        sSQL = "";
                                        sSQL += "SELECT MA_HANGHOA, TEN_HANGHOA, MA_BEP, ISNULL(IS_INBEP, 0) AS IS_INBEP, ISNULL(MONTHEM,0) AS MONTHEM" + "\n";
                                        sSQL += "FROM HANGHOA" + "\n";
                                        sSQL += "WHERE MA_HANGHOA = " + clsMain.SQLString(dt1.Rows[0]["HANGTANG"].ToString());
                                        DataTable dt2 = clsMain.ReturnDataTable(sSQL);
                                        hanghoakm.MA_HANGHOA = dt2.Rows[0]["MA_HANGHOA"].ToString();
                                        hanghoakm.TEN_HANGHOA = dt2.Rows[0]["TEN_HANGHOA"].ToString();
                                        hanghoakm.SOLUONG = int.Parse(dt.Rows[iFocusRow]["SOLUONG"].ToString()) / int.Parse(dt1.Rows[0]["SOLUONGHANGMUA"].ToString()) * int.Parse(dt1.Rows[0]["SOLUONGHANGTANG"].ToString());
                                        hanghoakm.MA_BEP = dt2.Rows[0]["MA_BEP"].ToString();
                                        hanghoakm.GIABAN = 0;
                                        hanghoakm.GHICHU = "";
                                        hanghoakm.THUE = 0;
                                        hanghoakm.TRANGTHAI = false;
                                        hanghoakm.IS_KHUYENMAI = true;
                                        hanghoakm.MONTHEM = bool.Parse(dt2.Rows[0]["MONTHEM"].ToString());
                                        //xem hàng khuyến mãi có chưa,nếu có thì cộng sl,nếu chưa thì thêm mới
                                        bool flag_km = false;
                                        for (int i = 0; i < dt.Rows.Count; i++)
                                        {
                                            if (dt.Rows[i]["MA_HANGHOA"].ToString() == hanghoakm.MA_HANGHOA && bool.Parse(dt.Rows[i]["IS_KHUYENMAI"].ToString()))
                                            {
                                                dt.Rows[i]["SOLUONG"] = double.Parse(dt.Rows[i]["SOLUONG"].ToString()) + double.Parse(hanghoakm.SOLUONG.ToString());

                                                flag_km = true;
                                                break;
                                            }
                                            else
                                                flag_km = false;
                                        }
                                        if (!flag_km)
                                        {
                                            DataRow dr = dt.NewRow();
                                            string sql_KM = string.Format("SELECT MA_HANGHOA, TEN_HANGHOA, ISNULL(MONTHEM,0) AS MONTHEM FROM HANGHOA WHERE MA_HANGHOA='" + hanghoakm.MA_HANGHOA + "'");
                                            DataTable dtHH_KM = clsMain.ReturnDataTable(sql_KM);
                                            bool sMONTHEM_KM = bool.Parse(dtHH_KM.Rows[0]["MONTHEM"].ToString());
                                            dr[0] = hanghoakm.MA_HANGHOA;
                                            dr[2] = hanghoakm.SOLUONG;
                                            dr[3] = hanghoakm.GIABAN;
                                            dr[4] = hanghoakm.GHICHU;
                                            dr[5] = hanghoakm.MA_BEP;
                                            dr[6] = hanghoakm.THUE;
                                            dr["CHIETKHAU"] = 0;
                                            dr["TRANGTHAI"] = true;
                                            dr["IS_COMBO"] = false;
                                            dr["IS_INBEP"] = bool.Parse(dt2.Rows[0]["IS_INBEP"].ToString());
                                            dr["IS_KHUYENMAI"] = true;
                                            dr["MONTHEM"] = hanghoakm.MONTHEM;
                                            dr["MACB"] = sMaCB;
                                            if (sButtonMonThem == false)
                                            {
                                                //sTEM = int.Parse(hanghoa.MA_HANGHOA);
                                                dr[1] = hanghoakm.TEN_HANGHOA;
                                                if (sMONTHEM_KM == true)
                                                {
                                                    dr["TEM"] = sTEM + 1;
                                                    sTEM += 1;
                                                }
                                                else
                                                    dr["TEM"] = 0;
                                                dr["MACP"] = "C";
                                                dr["STT"] = sTT + 1;
                                                sTT += 1;
                                                
                                            }
                                            else if (sButtonMonThem == true)
                                            {
                                                dr[1] = Kytu_Monthem + hanghoakm.TEN_HANGHOA;
                                                dr["TEM"] = sTEM;
                                                dr["MACP"] = "P";
                                            } 
                                            dt.Rows.Add(dr);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!testID)
                                    {
                                        sSQL = "";
                                        sSQL += "SELECT ISNULL(SOTIENKM,0) AS SOTIENKM, ISNULL(TYLEKM,0) AS TYLEKM" + "\n";
                                        sSQL += "FROM CT_KHUYENMAI" + "\n";
                                        sSQL += "WHERE MAKHUYENMAI = " + clsMain.SQLString(sMaKM) + "\n";
                                        sSQL += "AND (NHOMHANG_HANGHOA = " + clsMain.SQLString(dtNH.Rows[0]["MA_NHOMHANG"].ToString()) + "\n";
                                        sSQL += "OR NHOMHANG_HANGHOA = " + clsMain.SQLString(maHangHoa) + ")" + "\n";
                                        dt1 = clsMain.ReturnDataTable(sSQL);
                                        if (dt1.Rows.Count > 0)
                                        {
                                            if (float.Parse(dt1.Rows[0]["SOTIENKM"].ToString()) != 0)
                                                dt.Rows[iFocusRow]["GIABAN"] = double.Parse(dt.Rows[iFocusRow]["GIABAN"].ToString()) - double.Parse(dt1.Rows[0]["SOTIENKM"].ToString());
                                            else
                                                dt.Rows[iFocusRow]["GIABAN"] = double.Parse(dt.Rows[iFocusRow]["GIABAN"].ToString()) - (double.Parse(dt.Rows[iFocusRow]["GIABAN"].ToString()) * double.Parse(dt1.Rows[0]["TYLEKM"].ToString()) / 100);
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                #endregion

                DataView dv = dt.DefaultView;
                dv.Sort = "MACB ASC, TEM ASC";
                dt = dv.ToTable();
                

                gr_SelectProduct.DataSource = dt;
                if (gv_SelectProduct.RowCount > 0)
                {
                    TotalMoney();
                }
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = iFocusRow;

                if (sButtonMonThem == false)
                {
                    if (hanghoa.IS_COMBO == false)
                    {
                        sTT += 1;
                    }
                    if (sINTEM == true)
                    {
                        sTEM += 1;
                    }
                    if (CongCombo==false)
                    {
                        sMaCB += 1;
                    }
                    
                }
            }
            catch (Exception ex)
            {
                //XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK);
            }
        }

        private DataTable GetProductInCombo(string maHangHoa)
        {
            string sSQL = "";
            sSQL += "Select cb.MA_COMBO, hh.MA_HANGHOA, hh.TEN_HANGHOA, cb.SOLUONG,HH.MA_BEP, ISNULL(HH.MONTHEM,0) AS MONTHEM, ISNULL(HH.INTEM,0) AS INTEM" + "\n";
            sSQL += "From COMBO cb inner join HANGHOA hh on cb.MA_HANGHOA = hh.MA_HANGHOA" + "\n";
            sSQL += "Where MA_COMBO = '" + maHangHoa + "'";
            var dtCombo = clsMain.ReturnDataTable(sSQL);
            return dtCombo;
        }

        private DataTable DanhSachSpKM(string maHangHoa)
        {
            string sSQL = "";
            sSQL += "SELECT MA_NHOMHANG" + "\n";
            sSQL += "FROM HANGHOA" + "\n";
            sSQL += "WHERE MA_HANGHOA=" + clsMain.SQLString(maHangHoa);
            DataTable dtNH = clsMain.ReturnDataTable(sSQL);

            sSQL = "";
            sSQL += "Select DISTINCT A.MAKHUYENMAI, MALOAICTKM, THUTUUUTIEN" + "\n";
            sSQL += "From DM_KHUYENMAI A,CT_KHUYENMAI B" + "\n";
            sSQL += "Where A.MAKHUYENMAI=B.MAKHUYENMAI" + "\n";
            sSQL += "AND (CONVERT(DATE,TUNGAY,103) < CONVERT(DATE," + clsMain.SQLString(String.Format("{0:yyyyMMdd}", DateTime.Now)) + ") \n";
            sSQL += "AND CONVERT(DATE,DENNGAY,103) > CONVERT(DATE," + clsMain.SQLString(String.Format("{0:yyyyMMdd}", DateTime.Now)) + ") \n";
            sSQL += "OR (convert(datetime,THOIGIANBATDAU,108) < " + clsMain.SQLString(String.Format("{0:HH:mm:ss}", DateTime.Now)) + "\n";
            sSQL += "AND convert(DATE, TUNGAY, 103) = " + clsMain.SQLString(String.Format("{0:yyyyMMdd}", DateTime.Now)) + ") \n";
            sSQL += "OR (convert(datetime,THOIGIANKETTHUC,108) > " + clsMain.SQLString(String.Format("{0:HH:mm:ss}", DateTime.Now)) + "\n";
            sSQL += "AND convert(DATE, DENNGAY, 103) = " + clsMain.SQLString(String.Format("{0:yyyyMMdd}", DateTime.Now)) + ")) \n";
            sSQL += "AND(HANGMUA=" + clsMain.SQLString(maHangHoa) + "\n";
            sSQL += "OR NHOMHANG_HANGHOA = " + clsMain.SQLString(dtNH.Rows[0]["MA_NHOMHANG"].ToString()) + "\n";
            sSQL += "OR NHOMHANG_HANGHOA = " + clsMain.SQLString(maHangHoa) + ")";
            var dtKM = clsMain.ReturnDataTable(sSQL);
            return dtKM;
        }

        DataTable checkVAT = new DataTable();
        private void TotalMoney()
        {
            if (checkVAT.Rows.Count <= 0)
            {
                string sSQL = "Select GIATRI From CAUHINH Where TEN='GIABANTRUOCTHUE' ";
                checkVAT = clsMain.ReturnDataTable(sSQL);
            }
            bool kt = false;
            if (checkVAT.Rows[0]["GIATRI"].ToString() == "1")
                kt = true;
            double total = 0;
            try
            {
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    double thanhTien = 0, vat = 0;
                    double sl = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                    double gia = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                    double ck = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["CHIETKHAU"]).ToString());
                    thanhTien = (sl * gia) - ((sl * gia) * ck) / 100;
                    if (kt == true)
                    {
                        vat = (double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["THUE"]).ToString()) / 100) * thanhTien;
                    }
                    total += thanhTien + vat;
                    gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"], thanhTien + vat);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            lbTotal.Text = string.Format("{0:#,###0}", total);
        }

        private void txtID_Click(object sender, EventArgs e)
        {
            txtID.SelectAll();
        }

        private void txtID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(txtID.Text))
                {
                    txtID.Focus();
                    return;
                }
                else
                {
                    if (txtID.SelectedText != "")
                    {
                        string smahanghoa = txtID.EditValue.ToString();
                        string stenhanghoa = txtID.Text.ToString();
                        lbNameMon.Text = " >   " + stenhanghoa;
                        GetDishGoToBuy(smahanghoa, maKV);
                        txtID.Text = "";
                        txtID.Focus();
                    }
                    else
                    {
                        string sSQL = "";
                        sSQL += "Exec SP_Select_Hanghoa_Mavach " + clsMain.SQLString(txtID.Text.Replace("*", ""));
                        DataTable dtMon = clsMain.ReturnDataTable(sSQL);
                        if (dtMon.Rows.Count > 0)
                        {
                            lbNameMon.Text = " >   " + dtMon.Rows[0]["TEN_HANGHOA"].ToString();
                            GetDishGoToBuy(dtMon.Rows[0]["MA_HANGHOA"].ToString(), maKV);
                            txtID.Text = "";
                            txtID.Focus();
                        }
                        else
                        {
                            txtID.Text = "";
                            txtID.Focus();
                        }
                    }
                }
            }
        }

        private void btn_DowLast_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                iFocusRow = gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.RowCount - 1;
                btn_UpFirst.Enabled = true;
                btn_up.Enabled = true;
                btn_Dow.Enabled = false;
                btn_DowLast.Enabled = false;
                iFocusRow = gv_SelectProduct.FocusedRowHandle;
                lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
            }
            catch
            {
            }
        }

        private void btn_Dow_Click(object sender, EventArgs e)
        {
            try
            {
                if (gv_SelectProduct.FocusedRowHandle < gv_SelectProduct.RowCount)
                {
                    gv_SelectProduct.Focus();
                    gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.FocusedRowHandle + 1;
                    btn_up.Enabled = true;
                    btn_UpFirst.Enabled = true;
                    lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
                    if (gv_SelectProduct.FocusedRowHandle == gv_SelectProduct.RowCount - 1)
                    {
                        btn_Dow.Enabled = false;
                        btn_DowLast.Enabled = false;
                    }
                    iFocusRow = gv_SelectProduct.FocusedRowHandle;
                }
            }
            catch
            {

            }
        }

        private void btn_up_Click(object sender, EventArgs e)
        {
            try
            {

                if (gv_SelectProduct.FocusedRowHandle > 0)
                {
                    gv_SelectProduct.Focus();
                    iFocusRow = gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.FocusedRowHandle - 1;
                    btn_Dow.Enabled = true;
                    btn_DowLast.Enabled = true;
                    lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
                    if (gv_SelectProduct.FocusedRowHandle == 0)
                    {
                        btn_up.Enabled = false;
                        btn_UpFirst.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_UpFirst_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                iFocusRow = gv_SelectProduct.FocusedRowHandle = 0;
                btn_UpFirst.Enabled = false;
                btn_up.Enabled = false;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
                iFocusRow = gv_SelectProduct.FocusedRowHandle;
                lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
            }
            catch
            {
            }
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            try
            {
                bool sMONTHEM = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MONTHEM"]).ToString());
                string sMaHangHoaChonTrenLuoi = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                DataTable dt_combo = GetProductInCombo(sMaHangHoaChonTrenLuoi);
                bool sTrangThai = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString());
                bool sINTEM = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["INTEM"]).ToString());
                string sMaCP = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MACP"]).ToString();
                //if (KiemTraCombo(sMaHangHoaChonTrenLuoi) || sMONTHEM || sINTEM)
                if (KiemTraCombo(sMaHangHoaChonTrenLuoi))
                {
                    return;
                }
                else
                {
                    if (!sTrangThai & sMaCP == "C")
                    {
                        if (!Flag_soluong)
                        {
                            if (lbSoluong.Text.Length < 3)
                                lbSoluong.Text += btn0.Text;
                        }
                        int i = gv_SelectProduct.FocusedRowHandle;
                        int soluong = int.Parse(lbSoluong.Text);
                        int soluong_lucdau = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                        int iMaCB = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MACB"]).ToString());
                        if (dt_combo.Rows.Count > 0)
                        {
                            dt.Rows[i]["SOLUONG"] = soluong;
                            for (int j = 0; j < dt_combo.Rows.Count; j++)
                            {
                                int sl = int.Parse(dt_combo.Rows[j]["SOLUONG"].ToString());
                                dt.Rows[i + j + 1]["SOLUONG"] = soluong * sl;
                            }
                        }
                        else
                        {
                            gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"], soluong);
                            foreach (DataRow dr in dt.Rows)
                            {
                                if (int.Parse(dr["MACB"].ToString()) == iMaCB & dr["MACP"].ToString() == "P")
                                {
                                    dr["SOLUONG"] = soluong * (int.Parse(dr["SOLUONG"].ToString()) / soluong_lucdau);
                                }
                            }
                        }
                        TotalMoney();
                    }
                }
            }
            catch
            {
            }
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            try
            {
                string sMaHangHoaChonTrenLuoi = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                DataTable dt_combo = GetProductInCombo(sMaHangHoaChonTrenLuoi);
                bool sTrangThai = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString());
                bool sMONTHEM = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MONTHEM"]).ToString());
                bool sINTEM = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["INTEM"]).ToString());
                string sMaCP = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MACP"]).ToString();
                //if (KiemTraCombo(sMaHangHoaChonTrenLuoi) || sMONTHEM || sINTEM)
                if (KiemTraCombo(sMaHangHoaChonTrenLuoi))
                {
                    return;
                }
                else
                {

                    SimpleButton btn = (SimpleButton)sender;
                    if (!sTrangThai & sMaCP == "C")
                    {
                        if (Flag_soluong)
                        {
                            lbSoluong.Text = btn.Text;
                            Flag_soluong = false;
                        }
                        else
                        {
                            if (lbSoluong.Text.Length < 3)
                                lbSoluong.Text += btn.Text;
                        }

                        int i = gv_SelectProduct.FocusedRowHandle;
                        int soluong = int.Parse(lbSoluong.Text);
                        int soluong_lucdau = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                        int iMaCB = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MACB"]).ToString());
                        if (dt_combo.Rows.Count > 0)
                        {
                            dt.Rows[i]["SOLUONG"] = soluong;
                            for (int j = 0; j < dt_combo.Rows.Count; j++)
                            {
                                int sl = int.Parse(dt_combo.Rows[j]["SOLUONG"].ToString());
                                dt.Rows[i + j + 1]["SOLUONG"] = soluong * sl;
                            }
                        }
                        else
                        {
                            gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"], soluong);
                            foreach (DataRow dr in dt.Rows)
                            {
                                if (int.Parse(dr["MACB"].ToString()) == iMaCB & dr["MACP"].ToString() == "P")
                                {
                                    dr["SOLUONG"] = soluong * (int.Parse(dr["SOLUONG"].ToString()) / soluong_lucdau);
                                }
                            }
                        }
                        TotalMoney();

                    }

                }
            }
            catch
            {
            }
        }

        private void btn_tru_Click(object sender, EventArgs e)
        {
            try
            {
                string sMaHangHoaChonTrenLuoi = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                DataTable dt_combo = GetProductInCombo(sMaHangHoaChonTrenLuoi);
                bool sTrangThai = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString());
                bool sMONTHEM = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MONTHEM"]).ToString());
                bool sINTEM = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["INTEM"]).ToString());
                string sMaCP = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MACP"]).ToString();
                //if (KiemTraCombo(sMaHangHoaChonTrenLuoi) || sMONTHEM || sINTEM)
                if (KiemTraCombo(sMaHangHoaChonTrenLuoi))
                {
                    return;
                }
                else
                {
                    if (!sTrangThai & sMaCP == "C")
                    {
                        int i = gv_SelectProduct.FocusedRowHandle;
                        int soluong = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                        int iMaCB = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MACB"]).ToString());
                        if (soluong > 1)
                        {
                            Flag_soluong = true;
                            if (dt_combo.Rows.Count > 0)
                            {
                                dt.Rows[i]["SOLUONG"] = soluong - 1;
                                for (int j = 0; j < dt_combo.Rows.Count; j++)
                                {
                                    int sl_trenluoi = int.Parse(dt.Rows[i + j + 1]["SOLUONG"].ToString());
                                    int sl_combo = int.Parse(dt_combo.Rows[j]["SOLUONG"].ToString());
                                    dt.Rows[i + j + 1]["SOLUONG"] = sl_trenluoi - sl_combo;
                                }
                            }
                            else
                            {
                                gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"], soluong - 1);
                                foreach (DataRow dr in dt.Rows)
                                {
                                    if (int.Parse(dr["MACB"].ToString()) == iMaCB & dr["MACP"].ToString() == "P")
                                    {
                                        dr["SOLUONG"] = int.Parse(dr["SOLUONG"].ToString()) - (int.Parse(dr["SOLUONG"].ToString()) / soluong);
                                    }
                                }
                            }
                            lbSoluong.Text = (soluong - 1).ToString();
                        }
                        TotalMoney();
                    }
                }
            }
            catch
            {

            }
        }

        private void btn_cong_Click(object sender, EventArgs e)
        {
            try
            {
                string sMaHangHoaChonTrenLuoi = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                DataTable dt_combo = GetProductInCombo(sMaHangHoaChonTrenLuoi);
                bool sTrangThai = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString());
                bool sMONTHEM = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MONTHEM"]).ToString());
                bool sINTEM = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["INTEM"]).ToString());
                string sMaCP = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MACP"]).ToString();
                //if (KiemTraCombo(sMaHangHoaChonTrenLuoi) || sMONTHEM || sINTEM)
                if (KiemTraCombo(sMaHangHoaChonTrenLuoi))
                {
                    return;
                }
                else
                {
                    if (!sTrangThai & sMaCP == "C")
                    {
                        int i = gv_SelectProduct.FocusedRowHandle;
                        int soluong = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                        int iMaCB = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MACB"]).ToString());
                        if (soluong < 999)
                        {
                            Flag_soluong = true;
                            if (dt_combo.Rows.Count > 0)
                            {
                                dt.Rows[i]["SOLUONG"] = soluong + 1;
                                for (int j = 0; j < dt_combo.Rows.Count; j++)
                                {
                                    int sl_trenluoi = int.Parse(dt.Rows[i + j + 1]["SOLUONG"].ToString());
                                    int sl_combo = int.Parse(dt_combo.Rows[j]["SOLUONG"].ToString());
                                    dt.Rows[i + j + 1]["SOLUONG"] = sl_trenluoi + sl_combo;
                                }
                            }
                            else
                            {
                                gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"], soluong + 1);
                                foreach (DataRow dr in dt.Rows)
                                {
                                    if (int.Parse(dr["MACB"].ToString()) == iMaCB & dr["MACP"].ToString() == "P")
                                    {
                                        dr["SOLUONG"] = int.Parse(dr["SOLUONG"].ToString()) + (int.Parse(dr["SOLUONG"].ToString()) / soluong);
                                    }
                                }
                            }
                            lbSoluong.Text = (soluong + 1).ToString();
                            TotalMoney();
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            pnBtnDeleteGv.Visible = false;
            btnComboOption.Visible = false;
        }

        private void btn_xoa_Click(object sender, EventArgs e)
        {
            if (!bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
            {
                DataTable dtCombo = GetProductInCombo(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                DataTable dtKM = DanhSachSpKM(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                int MaCB = int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MACB"]).ToString());
                bool sMONTHEM = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MONTHEM"]).ToString());
                sTEM = int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TEM"]).ToString());
                
                int sSoDongXoa = 0;
                if (dtCombo.Rows.Count > 0 || dtKM.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (int.Parse(dt.Rows[i]["MACB"].ToString()) == MaCB)
                        {
                            sSoDongXoa += 1;
                        }
                    }
                    for (int i = 0; i < sSoDongXoa - 1; i++)
                    {
                        gv_SelectProduct.DeleteRow(gv_SelectProduct.FocusedRowHandle + 1);
                    }
                }
                else
                {
                    if (sMONTHEM == true)
                    {
                        sSoDongXoa = 0;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (int.Parse(dt.Rows[i]["TEM"].ToString()) == sTEM)
                            {
                                sSoDongXoa += 1;
                            }
                        }
                        for (int i = 0; i < sSoDongXoa - 1; i++)
                        {
                            gv_SelectProduct.DeleteRow(gv_SelectProduct.FocusedRowHandle + 1);
                        }
                    }
                }
                gv_SelectProduct.DeleteSelectedRows();
                btnComboOption.Visible = false;
                TotalMoney();
                if (gv_SelectProduct.RowCount <= 0)
                    pnBtnDeleteGv.Visible = false;
                else
                    lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();

                if (dt.Rows.Count > 0)
                {
                    sTEM = LayTemLonNhat() + 1;
                    sMaCB = LayMACBLonNhat() + 1;
                    DanhSoThuTu();
                }
                else
                {
                    sTEM = 1;
                    sTT = 1;
                    sMaCB = 1;
                    SetGiaoDienMacDinh();
                }
            }
        }

        private void btn_XoaTatCa_Click(object sender, EventArgs e)
        {
            dt.Rows.Clear();
            DataTable s = new DataTable();
            s = dt;
            TotalMoney();
            pnBtnDeleteGv.Visible = false;
            btnComboOption.Visible = false;
            sTEM = 1;
            sTT = 1;
            sMaCB = 1;
            SetGiaoDienMacDinh();
        }

        private void gv_SelectProduct_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == SOTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
        }

        private void gv_SelectProduct_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
			try
			{
				if (gv_SelectProduct.FocusedRowHandle >= 0)
				{
					clsKP_Terminal.ShowCustomerDisplayOneLine(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString() + " : " + (int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["GIABAN"]).ToString())).ToString("N0") + " VND");
				}
				else
				{
					clsKP_Terminal.ShowCustomerDisplayOneLine("Welcome to " + cls_ConfigCashier.nameShop);
				}
				XuLyGv();
			}
			catch (Exception)
			{
			}
        }

        private void gv_SelectProduct_RowClick(object sender, RowClickEventArgs e)
        {
            iFocusRow = gv_SelectProduct.FocusedRowHandle;
        }

       private void gv_SelectProduct_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
       {
            maMon = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
            sTenMon = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
            bool sMONTHEM = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MONTHEM"]).ToString());
            string sMACP = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MACP"]).ToString();
            if (sMONTHEM == true && sMACP == "C" || sMONTHEM == true && sMACP == "CBC" || sMONTHEM == false && sMACP == "P")
            {
                sTEM_THEMMON = int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TEM"]).ToString());
                sMaCB_THEMMON = int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MACB"]).ToString());
            }
            lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
            XuLyGv();
            pnBtnDeleteGv.Visible = true;
            Flag_soluong = true;
            if (bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
            {
                btnComboOption.Visible = true;
            }
            else
            {
                btnComboOption.Visible = false;
            }

            string yeucau = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["GHICHU"]).ToString();
            if (yeucau != "-")
                txtYeuCauThem.Text = yeucau;
            else
                txtYeuCauThem.Text = "";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            timer1.Enabled = false;
            string hour, minute, second;
            second = DateTime.Now.Second.ToString();
            hour = DateTime.Now.Hour.ToString();
            minute = DateTime.Now.Minute.ToString();
            if (second.Length < 2)
                lbgiay.Text = "0" + second;
            else
                lbgiay.Text = second;
            if (minute.Length < 2)
                lbGio.Text = hour + ":0" + minute;
            else
                lbGio.Text = hour + ":" + minute;
            if (MyGetData != null)
            {
                MyGetData(dt, contrDelegate, iFocusRow);
            }
            timer1.Enabled = true;
        }

        private void btn_logout_Click(object sender, EventArgs e)
        {
            contrDelegate = 1;
            if (MyGetData != null)
            {
                MyGetData(dt, contrDelegate, 0);
            }
            while (gv_SelectProduct.RowCount > 0)
            {
                gv_SelectProduct.DeleteRow(0);
            }
            this.Close();
        }

        private void btnKetKa_Click(object sender, EventArgs e)
        {
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }
            Frm_Ketca frm = new Frm_Ketca();
            frm.Width = this.Width;
            frm.Height = this.Height;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnMoKet_Click(object sender, EventArgs e)
        {
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }
            KP_Terminal.clsKP_Terminal.OpenCashDrawer(false);
        }

        private void btnInVeOnline_Click(object sender, EventArgs e)
        {
            Frm_InBillMuaTruoc frm = new Frm_InBillMuaTruoc();
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnGiaBan2_Click(object sender, EventArgs e)
        {
            //if (gv_SelectProduct.RowCount <= 0 || maMon == null || maMon == "")
            //    return;
            //if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            //{
            //    bool check;
            //    Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
            //    fr.ShowDialog();
            //    check = fr.kt;
            //    fr.Dispose();
            //    if (check == false)
            //        return;
            //}
            ////Kiểm tra nếu đang dùng bản giá theo khu hay cửa hàng thì return
            //string sSQL = "select GIATRI from CAUHINH where TEN='APDUNGBANGGIATHEOKHUVUC'";
            //DataTable dtcheck = clsMain.ReturnDataTable(sSQL);
            //if (dtcheck.Rows.Count > 0)
            //{
            //    if (dtcheck.Rows[0]["GIATRI"].ToString() != "0")
            //    {
            //        MessageBox.Show("Đang dùng chương trình tính giá theo khu hay cửa hàng. Không áp dụng được giá bán 2", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        return;
            //    }
            //}

            ////Kiểm tra hàng hóa là thời giá hay ko,nếu đúng thì return
            //sSQL = "Select ISNULL(SUAGIA,0) AS SUAGIA From HANGHOA Where MA_HANGHOA = " + clsMain.SQLString(maMon);
            //dtcheck = clsMain.ReturnDataTable(sSQL);
            //if (dtcheck.Rows.Count > 0)
            //{
            //    if (bool.Parse(dtcheck.Rows[0]["SUAGIA"].ToString()))
            //    {
            //        MessageBox.Show(_daylahhtheothoigia, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        return;
            //    }
            //}

            ////lây giá bán 1,2
            //sSQL = "select  ISNULL(GIABAN1,0) as GIABAN1,ISNULL(GIABAN2,0) as GIABAN2 from HANGHOA where MA_HANGHOA = " + clsMain.SQLString(maMon);
            //DataTable dtgiaban = clsMain.ReturnDataTable(sSQL);
            //if (dtgiaban.Rows.Count > 0)
            //{
            //    for (int i = 0; i < gv_SelectProduct.RowCount; i++)
            //    {
            //        if (gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString() == maMon)
            //        {
            //            if (gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString() == dtgiaban.Rows[0]["GIABAN1"].ToString())
            //            {
            //                gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"], dtgiaban.Rows[0]["GIABAN2"].ToString());
            //            }
            //            else
            //            {
            //                gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"], dtgiaban.Rows[0]["GIABAN1"].ToString());
            //            }

            //        }
            //    }
            //    if (gv_SelectProduct.RowCount > 0)
            //    {
            //        TotalMoney();
            //    }
            //}

            //30/11 nut nay ko sai chuyen thanh nut tang 
            if (gv_SelectProduct.RowCount <= 0 || maMon == null || maMon == "")
                return;
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }

            for (int i = 0; i < gv_SelectProduct.RowCount; i++)
            {
                if (gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString() == maMon && i == gv_SelectProduct.FocusedRowHandle)
                {
                    gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"], 0);
                }
            }
            if (gv_SelectProduct.RowCount > 0)
            {
                TotalMoney();
            }
        }

        private void btnSuaGia_Click(object sender, EventArgs e)
        {
            if (gv_SelectProduct.RowCount <= 0 || maMon == null || maMon == "")
                return;

            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }

            decimal Giamoi = 0;
            bool thoat = false;
            Frm_Chontheothoigia frm = new Frm_Chontheothoigia();
            frm.ShowDialog();
            Giamoi = frm.tmp_Thoigia;
            thoat = frm.bThoat;
            frm.Dispose();

            if (thoat)
            {
                return;
            }

            for (int i = 0; i < gv_SelectProduct.RowCount; i++)
            {
                if (gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString() == maMon)
                {
                    gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"], Giamoi.ToString());
                }
            }
            if (gv_SelectProduct.RowCount > 0)
            {
                TotalMoney();
            }
        }

        private void btnChietkhau_Click(object sender, EventArgs e)
        {
            if (gv_SelectProduct.RowCount <= 0 || maMon == null || maMon == "")
                return;

            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }

            decimal Giamoi = 0;
            Frm_Nhapchietkhau frm = new Frm_Nhapchietkhau();
            frm.ShowDialog();
            Giamoi = frm.tmp_Thoigia;
            frm.Dispose();
            if (Giamoi <= 100 && Giamoi >= 0)
            {
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    int sTem = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEM"]).ToString());
                    if (gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString() == maMon & sTem == sTEM_THEMMON)
                    {
                        gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["CHIETKHAU"], Giamoi.ToString());
                    }
                }
                if (gv_SelectProduct.RowCount > 0)
                {
                    TotalMoney();
                }
            }
        }

        private void btn_Payment_Click(object sender, EventArgs e)
        {
            try
            {
                if (gv_SelectProduct.RowCount <= 0)
                {
                    return;
                }
                btnComboOption.Visible = false;
                pnBtnDeleteGv.Visible = false;
                clsKP_Terminal.ShowCustomerDisplayOneLine("Tổng cộng" + " : " + lbTotal.Text + " VND");
                //Lấy mã bàn
                string maBan = "";
                //Lấy tên bàn
                string tenBan = "";
                //Lấy mã hóa don
                string idBill = "";
                //Lấy tiền đặt cọc
                decimal Tiendatcoc = 0;
                //Lấy danh sách hàng hóa
                DataTable _dt = (DataTable)gr_SelectProduct.DataSource;
                //gởi form thanh toán
                bool _check;
                Frm_Payment_Nhahang frm = new Frm_Payment_Nhahang(maBan, idBill, _dt, Tiendatcoc, tenBan);
                frm.ShowDialog();
                _check = frm.bHoanTacThanhToan;
                frm.Dispose();

                if (_check == true) //payment successful
                {
                    sTT = 1;
                    sTEM = 1;
                    sMaCB = 1;
                    dt.Rows.Clear();
                    lbTotal.Text = "0";
                    KP_Terminal.clsKP_Terminal.OpenCashDrawer(true);
                    txtID.Focus();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK);
            }
        }

        private void btn_InLaiHoaDon_Click(object sender, EventArgs e)
        {
            pnBtnDeleteGv.Visible = false;
            btnComboOption.Visible = false;
            if (exitReprint == true)
            {
                SetDefautlBackgroud(btn_InLaiHoaDon);
                btnThoatReprint_Click(null, null);
            }
            else
            {
                if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
                {
                    bool check;
                    Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                    fr.ShowDialog();
                    check = fr.kt;
                    fr.Dispose();
                    if (check == false)
                        return;
                }

                string sSQL = "exec SP_DANHSACHHOADON";
                DataTable dts = clsMain.ReturnDataTable(sSQL);
                gr_billPaymented.DataSource = dts;

                SetBackgroudOnClick(btn_InLaiHoaDon);
                panelControl1.Visible = false;
                IN.Visible = true;
                HUY.Visible = false;
                LAYLAI.Visible = false;
                pnInlaihoadon.BringToFront();

                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    if (btn.Name != "btn_InLaiHoaDon")
                    {
                        btn.Enabled = false;
                    }
                }
                exitReprint = true;
            }
        }

        private void btn_HuyHoaDon_Click(object sender, EventArgs e)
        {
            pnBtnDeleteGv.Visible = false;
            btnComboOption.Visible = false;

            if (exitReprint == true)
            {
                SetDefautlBackgroud(btn_HuyHoaDon);
                btnThoatReprint_Click(null, null);
            }
            else
            {
                if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
                {
                    bool check;
                    Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                    fr.ShowDialog();
                    check = fr.kt;
                    fr.Dispose();
                    if (check == false)
                        return;
                }

                string sSQL = "exec SP_DANHSACHHOADON";
                DataTable dts = clsMain.ReturnDataTable(sSQL);
                gr_billPaymented.DataSource = dts;

                SetBackgroudOnClick(btn_HuyHoaDon);
                panelControl1.Visible = true;
                HUY.Visible = true;
                IN.Visible = false;
                LAYLAI.Visible = false;
                pnInlaihoadon.BringToFront();

                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    if (btn.Name != "btn_HuyHoaDon")
                    {
                        btn.Enabled = false;
                    }
                }
                exitReprint = true;
            }
        }

        public bool exitReprint = false;
        private void btnThoatReprint_Click(object sender, EventArgs e)
        {
            pnInlaihoadon.SendToBack();
            foreach (SimpleButton btn in pnThuNgan.Controls)
            {
                btn.Enabled = true;
            }
            exitReprint = false;
            dt.Clear();
            DataTable dtTemp = (DataTable)gr_billPaymented.DataSource;
            if (dtTemp != null)
            {
                dtTemp.Clear();
                gr_billPaymented.DataSource = dtTemp;
            }
            dtTemp = (DataTable)gr_SelectProduct.DataSource;
            if (dtTemp != null)
            {
                dtTemp.Clear();
                gr_SelectProduct.DataSource = dtTemp;
            }
            idBill = "";
            lbPhuThu.Text = "0";
            lbGiamGia.Text = "0";
            lbTotal.Text = "0";
        }

        private void btn_upbill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = gvBillPaymented.FocusedRowHandle - 1;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
                if (gvBillPaymented.FocusedRowHandle == 0)
                {
                    btn_up.Enabled = false;
                    btn_UpFirst.Enabled = false;
                }
            }
            catch
            {
            }

        }

        private void btn_UpTopBill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = 0;
                btn_UpFirst.Enabled = false;
                btn_up.Enabled = false;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnDowBill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = gvBillPaymented.FocusedRowHandle + 1;
                btn_up.Enabled = true;
                btn_UpFirst.Enabled = true;
                if (gvBillPaymented.FocusedRowHandle == gvBillPaymented.RowCount - 1)
                {
                    btn_Dow.Enabled = false;
                    btn_DowLast.Enabled = false;
                }
            }
            catch
            {

            }
        }

        private void btnDowLastBill_Click(object sender, EventArgs e)
        {
            try
            {

                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = gvBillPaymented.RowCount - 1;
                btn_UpFirst.Enabled = true;
                btn_up.Enabled = true;
                btn_Dow.Enabled = false;
                btn_DowLast.Enabled = false;
            }
            catch
            {
            }
        }

        private void gvBillPaymented_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == SSTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
                lbTongSoRow.Text = gvBillPaymented.RowCount.ToString("00");
                labelControl45.Text = (gvBillPaymented.FocusedRowHandle + 1).ToString("00");
            }
        }

        public string idBill = "";
        private void btnHuy_Click(object sender, EventArgs e)
        {
            idBill = txtMaHoaDon.Text.ToString();

            if (idBill == "")
            {
                MessageBox.Show("Vui lòng nhập mã hóa đơn cần hủy", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            String sSQL = "";
            sSQL += "SELECT MA_HOADON FROM HOADON WHERE ISPAYMENT<>2 and  MA_HOADON = " + clsMain.SQLString(idBill) + "\n";

            if (clsMain.ReturnDataTable(sSQL).Rows.Count > 0)
            {
                if (XtraMessageBox.Show("Xác nhận bạn muốn hủy hóa đơn", _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;

                sSQL = "";
                sSQL += "Update HOADON Set " + "\n";
                sSQL += "NHANVIEN_HUYBILL=" + clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                sSQL += "ISPAYMENT=2 " + "\n";
                sSQL += "Where MA_HOADON=" + clsMain.SQLString(idBill);
                clsMain.ExecuteSQL(sSQL);
                //Trừ kho
                sSQL = String.Format("Exec SP_TRUKHO_DINHLUONG @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 0);
                clsMain.ExecuteSQL(sSQL);
                sSQL = String.Format("Exec SP_TRUKHO_HOADON @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 0);
                clsMain.ExecuteSQL(sSQL);
                //Công nợ
                sSQL = "EXEC SP_CN_HUYHOADON '" + idBill + "'";
                clsMain.ExecuteSQL(sSQL);
                //KHTT
                sSQL = "Select ISNULL(MA_KHACHHANG,'') AS MA_KHACHHANG,TONGTIEN,GIAMGIA From HOADON Where MA_HOADON =" + clsMain.SQLString(idBill);
                DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                if (dt1.Rows.Count > 0)
                {
                    if (dt1.Rows[0]["MA_KHACHHANG"].ToString() != "")
                    {
                        DataTable dtKH = cls_KHTT.ReturnCauHinhKHTT();
                        if (dtKH.Rows[0]["GIATRI"].ToString() == "1")
                        {
                            int tem = 100 - int.Parse(dt1.Rows[0]["GIAMGIA"].ToString());
                            decimal tongtien = decimal.Parse(dt1.Rows[0]["TONGTIEN"].ToString()) / tem * 100;
                            cls_KHTT.TruDiemKhachHangThanThiet(dt1.Rows[0]["MA_KHACHHANG"].ToString(), tongtien);
                        }
                        else
                        {
                            cls_KHTT.TruDiemKhachHangThanThiet(dt1.Rows[0]["MA_KHACHHANG"].ToString(), decimal.Parse(dt1.Rows[0]["TONGTIEN"].ToString()));
                        }
                    }
                }
                //In bill
                sSQL = "";
                sSQL += "Select ROW_NUMBER() over (order by (select 1)) as STT,'' as TenNV ,ISNULL(cthd.CHIETKHAU,0) as CHIETKHAU,";
                sSQL += "cthd.GIABAN, (cthd.SOLUONG * cthd.GIABAN) - (cthd.SOLUONG * cthd.GIABAN) * ISNULL(cthd.CHIETKHAU,0) / 100 as Thanh_Tien, hd.NGAYTAO, ISNULL(hd.GIORA,hd.NGAYTAO) as ngayHD,hh.TEN_HANGHOA, cthd.SOLUONG,";
                sSQL += "hd.PHIPHUCVU as Phiphucvu,hd.MA_HOADON,TEN_QUAY as MA_QUAY, ISNULL(hd.TIENDATCOC,0) as TIENDATCOC,";
                sSQL += "case when ISNULL(hd.NHANVIEN_RABILL,'')='' Then (hd.MANHANVIEN) Else (hd.NHANVIEN_RABILL) End as MANHANVIEN,";
                sSQL += "hd.GIAMGIA as Giam_Gia1, hd.TONGTIEN as Tong_Cong, hd.TIEN_KHACHTRA, hd.TIEN_TRAKHACH, hd.TONGTIEN, N'PHIẾU THANH TOÁN \n (HỦY)' as Ten_HoaDon" + "\n";
                sSQL += "From HANGHOA hh inner join CT_HOADON cthd on hh.MA_HANGHOA = cthd.MA_HANGHOA inner join HOADON hd on cthd.MA_HOADON = hd.MA_HOADON" + "\n";
                sSQL += "inner join QUAY q on hd.MA_QUAY = q.MA_QUAY" + "\n";
                sSQL += "Where hd.MA_HOADON ='" + idBill + "'";
                DataTable dtReport = clsMain.ReturnDataTable(sSQL);
                dtReport.Columns.Add("Giam_Gia", typeof(String));

                string TenNV = "";
                if (dtReport.Rows.Count > 0)
                {
                    sSQL = "Select TENNHANVIEN From DM_NHANVIEN Where MANHANVIEN=" + clsMain.SQLString(dtReport.Rows[0]["MANHANVIEN"].ToString());
                    DataTable dtTenNV = clsMain.ReturnDataTable(sSQL);
                    if (dtTenNV.Rows.Count > 0)
                        TenNV = dtTenNV.Rows[0]["TENNHANVIEN"].ToString();
                }

                sSQL = "Select TEN_BAN From CT_MOBAN ct,BAN b Where ct.MA_BAN=b.MA_BAN And ct.MA_HOADON=" + clsMain.SQLString(idBill);
                DataTable dtTenban = clsMain.ReturnDataTable(sSQL);
                if (dtTenban.Rows.Count > 0)
                {
                    dtReport.Columns.Add("TEN_BAN", typeof(String));

                    int n = dtReport.Rows.Count;
                    for (int i = 0; i < n; i++)
                    {
                        dtReport.Rows[i]["TEN_BAN"] = dtTenban.Rows[0]["TEN_BAN"].ToString();
                        dtReport.Rows[i]["TenNV"] = TenNV;
                        if (double.Parse(dtReport.Rows[i]["Giam_Gia1"].ToString()) > 100)
                            dtReport.Rows[i]["Giam_Gia"] = string.Format("{0:#,###0}", dtReport.Rows[i]["Giam_Gia1"]);
                        else
                            dtReport.Rows[i]["Giam_Gia"] = dtReport.Rows[i]["Giam_Gia1"].ToString() + "%";
                    }

                    Frm_Report1 frm = new Frm_Report1();
                    if (cls_KP_RES.Mode == 3)
                        frm.ReportName = "rpt_inhoadon_Bill_ban_1";
                    else if (cls_KP_RES.Mode == 4)
                        frm.ReportName = "rpt_inhoadon_Bill_ban_pho";
                    else
                        frm.ReportName = "rpt_inhoadon_Bill_ban";

                    frm.DataSource = dtReport;
                    frm.WindowState = FormWindowState.Minimized;
                    frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                    frm.IsPrint = true;
                    frm.ShowDialog();
                    frm.Dispose();
                    dt.Rows.Clear();
                    lbTotal.Text = "0";
                }
                else
                {
                    int n = dtReport.Rows.Count;
                    for (int i = 0; i < n; i++)
                    {
                        dtReport.Rows[i]["TenNV"] = TenNV;
                        if (double.Parse(dtReport.Rows[i]["Giam_Gia1"].ToString()) > 100)
                            dtReport.Rows[i]["Giam_Gia"] = string.Format("{0:#,###0}", dtReport.Rows[i]["Giam_Gia1"]);
                        else
                            dtReport.Rows[i]["Giam_Gia"] = dtReport.Rows[i]["Giam_Gia1"].ToString() + "%";
                    }

                    Frm_Report1 frm = new Frm_Report1();
                    frm.ReportName = "rpt_inhoadon_Bill";
                    frm.DataSource = dtReport;
                    frm.WindowState = FormWindowState.Minimized;
                    frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                    frm.IsPrint = true;
                    frm.ShowDialog();
                    frm.Dispose();
                    dt.Rows.Clear();
                    lbTotal.Text = "0";
                }
                txtMaHoaDon.Text = "";
                //load luoi hoa don
                sSQL = "exec SP_DANHSACHHOADON";
                DataTable dts = clsMain.ReturnDataTable(sSQL);
                gr_billPaymented.DataSource = dts;
                return;
            }
            else
            {
                MessageBox.Show("Mã hóa đơn không tồn tại", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void gvBillPaymented_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            try
            {
                idBill = gvBillPaymented.GetRowCellValue(gvBillPaymented.FocusedRowHandle, gvBillPaymented.Columns["MA_HOADON"]).ToString(); ;
                GetDishOfTable(idBill);
                GetDistcountABill(idBill);

                string sSQL;
                if (e.Column == IN)
                {
                    sSQL = "";
                    sSQL += "Insert into HOADON_INLAI(MA_HOADON,NHANVIEN_INLAI,GIO_INLAI)" + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(idBill) + ",";
                    sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                    sSQL += "GETDATE())";
                    clsMain.ExecuteSQL(sSQL);

                    sSQL = "";
                    sSQL += "Select ROW_NUMBER() over (order by (select 1)) as STT,'' as TenNV ,ISNULL(cthd.CHIETKHAU,0) as CHIETKHAU,";
                    sSQL += "cthd.GIABAN, (cthd.SOLUONG * cthd.GIABAN) - (cthd.SOLUONG * cthd.GIABAN) * ISNULL(cthd.CHIETKHAU,0) / 100 as Thanh_Tien, hd.NGAYTAO, ISNULL(hd.GIORA,hd.NGAYTAO) as ngayHD,hh.TEN_HANGHOA, cthd.SOLUONG,";
                    sSQL += "hd.PHIPHUCVU as Phiphucvu,hd.MA_HOADON,TEN_QUAY as MA_QUAY, ISNULL(hd.TIENDATCOC,0) as TIENDATCOC,";
                    sSQL += "case when ISNULL(hd.NHANVIEN_RABILL,'')='' Then (hd.MANHANVIEN) Else (hd.NHANVIEN_RABILL) End as MANHANVIEN,";
                    sSQL += "hd.GIAMGIA as Giam_Gia1, hd.TONGTIEN as Tong_Cong, hd.TIEN_KHACHTRA, hd.TIEN_TRAKHACH, hd.TONGTIEN, N'PHIẾU THANH TOÁN \n (IN LẠI)' as Ten_HoaDon,";
                    sSQL += "ISNULL(REPLACE((SELECT TENNHANVIEN + ',' FROM DM_NHANVIEN WHERE MANHANVIEN IN(SELECT data  FROM  [dbo].[SplitStringToTable](cthd.NHANVIEN, ',')) for xml path('')) + ',',',,',''),'') AS TenNhanVien,ISNULL(hd.GHICHU1,'') AS GhiChu,";
                    sSQL += "ISNULL(cthd.TEM,0) AS TEM, ISNULL(cthd.MACP,'C') AS MACP, ISNULL(cthd.Number,0) AS Number" + "\n";
                    sSQL += "From HANGHOA hh inner join CT_HOADON cthd on hh.MA_HANGHOA = cthd.MA_HANGHOA inner join HOADON hd on cthd.MA_HOADON = hd.MA_HOADON" + "\n";
                    sSQL += "inner join QUAY q on hd.MA_QUAY = q.MA_QUAY LEFT join DM_NHANVIEN NV on ISNULL(cthd.NHANVIEN,'') = NV.MANHANVIEN" + "\n";
                    sSQL += "Where hd.MA_HOADON ='" + idBill + "'";
                    DataTable dtReport = clsMain.ReturnDataTable(sSQL);
                    for (int i = 0; i < dtReport.Rows.Count; i++)
                    {
                        if (dtReport.Rows[i]["MACP"].ToString()=="P")
                        {
                            dtReport.Rows[i]["TEN_HANGHOA"] = Kytu_Monthem + dtReport.Rows[i]["TEN_HANGHOA"].ToString();
                        }
                        else if (dtReport.Rows[i]["MACP"].ToString() == "CBC")
                        {
                            dtReport.Rows[i]["TEN_HANGHOA"] = Kytu_Combo + dtReport.Rows[i]["TEN_HANGHOA"].ToString();
                        }
                    }
                    dtReport.Columns.Add("Giam_Gia", typeof(String));

                    string TenNV = "";
                    if (dtReport.Rows.Count > 0)
                    {
                        sSQL = "Select TENNHANVIEN From DM_NHANVIEN Where MANHANVIEN=" + clsMain.SQLString(dtReport.Rows[0]["MANHANVIEN"].ToString());
                        DataTable dtTenNV = clsMain.ReturnDataTable(sSQL);
                        if (dtTenNV.Rows.Count > 0)
                            TenNV = dtTenNV.Rows[0]["TENNHANVIEN"].ToString();
                    }

                    sSQL = "Select TEN_BAN From CT_MOBAN ct,BAN b Where ct.MA_BAN=b.MA_BAN And ct.MA_HOADON=" + clsMain.SQLString(idBill);
                    DataTable dtTenban = clsMain.ReturnDataTable(sSQL);
                    if (dtTenban.Rows.Count > 0)
                    {
                        dtReport.Columns.Add("TEN_BAN", typeof(String));

                        int n = dtReport.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            dtReport.Rows[i]["TEN_BAN"] = dtTenban.Rows[0]["TEN_BAN"].ToString();
                            dtReport.Rows[i]["TenNV"] = TenNV;
                            if (double.Parse(dtReport.Rows[i]["Giam_Gia1"].ToString()) > 100)
                                dtReport.Rows[i]["Giam_Gia"] = string.Format("{0:#,###0}", dtReport.Rows[i]["Giam_Gia1"]);
                            else
                                dtReport.Rows[i]["Giam_Gia"] = dtReport.Rows[i]["Giam_Gia1"].ToString() + "%";
                        }

                        Frm_Report1 frm = new Frm_Report1();
                        if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                            frm.ReportName = "rpt_inhoadon_Bill_ban_58";
                        else
                        {
                            if (cls_KP_RES.Mode == 3)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_ban_1";
                            }
                            else if (cls_KP_RES.Mode == 4)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_ban_pho";
                            }
                            else if (cls_KP_RES.Mode == 9)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_image";
                            }
                            else
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_ban";
                            }
                        }
                        frm.DataSource = dtReport;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                        frm.IsPrint = true;
                        frm.ShowDialog();
                        frm.Dispose();
                        dt.Rows.Clear();
                        lbTotal.Text = "0";
                        idBill = "";

                        return;
                    }
                    else
                    {
                        int n = dtReport.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            dtReport.Rows[i]["TenNV"] = TenNV;
                            if (double.Parse(dtReport.Rows[i]["Giam_Gia1"].ToString()) > 100)
                                dtReport.Rows[i]["Giam_Gia"] = string.Format("{0:#,###0}", dtReport.Rows[i]["Giam_Gia1"]);
                            else
                                dtReport.Rows[i]["Giam_Gia"] = dtReport.Rows[i]["Giam_Gia1"].ToString() + "%";
                        }

                        Frm_Report1 frm = new Frm_Report1();
                        if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                            frm.ReportName = "rpt_inhoadon_Bill_58";
                        else
                        {
                            if (cls_KP_RES.Mode == 8)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_thaomoc";
                            }
                            else if (cls_KP_RES.Mode == 9)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_image";
                            }
                            else if (cls_ConfigCashier.sIn_Number_Fastfood != "0")
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_TraSua";
                            }
                            else
                            {
                                frm.ReportName = "rpt_inhoadon_Bill";
                            }
                        }
                        frm.DataSource = dtReport;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                        frm.IsPrint = true;
                        frm.ShowDialog();
                        frm.Dispose();
                        dt.Rows.Clear();
                        lbTotal.Text = "0";
                        idBill = "";

                        return;
                    }
                }
                else if (e.Column == HUY)
                {
                    sSQL = "";
                    sSQL += "Update HOADON Set " + "\n";
                    sSQL += "NHANVIEN_HUYBILL=" + clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                    sSQL += "ISPAYMENT=2 " + "\n";
                    sSQL += "Where MA_HOADON=" + clsMain.SQLString(idBill);
                    clsMain.ExecuteSQL(sSQL);
                    //Trừ kho
                    sSQL = string.Format("Exec SP_TRUKHO_DINHLUONG @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 0);
                    clsMain.ExecuteSQL(sSQL);
                    sSQL = string.Format("Exec SP_TRUKHO_HOADON @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 0);
                    clsMain.ExecuteSQL(sSQL);
                    //Công nợ
                    sSQL = "EXEC SP_CN_HUYHOADON '" + idBill + "'";
                    clsMain.ExecuteSQL(sSQL);
                    //KHTT
                    sSQL = "Select ISNULL(MA_KHACHHANG,'') AS MA_KHACHHANG,TONGTIEN,GIAMGIA From HOADON Where MA_HOADON =" + clsMain.SQLString(idBill);
                    DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                    if (dt1.Rows.Count > 0)
                    {
                        if (dt1.Rows[0]["MA_KHACHHANG"].ToString() != "")
                        {
                            DataTable dtKH = cls_KHTT.ReturnCauHinhKHTT();
                            if (dtKH.Rows[0]["GIATRI"].ToString() == "1")
                            {
                                int tem = 100 - int.Parse(dt1.Rows[0]["GIAMGIA"].ToString());
                                decimal tongtien = decimal.Parse(dt1.Rows[0]["TONGTIEN"].ToString()) / tem * 100;
                                cls_KHTT.TruDiemKhachHangThanThiet(dt1.Rows[0]["MA_KHACHHANG"].ToString(), tongtien);
                            }
                            else
                            {
                                cls_KHTT.TruDiemKhachHangThanThiet(dt1.Rows[0]["MA_KHACHHANG"].ToString(), decimal.Parse(dt1.Rows[0]["TONGTIEN"].ToString()));
                            }
                        }
                    }
                    //In bill
                    sSQL = "";
                    sSQL += "Select ROW_NUMBER() over (order by (select 1)) as STT,'' as TenNV ,ISNULL(cthd.CHIETKHAU,0) as CHIETKHAU,";
                    sSQL += "cthd.GIABAN, (cthd.SOLUONG * cthd.GIABAN) - (cthd.SOLUONG * cthd.GIABAN) * ISNULL(cthd.CHIETKHAU,0) / 100 as Thanh_Tien, hd.NGAYTAO, ISNULL(hd.GIORA,hd.NGAYTAO) as ngayHD,hh.TEN_HANGHOA, cthd.SOLUONG,";
                    sSQL += "hd.PHIPHUCVU as Phiphucvu,hd.MA_HOADON,TEN_QUAY as MA_QUAY, ISNULL(hd.TIENDATCOC,0) as TIENDATCOC,";
                    sSQL += "case when ISNULL(hd.NHANVIEN_RABILL,'')='' Then (hd.MANHANVIEN) Else (hd.NHANVIEN_RABILL) End as MANHANVIEN,";
                    sSQL += "hd.GIAMGIA as Giam_Gia1, hd.TONGTIEN as Tong_Cong, hd.TIEN_KHACHTRA, hd.TIEN_TRAKHACH, hd.TONGTIEN, N'PHIẾU THANH TOÁN \n (HỦY)' as Ten_HoaDon,";
                    sSQL += "ISNULL(cthd.TEM,0) AS TEM, ISNULL(cthd.MACP,'C') AS MACP, ISNULL(cthd.Number,0) AS Number" + "\n";
                    sSQL += "From HANGHOA hh inner join CT_HOADON cthd on hh.MA_HANGHOA = cthd.MA_HANGHOA inner join HOADON hd on cthd.MA_HOADON = hd.MA_HOADON" + "\n";
                    sSQL += "inner join QUAY q on hd.MA_QUAY = q.MA_QUAY" + "\n";
                    sSQL += "Where hd.MA_HOADON ='" + idBill + "'";
                    DataTable dtReport = clsMain.ReturnDataTable(sSQL);
                    for (int i = 0; i < dtReport.Rows.Count; i++)
                    {
                        if (dtReport.Rows[i]["MACP"].ToString() == "P")
                        {
                            dtReport.Rows[i]["TEN_HANGHOA"] = Kytu_Monthem + dtReport.Rows[i]["TEN_HANGHOA"].ToString();
                        }
                        else if (dtReport.Rows[i]["MACP"].ToString() == "CBC")
                        {
                            dtReport.Rows[i]["TEN_HANGHOA"] = Kytu_Combo + dtReport.Rows[i]["TEN_HANGHOA"].ToString();
                        }
                    }
                    dtReport.Columns.Add("Giam_Gia", typeof(String));

                    string TenNV = "";
                    if (dtReport.Rows.Count > 0)
                    {
                        sSQL = "Select TENNHANVIEN From DM_NHANVIEN Where MANHANVIEN=" + clsMain.SQLString(dtReport.Rows[0]["MANHANVIEN"].ToString());
                        DataTable dtTenNV = clsMain.ReturnDataTable(sSQL);
                        if (dtTenNV.Rows.Count > 0)
                            TenNV = dtTenNV.Rows[0]["TENNHANVIEN"].ToString();
                    }

                    sSQL = "Select TEN_BAN From CT_MOBAN ct,BAN b Where ct.MA_BAN=b.MA_BAN And ct.MA_HOADON=" + clsMain.SQLString(idBill);
                    DataTable dtTenban = clsMain.ReturnDataTable(sSQL);
                    if (dtTenban.Rows.Count > 0)
                    {
                        dtReport.Columns.Add("TEN_BAN", typeof(String));

                        int n = dtReport.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            dtReport.Rows[i]["TEN_BAN"] = dtTenban.Rows[0]["TEN_BAN"].ToString();
                            dtReport.Rows[i]["TenNV"] = TenNV;
                            if (double.Parse(dtReport.Rows[i]["Giam_Gia1"].ToString()) > 100)
                                dtReport.Rows[i]["Giam_Gia"] = string.Format("{0:#,###0}", dtReport.Rows[i]["Giam_Gia1"]);
                            else
                                dtReport.Rows[i]["Giam_Gia"] = dtReport.Rows[i]["Giam_Gia1"].ToString() + "%";
                        }

                        Frm_Report1 frm = new Frm_Report1();
                        if (cls_KP_RES.Mode == 3)
                        {
                            frm.ReportName = "rpt_inhoadon_Bill_ban_1";
                        }
                        else if (cls_KP_RES.Mode == 4)
                        {
                            frm.ReportName = "rpt_inhoadon_Bill_ban_pho";
                        }
                        else
                        {
                            frm.ReportName = "rpt_inhoadon_Bill_ban";
                        }
                        frm.DataSource = dtReport;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                        frm.IsPrint = true;
                        frm.ShowDialog();
                        frm.Dispose();
                        dt.Rows.Clear();
                        lbTotal.Text = "0";
                    }
                    else
                    {
                        int n = dtReport.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            dtReport.Rows[i]["TenNV"] = TenNV;
                            if (double.Parse(dtReport.Rows[i]["Giam_Gia1"].ToString()) > 100)
                                dtReport.Rows[i]["Giam_Gia"] = string.Format("{0:#,###0}", dtReport.Rows[i]["Giam_Gia1"]);
                            else
                                dtReport.Rows[i]["Giam_Gia"] = dtReport.Rows[i]["Giam_Gia1"].ToString() + "%";
                        }

                        Frm_Report1 frm = new Frm_Report1();
                        if (cls_ConfigCashier.sIn_Number_Fastfood != "0")
                        {
                            if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_TraSua_58";
                            }
                            else
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_TraSua";
                            }
                        }
                        else
                            frm.ReportName = "rpt_inhoadon_Bill";
                        frm.DataSource = dtReport;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                        frm.IsPrint = true;
                        frm.ShowDialog();
                        frm.Dispose();
                        dt.Rows.Clear();
                        lbTotal.Text = "0";
                    }
                    //load luoi hoa don
                    sSQL = "exec SP_DANHSACHHOADON";
                    DataTable dts = clsMain.ReturnDataTable(sSQL);
                    gr_billPaymented.DataSource = dts;
                    return;
                }
            }
            catch
            {
            }
        }

        private void GetDishOfTable(string maHoaDon)
        {
            dt.Rows.Clear();
            string sSQL = "";
            sSQL += "Select cthd.MA_HANGHOA, hh.TEN_HANGHOA,cthd.SOLUONG,cthd.GIABAN,'a' as GHICHU,hh.MA_BEP,cthd.THUE,ISNULL(cthd.CHIETKHAU,0) as CHIETKHAU,";
            sSQL += "0 as Thanh_Tien,ISNULL(TRANGTHAI,0) AS TRANGTHAI,ISNULL(TRANGTHAI,0) as IS_COMBO,hh.IS_INBEP,'' as IS_KHUYENMAI" + "\n";
            sSQL += "From CT_HOADON as cthd inner join HANGHOA as hh on cthd.MA_HANGHOA=hh.MA_HANGHOA " + "\n";
            sSQL += "Where cthd.MA_HOADON ='" + maHoaDon + "'";
            dt = clsMain.ReturnDataTable(sSQL);
            gr_SelectProduct.DataSource = dt;
            if (gv_SelectProduct.RowCount > 0)
                TotalMoney();
            else
                lbTotal.Text = "0";
            gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.RowCount - 1;
        }

        private void GetDistcountABill(string maHoaDon)
        {
            string sql = " select TIENDATCOC, GIAMGIA, PHIPHUCVU from HOADON  where MA_HOADON ='" + maHoaDon + "'";
            var dtDistcount = clsMain.ReturnDataTable(sql);
            lbPhuThu.Text = string.Format("{0:#,###0}", double.Parse(dtDistcount.Rows[0]["PHIPHUCVU"].ToString()));
            lbGiamGia.Text = string.Format("{0:#,###0}", double.Parse(dtDistcount.Rows[0]["GIAMGIA"].ToString()));
        }

        private void btnComboOption_Click(object sender, EventArgs e)
        {
            try
            {
                string co_mahanghoa = "";
                string co_tenhanghoa = "";
                string co_soluong = "";
                string co_phuthu = "";

                if (bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
                {
                    string maCombo = "";
                    int i = 0;
                    for (i = gv_SelectProduct.FocusedRowHandle - 1; i >= 0; i--)
                    {
                        if (!bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
                        {
                            maCombo = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                            break;
                        }
                    }

                    string sSQL = "";
                    sSQL += "Select MA_HANGHOA_OPTION as MA,B.TEN_HANGHOA as TEN,A.SOLUONG,GIATHEM" + "\n";
                    sSQL += "From COMBO_OPTION A,HANGHOA B" + "\n";
                    sSQL += "Where MA_HANGHOA_OPTION=b.MA_HANGHOA" + "\n";
                    sSQL += "AND a.MA_COMBO=" + clsMain.SQLString(maCombo) + "\n";
                    sSQL += "AND a.MA_HANGHOA=" + clsMain.SQLString(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                    DataTable dt = clsMain.ReturnDataTable(sSQL);
                    if (dt.Rows.Count > 0)
                    {
                        Frm_Combo_Option frm = new Frm_Combo_Option(maCombo, gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                        frm.ShowDialog();
                        co_mahanghoa = frm.s_mahanghoa;
                        co_tenhanghoa = Kytu_Combo + frm.s_tenhanghoa;
                        co_soluong = (int.Parse(frm.s_soluong) * int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, "SOLUONG").ToString())).ToString();
                        co_phuthu = frm.s_phuthu;
                        if (co_mahanghoa != "")
                        {
                            float tt = float.Parse(gv_SelectProduct.GetRowCellValue(i, "GIABAN").ToString()) + float.Parse(co_phuthu);
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "MA_HANGHOA", co_mahanghoa);
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "TEN_HANGHOA", co_tenhanghoa);
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "SOLUONG", int.Parse(co_soluong));
                            gv_SelectProduct.SetRowCellValue(i, "GIABAN", tt);
                            gv_SelectProduct.SetRowCellValue(i, "Thanh_Tien", tt * int.Parse(gv_SelectProduct.GetRowCellValue(i, "SOLUONG").ToString()));
                        }
                    }
                    else if (dt.Rows.Count == 0)
                    {
                        sSQL = "";
                        sSQL += "Select a.MA_HANGHOA as MA,B.TEN_HANGHOA as TEN,A.SOLUONG,GIATHEM" + "\n";
                        sSQL += "From COMBO_OPTION A,HANGHOA B" + "\n";
                        sSQL += "Where a.MA_HANGHOA=b.MA_HANGHOA" + "\n";
                        sSQL += "AND a.MA_COMBO=" + clsMain.SQLString(maCombo) + "\n";
                        sSQL += "AND a.MA_HANGHOA_OPTION=" + clsMain.SQLString(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, "MA_HANGHOA").ToString());
                        dt = clsMain.ReturnDataTable(sSQL);
                        if (dt.Rows.Count > 0)
                        {
                            int sl = int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, "SOLUONG").ToString()) / int.Parse(dt.Rows[0]["SOLUONG"].ToString());
                            float tt = float.Parse(gv_SelectProduct.GetRowCellValue(i, "GIABAN").ToString()) - float.Parse(dt.Rows[0]["GIATHEM"].ToString());
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "MA_HANGHOA", dt.Rows[0]["MA"].ToString());
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "TEN_HANGHOA", Kytu_Combo + dt.Rows[0]["TEN"].ToString());
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "SOLUONG", sl);
                            gv_SelectProduct.SetRowCellValue(i, "GIABAN", tt);
                            gv_SelectProduct.SetRowCellValue(i, "Thanh_Tien", tt * int.Parse(gv_SelectProduct.GetRowCellValue(i, "SOLUONG").ToString()));
                        }
                        else
                        {
                            XtraMessageBox.Show(_khongcohhthaythe, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    else
                    {
                        XtraMessageBox.Show(_khongcohhthaythe, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                else
                {
                    XtraMessageBox.Show(_hhkhongthuoccombo, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                TotalMoney();
            }
            catch
            {
            }
        }

        private void btnThemHangHoa_Click(object sender, EventArgs e)
        {
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }
            Frm_ThemHangHoa frm = new Frm_ThemHangHoa();
            frm.ShowDialog();
            frm.Dispose();
        }

        String sTenMon = "";

        private void btn_YeuCauThem_Click(object sender, EventArgs e)
        {
            try
            {
                pnBtnDeleteGv.Visible = false;
                btnComboOption.Visible = false;
                string sMaCP = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MACP"]).ToString();
                if (sMaCP == "P" || sMaCP == "CB")
                    return;
                if (isYeuCauThem == false)
                {
                    SetBackgroudOnClick(btn_YeuCauThem);
                    lbkvYeuCauThem.Text = "";
                    lbTableYeuCauThem.Text = "";
                    lbMonYeuCauThem.Text = sTenMon;
                    us_keyboard3.SetTextbox(txtYeuCauThem);
                    pnYeucauthem.BringToFront();
                    txtYeuCauThem.Focus();
                    string sMA_HANGHOA = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                    SelectProduct(sMA_HANGHOA);
                    sTEM = int.Parse(gv_SelectProduct.GetFocusedRowCellValue("TEM").ToString());
                    sMaCB = int.Parse(gv_SelectProduct.GetFocusedRowCellValue("MACB").ToString());
                    isYeuCauThem = true;
                    foreach (SimpleButton btn in pnThuNgan.Controls)
                    {
                        if (btn.Name != "btn_YeuCauThem")
                        {
                            btn.Enabled = false;
                        }
                    }
                }
                else if (isYeuCauThem == true)
                {
                    isYeuCauThem = false;
                    GetNhomhang();
                    GetListMon();
                    SetDefautlBackgroud(btn_YeuCauThem);
                    if (dt.Rows.Count > 0)
                    {
                        sTEM = LayTemLonNhat() + 1;
                        sMaCB = LayMACBLonNhat() + 1;
                    }
                    foreach (SimpleButton btn in pnThuNgan.Controls)
                    {
                        btn.Enabled = true;
                    }
                }
            }
            catch (Exception)
            {

            }

        }

        private void SelectProduct(string sMA_HANGHOA)
        {
            string sql = "SELECT  * FROM YEUCAUTHEM WHERE Item_ID=" + clsMain.SQLString(sMA_HANGHOA) + "\n";
            sql += "UNION" + "\n";
            sql += "SELECT  * FROM YEUCAUTHEM WHERE Item_ID is null" + "\n";
            sql += "ORDER BY  YEUCAU" + "\n";
            DataTable requestAdd = clsMain.ReturnDataTable(sql);
            pnYeuCau.Controls.Clear();
            for (int i = 0; i < requestAdd.Rows.Count; i++)
            {
                SimpleButton btn = new SimpleButton();
                btn.Name = requestAdd.Rows[i]["id"].ToString();
                btn.Text = requestAdd.Rows[i]["YEUCAU"].ToString();
                btn.Click += new EventHandler(btn_Click);
                btn.Size = new Size(115, 68);
                pnYeuCau.Controls.Add(btn);
            }
        }

        private void btn_Click(object sender, EventArgs e)
        {
            foreach (SimpleButton btn in pnYeuCau.Controls)
            {
                if (btn.Name == this.ActiveControl.Name)
                {
                    txtYeuCauThem.Text += btn.Text + "," + " ";
                    break;
                }
            }
            txtYeuCauThem.Focus();
            int textLength = txtYeuCauThem.Text.Length;
            txtYeuCauThem.SelectionStart = textLength;
            txtYeuCauThem.SelectionLength = 0;
        }

        private void txtYeuCauThem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string sMaHangHoaChonTrenLuoi = gv_SelectProduct.GetRowCellValue(iFocusRow, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    string maHH = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                    int sTem = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEM"]).ToString());
                    if (maHH == sMaHangHoaChonTrenLuoi & sTem == sTEM_THEMMON)
                    {
                        gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"], txtYeuCauThem.Text);
                        break;
                    }
                }
                txtYeuCauThem.Text = "";
                pnShowHang.BringToFront();
                SetDefaultBackgroundBtnInPn(pnThuNgan);
                SetDefaultBackgroundBtnInPn(pnDacThu);

                isYeuCauThem = false;
                GetNhomhang();
                GetListMon();
                if (dt.Rows.Count > 0)
                {
                    sTEM = LayTemLonNhat() + 1;
                    sMaCB = LayMACBLonNhat() + 1;
                }
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    btn.Enabled = true;
                }
            }
        }

        private void btnYCT_Len_Click(object sender, EventArgs e)
        {
            try
            {
                pnYeuCau.AutoScrollPosition = new Point(0, pnYeuCau.VerticalScroll.Value - pnYeuCau.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnYCT_Xuong_Click(object sender, EventArgs e)
        {
            try
            {
                pnYeuCau.AutoScrollPosition = new Point(0, pnYeuCau.VerticalScroll.Value + pnYeuCau.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void SetDefaultBackgroundBtnInPn(PanelControl pn2)
        {
            foreach (SimpleButton bt in pn2.Controls)
            {
                SetDefautlBackgroud(bt);
            }
        }

        private void GetNhomhangMonThem()
        {
            try
            {
                dtNH.Rows.Clear();
                string SQLMON = string.Format("exec SelectDishMonThem @PageNumber={0},@PageSize={1},@maQUAY={2}", cPageDish, 7, int.Parse(cls_ConfigCashier.idCashier));
                dtNH = clsMain.ReturnDataTable(SQLMON);

                while (pnKV_Mon.Controls.Count > 0)
                {
                    pnKV_Mon.Controls[0].Dispose();
                }
                pnKV_Mon.Controls.Clear();
                int locateY = 2;
                int locateX = 3;
                for (int i = 0; i < dtNH.Rows.Count; i++)
                {
                    SimpleButton btnKV = new SimpleButton();
                    btnKV.Name = dtNH.Rows[i]["MA_NHOMHANG"].ToString();
                    if (i == 0)
                    {
                        maNH = dtNH.Rows[0]["MA_NHOMHANG"].ToString();
                        SetBackgroudOnClick(btnKV);
                    }
                    btnKV.Text = "" + dtNH.Rows[i]["TEN_NHOMHANG"].ToString();
                    try
                    {
                        byte[] tam = new byte[((byte[])dtNH.Rows[i]["HINHANH"]).LongLength];
                        tam = (byte[])dtNH.Rows[i]["HINHANH"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        //btnKV.Image = bm;
                        if (cls_KP_RES.iHinhanh == 1)
                        {
                            btnKV.BackgroundImage = bm;
                            btnKV.BackgroundImageLayout = ImageLayout.Stretch;
                            btnKV.Appearance.BackColor = Color.Transparent;
                            btnKV.Appearance.Options.UseBackColor = true;
                            btnKV.ButtonStyle = BorderStyles.NoBorder;
                            btnKV.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                        }
                        else
                        {
                            btnKV.Image = bm;
                        }
                    }
                    catch
                    {
                    }
                    btnKV.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btnKV.ImageLocation = ImageLocation.TopCenter;
                    btnKV.RightToLeft = RightToLeft.No;
                    btnKV.Size = new Size(125, 80);
                    if (i != 0)
                    {
                        locateY += int.Parse(btnKV.Location.Y.ToString()) + 85;
                    }
                    btnKV.Location = new Point(locateX, locateY);
                    btnKV.Click += new EventHandler(btnKV_Click);
                    pnKV_Mon.Controls.Add(btnKV);
                }
            }
            catch
            {
            }
        }

        private void btn_MonThem_Click(object sender, EventArgs e)
        {
            try
            {
                if (isYeuCauThem == true)
                    return;
                if (gv_SelectProduct.RowCount == 0)
                    sButtonMonThem = true;
                else
                {
                    bool sMONTHEM = bool.Parse(gv_SelectProduct.GetFocusedRowCellValue("MONTHEM").ToString());
                    string sMACP = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MACP"]).ToString();

                    if (sMONTHEM == false & sButtonMonThem == false)
                        //sButtonMonThem = true;
                        return;
                }

                pnBtnDeleteGv.Visible = false;
                btnComboOption.Visible = false;

                if (sButtonMonThem == false)
                {

                    GetNhomhangMonThem();
                    GetListMon();
                    sButtonMonThem = true;
                    SetBackgroudOnClick(btn_MonThem);
                    panelControl1.Visible = false;
                    sTEM = int.Parse(gv_SelectProduct.GetFocusedRowCellValue("TEM").ToString());
                    sMaCB = int.Parse(gv_SelectProduct.GetFocusedRowCellValue("MACB").ToString());
                    foreach (SimpleButton btn in pnThuNgan.Controls)
                    {
                        if (btn.Name != "btn_MonThem")
                        {
                            btn.Enabled = false;
                        }
                    }
                }
                else if (sButtonMonThem == true)
                {
                    GetNhomhang();
                    GetListMon();
                    sButtonMonThem = false;
                    SetDefautlBackgroud(btn_MonThem);
                    if (dt.Rows.Count > 0)
                    {
                        sTEM = LayTemLonNhat() + 1;
                        sMaCB = LayMACBLonNhat() + 1;
                    }
                    else
                        sTEM = 1;
                    foreach (SimpleButton btn in pnThuNgan.Controls)
                    {
                        btn.Enabled = true;
                        LoadPermission();
                    }
                } 
            }
            catch (Exception)
            {
            }
        }
        private void SetGiaoDienMacDinh()
        {

            GetNhomhang();
            GetListMon();
            sButtonMonThem = false;
            isYeuCauThem = false;
            foreach (SimpleButton btn in pnThuNgan.Controls)
            {
                btn.Enabled = true;
                SetDefautlBackgroud(btn);
            }
        }
        private void DanhSoThuTu()
        {
            sTT = 1;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["MACP"].ToString() == "C" || dt.Rows[i]["MACP"].ToString() == "CBC")
                {
                    dt.Rows[i]["STT"] = sTT;
                    sTT += 1;
                }
            }
        }

        private bool KiemTraCombo(string MACOMBO)
        {
            bool kq = false;
            DataTable dt_combo = GetProductInCombo(MACOMBO);
            string sSQL = "";
            for (int i = 0; i < dt_combo.Rows.Count; i++)
            {
                sSQL += "Select a.MA_HANGHOA as MA,B.TEN_HANGHOA as TEN,A.SOLUONG,GIATHEM" + "\n";
                sSQL += "From COMBO_OPTION A,HANGHOA B" + "\n";
                sSQL += "Where a.MA_HANGHOA=b.MA_HANGHOA" + "\n";
                sSQL += "AND a.MA_COMBO=" + clsMain.SQLString(MACOMBO) + "\n";
                sSQL += "AND a.MA_HANGHOA_OPTION=" + clsMain.SQLString(dt_combo.Rows[i]["MA_HANGHOA"].ToString());
                DataTable dt_option = clsMain.ReturnDataTable(sSQL);

                if (dt_option.Rows.Count > 0 || bool.Parse(dt_combo.Rows[i]["MONTHEM"].ToString()) == true)
                {
                    kq = true;
                    break;
                }
                else
                {
                    kq = false;
                }
            }
            return kq;
        }

        private bool KiemTraCot(DataTable _dt)
        {
            bool kq = false;
            foreach (DataRow dr in _dt.Rows)
            {
                if (dt.Columns.Contains("Number"))
                {
                    kq= true;
                    break;
                }
            }
            return kq;
        }

        private int LayTemLonNhat()
        {
            int kq = 1;
            foreach (DataRow dr in dt.Rows)
            {
                if (int.Parse(dr["TEM"].ToString()) > kq)
                {
                    kq = int.Parse(dr["TEM"].ToString());
                }
            }
            return kq;
        }
        private int LayMACBLonNhat()
        {
            int kq = 1;
            foreach (DataRow dr in dt.Rows)
            {
                if (int.Parse(dr["MACB"].ToString()) > kq)
                {
                    kq = int.Parse(dr["MACB"].ToString());
                }
            }
            return kq;
        }
        private int LaySoLuongLonNhat(int iMACB)
        {
            int kq = 1;
            foreach (DataRow dr in dt.Rows)
            {
                if (int.Parse(dr["MACB"].ToString()) == iMACB & dr["MACP"].ToString() == "C")
                {
                    kq = int.Parse(dr["SOLUONG"].ToString());
                }
            }
            return kq;
        }

        private void btnGoikhach_Click(object sender, EventArgs e)
        {
            Frm_Goikhach frm = new Frm_Goikhach();
            frm.Width =this.Width ;
            frm.Height = this.Height;
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnInbilldoi_Click(object sender, EventArgs e)
        {
            string sMaBillDoi = "";
            Frm_GhiChu frm = new Frm_GhiChu(sMaBillDoi);
            frm.smode = "1";
            frm.ShowDialog();
            sMaBillDoi = frm.sGhiChu;
            frm.Dispose();

            try
            {
                string sSQL = "";
                sSQL += "Exec Get_Hoadon_Doi " + clsMain.SQLString(sMaBillDoi);
                DataTable myDT = clsMain.ReturnDataTable(sSQL);

               
                if (myDT.Rows.Count > 0)
                {
                    dt.Rows.Clear();
                    for (int i = 0; i < myDT.Rows.Count; i++)
                    {
                        DataRow dr = dt.NewRow();
                        dr["MA_HANGHOA"] = myDT.Rows[i]["MA_HANGHOA"].ToString();
                        if (myDT.Rows[i]["MACP"].ToString() == "P")
                        {
                            dr["TEN_HANGHOA"] = Kytu_Monthem + myDT.Rows[i]["TEN_HANGHOA"].ToString();
                        }
                        else
                        {
                            dr["TEN_HANGHOA"] = myDT.Rows[i]["TEN_HANGHOA"].ToString();
                        }
                        dr["SOLUONG"] = myDT.Rows[i]["SOLUONG"].ToString();
                        dr["GIABAN"] = myDT.Rows[i]["GIABAN"].ToString();
                        dr["GHICHU"] = myDT.Rows[i]["GHICHU"].ToString();
                        dr["MA_BEP"] = myDT.Rows[i]["MA_BEP"].ToString();
                        dr["THUE"] = myDT.Rows[i]["THUE"].ToString();
                        dr["Thanh_Tien"] = myDT.Rows[i]["Thanh_Tien"].ToString();
                        dr["TRANGTHAI"] = bool.Parse(myDT.Rows[i]["TRANGTHAI"].ToString());
                        dr["IS_COMBO"] = bool.Parse(myDT.Rows[i]["IS_COMBO"].ToString());
                        dr["IS_INBEP"] = bool.Parse(myDT.Rows[i]["IS_INBEP"].ToString());
                        dr["IS_KHUYENMAI"] = bool.Parse(myDT.Rows[i]["IS_KHUYENMAI"].ToString());
                        dr["CHIETKHAU"] = int.Parse(myDT.Rows[i]["CHIETKHAU"].ToString());
                        dr["MONTHEM"] = bool.Parse(myDT.Rows[i]["MONTHEM"].ToString());
                        dr["TEM"] = int.Parse(myDT.Rows[i]["TEM"].ToString());
                        dr["MACP"] = myDT.Rows[i]["MACP"].ToString();
                        dr["STT"] = myDT.Rows[i]["STT"].ToString();
                        dr["MACB"] = myDT.Rows[i]["MACB"].ToString();
                        dr["INTEM"] = bool.Parse(myDT.Rows[i]["INTEM"].ToString());
                      
                        dt.Rows.Add(dr);
                    }

                    gr_SelectProduct.DataSource = dt;
                    if (gv_SelectProduct.RowCount > 0)
                    {
                        //sSQL = "";
                        //sSQL += "DELETE CT_HOADON_DOI WHERE MA_HOADON = " + clsMain.SQLString(gvBillPaymented.GetRowCellValue(gvBillPaymented.FocusedRowHandle, "MA_HOADON").ToString()) + "\n";
                        //sSQL += "DELETE HOADON_DOI WHERE MA_HOADON = " + clsMain.SQLString(gvBillPaymented.GetRowCellValue(gvBillPaymented.FocusedRowHandle, "MA_HOADON").ToString()) + "\n";
                        //clsMain.ExecuteSQL(sSQL);

                        TotalMoney();
                        btn_ThuNgan_Click(null, null);
                    }
                }
            }
            catch
            {
            }
        }

        private void btnChietkhau100_Click(object sender, EventArgs e)
        {
            if (gv_SelectProduct.RowCount <= 0 || maMon == null || maMon == "" || gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, "SOLUONG").ToString ()!="1")
                return;

            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }

            decimal Giamoi = 0;
            //Frm_Nhapchietkhau frm = new Frm_Nhapchietkhau();
            //frm.ShowDialog();
            //Giamoi = frm.tmp_Thoigia;
            //frm.Dispose();
            Giamoi = 100;
            if (Giamoi <= 100 && Giamoi >= 0)
            {
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    if (gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString() == maMon && i == gv_SelectProduct.FocusedRowHandle)
                    {
                        gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["CHIETKHAU"], Giamoi.ToString());
                    }
                }
                if (gv_SelectProduct.RowCount > 0)
                {
                    TotalMoney();
                }
            }
        }

       
    }
}