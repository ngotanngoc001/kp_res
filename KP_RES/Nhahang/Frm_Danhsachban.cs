﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using System.IO;

namespace KP_RES
{
    public partial class Frm_Danhsachban : DevExpress.XtraEditors.XtraForm
    {
        public string idTable = null;
        public static bool flag_Chuyenban = false;
        public static string mahoadon = "";


        public int funtions = 1;//Funtion 1 Sơ đồ bàn, Funtion 2 Mở bàn, 3 yêu cầu thêm, 4 send Order,5 Gép bàn , 6 Tách bàn, 7 Payments, 8 buy id Product, 9 Đặt bàn, 10 ket ca,11 Chuyển bàn.
        float xGIABAN;//giá đang bán cho khách
        int sendOrder = 0;
        string maBan = "";
        string tenBan = "";
        string maBanGhep = "";
        string idBillJoin = "";
        string maMon = "";
        bool _flag_chuyenban = false;
        string _mahoadon_chuyenban = "";
        DataTable dt = new DataTable("HANGHOA");
        DataTable dtSoDoBan = new DataTable();
        DataTable dtKhuVuc = new DataTable();
        DataTable dtNH = new DataTable();
        DataTable dtMon = new DataTable();
        DataTable dtkc = new DataTable();
        int tatol_cPageMon;
        int cPageKV = 1;
        int cPageDish = 1;
        int CurrenPages = 1;
        int SizePages = 30;
        int cPageMon = 1;
        int sizeMon = 30;
        int maKV = 1;
        int maNH = 1;
        string co_mahanghoa = "";
        string co_tenhanghoa = "";
        string co_soluong = "";
        string co_phuthu = "";
        DataTable divide_Table1 = new DataTable();
        DataTable divide_Table2 = new DataTable();
        bool exitReprint = false;
        bool Flag_soluong = false; //true:so luong tren lưới dc bắt đầu lại,False:sô lượng đang tạo +thêm chuỗi.
        string sSQL_DL = "";
        string sSQL_Yeucauthem = "";
        int iFocusRow = 0;
        DataTable dtQuay_Nhomhang = new DataTable();//Quầy bán cho nhóm hàng nào
        int selectTable = 0;
        public int selectDishes = 0;
        public DateTime txtngay;
        
        string _moban = "";
        string _dongban = "";
        string _thongbao = "";
        string _cbdsbk = "";
        public Frm_Danhsachban()
        {
            InitializeComponent();
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            groupControl1.Text = rm.GetString("sodoban", culture);


            GetKhuVuc();
            GetSoDoBan();
        }

        private void GetKhuVuc()
        {
            try
            {
                dtKhuVuc.Rows.Clear();
                string SQLKV = string.Format("Exec SelectKhuVuc @PageNumber={0},@PageSize={1}, @maCH={2}, @maQUAY={3}", cPageKV, 7, cls_ConfigCashier.idShop, cls_ConfigCashier.idCashier);
                dtKhuVuc = clsMain.ReturnDataTable(SQLKV);

                while (pnKV_Mon.Controls.Count > 0)
                {
                    pnKV_Mon.Controls[0].Dispose();
                }
                pnKV_Mon.Controls.Clear();
                int locateY = 2;
                int locateX = 3;
                for (int i = 0; i < dtKhuVuc.Rows.Count; i++)
                {
                    SimpleButton btnKV = new SimpleButton();
                    btnKV.Name = dtKhuVuc.Rows[i]["MA_KHUVUC"].ToString();
                    if (i == 0)
                    {
                        maKV = int.Parse(dtKhuVuc.Rows[0]["MA_KHUVUC"].ToString());
                        lbKhuVuc.Text = "" + dtKhuVuc.Rows[0]["TEN_KHUVUC"].ToString() + ">";
                        SetBackgroudOnClick(btnKV);
                    }
                    btnKV.Text = dtKhuVuc.Rows[i]["TEN_KHUVUC"].ToString();
                    try
                    {
                        byte[] tam = new byte[((byte[])dtKhuVuc.Rows[i]["HINHANH"]).LongLength];
                        tam = (byte[])dtKhuVuc.Rows[i]["HINHANH"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        btnKV.Image = bm;
                    }
                    catch
                    {
                    }
                    btnKV.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btnKV.ImageLocation = ImageLocation.TopCenter;
                    btnKV.RightToLeft = RightToLeft.No;
                    btnKV.Size = new Size(125, 80);
                    if (i != 0)
                    {
                        locateY += int.Parse(btnKV.Location.Y.ToString()) + 85;
                    }
                    btnKV.Location = new Point(locateX, locateY);
                    btnKV.Click += new EventHandler(btnKV_Click);
                    pnKV_Mon.Controls.Add(btnKV);
                }
            }
            catch
            {
            }
        }

        private void GetSoDoBan()
        {
            try
            {
                dtSoDoBan.Clear();
                string sql = string.Format("Exec SelectTable @PageNumber={0},@maKV={1}", CurrenPages, maKV);
                dtSoDoBan = clsMain.ReturnDataTable(sql);

                while (pnTable_Mon.Controls.Count > 0)
                {
                    pnTable_Mon.Controls[0].Dispose();
                }
                pnTable_Mon.Controls.Clear();
                for (int i = 0; i < dtSoDoBan.Rows.Count; i++)
                {
                    SimpleButton btnBan_Mon = new SimpleButton();
                    btnBan_Mon.Name = dtSoDoBan.Rows[i]["MA_BAN"].ToString();
                    if (dtSoDoBan.Rows[i]["SOGHE"].ToString() != "0")
                    {
                        btnBan_Mon.Text = dtSoDoBan.Rows[i]["TEN_BAN"].ToString() + " - " + dtSoDoBan.Rows[i]["SOGHE"].ToString() + "  Ghế";
                    }
                    else
                    {
                        btnBan_Mon.Text = dtSoDoBan.Rows[i]["TEN_BAN"].ToString();
                    }
                    try
                    {
                        byte[] tam = new byte[((byte[])dtSoDoBan.Rows[i]["HINHANH"]).LongLength];
                        tam = (byte[])dtSoDoBan.Rows[i]["HINHANH"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        btnBan_Mon.Image = bm;
                    }
                    catch
                    {
                    }
                    btnBan_Mon.Width = int.Parse(dtSoDoBan.Rows[i]["Width"].ToString());
                    btnBan_Mon.Height = int.Parse(dtSoDoBan.Rows[i]["Height"].ToString());
                    btnBan_Mon.Location = new Point(int.Parse(dtSoDoBan.Rows[i]["LocaX"].ToString()), int.Parse(dtSoDoBan.Rows[i]["LocaY"].ToString()));

                    try
                    {
                        if (bool.Parse(dtSoDoBan.Rows[i]["ISBANDAT"].ToString()) == true)
                        {
                            SetBackgroudOnClick2(btnBan_Mon);
                        }
                    }
                    catch
                    {
                    }
                    try
                    {
                        if (bool.Parse(dtSoDoBan.Rows[i]["ISMO_BAN"].ToString()) == true)
                        {
                            DataTable dataInPut = getGioVaoBan(btnBan_Mon.Name.ToString());
                            if (dataInPut.Rows.Count > 0)
                            {
                                btnBan_Mon.Text += "\n Giờ vào: " + "\n" + cls_KP_RES.DateToString_Full_NoSecond((DateTime)dataInPut.Rows[0][0]);
                            }
                            SetBackgroudOnClick1(btnBan_Mon);

                            string idHD = GetMaHoaDon(btnBan_Mon.Name.ToString());
                            DataTable tb = DemSoBanTrongBill(idHD);
                            if (tb.Rows.Count > 1)
                            {
                                btnBan_Mon.Text += " \n GB " + idHD.Substring(idHD.IndexOf("B") + 1, 4);
                            }
                        }
                    }
                    catch
                    {
                    }
                    try
                    {
                        if (bool.Parse(dtSoDoBan.Rows[i]["SEE_PAYMENTS"].ToString()) == true)
                        {
                            SetBackgroudOnClick3(btnBan_Mon);
                        }
                    }
                    catch
                    {
                    }

                    btnBan_Mon.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btnBan_Mon.ImageLocation = ImageLocation.TopCenter;
                    btnBan_Mon.RightToLeft = RightToLeft.No;
                    btnBan_Mon.Click += new EventHandler(btnBan_Mon_Click);
                    pnTable_Mon.Controls.Add(btnBan_Mon);
                }
            }
            catch
            {
            }
        }

        private void btnKV_Click(object sender, EventArgs e)
        {
            try
            {
                CurrenPages = 1;
                cPageMon = 1;
                lbPageTable.Text = CurrenPages.ToString();
                foreach (SimpleButton btnKV in pnKV_Mon.Controls)
                {
                    if (btnKV.Name == this.ActiveControl.Name)
                    {
                        SetBackgroudOnClick(btnKV);
                        if (selectDishes == 2)
                        {
                            funtions = 1;
                            lbKhuVuc.Text = btnKV.Text + "  >  ";
                            lbMon.Text = "";
                            lbBan.Text = "";
                            maBan = "";
                            maKV = int.Parse(btnKV.Name.ToString());
                            GetSoDoBan();
                            funtions = 9;
                        }
                        else if (selectDishes == 1)
                        {
                            funtions = 2;
                            maNH = int.Parse(btnKV.Name.ToString());
                            GetListMon();
                            funtions = 9;
                        }
                    }
                    else
                    {
                        SetDefautlBackgroud(btnKV);
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK);
            }
        }

        private void btnBan_Mon_Click(object sender, EventArgs e)
        {
            try
            {
                if (funtions == 9)
                {
                    if (selectDishes == 0)
                    {
                        return;
                    }
                    foreach (SimpleButton btnBan in pnTable_Mon.Controls)
                    {
                        if (btnBan.Name == this.ActiveControl.Name)
                        {
                            if (selectDishes == 1)
                            {
                                sendOrder = 1;
                                maMon = btnBan.Name;
                                if (selectTable == 1)
                                {
                                    selectTable = 0;
                                    dt.Rows.Clear();
                                    //gr_SelectProduct.RefreshDataSource();
                                }
                                //GetDishGoToBuy(maMon, 0);
                            }
                            else
                            {
                                if (selectDishes == 2)
                                {
                                    maBan = btnBan.Name;
                                    if (CheckOpenTable(maBan) == false)
                                    {
                                        if (CheckHaveISBANDAT(int.Parse(maBan)) == true)// research find table check
                                        {
                                            if (SelectTablesCheckt(maBan) == true)// kiểm tra time of this table have ok or no 
                                            {
                                                SelectCheckTable(maBan); // kiểm tra bàn này đã chọn đặt chưa ?
                                                SetBackgroudOnClick(btnBan);
                                            }
                                            else
                                            {
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            SelectCheckTable(maBan);
                                            SetBackgroudOnClick(btnBan);
                                        }
                                    }
                                    else
                                    {
                                        if (XacNhanBanDat(maBan) == true)
                                        {
                                            SelectCheckTable(maBan);
                                            SetBackgroudOnClick(btnBan);
                                        }
                                        else
                                        {
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK);
            }
        }

        private void SetBackgroudOnClick(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.YellowGreen;
            _btn.Appearance.BackColor2 = Color.YellowGreen;
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;

        }

        private void SetDefautlBackgroud(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.White;
            if (_btn.Appearance.Options.UseBackColor == true)
                _btn.Appearance.Options.UseBackColor = false;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == false)
                _btn.LookAndFeel.UseDefaultLookAndFeel = true;
        }

        private void SetBackgroudOnClick2(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.FromArgb(0, 173, 239);
            _btn.Appearance.BackColor2 = Color.FromArgb(0, 173, 239);
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private void SetBackgroudOnClick3(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.FromArgb(214, 223, 35);
            _btn.Appearance.BackColor2 = Color.FromArgb(214, 223, 35);
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;

        }

        private DataTable getGioVaoBan(string maBan)
        {
            string sSQL = "";
            sSQL += "Select GIOVAO" + "\n";
            sSQL += "From HOADON hd,CT_MOBAN ct" + "\n";
            sSQL += "Where hd.MA_HOADON = ct.MA_HOADON And hd.ISPAYMENT = 0 And hd.ISBANDAT=0" + "\n";
            sSQL += "And ct.MA_BAN = " + clsMain.SQLString(maBan);
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                return dt;
            }
            else
            {
                sSQL = "";
                sSQL += "Select GIOVAO" + "\n";
                sSQL += "From HOADON hd,CT_MOBAN ct" + "\n";
                sSQL += "Where hd.MA_HOADON = ct.MA_HOADON And hd.ISPAYMENT = 0 And hd.ISBANDAT=1" + "\n";
                sSQL += "And ct.MA_BAN = " + clsMain.SQLString(maBan) + "\n";
                sSQL += "Order by GIOVAO ASC";
                dt = clsMain.ReturnDataTable(sSQL);
                return dt;
            }
        }

        private void SetBackgroudOnClick1(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.FromArgb(229, 137, 45);
            _btn.Appearance.BackColor2 = Color.FromArgb(229, 137, 45);
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private string GetMaHoaDon(string maBan)
        {
            string sSQL = "";
            sSQL += "Select a.MA_HOADON " + "\n";
            sSQL += "From CT_MOBAN as a,HOADON as b,BAN c" + "\n";
            sSQL += "Where a.MA_HOADON = b.MA_HOADON And a.MA_BAN=c.MA_BAN And b.ISBANDAT=0" + "\n";
            sSQL += "And ISMO_BAN=1 And b.ISPAYMENT = 0 And a.MA_BAN = " + clsMain.SQLString(maBan) + "\n";
            sSQL += "Order by MA_HOADON desc";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0]["MA_HOADON"].ToString();
            }
            else
            {
                sSQL = "";
                sSQL += "Select a.MA_HOADON " + "\n";
                sSQL += "From CT_MOBAN as a,HOADON as b,BAN c" + "\n";
                sSQL += "Where a.MA_HOADON = b.MA_HOADON And a.MA_BAN=c.MA_BAN And b.ISBANDAT=1" + "\n";
                sSQL += "And ISMO_BAN=1 And b.ISPAYMENT = 0 And a.MA_BAN = " + clsMain.SQLString(maBan) + "\n";
                sSQL += "Order by GIOVAO ASC";
                dt = clsMain.ReturnDataTable(sSQL);
                if (dt.Rows.Count > 0)
                {
                    return dt.Rows[0]["MA_HOADON"].ToString();
                }
                else
                {
                    return "";
                }
            }
        }

        private DataTable DemSoBanTrongBill(string maBill)
        {
            DataTable dttabless;
            string sql = "select MA_BAN from CT_MOBAN where MA_HOADON =" + clsMain.SQLString(maBill);
            dttabless = clsMain.ReturnDataTable(sql);
            return dttabless;
        }

        private void GetNhomhang()
        {
            try
            {
                dtNH.Rows.Clear();
                string SQLMON = string.Format("exec SelectDish @PageNumber={0},@PageSize={1},@maQUAY={2}", cPageDish, 7, int.Parse(cls_ConfigCashier.idCashier));
                dtNH = clsMain.ReturnDataTable(SQLMON);

                while (pnKV_Mon.Controls.Count > 0)
                {
                    pnKV_Mon.Controls[0].Dispose();
                }
                pnKV_Mon.Controls.Clear();
                int locateY = 2;
                int locateX = 3;
                for (int i = 0; i < dtNH.Rows.Count; i++)
                {
                    SimpleButton btnKV = new SimpleButton();
                    btnKV.Name = dtNH.Rows[i]["MA_NHOMHANG"].ToString();
                    if (i == 0)
                    {
                        maNH = int.Parse(dtNH.Rows[0]["MA_NHOMHANG"].ToString());
                        SetBackgroudOnClick(btnKV);
                    }
                    btnKV.Text = "" + dtNH.Rows[i]["TEN_NHOMHANG"].ToString();
                    try
                    {
                        byte[] tam = new byte[((byte[])dtNH.Rows[i]["HINHANH"]).LongLength];
                        tam = (byte[])dtNH.Rows[i]["HINHANH"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        btnKV.Image = bm;
                    }
                    catch
                    {
                    }
                    btnKV.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btnKV.ImageLocation = ImageLocation.TopCenter;
                    btnKV.RightToLeft = RightToLeft.No;
                    btnKV.Size = new Size(125, 80);
                    if (i != 0)
                    {
                        locateY += int.Parse(btnKV.Location.Y.ToString()) + 85;
                    }
                    btnKV.Location = new Point(locateX, locateY);
                    btnKV.Click += new EventHandler(btnKV_Click);
                    pnKV_Mon.Controls.Add(btnKV);
                }
            }
            catch
            {
            }
        }

        private void GetListMon()
        {
            dtMon.Clear();
            int width = (pnTable_Mon.Width / 110);
            int height = (pnTable_Mon.Height / 80);
            sizeMon = width * height;
            string sql = string.Format("Exec SelectMON @PageNumber={0},@PageSize={1}, @maNH={2}, @maKV={3},@maCH={4}", cPageMon, sizeMon, maNH, maKV, cls_ConfigCashier.idShop);
            dtMon = clsMain.ReturnDataTable(sql);

            while (pnTable_Mon.Controls.Count > 0)
            {
                pnTable_Mon.Controls[0].Dispose();
            }
            pnTable_Mon.Controls.Clear();
            int count = (pnTable_Mon.Width / 120) + 1;
            int line = 0;
            int locateY = 2;
            int locateX = 2;
            int x = 0;
            int y = 0;
            for (int i = 0; i < dtMon.Rows.Count; i++)
            {
                line++;
                SimpleButton btnBan_Mon = new SimpleButton();
                btnBan_Mon.Name = dtMon.Rows[i]["MA_HANGHOA"].ToString();
                btnBan_Mon.Text = dtMon.Rows[i]["TEN_HANGHOA"].ToString() + "\n" + "Giá:" + string.Format("{0:0,0 }", double.Parse(dtMon.Rows[i]["GIABAN"].ToString())) + "";
                btnBan_Mon.Tag = dtMon.Rows[i]["TEN_HANGHOA"].ToString() + " - " + "Giá:" + string.Format("{0:0,0 }", double.Parse(dtMon.Rows[i]["GIABAN"].ToString())) + "";
                try
                {
                    byte[] tam = new byte[((byte[])dtMon.Rows[i]["HINHANH"]).LongLength];
                    tam = (byte[])dtMon.Rows[i]["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    btnBan_Mon.Image = bm;
                }
                catch
                {
                }
                btnBan_Mon.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btnBan_Mon.ImageLocation = ImageLocation.TopCenter;
                btnBan_Mon.RightToLeft = RightToLeft.No;
                btnBan_Mon.Size = new Size(110, 80);
                if (line == count)
                {
                    locateY += int.Parse(btnBan_Mon.Location.Y.ToString()) + 85;
                    locateX = 2;
                    line = 1;
                }
                else
                {
                    if (i != 0)
                    {
                        if (this.Width < 1100)
                        {
                            locateX += int.Parse(btnBan_Mon.Location.X.ToString()) + 132;
                        }
                        else
                        {
                            locateX += int.Parse(btnBan_Mon.Location.X.ToString()) + 124;
                        }
                    }
                }
                btnBan_Mon.Location = new Point(locateX, locateY);
                btnBan_Mon.Click += new EventHandler(btnBan_Mon_Click);
                pnTable_Mon.Controls.Add(btnBan_Mon);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_UpKV_Click(object sender, EventArgs e)
        {
            if (funtions == 1)
            {
                if (cPageKV > 1)
                {
                    cPageKV -= 1;
                    GetKhuVuc();
                    GetSoDoBan();
                }
            }
            else if (funtions == 2)
            {
                if (cPageDish > 1)
                {
                    cPageDish -= 1;
                    GetNhomhang();
                    GetListMon();
                }
            }
        }

        private void btn_DowKV_Click(object sender, EventArgs e)
        {
            if (funtions == 1)
            {
                if (cPageKV < 20)
                {
                    cPageKV += 1;
                    GetKhuVuc();
                    GetSoDoBan();
                }
            }
            else if (funtions == 2)
            {
                if (cPageDish < 20)
                {
                    cPageDish += 1;
                    GetNhomhang();
                    GetListMon();
                }
            }
        }

        private void btn_NextToRight_Click(object sender, EventArgs e)
        {
            if (funtions == 1 || funtions == 5 || funtions == 6 || funtions == 11)
            {
                if (CurrenPages == 5)
                    return;
                CurrenPages = CurrenPages + 1;
                lbPageTable.Text = CurrenPages.ToString();
                GetSoDoBan();
            }
            else if (funtions == 2)
            {
                if (cPageMon == 20)
                    return;
                cPageMon = cPageMon + 1;
                lbPageTable.Text = cPageMon.ToString();
                GetListMon();
            }
        }

        private void btn_NextToLeft_Click(object sender, EventArgs e)
        {
            if (funtions == 1 || funtions == 5 || funtions == 6 || funtions == 11)
            {
                if (CurrenPages == 1)
                    return;
                CurrenPages = CurrenPages - 1;
                lbPageTable.Text = CurrenPages.ToString();
                GetSoDoBan();
            }
            if (funtions == 2)
            {
                if (cPageMon == 1)
                    return;
                cPageMon = cPageMon - 1;
                lbPageTable.Text = cPageMon.ToString();
                GetListMon();
            }
        }

        private bool CheckOpenTable(string idTable)
        {
            string sql = "select ISMO_BAN from BAN where MA_BAN =" + int.Parse(idTable) + "";
            DataTable checkTable = clsMain.ReturnDataTable(sql);
            if (checkTable.Rows.Count > 0 && bool.Parse(checkTable.Rows[0]["ISMO_BAN"].ToString()) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool CheckHaveISBANDAT(int idTable)
        {
            string sqlIS = "Select ISBANDAT From BAN Where MA_BAN =" + idTable + " And ISBANDAT = 1";
            DataTable tableIS = clsMain.ReturnDataTable(sqlIS);
            if (tableIS.Rows.Count > 0)
                return true;
            return false;
        }

        private bool SelectTablesCheckt(string idTable)
        {
            DateTime now;
            DataTable timeIn = clsMain.ReturnDataTable("Select HD.GIOVAO from HOADON HD inner join CT_MOBAN BM on HD.MA_HOADON = BM.MA_HOADON where BM.MA_BAN = " + idTable + " and HD.ISBANDAT = 1 ");
            if (timeIn.Rows.Count > 0)
            {
                for (int i = 0; i < timeIn.Rows.Count; i++)
                {
                    now = DateTime.Parse(timeIn.Rows[i]["GIOVAO"].ToString().Substring(0, 10));
                    DateTime x = DateTime.Parse(txtngay.ToString().Substring(0, 10));
                    int outline = DateTime.Compare(now, x);
                    if (outline == 0)
                    {
                        XtraMessageBox.Show("Bàn này đã có người đặt", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
            }
            return true;

        }

        private void SelectCheckTable(string idTable)
        {
            //Checked table select 
            //foreach (SimpleButton btnBan in flCheckTable.Controls)
            //{
            //    if (btnBan.Name == this.ActiveControl.Name)
            //    {
            //        if (btnBan.Name == maBan)
            //            throw new Exception("Bàn này đã được chọn !");
            //    }
            //}
            //Select Table
            string sql = "select MA_BAN, TEN_BAN, SOGHE, HINHANH from BAN where MA_BAN = " + idTable + "";
            DataTable dtCheckTable = clsMain.ReturnDataTable(sql);
            SimpleButton btnTables = new SimpleButton();
            btnTables.Name = dtCheckTable.Rows[0]["MA_BAN"].ToString();
            btnTables.Text = dtCheckTable.Rows[0]["TEN_BAN"].ToString() + " - " + dtCheckTable.Rows[0]["SOGHE"].ToString() + "  Ghế";
            btnTables.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            btnTables.ImageLocation = ImageLocation.TopCenter;
            try
            {
                byte[] tam = new byte[((byte[])dtCheckTable.Rows[0]["HINHANH"]).LongLength];
                tam = (byte[])dtCheckTable.Rows[0]["HINHANH"];
                MemoryStream ms = new MemoryStream(tam);
                Bitmap bm = new Bitmap(ms);
                btnTables.Image = bm;
            }
            catch
            {
            }
            btnTables.RightToLeft = RightToLeft.No;
            btnTables.Size = new Size(110, 80);
            //btnTables.Click += new EventHandler(btnTables_Click);
            //flCheckTable.Controls.Add(btnTables);
        }

        public bool XacNhanBanDat(string idTable)
        {
            //DateTime now;
            //string idBill = GetMaHoaDon(idTable);
            //DataTable timeIn = clsMain.ReturnDataTable("Select GIOVAO from HOADON where MA_HOADON ='" + idBill + "'");
            //if (timeIn.Rows.Count > 0)
            //    now = DateTime.Parse(timeIn.Rows[0]["GIOVAO"].ToString().Substring(0, 10));
            //else
            //    throw new Exception(_tgvaocuabannaykxd);
            //DateTime x = DateTime.Parse(txtngay.EditValue.ToString().Substring(0, 10));
            //int outline = DateTime.Compare(now, x);
            //if (outline == 1)
            //{
            //    XtraMessageBox.Show(_ngaydatbankhonghople, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    return false;
            //}
            //else
            //{
            //    if (cbHH.Text != "")
            //        if ((int.Parse(cbHH.Text) - int.Parse(DateTime.Now.Hour.ToString())) <= 5 && outline == 0)
            //        {
            //            XtraMessageBox.Show(_tgdenkhonghople, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //            return false;
            //        }
            //}
            return true;
        }
    }
}