﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES
{
    public partial class Frm_Combo_Option : DevExpress.XtraEditors.XtraForm
    {
        public string s_mahanghoa = "";
        public string s_tenhanghoa = "";
        public string s_soluong = "";
        public string s_phuthu = "";
        public string s_mabep = "";
        public string s_inbep = "";

        public Frm_Combo_Option(string maCombo,string maHanghoa)
        {
            InitializeComponent();
            LoadComboOption(maCombo, maHanghoa);
        }

        DataTable dt;
        public void LoadComboOption(string maCombo, string maHanghoa)
        {
            string sSQL = "";
            sSQL += "Select MA_HANGHOA_OPTION as MA,B.TEN_HANGHOA as TEN,A.SOLUONG,GIATHEM,B.MA_BEP,B.IS_INBEP" + "\n";
            sSQL += "From COMBO_OPTION A,HANGHOA B" + "\n";
            sSQL += "Where MA_HANGHOA_OPTION=b.MA_HANGHOA" + "\n";
            sSQL += "AND a.MA_COMBO=" + clsMain.SQLString(maCombo) + "\n";
            sSQL += "AND a.MA_HANGHOA=" + clsMain.SQLString(maHanghoa);
            sSQL += "Order by B.STT,B.TEN_HANGHOA";
            dt = clsMain.ReturnDataTable(sSQL);
            gridControl2.DataSource = dt;
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == STT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            s_mahanghoa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, gridView2.Columns["MA"]).ToString();
            s_tenhanghoa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, gridView2.Columns["TEN"]).ToString();
            s_soluong = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, gridView2.Columns["SOLUONG"]).ToString();
            s_phuthu = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, gridView2.Columns["GIATHEM"]).ToString();
            s_mabep = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, gridView2.Columns["MA_BEP"]).ToString();
            s_inbep = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, gridView2.Columns["IS_INBEP"]).ToString();
            this.Close();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_Combo_Option_Load(object sender, EventArgs e)
        {
            if (dt.Rows.Count > 2)
            {
                int i = dt.Rows.Count - 2;
                this.Height = this.Height + 20 + i * 40;
                if (this.Height > 768)
                {
                    this.Height = 768;
                    this.Location = new Point(this.Location.X, 5);
                }
                else if (this.Height < 350)
                {
                    this.Height = 350;
                    this.Location = new Point(this.Location.X, 5);
                }
                else
                {
                    this.Location = new Point(this.Location.X, this.Location.Y - ((i * 40) / 2));
                }
            }

            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            this.Text = rm.GetString("tuychinhcombo", culture);
            btnThoat.Text = rm.GetString("thoat", culture);
            gridView2.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView2.Columns["TEN"].Caption = rm.GetString("tenhanghoa", culture);
            gridView2.Columns["SOLUONG"].Caption = rm.GetString("soluong", culture);
            gridView2.Columns["GIATHEM"].Caption = rm.GetString("phuthu1", culture);
        }

        private void btn_UpFirst_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
            }
            catch
            {
            }
        }

        private void btn_up_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView2.FocusedRowHandle > 0)
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                }
            }
            catch
            {
            }
        }

        private void btn_Dow_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView2.FocusedRowHandle < gridView2.RowCount)
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                }
            }
            catch
            {
            }
        }

        private void btn_DowLast_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
            }
            catch
            {
            }
        }

       
    }
}