﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace KP_RES
{
    public partial  class Us_keyboard : DevExpress.XtraEditors.XtraUserControl
    {
        // biến nhận thông tin từ keyboard
        public static TextEdit _textbox;
        private int indexshift = 0;  
        
        public Us_keyboard()
        {
            InitializeComponent();
            reSizeKeyBoard();
        }

        private void reSizeKeyBoard()
        {
            int size = panelControl1.Width;
        }
       
        public  void SetTextbox(TextEdit edit)
        {
            _textbox = edit;
        }
    
        private void Click_buttonNumber(string number)
        {
            int select = _textbox.SelectionStart;
            if (_textbox.SelectionLength < 1)
            {
                _textbox.Text = _textbox.Text.Insert(_textbox.SelectionStart, number);
                _textbox.Focus();
                _textbox.SelectionStart = select + 1;
                _textbox.SelectionLength = 0;
            }
            else
            {
                int length = _textbox.SelectionLength;
                _textbox.Text = _textbox.Text.Remove(select, length);
                _textbox.Text = _textbox.Text.Insert(select, number);
                _textbox.Focus();
                _textbox.SelectionStart = select + 1;
                _textbox.SelectionLength = 0;
            }
        }
        
        private void btn_ESC_Click(object sender, EventArgs e)
        {
            _textbox.Text = "";
            _textbox.Focus();
        }

        private void btn_1_Click(object sender, EventArgs e)
        {
            switch (indexshift)
            {
                case 0:
                    InputKeyboardNumber("1");
                    break;
                case 1:
                    Click_buttonNumber("!");
                    break;
            }
        }

        private void btn_2_Click(object sender, EventArgs e)
        {
            switch (indexshift)
            {
                case 0:
                    InputKeyboardNumber("2");
                    break;
                case 1:
                    Click_buttonNumber("@");
                    break;
            }
        }

        private void btn_3_Click(object sender, EventArgs e)
        {
            switch (indexshift)
            {
                case 0:
                    InputKeyboardNumber("3");
                    break;
                case 1:
                    Click_buttonNumber("#");
                    break;
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            int iPosition = _textbox.SelectionStart;
            if (iPosition < _textbox.Text.Length)
            {
                if (iPosition < _textbox.Text.Length - 1)
                {
                    if (_textbox.Text[iPosition + 1].ToString() == ",")
                    {
                        _textbox.SelectionStart = iPosition + 2;
                    }
                    else
                    {
                        _textbox.SelectionStart = iPosition + 1;
                    }
                }
                else
                {
                    _textbox.SelectionStart = iPosition + 1;
                }
            }
            else
            {
                //_textbox.SelectionStart = 0;
            }
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            int iPosition = _textbox.SelectionStart;
            if (iPosition > 1)
            {
                if (_textbox.Text[iPosition - 1].ToString() == ",")
                {
                    _textbox.SelectionStart = iPosition - 2;
                }
                else
                {
                    _textbox.SelectionStart = iPosition - 1;
                }
            }
            else
            {
                _textbox.SelectionStart = 0;

            }
        }

        private void btn_4_Click(object sender, EventArgs e)
        {
            switch (indexshift)
            {
                case 0:
                    InputKeyboardNumber("4");
                    break;
                case 1:
                    Click_buttonNumber("$");
                    break;
            }
        }

        private void btn_5_Click(object sender, EventArgs e)
        {
            switch (indexshift)
            {
                case 0:
                    InputKeyboardNumber("5");
                    break;
                case 1:
                    Click_buttonNumber("%");
                    break;
            }
        }

        private void btn_6_Click(object sender, EventArgs e)
        {
            switch (indexshift)
            {
                case 0:
                    InputKeyboardNumber("6");
                    break;
                case 1:
                    Click_buttonNumber("^");
                    break;
            }
        }

        private void btn_7_Click(object sender, EventArgs e)
        {
            switch (indexshift)
            {
                case 0:
                    InputKeyboardNumber("7");
                    break;
                case 1:
                    Click_buttonNumber("&");
                    break;
            }
        }

        private void btn_8_Click(object sender, EventArgs e)
        {
            switch (indexshift)
            {
                case 0:
                    InputKeyboardNumber("8");
                    break;
                case 1:
                    Click_buttonNumber("*");
                    break;
            }
        }

        private void btn_9_Click(object sender, EventArgs e)
        {
            switch (indexshift)
            {
                case 0:
                    InputKeyboardNumber("9");
                    break;
                case 1:
                    Click_buttonNumber("(");
                    break;
            }
        }

        private void btn_0_Click(object sender, EventArgs e)
        {
            switch (indexshift)
            {
                case 0:
                    InputKeyboardNumber("0");
                    break;
                case 1:
                    Click_buttonNumber(")");
                    break;
            }
        }

        private void btn_enter_Click(object sender, EventArgs e)
        {
            SendKeys.Send("{Enter}");
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (_textbox.Name == "txtPartMoneyPayments")
                DeleteTextBox();
            else
            {
                int select = _textbox.SelectionStart;
                if (select != 0)
                {
                    if (_textbox.SelectionLength < 1)
                    {
                        _textbox.Text = _textbox.Text.Remove(_textbox.SelectionStart - 1, 1);
                        _textbox.Focus();
                        _textbox.SelectionStart = select - 1;
                        _textbox.SelectionLength = 0;
                    }
                    else
                    {
                        int length = _textbox.SelectionLength;
                        _textbox.Text = _textbox.Text.Remove(select, length);
                        _textbox.Focus();
                        _textbox.SelectionStart = select;
                        _textbox.SelectionLength = 0;
                    }
                }
                else
                {
                    _textbox.Focus();
                    _textbox.SelectionStart = 0;
                    _textbox.SelectionLength = 0;
                }
            }
        }

        private void DeleteTextBox()
        {

            int iPosition = _textbox.SelectionStart;
            int iLenght = _textbox.Text.Length;
            if (iPosition > 1)
            {
                if (_textbox.Text[iPosition - 1].ToString() == ",")
                {
                    _textbox.Text = _textbox.Text.Remove(iPosition - 2, 1);

                    _textbox.SelectionStart = iPosition - 2;

                }
                else
                {
                    _textbox.Text = _textbox.Text.Remove(iPosition - 1, 1);
                    if (iLenght - _textbox.Text.Length == 1)
                    {
                        _textbox.SelectionStart = iPosition - 1;
                    }
                    else
                    {
                        _textbox.SelectionStart = iPosition - 2;
                    }
                }
            }
            else
                if (iPosition == 1)
                {
                    if (iLenght >= 2)
                    {
                        _textbox.Text = _textbox.Text.Remove(iPosition - 1, 1);
                        _textbox.SelectionStart = iPosition - 1;
                        if (_textbox.Text == "0")
                            _textbox.SelectionStart = 1;
                    }
                    else
                    {
                        _textbox.Text = "";
                        _textbox.SelectionStart = 1;
                    }
                }
        }

        private void btn_A_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("A");
                        break;
                    case 1:
                        Click_buttonNumber("a");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("a");
                        break;
                    case 1:
                        Click_buttonNumber("A");
                        break;
                }
            }
        }

        private void btn_B_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("B");
                        break;
                    case 1:
                        Click_buttonNumber("b");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("b");
                        break;
                    case 1:
                        Click_buttonNumber("B");
                        break;
                }
            }
        }

        private void btn_C_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("C");
                        break;
                    case 1:
                        Click_buttonNumber("c");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("c");
                        break;
                    case 1:
                        Click_buttonNumber("C");
                        break;
                }
            }
        }

        private void btn_D_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("D");
                        break;
                    case 1:
                        Click_buttonNumber("d");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("d");
                        break;
                    case 1:
                        Click_buttonNumber("D");
                        break;
                }
            }
        }

        private void btn_E_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("E");
                        break;
                    case 1:
                        Click_buttonNumber("e");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("e");
                        break;
                    case 1:
                        Click_buttonNumber("E");
                        break;
                }
            }
        }

        private void btn_F_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("F");
                        break;
                    case 1:
                        Click_buttonNumber("f");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("f");
                        break;
                    case 1:
                        Click_buttonNumber("F");
                        break;
                }
            }
        }

        private void btn_G_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("G");
                        break;
                    case 1:
                        Click_buttonNumber("g");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("g");
                        break;
                    case 1:
                        Click_buttonNumber("G");
                        break;
                }
            }
        }

        private void btn_H_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("H");
                        break;
                    case 1:
                        Click_buttonNumber("h");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("h");
                        break;
                    case 1:
                        Click_buttonNumber("H");
                        break;
                }
            }
        }

        private void btn_I_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("I");
                        break;
                    case 1:
                        Click_buttonNumber("i");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("i");
                        break;
                    case 1:
                        Click_buttonNumber("I");
                        break;
                }
            }
        }

        private void btn_J_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("J");
                        break;
                    case 1:
                        Click_buttonNumber("j");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("j");
                        break;
                    case 1:
                        Click_buttonNumber("J");
                        break;
                }
            }
        }

        private void btn_K_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("K");
                        break;
                    case 1:
                        Click_buttonNumber("k");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("k");
                        break;
                    case 1:
                        Click_buttonNumber("K");
                        break;
                }
            }
        }

        private void btn_L_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("L");
                        break;
                    case 1:
                        Click_buttonNumber("l");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("l");
                        break;
                    case 1:
                        Click_buttonNumber("L");
                        break;
                }
            }
        }

        private void btn_M_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("M");
                        break;
                    case 1:
                        Click_buttonNumber("m");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("m");
                        break;
                    case 1:
                        Click_buttonNumber("M");
                        break;
                }
            }
        }

        private void btn_N_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("N");
                        break;
                    case 1:
                        Click_buttonNumber("n");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("n");
                        break;
                    case 1:
                        Click_buttonNumber("N");
                        break;
                }
            }
        }

        private void btn_O_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("O");
                        break;
                    case 1:
                        Click_buttonNumber("o");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("o");
                        break;
                    case 1:
                        Click_buttonNumber("O");
                        break;
                }
            }
        }

        private void btn_P_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("P");
                        break;
                    case 1:
                        Click_buttonNumber("p");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("p");
                        break;
                    case 1:
                        Click_buttonNumber("P");
                        break;
                }
            }
        }

        private void btn_Q_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("Q");
                        break;
                    case 1:
                        Click_buttonNumber("q");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("q");
                        break;
                    case 1:
                        Click_buttonNumber("Q");
                        break;
                }
            }
        }

        private void btn_R_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("R");
                        break;
                    case 1:
                        Click_buttonNumber("r");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("r");
                        break;
                    case 1:
                        Click_buttonNumber("R");
                        break;
                }
            }
        }

        private void btn_S_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("S");
                        break;
                    case 1:
                        Click_buttonNumber("s");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("s");
                        break;
                    case 1:
                        Click_buttonNumber("S");
                        break;
                }
            }
        }

        private void btn_T_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("T");
                        break;
                    case 1:
                        Click_buttonNumber("t");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("t");
                        break;
                    case 1:
                        Click_buttonNumber("T");
                        break;
                }
            }
        }

        private void btn_U_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("U");
                        break;
                    case 1:
                        Click_buttonNumber("u");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("u");
                        break;
                    case 1:
                        Click_buttonNumber("U");
                        break;
                }
            }
        }

        private void btn_V_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("V");
                        break;
                    case 1:
                        Click_buttonNumber("v");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("v");
                        break;
                    case 1:
                        Click_buttonNumber("V");
                        break;
                }
            }
        }

        private void btn_W_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("W");
                        break;
                    case 1:
                        Click_buttonNumber("w");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("w");
                        break;
                    case 1:
                        Click_buttonNumber("W");
                        break;
                }
            }
        }

        private void btn_Y_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("Y");
                        break;
                    case 1:
                        Click_buttonNumber("y");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("y");
                        break;
                    case 1:
                        Click_buttonNumber("Y");
                        break;
                }
            }
        }

        private void btn_Z_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("Z");
                        break;
                    case 1:
                        Click_buttonNumber("z");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("z");
                        break;
                    case 1:
                        Click_buttonNumber("Z");
                        break;
                }
            }
        }

        private void btn_SLeft_Click(object sender, EventArgs e)
        {
            switch (indexshift)
            {
                case 0:
                    Click_buttonNumber("\\");
                    break;
                case 1:
                    Click_buttonNumber("|");
                    break;
            }
        }

        private void btn_Stop_Click(object sender, EventArgs e)
        {
            switch (indexshift)
            {
                case 0:
                    Click_buttonNumber(".");
                    break;
                case 1:
                    Click_buttonNumber(">");
                    break;
            }
        }

        private void btn_SRight_Click(object sender, EventArgs e)
        {
            switch (indexshift)
            {
                case 0:
                    Click_buttonNumber("/");
                    break;
                case 1:
                    Click_buttonNumber("?");
                    break;
            }
        }

        private void btn_X_Click(object sender, EventArgs e)
        {
            if (flag_CapsLock)
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("X");
                        break;
                    case 1:
                        Click_buttonNumber("x");
                        break;
                }
            }
            else
            {
                switch (indexshift)
                {
                    case 0:
                        Click_buttonNumber("x");
                        break;
                    case 1:
                        Click_buttonNumber("X");
                        break;
                }
            }
        }

        private void btn_Cross_Click(object sender, EventArgs e)
        {
            switch (indexshift)
            {
                case 0:
                    Click_buttonNumber("-");
                    break;
                case 1:
                    Click_buttonNumber("_");
                    break;
            }
        }

        private void btn_Leftcurlybrackets_Click(object sender, EventArgs e)
        {
            switch (indexshift)
            {
                case 0:
                    Click_buttonNumber("[");
                    break;
                case 1:
                    Click_buttonNumber("{");
                    break;
            }
        }

        private void btn_Rightcurlybrackets_Click(object sender, EventArgs e)
        {
            switch (indexshift)
            {
                case 0:
                    Click_buttonNumber("]");
                    break;
                case 1:
                    Click_buttonNumber("}");
                    break;
            }
        }

        private void btn_colon_Click(object sender, EventArgs e)
        {
            switch (indexshift)
            {
                case 0:
                    Click_buttonNumber(";");
                    break;
                case 1:
                    Click_buttonNumber(":");
                    break;
            }
        }

        private void btn_quotationmarks_Click(object sender, EventArgs e)
        {
            switch (indexshift)
            {
                case 0:
                    Click_buttonNumber("'");
                    break;
                case 1:
                    Click_buttonNumber("\"");
                    break;
            }
        }

        private void btn_Space_Click(object sender, EventArgs e)
        {
            Click_buttonNumber(" ");
        }

        private void btn_Comma_Click(object sender, EventArgs e)
        {
            switch (indexshift)
            {
                case 0:
                    Click_buttonNumber(",");
                    break;
                case 1:
                    Click_buttonNumber("<");
                    break;
            }
        }

        private void btn_Shift_Click(object sender, EventArgs e)
        {
            if (indexshift == 0)
                indexshift = 1;
            else if (indexshift == 1)
                indexshift = 0;

            if (indexshift == 1)
            {
                btn_Shift.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
                btn_Shift.ForeColor = Color.DarkOrange;
            }
            else
            {
                btn_Shift.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
                btn_Shift.ForeColor = Color.DarkOliveGreen;
            }
        }

        private void btnshift2_Click(object sender, EventArgs e)
        {
            Click_buttonNumber(".");
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            switch (indexshift)
            {
                case 0:
                    Click_buttonNumber("=");
                    break;
                case 1:
                    Click_buttonNumber("+");
                    break;
            }
        }

        bool flag_CapsLock = false;
        private void btnCapsLock_Click(object sender, EventArgs e)
        {
            if (!flag_CapsLock)
            {
                btnCapsLock.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
                btnCapsLock.ForeColor = Color.DarkOrange;
                flag_CapsLock = true;
                btn_Q.Text = "Q";
                btn_W.Text = "W";
                btn_E.Text = "E";
                btn_R.Text = "R";
                btn_T.Text = "T";
                btn_Y.Text = "Y";
                btn_U.Text = "U";
                btn_I.Text = "I";
                btn_O.Text = "O";
                btn_P.Text = "P";
                btn_A.Text = "A";
                btn_S.Text = "S";
                btn_D.Text = "D";
                btn_F.Text = "F";
                btn_G.Text = "G";
                btn_H.Text = "H";
                btn_J.Text = "J";
                btn_K.Text = "K";
                btn_L.Text = "L";
                btn_Z.Text = "Z";
                btn_X.Text = "X";
                btn_C.Text = "C";
                btn_V.Text = "V";
                btn_B.Text = "B";
                btn_N.Text = "N";
                btn_M.Text = "M";
            }
            else
            {
                btnCapsLock.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
                btnCapsLock.ForeColor = Color.Empty;
                flag_CapsLock = false;
                btn_Q.Text = "q";
                btn_W.Text = "w";
                btn_E.Text = "e";
                btn_R.Text = "r";
                btn_T.Text = "t";
                btn_Y.Text = "y";
                btn_U.Text = "u";
                btn_I.Text = "i";
                btn_O.Text = "o";
                btn_P.Text = "p";
                btn_A.Text = "a";
                btn_S.Text = "s";
                btn_D.Text = "d";
                btn_F.Text = "f";
                btn_G.Text = "g";
                btn_H.Text = "h";
                btn_J.Text = "j";
                btn_K.Text = "k";
                btn_L.Text = "l";
                btn_Z.Text = "z";
                btn_X.Text = "x";
                btn_C.Text = "c";
                btn_V.Text = "v";
                btn_B.Text = "b";
                btn_N.Text = "n";
                btn_M.Text = "m";
            }
        }

        private void InputKeyboardNumber(string sInput)
        {
            if (_textbox.Name == "txtPartMoneyPayments")
            {
                if (_textbox.Text.Length >= 15)
                {
                    int iPositon = _textbox.SelectionStart;
                    XtraMessageBox.Show("Bạn không được nhập quá 15 ký tự", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _textbox.SelectionStart = _textbox.Text.Length;
                    _textbox.SelectionStart = iPositon;
                    return;
                }

                if (_textbox.Text == "0")
                {
                    if (sInput == "0" || sInput == "00" || sInput == "000")
                        return;
                }
                if (_textbox.SelectedText == _textbox.Text)
                {
                    _textbox.Text = "0";
                }
                int iLenght, iPosition;
                iLenght = _textbox.Text.Length;
                iPosition = _textbox.SelectionStart;
                if (iPosition == 0)
                {
                    if (sInput == "0" || sInput == "00" || sInput == "000")
                        return;
                }

                if (_textbox.Text == "0")
                {
                    _textbox.Text = sInput;
                }
                else
                {
                    _textbox.Text = _textbox.Text.Insert(iPosition, sInput);
                }
                if (sInput.Length == 1)
                {
                    if (_textbox.Text.Length - iLenght == 1)
                    {
                        _textbox.SelectionStart = iPosition + 1;
                    }
                    else
                    {
                        _textbox.SelectionStart = iPosition + 2;
                    }
                }
                else
                    if (sInput.Length == 2)
                    {
                        if (_textbox.Text.Length - iLenght == 2)
                        {
                            _textbox.SelectionStart = iPosition + 2;
                        }
                        else
                        {
                            _textbox.SelectionStart = iPosition + 3;
                        }
                    }
                    else
                        if (sInput.Length == 3)
                        {
                            if (_textbox.Text.Length - iLenght == 3)
                            {
                                _textbox.SelectionStart = iPosition + 3;
                            }
                            else
                            {
                                _textbox.SelectionStart = iPosition + 4;
                            }
                        }
            }
            else
            {
                Click_buttonNumber(sInput);
            }

        }

        private void btncheo_Click(object sender, EventArgs e)
        {
            Click_buttonNumber("/");
        }
    }
}
