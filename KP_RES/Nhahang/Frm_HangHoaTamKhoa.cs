﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using DevExpress.UserSkins;
using DevExpress.XtraEditors;
using System.IO;
using WMPLib;
using KP_UserManagement;
using System.Data.SqlClient;
using DevExpress.Utils;

namespace  KP_RES 
{
    public partial class Frm_HangHoaTamKhoa : XtraForm
    {
        int sCount;
        int tonghinhtrentrang;
        int sotrang;
        int sTrang = 1;
        string sMaNhom;
        int bienfocus;

        public Frm_HangHoaTamKhoa()
        {
            InitializeComponent();
            LoadNhomHang();
        }

        private void LoadData(string sMaNhom)
        {
            try
            {
                DataTable myDT = clsMain.ReturnDataTable("SELECT A.MA_HANGHOA FROM HANGHOA A INNER JOIN NHOMHANG B ON A.MA_NHOMHANG = B.MA_NHOMHANG WHERE A.SUDUNG = 1 AND B.THUCDON = 1 AND A.MA_NHOMHANG = " + clsMain .SQLString ( sMaNhom));
                if (myDT.Rows.Count > 0)
                    sCount = myDT.Rows.Count;
                int sWidth = Screen.PrimaryScreen.WorkingArea.Width - 140; //140 panel phai
                int sHeight = Screen.PrimaryScreen.WorkingArea.Height - 60; //2 panel chuyển trang
                int pWidth = sWidth / 155;
                int pHeight = sHeight / 85;
                tonghinhtrentrang = pWidth * pHeight;
                sotrang = sCount / tonghinhtrentrang;
                if (sCount % tonghinhtrentrang != 0)
                    sotrang++;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void LoadImage(int tonghinhtrentrang, int trang, string sMaNhom)
        {
            try
            {
                pnlNhomhang.Controls.Clear();
                String sSQL = "EXEC SP_SelectHangHoaTamKhoa ";
                sSQL += (tonghinhtrentrang * (trang - 1)) + ",";
                sSQL += (tonghinhtrentrang * trang) + ",";
                sSQL += clsMain.SQLString ( sMaNhom);
                DataTable myDT = clsMain.ReturnDataTable(sSQL);
                foreach (DataRow dr in myDT.Rows)
                {
                    Panel palGroup = new Panel();
                    Panel palBtn = new Panel();
                    Panel palChk = new Panel();

                    CheckEdit chk = new CheckEdit();

                    SimpleButton btn = new SimpleButton();
                    btn.Font = new Font("Tahoma", 10);
                    btn.Appearance.TextOptions.WordWrap =WordWrap.Wrap;
                    btn.ImageLocation = ImageLocation.TopCenter;
                    btn.Image = global::KP_RES.Properties.Resources.vegetarian_food_26;
                    btn.Name = dr["MA_HANGHOA"].ToString();
                    btn.Text = dr["TEN_HANGHOA"].ToString();
                    btn.Click += new EventHandler(HangHoa_Click);

                    chk.Name = "chk" + dr["MA_HANGHOA"].ToString();
                    chk.Text = dr["TEN_HANGHOA"].ToString();
                    chk.Properties.ReadOnly = true;
                    chk.Checked = (Boolean)dr["IS_TAMKHOA"];
                    if ((Boolean)dr["IS_TAMKHOA"])
                        //Backgroud(btn, false);
                        SetBackgroudOnClick(btn);
                    else
                        //Backgroud(btn, true);
                        SetDefautlBackgroud(btn);

                    palGroup.Name = "G" + dr["MA_HANGHOA"].ToString();
                    palBtn.Name = "B" + dr["MA_HANGHOA"].ToString();
                    palChk.Name = "C" + dr["MA_HANGHOA"].ToString();

                    palGroup.Width = 140;
                    palGroup.Height = 75;

                    palChk.Width = 20;

                    palChk.Dock = DockStyle.Right;
                    palBtn.Dock = DockStyle.Fill;
                    btn.Dock = DockStyle.Fill;
                    chk.Dock = DockStyle.Fill;

                    palChk.Controls.Add(chk);
                    palBtn.Controls.Add(btn);
                    palGroup.Controls.Add(palBtn);
                    palGroup.Controls.Add(palChk);

                    pnlNhomhang.Controls.Add(palGroup);
                }
                lblTrang.Text = trang + "/" + sotrang;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void HangHoa_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            foreach (Panel pn in pnlNhomhang.Controls)
            {
                if (pn.Name == "G" + btn.Name)
                {
                    foreach (Panel pn1 in pn.Controls)
                    {
                        if (pn1.Name.Substring(0, 1) == "C")
                        {
                            foreach (CheckEdit chk1 in pn1.Controls)
                            {
                                if (btn.Appearance.BackColor == btnLenHangHoa.Appearance.BackColor)
                                {
                                    chk1.Checked = true;
                                    SetBackgroudOnClick(btn);
                                    //Backgroud(btn, false);
                                    String sSQL = "UPDATE HANGHOA SET IS_TAMKHOA = 1 WHERE MA_HANGHOA = " + clsMain.SQLString(btn.Name);
                                    clsMain.ExecuteSQL(sSQL);
                                }
                                else
                                {
                                    chk1.Checked = false;
                                    SetDefautlBackgroud(btn);
                                    //Backgroud(btn,true);
                                    String sSQL = "UPDATE HANGHOA SET IS_TAMKHOA = 0 WHERE MA_HANGHOA = " + clsMain.SQLString(btn.Name);
                                    clsMain.ExecuteSQL(sSQL);
                                }
                                return;
                            }
                        }
                    }
                }
            }
        }

        private void btnLenHangHoa_Click(object sender, EventArgs e)
        {
            if (sTrang > 1)
            {
                sTrang--;
                LoadImage(tonghinhtrentrang, sTrang, sMaNhom);
            }
        }

        private void btnXuongHangHoa_Click(object sender, EventArgs e)
        {
            if (sTrang < sotrang)
            {
                sTrang++;
                LoadImage(tonghinhtrentrang, sTrang, sMaNhom);
            }
        }

        private void LoadNhomHang()
        {
            try
            {
                lpnNhom.Controls.Clear();
                String sSQL = "SELECT ROW_NUMBER() OVER(ORDER BY TEN_NHOMHANG) AS STT, MA_NHOMHANG, TEN_NHOMHANG ";
                sSQL += "FROM NHOMHANG ";
                sSQL += "WHERE THUCDON = 1 AND SUDUNG = 1";
                DataTable myDT = clsMain.ReturnDataTable(sSQL);
                foreach (DataRow myDR in myDT.Rows)
                {
                    SimpleButton btn = new SimpleButton();
                    btn.Width = 110;
                    btn.Height = 74;
                    btn.Font = new Font("Tahoma", 10);
                    btn.Appearance.TextOptions.WordWrap = WordWrap.Wrap;
                    btn.Name = myDR["MA_NHOMHANG"].ToString();
                    btn.Text = myDR["TEN_NHOMHANG"].ToString();
                    btn.Tag = myDR["STT"].ToString();
                    btn.Click += new EventHandler(Nhomhang_Click);
                    lpnNhom.Controls.Add(btn);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Nhomhang_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in lpnNhom.Controls)
            {                
                if (btn1.Name == btn.Name)
                {
                    sTrang = 1;
                    bienfocus = Convert.ToInt32(btn.Tag);
                    sMaNhom = btn.Name;
                    LoadData(sMaNhom);
                    LoadImage(tonghinhtrentrang, sTrang, sMaNhom);
                }
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = Color.Black;
                }
            }
        }

        private void Frm_HangHoaTamKhoa_Load(object sender, EventArgs e)
        {
            if (lpnNhom.Controls.Count > 0)
            {
                Nhomhang_Click(lpnNhom.Controls[0], null);
            }
        }

        private void btnLenNhomhang_Click(object sender, EventArgs e)
        {
            try{
                lpnNhom.AutoScrollPosition = new Point(0, lpnNhom.VerticalScroll.Value - lpnNhom.VerticalScroll.Maximum / 10);
            }
            catch{
            }
        }

        private void btnXuongNhomhang_Click(object sender, EventArgs e)
        {
            try{
                lpnNhom.AutoScrollPosition = new Point(0, lpnNhom.VerticalScroll.Value + lpnNhom.VerticalScroll.Maximum / 10);
            }
            catch{
            }
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            LoadNhomHang();
            Nhomhang_Click(lpnNhom.Controls[bienfocus - 1], null);
        }

        private void SetBackgroudOnClick(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.YellowGreen;
            _btn.Appearance.BackColor2 = Color.YellowGreen;
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;

        }

        private void SetDefautlBackgroud(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.Empty;
            if (_btn.Appearance.Options.UseBackColor == true)
                _btn.Appearance.Options.UseBackColor = false;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == false)
                _btn.LookAndFeel.UseDefaultLookAndFeel = true;
        }
    }
}