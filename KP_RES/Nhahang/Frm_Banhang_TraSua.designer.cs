﻿namespace KP_RES
{
    partial class Frm_Banhang_TraSua
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Banhang_TraSua));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.pnHeader = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.lb_Quay = new DevExpress.XtraEditors.LabelControl();
            this.lb_Kho = new DevExpress.XtraEditors.LabelControl();
            this.lbgiay = new DevExpress.XtraEditors.LabelControl();
            this.lbCaBan = new DevExpress.XtraEditors.LabelControl();
            this.lbGio = new DevExpress.XtraEditors.LabelControl();
            this.lb_NhanVien = new DevExpress.XtraEditors.LabelControl();
            this.lbtimeby = new DevExpress.XtraEditors.LabelControl();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnShowHang = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.pnTable_Mon = new DevExpress.XtraEditors.XtraScrollableControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.lbPageTable = new DevExpress.XtraEditors.LabelControl();
            this.btn_NextRight = new DevExpress.XtraEditors.SimpleButton();
            this.btn_NextToLeft = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl37 = new DevExpress.XtraEditors.PanelControl();
            this.lbNameMon = new DevExpress.XtraEditors.LabelControl();
            this.txtID = new DevExpress.XtraEditors.LookUpEdit();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pnKV_Mon = new DevExpress.XtraEditors.PanelControl();
            this.panelControl28 = new DevExpress.XtraEditors.PanelControl();
            this.btn_UpKV = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl27 = new DevExpress.XtraEditors.PanelControl();
            this.btn_DowKV = new DevExpress.XtraEditors.SimpleButton();
            this.pnMain = new DevExpress.XtraEditors.PanelControl();
            this.pn_listProduct = new DevExpress.XtraEditors.PanelControl();
            this.gr_SelectProduct = new DevExpress.XtraGrid.GridControl();
            this.gv_SelectProduct = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SOTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIABAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CHIETKHAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Thanh_Tien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_BEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IS_COMBO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IS_INBEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IS_KHUYENMAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.INTEM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MONTHEM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MACP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MACB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnUpDowGv = new DevExpress.XtraEditors.PanelControl();
            this.btn_up = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Dow = new DevExpress.XtraEditors.SimpleButton();
            this.btn_DowLast = new DevExpress.XtraEditors.SimpleButton();
            this.btn_UpFirst = new DevExpress.XtraEditors.SimpleButton();
            this.pnBtnDeleteGv = new DevExpress.XtraEditors.PanelControl();
            this.panelControl29 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl33 = new DevExpress.XtraEditors.PanelControl();
            this.btn_tru = new DevExpress.XtraEditors.SimpleButton();
            this.btn_XoaTatCa = new DevExpress.XtraEditors.SimpleButton();
            this.btn_cong = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btn_xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btn9 = new DevExpress.XtraEditors.SimpleButton();
            this.btn8 = new DevExpress.XtraEditors.SimpleButton();
            this.btn7 = new DevExpress.XtraEditors.SimpleButton();
            this.btn6 = new DevExpress.XtraEditors.SimpleButton();
            this.btn5 = new DevExpress.XtraEditors.SimpleButton();
            this.btn4 = new DevExpress.XtraEditors.SimpleButton();
            this.btn3 = new DevExpress.XtraEditors.SimpleButton();
            this.btn2 = new DevExpress.XtraEditors.SimpleButton();
            this.btn1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn0 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl31 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl32 = new DevExpress.XtraEditors.PanelControl();
            this.lbSoluong = new DevExpress.XtraEditors.LabelControl();
            this.lb_soluong = new DevExpress.XtraEditors.LabelControl();
            this.panelControl20 = new DevExpress.XtraEditors.PanelControl();
            this.btnComboOption = new DevExpress.XtraEditors.SimpleButton();
            this.pnlInforMoney = new DevExpress.XtraEditors.PanelControl();
            this.panelControl50 = new DevExpress.XtraEditors.PanelControl();
            this.lbTotal = new DevExpress.XtraEditors.LabelControl();
            this.panelControl23 = new DevExpress.XtraEditors.PanelControl();
            this.lbGiamGia = new DevExpress.XtraEditors.LabelControl();
            this.lb_phuthu = new DevExpress.XtraEditors.LabelControl();
            this.lb_tongcong = new DevExpress.XtraEditors.LabelControl();
            this.panelControl13 = new DevExpress.XtraEditors.PanelControl();
            this.lbPhuThu = new DevExpress.XtraEditors.LabelControl();
            this.lb_giamgia = new DevExpress.XtraEditors.LabelControl();
            this.pnNghiepVu_ThuNgan = new DevExpress.XtraEditors.PanelControl();
            this.pnDacThu = new DevExpress.XtraEditors.PanelControl();
            this.btnChietkhau100 = new DevExpress.XtraEditors.SimpleButton();
            this.btnInbilldoi = new DevExpress.XtraEditors.SimpleButton();
            this.btnGoikhach = new DevExpress.XtraEditors.SimpleButton();
            this.btnSuaGia = new DevExpress.XtraEditors.SimpleButton();
            this.btnGiaBan2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnInVeOnline = new DevExpress.XtraEditors.SimpleButton();
            this.pnThuNgan = new DevExpress.XtraEditors.PanelControl();
            this.btn_YeuCauThem = new DevExpress.XtraEditors.SimpleButton();
            this.btn_MonThem = new DevExpress.XtraEditors.SimpleButton();
            this.btn_InLaiHoaDon = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoKet = new DevExpress.XtraEditors.SimpleButton();
            this.btnChietkhau = new DevExpress.XtraEditors.SimpleButton();
            this.btn_logout = new DevExpress.XtraEditors.SimpleButton();
            this.btn_HuyHoaDon = new DevExpress.XtraEditors.SimpleButton();
            this.btnKetKa = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Payment = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.btn_DatThu = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ThuNgan = new DevExpress.XtraEditors.SimpleButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pnInlaihoadon = new DevExpress.XtraEditors.PanelControl();
            this.panelControl53 = new DevExpress.XtraEditors.PanelControl();
            this.gr_billPaymented = new DevExpress.XtraGrid.GridControl();
            this.gvBillPaymented = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SSTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HOADON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYTAO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_KHUVUC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_BAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HUY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LAYLAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtMaHoaDon = new DevExpress.XtraEditors.TextEdit();
            this.btnHuy = new DevExpress.XtraEditors.SimpleButton();
            this.lb_maKH = new DevExpress.XtraEditors.LabelControl();
            this.panelControl52 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.lbTongSoRow = new DevExpress.XtraEditors.LabelControl();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.btn_upbill = new DevExpress.XtraEditors.SimpleButton();
            this.btnDowBill = new DevExpress.XtraEditors.SimpleButton();
            this.btnDowLastBill = new DevExpress.XtraEditors.SimpleButton();
            this.btn_UpTopBill = new DevExpress.XtraEditors.SimpleButton();
            this.pnWapper = new DevExpress.XtraEditors.PanelControl();
            this.pnYeucauthem = new DevExpress.XtraEditors.PanelControl();
            this.panelControl19 = new DevExpress.XtraEditors.PanelControl();
            this.pnYeuCau = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl22 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton23 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton24 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.us_keyboard3 = new KP_RES.Us_keyboard();
            this.panelControl21 = new DevExpress.XtraEditors.PanelControl();
            this.txtYeuCauThem = new DevExpress.XtraEditors.TextEdit();
            this.panelControl17 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.lbMonYeuCauThem = new DevExpress.XtraEditors.LabelControl();
            this.lbTableYeuCauThem = new DevExpress.XtraEditors.LabelControl();
            this.lbkvYeuCauThem = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnHeader)).BeginInit();
            this.pnHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnShowHang)).BeginInit();
            this.pnShowHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl37)).BeginInit();
            this.panelControl37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnKV_Mon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl28)).BeginInit();
            this.panelControl28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl27)).BeginInit();
            this.panelControl27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnMain)).BeginInit();
            this.pnMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_listProduct)).BeginInit();
            this.pn_listProduct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_SelectProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_SelectProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDowGv)).BeginInit();
            this.pnUpDowGv.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnBtnDeleteGv)).BeginInit();
            this.pnBtnDeleteGv.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl29)).BeginInit();
            this.panelControl29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl33)).BeginInit();
            this.panelControl33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl31)).BeginInit();
            this.panelControl31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl32)).BeginInit();
            this.panelControl32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl20)).BeginInit();
            this.panelControl20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInforMoney)).BeginInit();
            this.pnlInforMoney.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl50)).BeginInit();
            this.panelControl50.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl23)).BeginInit();
            this.panelControl23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).BeginInit();
            this.panelControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnNghiepVu_ThuNgan)).BeginInit();
            this.pnNghiepVu_ThuNgan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnDacThu)).BeginInit();
            this.pnDacThu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnThuNgan)).BeginInit();
            this.pnThuNgan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnInlaihoadon)).BeginInit();
            this.pnInlaihoadon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl53)).BeginInit();
            this.panelControl53.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_billPaymented)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBillPaymented)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHoaDon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl52)).BeginInit();
            this.panelControl52.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnWapper)).BeginInit();
            this.pnWapper.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnYeucauthem)).BeginInit();
            this.pnYeucauthem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).BeginInit();
            this.panelControl19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).BeginInit();
            this.panelControl22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl21)).BeginInit();
            this.panelControl21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtYeuCauThem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).BeginInit();
            this.panelControl17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            this.SuspendLayout();
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit1.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // repositoryItemButtonEdit2
            // 
            this.repositoryItemButtonEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit2.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
            this.repositoryItemButtonEdit2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // repositoryItemButtonEdit3
            // 
            this.repositoryItemButtonEdit3.AutoHeight = false;
            this.repositoryItemButtonEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit3.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", null, null, true)});
            this.repositoryItemButtonEdit3.Name = "repositoryItemButtonEdit3";
            this.repositoryItemButtonEdit3.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // pnHeader
            // 
            this.pnHeader.Controls.Add(this.panelControl6);
            this.pnHeader.Controls.Add(this.panelControl10);
            this.pnHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnHeader.Location = new System.Drawing.Point(0, 0);
            this.pnHeader.Name = "pnHeader";
            this.pnHeader.Size = new System.Drawing.Size(1036, 70);
            this.pnHeader.TabIndex = 0;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.lb_Quay);
            this.panelControl6.Controls.Add(this.lb_Kho);
            this.panelControl6.Controls.Add(this.lbgiay);
            this.panelControl6.Controls.Add(this.lbCaBan);
            this.panelControl6.Controls.Add(this.lbGio);
            this.panelControl6.Controls.Add(this.lb_NhanVien);
            this.panelControl6.Controls.Add(this.lbtimeby);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(198, 2);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(836, 66);
            this.panelControl6.TabIndex = 3;
            // 
            // lb_Quay
            // 
            this.lb_Quay.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_Quay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_Quay.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_Quay.Location = new System.Drawing.Point(15, 5);
            this.lb_Quay.Name = "lb_Quay";
            this.lb_Quay.Size = new System.Drawing.Size(41, 19);
            this.lb_Quay.TabIndex = 14;
            this.lb_Quay.Text = "Quầy";
            // 
            // lb_Kho
            // 
            this.lb_Kho.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_Kho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_Kho.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_Kho.Location = new System.Drawing.Point(15, 34);
            this.lb_Kho.Name = "lb_Kho";
            this.lb_Kho.Size = new System.Drawing.Size(41, 19);
            this.lb_Kho.TabIndex = 15;
            this.lb_Kho.Text = "Quầy";
            // 
            // lbgiay
            // 
            this.lbgiay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbgiay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbgiay.Location = new System.Drawing.Point(795, 27);
            this.lbgiay.Name = "lbgiay";
            this.lbgiay.Size = new System.Drawing.Size(18, 19);
            this.lbgiay.TabIndex = 19;
            this.lbgiay.Text = "00";
            // 
            // lbCaBan
            // 
            this.lbCaBan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCaBan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbCaBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbCaBan.Location = new System.Drawing.Point(728, 5);
            this.lbCaBan.Name = "lbCaBan";
            this.lbCaBan.Size = new System.Drawing.Size(94, 19);
            this.lbCaBan.TabIndex = 17;
            this.lbCaBan.Text = "21-12-2012";
            // 
            // lbGio
            // 
            this.lbGio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGio.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGio.Location = new System.Drawing.Point(743, 34);
            this.lbGio.Name = "lbGio";
            this.lbGio.Size = new System.Drawing.Size(46, 19);
            this.lbGio.TabIndex = 20;
            this.lbGio.Text = "00:00";
            // 
            // lb_NhanVien
            // 
            this.lb_NhanVien.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lb_NhanVien.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_NhanVien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_NhanVien.Location = new System.Drawing.Point(363, 5);
            this.lb_NhanVien.Name = "lb_NhanVien";
            this.lb_NhanVien.Size = new System.Drawing.Size(89, 19);
            this.lb_NhanVien.TabIndex = 18;
            this.lb_NhanVien.Text = "Nhân Viên:";
            // 
            // lbtimeby
            // 
            this.lbtimeby.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbtimeby.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbtimeby.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbtimeby.Location = new System.Drawing.Point(363, 38);
            this.lbtimeby.Name = "lbtimeby";
            this.lbtimeby.Size = new System.Drawing.Size(21, 19);
            this.lbtimeby.TabIndex = 16;
            this.lbtimeby.Text = "Ca";
            // 
            // panelControl10
            // 
            this.panelControl10.Controls.Add(this.pictureBox1);
            this.panelControl10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl10.Location = new System.Drawing.Point(2, 2);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(196, 66);
            this.panelControl10.TabIndex = 21;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(8, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(177, 41);
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // pnShowHang
            // 
            this.pnShowHang.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnShowHang.Controls.Add(this.panelControl2);
            this.pnShowHang.Controls.Add(this.panelControl4);
            this.pnShowHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnShowHang.Location = new System.Drawing.Point(0, 0);
            this.pnShowHang.Name = "pnShowHang";
            this.pnShowHang.Size = new System.Drawing.Size(686, 698);
            this.pnShowHang.TabIndex = 1;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.pnTable_Mon);
            this.panelControl2.Controls.Add(this.panelControl9);
            this.panelControl2.Controls.Add(this.panelControl7);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(551, 698);
            this.panelControl2.TabIndex = 5;
            // 
            // pnTable_Mon
            // 
            this.pnTable_Mon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnTable_Mon.Location = new System.Drawing.Point(2, 47);
            this.pnTable_Mon.Name = "pnTable_Mon";
            this.pnTable_Mon.Size = new System.Drawing.Size(547, 604);
            this.pnTable_Mon.TabIndex = 25;
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.labelControl3);
            this.panelControl9.Controls.Add(this.labelControl4);
            this.panelControl9.Controls.Add(this.lbPageTable);
            this.panelControl9.Controls.Add(this.btn_NextRight);
            this.panelControl9.Controls.Add(this.btn_NextToLeft);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl9.Location = new System.Drawing.Point(2, 651);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(547, 45);
            this.panelControl9.TabIndex = 1;
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl3.Location = new System.Drawing.Point(270, 17);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(6, 13);
            this.labelControl3.TabIndex = 10;
            this.labelControl3.Text = "/";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl4.Location = new System.Drawing.Point(288, 17);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(14, 13);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "20";
            // 
            // lbPageTable
            // 
            this.lbPageTable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lbPageTable.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbPageTable.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbPageTable.Location = new System.Drawing.Point(251, 17);
            this.lbPageTable.Name = "lbPageTable";
            this.lbPageTable.Size = new System.Drawing.Size(7, 13);
            this.lbPageTable.TabIndex = 8;
            this.lbPageTable.Text = "1";
            // 
            // btn_NextRight
            // 
            this.btn_NextRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_NextRight.Image = global::KP_RES.Properties.Resources.up33_26;
            this.btn_NextRight.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_NextRight.Location = new System.Drawing.Point(414, 2);
            this.btn_NextRight.Name = "btn_NextRight";
            this.btn_NextRight.Size = new System.Drawing.Size(131, 41);
            this.btn_NextRight.TabIndex = 1;
            this.btn_NextRight.Text = "simpleButton16";
            this.btn_NextRight.Click += new System.EventHandler(this.btn_NextToRight_Click);
            // 
            // btn_NextToLeft
            // 
            this.btn_NextToLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_NextToLeft.Image = global::KP_RES.Properties.Resources.up44_26;
            this.btn_NextToLeft.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_NextToLeft.Location = new System.Drawing.Point(2, 2);
            this.btn_NextToLeft.Name = "btn_NextToLeft";
            this.btn_NextToLeft.Size = new System.Drawing.Size(131, 41);
            this.btn_NextToLeft.TabIndex = 0;
            this.btn_NextToLeft.Text = "simpleButton15";
            this.btn_NextToLeft.Click += new System.EventHandler(this.btn_NextToLeft_Click);
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.panelControl37);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(2, 2);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(547, 45);
            this.panelControl7.TabIndex = 0;
            // 
            // panelControl37
            // 
            this.panelControl37.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl37.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl37.Controls.Add(this.lbNameMon);
            this.panelControl37.Controls.Add(this.txtID);
            this.panelControl37.Location = new System.Drawing.Point(2, 7);
            this.panelControl37.Name = "panelControl37";
            this.panelControl37.Size = new System.Drawing.Size(537, 31);
            this.panelControl37.TabIndex = 0;
            // 
            // lbNameMon
            // 
            this.lbNameMon.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbNameMon.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbNameMon.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbNameMon.Location = new System.Drawing.Point(437, 0);
            this.lbNameMon.Name = "lbNameMon";
            this.lbNameMon.Size = new System.Drawing.Size(100, 19);
            this.lbNameMon.TabIndex = 2;
            this.lbNameMon.Text = "     Lẩu Bò Tái";
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(3, 0);
            this.txtID.Name = "txtID";
            this.txtID.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtID.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.Properties.Appearance.Options.UseFont = true;
            this.txtID.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtID.Properties.AppearanceDropDown.Options.UseFont = true;
            this.txtID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "MA", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.txtID.Properties.DisplayMember = "TEN";
            this.txtID.Properties.DropDownItemHeight = 30;
            this.txtID.Properties.DropDownRows = 15;
            this.txtID.Properties.NullText = "";
            this.txtID.Properties.ShowHeader = false;
            this.txtID.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.txtID.Properties.ValueMember = "MA";
            this.txtID.Size = new System.Drawing.Size(192, 26);
            this.txtID.TabIndex = 4;
            this.txtID.Click += new System.EventHandler(this.txtID_Click);
            this.txtID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtID_KeyDown);
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.pnKV_Mon);
            this.panelControl4.Controls.Add(this.panelControl28);
            this.panelControl4.Controls.Add(this.panelControl27);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl4.Location = new System.Drawing.Point(551, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(135, 698);
            this.panelControl4.TabIndex = 4;
            // 
            // pnKV_Mon
            // 
            this.pnKV_Mon.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnKV_Mon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnKV_Mon.Location = new System.Drawing.Point(2, 47);
            this.pnKV_Mon.Name = "pnKV_Mon";
            this.pnKV_Mon.Size = new System.Drawing.Size(131, 604);
            this.pnKV_Mon.TabIndex = 2;
            // 
            // panelControl28
            // 
            this.panelControl28.Controls.Add(this.btn_UpKV);
            this.panelControl28.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl28.Location = new System.Drawing.Point(2, 2);
            this.panelControl28.Name = "panelControl28";
            this.panelControl28.Size = new System.Drawing.Size(131, 45);
            this.panelControl28.TabIndex = 4;
            // 
            // btn_UpKV
            // 
            this.btn_UpKV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_UpKV.Image = ((System.Drawing.Image)(resources.GetObject("btn_UpKV.Image")));
            this.btn_UpKV.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_UpKV.Location = new System.Drawing.Point(2, 2);
            this.btn_UpKV.Name = "btn_UpKV";
            this.btn_UpKV.Size = new System.Drawing.Size(127, 41);
            this.btn_UpKV.TabIndex = 0;
            this.btn_UpKV.Text = "simpleButton17";
            this.btn_UpKV.Click += new System.EventHandler(this.btn_UpKV_Click);
            // 
            // panelControl27
            // 
            this.panelControl27.Controls.Add(this.btn_DowKV);
            this.panelControl27.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl27.Location = new System.Drawing.Point(2, 651);
            this.panelControl27.Name = "panelControl27";
            this.panelControl27.Size = new System.Drawing.Size(131, 45);
            this.panelControl27.TabIndex = 3;
            // 
            // btn_DowKV
            // 
            this.btn_DowKV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_DowKV.Image = ((System.Drawing.Image)(resources.GetObject("btn_DowKV.Image")));
            this.btn_DowKV.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_DowKV.Location = new System.Drawing.Point(2, 2);
            this.btn_DowKV.Name = "btn_DowKV";
            this.btn_DowKV.Size = new System.Drawing.Size(127, 41);
            this.btn_DowKV.TabIndex = 1;
            this.btn_DowKV.Text = "simpleButton18";
            this.btn_DowKV.Click += new System.EventHandler(this.btn_DowKV_Click);
            // 
            // pnMain
            // 
            this.pnMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnMain.Controls.Add(this.pn_listProduct);
            this.pnMain.Controls.Add(this.pnNghiepVu_ThuNgan);
            this.pnMain.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnMain.Location = new System.Drawing.Point(686, 0);
            this.pnMain.Name = "pnMain";
            this.pnMain.Size = new System.Drawing.Size(350, 698);
            this.pnMain.TabIndex = 2;
            // 
            // pn_listProduct
            // 
            this.pn_listProduct.Controls.Add(this.gr_SelectProduct);
            this.pn_listProduct.Controls.Add(this.pnUpDowGv);
            this.pn_listProduct.Controls.Add(this.pnBtnDeleteGv);
            this.pn_listProduct.Controls.Add(this.panelControl20);
            this.pn_listProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_listProduct.Location = new System.Drawing.Point(0, 0);
            this.pn_listProduct.Name = "pn_listProduct";
            this.pn_listProduct.Size = new System.Drawing.Size(350, 440);
            this.pn_listProduct.TabIndex = 0;
            // 
            // gr_SelectProduct
            // 
            this.gr_SelectProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gr_SelectProduct.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gr_SelectProduct.Location = new System.Drawing.Point(2, 2);
            this.gr_SelectProduct.MainView = this.gv_SelectProduct;
            this.gr_SelectProduct.Name = "gr_SelectProduct";
            this.gr_SelectProduct.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1,
            this.repositoryItemMemoEdit2});
            this.gr_SelectProduct.Size = new System.Drawing.Size(313, 217);
            this.gr_SelectProduct.TabIndex = 19;
            this.gr_SelectProduct.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv_SelectProduct});
            // 
            // gv_SelectProduct
            // 
            this.gv_SelectProduct.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.gv_SelectProduct.Appearance.FocusedRow.Options.UseFont = true;
            this.gv_SelectProduct.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Red;
            this.gv_SelectProduct.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gv_SelectProduct.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.75F);
            this.gv_SelectProduct.Appearance.Row.Options.UseFont = true;
            this.gv_SelectProduct.ColumnPanelRowHeight = 43;
            this.gv_SelectProduct.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SOTT,
            this.STT,
            this.TEN_HANGHOA,
            this.GHICHU,
            this.SOLUONG,
            this.GIABAN,
            this.CHIETKHAU,
            this.Thanh_Tien,
            this.MA_HANGHOA,
            this.MA_BEP,
            this.THUE,
            this.TRANGTHAI,
            this.IS_COMBO,
            this.IS_INBEP,
            this.IS_KHUYENMAI,
            this.INTEM,
            this.MONTHEM,
            this.TEM,
            this.MACP,
            this.MACB,
            this.FILL});
            this.gv_SelectProduct.GridControl = this.gr_SelectProduct;
            this.gv_SelectProduct.Name = "gv_SelectProduct";
            this.gv_SelectProduct.OptionsView.RowAutoHeight = true;
            this.gv_SelectProduct.OptionsView.ShowGroupPanel = false;
            this.gv_SelectProduct.OptionsView.ShowIndicator = false;
            this.gv_SelectProduct.RowHeight = 35;
            this.gv_SelectProduct.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gv_SelectProduct.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gv_SelectProduct_RowClick);
            this.gv_SelectProduct.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gv_SelectProduct_RowCellClick);
            this.gv_SelectProduct.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gv_SelectProduct_CustomDrawCell);
            this.gv_SelectProduct.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gv_SelectProduct_FocusedRowChanged);
            // 
            // SOTT
            // 
            this.SOTT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.SOTT.AppearanceCell.Options.UseFont = true;
            this.SOTT.AppearanceCell.Options.UseTextOptions = true;
            this.SOTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOTT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.SOTT.AppearanceHeader.Options.UseFont = true;
            this.SOTT.AppearanceHeader.Options.UseTextOptions = true;
            this.SOTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOTT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOTT.Caption = "TT";
            this.SOTT.FieldName = "SOTT";
            this.SOTT.Name = "SOTT";
            this.SOTT.OptionsColumn.AllowEdit = false;
            this.SOTT.OptionsColumn.AllowFocus = false;
            this.SOTT.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.SOTT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.SOTT.OptionsColumn.FixedWidth = true;
            this.SOTT.OptionsFilter.AllowFilter = false;
            this.SOTT.Width = 37;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.STT.Caption = "TT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Width = 25;
            // 
            // TEN_HANGHOA
            // 
            this.TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceCell.Options.UseTextOptions = true;
            this.TEN_HANGHOA.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_HANGHOA.Caption = "Tên hàng hóa";
            this.TEN_HANGHOA.ColumnEdit = this.repositoryItemMemoEdit1;
            this.TEN_HANGHOA.FieldName = "TEN_HANGHOA";
            this.TEN_HANGHOA.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Value;
            this.TEN_HANGHOA.Name = "TEN_HANGHOA";
            this.TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this.TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this.TEN_HANGHOA.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.TEN_HANGHOA.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TEN_HANGHOA.OptionsColumn.FixedWidth = true;
            this.TEN_HANGHOA.OptionsFilter.AllowFilter = false;
            this.TEN_HANGHOA.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.TEN_HANGHOA.Visible = true;
            this.TEN_HANGHOA.VisibleIndex = 0;
            this.TEN_HANGHOA.Width = 120;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // GHICHU
            // 
            this.GHICHU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GHICHU.AppearanceCell.Options.UseFont = true;
            this.GHICHU.AppearanceCell.Options.UseTextOptions = true;
            this.GHICHU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GHICHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.GHICHU.AppearanceHeader.Options.UseFont = true;
            this.GHICHU.AppearanceHeader.Options.UseTextOptions = true;
            this.GHICHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GHICHU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GHICHU.Caption = "Ghi Chú";
            this.GHICHU.ColumnEdit = this.repositoryItemMemoEdit2;
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.MinWidth = 120;
            this.GHICHU.Name = "GHICHU";
            this.GHICHU.OptionsColumn.AllowEdit = false;
            this.GHICHU.OptionsColumn.AllowFocus = false;
            this.GHICHU.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.GHICHU.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.GHICHU.Visible = true;
            this.GHICHU.VisibleIndex = 1;
            this.GHICHU.Width = 120;
            // 
            // repositoryItemMemoEdit2
            // 
            this.repositoryItemMemoEdit2.Name = "repositoryItemMemoEdit2";
            // 
            // SOLUONG
            // 
            this.SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.SOLUONG.AppearanceCell.Options.UseFont = true;
            this.SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.Caption = "SL";
            this.SOLUONG.FieldName = "SOLUONG";
            this.SOLUONG.Name = "SOLUONG";
            this.SOLUONG.OptionsColumn.AllowEdit = false;
            this.SOLUONG.OptionsColumn.AllowFocus = false;
            this.SOLUONG.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.SOLUONG.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.SOLUONG.OptionsColumn.FixedWidth = true;
            this.SOLUONG.OptionsFilter.AllowFilter = false;
            this.SOLUONG.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.SOLUONG.Visible = true;
            this.SOLUONG.VisibleIndex = 2;
            this.SOLUONG.Width = 37;
            // 
            // GIABAN
            // 
            this.GIABAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GIABAN.AppearanceCell.Options.UseFont = true;
            this.GIABAN.AppearanceCell.Options.UseTextOptions = true;
            this.GIABAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIABAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.GIABAN.AppearanceHeader.Options.UseFont = true;
            this.GIABAN.AppearanceHeader.Options.UseTextOptions = true;
            this.GIABAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIABAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN.Caption = "Đơn giá";
            this.GIABAN.DisplayFormat.FormatString = "{0:#,###0}";
            this.GIABAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GIABAN.FieldName = "GIABAN";
            this.GIABAN.Name = "GIABAN";
            this.GIABAN.OptionsColumn.AllowEdit = false;
            this.GIABAN.OptionsColumn.AllowFocus = false;
            this.GIABAN.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.GIABAN.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.GIABAN.OptionsColumn.FixedWidth = true;
            this.GIABAN.OptionsFilter.AllowFilter = false;
            this.GIABAN.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.GIABAN.Visible = true;
            this.GIABAN.VisibleIndex = 3;
            this.GIABAN.Width = 60;
            // 
            // CHIETKHAU
            // 
            this.CHIETKHAU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.CHIETKHAU.AppearanceCell.Options.UseFont = true;
            this.CHIETKHAU.AppearanceCell.Options.UseTextOptions = true;
            this.CHIETKHAU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CHIETKHAU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHIETKHAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.CHIETKHAU.AppearanceHeader.Options.UseFont = true;
            this.CHIETKHAU.AppearanceHeader.Options.UseTextOptions = true;
            this.CHIETKHAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHIETKHAU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHIETKHAU.Caption = "CK";
            this.CHIETKHAU.DisplayFormat.FormatString = "{0:#,###0}%";
            this.CHIETKHAU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CHIETKHAU.FieldName = "CHIETKHAU";
            this.CHIETKHAU.Name = "CHIETKHAU";
            this.CHIETKHAU.OptionsColumn.AllowEdit = false;
            this.CHIETKHAU.OptionsColumn.AllowFocus = false;
            this.CHIETKHAU.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.CHIETKHAU.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.CHIETKHAU.OptionsColumn.FixedWidth = true;
            this.CHIETKHAU.OptionsFilter.AllowFilter = false;
            this.CHIETKHAU.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.CHIETKHAU.Visible = true;
            this.CHIETKHAU.VisibleIndex = 4;
            this.CHIETKHAU.Width = 30;
            // 
            // Thanh_Tien
            // 
            this.Thanh_Tien.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Thanh_Tien.AppearanceCell.Options.UseFont = true;
            this.Thanh_Tien.AppearanceCell.Options.UseTextOptions = true;
            this.Thanh_Tien.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Thanh_Tien.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Thanh_Tien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.Thanh_Tien.AppearanceHeader.Options.UseFont = true;
            this.Thanh_Tien.AppearanceHeader.Options.UseTextOptions = true;
            this.Thanh_Tien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Thanh_Tien.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Thanh_Tien.Caption = "T tiền";
            this.Thanh_Tien.DisplayFormat.FormatString = "{0:#,###0}";
            this.Thanh_Tien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.Thanh_Tien.FieldName = "Thanh_Tien";
            this.Thanh_Tien.Name = "Thanh_Tien";
            this.Thanh_Tien.OptionsColumn.AllowEdit = false;
            this.Thanh_Tien.OptionsColumn.AllowFocus = false;
            this.Thanh_Tien.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.Thanh_Tien.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.Thanh_Tien.OptionsColumn.FixedWidth = true;
            this.Thanh_Tien.OptionsFilter.AllowFilter = false;
            this.Thanh_Tien.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.Thanh_Tien.Visible = true;
            this.Thanh_Tien.VisibleIndex = 5;
            this.Thanh_Tien.Width = 58;
            // 
            // MA_HANGHOA
            // 
            this.MA_HANGHOA.Caption = "Mã Hàng Hóa";
            this.MA_HANGHOA.FieldName = "MA_HANGHOA";
            this.MA_HANGHOA.Name = "MA_HANGHOA";
            this.MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this.MA_HANGHOA.OptionsColumn.AllowFocus = false;
            // 
            // MA_BEP
            // 
            this.MA_BEP.Caption = "Máy In Bếp";
            this.MA_BEP.FieldName = "MA_BEP";
            this.MA_BEP.Name = "MA_BEP";
            // 
            // THUE
            // 
            this.THUE.Caption = "VAT";
            this.THUE.FieldName = "THUE";
            this.THUE.Name = "THUE";
            // 
            // TRANGTHAI
            // 
            this.TRANGTHAI.Caption = "TRANGTHAI";
            this.TRANGTHAI.FieldName = "TRANGTHAI";
            this.TRANGTHAI.Name = "TRANGTHAI";
            this.TRANGTHAI.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI.OptionsColumn.AllowFocus = false;
            // 
            // IS_COMBO
            // 
            this.IS_COMBO.Caption = "IS_COMBO";
            this.IS_COMBO.FieldName = "IS_COMBO";
            this.IS_COMBO.Name = "IS_COMBO";
            // 
            // IS_INBEP
            // 
            this.IS_INBEP.Caption = "IS_INBEP";
            this.IS_INBEP.FieldName = "IS_INBEP";
            this.IS_INBEP.Name = "IS_INBEP";
            // 
            // IS_KHUYENMAI
            // 
            this.IS_KHUYENMAI.Caption = "IS_KHUYENMAI";
            this.IS_KHUYENMAI.FieldName = "IS_KHUYENMAI";
            this.IS_KHUYENMAI.Name = "IS_KHUYENMAI";
            // 
            // INTEM
            // 
            this.INTEM.Caption = "INTEM";
            this.INTEM.FieldName = "INTEM";
            this.INTEM.Name = "INTEM";
            // 
            // MONTHEM
            // 
            this.MONTHEM.Caption = "MONTHEM";
            this.MONTHEM.FieldName = "MONTHEM";
            this.MONTHEM.Name = "MONTHEM";
            // 
            // TEM
            // 
            this.TEM.Caption = "TEM";
            this.TEM.FieldName = "TEM";
            this.TEM.Name = "TEM";
            this.TEM.OptionsColumn.AllowEdit = false;
            this.TEM.OptionsColumn.AllowFocus = false;
            this.TEM.Width = 25;
            // 
            // MACP
            // 
            this.MACP.Caption = "MACP";
            this.MACP.FieldName = "MACP";
            this.MACP.Name = "MACP";
            this.MACP.OptionsColumn.AllowEdit = false;
            this.MACP.OptionsColumn.AllowFocus = false;
            this.MACP.Width = 25;
            // 
            // MACB
            // 
            this.MACB.Caption = "MACB";
            this.MACB.FieldName = "MACB";
            this.MACB.Name = "MACB";
            this.MACB.OptionsColumn.AllowEdit = false;
            this.MACB.OptionsColumn.AllowFocus = false;
            this.MACB.Width = 25;
            // 
            // FILL
            // 
            this.FILL.Name = "FILL";
            this.FILL.Width = 20;
            // 
            // pnUpDowGv
            // 
            this.pnUpDowGv.Controls.Add(this.btn_up);
            this.pnUpDowGv.Controls.Add(this.btn_Dow);
            this.pnUpDowGv.Controls.Add(this.btn_DowLast);
            this.pnUpDowGv.Controls.Add(this.btn_UpFirst);
            this.pnUpDowGv.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnUpDowGv.Location = new System.Drawing.Point(315, 2);
            this.pnUpDowGv.Name = "pnUpDowGv";
            this.pnUpDowGv.Size = new System.Drawing.Size(33, 217);
            this.pnUpDowGv.TabIndex = 17;
            // 
            // btn_up
            // 
            this.btn_up.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_up.Image = ((System.Drawing.Image)(resources.GetObject("btn_up.Image")));
            this.btn_up.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_up.Location = new System.Drawing.Point(2, 42);
            this.btn_up.Name = "btn_up";
            this.btn_up.Size = new System.Drawing.Size(29, 45);
            this.btn_up.TabIndex = 4;
            this.btn_up.Text = "simpleButton7";
            this.btn_up.Click += new System.EventHandler(this.btn_up_Click);
            // 
            // btn_Dow
            // 
            this.btn_Dow.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_Dow.Image = ((System.Drawing.Image)(resources.GetObject("btn_Dow.Image")));
            this.btn_Dow.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_Dow.Location = new System.Drawing.Point(2, 130);
            this.btn_Dow.Name = "btn_Dow";
            this.btn_Dow.Size = new System.Drawing.Size(29, 45);
            this.btn_Dow.TabIndex = 3;
            this.btn_Dow.Click += new System.EventHandler(this.btn_Dow_Click);
            // 
            // btn_DowLast
            // 
            this.btn_DowLast.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_DowLast.Image = ((System.Drawing.Image)(resources.GetObject("btn_DowLast.Image")));
            this.btn_DowLast.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_DowLast.Location = new System.Drawing.Point(2, 175);
            this.btn_DowLast.Name = "btn_DowLast";
            this.btn_DowLast.Size = new System.Drawing.Size(29, 40);
            this.btn_DowLast.TabIndex = 2;
            this.btn_DowLast.Text = "simpleButton4";
            this.btn_DowLast.Click += new System.EventHandler(this.btn_DowLast_Click);
            // 
            // btn_UpFirst
            // 
            this.btn_UpFirst.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_UpFirst.Image = ((System.Drawing.Image)(resources.GetObject("btn_UpFirst.Image")));
            this.btn_UpFirst.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_UpFirst.Location = new System.Drawing.Point(2, 2);
            this.btn_UpFirst.Name = "btn_UpFirst";
            this.btn_UpFirst.Size = new System.Drawing.Size(29, 40);
            this.btn_UpFirst.TabIndex = 0;
            this.btn_UpFirst.Text = "simpleButton1";
            this.btn_UpFirst.Click += new System.EventHandler(this.btn_UpFirst_Click);
            // 
            // pnBtnDeleteGv
            // 
            this.pnBtnDeleteGv.Controls.Add(this.panelControl29);
            this.pnBtnDeleteGv.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBtnDeleteGv.Location = new System.Drawing.Point(2, 219);
            this.pnBtnDeleteGv.Name = "pnBtnDeleteGv";
            this.pnBtnDeleteGv.Size = new System.Drawing.Size(346, 151);
            this.pnBtnDeleteGv.TabIndex = 18;
            // 
            // panelControl29
            // 
            this.panelControl29.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl29.Controls.Add(this.panelControl33);
            this.panelControl29.Controls.Add(this.panelControl31);
            this.panelControl29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl29.Location = new System.Drawing.Point(2, 2);
            this.panelControl29.Name = "panelControl29";
            this.panelControl29.Size = new System.Drawing.Size(342, 147);
            this.panelControl29.TabIndex = 20;
            // 
            // panelControl33
            // 
            this.panelControl33.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl33.Controls.Add(this.btn_tru);
            this.panelControl33.Controls.Add(this.btn_XoaTatCa);
            this.panelControl33.Controls.Add(this.btn_cong);
            this.panelControl33.Controls.Add(this.btnOK);
            this.panelControl33.Controls.Add(this.btn_xoa);
            this.panelControl33.Controls.Add(this.btn9);
            this.panelControl33.Controls.Add(this.btn8);
            this.panelControl33.Controls.Add(this.btn7);
            this.panelControl33.Controls.Add(this.btn6);
            this.panelControl33.Controls.Add(this.btn5);
            this.panelControl33.Controls.Add(this.btn4);
            this.panelControl33.Controls.Add(this.btn3);
            this.panelControl33.Controls.Add(this.btn2);
            this.panelControl33.Controls.Add(this.btn1);
            this.panelControl33.Controls.Add(this.btn0);
            this.panelControl33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl33.Location = new System.Drawing.Point(0, 24);
            this.panelControl33.Name = "panelControl33";
            this.panelControl33.Size = new System.Drawing.Size(342, 123);
            this.panelControl33.TabIndex = 22;
            // 
            // btn_tru
            // 
            this.btn_tru.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn_tru.Appearance.Options.UseFont = true;
            this.btn_tru.Location = new System.Drawing.Point(189, 43);
            this.btn_tru.Name = "btn_tru";
            this.btn_tru.Size = new System.Drawing.Size(55, 36);
            this.btn_tru.TabIndex = 1;
            this.btn_tru.Text = "-";
            this.btn_tru.Click += new System.EventHandler(this.btn_tru_Click);
            // 
            // btn_XoaTatCa
            // 
            this.btn_XoaTatCa.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_XoaTatCa.Appearance.Options.UseFont = true;
            this.btn_XoaTatCa.Location = new System.Drawing.Point(250, 83);
            this.btn_XoaTatCa.Name = "btn_XoaTatCa";
            this.btn_XoaTatCa.Size = new System.Drawing.Size(86, 36);
            this.btn_XoaTatCa.TabIndex = 0;
            this.btn_XoaTatCa.Text = "Xóa tất cả";
            this.btn_XoaTatCa.Click += new System.EventHandler(this.btn_XoaTatCa_Click);
            // 
            // btn_cong
            // 
            this.btn_cong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn_cong.Appearance.Options.UseFont = true;
            this.btn_cong.Location = new System.Drawing.Point(189, 3);
            this.btn_cong.Name = "btn_cong";
            this.btn_cong.Size = new System.Drawing.Size(55, 36);
            this.btn_cong.TabIndex = 3;
            this.btn_cong.Text = "+";
            this.btn_cong.Click += new System.EventHandler(this.btn_cong_Click);
            // 
            // btnOK
            // 
            this.btnOK.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnOK.Appearance.Options.UseFont = true;
            this.btnOK.Location = new System.Drawing.Point(250, 3);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(86, 36);
            this.btnOK.TabIndex = 26;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btn_xoa
            // 
            this.btn_xoa.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_xoa.Appearance.Options.UseFont = true;
            this.btn_xoa.Location = new System.Drawing.Point(250, 43);
            this.btn_xoa.Name = "btn_xoa";
            this.btn_xoa.Size = new System.Drawing.Size(86, 36);
            this.btn_xoa.TabIndex = 2;
            this.btn_xoa.Text = "Xóa";
            this.btn_xoa.Click += new System.EventHandler(this.btn_xoa_Click);
            // 
            // btn9
            // 
            this.btn9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn9.Appearance.Options.UseFont = true;
            this.btn9.Location = new System.Drawing.Point(127, 3);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(55, 36);
            this.btn9.TabIndex = 25;
            this.btn9.Text = "9";
            this.btn9.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn8
            // 
            this.btn8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn8.Appearance.Options.UseFont = true;
            this.btn8.Location = new System.Drawing.Point(66, 3);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(55, 36);
            this.btn8.TabIndex = 24;
            this.btn8.Text = "8";
            this.btn8.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn7
            // 
            this.btn7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn7.Appearance.Options.UseFont = true;
            this.btn7.Location = new System.Drawing.Point(5, 3);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(55, 36);
            this.btn7.TabIndex = 23;
            this.btn7.Text = "7";
            this.btn7.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn6
            // 
            this.btn6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn6.Appearance.Options.UseFont = true;
            this.btn6.Location = new System.Drawing.Point(127, 43);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(55, 36);
            this.btn6.TabIndex = 22;
            this.btn6.Text = "6";
            this.btn6.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn5
            // 
            this.btn5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn5.Appearance.Options.UseFont = true;
            this.btn5.Location = new System.Drawing.Point(66, 43);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(55, 36);
            this.btn5.TabIndex = 21;
            this.btn5.Text = "5";
            this.btn5.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn4
            // 
            this.btn4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn4.Appearance.Options.UseFont = true;
            this.btn4.Location = new System.Drawing.Point(5, 43);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(55, 36);
            this.btn4.TabIndex = 20;
            this.btn4.Text = "4";
            this.btn4.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn3
            // 
            this.btn3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn3.Appearance.Options.UseFont = true;
            this.btn3.Location = new System.Drawing.Point(127, 83);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(55, 36);
            this.btn3.TabIndex = 19;
            this.btn3.Text = "3";
            this.btn3.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn2
            // 
            this.btn2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn2.Appearance.Options.UseFont = true;
            this.btn2.Location = new System.Drawing.Point(66, 83);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(55, 36);
            this.btn2.TabIndex = 18;
            this.btn2.Text = "2";
            this.btn2.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn1
            // 
            this.btn1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn1.Appearance.Options.UseFont = true;
            this.btn1.Location = new System.Drawing.Point(5, 83);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(55, 36);
            this.btn1.TabIndex = 17;
            this.btn1.Text = "1";
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn0
            // 
            this.btn0.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn0.Appearance.Options.UseFont = true;
            this.btn0.Location = new System.Drawing.Point(189, 83);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(55, 36);
            this.btn0.TabIndex = 16;
            this.btn0.Text = "0";
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // panelControl31
            // 
            this.panelControl31.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl31.Controls.Add(this.panelControl32);
            this.panelControl31.Controls.Add(this.lb_soluong);
            this.panelControl31.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl31.Location = new System.Drawing.Point(0, 0);
            this.panelControl31.Name = "panelControl31";
            this.panelControl31.Size = new System.Drawing.Size(342, 24);
            this.panelControl31.TabIndex = 21;
            // 
            // panelControl32
            // 
            this.panelControl32.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl32.Controls.Add(this.lbSoluong);
            this.panelControl32.Location = new System.Drawing.Point(125, 5);
            this.panelControl32.Name = "panelControl32";
            this.panelControl32.Size = new System.Drawing.Size(170, 15);
            this.panelControl32.TabIndex = 18;
            // 
            // lbSoluong
            // 
            this.lbSoluong.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbSoluong.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbSoluong.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbSoluong.Location = new System.Drawing.Point(163, 0);
            this.lbSoluong.Name = "lbSoluong";
            this.lbSoluong.Size = new System.Drawing.Size(7, 13);
            this.lbSoluong.TabIndex = 7;
            this.lbSoluong.Text = "0";
            // 
            // lb_soluong
            // 
            this.lb_soluong.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_soluong.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_soluong.Location = new System.Drawing.Point(14, 5);
            this.lb_soluong.Name = "lb_soluong";
            this.lb_soluong.Size = new System.Drawing.Size(55, 13);
            this.lb_soluong.TabIndex = 14;
            this.lb_soluong.Text = "Số lượng :";
            // 
            // panelControl20
            // 
            this.panelControl20.Controls.Add(this.btnComboOption);
            this.panelControl20.Controls.Add(this.pnlInforMoney);
            this.panelControl20.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl20.Location = new System.Drawing.Point(2, 370);
            this.panelControl20.Name = "panelControl20";
            this.panelControl20.Size = new System.Drawing.Size(346, 68);
            this.panelControl20.TabIndex = 20;
            // 
            // btnComboOption
            // 
            this.btnComboOption.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnComboOption.Appearance.Options.UseFont = true;
            this.btnComboOption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnComboOption.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnComboOption.Location = new System.Drawing.Point(2, 2);
            this.btnComboOption.Name = "btnComboOption";
            this.btnComboOption.Size = new System.Drawing.Size(171, 64);
            this.btnComboOption.TabIndex = 27;
            this.btnComboOption.Text = "Tùy chọn  ";
            this.btnComboOption.Visible = false;
            this.btnComboOption.Click += new System.EventHandler(this.btnComboOption_Click);
            // 
            // pnlInforMoney
            // 
            this.pnlInforMoney.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlInforMoney.Controls.Add(this.panelControl50);
            this.pnlInforMoney.Controls.Add(this.panelControl23);
            this.pnlInforMoney.Controls.Add(this.lb_phuthu);
            this.pnlInforMoney.Controls.Add(this.lb_tongcong);
            this.pnlInforMoney.Controls.Add(this.panelControl13);
            this.pnlInforMoney.Controls.Add(this.lb_giamgia);
            this.pnlInforMoney.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlInforMoney.Location = new System.Drawing.Point(173, 2);
            this.pnlInforMoney.Name = "pnlInforMoney";
            this.pnlInforMoney.Size = new System.Drawing.Size(171, 64);
            this.pnlInforMoney.TabIndex = 16;
            // 
            // panelControl50
            // 
            this.panelControl50.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl50.Controls.Add(this.lbTotal);
            this.panelControl50.Location = new System.Drawing.Point(70, 44);
            this.panelControl50.Name = "panelControl50";
            this.panelControl50.Size = new System.Drawing.Size(93, 15);
            this.panelControl50.TabIndex = 18;
            // 
            // lbTotal
            // 
            this.lbTotal.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbTotal.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbTotal.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbTotal.Location = new System.Drawing.Point(86, 0);
            this.lbTotal.Name = "lbTotal";
            this.lbTotal.Size = new System.Drawing.Size(7, 13);
            this.lbTotal.TabIndex = 2;
            this.lbTotal.Text = "0";
            // 
            // panelControl23
            // 
            this.panelControl23.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl23.Controls.Add(this.lbGiamGia);
            this.panelControl23.Location = new System.Drawing.Point(70, 23);
            this.panelControl23.Name = "panelControl23";
            this.panelControl23.Size = new System.Drawing.Size(93, 15);
            this.panelControl23.TabIndex = 18;
            // 
            // lbGiamGia
            // 
            this.lbGiamGia.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbGiamGia.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbGiamGia.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbGiamGia.Location = new System.Drawing.Point(86, 0);
            this.lbGiamGia.Name = "lbGiamGia";
            this.lbGiamGia.Size = new System.Drawing.Size(7, 13);
            this.lbGiamGia.TabIndex = 5;
            this.lbGiamGia.Text = "0";
            // 
            // lb_phuthu
            // 
            this.lb_phuthu.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_phuthu.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_phuthu.Location = new System.Drawing.Point(8, 5);
            this.lb_phuthu.Name = "lb_phuthu";
            this.lb_phuthu.Size = new System.Drawing.Size(46, 13);
            this.lb_phuthu.TabIndex = 6;
            this.lb_phuthu.Text = "Phụ thu:";
            // 
            // lb_tongcong
            // 
            this.lb_tongcong.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_tongcong.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_tongcong.Location = new System.Drawing.Point(8, 46);
            this.lb_tongcong.Name = "lb_tongcong";
            this.lb_tongcong.Size = new System.Drawing.Size(61, 13);
            this.lb_tongcong.TabIndex = 0;
            this.lb_tongcong.Text = "Tổng cộng:";
            // 
            // panelControl13
            // 
            this.panelControl13.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl13.Controls.Add(this.lbPhuThu);
            this.panelControl13.Location = new System.Drawing.Point(70, 4);
            this.panelControl13.Name = "panelControl13";
            this.panelControl13.Size = new System.Drawing.Size(93, 15);
            this.panelControl13.TabIndex = 17;
            // 
            // lbPhuThu
            // 
            this.lbPhuThu.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbPhuThu.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbPhuThu.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbPhuThu.Location = new System.Drawing.Point(86, 0);
            this.lbPhuThu.Name = "lbPhuThu";
            this.lbPhuThu.Size = new System.Drawing.Size(7, 13);
            this.lbPhuThu.TabIndex = 7;
            this.lbPhuThu.Text = "0";
            // 
            // lb_giamgia
            // 
            this.lb_giamgia.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_giamgia.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_giamgia.Location = new System.Drawing.Point(8, 24);
            this.lb_giamgia.Name = "lb_giamgia";
            this.lb_giamgia.Size = new System.Drawing.Size(52, 13);
            this.lb_giamgia.TabIndex = 4;
            this.lb_giamgia.Text = "Giảm giá:";
            // 
            // pnNghiepVu_ThuNgan
            // 
            this.pnNghiepVu_ThuNgan.Controls.Add(this.pnDacThu);
            this.pnNghiepVu_ThuNgan.Controls.Add(this.pnThuNgan);
            this.pnNghiepVu_ThuNgan.Controls.Add(this.panelControl8);
            this.pnNghiepVu_ThuNgan.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnNghiepVu_ThuNgan.Location = new System.Drawing.Point(0, 440);
            this.pnNghiepVu_ThuNgan.Name = "pnNghiepVu_ThuNgan";
            this.pnNghiepVu_ThuNgan.Size = new System.Drawing.Size(350, 258);
            this.pnNghiepVu_ThuNgan.TabIndex = 1;
            // 
            // pnDacThu
            // 
            this.pnDacThu.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnDacThu.Controls.Add(this.btnChietkhau100);
            this.pnDacThu.Controls.Add(this.btnInbilldoi);
            this.pnDacThu.Controls.Add(this.btnGoikhach);
            this.pnDacThu.Controls.Add(this.btnSuaGia);
            this.pnDacThu.Controls.Add(this.btnGiaBan2);
            this.pnDacThu.Controls.Add(this.btnThemHangHoa);
            this.pnDacThu.Controls.Add(this.btnInVeOnline);
            this.pnDacThu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnDacThu.Location = new System.Drawing.Point(2, 47);
            this.pnDacThu.Name = "pnDacThu";
            this.pnDacThu.Size = new System.Drawing.Size(346, 209);
            this.pnDacThu.TabIndex = 22;
            // 
            // btnChietkhau100
            // 
            this.btnChietkhau100.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChietkhau100.Appearance.Options.UseFont = true;
            this.btnChietkhau100.AutoWidthInLayoutControl = true;
            this.btnChietkhau100.Location = new System.Drawing.Point(236, 5);
            this.btnChietkhau100.Name = "btnChietkhau100";
            this.btnChietkhau100.Size = new System.Drawing.Size(100, 62);
            this.btnChietkhau100.TabIndex = 36;
            this.btnChietkhau100.Text = "Giảm giá 100%";
            this.btnChietkhau100.Click += new System.EventHandler(this.btnChietkhau100_Click);
            // 
            // btnInbilldoi
            // 
            this.btnInbilldoi.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnInbilldoi.Appearance.Options.UseFont = true;
            this.btnInbilldoi.Location = new System.Drawing.Point(236, 142);
            this.btnInbilldoi.Name = "btnInbilldoi";
            this.btnInbilldoi.Size = new System.Drawing.Size(100, 62);
            this.btnInbilldoi.TabIndex = 35;
            this.btnInbilldoi.Text = "In bill đợi";
            this.btnInbilldoi.Click += new System.EventHandler(this.btnInbilldoi_Click);
            // 
            // btnGoikhach
            // 
            this.btnGoikhach.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnGoikhach.Appearance.Options.UseFont = true;
            this.btnGoikhach.Location = new System.Drawing.Point(122, 74);
            this.btnGoikhach.Name = "btnGoikhach";
            this.btnGoikhach.Size = new System.Drawing.Size(100, 62);
            this.btnGoikhach.TabIndex = 34;
            this.btnGoikhach.Text = "Gọi khách";
            this.btnGoikhach.Click += new System.EventHandler(this.btnGoikhach_Click);
            // 
            // btnSuaGia
            // 
            this.btnSuaGia.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnSuaGia.Appearance.Options.UseFont = true;
            this.btnSuaGia.Location = new System.Drawing.Point(11, 74);
            this.btnSuaGia.Name = "btnSuaGia";
            this.btnSuaGia.Size = new System.Drawing.Size(100, 62);
            this.btnSuaGia.TabIndex = 33;
            this.btnSuaGia.Text = "Sửa Giá";
            this.btnSuaGia.Click += new System.EventHandler(this.btnSuaGia_Click);
            // 
            // btnGiaBan2
            // 
            this.btnGiaBan2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGiaBan2.Appearance.Options.UseFont = true;
            this.btnGiaBan2.Location = new System.Drawing.Point(236, 74);
            this.btnGiaBan2.Name = "btnGiaBan2";
            this.btnGiaBan2.Size = new System.Drawing.Size(100, 62);
            this.btnGiaBan2.TabIndex = 8;
            this.btnGiaBan2.Text = "Tặng";
            this.btnGiaBan2.Click += new System.EventHandler(this.btnGiaBan2_Click);
            // 
            // btnThemHangHoa
            // 
            this.btnThemHangHoa.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnThemHangHoa.Appearance.Options.UseFont = true;
            this.btnThemHangHoa.Location = new System.Drawing.Point(122, 5);
            this.btnThemHangHoa.Name = "btnThemHangHoa";
            this.btnThemHangHoa.Size = new System.Drawing.Size(100, 62);
            this.btnThemHangHoa.TabIndex = 31;
            this.btnThemHangHoa.Text = "Thêm hàng hóa";
            this.btnThemHangHoa.Click += new System.EventHandler(this.btnThemHangHoa_Click);
            // 
            // btnInVeOnline
            // 
            this.btnInVeOnline.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnInVeOnline.Appearance.Options.UseFont = true;
            this.btnInVeOnline.Location = new System.Drawing.Point(11, 5);
            this.btnInVeOnline.Name = "btnInVeOnline";
            this.btnInVeOnline.Size = new System.Drawing.Size(100, 62);
            this.btnInVeOnline.TabIndex = 30;
            this.btnInVeOnline.Text = "In Bill Online";
            this.btnInVeOnline.Click += new System.EventHandler(this.btnInVeOnline_Click);
            // 
            // pnThuNgan
            // 
            this.pnThuNgan.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnThuNgan.Controls.Add(this.btn_YeuCauThem);
            this.pnThuNgan.Controls.Add(this.btn_MonThem);
            this.pnThuNgan.Controls.Add(this.btn_InLaiHoaDon);
            this.pnThuNgan.Controls.Add(this.btnMoKet);
            this.pnThuNgan.Controls.Add(this.btnChietkhau);
            this.pnThuNgan.Controls.Add(this.btn_logout);
            this.pnThuNgan.Controls.Add(this.btn_HuyHoaDon);
            this.pnThuNgan.Controls.Add(this.btnKetKa);
            this.pnThuNgan.Controls.Add(this.btn_Payment);
            this.pnThuNgan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnThuNgan.Location = new System.Drawing.Point(2, 47);
            this.pnThuNgan.Name = "pnThuNgan";
            this.pnThuNgan.Size = new System.Drawing.Size(346, 209);
            this.pnThuNgan.TabIndex = 20;
            // 
            // btn_YeuCauThem
            // 
            this.btn_YeuCauThem.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_YeuCauThem.Appearance.Options.UseFont = true;
            this.btn_YeuCauThem.Location = new System.Drawing.Point(11, 74);
            this.btn_YeuCauThem.Name = "btn_YeuCauThem";
            this.btn_YeuCauThem.Size = new System.Drawing.Size(100, 62);
            this.btn_YeuCauThem.TabIndex = 35;
            this.btn_YeuCauThem.Text = "Yêu Cầu \r\nThêm";
            this.btn_YeuCauThem.Click += new System.EventHandler(this.btn_YeuCauThem_Click);
            // 
            // btn_MonThem
            // 
            this.btn_MonThem.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_MonThem.Appearance.Options.UseFont = true;
            this.btn_MonThem.Location = new System.Drawing.Point(11, 5);
            this.btn_MonThem.Name = "btn_MonThem";
            this.btn_MonThem.Size = new System.Drawing.Size(100, 62);
            this.btn_MonThem.TabIndex = 34;
            this.btn_MonThem.Text = "Món thêm";
            this.btn_MonThem.Click += new System.EventHandler(this.btn_MonThem_Click);
            // 
            // btn_InLaiHoaDon
            // 
            this.btn_InLaiHoaDon.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_InLaiHoaDon.Appearance.Options.UseFont = true;
            this.btn_InLaiHoaDon.Location = new System.Drawing.Point(122, 74);
            this.btn_InLaiHoaDon.Name = "btn_InLaiHoaDon";
            this.btn_InLaiHoaDon.Size = new System.Drawing.Size(100, 62);
            this.btn_InLaiHoaDon.TabIndex = 27;
            this.btn_InLaiHoaDon.Text = "In lại\r\nhóa đơn";
            this.btn_InLaiHoaDon.Click += new System.EventHandler(this.btn_InLaiHoaDon_Click);
            // 
            // btnMoKet
            // 
            this.btnMoKet.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnMoKet.Appearance.Options.UseFont = true;
            this.btnMoKet.Location = new System.Drawing.Point(235, 5);
            this.btnMoKet.Name = "btnMoKet";
            this.btnMoKet.Size = new System.Drawing.Size(100, 62);
            this.btnMoKet.TabIndex = 22;
            this.btnMoKet.Text = "Mở két";
            this.btnMoKet.Click += new System.EventHandler(this.btnMoKet_Click);
            // 
            // btnChietkhau
            // 
            this.btnChietkhau.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnChietkhau.Appearance.Options.UseFont = true;
            this.btnChietkhau.Location = new System.Drawing.Point(122, 5);
            this.btnChietkhau.Name = "btnChietkhau";
            this.btnChietkhau.Size = new System.Drawing.Size(100, 62);
            this.btnChietkhau.TabIndex = 33;
            this.btnChietkhau.Text = "Chiết Khấu";
            this.btnChietkhau.Click += new System.EventHandler(this.btnChietkhau_Click);
            // 
            // btn_logout
            // 
            this.btn_logout.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_logout.Appearance.Options.UseFont = true;
            this.btn_logout.Location = new System.Drawing.Point(235, 142);
            this.btn_logout.Name = "btn_logout";
            this.btn_logout.Size = new System.Drawing.Size(100, 62);
            this.btn_logout.TabIndex = 20;
            this.btn_logout.Text = "Đóng";
            this.btn_logout.Click += new System.EventHandler(this.btn_logout_Click);
            // 
            // btn_HuyHoaDon
            // 
            this.btn_HuyHoaDon.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_HuyHoaDon.Appearance.Options.UseFont = true;
            this.btn_HuyHoaDon.Location = new System.Drawing.Point(122, 142);
            this.btn_HuyHoaDon.Name = "btn_HuyHoaDon";
            this.btn_HuyHoaDon.Size = new System.Drawing.Size(100, 62);
            this.btn_HuyHoaDon.TabIndex = 29;
            this.btn_HuyHoaDon.Text = "Hủy hóa đơn";
            this.btn_HuyHoaDon.Click += new System.EventHandler(this.btn_HuyHoaDon_Click);
            // 
            // btnKetKa
            // 
            this.btnKetKa.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnKetKa.Appearance.Options.UseFont = true;
            this.btnKetKa.Location = new System.Drawing.Point(235, 74);
            this.btnKetKa.Name = "btnKetKa";
            this.btnKetKa.Size = new System.Drawing.Size(100, 62);
            this.btnKetKa.TabIndex = 34;
            this.btnKetKa.Text = "Kết ca";
            this.btnKetKa.Click += new System.EventHandler(this.btnKetKa_Click);
            // 
            // btn_Payment
            // 
            this.btn_Payment.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btn_Payment.Appearance.Options.UseFont = true;
            this.btn_Payment.Location = new System.Drawing.Point(11, 142);
            this.btn_Payment.Name = "btn_Payment";
            this.btn_Payment.Size = new System.Drawing.Size(100, 62);
            this.btn_Payment.TabIndex = 14;
            this.btn_Payment.Text = "F1\r\nThanh Toán";
            this.btn_Payment.Click += new System.EventHandler(this.btn_Payment_Click);
            // 
            // panelControl8
            // 
            this.panelControl8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl8.Controls.Add(this.btn_DatThu);
            this.panelControl8.Controls.Add(this.btn_ThuNgan);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl8.Location = new System.Drawing.Point(2, 2);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(346, 45);
            this.panelControl8.TabIndex = 2;
            // 
            // btn_DatThu
            // 
            this.btn_DatThu.AllowFocus = false;
            this.btn_DatThu.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.btn_DatThu.Appearance.Options.UseFont = true;
            this.btn_DatThu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_DatThu.Location = new System.Drawing.Point(173, 0);
            this.btn_DatThu.Name = "btn_DatThu";
            this.btn_DatThu.Size = new System.Drawing.Size(173, 45);
            this.btn_DatThu.TabIndex = 3;
            this.btn_DatThu.Text = "Đặc thù";
            this.btn_DatThu.Click += new System.EventHandler(this.btn_DatThu_Click);
            // 
            // btn_ThuNgan
            // 
            this.btn_ThuNgan.AllowFocus = false;
            this.btn_ThuNgan.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.btn_ThuNgan.Appearance.Options.UseFont = true;
            this.btn_ThuNgan.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_ThuNgan.Location = new System.Drawing.Point(0, 0);
            this.btn_ThuNgan.Name = "btn_ThuNgan";
            this.btn_ThuNgan.Size = new System.Drawing.Size(173, 45);
            this.btn_ThuNgan.TabIndex = 0;
            this.btn_ThuNgan.Text = "THU NGÂN";
            this.btn_ThuNgan.Click += new System.EventHandler(this.btn_ThuNgan_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pnInlaihoadon
            // 
            this.pnInlaihoadon.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnInlaihoadon.Controls.Add(this.panelControl53);
            this.pnInlaihoadon.Controls.Add(this.panelControl52);
            this.pnInlaihoadon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnInlaihoadon.Location = new System.Drawing.Point(0, 0);
            this.pnInlaihoadon.Name = "pnInlaihoadon";
            this.pnInlaihoadon.Size = new System.Drawing.Size(686, 698);
            this.pnInlaihoadon.TabIndex = 2;
            // 
            // panelControl53
            // 
            this.panelControl53.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl53.Controls.Add(this.gr_billPaymented);
            this.panelControl53.Controls.Add(this.panelControl1);
            this.panelControl53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl53.Location = new System.Drawing.Point(0, 0);
            this.panelControl53.Name = "panelControl53";
            this.panelControl53.Size = new System.Drawing.Size(646, 698);
            this.panelControl53.TabIndex = 2;
            // 
            // gr_billPaymented
            // 
            this.gr_billPaymented.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gr_billPaymented.Location = new System.Drawing.Point(0, 43);
            this.gr_billPaymented.MainView = this.gvBillPaymented;
            this.gr_billPaymented.Name = "gr_billPaymented";
            this.gr_billPaymented.Size = new System.Drawing.Size(646, 655);
            this.gr_billPaymented.TabIndex = 4;
            this.gr_billPaymented.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvBillPaymented});
            // 
            // gvBillPaymented
            // 
            this.gvBillPaymented.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.gvBillPaymented.Appearance.FocusedRow.Options.UseFont = true;
            this.gvBillPaymented.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.gvBillPaymented.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvBillPaymented.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.75F);
            this.gvBillPaymented.Appearance.Row.Options.UseFont = true;
            this.gvBillPaymented.ColumnPanelRowHeight = 45;
            this.gvBillPaymented.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SSTT,
            this.MA_HOADON,
            this.NGAYTAO,
            this.TEN_KHUVUC,
            this.TONGTIEN,
            this.TEN_BAN,
            this.IN,
            this.HUY,
            this.LAYLAI});
            this.gvBillPaymented.GridControl = this.gr_billPaymented;
            this.gvBillPaymented.Name = "gvBillPaymented";
            this.gvBillPaymented.OptionsView.EnableAppearanceEvenRow = true;
            this.gvBillPaymented.OptionsView.ShowGroupPanel = false;
            this.gvBillPaymented.OptionsView.ShowIndicator = false;
            this.gvBillPaymented.RowHeight = 45;
            this.gvBillPaymented.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gvBillPaymented.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvBillPaymented_RowCellClick);
            this.gvBillPaymented.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gvBillPaymented_CustomDrawCell);
            // 
            // SSTT
            // 
            this.SSTT.AppearanceCell.Options.UseTextOptions = true;
            this.SSTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SSTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SSTT.AppearanceHeader.Options.UseFont = true;
            this.SSTT.AppearanceHeader.Options.UseTextOptions = true;
            this.SSTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SSTT.Caption = "STT";
            this.SSTT.FieldName = "SSTT";
            this.SSTT.MinWidth = 40;
            this.SSTT.Name = "SSTT";
            this.SSTT.OptionsColumn.AllowEdit = false;
            this.SSTT.OptionsColumn.AllowFocus = false;
            this.SSTT.OptionsFilter.AllowAutoFilter = false;
            this.SSTT.OptionsFilter.AllowFilter = false;
            this.SSTT.Visible = true;
            this.SSTT.VisibleIndex = 0;
            this.SSTT.Width = 40;
            // 
            // MA_HOADON
            // 
            this.MA_HOADON.AppearanceCell.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_HOADON.AppearanceHeader.Options.UseFont = true;
            this.MA_HOADON.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.Caption = "Mã Hóa Đơn";
            this.MA_HOADON.FieldName = "MA_HOADON";
            this.MA_HOADON.MinWidth = 135;
            this.MA_HOADON.Name = "MA_HOADON";
            this.MA_HOADON.OptionsColumn.AllowEdit = false;
            this.MA_HOADON.OptionsColumn.AllowFocus = false;
            this.MA_HOADON.Visible = true;
            this.MA_HOADON.VisibleIndex = 1;
            this.MA_HOADON.Width = 135;
            // 
            // NGAYTAO
            // 
            this.NGAYTAO.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYTAO.AppearanceHeader.Options.UseFont = true;
            this.NGAYTAO.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.Caption = "Ngày Tạo";
            this.NGAYTAO.DisplayFormat.FormatString = "{0:dd/MM/yyyy HH:mm}";
            this.NGAYTAO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYTAO.FieldName = "NGAYTAO";
            this.NGAYTAO.MinWidth = 135;
            this.NGAYTAO.Name = "NGAYTAO";
            this.NGAYTAO.OptionsColumn.AllowEdit = false;
            this.NGAYTAO.OptionsColumn.AllowFocus = false;
            this.NGAYTAO.Visible = true;
            this.NGAYTAO.VisibleIndex = 2;
            this.NGAYTAO.Width = 135;
            // 
            // TEN_KHUVUC
            // 
            this.TEN_KHUVUC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_KHUVUC.AppearanceHeader.Options.UseFont = true;
            this.TEN_KHUVUC.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_KHUVUC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_KHUVUC.Caption = "Khu vực";
            this.TEN_KHUVUC.FieldName = "TEN_KHUVUC";
            this.TEN_KHUVUC.MinWidth = 70;
            this.TEN_KHUVUC.Name = "TEN_KHUVUC";
            this.TEN_KHUVUC.OptionsColumn.AllowEdit = false;
            this.TEN_KHUVUC.OptionsColumn.AllowFocus = false;
            this.TEN_KHUVUC.OptionsFilter.AllowAutoFilter = false;
            this.TEN_KHUVUC.OptionsFilter.AllowFilter = false;
            this.TEN_KHUVUC.Width = 70;
            // 
            // TONGTIEN
            // 
            this.TONGTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TONGTIEN.AppearanceHeader.Options.UseFont = true;
            this.TONGTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGTIEN.Caption = "Tổng Tiền";
            this.TONGTIEN.DisplayFormat.FormatString = "N0";
            this.TONGTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TONGTIEN.FieldName = "TONGTIEN";
            this.TONGTIEN.MinWidth = 70;
            this.TONGTIEN.Name = "TONGTIEN";
            this.TONGTIEN.OptionsColumn.AllowEdit = false;
            this.TONGTIEN.OptionsColumn.AllowFocus = false;
            this.TONGTIEN.Visible = true;
            this.TONGTIEN.VisibleIndex = 3;
            this.TONGTIEN.Width = 70;
            // 
            // TEN_BAN
            // 
            this.TEN_BAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_BAN.AppearanceHeader.Options.UseFont = true;
            this.TEN_BAN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_BAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_BAN.Caption = "Bàn";
            this.TEN_BAN.FieldName = "TEN_BAN";
            this.TEN_BAN.MinWidth = 70;
            this.TEN_BAN.Name = "TEN_BAN";
            this.TEN_BAN.OptionsColumn.AllowEdit = false;
            this.TEN_BAN.OptionsColumn.AllowFocus = false;
            this.TEN_BAN.OptionsFilter.AllowAutoFilter = false;
            this.TEN_BAN.OptionsFilter.AllowFilter = false;
            this.TEN_BAN.Width = 70;
            // 
            // IN
            // 
            this.IN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IN.AppearanceHeader.Options.UseFont = true;
            this.IN.AppearanceHeader.Options.UseTextOptions = true;
            this.IN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.IN.Caption = "In";
            this.IN.ColumnEdit = this.repositoryItemButtonEdit1;
            this.IN.MaxWidth = 100;
            this.IN.MinWidth = 70;
            this.IN.Name = "IN";
            this.IN.OptionsColumn.AllowEdit = false;
            this.IN.OptionsColumn.AllowFocus = false;
            this.IN.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.IN.Visible = true;
            this.IN.VisibleIndex = 4;
            this.IN.Width = 70;
            // 
            // HUY
            // 
            this.HUY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HUY.AppearanceHeader.Options.UseFont = true;
            this.HUY.AppearanceHeader.Options.UseTextOptions = true;
            this.HUY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HUY.Caption = "Hủy";
            this.HUY.ColumnEdit = this.repositoryItemButtonEdit2;
            this.HUY.MaxWidth = 100;
            this.HUY.MinWidth = 70;
            this.HUY.Name = "HUY";
            this.HUY.OptionsColumn.AllowEdit = false;
            this.HUY.OptionsColumn.AllowFocus = false;
            this.HUY.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.HUY.Visible = true;
            this.HUY.VisibleIndex = 5;
            this.HUY.Width = 70;
            // 
            // LAYLAI
            // 
            this.LAYLAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LAYLAI.AppearanceHeader.Options.UseFont = true;
            this.LAYLAI.AppearanceHeader.Options.UseTextOptions = true;
            this.LAYLAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LAYLAI.Caption = "Lấy lại HĐ";
            this.LAYLAI.ColumnEdit = this.repositoryItemButtonEdit3;
            this.LAYLAI.MaxWidth = 100;
            this.LAYLAI.MinWidth = 70;
            this.LAYLAI.Name = "LAYLAI";
            this.LAYLAI.OptionsColumn.AllowEdit = false;
            this.LAYLAI.OptionsColumn.AllowFocus = false;
            this.LAYLAI.OptionsColumn.FixedWidth = true;
            this.LAYLAI.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.LAYLAI.Width = 70;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.txtMaHoaDon);
            this.panelControl1.Controls.Add(this.btnHuy);
            this.panelControl1.Controls.Add(this.lb_maKH);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(646, 43);
            this.panelControl1.TabIndex = 3;
            // 
            // txtMaHoaDon
            // 
            this.txtMaHoaDon.EditValue = "";
            this.txtMaHoaDon.Location = new System.Drawing.Point(87, 7);
            this.txtMaHoaDon.Name = "txtMaHoaDon";
            this.txtMaHoaDon.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtMaHoaDon.Properties.Appearance.Options.UseFont = true;
            this.txtMaHoaDon.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMaHoaDon.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.txtMaHoaDon.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMaHoaDon.Size = new System.Drawing.Size(215, 30);
            this.txtMaHoaDon.TabIndex = 14;
            // 
            // btnHuy
            // 
            this.btnHuy.AllowFocus = false;
            this.btnHuy.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuy.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnHuy.Appearance.Options.UseFont = true;
            this.btnHuy.Appearance.Options.UseForeColor = true;
            this.btnHuy.Location = new System.Drawing.Point(308, 7);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(76, 30);
            this.btnHuy.TabIndex = 16;
            this.btnHuy.Text = "Hủy";
            this.btnHuy.Click += new System.EventHandler(this.btnHuy_Click);
            // 
            // lb_maKH
            // 
            this.lb_maKH.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_maKH.Location = new System.Drawing.Point(6, 14);
            this.lb_maKH.Name = "lb_maKH";
            this.lb_maKH.Size = new System.Drawing.Size(75, 16);
            this.lb_maKH.TabIndex = 15;
            this.lb_maKH.Text = "Mã hóa đơn";
            // 
            // panelControl52
            // 
            this.panelControl52.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl52.Controls.Add(this.labelControl43);
            this.panelControl52.Controls.Add(this.lbTongSoRow);
            this.panelControl52.Controls.Add(this.labelControl45);
            this.panelControl52.Controls.Add(this.btn_upbill);
            this.panelControl52.Controls.Add(this.btnDowBill);
            this.panelControl52.Controls.Add(this.btnDowLastBill);
            this.panelControl52.Controls.Add(this.btn_UpTopBill);
            this.panelControl52.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl52.Location = new System.Drawing.Point(646, 0);
            this.panelControl52.Name = "panelControl52";
            this.panelControl52.Size = new System.Drawing.Size(40, 698);
            this.panelControl52.TabIndex = 1;
            // 
            // labelControl43
            // 
            this.labelControl43.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl43.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl43.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl43.Location = new System.Drawing.Point(10, 323);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(20, 13);
            this.labelControl43.TabIndex = 14;
            this.labelControl43.Text = "----";
            // 
            // lbTongSoRow
            // 
            this.lbTongSoRow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTongSoRow.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbTongSoRow.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbTongSoRow.Location = new System.Drawing.Point(12, 337);
            this.lbTongSoRow.Name = "lbTongSoRow";
            this.lbTongSoRow.Size = new System.Drawing.Size(14, 13);
            this.lbTongSoRow.TabIndex = 13;
            this.lbTongSoRow.Text = "00";
            // 
            // labelControl45
            // 
            this.labelControl45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl45.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl45.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl45.Location = new System.Drawing.Point(12, 311);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(14, 13);
            this.labelControl45.TabIndex = 12;
            this.labelControl45.Text = "00";
            // 
            // btn_upbill
            // 
            this.btn_upbill.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_upbill.Image = ((System.Drawing.Image)(resources.GetObject("btn_upbill.Image")));
            this.btn_upbill.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_upbill.Location = new System.Drawing.Point(0, 80);
            this.btn_upbill.Name = "btn_upbill";
            this.btn_upbill.Size = new System.Drawing.Size(40, 80);
            this.btn_upbill.TabIndex = 11;
            this.btn_upbill.Click += new System.EventHandler(this.btn_upbill_Click);
            // 
            // btnDowBill
            // 
            this.btnDowBill.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnDowBill.Image = ((System.Drawing.Image)(resources.GetObject("btnDowBill.Image")));
            this.btnDowBill.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDowBill.Location = new System.Drawing.Point(0, 538);
            this.btnDowBill.Name = "btnDowBill";
            this.btnDowBill.Size = new System.Drawing.Size(40, 80);
            this.btnDowBill.TabIndex = 10;
            this.btnDowBill.Click += new System.EventHandler(this.btnDowBill_Click);
            // 
            // btnDowLastBill
            // 
            this.btnDowLastBill.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnDowLastBill.Image = ((System.Drawing.Image)(resources.GetObject("btnDowLastBill.Image")));
            this.btnDowLastBill.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDowLastBill.Location = new System.Drawing.Point(0, 618);
            this.btnDowLastBill.Name = "btnDowLastBill";
            this.btnDowLastBill.Size = new System.Drawing.Size(40, 80);
            this.btnDowLastBill.TabIndex = 9;
            this.btnDowLastBill.Text = "simpleButton4";
            this.btnDowLastBill.Click += new System.EventHandler(this.btnDowLastBill_Click);
            // 
            // btn_UpTopBill
            // 
            this.btn_UpTopBill.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_UpTopBill.Image = ((System.Drawing.Image)(resources.GetObject("btn_UpTopBill.Image")));
            this.btn_UpTopBill.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_UpTopBill.Location = new System.Drawing.Point(0, 0);
            this.btn_UpTopBill.Name = "btn_UpTopBill";
            this.btn_UpTopBill.Size = new System.Drawing.Size(40, 80);
            this.btn_UpTopBill.TabIndex = 8;
            this.btn_UpTopBill.Text = "simpleButton1";
            this.btn_UpTopBill.Click += new System.EventHandler(this.btn_UpTopBill_Click);
            // 
            // pnWapper
            // 
            this.pnWapper.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnWapper.Controls.Add(this.pnYeucauthem);
            this.pnWapper.Controls.Add(this.pnShowHang);
            this.pnWapper.Controls.Add(this.pnInlaihoadon);
            this.pnWapper.Controls.Add(this.pnMain);
            this.pnWapper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnWapper.Location = new System.Drawing.Point(0, 70);
            this.pnWapper.Name = "pnWapper";
            this.pnWapper.Size = new System.Drawing.Size(1036, 698);
            this.pnWapper.TabIndex = 24;
            // 
            // pnYeucauthem
            // 
            this.pnYeucauthem.Controls.Add(this.panelControl19);
            this.pnYeucauthem.Controls.Add(this.panelControl5);
            this.pnYeucauthem.Controls.Add(this.panelControl21);
            this.pnYeucauthem.Controls.Add(this.panelControl17);
            this.pnYeucauthem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnYeucauthem.Location = new System.Drawing.Point(0, 0);
            this.pnYeucauthem.Name = "pnYeucauthem";
            this.pnYeucauthem.Size = new System.Drawing.Size(686, 698);
            this.pnYeucauthem.TabIndex = 9;
            // 
            // panelControl19
            // 
            this.panelControl19.Controls.Add(this.pnYeuCau);
            this.panelControl19.Controls.Add(this.panelControl22);
            this.panelControl19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl19.Location = new System.Drawing.Point(2, 98);
            this.panelControl19.Name = "panelControl19";
            this.panelControl19.Size = new System.Drawing.Size(682, 378);
            this.panelControl19.TabIndex = 3;
            // 
            // pnYeuCau
            // 
            this.pnYeuCau.AutoScroll = true;
            this.pnYeuCau.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnYeuCau.Location = new System.Drawing.Point(2, 2);
            this.pnYeuCau.Name = "pnYeuCau";
            this.pnYeuCau.Size = new System.Drawing.Size(645, 374);
            this.pnYeuCau.TabIndex = 1;
            // 
            // panelControl22
            // 
            this.panelControl22.Controls.Add(this.simpleButton23);
            this.panelControl22.Controls.Add(this.simpleButton24);
            this.panelControl22.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl22.Location = new System.Drawing.Point(647, 2);
            this.panelControl22.Name = "panelControl22";
            this.panelControl22.Size = new System.Drawing.Size(33, 374);
            this.panelControl22.TabIndex = 0;
            // 
            // simpleButton23
            // 
            this.simpleButton23.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.simpleButton23.Image = global::KP_RES.Properties.Resources.up11_26;
            this.simpleButton23.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton23.Location = new System.Drawing.Point(2, 301);
            this.simpleButton23.Name = "simpleButton23";
            this.simpleButton23.Size = new System.Drawing.Size(29, 71);
            this.simpleButton23.TabIndex = 7;
            this.simpleButton23.Text = ">";
            // 
            // simpleButton24
            // 
            this.simpleButton24.Dock = System.Windows.Forms.DockStyle.Top;
            this.simpleButton24.Image = global::KP_RES.Properties.Resources.up22_26;
            this.simpleButton24.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton24.Location = new System.Drawing.Point(2, 2);
            this.simpleButton24.Name = "simpleButton24";
            this.simpleButton24.Size = new System.Drawing.Size(29, 71);
            this.simpleButton24.TabIndex = 8;
            this.simpleButton24.Text = "<";
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.us_keyboard3);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl5.Location = new System.Drawing.Point(2, 476);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(682, 220);
            this.panelControl5.TabIndex = 80;
            // 
            // us_keyboard3
            // 
            this.us_keyboard3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.us_keyboard3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.us_keyboard3.Location = new System.Drawing.Point(16, 4);
            this.us_keyboard3.Name = "us_keyboard3";
            this.us_keyboard3.Size = new System.Drawing.Size(649, 213);
            this.us_keyboard3.TabIndex = 10;
            // 
            // panelControl21
            // 
            this.panelControl21.Controls.Add(this.txtYeuCauThem);
            this.panelControl21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl21.Location = new System.Drawing.Point(2, 44);
            this.panelControl21.Name = "panelControl21";
            this.panelControl21.Size = new System.Drawing.Size(682, 54);
            this.panelControl21.TabIndex = 0;
            // 
            // txtYeuCauThem
            // 
            this.txtYeuCauThem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtYeuCauThem.Location = new System.Drawing.Point(3, 6);
            this.txtYeuCauThem.Name = "txtYeuCauThem";
            this.txtYeuCauThem.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.txtYeuCauThem.Properties.Appearance.Options.UseFont = true;
            this.txtYeuCauThem.Size = new System.Drawing.Size(677, 40);
            this.txtYeuCauThem.TabIndex = 1;
            this.txtYeuCauThem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtYeuCauThem_KeyDown);
            // 
            // panelControl17
            // 
            this.panelControl17.Controls.Add(this.panelControl11);
            this.panelControl17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl17.Location = new System.Drawing.Point(2, 2);
            this.panelControl17.Name = "panelControl17";
            this.panelControl17.Size = new System.Drawing.Size(682, 42);
            this.panelControl17.TabIndex = 2;
            // 
            // panelControl11
            // 
            this.panelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl11.Controls.Add(this.lbMonYeuCauThem);
            this.panelControl11.Controls.Add(this.lbTableYeuCauThem);
            this.panelControl11.Controls.Add(this.lbkvYeuCauThem);
            this.panelControl11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl11.Location = new System.Drawing.Point(2, 12);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(678, 28);
            this.panelControl11.TabIndex = 3;
            // 
            // lbMonYeuCauThem
            // 
            this.lbMonYeuCauThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbMonYeuCauThem.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbMonYeuCauThem.Location = new System.Drawing.Point(306, 0);
            this.lbMonYeuCauThem.Name = "lbMonYeuCauThem";
            this.lbMonYeuCauThem.Size = new System.Drawing.Size(116, 19);
            this.lbMonYeuCauThem.TabIndex = 2;
            this.lbMonYeuCauThem.Text = "Coffee Sửa Đá";
            // 
            // lbTableYeuCauThem
            // 
            this.lbTableYeuCauThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbTableYeuCauThem.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbTableYeuCauThem.Location = new System.Drawing.Point(192, 0);
            this.lbTableYeuCauThem.Name = "lbTableYeuCauThem";
            this.lbTableYeuCauThem.Size = new System.Drawing.Size(114, 19);
            this.lbTableYeuCauThem.TabIndex = 1;
            this.lbTableYeuCauThem.Text = "Bàn 1           >";
            // 
            // lbkvYeuCauThem
            // 
            this.lbkvYeuCauThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbkvYeuCauThem.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbkvYeuCauThem.Location = new System.Drawing.Point(0, 0);
            this.lbkvYeuCauThem.Name = "lbkvYeuCauThem";
            this.lbkvYeuCauThem.Size = new System.Drawing.Size(192, 19);
            this.lbkvYeuCauThem.TabIndex = 0;
            this.lbkvYeuCauThem.Text = "KV Sân Vườn               >";
            // 
            // Frm_Banhang_TraSua
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 768);
            this.Controls.Add(this.pnWapper);
            this.Controls.Add(this.pnHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Frm_Banhang_TraSua";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_FastFood";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frm_FastFood_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_FastFood_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_FastFood_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnHeader)).EndInit();
            this.pnHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnShowHang)).EndInit();
            this.pnShowHang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.panelControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl37)).EndInit();
            this.panelControl37.ResumeLayout(false);
            this.panelControl37.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnKV_Mon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl28)).EndInit();
            this.panelControl28.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl27)).EndInit();
            this.panelControl27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnMain)).EndInit();
            this.pnMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_listProduct)).EndInit();
            this.pn_listProduct.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_SelectProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_SelectProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDowGv)).EndInit();
            this.pnUpDowGv.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnBtnDeleteGv)).EndInit();
            this.pnBtnDeleteGv.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl29)).EndInit();
            this.panelControl29.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl33)).EndInit();
            this.panelControl33.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl31)).EndInit();
            this.panelControl31.ResumeLayout(false);
            this.panelControl31.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl32)).EndInit();
            this.panelControl32.ResumeLayout(false);
            this.panelControl32.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl20)).EndInit();
            this.panelControl20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlInforMoney)).EndInit();
            this.pnlInforMoney.ResumeLayout(false);
            this.pnlInforMoney.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl50)).EndInit();
            this.panelControl50.ResumeLayout(false);
            this.panelControl50.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl23)).EndInit();
            this.panelControl23.ResumeLayout(false);
            this.panelControl23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).EndInit();
            this.panelControl13.ResumeLayout(false);
            this.panelControl13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnNghiepVu_ThuNgan)).EndInit();
            this.pnNghiepVu_ThuNgan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnDacThu)).EndInit();
            this.pnDacThu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnThuNgan)).EndInit();
            this.pnThuNgan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnInlaihoadon)).EndInit();
            this.pnInlaihoadon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl53)).EndInit();
            this.panelControl53.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_billPaymented)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBillPaymented)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHoaDon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl52)).EndInit();
            this.panelControl52.ResumeLayout(false);
            this.panelControl52.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnWapper)).EndInit();
            this.pnWapper.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnYeucauthem)).EndInit();
            this.pnYeucauthem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).EndInit();
            this.panelControl19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).EndInit();
            this.panelControl22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl21)).EndInit();
            this.panelControl21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtYeuCauThem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).EndInit();
            this.panelControl17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            this.panelControl11.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnHeader;
        private DevExpress.XtraEditors.PanelControl pnShowHang;
        private DevExpress.XtraEditors.PanelControl pnMain;
        private DevExpress.XtraEditors.PanelControl pn_listProduct;
        private DevExpress.XtraEditors.PanelControl pnUpDowGv;
        private DevExpress.XtraEditors.SimpleButton btn_up;
        private DevExpress.XtraEditors.SimpleButton btn_Dow;
        private DevExpress.XtraEditors.SimpleButton btn_DowLast;
        private DevExpress.XtraEditors.SimpleButton btn_UpFirst;
        private DevExpress.XtraGrid.GridControl gr_SelectProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView gv_SelectProduct;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn GIABAN;
        private DevExpress.XtraGrid.Columns.GridColumn Thanh_Tien;
        private DevExpress.XtraEditors.PanelControl pnBtnDeleteGv;
        private DevExpress.XtraGrid.Columns.GridColumn SOTT;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA;
        private DevExpress.XtraEditors.PanelControl pnNghiepVu_ThuNgan;
        private DevExpress.XtraEditors.SimpleButton btn_cong;
        private DevExpress.XtraEditors.SimpleButton btn_xoa;
        private DevExpress.XtraEditors.SimpleButton btn_tru;
        private DevExpress.XtraEditors.SimpleButton btn_XoaTatCa;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.SimpleButton btn_ThuNgan;
        private DevExpress.XtraEditors.SimpleButton btn_Payment;
        private DevExpress.XtraEditors.PanelControl pnThuNgan;
        private DevExpress.XtraEditors.SimpleButton btn_InLaiHoaDon;
        private DevExpress.XtraEditors.SimpleButton btnMoKet;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl lbgiay;
        private DevExpress.XtraEditors.LabelControl lbGio;
        private DevExpress.XtraEditors.LabelControl lbtimeby;
        private DevExpress.XtraEditors.LabelControl lb_NhanVien;
        private DevExpress.XtraEditors.LabelControl lb_Quay;
        private DevExpress.XtraEditors.LabelControl lbCaBan;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl lbPageTable;
        private DevExpress.XtraEditors.SimpleButton btn_NextRight;
        private DevExpress.XtraEditors.SimpleButton btn_NextToLeft;
        private DevExpress.XtraEditors.SimpleButton btn_DowKV;
        private DevExpress.XtraEditors.SimpleButton btn_UpKV;
        private DevExpress.XtraEditors.PanelControl panelControl20;
        private DevExpress.XtraEditors.LabelControl lb_tongcong;
        private DevExpress.XtraEditors.LabelControl lbNameMon;
        private DevExpress.XtraEditors.PanelControl pnKV_Mon;
        private DevExpress.XtraEditors.SimpleButton btn_logout;
        private DevExpress.XtraEditors.LabelControl lbTotal;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;
        private DevExpress.XtraEditors.PanelControl panelControl37;
        private DevExpress.XtraGrid.Columns.GridColumn MA_BEP;
        private DevExpress.XtraEditors.PanelControl pnInlaihoadon;
        private DevExpress.XtraEditors.PanelControl panelControl52;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.LabelControl lbTongSoRow;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.SimpleButton btn_upbill;
        private DevExpress.XtraEditors.SimpleButton btnDowBill;
        private DevExpress.XtraEditors.SimpleButton btnDowLastBill;
        private DevExpress.XtraEditors.SimpleButton btn_UpTopBill;
        private DevExpress.XtraEditors.PanelControl panelControl53;
        private DevExpress.XtraGrid.Columns.GridColumn THUE;
        private DevExpress.XtraEditors.LabelControl lbGiamGia;
        private DevExpress.XtraEditors.LabelControl lb_giamgia;
        private DevExpress.XtraEditors.LabelControl lbPhuThu;
        private DevExpress.XtraEditors.LabelControl lb_phuthu;
        private DevExpress.XtraEditors.PanelControl pnWapper;
        private DevExpress.XtraEditors.SimpleButton btnGiaBan2;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI;
        private DevExpress.XtraEditors.PanelControl pnlInforMoney;
        private DevExpress.XtraEditors.PanelControl panelControl50;
        private DevExpress.XtraEditors.PanelControl panelControl23;
        private DevExpress.XtraEditors.PanelControl panelControl13;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.PanelControl panelControl28;
        private DevExpress.XtraEditors.PanelControl panelControl27;
        private DevExpress.XtraEditors.SimpleButton btn_HuyHoaDon;
        private DevExpress.XtraGrid.Columns.GridColumn IS_COMBO;
        private DevExpress.XtraGrid.Columns.GridColumn IS_INBEP;
        private DevExpress.XtraEditors.PanelControl panelControl29;
        private DevExpress.XtraEditors.PanelControl panelControl31;
        private DevExpress.XtraEditors.PanelControl panelControl32;
        private DevExpress.XtraEditors.LabelControl lbSoluong;
        private DevExpress.XtraEditors.LabelControl lb_soluong;
        private DevExpress.XtraEditors.PanelControl panelControl33;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btn9;
        private DevExpress.XtraEditors.SimpleButton btn8;
        private DevExpress.XtraEditors.SimpleButton btn7;
        private DevExpress.XtraEditors.SimpleButton btn6;
        private DevExpress.XtraEditors.SimpleButton btn5;
        private DevExpress.XtraEditors.SimpleButton btn4;
        private DevExpress.XtraEditors.SimpleButton btn3;
        private DevExpress.XtraEditors.SimpleButton btn2;
        private DevExpress.XtraEditors.SimpleButton btn1;
        private DevExpress.XtraEditors.SimpleButton btn0;
        private DevExpress.XtraEditors.SimpleButton btnComboOption;
        private DevExpress.XtraGrid.Columns.GridColumn IS_KHUYENMAI;
        private DevExpress.XtraEditors.LabelControl lb_Kho;
        private DevExpress.XtraEditors.SimpleButton btnInVeOnline;
        private DevExpress.XtraEditors.LookUpEdit txtID;
        private DevExpress.XtraEditors.SimpleButton btn_DatThu;
        private DevExpress.XtraEditors.PanelControl pnDacThu;
        private DevExpress.XtraEditors.SimpleButton btnChietkhau;
        private DevExpress.XtraGrid.Columns.GridColumn CHIETKHAU;
        private DevExpress.XtraGrid.GridControl gr_billPaymented;
        private DevExpress.XtraGrid.Views.Grid.GridView gvBillPaymented;
        private DevExpress.XtraGrid.Columns.GridColumn SSTT;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HOADON;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYTAO;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_KHUVUC;
        private DevExpress.XtraGrid.Columns.GridColumn TONGTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_BAN;
        private DevExpress.XtraGrid.Columns.GridColumn IN;
        private DevExpress.XtraGrid.Columns.GridColumn HUY;
        private DevExpress.XtraGrid.Columns.GridColumn LAYLAI;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TextEdit txtMaHoaDon;
        private DevExpress.XtraEditors.SimpleButton btnHuy;
        private DevExpress.XtraEditors.LabelControl lb_maKH;
        private DevExpress.XtraEditors.SimpleButton btnThemHangHoa;
        private DevExpress.XtraEditors.XtraScrollableControl pnTable_Mon;
        private DevExpress.XtraEditors.PanelControl pnYeucauthem;
        private DevExpress.XtraEditors.PanelControl panelControl19;
        private System.Windows.Forms.FlowLayoutPanel pnYeuCau;
        private DevExpress.XtraEditors.PanelControl panelControl22;
        private DevExpress.XtraEditors.SimpleButton simpleButton23;
        private DevExpress.XtraEditors.SimpleButton simpleButton24;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private Us_keyboard us_keyboard3;
        private DevExpress.XtraEditors.PanelControl panelControl21;
        private DevExpress.XtraEditors.TextEdit txtYeuCauThem;
        private DevExpress.XtraEditors.PanelControl panelControl17;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraEditors.LabelControl lbMonYeuCauThem;
        private DevExpress.XtraEditors.LabelControl lbTableYeuCauThem;
        private DevExpress.XtraEditors.LabelControl lbkvYeuCauThem;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit3;
        private DevExpress.XtraEditors.SimpleButton btnSuaGia;
        private DevExpress.XtraEditors.SimpleButton btn_MonThem;
        private DevExpress.XtraGrid.Columns.GridColumn MONTHEM;
        private DevExpress.XtraGrid.Columns.GridColumn TEM;
        private DevExpress.XtraGrid.Columns.GridColumn MACP;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MACB;
        private DevExpress.XtraEditors.SimpleButton btn_YeuCauThem;
        private DevExpress.XtraEditors.SimpleButton btnKetKa;
        private DevExpress.XtraGrid.Columns.GridColumn INTEM;
        private DevExpress.XtraEditors.SimpleButton btnGoikhach;
        private DevExpress.XtraEditors.SimpleButton btnInbilldoi;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit2;
        private DevExpress.XtraEditors.SimpleButton btnChietkhau100;
    }
}