﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES
{
    public partial class Frm_Giaohang : DevExpress.XtraEditors.XtraForm
    {
        bool flag = false;//true:đã có khách hàng,ko cần thêm ;  false:khách hàng mới,insert vào KHTT
        bool[] ctltxt;
        public string sdiachi = "";
        public string sMaKH = "";
        public string sTenKH = "";
        public string sDienthoai = "";
        public bool chkdiachikhac = false;

        public Frm_Giaohang()
        {
            InitializeComponent();
            btnMaAuto_Click(null, null);
            dtpNgaysinh.EditValue = DateTime.Now;
            //txtDienthoai.Text = "123";
            //txtDiachi.Text = "abc";
        }

        public Frm_Giaohang(string makh,bool _chkdiachikhac,string _diachikhac)
        {
            InitializeComponent();
            string sSQL = "";
            sSQL += "Select MAKHTT,TEN,DIENTHOAI,DIACHI,NGAYSINH" + "\n";
            sSQL += "From KHACHHANG_THANTHIET A,KHTT B" + "\n";
            sSQL += "Where A.Ma_KHTT=B.MA" + "\n";
            sSQL += "And A.MAKHTT=" + clsMain.SQLString(makh);
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                txtMaKH.Text = makh;
                txtTenKH.Text = dt.Rows[0]["TEN"].ToString();
                txtDienthoai.Text = dt.Rows[0]["DIENTHOAI"].ToString();
                txtDiachi.Text = dt.Rows[0]["DIACHI"].ToString();
                dtpNgaysinh.EditValue = dt.Rows[0]["NGAYSINH"].ToString() == "" ? DateTime.Now : DateTime.Parse(dt.Rows[0]["NGAYSINH"].ToString());
                txtDiachikhac.Enabled = chkDiachikhac.Enabled = true;
                if (_chkdiachikhac)
                {
                    chkDiachikhac.Checked = true;
                    txtDiachikhac.Text = _diachikhac;
                }
                else
                {
                    chkDiachikhac.Checked = false;
                }
            }
            
        }

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                if (!ctltxt[int.Parse(txt.Tag.ToString())])
                {
                    txt.SelectAll();
                    ctltxt[int.Parse(txt.Tag.ToString())] = true;
                }
                else
                {
                    ctltxt[int.Parse(txt.Tag.ToString())] = false;
                }
            }
            catch
            {
            }
        }

        bool flag_leave_ma = false;
        private void txtMaKH_Click(object sender, EventArgs e)
        {
            us_keyboard1.SetTextbox(txtMaKH);
            flag_leave_ma = true;
            flag_leave_dt = false;
        }

        private void txtTenKH_Click(object sender, EventArgs e)
        {
            us_keyboard1.SetTextbox(txtTenKH);
            flag_leave_ma = false;
            flag_leave_dt = false;
        }

        bool flag_leave_dt = false;
        private void txtDienthoai_Click(object sender, EventArgs e)
        {
            us_keyboard1.SetTextbox(txtDienthoai);
            flag_leave_dt = true;
            flag_leave_ma = false;
        }

        private void txtDiachi_Click(object sender, EventArgs e)
        {
            us_keyboard1.SetTextbox(txtDiachi);
            flag_leave_ma = false;
            flag_leave_dt = false;
        }

        private void txtDiachikhac_Click(object sender, EventArgs e)
        {
            us_keyboard1.SetTextbox(txtDiachikhac);
            flag_leave_ma = false;
            flag_leave_dt = false;
        }

        private void txtDienthoai_Leave(object sender, EventArgs e)
        {
            string sSQL = "";
            sSQL += "Select MAKHTT,TEN,DIENTHOAI,DIACHI,NGAYSINH" + "\n";
            sSQL += "From KHACHHANG_THANTHIET A,KHTT B" + "\n";
            sSQL += "Where A.Ma_KHTT=B.MA" + "\n";
            sSQL += "And B.DIENTHOAI=" + clsMain.SQLString(txtDienthoai.Text.Trim());
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                txtMaKH.Text = dt.Rows[0]["MAKHTT"].ToString();
                txtTenKH.Text = dt.Rows[0]["TEN"].ToString();
                txtDiachi.Text = dt.Rows[0]["DIACHI"].ToString();
                dtpNgaysinh.EditValue = dt.Rows[0]["NGAYSINH"].ToString() == "" ? DateTime.Now : DateTime.Parse(dt.Rows[0]["NGAYSINH"].ToString());
                txtDiachikhac.Enabled = chkDiachikhac.Enabled = true;
                flag = true;
            }
        }

        private void txtMaKH_Leave(object sender, EventArgs e)
        {
            string sSQL = "";
            sSQL += "Select MAKHTT,TEN,DIENTHOAI,DIACHI,NGAYSINH" + "\n";
            sSQL += "From KHACHHANG_THANTHIET A,KHTT B" + "\n";
            sSQL += "Where A.Ma_KHTT=B.MA" + "\n";
            sSQL += "And A.MAKHTT=" + clsMain.SQLString(txtMaKH.Text.Trim());
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                txtTenKH.Text = dt.Rows[0]["TEN"].ToString();
                txtDienthoai.Text = dt.Rows[0]["DIENTHOAI"].ToString();
                txtDiachi.Text = dt.Rows[0]["DIACHI"].ToString();
                dtpNgaysinh.EditValue = dt.Rows[0]["NGAYSINH"].ToString() == "" ? DateTime.Now : DateTime.Parse(dt.Rows[0]["NGAYSINH"].ToString());
                txtDiachikhac.Enabled = chkDiachikhac.Enabled = true;
                flag = true;
            }
            else
            {
                txtDienthoai.Text = "";
                txtTenKH.Text = "";
                txtDiachi.Text = "";
                txtDiachikhac.Enabled = chkDiachikhac.Enabled = false;
                flag = false;
            }
        }

        private void btnMaAuto_Click(object sender, EventArgs e)
        {
            string ngay = string.Format("{0:yyMMdd}", clsGlobal.gdServerDate);
            DataTable dt = clsMain.ReturnDataTable("Select [dbo].[fc_NewCodeID_KHTT]  ('"+ ngay +"')");
            txtMaKH.Text = dt.Rows[0][0].ToString();
            txtTenKH.Text = txtDienthoai.Text = txtDiachi.Text = txtDiachikhac.Text = "";
            chkDiachikhac.Checked = false;
            txtDiachikhac.Enabled = chkDiachikhac.Enabled = false;
            txtTenKH.Focus();
            us_keyboard1.SetTextbox(txtTenKH);
            flag = false;
        }

        private bool checkinput()
        {
            if (txtMaKH.Text.Trim() == "")
            {
                XtraMessageBox.Show(_cnmkh, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            else if (txtTenKH.Text.Trim() == "")
            {
                XtraMessageBox.Show(_cntkh, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            else if (txtDienthoai.Text.Trim() == "")
            {
                XtraMessageBox.Show(_cnsdtkh, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            else if (txtDiachi.Text.Trim() == "")
            {
                XtraMessageBox.Show(_cndckh, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            else
            {
                if (chkDiachikhac.Checked)
                {
                    XtraMessageBox.Show(_cndcgh, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                else
                    return true;
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (flag)
            {
                sTenKH = txtTenKH.Text;
                sDienthoai = txtDienthoai.Text;
                sMaKH = txtMaKH.Text;
                if (chkDiachikhac.Checked)
                {
                    sdiachi = txtDiachikhac.Text.Trim();
                    chkdiachikhac = true;
                }
                else
                {
                    sdiachi = txtDiachi.Text.Trim();
                    chkdiachikhac = false;
                }
            }
            else
            {
                if (!checkinput())
                    return;
                string sSQL = "";
                sSQL += "Insert into KHTT (MA,TEN,DIACHI,DIENTHOAI,CMND,NGAYSINH,GIOITINH,NGUOITAO,SUDUNG)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLString(txtMaKH.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtTenKH.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtDiachi.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtDienthoai.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtDienthoai.Text) + ",";
                sSQL += clsMain.SQLString(dtpNgaysinh.Text) + ",";
                sSQL += clsMain.SQLBit(true) + ",";
                sSQL += clsMain.SQLString(clsGlobal .gsUserID) + ",";
                sSQL += clsMain.SQLBit(true) + ")";
                clsMain.ExecuteSQL(sSQL);

                int MaCapDo = int.Parse(clsMain.ReturnDataTable("select dbo.[fn_DoiDiemRaCapDo_NS](0)").Rows[0][0].ToString());
                int makhtt = 0;
                int diemtv = 0;
                int diemgt = 0;

                sSQL  = "";
                sSQL += "Insert into KHACHHANG_THANHTOAN_NOIBO(Ma_KHTT,SoDuTaiKhoan,SuDung,NgayThamGia,MAKHTTNB,FLAGKHTTNB)" + "\n";
                sSQL += "Values(";
                sSQL += clsMain.SQLString(txtMaKH.Text) + ",";
                sSQL += 0 + ",";
                sSQL += clsMain.SQLBit(true) + ",";
                sSQL += "GETDATE()" + ",";
                sSQL += clsMain.SQLString(txtMaKH.Text) + ",";
                sSQL += 0 + ")";
                clsMain.ExecuteSQL(sSQL);

                sSQL = "";
                sSQL += "Insert into KHACHHANG_THANTHIET(Ma_KHTT,SoLe,SoDiem,SoDiemHuuDung,MACAPDO,SoLanChoThuong,SoLanDaDuocThuong,SuDung,SoLanReset,SoLanThuongReSet,SoLanTinh,NgayThamGia,LastUpdate,LoaiThanhVien,MAKHTT,FLAGKHTT)" + "\n";
                sSQL += "Values(";
                sSQL += clsMain.SQLString(txtMaKH.Text) + ",";
                sSQL += 0 + ",";
                sSQL += diemtv + ",";
                sSQL += diemtv + ",";
                sSQL += MaCapDo + ",";
                sSQL += 0 + ",";
                sSQL += 0 + ",";
                sSQL += clsMain.SQLBit(true) + ",";
                sSQL += 0 + ",";
                sSQL += 0 + ",";
                sSQL += 0 + ",";
                sSQL += "GETDATE()" + ",";
                sSQL += "GETDATE()" + ",";
                sSQL += 0 + ",";
                sSQL += clsMain.SQLString(txtMaKH.Text) + ",";
                sSQL += 0 + ")";
                clsMain.ExecuteSQL(sSQL);

                sdiachi = txtDiachi.Text.Trim();
                sMaKH = txtMaKH.Text;
                sTenKH = txtTenKH.Text;
                sDienthoai = txtDienthoai.Text;
                chkdiachikhac = false;
            }
            this.Close();
        }

        private void Frm_Giaohang_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (flag_leave_ma)
                {
                    flag_leave_ma = false;
                    txtTenKH.Focus();
                    txtTenKH.SelectAll();

                }
                else if (flag_leave_dt)
                {
                    flag_leave_dt = false;
                    txtDiachi.Focus();
                    txtDiachi.SelectAll();
                }
                else
                    btnLuu_Click(sender, e);
            }
        }

        string _thongbao = "";
        string _cnmkh = "";
        string _cntkh = "";
        string _cnsdtkh = "";
        string _cndckh = "";
        string _cndcgh = "";
        private void Frm_ConfigFuntion_Load(object sender, EventArgs e)
        {
            txtMaKH.Focus();
            us_keyboard1.SetTextbox(txtMaKH);
            flag_leave_ma = true;

            int dem = 1;
            foreach (Control ctl in panelControl2.Controls)
            {
                if (ctl is TextEdit)
                {
                    ctl.Tag = dem;
                    dem += 1;
                }
                ctl.Click += new EventHandler(ctl_Click);
            }
            ctltxt = new bool[dem];

            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            this.Text = rm.GetString("giaohang", culture);
            btnLuu.Text = rm.GetString("luu", culture);
            btnMaAuto.Text = rm.GetString("tudong",culture);
            lb_maKH.Text = rm.GetString("makh", culture);
            lb_tenKH.Text = rm.GetString("tenkh1", culture);
            lb_dienthoai.Text = rm.GetString("dienthoai", culture);
            lb_diachi.Text = rm.GetString("diachi", culture);
            labelControl1.Text = rm.GetString("ngaysinh",culture);
            chkDiachikhac.Text = rm.GetString("diachikhac", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _cnmkh = rm.GetString("cnmkh", culture);
            _cntkh = rm.GetString("cntkh", culture);
            _cnsdtkh = rm.GetString("cnsdtkh", culture);
            _cndckh = rm.GetString("cndckh", culture);
            _cndcgh = rm.GetString("cndcgh", culture);
            LoadKH();
        }

        private void LoadKH()
        {
            string sSQL = "";
            sSQL += "Select MAKHTT as MA,TEN,DIENTHOAI,DIACHI,NGAYSINH" + "\n";
            sSQL += "From KHACHHANG_THANTHIET A,KHTT B" + "\n";
            sSQL += "Where A.Ma_KHTT=B.MA" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            cboKH.Properties.DataSource = dt;
        }
        
        private void cboKH_EditValueChanged(object sender, EventArgs e)
        {
            string sSQL = "";
            sSQL += "Select MAKHTT,TEN,DIENTHOAI,DIACHI,NGAYSINH" + "\n";
            sSQL += "From KHACHHANG_THANTHIET A,KHTT B" + "\n";
            sSQL += "Where A.Ma_KHTT=B.MA" + "\n";
            sSQL += "And A.MAKHTT=" + clsMain.SQLString(cboKH .EditValue .ToString ());
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                txtMaKH.Text = dt.Rows[0]["MAKHTT"].ToString();
                txtTenKH.Text = dt.Rows[0]["TEN"].ToString();
                txtDienthoai.Text = dt.Rows[0]["DIENTHOAI"].ToString();
                dtpNgaysinh.EditValue = dt.Rows[0]["NGAYSINH"].ToString() == "" ? DateTime.Now : DateTime.Parse(dt.Rows[0]["NGAYSINH"].ToString());
                txtDiachi.Text = dt.Rows[0]["DIACHI"].ToString();
                txtDiachikhac.Enabled = chkDiachikhac.Enabled = true;
                flag = true;
            }
        }
    }
}