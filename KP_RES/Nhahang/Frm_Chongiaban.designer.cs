﻿namespace KP_RES 
{
    partial class Frm_Chongiaban 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnGiaban1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnGiaban2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnGiaban3 = new DevExpress.XtraEditors.SimpleButton();
            this.btnGiaban4 = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(13, 44);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(94, 19);
            this.lblTEN.TabIndex = 0;
            this.lblTEN.Text = "Chọn giá bán";
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_300x15;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(492, 35);
            this.panel2.TabIndex = 7;
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(13, 423);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(466, 80);
            this.btnDong.TabIndex = 5;
            this.btnDong.Text = "&Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnGiaban1
            // 
            this.btnGiaban1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGiaban1.Appearance.Options.UseFont = true;
            this.btnGiaban1.Appearance.Options.UseTextOptions = true;
            this.btnGiaban1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnGiaban1.Image = global::KP_RES.Properties.Resources.vegetarian_food_26;
            this.btnGiaban1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnGiaban1.Location = new System.Drawing.Point(13, 71);
            this.btnGiaban1.Margin = new System.Windows.Forms.Padding(4);
            this.btnGiaban1.Name = "btnGiaban1";
            this.btnGiaban1.Size = new System.Drawing.Size(466, 80);
            this.btnGiaban1.TabIndex = 11;
            this.btnGiaban1.Text = "   GIÁ BÁN 1 : GIÁ BÁN MẶC ĐỊNH CHO KHÁCH";
            this.btnGiaban1.Click += new System.EventHandler(this.btnGiaban1_Click);
            // 
            // btnGiaban2
            // 
            this.btnGiaban2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGiaban2.Appearance.Options.UseFont = true;
            this.btnGiaban2.Appearance.Options.UseTextOptions = true;
            this.btnGiaban2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnGiaban2.Image = global::KP_RES.Properties.Resources.vegetarian_food_26;
            this.btnGiaban2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnGiaban2.Location = new System.Drawing.Point(13, 159);
            this.btnGiaban2.Margin = new System.Windows.Forms.Padding(4);
            this.btnGiaban2.Name = "btnGiaban2";
            this.btnGiaban2.Size = new System.Drawing.Size(466, 80);
            this.btnGiaban2.TabIndex = 12;
            this.btnGiaban2.Text = "   GIÁ BÁN 2 : GIÁ BÁN GIAO HÀNG";
            this.btnGiaban2.Click += new System.EventHandler(this.btnGiaban2_Click);
            // 
            // btnGiaban3
            // 
            this.btnGiaban3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGiaban3.Appearance.Options.UseFont = true;
            this.btnGiaban3.Appearance.Options.UseTextOptions = true;
            this.btnGiaban3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnGiaban3.Image = global::KP_RES.Properties.Resources.vegetarian_food_26;
            this.btnGiaban3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnGiaban3.Location = new System.Drawing.Point(13, 247);
            this.btnGiaban3.Margin = new System.Windows.Forms.Padding(4);
            this.btnGiaban3.Name = "btnGiaban3";
            this.btnGiaban3.Size = new System.Drawing.Size(466, 80);
            this.btnGiaban3.TabIndex = 13;
            this.btnGiaban3.Text = "   GIÁ BÁN 3 : GIÁ BÁN CHO NHÂN VIÊN";
            this.btnGiaban3.Click += new System.EventHandler(this.btnGiaban3_Click);
            // 
            // btnGiaban4
            // 
            this.btnGiaban4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGiaban4.Appearance.Options.UseFont = true;
            this.btnGiaban4.Appearance.Options.UseTextOptions = true;
            this.btnGiaban4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnGiaban4.Image = global::KP_RES.Properties.Resources.vegetarian_food_26;
            this.btnGiaban4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnGiaban4.Location = new System.Drawing.Point(13, 335);
            this.btnGiaban4.Margin = new System.Windows.Forms.Padding(4);
            this.btnGiaban4.Name = "btnGiaban4";
            this.btnGiaban4.Size = new System.Drawing.Size(466, 80);
            this.btnGiaban4.TabIndex = 14;
            this.btnGiaban4.Text = "   GIÁ BÁN 4 : GIÁ BÁN NỘI BỘ";
            this.btnGiaban4.Click += new System.EventHandler(this.btnGiaban4_Click);
            // 
            // Frm_Chongiaban
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 519);
            this.ControlBox = false;
            this.Controls.Add(this.btnGiaban4);
            this.Controls.Add(this.btnGiaban3);
            this.Controls.Add(this.btnGiaban2);
            this.Controls.Add(this.btnGiaban1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lblTEN);
            this.Controls.Add(this.btnDong);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_Chongiaban";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chọn giá bán";
            this.Load += new System.EventHandler(this.Frm_Chongiaban_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.SimpleButton btnGiaban1;
        private DevExpress.XtraEditors.SimpleButton btnGiaban2;
        private DevExpress.XtraEditors.SimpleButton btnGiaban3;
        private DevExpress.XtraEditors.SimpleButton btnGiaban4;




    }
}