﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_Chongiaban : DevExpress.XtraEditors.XtraForm
    {
        public string sChon_Gia = "";
        public string smaMon_Dau = "";
        public string sgiaMon_Dau = "";
        public Frm_Chongiaban()
        {
            InitializeComponent();
        }

        private void Frm_Chongiaban_Load(object sender, EventArgs e)
        {
            LoadOption();
        }

        private void LoadOption()
        {
            string sSQL = "Select GIABAN1, GIABAN2,GIABAN3,GIABAN4 From HANGHOA where MA_HANGHOA= "+ clsMain.SQLString (smaMon_Dau);
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["GIABAN1"].ToString() == sgiaMon_Dau)
                {
                    SetBackgroudOnClick(btnGiaban1);
                }
                else if (dr["GIABAN2"].ToString() == sgiaMon_Dau)
                {
                    SetBackgroudOnClick(btnGiaban2);
                }
                else if (dr["GIABAN3"].ToString() == sgiaMon_Dau)
                {
                    SetBackgroudOnClick(btnGiaban3);
                }
                else if (dr["GIABAN4"].ToString() == sgiaMon_Dau)
                {
                    SetBackgroudOnClick(btnGiaban4);
                }
            }
        }

        private void SetBackgroudOnClick(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.YellowGreen;
            _btn.Appearance.BackColor2 = Color.YellowGreen;
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            sChon_Gia = "";
            this.Close();
        }

        private void btnGiaban1_Click(object sender, EventArgs e)
        {
            sChon_Gia = "GIABAN1";
            this.Close();
        }

        private void btnGiaban2_Click(object sender, EventArgs e)
        {
            sChon_Gia = "GIABAN2";
            this.Close();
        }

        private void btnGiaban3_Click(object sender, EventArgs e)
        {
            sChon_Gia = "GIABAN3";
            this.Close();
        }

        private void btnGiaban4_Click(object sender, EventArgs e)
        {
            sChon_Gia = "GIABAN4";
            this.Close();
        }
    }
}