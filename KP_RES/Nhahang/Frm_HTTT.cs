﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;

namespace KP_RES 
{
    public partial class Frm_HTTT : DevExpress.XtraEditors.XtraForm
    {
        public string sMaHD = "";
        public Frm_HTTT()
        {
            InitializeComponent();
        }

        private void Frm_HTTT_Load(object sender, EventArgs e)
        {
            LoadOption();
        }

        private void LoadOption()
        {
            string sSQL = "exec SP_DANHSACHHOADON";
            DataTable dts = clsMain.ReturnDataTable(sSQL);
            gr_billPaymented.DataSource = dts;
        }

        private void gvBillPaymented_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == SSTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
        }

        private void gvBillPaymented_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            lblBill.Text = gvBillPaymented.GetRowCellValue(gvBillPaymented.FocusedRowHandle, "MA_HOADON").ToString() + " : " + gvBillPaymented.GetRowCellValue(gvBillPaymented.FocusedRowHandle, "TONGTIEN").ToString();
            lblBill.Tag = gvBillPaymented.GetRowCellValue(gvBillPaymented.FocusedRowHandle, "MA_HOADON").ToString();
            string sql = "";
            sql = "SELECT MA_HOADON,TONGTIEN,MONEYVISA,	SOTHETT,MONEYINNERCARD,	MONEYVOUCHER,	MONEYCASH FROM HOADON WHERE MA_HOADON=" + clsMain.SQLString(lblBill.Tag.ToString());
            DataTable dt = clsMain.ReturnDataTable(sql);

            txtSOthe .Tag = dt.Rows[0]["TONGTIEN"].ToString();
            if (int.Parse(dt.Rows[0]["MONEYCASH"].ToString()) > 0)
            {
                chkTienmat.Checked = true;
                chkVisa.Checked = false;
                txtSOthe.Text = "";
                btnChon.Enabled = true;
            }
            else if (int.Parse(dt.Rows[0]["MONEYVISA"].ToString()) > 0)
            {
                chkTienmat.Checked = false;
                chkVisa.Checked = true;
                txtSOthe.Text = dt.Rows[0]["SOTHETT"].ToString();
                btnChon.Enabled = true;
            }
            else
            {
                btnChon.Enabled = false;
            }

        }


        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       
        private void btnChon_Click(object sender, EventArgs e)
        {
            string sql = "";
            if (chkTienmat.Checked)
            {
                sql = "Update HOADON SET  MONEYCASH = TONGTIEN,MONEYVISA=0,SOTHETT='' where MA_HOADON=" + clsMain.SQLString(lblBill.Tag.ToString());
            }
            else
            {
                sql = "Update HOADON SET  MONEYVISA = TONGTIEN,MONEYCASH=0,SOTHETT=" + clsMain.SQLString(txtSOthe.Text) + " where MA_HOADON=" + clsMain.SQLString(lblBill.Tag.ToString());
            }

            bool bsql = clsMain.ExecuteSQL(sql);

            if (bsql)
            {
                XtraMessageBox.Show("Thay đổi thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LoadOption();
            }
            
        }
                
    }
}
