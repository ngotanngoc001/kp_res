﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES
{
    public partial class Frm_SuaGia : DevExpress.XtraEditors.XtraForm
    {
        public bool  bCoSua=false ;
        public DataTable dtHanghoa;
        public bool bTang = false;

        public Frm_SuaGia()
        {
            InitializeComponent();
        }

        private void Frm_SuaGia_Load(object sender, EventArgs e)
        {
            gr_SelectProduct.DataSource = dtHanghoa;
            if (bTang)
            {
                gv_SelectProduct.SetRowCellValue(0, gv_SelectProduct.Columns["GiaHangHoa"], 0);
                btn_Payments_Click(null, null);
            }
        }

        private void gv_SelectProduct_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == SOTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
        }

        private void InputKeyboardNumber(string sInput)
        {
            string sGia = gv_SelectProduct.GetFocusedRowCellValue("GiaHangHoa").ToString();
            if (sGia == "0")
            {
                sGia = sInput;
            }
            else
            {
                sGia = sGia + sInput;
            }
            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "GiaHangHoa", sGia);

        }
       
        private void btn_9_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("9");
        }

        private void btn_8_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("8");
        }

        private void btn_7_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("7");
        }

        private void btn_6_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("6");
        }

        private void btn_5_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("5");
        }

        private void btn_4_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("4");
        }

        private void btn_3_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("3");
        }

        private void btn_1_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("1");
        }

        private void btn_2_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("2");
        }

        private void btn_000_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("000");
        }

        private void btn_0_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("0");
        }

        private void btn_ESC_Click(object sender, EventArgs e)
        {
            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "GiaHangHoa",0);
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "GiaHangHoa", 0);
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.FocusedRowHandle - 1;
                btnMoveRightNumber.Enabled = true;
                if (gv_SelectProduct.FocusedRowHandle == 0)
                {
                    btnMoveLeftNumber.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnMoveRightNumber_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.FocusedRowHandle + 1;
                btnMoveLeftNumber.Enabled = true;
                if (gv_SelectProduct.FocusedRowHandle == gv_SelectProduct.RowCount - 1)
                {
                    btnMoveRightNumber.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btn_Thoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Payments_Click(object sender, EventArgs e)
        {
            btn_0.Focus();
            DataView dv = (DataView )gv_SelectProduct.DataSource;
            DataTable dt = dv.ToTable();
            for (int i = 0; i < dt.Rows .Count; i++)
            {
                try
                {
                    int.Parse (dt.Rows[i]["GiaHangHoa"].ToString ());
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Có lổi . " + ex.Message, "Thông báo");
                    return ;
                }
            }
            
            string sSQL = "";
            sSQL += "delete from CT_HOADON where MA_HOADON=" + clsMain.SQLString(dt.Rows[0]["MaHoaDon"].ToString()) + " and MA_HANGHOA=" + clsMain.SQLString(dt.Rows[0]["MaHangHoa"].ToString()) +"\n";
            clsMain.ExecuteSQL(sSQL);
            sSQL ="";
            for (int i = 0; i < dt.Rows .Count; i++)
            {
                string sSQLTemp = "select MA_CTHOADON,	MA_HANGHOA,	MA_HOADON,	SOLUONG,	GIABAN from CT_HOADON where MA_HOADON=" + clsMain.SQLString(dt.Rows[i]["MaHoaDon"].ToString()) + " and MA_HANGHOA=" + clsMain.SQLString(dt.Rows[i]["MaHangHoa"].ToString()) + " and GIABAN=" + clsMain.SQLString(dt.Rows[i]["GiaHangHoa"].ToString());
                DataTable dtTemp =clsMain .ReturnDataTable (sSQLTemp);
                if(dtTemp .Rows .Count >0)
                {
                    sSQL = "";
                    sSQL += "UPDATE CT_HOADON SET " + "\n";
                    sSQL += "SOLUONG +=" + clsMain.SQLString(dt.Rows[i]["SLHangHoa"].ToString()) + "\n";
                    sSQL += "WHERE MA_HOADON =" + clsMain.SQLString(dt.Rows[i]["MaHoaDon"].ToString()) + "\n";
                    sSQL += "AND Isnull(Is_Combo,0)=" + clsMain.SQLBit(false) + "\n";
                    sSQL += "AND MA_HANGHOA =" + clsMain.SQLString(dt.Rows[i]["MaHangHoa"].ToString()) + "\n";
                    sSQL += "AND GIABAN =" + clsMain.SQLString(dt.Rows[i]["GiaHangHoa"].ToString()) + "\n";
                    clsMain.ExecuteSQL(sSQL);   
                }
                else 
                {
                    sSQL = "";
                    sSQL += "Insert into CT_HOADON(MA_HOADON,MA_HANGHOA,SOLUONG,GIABAN,GIABAN_THAT,THUE,TRANGTHAI,IS_COMBO)";
                    sSQL += "Values (";
                    sSQL += clsMain.SQLString(dt.Rows[i]["MaHoaDon"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dt.Rows[i]["MaHangHoa"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dt.Rows[i]["SLHangHoa"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dt.Rows[i]["GiaHangHoa"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dt.Rows[i]["GiaHangHoa"].ToString()) + ",";
                    sSQL += clsMain.SQLString("0") + ",";
                    sSQL += clsMain.SQLBit(false) + ",";
                    sSQL += clsMain.SQLBit(false) + ")";
                    clsMain.ExecuteSQL(sSQL);
                }
            }
            bCoSua = true;
            this.Close();
        }

    }
}