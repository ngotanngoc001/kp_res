﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Threading;
using KP_UserManagement;
using System.IO;
using KP_Terminal;
using DevExpress.XtraGrid.Views.Grid;
using System.Net.Mail;
using KP_Report;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;

namespace KP_RES
{
    public partial class Frm_BanHang_Buffet : DevExpress.XtraEditors.XtraForm
    {
        int funtions = 1;//Funtion 1 Sơ đồ bàn, Funtion 2 Mở bàn, 3 yêu cầu thêm, 4 send Order,5 Gép bàn , 6 Tách bàn, 7 Payments, 8 buy id Product, 9 Đặt bàn, 10 ket ca,11 Chuyển bàn.
        int sendOrder = 0;
        string maBan = "";
        string tenBan = "";
        string maBanGhep = "";
        string maMon = "";
        DataTable dtLuoi = new DataTable("HANGHOA");
        DataTable dtBan = new DataTable();
        DataTable dtLuoi_First = new DataTable(); // lưu danh sách món trước khi thao tác tăng - giảm món nhanh 30-06-2018 
        int cPageKV = 1;
        int cPageNH = 1;
        int cPageBuffet = 1;
        int cPageMon = 1;
        int cPageBan = 1;
        int maKV = 1;
        string maNH = "1";
        string maBuffet = "1";
        DataTable divide_Table1 = new DataTable();
        DataTable divide_Table2 = new DataTable();
        bool exitReprint = false;
        bool Flag_soluong = false; //true:so luong tren lưới dc bắt đầu lại,False:sô lượng đang tạo +thêm chuỗi.
        string sSQL_DL = "";
        string sSQL_Yeucauthem = "";
        int sokhach = 0;
        bool flag_sole = false; // nếu như hủy món có số lượng là số lẻ
        decimal SoLuong_HuyMon = 0;
        string GhiChu = "";

        public Frm_BanHang_Buffet()
        {
            InitializeComponent();
            cls_KP_RES.LoadBangCauHinh();
            LoadPermission();
            AddColumnDatatable();
            lb_Quay.Text = cls_ConfigCashier.nameCashier;
            lb_Kho.Text = cls_ConfigCashier.nameWarehouse;
            lbBan.Text = "";
            lbMon.Text = "";
            lbCaBan.Text = DateTime.Now.ToString("dd-MM-yyyy");
            lb_NhanVien.Text = clsGlobal.gsNameOfUserLogin;
            lbtimeby.Text = cls_ConfigCashier.sTenCB + ": " + cls_ConfigCashier.sGioBD + " --> " + cls_ConfigCashier.sGioKT;
        }

        string _cb = "";
        string _hcb = "";
        string _mb = "";
        string _sdb = "";
        string _dsb = "";
        string _dsm = "";
        string _gb = "";
        string _hgb = "";
        string _gbd = "";
        string _tb = "";
        string _htb = "";
        string _ngayht = "";
        string _dsmonmuontach = "";
        string _dsmoncanhuy = "";
        string _kc = "";
        string _hkc = "";
        string _thongbao = "";
        string _giaban1 = "";
        string _giaban2 = "";
        string _tgvaocuabannaykxd = "";
        string _ngaydatbankhonghople = "";
        string _tgdenkhonghople = "";
        string _bannaydaconguoidat = "";
        string _manvrong = "";
        string _chuachonmayinchomathang = "";
        string _thanhtoankhongthanhcong = "";
        string _huyhdcothelamhtmatondinh = "";
        string _chuakhaibaodinhluong = "";
        string _chuachonmayinchomhnay = "";
        string _ngaydbkhonghople = "";
        string _chuachontgden = "";
        string _banphainhapdayduthongtin = "";
        string _chuanhapttxacnhan = "";
        string _emailkhople = "";
        string _tiendatcoclakieuso = "";
        string _xacnhandatbanthanhcong = "";
        string _bandangmokhongthelaylaihd = "";
        string _Orderinlailan = "";
        string _daylahhtheothoigia = "";
        string _cobansaptoigiovao = "";
        string _khongcohhthaythe = "";
        string _hhkhongthuoccombo = "";
        string _dong = "";
        string _tachbanloi = "";
        string _loiupdatehh = "";
        string _bgphailabdmtkg = "";
        string _ktlaphdnay = "";
        private void Frm_BanHang_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            _loiupdatehh = rm.GetString("loiupdatehh", culture);
            _bgphailabdmtkg = rm.GetString("bgphailabdmtkg", culture);
            _tachbanloi = rm.GetString("tachbanloi", culture);
            btn_ThuNgan.Text = rm.GetString("thungan", culture);
            btn_NghiepVu.Text = rm.GetString("nghiepvu", culture);
            btn_Chuyenban.Text = rm.GetString("chuyenban", culture);
            _cb = rm.GetString("chuyenban", culture);
            _hcb = rm.GetString("huychuyenban", culture);
            btn_SoDoBan.Text = rm.GetString("moban", culture);
            _mb = rm.GetString("moban", culture);
            _sdb = rm.GetString("sodoban", culture);
            _dsb = rm.GetString("danhsachban", culture);
            _dsm = rm.GetString("danhsachmon", culture);
            btn_GuiOrder.Text = rm.GetString("guiorder", culture);
            btn_YeuCauThem.Text = rm.GetString("yeucauthem", culture);
            btn_GhepBan.Text = rm.GetString("ghepban", culture);
            _gb = rm.GetString("ghepban", culture);
            _hgb = rm.GetString("huyghepban", culture);
            _gbd = rm.GetString("ghepbanden", culture);
            btn_Tachban.Text = rm.GetString("tachban", culture);
            _tb = rm.GetString("tachban", culture);
            _htb = rm.GetString("huytachban", culture);
            _ngayht = rm.GetString("ngayht", culture);
            lbCaBan.Text = DateTime.Now.ToString("dd-MM-yyyy");
            btn_ThanhToan.Text = rm.GetString("F1thanhtoan", culture);
            btn_TamTinh.Text = "F5" + "\n" + rm.GetString("intamtinh", culture);
            //btn_BanGia2.Text = rm.GetString("giaban2", culture);
            _giaban1 = rm.GetString("giaban1", culture);
            _giaban2 = rm.GetString("giaban2", culture);
            btn_InLaiHoaDon.Text = rm.GetString("inlaihoadon", culture);
            btn_DatBan.Text = rm.GetString("datban", culture);
            btn_KetCa.Text = rm.GetString("ketca", culture);
            _kc = rm.GetString("ketca", culture);
            _dong = rm.GetString("dong", culture);
            _hkc = rm.GetString("huyketca", culture);
            btn_LayLaiHoaDon.Text = rm.GetString("laylaihoadon", culture);
            btn_HuyBan.Text = rm.GetString("huyban", culture);
            btn_HuyMon.Text = rm.GetString("huymon", culture);
            btn_InlaiOrder.Text = rm.GetString("inlaiorder", culture);
            btn_MoKet.Text = rm.GetString("moket", culture);
            btn_Dong.Text = "ESC" + "\n" + rm.GetString("dong", culture);
            lb_phuthu.Text = rm.GetString("phuthu", culture);
            lb_giamgia.Text = rm.GetString("giamgia", culture);
            lb_tongcong.Text = rm.GetString("tongcong", culture);
            btnComboOption.Text = rm.GetString("tuychon", culture);
            btn_xoa.Text = rm.GetString("xoa", culture);
            btn_XoaTatCa.Text = rm.GetString("xoatatca", culture);
            gv_SelectProduct.Columns["SOTT"].Caption = rm.GetString("stt", culture);
            gv_SelectProduct.Columns["TEN_HANGHOA"].Caption = rm.GetString("tenhanghoa", culture);
            gv_SelectProduct.Columns["SOLUONG"].Caption = rm.GetString("soluong", culture);
            gv_SelectProduct.Columns["GIABAN"].Caption = rm.GetString("dongia", culture);
            gv_SelectProduct.Columns["Thanh_Tien"].Caption = rm.GetString("thanhtien", culture);
            lb_soluong.Text = rm.GetString("soluong1", culture);
            lb_KhuVuc.Text = rm.GetString("khuvuc", culture);
            lb_Ban.Text = rm.GetString("ban", culture);
            lb_TenKH.Text = rm.GetString("tenkh", culture);
            _dsmonmuontach = rm.GetString("dsmonmuontach", culture);
            gvBillPaymented.Columns["SSTT"].Caption = rm.GetString("stt", culture);
            gvBillPaymented.Columns["MA_HOADON"].Caption = rm.GetString("mahoadon", culture);
            gvBillPaymented.Columns["TEN_KHUVUC"].Caption = rm.GetString("khuvuc", culture);
            gvBillPaymented.Columns["TEN_BAN"].Caption = rm.GetString("ban", culture);
            gvBillPaymented.Columns["NGAYTAO"].Caption = rm.GetString("ngaytao", culture);
            gvBillPaymented.Columns["TONGTIEN"].Caption = rm.GetString("tongtien", culture);
            gv_divide_Table_1.Columns["_SSTT"].Caption = rm.GetString("stt", culture);
            gv_divide_Table_1.Columns["_TEN_HANGHOA"].Caption = rm.GetString("tenhanghoa", culture);
            gv_divide_Table_1.Columns["_SOLUONG"].Caption = rm.GetString("soluong", culture);
            gv_divide_Table_1.Columns["_GIABAN"].Caption = rm.GetString("dongia", culture);
            gv_divide_Table_1.Columns["_Thanh_Tien"].Caption = rm.GetString("thanhtien", culture);
            gv_divide_Table_2.Columns["__STT"].Caption = rm.GetString("stt", culture);
            gv_divide_Table_2.Columns["__TEN_HANGHOA"].Caption = rm.GetString("tenhanghoa", culture);
            gv_divide_Table_2.Columns["__SOLUONG"].Caption = rm.GetString("soluong", culture);
            gv_divide_Table_2.Columns["__GIABAN"].Caption = rm.GetString("dongia", culture);
            gv_divide_Table_2.Columns["__Thanh_Tien"].Caption = rm.GetString("thanhtien", culture);
            _dsmoncanhuy = rm.GetString("dsmoncanhuy", culture);
            pnInforTable.Text = rm.GetString("thongtinbandat", culture);
            lb_TenKH2.Text = rm.GetString("khachhang", culture);
            lb_dienthoai2.Text = rm.GetString("dienthoai", culture);
            lb_ngay2.Text = rm.GetString("ngay", culture);
            lb_Diachi.Text = rm.GetString("diachi", culture);
            lb_Tiendatcoc.Text = rm.GetString("tiendatcoc", culture);
            lb_Gio.Text = rm.GetString("gio", culture);
            chkEmail.Text = rm.GetString("guiemail", culture);
            btn_SelectTable.Text = rm.GetString("chonban", culture);
            btn_SelectDishes.Text = rm.GetString("chonmon", culture);
            btnSendEmail.Text = rm.GetString("guiemail", culture);
            btnRemove.Text = rm.GetString("huyban", culture);
            btn_Danhsachdatban.Text = rm.GetString("danhsachdb", culture);
            btnMoBanNgay.Text = rm.GetString("mobanngay", culture);
            btnMobansau.Text = rm.GetString("mobansau", culture);
            btn_Close.Text = rm.GetString("dong", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _tgvaocuabannaykxd = rm.GetString("tgvaocuabannaykxd", culture);
            _ngaydatbankhonghople = rm.GetString("ngaydatbankhonghople", culture);
            _tgdenkhonghople = rm.GetString("tgdenkhonghople", culture);
            _bannaydaconguoidat = rm.GetString("bannaydaconguoidat", culture);
            _manvrong = rm.GetString("manvrong", culture);
            _chuachonmayinchomathang = rm.GetString("chuachonmayinchomathang", culture);
            _thanhtoankhongthanhcong = rm.GetString("thanhtoankhongthanhcong", culture);
            _huyhdcothelamhtmatondinh = rm.GetString("huyhdcothelamhtmatondinh", culture);
            _chuakhaibaodinhluong = rm.GetString("chuakhaibaodinhluong", culture);
            _chuachonmayinchomhnay = rm.GetString("chuachonmayinchomhnay", culture);
            _ngaydbkhonghople = rm.GetString("ngaydbkhonghople", culture);
            _chuachontgden = rm.GetString("chuachontgden", culture);
            _banphainhapdayduthongtin = rm.GetString("banphainhapdayduthongtin", culture);
            _chuanhapttxacnhan = rm.GetString("chuanhapttxacnhan", culture);
            _emailkhople = rm.GetString("emailkhople", culture);
            _tiendatcoclakieuso = rm.GetString("tiendatcoclakieuso", culture);
            _xacnhandatbanthanhcong = rm.GetString("xacnhandatbanthanhcong", culture);
            _bandangmokhongthelaylaihd = rm.GetString("bandangmokhongthelaylaihd", culture);
            _Orderinlailan = rm.GetString("Orderinlailan", culture);
            _daylahhtheothoigia = rm.GetString("daylahhtheothoigia", culture);
            _cobansaptoigiovao = rm.GetString("cobansaptoigiovao", culture);
            _khongcohhthaythe = rm.GetString("khongcohhthaythe", culture);
            _hhkhongthuoccombo = rm.GetString("hhkhongthuoccombo", culture);
        }

        private void Frm_BanHang_Load(object sender, EventArgs e)
        {
            SetBackgroudOnClick(btn_ThuNgan);
            SetDefautlBackgroud(btn_NghiepVu);
            SetDefautlBackgroud(btn_DatThu);
            pnThuNgan.BringToFront();
            pnShowHang.BringToFront();
            pnBtnDeleteGv.Visible = false;
            pnUpDowGv.Visible = false;
            btnComboOption.Visible = false;
            txtID.Visible = false;
            LoadCombo();
            GetKhuVuc();
            GetSoDoBan();
        }

        private void Frm_BanHang_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                if (funtions == 1 && maBan != "" && gv_SelectProduct.RowCount > 0)
                {
                    btn_Payment_Click(sender, e);
                }
            }
            else if (e.KeyCode == Keys.F5)
            {
                btn_TamTinh_Click(null, null);
            }
            else if (e.KeyCode == Keys.Escape)
            {
                btn_logout_Click(null, null);
            }
        }

        private void LoadPermission()
        {
            //30/08/2016 Thong them cau hinh co dung chuc nang in tam tinh thay khong
            string sIntamtinh = clsUserManagement.CheckCauhinh("INTAMTINH");
            bool bIntamtinh = false;
            if (sIntamtinh != "Error")
            {
                bIntamtinh = sIntamtinh == "1" ? true : false;
            }
            btn_ThanhToan.Enabled = clsUserManagement.AllowAdd("28071801");
            btn_TamTinh.Enabled = bIntamtinh && clsUserManagement.AllowView("28071801");
            btn_InLaiHoaDon.Enabled = clsUserManagement.AllowEdit("28071801");
            btn_LayLaiHoaDon.Enabled = clsUserManagement.AllowView("25101801");
            btn_HuyBan.Enabled = clsUserManagement.AllowDelete("28071801");
            btn_HuyMon.Enabled = clsUserManagement.AllowDelete("28071801");
            btn_NghiepVu.Enabled = clsUserManagement.AllowAdd("28071801");
            btn_DatThu.Enabled = clsUserManagement.AllowAdd("28071801");
        }

        private void LoadCombo()
        {
            string sSQL = "";
            DataTable dt = new DataTable();
            sSQL = string.Format("Exec SelectHangHoa_Nhahang @maQUAY={0}", cls_ConfigCashier.idCashier);
            dt = clsMain.ReturnDataTable(sSQL);
            txtID.Properties.DataSource = dt;
            txtID.EditValue = txtID.Properties.GetDataSourceValue(txtID.Properties.ValueMember, 0);
        }

        private void AddColumnDatatable()
        {
            //add column dt luoi
            dtLuoi.Columns.Add("MA_HANGHOA", Type.GetType("System.String"));
            dtLuoi.Columns.Add("TEN_HANGHOA", Type.GetType("System.String"));
            dtLuoi.Columns.Add("SOLUONG", Type.GetType("System.Double"));
            dtLuoi.Columns.Add("GIABAN", Type.GetType("System.Double"));
            dtLuoi.Columns.Add("GHICHU", Type.GetType("System.String"));
            dtLuoi.Columns.Add("MA_BEP", typeof(Int32));
            dtLuoi.Columns.Add("THUE", Type.GetType("System.Double"));
            dtLuoi.Columns.Add("Thanh_Tien", Type.GetType("System.Double"));
            dtLuoi.Columns.Add("TRANGTHAI", typeof(bool));
            dtLuoi.Columns.Add("IS_COMBO", typeof(bool));
            dtLuoi.Columns.Add("IS_INBEP", typeof(bool));
            dtLuoi.Columns.Add("IS_KHUYENMAI", typeof(bool));
            dtLuoi.Columns.Add("CHIETKHAU", Type.GetType("System.Double"));
            //add colum tach ban
            divide_Table1.Columns.Add("_MA_HANGHOA", Type.GetType("System.String"));
            divide_Table1.Columns.Add("_TEN_HANGHOA", Type.GetType("System.String"));
            divide_Table1.Columns.Add("_SOLUONG", Type.GetType("System.Double"));
            divide_Table1.Columns.Add("_GIABAN", Type.GetType("System.Double"));
            divide_Table1.Columns.Add("__MA_BEP", typeof(Int32));
            divide_Table1.Columns.Add("_Thanh_Tien", Type.GetType("System.Double"));
            //add colum tach ban
            divide_Table2.Columns.Add("__MA_HANGHOA", Type.GetType("System.String"));
            divide_Table2.Columns.Add("__TEN_HANGHOA", Type.GetType("System.String"));
            divide_Table2.Columns.Add("__SOLUONG", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("__GIABAN", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("__MA_BEP", typeof(Int32));
            divide_Table2.Columns.Add("__Thanh_Tien", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("__IS_COMBO", typeof(bool));
        }

        private void SetBackgroudOnClick(SimpleButton _btn)
        {
            if (cls_KP_RES.iHinhanh == 1)
            {
                if (_btn.BackgroundImageLayout == ImageLayout.Stretch)
                {
                    return;
                }
            }
            _btn.Appearance.BackColor = Color.YellowGreen;
            _btn.Appearance.BackColor2 = Color.YellowGreen;
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private void SetDefautlBackgroud(SimpleButton _btn)
        {
            if (cls_KP_RES.iHinhanh == 1)
            {
                if (_btn.BackgroundImageLayout == ImageLayout.Stretch)
                {
                    return;
                }
            }
            _btn.Appearance.BackColor = Color.White;
            if (_btn.Appearance.Options.UseBackColor == true)
                _btn.Appearance.Options.UseBackColor = false;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == false)
                _btn.LookAndFeel.UseDefaultLookAndFeel = true;
        }

        private void GetKhuVuc()
        {
            try
            {
                pnlBuffet.Visible = false;
                DataTable dtKhuVuc = new DataTable();
                dtKhuVuc.Rows.Clear();
                string SQLKV = string.Format("Exec SelectKhuVuc @PageNumber={0},@PageSize={1}, @maCH={2}, @maQUAY={3}", cPageKV, 7, cls_ConfigCashier.idShop, cls_ConfigCashier.idCashier);
                dtKhuVuc = clsMain.ReturnDataTable(SQLKV);

                while (pnKV_Mon.Controls.Count > 0)
                {
                    pnKV_Mon.Controls[0].Dispose();
                }
                pnKV_Mon.Controls.Clear();
                int locateY = 2;
                int locateX = 3;
                if (maKV == 1 && dtKhuVuc.Rows.Count > 0)
                {
                    maKV = int.Parse(dtKhuVuc.Rows[0]["MA_KHUVUC"].ToString());
                }
                for (int i = 0; i < dtKhuVuc.Rows.Count; i++)
                {
                    SimpleButton btnKV = new SimpleButton();
                    btnKV.Name = dtKhuVuc.Rows[i]["MA_KHUVUC"].ToString();
                    if (int.Parse(dtKhuVuc.Rows[i]["MA_KHUVUC"].ToString()) == maKV)
                    {
                        lbKhuVuc.Text = "" + dtKhuVuc.Rows[i]["TEN_KHUVUC"].ToString() + ">";
                        SetBackgroudOnClick(btnKV);
                    }
                    btnKV.Text = dtKhuVuc.Rows[i]["TEN_KHUVUC"].ToString();
                    try
                    {
                        byte[] tam = new byte[((byte[])dtKhuVuc.Rows[i]["HINHANH"]).LongLength];
                        tam = (byte[])dtKhuVuc.Rows[i]["HINHANH"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        if (cls_KP_RES.iHinhanh == 1)
                        {
                            btnKV.BackgroundImage = bm;
                            btnKV.BackgroundImageLayout = ImageLayout.Stretch;
                            btnKV.Appearance.BackColor = Color.Transparent;
                            btnKV.Appearance.Options.UseBackColor = true;
                            btnKV.ButtonStyle = BorderStyles.NoBorder;
                            btnKV.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                        }
                        else
                        {
                            btnKV.Image = bm;
                        }
                    }
                    catch
                    {
                    }
                    btnKV.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btnKV.ImageLocation = ImageLocation.TopCenter;
                    btnKV.RightToLeft = RightToLeft.No;
                    btnKV.Size = new Size(125, 80);
                    if (i != 0)
                    {
                        locateY += int.Parse(btnKV.Location.Y.ToString()) + 85;
                    }
                    btnKV.Location = new Point(locateX, locateY);
                    btnKV.Click += new EventHandler(btnKV_Click);
                    pnKV_Mon.Controls.Add(btnKV);
                }
            }
            catch
            {
            }
        }

        private void GetSoDoBan()
        {
            try
            {
                dtBan.Clear();
                string sql = string.Format("Exec SelectTable @PageNumber={0},@maKV={1}", cPageBan, maKV);
                dtBan = clsMain.ReturnDataTable(sql);

                while (pnTable_Mon.Controls.Count > 0)
                {
                    pnTable_Mon.Controls[0].Dispose();
                }
                pnTable_Mon.Controls.Clear();
                for (int i = 0; i < dtBan.Rows.Count; i++)
                {
                    SimpleButton btnBan_Mon = new SimpleButton();
                    btnBan_Mon.Name = dtBan.Rows[i]["MA_BAN"].ToString();
                    if (dtBan.Rows[i]["SOGHE"].ToString() != "0")
                    {
                        btnBan_Mon.Text = dtBan.Rows[i]["TEN_BAN"].ToString() + " - " + dtBan.Rows[i]["SOGHE"].ToString() + "  Ghế";
                    }
                    else
                    {
                        btnBan_Mon.Text = dtBan.Rows[i]["TEN_BAN"].ToString();
                    }
                    try
                    {
                        byte[] tam = new byte[((byte[])dtBan.Rows[i]["HINHANH"]).LongLength];
                        tam = (byte[])dtBan.Rows[i]["HINHANH"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        if (cls_KP_RES.iHinhanh == 1)
                        {
                            btnBan_Mon.BackgroundImage = bm;
                            btnBan_Mon.BackgroundImageLayout = ImageLayout.Stretch;
                            btnBan_Mon.Appearance.BackColor = Color.Transparent;
                            btnBan_Mon.Appearance.Options.UseBackColor = true;
                            btnBan_Mon.ButtonStyle = BorderStyles.NoBorder;
                            btnBan_Mon.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                        }
                        else
                        {
                            btnBan_Mon.Image = bm;
                        }
                    }
                    catch
                    {
                    }
                    btnBan_Mon.Width = int.Parse(dtBan.Rows[i]["Width"].ToString());
                    btnBan_Mon.Height = int.Parse(dtBan.Rows[i]["Height"].ToString());
                    btnBan_Mon.Location = new Point(int.Parse(dtBan.Rows[i]["LocaX"].ToString()), int.Parse(dtBan.Rows[i]["LocaY"].ToString()));

                    try
                    {
                        if (bool.Parse(dtBan.Rows[i]["ISBANDAT"].ToString()) == true)
                        {
                            SetBackgroudOnClick2(btnBan_Mon);
                        }
                    }
                    catch
                    {
                    }
                    try
                    {
                        if (bool.Parse(dtBan.Rows[i]["ISMO_BAN"].ToString()) == true)
                        {
                            SetBackgroudOnClick1(btnBan_Mon);
                            DataTable dataInPut = getGioVaoBan(btnBan_Mon.Name.ToString());
                            if (dataInPut.Rows.Count > 0)
                            {
                                btnBan_Mon.Text += "\n Giờ vào: " + cls_KP_RES.DateToString_Full_NoSecond((DateTime)dataInPut.Rows[0][0]);
                                DateTime aDateTime = DateTime.Now;
                                TimeSpan newTime = aDateTime - ((DateTime)dataInPut.Rows[0][0]);
                                btnBan_Mon.Text += "\n Đã ngồi: " + newTime.Hours + " giờ " + newTime.Minutes + " phút ";

                                int sThoiGianDaNgoi = (int)newTime.TotalMinutes;
                                int sThoiGianQuyDinh = int.Parse(cls_ConfigCashier.sCauHinhThoiGian);
                                if (sThoiGianDaNgoi >= sThoiGianQuyDinh)
                                {
                                    btnBan_Mon.Tag = "ngoiquagio";
                                    SetBackgroudOnClick4(btnBan_Mon);
                                }
                            }
                            DataTable dataTongtien = getTongtien(btnBan_Mon.Name.ToString());
                            if (dataTongtien.Rows.Count > 0)
                            {
                                btnBan_Mon.Text += "\n Tổng tiền: " + int.Parse(dataTongtien.Rows[0]["TONGTIEN"].ToString()).ToString("N0");
                            }
                            string idHD = GetMaHoaDon(btnBan_Mon.Name.ToString());
                            DataTable tb = DemSoBanTrongBill(idHD);
                            if (tb.Rows.Count > 1)
                            {
                                btnBan_Mon.Text += " \n GB " + idHD.Substring(idHD.IndexOf("B") + 1, 4);
                            }
                        }
                    }
                    catch
                    {
                    }
                    try
                    {
                        if (bool.Parse(dtBan.Rows[i]["SEE_PAYMENTS"].ToString()) == true)
                        {
                            SetBackgroudOnClick3(btnBan_Mon);
                        }
                    }
                    catch
                    {
                        dtBan.Rows[i]["SEE_PAYMENTS"] = 0;
                        clsMain.ExecuteSQL("update ban set SEE_PAYMENTS=0 where SEE_PAYMENTS is null");
                    }

                    btnBan_Mon.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btnBan_Mon.ImageLocation = ImageLocation.TopCenter;
                    btnBan_Mon.RightToLeft = RightToLeft.No;
                    btnBan_Mon.Click += new EventHandler(btnBan_Mon_Click);
                    pnTable_Mon.Controls.Add(btnBan_Mon);
                }
            }
            catch
            {
            }
        }

        private void SetBackgroudOnClick2(SimpleButton _btn)
        {
            if (cls_KP_RES.iHinhanh == 1)
            {
                if (_btn.BackgroundImageLayout == ImageLayout.Stretch)
                {
                    return;
                }
            }
            _btn.Appearance.BackColor = Color.FromArgb(0, 173, 239);
            _btn.Appearance.BackColor2 = Color.FromArgb(0, 173, 239);
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private void SetBackgroudOnClick3(SimpleButton _btn)
        {
            if (cls_KP_RES.iHinhanh == 1)
            {
                if (_btn.BackgroundImageLayout == ImageLayout.Stretch)
                {
                    return;
                }
            }
            _btn.Appearance.BackColor = Color.FromArgb(214, 223, 35);
            _btn.Appearance.BackColor2 = Color.FromArgb(214, 223, 35);
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;

        }

        private void SetBackgroudOnClick4(SimpleButton _btn)
        {
            if (cls_KP_RES.iHinhanh == 1)
            {
                if (_btn.BackgroundImageLayout == ImageLayout.Stretch)
                {
                    return;
                }
            }
            _btn.Appearance.BackColor = Color.FromArgb(199, 21, 112);
            _btn.Appearance.BackColor2 = Color.FromArgb(199, 21, 112);
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private void SetBackgroudOnClick5(SimpleButton _btn)
        {
            if (cls_KP_RES.iHinhanh == 1)
            {
                if (_btn.BackgroundImageLayout == ImageLayout.Stretch)
                {
                    return;
                }
            }
            _btn.Appearance.BackColor = Color.FromArgb(255, 184, 193);
            _btn.Appearance.BackColor2 = Color.FromArgb(255, 184, 193);
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private DataTable getGioVaoBan(string maBan)
        {
            string sSQL = "";
            sSQL += "Select GIOVAO" + "\n";
            sSQL += "From HOADON hd,CT_MOBAN ct" + "\n";
            sSQL += "Where hd.MA_HOADON = ct.MA_HOADON And hd.ISPAYMENT = 0 And hd.ISBANDAT=0" + "\n";
            sSQL += "And ct.MA_BAN = " + clsMain.SQLString(maBan);
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                return dt;
            }
            else
            {
                sSQL = "";
                sSQL += "Select GIOVAO" + "\n";
                sSQL += "From HOADON hd,CT_MOBAN ct" + "\n";
                sSQL += "Where hd.MA_HOADON = ct.MA_HOADON And hd.ISPAYMENT = 0 And hd.ISBANDAT=1" + "\n";
                sSQL += "And ct.MA_BAN = " + clsMain.SQLString(maBan) + "\n";
                sSQL += "Order by GIOVAO ASC";
                dt = clsMain.ReturnDataTable(sSQL);
                return dt;
            }
        }

        private DataTable getTongtien(string maBan)
        {
            string sSQL = "";
            sSQL += "SELECT SUM((CT_HOADON .SOLUONG * ISNULL(CT_HOADON .GIABAN_THAT,CT_HOADON .GIABAN)))   AS TONGTIEN" + "\n";
            sSQL += "FROM HOADON " + "\n";
            sSQL += "INNER JOIN CT_HOADON ON HOADON .MA_HOADON =CT_HOADON .MA_HOADON " + "\n";
            sSQL += "INNER JOIN CT_MOBAN ON HOADON .MA_HOADON =CT_MOBAN.MA_HOADON " + "\n";
            sSQL += "WHERE HOADON .ISPAYMENT =0" + "\n";
            sSQL += "AND CT_MOBAN .MA_BAN =  " + clsMain.SQLString(maBan) + "\n";
            sSQL += "GROUP BY HOADON  .MA_HOADON  " + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            return dt;
        }

        private void SetBackgroudOnClick1(SimpleButton _btn)
        {
            if (cls_KP_RES.iHinhanh == 1)
            {
                if (_btn.BackgroundImageLayout == ImageLayout.Stretch)
                {
                    return;
                }
            }
            _btn.Appearance.BackColor = Color.FromArgb(229, 137, 45);
            _btn.Appearance.BackColor2 = Color.FromArgb(229, 137, 45);
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private string GetMaHoaDon(string maBan)
        {
            string sSQL = "";
            sSQL += "Select a.MA_HOADON " + "\n";
            sSQL += "From CT_MOBAN as a,HOADON as b,BAN c" + "\n";
            sSQL += "Where a.MA_HOADON = b.MA_HOADON And a.MA_BAN=c.MA_BAN And b.ISBANDAT=0" + "\n";
            sSQL += "And ISMO_BAN=1 And b.ISPAYMENT = 0 And a.MA_BAN = " + clsMain.SQLString(maBan) + "\n";
            sSQL += "Order by MA_HOADON desc";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0]["MA_HOADON"].ToString();
            }
            else
            {
                sSQL = "";
                sSQL += "Select a.MA_HOADON " + "\n";
                sSQL += "From CT_MOBAN as a,HOADON as b,BAN c" + "\n";
                sSQL += "Where a.MA_HOADON = b.MA_HOADON And a.MA_BAN=c.MA_BAN And b.ISBANDAT=1" + "\n";
                sSQL += "And ISMO_BAN=1 And b.ISPAYMENT = 0 And a.MA_BAN = " + clsMain.SQLString(maBan) + "\n";
                sSQL += "Order by GIOVAO ASC";
                dt = clsMain.ReturnDataTable(sSQL);
                if (dt.Rows.Count > 0)
                {
                    return dt.Rows[0]["MA_HOADON"].ToString();
                }
                else
                {
                    return "";
                }
            }
        }

        private DataTable DemSoBanTrongBill(string maBill)
        {
            DataTable dttabless;
            string sql = "select MA_BAN from CT_MOBAN where MA_HOADON =" + clsMain.SQLString(maBill);
            dttabless = clsMain.ReturnDataTable(sql);
            return dttabless;
        }

        private void btn_UpBF_Click(object sender, EventArgs e)
        {
            if (cPageBuffet > 1)
            {
                cPageBuffet -= 1;
                GetBuffet();
            }
        }

        private void btn_DowBF_Click(object sender, EventArgs e)
        {
            if (cPageBuffet < 20)
            {
                cPageBuffet += 1;
                GetBuffet();
            }
        }

        private void btn_UpKV_Click(object sender, EventArgs e)
        {
            if (funtions == 1 || funtions == 11 || funtions == 6)
            {
                if (cPageKV > 1)
                {
                    cPageKV -= 1;
                    GetKhuVuc();
                    GetSoDoBan();
                }
            }
            else if (funtions == 2)
            {
                if (cPageNH > 1)
                {
                    cPageNH -= 1;
                    GetNhomhang();
                    GetListMon();
                }
            }
        }

        private void btn_DowKV_Click(object sender, EventArgs e)
        {
            if (funtions == 1 || funtions == 11 || funtions == 6)
            {
                if (cPageKV < 20)
                {
                    cPageKV += 1;
                    GetKhuVuc();
                    GetSoDoBan();
                }
            }
            else if (funtions == 2)
            {
                if (cPageNH < 20)
                {
                    cPageNH += 1;
                    GetNhomhang();
                    GetListMon();
                }
            }
        }

        private void btn_NextToRight_Click(object sender, EventArgs e)
        {
            if (funtions == 1 || funtions == 5 || funtions == 6 || funtions == 11)
            {
                if (pnTable_Mon.HorizontalScroll.Visible)
                {
                    try
                    {
                        pnTable_Mon.AutoScrollPosition = new Point(pnTable_Mon.HorizontalScroll.Value + pnTable_Mon.HorizontalScroll.Maximum / 10, 0);
                        return;
                    }
                    catch
                    {
                        pnTable_Mon.HorizontalScroll.Value = pnTable_Mon.HorizontalScroll.Maximum;
                        return;
                    }
                }
                if (cPageBan == 5)
                    return;
                cPageBan = cPageBan + 1;
                lbPageTable.Text = cPageBan.ToString();
                GetSoDoBan();
            }
            else if (funtions == 2)
            {
                if (cPageMon == 20)
                    return;
                cPageMon = cPageMon + 1;
                lbPageTable.Text = cPageMon.ToString();
                GetListMon();
            }
        }

        private void btn_NextToLeft_Click(object sender, EventArgs e)
        {

            if (funtions == 1 || funtions == 5 || funtions == 6 || funtions == 11)
            {
                if (pnTable_Mon.HorizontalScroll.Visible)
                {
                    try
                    {
                        pnTable_Mon.AutoScrollPosition = new Point(pnTable_Mon.HorizontalScroll.Value - pnTable_Mon.HorizontalScroll.Maximum / 10, 0);
                        return;
                    }
                    catch
                    {
                        pnTable_Mon.HorizontalScroll.Value = pnTable_Mon.HorizontalScroll.Minimum;
                        return;
                    }
                }
                if (cPageBan == 1)
                    return;
                cPageBan = cPageBan - 1;
                lbPageTable.Text = cPageBan.ToString();
                GetSoDoBan();
            }
            else if (funtions == 2)
            {
                if (cPageMon == 1)
                    return;
                cPageMon = cPageMon - 1;
                lbPageTable.Text = cPageMon.ToString();
                GetListMon();
            }
        }

        private void GetBuffet()
        {
            try
            {
                int PageSize = pnBuffet.Width / 130;
                pnlBuffet.Visible = true;
                DataTable dtBuffet = new DataTable();
                dtBuffet.Rows.Clear();
                string SQLMON = string.Format("exec SelectBuffet @PageNumber={0},@PageSize={1},@maQUAY={2},@maBan={3}", cPageBuffet, PageSize, int.Parse(cls_ConfigCashier.idCashier), maBan);
                dtBuffet = clsMain.ReturnDataTable(SQLMON);

                while (pnBuffet.Controls.Count > 0)
                {
                    pnBuffet.Controls[0].Dispose();
                }
                pnBuffet.Controls.Clear();
                int locateY = 0;
                int locateX = 3;
                for (int i = 0; i < dtBuffet.Rows.Count; i++)
                {
                    SimpleButton btnBuffet = new SimpleButton();
                    btnBuffet.Name = dtBuffet.Rows[i]["MA_BUFFET"].ToString();
                    if (i == 0)
                    {
                        maBuffet = dtBuffet.Rows[0]["MA_BUFFET"].ToString();
                        SetBackgroudOnClick(btnBuffet);
                    }
                    btnBuffet.Text = dtBuffet.Rows[i]["TEN_BUFFET"].ToString();
                    btnBuffet.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btnBuffet.ImageLocation = ImageLocation.TopCenter;
                    btnBuffet.RightToLeft = RightToLeft.No;
                    btnBuffet.Size = new Size(125, 36);
                    if (i != 0)
                    {
                        locateX += int.Parse(btnBuffet.Location.X.ToString()) + 130;
                    }
                    btnBuffet.Location = new Point(locateX, locateY);
                    btnBuffet.Click += new EventHandler(btnBuffet_Click);
                    pnBuffet.Controls.Add(btnBuffet);
                }
            }
            catch
            {
            }
        }

        private void GetNhomhang()
        {
            try
            {
                DataTable dtNhomhang = new DataTable();
                dtNhomhang.Rows.Clear();
                string SQLMON = string.Format("exec SelectDish_Buffet @PageNumber={0},@PageSize={1},@maQUAY={2},@maBuffet='{3}'", cPageNH, 7, int.Parse(cls_ConfigCashier.idCashier), maBuffet);
                dtNhomhang = clsMain.ReturnDataTable(SQLMON);

                while (pnKV_Mon.Controls.Count > 0)
                {
                    pnKV_Mon.Controls[0].Dispose();
                }
                pnKV_Mon.Controls.Clear();
                int locateY = 2;
                int locateX = 3;
                for (int i = 0; i < dtNhomhang.Rows.Count; i++)
                {
                    SimpleButton btnKV = new SimpleButton();
                    btnKV.Name = dtNhomhang.Rows[i]["MA_NHOMHANG"].ToString();
                    if (i == 0)
                    {
                        maNH = dtNhomhang.Rows[0]["MA_NHOMHANG"].ToString();
                        SetBackgroudOnClick(btnKV);
                    }
                    btnKV.Text = "" + dtNhomhang.Rows[i]["TEN_NHOMHANG"].ToString();
                    try
                    {
                        byte[] tam = new byte[((byte[])dtNhomhang.Rows[i]["HINHANH"]).LongLength];
                        tam = (byte[])dtNhomhang.Rows[i]["HINHANH"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        if (cls_KP_RES.iHinhanh == 1)
                        {
                            btnKV.BackgroundImage = bm;
                            btnKV.BackgroundImageLayout = ImageLayout.Stretch;
                            btnKV.Appearance.BackColor = Color.Transparent;
                            btnKV.Appearance.Options.UseBackColor = true;
                            btnKV.ButtonStyle = BorderStyles.NoBorder;
                            btnKV.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                        }
                        else
                        {
                            btnKV.Image = bm;
                        }
                    }
                    catch
                    {
                    }
                    btnKV.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btnKV.ImageLocation = ImageLocation.TopCenter;
                    btnKV.RightToLeft = RightToLeft.No;
                    btnKV.Size = new Size(125, 80);
                    if (i != 0)
                    {
                        locateY += int.Parse(btnKV.Location.Y.ToString()) + 85;
                    }
                    btnKV.Location = new Point(locateX, locateY);
                    btnKV.Click += new EventHandler(btnKV_Click);
                    pnKV_Mon.Controls.Add(btnKV);
                }
            }
            catch
            {
            }
        }

        private void GetListMon()
        {
            DataTable dtMon = new DataTable();
            dtMon.Clear();
            int width = (pnTable_Mon.Width / 110);
            int height = (pnTable_Mon.Height / 80);
            int sizeMon = width * height;
            string sql = string.Format("Exec SelectMON_Buffet @PageNumber={0},@PageSize={1}, @maNH='{2}', @maKV={3},@maCH={4},@maBuffet='{5}'", cPageMon, sizeMon, maNH, maKV, cls_ConfigCashier.idShop, maBuffet);
            dtMon = clsMain.ReturnDataTable(sql);

            while (pnTable_Mon.Controls.Count > 0)
            {
                pnTable_Mon.Controls[0].Dispose();
            }
            pnTable_Mon.Controls.Clear();
            int count = (pnTable_Mon.Width / 120) + 1;
            int line = 0;
            int locateY = 2;
            int locateX = 2;
            for (int i = 0; i < dtMon.Rows.Count; i++)
            {
                line++;
                SimpleButton btnBan_Mon = new SimpleButton();
                btnBan_Mon.Name = dtMon.Rows[i]["MA_HANGHOA"].ToString();
                btnBan_Mon.Text = dtMon.Rows[i]["TEN_HANGHOA"].ToString() + "\n" + "Giá:" + string.Format("{0:0,0 }", double.Parse(dtMon.Rows[i]["GIABAN"].ToString())) + "";
                btnBan_Mon.Tag = dtMon.Rows[i]["TEN_HANGHOA"].ToString() + " - " + "Giá:" + string.Format("{0:0,0 }", double.Parse(dtMon.Rows[i]["GIABAN"].ToString())) + "";
                try
                {
                    byte[] tam = new byte[((byte[])dtMon.Rows[i]["HINHANH"]).LongLength];
                    tam = (byte[])dtMon.Rows[i]["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    if (cls_KP_RES.iHinhanh == 1)
                    {
                        btnBan_Mon.BackgroundImage = bm;
                        btnBan_Mon.BackgroundImageLayout = ImageLayout.Stretch;
                        btnBan_Mon.Appearance.BackColor = Color.Transparent;
                        btnBan_Mon.Appearance.Options.UseBackColor = true;
                        btnBan_Mon.ButtonStyle = BorderStyles.NoBorder;
                        btnBan_Mon.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                    }
                    else
                    {
                        btnBan_Mon.Image = bm;
                    }

                }
                catch
                {
                }
                btnBan_Mon.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btnBan_Mon.ImageLocation = ImageLocation.TopCenter;
                btnBan_Mon.RightToLeft = RightToLeft.No;
                if (cls_KP_RES.iHinhanh == 1)
                {
                    btnBan_Mon.Size = new Size(168, 130);
                    if (this.Width < 1100)
                    {
                        if (line == 4)
                        {
                            locateY += int.Parse(btnBan_Mon.Location.Y.ToString()) + 133;
                            locateX = 2;
                            line = 1;
                        }
                        else
                        {
                            if (i != 0)
                            {
                                locateX += int.Parse(btnBan_Mon.Location.X.ToString()) + 171;
                            }
                        }
                    }
                    else
                    {
                        if (line == (pnTable_Mon.Width / btnBan_Mon.Width) + 1)
                        {
                            locateY += int.Parse(btnBan_Mon.Location.Y.ToString()) + 133;
                            locateX = 2;
                            line = 1;
                        }
                        else
                        {
                            if (i != 0)
                            {
                                locateX += int.Parse(btnBan_Mon.Location.X.ToString()) + 171;
                            }
                        }
                    }
                }
                else
                {
                    btnBan_Mon.Size = new Size(110, 80);
                    if (line == count)
                    {
                        locateY += int.Parse(btnBan_Mon.Location.Y.ToString()) + 85;
                        locateX = 2;
                        line = 1;
                    }
                    else
                    {
                        if (i != 0)
                        {
                            if (this.Width < 1100)
                            {
                                locateX += int.Parse(btnBan_Mon.Location.X.ToString()) + 132;
                            }
                            else
                            {
                                locateX += int.Parse(btnBan_Mon.Location.X.ToString()) + 124;
                            }
                        }
                    }
                }
                btnBan_Mon.Location = new Point(locateX, locateY);
                btnBan_Mon.Click += new EventHandler(btnBan_Mon_Click);
                pnTable_Mon.Controls.Add(btnBan_Mon);
            }
        }

        private void btnBuffet_Click(object sender, EventArgs e)
        {
            try
            {
                if (gv_SelectProduct.RowCount > 0)
                    return;
                foreach (SimpleButton btnBuffet in pnBuffet.Controls)
                {
                    if (btnBuffet.Name == this.ActiveControl.Name)
                    {
                        SetBackgroudOnClick(btnBuffet);
                        maBuffet = btnBuffet.Name.ToString();
                        GetNhomhang();
                        GetListMon();
                    }
                    else
                    {
                        SetDefautlBackgroud(btnBuffet);
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK);
            }
        }

        private void btnKV_Click(object sender, EventArgs e)
        {
            try
            {
                cPageBan = 1;
                cPageMon = 1;
                lbPageTable.Text = cPageBan.ToString();
                btnComboOption.Visible = false;
                foreach (SimpleButton btnKV in pnKV_Mon.Controls)
                {
                    if (btnKV.Name == this.ActiveControl.Name)
                    {
                        SetBackgroudOnClick(btnKV);
                        if (funtions == 1)
                        {
                            lbKhuVuc.Text = btnKV.Text + "  >  ";
                            lbMon.Text = "";
                            lbBan.Text = "";
                            maBan = "";
                            maKV = int.Parse(btnKV.Name.ToString());
                            GetSoDoBan();
                        }
                        else if (funtions == 2)
                        {
                            maNH = btnKV.Name.ToString();
                            GetListMon();
                        }
                        else if (funtions == 6)
                        {
                            funtions = 1;
                            lbKhuVuc.Text = btnKV.Text + "  >  ";
                            lbMon.Text = "";
                            lbBan.Text = "";
                            maKV = int.Parse(btnKV.Name.ToString());
                            GetSoDoBan();
                            funtions = 6;
                        }
                        else if (funtions == 11)
                        {
                            funtions = 1;
                            lbKhuVuc.Text = btnKV.Text + "  >  ";
                            lbMon.Text = "";
                            lbBan.Text = "";
                            maKV = int.Parse(btnKV.Name.ToString());
                            GetSoDoBan();
                            funtions = 11;
                        }
                        else if (selectDishes == 2)
                        {
                            funtions = 1;
                            lbKhuVuc.Text = btnKV.Text + "  >  ";
                            lbMon.Text = "";
                            lbBan.Text = "";
                            maBan = "";
                            maKV = int.Parse(btnKV.Name.ToString());
                            GetSoDoBan();
                            funtions = 9;
                        }
                        else if (selectDishes == 1)
                        {
                            funtions = 2;
                            maNH = btnKV.Name.ToString();
                            GetListMon();
                            funtions = 9;
                        }
                    }
                    else
                    {
                        SetDefautlBackgroud(btnKV);
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK);
            }
        }

        private void btnBan_Mon_Click(object sender, EventArgs e)
        {
            try
            {
                if (funtions == 9)
                {
                    if (selectDishes == 0)
                    {
                        return;
                    }
                    foreach (SimpleButton btnBan in pnTable_Mon.Controls)
                    {
                        if (btnBan.Name == this.ActiveControl.Name)
                        {
                            if (selectDishes == 1)
                            {
                                sendOrder = 1;
                                maMon = btnBan.Name;
                                if (selectTable == 1)
                                {
                                    selectTable = 0;
                                    dtLuoi.Rows.Clear();
                                    gr_SelectProduct.RefreshDataSource();
                                }
                                GetDishGoToBuy(maMon, 0);
                            }
                            else
                            {
                                if (selectDishes == 2)
                                {
                                    maBan = btnBan.Name;
                                    if (CheckOpenTable(maBan) == false)
                                    {
                                        if (CheckHaveISBANDAT(int.Parse(maBan)) == true)// research find table check
                                        {
                                            if (SelectTablesCheckt(maBan) == true)// kiểm tra time of this table have ok or no 
                                            {
                                                SelectCheckTable(maBan); // kiểm tra bàn này đã chọn đặt chưa ?
                                                SetBackgroudOnClick(btnBan);
                                            }
                                            else
                                            {
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            SelectCheckTable(maBan);
                                            SetBackgroudOnClick(btnBan);
                                        }
                                    }
                                    else
                                    {
                                        if (XacNhanBanDat(maBan) == true)
                                        {
                                            SelectCheckTable(maBan);
                                            SetBackgroudOnClick(btnBan);
                                        }
                                        else
                                        {
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    us_keyboard2.SetTextbox(txtDBTenKH);
                    txtTenKhacHang.Focus();
                    return;
                }

                btnComboOption.Visible = false;
                pnBtnDeleteGv.Visible = false;
                foreach (SimpleButton btnBan_Mon in pnTable_Mon.Controls)
                {
                    if (btnBan_Mon.Name == this.ActiveControl.Name)
                    {
                        SetBackgroudOnClick(btnBan_Mon);
                        if (funtions == 1)
                        {
                            SetBackgroudOnClick(btn_ThuNgan);
                            SetDefautlBackgroud(btn_NghiepVu);
                            SetDefautlBackgroud(btn_DatThu);
                            pnThuNgan.BringToFront();
                            sendOrder = 0;
                            maBan = btnBan_Mon.Name;
                            if (btnBan_Mon.Text.IndexOf("\n") > 0)
                            {
                                tenBan = btnBan_Mon.Text.Substring(0, btnBan_Mon.Text.IndexOf("\n"));
                                lbBan.Text = btnBan_Mon.Text.Substring(0, btnBan_Mon.Text.IndexOf("\n"));
                            }
                            else
                            {
                                tenBan = btnBan_Mon.Text;
                                lbBan.Text = btnBan_Mon.Text;
                            }
                            lbMon.Text = "";
                            lbTotal.Text = "0";
                            if (CheckOpenTable(maBan) == true)
                            {
                                selectTable = 1;
                                lbTotal.Text = "0";
                                string maHOADON = GetMaHoaDon(maBan);
                                GetDishOfTable(maHOADON);
                                if (((DataTable)gr_SelectProduct.DataSource).Rows.Count > 0)
                                {
                                    pnBtnDeleteGv.Visible = false;
                                    dtLuoi_First = ((DataTable)gr_SelectProduct.DataSource).Copy();
                                }
                            }
                            else
                            {
                                dtLuoi.Rows.Clear();
                                gr_SelectProduct.DataSource = dtLuoi;
                            }
                        }
                        else if (funtions == 2)
                        {
                            sendOrder = 1;
                            if (selectTable == 1)
                            {
                                selectTable = 0;
                                dtLuoi.Rows.Clear();
                                gr_SelectProduct.DataSource = dtLuoi;
                            }
                            maMon = btnBan_Mon.Name;
                            lbMon.Text = " >   " + btnBan_Mon.Tag;
                            if (KiemTraTrangThaiKhoa() == true)
                            {
                                XtraMessageBox.Show("Món này đang tạm khóa", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                            GetDishGoToBuy(maMon, maKV);
                        }
                        else if (funtions == 5)
                        {
                            maBanGhep = btnBan_Mon.Name;
                            if (maBanGhep == maBan)
                            {
                                return;
                            }
                            string idHD = GetMaHoaDon(maBan);
                            string idHD_BanGhep = GetMaHoaDon(maBanGhep);
                            if (idHD == idHD_BanGhep)
                            {
                                return;
                            }
                            if (idHD_BanGhep == "")
                            {
                                string sqlBAN = "Update BAN SET ISMO_BAN=1 where MA_BAN =" + int.Parse(maBanGhep);
                                clsMain.ExecuteSQL(sqlBAN);
                                string sqlCTMOBAN = "Insert into CT_MOBAN(MA_HOADON, MA_BAN)";
                                sqlCTMOBAN += " values(" + "'" + idHD + "'," + int.Parse(maBanGhep) + ")";
                                clsMain.ExecuteSQL(sqlCTMOBAN);

                                if (cls_ConfigCashier.sInbep_Chuyenban == "1")
                                {
                                    string bangoc = "";
                                    string banchuyen = "";
                                    DataTable dtTenban = new DataTable();
                                    string sSQL = "";
                                    sSQL += "Select TEN_BAN" + "\n";
                                    sSQL += "From BAN" + "\n";
                                    sSQL += "Where MA_BAN=" + clsMain.SQLString(maBan);
                                    dtTenban = clsMain.ReturnDataTable(sSQL);
                                    bangoc = dtTenban.Rows[0]["TEN_BAN"].ToString();

                                    sSQL = "";
                                    sSQL += "Select TEN_BAN" + "\n";
                                    sSQL += "From BAN" + "\n";
                                    sSQL += "Where MA_BAN=" + clsMain.SQLString(maBanGhep);
                                    dtTenban = clsMain.ReturnDataTable(sSQL);
                                    banchuyen = dtTenban.Rows[0]["TEN_BAN"].ToString();

                                    sSQL = "";
                                    sSQL += "Select MAYINBEP From DM_BEP Where MA_CUAHANG=" + clsMain.SQLString(cls_ConfigCashier.idShop);
                                    DataTable dtbep = clsMain.ReturnDataTable(sSQL);
                                    string print = "";
                                    for (int i = 0; i < dtbep.Rows.Count; i++)
                                    {
                                        print = dtbep.Rows[i]["MAYINBEP"].ToString();
                                        DataTable dt = new DataTable();
                                        dt.Columns.Add("TITLE");
                                        dt.Columns.Add("BANGOC");
                                        dt.Columns.Add("BANCHUYEN");
                                        dt.Columns.Add("GIUA");
                                        dt.Rows.Add("Ghép Bàn", bangoc, banchuyen, "Ghép với");
                                        Frm_Report1 frm = new Frm_Report1();
                                        frm.ReportName = "rpt_Chuyenban";
                                        frm.DataSource = dt;
                                        frm.WindowState = FormWindowState.Minimized;
                                        frm.PrinterName = print;
                                        frm.IsPrint = true;
                                        frm.ShowDialog();
                                        frm.Dispose();
                                    }
                                }
                            }
                            else
                            {
                                string sqlSelect = "";
                                sqlSelect += "Select cthd.MA_HANGHOA, hh.TEN_HANGHOA, cthd.SOLUONG, cthd.GIABAN,Isnull(cthd.Is_Combo,0) as Is_Combo,cthd.CHIETKHAU" + "\n";
                                sqlSelect += "From CT_HOADON as cthd inner join HANGHOA as hh on cthd.MA_HANGHOA=hh.MA_HANGHOA where cthd.MA_HOADON ='" + idHD_BanGhep + "'";
                                DataTable disheJoinTable = clsMain.ReturnDataTable(sqlSelect);
                                if (disheJoinTable.Rows.Count > 0)
                                {
                                    for (int i = 0; i < disheJoinTable.Rows.Count; i++)
                                    {
                                        if (!ChecBishInTable(idHD, disheJoinTable.Rows[i]["MA_HANGHOA"].ToString(), bool.Parse(disheJoinTable.Rows[i]["Is_Combo"].ToString()), disheJoinTable.Rows[i]["GIABAN"].ToString()))
                                        {
                                            string sqls = "";
                                            sqls += "Insert into CT_HOADON( MA_HOADON, MA_HANGHOA, SOLUONG, GIABAN ,GIABAN_THAT,Is_Combo,CHIETKHAU)";
                                            sqls += "Values(";
                                            sqls += clsMain.SQLString(idHD) + ",";
                                            sqls += clsMain.SQLString(disheJoinTable.Rows[i]["MA_HANGHOA"].ToString()) + ",";
                                            sqls += clsMain.SQLString(disheJoinTable.Rows[i]["SOLUONG"].ToString()) + ",";
                                            sqls += clsMain.SQLString(disheJoinTable.Rows[i]["GIABAN"].ToString()) + ",";
                                            sqls += clsMain.SQLString(disheJoinTable.Rows[i]["GIABAN"].ToString()) + ",";
                                            sqls += clsMain.SQLBit(bool.Parse(disheJoinTable.Rows[i]["Is_Combo"].ToString())) + ",";
                                            sqls += clsMain.SQLString(disheJoinTable.Rows[i]["CHIETKHAU"].ToString()) + ")";
                                            clsMain.ExecuteSQL(sqls);
                                        }
                                        else
                                        {
                                            string sqlup = "";
                                            sqlup += "UPDATE CT_HOADON" + "\n";
                                            sqlup += "SET SOLUONG +=" + disheJoinTable.Rows[i]["SOLUONG"] + "," + " \n";
                                            sqlup += "CHIETKHAU =" + clsMain.SQLString(disheJoinTable.Rows[i]["CHIETKHAU"].ToString()) + " \n";
                                            sqlup += "WHERE MA_HOADON =" + clsMain.SQLString(idHD) + " \n";
                                            sqlup += "And Is_Combo =" + clsMain.SQLBit(bool.Parse(disheJoinTable.Rows[i]["Is_Combo"].ToString())) + " \n";
                                            sqlup += "And MA_HANGHOA =" + clsMain.SQLString(disheJoinTable.Rows[i]["MA_HANGHOA"].ToString());
                                            clsMain.ExecuteSQL(sqlup);
                                        }
                                    }
                                }
                                string sqlCT_MOBAN = "update CT_MOBAN set MA_HOADON ='" + idHD + "' where MA_HOADON ='" + idHD_BanGhep + "'";
                                clsMain.ExecuteSQL(sqlCT_MOBAN);
                                string deleteCT = "Delete FROM CT_HOADON  where MA_HOADON ='" + idHD_BanGhep + "'";
                                clsMain.ExecuteSQL(deleteCT);
                                string deleteBill = "Delete FROM HOADON  where MA_HOADON ='" + idHD_BanGhep + "'";
                                clsMain.ExecuteSQL(deleteBill);

                                if (cls_ConfigCashier.sInbep_Chuyenban == "1")
                                {
                                    string bangoc = "";
                                    string banchuyen = "";
                                    DataTable dtTenban = new DataTable();
                                    string sSQL = "";
                                    sSQL += "Select TEN_BAN" + "\n";
                                    sSQL += "From BAN" + "\n";
                                    sSQL += "Where MA_BAN=" + clsMain.SQLString(maBan);
                                    dtTenban = clsMain.ReturnDataTable(sSQL);
                                    bangoc = dtTenban.Rows[0]["TEN_BAN"].ToString();

                                    sSQL = "";
                                    sSQL += "Select TEN_BAN" + "\n";
                                    sSQL += "From BAN" + "\n";
                                    sSQL += "Where MA_BAN=" + clsMain.SQLString(maBanGhep);
                                    dtTenban = clsMain.ReturnDataTable(sSQL);
                                    banchuyen = dtTenban.Rows[0]["TEN_BAN"].ToString();

                                    sSQL = "";
                                    sSQL += "Select MAYINBEP From DM_BEP Where MA_CUAHANG=" + clsMain.SQLString(cls_ConfigCashier.idShop);
                                    DataTable dtbep = clsMain.ReturnDataTable(sSQL);
                                    string print = "";
                                    for (int i = 0; i < dtbep.Rows.Count; i++)
                                    {
                                        print = dtbep.Rows[i]["MAYINBEP"].ToString();
                                        DataTable dt = new DataTable();
                                        dt.Columns.Add("TITLE");
                                        dt.Columns.Add("BANGOC");
                                        dt.Columns.Add("BANCHUYEN");
                                        dt.Columns.Add("GIUA");
                                        dt.Rows.Add("Ghép Bàn", bangoc, banchuyen, "Ghép với");
                                        Frm_Report1 frm = new Frm_Report1();
                                        frm.ReportName = "rpt_Chuyenban";
                                        frm.DataSource = dt;
                                        frm.WindowState = FormWindowState.Minimized;
                                        frm.PrinterName = print;
                                        frm.IsPrint = true;
                                        frm.ShowDialog();
                                        frm.Dispose();
                                    }
                                }
                            }
                            ExitDivideTable();
                        }
                        else if (funtions == 6)
                        {
                            maBanGhep = btnBan_Mon.Name;
                            if (maBanGhep == maBan)
                            {
                                return;
                            }
                            txtmaKVBanGhep.Text = lbKhuVuc.Text;
                            txtmaBanGhep.Text = btnBan_Mon.Text;
                            if (dtLuoi.Rows.Count > 0 && maBanGhep != "")
                            {
                                List<DM_ListDisheTableOne> lisHangHoa = new List<DM_ListDisheTableOne>();
                                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                                {
                                    DM_ListDisheTableOne dmHangHoa = new DM_ListDisheTableOne();
                                    dmHangHoa._MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                                    dmHangHoa._TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                                    dmHangHoa._SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                                    dmHangHoa._GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                                    dmHangHoa._Thanh_Tien = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"]).ToString());
                                    dmHangHoa._GHICHU = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"]).ToString();
                                    dmHangHoa._IS_COMBO = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["IS_COMBO"]).ToString());
                                    lisHangHoa.Add(dmHangHoa);
                                }
                                convert cvrt = new convert();
                                divide_Table1 = cvrt.ConvertToDataTable(lisHangHoa);
                                gr_divide_Table_1.DataSource = divide_Table1;
                                gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.RowCount - 1;

                                pnInforTablecheck.Visible = true;
                                pnTachban.BringToFront();
                                int w = (panelControl14.Parent.Width - 110) / 2;
                                panelControl14.Width = w;
                                pn_TableShare.Width = w;
                                panelControl15.Width = w;
                                pn_TableTow.Width = w;
                            }
                        }
                        else if (funtions == 11)
                        {
                            maBanGhep = btnBan_Mon.Name;
                            if (maBanGhep == maBan)
                            {
                                return;
                            }
                            txtmaKVBanGhep.Text = lbKhuVuc.Text;
                            txtmaBanGhep.Text = btnBan_Mon.Text;
                            string sLocal_maBan = maBan;
                            string sLocal_maBanGhep = maBanGhep;

                            string sSQL = "";
                            if (CheckOpenTable(maBanGhep))
                            {
                                string maHD = GetMaHoaDon(maBan);
                                string maHD_banghep = GetMaHoaDon(maBanGhep);
                                sSQL = "";
                                sSQL += "Select MA_HANGHOA as MA,SOLUONG,GIABAN,THUE,TRANGTHAI,Isnull(Is_Combo,0) as Is_Combo,ISNULL(CHIETKHAU,0) AS CHIETKHAU" + "\n";
                                sSQL += "From CT_HOADON" + "\n";
                                sSQL += "Where MA_HOADON =" + clsMain.SQLString(maHD);
                                DataTable dt1 = clsMain.ReturnDataTable(sSQL);

                                for (int i = 0; i < dt1.Rows.Count; i++)
                                {
                                    if (!ChecBishInTable(maHD_banghep, dt1.Rows[i]["MA"].ToString(), bool.Parse(dt1.Rows[i]["Is_Combo"].ToString()), dt1.Rows[i]["GIABAN"].ToString()))
                                    {
                                        sSQL = "";
                                        sSQL += "Insert Into CT_HOADON(MA_HOADON,MA_HANGHOA,SOLUONG,GIABAN,GIABAN_THAT,THUE,TRANGTHAI,Is_Combo,CHIETKHAU)" + "\n";
                                        sSQL += "Values(";
                                        sSQL += clsMain.SQLString(maHD_banghep) + ",";
                                        sSQL += clsMain.SQLString(dt1.Rows[i]["MA"].ToString()) + ",";
                                        sSQL += clsMain.SQLString(dt1.Rows[i]["SOLUONG"].ToString()) + ",";
                                        sSQL += clsMain.SQLString(dt1.Rows[i]["GIABAN"].ToString()) + ",";
                                        sSQL += clsMain.SQLString(dt1.Rows[i]["GIABAN"].ToString()) + ",";
                                        sSQL += clsMain.SQLString(dt1.Rows[i]["THUE"].ToString()) + ",";
                                        sSQL += clsMain.SQLBit(bool.Parse(dt1.Rows[i]["TRANGTHAI"].ToString())) + ",";
                                        sSQL += clsMain.SQLBit(bool.Parse(dt1.Rows[i]["Is_Combo"].ToString())) + ",";
                                        sSQL += clsMain.SQLString(dt1.Rows[i]["CHIETKHAU"].ToString()) + ")";
                                        clsMain.ExecuteSQL(sSQL);
                                    }
                                    else
                                    {
                                        sSQL = "";
                                        sSQL += "UPDATE CT_HOADON SET" + "\n";
                                        sSQL += "SOLUONG +=" + clsMain.SQLString(dt1.Rows[i]["SOLUONG"].ToString()) + "," + "\n";
                                        sSQL += "CHIETKHAU =" + clsMain.SQLString(dt1.Rows[i]["CHIETKHAU"].ToString()) + "\n";
                                        sSQL += "WHERE MA_HOADON =" + clsMain.SQLString(maHD_banghep) + "\n";
                                        sSQL += "And Is_Combo =" + clsMain.SQLBit(bool.Parse(dt1.Rows[i]["Is_Combo"].ToString())) + "\n";
                                        sSQL += "And MA_HANGHOA =" + clsMain.SQLString(dt1.Rows[i]["MA"].ToString());
                                        clsMain.ExecuteSQL(sSQL);
                                    }

                                }
                                sSQL = "";
                                sSQL += " Delete CT_HOADON where MA_HOADON='" + maHD + "'";
                                clsMain.ExecuteSQL(sSQL);
                                sSQL = "";
                                sSQL += " Delete CT_MOBAN where MA_HOADON='" + maHD + "'";
                                clsMain.ExecuteSQL(sSQL);
                                sSQL = "";
                                sSQL += " Delete HOADON where MA_HOADON ='" + maHD + "'";
                                clsMain.ExecuteSQL(sSQL);
                                sSQL = "";
                                sSQL += "update BAN SET ISMO_BAN=0 where MA_BAN =" + int.Parse(maBan) + "";
                                clsMain.ExecuteSQL(sSQL);
                                sSQL = "";
                                sSQL += "Update  DISPLAY_ORDER Set MA_BAN=" + clsMain.SQLString(maBanGhep) + " ,MA_HOADON=" + clsMain.SQLString(maHD_banghep) + " Where MA_HOADON =" + clsMain.SQLString(maHD) + " And MA_BAN=" + clsMain.SQLString(maBan) + "\n";
                                clsMain.ExecuteSQL(sSQL);
                            }
                            else
                            {
                                string maHD = GetMaHoaDon(maBan);
                                sSQL = "";
                                sSQL += "Update BAN SET ISMO_BAN=1 where MA_BAN =" + int.Parse(maBanGhep) + "";
                                clsMain.ExecuteSQL(sSQL);
                                sSQL = "";
                                sSQL += " Update CT_MOBAN SET MA_BAN =" + int.Parse(maBanGhep) + " where MA_HOADON='" + maHD + "'";
                                clsMain.ExecuteSQL(sSQL);
                                sSQL = "";
                                sSQL += "Update BAN SET ISMO_BAN=0 where MA_BAN =" + int.Parse(maBan) + "";
                                clsMain.ExecuteSQL(sSQL);
                                sSQL = "";
                                sSQL += "Update  DISPLAY_ORDER Set MA_BAN=" + clsMain.SQLString(maBanGhep) + " Where MA_HOADON =" + clsMain.SQLString(maHD) + " And MA_BAN=" + clsMain.SQLString(maBan) + "\n";
                                clsMain.ExecuteSQL(sSQL);
                            }
                            ExitDivideTable();
                            if (cls_ConfigCashier.sInbep_Chuyenban == "1")
                            {
                                string bangoc = "";
                                string banchuyen = "";
                                DataTable dtTenban = new DataTable();

                                sSQL = "";
                                sSQL += "Select TEN_BAN" + "\n";
                                sSQL += "From BAN" + "\n";
                                sSQL += "Where MA_BAN=" + clsMain.SQLString(sLocal_maBan);
                                dtTenban = clsMain.ReturnDataTable(sSQL);
                                bangoc = dtTenban.Rows[0]["TEN_BAN"].ToString();

                                sSQL = "";
                                sSQL += "Select TEN_BAN" + "\n";
                                sSQL += "From BAN" + "\n";
                                sSQL += "Where MA_BAN=" + clsMain.SQLString(sLocal_maBanGhep);
                                dtTenban = clsMain.ReturnDataTable(sSQL);
                                banchuyen = dtTenban.Rows[0]["TEN_BAN"].ToString();

                                sSQL = "";
                                sSQL += "Select MAYINBEP From DM_BEP Where MA_CUAHANG=" + clsMain.SQLString(cls_ConfigCashier.idShop);
                                DataTable dtbep = clsMain.ReturnDataTable(sSQL);
                                string print = "";
                                for (int i = 0; i < dtbep.Rows.Count; i++)
                                {
                                    print = dtbep.Rows[i]["MAYINBEP"].ToString();
                                    DataTable dt = new DataTable();
                                    dt.Columns.Add("TITLE");
                                    dt.Columns.Add("BANGOC");
                                    dt.Columns.Add("BANCHUYEN");
                                    dt.Columns.Add("GIUA");
                                    dt.Rows.Add("Chuyển Bàn", bangoc, banchuyen, "chuyển qua");
                                    Frm_Report1 frm = new Frm_Report1();
                                    frm.ReportName = "rpt_Chuyenban";
                                    frm.DataSource = dt;
                                    frm.WindowState = FormWindowState.Minimized;
                                    frm.PrinterName = print;
                                    frm.IsPrint = true;
                                    frm.ShowDialog();
                                    frm.Dispose();
                                }
                            }
                        }
                    }
                    else
                    {
                        SetDefautlBackgroud(btnBan_Mon);

                        if (funtions == 1)
                        {
                            for (int i = 0; i < dtBan.Rows.Count; i++)
                            {
                                if (bool.Parse(dtBan.Rows[i]["ISBANDAT"].ToString()) == true && btnBan_Mon.Name == dtBan.Rows[i]["MA_BAN"].ToString())
                                {
                                    SetBackgroudOnClick2(btnBan_Mon);
                                }
                                if (bool.Parse(dtBan.Rows[i]["ISMO_BAN"].ToString()) == true && btnBan_Mon.Name == dtBan.Rows[i]["MA_BAN"].ToString())
                                {
                                    SetBackgroudOnClick1(btnBan_Mon);
                                    if (btnBan_Mon.Tag == "ngoiquagio")
                                    {
                                        SetBackgroudOnClick4(btnBan_Mon);
                                    }
                                }
                                if (bool.Parse(dtBan.Rows[i]["SEE_PAYMENTS"].ToString()) == true && btnBan_Mon.Name == dtBan.Rows[i]["MA_BAN"].ToString())
                                {
                                    SetBackgroudOnClick3(btnBan_Mon);
                                }
                            }
                        }
                    }
                }


                if (funtions == 1)
                {
                    string mHD = GetMaHoaDon(maBan);
                    if (mHD != null && mHD != "")
                    {
                        DataTable dtcheck = clsMain.ReturnDataTable("select MA_BAN from CT_MOBAN where MA_HOADON='" + mHD + "'");
                        if (dtcheck.Rows.Count > 1)
                        {
                            for (int i = 0; i < dtcheck.Rows.Count; i++)
                            {
                                foreach (SimpleButton btnTable in pnTable_Mon.Controls)
                                {
                                    if (btnTable.Name == dtcheck.Rows[i]["MA_BAN"].ToString())
                                    {
                                        SetBackgroudOnClick(btnTable);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK);
            }
        }

        private bool CheckOpenTable(string idTable)
        {
            string sql = "select ISMO_BAN from BAN where MA_BAN =" + int.Parse(idTable) + "";
            DataTable checkTable = clsMain.ReturnDataTable(sql);
            if (checkTable.Rows.Count > 0 && bool.Parse(checkTable.Rows[0]["ISMO_BAN"].ToString()) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void GetDishOfTable(string maHoaDon)
        {
            dtLuoi.Rows.Clear();
            string sSQL = "";
            sSQL += "Select cthd.MA_HANGHOA, hh.TEN_HANGHOA,cthd.SOLUONG,cthd.GIABAN,'a' as GHICHU,hh.MA_BEP,ISNULL(cthd.THUE,0) as THUE,";
            sSQL += "0 as Thanh_Tien,ISNULL(TRANGTHAI,0) AS TRANGTHAI,ISNULL(TRANGTHAI,0) as IS_COMBO,hh.IS_INBEP,'' as IS_KHUYENMAI, ISNULL(cthd.CHIETKHAU,0) AS CHIETKHAU" + "\n";
            sSQL += "From CT_HOADON as cthd inner join HANGHOA as hh on cthd.MA_HANGHOA=hh.MA_HANGHOA " + "\n";
            sSQL += "Where cthd.MA_HOADON ='" + maHoaDon + "'";
            dtLuoi = clsMain.ReturnDataTable(sSQL);
            gr_SelectProduct.DataSource = dtLuoi;
            if (gv_SelectProduct.RowCount > 0)
                TotalMoney();
            else
                lbTotal.Text = "0";
            gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.RowCount - 1;
        }

        private void GetDishGoToBuy(string maHangHoa, int idKV)
        {
            try
            {
                string sql = string.Format("Exec SelectByMON_Buffet @MAHANG='{0}',@MAVACH='{1}',@maKV={2},@maCH={3},@maBuffet='{4}'", maHangHoa, maHangHoa, idKV, cls_ConfigCashier.idShop, maBuffet);
                DataTable dtHH = clsMain.ReturnDataTable(sql);
                if (dtHH.Rows.Count < 1)
                {
                    return;
                }
                //sửa định lượng
                if (bool.Parse(dtHH.Rows[0]["SUADINHLUONG"].ToString()))
                {
                    bool flag_Exist = false; // flag_Exist=false: mặt hàng này chưa bán và ngược lại
                    if (gv_SelectProduct.RowCount > 0)
                    {
                        for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                        {
                            if (gv_SelectProduct.GetRowCellValue(i, "MA_HANGHOA").ToString() == maHangHoa)
                            {
                                flag_Exist = true;
                                break;
                            }
                        }
                    }
                    if (!flag_Exist)
                    {
                        sql = "Select MANGUYENLIEU" + "\n";
                        sql += "From DINHLUONG" + "\n";
                        sql += "Where MATHANHPHAM=" + clsMain.SQLString(dtHH.Rows[0]["MA_HANGHOA"].ToString());
                        DataTable dtDL = clsMain.ReturnDataTable(sql);
                        if (dtDL.Rows.Count > 0)
                        {
                            Frm_Suadinhluong frm = new Frm_Suadinhluong(dtHH.Rows[0]["MA_HANGHOA"].ToString());
                            frm.ShowDialog();
                            sSQL_DL += frm.tmp_sSQL;
                            sSQL_DL += "\n";
                            sSQL_Yeucauthem += frm.tmp_sSQL_Yeucauthem;
                            sSQL_Yeucauthem += "\n";
                            frm.Dispose();
                        }
                        else
                        {
                            XtraMessageBox.Show(_chuakhaibaodinhluong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                }
                //sửa giá
                decimal Giaban_Cur = 0;  //giá bán thực của hàng hóa.
                decimal SoLuong_Cur = 0;
                if (bool.Parse(dtHH.Rows[0]["SUAGIA"].ToString()))
                {
                    bool flag_Exist = false; // flag_Exist=false: mặt hàng này chưa bán và ngược lại
                    if (gv_SelectProduct.RowCount > 0)
                    {
                        for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                        {
                            if (gv_SelectProduct.GetRowCellValue(i, "MA_HANGHOA").ToString() == maHangHoa)
                            {
                                flag_Exist = true;
                                break;
                            }
                        }
                    }
                    if (!flag_Exist)
                    {
                        Frm_Chontheothoigia frm = new Frm_Chontheothoigia();
                        frm.ShowDialog();
                        Giaban_Cur = frm.tmp_Thoigia;
                        SoLuong_Cur = frm.tmp_SoLuong;
                        frm.Dispose();
                    }
                }
                else
                {
                    SoLuong_Cur = 1;
                    Giaban_Cur = decimal.Parse(dtHH.Rows[0]["GIABAN"].ToString());
                }
                //lấy dữ liệu hàng hóa mới chọn
                DM_HangHoa hanghoa = new DM_HangHoa();
                hanghoa.MA_HANGHOA = dtHH.Rows[0]["MA_HANGHOA"].ToString();
                hanghoa.TEN_HANGHOA = dtHH.Rows[0]["TEN_HANGHOA"].ToString();
                hanghoa.SOLUONG = SoLuong_Cur;
                hanghoa.MA_BEP = dtHH.Rows[0]["MA_BEP"].ToString();
                hanghoa.GIABAN = Giaban_Cur;
                hanghoa.GHICHU = dtHH.Rows[0]["GHICHU"].ToString();
                hanghoa.THUE = double.Parse(dtHH.Rows[0]["THUE"].ToString());
                hanghoa.TRANGTHAI = false;
                //kiem tra co tren luoi chua
                bool testID = false;  // bien ktra trùng tên khi addrow hàng hóa
                int iFocusRow = 0;
                if (dtLuoi.Rows.Count < 1)
                {
                    testID = false;
                }
                else
                {
                    for (int i = 0; i < dtLuoi.Rows.Count; i++)
                    {
                        if (dtLuoi.Rows[i]["MA_HANGHOA"].ToString() == hanghoa.MA_HANGHOA && "cb" != dtLuoi.Rows[i]["GHICHU"].ToString() && !bool.Parse(dtLuoi.Rows[i]["IS_KHUYENMAI"].ToString()))
                        {
                            var dtCombo = GetProductInCombo(hanghoa.MA_HANGHOA);
                            if (dtCombo.Rows.Count > 0)
                            {
                                DataRow dr = dtLuoi.NewRow();
                                dr[0] = hanghoa.MA_HANGHOA;
                                dr[1] = hanghoa.TEN_HANGHOA;
                                dr[2] = hanghoa.SOLUONG;
                                dr[3] = hanghoa.GIABAN;
                                dr[4] = hanghoa.GHICHU;
                                dr[5] = hanghoa.MA_BEP;
                                dr[6] = hanghoa.THUE;
                                dr["CHIETKHAU"] = 0;
                                dr["TRANGTHAI"] = false;//hàng hóa bình thường,được xóa hay thêm giảm
                                dr["IS_COMBO"] = false;//hàng hóa bình thường
                                dr["IS_INBEP"] = bool.Parse(dtHH.Rows[0]["IS_INBEP"].ToString());
                                dr["IS_KHUYENMAI"] = false;
                                dtLuoi.Rows.Add(dr);
                                for (int j = 0; j < dtCombo.Rows.Count; j++)
                                {
                                    DataRow drr = dtLuoi.NewRow();
                                    drr[0] = int.Parse(dtCombo.Rows[j]["MA_HANGHOA"].ToString());
                                    drr[1] = dtCombo.Rows[j]["TEN_HANGHOA"].ToString();
                                    drr[2] = dtCombo.Rows[j]["SOLUONG"].ToString();
                                    drr[3] = 0;
                                    drr[4] = "cb";
                                    drr[5] = dtCombo.Rows[j]["MA_BEP"].ToString();
                                    drr[6] = 0;
                                    drr["CHIETKHAU"] = 0;
                                    drr["TRANGTHAI"] = true;//hàng hóa thuộc Combo,không được xóa hay thêm giảm
                                    drr["IS_COMBO"] = true;//hàng hóa thuộc Combo
                                    drr["IS_INBEP"] = bool.Parse(dtHH.Rows[0]["IS_INBEP"].ToString());
                                    drr["IS_KHUYENMAI"] = false;
                                    dtLuoi.Rows.Add(drr);
                                }
                                iFocusRow = i;
                                testID = true;
                                break;
                            }
                            else
                            {
                                iFocusRow = i;
                                dtLuoi.Rows[i]["SOLUONG"] = double.Parse(dtLuoi.Rows[i]["SOLUONG"].ToString()) + 1;
                                testID = true;
                                break;
                            }
                        }
                        else
                        {
                            testID = false;
                        }
                    }
                }
                //Hàng hóa chưa được order trong bàn nên thêm dòng mới
                if (testID == false)//Hàng hóa chưa được order trong bàn nên thêm dòng mới
                {
                    DataRow dr = dtLuoi.NewRow();
                    dr[0] = hanghoa.MA_HANGHOA;
                    dr[1] = hanghoa.TEN_HANGHOA;
                    dr[2] = hanghoa.SOLUONG;
                    dr[3] = hanghoa.GIABAN;
                    dr[4] = hanghoa.GHICHU;
                    dr[5] = hanghoa.MA_BEP;
                    dr[6] = hanghoa.THUE;
                    dr["CHIETKHAU"] = 0;
                    dr["TRANGTHAI"] = false;//hàng hóa bình thường,được xóa hay thêm giảm
                    dr["IS_COMBO"] = false;//hàng hóa bình thường
                    dr["IS_INBEP"] = bool.Parse(dtHH.Rows[0]["IS_INBEP"].ToString());
                    dr["IS_KHUYENMAI"] = false;
                    dtLuoi.Rows.Add(dr);
                    // Combo 
                    var dtCombo = GetProductInCombo(hanghoa.MA_HANGHOA);
                    int iNumberCombo = 0;
                    iNumberCombo = dtCombo.Rows.Count;
                    if (dtCombo.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtCombo.Rows.Count; i++)
                        {
                            DataRow drr = dtLuoi.NewRow();
                            drr[0] = int.Parse(dtCombo.Rows[i]["MA_HANGHOA"].ToString());
                            drr[1] = dtCombo.Rows[i]["TEN_HANGHOA"].ToString();
                            drr[2] = dtCombo.Rows[i]["SOLUONG"].ToString();
                            drr[3] = 0;
                            drr[4] = "cb";
                            drr[5] = dtCombo.Rows[i]["MA_BEP"].ToString();
                            drr[6] = 0;
                            drr["CHIETKHAU"] = 0;
                            drr["TRANGTHAI"] = true;//hàng hóa thuộc Combo,không được xóa hay thêm giảm
                            drr["IS_COMBO"] = true;//hàng hóa thuộc Combo
                            drr["IS_INBEP"] = bool.Parse(dtHH.Rows[0]["IS_INBEP"].ToString());
                            drr["IS_KHUYENMAI"] = false;
                            dtLuoi.Rows.Add(drr);
                        }
                    }
                    iFocusRow = dtLuoi.Rows.Count - 1 - iNumberCombo;
                }
                gr_SelectProduct.DataSource = dtLuoi;
                if (gv_SelectProduct.RowCount > 0)
                {
                    TotalMoney();
                }
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = iFocusRow;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK);
            }
        }

        private DataTable GetProductInCombo(string maHangHoa)
        {
            string sSQL = "";
            sSQL += "Select cb.MA_COMBO, hh.MA_HANGHOA, hh.TEN_HANGHOA, cb.SOLUONG,HH.MA_BEP" + "\n";
            sSQL += "From COMBO cb inner join HANGHOA hh on cb.MA_HANGHOA = hh.MA_HANGHOA" + "\n";
            sSQL += "Where MA_COMBO = " + clsMain.SQLString(maHangHoa) + "";
            var dtCombo = clsMain.ReturnDataTable(sSQL);
            return dtCombo;
        }

        DataTable checkVAT = new DataTable();
        private void TotalMoney()
        {
            if (checkVAT.Rows.Count <= 0)
            {
                string sSQL = "Select GIATRI From CAUHINH Where TEN='GIABANTRUOCTHUE' ";
                checkVAT = clsMain.ReturnDataTable(sSQL);
            }
            bool kt = false;
            if (checkVAT.Rows[0]["GIATRI"].ToString() == "1")
                kt = true;
            double total = 0;
            try
            {
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    double thanhTien = 0, vat = 0;
                    double sl = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                    double gia = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                    double CK = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["CHIETKHAU"]).ToString());
                    thanhTien = (sl * gia) - (sl * gia) * CK / 100;
                    if (kt == true)
                    {
                        vat = (double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["THUE"]).ToString()) / 100) * thanhTien;
                    }
                    total += thanhTien + vat;
                    gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"], thanhTien + vat);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            lbTotal.Text = string.Format("{0:#,###0}", total);
        }

        private void txtID_Click(object sender, EventArgs e)
        {
            txtID.SelectAll();
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void txtID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(txtID.Text))
                {
                    txtID.Focus();
                    return;
                }
                if (funtions == 2)
                {
                    if (txtID.SelectedText != "")
                    {
                        string smahanghoa = txtID.EditValue.ToString();
                        string stenhanghoa = txtID.Text.ToString();
                        sendOrder = 1;
                        lbMon.Text = " >   " + stenhanghoa;
                        GetDishGoToBuy(smahanghoa, maKV);
                        txtID.Text = "";
                        txtID.Focus();
                    }
                    else
                    {
                        string sSQL = "";
                        sSQL += "Exec SP_Select_Hanghoa_Mavach " + clsMain.SQLString(txtID.Text);
                        DataTable dtMon = clsMain.ReturnDataTable(sSQL);
                        if (dtMon.Rows.Count > 0)
                        {
                            sendOrder = 1;
                            lbMon.Text = " >   " + dtMon.Rows[0]["TEN_HANGHOA"].ToString();
                            GetDishGoToBuy(dtMon.Rows[0]["MA_HANGHOA"].ToString(), maKV);
                            txtID.Text = "";
                            txtID.Focus();
                        }
                        else
                        {
                            txtID.Text = "";
                            txtID.Focus();
                        }
                    }
                }
            }
        }

        private void btn_DowLast_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.RowCount - 1;
                btn_UpFirst.Enabled = true;
                btn_up.Enabled = true;
                btn_Dow.Enabled = false;
                btn_DowLast.Enabled = false;
                lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
            }
            catch
            {
            }
        }

        private void btn_Dow_Click(object sender, EventArgs e)
        {
            try
            {
                if (gv_SelectProduct.FocusedRowHandle < gv_SelectProduct.RowCount)
                {
                    gv_SelectProduct.Focus();
                    gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.FocusedRowHandle + 1;
                    btn_up.Enabled = true;
                    btn_UpFirst.Enabled = true;
                    lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
                    if (gv_SelectProduct.FocusedRowHandle == gv_SelectProduct.RowCount - 1)
                    {
                        btn_Dow.Enabled = false;
                        btn_DowLast.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_up_Click(object sender, EventArgs e)
        {
            try
            {
                if (gv_SelectProduct.FocusedRowHandle > 0)
                {
                    gv_SelectProduct.Focus();
                    gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.FocusedRowHandle - 1;
                    btn_Dow.Enabled = true;
                    btn_DowLast.Enabled = true;
                    lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
                    if (gv_SelectProduct.FocusedRowHandle == 0)
                    {
                        btn_up.Enabled = false;
                        btn_UpFirst.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_UpFirst_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = 0;
                btn_UpFirst.Enabled = false;
                btn_up.Enabled = false;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
                lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
            }
            catch
            {
            }
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            try
            {
                if (!bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
                {
                    if (!Flag_soluong)
                    {
                        if (lbSoluong.Text.Length < 3)
                            lbSoluong.Text += btn0.Text;
                    }
                    int soluong = int.Parse(lbSoluong.Text);
                    gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"], soluong);
                    TotalMoney();
                }
            }
            catch
            {
            }
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            try
            {
                SimpleButton btn = (SimpleButton)sender;
                if (!bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
                {
                    if (Flag_soluong)
                    {
                        lbSoluong.Text = btn.Text;
                        Flag_soluong = false;
                    }
                    else
                    {
                        if (lbSoluong.Text.Length < 3)
                            lbSoluong.Text += btn.Text;
                    }

                    int soluong = int.Parse(lbSoluong.Text);
                    gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"], soluong);
                    TotalMoney();
                }
            }
            catch
            {
            }
        }

        private void btn_tru_Click(object sender, EventArgs e)
        {
            try
            {
                if (!bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
                {
                    int soluong = int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                    if (soluong > 1)
                    {
                        Flag_soluong = true;
                        gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"], soluong - 1);
                        lbSoluong.Text = (soluong - 1).ToString();
                    }
                    TotalMoney();
                }
            }
            catch
            {

            }
        }

        private void btn_cong_Click(object sender, EventArgs e)
        {
            try
            {
                if (!bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
                {
                    int soluong = int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                    if (soluong < 999)
                    {
                        Flag_soluong = true;
                        gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"], soluong + 1);
                        lbSoluong.Text = (soluong + 1).ToString();
                        TotalMoney();
                    }
                }
            }
            catch
            {
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (funtions == 1)
            {
                //Tìm món thêm mới hoặc món hủy
                DataTable dtFirst = dtLuoi_First.Copy();

                DataView myDV = (DataView)gv_SelectProduct.DataSource;
                DataTable dtLast = myDV.ToTable().Copy();

                DataTable dtAdd = dtFirst.Copy();
                DataTable dtDelete = dtFirst.Copy();
                dtAdd.Clear();
                dtDelete.Clear();

                foreach (DataRow drFirst in dtFirst.Rows)
                {
                    DataRow drTemp = dtFirst.NewRow();
                    drTemp.ItemArray = drFirst.ItemArray;

                    DataRow[] dr = dtLast.Select("MA_HANGHOA=" + clsMain.SQLString(drFirst["MA_HANGHOA"].ToString()));
                    if (dr.Length == 0)
                    {
                        dtDelete.Rows.Add(drTemp.ItemArray);
                    }
                    else
                    {
                        if (float.Parse(drFirst["SOLUONG"].ToString()) - float.Parse(dr[0]["SOLUONG"].ToString()) > 0)
                        {
                            drTemp["SOLUONG"] = float.Parse(drFirst["SOLUONG"].ToString()) - float.Parse(dr[0]["SOLUONG"].ToString());
                            dtDelete.Rows.Add(drTemp.ItemArray);
                        }
                        else if (float.Parse(drFirst["SOLUONG"].ToString()) - float.Parse(dr[0]["SOLUONG"].ToString()) < 0)
                        {
                            drTemp["SOLUONG"] = float.Parse(dr[0]["SOLUONG"].ToString()) - float.Parse(drFirst["SOLUONG"].ToString());
                            dtAdd.Rows.Add(drTemp.ItemArray);
                        }
                    }
                }
                //Món thêm mới thì thêm vào dữ liệu và in order bếp như khi mở bàn gọi món
                if (dtAdd.Rows.Count > 0)
                {
                    DataTable dt = dtAdd.Copy();
                    dt.Clear();
                    gr_SelectProduct.DataSource = dt;
                    gr_SelectProduct.DataSource = dtAdd;

                    string maHDB = GetMaHoaDon(maBan);
                    //31/08/2017 fastfood sau khi in bill thì trên lưới có bao nhiêu hàng mỗi hàng sẻ in tem qua máy in tem
                    //PrintMaVachFastFood(maHDB, maBan);

                    string nameTable = clsMain.ReturnDataTable("Select TEN_BAN From BAN Where MA_BAN = " + clsMain.SQLString(maBan) + "").Rows[0][0].ToString();
                    while (gv_SelectProduct.RowCount > 0)
                    {
                        List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                        int j = 0;
                        string maBEP = "";
                        for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                        {
                            DM_HangHoa dmHangHoa = new DM_HangHoa();
                            maBEP = gv_SelectProduct.GetRowCellValue(0, gv_SelectProduct.Columns["MA_BEP"]).ToString();

                            if (maBEP == gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_BEP"]).ToString())
                            {
                                dmHangHoa.Ma_HoaDon = maHDB;
                                dmHangHoa.Ten_HoaDon = "Order Bếp";
                                dmHangHoa.TEN_BAN = nameTable;
                                dmHangHoa.MaQuay = cls_ConfigCashier.nameCashier;
                                dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                                dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                                dmHangHoa.STT = (++j).ToString();
                                dmHangHoa.MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                                dmHangHoa.TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                                dmHangHoa.SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                                dmHangHoa.THUE = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["THUE"]).ToString());
                                dmHangHoa.GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                                dmHangHoa.TRANGTHAI = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TRANGTHAI"]).ToString());
                                dmHangHoa.IS_COMBO = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["IS_COMBO"]).ToString());
                                dmHangHoa.IS_INBEP = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["IS_INBEP"]).ToString());
                                dmHangHoa.CHIETKHAU = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["CHIETKHAU"]).ToString());
                                string repuest = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"]).ToString();
                                if (repuest == "a")
                                    dmHangHoa.GHICHU = "";
                                else
                                    dmHangHoa.GHICHU = repuest;
                                //Insert vào bảng DINHLUONG_DIEUCHINH
                                if (sSQL_DL != "")
                                {
                                    sSQL_DL = sSQL_DL.Replace("TEMP_MA_HOADON", clsMain.SQLString(maHDB));
                                    string[] arr = sSQL_DL.Split('*');
                                    for (int z = 0; z < arr.Length; z++)
                                    {
                                        string[] arr1 = arr[z].Split('=');
                                        if (arr1[0].Contains(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString()))
                                        {
                                            clsMain.ExecuteSQL(arr1[1]);
                                        }
                                    }
                                    arr = sSQL_Yeucauthem.Split('*');
                                    for (int z = 0; z < arr.Length; z++)
                                    {
                                        string[] arr1 = arr[z].Split('=');
                                        if (arr1[0].Contains(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString()))
                                        {
                                            dmHangHoa.GHICHU = arr1[1];
                                        }
                                    }
                                }
                                //nếu hàng hóa được in bếp mới cho in xuống bếp
                                if (dmHangHoa.IS_INBEP)
                                {
                                    listDM_HangHoa.Add(dmHangHoa);
                                }
                                //CT hóa đơn
                                string sSQL = "";
                                if (ChecBishInTable(maHDB, dmHangHoa.MA_HANGHOA, dmHangHoa.IS_COMBO, dmHangHoa.GIABAN.ToString()) == false)
                                {
                                    sSQL += "Insert into CT_HOADON(MA_HOADON,MA_HANGHOA,SOLUONG,GIABAN,GIABAN_THAT,THUE,TRANGTHAI,IS_COMBO,CHIETKHAU)";
                                    sSQL += "Values (";
                                    sSQL += clsMain.SQLString(maHDB) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.THUE.ToString()) + ",";
                                    sSQL += clsMain.SQLBit(dmHangHoa.TRANGTHAI) + ",";
                                    sSQL += clsMain.SQLBit(dmHangHoa.IS_COMBO) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.CHIETKHAU.ToString()) + ")";
                                    clsMain.ExecuteSQL(sSQL);
                                }
                                else
                                {
                                    sSQL += "UPDATE CT_HOADON SET " + "\n";
                                    sSQL += "SOLUONG +=" + clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + "," + "\n";
                                    sSQL += "CHIETKHAU =" + clsMain.SQLString(dmHangHoa.CHIETKHAU.ToString()) + "\n";
                                    sSQL += "WHERE MA_HOADON =" + clsMain.SQLString(maHDB) + "\n";
                                    sSQL += "AND Isnull(Is_Combo,0)=" + clsMain.SQLBit(dmHangHoa.IS_COMBO) + "\n";
                                    sSQL += "AND MA_HANGHOA =" + clsMain.SQLString(dmHangHoa.MA_HANGHOA);
                                    clsMain.ExecuteSQL(sSQL);
                                }
                                //Man hinh bep
                                if (dmHangHoa.IS_INBEP)
                                {
                                    sSQL = "";
                                    sSQL += "Insert into DISPLAY_ORDER (MA_HANGHOA,MA_HOADON, MA_BAN, GIOORDER, SOLUONG,NHANVIEN, YEUCAU,FLAG)";
                                    sSQL += "Values (";
                                    sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                                    sSQL += clsMain.SQLString(maHDB) + ",";
                                    sSQL += clsMain.SQLString(maBan) + ",";
                                    sSQL += "GETDATE()" + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                                    sSQL += clsMain.SQLString(clsGlobal.gsUserID) + ",";
                                    sSQL += clsMain.SQLStringUnicode(dmHangHoa.GHICHU) + ",";
                                    sSQL += clsMain.SQLBit(true) + ")";
                                    clsMain.ExecuteSQL(sSQL);
                                }
                                // Delete product selected 
                                if (i != 0)
                                {
                                    gv_SelectProduct.DeleteRow(i);
                                    i--;
                                }
                            }
                        }
                        // Printer Order 
                        gv_SelectProduct.DeleteRow(0);
                        convert cvrt = new convert();
                        DataTable Orderbill = cvrt.ConvertToDataTable(listDM_HangHoa);

                        if (Orderbill.Rows.Count > 0)
                        {
                            string print = GetIdKitchen(maBEP);
                            if (print == null)
                            {
                                XtraMessageBox.Show(_chuachonmayinchomathang, _thongbao, MessageBoxButtons.OK);
                                print = "";
                            }

                            //8/11/2016 căt từng món theo yêu cầu huy việt nam
                            if (cls_KP_RES.Mode == 4)
                            {
                                string sSoKhach = "1";
                                try
                                {
                                    DataTable dtKhach = clsMain.ReturnDataTable("select SOLUONGKHACH from HOADON where MA_HOADON=" + clsMain.SQLString(Orderbill.Rows[0]["Ma_HoaDon"].ToString()));
                                    sSoKhach = dtKhach.Rows[0][0].ToString();
                                }
                                catch
                                {
                                }
                                DataColumn dctemp = new DataColumn();
                                dctemp.DefaultValue = sSoKhach;
                                dctemp.ColumnName = "sokhach";
                                Orderbill.Columns.Add(dctemp);
                                for (int i = 0; i < Orderbill.Rows.Count; i++)
                                {
                                    DataTable dtTmp1 = Orderbill.Copy();
                                    dtTmp1.Clear();
                                    dtTmp1.Rows.Add(Orderbill.Rows[i].ItemArray);

                                    Frm_Report1 frmTmp1 = new Frm_Report1();
                                    frmTmp1.ReportName = "rpt_Order_1_Cat1";
                                    frmTmp1.DataSource = dtTmp1;
                                    frmTmp1.WindowState = FormWindowState.Minimized;
                                    frmTmp1.PrinterName = print;
                                    frmTmp1.IsPrint = true;
                                    frmTmp1.ShowDialog();
                                    frmTmp1.Dispose();
                                }
                            }
                            else
                            {
                                Frm_Report1 frm = new Frm_Report1();
                                if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                                {
                                    frm.ReportName = "rpt_Order_58";
                                }
                                else
                                {
                                    if (cls_KP_RES.Mode == 3 || cls_KP_RES.Mode == 4)
                                    {
                                        frm.ReportName = "rpt_Order_1";
                                    }
                                    else
                                    {
                                        frm.ReportName = "rpt_Order";
                                    }
                                }
                                frm.DataSource = Orderbill;
                                frm.WindowState = FormWindowState.Minimized;
                                frm.PrinterName = print;
                                frm.IsPrint = true;
                                frm.ShowDialog();
                                frm.Dispose();
                            }
                        }
                    }
                }
                //Món hủy thì giảm bớt vào dữ lệu và in order bếp như khi hủy món
                if (dtDelete.Rows.Count > 0)
                {
                    divide_Table2.Rows.Clear();
                    foreach (DataRow dr in dtDelete.Rows)
                    {
                        DataRow drdivide_Table2 = divide_Table2.NewRow();
                        drdivide_Table2["__MA_HANGHOA"] = dr["MA_HANGHOA"];
                        drdivide_Table2["__TEN_HANGHOA"] = dr["TEN_HANGHOA"];
                        drdivide_Table2["__SOLUONG"] = dr["SOLUONG"];
                        drdivide_Table2["__GIABAN"] = dr["GIABAN"];
                        drdivide_Table2["__MA_BEP"] = dr["MA_BEP"];
                        drdivide_Table2["__Thanh_Tien"] = dr["Thanh_Tien"];
                        drdivide_Table2["__IS_COMBO"] = dr["IS_COMBO"];

                        divide_Table2.Rows.Add(drdivide_Table2.ItemArray);
                    }
                    gr_divide_Table_2.DataSource = divide_Table2;

                    string maHoaDon = GetMaHoaDon(maBan);
                    string sSQL = "";
                    for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                    {
                        string idDisher = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                        decimal soLuong = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());

                        sSQL = "";
                        sSQL += "EXEC SP_InsertHuyMon " + clsMain.SQLString(maHoaDon) + ",";
                        sSQL += clsMain.SQLString(maBan) + ",";
                        sSQL += clsMain.SQLString(idDisher) + ",";
                        sSQL += clsMain.SQLString(soLuong.ToString()) + ",";
                        sSQL += clsMain.SQLString(clsGlobal.gsUserID);
                        clsMain.ExecuteSQL(sSQL);


                        sSQL = "";
                        sSQL += "Update CT_HOADON Set" + "\n";
                        sSQL += "SOLUONG= SOLUONG - " + soLuong + "\n";
                        sSQL += "Where MA_HOADON=" + clsMain.SQLString(maHoaDon) + "\n";
                        sSQL += "And MA_HANGHOA=" + clsMain.SQLString(idDisher) + "\n";
                        clsMain.ExecuteSQL(sSQL);
                        // Kiểm tra sau khi trừ nếu số lượng =0 thì delete hàng hóa đó khỏi CT HOADON và  DINHLUONG DIEUCHINH và  DISPLAY ORDER
                        sSQL = "";
                        sSQL += "Select MA_HOADON,MA_HANGHOA" + "\n";
                        sSQL += "From CT_HOADON" + "\n";
                        sSQL += "Where SOLUONG= 0" + "\n";
                        sSQL += "And MA_HOADON=" + clsMain.SQLString(maHoaDon) + "\n";
                        sSQL += "And MA_HANGHOA=" + clsMain.SQLString(idDisher);
                        DataTable dt = clsMain.ReturnDataTable(sSQL);
                        if (dt.Rows.Count > 0)
                        {
                            sSQL = "";
                            sSQL += "Select MA_HOADON" + "\n";
                            sSQL += "From DINHLUONG_DIEUCHINH" + "\n";
                            sSQL += "Where MA_HOADON=" + clsMain.SQLString(maHoaDon) + "\n";
                            sSQL += "And MATHANHPHAM=" + clsMain.SQLString(idDisher);
                            dt = clsMain.ReturnDataTable(sSQL);
                            sSQL = "";
                            if (dt.Rows.Count > 0)
                            {
                                sSQL += "Delete DINHLUONG_DIEUCHINH" + "\n";
                                sSQL += "Where MA_HOADON=" + clsMain.SQLString(maHoaDon) + "\n";
                                sSQL += "And MATHANHPHAM=" + clsMain.SQLString(idDisher) + "\n";
                            }
                            sSQL += "Delete from CT_HOADON" + "\n";
                            sSQL += "Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + "\n";
                            sSQL += "And MA_HANGHOA=" + clsMain.SQLString(idDisher) + "\n";

                            sSQL += "Update  DISPLAY_ORDER Set FLAG=0" + "\n";
                            sSQL += "Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + "\n";
                            sSQL += "And MA_HANGHOA=" + clsMain.SQLString(idDisher) + "\n";
                            sSQL += "And MA_BAN=" + clsMain.SQLString(maBan) + "\n";
                            clsMain.ExecuteSQL(sSQL);
                        }
                        else
                        {
                            //code cho man hinh bep
                            sSQL = "";
                            sSQL += "Select *" + "\n";
                            sSQL += "From DISPLAY_ORDER" + "\n";
                            sSQL += "Where TRANGTHAI=0 AND FLAG=1" + "\n";
                            sSQL += "And MA_HOADON=" + clsMain.SQLString(maHoaDon) + "\n";
                            sSQL += "And MA_HANGHOA=" + clsMain.SQLString(idDisher) + "\n";
                            sSQL += "Order by GIOORDER Desc" + "\n";
                            DataTable dtBep = clsMain.ReturnDataTable(sSQL);
                            foreach (DataRow dr in dtBep.Rows)
                            {
                                if (decimal.Parse(dr["SOLUONG"].ToString()) > soLuong)
                                {
                                    sSQL = "";
                                    sSQL += "Update DISPLAY_ORDER Set" + "\n";
                                    sSQL += "SOLUONG= SOLUONG - " + soLuong + "\n";
                                    sSQL += "Where STT=" + clsMain.SQLString(dr["STT"].ToString()) + "\n";
                                    clsMain.ExecuteSQL(sSQL);
                                    break;
                                }
                                else if (decimal.Parse(dr["SOLUONG"].ToString()) == soLuong)
                                {
                                    sSQL += "Update  DISPLAY_ORDER Set FLAG=0" + "\n";
                                    sSQL += "Where STT=" + clsMain.SQLString(dr["STT"].ToString()) + "\n";
                                    clsMain.ExecuteSQL(sSQL);
                                    break;
                                }
                                else
                                {
                                    sSQL += "Update  DISPLAY_ORDER Set FLAG=0" + "\n";
                                    sSQL += "Where STT=" + clsMain.SQLString(dr["STT"].ToString()) + "\n";
                                    clsMain.ExecuteSQL(sSQL);
                                    soLuong = soLuong - decimal.Parse(dr["SOLUONG"].ToString());
                                }
                            }
                        }
                    }
                    // Printer Order 
                    string sTen_ban = "";
                    sSQL = "";
                    sSQL += "Select TEN_BAN From BAN Where MA_BAN=" + clsMain.SQLString(maBan) + "\n";
                    DataTable dtb = clsMain.ReturnDataTable(sSQL);
                    if (dtb.Rows.Count > 0)
                    {
                        sTen_ban = dtb.Rows[0]["TEN_BAN"].ToString();
                    }
                    while (gv_divide_Table_2.RowCount > 0)
                    {
                        List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                        int j = 0;
                        string maBEP = "";
                        for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                        {
                            DM_HangHoa dmHangHoa = new DM_HangHoa();
                            maBEP = gv_divide_Table_2.GetRowCellValue(0, gv_divide_Table_2.Columns["__MA_BEP"]).ToString();
                            if (maBEP == gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_BEP"]).ToString())
                            {
                                dmHangHoa.Ma_HoaDon = maHoaDon;
                                dmHangHoa.Ten_HoaDon = "Hủy Order Bếp";
                                dmHangHoa.MaQuay = cls_ConfigCashier.nameCashier;
                                dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                                dmHangHoa.Ngay = string.Format("{0:dd/MM/yyyy hh:mm:ss}", DateTime.Now);
                                dmHangHoa.TEN_BAN = sTen_ban;
                                dmHangHoa.STT = (++j).ToString();
                                dmHangHoa.MA_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                                dmHangHoa.TEN_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__TEN_HANGHOA"]).ToString();
                                dmHangHoa.SOLUONG = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                                // dmHangHoa.IS_INBEP = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["IS_INBEP"]).ToString());
                                dmHangHoa.IS_INBEP = bool.Parse(dtFirst.Rows[i]["IS_INBEP"].ToString());
                                dmHangHoa.GHICHU = "Hủy Món";

                                if (dmHangHoa.IS_INBEP)
                                    listDM_HangHoa.Add(dmHangHoa);

                                if (i != 0)
                                {
                                    gv_divide_Table_2.DeleteRow(i);
                                    i--;
                                }
                            }
                        }
                        gv_divide_Table_2.DeleteRow(0);
                        string print = GetIdKitchen(maBEP);
                        if (print == null)
                            XtraMessageBox.Show(_chuachonmayinchomhnay, _thongbao, MessageBoxButtons.OK);
                        convert cvrt = new convert();
                        DataTable Orderbill = cvrt.ConvertToDataTable(listDM_HangHoa);
                        if (Orderbill.Rows.Count > 0)
                        {
                            Frm_Report1 frm = new Frm_Report1();
                            if (cls_KP_RES.Mode == 3)
                                frm.ReportName = "rpt_Order_1";
                            else
                                frm.ReportName = "rpt_Order";
                            frm.DataSource = Orderbill;
                            frm.WindowState = FormWindowState.Minimized;
                            frm.PrinterName = print;
                            frm.IsPrint = true;
                            frm.ShowDialog();
                            frm.Dispose();
                        }
                    }
                }

                funtions = 1;
                sendOrder = 0;
                exitReprint = false;
                maBan = "";
                maBanGhep = "";
                txtmaBanGhep.Text = "";
                txtmaKVBanGhep.Text = "";
                sSQL_DL = "";
                sSQL_Yeucauthem = "";
                dtLuoi.Rows.Clear();
                divide_Table1.Rows.Clear();
                divide_Table2.Rows.Clear();
                gr_SelectProduct.DataSource = dtLuoi;
                maMon = "";
                lbMon.Text = "";
                lbBan.Text = "";
                lbTotal.Text = "0";
                btn_SoDoBan.Text = _mb;
                txtID.Visible = false;
                pnShowHang.BringToFront();
                SetDefautlBackgroud(btn_SoDoBan);
                SetDefaultBackgroundBtnInPn(pnThuNgan);
                SetDefaultBackgroundBtnInPn(pnNghiepVu);
                GetKhuVuc();
                GetSoDoBan();
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    btn.Visible = true;
                }
            }
            pnBtnDeleteGv.Visible = false;
            btnComboOption.Visible = false;
        }

        private void btn_xoa_Click(object sender, EventArgs e)
        {
            if (!bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
            {
                DataTable dtCombo = GetProductInCombo(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                if (dtCombo.Rows.Count > 0)
                {
                    int n = dtCombo.Rows.Count;
                    for (int i = 0; i < n; i++)
                    {
                        gv_SelectProduct.DeleteRow(gv_SelectProduct.FocusedRowHandle + 1);
                    }
                }
                gv_SelectProduct.DeleteSelectedRows();
                btnComboOption.Visible = false;
                TotalMoney();
                if (gv_SelectProduct.RowCount <= 0)
                    pnBtnDeleteGv.Visible = false;
                else
                    lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
            }
        }

        private void btn_XoaTatCa_Click(object sender, EventArgs e)
        {
            dtLuoi.Rows.Clear();
            DataTable s = new DataTable();
            s = dtLuoi;
            sSQL_DL = "";
            sSQL_Yeucauthem = "";
            TotalMoney();
            pnBtnDeleteGv.Visible = false;
            btnComboOption.Visible = false;
        }

        private void gv_SelectProduct_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == SOTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
        }

        private void gv_SelectProduct_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gv_SelectProduct.FocusedRowHandle >= 0)
            {
                clsKP_Terminal.ShowCustomerDisplayOneLine(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString() + " : " + (int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["GIABAN"]).ToString())).ToString("N0") + " VND");
            }
            else
            {
                clsKP_Terminal.ShowCustomerDisplayOneLine("Welcome to " + cls_ConfigCashier.nameShop);
            }
            XuLyGv();
        }

        private void gv_SelectProduct_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            maMon = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();

            if (sendOrder != 1)
                return;
            XuLyGv();
            pnBtnDeleteGv.Visible = true;
            Flag_soluong = true;
            lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();

            if (bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
                btnComboOption.Visible = true;
            else
                btnComboOption.Visible = false;

            string yeucau = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["GHICHU"]).ToString();
            if (yeucau != "a")
                txtYeuCauThem.Text = yeucau;
            else
                txtYeuCauThem.Text = "";
        }

        private void XuLyGv()
        {
            if (pnBtnDeleteGv.Visible)
            {
                if (gv_SelectProduct.RowCount > 5)
                {
                    pnUpDowGv.Visible = true;
                }
                else
                {
                    pnUpDowGv.Visible = false;
                }
            }
            else
            {
                if (gv_SelectProduct.RowCount > 8)
                {
                    pnUpDowGv.Visible = true;
                }
                else
                {
                    pnUpDowGv.Visible = false;
                }
            }
        }

        int iTudong_load_sodoban = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            string hour, minute, second;
            second = DateTime.Now.Second.ToString();
            hour = DateTime.Now.Hour.ToString();
            minute = DateTime.Now.Minute.ToString();
            if (second.Length < 2)
                lbgiay.Text = "0" + second;
            else
                lbgiay.Text = second;
            if (minute.Length < 2)
                lbGio.Text = hour + ":0" + minute;
            else
                lbGio.Text = hour + ":" + minute;

            if (iTudong_load_sodoban < 80)
            {
                iTudong_load_sodoban++;
            }
            else
            {
                if (funtions == 1)
                {
                    GetSoDoBan();
                }
                iTudong_load_sodoban = 0;

            }
            timer1.Enabled = true;
        }

        bool flagdatban = false;
        bool _flag_chuyenban = false;
        string _mahoadon_chuyenban = "";
        private void timer2_Tick(object sender, EventArgs e)
        {
            timer2.Enabled = false;
            try
            {
                if (flagdatban)
                    return;
                string sSQL = "";
                sSQL += "Select Distinct HD.MA_HOADON,DATEDIFF(minute, GETDATE(),GIOVAO) as PHUT,ISMO_BAN" + "\n";
                sSQL += "From HOADON HD,CT_MOBAN CT,BAN B" + "\n";
                sSQL += "Where HD.ISBANDAT=1 AND ISPAYMENT = 0" + "\n";
                sSQL += "AND HD.MA_HOADON=CT.MA_HOADON AND CT.MA_BAN=B.MA_BAN AND ISMO_BAN=0";
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                if (dt.Rows.Count > 0)
                {
                    sSQL = "Select TEN,GIATRI From CAUHINH Where TEN='NHACNHO_DATBAN'";
                    DataTable dtCauhinh = clsMain.ReturnDataTable(sSQL);

                    int n = dt.Rows.Count;
                    for (int i = 0; i < n; i++)
                    {
                        if (int.Parse(dt.Rows[i]["PHUT"].ToString()) <= int.Parse(dtCauhinh.Rows[i]["GIATRI"].ToString()) && int.Parse(dt.Rows[i]["PHUT"].ToString()) >= 0)
                        {
                            flagdatban = true;
                            if (DialogResult.Yes == (XtraMessageBox.Show(_cobansaptoigiovao, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Information)))
                            {
                                Frm_Danhsachdatban frm = new Frm_Danhsachdatban();
                                frm.ShowDialog();

                                _flag_chuyenban = Frm_Danhsachdatban.flag_Chuyenban;
                                _mahoadon_chuyenban = Frm_Danhsachdatban.mahoadon;
                                if (_flag_chuyenban)
                                {
                                    btn_DatBan_Click(sender, e);
                                    sSQL = "";
                                    sSQL += "Select TEN,hd.SODIENTHOAI,DIACHI,TIENDATCOC,NGAYDAT,GIOVAO" + "\n";
                                    sSQL += "From HOADON hd,NHACUNGCAP n" + "\n";
                                    sSQL += "Where hd.MA_KHACHHANG=n.MA" + "\n";
                                    sSQL += "And hd.MA_HOADON=" + clsMain.SQLString(_mahoadon_chuyenban);
                                    DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                                    if (dt1.Rows.Count > 0)
                                    {
                                        txtDBTenKH.Text = dt1.Rows[0]["TEN"].ToString();
                                        txtDienThoai.Text = dt1.Rows[0]["SODIENTHOAI"].ToString();
                                        txtAddress.Text = dt1.Rows[0]["DIACHI"].ToString();
                                        txtPartMoneyPayments.EditValue = dt1.Rows[0]["TIENDATCOC"].ToString();
                                        txtngay.EditValue = DateTime.Parse(dt1.Rows[0]["NGAYDAT"].ToString());
                                        cbHH.Text = DateTime.Parse(dt1.Rows[0]["GIOVAO"].ToString()).Hour.ToString().Length < 2 ? "0" + DateTime.Parse(dt1.Rows[0]["GIOVAO"].ToString()).Hour.ToString() : DateTime.Parse(dt1.Rows[0]["GIOVAO"].ToString()).Hour.ToString();
                                        cbMM.Text = DateTime.Parse(dt1.Rows[0]["GIOVAO"].ToString()).Minute.ToString().Length < 2 ? "0" + DateTime.Parse(dt1.Rows[0]["GIOVAO"].ToString()).Minute.ToString() : DateTime.Parse(dt1.Rows[0]["GIOVAO"].ToString()).Minute.ToString();
                                        KhoamoControl_Chuyenban(true);
                                    }
                                }
                                else
                                {
                                    GetSoDoBan();
                                    flagdatban = false;
                                }

                            }
                            else
                                flagdatban = false;
                        }
                    }
                }
            }
            catch
            {
            }
            timer2.Enabled = true;
        }

        public void KhoamoControl_Chuyenban(bool flag)
        {
            txtDBTenKH.Properties.ReadOnly = txtDienThoai.Properties.ReadOnly = txtngay.Properties.ReadOnly = flag;
            txtAddress.Properties.ReadOnly = txtPartMoneyPayments.Properties.ReadOnly = flag;
            cbHH.Properties.ReadOnly = cbMM.Properties.ReadOnly = chkEmail.Properties.ReadOnly = flag;
            btn_SelectDishes.Enabled = btn_Danhsachdatban.Enabled = !flag;
        }

        private void btn_ThuNgan_Click(object sender, EventArgs e)
        {
            if (funtions == 9 || exitReprint == true)
                return;
            SetBackgroudOnClick(btn_ThuNgan);
            SetDefautlBackgroud(btn_NghiepVu);
            SetDefautlBackgroud(btn_DatThu);
            pnThuNgan.BringToFront();
        }

        private void btn_NghiepVu_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
                return;
            SetBackgroudOnClick(btn_NghiepVu);
            SetDefautlBackgroud(btn_ThuNgan);
            SetDefautlBackgroud(btn_DatThu);
            pnNghiepVu.BringToFront();
        }

        private void btn_DatThu_Click(object sender, EventArgs e)
        {
            if (funtions != 1 || exitReprint == true)
                return;
            SetBackgroudOnClick(btn_DatThu);
            SetDefautlBackgroud(btn_ThuNgan);
            SetDefautlBackgroud(btn_NghiepVu);
            pnDacThu.BringToFront();
        }

        private void btn_logout_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnMoKet_Click(object sender, EventArgs e)
        {
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }
            KP_Terminal.clsKP_Terminal.OpenCashDrawer(false);
        }

        private void btnKetKa_Click(object sender, EventArgs e)
        {
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }
            Frm_Ketca frm = new Frm_Ketca();
            frm.Width = this.Width;
            frm.Height = this.Height;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btn_HuyBan_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
                return;
            if (maBan == "")
                return;
            if (CheckOpenTable(maBan) == false)
                return;
            if (XtraMessageBox.Show("Xác nhận bạn muốn hủy bàn ? ", _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.Yes)
                return;
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }

            string maHoaDon = GetMaHoaDon(maBan);
            DataTable table = GetTableInBill(maHoaDon);
            string sSQL = "";
            if (table.Rows.Count > 0)
            {
                sSQL = "";
                sSQL += "EXEC SP_InsertHuyBan " + clsMain.SQLString(maHoaDon) + "," + clsMain.SQLString(table.Rows[0]["MA_BAN"].ToString()) + "," + clsMain.SQLString(clsGlobal.gsUserID) + "";
                clsMain.ExecuteSQL(sSQL);

                sSQL = "";
                sSQL += "Delete From CT_HOADON Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + "\n";
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    sSQL += "Delete From CT_MOBAN Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + " And MA_BAN=" + clsMain.SQLString(table.Rows[i]["MA_BAN"].ToString()) + "\n";

                    sSQL += "Update  DISPLAY_ORDER Set FLAG=0 Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + " And MA_BAN=" + clsMain.SQLString(table.Rows[i]["MA_BAN"].ToString()) + "\n";

                    sSQL += "Update BAN SET ISMO_BAN=0, SEE_PAYMENTS = 0 Where MA_BAN =" + clsMain.SQLString(table.Rows[i]["MA_BAN"].ToString()) + "\n";
                }
                sSQL += "Delete From HOADON Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + "\n";
                sSQL += "Delete From DINHLUONG_DIEUCHINH Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + "\n";


                clsMain.ExecuteSQL(sSQL);
            }
            else
            {
                sSQL = "";
                sSQL += "Update BAN SET ISMO_BAN=0, SEE_PAYMENTS = 0 Where MA_BAN =" + clsMain.SQLString(maBan) + "\n";
                clsMain.ExecuteSQL(sSQL);
            }

            //in bill hủy bàn
            string sTen_ban = "";
            sSQL = "";
            sSQL += "Select TEN_BAN From BAN Where MA_BAN=" + clsMain.SQLString(maBan) + "\n";
            DataTable dtb = clsMain.ReturnDataTable(sSQL);
            if (dtb.Rows.Count > 0)
            {
                sTen_ban = dtb.Rows[0]["TEN_BAN"].ToString();
            }

            while (gv_SelectProduct.RowCount > 0)
            {
                List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                int j = 0;
                string maBEP = "";
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    DM_HangHoa dmHangHoa = new DM_HangHoa();
                    maBEP = gv_SelectProduct.GetRowCellValue(0, gv_SelectProduct.Columns["MA_BEP"]).ToString();
                    if (maBEP == gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_BEP"]).ToString())
                    {
                        dmHangHoa.Ma_HoaDon = maHoaDon;
                        dmHangHoa.Ten_HoaDon = "Hủy Bàn";
                        dmHangHoa.MaQuay = cls_ConfigCashier.nameCashier;
                        dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                        dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                        dmHangHoa.TEN_BAN = sTen_ban;
                        dmHangHoa.STT = (++j).ToString();
                        dmHangHoa.MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                        dmHangHoa.TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                        dmHangHoa.SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                        dmHangHoa.IS_INBEP = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["IS_INBEP"]).ToString());
                        dmHangHoa.GHICHU = "Hủy Món";

                        if (dmHangHoa.IS_INBEP)
                            listDM_HangHoa.Add(dmHangHoa);

                        if (i != 0)
                        {
                            gv_SelectProduct.DeleteRow(i);
                            i--;
                        }
                    }
                }
                gv_SelectProduct.DeleteRow(0);
                string print = GetIdKitchen(maBEP);
                if (print == null)
                    XtraMessageBox.Show(_chuachonmayinchomhnay, _thongbao, MessageBoxButtons.OK);
                convert cvrt = new convert();
                DataTable Orderbill = cvrt.ConvertToDataTable(listDM_HangHoa);
                if (Orderbill.Rows.Count > 0)
                {
                    Frm_Report1 frm1 = new Frm_Report1();
                    if (cls_KP_RES.Mode == 3)
                    {
                        frm1.ReportName = "rpt_Order_1";
                    }
                    else
                    {
                        frm1.ReportName = "rpt_Order";
                    }
                    frm1.DataSource = Orderbill;
                    frm1.WindowState = FormWindowState.Minimized;
                    frm1.PrinterName = print;
                    frm1.IsPrint = true;
                    frm1.ShowDialog();
                    frm1.Dispose();
                }
                // Printer Order huy viet nam khi in thì in thêm 1 bill ngay tai quay order
                if (Orderbill.Rows.Count > 0)
                {
                    print = KP_Terminal.clsKP_Terminal.Mayinbill;
                    Frm_Report1 frm1 = new Frm_Report1();
                    if (cls_KP_RES.Mode == 3)
                    {
                        frm1.ReportName = "rpt_Order_1";
                    }
                    else
                    {
                        frm1.ReportName = "rpt_Order";
                    }
                    frm1.DataSource = Orderbill;
                    frm1.WindowState = FormWindowState.Minimized;
                    frm1.PrinterName = print;
                    frm1.IsPrint = true;
                    frm1.ShowDialog();
                    frm1.Dispose();
                }
            }
            ExitDivideTable();
        }

        private DataTable GetTableInBill(string idBill)
        {
            string sql = "select MA_BAN from CT_MOBAN where MA_HOADON='" + idBill + "'";
            DataTable retTables = clsMain.ReturnDataTable(sql);
            return retTables;
        }

        private void btnSuaGia_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
                return;
            string sMaHoaDon = GetMaHoaDon(maBan);
            if (sMaHoaDon == "")
                return;
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }

            string sMaHangHoa = gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
            string sTenHangHoa = gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
            string sSLHangHoa = gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["SOLUONG"]).ToString();
            string sGiaHangHoa = gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["GIABAN"]).ToString();
            DataTable dt = new DataTable();
            dt.Columns.Add("MaHoaDon");
            dt.Columns.Add("MaHangHoa");
            dt.Columns.Add("TenHangHoa");
            dt.Columns.Add("SLHangHoa");
            dt.Columns.Add("GiaHangHoa");
            string sSQL = "";
            sSQL += "select MA_CTHOADON,MA_HANGHOA,MA_HOADON,SOLUONG,GIABAN from CT_HOADON where MA_HOADON=" + clsMain.SQLString(sMaHoaDon) + " and MA_HANGHOA=" + clsMain.SQLString(sMaHangHoa) + "\n";
            DataTable dtTemp = clsMain.ReturnDataTable(sSQL);
            foreach (DataRow dr in dtTemp.Rows)
            {
                sSLHangHoa = dr["SOLUONG"].ToString();
                sGiaHangHoa = dr["GIABAN"].ToString();

                if (sSLHangHoa.Contains("."))
                {
                    dt.Rows.Add(sMaHoaDon, sMaHangHoa, sTenHangHoa, decimal.Parse(sSLHangHoa), sGiaHangHoa);
                }
                else
                {
                    for (int i = 0; i < decimal.Parse(sSLHangHoa); i++)
                    {
                        dt.Rows.Add(sMaHoaDon, sMaHangHoa, sTenHangHoa, "1", sGiaHangHoa);
                    }
                }
            }

            bool bUpdataGia = false;
            Frm_SuaGia frm1 = new Frm_SuaGia();
            frm1.dtHanghoa = dt;
            frm1.ShowDialog();
            bUpdataGia = frm1.bCoSua;
            frm1.Dispose();
            if (bUpdataGia)
            {
                ExitDivideTable();
            }
        }

        private void btnChietkhau_Click(object sender, EventArgs e)
        {
            if (gv_SelectProduct.RowCount <= 0 || maMon == null || maMon == "")
                return;

            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }

            decimal Giamoi = 0;
            Frm_Nhapchietkhau frm = new Frm_Nhapchietkhau();
            frm.ShowDialog();
            Giamoi = frm.tmp_Thoigia;
            frm.Dispose();
            if (Giamoi <= 100 && Giamoi >= 0)
            {
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    if (gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString() == maMon)
                    {
                        gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["CHIETKHAU"], Giamoi.ToString());
                    }
                }
                if (gv_SelectProduct.RowCount > 0)
                {
                    TotalMoney();
                }
            }
        }

        private void btn_SoDoBan_Click(object sender, EventArgs e)
        {
            if (funtions == 1 && maBan != "")
            {
                funtions = 2;
                btn_SoDoBan.Text = _sdb;
                sSQL_DL = "";
                sSQL_Yeucauthem = "";
                dtLuoi.Rows.Clear();
                gr_SelectProduct.DataSource = dtLuoi;
                lbTotal.Text = "0";
                txtID.Visible = true;
                txtID.Focus();
                pnShowHang.BringToFront();
                SetBackgroudOnClick(btn_SoDoBan);
                SetDefaultBackgroundBtnInPn(pnThuNgan);
                SetDefaultBackgroundBtnInPn(pnNghiepVu);
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    if (btn.Name == btn_SoDoBan.Name || btn.Name == btn_GuiOrder.Name || btn.Name == btn_YeuCauThem.Name)
                    {
                        btn.Visible = true;
                    }
                    else
                    {
                        btn.Visible = false;
                    }
                }
                //8/11/2016 them so khach theo yeu cau huy viet nam 22/09/2018 lay thong tin cau hinh len
                if (cls_ConfigCashier.sSOLUONGKHACH == "1")
                {
                    if (!CheckOpenTable(maBan))
                    {
                        Frm_Soluongkhach frmTemp = new Frm_Soluongkhach();
                        frmTemp.ShowDialog();
                        sokhach = int.Parse(frmTemp.tmp_Thoigia.ToString());
                        frmTemp.Dispose();
                    }
                }
                GetBuffet();
                GetNhomhang();
                GetListMon();
            }
            else if (funtions == 2)
            {
                funtions = 1;
                maBan = "";
                sSQL_DL = "";
                sSQL_Yeucauthem = "";
                dtLuoi.Rows.Clear();
                gr_SelectProduct.DataSource = dtLuoi;
                lbBan.Text = "";
                lbMon.Text = "";
                lbTotal.Text = "0";
                btn_SoDoBan.Text = _mb;
                txtID.Visible = false;
                pnShowHang.BringToFront();
                SetDefautlBackgroud(btn_SoDoBan);
                SetDefaultBackgroundBtnInPn(pnThuNgan);
                SetDefaultBackgroundBtnInPn(pnNghiepVu);
                GetKhuVuc();
                GetSoDoBan();
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    btn.Visible = true;
                }
            }
        }

        private void SetDefaultBackgroundBtnInPn(PanelControl pn2)
        {
            foreach (SimpleButton bt in pn2.Controls)
            {
                SetDefautlBackgroud(bt);
            }
        }

        private void PrintMaVachFastFood(String sIdBill, String maBan)
        {
            if (cls_ConfigCashier.sIn_Mavach_Fastfood != "0")
            {
                DataView myDV = (DataView)gv_SelectProduct.DataSource;
                DataTable myDT = myDV.ToTable().Copy();
                DataTable myDT1 = myDV.ToTable().Copy();

                myDT1.Clear();
                DataColumn sTem = new DataColumn();
                sTem.ColumnName = "TEM";
                sTem.DataType = System.Type.GetType("System.String");
                sTem.DefaultValue = 1;
                myDT1.Columns.Add(sTem);

                foreach (DataRow dr in myDT.Rows)
                {
                    String sSLHangHoa = dr["SOLUONG"].ToString();

                    if (sSLHangHoa.Contains("."))
                    {
                        myDT1.Rows.Add(dr["MA_HANGHOA"].ToString(), dr["TEN_HANGHOA"].ToString(), "1", dr["GIABAN"].ToString(), dr["GHICHU"].ToString(), dr["MA_BEP"].ToString(), dr["THUE"].ToString(), dr["Thanh_Tien"].ToString(), Boolean.Parse(dr["TRANGTHAI"].ToString()), Boolean.Parse(dr["IS_COMBO"].ToString()), Boolean.Parse(dr["IS_INBEP"].ToString()), Boolean.Parse(dr["IS_KHUYENMAI"].ToString()), dr["CHIETKHAU"].ToString(), "1");
                    }
                    else
                    {
                        for (int i = 0; i < decimal.Parse(sSLHangHoa); i++)
                        {
                            myDT1.Rows.Add(dr["MA_HANGHOA"].ToString(), dr["TEN_HANGHOA"].ToString(), "1", dr["GIABAN"].ToString(), dr["GHICHU"].ToString(), dr["MA_BEP"].ToString(), dr["THUE"].ToString(), dr["Thanh_Tien"].ToString(), Boolean.Parse(dr["TRANGTHAI"].ToString()), Boolean.Parse(dr["IS_COMBO"].ToString()), Boolean.Parse(dr["IS_INBEP"].ToString()), Boolean.Parse(dr["IS_KHUYENMAI"].ToString()), dr["CHIETKHAU"].ToString(), "1");
                        }
                    }
                }

                Frm_OrderTem frm = new Frm_OrderTem(myDT1);
                frm.ShowDialog();
                myDT = frm.dtGridviewProduct;
                frm.Dispose();

                myDT.DefaultView.Sort = "TEM";
                myDT = myDT.DefaultView.ToTable();

                String nameTable = clsMain.ReturnDataTable("Select TEN_BAN From BAN Where MA_BAN = " + clsMain.SQLString(maBan) + "").Rows[0][0].ToString();
                String pTem = "";

                Frm_Report1 frmTmp1;
                DataTable dt1 = new DataTable();
                dt1.Columns.Add("MA_HOADON");
                dt1.Columns.Add("TEN_HANGHOA");
                dt1.Columns.Add("NGAY_HOADON");
                dt1.Columns.Add("DON_GIA");
                dt1.Columns.Add("YEUCAUTHEM");
                dt1.Columns.Add("BAN");
                pTem = myDT.Rows[0]["TEM"].ToString();

                foreach (DataRow dr in myDT.Rows)
                {
                    if (pTem == dr["TEM"].ToString())
                    {
                        dt1.Rows.Add(sIdBill, dr["TEN_HANGHOA"].ToString(), cls_KP_RES.DateToString_Full_NoSecond(DateTime.Now), int.Parse(dr["GIABAN"].ToString()).ToString("N0"), dr["GHICHU"].ToString().Length > 1 ? dr["GHICHU"].ToString() : "", nameTable);
                    }
                    else
                    {
                        frmTmp1 = new Frm_Report1();
                        frmTmp1.ReportName = "rpt_MaVach_Fastfood";
                        frmTmp1.DataSource = dt1;
                        frmTmp1.WindowState = FormWindowState.Minimized;
                        frmTmp1.PrinterName = KP_Terminal.clsKP_Terminal.Mayinmavach;
                        frmTmp1.IsPrint = true;
                        frmTmp1.ShowDialog();
                        frmTmp1.Dispose();

                        dt1.Clear();
                        dt1.Rows.Add(sIdBill, dr["TEN_HANGHOA"].ToString(), cls_KP_RES.DateToString_Full_NoSecond(DateTime.Now), int.Parse(dr["GIABAN"].ToString()).ToString("N0"), dr["GHICHU"].ToString().Length > 1 ? dr["GHICHU"].ToString() : "", nameTable);

                        pTem = dr["TEM"].ToString();
                    }
                }

                frmTmp1 = new Frm_Report1();
                frmTmp1.ReportName = "rpt_MaVach_Fastfood";
                frmTmp1.DataSource = dt1;
                frmTmp1.WindowState = FormWindowState.Minimized;
                frmTmp1.PrinterName = KP_Terminal.clsKP_Terminal.Mayinmavach;
                frmTmp1.IsPrint = true;
                frmTmp1.ShowDialog();
                frmTmp1.Dispose();
            }
        }

        private void PrintMaVachFastFood1(String sIdBill, String maBan)
        {
            if (cls_ConfigCashier.sIn_Mavach_Fastfood != "0")
            {
                String nameTable = clsMain.ReturnDataTable("Select TEN_BAN From BAN Where MA_BAN = " + clsMain.SQLString(maBan) + "").Rows[0][0].ToString();
                DataView dv = (DataView)gv_SelectProduct.DataSource;
                DataTable dt = dv.ToTable().Copy();
                foreach (DataRow dr in dt.Rows)
                {
                    for (int i = 0; i < int.Parse(dr["SOLUONG"].ToString()); i++)
                    {
                        DataTable dt1 = new DataTable();
                        dt1.Columns.Add("MA_HOADON");
                        dt1.Columns.Add("TEN_HANGHOA");
                        dt1.Columns.Add("NGAY_HOADON");
                        dt1.Columns.Add("DON_GIA");
                        dt1.Columns.Add("YEUCAUTHEM");
                        dt1.Columns.Add("BAN");
                        dt1.Rows.Add(sIdBill, dr["TEN_HANGHOA"].ToString(), cls_KP_RES.DateToString_Full_NoSecond(DateTime.Now), int.Parse(dr["GIABAN"].ToString()).ToString("N0"), dr["GHICHU"].ToString().Length > 1 ? dr["GHICHU"].ToString() : "", nameTable);

                        Frm_Report1 frmTmp1 = new Frm_Report1();
                        frmTmp1.ReportName = "rpt_MaVach_Fastfood";
                        frmTmp1.DataSource = dt1;
                        frmTmp1.WindowState = FormWindowState.Minimized;
                        frmTmp1.PrinterName = KP_Terminal.clsKP_Terminal.Mayinmavach;
                        frmTmp1.IsPrint = true;
                        frmTmp1.ShowDialog();
                        frmTmp1.Dispose();
                    }
                }
            }
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
                return true;
            else
                return false;
        }

        private void btn_OrderSent_Click(object sender, EventArgs e)
        {
            if (funtions != 2)
                return;
            if (sendOrder != 1)
                return;
            if (string.IsNullOrEmpty(clsGlobal.gsMaNVOfUserLogin))
            {
                XtraMessageBox.Show(_manvrong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            try
            {
                pnBtnDeleteGv.Visible = false;
                btnComboOption.Visible = false;
                string sSQL = "";
                if (CheckOpenTable(maBan) == false)
                {
                    if (lbTotal.Text == "0")
                    {
                        return;
                    }
                    string maHoaDon = GetNewCodeBill();
                    //bàn
                    sSQL += "Update BAN SET ISMO_BAN=1 Where MA_BAN = " + clsMain.SQLString(maBan) + "\n";
                    //hóa đơn
                    sSQL += "Insert Into HOADON(MA_HOADON,SOLUONGKHACH, MANHANVIEN, NGAYTAO, GIOVAO)" + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(maHoaDon) + ",";
                    sSQL += clsMain.SQLString(sokhach.ToString()) + ",";
                    sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                    sSQL += "GETDATE(),GETDATE())" + "\n";
                    //ct mở bàn
                    sSQL += "Insert Into CT_MOBAN(MA_HOADON, MA_BAN)" + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(maHoaDon) + ",";
                    sSQL += clsMain.SQLString(maBan) + ")" + "\n";

                    clsMain.ExecuteSQL(sSQL);
                }

                string maHDB = GetMaHoaDon(maBan);
                //31/08/2017 fastfood sau khi in bill thì trên lưới có bao nhiêu hàng mỗi hàng sẻ in tem qua máy in tem
                PrintMaVachFastFood(maHDB, maBan);

                string nameTable = clsMain.ReturnDataTable("Select TEN_BAN From BAN Where MA_BAN = " + clsMain.SQLString(maBan) + "").Rows[0][0].ToString();
                List<DM_HangHoa> listDM_HangHoa_All = new List<DM_HangHoa>();//23/08/2018 Order All huy viet nam khi in thì in thêm 1 bill tổng ngay tai quay order
                while (gv_SelectProduct.RowCount > 0)
                {
                    List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                    int j = 0;
                    string maBEP = "";
                    for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                    {
                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        maBEP = gv_SelectProduct.GetRowCellValue(0, gv_SelectProduct.Columns["MA_BEP"]).ToString();

                        if (maBEP == gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_BEP"]).ToString())
                        {
                            dmHangHoa.Ma_HoaDon = maHDB;
                            dmHangHoa.Ten_HoaDon = "Order Bếp";
                            dmHangHoa.TEN_BAN = nameTable;
                            dmHangHoa.MaQuay = cls_ConfigCashier.nameCashier;
                            dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                            dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                            dmHangHoa.STT = (++j).ToString();
                            dmHangHoa.MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                            dmHangHoa.TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                            dmHangHoa.SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                            dmHangHoa.THUE = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["THUE"]).ToString());
                            dmHangHoa.GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                            dmHangHoa.TRANGTHAI = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TRANGTHAI"]).ToString());
                            dmHangHoa.IS_COMBO = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["IS_COMBO"]).ToString());
                            dmHangHoa.IS_INBEP = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["IS_INBEP"]).ToString());
                            dmHangHoa.CHIETKHAU = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["CHIETKHAU"]).ToString());
                            string repuest = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"]).ToString();
                            if (repuest == "a")
                                dmHangHoa.GHICHU = "";
                            else
                                dmHangHoa.GHICHU = repuest;
                            //Insert vào bảng DINHLUONG_DIEUCHINH
                            if (sSQL_DL != "")
                            {
                                sSQL_DL = sSQL_DL.Replace("TEMP_MA_HOADON", clsMain.SQLString(maHDB));
                                string[] arr = sSQL_DL.Split('*');
                                for (int z = 0; z < arr.Length; z++)
                                {
                                    string[] arr1 = arr[z].Split('=');
                                    if (arr1[0].Contains(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString()))
                                    {
                                        clsMain.ExecuteSQL(arr1[1]);
                                    }
                                }
                                arr = sSQL_Yeucauthem.Split('*');
                                for (int z = 0; z < arr.Length; z++)
                                {
                                    string[] arr1 = arr[z].Split('=');
                                    if (arr1[0].Contains(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString()))
                                    {
                                        dmHangHoa.GHICHU = arr1[1];
                                    }
                                }
                            }
                            //nếu hàng hóa được in bếp mới cho in xuống bếp
                            if (dmHangHoa.IS_INBEP)
                            {
                                listDM_HangHoa.Add(dmHangHoa);
                                listDM_HangHoa_All.Add(dmHangHoa);
                            }
                            //CT hóa đơn
                            sSQL = "";
                            if (ChecBishInTable(maHDB, dmHangHoa.MA_HANGHOA, dmHangHoa.IS_COMBO, dmHangHoa.GIABAN.ToString()) == false)
                            {
                                sSQL += "Insert into CT_HOADON(MA_HOADON,MA_HANGHOA,SOLUONG,GIABAN,GIABAN_THAT,THUE,TRANGTHAI,IS_COMBO,CHIETKHAU)";
                                sSQL += "Values (";
                                sSQL += clsMain.SQLString(maHDB) + ",";
                                sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                                sSQL += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                                sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                                sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                                sSQL += clsMain.SQLString(dmHangHoa.THUE.ToString()) + ",";
                                sSQL += clsMain.SQLBit(dmHangHoa.TRANGTHAI) + ",";
                                sSQL += clsMain.SQLBit(dmHangHoa.IS_COMBO) + ",";
                                sSQL += clsMain.SQLString(dmHangHoa.CHIETKHAU.ToString()) + ")";
                                clsMain.ExecuteSQL(sSQL);
                            }
                            else
                            {
                                sSQL += "UPDATE CT_HOADON SET " + "\n";
                                sSQL += "SOLUONG +=" + clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + "," + "\n";
                                sSQL += "CHIETKHAU =" + clsMain.SQLString(dmHangHoa.CHIETKHAU.ToString()) + "\n";
                                sSQL += "WHERE MA_HOADON =" + clsMain.SQLString(maHDB) + "\n";
                                sSQL += "AND Isnull(Is_Combo,0)=" + clsMain.SQLBit(dmHangHoa.IS_COMBO) + "\n";
                                sSQL += "AND MA_HANGHOA =" + clsMain.SQLString(dmHangHoa.MA_HANGHOA);
                                clsMain.ExecuteSQL(sSQL);
                            }
                            //Man hinh bep
                            if (dmHangHoa.IS_INBEP)
                            {
                                sSQL = "";
                                sSQL += "Insert into DISPLAY_ORDER (MA_HANGHOA,MA_HOADON, MA_BAN, GIOORDER, SOLUONG,NHANVIEN, YEUCAU,FLAG)";
                                sSQL += "Values (";
                                sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                                sSQL += clsMain.SQLString(maHDB) + ",";
                                sSQL += clsMain.SQLString(maBan) + ",";
                                sSQL += "GETDATE()" + ",";
                                sSQL += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                                sSQL += clsMain.SQLString(clsGlobal.gsUserID) + ",";
                                sSQL += clsMain.SQLStringUnicode(dmHangHoa.GHICHU) + ",";
                                sSQL += clsMain.SQLBit(true) + ")";
                                clsMain.ExecuteSQL(sSQL);
                            }
                            // Delete product selected 
                            if (i != 0)
                            {
                                gv_SelectProduct.DeleteRow(i);
                                i--;
                            }
                        }
                    }
                    // Printer Order 
                    gv_SelectProduct.DeleteRow(0);
                    convert cvrt = new convert();
                    DataTable Orderbill = cvrt.ConvertToDataTable(listDM_HangHoa);

                    if (Orderbill.Rows.Count > 0)
                    {
                        string print = GetIdKitchen(maBEP);
                        if (print == null)
                        {
                            XtraMessageBox.Show(_chuachonmayinchomathang, _thongbao, MessageBoxButtons.OK);
                            print = "";
                        }

                        //8/11/2016 căt từng món theo yêu cầu huy việt nam
                        if (cls_KP_RES.Mode == 4)
                        {
                            string sSoKhach = "1";
                            try
                            {
                                DataTable dtKhach = clsMain.ReturnDataTable("select SOLUONGKHACH from HOADON where MA_HOADON=" + clsMain.SQLString(Orderbill.Rows[0]["Ma_HoaDon"].ToString()));
                                sSoKhach = dtKhach.Rows[0][0].ToString();
                            }
                            catch
                            {
                            }
                            DataColumn dctemp = new DataColumn();
                            dctemp.DefaultValue = sSoKhach;
                            dctemp.ColumnName = "sokhach";
                            Orderbill.Columns.Add(dctemp);
                            for (int i = 0; i < Orderbill.Rows.Count; i++)
                            {
                                DataTable dtTmp1 = Orderbill.Copy();
                                dtTmp1.Clear();
                                dtTmp1.Rows.Add(Orderbill.Rows[i].ItemArray);

                                Frm_Report1 frmTmp1 = new Frm_Report1();
                                frmTmp1.ReportName = "rpt_Order_1_Cat1";
                                frmTmp1.DataSource = dtTmp1;
                                frmTmp1.WindowState = FormWindowState.Minimized;
                                frmTmp1.PrinterName = print;
                                frmTmp1.IsPrint = true;
                                frmTmp1.ShowDialog();
                                frmTmp1.Dispose();
                            }
                        }
                        else
                        {
                            Frm_Report1 frm = new Frm_Report1();
                            if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                            {
                                frm.ReportName = "rpt_Order_58";
                            }
                            else
                            {
                                if (cls_KP_RES.Mode == 3 || cls_KP_RES.Mode == 4)
                                {
                                    frm.ReportName = "rpt_Order_1";
                                }
                                else
                                {
                                    frm.ReportName = "rpt_Order";
                                }
                            }
                            frm.DataSource = Orderbill;
                            frm.WindowState = FormWindowState.Minimized;
                            frm.PrinterName = print;
                            frm.IsPrint = true;
                            frm.ShowDialog();
                            frm.Dispose();
                        }
                    }

                }

                if (cls_ConfigCashier.sInbilltong == "1")
                {
                    // Printer Order All huy viet nam khi in thì in thêm 1 bill tổng ngay tai quay order
                    convert cvrt_All = new convert();
                    DataTable Orderbill_All = cvrt_All.ConvertToDataTable(listDM_HangHoa_All);
                    if (Orderbill_All.Rows.Count > 0)
                    {
                        string print = KP_Terminal.clsKP_Terminal.Mayinbill;
                        Frm_Report1 frm = new Frm_Report1();
                        if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                        {
                            frm.ReportName = "rpt_Order_58";
                        }
                        else
                        {
                            if (cls_KP_RES.Mode == 3 || cls_KP_RES.Mode == 4)
                            {
                                frm.ReportName = "rpt_Order_1";
                            }
                            else
                            {
                                frm.ReportName = "rpt_Order_Tong";
                            }
                        }
                        frm.DataSource = Orderbill_All;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.PrinterName = print;
                        frm.IsPrint = true;
                        frm.ShowDialog();
                        frm.Dispose();
                        //10/10 in thêm bill tổng ra các máy khác nữa
                        string SQL = "";
                        SQL += "Select MA_BEP As MA, MAYINBEP, SUDUNG" + "\n";
                        SQL += "From DM_BEP_INTONG" + "\n";
                        SQL += "Where SUDUNG=1" + "\n";
                        SQL += "Order by MA" + "\n";
                        DataTable dt_bep = clsMain.ReturnDataTable(SQL);
                        for (int i = 0; i < dt_bep.Rows.Count; i++)
                        {
                            print = dt_bep.Rows[i]["MAYINBEP"].ToString();
                            Frm_Report1 frm1 = new Frm_Report1();
                            if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                            {
                                frm1.ReportName = "rpt_Order_58";
                            }
                            else
                            {
                                if (cls_KP_RES.Mode == 3 || cls_KP_RES.Mode == 4)
                                {
                                    frm1.ReportName = "rpt_Order_1";
                                }
                                else
                                {
                                    frm1.ReportName = "rpt_Order_Tong";
                                }
                            }
                            frm1.DataSource = Orderbill_All;
                            frm1.WindowState = FormWindowState.Minimized;
                            frm1.PrinterName = print;
                            frm1.IsPrint = true;
                            frm1.ShowDialog();
                            frm1.Dispose();
                        }

                    }
                }

                funtions = 1;
                sendOrder = 0;
                maBan = "";
                sSQL_DL = "";
                sSQL_Yeucauthem = "";
                dtLuoi.Rows.Clear();
                gr_SelectProduct.DataSource = dtLuoi;
                maMon = "";
                lbMon.Text = "";
                lbBan.Text = "";
                lbTotal.Text = "0";
                btn_SoDoBan.Text = _mb;
                txtID.Visible = false;
                pnShowHang.BringToFront();
                SetDefautlBackgroud(btn_SoDoBan);
                SetDefaultBackgroundBtnInPn(pnThuNgan);
                SetDefaultBackgroundBtnInPn(pnNghiepVu);
                GetKhuVuc();
                GetSoDoBan();
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    btn.Visible = true;
                }
            }
            catch (Exception ex)
            {
                txtID.Visible = false;
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string GetNewCodeBill()
        {
            string _mahoadon = "";
            string ngay = cls_ConfigCashier.idCashier + DateTime.Now.Day.ToString("00") + DateTime.Now.Month.ToString("00") + DateTime.Now.Year.ToString("0000");
            DataTable dtMA = clsMain.ReturnDataTable("select [dbo].[fc_NewcodeHoaDon] ('" + ngay + "')");
            _mahoadon = dtMA.Rows[0][0].ToString();
            return _mahoadon;
        }

        private bool ChecBishInTable(string _maHOADON, string maHH, bool IsCombo, string giaban)
        {
            string sSQL = "";
            sSQL += "Select MA_CTHOADON" + "\n";
            sSQL += "From CT_HOADON" + "\n";
            sSQL += "Where Isnull(Is_Combo,0)=" + clsMain.SQLBit(IsCombo) + "\n";
            sSQL += "And MA_HOADON =" + clsMain.SQLString(_maHOADON) + "\n";
            sSQL += "And MA_HANGHOA =" + clsMain.SQLString(maHH);
            sSQL += "And GIABAN =" + clsMain.SQLString(giaban);
            DataTable dish = clsMain.ReturnDataTable(sSQL);
            if (dish.Rows.Count > 0)
                return true;
            return false;
        }

        private string GetIdKitchen(string idKitchen)
        {
            string printKitchen = null;
            string sql = "Select MAYINBEP From DM_BEP Where MA_BEP =" + clsMain.SQLString(idKitchen);
            DataTable kitchen = clsMain.ReturnDataTable(sql);
            if (kitchen.Rows.Count > 0)
            {
                printKitchen = kitchen.Rows[0]["MAYINBEP"].ToString();
                return printKitchen;
            }
            else
                return printKitchen;
        }

        private void btn_YeuCauThem_Click(object sender, EventArgs e)
        {
            if (maMon == "" || funtions != 2 || sendOrder != 1)
                return;
            SetBackgroudOnClick(btn_YeuCauThem);
            lbkvYeuCauThem.Text = lbKhuVuc.Text;
            lbTableYeuCauThem.Text = lbBan.Text;
            lbMonYeuCauThem.Text = lbMon.Text;
            us_keyboard3.SetTextbox(txtYeuCauThem);
            pnYeucauthem.BringToFront();
            txtYeuCauThem.Focus();
            SelectProduct();
        }

        private void SelectProduct()
        {
            if (pnYeuCau.Controls.Count > 0)
                return;
            string sql = "SELECT  * FROM YEUCAUTHEM ORDER BY  YEUCAU ";
            DataTable requestAdd = clsMain.ReturnDataTable(sql);
            pnYeuCau.Controls.Clear();
            for (int i = 0; i < requestAdd.Rows.Count; i++)
            {
                SimpleButton btn = new SimpleButton();
                btn.Name = requestAdd.Rows[i]["id"].ToString();
                btn.Text = requestAdd.Rows[i]["YEUCAU"].ToString();
                btn.Click += new EventHandler(btn_Click);
                btn.Size = new Size(115, 68);
                pnYeuCau.Controls.Add(btn);
            }
        }

        private void btn_Click(object sender, EventArgs e)
        {
            foreach (SimpleButton btn in pnYeuCau.Controls)
            {
                if (btn.Name == this.ActiveControl.Name)
                {
                    txtYeuCauThem.Text += btn.Text + "," + " ";
                    break;
                }
            }
            txtYeuCauThem.Focus();
            int textLength = txtYeuCauThem.Text.Length;
            txtYeuCauThem.SelectionStart = textLength;
            txtYeuCauThem.SelectionLength = 0;
        }

        private void txtYeuCauThem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    string maHH = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                    if (maHH == maMon)
                    {
                        gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"], txtYeuCauThem.Text);
                        break;
                    }
                }
                txtYeuCauThem.Text = "";
                pnShowHang.BringToFront();
                SetDefaultBackgroundBtnInPn(pnThuNgan);
                SetDefaultBackgroundBtnInPn(pnNghiepVu);
            }
        }

        private void btnYCT_Len_Click(object sender, EventArgs e)
        {
            try
            {
                pnYeuCau.AutoScrollPosition = new Point(0, pnYeuCau.VerticalScroll.Value - pnYeuCau.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnYCT_Xuong_Click(object sender, EventArgs e)
        {
            try
            {
                pnYeuCau.AutoScrollPosition = new Point(0, pnYeuCau.VerticalScroll.Value + pnYeuCau.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnChuyenban_Click(object sender, EventArgs e)
        {
            if (funtions == 1)
            {
                // Check Table Opened or table paymented
                if (maBan == "")
                    return;
                if (GetMaHoaDon(maBan) == "")
                    return;
                btn_Chuyenban.Text = _hcb;
                SetBackgroudOnClick(btn_Chuyenban);
                txtmaKVBanGoc.Text = lbKhuVuc.Text;
                txtmaBanGoc.Text = lbBan.Text;
                funtions = 11;
                string sql = "UPDATE BAN SET SEE_PAYMENTS = 0 Where MA_BAN =" + clsMain.SQLString(maBan);
                clsMain.ExecuteSQL(sql);
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    if (btn.Name == btn_Chuyenban.Name)
                    {
                        btn.Visible = true;
                    }
                    else
                    {
                        btn.Visible = false;
                    }
                }
            }
            else if (funtions == 11)
            {
                funtions = 1;
                btn_Chuyenban.Text = _cb;
                SetDefautlBackgroud(btn_Chuyenban);
                dtLuoi.Rows.Clear();
                gr_SelectProduct.DataSource = dtLuoi;
                maBan = "";
                maBanGhep = "";
                lbBan.Text = "";
                lbMon.Text = "";
                lbTotal.Text = "0";
                GetKhuVuc();
                GetSoDoBan();
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    btn.Visible = true;
                }
            }
        }

        private void ExitDivideTable()
        {
            sendOrder = 0;
            funtions = 1;
            exitReprint = false;
            cPageBan = 1;
            btn_SoDoBan.Text = _mb;
            btn_Tachban.Text = _tb;
            btn_Chuyenban.Text = _cb;
            btn_GhepBan.Text = _gb;
            maBan = "";
            maBanGhep = "";
            txtmaBanGhep.Text = "";
            txtmaKVBanGhep.Text = "";
            divide_Table1.Rows.Clear();
            divide_Table2.Rows.Clear();
            SetDefaultBackgroundBtnInPn(pnThuNgan);
            SetDefaultBackgroundBtnInPn(pnNghiepVu);
            foreach (SimpleButton btn in pnThuNgan.Controls)
            {
                btn.Visible = true;
            }
            foreach (SimpleButton btn in pnNghiepVu.Controls)
            {
                btn.Visible = true;
            }
            pnShowHang.BringToFront();
            GetKhuVuc();
            GetSoDoBan();
            string maHD = GetMaHoaDon(maBan);
            GetDishOfTable(maHD);
        }

        private void btn_GhepBan_Click(object sender, EventArgs e)
        {
            if (funtions == 1)
            {
                // Check Table Opened or table paymented
                if (maBan == "")
                    return;
                if (GetMaHoaDon(maBan) == "")
                    return;
                btn_GhepBan.Text = _hgb;
                SetBackgroudOnClick(btn_GhepBan);
                funtions = 5;
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    if (btn.Name == btn_GhepBan.Name)
                    {
                        btn.Visible = true;
                    }
                    else
                    {
                        btn.Visible = false;
                    }
                }
            }
            else if (funtions == 5)
            {
                funtions = 1;
                btn_GhepBan.Text = _gb;
                SetDefautlBackgroud(btn_GhepBan);
                dtLuoi.Rows.Clear();
                gr_SelectProduct.DataSource = dtLuoi;
                maBan = "";
                maBanGhep = "";
                lbBan.Text = "";
                lbMon.Text = "";
                lbTotal.Text = "0";
                GetKhuVuc();
                GetSoDoBan();
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    btn.Visible = true;
                }
            }
        }

        private void btn_TachBan_Click(object sender, EventArgs e)
        {
            if (funtions == 1)
            {
                // Check Table Opened or table paymented
                if (maBan == "")
                    return;
                if (GetMaHoaDon(maBan) == "")
                    return;
                btn_Tachban.Text = _htb;
                txtmaKVBanGoc.Text = lbKhuVuc.Text;
                txtmaBanGoc.Text = lbBan.Text;
                SetBackgroudOnClick(btn_Tachban);
                funtions = 6;
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    if (btn.Name == btn_Tachban.Name)
                    {
                        btn.Visible = true;
                    }
                    else
                    {
                        btn.Visible = false;
                    }
                }
            }
            else if (funtions == 6)
            {
                funtions = 1;
                btn_Tachban.Text = _tb;
                SetDefautlBackgroud(btn_Tachban);
                dtLuoi.Rows.Clear();
                gr_SelectProduct.DataSource = dtLuoi;
                maBan = "";
                maBanGhep = "";
                lbBan.Text = "";
                lbMon.Text = "";
                lbTotal.Text = "0";
                pnShowHang.BringToFront();
                GetKhuVuc();
                GetSoDoBan();
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    btn.Visible = true;
                }
            }
        }

        private void gv_divide_Table_1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == _SSTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
        }

        private void gv_divide_Table_2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == __STT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
        }

        private void btn_DowDivide_Click(object sender, EventArgs e)
        {
            try
            {
                gv_divide_Table_1.Focus();
                gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.FocusedRowHandle + 1;
                btn_UpDivide.Enabled = true;
                if (gv_divide_Table_1.FocusedRowHandle == gv_divide_Table_1.RowCount - 1)
                    btn_DowDivide.Enabled = false;
            }
            catch
            {
            }
        }

        private void btn_UpDivide_Click(object sender, EventArgs e)
        {
            try
            {
                gv_divide_Table_1.Focus();
                gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.FocusedRowHandle - 1;
                btn_DowDivide.Enabled = true;
                if (gv_divide_Table_1.FocusedRowHandle == 0)
                    btn_UpDivide.Enabled = false;
            }
            catch
            {
            }
        }

        private void btn_ToRightAll_Click(object sender, EventArgs e)
        {
            try
            {
                while (gv_divide_Table_1.RowCount > 0)
                    ThrowTable1ToTable2();
            }
            catch
            {
            }
        }

        private void btn_ToLeftAll_Click(object sender, EventArgs e)
        {
            try
            {
                while (gv_divide_Table_2.RowCount > 0)
                    ThrowTable2ToTable1();
            }
            catch
            {
            }
        }

        private void btn_ToRight_Click(object sender, EventArgs e)
        {
            try
            {
                ThrowTable1ToTable2();
            }
            catch
            {
            }
        }

        private void btn_Toleft_Click(object sender, EventArgs e)
        {
            try
            {
                ThrowTable2ToTable1();
            }
            catch
            {
            }
        }

        public void ThrowTable1ToTable2()
        {
            decimal sl = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
            if (SoLuong_HuyMon > sl)
            {
                XtraMessageBox.Show("Số lượng nhập lớn hơn số lượng trên order", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                SoLuong_HuyMon = 0;
                flag_sole = false;
                return;
            }
            if (gv_divide_Table_1.RowCount <= 0)
                return;
            bool checkProductOnGv = false;
            if (divide_Table2.Rows.Count > 0)
            {
                for (int i = 0; i < divide_Table2.Rows.Count; i++)
                {
                    string maHH = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString();
                    string giaban = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_GIABAN"]).ToString();
                    if (divide_Table2.Rows[i]["__MA_HANGHOA"].ToString() == maHH && divide_Table2.Rows[i]["__GIABAN"].ToString() == giaban)
                    {
                        if (decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString()) <= 1)
                        {
                            if (flag_sole)
                                divide_Table2.Rows[i]["__SOLUONG"] = decimal.Parse(divide_Table2.Rows[i]["__SOLUONG"].ToString()) + SoLuong_HuyMon;
                            else
                                divide_Table2.Rows[i]["__SOLUONG"] = decimal.Parse(divide_Table2.Rows[i]["__SOLUONG"].ToString()) + decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                        }
                        else
                        {
                            if (flag_sole)
                                divide_Table2.Rows[i]["__SOLUONG"] = decimal.Parse(divide_Table2.Rows[i]["__SOLUONG"].ToString()) + SoLuong_HuyMon;
                            else
                                divide_Table2.Rows[i]["__SOLUONG"] = decimal.Parse(divide_Table2.Rows[i]["__SOLUONG"].ToString()) + 1;
                        }
                        divide_Table2.Rows[i]["__Thanh_Tien"] = decimal.Parse(divide_Table2.Rows[i]["__GIABAN"].ToString()) * decimal.Parse(divide_Table2.Rows[i]["__SOLUONG"].ToString());
                        checkProductOnGv = true;
                    }
                }
            }

            if (checkProductOnGv == false)
            {
                DataRow dr = divide_Table2.NewRow();
                dr[0] = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString();
                dr[1] = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_TEN_HANGHOA"]).ToString();

                if (decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString()) <= 1)
                {
                    if (flag_sole)
                        dr[2] = SoLuong_HuyMon;
                    else
                        dr[2] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                }
                else
                {
                    if (flag_sole)
                        dr[2] = SoLuong_HuyMon;
                    else
                        dr[2] = 1;
                }
                dr[3] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_GIABAN"]).ToString());
                dr[4] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_MA_BEP"]).ToString());
                dr[5] = decimal.Parse(dr[2].ToString()) * decimal.Parse(dr[3].ToString());
                dr[6] = false;
                divide_Table2.Rows.Add(dr);
            }
            gr_divide_Table_2.DataSource = divide_Table2;
            gv_divide_Table_2.FocusedRowHandle = gv_divide_Table_2.RowCount - 1;

            if (decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString()) <= 1)
            {
                if (flag_sole)
                {
                    gv_divide_Table_1.SetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"], sl - SoLuong_HuyMon);
                    if (decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString()) == 0)
                        gv_divide_Table_1.DeleteSelectedRows();
                }
                else
                    gv_divide_Table_1.DeleteSelectedRows();
            }
            else
            {
                if (flag_sole)
                    gv_divide_Table_1.SetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"], sl - SoLuong_HuyMon);
                else
                    gv_divide_Table_1.SetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"], sl - 1);
            }
            gv_divide_Table_1.SetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_Thanh_Tien"], decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString()) * decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_GIABAN"]).ToString()));
            flag_sole = false;
        }

        private void ThrowTable2ToTable1()
        {
            if (gv_divide_Table_2.RowCount <= 0)
                return;
            bool checkProductOnGv = false;
            if (divide_Table1.Rows.Count > 0)
            {
                for (int i = 0; i < divide_Table1.Rows.Count; i++)
                {
                    string maHH = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                    string giaban = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__GIABAN"]).ToString();
                    if (divide_Table1.Rows[i]["_MA_HANGHOA"].ToString() == maHH && divide_Table1.Rows[i]["_GIABAN"].ToString() == giaban)
                    {
                        if (decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString()) < 1)
                            divide_Table1.Rows[i]["_SOLUONG"] = decimal.Parse(divide_Table1.Rows[i]["_SOLUONG"].ToString()) + decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                        else
                            divide_Table1.Rows[i]["_SOLUONG"] = decimal.Parse(divide_Table1.Rows[i]["_SOLUONG"].ToString()) + 1;
                        divide_Table1.Rows[i]["_Thanh_Tien"] = decimal.Parse(divide_Table1.Rows[i]["_GIABAN"].ToString()) * decimal.Parse(divide_Table1.Rows[i]["_SOLUONG"].ToString());
                        checkProductOnGv = true;
                    }
                }
            }
            if (checkProductOnGv == false)
            {
                DataRow dr = divide_Table1.NewRow();
                dr[0] = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                dr[1] = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__TEN_HANGHOA"]).ToString();
                if (decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString()) < 1)
                    dr[2] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                else
                    dr[2] = 1;
                dr[3] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__GIABAN"]).ToString());
                dr[4] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__MA_BEP"]).ToString());
                dr[5] = decimal.Parse(dr[2].ToString()) * decimal.Parse(dr[3].ToString());
                divide_Table1.Rows.Add(dr);
            }
            gr_divide_Table_1.DataSource = divide_Table1;
            gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.RowCount - 1;
            if (decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString()) <= 1)
            {
                gv_divide_Table_2.DeleteSelectedRows();
            }
            else
            {
                decimal sl = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                gv_divide_Table_2.SetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"], sl - 1);
            }
            gv_divide_Table_2.SetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__Thanh_Tien"], decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString()) * decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["_GIABAN"]).ToString()));
        }

        private void btn_ESCThroughtTable_Click(object sender, EventArgs e)
        {
            try
            {
                ExitDivideTable();
            }
            catch
            {
            }
        }

        private void btn_OkThrough_Click(object sender, EventArgs e)
        {
            try
            {
                if (gv_divide_Table_1.RowCount <= 0 || gv_divide_Table_2.RowCount <= 0)
                    return;
                //huy mon
                if (funtions == 1)
                {
                    string maHoaDon = GetMaHoaDon(maBan);
                    string sSQL = "";
                    for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                    {
                        string idDisher = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                        decimal soLuong = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                        string giaban = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__GIABAN"]).ToString();

                        sSQL = "";
                        sSQL += "EXEC SP_InsertHuyMon " + clsMain.SQLString(maHoaDon) + ",";
                        sSQL += clsMain.SQLString(maBan) + ",";
                        sSQL += clsMain.SQLString(idDisher) + ",";
                        sSQL += clsMain.SQLString(soLuong.ToString()) + ",";
                        sSQL += clsMain.SQLString(giaban) + ",";
                        sSQL += clsMain.SQLString(clsGlobal.gsUserID);
                        clsMain.ExecuteSQL(sSQL);


                        sSQL = "";
                        sSQL += "Update CT_HOADON Set" + "\n";
                        sSQL += "SOLUONG= SOLUONG - " + soLuong + "\n";
                        sSQL += "Where MA_HOADON=" + clsMain.SQLString(maHoaDon) + "\n";
                        sSQL += "And MA_HANGHOA=" + clsMain.SQLString(idDisher) + "\n";
                        sSQL += "And GIABAN=" + clsMain.SQLString(giaban) + "\n";
                        clsMain.ExecuteSQL(sSQL);
                        // Kiểm tra sau khi trừ nếu số lượng =0 thì delete hàng hóa đó khỏi CT HOADON và  DINHLUONG DIEUCHINH và  DISPLAY ORDER
                        sSQL = "";
                        sSQL += "Select MA_HOADON,MA_HANGHOA" + "\n";
                        sSQL += "From CT_HOADON" + "\n";
                        sSQL += "Where SOLUONG= 0" + "\n";
                        sSQL += "And MA_HOADON=" + clsMain.SQLString(maHoaDon) + "\n";
                        sSQL += "And MA_HANGHOA=" + clsMain.SQLString(idDisher) + "\n";
                        sSQL += "And GIABAN=" + clsMain.SQLString(giaban) + "\n";
                        DataTable dt = clsMain.ReturnDataTable(sSQL);
                        if (dt.Rows.Count > 0)
                        {
                            sSQL = "";
                            sSQL += "Select MA_HOADON" + "\n";
                            sSQL += "From DINHLUONG_DIEUCHINH" + "\n";
                            sSQL += "Where MA_HOADON=" + clsMain.SQLString(maHoaDon) + "\n";
                            sSQL += "And MATHANHPHAM=" + clsMain.SQLString(idDisher);
                            dt = clsMain.ReturnDataTable(sSQL);
                            sSQL = "";
                            if (dt.Rows.Count > 0)
                            {
                                sSQL += "Delete DINHLUONG_DIEUCHINH" + "\n";
                                sSQL += "Where MA_HOADON=" + clsMain.SQLString(maHoaDon) + "\n";
                                sSQL += "And MATHANHPHAM=" + clsMain.SQLString(idDisher) + "\n";
                            }
                            sSQL += "Delete from CT_HOADON" + "\n";
                            sSQL += "Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + "\n";
                            sSQL += "And MA_HANGHOA=" + clsMain.SQLString(idDisher) + "\n";
                            sSQL += "And GIABAN=" + clsMain.SQLString(giaban) + "\n";

                            sSQL += "Update  DISPLAY_ORDER Set FLAG=0" + "\n";
                            sSQL += "Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + "\n";
                            sSQL += "And MA_HANGHOA=" + clsMain.SQLString(idDisher) + "\n";
                            sSQL += "And MA_BAN=" + clsMain.SQLString(maBan) + "\n";
                            clsMain.ExecuteSQL(sSQL);
                        }
                        else
                        {
                            //code cho man hinh bep
                            sSQL = "";
                            sSQL += "Select *" + "\n";
                            sSQL += "From DISPLAY_ORDER" + "\n";
                            sSQL += "Where TRANGTHAI=0 AND FLAG=1" + "\n";
                            sSQL += "And MA_HOADON=" + clsMain.SQLString(maHoaDon) + "\n";
                            sSQL += "And MA_HANGHOA=" + clsMain.SQLString(idDisher) + "\n";
                            sSQL += "Order by GIOORDER Desc" + "\n";
                            DataTable dtBep = clsMain.ReturnDataTable(sSQL);
                            foreach (DataRow dr in dtBep.Rows)
                            {
                                if (decimal.Parse(dr["SOLUONG"].ToString()) > soLuong)
                                {
                                    sSQL = "";
                                    sSQL += "Update DISPLAY_ORDER Set" + "\n";
                                    sSQL += "SOLUONG= SOLUONG - " + soLuong + "\n";
                                    sSQL += "Where STT=" + clsMain.SQLString(dr["STT"].ToString()) + "\n";
                                    clsMain.ExecuteSQL(sSQL);
                                    break;
                                }
                                else if (decimal.Parse(dr["SOLUONG"].ToString()) == soLuong)
                                {
                                    sSQL += "Update  DISPLAY_ORDER Set FLAG=0" + "\n";
                                    sSQL += "Where STT=" + clsMain.SQLString(dr["STT"].ToString()) + "\n";
                                    clsMain.ExecuteSQL(sSQL);
                                    break;
                                }
                                else
                                {
                                    sSQL += "Update  DISPLAY_ORDER Set FLAG=0" + "\n";
                                    sSQL += "Where STT=" + clsMain.SQLString(dr["STT"].ToString()) + "\n";
                                    clsMain.ExecuteSQL(sSQL);
                                    soLuong = soLuong - decimal.Parse(dr["SOLUONG"].ToString());
                                }
                            }
                        }
                    }
                    // Printer Order 
                    string sTen_ban = "";
                    sSQL = "";
                    sSQL += "Select TEN_BAN From BAN Where MA_BAN=" + clsMain.SQLString(maBan) + "\n";
                    DataTable dtb = clsMain.ReturnDataTable(sSQL);
                    if (dtb.Rows.Count > 0)
                    {
                        sTen_ban = dtb.Rows[0]["TEN_BAN"].ToString();
                    }
                    while (gv_divide_Table_2.RowCount > 0)
                    {
                        List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                        int j = 0;
                        string maBEP = "";
                        for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                        {
                            DM_HangHoa dmHangHoa = new DM_HangHoa();
                            maBEP = gv_divide_Table_2.GetRowCellValue(0, gv_divide_Table_2.Columns["__MA_BEP"]).ToString();
                            if (maBEP == gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_BEP"]).ToString())
                            {
                                dmHangHoa.Ma_HoaDon = maHoaDon;
                                dmHangHoa.Ten_HoaDon = "Hủy Order Bếp";
                                dmHangHoa.MaQuay = cls_ConfigCashier.nameCashier;
                                dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                                dmHangHoa.Ngay = string.Format("{0:dd/MM/yyyy hh:mm:ss}", DateTime.Now);
                                dmHangHoa.TEN_BAN = sTen_ban;
                                dmHangHoa.STT = (++j).ToString();
                                dmHangHoa.MA_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                                dmHangHoa.TEN_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__TEN_HANGHOA"]).ToString();
                                dmHangHoa.SOLUONG = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                                dmHangHoa.IS_INBEP = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["IS_INBEP"]).ToString());
                                dmHangHoa.GHICHU = "Hủy Món";

                                if (dmHangHoa.IS_INBEP)
                                    listDM_HangHoa.Add(dmHangHoa);

                                if (i != 0)
                                {
                                    gv_divide_Table_2.DeleteRow(i);
                                    i--;
                                }
                            }
                        }
                        gv_divide_Table_2.DeleteRow(0);
                        string print = GetIdKitchen(maBEP);
                        if (print == null)
                            XtraMessageBox.Show(_chuachonmayinchomhnay, _thongbao, MessageBoxButtons.OK);
                        convert cvrt = new convert();
                        DataTable Orderbill = cvrt.ConvertToDataTable(listDM_HangHoa);
                        if (Orderbill.Rows.Count > 0)
                        {
                            Frm_Report1 frm = new Frm_Report1();
                            if (cls_KP_RES.Mode == 3)
                                frm.ReportName = "rpt_Order_1";
                            else
                                frm.ReportName = "rpt_Order";
                            frm.DataSource = Orderbill;
                            frm.WindowState = FormWindowState.Minimized;
                            frm.PrinterName = print;
                            frm.IsPrint = true;
                            frm.ShowDialog();
                            frm.Dispose();
                        }
                        // Printer Order huy viet nam khi in thì in thêm 1 bill ngay tai quay order
                        if (Orderbill.Rows.Count > 0)
                        {
                            print = KP_Terminal.clsKP_Terminal.Mayinbill;
                            Frm_Report1 frm = new Frm_Report1();
                            if (cls_KP_RES.Mode == 3)
                                frm.ReportName = "rpt_Order_1";
                            else
                                frm.ReportName = "rpt_Order";
                            frm.DataSource = Orderbill;
                            frm.WindowState = FormWindowState.Minimized;
                            frm.PrinterName = print;
                            frm.IsPrint = true;
                            frm.ShowDialog();
                            frm.Dispose();
                        }
                    }
                }
                //tach ban
                else if (funtions == 6)
                {
                    if (CheckOpenTable(maBanGhep) == false)
                    {
                        string sql = "Update BAN SET ISMO_BAN=1 where MA_BAN =" + int.Parse(maBanGhep);
                        clsMain.ExecuteSQL(sql);

                        string maHoaDon = GetNewCodeBill();
                        string sqlInserter = "Insert Into HOADON(MA_HOADON,SOLUONGKHACH, MANHANVIEN, NGAYTAO, GIOVAO)" + "\n";
                        sqlInserter += "Values(";
                        sqlInserter += clsMain.SQLString(maHoaDon) + ",";
                        sqlInserter += clsMain.SQLString(sokhach.ToString()) + ",";
                        sqlInserter += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                        sqlInserter += "GETDATE(),GETDATE())" + "\n";
                        clsMain.ExecuteSQL(sqlInserter);

                        string sqlCTMOBAN = "Insert into CT_MOBAN(MA_HOADON, MA_BAN)";
                        sqlCTMOBAN += " values(" + "'" + maHoaDon + "'," + int.Parse(maBanGhep) + ")";
                        clsMain.ExecuteSQL(sqlCTMOBAN);

                        for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                        {
                            if (maHoaDon != null)
                            {
                                if (ChecBishInTable(maHoaDon, gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString(), bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__IS_COMBO"]).ToString()), gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__GIABAN"]).ToString()) == false)
                                {
                                    string sqlc = "";
                                    sqlc += "Insert into CT_HOADON( MA_HOADON, MA_HANGHOA,  SOLUONG, GIABAN,GIABAN_THAT, Is_Combo)" + "\n";
                                    sqlc += "Values(" + "\n";
                                    sqlc += clsMain.SQLString(maHoaDon) + ",";
                                    sqlc += clsMain.SQLString(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString()) + ",";
                                    sqlc += clsMain.SQLString(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString()) + ",";
                                    sqlc += clsMain.SQLString(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__GIABAN"]).ToString()) + ",";
                                    sqlc += clsMain.SQLString(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__GIABAN"]).ToString()) + ",";
                                    sqlc += clsMain.SQLBit(bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__IS_COMBO"]).ToString())) + ")";
                                    clsMain.ExecuteSQL(sqlc);
                                }
                                else
                                {
                                    string sqlc = "";
                                    sqlc += "UPDATE CT_HOADON SET" + "\n";
                                    sqlc += "SOLUONG +=" + gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString() + "\n";
                                    sqlc += "WHERE MA_HOADON =" + clsMain.SQLString(maHoaDon) + "\n";
                                    sqlc += "And Is_Combo =" + clsMain.SQLBit(bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__IS_COMBO"]).ToString())) + "\n";
                                    sqlc += "And MA_HANGHOA =" + clsMain.SQLString(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString());
                                    clsMain.ExecuteSQL(sqlc);
                                }
                            }
                            else
                            {
                                MessageBox.Show(_tachbanloi);
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                        {
                            string maHDB = GetMaHoaDon(maBanGhep);
                            DM_HangHoa dmHangHoa = new DM_HangHoa();
                            dmHangHoa.MA_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                            dmHangHoa.SOLUONG = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                            dmHangHoa.GIABAN = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__GIABAN"]).ToString());
                            if (maHDB != null)
                            {
                                if (ChecBishInTable(maHDB, gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString(), bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__IS_COMBO"]).ToString()), gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__GIABAN"]).ToString()) == false)
                                {
                                    string sqlc = "";
                                    sqlc += "Insert into CT_HOADON( MA_HOADON, MA_HANGHOA, SOLUONG, GIABAN ,GIABAN_THAT, Is_Combo)" + "\n";
                                    sqlc += "Values(";
                                    sqlc += clsMain.SQLString(maHDB) + ",";
                                    sqlc += clsMain.SQLString(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString()) + ",";
                                    sqlc += clsMain.SQLString(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString()) + ",";
                                    sqlc += clsMain.SQLString(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__GIABAN"]).ToString()) + ",";
                                    sqlc += clsMain.SQLString(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__GIABAN"]).ToString()) + ",";
                                    sqlc += clsMain.SQLBit(bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__IS_COMBO"]).ToString())) + ")";
                                    clsMain.ExecuteSQL(sqlc);
                                }
                                else
                                {
                                    string sqlc = "";
                                    sqlc += "UPDATE CT_HOADON SET" + "\n";
                                    sqlc += "SOLUONG +=" + gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString() + "\n";
                                    sqlc += "WHERE MA_HOADON =" + clsMain.SQLString(maHDB) + "\n";
                                    sqlc += "And Is_Combo =" + clsMain.SQLBit(bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__IS_COMBO"]).ToString())) + "\n";
                                    sqlc += "And MA_HANGHOA =" + clsMain.SQLString(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString());
                                    clsMain.ExecuteSQL(sqlc);
                                }
                            }
                            else
                            {
                                throw new Exception(_tachbanloi);
                            }
                        }
                    }
                    //ReUpdate table 1
                    string idBill1 = GetMaHoaDon(maBan);
                    string sqldelte = "Delete CT_HOADON where MA_HOADON ='" + idBill1 + "'";
                    clsMain.ExecuteSQL(sqldelte);
                    for (int i = 0; i < gv_divide_Table_1.RowCount; i++)
                    {
                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        dmHangHoa.MA_HANGHOA = gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString();
                        dmHangHoa.SOLUONG = decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                        dmHangHoa.GIABAN = decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_GIABAN"]).ToString());
                        if (idBill1 != null)
                        {
                            if (ChecBishInTable(idBill1, dmHangHoa.MA_HANGHOA, false, dmHangHoa.GIABAN.ToString()) == false)
                            {
                                string sqlc = "";
                                sqlc += "Insert into CT_HOADON( MA_HOADON, MA_HANGHOA,  SOLUONG, GIABAN,GIABAN_THAT, Is_Combo)" + "\n";
                                sqlc += "Values(";
                                sqlc += clsMain.SQLString(idBill1) + ",";
                                sqlc += clsMain.SQLString(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString()) + ",";
                                sqlc += clsMain.SQLString(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_SOLUONG"]).ToString()) + ",";
                                sqlc += clsMain.SQLString(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_GIABAN"]).ToString()) + ",";
                                sqlc += clsMain.SQLString(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_GIABAN"]).ToString()) + ",";
                                sqlc += clsMain.SQLBit(bool.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_IS_COMBO"]).ToString())) + ")";
                                clsMain.ExecuteSQL(sqlc);
                            }
                            else
                            {
                                string sqlc = "";
                                sqlc += "UPDATE CT_HOADON SET" + "\n";
                                sqlc += "SOLUONG +=" + gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_SOLUONG"]).ToString() + "\n";
                                sqlc += "WHERE MA_HOADON =" + clsMain.SQLString(idBill1) + "\n";
                                sqlc += "And Is_Combo =" + clsMain.SQLBit(bool.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_IS_COMBO"]).ToString())) + "\n";
                                sqlc += "And MA_HANGHOA =" + clsMain.SQLString(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString());
                                clsMain.ExecuteSQL(sqlc);
                            }
                        }
                        else
                        {
                            XtraMessageBox.Show(_tachbanloi, _thongbao, MessageBoxButtons.OK);
                        }
                    }
                    if (cls_ConfigCashier.sInbep_Chuyenban == "1")
                    {
                        string bangoc = "";
                        string banchuyen = "";
                        DataTable dtTenban = new DataTable();
                        string sSQL = "";
                        sSQL += "Select TEN_BAN" + "\n";
                        sSQL += "From BAN" + "\n";
                        sSQL += "Where MA_BAN=" + clsMain.SQLString(maBan);
                        dtTenban = clsMain.ReturnDataTable(sSQL);
                        bangoc = dtTenban.Rows[0]["TEN_BAN"].ToString();

                        sSQL = "";
                        sSQL += "Select TEN_BAN" + "\n";
                        sSQL += "From BAN" + "\n";
                        sSQL += "Where MA_BAN=" + clsMain.SQLString(maBanGhep);
                        dtTenban = clsMain.ReturnDataTable(sSQL);
                        banchuyen = dtTenban.Rows[0]["TEN_BAN"].ToString();

                        sSQL = "";
                        sSQL += "Select MAYINBEP From DM_BEP Where MA_CUAHANG=" + clsMain.SQLString(cls_ConfigCashier.idShop);
                        DataTable dtbep = clsMain.ReturnDataTable(sSQL);
                        string print = "";
                        for (int i = 0; i < dtbep.Rows.Count; i++)
                        {
                            print = dtbep.Rows[i]["MAYINBEP"].ToString();
                            DataTable dt = new DataTable();
                            dt.Columns.Add("TITLE");
                            dt.Columns.Add("BANGOC");
                            dt.Columns.Add("BANCHUYEN");
                            dt.Columns.Add("GIUA");
                            dt.Rows.Add("Tách Bàn", bangoc, banchuyen, "tách món");
                            Frm_Report1 frm = new Frm_Report1();
                            frm.ReportName = "rpt_Chuyenban";
                            frm.DataSource = dt;
                            frm.WindowState = FormWindowState.Minimized;
                            frm.PrinterName = print;
                            frm.IsPrint = true;
                            frm.ShowDialog();
                            frm.Dispose();
                        }
                    }
                }
                divide_Table1.Rows.Clear();
                divide_Table2.Rows.Clear();
                ExitDivideTable();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString(), _thongbao, MessageBoxButtons.OK);
            }
        }

        private void btn_ExitDishe_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
                return;
            if (maBan == "")
                return;
            if (CheckOpenTable(maBan) == false)
                return;

            if (exitReprint == true)
            {
                SetDefautlBackgroud(btn_HuyMon);
                ExitDivideTable();
            }
            else
            {
                if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
                {
                    bool check;
                    Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                    fr.ShowDialog();
                    check = fr.kt;
                    fr.Dispose();
                    if (check == false)
                        return;
                }

                SetBackgroudOnClick(btn_HuyMon);
                pnInforTablecheck.Visible = false;
                pnTachban.BringToFront();
                int w = (pn_GroupTable.Width - 110) / 2;
                panelControl15.Width = w;
                pn_TableTow.Width = w;
                List<DM_ListDisheTableOne> lisHangHoa = new List<DM_ListDisheTableOne>();
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    DM_ListDisheTableOne dmHangHoa = new DM_ListDisheTableOne();
                    dmHangHoa._MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                    dmHangHoa._TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                    dmHangHoa._SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                    dmHangHoa._GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                    dmHangHoa._MA_BEP = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_BEP"]).ToString());
                    dmHangHoa._Thanh_Tien = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"]).ToString());
                    dmHangHoa._GHICHU = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"]).ToString();
                    lisHangHoa.Add(dmHangHoa);
                }
                convert cvrt = new convert();
                divide_Table1 = cvrt.ConvertToDataTable(lisHangHoa);
                gr_divide_Table_1.DataSource = divide_Table1;
                gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.RowCount - 1;

                foreach (SimpleButton btn in pnNghiepVu.Controls)
                {
                    if (btn.Name == btn_HuyMon.Name)
                    {
                        btn.Visible = true;
                    }
                    else
                    {
                        btn.Visible = false;
                    }
                }
                exitReprint = true;
            }
        }

        private void btnGiaBan2_Click(object sender, EventArgs e)
        {
            if (funtions != 1 || gv_SelectProduct.RowCount <= 0 || maBan == "")
                return;

            //if (cls_ConfigCashier.sBANGIA2 != "0")
            //{
            //    bool check;
            //    Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
            //    fr.ShowDialog();
            //    check = fr.kt;
            //    fr.Dispose();
            //    if (check == false)
            //        return;
            //}

            ////string smaMon = gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
            ////string maHD = GetMaHoaDon(maBan);

            //////Kiểm tra nếu đang dùng bản giá theo khu hay cửa hàng thì return
            ////string sSQL = "select GIATRI from CAUHINH where TEN='APDUNGBANGGIATHEOKHUVUC'";
            ////DataTable dtcheck = clsMain.ReturnDataTable(sSQL);
            ////if (dtcheck.Rows.Count > 0)
            ////{
            ////    if (dtcheck.Rows[0]["GIATRI"].ToString() != "0")
            ////    {
            ////        MessageBox.Show("Đang dùng chương trình tính giá theo khu hay cửa hàng. Không áp dụng được giá bán 2", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            ////        return;
            ////    }
            ////}

            //////Kiểm tra hàng hóa là thời giá hay ko,nếu đúng thì return
            ////sSQL = "Select ISNULL(SUAGIA,0) AS SUAGIA From HANGHOA Where MA_HANGHOA = " + clsMain.SQLString(smaMon);
            ////dtcheck = clsMain.ReturnDataTable(sSQL);
            ////if (dtcheck.Rows.Count > 0)
            ////{
            ////    if (bool.Parse(dtcheck.Rows[0]["SUAGIA"].ToString()))
            ////    {
            ////        MessageBox.Show(_daylahhtheothoigia, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            ////        return;
            ////    }
            ////}

            //////lây giá bán 1,2
            ////sSQL = "select  ISNULL(GIABAN1,0) as GIABAN1,ISNULL(GIABAN2,0) as GIABAN2 from HANGHOA where MA_HANGHOA = " + clsMain.SQLString(smaMon);
            ////DataTable dtgiaban = clsMain.ReturnDataTable(sSQL);
            ////if (dtgiaban.Rows.Count > 0)
            ////{
            ////    if (gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["GIABAN"]).ToString() == dtgiaban.Rows[0]["GIABAN1"].ToString())
            ////    {
            ////        //gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"], dtgiaban.Rows[0]["GIABAN2"].ToString());
            ////        sSQL = "";
            ////        sSQL += "UPDATE CT_HOADON SET " + "\n";
            ////        sSQL += "GIABAN =" + clsMain.SQLString(dtgiaban.Rows[0]["GIABAN2"].ToString()) + "\n";
            ////        sSQL += "WHERE MA_HOADON =" + clsMain.SQLString(maHD) + "\n";
            ////        sSQL += "AND Isnull(Is_Combo,0)=" + clsMain.SQLBit(false) + "\n";
            ////        sSQL += "AND MA_HANGHOA =" + clsMain.SQLString(smaMon) + "\n";
            ////        sSQL += "AND GIABAN =" + clsMain.SQLString(gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["GIABAN"]).ToString()) + "\n";
            ////        clsMain.ExecuteSQL(sSQL);
            ////    }
            ////    else
            ////    {
            ////        //gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"], dtgiaban.Rows[0]["GIABAN1"].ToString());
            ////        sSQL = "";
            ////        sSQL += "UPDATE CT_HOADON SET " + "\n";
            ////        sSQL += "GIABAN =" + clsMain.SQLString(dtgiaban.Rows[0]["GIABAN1"].ToString()) + "\n";
            ////        sSQL += "WHERE MA_HOADON =" + clsMain.SQLString(maHD) + "\n";
            ////        sSQL += "AND Isnull(Is_Combo,0)=" + clsMain.SQLBit(false) + "\n";
            ////        sSQL += "AND MA_HANGHOA =" + clsMain.SQLString(smaMon) + "\n";
            ////        sSQL += "AND GIABAN =" + clsMain.SQLString(gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["GIABAN"]).ToString()) + "\n";
            ////        clsMain.ExecuteSQL(sSQL);
            ////    }
            //    ExitDivideTable();
            //}

            //20/10 nút nỳ ko sài chuyên thành nút tặng -> giá =0
            if (funtions != 1)
                return;
            string sMaHoaDon = GetMaHoaDon(maBan);
            if (sMaHoaDon == "")
                return;
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }

            string sMaHangHoa = gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
            string sTenHangHoa = gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
            string sSLHangHoa = gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["SOLUONG"]).ToString();
            string sGiaHangHoa = gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["GIABAN"]).ToString();
            DataTable dt = new DataTable();
            dt.Columns.Add("MaHoaDon");
            dt.Columns.Add("MaHangHoa");
            dt.Columns.Add("TenHangHoa");
            dt.Columns.Add("SLHangHoa");
            dt.Columns.Add("GiaHangHoa");
            string sSQL = "";
            sSQL += "select MA_CTHOADON,MA_HANGHOA,MA_HOADON,SOLUONG,GIABAN from CT_HOADON where MA_HOADON=" + clsMain.SQLString(sMaHoaDon) + " and MA_HANGHOA=" + clsMain.SQLString(sMaHangHoa) + "\n";
            DataTable dtTemp = clsMain.ReturnDataTable(sSQL);
            foreach (DataRow dr in dtTemp.Rows)
            {
                sSLHangHoa = dr["SOLUONG"].ToString();
                sGiaHangHoa = dr["GIABAN"].ToString();

                if (sSLHangHoa.Contains("."))
                {
                    dt.Rows.Add(sMaHoaDon, sMaHangHoa, sTenHangHoa, decimal.Parse(sSLHangHoa), sGiaHangHoa);
                }
                else
                {
                    for (int i = 0; i < decimal.Parse(sSLHangHoa); i++)
                    {
                        dt.Rows.Add(sMaHoaDon, sMaHangHoa, sTenHangHoa, "1", sGiaHangHoa);
                    }
                }
            }

            bool bUpdataGia = false;
            Frm_SuaGia frm1 = new Frm_SuaGia();
            frm1.dtHanghoa = dt;
            frm1.bTang = true;
            frm1.ShowDialog();
            bUpdataGia = frm1.bCoSua;
            frm1.Dispose();
            if (bUpdataGia)
            {
                ExitDivideTable();
            }
        }

        private void btn_InlaiOrder_Click(object sender, EventArgs e)
        {
            if (funtions == 1)
            {
                // Check Table Opened or table paymented
                if (maBan == "")
                    return;
                if (gv_SelectProduct.RowCount <= 0)
                    return;
                string maHDB = GetMaHoaDon(maBan);
                if (maHDB == "")
                    return;

                string nameTable = clsMain.ReturnDataTable("Select TEN_BAN From BAN Where MA_BAN = " + clsMain.SQLString(maBan) + "").Rows[0][0].ToString();
                string solan = "";
                string sSQL = "";
                sSQL += "Select LANIN From CT_INLAI Where MA_HOADON=" + clsMain.SQLString(maHDB);
                DataTable dtinlai = clsMain.ReturnDataTable(sSQL);
                if (dtinlai.Rows.Count <= 0)
                {
                    sSQL = "";
                    sSQL += "INSERT INTO CT_INLAI(MA_HOADON,LANIN)" + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(maHDB) + ",";
                    sSQL += clsMain.SQLString(1.ToString()) + ")";
                    clsMain.ExecuteSQL(sSQL);
                    solan = "1";
                }
                else
                {
                    solan = (int.Parse(dtinlai.Rows[0]["LANIN"].ToString()) + 1).ToString();
                    sSQL = "";
                    sSQL += "UPDATE CT_INLAI SET" + "\n";
                    sSQL += "LANIN = " + clsMain.SQLString(solan) + "\n";
                    sSQL += "WHERE MA_HOADON=" + clsMain.SQLString(maHDB);
                    clsMain.ExecuteSQL(sSQL);
                }

                while (gv_SelectProduct.RowCount > 0)
                {
                    List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                    int j = 0;
                    string maBEP = "";
                    for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                    {
                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        maBEP = gv_SelectProduct.GetRowCellValue(0, gv_SelectProduct.Columns["MA_BEP"]).ToString();
                        dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                        if (maBEP == gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_BEP"]).ToString())
                        {
                            dmHangHoa.Ma_HoaDon = maHDB;
                            dmHangHoa.Ten_HoaDon = _Orderinlailan + " " + solan;
                            dmHangHoa.TEN_BAN = nameTable;
                            dmHangHoa.MaQuay = cls_ConfigCashier.nameCashier;
                            dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                            dmHangHoa.STT = (++j).ToString();
                            dmHangHoa.MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                            dmHangHoa.TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                            dmHangHoa.SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                            dmHangHoa.THUE = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["THUE"]).ToString());
                            dmHangHoa.GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                            dmHangHoa.IS_INBEP = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["IS_INBEP"]).ToString());
                            string repuest = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"]).ToString();
                            if (repuest == "a")
                                dmHangHoa.GHICHU = "";
                            else
                                dmHangHoa.GHICHU = repuest;

                            if (dmHangHoa.IS_INBEP)//nếu hàng hóa được in bếp mới cho in xuống bếp
                                listDM_HangHoa.Add(dmHangHoa);
                            // Delete product selected 
                            if (i != 0)
                            {
                                gv_SelectProduct.DeleteRow(i);
                                i--;
                            }
                        }
                    }
                    string print = GetIdKitchen(maBEP);
                    if (print == null)
                        XtraMessageBox.Show(_chuachonmayinchomhnay, _thongbao, MessageBoxButtons.OK);
                    gv_SelectProduct.DeleteRow(0);
                    // Printer Order 
                    convert cvrt = new convert();
                    DataTable Orderbill = cvrt.ConvertToDataTable(listDM_HangHoa);
                    if (Orderbill.Rows.Count > 0)
                    {
                        Frm_Report1 frm = new Frm_Report1();
                        if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                            frm.ReportName = "rpt_Order_58";
                        else
                        {
                            if (cls_KP_RES.Mode == 3)
                            {
                                frm.ReportName = "rpt_Order_1";
                            }
                            else
                            {
                                frm.ReportName = "rpt_Order";
                            }
                        }
                        frm.DataSource = Orderbill;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.PrinterName = print;
                        frm.IsPrint = true;
                        frm.ShowDialog();
                        frm.Dispose();
                    }
                }
                ExitDivideTable();
            }
        }

        private void btn_TamTinh_Click(object sender, EventArgs e)
        {
            try
            {
                btnComboOption.Visible = false;
                pnBtnDeleteGv.Visible = false;
                if (funtions != 1)
                    return;
                if (maBan == "")
                    return;
                if (gv_SelectProduct.RowCount <= 0)
                    return;

                //Lấy mã hóa don
                string idBill = GetMaHoaDon(maBan);
                if (idBill == "")
                    return;
                //Lấy tiền đặt cọc
                decimal Tiendatcoc = 0;
                DataTable dtMoney = clsMain.ReturnDataTable("Select ISNULL(TIENDATCOC,0) AS TIENDATCOC From HOADON Where MA_HOADON=" + clsMain.SQLString(idBill));
                if (dtMoney.Rows.Count > 0)
                {
                    Tiendatcoc = decimal.Parse(dtMoney.Rows[0]["TIENDATCOC"].ToString());
                }
                else
                {
                    Tiendatcoc = 0;
                }
                //Lấy danh sách hàng hóa
                DataTable _dt = null;
                _dt = gr_SelectProduct.DataSource as DataTable;
                //Gởi form thanh toán
                bool _check;
                clsKP_Terminal.ShowCustomerDisplayOneLine("Tổng cộng" + " : " + lbTotal.Text + " VND");
                Frm_Payment_Nhahang_Tamtinh frm = new Frm_Payment_Nhahang_Tamtinh(maBan, idBill, _dt, Tiendatcoc, tenBan);
                frm.ShowDialog();
                _check = frm.kt;
                frm.Dispose();
                // if payment successful
                if (_check == true)
                {
                    ExitDivideTable();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK);
            }
        }

        private void btn_Payment_Click(object sender, EventArgs e)
        {
            try
            {
                btnComboOption.Visible = false;
                pnBtnDeleteGv.Visible = false;
                if (funtions != 1)
                    return;
                if (maBan == "")
                    return;
                if (gv_SelectProduct.RowCount <= 0)
                    return;
                //Lấy mã hóa don
                string idBill = GetMaHoaDon(maBan);
                if (idBill == "")
                    return;
                //Lấy tiền đặt cọc
                decimal Tiendatcoc = 0;
                DataTable dtMoney = clsMain.ReturnDataTable("Select ISNULL(TIENDATCOC,0) AS TIENDATCOC From HOADON Where MA_HOADON=" + clsMain.SQLString(idBill));
                if (dtMoney.Rows.Count > 0)
                {
                    Tiendatcoc = decimal.Parse(dtMoney.Rows[0]["TIENDATCOC"].ToString());
                }
                else
                {
                    Tiendatcoc = 0;
                }
                //Lấy danh sách hàng hóa
                DataTable _dt = null;
                _dt = gr_SelectProduct.DataSource as DataTable;
                //Gởi form thanh toán
                bool _check;
                clsKP_Terminal.ShowCustomerDisplayOneLine("Tổng cộng" + " : " + lbTotal.Text + " VND");
                Frm_Payment_Nhahang frm = new Frm_Payment_Nhahang(maBan, idBill, _dt, Tiendatcoc, tenBan);
                frm.ShowDialog();
                _check = frm.bHoanTacThanhToan;
                frm.Dispose();
                // if payment successful
                if (_check == true)
                {
                    ExitDivideTable();
                    KP_Terminal.clsKP_Terminal.OpenCashDrawer(true);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK);
            }
        }

        private void btn_InLaiHoaDon_Click(object sender, EventArgs e)
        {
            pnBtnDeleteGv.Visible = false;
            btnComboOption.Visible = false;
            if (funtions != 1)
                return;

            if (exitReprint == true)
            {
                SetDefautlBackgroud(btn_InLaiHoaDon);
                ExitDivideTable();
            }
            else
            {
                if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
                {
                    bool check;
                    Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                    fr.ShowDialog();
                    check = fr.kt;
                    fr.Dispose();
                    if (check == false)
                        return;
                }

                string sSQL = "exec SP_DANHSACHHOADON";
                DataTable dts = clsMain.ReturnDataTable(sSQL);
                gr_billPaymented.DataSource = dts;
                gr_billPaymented.RefreshDataSource();

                SetBackgroudOnClick(btn_InLaiHoaDon);
                IN.Visible = true;
                HUY.Visible = false;
                LAYLAI.Visible = false;
                pnInlaihoadon.BringToFront();

                foreach (SimpleButton btn in pnNghiepVu.Controls)
                {
                    if (btn.Name != btn_InLaiHoaDon.Name)
                    {
                        btn.Visible = false;
                    }
                }
                exitReprint = true;
            }
        }

        private void btn_LayLaiHoaDon_Click(object sender, EventArgs e)
        {
            pnBtnDeleteGv.Visible = false;
            btnComboOption.Visible = false;
            if (funtions != 1)
                return;

            if (exitReprint == true)
            {
                SetDefautlBackgroud(btn_LayLaiHoaDon);
                ExitDivideTable();
            }
            else
            {
                if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
                {
                    bool check;
                    Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                    fr.ShowDialog();
                    check = fr.kt;
                    fr.Dispose();
                    if (check == false)
                        return;
                }

                string sSQL = "exec SP_DANHSACHHOADON";
                DataTable dts = clsMain.ReturnDataTable(sSQL);
                gr_billPaymented.DataSource = dts;
                gr_billPaymented.RefreshDataSource();

                SetBackgroudOnClick(btn_LayLaiHoaDon);
                LAYLAI.Visible = true;
                HUY.Visible = false;
                IN.Visible = false;
                pnInlaihoadon.BringToFront();
                foreach (SimpleButton btn in pnNghiepVu.Controls)
                {
                    if (btn.Name != btn_LayLaiHoaDon.Name)
                    {
                        btn.Visible = false;
                    }
                }
                exitReprint = true;
            }
        }

        private void btn_upbill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = gvBillPaymented.FocusedRowHandle - 1;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
                if (gvBillPaymented.FocusedRowHandle == 0)
                {
                    btn_up.Enabled = false;
                    btn_UpFirst.Enabled = false;
                }
            }
            catch
            {
            }

        }

        private void btn_UpTopBill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = 0;
                btn_UpFirst.Enabled = false;
                btn_up.Enabled = false;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnDowBill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = gvBillPaymented.FocusedRowHandle + 1;
                btn_up.Enabled = true;
                btn_UpFirst.Enabled = true;
                if (gvBillPaymented.FocusedRowHandle == gvBillPaymented.RowCount - 1)
                {
                    btn_Dow.Enabled = false;
                    btn_DowLast.Enabled = false;
                }
            }
            catch
            {

            }
        }

        private void btnDowLastBill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = gvBillPaymented.RowCount - 1;
                btn_UpFirst.Enabled = true;
                btn_up.Enabled = true;
                btn_Dow.Enabled = false;
                btn_DowLast.Enabled = false;
            }
            catch
            {
            }
        }

        private void gvBillPaymented_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == SSTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
                lbTongSoRow.Text = gvBillPaymented.RowCount.ToString("00");
                labelControl45.Text = (gvBillPaymented.FocusedRowHandle + 1).ToString("00");
            }
        }

        string idBill = "";
        private void gvBillPaymented_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            try
            {
                idBill = gvBillPaymented.GetRowCellValue(gvBillPaymented.FocusedRowHandle, gvBillPaymented.Columns["MA_HOADON"]).ToString(); ;
                GetDishOfTable(idBill);
                GetDistcountABill(idBill);

                string sSQL;
                if (e.Column == IN)
                {
                    sSQL = "";
                    sSQL += "Insert into HOADON_INLAI(MA_HOADON,NHANVIEN_INLAI,GIO_INLAI)" + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(idBill) + ",";
                    sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                    sSQL += "GETDATE())";
                    clsMain.ExecuteSQL(sSQL);

                    sSQL = "";
                    sSQL += "Select ROW_NUMBER() over (order by (select 1)) as STT,'' as TenNV ,ISNULL(cthd.CHIETKHAU,0) AS CHIETKHAU, ISNULL(cthd.THUE,0) AS THUE,cthd.MA_HANGHOA , hd.GHICHU,   " + "\n";
                    sSQL += "cthd.GIABAN, (cthd.SOLUONG * cthd.GIABAN) - (cthd.SOLUONG * cthd.GIABAN) * ISNULL(cthd.CHIETKHAU,0) / 100 as Thanh_Tien, hd.NGAYTAO, ISNULL(hd.GIORA,hd.NGAYTAO) as ngayHD,hh.TEN_HANGHOA, cthd.SOLUONG,";
                    sSQL += "hd.PHIPHUCVU as Phiphucvu,hd.MA_HOADON,TEN_QUAY as MA_QUAY, ISNULL(hd.TIENDATCOC,0) as TIENDATCOC,";
                    sSQL += "case when ISNULL(hd.NHANVIEN_RABILL,'')='' Then (hd.MANHANVIEN) Else (hd.NHANVIEN_RABILL) End as MANHANVIEN,";
                    if (cls_KP_RES.Mode == 4)
                    {
                        sSQL += "hd.GIAMGIA as Giam_Gia1, hd.TONGTIEN as Tong_Cong, hd.TIEN_KHACHTRA, hd.TIEN_TRAKHACH, hd.TONGTIEN, N'PHIẾU IN LẠI' as Ten_HoaDon" + "\n";
                    }
                    else
                    {
                        sSQL += "hd.GIAMGIA as Giam_Gia1, hd.TONGTIEN as Tong_Cong, hd.TIEN_KHACHTRA, hd.TIEN_TRAKHACH, hd.TONGTIEN, N'PHIẾU THANH TOÁN \n (IN LẠI)' as Ten_HoaDon" + "\n";
                        //sSQL += "hd.GIAMGIA as Giam_Gia1, hd.TONGTIEN as Tong_Cong, hd.TIEN_KHACHTRA, hd.TIEN_TRAKHACH, hd.TONGTIEN, N'PHIẾU THANH TOÁN' as Ten_HoaDon" + "\n";
                    }
                    sSQL += "From HANGHOA hh inner join CT_HOADON cthd on hh.MA_HANGHOA = cthd.MA_HANGHOA inner join HOADON hd on cthd.MA_HOADON = hd.MA_HOADON" + "\n";
                    sSQL += "inner join QUAY q on hd.MA_QUAY = q.MA_QUAY" + "\n";
                    sSQL += "Where hd.MA_HOADON ='" + idBill + "'";
                    DataTable dtReport = clsMain.ReturnDataTable(sSQL);
                    if (dtReport.Rows.Count > 0)
                    {
                        string sTenNV = "";
                        string sTenBan = "";
                        sSQL = "Select TENNHANVIEN From DM_NHANVIEN Where MANHANVIEN=" + clsMain.SQLString(dtReport.Rows[0]["MANHANVIEN"].ToString());
                        DataTable dtTenNV = clsMain.ReturnDataTable(sSQL);
                        if (dtTenNV.Rows.Count > 0)
                        {
                            sTenNV = dtTenNV.Rows[0]["TENNHANVIEN"].ToString();
                        }
                        sSQL = "Select TEN_BAN From CT_MOBAN ct,BAN b Where ct.MA_BAN=b.MA_BAN And ct.MA_HOADON=" + clsMain.SQLString(idBill);
                        DataTable dtTenban = clsMain.ReturnDataTable(sSQL);
                        if (dtTenban.Rows.Count > 0)
                        {
                            sTenBan = dtTenban.Rows[0]["TEN_BAN"].ToString();
                        }
                        dtReport.Columns.Add("Giam_Gia", typeof(String));
                        dtReport.Columns.Add("TEN_BAN", typeof(String));
                        for (int i = 0; i < dtReport.Rows.Count; i++)
                        {
                            dtReport.Rows[i]["TEN_BAN"] = sTenBan;
                            dtReport.Rows[i]["TenNV"] = sTenNV;
                            if (double.Parse(dtReport.Rows[i]["Giam_Gia1"].ToString()) > 100)
                                dtReport.Rows[i]["Giam_Gia"] = string.Format("{0:#,###0}", dtReport.Rows[i]["Giam_Gia1"]);
                            else
                                dtReport.Rows[i]["Giam_Gia"] = dtReport.Rows[i]["Giam_Gia1"].ToString() + "%";
                        }

                        Frm_Report1 frm = new Frm_Report1();
                        if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                        {
                            frm.ReportName = "rpt_inhoadon_Bill_ban_58";
                        }
                        else
                        {
                            if (cls_KP_RES.Mode == 3)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_ban_1";
                            }
                            else if (cls_KP_RES.Mode == 4)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_ban_pho";
                            }
                            else if (cls_KP_RES.Mode == 5)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_ban_2";
                            }
                            else if (cls_KP_RES.Mode == 9)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_ban_image";
                            }
                            else
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_ban";
                            }
                        }
                        frm.DataSource = dtReport;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                        frm.IsPrint = true;
                        frm.ShowDialog();
                        frm.Dispose();
                        dtLuoi.Rows.Clear();
                        lbTotal.Text = "0";
                        idBill = "";
                    }
                }
                else if (e.Column == LAYLAI)
                {
                    //Kiểm tra bàn có đang trống
                    DataTable dtTables = clsMain.ReturnDataTable("Select MA_BAN From CT_MOBAN where MA_HOADON =" + clsMain.SQLString(idBill));
                    if (dtTables.Rows.Count > 0)
                    {
                        DataTable dtCheckban;
                        for (int i = 0; i < dtTables.Rows.Count; i++)
                        {
                            dtCheckban = clsMain.ReturnDataTable("Select ISMO_BAN From BAN Where MA_BAN =" + clsMain.SQLString(dtTables.Rows[i]["MA_BAN"].ToString()));
                            if (dtCheckban.Rows.Count > 0)
                            {
                                if (bool.Parse(dtCheckban.Rows[0]["ISMO_BAN"].ToString()))
                                {
                                    XtraMessageBox.Show(_bandangmokhongthelaylaihd, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return;
                                }
                            }
                        }
                    }

                    //nhập lý do
                    Frm_GhiChu frm = new Frm_GhiChu(GhiChu);
                    frm.ShowDialog();
                    GhiChu = frm.sGhiChu;
                    frm.Dispose();
                    if (GhiChu.Trim() == "")
                    {
                        return;
                    }
                    sSQL = "";
                    sSQL += "EXEC SP_InsertDSLayLaiHD " + clsMain.SQLString(idBill) + "," + clsMain.SQLString(dtTables.Rows[0]["MA_BAN"].ToString()) + "," + clsMain.SQLString(clsGlobal.gsUserID) + "," + clsMain.SQLStringUnicode(GhiChu) + "";
                    clsMain.ExecuteSQL(sSQL);

                    //hóa đơn
                    sSQL = "";
                    sSQL = "Update HOADON set ISPAYMENT=0, GIAMGIA=" + 0 + ", TIEN_TRAKHACH =" + 0 + ", TIEN_KHACHTRA =" + 0 + ", TONGTIEN=" + 0 + "  where MA_HOADON=" + clsMain.SQLString(idBill);
                    clsMain.ExecuteSQL(sSQL);
                    //Bàn
                    if (dtTables.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtTables.Rows.Count; i++)
                        {
                            sSQL = "Update BAN SET ISMO_BAN=1 where MA_BAN =" + clsMain.SQLString(dtTables.Rows[i]["MA_BAN"].ToString());
                            clsMain.ExecuteSQL(sSQL);
                        }
                    }
                    //Kho
                    sSQL = string.Format("Exec SP_TRUKHO_DINHLUONG @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 0);
                    clsMain.ExecuteSQL(sSQL);
                    sSQL = string.Format("Exec SP_TRUKHO_HOADON @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 0);
                    clsMain.ExecuteSQL(sSQL);
                    // KHTT -TTNB
                    sSQL = "Select ISNULL(MA_KHACHHANG,'') AS MA_KHACHHANG,TONGTIEN,GIAMGIA From HOADON Where MA_HOADON =" + clsMain.SQLString(idBill);
                    DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                    if (dt1.Rows.Count > 0)
                    {
                        if (dt1.Rows[0]["MA_KHACHHANG"].ToString() != "")
                        {
                            DataTable dtKH = cls_KHTT.ReturnCauHinhKHTT();
                            if (dtKH.Rows[0]["GIATRI"].ToString() == "1")
                            {
                                int tem = 100 - int.Parse(dt1.Rows[0]["GIAMGIA"].ToString());
                                decimal tongtien = decimal.Parse(dt1.Rows[0]["TONGTIEN"].ToString()) / tem * 100;
                                cls_KHTT.TruDiemKhachHangThanThiet(dt1.Rows[0]["MA_KHACHHANG"].ToString(), tongtien);
                                cls_KHTT.TruTien_TTTNB(dt1.Rows[0]["MA_KHACHHANG"].ToString(), tongtien, "Lấy lại hóa đơn");
                            }
                            else
                            {
                                cls_KHTT.TruDiemKhachHangThanThiet(dt1.Rows[0]["MA_KHACHHANG"].ToString(), decimal.Parse(dt1.Rows[0]["TONGTIEN"].ToString()));
                                cls_KHTT.TruTien_TTTNB(dt1.Rows[0]["MA_KHACHHANG"].ToString(), decimal.Parse(dt1.Rows[0]["TONGTIEN"].ToString()), "Lấy lại hóa đơn");
                            }
                        }
                    }
                    //Công nợ
                    sSQL = "EXEC SP_CN_HUYHOADON '" + idBill + "'";
                    clsMain.ExecuteSQL(sSQL);

                    SetDefautlBackgroud(btn_LayLaiHoaDon);
                    ExitDivideTable();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK);
            }
        }

        private void GetDistcountABill(string maHoaDon)
        {
            string sql = " select TIENDATCOC, GIAMGIA, PHIPHUCVU from HOADON  where MA_HOADON ='" + maHoaDon + "'";
            var dtDistcount = clsMain.ReturnDataTable(sql);
            lbPhuThu.Text = string.Format("{0:#,###0}", double.Parse(dtDistcount.Rows[0]["PHIPHUCVU"].ToString()));
            lbGiamGia.Text = string.Format("{0:#,###0}", double.Parse(dtDistcount.Rows[0]["GIAMGIA"].ToString()));
        }

        int selectDishes = 0;
        int selectTable = 0;
        private void btn_DatBan_Click(object sender, EventArgs e)
        {
            if (funtions == 1)
            {
                funtions = 9;
                SetBackgroudOnClick(btn_DatBan);
                foreach (SimpleButton btn in pnNghiepVu.Controls)
                {
                    if (btn.Name == btn_DatBan.Name)
                    {
                        btn.Visible = true;
                    }
                    else
                    {
                        btn.Visible = false;
                    }
                }
                pnDatban.BringToFront();
                txtDBTenKH.Focus();
                us_keyboard2.SetTextbox(txtDBTenKH);
                txtngay.EditValue = clsGlobal.gdServerDate;
                cbHH.Text = "00";
                cbMM.Text = "00";
                selectTable = 1;
            }
            else if (funtions == 9)
            {
                //ExitDivideTable();
                selectDishes = 0;
                pnDatban.BringToFront();
                return;
            }
        }

        private void txtDBTenKH_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(txtDBTenKH);
        }

        private void txtAddress_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(txtAddress);
        }

        private void txtDienThoai_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(txtDienThoai);
        }

        private void txtEmail_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(txtEmail);
        }

        private void cbHH_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(cbHH);
        }

        private void cbMM_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(cbMM);
        }

        private void txtPartMoneyPayments_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(txtPartMoneyPayments);
        }

        private void txtngay_TextChanged(object sender, EventArgs e)
        {
            flCheckTable.Controls.Clear();
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
            {
                pnShowHang.BringToFront();
                txtDBTenKH.Text = "";
                txtDienThoai.Text = "";
                txtAddress.Text = "";
                txtPartMoneyPayments.Text = "";
                flCheckTable.Controls.Clear();
                KhoamoControl_Chuyenban(false);
                flagdatban = false;
                ExitDivideTable();
            }
        }

        private void btn_SelectTable_Click(object sender, EventArgs e)
        {
            if (txtngay.Text == "" || cbHH.Text == "")
                return;
            DateTime now = DateTime.Parse(DateTime.Now.ToShortDateString());
            DateTime x = DateTime.Parse(((DateTime)txtngay.EditValue).ToShortDateString());
            int outline = DateTime.Compare(x, now);
            if (outline < 0)
            {
                XtraMessageBox.Show(_ngaydbkhonghople, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                if (int.Parse(cbHH.Text) < int.Parse(DateTime.Now.Hour.ToString()) && outline == 0)
                {
                    XtraMessageBox.Show(_chuachontgden, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            funtions = 1;
            if (funtions == 1)
            {
                pnShowHang.BringToFront();
                btn_SoDoBan.Text = _sdb;
                GetKhuVuc();
                GetSoDoBan();
                foreach (SimpleButton btn in pnKV_Mon.Controls)
                {
                    if (btn.Name == maKV.ToString())
                    {
                        SetBackgroudOnClick(btn);
                    }
                }
                selectDishes = 2;
                funtions = 9;
            }

            //Frm_Danhsachban frm = new Frm_Danhsachban();
            //frm.selectDishes = 2;
            //frm.funtions = 9;
            //frm.txtngay = x;
            //frm.ShowDialog();
            //frm.Dispose();
        }

        private void btn_SelectDishes_Click(object sender, EventArgs e)
        {
            funtions = 2;
            pnShowHang.BringToFront();
            btn_SoDoBan.Text = _mb;
            GetNhomhang();
            GetListMon();
            foreach (SimpleButton btn in pnKV_Mon.Controls)
            {
                if (btn.Name == maNH.ToString())
                {
                    SetBackgroudOnClick(btn);
                }
            }
            funtions = 9;
            selectDishes = 1;
        }

        string idCheckTable = "";
        private void btnRemove_Click(object sender, EventArgs e)
        {
            foreach (SimpleButton btnBan in flCheckTable.Controls)
            {
                if (btnBan.Name == idCheckTable)//ActiveControl.Name
                {
                    flCheckTable.Controls.Remove(btnBan);
                    idCheckTable = "";
                }
            }
        }

        private void btnSendEmail_Click(object sender, EventArgs e)
        {

        }

        private void btnMoBanNgay_Click(object sender, EventArgs e)
        {
            if (_flag_chuyenban)
            {
                string sSQL = "";
                sSQL += "Delete CT_MOBAN Where MA_HOADON=" + clsMain.SQLString(_mahoadon_chuyenban);
                clsMain.ExecuteSQL(sSQL);
                foreach (SimpleButton btn in flCheckTable.Controls)
                {
                    string[] arr = btn.Text.Split('-');
                    sSQL = "";
                    sSQL += "Insert into CT_MOBAN(MA_HOADON, MA_BAN)" + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(_mahoadon_chuyenban) + ",";
                    sSQL += clsMain.SQLString(btn.Name) + ")";
                    clsMain.ExecuteSQL(sSQL);

                    sSQL = "";
                    sSQL += "Update BAN Set" + "\n";
                    sSQL += "ISMO_BAN=1" + "\n";
                    sSQL += "Where MA_BAN=" + clsMain.SQLString(btn.Name) + "\n";
                    clsMain.ExecuteSQL(sSQL);
                }
                //Reload Table
                funtions = 1;
                GetSoDoBan();
                funtions = 9;

                // Dispes funtion 9 Check Table
                if (funtions != 1)
                {
                    funtions = 1;
                    btn_SoDoBan.Text = _mb;
                    pnShowHang.BringToFront();
                    GetKhuVuc();
                    GetSoDoBan();
                    SetDefaultBackgroundBtnInPn(pnThuNgan);
                    SetDefaultBackgroundBtnInPn(pnNghiepVu);
                    foreach (SimpleButton btn in pnKV_Mon.Controls)
                    {
                        if (btn.Name == maKV.ToString())
                        {
                            SetBackgroudOnClick(btn);
                        }
                    }
                    foreach (SimpleButton btnb in pnTable_Mon.Controls)
                    {
                        if (btnb.Name == maBan.ToString())
                        {
                            SetBackgroudOnClick(btnb);
                        }
                    }
                    KhoamoControl_Chuyenban(false);
                    dtLuoi.Rows.Clear();
                    gr_SelectProduct.RefreshDataSource();
                    txtDBTenKH.Text = "";
                    txtDienThoai.Text = "";
                    txtAddress.Text = "";
                    txtPartMoneyPayments.Text = "";
                    flCheckTable.Controls.Clear();
                    flagdatban = false;
                }
            }
            else
            {
                int dem = 0;
                foreach (SimpleButton btn in flCheckTable.Controls)
                {
                    dem++;
                }
                if (funtions != 9 || txtDBTenKH.Text == "" || dem == 0 || txtngay.Text == "")
                {
                    XtraMessageBox.Show(_banphainhapdayduthongtin, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (txtDienThoai.Text == "" || txtAddress.Text == "")
                {
                    XtraMessageBox.Show(_chuanhapttxacnhan, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (chkEmail.Checked)
                {
                    if (!cls_KP_RES.isValidEmail(txtEmail.Text.Trim()))
                    {
                        XtraMessageBox.Show(_emailkhople, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                }
                else
                {
                    if (!cls_KP_RES.isValidEmail(txtEmail.Text.Trim()) && txtEmail.Text != "")
                    {
                        XtraMessageBox.Show(_emailkhople, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                string date = string.Format("{0:dd-MM-yyyy}", txtngay.EditValue);
                string time = cbHH.Text + ":" + cbMM.Text + ":00";
                string ngayDat = date + " " + time;
                //Creater id Bill
                if (txtPartMoneyPayments.Text == "")
                    txtPartMoneyPayments.Text = "0";
                int n;
                bool isNumeric = int.TryParse(txtPartMoneyPayments.Text.Replace(",", String.Empty), out n);
                if (isNumeric == false)
                {
                    XtraMessageBox.Show(_tiendatcoclakieuso, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                string maHoaDon = GetNewCodeBill();
                string sSQL = "";
                //update Check Table           
                //Update infor Table checked
                if (maHoaDon != null)
                {
                    DataTable KH;
                    KH = clsMain.ReturnDataTable("Select MA From NHACUNGCAP Where DIENTHOAI =" + clsMain.SQLString(txtDienThoai.Text) + " And LOAI = 0");
                    if (KH.Rows.Count <= 0)
                    {
                        sSQL += "Insert into NHACUNGCAP(TEN, DIACHI, EMAIL, DIENTHOAI)" + "\n";
                        sSQL += "values(";
                        sSQL += clsMain.SQLStringUnicode(txtDBTenKH.Text) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtAddress.Text) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtEmail.Text) + ",";
                        sSQL += clsMain.SQLString(txtDienThoai.Text) + ")";
                        clsMain.ExecuteSQL(sSQL);
                        KH = clsMain.ReturnDataTable("Select MA From NHACUNGCAP Where DIENTHOAI =" + clsMain.SQLString(txtDienThoai.Text) + "  And LOAI = 0");
                    }
                    else
                    {
                        sSQL += "UPDATE NHACUNGCAP SET " + "\n";
                        sSQL += "TEN=" + clsMain.SQLStringUnicode(txtDBTenKH.Text) + "," + "\n";
                        sSQL += "EMAIL=" + clsMain.SQLStringUnicode(txtEmail.Text) + "," + "\n";
                        sSQL += "DIACHI=" + clsMain.SQLStringUnicode(txtAddress.Text) + "\n";
                        sSQL += "Where MA=" + clsMain.SQLString(KH.Rows[0]["MA"].ToString());
                        clsMain.ExecuteSQL(sSQL);
                    }

                    sSQL = "";
                    sSQL += "Insert into HOADON(MA_HOADON, MANHANVIEN,NGAYTAO, NGAYDAT, TIENDATCOC, GIOVAO, SODIENTHOAI, MA_KHACHHANG, ISBANDAT) ";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(maHoaDon) + ",";
                    sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                    sSQL += "GETDATE()" + ",";
                    sSQL += clsMain.SQLString(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")) + ",";
                    sSQL += clsMain.SQLString(txtPartMoneyPayments.Text.Replace(",", "")) + ",";
                    sSQL += clsMain.SQLString(ngayDat) + ",";
                    sSQL += clsMain.SQLString(txtDienThoai.Text) + ",";
                    sSQL += clsMain.SQLString(KH.Rows[0]["MA"].ToString()) + ",";
                    sSQL += 1 + ")";
                    clsMain.ExecuteSQL(sSQL);
                    string nameTable = "";
                    foreach (SimpleButton btn in flCheckTable.Controls)
                    {
                        string[] arr = btn.Text.Split('-');
                        nameTable += arr[0] + " ";
                        sSQL = "";
                        sSQL += "Insert into CT_MOBAN(MA_HOADON, MA_BAN)" + "\n";
                        sSQL += "Values(";
                        sSQL += clsMain.SQLString(maHoaDon) + ",";
                        sSQL += clsMain.SQLString(btn.Name) + ")";
                        clsMain.ExecuteSQL(sSQL);

                        sSQL = "";
                        sSQL += "Update BAN Set" + "\n";
                        sSQL += "ISMO_BAN=1" + "\n";
                        sSQL += "Where MA_BAN=" + clsMain.SQLString(btn.Name) + "\n";
                        clsMain.ExecuteSQL(sSQL);
                    }
                    string NameDistrict = "";
                    DataTable dtKVV = clsMain.ReturnDataTable("Select TEN_KHUVUC From KHUVUC Where MA_KHUVUC=" + clsMain.SQLString(maKV.ToString()));
                    if (dtKVV.Rows.Count > 0)
                        NameDistrict = dtKVV.Rows[0]["TEN_KHUVUC"].ToString();

                    List<DM_HangHoa> listHH = new List<DM_HangHoa>();
                    int j = 1;
                    if (gv_SelectProduct.RowCount > 0)
                    {
                        for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                        {
                            DM_HangHoa dmHangHoa = new DM_HangHoa();
                            dmHangHoa.Ten_HoaDon = "Phiếu xác nhận đặt Bàn";
                            dmHangHoa.STT = (j++).ToString();
                            dmHangHoa.Ma_HoaDon = maHoaDon;
                            dmHangHoa.TenKhachHang = txtDBTenKH.Text;
                            dmHangHoa.DienThoai = txtDienThoai.Text;
                            dmHangHoa.GIOVAO = ngayDat;
                            dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", DateTime.Now);
                            dmHangHoa.MaBan = nameTable;
                            dmHangHoa.TienDatCoc = decimal.Parse(txtPartMoneyPayments.Text);
                            dmHangHoa.KhuVuc = NameDistrict;
                            dmHangHoa.MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                            dmHangHoa.TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                            dmHangHoa.SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                            dmHangHoa.GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                            dmHangHoa.Thanh_Tien = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"]).ToString());

                            dmHangHoa.Ma_Quay = "QCO";
                            dmHangHoa.TenNV = clsGlobal.gsNameOfUserLogin;
                            listHH.Add(dmHangHoa);

                            sSQL = "";
                            sSQL += "Insert Into CT_HOADON(MA_HOADON, MA_HANGHOA,SOLUONG,GIABAN_THAT,GIABAN)" + "\n";
                            sSQL += "values(";
                            sSQL += clsMain.SQLString(maHoaDon) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ")";
                            clsMain.ExecuteSQL(sSQL);
                        }
                    }
                    else
                    {

                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        dmHangHoa.Ten_HoaDon = "Phiếu xác nhận đặt Bàn";
                        dmHangHoa.Ma_HoaDon = maHoaDon;
                        dmHangHoa.TenKhachHang = txtDBTenKH.Text;
                        dmHangHoa.DienThoai = txtDienThoai.Text;
                        dmHangHoa.GIOVAO = ngayDat;
                        dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", DateTime.Now);
                        dmHangHoa.MaBan = nameTable;
                        dmHangHoa.TienDatCoc = decimal.Parse(txtPartMoneyPayments.Text);
                        dmHangHoa.KhuVuc = NameDistrict;
                        dmHangHoa.Ma_Quay = "QCO";
                        dmHangHoa.TenNV = clsGlobal.gsNameOfUserLogin;
                        listHH.Add(dmHangHoa);
                    }
                    //


                    convert cvrt = new convert();
                    DataTable TableCheck = cvrt.ConvertToDataTable(listHH);

                    #region Gửi Email cho khách hàng
                    if (chkEmail.Checked)
                    {
                        int dangfile = 0;
                        DataTable dtTG = new DataTable();

                        string sql = "select TAPTIN,EMAIL_GUI,MATKHAU,CHUDE,NOIDUNG,HOST,PORT,MA_CUAHANG from CHITIETMAIL";
                        dtTG = clsMain.ReturnDataTable(sql);
                        if (dtTG.Rows.Count > 0)
                        {
                            if (dtTG.Rows[0]["TAPTIN"].ToString() == "E")
                                dangfile = 0;
                            else
                                dangfile = 1000;
                        }

                        //kiểm tra nếu thư mục "Report" chưa tồn tại thì tạo mới
                        string directoryPath = @"C:\Report";
                        if (!System.IO.Directory.Exists(directoryPath))
                        {
                            System.IO.Directory.CreateDirectory(directoryPath);
                            DirectoryInfo theFolder = new DirectoryInfo(@"C:\Report");
                            theFolder.Attributes = FileAttributes.Hidden | FileAttributes.System;   //set thư mục Report ẩn
                        }

                        Frm_Report1 fm = new Frm_Report1();
                        fm.ReportName = "rpt_BillDatBan";
                        fm.DataSource = TableCheck;
                        fm.IsSave = true;
                        fm.Fl = dangfile;
                        fm.FileName = "BillDatBan" + maHoaDon;
                        fm.Show();

                        MailMessage msg = new MailMessage();
                        msg.From = new MailAddress(dtTG.Rows[0]["EMAIL_GUI"].ToString());
                        msg.To.Add(txtEmail.Text.Trim());
                        msg.Subject = dtTG.Rows[0]["CHUDE"].ToString();         // chủ đề email
                        msg.Body = dtTG.Rows[0]["NOIDUNG"].ToString();          // nội dung của email
                        msg.Priority = MailPriority.High;                       // email ưu tiên
                        msg.IsBodyHtml = true;

                        string bdb = "";
                        if (dangfile == 0)
                        {
                            bdb = @"C:\Report\BillDatBan" + maHoaDon + ".xlsx";
                        }
                        else
                        {
                            bdb = @"C:\Report\BillDatBan" + maHoaDon + ".pdf";
                        }
                        Attachment attbcbh = new Attachment(bdb);
                        msg.Attachments.Add(attbcbh);

                        SmtpClient client = new SmtpClient();
                        client.Host = dtTG.Rows[0]["HOST"].ToString();
                        client.Port = int.Parse(dtTG.Rows[0]["PORT"].ToString());   //cổng 587
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.UseDefaultCredentials = false;
                        client.Credentials = new System.Net.NetworkCredential(dtTG.Rows[0]["EMAIL_GUI"].ToString(), dtTG.Rows[0]["MATKHAU"].ToString());
                        client.EnableSsl = true;
                        client.Send(msg);
                    }
                    #endregion

                    KP_Report.Frm_Report1 frm = new KP_Report.Frm_Report1();
                    if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                        frm.ReportName = "rpt_BillDatBan_58";
                    else
                        frm.ReportName = "rpt_BillDatBan";
                    frm.DataSource = TableCheck;
                    frm.WindowState = FormWindowState.Minimized;
                    frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                    frm.IsPrint = true;
                    frm.ShowDialog();
                    frm.Dispose();


                    XtraMessageBox.Show(_xacnhandatbanthanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CheckedIsTable();

                    //Reload Table
                    funtions = 1;
                    GetSoDoBan();
                    funtions = 9;
                }
                // Dispes funtion 9 Check Table
                if (funtions != 1)
                {
                    funtions = 1;
                    btn_SoDoBan.Text = _mb;
                    pnShowHang.BringToFront();
                    GetKhuVuc();
                    GetSoDoBan();
                    SetDefaultBackgroundBtnInPn(pnThuNgan);
                    SetDefaultBackgroundBtnInPn(pnNghiepVu);
                    foreach (SimpleButton btn in pnKV_Mon.Controls)
                    {
                        if (btn.Name == maKV.ToString())
                        {
                            SetBackgroudOnClick(btn);
                        }
                    }
                    foreach (SimpleButton btnb in pnTable_Mon.Controls)
                    {
                        if (btnb.Name == maBan.ToString())
                        {
                            SetBackgroudOnClick(btnb);
                        }
                    }

                    dtLuoi.Rows.Clear();
                    gr_SelectProduct.RefreshDataSource();
                    txtDBTenKH.Text = "";
                    txtDienThoai.Text = "";
                    txtAddress.Text = "";
                    txtPartMoneyPayments.Text = "";
                    flCheckTable.Controls.Clear();
                }
            }
            ExitDivideTable();
        }

        private void btnMobansau_Click(object sender, EventArgs e)
        {
            if (_flag_chuyenban)
            {
                string sSQL = "";
                sSQL += "Delete CT_MOBAN Where MA_HOADON=" + clsMain.SQLString(_mahoadon_chuyenban);
                clsMain.ExecuteSQL(sSQL);
                foreach (SimpleButton btn in flCheckTable.Controls)
                {
                    string[] arr = btn.Text.Split('-');
                    sSQL = "";
                    sSQL += "Insert into CT_MOBAN(MA_HOADON, MA_BAN)" + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(_mahoadon_chuyenban) + ",";
                    sSQL += clsMain.SQLString(btn.Name) + ")";
                    clsMain.ExecuteSQL(sSQL);
                }
                //Reload Table
                funtions = 1;
                GetSoDoBan();
                funtions = 9;

                // Dispes funtion 9 Check Table
                if (funtions != 1)
                {
                    funtions = 1;
                    btn_SoDoBan.Text = _mb;
                    pnShowHang.BringToFront();
                    GetKhuVuc();
                    GetSoDoBan();
                    SetDefaultBackgroundBtnInPn(pnThuNgan);
                    SetDefaultBackgroundBtnInPn(pnNghiepVu);
                    foreach (SimpleButton btn in pnKV_Mon.Controls)
                    {
                        if (btn.Name == maKV.ToString())
                        {
                            SetBackgroudOnClick(btn);
                        }
                    }
                    foreach (SimpleButton btnb in pnTable_Mon.Controls)
                    {
                        if (btnb.Name == maBan.ToString())
                        {
                            SetBackgroudOnClick(btnb);
                        }
                    }
                    KhoamoControl_Chuyenban(false);
                    dtLuoi.Rows.Clear();
                    gr_SelectProduct.RefreshDataSource();
                    txtDBTenKH.Text = "";
                    txtDienThoai.Text = "";
                    txtAddress.Text = "";
                    txtPartMoneyPayments.Text = "";
                    flCheckTable.Controls.Clear();
                    flagdatban = false;
                }
            }
            else
            {
                string sSQL = "";
                int dem = 0;
                foreach (SimpleButton btn in flCheckTable.Controls)
                {
                    dem++;
                }
                if (funtions != 9 || txtDBTenKH.Text == "" || dem == 0 || txtngay.Text == "")
                {
                    XtraMessageBox.Show(_banphainhapdayduthongtin, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (txtDienThoai.Text == "" || txtAddress.Text == "")
                {
                    XtraMessageBox.Show(_chuanhapttxacnhan, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (chkEmail.Checked)
                {
                    if (!cls_KP_RES.isValidEmail(txtEmail.Text.Trim()))
                    {
                        XtraMessageBox.Show(_emailkhople, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                else
                {
                    if (!cls_KP_RES.isValidEmail(txtEmail.Text.Trim()) && txtEmail.Text != "")
                    {
                        XtraMessageBox.Show(_emailkhople, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                string date = string.Format("{0:dd-MM-yyyy}", txtngay.EditValue);
                string time = cbHH.Text + ":" + cbMM.Text + ":00";
                string ngayDat = date + " " + time;
                //Creater id Bill
                if (txtPartMoneyPayments.Text == "")
                    txtPartMoneyPayments.Text = "0";
                int n;
                bool isNumeric = int.TryParse(txtPartMoneyPayments.Text.Replace(",", String.Empty), out n);
                if (isNumeric == false)
                {
                    XtraMessageBox.Show(_tiendatcoclakieuso, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                string maHoaDon = GetNewCodeBill();
                //update Check Table           
                //Update infor Table checked
                if (maHoaDon != null)
                {
                    DataTable KH;
                    KH = clsMain.ReturnDataTable("Select MA From NHACUNGCAP Where DIENTHOAI =" + clsMain.SQLString(txtDienThoai.Text) + " And LOAI = 0");
                    if (KH.Rows.Count <= 0)
                    {
                        sSQL += "Insert into NHACUNGCAP(TEN, DIACHI,EMAIL, DIENTHOAI)" + "\n";
                        sSQL += "values(";
                        sSQL += clsMain.SQLStringUnicode(txtDBTenKH.Text) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtAddress.Text) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtEmail.Text) + ",";
                        sSQL += clsMain.SQLString(txtDienThoai.Text) + ")";
                        clsMain.ExecuteSQL(sSQL);
                        KH = clsMain.ReturnDataTable("Select MA From NHACUNGCAP Where DIENTHOAI =" + clsMain.SQLString(txtDienThoai.Text) + "  And LOAI = 0");
                    }
                    else
                    {
                        sSQL += "UPDATE NHACUNGCAP SET " + "\n";
                        sSQL += "TEN=" + clsMain.SQLStringUnicode(txtDBTenKH.Text) + "," + "\n";
                        sSQL += "DIACHI=" + clsMain.SQLStringUnicode(txtAddress.Text) + "," + "\n";
                        sSQL += "EMAIL=" + clsMain.SQLStringUnicode(txtEmail.Text) + "\n";
                        sSQL += "Where MA=" + clsMain.SQLString(KH.Rows[0]["MA"].ToString());
                        clsMain.ExecuteSQL(sSQL);
                    }

                    sSQL = "";
                    sSQL += "Insert into HOADON(MA_HOADON, MANHANVIEN,NGAYTAO, NGAYDAT, TIENDATCOC, GIOVAO, SODIENTHOAI, MA_KHACHHANG, ISBANDAT) ";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(maHoaDon) + ",";
                    sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                    sSQL += "GETDATE()" + ",";
                    sSQL += clsMain.SQLString(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")) + ",";
                    sSQL += float.Parse(txtPartMoneyPayments.Text.Replace(",", "")) + ",";
                    sSQL += clsMain.SQLString(ngayDat) + ",";
                    sSQL += clsMain.SQLString(txtDienThoai.Text) + ",";
                    sSQL += clsMain.SQLString(KH.Rows[0]["MA"].ToString()) + ",";
                    sSQL += 1 + ")";
                    clsMain.ExecuteSQL(sSQL);
                    string nameTable = "";
                    foreach (SimpleButton btn in flCheckTable.Controls)
                    {
                        string[] arr = btn.Text.Split('-');
                        nameTable += arr[0] + " ";
                        sSQL = "";
                        sSQL += "Insert into CT_MOBAN(MA_HOADON, MA_BAN)" + "\n";
                        sSQL += "Values(";
                        sSQL += clsMain.SQLString(maHoaDon) + ",";
                        sSQL += clsMain.SQLString(btn.Name) + ")";
                        clsMain.ExecuteSQL(sSQL);
                    }
                    string NameDistrict = "";
                    DataTable dtKVV = clsMain.ReturnDataTable("Select TEN_KHUVUC From KHUVUC Where MA_KHUVUC=" + maKV + "");
                    if (dtKVV.Rows.Count > 0)
                        NameDistrict = dtKVV.Rows[0]["TEN_KHUVUC"].ToString();

                    List<DM_HangHoa> listHH = new List<DM_HangHoa>();
                    int j = 1;
                    if (gv_SelectProduct.RowCount > 0)
                    {
                        for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                        {
                            DM_HangHoa dmHangHoa = new DM_HangHoa();
                            dmHangHoa.Ten_HoaDon = "Phiếu xác nhận đặt Bàn";
                            dmHangHoa.STT = (j++).ToString();
                            dmHangHoa.Ma_HoaDon = maHoaDon;
                            dmHangHoa.TenKhachHang = txtDBTenKH.Text;
                            dmHangHoa.DienThoai = txtDienThoai.Text;
                            dmHangHoa.GIOVAO = ngayDat;
                            dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", DateTime.Now);
                            dmHangHoa.MaBan = nameTable;
                            dmHangHoa.TienDatCoc = decimal.Parse(txtPartMoneyPayments.Text);
                            dmHangHoa.KhuVuc = NameDistrict;
                            dmHangHoa.MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                            dmHangHoa.TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                            dmHangHoa.SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                            dmHangHoa.GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                            dmHangHoa.Thanh_Tien = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"]).ToString());
                            dmHangHoa.Ma_Quay = "QCO";
                            dmHangHoa.TenNV = clsGlobal.gsNameOfUserLogin;
                            listHH.Add(dmHangHoa);

                            sSQL = "";
                            sSQL += "Insert Into CT_HOADON(MA_HOADON, MA_HANGHOA,SOLUONG,GIABAN_THAT,GIABAN)" + "\n";
                            sSQL += "values(";
                            sSQL += clsMain.SQLString(maHoaDon) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ")";
                            clsMain.ExecuteSQL(sSQL);
                        }
                    }
                    else
                    {

                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        dmHangHoa.Ten_HoaDon = "Phiếu xác nhận đặt Bàn";
                        dmHangHoa.Ma_HoaDon = maHoaDon;
                        dmHangHoa.TenKhachHang = txtDBTenKH.Text;
                        dmHangHoa.DienThoai = txtDienThoai.Text;
                        dmHangHoa.GIOVAO = ngayDat;
                        dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", DateTime.Now);
                        dmHangHoa.MaBan = nameTable;
                        dmHangHoa.TienDatCoc = decimal.Parse(txtPartMoneyPayments.Text);
                        dmHangHoa.KhuVuc = NameDistrict;
                        dmHangHoa.Ma_Quay = "QCO";
                        dmHangHoa.TenNV = clsGlobal.gsNameOfUserLogin;
                        listHH.Add(dmHangHoa);
                    }
                    //
                    convert cvrt = new convert();
                    DataTable TableCheck = cvrt.ConvertToDataTable(listHH);
                    #region Gửi Email cho khách hàng
                    if (chkEmail.Checked)
                    {
                        int dangfile = 0;
                        DataTable dtTG = new DataTable();

                        string sql = "select TAPTIN,EMAIL_GUI,MATKHAU,CHUDE,NOIDUNG,HOST,PORT,MA_CUAHANG from CHITIETMAIL";
                        dtTG = clsMain.ReturnDataTable(sql);
                        if (dtTG.Rows.Count > 0)
                        {
                            if (dtTG.Rows[0]["TAPTIN"].ToString() == "E")
                                dangfile = 0;
                            else
                                dangfile = 1000;
                        }

                        //kiểm tra nếu thư mục "Report" chưa tồn tại thì tạo mới
                        string directoryPath = @"C:\Report";
                        if (!System.IO.Directory.Exists(directoryPath))
                        {
                            System.IO.Directory.CreateDirectory(directoryPath);
                            DirectoryInfo theFolder = new DirectoryInfo(@"C:\Report");
                            theFolder.Attributes = FileAttributes.Hidden | FileAttributes.System;   //set thư mục Report ẩn
                        }

                        KP_Report.Frm_Report1 fm = new KP_Report.Frm_Report1();
                        fm.ReportName = "rpt_BillDatBan";
                        fm.DataSource = TableCheck;
                        fm.IsSave = true;
                        fm.Fl = dangfile;
                        fm.FileName = "BillDatBan" + maHoaDon;
                        fm.Show();

                        MailMessage msg = new MailMessage();
                        msg.From = new MailAddress(dtTG.Rows[0]["EMAIL_GUI"].ToString());
                        msg.To.Add(txtEmail.Text.Trim());
                        msg.Subject = dtTG.Rows[0]["CHUDE"].ToString();         // chủ đề email
                        msg.Body = dtTG.Rows[0]["NOIDUNG"].ToString();          // nội dung của email
                        msg.Priority = MailPriority.High;                       // email ưu tiên
                        msg.IsBodyHtml = true;

                        string bdb = "";
                        if (dangfile == 0)
                        {
                            bdb = @"C:\Report\BillDatBan" + maHoaDon + ".xlsx";
                        }
                        else
                        {
                            bdb = @"C:\Report\BillDatBan" + maHoaDon + ".pdf";
                        }
                        Attachment attbcbh = new Attachment(bdb);
                        msg.Attachments.Add(attbcbh);

                        SmtpClient client = new SmtpClient();
                        client.Host = dtTG.Rows[0]["HOST"].ToString();
                        client.Port = int.Parse(dtTG.Rows[0]["PORT"].ToString());   //cổng 587
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.UseDefaultCredentials = false;
                        client.Credentials = new System.Net.NetworkCredential(dtTG.Rows[0]["EMAIL_GUI"].ToString(), dtTG.Rows[0]["MATKHAU"].ToString());
                        client.EnableSsl = true;
                        client.Send(msg);
                    }
                    #endregion
                    KP_Report.Frm_Report1 frm = new KP_Report.Frm_Report1();
                    if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                        frm.ReportName = "rpt_BillDatBan_58";
                    else
                        frm.ReportName = "rpt_BillDatBan";
                    frm.DataSource = TableCheck;
                    frm.WindowState = FormWindowState.Minimized;
                    frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                    frm.IsPrint = true;
                    frm.ShowDialog();
                    frm.Dispose();
                    XtraMessageBox.Show(_xacnhandatbanthanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CheckedIsTable();

                    //Reload Table
                    funtions = 1;
                    GetSoDoBan();
                    funtions = 9;
                }
                // Dispes funtion 9 Check Table
                if (funtions != 1)
                {
                    funtions = 1;
                    btn_SoDoBan.Text = _mb;
                    pnShowHang.BringToFront();
                    GetKhuVuc();
                    GetSoDoBan();
                    SetDefaultBackgroundBtnInPn(pnThuNgan);
                    SetDefaultBackgroundBtnInPn(pnNghiepVu);
                    foreach (SimpleButton btn in pnKV_Mon.Controls)
                    {
                        if (btn.Name == maKV.ToString())
                        {
                            SetBackgroudOnClick(btn);
                        }
                    }
                    foreach (SimpleButton btnb in pnTable_Mon.Controls)
                    {
                        if (btnb.Name == maBan.ToString())
                        {
                            SetBackgroudOnClick(btnb);
                        }
                    }

                    dtLuoi.Rows.Clear();
                    gr_SelectProduct.RefreshDataSource();
                    txtDBTenKH.Text = "";
                    txtDienThoai.Text = "";
                    txtAddress.Text = "";
                    txtPartMoneyPayments.Text = "";
                    flCheckTable.Controls.Clear();
                }
            }
            ExitDivideTable();
        }

        private void btn_Danhsachdatban_Click(object sender, EventArgs e)
        {
            Frm_Danhsachdatban frm = new Frm_Danhsachdatban();
            frm.ShowDialog();
            _flag_chuyenban = Frm_Danhsachdatban.flag_Chuyenban;
            _mahoadon_chuyenban = Frm_Danhsachdatban.mahoadon;
            if (_flag_chuyenban)
            {
                string sSQL = "";
                sSQL += "Select TEN,hd.SODIENTHOAI,DIACHI,TIENDATCOC,NGAYDAT,GIOVAO" + "\n";
                sSQL += "From HOADON hd,NHACUNGCAP n" + "\n";
                sSQL += "Where hd.MA_KHACHHANG=n.MA" + "\n";
                sSQL += "And hd.MA_HOADON=" + clsMain.SQLString(_mahoadon_chuyenban);
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                if (dt.Rows.Count > 0)
                {
                    txtDBTenKH.Text = dt.Rows[0]["TEN"].ToString();
                    txtDienThoai.Text = dt.Rows[0]["SODIENTHOAI"].ToString();
                    txtAddress.Text = dt.Rows[0]["DIACHI"].ToString();
                    txtPartMoneyPayments.EditValue = dt.Rows[0]["TIENDATCOC"].ToString();
                    txtngay.EditValue = DateTime.Parse(dt.Rows[0]["NGAYDAT"].ToString());
                    cbHH.Text = DateTime.Parse(dt.Rows[0]["GIOVAO"].ToString()).Hour.ToString().Length < 2 ? "0" + DateTime.Parse(dt.Rows[0]["GIOVAO"].ToString()).Hour.ToString() : DateTime.Parse(dt.Rows[0]["GIOVAO"].ToString()).Hour.ToString();
                    cbMM.Text = DateTime.Parse(dt.Rows[0]["GIOVAO"].ToString()).Minute.ToString().Length < 2 ? "0" + DateTime.Parse(dt.Rows[0]["GIOVAO"].ToString()).Minute.ToString() : DateTime.Parse(dt.Rows[0]["GIOVAO"].ToString()).Minute.ToString();

                    KhoamoControl_Chuyenban(true);
                }
            }
            else
            {
            }
        }

        // Select check Table have in flow funtion 9
        private void SelectCheckTable(string idTable)
        {
            //Checked table select 
            foreach (SimpleButton btnBan in flCheckTable.Controls)
            {
                if (btnBan.Name == this.ActiveControl.Name)
                {
                    if (btnBan.Name == maBan)
                        throw new Exception("Bàn này đã được chọn !");
                }
            }
            //Select Table
            string sql = "select MA_BAN, TEN_BAN, SOGHE, HINHANH from BAN where MA_BAN = " + idTable + "";
            DataTable dtCheckTable = clsMain.ReturnDataTable(sql);
            SimpleButton btnTables = new SimpleButton();
            btnTables.Name = dtCheckTable.Rows[0]["MA_BAN"].ToString();
            btnTables.Text = dtCheckTable.Rows[0]["TEN_BAN"].ToString() + " - " + dtCheckTable.Rows[0]["SOGHE"].ToString() + "  Ghế";
            btnTables.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            btnTables.ImageLocation = ImageLocation.TopCenter;
            try
            {
                byte[] tam = new byte[((byte[])dtCheckTable.Rows[0]["HINHANH"]).LongLength];
                tam = (byte[])dtCheckTable.Rows[0]["HINHANH"];
                MemoryStream ms = new MemoryStream(tam);
                Bitmap bm = new Bitmap(ms);
                if (cls_KP_RES.iHinhanh == 1)
                {
                    btnTables.BackgroundImage = bm;
                    btnTables.BackgroundImageLayout = ImageLayout.Stretch;
                    btnTables.Appearance.BackColor = Color.Transparent;
                    btnTables.Appearance.Options.UseBackColor = true;
                    btnTables.ButtonStyle = BorderStyles.NoBorder;
                    btnTables.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                }
                else
                {
                    btnTables.Image = bm;
                }
            }
            catch
            {
            }
            btnTables.RightToLeft = RightToLeft.No;
            btnTables.Size = new Size(110, 80);
            btnTables.Click += new EventHandler(btnTables_Click);
            flCheckTable.Controls.Add(btnTables);
        }

        private void btnTables_Click(object sender, EventArgs e)
        {
            if (funtions == 9)
            {
                foreach (SimpleButton btnTables in flCheckTable.Controls)
                {
                    if (btnTables.Name == this.ActiveControl.Name)
                    {
                        idCheckTable = btnTables.Name;
                        SetBackgroudOnClick(btnTables);
                    }
                    else
                        SetDefautlBackgroud(btnTables);
                }
            }
        }

        private bool XacNhanBanDat(string idTable)
        {
            DateTime now;
            string idBill = GetMaHoaDon(idTable);
            DataTable timeIn = clsMain.ReturnDataTable("Select GIOVAO from HOADON where MA_HOADON ='" + idBill + "'");
            if (timeIn.Rows.Count > 0)
                now = DateTime.Parse(timeIn.Rows[0]["GIOVAO"].ToString().Substring(0, 10));
            else
                throw new Exception(_tgvaocuabannaykxd);
            DateTime x = DateTime.Parse(txtngay.EditValue.ToString().Substring(0, 10));
            int outline = DateTime.Compare(now, x);
            if (outline == 1)
            {
                XtraMessageBox.Show(_ngaydatbankhonghople, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            else
            {
                if (cbHH.Text != "")
                    if ((int.Parse(cbHH.Text) - int.Parse(DateTime.Now.Hour.ToString())) <= 5 && outline == 0)
                    {
                        XtraMessageBox.Show(_tgdenkhonghople, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
            }
            return true;
        }

        // select table isbandat
        private bool SelectTablesCheckt(string idTable)
        {
            DateTime now;
            DataTable timeIn = clsMain.ReturnDataTable("Select HD.GIOVAO from HOADON HD inner join CT_MOBAN BM on HD.MA_HOADON = BM.MA_HOADON where BM.MA_BAN = " + idTable + " and HD.ISBANDAT = 1 ");
            if (timeIn.Rows.Count > 0)
            {
                for (int i = 0; i < timeIn.Rows.Count; i++)
                {
                    now = DateTime.Parse(timeIn.Rows[i]["GIOVAO"].ToString().Substring(0, 10));
                    DateTime x = DateTime.Parse(txtngay.EditValue.ToString().Substring(0, 10));
                    int outline = DateTime.Compare(now, x);
                    if (outline == 0)
                    {
                        XtraMessageBox.Show(_bannaydaconguoidat, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
            }
            return true;

        }

        private bool CheckHaveISBANDAT(int idTable)
        {
            string sqlIS = "Select ISBANDAT From BAN Where MA_BAN =" + idTable + " And ISBANDAT = 1";
            DataTable tableIS = clsMain.ReturnDataTable(sqlIS);
            if (tableIS.Rows.Count > 0)
                return true;
            return false;
        }

        private void CheckedIsTable()
        {
            try
            {
                string time1 = Convert.ToDateTime(DateTime.Now.ToShortDateString()).ToString("dd/MM/yyyy");
                DateTime now = DateTime.Parse(time1);
                DataTable dttt = clsMain.ReturnDataTable("Select mb.MA_BAN, convert(varchar(25), hd.GIOVAO , 120) as GIOVAO, convert(varchar(10),hd.GIOVAO , 101) as NgayDat from CT_MOBAN mb inner join HOADON hd on mb.MA_HOADON=hd.MA_HOADON  where ISBANDAT =1");
                if (dttt.Rows.Count <= 0)
                    return;
                for (int i = 0; i < dttt.Rows.Count; i++)
                {
                    string dd = dttt.Rows[i]["GIOVAO"].ToString();
                    if (dttt.Rows[i]["GIOVAO"].ToString() != "")
                    {
                        string sso = dttt.Rows[i]["NgayDat"].ToString();
                        DateTime x = DateTime.Parse(sso);
                        int outline = DateTime.Compare(now, x);
                        if ((int.Parse(dttt.Rows[i]["GIOVAO"].ToString().Substring(11, 2)) - int.Parse(DateTime.Now.Hour.ToString()) <= 5) && outline == 0)
                        {
                            string sql = "Update BAN SET ISBANDAT = 1 where MA_BAN =" + int.Parse(dttt.Rows[i]["MA_BAN"].ToString());
                            clsMain.ExecuteSQL(sql);
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void btnComboOption_Click(object sender, EventArgs e)
        {
            try
            {
                string co_mahanghoa = "";
                string co_tenhanghoa = "";
                string co_soluong = "";
                string co_phuthu = "";

                if (bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
                {
                    string maCombo = "";
                    int i = 0;
                    for (i = gv_SelectProduct.FocusedRowHandle - 1; i >= 0; i--)
                    {
                        if (!bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
                        {
                            maCombo = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                            break;
                        }
                    }

                    string sSQL = "";
                    sSQL += "Select MA_HANGHOA_OPTION as MA,B.TEN_HANGHOA as TEN,A.SOLUONG,GIATHEM" + "\n";
                    sSQL += "From COMBO_OPTION A,HANGHOA B" + "\n";
                    sSQL += "Where MA_HANGHOA_OPTION=b.MA_HANGHOA" + "\n";
                    sSQL += "AND a.MA_COMBO=" + clsMain.SQLString(maCombo) + "\n";
                    sSQL += "AND a.MA_HANGHOA=" + clsMain.SQLString(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                    DataTable dt = clsMain.ReturnDataTable(sSQL);
                    if (dt.Rows.Count > 0)
                    {
                        Frm_Combo_Option frm = new Frm_Combo_Option(maCombo, gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                        frm.ShowDialog();
                        co_mahanghoa = frm.s_mahanghoa;
                        co_tenhanghoa = frm.s_tenhanghoa;
                        co_soluong = (int.Parse(frm.s_soluong) * int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, "SOLUONG").ToString())).ToString();
                        co_phuthu = frm.s_phuthu;
                        if (co_mahanghoa != "")
                        {
                            float tt = float.Parse(gv_SelectProduct.GetRowCellValue(i, "GIABAN").ToString()) + float.Parse(co_phuthu);
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "MA_HANGHOA", co_mahanghoa);
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "TEN_HANGHOA", co_tenhanghoa);
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "SOLUONG", int.Parse(co_soluong));
                            gv_SelectProduct.SetRowCellValue(i, "GIABAN", tt);
                            gv_SelectProduct.SetRowCellValue(i, "Thanh_Tien", tt * int.Parse(gv_SelectProduct.GetRowCellValue(i, "SOLUONG").ToString()));
                        }
                    }
                    else if (dt.Rows.Count == 0)
                    {
                        sSQL = "";
                        sSQL += "Select a.MA_HANGHOA as MA,B.TEN_HANGHOA as TEN,A.SOLUONG,GIATHEM" + "\n";
                        sSQL += "From COMBO_OPTION A,HANGHOA B" + "\n";
                        sSQL += "Where a.MA_HANGHOA=b.MA_HANGHOA" + "\n";
                        sSQL += "AND a.MA_COMBO=" + clsMain.SQLString(maCombo) + "\n";
                        sSQL += "AND a.MA_HANGHOA_OPTION=" + clsMain.SQLString(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, "MA_HANGHOA").ToString());
                        dt = clsMain.ReturnDataTable(sSQL);
                        if (dt.Rows.Count > 0)
                        {
                            int sl = int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, "SOLUONG").ToString()) / int.Parse(dt.Rows[0]["SOLUONG"].ToString());
                            float tt = float.Parse(gv_SelectProduct.GetRowCellValue(i, "GIABAN").ToString()) - float.Parse(dt.Rows[0]["GIATHEM"].ToString());
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "MA_HANGHOA", dt.Rows[0]["MA"].ToString());
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "TEN_HANGHOA", dt.Rows[0]["TEN"].ToString());
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "SOLUONG", sl);
                            gv_SelectProduct.SetRowCellValue(i, "GIABAN", tt);
                            gv_SelectProduct.SetRowCellValue(i, "Thanh_Tien", tt * int.Parse(gv_SelectProduct.GetRowCellValue(i, "SOLUONG").ToString()));
                        }
                        else
                        {
                            XtraMessageBox.Show(_khongcohhthaythe, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    else
                    {
                        XtraMessageBox.Show(_khongcohhthaythe, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                else
                {
                    XtraMessageBox.Show(_hhkhongthuoccombo, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch
            {
            }
        }

        private void btnThemHangHoa_Click(object sender, EventArgs e)
        {
            Frm_ThemHangHoa frm = new Frm_ThemHangHoa();
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btn_NhapSL_Click(object sender, EventArgs e)
        {
            Frm_Chontheosoluong frm = new Frm_Chontheosoluong();
            frm.Mode = 1;
            frm.ShowDialog();
            SoLuong_HuyMon = frm.tmp_SoLuong;
            frm.Dispose();
            flag_sole = true;
            ThrowTable1ToTable2();
        }

        private void btnChongia_Click(object sender, EventArgs e)
        {
            if (funtions != 1 || gv_SelectProduct.RowCount <= 0 || maBan == "")
                return;

            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }

            string SGia = "GIABAN1";
            string smaMon = gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
            string sgiaMon = gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["GIABAN"]).ToString();
            string maHD = GetMaHoaDon(maBan);

            Frm_Chongiaban frm = new Frm_Chongiaban();
            frm.smaMon_Dau = smaMon;
            frm.sgiaMon_Dau = sgiaMon;
            frm.ShowDialog();
            SGia = frm.sChon_Gia;
            frm.Dispose();

            //Cancel thao tác
            if (SGia == "")
            {
                return;
            }
            //Kiểm tra nếu đang dùng bản giá theo khu hay cửa hàng thì return
            string sSQL = "select GIATRI from CAUHINH where TEN='APDUNGBANGGIATHEOKHUVUC'";
            DataTable dtcheck = clsMain.ReturnDataTable(sSQL);
            if (dtcheck.Rows.Count > 0)
            {
                if (dtcheck.Rows[0]["GIATRI"].ToString() != "0")
                {
                    MessageBox.Show("Đang dùng chương trình tính giá theo khu hay cửa hàng. Không áp dụng được giá bán 2", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }

            //lây giá bán 1,2,3,4
            for (int i = 0; i < gv_SelectProduct.RowCount; i++)
            {
                smaMon = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();

                sSQL = "select  ISNULL(" + SGia + ",0) as GIABAN_CHON from HANGHOA where MA_HANGHOA = " + clsMain.SQLString(smaMon);

                DataTable dtgiaban = clsMain.ReturnDataTable(sSQL);
                if (dtgiaban.Rows.Count > 0)
                {
                    sSQL = "";
                    sSQL += "UPDATE CT_HOADON SET " + "\n";
                    sSQL += "GIABAN =" + clsMain.SQLString(dtgiaban.Rows[0]["GIABAN_CHON"].ToString()) + "," + "\n";
                    sSQL += "GIABAN_THAT =" + clsMain.SQLString(dtgiaban.Rows[0]["GIABAN_CHON"].ToString()) + "\n";
                    sSQL += "WHERE MA_HOADON =" + clsMain.SQLString(maHD) + "\n";
                    sSQL += "AND Isnull(Is_Combo,0)=" + clsMain.SQLBit(false) + "\n";
                    sSQL += "AND MA_HANGHOA =" + clsMain.SQLString(smaMon) + "\n";
                    clsMain.ExecuteSQL(sSQL);
                }
            }

            ExitDivideTable();

        }

        private void btnBaoCaoBanHang_Click(object sender, EventArgs e)
        {
            Frm_BCInPhieu frm = new Frm_BCInPhieu();
            frm.WindowState = FormWindowState.Maximized;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.Mode = 125;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnHetmon_Click(object sender, EventArgs e)
        {
            Frm_BCInPhieu frm = new Frm_BCInPhieu();
            frm.WindowState = FormWindowState.Maximized;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.Mode = 126;
            frm.ShowDialog();
            frm.Dispose();
        }

        private bool KiemTraTrangThaiKhoa()
        {
            bool kq;
            string sql = "SELECT ISNULL(IS_TAMKHOA,0) FROM HANGHOA WHERE MA_HANGHOA=" + clsMain.SQLString(maMon);
            DataTable dt = clsMain.ReturnDataTable(sql);
            kq = bool.Parse(dt.Rows[0][0].ToString());
            return kq;
        }

        private void btnBoKhoaBan_Click(object sender, EventArgs e)
        {
            if (funtions == 1 && maBan != "")
            {
                if (XtraMessageBox.Show("Xác nhận bạn muốn bỏ khóa bàn ? ", _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    XtraMessageBox.Show("Đã bỏ khóa bàn ", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void btnNangcap_Click(object sender, EventArgs e)
        {
            if (funtions != 1 || gv_SelectProduct.RowCount <= 0 || maBan == "")
                return;

            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }


            string maHD = GetMaHoaDon(maBan);

            Frm_Nanghangbuffet frm = new Frm_Nanghangbuffet();
            frm.sMaHD = maHD;
            frm.ShowDialog();
            frm.Dispose();

            ExitDivideTable();
        }

        private void btnDoihinhthucthanhtoan_Click(object sender, EventArgs e)
        {
            Frm_HTTT frm = new Frm_HTTT();
            frm.ShowDialog();
            frm.Dispose();
        }

    }
}