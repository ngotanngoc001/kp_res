﻿namespace KP_RES 
{
    partial class Frm_HTTT 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl19 = new DevExpress.XtraEditors.PanelControl();
            this.lblBill = new DevExpress.XtraEditors.LabelControl();
            this.btnChon = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl53 = new DevExpress.XtraEditors.PanelControl();
            this.gr_billPaymented = new DevExpress.XtraGrid.GridControl();
            this.gvBillPaymented = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SSTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HOADON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYTAO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_KHUVUC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONGTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_BAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtSOthe = new DevExpress.XtraEditors.TextEdit();
            this.chkVisa = new DevExpress.XtraEditors.CheckEdit();
            this.chkTienmat = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).BeginInit();
            this.panelControl19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl53)).BeginInit();
            this.panelControl53.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_billPaymented)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBillPaymented)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSOthe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVisa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTienmat.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_300x15;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(855, 35);
            this.panel2.TabIndex = 7;
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(736, 2);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(117, 42);
            this.btnDong.TabIndex = 5;
            this.btnDong.Text = "&Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // panelControl19
            // 
            this.panelControl19.Controls.Add(this.lblBill);
            this.panelControl19.Controls.Add(this.btnChon);
            this.panelControl19.Controls.Add(this.btnDong);
            this.panelControl19.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl19.Location = new System.Drawing.Point(0, 571);
            this.panelControl19.Name = "panelControl19";
            this.panelControl19.Size = new System.Drawing.Size(855, 46);
            this.panelControl19.TabIndex = 15;
            // 
            // lblBill
            // 
            this.lblBill.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lblBill.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblBill.Location = new System.Drawing.Point(2, 2);
            this.lblBill.Name = "lblBill";
            this.lblBill.Size = new System.Drawing.Size(26, 19);
            this.lblBill.TabIndex = 7;
            this.lblBill.Text = "Bill";
            // 
            // btnChon
            // 
            this.btnChon.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChon.Appearance.Options.UseFont = true;
            this.btnChon.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnChon.Image = global::KP_RES.Properties.Resources.card_in_use_26;
            this.btnChon.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnChon.Location = new System.Drawing.Point(508, 2);
            this.btnChon.Margin = new System.Windows.Forms.Padding(4);
            this.btnChon.Name = "btnChon";
            this.btnChon.Size = new System.Drawing.Size(228, 42);
            this.btnChon.TabIndex = 6;
            this.btnChon.Text = "&Đổi hình thức thanh toán";
            this.btnChon.Click += new System.EventHandler(this.btnChon_Click);
            // 
            // panelControl53
            // 
            this.panelControl53.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl53.Controls.Add(this.gr_billPaymented);
            this.panelControl53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl53.Location = new System.Drawing.Point(0, 35);
            this.panelControl53.Name = "panelControl53";
            this.panelControl53.Size = new System.Drawing.Size(855, 490);
            this.panelControl53.TabIndex = 16;
            // 
            // gr_billPaymented
            // 
            this.gr_billPaymented.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gr_billPaymented.Location = new System.Drawing.Point(0, 0);
            this.gr_billPaymented.MainView = this.gvBillPaymented;
            this.gr_billPaymented.Name = "gr_billPaymented";
            this.gr_billPaymented.Size = new System.Drawing.Size(855, 490);
            this.gr_billPaymented.TabIndex = 1;
            this.gr_billPaymented.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvBillPaymented});
            // 
            // gvBillPaymented
            // 
            this.gvBillPaymented.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.gvBillPaymented.Appearance.FocusedRow.Options.UseFont = true;
            this.gvBillPaymented.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.gvBillPaymented.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvBillPaymented.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.75F);
            this.gvBillPaymented.Appearance.Row.Options.UseFont = true;
            this.gvBillPaymented.ColumnPanelRowHeight = 45;
            this.gvBillPaymented.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SSTT,
            this.MA_HOADON,
            this.NGAYTAO,
            this.TEN_KHUVUC,
            this.TONGTIEN,
            this.TEN_BAN});
            this.gvBillPaymented.GridControl = this.gr_billPaymented;
            this.gvBillPaymented.Name = "gvBillPaymented";
            this.gvBillPaymented.OptionsView.EnableAppearanceEvenRow = true;
            this.gvBillPaymented.OptionsView.ShowGroupPanel = false;
            this.gvBillPaymented.OptionsView.ShowIndicator = false;
            this.gvBillPaymented.RowHeight = 45;
            this.gvBillPaymented.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gvBillPaymented.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gvBillPaymented_RowClick);
            this.gvBillPaymented.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gvBillPaymented_CustomDrawCell);
            // 
            // SSTT
            // 
            this.SSTT.AppearanceCell.Options.UseTextOptions = true;
            this.SSTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SSTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SSTT.AppearanceHeader.Options.UseFont = true;
            this.SSTT.AppearanceHeader.Options.UseTextOptions = true;
            this.SSTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SSTT.Caption = "STT";
            this.SSTT.FieldName = "SSTT";
            this.SSTT.MinWidth = 40;
            this.SSTT.Name = "SSTT";
            this.SSTT.OptionsColumn.AllowEdit = false;
            this.SSTT.OptionsColumn.AllowFocus = false;
            this.SSTT.OptionsFilter.AllowAutoFilter = false;
            this.SSTT.OptionsFilter.AllowFilter = false;
            this.SSTT.Visible = true;
            this.SSTT.VisibleIndex = 0;
            this.SSTT.Width = 40;
            // 
            // MA_HOADON
            // 
            this.MA_HOADON.AppearanceCell.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA_HOADON.AppearanceHeader.Options.UseFont = true;
            this.MA_HOADON.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.Caption = "Mã Hóa Đơn";
            this.MA_HOADON.FieldName = "MA_HOADON";
            this.MA_HOADON.MaxWidth = 150;
            this.MA_HOADON.MinWidth = 135;
            this.MA_HOADON.Name = "MA_HOADON";
            this.MA_HOADON.OptionsColumn.AllowEdit = false;
            this.MA_HOADON.OptionsColumn.AllowFocus = false;
            this.MA_HOADON.Visible = true;
            this.MA_HOADON.VisibleIndex = 1;
            this.MA_HOADON.Width = 135;
            // 
            // NGAYTAO
            // 
            this.NGAYTAO.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYTAO.AppearanceHeader.Options.UseFont = true;
            this.NGAYTAO.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYTAO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYTAO.Caption = "Ngày Tạo";
            this.NGAYTAO.DisplayFormat.FormatString = "{0:dd/MM/yyyy HH:mm}";
            this.NGAYTAO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYTAO.FieldName = "NGAYTAO";
            this.NGAYTAO.MaxWidth = 150;
            this.NGAYTAO.MinWidth = 135;
            this.NGAYTAO.Name = "NGAYTAO";
            this.NGAYTAO.OptionsColumn.AllowEdit = false;
            this.NGAYTAO.OptionsColumn.AllowFocus = false;
            this.NGAYTAO.Visible = true;
            this.NGAYTAO.VisibleIndex = 4;
            this.NGAYTAO.Width = 135;
            // 
            // TEN_KHUVUC
            // 
            this.TEN_KHUVUC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_KHUVUC.AppearanceHeader.Options.UseFont = true;
            this.TEN_KHUVUC.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_KHUVUC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_KHUVUC.Caption = "Khu vực";
            this.TEN_KHUVUC.FieldName = "TEN_KHUVUC";
            this.TEN_KHUVUC.MinWidth = 70;
            this.TEN_KHUVUC.Name = "TEN_KHUVUC";
            this.TEN_KHUVUC.OptionsColumn.AllowEdit = false;
            this.TEN_KHUVUC.OptionsColumn.AllowFocus = false;
            this.TEN_KHUVUC.OptionsFilter.AllowAutoFilter = false;
            this.TEN_KHUVUC.OptionsFilter.AllowFilter = false;
            this.TEN_KHUVUC.Visible = true;
            this.TEN_KHUVUC.VisibleIndex = 2;
            this.TEN_KHUVUC.Width = 70;
            // 
            // TONGTIEN
            // 
            this.TONGTIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TONGTIEN.AppearanceHeader.Options.UseFont = true;
            this.TONGTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TONGTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONGTIEN.Caption = "Tổng Tiền";
            this.TONGTIEN.DisplayFormat.FormatString = "N0";
            this.TONGTIEN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TONGTIEN.FieldName = "TONGTIEN";
            this.TONGTIEN.MaxWidth = 120;
            this.TONGTIEN.MinWidth = 70;
            this.TONGTIEN.Name = "TONGTIEN";
            this.TONGTIEN.OptionsColumn.AllowEdit = false;
            this.TONGTIEN.OptionsColumn.AllowFocus = false;
            this.TONGTIEN.Visible = true;
            this.TONGTIEN.VisibleIndex = 5;
            this.TONGTIEN.Width = 70;
            // 
            // TEN_BAN
            // 
            this.TEN_BAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_BAN.AppearanceHeader.Options.UseFont = true;
            this.TEN_BAN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_BAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_BAN.Caption = "Bàn";
            this.TEN_BAN.FieldName = "TEN_BAN";
            this.TEN_BAN.MinWidth = 70;
            this.TEN_BAN.Name = "TEN_BAN";
            this.TEN_BAN.OptionsColumn.AllowEdit = false;
            this.TEN_BAN.OptionsColumn.AllowFocus = false;
            this.TEN_BAN.OptionsFilter.AllowAutoFilter = false;
            this.TEN_BAN.OptionsFilter.AllowFilter = false;
            this.TEN_BAN.Visible = true;
            this.TEN_BAN.VisibleIndex = 3;
            this.TEN_BAN.Width = 70;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.txtSOthe);
            this.panelControl1.Controls.Add(this.chkVisa);
            this.panelControl1.Controls.Add(this.chkTienmat);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 525);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(855, 46);
            this.panelControl1.TabIndex = 17;
            // 
            // txtSOthe
            // 
            this.txtSOthe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSOthe.EditValue = "0000";
            this.txtSOthe.Location = new System.Drawing.Point(449, 5);
            this.txtSOthe.Name = "txtSOthe";
            this.txtSOthe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.txtSOthe.Properties.Appearance.Options.UseFont = true;
            this.txtSOthe.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSOthe.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSOthe.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtSOthe.Size = new System.Drawing.Size(272, 32);
            this.txtSOthe.TabIndex = 46;
            // 
            // chkVisa
            // 
            this.chkVisa.EnterMoveNextControl = true;
            this.chkVisa.Location = new System.Drawing.Point(241, 10);
            this.chkVisa.Name = "chkVisa";
            this.chkVisa.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkVisa.Properties.Appearance.Options.UseFont = true;
            this.chkVisa.Properties.Caption = "Thanh toán bằng thẻ visa";
            this.chkVisa.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chkVisa.Properties.RadioGroupIndex = 1;
            this.chkVisa.Size = new System.Drawing.Size(212, 24);
            this.chkVisa.TabIndex = 25;
            this.chkVisa.TabStop = false;
            // 
            // chkTienmat
            // 
            this.chkTienmat.EditValue = true;
            this.chkTienmat.EnterMoveNextControl = true;
            this.chkTienmat.Location = new System.Drawing.Point(12, 10);
            this.chkTienmat.Name = "chkTienmat";
            this.chkTienmat.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTienmat.Properties.Appearance.Options.UseFont = true;
            this.chkTienmat.Properties.Caption = "Thanh toán bằng tiền mặt";
            this.chkTienmat.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chkTienmat.Properties.RadioGroupIndex = 1;
            this.chkTienmat.Size = new System.Drawing.Size(223, 24);
            this.chkTienmat.TabIndex = 24;
            // 
            // Frm_HTTT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(855, 617);
            this.ControlBox = false;
            this.Controls.Add(this.panelControl53);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl19);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_HTTT";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nâng hạng bufet";
            this.Load += new System.EventHandler(this.Frm_HTTT_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).EndInit();
            this.panelControl19.ResumeLayout(false);
            this.panelControl19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl53)).EndInit();
            this.panelControl53.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_billPaymented)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBillPaymented)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSOthe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVisa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTienmat.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnDong;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.PanelControl panelControl19;
        private DevExpress.XtraEditors.PanelControl panelControl53;
        private DevExpress.XtraGrid.GridControl gr_billPaymented;
        private DevExpress.XtraGrid.Views.Grid.GridView gvBillPaymented;
        private DevExpress.XtraGrid.Columns.GridColumn SSTT;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HOADON;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYTAO;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_KHUVUC;
        private DevExpress.XtraGrid.Columns.GridColumn TONGTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_BAN;
        private DevExpress.XtraEditors.SimpleButton btnChon;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit chkVisa;
        private DevExpress.XtraEditors.CheckEdit chkTienmat;
        private DevExpress.XtraEditors.TextEdit txtSOthe;
        private DevExpress.XtraEditors.LabelControl lblBill;




    }
}