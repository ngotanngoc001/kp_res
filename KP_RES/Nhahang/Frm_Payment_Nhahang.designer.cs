﻿namespace KP_RES 
{
    partial class Frm_Payment_Nhahang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Payment_Nhahang));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            this.pnlGridviewActionMain = new DevExpress.XtraEditors.PanelControl();
            this.pnlInforBuyProduct = new DevExpress.XtraEditors.PanelControl();
            this.pnlGridview = new DevExpress.XtraEditors.PanelControl();
            this.gcListProduct = new DevExpress.XtraGrid.GridControl();
            this.gvListProduct = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ProductName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.Quantum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CHIETKHAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TheMoney = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_BEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Discount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IS_COMBO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IS_INBEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MONTHEM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MACP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MACB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnUpDownGridview = new DevExpress.XtraEditors.PanelControl();
            this.btnUpGridview = new DevExpress.XtraEditors.SimpleButton();
            this.btnDownGridview = new DevExpress.XtraEditors.SimpleButton();
            this.btnLastGridview = new DevExpress.XtraEditors.SimpleButton();
            this.btnFirstGridview = new DevExpress.XtraEditors.SimpleButton();
            this.pnlInforMoneyAndCustomer = new DevExpress.XtraEditors.PanelControl();
            this.pnlInforCustomer = new DevExpress.XtraEditors.PanelControl();
            this.pnlInnerCustomerShow = new DevExpress.XtraEditors.PanelControl();
            this.lbMoneySurplus = new DevExpress.XtraEditors.LabelControl();
            this.lbIdInnerCustomerShow = new DevExpress.XtraEditors.LabelControl();
            this.lbNameInnerCustomer = new DevExpress.XtraEditors.LabelControl();
            this.pnlDearCustomerShow = new DevExpress.XtraEditors.PanelControl();
            this.lbMark = new DevExpress.XtraEditors.LabelControl();
            this.lbIDDearCustomerShow = new DevExpress.XtraEditors.LabelControl();
            this.lbNameDearCustomer = new DevExpress.XtraEditors.LabelControl();
            this.pnlInforMoney = new DevExpress.XtraEditors.PanelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.lbTotalMoney = new DevExpress.XtraEditors.LabelControl();
            this.lb_tongcong = new DevExpress.XtraEditors.LabelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.lb_tiengio = new DevExpress.XtraEditors.LabelControl();
            this.lbHourMoney = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lbTheMoney = new DevExpress.XtraEditors.LabelControl();
            this.lb_thanhtien = new DevExpress.XtraEditors.LabelControl();
            this.pnlActionCashier = new DevExpress.XtraEditors.PanelControl();
            this.pnDacThu = new DevExpress.XtraEditors.PanelControl();
            this.btnCongNo = new DevExpress.XtraEditors.SimpleButton();
            this.btnNhanvienthuchien = new DevExpress.XtraEditors.SimpleButton();
            this.btnKhachhang = new DevExpress.XtraEditors.SimpleButton();
            this.btnGhichu = new DevExpress.XtraEditors.SimpleButton();
            this.btnGiaohang = new DevExpress.XtraEditors.SimpleButton();
            this.pnlPayment = new DevExpress.XtraEditors.PanelControl();
            this.btnPhieuMuaHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnTachBill = new DevExpress.XtraEditors.SimpleButton();
            this.btnMakhuyenmai = new DevExpress.XtraEditors.SimpleButton();
            this.btnVisa = new DevExpress.XtraEditors.SimpleButton();
            this.btnVoucher = new DevExpress.XtraEditors.SimpleButton();
            this.btnServiceMoney = new DevExpress.XtraEditors.SimpleButton();
            this.btnRetypePayment = new DevExpress.XtraEditors.SimpleButton();
            this.btnDiscount = new DevExpress.XtraEditors.SimpleButton();
            this.btnCardImtimateCustomer = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancelPayment = new DevExpress.XtraEditors.SimpleButton();
            this.btnNotPrintBill = new DevExpress.XtraEditors.SimpleButton();
            this.btnCardInternalPayment = new DevExpress.XtraEditors.SimpleButton();
            this.pnlPaymentButton = new DevExpress.XtraEditors.PanelControl();
            this.btn_DatThu = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ThuNgan = new DevExpress.XtraEditors.SimpleButton();
            this.pnlPaymentShowFill = new DevExpress.XtraEditors.PanelControl();
            this.pnlShowInforPayment = new DevExpress.XtraEditors.PanelControl();
            this.pnlPhieuMuaHang = new DevExpress.XtraEditors.PanelControl();
            this.txtNhapPhieuMuaHang = new DevExpress.XtraEditors.TextEdit();
            this.lbNhapPhieuMuaHang = new DevExpress.XtraEditors.LabelControl();
            this.pnlTienKhachTra = new DevExpress.XtraEditors.PanelControl();
            this.lbcurrencyGetMoney = new DevExpress.XtraEditors.LabelControl();
            this.txtInputMoney = new DevExpress.XtraEditors.TextEdit();
            this.lbPayment = new DevExpress.XtraEditors.LabelControl();
            this.pnlPhiPhuVu = new DevExpress.XtraEditors.PanelControl();
            this.lbcurrencyService = new DevExpress.XtraEditors.LabelControl();
            this.txtServiceMoney = new DevExpress.XtraEditors.TextEdit();
            this.lb_nhaptienphucvu = new DevExpress.XtraEditors.LabelControl();
            this.pnlTTTNB = new DevExpress.XtraEditors.PanelControl();
            this.txtInnerCustomer = new DevExpress.XtraEditors.TextEdit();
            this.lb_nhapmaTTNB = new DevExpress.XtraEditors.LabelControl();
            this.pnlChietKhau = new DevExpress.XtraEditors.PanelControl();
            this.txtInputDiscount = new DevExpress.XtraEditors.TextEdit();
            this.lb_nhapchietkhau = new DevExpress.XtraEditors.LabelControl();
            this.pnlKHTT = new DevExpress.XtraEditors.PanelControl();
            this.txtDearCustomer = new DevExpress.XtraEditors.TextEdit();
            this.lb_nhapma_KHTT = new DevExpress.XtraEditors.LabelControl();
            this.pnlVoucher = new DevExpress.XtraEditors.PanelControl();
            this.txtInputVoucher = new DevExpress.XtraEditors.TextEdit();
            this.lb_nhapVoucher = new DevExpress.XtraEditors.LabelControl();
            this.pnlMagiamgia = new DevExpress.XtraEditors.PanelControl();
            this.txtInputMagiamgia = new DevExpress.XtraEditors.TextEdit();
            this.lb_nhapMagiamgia = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.pnlMoneyReply = new DevExpress.XtraEditors.PanelControl();
            this.lbReplyCustomer = new DevExpress.XtraEditors.LabelControl();
            this.lbVNDReply = new DevExpress.XtraEditors.LabelControl();
            this.lb_tientralai = new DevExpress.XtraEditors.LabelControl();
            this.panelControl30 = new DevExpress.XtraEditors.PanelControl();
            this.lbGetCustomer = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lb_tienkhachtra = new DevExpress.XtraEditors.LabelControl();
            this.pnlFirst = new DevExpress.XtraEditors.PanelControl();
            this.lbFirst = new DevExpress.XtraEditors.LabelControl();
            this.lbFirstMoney = new DevExpress.XtraEditors.LabelControl();
            this.pnlSecond = new DevExpress.XtraEditors.PanelControl();
            this.lbSecond = new DevExpress.XtraEditors.LabelControl();
            this.lbSecondMoney = new DevExpress.XtraEditors.LabelControl();
            this.pnlThird = new DevExpress.XtraEditors.PanelControl();
            this.lbThird = new DevExpress.XtraEditors.LabelControl();
            this.lbThirdMoney = new DevExpress.XtraEditors.LabelControl();
            this.panelControl29 = new DevExpress.XtraEditors.PanelControl();
            this.lbTotalMoneyAll = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.lb_TCthanhtoan = new DevExpress.XtraEditors.LabelControl();
            this.panelControl55 = new DevExpress.XtraEditors.PanelControl();
            this.lbMoneyGetFirst = new DevExpress.XtraEditors.LabelControl();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.lb_tiendatcoc = new DevExpress.XtraEditors.LabelControl();
            this.panelControl51 = new DevExpress.XtraEditors.PanelControl();
            this.lbServiceMoney = new DevExpress.XtraEditors.LabelControl();
            this.lbService = new DevExpress.XtraEditors.LabelControl();
            this.lb_phiphucvu = new DevExpress.XtraEditors.LabelControl();
            this.panelControl27 = new DevExpress.XtraEditors.PanelControl();
            this.lbDiscountPayment = new DevExpress.XtraEditors.LabelControl();
            this.lbDiscount = new DevExpress.XtraEditors.LabelControl();
            this.lb_chietkhau = new DevExpress.XtraEditors.LabelControl();
            this.panelControl31 = new DevExpress.XtraEditors.PanelControl();
            this.lbTotalMoneyPayment = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.lb_tongthanhtoan = new DevExpress.XtraEditors.LabelControl();
            this.pnlVisa = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.txtCountry = new DevExpress.XtraEditors.TextEdit();
            this.txtNameVisa = new DevExpress.XtraEditors.TextEdit();
            this.lb_tenchuthe = new DevExpress.XtraEditors.LabelControl();
            this.btnRefeshIdCode = new DevExpress.XtraEditors.SimpleButton();
            this.txtCwCvc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtIdCodeCreate = new DevExpress.XtraEditors.TextEdit();
            this.lb_thongtinthem = new DevExpress.XtraEditors.LabelControl();
            this.txtCity = new DevExpress.XtraEditors.TextEdit();
            this.txtIdCodeInput = new DevExpress.XtraEditors.TextEdit();
            this.txtAddress = new DevExpress.XtraEditors.TextEdit();
            this.lb_maantoan = new DevExpress.XtraEditors.LabelControl();
            this.lb_tinhtp = new DevExpress.XtraEditors.LabelControl();
            this.lb_quocgia = new DevExpress.XtraEditors.LabelControl();
            this.lb_diachichuthe = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.txtYear = new DevExpress.XtraEditors.TextEdit();
            this.txtMonth = new DevExpress.XtraEditors.TextEdit();
            this.txtMoneyVisa = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.lb_sotien = new DevExpress.XtraEditors.LabelControl();
            this.txtIdCardVisa = new DevExpress.XtraEditors.TextEdit();
            this.lb_thongtinbatbuoc = new DevExpress.XtraEditors.LabelControl();
            this.lb_ngayhethan = new DevExpress.XtraEditors.LabelControl();
            this.lb_sothe = new DevExpress.XtraEditors.LabelControl();
            this.btnPaymentVisa = new DevExpress.XtraEditors.SimpleButton();
            this.bntCancelVisa = new DevExpress.XtraEditors.SimpleButton();
            this.pnlHeaderVisa = new DevExpress.XtraEditors.PanelControl();
            this.lb_thanhtoanbangthe = new DevExpress.XtraEditors.LabelControl();
            this.pnlCongNo = new DevExpress.XtraEditors.PanelControl();
            this.palCongNo_Noidung = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.txtCNMaKH = new DevExpress.XtraEditors.TextEdit();
            this.txtCNSoDT = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.dtpNgayHenTra = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnCongNoSearch = new DevExpress.XtraEditors.SimpleButton();
            this.txtCongNo_Tien = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.btnCongNoXacNhan = new DevExpress.XtraEditors.SimpleButton();
            this.btnHuyCongNo = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl13 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.pnlBottomPayment = new DevExpress.XtraEditors.PanelControl();
            this.pnlKeyboardAndShowForeignCurrency = new DevExpress.XtraEditors.PanelControl();
            this.pnlKeyBoardText = new DevExpress.XtraEditors.PanelControl();
            this.btnP = new DevExpress.XtraEditors.SimpleButton();
            this.btnZ = new DevExpress.XtraEditors.SimpleButton();
            this.btnY = new DevExpress.XtraEditors.SimpleButton();
            this.btnPaymentText = new DevExpress.XtraEditors.SimpleButton();
            this.btnShift = new DevExpress.XtraEditors.SimpleButton();
            this.btnX = new DevExpress.XtraEditors.SimpleButton();
            this.btnW = new DevExpress.XtraEditors.SimpleButton();
            this.btnV = new DevExpress.XtraEditors.SimpleButton();
            this.btnU = new DevExpress.XtraEditors.SimpleButton();
            this.btnT = new DevExpress.XtraEditors.SimpleButton();
            this.btnS = new DevExpress.XtraEditors.SimpleButton();
            this.btnF = new DevExpress.XtraEditors.SimpleButton();
            this.btnE = new DevExpress.XtraEditors.SimpleButton();
            this.btnD = new DevExpress.XtraEditors.SimpleButton();
            this.btnC = new DevExpress.XtraEditors.SimpleButton();
            this.btnB = new DevExpress.XtraEditors.SimpleButton();
            this.btnA = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoveRigthText = new DevExpress.XtraEditors.SimpleButton();
            this.btnM = new DevExpress.XtraEditors.SimpleButton();
            this.btnTranslateText = new DevExpress.XtraEditors.SimpleButton();
            this.btnR = new DevExpress.XtraEditors.SimpleButton();
            this.btnQ = new DevExpress.XtraEditors.SimpleButton();
            this.btnOkText = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteText = new DevExpress.XtraEditors.SimpleButton();
            this.btnL = new DevExpress.XtraEditors.SimpleButton();
            this.btnI = new DevExpress.XtraEditors.SimpleButton();
            this.btnH = new DevExpress.XtraEditors.SimpleButton();
            this.btnG = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoveLeftText = new DevExpress.XtraEditors.SimpleButton();
            this.btnK = new DevExpress.XtraEditors.SimpleButton();
            this.btnJ = new DevExpress.XtraEditors.SimpleButton();
            this.btnO = new DevExpress.XtraEditors.SimpleButton();
            this.btnN = new DevExpress.XtraEditors.SimpleButton();
            this.btnEscText = new DevExpress.XtraEditors.SimpleButton();
            this.pnlKeyboardMoney = new DevExpress.XtraEditors.PanelControl();
            this.btn500000 = new DevExpress.XtraEditors.SimpleButton();
            this.btn200000 = new DevExpress.XtraEditors.SimpleButton();
            this.btn100000 = new DevExpress.XtraEditors.SimpleButton();
            this.btn50000 = new DevExpress.XtraEditors.SimpleButton();
            this.btn20000 = new DevExpress.XtraEditors.SimpleButton();
            this.btn10000 = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoveRightMoney = new DevExpress.XtraEditors.SimpleButton();
            this.btnPaymentMoney = new DevExpress.XtraEditors.SimpleButton();
            this.btnTranslateMoney = new DevExpress.XtraEditors.SimpleButton();
            this.btnOkMoney = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteMoney = new DevExpress.XtraEditors.SimpleButton();
            this.btn2000 = new DevExpress.XtraEditors.SimpleButton();
            this.btn1000 = new DevExpress.XtraEditors.SimpleButton();
            this.btn500 = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoveLeftMoney = new DevExpress.XtraEditors.SimpleButton();
            this.btn5000 = new DevExpress.XtraEditors.SimpleButton();
            this.btnEcsMoney = new DevExpress.XtraEditors.SimpleButton();
            this.pnlKeyboardNumber = new DevExpress.XtraEditors.PanelControl();
            this.btnMoveRightNumber = new DevExpress.XtraEditors.SimpleButton();
            this.btn1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPaymentNumber = new DevExpress.XtraEditors.SimpleButton();
            this.btnTranslateNumber = new DevExpress.XtraEditors.SimpleButton();
            this.btn000 = new DevExpress.XtraEditors.SimpleButton();
            this.btn00 = new DevExpress.XtraEditors.SimpleButton();
            this.btn0 = new DevExpress.XtraEditors.SimpleButton();
            this.btnOkNumber = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteNumber = new DevExpress.XtraEditors.SimpleButton();
            this.btn6 = new DevExpress.XtraEditors.SimpleButton();
            this.btn9 = new DevExpress.XtraEditors.SimpleButton();
            this.btn8 = new DevExpress.XtraEditors.SimpleButton();
            this.btn7 = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoveLeftNumber = new DevExpress.XtraEditors.SimpleButton();
            this.btn5 = new DevExpress.XtraEditors.SimpleButton();
            this.btn4 = new DevExpress.XtraEditors.SimpleButton();
            this.btn3 = new DevExpress.XtraEditors.SimpleButton();
            this.btn2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnEscNumber = new DevExpress.XtraEditors.SimpleButton();
            this.pnlShowForeignCurrency = new DevExpress.XtraEditors.PanelControl();
            this.lbExchangeRate = new DevExpress.XtraEditors.LabelControl();
            this.lbKindMoney = new DevExpress.XtraEditors.LabelControl();
            this.lb_loaitien = new DevExpress.XtraEditors.LabelControl();
            this.lb_tygia = new DevExpress.XtraEditors.LabelControl();
            this.pnlUpDownForeignCurrency = new DevExpress.XtraEditors.PanelControl();
            this.btnDownForeignCurrency = new DevExpress.XtraEditors.SimpleButton();
            this.btnUpForeignCurrency = new DevExpress.XtraEditors.SimpleButton();
            this.pnlForeignCurrency = new DevExpress.XtraEditors.PanelControl();
            this.gcForeignCurrency = new DevExpress.XtraGrid.GridControl();
            this.gvListForiegnCurrency = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TYGIA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PHUTHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnlHeaderShowPayment = new DevExpress.XtraEditors.PanelControl();
            this.lbInforName = new DevExpress.XtraEditors.LabelControl();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.lb_Quay = new DevExpress.XtraEditors.LabelControl();
            this.lb_Kho = new DevExpress.XtraEditors.LabelControl();
            this.lbgiay = new DevExpress.XtraEditors.LabelControl();
            this.lbCaBan = new DevExpress.XtraEditors.LabelControl();
            this.lbGio = new DevExpress.XtraEditors.LabelControl();
            this.lb_NhanVien = new DevExpress.XtraEditors.LabelControl();
            this.lbtimeby = new DevExpress.XtraEditors.LabelControl();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pnlGridviewActionMain)).BeginInit();
            this.pnlGridviewActionMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInforBuyProduct)).BeginInit();
            this.pnlInforBuyProduct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlGridview)).BeginInit();
            this.pnlGridview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcListProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDownGridview)).BeginInit();
            this.pnUpDownGridview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInforMoneyAndCustomer)).BeginInit();
            this.pnlInforMoneyAndCustomer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInforCustomer)).BeginInit();
            this.pnlInforCustomer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInnerCustomerShow)).BeginInit();
            this.pnlInnerCustomerShow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDearCustomerShow)).BeginInit();
            this.pnlDearCustomerShow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInforMoney)).BeginInit();
            this.pnlInforMoney.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlActionCashier)).BeginInit();
            this.pnlActionCashier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnDacThu)).BeginInit();
            this.pnDacThu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPayment)).BeginInit();
            this.pnlPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPaymentButton)).BeginInit();
            this.pnlPaymentButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPaymentShowFill)).BeginInit();
            this.pnlPaymentShowFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlShowInforPayment)).BeginInit();
            this.pnlShowInforPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPhieuMuaHang)).BeginInit();
            this.pnlPhieuMuaHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNhapPhieuMuaHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTienKhachTra)).BeginInit();
            this.pnlTienKhachTra.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputMoney.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPhiPhuVu)).BeginInit();
            this.pnlPhiPhuVu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceMoney.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTTTNB)).BeginInit();
            this.pnlTTTNB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInnerCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlChietKhau)).BeginInit();
            this.pnlChietKhau.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputDiscount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKHTT)).BeginInit();
            this.pnlKHTT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDearCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlVoucher)).BeginInit();
            this.pnlVoucher.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputVoucher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMagiamgia)).BeginInit();
            this.pnlMagiamgia.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputMagiamgia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMoneyReply)).BeginInit();
            this.pnlMoneyReply.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl30)).BeginInit();
            this.panelControl30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlFirst)).BeginInit();
            this.pnlFirst.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSecond)).BeginInit();
            this.pnlSecond.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlThird)).BeginInit();
            this.pnlThird.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl29)).BeginInit();
            this.panelControl29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl55)).BeginInit();
            this.panelControl55.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl51)).BeginInit();
            this.panelControl51.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl27)).BeginInit();
            this.panelControl27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl31)).BeginInit();
            this.panelControl31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlVisa)).BeginInit();
            this.pnlVisa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameVisa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCwCvc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdCodeCreate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdCodeInput.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMoneyVisa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdCardVisa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHeaderVisa)).BeginInit();
            this.pnlHeaderVisa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlCongNo)).BeginInit();
            this.pnlCongNo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palCongNo_Noidung)).BeginInit();
            this.palCongNo_Noidung.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCNMaKH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCNSoDT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayHenTra.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayHenTra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCongNo_Tien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).BeginInit();
            this.panelControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBottomPayment)).BeginInit();
            this.pnlBottomPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKeyboardAndShowForeignCurrency)).BeginInit();
            this.pnlKeyboardAndShowForeignCurrency.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKeyBoardText)).BeginInit();
            this.pnlKeyBoardText.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKeyboardMoney)).BeginInit();
            this.pnlKeyboardMoney.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKeyboardNumber)).BeginInit();
            this.pnlKeyboardNumber.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlShowForeignCurrency)).BeginInit();
            this.pnlShowForeignCurrency.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlUpDownForeignCurrency)).BeginInit();
            this.pnlUpDownForeignCurrency.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlForeignCurrency)).BeginInit();
            this.pnlForeignCurrency.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcForeignCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListForiegnCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHeaderShowPayment)).BeginInit();
            this.pnlHeaderShowPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlGridviewActionMain
            // 
            this.pnlGridviewActionMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlGridviewActionMain.Controls.Add(this.pnlInforBuyProduct);
            this.pnlGridviewActionMain.Controls.Add(this.pnlActionCashier);
            this.pnlGridviewActionMain.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlGridviewActionMain.Location = new System.Drawing.Point(647, 70);
            this.pnlGridviewActionMain.Name = "pnlGridviewActionMain";
            this.pnlGridviewActionMain.Size = new System.Drawing.Size(377, 542);
            this.pnlGridviewActionMain.TabIndex = 3;
            // 
            // pnlInforBuyProduct
            // 
            this.pnlInforBuyProduct.Controls.Add(this.pnlGridview);
            this.pnlInforBuyProduct.Controls.Add(this.pnUpDownGridview);
            this.pnlInforBuyProduct.Controls.Add(this.pnlInforMoneyAndCustomer);
            this.pnlInforBuyProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlInforBuyProduct.Location = new System.Drawing.Point(0, 0);
            this.pnlInforBuyProduct.Name = "pnlInforBuyProduct";
            this.pnlInforBuyProduct.Size = new System.Drawing.Size(377, 244);
            this.pnlInforBuyProduct.TabIndex = 4;
            // 
            // pnlGridview
            // 
            this.pnlGridview.Controls.Add(this.gcListProduct);
            this.pnlGridview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGridview.Location = new System.Drawing.Point(2, 2);
            this.pnlGridview.Name = "pnlGridview";
            this.pnlGridview.Size = new System.Drawing.Size(334, 172);
            this.pnlGridview.TabIndex = 2;
            // 
            // gcListProduct
            // 
            this.gcListProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gcListProduct.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gcListProduct.Location = new System.Drawing.Point(2, 2);
            this.gcListProduct.MainView = this.gvListProduct;
            this.gcListProduct.Name = "gcListProduct";
            this.gcListProduct.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1});
            this.gcListProduct.Size = new System.Drawing.Size(330, 168);
            this.gcListProduct.TabIndex = 20;
            this.gcListProduct.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvListProduct});
            // 
            // gvListProduct
            // 
            this.gvListProduct.Appearance.EvenRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gvListProduct.Appearance.EvenRow.Options.UseFont = true;
            this.gvListProduct.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gvListProduct.Appearance.FocusedRow.Options.UseFont = true;
            this.gvListProduct.Appearance.SelectedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gvListProduct.Appearance.SelectedRow.Options.UseFont = true;
            this.gvListProduct.ColumnPanelRowHeight = 30;
            this.gvListProduct.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.ProductName,
            this.Quantum,
            this.Price,
            this.CHIETKHAU,
            this.TheMoney,
            this.MA_HANGHOA,
            this.GHICHU,
            this.MA_BEP,
            this.THUE,
            this.Discount,
            this.IS_COMBO,
            this.IS_INBEP,
            this.MONTHEM,
            this.TEM,
            this.MACP,
            this.MACB});
            this.gvListProduct.GridControl = this.gcListProduct;
            this.gvListProduct.Name = "gvListProduct";
            this.gvListProduct.OptionsView.ShowGroupPanel = false;
            this.gvListProduct.OptionsView.ShowIndicator = false;
            this.gvListProduct.RowHeight = 35;
            this.gvListProduct.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            // 
            // STT
            // 
            this.STT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceCell.Options.UseFont = true;
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "TT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.Width = 25;
            // 
            // ProductName
            // 
            this.ProductName.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.ProductName.AppearanceCell.Options.UseFont = true;
            this.ProductName.AppearanceCell.Options.UseTextOptions = true;
            this.ProductName.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ProductName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.ProductName.AppearanceHeader.Options.UseFont = true;
            this.ProductName.AppearanceHeader.Options.UseTextOptions = true;
            this.ProductName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ProductName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ProductName.Caption = "Tên Hàng Hóa";
            this.ProductName.ColumnEdit = this.repositoryItemMemoEdit1;
            this.ProductName.FieldName = "TEN_HANGHOA";
            this.ProductName.Name = "ProductName";
            this.ProductName.OptionsColumn.AllowEdit = false;
            this.ProductName.OptionsColumn.AllowFocus = false;
            this.ProductName.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.ProductName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.ProductName.OptionsFilter.AllowFilter = false;
            this.ProductName.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.ProductName.Visible = true;
            this.ProductName.VisibleIndex = 0;
            this.ProductName.Width = 160;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // Quantum
            // 
            this.Quantum.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Quantum.AppearanceCell.Options.UseFont = true;
            this.Quantum.AppearanceCell.Options.UseTextOptions = true;
            this.Quantum.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Quantum.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Quantum.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.Quantum.AppearanceHeader.Options.UseFont = true;
            this.Quantum.AppearanceHeader.Options.UseTextOptions = true;
            this.Quantum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Quantum.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Quantum.Caption = "SL";
            this.Quantum.FieldName = "SOLUONG";
            this.Quantum.Name = "Quantum";
            this.Quantum.OptionsColumn.AllowEdit = false;
            this.Quantum.OptionsColumn.AllowFocus = false;
            this.Quantum.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.Quantum.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.Quantum.OptionsColumn.FixedWidth = true;
            this.Quantum.OptionsFilter.AllowFilter = false;
            this.Quantum.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.Quantum.Visible = true;
            this.Quantum.VisibleIndex = 1;
            this.Quantum.Width = 50;
            // 
            // Price
            // 
            this.Price.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Price.AppearanceCell.Options.UseFont = true;
            this.Price.AppearanceCell.Options.UseTextOptions = true;
            this.Price.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Price.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Price.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.Price.AppearanceHeader.Options.UseFont = true;
            this.Price.AppearanceHeader.Options.UseTextOptions = true;
            this.Price.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Price.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Price.Caption = "Đơn giá";
            this.Price.DisplayFormat.FormatString = "{0:#,###0}";
            this.Price.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.Price.FieldName = "GIABAN";
            this.Price.Name = "Price";
            this.Price.OptionsColumn.AllowEdit = false;
            this.Price.OptionsColumn.AllowFocus = false;
            this.Price.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.Price.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.Price.OptionsColumn.FixedWidth = true;
            this.Price.OptionsFilter.AllowFilter = false;
            this.Price.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.Price.Visible = true;
            this.Price.VisibleIndex = 2;
            this.Price.Width = 50;
            // 
            // CHIETKHAU
            // 
            this.CHIETKHAU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.CHIETKHAU.AppearanceCell.Options.UseFont = true;
            this.CHIETKHAU.AppearanceCell.Options.UseTextOptions = true;
            this.CHIETKHAU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CHIETKHAU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHIETKHAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.CHIETKHAU.AppearanceHeader.Options.UseFont = true;
            this.CHIETKHAU.AppearanceHeader.Options.UseTextOptions = true;
            this.CHIETKHAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHIETKHAU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CHIETKHAU.Caption = "CK";
            this.CHIETKHAU.DisplayFormat.FormatString = "{0:#,###0}%";
            this.CHIETKHAU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CHIETKHAU.FieldName = "CHIETKHAU";
            this.CHIETKHAU.Name = "CHIETKHAU";
            this.CHIETKHAU.OptionsColumn.AllowEdit = false;
            this.CHIETKHAU.OptionsColumn.AllowFocus = false;
            this.CHIETKHAU.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.CHIETKHAU.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.CHIETKHAU.OptionsColumn.FixedWidth = true;
            this.CHIETKHAU.OptionsFilter.AllowFilter = false;
            this.CHIETKHAU.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.CHIETKHAU.Visible = true;
            this.CHIETKHAU.VisibleIndex = 3;
            this.CHIETKHAU.Width = 30;
            // 
            // TheMoney
            // 
            this.TheMoney.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TheMoney.AppearanceCell.Options.UseFont = true;
            this.TheMoney.AppearanceCell.Options.UseTextOptions = true;
            this.TheMoney.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TheMoney.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TheMoney.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.TheMoney.AppearanceHeader.Options.UseFont = true;
            this.TheMoney.AppearanceHeader.Options.UseTextOptions = true;
            this.TheMoney.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TheMoney.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TheMoney.Caption = "T Tiền";
            this.TheMoney.DisplayFormat.FormatString = "{0:#,###0}";
            this.TheMoney.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TheMoney.FieldName = "Thanh_Tien";
            this.TheMoney.Name = "TheMoney";
            this.TheMoney.OptionsColumn.AllowEdit = false;
            this.TheMoney.OptionsColumn.AllowFocus = false;
            this.TheMoney.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.TheMoney.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TheMoney.OptionsColumn.FixedWidth = true;
            this.TheMoney.OptionsFilter.AllowFilter = false;
            this.TheMoney.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.TheMoney.Visible = true;
            this.TheMoney.VisibleIndex = 4;
            this.TheMoney.Width = 58;
            // 
            // MA_HANGHOA
            // 
            this.MA_HANGHOA.Caption = "Mã Hàng Hóa";
            this.MA_HANGHOA.FieldName = "MA_HANGHOA";
            this.MA_HANGHOA.Name = "MA_HANGHOA";
            this.MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this.MA_HANGHOA.OptionsColumn.AllowFocus = false;
            // 
            // GHICHU
            // 
            this.GHICHU.Caption = "Ghi Chú";
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.Name = "GHICHU";
            // 
            // MA_BEP
            // 
            this.MA_BEP.Caption = "Máy In Bếp";
            this.MA_BEP.FieldName = "MA_BEP";
            this.MA_BEP.Name = "MA_BEP";
            // 
            // THUE
            // 
            this.THUE.Caption = "VAT";
            this.THUE.FieldName = "THUE";
            this.THUE.Name = "THUE";
            // 
            // Discount
            // 
            this.Discount.Caption = "Giảm Giá";
            this.Discount.FieldName = "GiamGia";
            this.Discount.Name = "Discount";
            // 
            // IS_COMBO
            // 
            this.IS_COMBO.Caption = "IS_COMBO";
            this.IS_COMBO.FieldName = "IS_COMBO";
            this.IS_COMBO.Name = "IS_COMBO";
            // 
            // IS_INBEP
            // 
            this.IS_INBEP.Caption = "IS_INBEP";
            this.IS_INBEP.FieldName = "IS_INBEP";
            this.IS_INBEP.Name = "IS_INBEP";
            // 
            // MONTHEM
            // 
            this.MONTHEM.Caption = "MONTHEM";
            this.MONTHEM.FieldName = "MONTHEM";
            this.MONTHEM.Name = "MONTHEM";
            this.MONTHEM.Width = 20;
            // 
            // TEM
            // 
            this.TEM.Caption = "TEM";
            this.TEM.FieldName = "TEM";
            this.TEM.Name = "TEM";
            this.TEM.Width = 20;
            // 
            // MACP
            // 
            this.MACP.Caption = "MACP";
            this.MACP.FieldName = "MACP";
            this.MACP.Name = "MACP";
            this.MACP.Width = 20;
            // 
            // MACB
            // 
            this.MACB.Caption = "MACB";
            this.MACB.FieldName = "MACB";
            this.MACB.Name = "MACB";
            // 
            // pnUpDownGridview
            // 
            this.pnUpDownGridview.Controls.Add(this.btnUpGridview);
            this.pnUpDownGridview.Controls.Add(this.btnDownGridview);
            this.pnUpDownGridview.Controls.Add(this.btnLastGridview);
            this.pnUpDownGridview.Controls.Add(this.btnFirstGridview);
            this.pnUpDownGridview.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnUpDownGridview.Location = new System.Drawing.Point(336, 2);
            this.pnUpDownGridview.Name = "pnUpDownGridview";
            this.pnUpDownGridview.Size = new System.Drawing.Size(39, 172);
            this.pnUpDownGridview.TabIndex = 21;
            this.pnUpDownGridview.Visible = false;
            // 
            // btnUpGridview
            // 
            this.btnUpGridview.Image = ((System.Drawing.Image)(resources.GetObject("btnUpGridview.Image")));
            this.btnUpGridview.Location = new System.Drawing.Point(4, 71);
            this.btnUpGridview.Name = "btnUpGridview";
            this.btnUpGridview.Size = new System.Drawing.Size(31, 53);
            this.btnUpGridview.TabIndex = 4;
            this.btnUpGridview.Text = "simpleButton7";
            this.btnUpGridview.Click += new System.EventHandler(this.btnUpGridview_Click);
            // 
            // btnDownGridview
            // 
            this.btnDownGridview.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDownGridview.Image = ((System.Drawing.Image)(resources.GetObject("btnDownGridview.Image")));
            this.btnDownGridview.Location = new System.Drawing.Point(4, 47);
            this.btnDownGridview.Name = "btnDownGridview";
            this.btnDownGridview.Size = new System.Drawing.Size(31, 53);
            this.btnDownGridview.TabIndex = 3;
            this.btnDownGridview.Click += new System.EventHandler(this.btnDownGridview_Click);
            // 
            // btnLastGridview
            // 
            this.btnLastGridview.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLastGridview.Image = ((System.Drawing.Image)(resources.GetObject("btnLastGridview.Image")));
            this.btnLastGridview.Location = new System.Drawing.Point(4, 109);
            this.btnLastGridview.Name = "btnLastGridview";
            this.btnLastGridview.Size = new System.Drawing.Size(31, 53);
            this.btnLastGridview.TabIndex = 2;
            this.btnLastGridview.Text = "simpleButton4";
            this.btnLastGridview.Click += new System.EventHandler(this.btnLastGridview_Click);
            // 
            // btnFirstGridview
            // 
            this.btnFirstGridview.Image = ((System.Drawing.Image)(resources.GetObject("btnFirstGridview.Image")));
            this.btnFirstGridview.Location = new System.Drawing.Point(4, 9);
            this.btnFirstGridview.Name = "btnFirstGridview";
            this.btnFirstGridview.Size = new System.Drawing.Size(31, 53);
            this.btnFirstGridview.TabIndex = 0;
            this.btnFirstGridview.Text = "simpleButton1";
            this.btnFirstGridview.Click += new System.EventHandler(this.btnFirstGridview_Click);
            // 
            // pnlInforMoneyAndCustomer
            // 
            this.pnlInforMoneyAndCustomer.Controls.Add(this.pnlInforCustomer);
            this.pnlInforMoneyAndCustomer.Controls.Add(this.pnlInforMoney);
            this.pnlInforMoneyAndCustomer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlInforMoneyAndCustomer.Location = new System.Drawing.Point(2, 174);
            this.pnlInforMoneyAndCustomer.Name = "pnlInforMoneyAndCustomer";
            this.pnlInforMoneyAndCustomer.Size = new System.Drawing.Size(373, 68);
            this.pnlInforMoneyAndCustomer.TabIndex = 0;
            // 
            // pnlInforCustomer
            // 
            this.pnlInforCustomer.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlInforCustomer.Controls.Add(this.pnlInnerCustomerShow);
            this.pnlInforCustomer.Controls.Add(this.pnlDearCustomerShow);
            this.pnlInforCustomer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlInforCustomer.Location = new System.Drawing.Point(2, 2);
            this.pnlInforCustomer.Name = "pnlInforCustomer";
            this.pnlInforCustomer.Size = new System.Drawing.Size(220, 64);
            this.pnlInforCustomer.TabIndex = 16;
            // 
            // pnlInnerCustomerShow
            // 
            this.pnlInnerCustomerShow.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlInnerCustomerShow.Controls.Add(this.lbMoneySurplus);
            this.pnlInnerCustomerShow.Controls.Add(this.lbIdInnerCustomerShow);
            this.pnlInnerCustomerShow.Controls.Add(this.lbNameInnerCustomer);
            this.pnlInnerCustomerShow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlInnerCustomerShow.Location = new System.Drawing.Point(0, 32);
            this.pnlInnerCustomerShow.Name = "pnlInnerCustomerShow";
            this.pnlInnerCustomerShow.Size = new System.Drawing.Size(220, 32);
            this.pnlInnerCustomerShow.TabIndex = 1;
            this.pnlInnerCustomerShow.Visible = false;
            // 
            // lbMoneySurplus
            // 
            this.lbMoneySurplus.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbMoneySurplus.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbMoneySurplus.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbMoneySurplus.Location = new System.Drawing.Point(174, 16);
            this.lbMoneySurplus.Name = "lbMoneySurplus";
            this.lbMoneySurplus.Padding = new System.Windows.Forms.Padding(0, 0, 15, 5);
            this.lbMoneySurplus.Size = new System.Drawing.Size(46, 18);
            this.lbMoneySurplus.TabIndex = 23;
            this.lbMoneySurplus.Text = "số dư";
            // 
            // lbIdInnerCustomerShow
            // 
            this.lbIdInnerCustomerShow.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbIdInnerCustomerShow.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbIdInnerCustomerShow.Location = new System.Drawing.Point(3, 18);
            this.lbIdInnerCustomerShow.Name = "lbIdInnerCustomerShow";
            this.lbIdInnerCustomerShow.Size = new System.Drawing.Size(0, 13);
            this.lbIdInnerCustomerShow.TabIndex = 21;
            this.lbIdInnerCustomerShow.Visible = false;
            // 
            // lbNameInnerCustomer
            // 
            this.lbNameInnerCustomer.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbNameInnerCustomer.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbNameInnerCustomer.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbNameInnerCustomer.Location = new System.Drawing.Point(0, 0);
            this.lbNameInnerCustomer.Name = "lbNameInnerCustomer";
            this.lbNameInnerCustomer.Padding = new System.Windows.Forms.Padding(5, 3, 0, 0);
            this.lbNameInnerCustomer.Size = new System.Drawing.Size(81, 16);
            this.lbNameInnerCustomer.TabIndex = 19;
            this.lbNameInnerCustomer.Text = "Lệnh Hồ Xuân";
            // 
            // pnlDearCustomerShow
            // 
            this.pnlDearCustomerShow.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlDearCustomerShow.Controls.Add(this.lbMark);
            this.pnlDearCustomerShow.Controls.Add(this.lbIDDearCustomerShow);
            this.pnlDearCustomerShow.Controls.Add(this.lbNameDearCustomer);
            this.pnlDearCustomerShow.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDearCustomerShow.Location = new System.Drawing.Point(0, 0);
            this.pnlDearCustomerShow.Name = "pnlDearCustomerShow";
            this.pnlDearCustomerShow.Size = new System.Drawing.Size(220, 32);
            this.pnlDearCustomerShow.TabIndex = 0;
            this.pnlDearCustomerShow.Visible = false;
            // 
            // lbMark
            // 
            this.lbMark.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbMark.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbMark.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbMark.Location = new System.Drawing.Point(175, 16);
            this.lbMark.Name = "lbMark";
            this.lbMark.Padding = new System.Windows.Forms.Padding(0, 0, 15, 5);
            this.lbMark.Size = new System.Drawing.Size(45, 18);
            this.lbMark.TabIndex = 18;
            this.lbMark.Text = "Điểm";
            // 
            // lbIDDearCustomerShow
            // 
            this.lbIDDearCustomerShow.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbIDDearCustomerShow.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbIDDearCustomerShow.Location = new System.Drawing.Point(3, 18);
            this.lbIDDearCustomerShow.Name = "lbIDDearCustomerShow";
            this.lbIDDearCustomerShow.Size = new System.Drawing.Size(0, 13);
            this.lbIDDearCustomerShow.TabIndex = 16;
            this.lbIDDearCustomerShow.Visible = false;
            // 
            // lbNameDearCustomer
            // 
            this.lbNameDearCustomer.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbNameDearCustomer.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbNameDearCustomer.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbNameDearCustomer.Location = new System.Drawing.Point(0, 0);
            this.lbNameDearCustomer.Name = "lbNameDearCustomer";
            this.lbNameDearCustomer.Padding = new System.Windows.Forms.Padding(5, 3, 0, 0);
            this.lbNameDearCustomer.Size = new System.Drawing.Size(81, 16);
            this.lbNameDearCustomer.TabIndex = 14;
            this.lbNameDearCustomer.Text = "Lệnh Hồ Xuân";
            // 
            // pnlInforMoney
            // 
            this.pnlInforMoney.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlInforMoney.Controls.Add(this.panelControl7);
            this.pnlInforMoney.Controls.Add(this.panelControl5);
            this.pnlInforMoney.Controls.Add(this.panelControl1);
            this.pnlInforMoney.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlInforMoney.Location = new System.Drawing.Point(222, 2);
            this.pnlInforMoney.Name = "pnlInforMoney";
            this.pnlInforMoney.Size = new System.Drawing.Size(149, 64);
            this.pnlInforMoney.TabIndex = 15;
            // 
            // panelControl7
            // 
            this.panelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl7.Controls.Add(this.lbTotalMoney);
            this.panelControl7.Controls.Add(this.lb_tongcong);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(0, 42);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Padding = new System.Windows.Forms.Padding(0, 5, 10, 0);
            this.panelControl7.Size = new System.Drawing.Size(149, 21);
            this.panelControl7.TabIndex = 40;
            // 
            // lbTotalMoney
            // 
            this.lbTotalMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbTotalMoney.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbTotalMoney.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbTotalMoney.Location = new System.Drawing.Point(132, 5);
            this.lbTotalMoney.Margin = new System.Windows.Forms.Padding(0);
            this.lbTotalMoney.Name = "lbTotalMoney";
            this.lbTotalMoney.Size = new System.Drawing.Size(7, 13);
            this.lbTotalMoney.TabIndex = 9;
            this.lbTotalMoney.Text = "0";
            this.lbTotalMoney.TextChanged += new System.EventHandler(this.lbTotalMoney_TextChanged);
            // 
            // lb_tongcong
            // 
            this.lb_tongcong.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_tongcong.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_tongcong.Dock = System.Windows.Forms.DockStyle.Left;
            this.lb_tongcong.Location = new System.Drawing.Point(0, 5);
            this.lb_tongcong.Name = "lb_tongcong";
            this.lb_tongcong.Size = new System.Drawing.Size(64, 13);
            this.lb_tongcong.TabIndex = 8;
            this.lb_tongcong.Text = "Tổng cộng  ";
            // 
            // panelControl5
            // 
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl5.Controls.Add(this.lb_tiengio);
            this.panelControl5.Controls.Add(this.lbHourMoney);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl5.Location = new System.Drawing.Point(0, 21);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Padding = new System.Windows.Forms.Padding(0, 5, 10, 0);
            this.panelControl5.Size = new System.Drawing.Size(149, 21);
            this.panelControl5.TabIndex = 39;
            // 
            // lb_tiengio
            // 
            this.lb_tiengio.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_tiengio.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_tiengio.Dock = System.Windows.Forms.DockStyle.Left;
            this.lb_tiengio.Location = new System.Drawing.Point(0, 5);
            this.lb_tiengio.Name = "lb_tiengio";
            this.lb_tiengio.Size = new System.Drawing.Size(44, 13);
            this.lb_tiengio.TabIndex = 11;
            this.lb_tiengio.Text = "Tiền giờ";
            // 
            // lbHourMoney
            // 
            this.lbHourMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbHourMoney.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbHourMoney.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbHourMoney.Location = new System.Drawing.Point(132, 5);
            this.lbHourMoney.Margin = new System.Windows.Forms.Padding(0);
            this.lbHourMoney.Name = "lbHourMoney";
            this.lbHourMoney.Size = new System.Drawing.Size(7, 13);
            this.lbHourMoney.TabIndex = 12;
            this.lbHourMoney.Tag = "0";
            this.lbHourMoney.Text = "0";
            this.lbHourMoney.TextChanged += new System.EventHandler(this.lbHourMoney_TextChanged);
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.lbTheMoney);
            this.panelControl1.Controls.Add(this.lb_thanhtien);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Padding = new System.Windows.Forms.Padding(0, 5, 10, 0);
            this.panelControl1.Size = new System.Drawing.Size(149, 21);
            this.panelControl1.TabIndex = 38;
            // 
            // lbTheMoney
            // 
            this.lbTheMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbTheMoney.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbTheMoney.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbTheMoney.Location = new System.Drawing.Point(132, 5);
            this.lbTheMoney.Margin = new System.Windows.Forms.Padding(0);
            this.lbTheMoney.Name = "lbTheMoney";
            this.lbTheMoney.Size = new System.Drawing.Size(7, 13);
            this.lbTheMoney.TabIndex = 14;
            this.lbTheMoney.Text = "0";
            this.lbTheMoney.TextChanged += new System.EventHandler(this.lbTheMoney_TextChanged);
            // 
            // lb_thanhtien
            // 
            this.lb_thanhtien.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_thanhtien.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lb_thanhtien.Dock = System.Windows.Forms.DockStyle.Left;
            this.lb_thanhtien.Location = new System.Drawing.Point(0, 5);
            this.lb_thanhtien.Name = "lb_thanhtien";
            this.lb_thanhtien.Size = new System.Drawing.Size(62, 13);
            this.lb_thanhtien.TabIndex = 13;
            this.lb_thanhtien.Text = "Thành Tiền";
            // 
            // pnlActionCashier
            // 
            this.pnlActionCashier.Controls.Add(this.pnlPayment);
            this.pnlActionCashier.Controls.Add(this.pnDacThu);
            this.pnlActionCashier.Controls.Add(this.pnlPaymentButton);
            this.pnlActionCashier.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlActionCashier.Location = new System.Drawing.Point(0, 244);
            this.pnlActionCashier.Name = "pnlActionCashier";
            this.pnlActionCashier.Size = new System.Drawing.Size(377, 298);
            this.pnlActionCashier.TabIndex = 3;
            // 
            // pnDacThu
            // 
            this.pnDacThu.Controls.Add(this.btnCongNo);
            this.pnDacThu.Controls.Add(this.btnNhanvienthuchien);
            this.pnDacThu.Controls.Add(this.btnKhachhang);
            this.pnDacThu.Controls.Add(this.btnGhichu);
            this.pnDacThu.Controls.Add(this.btnGiaohang);
            this.pnDacThu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnDacThu.Location = new System.Drawing.Point(2, 43);
            this.pnDacThu.Name = "pnDacThu";
            this.pnDacThu.Size = new System.Drawing.Size(373, 253);
            this.pnDacThu.TabIndex = 21;
            // 
            // btnCongNo
            // 
            this.btnCongNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnCongNo.Appearance.Options.UseFont = true;
            this.btnCongNo.Appearance.Options.UseTextOptions = true;
            this.btnCongNo.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnCongNo.Location = new System.Drawing.Point(283, 6);
            this.btnCongNo.Name = "btnCongNo";
            this.btnCongNo.Size = new System.Drawing.Size(80, 75);
            this.btnCongNo.TabIndex = 35;
            this.btnCongNo.Text = "Công Nợ";
            this.btnCongNo.Click += new System.EventHandler(this.btnCongNo_Click);
            // 
            // btnNhanvienthuchien
            // 
            this.btnNhanvienthuchien.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            this.btnNhanvienthuchien.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnNhanvienthuchien.Appearance.Options.UseBackColor = true;
            this.btnNhanvienthuchien.Appearance.Options.UseFont = true;
            this.btnNhanvienthuchien.Appearance.Options.UseTextOptions = true;
            this.btnNhanvienthuchien.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnNhanvienthuchien.Location = new System.Drawing.Point(191, 6);
            this.btnNhanvienthuchien.Name = "btnNhanvienthuchien";
            this.btnNhanvienthuchien.Size = new System.Drawing.Size(80, 75);
            this.btnNhanvienthuchien.TabIndex = 31;
            this.btnNhanvienthuchien.Text = "Nhân Viên Thực Hiện";
            this.btnNhanvienthuchien.Click += new System.EventHandler(this.btnNhanvienthuchien_Click);
            // 
            // btnKhachhang
            // 
            this.btnKhachhang.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            this.btnKhachhang.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnKhachhang.Appearance.Options.UseBackColor = true;
            this.btnKhachhang.Appearance.Options.UseFont = true;
            this.btnKhachhang.Appearance.Options.UseTextOptions = true;
            this.btnKhachhang.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnKhachhang.Location = new System.Drawing.Point(6, 6);
            this.btnKhachhang.Name = "btnKhachhang";
            this.btnKhachhang.Size = new System.Drawing.Size(80, 75);
            this.btnKhachhang.TabIndex = 29;
            this.btnKhachhang.Text = "Khách Hàng";
            this.btnKhachhang.Click += new System.EventHandler(this.btnKhachhang_Click);
            // 
            // btnGhichu
            // 
            this.btnGhichu.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            this.btnGhichu.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnGhichu.Appearance.Options.UseBackColor = true;
            this.btnGhichu.Appearance.Options.UseFont = true;
            this.btnGhichu.Appearance.Options.UseTextOptions = true;
            this.btnGhichu.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnGhichu.Location = new System.Drawing.Point(100, 6);
            this.btnGhichu.Name = "btnGhichu";
            this.btnGhichu.Size = new System.Drawing.Size(80, 75);
            this.btnGhichu.TabIndex = 21;
            this.btnGhichu.Text = "Ghi Chú";
            this.btnGhichu.Click += new System.EventHandler(this.btnGhichu_Click);
            // 
            // btnGiaohang
            // 
            this.btnGiaohang.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            this.btnGiaohang.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnGiaohang.Appearance.Options.UseBackColor = true;
            this.btnGiaohang.Appearance.Options.UseFont = true;
            this.btnGiaohang.Appearance.Options.UseTextOptions = true;
            this.btnGiaohang.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnGiaohang.Location = new System.Drawing.Point(6, 87);
            this.btnGiaohang.Name = "btnGiaohang";
            this.btnGiaohang.Size = new System.Drawing.Size(80, 75);
            this.btnGiaohang.TabIndex = 33;
            this.btnGiaohang.Text = "Giao Hàng";
            this.btnGiaohang.Click += new System.EventHandler(this.btnGiaohang_Click);
            // 
            // pnlPayment
            // 
            this.pnlPayment.Controls.Add(this.btnPhieuMuaHang);
            this.pnlPayment.Controls.Add(this.btnTachBill);
            this.pnlPayment.Controls.Add(this.btnMakhuyenmai);
            this.pnlPayment.Controls.Add(this.btnVisa);
            this.pnlPayment.Controls.Add(this.btnVoucher);
            this.pnlPayment.Controls.Add(this.btnServiceMoney);
            this.pnlPayment.Controls.Add(this.btnRetypePayment);
            this.pnlPayment.Controls.Add(this.btnDiscount);
            this.pnlPayment.Controls.Add(this.btnCardImtimateCustomer);
            this.pnlPayment.Controls.Add(this.btnCancelPayment);
            this.pnlPayment.Controls.Add(this.btnNotPrintBill);
            this.pnlPayment.Controls.Add(this.btnCardInternalPayment);
            this.pnlPayment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPayment.Location = new System.Drawing.Point(2, 43);
            this.pnlPayment.Name = "pnlPayment";
            this.pnlPayment.Size = new System.Drawing.Size(373, 253);
            this.pnlPayment.TabIndex = 20;
            // 
            // btnPhieuMuaHang
            // 
            this.btnPhieuMuaHang.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnPhieuMuaHang.Appearance.Options.UseFont = true;
            this.btnPhieuMuaHang.Appearance.Options.UseTextOptions = true;
            this.btnPhieuMuaHang.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnPhieuMuaHang.Location = new System.Drawing.Point(100, 172);
            this.btnPhieuMuaHang.Name = "btnPhieuMuaHang";
            this.btnPhieuMuaHang.Size = new System.Drawing.Size(80, 75);
            this.btnPhieuMuaHang.TabIndex = 36;
            this.btnPhieuMuaHang.Text = "Phiếu mua hàng";
            this.btnPhieuMuaHang.Click += new System.EventHandler(this.btnPhieuMuaHang_Click);
            // 
            // btnTachBill
            // 
            this.btnTachBill.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnTachBill.Appearance.Options.UseFont = true;
            this.btnTachBill.Appearance.Options.UseTextOptions = true;
            this.btnTachBill.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnTachBill.Location = new System.Drawing.Point(282, 87);
            this.btnTachBill.Name = "btnTachBill";
            this.btnTachBill.Size = new System.Drawing.Size(80, 75);
            this.btnTachBill.TabIndex = 36;
            this.btnTachBill.Text = "F4           Tách Bill";
            this.btnTachBill.Click += new System.EventHandler(this.btnPaymentTachBill_Click);
            // 
            // btnMakhuyenmai
            // 
            this.btnMakhuyenmai.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            this.btnMakhuyenmai.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnMakhuyenmai.Appearance.Options.UseBackColor = true;
            this.btnMakhuyenmai.Appearance.Options.UseFont = true;
            this.btnMakhuyenmai.Appearance.Options.UseTextOptions = true;
            this.btnMakhuyenmai.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnMakhuyenmai.Location = new System.Drawing.Point(191, 172);
            this.btnMakhuyenmai.Name = "btnMakhuyenmai";
            this.btnMakhuyenmai.Size = new System.Drawing.Size(80, 75);
            this.btnMakhuyenmai.TabIndex = 35;
            this.btnMakhuyenmai.Text = "Mã Khuyến Mãi";
            this.btnMakhuyenmai.Click += new System.EventHandler(this.btnMakhuyenmai_Click);
            // 
            // btnVisa
            // 
            this.btnVisa.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            this.btnVisa.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnVisa.Appearance.Options.UseBackColor = true;
            this.btnVisa.Appearance.Options.UseFont = true;
            this.btnVisa.Appearance.Options.UseTextOptions = true;
            this.btnVisa.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnVisa.Location = new System.Drawing.Point(191, 87);
            this.btnVisa.Name = "btnVisa";
            this.btnVisa.Size = new System.Drawing.Size(80, 75);
            this.btnVisa.TabIndex = 32;
            this.btnVisa.Text = "Thẻ Visa";
            this.btnVisa.Click += new System.EventHandler(this.btnVisa_Click);
            // 
            // btnVoucher
            // 
            this.btnVoucher.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            this.btnVoucher.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnVoucher.Appearance.Options.UseBackColor = true;
            this.btnVoucher.Appearance.Options.UseFont = true;
            this.btnVoucher.Appearance.Options.UseTextOptions = true;
            this.btnVoucher.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnVoucher.Location = new System.Drawing.Point(191, 6);
            this.btnVoucher.Name = "btnVoucher";
            this.btnVoucher.Size = new System.Drawing.Size(80, 75);
            this.btnVoucher.TabIndex = 31;
            this.btnVoucher.Text = "Voucher";
            this.btnVoucher.Click += new System.EventHandler(this.btnVoucher_Click);
            // 
            // btnServiceMoney
            // 
            this.btnServiceMoney.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            this.btnServiceMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnServiceMoney.Appearance.Options.UseBackColor = true;
            this.btnServiceMoney.Appearance.Options.UseFont = true;
            this.btnServiceMoney.Appearance.Options.UseTextOptions = true;
            this.btnServiceMoney.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnServiceMoney.Location = new System.Drawing.Point(9, 6);
            this.btnServiceMoney.Name = "btnServiceMoney";
            this.btnServiceMoney.Size = new System.Drawing.Size(80, 75);
            this.btnServiceMoney.TabIndex = 29;
            this.btnServiceMoney.Text = "Phí Phục Vụ";
            this.btnServiceMoney.Click += new System.EventHandler(this.btnServiceMoney_Click);
            // 
            // btnRetypePayment
            // 
            this.btnRetypePayment.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnRetypePayment.Appearance.Options.UseFont = true;
            this.btnRetypePayment.Appearance.Options.UseTextOptions = true;
            this.btnRetypePayment.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnRetypePayment.Location = new System.Drawing.Point(9, 172);
            this.btnRetypePayment.Name = "btnRetypePayment";
            this.btnRetypePayment.Size = new System.Drawing.Size(80, 75);
            this.btnRetypePayment.TabIndex = 28;
            this.btnRetypePayment.Text = "Nhập Lại\r\n Tiền Trả";
            this.btnRetypePayment.Click += new System.EventHandler(this.btnRetypePayment_Click);
            // 
            // btnDiscount
            // 
            this.btnDiscount.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnDiscount.Appearance.Options.UseFont = true;
            this.btnDiscount.Appearance.Options.UseTextOptions = true;
            this.btnDiscount.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnDiscount.Location = new System.Drawing.Point(9, 87);
            this.btnDiscount.Name = "btnDiscount";
            this.btnDiscount.Size = new System.Drawing.Size(80, 75);
            this.btnDiscount.TabIndex = 26;
            this.btnDiscount.Text = "Chiết Khấu";
            this.btnDiscount.Click += new System.EventHandler(this.btnDiscount_Click);
            // 
            // btnCardImtimateCustomer
            // 
            this.btnCardImtimateCustomer.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            this.btnCardImtimateCustomer.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnCardImtimateCustomer.Appearance.Options.UseBackColor = true;
            this.btnCardImtimateCustomer.Appearance.Options.UseFont = true;
            this.btnCardImtimateCustomer.Appearance.Options.UseTextOptions = true;
            this.btnCardImtimateCustomer.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnCardImtimateCustomer.Location = new System.Drawing.Point(100, 87);
            this.btnCardImtimateCustomer.Name = "btnCardImtimateCustomer";
            this.btnCardImtimateCustomer.Size = new System.Drawing.Size(80, 75);
            this.btnCardImtimateCustomer.TabIndex = 25;
            this.btnCardImtimateCustomer.Text = "KHTT";
            this.btnCardImtimateCustomer.Click += new System.EventHandler(this.btnCardImtimateCustomer_Click);
            // 
            // btnCancelPayment
            // 
            this.btnCancelPayment.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnCancelPayment.Appearance.Options.UseFont = true;
            this.btnCancelPayment.Appearance.Options.UseTextOptions = true;
            this.btnCancelPayment.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnCancelPayment.Location = new System.Drawing.Point(282, 172);
            this.btnCancelPayment.Name = "btnCancelPayment";
            this.btnCancelPayment.Size = new System.Drawing.Size(80, 75);
            this.btnCancelPayment.TabIndex = 23;
            this.btnCancelPayment.Text = "ESC - Hủy\r\nThanh Toán";
            this.btnCancelPayment.Click += new System.EventHandler(this.btnCancelPayment_Click);
            // 
            // btnNotPrintBill
            // 
            this.btnNotPrintBill.Appearance.Font = new System.Drawing.Font("Tahoma", 8.2F, System.Drawing.FontStyle.Bold);
            this.btnNotPrintBill.Appearance.Options.UseFont = true;
            this.btnNotPrintBill.Appearance.Options.UseTextOptions = true;
            this.btnNotPrintBill.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnNotPrintBill.Location = new System.Drawing.Point(282, 6);
            this.btnNotPrintBill.Name = "btnNotPrintBill";
            this.btnNotPrintBill.Size = new System.Drawing.Size(80, 75);
            this.btnNotPrintBill.TabIndex = 22;
            this.btnNotPrintBill.Text = "Không In \r\nHóa Đơn\r\n";
            this.btnNotPrintBill.Click += new System.EventHandler(this.btnNotPrintBill_Click);
            // 
            // btnCardInternalPayment
            // 
            this.btnCardInternalPayment.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            this.btnCardInternalPayment.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            this.btnCardInternalPayment.Appearance.Options.UseBackColor = true;
            this.btnCardInternalPayment.Appearance.Options.UseFont = true;
            this.btnCardInternalPayment.Appearance.Options.UseTextOptions = true;
            this.btnCardInternalPayment.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnCardInternalPayment.Location = new System.Drawing.Point(100, 6);
            this.btnCardInternalPayment.Name = "btnCardInternalPayment";
            this.btnCardInternalPayment.Size = new System.Drawing.Size(80, 75);
            this.btnCardInternalPayment.TabIndex = 21;
            this.btnCardInternalPayment.Text = "Thẻ TTNB";
            this.btnCardInternalPayment.Click += new System.EventHandler(this.btnCardInternalPayment_Click);
            // 
            // pnlPaymentButton
            // 
            this.pnlPaymentButton.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            this.pnlPaymentButton.Appearance.Options.UseBackColor = true;
            this.pnlPaymentButton.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlPaymentButton.Controls.Add(this.btn_DatThu);
            this.pnlPaymentButton.Controls.Add(this.btn_ThuNgan);
            this.pnlPaymentButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlPaymentButton.Location = new System.Drawing.Point(2, 2);
            this.pnlPaymentButton.Name = "pnlPaymentButton";
            this.pnlPaymentButton.Size = new System.Drawing.Size(373, 41);
            this.pnlPaymentButton.TabIndex = 4;
            // 
            // btn_DatThu
            // 
            this.btn_DatThu.AllowFocus = false;
            this.btn_DatThu.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.btn_DatThu.Appearance.Options.UseFont = true;
            this.btn_DatThu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_DatThu.Location = new System.Drawing.Point(173, 0);
            this.btn_DatThu.Name = "btn_DatThu";
            this.btn_DatThu.Size = new System.Drawing.Size(200, 41);
            this.btn_DatThu.TabIndex = 16;
            this.btn_DatThu.Text = "ĐẶC THÙ";
            this.btn_DatThu.Click += new System.EventHandler(this.btn_DatThu_Click);
            // 
            // btn_ThuNgan
            // 
            this.btn_ThuNgan.AllowFocus = false;
            this.btn_ThuNgan.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.btn_ThuNgan.Appearance.Options.UseFont = true;
            this.btn_ThuNgan.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_ThuNgan.Location = new System.Drawing.Point(0, 0);
            this.btn_ThuNgan.Name = "btn_ThuNgan";
            this.btn_ThuNgan.Size = new System.Drawing.Size(173, 41);
            this.btn_ThuNgan.TabIndex = 17;
            this.btn_ThuNgan.Text = "Thanh toán";
            this.btn_ThuNgan.Click += new System.EventHandler(this.btn_ThuNgan_Click);
            // 
            // pnlPaymentShowFill
            // 
            this.pnlPaymentShowFill.Controls.Add(this.pnlShowInforPayment);
            this.pnlPaymentShowFill.Controls.Add(this.pnlVisa);
            this.pnlPaymentShowFill.Controls.Add(this.pnlCongNo);
            this.pnlPaymentShowFill.Controls.Add(this.pnlBottomPayment);
            this.pnlPaymentShowFill.Controls.Add(this.pnlHeaderShowPayment);
            this.pnlPaymentShowFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPaymentShowFill.Location = new System.Drawing.Point(0, 70);
            this.pnlPaymentShowFill.Name = "pnlPaymentShowFill";
            this.pnlPaymentShowFill.Size = new System.Drawing.Size(647, 542);
            this.pnlPaymentShowFill.TabIndex = 4;
            // 
            // pnlShowInforPayment
            // 
            this.pnlShowInforPayment.Controls.Add(this.pnlPhieuMuaHang);
            this.pnlShowInforPayment.Controls.Add(this.pnlTienKhachTra);
            this.pnlShowInforPayment.Controls.Add(this.pnlPhiPhuVu);
            this.pnlShowInforPayment.Controls.Add(this.pnlTTTNB);
            this.pnlShowInforPayment.Controls.Add(this.pnlChietKhau);
            this.pnlShowInforPayment.Controls.Add(this.pnlKHTT);
            this.pnlShowInforPayment.Controls.Add(this.pnlVoucher);
            this.pnlShowInforPayment.Controls.Add(this.pnlMagiamgia);
            this.pnlShowInforPayment.Controls.Add(this.labelControl22);
            this.pnlShowInforPayment.Controls.Add(this.labelControl30);
            this.pnlShowInforPayment.Controls.Add(this.pnlMoneyReply);
            this.pnlShowInforPayment.Controls.Add(this.panelControl30);
            this.pnlShowInforPayment.Controls.Add(this.pnlFirst);
            this.pnlShowInforPayment.Controls.Add(this.pnlSecond);
            this.pnlShowInforPayment.Controls.Add(this.pnlThird);
            this.pnlShowInforPayment.Controls.Add(this.panelControl29);
            this.pnlShowInforPayment.Controls.Add(this.panelControl55);
            this.pnlShowInforPayment.Controls.Add(this.panelControl51);
            this.pnlShowInforPayment.Controls.Add(this.panelControl27);
            this.pnlShowInforPayment.Controls.Add(this.panelControl31);
            this.pnlShowInforPayment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlShowInforPayment.Location = new System.Drawing.Point(2, 37);
            this.pnlShowInforPayment.Name = "pnlShowInforPayment";
            this.pnlShowInforPayment.Size = new System.Drawing.Size(643, 205);
            this.pnlShowInforPayment.TabIndex = 36;
            // 
            // pnlPhieuMuaHang
            // 
            this.pnlPhieuMuaHang.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlPhieuMuaHang.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlPhieuMuaHang.Controls.Add(this.txtNhapPhieuMuaHang);
            this.pnlPhieuMuaHang.Controls.Add(this.lbNhapPhieuMuaHang);
            this.pnlPhieuMuaHang.Location = new System.Drawing.Point(51, 156);
            this.pnlPhieuMuaHang.Name = "pnlPhieuMuaHang";
            this.pnlPhieuMuaHang.Size = new System.Drawing.Size(508, 47);
            this.pnlPhieuMuaHang.TabIndex = 55;
            this.pnlPhieuMuaHang.Visible = false;
            // 
            // txtNhapPhieuMuaHang
            // 
            this.txtNhapPhieuMuaHang.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNhapPhieuMuaHang.EditValue = "0";
            this.txtNhapPhieuMuaHang.Location = new System.Drawing.Point(174, 7);
            this.txtNhapPhieuMuaHang.Name = "txtNhapPhieuMuaHang";
            this.txtNhapPhieuMuaHang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.txtNhapPhieuMuaHang.Properties.Appearance.Options.UseFont = true;
            this.txtNhapPhieuMuaHang.Properties.Appearance.Options.UseTextOptions = true;
            this.txtNhapPhieuMuaHang.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtNhapPhieuMuaHang.Properties.Mask.EditMask = "N0";
            this.txtNhapPhieuMuaHang.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtNhapPhieuMuaHang.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtNhapPhieuMuaHang.Size = new System.Drawing.Size(253, 32);
            this.txtNhapPhieuMuaHang.TabIndex = 45;
            this.txtNhapPhieuMuaHang.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNhapPhieuMuaHang_KeyPress);
            // 
            // lbNhapPhieuMuaHang
            // 
            this.lbNhapPhieuMuaHang.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbNhapPhieuMuaHang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNhapPhieuMuaHang.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lbNhapPhieuMuaHang.Location = new System.Drawing.Point(5, 13);
            this.lbNhapPhieuMuaHang.Name = "lbNhapPhieuMuaHang";
            this.lbNhapPhieuMuaHang.Size = new System.Drawing.Size(177, 19);
            this.lbNhapPhieuMuaHang.TabIndex = 44;
            this.lbNhapPhieuMuaHang.Text = "Nhập phiếu mua hàng";
            // 
            // pnlTienKhachTra
            // 
            this.pnlTienKhachTra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlTienKhachTra.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlTienKhachTra.Controls.Add(this.lbcurrencyGetMoney);
            this.pnlTienKhachTra.Controls.Add(this.txtInputMoney);
            this.pnlTienKhachTra.Controls.Add(this.lbPayment);
            this.pnlTienKhachTra.Location = new System.Drawing.Point(51, 155);
            this.pnlTienKhachTra.Name = "pnlTienKhachTra";
            this.pnlTienKhachTra.Size = new System.Drawing.Size(508, 47);
            this.pnlTienKhachTra.TabIndex = 46;
            // 
            // lbcurrencyGetMoney
            // 
            this.lbcurrencyGetMoney.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbcurrencyGetMoney.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbcurrencyGetMoney.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbcurrencyGetMoney.Location = new System.Drawing.Point(465, 0);
            this.lbcurrencyGetMoney.Name = "lbcurrencyGetMoney";
            this.lbcurrencyGetMoney.Padding = new System.Windows.Forms.Padding(0, 15, 0, 0);
            this.lbcurrencyGetMoney.Size = new System.Drawing.Size(43, 34);
            this.lbcurrencyGetMoney.TabIndex = 46;
            this.lbcurrencyGetMoney.Text = "  VND";
            // 
            // txtInputMoney
            // 
            this.txtInputMoney.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInputMoney.EditValue = "0";
            this.txtInputMoney.Location = new System.Drawing.Point(229, 7);
            this.txtInputMoney.Name = "txtInputMoney";
            this.txtInputMoney.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.txtInputMoney.Properties.Appearance.Options.UseFont = true;
            this.txtInputMoney.Properties.Appearance.Options.UseTextOptions = true;
            this.txtInputMoney.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtInputMoney.Properties.Mask.EditMask = "N0";
            this.txtInputMoney.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtInputMoney.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtInputMoney.Size = new System.Drawing.Size(198, 32);
            this.txtInputMoney.TabIndex = 45;
            this.txtInputMoney.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInputMoney_KeyPress);
            // 
            // lbPayment
            // 
            this.lbPayment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbPayment.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPayment.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lbPayment.Location = new System.Drawing.Point(5, 13);
            this.lbPayment.Name = "lbPayment";
            this.lbPayment.Size = new System.Drawing.Size(108, 19);
            this.lbPayment.TabIndex = 44;
            this.lbPayment.Text = "Nhập tiền trả";
            // 
            // pnlPhiPhuVu
            // 
            this.pnlPhiPhuVu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlPhiPhuVu.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlPhiPhuVu.Controls.Add(this.lbcurrencyService);
            this.pnlPhiPhuVu.Controls.Add(this.txtServiceMoney);
            this.pnlPhiPhuVu.Controls.Add(this.lb_nhaptienphucvu);
            this.pnlPhiPhuVu.Location = new System.Drawing.Point(51, 155);
            this.pnlPhiPhuVu.Name = "pnlPhiPhuVu";
            this.pnlPhiPhuVu.Size = new System.Drawing.Size(508, 47);
            this.pnlPhiPhuVu.TabIndex = 48;
            this.pnlPhiPhuVu.Visible = false;
            // 
            // lbcurrencyService
            // 
            this.lbcurrencyService.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbcurrencyService.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbcurrencyService.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbcurrencyService.Location = new System.Drawing.Point(465, 0);
            this.lbcurrencyService.Name = "lbcurrencyService";
            this.lbcurrencyService.Padding = new System.Windows.Forms.Padding(0, 15, 0, 0);
            this.lbcurrencyService.Size = new System.Drawing.Size(43, 34);
            this.lbcurrencyService.TabIndex = 46;
            this.lbcurrencyService.Text = "  VND";
            // 
            // txtServiceMoney
            // 
            this.txtServiceMoney.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtServiceMoney.EditValue = "0";
            this.txtServiceMoney.Location = new System.Drawing.Point(174, 7);
            this.txtServiceMoney.Name = "txtServiceMoney";
            this.txtServiceMoney.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.txtServiceMoney.Properties.Appearance.Options.UseFont = true;
            this.txtServiceMoney.Properties.Appearance.Options.UseTextOptions = true;
            this.txtServiceMoney.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtServiceMoney.Properties.Mask.EditMask = "N0";
            this.txtServiceMoney.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtServiceMoney.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtServiceMoney.Size = new System.Drawing.Size(253, 32);
            this.txtServiceMoney.TabIndex = 45;
            this.txtServiceMoney.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtServiceMoney_KeyPress);
            // 
            // lb_nhaptienphucvu
            // 
            this.lb_nhaptienphucvu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_nhaptienphucvu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_nhaptienphucvu.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lb_nhaptienphucvu.Location = new System.Drawing.Point(5, 13);
            this.lb_nhaptienphucvu.Name = "lb_nhaptienphucvu";
            this.lb_nhaptienphucvu.Size = new System.Drawing.Size(146, 19);
            this.lb_nhaptienphucvu.TabIndex = 44;
            this.lb_nhaptienphucvu.Text = "Nhập tiền phục vụ";
            // 
            // pnlTTTNB
            // 
            this.pnlTTTNB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlTTTNB.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlTTTNB.Controls.Add(this.txtInnerCustomer);
            this.pnlTTTNB.Controls.Add(this.lb_nhapmaTTNB);
            this.pnlTTTNB.Location = new System.Drawing.Point(51, 155);
            this.pnlTTTNB.Name = "pnlTTTNB";
            this.pnlTTTNB.Size = new System.Drawing.Size(508, 47);
            this.pnlTTTNB.TabIndex = 50;
            // 
            // txtInnerCustomer
            // 
            this.txtInnerCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInnerCustomer.EditValue = "";
            this.txtInnerCustomer.Location = new System.Drawing.Point(174, 7);
            this.txtInnerCustomer.Name = "txtInnerCustomer";
            this.txtInnerCustomer.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.txtInnerCustomer.Properties.Appearance.Options.UseFont = true;
            this.txtInnerCustomer.Properties.Appearance.Options.UseTextOptions = true;
            this.txtInnerCustomer.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtInnerCustomer.Properties.PasswordChar = '*';
            this.txtInnerCustomer.Size = new System.Drawing.Size(253, 32);
            this.txtInnerCustomer.TabIndex = 45;
            this.txtInnerCustomer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInnerCustomer_KeyPress);
            // 
            // lb_nhapmaTTNB
            // 
            this.lb_nhapmaTTNB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_nhapmaTTNB.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_nhapmaTTNB.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lb_nhapmaTTNB.Location = new System.Drawing.Point(5, 13);
            this.lb_nhapmaTTNB.Name = "lb_nhapmaTTNB";
            this.lb_nhapmaTTNB.Size = new System.Drawing.Size(127, 19);
            this.lb_nhapmaTTNB.TabIndex = 44;
            this.lb_nhapmaTTNB.Text = "Nhập mã TTNB ";
            // 
            // pnlChietKhau
            // 
            this.pnlChietKhau.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlChietKhau.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlChietKhau.Controls.Add(this.txtInputDiscount);
            this.pnlChietKhau.Controls.Add(this.lb_nhapchietkhau);
            this.pnlChietKhau.Location = new System.Drawing.Point(51, 155);
            this.pnlChietKhau.Name = "pnlChietKhau";
            this.pnlChietKhau.Size = new System.Drawing.Size(508, 47);
            this.pnlChietKhau.TabIndex = 47;
            this.pnlChietKhau.Visible = false;
            // 
            // txtInputDiscount
            // 
            this.txtInputDiscount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInputDiscount.EditValue = "0";
            this.txtInputDiscount.Location = new System.Drawing.Point(269, 7);
            this.txtInputDiscount.Name = "txtInputDiscount";
            this.txtInputDiscount.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.txtInputDiscount.Properties.Appearance.Options.UseFont = true;
            this.txtInputDiscount.Properties.Appearance.Options.UseTextOptions = true;
            this.txtInputDiscount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtInputDiscount.Properties.Mask.EditMask = "N0";
            this.txtInputDiscount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtInputDiscount.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtInputDiscount.Size = new System.Drawing.Size(158, 32);
            this.txtInputDiscount.TabIndex = 45;
            this.txtInputDiscount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInputDiscount_KeyPress);
            // 
            // lb_nhapchietkhau
            // 
            this.lb_nhapchietkhau.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_nhapchietkhau.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_nhapchietkhau.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lb_nhapchietkhau.Location = new System.Drawing.Point(5, 13);
            this.lb_nhapchietkhau.Name = "lb_nhapchietkhau";
            this.lb_nhapchietkhau.Size = new System.Drawing.Size(132, 19);
            this.lb_nhapchietkhau.TabIndex = 44;
            this.lb_nhapchietkhau.Text = "Nhập chiết khấu";
            // 
            // pnlKHTT
            // 
            this.pnlKHTT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlKHTT.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlKHTT.Controls.Add(this.txtDearCustomer);
            this.pnlKHTT.Controls.Add(this.lb_nhapma_KHTT);
            this.pnlKHTT.Location = new System.Drawing.Point(51, 155);
            this.pnlKHTT.Name = "pnlKHTT";
            this.pnlKHTT.Size = new System.Drawing.Size(508, 47);
            this.pnlKHTT.TabIndex = 47;
            // 
            // txtDearCustomer
            // 
            this.txtDearCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDearCustomer.EditValue = "";
            this.txtDearCustomer.Location = new System.Drawing.Point(175, 7);
            this.txtDearCustomer.Name = "txtDearCustomer";
            this.txtDearCustomer.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.txtDearCustomer.Properties.Appearance.Options.UseFont = true;
            this.txtDearCustomer.Properties.Appearance.Options.UseTextOptions = true;
            this.txtDearCustomer.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtDearCustomer.Properties.PasswordChar = '*';
            this.txtDearCustomer.Size = new System.Drawing.Size(252, 32);
            this.txtDearCustomer.TabIndex = 45;
            this.txtDearCustomer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDearCustomer_KeyPress);
            // 
            // lb_nhapma_KHTT
            // 
            this.lb_nhapma_KHTT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_nhapma_KHTT.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_nhapma_KHTT.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lb_nhapma_KHTT.Location = new System.Drawing.Point(5, 13);
            this.lb_nhapma_KHTT.Name = "lb_nhapma_KHTT";
            this.lb_nhapma_KHTT.Size = new System.Drawing.Size(127, 19);
            this.lb_nhapma_KHTT.TabIndex = 44;
            this.lb_nhapma_KHTT.Text = "Nhập mã KHTT ";
            // 
            // pnlVoucher
            // 
            this.pnlVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlVoucher.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlVoucher.Controls.Add(this.txtInputVoucher);
            this.pnlVoucher.Controls.Add(this.lb_nhapVoucher);
            this.pnlVoucher.Location = new System.Drawing.Point(51, 156);
            this.pnlVoucher.Name = "pnlVoucher";
            this.pnlVoucher.Size = new System.Drawing.Size(508, 47);
            this.pnlVoucher.TabIndex = 48;
            this.pnlVoucher.Visible = false;
            // 
            // txtInputVoucher
            // 
            this.txtInputVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInputVoucher.EditValue = "";
            this.txtInputVoucher.Location = new System.Drawing.Point(269, 7);
            this.txtInputVoucher.Name = "txtInputVoucher";
            this.txtInputVoucher.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.txtInputVoucher.Properties.Appearance.Options.UseFont = true;
            this.txtInputVoucher.Properties.Appearance.Options.UseTextOptions = true;
            this.txtInputVoucher.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtInputVoucher.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtInputVoucher.Size = new System.Drawing.Size(158, 32);
            this.txtInputVoucher.TabIndex = 45;
            this.txtInputVoucher.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInputVoucher_KeyPress);
            // 
            // lb_nhapVoucher
            // 
            this.lb_nhapVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_nhapVoucher.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_nhapVoucher.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lb_nhapVoucher.Location = new System.Drawing.Point(5, 13);
            this.lb_nhapVoucher.Name = "lb_nhapVoucher";
            this.lb_nhapVoucher.Size = new System.Drawing.Size(113, 19);
            this.lb_nhapVoucher.TabIndex = 44;
            this.lb_nhapVoucher.Text = "Nhập Voucher";
            // 
            // pnlMagiamgia
            // 
            this.pnlMagiamgia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlMagiamgia.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlMagiamgia.Controls.Add(this.txtInputMagiamgia);
            this.pnlMagiamgia.Controls.Add(this.lb_nhapMagiamgia);
            this.pnlMagiamgia.Location = new System.Drawing.Point(51, 156);
            this.pnlMagiamgia.Name = "pnlMagiamgia";
            this.pnlMagiamgia.Size = new System.Drawing.Size(508, 47);
            this.pnlMagiamgia.TabIndex = 51;
            this.pnlMagiamgia.Visible = false;
            // 
            // txtInputMagiamgia
            // 
            this.txtInputMagiamgia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInputMagiamgia.EditValue = "";
            this.txtInputMagiamgia.Location = new System.Drawing.Point(269, 7);
            this.txtInputMagiamgia.Name = "txtInputMagiamgia";
            this.txtInputMagiamgia.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.txtInputMagiamgia.Properties.Appearance.Options.UseFont = true;
            this.txtInputMagiamgia.Properties.Appearance.Options.UseTextOptions = true;
            this.txtInputMagiamgia.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtInputMagiamgia.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtInputMagiamgia.Size = new System.Drawing.Size(158, 32);
            this.txtInputMagiamgia.TabIndex = 45;
            this.txtInputMagiamgia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInputMagiamgia_KeyPress);
            // 
            // lb_nhapMagiamgia
            // 
            this.lb_nhapMagiamgia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_nhapMagiamgia.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_nhapMagiamgia.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lb_nhapMagiamgia.Location = new System.Drawing.Point(5, 13);
            this.lb_nhapMagiamgia.Name = "lb_nhapMagiamgia";
            this.lb_nhapMagiamgia.Size = new System.Drawing.Size(100, 19);
            this.lb_nhapMagiamgia.TabIndex = 44;
            this.lb_nhapMagiamgia.Text = "Nhập Mã GG";
            // 
            // labelControl22
            // 
            this.labelControl22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl22.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.labelControl22.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl22.Location = new System.Drawing.Point(53, 153);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(506, 1);
            this.labelControl22.TabIndex = 41;
            // 
            // labelControl30
            // 
            this.labelControl30.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl30.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.labelControl30.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl30.Location = new System.Drawing.Point(54, 122);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(506, 1);
            this.labelControl30.TabIndex = 32;
            // 
            // pnlMoneyReply
            // 
            this.pnlMoneyReply.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlMoneyReply.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlMoneyReply.Controls.Add(this.lbReplyCustomer);
            this.pnlMoneyReply.Controls.Add(this.lbVNDReply);
            this.pnlMoneyReply.Controls.Add(this.lb_tientralai);
            this.pnlMoneyReply.Location = new System.Drawing.Point(51, 124);
            this.pnlMoneyReply.Name = "pnlMoneyReply";
            this.pnlMoneyReply.Size = new System.Drawing.Size(508, 25);
            this.pnlMoneyReply.TabIndex = 34;
            // 
            // lbReplyCustomer
            // 
            this.lbReplyCustomer.Appearance.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbReplyCustomer.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lbReplyCustomer.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbReplyCustomer.Location = new System.Drawing.Point(454, 0);
            this.lbReplyCustomer.Name = "lbReplyCustomer";
            this.lbReplyCustomer.Size = new System.Drawing.Size(11, 24);
            this.lbReplyCustomer.TabIndex = 0;
            this.lbReplyCustomer.Tag = "0";
            this.lbReplyCustomer.Text = "0";
            // 
            // lbVNDReply
            // 
            this.lbVNDReply.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVNDReply.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbVNDReply.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbVNDReply.Location = new System.Drawing.Point(465, 0);
            this.lbVNDReply.Name = "lbVNDReply";
            this.lbVNDReply.Size = new System.Drawing.Size(43, 19);
            this.lbVNDReply.TabIndex = 1;
            this.lbVNDReply.Text = "  VND";
            // 
            // lb_tientralai
            // 
            this.lb_tientralai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_tientralai.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lb_tientralai.Location = new System.Drawing.Point(5, 3);
            this.lb_tientralai.Name = "lb_tientralai";
            this.lb_tientralai.Size = new System.Drawing.Size(120, 19);
            this.lb_tientralai.TabIndex = 0;
            this.lb_tientralai.Text = "Tiền trả lại      ";
            // 
            // panelControl30
            // 
            this.panelControl30.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl30.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl30.Controls.Add(this.lbGetCustomer);
            this.panelControl30.Controls.Add(this.labelControl2);
            this.panelControl30.Controls.Add(this.lb_tienkhachtra);
            this.panelControl30.Location = new System.Drawing.Point(51, 95);
            this.panelControl30.Name = "panelControl30";
            this.panelControl30.Size = new System.Drawing.Size(508, 26);
            this.panelControl30.TabIndex = 36;
            // 
            // lbGetCustomer
            // 
            this.lbGetCustomer.Appearance.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGetCustomer.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lbGetCustomer.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbGetCustomer.Location = new System.Drawing.Point(454, 0);
            this.lbGetCustomer.Name = "lbGetCustomer";
            this.lbGetCustomer.Size = new System.Drawing.Size(11, 24);
            this.lbGetCustomer.TabIndex = 0;
            this.lbGetCustomer.Tag = "0";
            this.lbGetCustomer.Text = "0";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelControl2.Location = new System.Drawing.Point(465, 0);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(43, 19);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "  VND";
            // 
            // lb_tienkhachtra
            // 
            this.lb_tienkhachtra.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_tienkhachtra.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lb_tienkhachtra.Location = new System.Drawing.Point(5, 1);
            this.lb_tienkhachtra.Name = "lb_tienkhachtra";
            this.lb_tienkhachtra.Size = new System.Drawing.Size(118, 19);
            this.lb_tienkhachtra.TabIndex = 0;
            this.lb_tienkhachtra.Text = "Tiền khách trả";
            // 
            // pnlFirst
            // 
            this.pnlFirst.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlFirst.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlFirst.Controls.Add(this.lbFirst);
            this.pnlFirst.Controls.Add(this.lbFirstMoney);
            this.pnlFirst.Location = new System.Drawing.Point(51, 55);
            this.pnlFirst.Name = "pnlFirst";
            this.pnlFirst.Size = new System.Drawing.Size(508, 25);
            this.pnlFirst.TabIndex = 38;
            this.pnlFirst.Visible = false;
            // 
            // lbFirst
            // 
            this.lbFirst.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFirst.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbFirst.Location = new System.Drawing.Point(5, 1);
            this.lbFirst.Name = "lbFirst";
            this.lbFirst.Size = new System.Drawing.Size(0, 19);
            this.lbFirst.TabIndex = 0;
            // 
            // lbFirstMoney
            // 
            this.lbFirstMoney.Appearance.Font = new System.Drawing.Font("Times New Roman", 14.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFirstMoney.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbFirstMoney.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbFirstMoney.Location = new System.Drawing.Point(508, 0);
            this.lbFirstMoney.Name = "lbFirstMoney";
            this.lbFirstMoney.Size = new System.Drawing.Size(0, 22);
            this.lbFirstMoney.TabIndex = 0;
            this.lbFirstMoney.TextChanged += new System.EventHandler(this.lbFirstMoney_TextChanged);
            // 
            // pnlSecond
            // 
            this.pnlSecond.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlSecond.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlSecond.Controls.Add(this.lbSecond);
            this.pnlSecond.Controls.Add(this.lbSecondMoney);
            this.pnlSecond.Location = new System.Drawing.Point(51, 26);
            this.pnlSecond.Name = "pnlSecond";
            this.pnlSecond.Size = new System.Drawing.Size(508, 25);
            this.pnlSecond.TabIndex = 43;
            this.pnlSecond.Visible = false;
            // 
            // lbSecond
            // 
            this.lbSecond.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSecond.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbSecond.Location = new System.Drawing.Point(5, 1);
            this.lbSecond.Name = "lbSecond";
            this.lbSecond.Size = new System.Drawing.Size(0, 19);
            this.lbSecond.TabIndex = 0;
            // 
            // lbSecondMoney
            // 
            this.lbSecondMoney.Appearance.Font = new System.Drawing.Font("Times New Roman", 14.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSecondMoney.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbSecondMoney.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbSecondMoney.Location = new System.Drawing.Point(508, 0);
            this.lbSecondMoney.Name = "lbSecondMoney";
            this.lbSecondMoney.Size = new System.Drawing.Size(0, 22);
            this.lbSecondMoney.TabIndex = 0;
            this.lbSecondMoney.TextChanged += new System.EventHandler(this.lbSecondMoney_TextChanged);
            // 
            // pnlThird
            // 
            this.pnlThird.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlThird.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlThird.Controls.Add(this.lbThird);
            this.pnlThird.Controls.Add(this.lbThirdMoney);
            this.pnlThird.Location = new System.Drawing.Point(51, -3);
            this.pnlThird.Name = "pnlThird";
            this.pnlThird.Size = new System.Drawing.Size(508, 25);
            this.pnlThird.TabIndex = 37;
            this.pnlThird.Visible = false;
            // 
            // lbThird
            // 
            this.lbThird.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbThird.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbThird.Location = new System.Drawing.Point(5, 1);
            this.lbThird.Name = "lbThird";
            this.lbThird.Size = new System.Drawing.Size(0, 19);
            this.lbThird.TabIndex = 0;
            // 
            // lbThirdMoney
            // 
            this.lbThirdMoney.Appearance.Font = new System.Drawing.Font("Times New Roman", 14.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbThirdMoney.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbThirdMoney.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbThirdMoney.Location = new System.Drawing.Point(508, 0);
            this.lbThirdMoney.Name = "lbThirdMoney";
            this.lbThirdMoney.Size = new System.Drawing.Size(0, 22);
            this.lbThirdMoney.TabIndex = 0;
            this.lbThirdMoney.TextChanged += new System.EventHandler(this.lbThirdMoney_TextChanged);
            // 
            // panelControl29
            // 
            this.panelControl29.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl29.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl29.Controls.Add(this.lbTotalMoneyAll);
            this.panelControl29.Controls.Add(this.labelControl18);
            this.panelControl29.Controls.Add(this.lb_TCthanhtoan);
            this.panelControl29.Location = new System.Drawing.Point(51, 128);
            this.panelControl29.Name = "panelControl29";
            this.panelControl29.Size = new System.Drawing.Size(508, 29);
            this.panelControl29.TabIndex = 35;
            // 
            // lbTotalMoneyAll
            // 
            this.lbTotalMoneyAll.Appearance.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalMoneyAll.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lbTotalMoneyAll.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbTotalMoneyAll.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbTotalMoneyAll.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbTotalMoneyAll.Location = new System.Drawing.Point(450, 0);
            this.lbTotalMoneyAll.Name = "lbTotalMoneyAll";
            this.lbTotalMoneyAll.Size = new System.Drawing.Size(11, 24);
            this.lbTotalMoneyAll.TabIndex = 0;
            this.lbTotalMoneyAll.Tag = "0";
            this.lbTotalMoneyAll.Text = "0";
            this.lbTotalMoneyAll.TextChanged += new System.EventHandler(this.lbTotalMoneyAll_TextChanged);
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl18.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl18.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelControl18.Location = new System.Drawing.Point(461, 0);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(47, 19);
            this.labelControl18.TabIndex = 1;
            this.labelControl18.Text = "   VND";
            // 
            // lb_TCthanhtoan
            // 
            this.lb_TCthanhtoan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_TCthanhtoan.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lb_TCthanhtoan.Location = new System.Drawing.Point(5, 1);
            this.lb_TCthanhtoan.Name = "lb_TCthanhtoan";
            this.lb_TCthanhtoan.Size = new System.Drawing.Size(116, 19);
            this.lb_TCthanhtoan.TabIndex = 0;
            this.lb_TCthanhtoan.Text = "TC thanh toán";
            // 
            // panelControl55
            // 
            this.panelControl55.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl55.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl55.Controls.Add(this.lbMoneyGetFirst);
            this.panelControl55.Controls.Add(this.labelControl48);
            this.panelControl55.Controls.Add(this.lb_tiendatcoc);
            this.panelControl55.Location = new System.Drawing.Point(51, 88);
            this.panelControl55.Name = "panelControl55";
            this.panelControl55.Size = new System.Drawing.Size(508, 23);
            this.panelControl55.TabIndex = 49;
            // 
            // lbMoneyGetFirst
            // 
            this.lbMoneyGetFirst.Appearance.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMoneyGetFirst.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lbMoneyGetFirst.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbMoneyGetFirst.Location = new System.Drawing.Point(451, 0);
            this.lbMoneyGetFirst.Name = "lbMoneyGetFirst";
            this.lbMoneyGetFirst.Size = new System.Drawing.Size(10, 22);
            this.lbMoneyGetFirst.TabIndex = 0;
            this.lbMoneyGetFirst.Tag = "0";
            this.lbMoneyGetFirst.Text = "0";
            this.lbMoneyGetFirst.TextChanged += new System.EventHandler(this.lbMoneyGetFirst_TextChanged);
            // 
            // labelControl48
            // 
            this.labelControl48.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl48.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl48.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelControl48.Location = new System.Drawing.Point(461, 0);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(47, 19);
            this.labelControl48.TabIndex = 1;
            this.labelControl48.Text = "   VND";
            // 
            // lb_tiendatcoc
            // 
            this.lb_tiendatcoc.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_tiendatcoc.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lb_tiendatcoc.Location = new System.Drawing.Point(5, 1);
            this.lb_tiendatcoc.Name = "lb_tiendatcoc";
            this.lb_tiendatcoc.Size = new System.Drawing.Size(124, 19);
            this.lb_tiendatcoc.TabIndex = 0;
            this.lb_tiendatcoc.Text = "Tiền đặt cọc     ";
            // 
            // panelControl51
            // 
            this.panelControl51.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl51.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl51.Controls.Add(this.lbServiceMoney);
            this.panelControl51.Controls.Add(this.lbService);
            this.panelControl51.Controls.Add(this.lb_phiphucvu);
            this.panelControl51.Location = new System.Drawing.Point(51, 61);
            this.panelControl51.Name = "panelControl51";
            this.panelControl51.Size = new System.Drawing.Size(508, 23);
            this.panelControl51.TabIndex = 48;
            // 
            // lbServiceMoney
            // 
            this.lbServiceMoney.Appearance.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbServiceMoney.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lbServiceMoney.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbServiceMoney.Location = new System.Drawing.Point(451, 0);
            this.lbServiceMoney.Name = "lbServiceMoney";
            this.lbServiceMoney.Size = new System.Drawing.Size(10, 22);
            this.lbServiceMoney.TabIndex = 0;
            this.lbServiceMoney.Tag = "0";
            this.lbServiceMoney.Text = "0";
            this.lbServiceMoney.TextChanged += new System.EventHandler(this.lbMoneyService_TextChanged);
            // 
            // lbService
            // 
            this.lbService.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbService.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbService.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbService.Location = new System.Drawing.Point(461, 0);
            this.lbService.Name = "lbService";
            this.lbService.Size = new System.Drawing.Size(47, 19);
            this.lbService.TabIndex = 1;
            this.lbService.Text = "   VND";
            // 
            // lb_phiphucvu
            // 
            this.lb_phiphucvu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_phiphucvu.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lb_phiphucvu.Location = new System.Drawing.Point(5, 1);
            this.lb_phiphucvu.Name = "lb_phiphucvu";
            this.lb_phiphucvu.Size = new System.Drawing.Size(128, 19);
            this.lb_phiphucvu.TabIndex = 0;
            this.lb_phiphucvu.Text = "Phí phục vụ       ";
            // 
            // panelControl27
            // 
            this.panelControl27.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl27.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl27.Controls.Add(this.lbDiscountPayment);
            this.panelControl27.Controls.Add(this.lbDiscount);
            this.panelControl27.Controls.Add(this.lb_chietkhau);
            this.panelControl27.Location = new System.Drawing.Point(51, 35);
            this.panelControl27.Name = "panelControl27";
            this.panelControl27.Size = new System.Drawing.Size(508, 23);
            this.panelControl27.TabIndex = 39;
            // 
            // lbDiscountPayment
            // 
            this.lbDiscountPayment.Appearance.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDiscountPayment.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lbDiscountPayment.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbDiscountPayment.Location = new System.Drawing.Point(451, 0);
            this.lbDiscountPayment.Name = "lbDiscountPayment";
            this.lbDiscountPayment.Size = new System.Drawing.Size(10, 22);
            this.lbDiscountPayment.TabIndex = 0;
            this.lbDiscountPayment.Tag = "0";
            this.lbDiscountPayment.Text = "0";
            this.lbDiscountPayment.TextChanged += new System.EventHandler(this.lbDiscountPayment_TextChanged);
            // 
            // lbDiscount
            // 
            this.lbDiscount.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDiscount.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbDiscount.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbDiscount.Location = new System.Drawing.Point(461, 0);
            this.lbDiscount.Name = "lbDiscount";
            this.lbDiscount.Size = new System.Drawing.Size(47, 19);
            this.lbDiscount.TabIndex = 1;
            this.lbDiscount.Text = "   VND";
            // 
            // lb_chietkhau
            // 
            this.lb_chietkhau.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_chietkhau.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lb_chietkhau.Location = new System.Drawing.Point(5, 1);
            this.lb_chietkhau.Name = "lb_chietkhau";
            this.lb_chietkhau.Size = new System.Drawing.Size(129, 19);
            this.lb_chietkhau.TabIndex = 0;
            this.lb_chietkhau.Text = "Chiết Khấu        ";
            // 
            // panelControl31
            // 
            this.panelControl31.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl31.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl31.Controls.Add(this.lbTotalMoneyPayment);
            this.panelControl31.Controls.Add(this.labelControl20);
            this.panelControl31.Controls.Add(this.lb_tongthanhtoan);
            this.panelControl31.Location = new System.Drawing.Point(51, 7);
            this.panelControl31.Name = "panelControl31";
            this.panelControl31.Size = new System.Drawing.Size(508, 25);
            this.panelControl31.TabIndex = 33;
            // 
            // lbTotalMoneyPayment
            // 
            this.lbTotalMoneyPayment.Appearance.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalMoneyPayment.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lbTotalMoneyPayment.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbTotalMoneyPayment.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbTotalMoneyPayment.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbTotalMoneyPayment.Location = new System.Drawing.Point(451, 0);
            this.lbTotalMoneyPayment.Name = "lbTotalMoneyPayment";
            this.lbTotalMoneyPayment.Size = new System.Drawing.Size(10, 22);
            this.lbTotalMoneyPayment.TabIndex = 0;
            this.lbTotalMoneyPayment.Tag = "0";
            this.lbTotalMoneyPayment.Text = "0";
            this.lbTotalMoneyPayment.TextChanged += new System.EventHandler(this.lbTotalMoneyPayment_TextChanged);
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl20.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl20.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelControl20.Location = new System.Drawing.Point(461, 0);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(47, 19);
            this.labelControl20.TabIndex = 1;
            this.labelControl20.Text = "   VND";
            // 
            // lb_tongthanhtoan
            // 
            this.lb_tongthanhtoan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_tongthanhtoan.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lb_tongthanhtoan.Location = new System.Drawing.Point(5, 3);
            this.lb_tongthanhtoan.Name = "lb_tongthanhtoan";
            this.lb_tongthanhtoan.Size = new System.Drawing.Size(143, 19);
            this.lb_tongthanhtoan.TabIndex = 0;
            this.lb_tongthanhtoan.Text = "Tổng Thanh Toán";
            // 
            // pnlVisa
            // 
            this.pnlVisa.Controls.Add(this.panelControl2);
            this.pnlVisa.Controls.Add(this.btnPaymentVisa);
            this.pnlVisa.Controls.Add(this.bntCancelVisa);
            this.pnlVisa.Controls.Add(this.pnlHeaderVisa);
            this.pnlVisa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlVisa.Location = new System.Drawing.Point(2, 37);
            this.pnlVisa.Name = "pnlVisa";
            this.pnlVisa.Size = new System.Drawing.Size(643, 205);
            this.pnlVisa.TabIndex = 51;
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.panelControl4);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Location = new System.Drawing.Point(5, 41);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(591, 250);
            this.panelControl2.TabIndex = 25;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.txtCountry);
            this.panelControl4.Controls.Add(this.txtNameVisa);
            this.panelControl4.Controls.Add(this.lb_tenchuthe);
            this.panelControl4.Controls.Add(this.btnRefeshIdCode);
            this.panelControl4.Controls.Add(this.txtCwCvc);
            this.panelControl4.Controls.Add(this.labelControl12);
            this.panelControl4.Controls.Add(this.txtIdCodeCreate);
            this.panelControl4.Controls.Add(this.lb_thongtinthem);
            this.panelControl4.Controls.Add(this.txtCity);
            this.panelControl4.Controls.Add(this.txtIdCodeInput);
            this.panelControl4.Controls.Add(this.txtAddress);
            this.panelControl4.Controls.Add(this.lb_maantoan);
            this.panelControl4.Controls.Add(this.lb_tinhtp);
            this.panelControl4.Controls.Add(this.lb_quocgia);
            this.panelControl4.Controls.Add(this.lb_diachichuthe);
            this.panelControl4.Location = new System.Drawing.Point(5, 98);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(610, 149);
            this.panelControl4.TabIndex = 106;
            // 
            // txtCountry
            // 
            this.txtCountry.Location = new System.Drawing.Point(141, 113);
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtCountry.Properties.Appearance.Options.UseFont = true;
            this.txtCountry.Size = new System.Drawing.Size(195, 26);
            this.txtCountry.TabIndex = 121;
            // 
            // txtNameVisa
            // 
            this.txtNameVisa.Location = new System.Drawing.Point(140, 34);
            this.txtNameVisa.Name = "txtNameVisa";
            this.txtNameVisa.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtNameVisa.Properties.Appearance.Options.UseFont = true;
            this.txtNameVisa.Size = new System.Drawing.Size(195, 26);
            this.txtNameVisa.TabIndex = 120;
            // 
            // lb_tenchuthe
            // 
            this.lb_tenchuthe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lb_tenchuthe.Location = new System.Drawing.Point(2, 37);
            this.lb_tenchuthe.Name = "lb_tenchuthe";
            this.lb_tenchuthe.Size = new System.Drawing.Size(97, 19);
            this.lb_tenchuthe.TabIndex = 119;
            this.lb_tenchuthe.Text = "Tên Chủ Thẻ ";
            // 
            // btnRefeshIdCode
            // 
            this.btnRefeshIdCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnRefeshIdCode.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnRefeshIdCode.Location = new System.Drawing.Point(569, 116);
            this.btnRefeshIdCode.Name = "btnRefeshIdCode";
            this.btnRefeshIdCode.Size = new System.Drawing.Size(35, 26);
            this.btnRefeshIdCode.TabIndex = 118;
            this.btnRefeshIdCode.Click += new System.EventHandler(this.btnRefeshIdCode_Click);
            // 
            // txtCwCvc
            // 
            this.txtCwCvc.Location = new System.Drawing.Point(140, 75);
            this.txtCwCvc.Name = "txtCwCvc";
            this.txtCwCvc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtCwCvc.Properties.Appearance.Options.UseFont = true;
            this.txtCwCvc.Size = new System.Drawing.Size(195, 26);
            this.txtCwCvc.TabIndex = 116;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl12.Location = new System.Drawing.Point(2, 78);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(78, 19);
            this.labelControl12.TabIndex = 115;
            this.labelControl12.Text = "CW, CVC2 ";
            // 
            // txtIdCodeCreate
            // 
            this.txtIdCodeCreate.EditValue = "";
            this.txtIdCodeCreate.Location = new System.Drawing.Point(508, 117);
            this.txtIdCodeCreate.Name = "txtIdCodeCreate";
            this.txtIdCodeCreate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdCodeCreate.Properties.Appearance.Options.UseFont = true;
            this.txtIdCodeCreate.Properties.ReadOnly = true;
            this.txtIdCodeCreate.Size = new System.Drawing.Size(56, 24);
            this.txtIdCodeCreate.TabIndex = 114;
            // 
            // lb_thongtinthem
            // 
            this.lb_thongtinthem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_thongtinthem.Location = new System.Drawing.Point(2, 4);
            this.lb_thongtinthem.Name = "lb_thongtinthem";
            this.lb_thongtinthem.Size = new System.Drawing.Size(133, 19);
            this.lb_thongtinthem.TabIndex = 112;
            this.lb_thongtinthem.Text = "Thông Tin Thêm";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(434, 75);
            this.txtCity.Name = "txtCity";
            this.txtCity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtCity.Properties.Appearance.Options.UseFont = true;
            this.txtCity.Size = new System.Drawing.Size(169, 26);
            this.txtCity.TabIndex = 111;
            // 
            // txtIdCodeInput
            // 
            this.txtIdCodeInput.Location = new System.Drawing.Point(434, 116);
            this.txtIdCodeInput.Name = "txtIdCodeInput";
            this.txtIdCodeInput.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtIdCodeInput.Properties.Appearance.Options.UseFont = true;
            this.txtIdCodeInput.Size = new System.Drawing.Size(67, 26);
            this.txtIdCodeInput.TabIndex = 110;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(434, 34);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtAddress.Properties.Appearance.Options.UseFont = true;
            this.txtAddress.Size = new System.Drawing.Size(169, 26);
            this.txtAddress.TabIndex = 109;
            // 
            // lb_maantoan
            // 
            this.lb_maantoan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_maantoan.Location = new System.Drawing.Point(340, 119);
            this.lb_maantoan.Name = "lb_maantoan";
            this.lb_maantoan.Size = new System.Drawing.Size(86, 19);
            this.lb_maantoan.TabIndex = 108;
            this.lb_maantoan.Text = "Mã An Toàn";
            // 
            // lb_tinhtp
            // 
            this.lb_tinhtp.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_tinhtp.Location = new System.Drawing.Point(340, 78);
            this.lb_tinhtp.Name = "lb_tinhtp";
            this.lb_tinhtp.Size = new System.Drawing.Size(57, 19);
            this.lb_tinhtp.TabIndex = 107;
            this.lb_tinhtp.Text = "Tỉnh/TP";
            // 
            // lb_quocgia
            // 
            this.lb_quocgia.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_quocgia.Location = new System.Drawing.Point(2, 119);
            this.lb_quocgia.Name = "lb_quocgia";
            this.lb_quocgia.Size = new System.Drawing.Size(70, 19);
            this.lb_quocgia.TabIndex = 105;
            this.lb_quocgia.Text = "Quốc Gia ";
            // 
            // lb_diachichuthe
            // 
            this.lb_diachichuthe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_diachichuthe.Location = new System.Drawing.Point(340, 37);
            this.lb_diachichuthe.Name = "lb_diachichuthe";
            this.lb_diachichuthe.Size = new System.Drawing.Size(56, 19);
            this.lb_diachichuthe.TabIndex = 106;
            this.lb_diachichuthe.Text = "Địa Chỉ ";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.labelControl31);
            this.panelControl3.Controls.Add(this.txtYear);
            this.panelControl3.Controls.Add(this.txtMonth);
            this.panelControl3.Controls.Add(this.txtMoneyVisa);
            this.panelControl3.Controls.Add(this.labelControl19);
            this.panelControl3.Controls.Add(this.lb_sotien);
            this.panelControl3.Controls.Add(this.txtIdCardVisa);
            this.panelControl3.Controls.Add(this.lb_thongtinbatbuoc);
            this.panelControl3.Controls.Add(this.lb_ngayhethan);
            this.panelControl3.Controls.Add(this.lb_sothe);
            this.panelControl3.Location = new System.Drawing.Point(5, 4);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(610, 89);
            this.panelControl3.TabIndex = 105;
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl31.Location = new System.Drawing.Point(495, 30);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(6, 19);
            this.labelControl31.TabIndex = 114;
            this.labelControl31.Text = "/";
            // 
            // txtYear
            // 
            this.txtYear.Location = new System.Drawing.Point(508, 27);
            this.txtYear.Name = "txtYear";
            this.txtYear.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtYear.Properties.Appearance.Options.UseFont = true;
            this.txtYear.Properties.Appearance.Options.UseTextOptions = true;
            this.txtYear.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtYear.Properties.Mask.EditMask = "d";
            this.txtYear.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtYear.Properties.MaxLength = 2;
            this.txtYear.Size = new System.Drawing.Size(45, 26);
            this.txtYear.TabIndex = 113;
            // 
            // txtMonth
            // 
            this.txtMonth.Location = new System.Drawing.Point(444, 27);
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtMonth.Properties.Appearance.Options.UseFont = true;
            this.txtMonth.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMonth.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMonth.Properties.Mask.EditMask = "d";
            this.txtMonth.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtMonth.Properties.MaxLength = 2;
            this.txtMonth.Size = new System.Drawing.Size(45, 26);
            this.txtMonth.TabIndex = 112;
            // 
            // txtMoneyVisa
            // 
            this.txtMoneyVisa.EditValue = "0";
            this.txtMoneyVisa.Location = new System.Drawing.Point(111, 58);
            this.txtMoneyVisa.Name = "txtMoneyVisa";
            this.txtMoneyVisa.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtMoneyVisa.Properties.Appearance.Options.UseFont = true;
            this.txtMoneyVisa.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMoneyVisa.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtMoneyVisa.Properties.Mask.EditMask = "N0";
            this.txtMoneyVisa.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtMoneyVisa.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtMoneyVisa.Size = new System.Drawing.Size(442, 26);
            this.txtMoneyVisa.TabIndex = 111;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl19.Location = new System.Drawing.Point(560, 60);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(43, 19);
            this.labelControl19.TabIndex = 110;
            this.labelControl19.Text = "  VND";
            // 
            // lb_sotien
            // 
            this.lb_sotien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lb_sotien.Location = new System.Drawing.Point(11, 59);
            this.lb_sotien.Name = "lb_sotien";
            this.lb_sotien.Size = new System.Drawing.Size(75, 19);
            this.lb_sotien.TabIndex = 108;
            this.lb_sotien.Text = "Số Tiền(*)";
            // 
            // txtIdCardVisa
            // 
            this.txtIdCardVisa.EditValue = "0000-0000-0000-0000";
            this.txtIdCardVisa.Location = new System.Drawing.Point(111, 27);
            this.txtIdCardVisa.Name = "txtIdCardVisa";
            this.txtIdCardVisa.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtIdCardVisa.Properties.Appearance.Options.UseFont = true;
            this.txtIdCardVisa.Size = new System.Drawing.Size(195, 26);
            this.txtIdCardVisa.TabIndex = 105;
            // 
            // lb_thongtinbatbuoc
            // 
            this.lb_thongtinbatbuoc.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lb_thongtinbatbuoc.Location = new System.Drawing.Point(11, 4);
            this.lb_thongtinbatbuoc.Name = "lb_thongtinbatbuoc";
            this.lb_thongtinbatbuoc.Size = new System.Drawing.Size(137, 17);
            this.lb_thongtinbatbuoc.TabIndex = 103;
            this.lb_thongtinbatbuoc.Text = "Thông Tin Bắt Buộc";
            // 
            // lb_ngayhethan
            // 
            this.lb_ngayhethan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lb_ngayhethan.Location = new System.Drawing.Point(320, 31);
            this.lb_ngayhethan.Name = "lb_ngayhethan";
            this.lb_ngayhethan.Size = new System.Drawing.Size(124, 19);
            this.lb_ngayhethan.TabIndex = 102;
            this.lb_ngayhethan.Text = "Ngày Hết Hạn (*)";
            // 
            // lb_sothe
            // 
            this.lb_sothe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lb_sothe.Location = new System.Drawing.Point(10, 31);
            this.lb_sothe.Name = "lb_sothe";
            this.lb_sothe.Size = new System.Drawing.Size(76, 19);
            this.lb_sothe.TabIndex = 101;
            this.lb_sothe.Text = "Số Thẻ (*)";
            // 
            // btnPaymentVisa
            // 
            this.btnPaymentVisa.Appearance.Font = new System.Drawing.Font("Tahoma", 14F);
            this.btnPaymentVisa.Appearance.Options.UseFont = true;
            this.btnPaymentVisa.Location = new System.Drawing.Point(288, 296);
            this.btnPaymentVisa.Name = "btnPaymentVisa";
            this.btnPaymentVisa.Size = new System.Drawing.Size(156, 51);
            this.btnPaymentVisa.TabIndex = 4;
            this.btnPaymentVisa.Text = "Giao Dịch";
            this.btnPaymentVisa.Click += new System.EventHandler(this.btnPaymentVisa_Click);
            // 
            // bntCancelVisa
            // 
            this.bntCancelVisa.Appearance.Font = new System.Drawing.Font("Tahoma", 14F);
            this.bntCancelVisa.Appearance.Options.UseFont = true;
            this.bntCancelVisa.Location = new System.Drawing.Point(457, 296);
            this.bntCancelVisa.Name = "bntCancelVisa";
            this.bntCancelVisa.Size = new System.Drawing.Size(156, 51);
            this.bntCancelVisa.TabIndex = 5;
            this.bntCancelVisa.Text = "Hủy Giao Dịch";
            this.bntCancelVisa.Click += new System.EventHandler(this.bntCancelVisa_Click);
            // 
            // pnlHeaderVisa
            // 
            this.pnlHeaderVisa.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlHeaderVisa.Controls.Add(this.lb_thanhtoanbangthe);
            this.pnlHeaderVisa.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeaderVisa.Location = new System.Drawing.Point(2, 2);
            this.pnlHeaderVisa.Name = "pnlHeaderVisa";
            this.pnlHeaderVisa.Size = new System.Drawing.Size(639, 35);
            this.pnlHeaderVisa.TabIndex = 10;
            // 
            // lb_thanhtoanbangthe
            // 
            this.lb_thanhtoanbangthe.Appearance.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Bold);
            this.lb_thanhtoanbangthe.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.lb_thanhtoanbangthe.Location = new System.Drawing.Point(18, 2);
            this.lb_thanhtoanbangthe.Name = "lb_thanhtoanbangthe";
            this.lb_thanhtoanbangthe.Size = new System.Drawing.Size(539, 27);
            this.lb_thanhtoanbangthe.TabIndex = 1;
            this.lb_thanhtoanbangthe.Text = "Thanh Toán Bằng Thẻ Tín Dụng && Ghi Nợ Quốc Tế";
            // 
            // pnlCongNo
            // 
            this.pnlCongNo.Controls.Add(this.palCongNo_Noidung);
            this.pnlCongNo.Controls.Add(this.panelControl13);
            this.pnlCongNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCongNo.Location = new System.Drawing.Point(2, 37);
            this.pnlCongNo.Name = "pnlCongNo";
            this.pnlCongNo.Size = new System.Drawing.Size(643, 205);
            this.pnlCongNo.TabIndex = 52;
            // 
            // palCongNo_Noidung
            // 
            this.palCongNo_Noidung.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palCongNo_Noidung.Controls.Add(this.panelControl9);
            this.palCongNo_Noidung.Controls.Add(this.panelControl11);
            this.palCongNo_Noidung.Location = new System.Drawing.Point(3, 56);
            this.palCongNo_Noidung.Name = "palCongNo_Noidung";
            this.palCongNo_Noidung.Size = new System.Drawing.Size(643, 218);
            this.palCongNo_Noidung.TabIndex = 25;
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.txtCNMaKH);
            this.panelControl9.Controls.Add(this.txtCNSoDT);
            this.panelControl9.Controls.Add(this.labelControl3);
            this.panelControl9.Controls.Add(this.dtpNgayHenTra);
            this.panelControl9.Controls.Add(this.labelControl1);
            this.panelControl9.Controls.Add(this.btnCongNoSearch);
            this.panelControl9.Controls.Add(this.txtCongNo_Tien);
            this.panelControl9.Controls.Add(this.labelControl10);
            this.panelControl9.Controls.Add(this.labelControl13);
            this.panelControl9.Controls.Add(this.labelControl16);
            this.panelControl9.Location = new System.Drawing.Point(3, 8);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(635, 145);
            this.panelControl9.TabIndex = 105;
            // 
            // txtCNMaKH
            // 
            this.txtCNMaKH.EditValue = "";
            this.txtCNMaKH.Location = new System.Drawing.Point(131, 4);
            this.txtCNMaKH.Name = "txtCNMaKH";
            this.txtCNMaKH.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtCNMaKH.Properties.Appearance.Options.UseFont = true;
            this.txtCNMaKH.Size = new System.Drawing.Size(422, 26);
            this.txtCNMaKH.TabIndex = 119;
            // 
            // txtCNSoDT
            // 
            this.txtCNSoDT.EditValue = "";
            this.txtCNSoDT.Location = new System.Drawing.Point(131, 42);
            this.txtCNSoDT.Name = "txtCNSoDT";
            this.txtCNSoDT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtCNSoDT.Properties.Appearance.Options.UseFont = true;
            this.txtCNSoDT.Size = new System.Drawing.Size(422, 26);
            this.txtCNSoDT.TabIndex = 118;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl3.Location = new System.Drawing.Point(10, 44);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(72, 19);
            this.labelControl3.TabIndex = 114;
            this.labelControl3.Text = "Điện thoại";
            // 
            // dtpNgayHenTra
            // 
            this.dtpNgayHenTra.EditValue = null;
            this.dtpNgayHenTra.EnterMoveNextControl = true;
            this.dtpNgayHenTra.Location = new System.Drawing.Point(131, 112);
            this.dtpNgayHenTra.Name = "dtpNgayHenTra";
            this.dtpNgayHenTra.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayHenTra.Properties.Appearance.Options.UseFont = true;
            this.dtpNgayHenTra.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayHenTra.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgayHenTra.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgayHenTra.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayHenTra.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayHenTra.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgayHenTra.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgayHenTra.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgayHenTra.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgayHenTra.Size = new System.Drawing.Size(422, 26);
            this.dtpNgayHenTra.TabIndex = 113;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl1.Location = new System.Drawing.Point(9, 115);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 19);
            this.labelControl1.TabIndex = 112;
            this.labelControl1.Text = "Ngày trả";
            // 
            // btnCongNoSearch
            // 
            this.btnCongNoSearch.Appearance.Font = new System.Drawing.Font("Tahoma", 14F);
            this.btnCongNoSearch.Appearance.Options.UseFont = true;
            this.btnCongNoSearch.Image = global::KP_RES.Properties.Resources.search_26;
            this.btnCongNoSearch.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCongNoSearch.Location = new System.Drawing.Point(559, 4);
            this.btnCongNoSearch.Name = "btnCongNoSearch";
            this.btnCongNoSearch.Size = new System.Drawing.Size(71, 64);
            this.btnCongNoSearch.TabIndex = 6;
            this.btnCongNoSearch.Click += new System.EventHandler(this.btnCongNoSearch_Click);
            // 
            // txtCongNo_Tien
            // 
            this.txtCongNo_Tien.EditValue = "0";
            this.txtCongNo_Tien.Enabled = false;
            this.txtCongNo_Tien.Location = new System.Drawing.Point(131, 76);
            this.txtCongNo_Tien.Name = "txtCongNo_Tien";
            this.txtCongNo_Tien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtCongNo_Tien.Properties.Appearance.Options.UseFont = true;
            this.txtCongNo_Tien.Properties.Appearance.Options.UseTextOptions = true;
            this.txtCongNo_Tien.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtCongNo_Tien.Properties.Mask.EditMask = "N0";
            this.txtCongNo_Tien.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCongNo_Tien.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtCongNo_Tien.Size = new System.Drawing.Size(422, 26);
            this.txtCongNo_Tien.TabIndex = 111;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl10.Location = new System.Drawing.Point(565, 79);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(43, 19);
            this.labelControl10.TabIndex = 110;
            this.labelControl10.Text = "  VND";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl13.Location = new System.Drawing.Point(10, 79);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(54, 19);
            this.labelControl13.TabIndex = 108;
            this.labelControl13.Text = "Số Tiền";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl16.Location = new System.Drawing.Point(10, 7);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(106, 19);
            this.labelControl16.TabIndex = 101;
            this.labelControl16.Text = "Mã khách hàng";
            // 
            // panelControl11
            // 
            this.panelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl11.Controls.Add(this.btnCongNoXacNhan);
            this.panelControl11.Controls.Add(this.btnHuyCongNo);
            this.panelControl11.Location = new System.Drawing.Point(3, 155);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(635, 61);
            this.panelControl11.TabIndex = 24;
            // 
            // btnCongNoXacNhan
            // 
            this.btnCongNoXacNhan.Appearance.Font = new System.Drawing.Font("Tahoma", 14F);
            this.btnCongNoXacNhan.Appearance.Options.UseFont = true;
            this.btnCongNoXacNhan.Location = new System.Drawing.Point(235, 5);
            this.btnCongNoXacNhan.Name = "btnCongNoXacNhan";
            this.btnCongNoXacNhan.Size = new System.Drawing.Size(156, 53);
            this.btnCongNoXacNhan.TabIndex = 4;
            this.btnCongNoXacNhan.Text = "OK";
            this.btnCongNoXacNhan.Click += new System.EventHandler(this.btnCongNoXacNhan_Click);
            // 
            // btnHuyCongNo
            // 
            this.btnHuyCongNo.Appearance.Font = new System.Drawing.Font("Tahoma", 14F);
            this.btnHuyCongNo.Appearance.Options.UseFont = true;
            this.btnHuyCongNo.Location = new System.Drawing.Point(397, 5);
            this.btnHuyCongNo.Name = "btnHuyCongNo";
            this.btnHuyCongNo.Size = new System.Drawing.Size(156, 53);
            this.btnHuyCongNo.TabIndex = 5;
            this.btnHuyCongNo.Text = "Hủy";
            this.btnHuyCongNo.Click += new System.EventHandler(this.btnHuyCongNo_Click);
            // 
            // panelControl13
            // 
            this.panelControl13.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl13.Controls.Add(this.labelControl21);
            this.panelControl13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl13.Location = new System.Drawing.Point(2, 2);
            this.panelControl13.Name = "panelControl13";
            this.panelControl13.Size = new System.Drawing.Size(639, 52);
            this.panelControl13.TabIndex = 10;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Bold);
            this.labelControl21.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl21.Location = new System.Drawing.Point(18, 8);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(94, 27);
            this.labelControl21.TabIndex = 1;
            this.labelControl21.Text = "Công Nợ";
            // 
            // pnlBottomPayment
            // 
            this.pnlBottomPayment.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlBottomPayment.Controls.Add(this.pnlKeyboardAndShowForeignCurrency);
            this.pnlBottomPayment.Controls.Add(this.pnlUpDownForeignCurrency);
            this.pnlBottomPayment.Controls.Add(this.pnlForeignCurrency);
            this.pnlBottomPayment.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottomPayment.Location = new System.Drawing.Point(2, 242);
            this.pnlBottomPayment.Name = "pnlBottomPayment";
            this.pnlBottomPayment.Size = new System.Drawing.Size(643, 298);
            this.pnlBottomPayment.TabIndex = 48;
            // 
            // pnlKeyboardAndShowForeignCurrency
            // 
            this.pnlKeyboardAndShowForeignCurrency.Controls.Add(this.pnlKeyBoardText);
            this.pnlKeyboardAndShowForeignCurrency.Controls.Add(this.pnlKeyboardMoney);
            this.pnlKeyboardAndShowForeignCurrency.Controls.Add(this.pnlKeyboardNumber);
            this.pnlKeyboardAndShowForeignCurrency.Controls.Add(this.pnlShowForeignCurrency);
            this.pnlKeyboardAndShowForeignCurrency.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlKeyboardAndShowForeignCurrency.Location = new System.Drawing.Point(318, 0);
            this.pnlKeyboardAndShowForeignCurrency.Name = "pnlKeyboardAndShowForeignCurrency";
            this.pnlKeyboardAndShowForeignCurrency.Size = new System.Drawing.Size(325, 298);
            this.pnlKeyboardAndShowForeignCurrency.TabIndex = 4;
            // 
            // pnlKeyBoardText
            // 
            this.pnlKeyBoardText.Controls.Add(this.btnP);
            this.pnlKeyBoardText.Controls.Add(this.btnZ);
            this.pnlKeyBoardText.Controls.Add(this.btnY);
            this.pnlKeyBoardText.Controls.Add(this.btnPaymentText);
            this.pnlKeyBoardText.Controls.Add(this.btnShift);
            this.pnlKeyBoardText.Controls.Add(this.btnX);
            this.pnlKeyBoardText.Controls.Add(this.btnW);
            this.pnlKeyBoardText.Controls.Add(this.btnV);
            this.pnlKeyBoardText.Controls.Add(this.btnU);
            this.pnlKeyBoardText.Controls.Add(this.btnT);
            this.pnlKeyBoardText.Controls.Add(this.btnS);
            this.pnlKeyBoardText.Controls.Add(this.btnF);
            this.pnlKeyBoardText.Controls.Add(this.btnE);
            this.pnlKeyBoardText.Controls.Add(this.btnD);
            this.pnlKeyBoardText.Controls.Add(this.btnC);
            this.pnlKeyBoardText.Controls.Add(this.btnB);
            this.pnlKeyBoardText.Controls.Add(this.btnA);
            this.pnlKeyBoardText.Controls.Add(this.btnMoveRigthText);
            this.pnlKeyBoardText.Controls.Add(this.btnM);
            this.pnlKeyBoardText.Controls.Add(this.btnTranslateText);
            this.pnlKeyBoardText.Controls.Add(this.btnR);
            this.pnlKeyBoardText.Controls.Add(this.btnQ);
            this.pnlKeyBoardText.Controls.Add(this.btnOkText);
            this.pnlKeyBoardText.Controls.Add(this.btnDeleteText);
            this.pnlKeyBoardText.Controls.Add(this.btnL);
            this.pnlKeyBoardText.Controls.Add(this.btnI);
            this.pnlKeyBoardText.Controls.Add(this.btnH);
            this.pnlKeyBoardText.Controls.Add(this.btnG);
            this.pnlKeyBoardText.Controls.Add(this.btnMoveLeftText);
            this.pnlKeyBoardText.Controls.Add(this.btnK);
            this.pnlKeyBoardText.Controls.Add(this.btnJ);
            this.pnlKeyBoardText.Controls.Add(this.btnO);
            this.pnlKeyBoardText.Controls.Add(this.btnN);
            this.pnlKeyBoardText.Controls.Add(this.btnEscText);
            this.pnlKeyBoardText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlKeyBoardText.Location = new System.Drawing.Point(2, 2);
            this.pnlKeyBoardText.Name = "pnlKeyBoardText";
            this.pnlKeyBoardText.Size = new System.Drawing.Size(321, 260);
            this.pnlKeyBoardText.TabIndex = 92;
            // 
            // btnP
            // 
            this.btnP.AllowFocus = false;
            this.btnP.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnP.Appearance.Options.UseFont = true;
            this.btnP.Location = new System.Drawing.Point(365, 5);
            this.btnP.Name = "btnP";
            this.btnP.Size = new System.Drawing.Size(38, 45);
            this.btnP.TabIndex = 78;
            this.btnP.Text = "P";
            this.btnP.Click += new System.EventHandler(this.btnP_Click);
            // 
            // btnZ
            // 
            this.btnZ.AllowFocus = false;
            this.btnZ.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnZ.Appearance.Options.UseFont = true;
            this.btnZ.Location = new System.Drawing.Point(5, 105);
            this.btnZ.Name = "btnZ";
            this.btnZ.Size = new System.Drawing.Size(38, 45);
            this.btnZ.TabIndex = 103;
            this.btnZ.Text = "Z";
            this.btnZ.Click += new System.EventHandler(this.btnZ_Click);
            // 
            // btnY
            // 
            this.btnY.AllowFocus = false;
            this.btnY.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnY.Appearance.Options.UseFont = true;
            this.btnY.Location = new System.Drawing.Point(205, 5);
            this.btnY.Name = "btnY";
            this.btnY.Size = new System.Drawing.Size(38, 45);
            this.btnY.TabIndex = 102;
            this.btnY.Text = "Y";
            this.btnY.Click += new System.EventHandler(this.btnY_Click);
            // 
            // btnPaymentText
            // 
            this.btnPaymentText.AllowFocus = false;
            this.btnPaymentText.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPaymentText.Appearance.Options.UseFont = true;
            this.btnPaymentText.Location = new System.Drawing.Point(285, 156);
            this.btnPaymentText.Name = "btnPaymentText";
            this.btnPaymentText.Size = new System.Drawing.Size(78, 97);
            this.btnPaymentText.TabIndex = 83;
            this.btnPaymentText.Text = "F2    \r\nIn Hóa Đơn";
            this.btnPaymentText.Click += new System.EventHandler(this.btnPaymentText_Click);
            // 
            // btnShift
            // 
            this.btnShift.AllowFocus = false;
            this.btnShift.Appearance.BackColor = System.Drawing.Color.GreenYellow;
            this.btnShift.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShift.Appearance.Options.UseBackColor = true;
            this.btnShift.Appearance.Options.UseFont = true;
            this.btnShift.Location = new System.Drawing.Point(285, 105);
            this.btnShift.Name = "btnShift";
            this.btnShift.Size = new System.Drawing.Size(118, 45);
            this.btnShift.TabIndex = 101;
            this.btnShift.Text = "Shift";
            this.btnShift.Click += new System.EventHandler(this.btnShift_Click);
            // 
            // btnX
            // 
            this.btnX.AllowFocus = false;
            this.btnX.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnX.Appearance.Options.UseFont = true;
            this.btnX.Location = new System.Drawing.Point(45, 105);
            this.btnX.Name = "btnX";
            this.btnX.Size = new System.Drawing.Size(38, 45);
            this.btnX.TabIndex = 100;
            this.btnX.Text = "X";
            this.btnX.Click += new System.EventHandler(this.btnX_Click);
            // 
            // btnW
            // 
            this.btnW.AllowFocus = false;
            this.btnW.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnW.Appearance.Options.UseFont = true;
            this.btnW.Location = new System.Drawing.Point(45, 5);
            this.btnW.Name = "btnW";
            this.btnW.Size = new System.Drawing.Size(38, 45);
            this.btnW.TabIndex = 96;
            this.btnW.Text = "W";
            this.btnW.Click += new System.EventHandler(this.btnW_Click);
            // 
            // btnV
            // 
            this.btnV.AllowFocus = false;
            this.btnV.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnV.Appearance.Options.UseFont = true;
            this.btnV.Location = new System.Drawing.Point(125, 105);
            this.btnV.Name = "btnV";
            this.btnV.Size = new System.Drawing.Size(38, 45);
            this.btnV.TabIndex = 95;
            this.btnV.Text = "V";
            this.btnV.Click += new System.EventHandler(this.btnV_Click);
            // 
            // btnU
            // 
            this.btnU.AllowFocus = false;
            this.btnU.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnU.Appearance.Options.UseFont = true;
            this.btnU.Location = new System.Drawing.Point(245, 5);
            this.btnU.Name = "btnU";
            this.btnU.Size = new System.Drawing.Size(38, 45);
            this.btnU.TabIndex = 94;
            this.btnU.Text = "U";
            this.btnU.Click += new System.EventHandler(this.btnU_Click);
            // 
            // btnT
            // 
            this.btnT.AllowFocus = false;
            this.btnT.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnT.Appearance.Options.UseFont = true;
            this.btnT.Location = new System.Drawing.Point(165, 5);
            this.btnT.Name = "btnT";
            this.btnT.Size = new System.Drawing.Size(38, 45);
            this.btnT.TabIndex = 93;
            this.btnT.Text = "T";
            this.btnT.Click += new System.EventHandler(this.btnT_Click);
            // 
            // btnS
            // 
            this.btnS.AllowFocus = false;
            this.btnS.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnS.Appearance.Options.UseFont = true;
            this.btnS.Location = new System.Drawing.Point(65, 55);
            this.btnS.Name = "btnS";
            this.btnS.Size = new System.Drawing.Size(38, 45);
            this.btnS.TabIndex = 92;
            this.btnS.Text = "S";
            this.btnS.Click += new System.EventHandler(this.btnS_Click);
            // 
            // btnF
            // 
            this.btnF.AllowFocus = false;
            this.btnF.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnF.Appearance.Options.UseFont = true;
            this.btnF.Location = new System.Drawing.Point(145, 55);
            this.btnF.Name = "btnF";
            this.btnF.Size = new System.Drawing.Size(38, 45);
            this.btnF.TabIndex = 91;
            this.btnF.Text = "F";
            this.btnF.Click += new System.EventHandler(this.btnF_Click);
            // 
            // btnE
            // 
            this.btnE.AllowFocus = false;
            this.btnE.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnE.Appearance.Options.UseFont = true;
            this.btnE.Location = new System.Drawing.Point(85, 5);
            this.btnE.Name = "btnE";
            this.btnE.Size = new System.Drawing.Size(38, 45);
            this.btnE.TabIndex = 90;
            this.btnE.Text = "E";
            this.btnE.Click += new System.EventHandler(this.btnE_Click);
            // 
            // btnD
            // 
            this.btnD.AllowFocus = false;
            this.btnD.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnD.Appearance.Options.UseFont = true;
            this.btnD.Location = new System.Drawing.Point(105, 55);
            this.btnD.Name = "btnD";
            this.btnD.Size = new System.Drawing.Size(38, 45);
            this.btnD.TabIndex = 89;
            this.btnD.Text = "D";
            this.btnD.Click += new System.EventHandler(this.btnD_Click);
            // 
            // btnC
            // 
            this.btnC.AllowFocus = false;
            this.btnC.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnC.Appearance.Options.UseFont = true;
            this.btnC.Location = new System.Drawing.Point(85, 105);
            this.btnC.Name = "btnC";
            this.btnC.Size = new System.Drawing.Size(38, 45);
            this.btnC.TabIndex = 88;
            this.btnC.Text = "C";
            this.btnC.Click += new System.EventHandler(this.btnC_Click);
            // 
            // btnB
            // 
            this.btnB.AllowFocus = false;
            this.btnB.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnB.Appearance.Options.UseFont = true;
            this.btnB.Location = new System.Drawing.Point(165, 105);
            this.btnB.Name = "btnB";
            this.btnB.Size = new System.Drawing.Size(38, 45);
            this.btnB.TabIndex = 87;
            this.btnB.Text = "B";
            this.btnB.Click += new System.EventHandler(this.btnB_Click);
            // 
            // btnA
            // 
            this.btnA.AllowFocus = false;
            this.btnA.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA.Appearance.Options.UseFont = true;
            this.btnA.Location = new System.Drawing.Point(25, 55);
            this.btnA.Name = "btnA";
            this.btnA.Size = new System.Drawing.Size(38, 45);
            this.btnA.TabIndex = 86;
            this.btnA.Text = "A";
            this.btnA.Click += new System.EventHandler(this.btnA_Click);
            // 
            // btnMoveRigthText
            // 
            this.btnMoveRigthText.AllowFocus = false;
            this.btnMoveRigthText.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveRigthText.Appearance.Options.UseFont = true;
            this.btnMoveRigthText.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveRigthText.Image")));
            this.btnMoveRigthText.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveRigthText.Location = new System.Drawing.Point(45, 156);
            this.btnMoveRigthText.Name = "btnMoveRigthText";
            this.btnMoveRigthText.Size = new System.Drawing.Size(38, 45);
            this.btnMoveRigthText.TabIndex = 85;
            this.btnMoveRigthText.Click += new System.EventHandler(this.btnMoveRigthText_Click);
            // 
            // btnM
            // 
            this.btnM.AllowFocus = false;
            this.btnM.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnM.Appearance.Options.UseFont = true;
            this.btnM.Location = new System.Drawing.Point(245, 105);
            this.btnM.Name = "btnM";
            this.btnM.Size = new System.Drawing.Size(38, 45);
            this.btnM.TabIndex = 84;
            this.btnM.Text = "M";
            this.btnM.Click += new System.EventHandler(this.btnM_Click);
            // 
            // btnTranslateText
            // 
            this.btnTranslateText.Appearance.BorderColor = System.Drawing.Color.Black;
            this.btnTranslateText.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTranslateText.Appearance.Options.UseBorderColor = true;
            this.btnTranslateText.Appearance.Options.UseFont = true;
            this.btnTranslateText.Location = new System.Drawing.Point(5, 207);
            this.btnTranslateText.Name = "btnTranslateText";
            this.btnTranslateText.Size = new System.Drawing.Size(78, 45);
            this.btnTranslateText.TabIndex = 82;
            this.btnTranslateText.Text = "$$$";
            this.btnTranslateText.Click += new System.EventHandler(this.btnTranslateText_Click);
            // 
            // btnR
            // 
            this.btnR.AllowFocus = false;
            this.btnR.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnR.Appearance.Options.UseFont = true;
            this.btnR.Location = new System.Drawing.Point(125, 5);
            this.btnR.Name = "btnR";
            this.btnR.Size = new System.Drawing.Size(38, 45);
            this.btnR.TabIndex = 81;
            this.btnR.Text = "R";
            this.btnR.Click += new System.EventHandler(this.btnR_Click);
            // 
            // btnQ
            // 
            this.btnQ.AllowFocus = false;
            this.btnQ.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQ.Appearance.Options.UseFont = true;
            this.btnQ.Location = new System.Drawing.Point(5, 5);
            this.btnQ.Name = "btnQ";
            this.btnQ.Size = new System.Drawing.Size(38, 45);
            this.btnQ.TabIndex = 80;
            this.btnQ.Text = "Q";
            this.btnQ.Click += new System.EventHandler(this.btnQ_Click);
            // 
            // btnOkText
            // 
            this.btnOkText.AllowFocus = false;
            this.btnOkText.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOkText.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btnOkText.Appearance.Options.UseFont = true;
            this.btnOkText.Appearance.Options.UseForeColor = true;
            this.btnOkText.Location = new System.Drawing.Point(205, 156);
            this.btnOkText.Name = "btnOkText";
            this.btnOkText.Size = new System.Drawing.Size(78, 96);
            this.btnOkText.TabIndex = 66;
            this.btnOkText.Text = "OK";
            this.btnOkText.Click += new System.EventHandler(this.btnOkText_Click);
            // 
            // btnDeleteText
            // 
            this.btnDeleteText.AllowFocus = false;
            this.btnDeleteText.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteText.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btnDeleteText.Appearance.Options.UseFont = true;
            this.btnDeleteText.Appearance.Options.UseForeColor = true;
            this.btnDeleteText.Location = new System.Drawing.Point(87, 156);
            this.btnDeleteText.Name = "btnDeleteText";
            this.btnDeleteText.Size = new System.Drawing.Size(116, 45);
            this.btnDeleteText.TabIndex = 79;
            this.btnDeleteText.Text = "Del";
            this.btnDeleteText.Click += new System.EventHandler(this.btnDeleteText_Click);
            // 
            // btnL
            // 
            this.btnL.AllowFocus = false;
            this.btnL.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnL.Appearance.Options.UseFont = true;
            this.btnL.Location = new System.Drawing.Point(345, 55);
            this.btnL.Name = "btnL";
            this.btnL.Size = new System.Drawing.Size(38, 45);
            this.btnL.TabIndex = 74;
            this.btnL.Text = "L";
            this.btnL.Click += new System.EventHandler(this.btnL_Click);
            // 
            // btnI
            // 
            this.btnI.AllowFocus = false;
            this.btnI.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnI.Appearance.Options.UseFont = true;
            this.btnI.Location = new System.Drawing.Point(285, 5);
            this.btnI.Name = "btnI";
            this.btnI.Size = new System.Drawing.Size(38, 45);
            this.btnI.TabIndex = 77;
            this.btnI.Text = "I";
            this.btnI.Click += new System.EventHandler(this.btnI_Click);
            // 
            // btnH
            // 
            this.btnH.AllowFocus = false;
            this.btnH.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnH.Appearance.Options.UseFont = true;
            this.btnH.Location = new System.Drawing.Point(225, 55);
            this.btnH.Name = "btnH";
            this.btnH.Size = new System.Drawing.Size(38, 45);
            this.btnH.TabIndex = 76;
            this.btnH.Text = "H";
            this.btnH.Click += new System.EventHandler(this.btnH_Click);
            // 
            // btnG
            // 
            this.btnG.AllowFocus = false;
            this.btnG.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnG.Appearance.Options.UseFont = true;
            this.btnG.Location = new System.Drawing.Point(185, 55);
            this.btnG.Name = "btnG";
            this.btnG.Size = new System.Drawing.Size(38, 45);
            this.btnG.TabIndex = 75;
            this.btnG.Text = "G";
            this.btnG.Click += new System.EventHandler(this.btnG_Click);
            // 
            // btnMoveLeftText
            // 
            this.btnMoveLeftText.AllowFocus = false;
            this.btnMoveLeftText.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveLeftText.Appearance.Options.UseFont = true;
            this.btnMoveLeftText.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveLeftText.Image")));
            this.btnMoveLeftText.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveLeftText.Location = new System.Drawing.Point(5, 156);
            this.btnMoveLeftText.Name = "btnMoveLeftText";
            this.btnMoveLeftText.Size = new System.Drawing.Size(38, 45);
            this.btnMoveLeftText.TabIndex = 73;
            this.btnMoveLeftText.Click += new System.EventHandler(this.btnMoveLeftText_Click);
            // 
            // btnK
            // 
            this.btnK.AllowFocus = false;
            this.btnK.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnK.Appearance.Options.UseFont = true;
            this.btnK.Location = new System.Drawing.Point(305, 55);
            this.btnK.Name = "btnK";
            this.btnK.Size = new System.Drawing.Size(38, 45);
            this.btnK.TabIndex = 72;
            this.btnK.Text = "K";
            this.btnK.Click += new System.EventHandler(this.btnK_Click);
            // 
            // btnJ
            // 
            this.btnJ.AllowFocus = false;
            this.btnJ.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnJ.Appearance.Options.UseFont = true;
            this.btnJ.Location = new System.Drawing.Point(265, 55);
            this.btnJ.Name = "btnJ";
            this.btnJ.Size = new System.Drawing.Size(38, 45);
            this.btnJ.TabIndex = 71;
            this.btnJ.Text = "J";
            this.btnJ.Click += new System.EventHandler(this.btnJ_Click);
            // 
            // btnO
            // 
            this.btnO.AllowFocus = false;
            this.btnO.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnO.Appearance.Options.UseFont = true;
            this.btnO.Location = new System.Drawing.Point(325, 5);
            this.btnO.Name = "btnO";
            this.btnO.Size = new System.Drawing.Size(38, 45);
            this.btnO.TabIndex = 70;
            this.btnO.Text = "O";
            this.btnO.Click += new System.EventHandler(this.btnO_Click);
            // 
            // btnN
            // 
            this.btnN.AllowFocus = false;
            this.btnN.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnN.Appearance.Options.UseFont = true;
            this.btnN.Location = new System.Drawing.Point(205, 105);
            this.btnN.Name = "btnN";
            this.btnN.Size = new System.Drawing.Size(38, 45);
            this.btnN.TabIndex = 69;
            this.btnN.Text = "N";
            this.btnN.Click += new System.EventHandler(this.btnN_Click);
            // 
            // btnEscText
            // 
            this.btnEscText.AllowFocus = false;
            this.btnEscText.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEscText.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnEscText.Appearance.Options.UseFont = true;
            this.btnEscText.Appearance.Options.UseForeColor = true;
            this.btnEscText.Location = new System.Drawing.Point(85, 207);
            this.btnEscText.Name = "btnEscText";
            this.btnEscText.Size = new System.Drawing.Size(116, 45);
            this.btnEscText.TabIndex = 67;
            this.btnEscText.Text = "ESC";
            this.btnEscText.Click += new System.EventHandler(this.btnEscText_Click);
            // 
            // pnlKeyboardMoney
            // 
            this.pnlKeyboardMoney.Controls.Add(this.btn500000);
            this.pnlKeyboardMoney.Controls.Add(this.btn200000);
            this.pnlKeyboardMoney.Controls.Add(this.btn100000);
            this.pnlKeyboardMoney.Controls.Add(this.btn50000);
            this.pnlKeyboardMoney.Controls.Add(this.btn20000);
            this.pnlKeyboardMoney.Controls.Add(this.btn10000);
            this.pnlKeyboardMoney.Controls.Add(this.btnMoveRightMoney);
            this.pnlKeyboardMoney.Controls.Add(this.btnPaymentMoney);
            this.pnlKeyboardMoney.Controls.Add(this.btnTranslateMoney);
            this.pnlKeyboardMoney.Controls.Add(this.btnOkMoney);
            this.pnlKeyboardMoney.Controls.Add(this.btnDeleteMoney);
            this.pnlKeyboardMoney.Controls.Add(this.btn2000);
            this.pnlKeyboardMoney.Controls.Add(this.btn1000);
            this.pnlKeyboardMoney.Controls.Add(this.btn500);
            this.pnlKeyboardMoney.Controls.Add(this.btnMoveLeftMoney);
            this.pnlKeyboardMoney.Controls.Add(this.btn5000);
            this.pnlKeyboardMoney.Controls.Add(this.btnEcsMoney);
            this.pnlKeyboardMoney.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlKeyboardMoney.Location = new System.Drawing.Point(2, 2);
            this.pnlKeyboardMoney.Name = "pnlKeyboardMoney";
            this.pnlKeyboardMoney.Size = new System.Drawing.Size(321, 260);
            this.pnlKeyboardMoney.TabIndex = 92;
            // 
            // btn500000
            // 
            this.btn500000.AllowFocus = false;
            this.btn500000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn500000.Appearance.Options.UseFont = true;
            this.btn500000.Location = new System.Drawing.Point(333, 67);
            this.btn500000.Name = "btn500000";
            this.btn500000.Size = new System.Drawing.Size(70, 60);
            this.btn500000.TabIndex = 91;
            this.btn500000.Text = "500,000";
            this.btn500000.Click += new System.EventHandler(this.btn500000_Click);
            // 
            // btn200000
            // 
            this.btn200000.AllowFocus = false;
            this.btn200000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn200000.Appearance.Options.UseFont = true;
            this.btn200000.Location = new System.Drawing.Point(251, 67);
            this.btn200000.Name = "btn200000";
            this.btn200000.Size = new System.Drawing.Size(70, 60);
            this.btn200000.TabIndex = 90;
            this.btn200000.Text = "200,000";
            this.btn200000.Click += new System.EventHandler(this.btn200000_Click);
            // 
            // btn100000
            // 
            this.btn100000.AllowFocus = false;
            this.btn100000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn100000.Appearance.Options.UseFont = true;
            this.btn100000.Location = new System.Drawing.Point(169, 67);
            this.btn100000.Name = "btn100000";
            this.btn100000.Size = new System.Drawing.Size(70, 60);
            this.btn100000.TabIndex = 89;
            this.btn100000.Text = "100,000";
            this.btn100000.Click += new System.EventHandler(this.btn100000_Click);
            // 
            // btn50000
            // 
            this.btn50000.AllowFocus = false;
            this.btn50000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn50000.Appearance.Options.UseFont = true;
            this.btn50000.Location = new System.Drawing.Point(87, 67);
            this.btn50000.Name = "btn50000";
            this.btn50000.Size = new System.Drawing.Size(70, 60);
            this.btn50000.TabIndex = 88;
            this.btn50000.Text = "50,000";
            this.btn50000.Click += new System.EventHandler(this.btn50000_Click);
            // 
            // btn20000
            // 
            this.btn20000.AllowFocus = false;
            this.btn20000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn20000.Appearance.Options.UseFont = true;
            this.btn20000.Location = new System.Drawing.Point(5, 67);
            this.btn20000.Name = "btn20000";
            this.btn20000.Size = new System.Drawing.Size(70, 60);
            this.btn20000.TabIndex = 87;
            this.btn20000.Text = "20,000";
            this.btn20000.Click += new System.EventHandler(this.btn20000_Click);
            // 
            // btn10000
            // 
            this.btn10000.AllowFocus = false;
            this.btn10000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn10000.Appearance.Options.UseFont = true;
            this.btn10000.Location = new System.Drawing.Point(333, 5);
            this.btn10000.Name = "btn10000";
            this.btn10000.Size = new System.Drawing.Size(70, 60);
            this.btn10000.TabIndex = 86;
            this.btn10000.Text = "10,000";
            this.btn10000.Click += new System.EventHandler(this.btn10000_Click);
            // 
            // btnMoveRightMoney
            // 
            this.btnMoveRightMoney.AllowFocus = false;
            this.btnMoveRightMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveRightMoney.Appearance.Options.UseFont = true;
            this.btnMoveRightMoney.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveRightMoney.Image")));
            this.btnMoveRightMoney.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveRightMoney.Location = new System.Drawing.Point(87, 130);
            this.btnMoveRightMoney.Name = "btnMoveRightMoney";
            this.btnMoveRightMoney.Size = new System.Drawing.Size(70, 60);
            this.btnMoveRightMoney.TabIndex = 85;
            this.btnMoveRightMoney.Text = "--->";
            this.btnMoveRightMoney.Click += new System.EventHandler(this.btnMoveRightMoney_Click);
            // 
            // btnPaymentMoney
            // 
            this.btnPaymentMoney.AllowFocus = false;
            this.btnPaymentMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPaymentMoney.Appearance.Options.UseFont = true;
            this.btnPaymentMoney.Location = new System.Drawing.Point(251, 130);
            this.btnPaymentMoney.Name = "btnPaymentMoney";
            this.btnPaymentMoney.Size = new System.Drawing.Size(152, 122);
            this.btnPaymentMoney.TabIndex = 83;
            this.btnPaymentMoney.Text = "F2    \r\n In Hóa Đơn";
            this.btnPaymentMoney.Click += new System.EventHandler(this.btnPaymentMoney_Click);
            // 
            // btnTranslateMoney
            // 
            this.btnTranslateMoney.Appearance.BorderColor = System.Drawing.Color.Black;
            this.btnTranslateMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTranslateMoney.Appearance.Options.UseBorderColor = true;
            this.btnTranslateMoney.Appearance.Options.UseFont = true;
            this.btnTranslateMoney.Location = new System.Drawing.Point(4, 193);
            this.btnTranslateMoney.Name = "btnTranslateMoney";
            this.btnTranslateMoney.Size = new System.Drawing.Size(70, 60);
            this.btnTranslateMoney.TabIndex = 82;
            this.btnTranslateMoney.Text = "123";
            this.btnTranslateMoney.Click += new System.EventHandler(this.btnTranslateMoney_Click);
            // 
            // btnOkMoney
            // 
            this.btnOkMoney.AllowFocus = false;
            this.btnOkMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOkMoney.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btnOkMoney.Appearance.Options.UseFont = true;
            this.btnOkMoney.Appearance.Options.UseForeColor = true;
            this.btnOkMoney.Location = new System.Drawing.Point(169, 193);
            this.btnOkMoney.Name = "btnOkMoney";
            this.btnOkMoney.Size = new System.Drawing.Size(70, 60);
            this.btnOkMoney.TabIndex = 66;
            this.btnOkMoney.Text = "OK";
            this.btnOkMoney.Click += new System.EventHandler(this.btnOkMoney_Click);
            // 
            // btnDeleteMoney
            // 
            this.btnDeleteMoney.AllowFocus = false;
            this.btnDeleteMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteMoney.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btnDeleteMoney.Appearance.Options.UseFont = true;
            this.btnDeleteMoney.Appearance.Options.UseForeColor = true;
            this.btnDeleteMoney.Location = new System.Drawing.Point(169, 130);
            this.btnDeleteMoney.Name = "btnDeleteMoney";
            this.btnDeleteMoney.Size = new System.Drawing.Size(70, 60);
            this.btnDeleteMoney.TabIndex = 79;
            this.btnDeleteMoney.Text = "Del";
            this.btnDeleteMoney.Click += new System.EventHandler(this.btnDeleteMoney_Click);
            // 
            // btn2000
            // 
            this.btn2000.AllowFocus = false;
            this.btn2000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2000.Appearance.Options.UseFont = true;
            this.btn2000.Location = new System.Drawing.Point(169, 5);
            this.btn2000.Name = "btn2000";
            this.btn2000.Size = new System.Drawing.Size(70, 60);
            this.btn2000.TabIndex = 77;
            this.btn2000.Text = "2,000";
            this.btn2000.Click += new System.EventHandler(this.btn2000_Click);
            // 
            // btn1000
            // 
            this.btn1000.AllowFocus = false;
            this.btn1000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1000.Appearance.Options.UseFont = true;
            this.btn1000.Location = new System.Drawing.Point(87, 5);
            this.btn1000.Name = "btn1000";
            this.btn1000.Size = new System.Drawing.Size(70, 60);
            this.btn1000.TabIndex = 76;
            this.btn1000.Text = "1,000";
            this.btn1000.Click += new System.EventHandler(this.btn1000_Click);
            // 
            // btn500
            // 
            this.btn500.AllowFocus = false;
            this.btn500.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn500.Appearance.Options.UseFont = true;
            this.btn500.Location = new System.Drawing.Point(5, 4);
            this.btn500.Name = "btn500";
            this.btn500.Size = new System.Drawing.Size(70, 60);
            this.btn500.TabIndex = 75;
            this.btn500.Text = "500";
            this.btn500.Click += new System.EventHandler(this.btn500_Click);
            // 
            // btnMoveLeftMoney
            // 
            this.btnMoveLeftMoney.AllowFocus = false;
            this.btnMoveLeftMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveLeftMoney.Appearance.Options.UseFont = true;
            this.btnMoveLeftMoney.BackgroundImage = global::KP_RES.Properties.Resources.advance_back_32;
            this.btnMoveLeftMoney.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveLeftMoney.Image")));
            this.btnMoveLeftMoney.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveLeftMoney.Location = new System.Drawing.Point(5, 130);
            this.btnMoveLeftMoney.Name = "btnMoveLeftMoney";
            this.btnMoveLeftMoney.Size = new System.Drawing.Size(70, 60);
            this.btnMoveLeftMoney.TabIndex = 73;
            this.btnMoveLeftMoney.Text = "<---";
            this.btnMoveLeftMoney.Click += new System.EventHandler(this.btnMoveLeftMoney_Click);
            // 
            // btn5000
            // 
            this.btn5000.AllowFocus = false;
            this.btn5000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5000.Appearance.Options.UseFont = true;
            this.btn5000.Location = new System.Drawing.Point(251, 5);
            this.btn5000.Name = "btn5000";
            this.btn5000.Size = new System.Drawing.Size(70, 60);
            this.btn5000.TabIndex = 71;
            this.btn5000.Text = "5,000";
            this.btn5000.Click += new System.EventHandler(this.btn5000_Click);
            // 
            // btnEcsMoney
            // 
            this.btnEcsMoney.AllowFocus = false;
            this.btnEcsMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEcsMoney.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnEcsMoney.Appearance.Options.UseFont = true;
            this.btnEcsMoney.Appearance.Options.UseForeColor = true;
            this.btnEcsMoney.Location = new System.Drawing.Point(87, 193);
            this.btnEcsMoney.Name = "btnEcsMoney";
            this.btnEcsMoney.Size = new System.Drawing.Size(70, 60);
            this.btnEcsMoney.TabIndex = 67;
            this.btnEcsMoney.Text = "ESC";
            this.btnEcsMoney.Click += new System.EventHandler(this.btnEcsMoney_Click);
            // 
            // pnlKeyboardNumber
            // 
            this.pnlKeyboardNumber.Controls.Add(this.btnMoveRightNumber);
            this.pnlKeyboardNumber.Controls.Add(this.btn1);
            this.pnlKeyboardNumber.Controls.Add(this.btnPaymentNumber);
            this.pnlKeyboardNumber.Controls.Add(this.btnTranslateNumber);
            this.pnlKeyboardNumber.Controls.Add(this.btn000);
            this.pnlKeyboardNumber.Controls.Add(this.btn00);
            this.pnlKeyboardNumber.Controls.Add(this.btn0);
            this.pnlKeyboardNumber.Controls.Add(this.btnOkNumber);
            this.pnlKeyboardNumber.Controls.Add(this.btnDeleteNumber);
            this.pnlKeyboardNumber.Controls.Add(this.btn6);
            this.pnlKeyboardNumber.Controls.Add(this.btn9);
            this.pnlKeyboardNumber.Controls.Add(this.btn8);
            this.pnlKeyboardNumber.Controls.Add(this.btn7);
            this.pnlKeyboardNumber.Controls.Add(this.btnMoveLeftNumber);
            this.pnlKeyboardNumber.Controls.Add(this.btn5);
            this.pnlKeyboardNumber.Controls.Add(this.btn4);
            this.pnlKeyboardNumber.Controls.Add(this.btn3);
            this.pnlKeyboardNumber.Controls.Add(this.btn2);
            this.pnlKeyboardNumber.Controls.Add(this.btnEscNumber);
            this.pnlKeyboardNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlKeyboardNumber.Location = new System.Drawing.Point(2, 2);
            this.pnlKeyboardNumber.Name = "pnlKeyboardNumber";
            this.pnlKeyboardNumber.Size = new System.Drawing.Size(321, 260);
            this.pnlKeyboardNumber.TabIndex = 68;
            // 
            // btnMoveRightNumber
            // 
            this.btnMoveRightNumber.AllowFocus = false;
            this.btnMoveRightNumber.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveRightNumber.Appearance.Options.UseFont = true;
            this.btnMoveRightNumber.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveRightNumber.Image")));
            this.btnMoveRightNumber.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveRightNumber.Location = new System.Drawing.Point(272, 5);
            this.btnMoveRightNumber.Name = "btnMoveRightNumber";
            this.btnMoveRightNumber.Size = new System.Drawing.Size(60, 60);
            this.btnMoveRightNumber.TabIndex = 85;
            this.btnMoveRightNumber.Text = "--->";
            this.btnMoveRightNumber.Click += new System.EventHandler(this.btnMoveRightNumber_Click);
            // 
            // btn1
            // 
            this.btn1.AllowFocus = false;
            this.btn1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.Appearance.Options.UseFont = true;
            this.btn1.Location = new System.Drawing.Point(12, 131);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(60, 60);
            this.btn1.TabIndex = 84;
            this.btn1.Text = "1";
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btnPaymentNumber
            // 
            this.btnPaymentNumber.AllowFocus = false;
            this.btnPaymentNumber.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPaymentNumber.Appearance.Options.UseFont = true;
            this.btnPaymentNumber.Location = new System.Drawing.Point(272, 134);
            this.btnPaymentNumber.Name = "btnPaymentNumber";
            this.btnPaymentNumber.Size = new System.Drawing.Size(124, 116);
            this.btnPaymentNumber.TabIndex = 83;
            this.btnPaymentNumber.Text = "F2    \r\nIn Hóa Đơn\r\n";
            this.btnPaymentNumber.Click += new System.EventHandler(this.btnPaymentNumber_Click);
            // 
            // btnTranslateNumber
            // 
            this.btnTranslateNumber.Appearance.BorderColor = System.Drawing.Color.Black;
            this.btnTranslateNumber.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTranslateNumber.Appearance.Options.UseBorderColor = true;
            this.btnTranslateNumber.Appearance.Options.UseFont = true;
            this.btnTranslateNumber.Location = new System.Drawing.Point(12, 193);
            this.btnTranslateNumber.Name = "btnTranslateNumber";
            this.btnTranslateNumber.Size = new System.Drawing.Size(60, 60);
            this.btnTranslateNumber.TabIndex = 82;
            this.btnTranslateNumber.Text = "ABC";
            this.btnTranslateNumber.Click += new System.EventHandler(this.btnTranslateNumber_Click);
            // 
            // btn000
            // 
            this.btn000.AllowFocus = false;
            this.btn000.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn000.Appearance.Options.UseFont = true;
            this.btn000.Location = new System.Drawing.Point(207, 193);
            this.btn000.Name = "btn000";
            this.btn000.Size = new System.Drawing.Size(60, 60);
            this.btn000.TabIndex = 81;
            this.btn000.Text = "000";
            this.btn000.Click += new System.EventHandler(this.btn000_Click);
            // 
            // btn00
            // 
            this.btn00.AllowFocus = false;
            this.btn00.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn00.Appearance.Options.UseFont = true;
            this.btn00.Location = new System.Drawing.Point(141, 193);
            this.btn00.Name = "btn00";
            this.btn00.Size = new System.Drawing.Size(60, 60);
            this.btn00.TabIndex = 80;
            this.btn00.Text = "00";
            this.btn00.Click += new System.EventHandler(this.btn00_Click);
            // 
            // btn0
            // 
            this.btn0.AllowFocus = false;
            this.btn0.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn0.Appearance.Options.UseFont = true;
            this.btn0.Location = new System.Drawing.Point(77, 193);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(60, 60);
            this.btn0.TabIndex = 78;
            this.btn0.Text = "0";
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btnOkNumber
            // 
            this.btnOkNumber.AllowFocus = false;
            this.btnOkNumber.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOkNumber.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btnOkNumber.Appearance.Options.UseFont = true;
            this.btnOkNumber.Appearance.Options.UseForeColor = true;
            this.btnOkNumber.Location = new System.Drawing.Point(272, 68);
            this.btnOkNumber.Name = "btnOkNumber";
            this.btnOkNumber.Size = new System.Drawing.Size(124, 60);
            this.btnOkNumber.TabIndex = 66;
            this.btnOkNumber.Text = "OK";
            this.btnOkNumber.Click += new System.EventHandler(this.btnOkNumber_Click);
            // 
            // btnDeleteNumber
            // 
            this.btnDeleteNumber.AllowFocus = false;
            this.btnDeleteNumber.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteNumber.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btnDeleteNumber.Appearance.Options.UseFont = true;
            this.btnDeleteNumber.Appearance.Options.UseForeColor = true;
            this.btnDeleteNumber.Location = new System.Drawing.Point(336, 5);
            this.btnDeleteNumber.Name = "btnDeleteNumber";
            this.btnDeleteNumber.Size = new System.Drawing.Size(60, 60);
            this.btnDeleteNumber.TabIndex = 79;
            this.btnDeleteNumber.Text = "Del";
            this.btnDeleteNumber.Click += new System.EventHandler(this.btnDeleteNumber_Click);
            // 
            // btn6
            // 
            this.btn6.AllowFocus = false;
            this.btn6.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn6.Appearance.Options.UseFont = true;
            this.btn6.Location = new System.Drawing.Point(141, 68);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(60, 60);
            this.btn6.TabIndex = 74;
            this.btn6.Text = "6";
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            // 
            // btn9
            // 
            this.btn9.AllowFocus = false;
            this.btn9.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn9.Appearance.Options.UseFont = true;
            this.btn9.Location = new System.Drawing.Point(141, 5);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(60, 60);
            this.btn9.TabIndex = 77;
            this.btn9.Text = "9";
            this.btn9.Click += new System.EventHandler(this.btn9_Click);
            // 
            // btn8
            // 
            this.btn8.AllowFocus = false;
            this.btn8.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn8.Appearance.Options.UseFont = true;
            this.btn8.Location = new System.Drawing.Point(77, 5);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(60, 60);
            this.btn8.TabIndex = 76;
            this.btn8.Text = "8";
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            // 
            // btn7
            // 
            this.btn7.AllowFocus = false;
            this.btn7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn7.Appearance.Options.UseFont = true;
            this.btn7.Location = new System.Drawing.Point(12, 5);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(60, 60);
            this.btn7.TabIndex = 75;
            this.btn7.Text = "7";
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // btnMoveLeftNumber
            // 
            this.btnMoveLeftNumber.AllowFocus = false;
            this.btnMoveLeftNumber.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveLeftNumber.Appearance.Options.UseFont = true;
            this.btnMoveLeftNumber.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveLeftNumber.Image")));
            this.btnMoveLeftNumber.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveLeftNumber.Location = new System.Drawing.Point(207, 4);
            this.btnMoveLeftNumber.Name = "btnMoveLeftNumber";
            this.btnMoveLeftNumber.Size = new System.Drawing.Size(60, 60);
            this.btnMoveLeftNumber.TabIndex = 73;
            this.btnMoveLeftNumber.Click += new System.EventHandler(this.btnMoveLeftNumber_Click);
            // 
            // btn5
            // 
            this.btn5.AllowFocus = false;
            this.btn5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5.Appearance.Options.UseFont = true;
            this.btn5.Location = new System.Drawing.Point(77, 68);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(60, 60);
            this.btn5.TabIndex = 72;
            this.btn5.Text = "5";
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btn4
            // 
            this.btn4.AllowFocus = false;
            this.btn4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4.Appearance.Options.UseFont = true;
            this.btn4.Location = new System.Drawing.Point(12, 68);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(60, 60);
            this.btn4.TabIndex = 71;
            this.btn4.Text = "4";
            this.btn4.Click += new System.EventHandler(this.btn4_Click);
            // 
            // btn3
            // 
            this.btn3.AllowFocus = false;
            this.btn3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn3.Appearance.Options.UseFont = true;
            this.btn3.Location = new System.Drawing.Point(141, 131);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(60, 60);
            this.btn3.TabIndex = 70;
            this.btn3.Text = "3";
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btn2
            // 
            this.btn2.AllowFocus = false;
            this.btn2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2.Appearance.Options.UseFont = true;
            this.btn2.Location = new System.Drawing.Point(77, 131);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(60, 60);
            this.btn2.TabIndex = 69;
            this.btn2.Text = "2";
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btnEscNumber
            // 
            this.btnEscNumber.AllowFocus = false;
            this.btnEscNumber.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEscNumber.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnEscNumber.Appearance.Options.UseFont = true;
            this.btnEscNumber.Appearance.Options.UseForeColor = true;
            this.btnEscNumber.Location = new System.Drawing.Point(207, 68);
            this.btnEscNumber.Name = "btnEscNumber";
            this.btnEscNumber.Size = new System.Drawing.Size(60, 123);
            this.btnEscNumber.TabIndex = 67;
            this.btnEscNumber.Text = "ESC";
            this.btnEscNumber.Click += new System.EventHandler(this.btnEscNumber_Click);
            // 
            // pnlShowForeignCurrency
            // 
            this.pnlShowForeignCurrency.Controls.Add(this.lbExchangeRate);
            this.pnlShowForeignCurrency.Controls.Add(this.lbKindMoney);
            this.pnlShowForeignCurrency.Controls.Add(this.lb_loaitien);
            this.pnlShowForeignCurrency.Controls.Add(this.lb_tygia);
            this.pnlShowForeignCurrency.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlShowForeignCurrency.Location = new System.Drawing.Point(2, 262);
            this.pnlShowForeignCurrency.Name = "pnlShowForeignCurrency";
            this.pnlShowForeignCurrency.Size = new System.Drawing.Size(321, 34);
            this.pnlShowForeignCurrency.TabIndex = 67;
            // 
            // lbExchangeRate
            // 
            this.lbExchangeRate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbExchangeRate.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lbExchangeRate.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbExchangeRate.Location = new System.Drawing.Point(242, 8);
            this.lbExchangeRate.Name = "lbExchangeRate";
            this.lbExchangeRate.Size = new System.Drawing.Size(8, 16);
            this.lbExchangeRate.TabIndex = 11;
            this.lbExchangeRate.Text = "1";
            // 
            // lbKindMoney
            // 
            this.lbKindMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lbKindMoney.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbKindMoney.Location = new System.Drawing.Point(97, 8);
            this.lbKindMoney.Name = "lbKindMoney";
            this.lbKindMoney.Size = new System.Drawing.Size(27, 16);
            this.lbKindMoney.TabIndex = 10;
            this.lbKindMoney.Text = "VNĐ";
            // 
            // lb_loaitien
            // 
            this.lb_loaitien.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lb_loaitien.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lb_loaitien.Location = new System.Drawing.Point(6, 8);
            this.lb_loaitien.Name = "lb_loaitien";
            this.lb_loaitien.Size = new System.Drawing.Size(66, 17);
            this.lb_loaitien.TabIndex = 9;
            this.lb_loaitien.Text = "Loại Tiền:";
            // 
            // lb_tygia
            // 
            this.lb_tygia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_tygia.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lb_tygia.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lb_tygia.Location = new System.Drawing.Point(140, 8);
            this.lb_tygia.Name = "lb_tygia";
            this.lb_tygia.Size = new System.Drawing.Size(51, 17);
            this.lb_tygia.TabIndex = 8;
            this.lb_tygia.Text = "Tỷ Giá :";
            // 
            // pnlUpDownForeignCurrency
            // 
            this.pnlUpDownForeignCurrency.Controls.Add(this.btnDownForeignCurrency);
            this.pnlUpDownForeignCurrency.Controls.Add(this.btnUpForeignCurrency);
            this.pnlUpDownForeignCurrency.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlUpDownForeignCurrency.Location = new System.Drawing.Point(280, 0);
            this.pnlUpDownForeignCurrency.Name = "pnlUpDownForeignCurrency";
            this.pnlUpDownForeignCurrency.Size = new System.Drawing.Size(38, 298);
            this.pnlUpDownForeignCurrency.TabIndex = 3;
            // 
            // btnDownForeignCurrency
            // 
            this.btnDownForeignCurrency.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnDownForeignCurrency.Image = ((System.Drawing.Image)(resources.GetObject("btnDownForeignCurrency.Image")));
            this.btnDownForeignCurrency.Location = new System.Drawing.Point(2, 243);
            this.btnDownForeignCurrency.Name = "btnDownForeignCurrency";
            this.btnDownForeignCurrency.Size = new System.Drawing.Size(34, 53);
            this.btnDownForeignCurrency.TabIndex = 8;
            this.btnDownForeignCurrency.Text = "simpleButton4";
            this.btnDownForeignCurrency.Click += new System.EventHandler(this.btnDownForeignCurrency_Click);
            // 
            // btnUpForeignCurrency
            // 
            this.btnUpForeignCurrency.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnUpForeignCurrency.Image = ((System.Drawing.Image)(resources.GetObject("btnUpForeignCurrency.Image")));
            this.btnUpForeignCurrency.Location = new System.Drawing.Point(2, 2);
            this.btnUpForeignCurrency.Name = "btnUpForeignCurrency";
            this.btnUpForeignCurrency.Size = new System.Drawing.Size(34, 53);
            this.btnUpForeignCurrency.TabIndex = 7;
            this.btnUpForeignCurrency.Text = "simpleButton1";
            this.btnUpForeignCurrency.Click += new System.EventHandler(this.btnUpForeignCurrency_Click);
            // 
            // pnlForeignCurrency
            // 
            this.pnlForeignCurrency.Controls.Add(this.gcForeignCurrency);
            this.pnlForeignCurrency.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlForeignCurrency.Location = new System.Drawing.Point(0, 0);
            this.pnlForeignCurrency.Name = "pnlForeignCurrency";
            this.pnlForeignCurrency.Size = new System.Drawing.Size(280, 298);
            this.pnlForeignCurrency.TabIndex = 1;
            // 
            // gcForeignCurrency
            // 
            this.gcForeignCurrency.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.RelationName = "Level1";
            this.gcForeignCurrency.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gcForeignCurrency.Location = new System.Drawing.Point(2, 2);
            this.gcForeignCurrency.MainView = this.gvListForiegnCurrency;
            this.gcForeignCurrency.Name = "gcForeignCurrency";
            this.gcForeignCurrency.Size = new System.Drawing.Size(276, 294);
            this.gcForeignCurrency.TabIndex = 6;
            this.gcForeignCurrency.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvListForiegnCurrency});
            // 
            // gvListForiegnCurrency
            // 
            this.gvListForiegnCurrency.ColumnPanelRowHeight = 30;
            this.gvListForiegnCurrency.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT1,
            this.MA,
            this.TEN,
            this.TYGIA,
            this.PHUTHU});
            this.gvListForiegnCurrency.GridControl = this.gcForeignCurrency;
            this.gvListForiegnCurrency.Name = "gvListForiegnCurrency";
            this.gvListForiegnCurrency.OptionsView.ShowGroupPanel = false;
            this.gvListForiegnCurrency.OptionsView.ShowIndicator = false;
            this.gvListForiegnCurrency.RowHeight = 43;
            this.gvListForiegnCurrency.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gvListForiegnCurrency.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gvListForiegnCurrency_CustomDrawCell);
            this.gvListForiegnCurrency.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvListForiegnCurrency_FocusedRowChanged);
            // 
            // STT1
            // 
            this.STT1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.STT1.AppearanceCell.Options.UseFont = true;
            this.STT1.AppearanceCell.Options.UseTextOptions = true;
            this.STT1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.STT1.AppearanceHeader.Options.UseFont = true;
            this.STT1.AppearanceHeader.Options.UseTextOptions = true;
            this.STT1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT1.Caption = "STT";
            this.STT1.FieldName = "STT1";
            this.STT1.Name = "STT1";
            this.STT1.OptionsColumn.AllowEdit = false;
            this.STT1.OptionsColumn.AllowFocus = false;
            this.STT1.Visible = true;
            this.STT1.VisibleIndex = 0;
            this.STT1.Width = 26;
            // 
            // MA
            // 
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.Caption = "Mã Nguyên Tệ";
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            // 
            // TEN
            // 
            this.TEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TEN.AppearanceCell.Options.UseFont = true;
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.Caption = "Nguyên Tệ";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowEdit = false;
            this.TEN.OptionsColumn.AllowFocus = false;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 1;
            this.TEN.Width = 55;
            // 
            // TYGIA
            // 
            this.TYGIA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TYGIA.AppearanceCell.Options.UseFont = true;
            this.TYGIA.AppearanceCell.Options.UseTextOptions = true;
            this.TYGIA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TYGIA.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TYGIA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TYGIA.AppearanceHeader.Options.UseFont = true;
            this.TYGIA.AppearanceHeader.Options.UseTextOptions = true;
            this.TYGIA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TYGIA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TYGIA.Caption = "Tỷ Giá";
            this.TYGIA.DisplayFormat.FormatString = "N0";
            this.TYGIA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TYGIA.FieldName = "TYGIA";
            this.TYGIA.Name = "TYGIA";
            this.TYGIA.OptionsColumn.AllowEdit = false;
            this.TYGIA.OptionsColumn.AllowFocus = false;
            this.TYGIA.Visible = true;
            this.TYGIA.VisibleIndex = 2;
            this.TYGIA.Width = 46;
            // 
            // PHUTHU
            // 
            this.PHUTHU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.PHUTHU.AppearanceCell.Options.UseFont = true;
            this.PHUTHU.AppearanceCell.Options.UseTextOptions = true;
            this.PHUTHU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PHUTHU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PHUTHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PHUTHU.AppearanceHeader.Options.UseFont = true;
            this.PHUTHU.AppearanceHeader.Options.UseTextOptions = true;
            this.PHUTHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PHUTHU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PHUTHU.Caption = "% Phụ Thu";
            this.PHUTHU.FieldName = "PHUTHU";
            this.PHUTHU.Name = "PHUTHU";
            this.PHUTHU.OptionsColumn.AllowEdit = false;
            this.PHUTHU.OptionsColumn.AllowFocus = false;
            this.PHUTHU.Width = 52;
            // 
            // pnlHeaderShowPayment
            // 
            this.pnlHeaderShowPayment.Controls.Add(this.lbInforName);
            this.pnlHeaderShowPayment.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeaderShowPayment.Location = new System.Drawing.Point(2, 2);
            this.pnlHeaderShowPayment.Name = "pnlHeaderShowPayment";
            this.pnlHeaderShowPayment.Size = new System.Drawing.Size(643, 35);
            this.pnlHeaderShowPayment.TabIndex = 34;
            // 
            // lbInforName
            // 
            this.lbInforName.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbInforName.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbInforName.Location = new System.Drawing.Point(5, 7);
            this.lbInforName.Name = "lbInforName";
            this.lbInforName.Size = new System.Drawing.Size(129, 19);
            this.lbInforName.TabIndex = 1;
            this.lbInforName.Text = "thông tin phòng";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.panelControl8);
            this.panelControl6.Controls.Add(this.panelControl10);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl6.Location = new System.Drawing.Point(0, 0);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(1024, 70);
            this.panelControl6.TabIndex = 1;
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.lb_Quay);
            this.panelControl8.Controls.Add(this.lb_Kho);
            this.panelControl8.Controls.Add(this.lbgiay);
            this.panelControl8.Controls.Add(this.lbCaBan);
            this.panelControl8.Controls.Add(this.lbGio);
            this.panelControl8.Controls.Add(this.lb_NhanVien);
            this.panelControl8.Controls.Add(this.lbtimeby);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl8.Location = new System.Drawing.Point(198, 2);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(824, 66);
            this.panelControl8.TabIndex = 3;
            // 
            // lb_Quay
            // 
            this.lb_Quay.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_Quay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_Quay.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_Quay.Location = new System.Drawing.Point(15, 5);
            this.lb_Quay.Name = "lb_Quay";
            this.lb_Quay.Size = new System.Drawing.Size(41, 19);
            this.lb_Quay.TabIndex = 14;
            this.lb_Quay.Text = "Quầy";
            // 
            // lb_Kho
            // 
            this.lb_Kho.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_Kho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_Kho.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lb_Kho.Location = new System.Drawing.Point(15, 34);
            this.lb_Kho.Name = "lb_Kho";
            this.lb_Kho.Size = new System.Drawing.Size(41, 19);
            this.lb_Kho.TabIndex = 15;
            this.lb_Kho.Text = "Quầy";
            // 
            // lbgiay
            // 
            this.lbgiay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbgiay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbgiay.Location = new System.Drawing.Point(783, 27);
            this.lbgiay.Name = "lbgiay";
            this.lbgiay.Size = new System.Drawing.Size(18, 19);
            this.lbgiay.TabIndex = 19;
            this.lbgiay.Text = "00";
            // 
            // lbCaBan
            // 
            this.lbCaBan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCaBan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbCaBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbCaBan.Location = new System.Drawing.Point(716, 5);
            this.lbCaBan.Name = "lbCaBan";
            this.lbCaBan.Size = new System.Drawing.Size(94, 19);
            this.lbCaBan.TabIndex = 17;
            this.lbCaBan.Text = "21-12-2012";
            // 
            // lbGio
            // 
            this.lbGio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGio.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGio.Location = new System.Drawing.Point(731, 34);
            this.lbGio.Name = "lbGio";
            this.lbGio.Size = new System.Drawing.Size(46, 19);
            this.lbGio.TabIndex = 20;
            this.lbGio.Text = "00:00";
            // 
            // lb_NhanVien
            // 
            this.lb_NhanVien.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lb_NhanVien.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lb_NhanVien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lb_NhanVien.Location = new System.Drawing.Point(357, 5);
            this.lb_NhanVien.Name = "lb_NhanVien";
            this.lb_NhanVien.Size = new System.Drawing.Size(89, 19);
            this.lb_NhanVien.TabIndex = 18;
            this.lb_NhanVien.Text = "Nhân Viên:";
            // 
            // lbtimeby
            // 
            this.lbtimeby.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbtimeby.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbtimeby.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbtimeby.Location = new System.Drawing.Point(357, 38);
            this.lbtimeby.Name = "lbtimeby";
            this.lbtimeby.Size = new System.Drawing.Size(21, 19);
            this.lbtimeby.TabIndex = 16;
            this.lbtimeby.Text = "Ca";
            // 
            // panelControl10
            // 
            this.panelControl10.Controls.Add(this.pictureBox1);
            this.panelControl10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl10.Location = new System.Drawing.Point(2, 2);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(196, 66);
            this.panelControl10.TabIndex = 21;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(8, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(177, 41);
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // Frm_Payment_Nhahang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 612);
            this.Controls.Add(this.pnlPaymentShowFill);
            this.Controls.Add(this.pnlGridviewActionMain);
            this.Controls.Add(this.panelControl6);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "Frm_Payment_Nhahang";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Payment";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frm_Payment_Nhahang_FormClosing);
            this.Load += new System.EventHandler(this.Payment_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Payment_Nhahang_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pnlGridviewActionMain)).EndInit();
            this.pnlGridviewActionMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlInforBuyProduct)).EndInit();
            this.pnlInforBuyProduct.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlGridview)).EndInit();
            this.pnlGridview.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcListProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnUpDownGridview)).EndInit();
            this.pnUpDownGridview.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlInforMoneyAndCustomer)).EndInit();
            this.pnlInforMoneyAndCustomer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlInforCustomer)).EndInit();
            this.pnlInforCustomer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlInnerCustomerShow)).EndInit();
            this.pnlInnerCustomerShow.ResumeLayout(false);
            this.pnlInnerCustomerShow.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDearCustomerShow)).EndInit();
            this.pnlDearCustomerShow.ResumeLayout(false);
            this.pnlDearCustomerShow.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInforMoney)).EndInit();
            this.pnlInforMoney.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.panelControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlActionCashier)).EndInit();
            this.pnlActionCashier.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnDacThu)).EndInit();
            this.pnDacThu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlPayment)).EndInit();
            this.pnlPayment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlPaymentButton)).EndInit();
            this.pnlPaymentButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlPaymentShowFill)).EndInit();
            this.pnlPaymentShowFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlShowInforPayment)).EndInit();
            this.pnlShowInforPayment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlPhieuMuaHang)).EndInit();
            this.pnlPhieuMuaHang.ResumeLayout(false);
            this.pnlPhieuMuaHang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNhapPhieuMuaHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTienKhachTra)).EndInit();
            this.pnlTienKhachTra.ResumeLayout(false);
            this.pnlTienKhachTra.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputMoney.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPhiPhuVu)).EndInit();
            this.pnlPhiPhuVu.ResumeLayout(false);
            this.pnlPhiPhuVu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceMoney.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTTTNB)).EndInit();
            this.pnlTTTNB.ResumeLayout(false);
            this.pnlTTTNB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInnerCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlChietKhau)).EndInit();
            this.pnlChietKhau.ResumeLayout(false);
            this.pnlChietKhau.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputDiscount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKHTT)).EndInit();
            this.pnlKHTT.ResumeLayout(false);
            this.pnlKHTT.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDearCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlVoucher)).EndInit();
            this.pnlVoucher.ResumeLayout(false);
            this.pnlVoucher.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputVoucher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMagiamgia)).EndInit();
            this.pnlMagiamgia.ResumeLayout(false);
            this.pnlMagiamgia.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputMagiamgia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMoneyReply)).EndInit();
            this.pnlMoneyReply.ResumeLayout(false);
            this.pnlMoneyReply.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl30)).EndInit();
            this.panelControl30.ResumeLayout(false);
            this.panelControl30.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlFirst)).EndInit();
            this.pnlFirst.ResumeLayout(false);
            this.pnlFirst.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSecond)).EndInit();
            this.pnlSecond.ResumeLayout(false);
            this.pnlSecond.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlThird)).EndInit();
            this.pnlThird.ResumeLayout(false);
            this.pnlThird.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl29)).EndInit();
            this.panelControl29.ResumeLayout(false);
            this.panelControl29.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl55)).EndInit();
            this.panelControl55.ResumeLayout(false);
            this.panelControl55.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl51)).EndInit();
            this.panelControl51.ResumeLayout(false);
            this.panelControl51.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl27)).EndInit();
            this.panelControl27.ResumeLayout(false);
            this.panelControl27.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl31)).EndInit();
            this.panelControl31.ResumeLayout(false);
            this.panelControl31.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlVisa)).EndInit();
            this.pnlVisa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameVisa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCwCvc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdCodeCreate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdCodeInput.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMoneyVisa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdCardVisa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHeaderVisa)).EndInit();
            this.pnlHeaderVisa.ResumeLayout(false);
            this.pnlHeaderVisa.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlCongNo)).EndInit();
            this.pnlCongNo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palCongNo_Noidung)).EndInit();
            this.palCongNo_Noidung.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.panelControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCNMaKH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCNSoDT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayHenTra.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayHenTra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCongNo_Tien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).EndInit();
            this.panelControl13.ResumeLayout(false);
            this.panelControl13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBottomPayment)).EndInit();
            this.pnlBottomPayment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlKeyboardAndShowForeignCurrency)).EndInit();
            this.pnlKeyboardAndShowForeignCurrency.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlKeyBoardText)).EndInit();
            this.pnlKeyBoardText.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlKeyboardMoney)).EndInit();
            this.pnlKeyboardMoney.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlKeyboardNumber)).EndInit();
            this.pnlKeyboardNumber.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlShowForeignCurrency)).EndInit();
            this.pnlShowForeignCurrency.ResumeLayout(false);
            this.pnlShowForeignCurrency.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlUpDownForeignCurrency)).EndInit();
            this.pnlUpDownForeignCurrency.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlForeignCurrency)).EndInit();
            this.pnlForeignCurrency.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcForeignCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListForiegnCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHeaderShowPayment)).EndInit();
            this.pnlHeaderShowPayment.ResumeLayout(false);
            this.pnlHeaderShowPayment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            this.panelControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnlGridviewActionMain;
        private DevExpress.XtraEditors.PanelControl pnlPaymentShowFill;
        private DevExpress.XtraEditors.PanelControl pnlHeaderShowPayment;
        private DevExpress.XtraEditors.LabelControl lbInforName;
        private DevExpress.XtraEditors.PanelControl pnlShowInforPayment;
        private DevExpress.XtraEditors.PanelControl pnlTienKhachTra;
        private DevExpress.XtraEditors.TextEdit txtInputMoney;
        private DevExpress.XtraEditors.LabelControl lbPayment;
        private DevExpress.XtraEditors.PanelControl pnlChietKhau;
        private DevExpress.XtraEditors.TextEdit txtInputDiscount;
        private DevExpress.XtraEditors.LabelControl lb_nhapchietkhau;
        private DevExpress.XtraEditors.PanelControl pnlThird;
        private DevExpress.XtraEditors.LabelControl lbThird;
        private DevExpress.XtraEditors.LabelControl lbThirdMoney;
        private DevExpress.XtraEditors.PanelControl pnlSecond;
        private DevExpress.XtraEditors.LabelControl lbSecond;
        private DevExpress.XtraEditors.LabelControl lbSecondMoney;
        private DevExpress.XtraEditors.PanelControl pnlFirst;
        private DevExpress.XtraEditors.LabelControl lbFirst;
        private DevExpress.XtraEditors.LabelControl lbFirstMoney;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.PanelControl panelControl27;
        private DevExpress.XtraEditors.LabelControl lbDiscountPayment;
        private DevExpress.XtraEditors.LabelControl lbDiscount;
        private DevExpress.XtraEditors.LabelControl lb_chietkhau;
        private DevExpress.XtraEditors.PanelControl pnlMoneyReply;
        private DevExpress.XtraEditors.LabelControl lbReplyCustomer;
        private DevExpress.XtraEditors.LabelControl lbVNDReply;
        private DevExpress.XtraEditors.LabelControl lb_tientralai;
        private DevExpress.XtraEditors.PanelControl panelControl29;
        private DevExpress.XtraEditors.LabelControl lbTotalMoneyAll;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl lb_TCthanhtoan;
        private DevExpress.XtraEditors.PanelControl panelControl30;
        private DevExpress.XtraEditors.LabelControl lbGetCustomer;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl lb_tienkhachtra;
        private DevExpress.XtraEditors.PanelControl panelControl31;
        private DevExpress.XtraEditors.LabelControl lbTotalMoneyPayment;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl lb_tongthanhtoan;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.PanelControl pnlBottomPayment;
        private DevExpress.XtraEditors.PanelControl pnlForeignCurrency;
        private DevExpress.XtraGrid.GridControl gcForeignCurrency;
        private DevExpress.XtraGrid.Views.Grid.GridView gvListForiegnCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn STT1;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraGrid.Columns.GridColumn TYGIA;
        private DevExpress.XtraGrid.Columns.GridColumn PHUTHU;
        private DevExpress.XtraEditors.PanelControl pnlUpDownForeignCurrency;
        private DevExpress.XtraEditors.SimpleButton btnUpForeignCurrency;
      
        private DevExpress.XtraEditors.PanelControl pnlKeyboardAndShowForeignCurrency;
        private DevExpress.XtraEditors.PanelControl pnlKeyboardNumber;
        private DevExpress.XtraEditors.SimpleButton btnMoveRightNumber;
        private DevExpress.XtraEditors.SimpleButton btn1;
        private DevExpress.XtraEditors.SimpleButton btnPaymentNumber;
        private DevExpress.XtraEditors.SimpleButton btnTranslateNumber;
        private DevExpress.XtraEditors.SimpleButton btn000;
        private DevExpress.XtraEditors.SimpleButton btn00;
        private DevExpress.XtraEditors.SimpleButton btn0;
        private DevExpress.XtraEditors.SimpleButton btnOkNumber;
        private DevExpress.XtraEditors.SimpleButton btnDeleteNumber;
        private DevExpress.XtraEditors.SimpleButton btn6;
        private DevExpress.XtraEditors.SimpleButton btn9;
        private DevExpress.XtraEditors.SimpleButton btn8;
        private DevExpress.XtraEditors.SimpleButton btn7;
        private DevExpress.XtraEditors.SimpleButton btnMoveLeftNumber;
        private DevExpress.XtraEditors.SimpleButton btn5;
        private DevExpress.XtraEditors.SimpleButton btn4;
        private DevExpress.XtraEditors.SimpleButton btn3;
        private DevExpress.XtraEditors.SimpleButton btn2;
        private DevExpress.XtraEditors.SimpleButton btnEscNumber;
        private DevExpress.XtraEditors.PanelControl pnlShowForeignCurrency;
        private DevExpress.XtraEditors.LabelControl lbExchangeRate;
        private DevExpress.XtraEditors.LabelControl lbKindMoney;
        private DevExpress.XtraEditors.LabelControl lb_loaitien;
        private DevExpress.XtraEditors.LabelControl lb_tygia;
        private DevExpress.XtraEditors.PanelControl pnlActionCashier;
        private DevExpress.XtraEditors.PanelControl pnlPayment;
        private DevExpress.XtraEditors.SimpleButton btnRetypePayment;
        private DevExpress.XtraEditors.SimpleButton btnDiscount;
        private DevExpress.XtraEditors.SimpleButton btnCardImtimateCustomer;
        private DevExpress.XtraEditors.SimpleButton btnCancelPayment;
        private DevExpress.XtraEditors.SimpleButton btnNotPrintBill;
        private DevExpress.XtraEditors.SimpleButton btnCardInternalPayment;
        private DevExpress.XtraEditors.PanelControl pnlPaymentButton;
        private DevExpress.XtraEditors.PanelControl pnlInforBuyProduct;
        private DevExpress.XtraEditors.PanelControl pnlGridview;
        private DevExpress.XtraGrid.GridControl gcListProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView gvListProduct;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn ProductName;
        private DevExpress.XtraGrid.Columns.GridColumn Quantum;
        private DevExpress.XtraGrid.Columns.GridColumn Price;
        private DevExpress.XtraGrid.Columns.GridColumn TheMoney;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;
        private DevExpress.XtraGrid.Columns.GridColumn MA_BEP;
        private DevExpress.XtraGrid.Columns.GridColumn THUE;
        private DevExpress.XtraEditors.PanelControl pnlInforMoneyAndCustomer;
        private DevExpress.XtraEditors.LabelControl lbTheMoney;
        private DevExpress.XtraEditors.LabelControl lb_thanhtien;
        private DevExpress.XtraEditors.LabelControl lbHourMoney;
        private DevExpress.XtraEditors.LabelControl lb_tiengio;
        private DevExpress.XtraEditors.LabelControl lbTotalMoney;
        private DevExpress.XtraEditors.LabelControl lb_tongcong;
        private DevExpress.XtraEditors.PanelControl panelControl55;
        private DevExpress.XtraEditors.LabelControl lbMoneyGetFirst;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.LabelControl lb_tiendatcoc;
        private DevExpress.XtraEditors.PanelControl panelControl51;
        private DevExpress.XtraEditors.LabelControl lbServiceMoney;
        private DevExpress.XtraEditors.LabelControl lbService;
        private DevExpress.XtraEditors.LabelControl lb_phiphucvu;
        private DevExpress.XtraEditors.PanelControl pnlKeyBoardText;
        private DevExpress.XtraEditors.SimpleButton btnF;
        private DevExpress.XtraEditors.SimpleButton btnE;
        private DevExpress.XtraEditors.SimpleButton btnD;
        private DevExpress.XtraEditors.SimpleButton btnC;
        private DevExpress.XtraEditors.SimpleButton btnB;
        private DevExpress.XtraEditors.SimpleButton btnA;
        private DevExpress.XtraEditors.SimpleButton btnMoveRigthText;
        private DevExpress.XtraEditors.SimpleButton btnM;
        private DevExpress.XtraEditors.SimpleButton btnPaymentText;
        private DevExpress.XtraEditors.SimpleButton btnTranslateText;
        private DevExpress.XtraEditors.SimpleButton btnR;
        private DevExpress.XtraEditors.SimpleButton btnQ;
        private DevExpress.XtraEditors.SimpleButton btnP;
        private DevExpress.XtraEditors.SimpleButton btnOkText;
        private DevExpress.XtraEditors.SimpleButton btnDeleteText;
        private DevExpress.XtraEditors.SimpleButton btnL;
        private DevExpress.XtraEditors.SimpleButton btnI;
        private DevExpress.XtraEditors.SimpleButton btnH;
        private DevExpress.XtraEditors.SimpleButton btnG;
        private DevExpress.XtraEditors.SimpleButton btnMoveLeftText;
        private DevExpress.XtraEditors.SimpleButton btnK;
        private DevExpress.XtraEditors.SimpleButton btnJ;
        private DevExpress.XtraEditors.SimpleButton btnO;
        private DevExpress.XtraEditors.SimpleButton btnN;
        private DevExpress.XtraEditors.SimpleButton btnEscText;
        private DevExpress.XtraEditors.PanelControl pnlKeyboardMoney;
        private DevExpress.XtraEditors.SimpleButton btn500000;
        private DevExpress.XtraEditors.SimpleButton btn200000;
        private DevExpress.XtraEditors.SimpleButton btn100000;
        private DevExpress.XtraEditors.SimpleButton btn50000;
        private DevExpress.XtraEditors.SimpleButton btn20000;
        private DevExpress.XtraEditors.SimpleButton btn10000;
        private DevExpress.XtraEditors.SimpleButton btnMoveRightMoney;
        private DevExpress.XtraEditors.SimpleButton btnPaymentMoney;
        private DevExpress.XtraEditors.SimpleButton btnTranslateMoney;
        private DevExpress.XtraEditors.SimpleButton btnOkMoney;
        private DevExpress.XtraEditors.SimpleButton btnDeleteMoney;
        private DevExpress.XtraEditors.SimpleButton btn2000;
        private DevExpress.XtraEditors.SimpleButton btn1000;
        private DevExpress.XtraEditors.SimpleButton btn500;
        private DevExpress.XtraEditors.SimpleButton btnMoveLeftMoney;
        private DevExpress.XtraEditors.SimpleButton btn5000;
        private DevExpress.XtraEditors.SimpleButton btnEcsMoney;
        private DevExpress.XtraEditors.SimpleButton btnS;
        private DevExpress.XtraEditors.SimpleButton btnW;
        private DevExpress.XtraEditors.SimpleButton btnV;
        private DevExpress.XtraEditors.SimpleButton btnU;
        private DevExpress.XtraEditors.SimpleButton btnT;
        private DevExpress.XtraEditors.SimpleButton btnZ;
        private DevExpress.XtraEditors.SimpleButton btnY;
        private DevExpress.XtraEditors.SimpleButton btnShift;
        private DevExpress.XtraEditors.SimpleButton btnX;
        private DevExpress.XtraEditors.PanelControl pnlKHTT;
        private DevExpress.XtraEditors.TextEdit txtDearCustomer;
        private DevExpress.XtraEditors.LabelControl lb_nhapma_KHTT;
        private DevExpress.XtraEditors.PanelControl pnlTTTNB;
        private DevExpress.XtraEditors.TextEdit txtInnerCustomer;
        private DevExpress.XtraEditors.LabelControl lb_nhapmaTTNB;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.SimpleButton btnDownForeignCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn Discount;
        private DevExpress.XtraEditors.SimpleButton btnServiceMoney;
        private DevExpress.XtraEditors.PanelControl pnlPhiPhuVu;
        private DevExpress.XtraEditors.TextEdit txtServiceMoney;
        private DevExpress.XtraEditors.LabelControl lb_nhaptienphucvu;
        private DevExpress.XtraEditors.PanelControl pnlInforMoney;
        private DevExpress.XtraEditors.PanelControl pnlInforCustomer;
        private DevExpress.XtraEditors.PanelControl pnlInnerCustomerShow;
        private DevExpress.XtraEditors.PanelControl pnlDearCustomerShow;
        private DevExpress.XtraEditors.LabelControl lbIDDearCustomerShow;
        private DevExpress.XtraEditors.LabelControl lbNameDearCustomer;
        private DevExpress.XtraEditors.LabelControl lbMark;
        private DevExpress.XtraEditors.LabelControl lbMoneySurplus;
        private DevExpress.XtraEditors.LabelControl lbIdInnerCustomerShow;
        private DevExpress.XtraEditors.LabelControl lbNameInnerCustomer;
        private DevExpress.XtraEditors.SimpleButton btnVisa;
        private DevExpress.XtraEditors.SimpleButton btnVoucher;
        private DevExpress.XtraEditors.PanelControl pnUpDownGridview;
        private DevExpress.XtraEditors.SimpleButton btnUpGridview;
        private DevExpress.XtraEditors.SimpleButton btnDownGridview;
        private DevExpress.XtraEditors.SimpleButton btnLastGridview;
        private DevExpress.XtraEditors.SimpleButton btnFirstGridview;
        private DevExpress.XtraEditors.LabelControl lbcurrencyGetMoney;
        private DevExpress.XtraEditors.LabelControl lbcurrencyService;
        private DevExpress.XtraEditors.PanelControl pnlVisa;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.TextEdit txtCountry;
        private DevExpress.XtraEditors.TextEdit txtNameVisa;
        private DevExpress.XtraEditors.LabelControl lb_tenchuthe;
        private DevExpress.XtraEditors.SimpleButton btnRefeshIdCode;
        private DevExpress.XtraEditors.TextEdit txtCwCvc;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txtIdCodeCreate;
        private DevExpress.XtraEditors.LabelControl lb_thongtinthem;
        private DevExpress.XtraEditors.TextEdit txtCity;
        private DevExpress.XtraEditors.TextEdit txtIdCodeInput;
        private DevExpress.XtraEditors.TextEdit txtAddress;
        private DevExpress.XtraEditors.LabelControl lb_maantoan;
        private DevExpress.XtraEditors.LabelControl lb_tinhtp;
        private DevExpress.XtraEditors.LabelControl lb_quocgia;
        private DevExpress.XtraEditors.LabelControl lb_diachichuthe;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.TextEdit txtYear;
        private DevExpress.XtraEditors.TextEdit txtMonth;
        private DevExpress.XtraEditors.TextEdit txtMoneyVisa;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl lb_sotien;
        private DevExpress.XtraEditors.TextEdit txtIdCardVisa;
        private DevExpress.XtraEditors.LabelControl lb_thongtinbatbuoc;
        private DevExpress.XtraEditors.LabelControl lb_ngayhethan;
        private DevExpress.XtraEditors.LabelControl lb_sothe;
        private DevExpress.XtraEditors.SimpleButton btnPaymentVisa;
        private DevExpress.XtraEditors.SimpleButton bntCancelVisa;
        private DevExpress.XtraEditors.PanelControl pnlHeaderVisa;
        private DevExpress.XtraEditors.LabelControl lb_thanhtoanbangthe;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.PanelControl pnlVoucher;
        private DevExpress.XtraEditors.TextEdit txtInputVoucher;
        private DevExpress.XtraEditors.LabelControl lb_nhapVoucher;
        private DevExpress.XtraGrid.Columns.GridColumn IS_COMBO;
        private DevExpress.XtraEditors.SimpleButton btnGiaohang;
        private DevExpress.XtraGrid.Columns.GridColumn IS_INBEP;
        private DevExpress.XtraEditors.PanelControl pnlCongNo;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.TextEdit txtCongNo_Tien;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.SimpleButton btnCongNoXacNhan;
        private DevExpress.XtraEditors.SimpleButton btnHuyCongNo;
        private DevExpress.XtraEditors.PanelControl panelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.PanelControl palCongNo_Noidung;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraEditors.SimpleButton btnCongNoSearch;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dtpNgayHenTra;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtCNMaKH;
        private DevExpress.XtraEditors.TextEdit txtCNSoDT;
        private DevExpress.XtraEditors.SimpleButton btnMakhuyenmai;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.LabelControl lb_Quay;
        private DevExpress.XtraEditors.LabelControl lb_Kho;
        private DevExpress.XtraEditors.LabelControl lbgiay;
        private DevExpress.XtraEditors.LabelControl lbCaBan;
        private DevExpress.XtraEditors.LabelControl lbGio;
        private DevExpress.XtraEditors.LabelControl lb_NhanVien;
        private DevExpress.XtraEditors.LabelControl lbtimeby;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraGrid.Columns.GridColumn CHIETKHAU;
        private DevExpress.XtraEditors.SimpleButton btn_DatThu;
        private DevExpress.XtraEditors.SimpleButton btn_ThuNgan;
        private DevExpress.XtraEditors.PanelControl pnDacThu;
        private DevExpress.XtraEditors.SimpleButton btnKhachhang;
        private DevExpress.XtraEditors.SimpleButton btnGhichu;
        private DevExpress.XtraEditors.SimpleButton btnNhanvienthuchien;
        private DevExpress.XtraGrid.Columns.GridColumn MONTHEM;
        private DevExpress.XtraGrid.Columns.GridColumn TEM;
        private DevExpress.XtraGrid.Columns.GridColumn MACP;
        private DevExpress.XtraGrid.Columns.GridColumn MACB;
        private DevExpress.XtraEditors.SimpleButton btnTachBill;
        private DevExpress.XtraEditors.SimpleButton btnCongNo;
        private DevExpress.XtraEditors.PanelControl pnlMagiamgia;
        private DevExpress.XtraEditors.TextEdit txtInputMagiamgia;
        private DevExpress.XtraEditors.LabelControl lb_nhapMagiamgia;
        private DevExpress.XtraEditors.PanelControl pnlPhieuMuaHang;
        private DevExpress.XtraEditors.TextEdit txtNhapPhieuMuaHang;
        private DevExpress.XtraEditors.LabelControl lbNhapPhieuMuaHang;
        private DevExpress.XtraEditors.SimpleButton btnPhieuMuaHang;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
    }
}