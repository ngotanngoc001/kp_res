﻿using System;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using KP_Report;
using KP_Terminal;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Resources;

namespace KP_RES
{
    public partial class Frm_InBillMuaTruoc : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt = new DataTable();
        string ControlName = "";
        public ArrayList danhsachvedat = new ArrayList();
        public string sMalichchieu = "";
        public Frm_InBillMuaTruoc()
        {
            InitializeComponent();
            txtMave .GotFocus += new EventHandler(txtMave_GotFocus);
            txtVe.GotFocus += new EventHandler(txtMave_GotFocus);
            txtSodienthoai.GotFocus += new EventHandler(txtMave_GotFocus);
        }

        private void Frm_InveMuaTruoc_Load(object sender, EventArgs e)
        {
            palBill.Width = palShow.Width / 2;

            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
           
            title.Text = rm.GetString("inhangmuaonline", culture);
            lblMadatve.Text = rm.GetString("ma", culture);
            labelControl2.Text = rm.GetString("mave", culture);
            labelControl1.Text = rm.GetString("dienthoai", culture);
            btnTim.Text = rm.GetString("timkiem", culture);
            btnInve.Text = rm.GetString("inve", culture);
            btnDong.Text = rm.GetString("dong", culture);
            btnBanphim.Text = rm.GetString("banphim", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
            //_thongbao = rm.GetString("thongbao", culture);
            //_chuachonmayinchomathang = rm.GetString("chuachonmayinchomathang", culture);
            //_nhap = rm.GetString("nhap", culture);
            //_mavenhapkhongdung = rm.GetString("mavenhapkhongdung", culture);
            //_mavekhongdung = rm.GetString("mavekhongdung", culture);
            //_xacnhan = rm.GetString("xacnhan", culture);
        }

        private void Frm_InveMuaTruoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void txtMave_GotFocus(object sender, System.EventArgs e)
        {
            TextEdit txt = (TextEdit)sender;
            ControlName = txt.Name;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            if (ControlName == txtMave.Name)
            {
                txtMave.Text = txtMave.Text + btn.Text;
            }
            else if (ControlName == txtSodienthoai.Name)
            {
                txtSodienthoai.Text = txtSodienthoai.Text + btn.Text;
            }
            else if (ControlName == txtVe.Name)
            {
                txtVe.Text = txtVe.Text + btn.Text;
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (ControlName == txtMave.Name)
            {
                txtMave.Text = "";
            }
            else if (ControlName == txtVe.Name)
            {
                txtVe.Text = "";
            }
            else
            {
                txtSodienthoai.Text = "";
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            if (txtMave.Text == "" && txtSodienthoai.Text == "" && txtVe.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblMadatve.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMave.Focus();
                return;
            }

            string sSQL = "";
            sSQL = "";
            sSQL += "Select A.MAVE,A.MABARCODE,A.BIENNHAN,Y.MAHANGHOA " + "\n";
            sSQL += "From VE A ,  BIENNHAN C , LICHCHIEU D ,DM_NHANVIEN H,SYS_USER I,BN_HANGHOA Y" + "\n";
            sSQL += "Where A.BIENNHAN = C.MABARCODE" + "\n";
            sSQL += "And A.BIENNHAN = Y.MABIENNHAN" + "\n";
            sSQL += "And A.MALICHCHIEU = D.MALICHCHIEU" + "\n";
            sSQL += "And A.NGUOITAO = I.UserID" + "\n";
            sSQL += "And I.MaNV = H.MANHANVIEN" + "\n";
            sSQL += "And D.NGAYCHIEU >= " + clsMain.SQLString(String.Format("{0:dd/MM/yyyy}", clsGlobal.gdServerDate)) + "\n";
            //sSQL += "And C.TRANGTHAI =" + clsMain.SQLString("0") + "\n";
            sSQL += "And C.DATHANHTOAN =" + clsMain.SQLString("1") + "\n";
            sSQL += "And LEN(C.CMND)>0" + "\n";
            sSQL += "And Y.LAY_HANGHOA=0" + "\n";
            if (txtMave.Text != "")
            {
                sSQL += "And A.BIENNHAN =" + clsMain.SQLString(txtMave.Text) + "\n";
            }
            else if (txtVe.Text != "")
            {
                sSQL += "And A.MABARCODE =" + clsMain.SQLString(txtVe.Text) + "\n";
            }
            else
            {
                sSQL += "And A.DIENTHOAI =" + clsMain.SQLString(txtSodienthoai.Text) + "\n";
            }
           // sSQL += "Order by A.SOGHE" + "\n";
            dt = clsMain.ReturnDataTable(sSQL);

            if (dt.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Mã vé không đúng . Hoặc đã hủy . Hoặc đã lấy hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMave.Focus();
            }
            else
            {
                txtMave.Text = dt.Rows[0]["BIENNHAN"].ToString();
                XemBillHoadon();
                btnInve.Focus();
            }
        }

        private void btnInve_Click(object sender, EventArgs e)
        {
            if (txtMave.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Nhập " + lblMadatve.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMave.Focus();
                return;
            }
            if (dt.Rows.Count <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Mã vé không đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMave.Focus();
                return;
            }
                      
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Xác nhận in vé cho biên nhận này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                InsertHoadon();
                dt.Clear();
                txtMave.Text = "";
                txtSodienthoai.Text = "";
                txtMave.Focus();
                this.Close();
            }
        }

        private string GetIdKitchen(string idKitchen)
        {
            string printKitchen = null;
            string sql = "Select MAYINBEP From DM_BEP A,HANGHOA B Where A.MA_BEP=B.MA_BEP AND B.IS_INBEP=1 AND B.MA_BEP =" + clsMain.SQLString(idKitchen);
            DataTable kitchen = clsMain.ReturnDataTable(sql);
            if (kitchen.Rows.Count > 0)
            {
                printKitchen = kitchen.Rows[0]["MAYINBEP"].ToString();
                return printKitchen;
            }
            else
                return printKitchen;
        }

        private string GetNewCodeBill()
        {
            string _mahoadon = "";
            string ngay = cls_ConfigCashier.idCashier + DateTime.Now.Day.ToString("00") + DateTime.Now.Month.ToString("00") + DateTime.Now.Year.ToString("0000");
            DataTable dtMA = clsMain.ReturnDataTable("select [dbo].[fc_NewcodeHoaDon] ('" + ngay + "')");
            _mahoadon = dtMA.Rows[0][0].ToString();
            return _mahoadon;
        }

        private void XemBillHoadon()
        {
            bool flag_IsCombo = false;
            int dem = 0;
            if (dt.Rows.Count > 0)
            {
                List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                //Thêm vao bảng hóa đơn
                string sSQL = "";
                sSQL += "SELECT MABIENNHAN,MAHANGHOA,SOLUONG,NGUOITAO,ISNULL(CHIETKHAU,0) AS CHIETKHAU,DONGIA AS DONGIA1," + "\n";
                sSQL += "CASE ISNULL(CHIETKHAU,0) WHEN 0 THEN DONGIA WHEN 100 THEN 0 ELSE (DONGIA/(1 - CHIETKHAU/100)) END AS DONGIA" + "\n";
                sSQL += "FROM BN_HANGHOA" + "\n";
                sSQL += "WHERE MABIENNHAN=" + clsMain.SQLString(dt.Rows[0]["BIENNHAN"].ToString());
                DataTable dtBN = clsMain.ReturnDataTable(sSQL);
                if (dtBN.Rows.Count > 0)
                {
                    sSQL = "";
                    sSQL += "Select A.MANHANVIEN,A.TENNHANVIEN" + "\n";
                    sSQL += "From DM_NHANVIEN A,SYS_USER B" + "\n";
                    sSQL += "WHERE A.MANHANVIEN=B.MaNV And UserID=" + clsMain.SQLString(dtBN.Rows[0]["NGUOITAO"].ToString());
                    DataTable dt2 = clsMain.ReturnDataTable(sSQL);
                    string TenNV = dt2.Rows[0]["TENNHANVIEN"].ToString();
                    float tongtien = 0;
                    int n = dtBN.Rows.Count;
                    for (int i = 0; i < n; i++)
                    {
                        tongtien += float.Parse(dtBN.Rows[i]["SOLUONG"].ToString()) * float.Parse(dtBN.Rows[i]["DONGIA1"].ToString());
                    }

                    string idBill = GetNewCodeBill();

                    DataTable dtCombo = new DataTable();
                    sSQL = "";
                    sSQL += "Select A.MABIENNHAN,A.MAHANGHOA as MA_HANGHOA,B.TEN_HANGHOA,A.SOLUONG,A.DONGIA as GIABAN1,A.NGUOITAO,B.IS_COMBO,B.IS_INBEP,B.MA_BEP,'' AS COMBOCON,ISNULL(A.CHIETKHAU,0) AS CHIETKHAU," + "\n";
                    sSQL += "CASE ISNULL(A.CHIETKHAU,0) WHEN 0 THEN DONGIA WHEN 100 THEN 0 ELSE (DONGIA/(1 - A.CHIETKHAU/100)) END AS GIABAN" + "\n";
                    sSQL += "From BN_HANGHOA A,HANGHOA B" + "\n";
                    sSQL += "where A.MAHANGHOA=B.MA_HANGHOA AND MABIENNHAN=" + clsMain.SQLString(dt.Rows[0]["BIENNHAN"].ToString());
                    DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                    DataTable dtHH = clsMain.ReturnDataTable(sSQL);
                    dtHH.Rows.Clear();
                    n = dt1.Rows.Count;
                    foreach (DataRow dr in dt1.Rows)
                    {
                        if (!bool.Parse(dr["IS_COMBO"].ToString()))
                        {
                            dtHH.Rows.Add(dr["MABIENNHAN"].ToString(), dr["MA_HANGHOA"].ToString(), dr["TEN_HANGHOA"].ToString(),
                                dr["SOLUONG"].ToString(), dr["GIABAN1"].ToString(), dr["NGUOITAO"].ToString(), dr["IS_COMBO"].ToString(),
                                dr["IS_INBEP"].ToString(), dr["MA_BEP"].ToString(), dr["COMBOCON"].ToString(), dr["CHIETKHAU"].ToString(),
                               dr["GIABAN"].ToString());
                        }
                        else
                        {
                            dtHH.Rows.Add(dr["MABIENNHAN"].ToString(), dr["MA_HANGHOA"].ToString(), dr["TEN_HANGHOA"].ToString(),
                               dr["SOLUONG"].ToString(), dr["GIABAN1"].ToString(), dr["NGUOITAO"].ToString(), dr["IS_COMBO"].ToString(),
                               dr["IS_INBEP"].ToString(), dr["MA_BEP"].ToString(), dr["COMBOCON"].ToString(), dr["CHIETKHAU"].ToString(),
                               dr["GIABAN"].ToString());

                            sSQL = "";
                            sSQL += "Select A.MA_HANGHOA,B.TEN_HANGHOA,A.SOLUONG,A.GIA,B.IS_INBEP,B.MA_BEP" + "\n";
                            sSQL += "From COMBO A,HANGHOA B" + "\n";
                            sSQL += "where A.MA_HANGHOA=B.MA_HANGHOA AND MA_COMBO=" + clsMain.SQLString(dr["MA_HANGHOA"].ToString());
                            dt2 = clsMain.ReturnDataTable(sSQL);
                            for (int i = 0; i < dt2.Rows.Count; i++)
                            {
                                dtHH.Rows.Add(dr["MABIENNHAN"].ToString(), dt2.Rows[i]["MA_HANGHOA"].ToString(), dt2.Rows[i]["TEN_HANGHOA"].ToString(), dt2.Rows[i]["SOLUONG"].ToString(), 0, dr["NGUOITAO"].ToString(), 0, bool.Parse(dt2.Rows[i]["IS_INBEP"].ToString()), dt2.Rows[i]["MA_BEP"].ToString(), "1", dr["CHIETKHAU"].ToString(), 0);
                            }
                        }
                    }


                    decimal sl_combo = 0;
                    for (int i = 0; i < dtHH.Rows.Count; i++)
                    {
                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        dmHangHoa.STT = (i + 1).ToString();
                        dmHangHoa.Ten_HoaDon = "PHIẾU THANH TOÁN";
                        dmHangHoa.MA_HANGHOA = dtHH.Rows[i]["MA_HANGHOA"].ToString();
                        dmHangHoa.TEN_HANGHOA = dtHH.Rows[i]["TEN_HANGHOA"].ToString();
                        dmHangHoa.SOLUONG = decimal.Parse(dtHH.Rows[i]["SOLUONG"].ToString());
                        dmHangHoa.GIABAN = decimal.Parse(dtHH.Rows[i]["GIABAN"].ToString());
                        dmHangHoa.Thanh_Tien = decimal.Parse(dtHH.Rows[i]["SOLUONG"].ToString()) * decimal.Parse(dtHH.Rows[i]["GIABAN"].ToString());
                        dmHangHoa.THUE = 0;
                        dmHangHoa.IS_INBEP = bool.Parse(dtHH.Rows[i]["IS_INBEP"].ToString());
                        dmHangHoa.Giam_Gia = dtHH.Rows[i]["CHIETKHAU"].ToString() + "%";
                        dmHangHoa.Phiphucvu = "0";
                        dmHangHoa.Tiendatcoc = "0";
                        dmHangHoa.Tong_Cong = string.Format("{0:#,###0}", tongtien);
                        dmHangHoa.Tien_KhachTra = string.Format("{0:#,###0}", tongtien);
                        dmHangHoa.Tien_TraKhach = "0";
                        dmHangHoa.Ma_HoaDon = idBill;
                        dmHangHoa.Ma_Quay = cls_ConfigCashier.nameCashier;
                        dmHangHoa.TenNV = TenNV;
                        dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                        dmHangHoa.ngayHD = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                        if (dtHH.Rows[i]["COMBOCON"].ToString() == "1")
                            dmHangHoa.IS_COMBO = true;
                        listDM_HangHoa.Add(dmHangHoa);

                    }

                    convert cvrt = new convert();

                    // printer bill
                    DataTable bill = cvrt.ConvertToDataTable(listDM_HangHoa);
                    Frm_Report1 frm = new Frm_Report1();
                    frm.ReportName = "rpt_inhoadon_Bill";
                    frm.DataSource = bill;
                    frm.IsPrint = false;
                    frm.PrinterName = clsKP_Terminal.Mayinbill;
                    frm.ExportName = "In Bill";
                    frm.WindowState = FormWindowState.Maximized;
                    frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    frm.TopLevel = false;
                    frm.Parent = palBill;
                    frm.Show();
                }
            }
        }

        private void InsertHoadon()
        {
            bool flag_IsCombo = false;
            int dem = 0;
            if (dt.Rows.Count > 0)
            {
                List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                //Thêm vao bảng hóa đơn
                string sSQL = "";
                sSQL += "Select MABIENNHAN,MAHANGHOA,SOLUONG,NGUOITAO,ISNULL(CHIETKHAU,0) AS CHIETKHAU,DONGIA AS DONGIA1," + "\n";
                sSQL += "CASE ISNULL(CHIETKHAU,0) WHEN 0 THEN DONGIA WHEN 100 THEN 0 ELSE (DONGIA/(1 - CHIETKHAU/100)) END AS DONGIA,TAIKHOAN,(select top 1 MA  from KHTT where KHTT.TAIKHOAN=BN_HANGHOA.TAIKHOAN) as ID_KHTT" + "\n";
                sSQL += "From BN_HANGHOA" + "\n";
                sSQL += "Where MABIENNHAN=" + clsMain.SQLString(dt.Rows[0]["BIENNHAN"].ToString());
                DataTable dtBN = clsMain.ReturnDataTable(sSQL);
                if (dtBN.Rows.Count > 0)
                {
                    sSQL = "";
                    sSQL += "Select A.MANHANVIEN,A.TENNHANVIEN" + "\n";
                    sSQL += "From DM_NHANVIEN A,SYS_USER B" + "\n";
                    sSQL += "WHERE A.MANHANVIEN=B.MaNV And UserID=" + clsMain.SQLString(dtBN.Rows[0]["NGUOITAO"].ToString());
                    DataTable dt2 = clsMain.ReturnDataTable(sSQL);
                    string TenNV = dt2.Rows[0]["TENNHANVIEN"].ToString();

                    float tongtien = 0;
                    int n = dtBN.Rows.Count;
                    for (int i = 0; i < n; i++)
                    {
                        tongtien += float.Parse(dtBN.Rows[i]["SOLUONG"].ToString()) * float.Parse(dtBN.Rows[i]["DONGIA1"].ToString());
                    }
                    //03-04/2017 Thêm cộng điểm KHTT khi mua online
                    if (dtBN.Rows[0]["ID_KHTT"].ToString() != "")
                    {
                        cls_KHTT.CongDiemKhachHangThanThiet_Nhanvien(dtBN.Rows[0]["ID_KHTT"].ToString(), decimal.Parse(tongtien.ToString()), dt2.Rows[0]["MANHANVIEN"].ToString());
                    }
                    //
                    string idBill = GetNewCodeBill();
                    sSQL = "";
                    sSQL += "Insert Into HOADON( MA_HOADON, MANHANVIEN, GIAMGIA, PHIPHUCVU, TIEN_TRAKHACH, TIEN_KHACHTRA,";
                    sSQL += "TONGTIEN, NGAYTAO, GIOVAO, GIORA,NHANVIEN_RABILL,ISPAYMENT, MA_CUAHANG, MA_KHO, MA_QUAY,MA_KHACHHANG,";
                    sSQL += "SOTHETT,NGAYTHETT,TIENTRATHETT,MONEYVISA,MONEYINNERCARD,MONEYVOUCHER,MONEYCASH)" + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(idBill) + ",";
                    sSQL += clsMain.SQLString(dt2.Rows[0]["MANHANVIEN"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dtBN.Rows[0]["CHIETKHAU"].ToString()) + ",";
                    sSQL += clsMain.SQLString("0") + ",";
                    sSQL += clsMain.SQLString("0") + ",";
                    sSQL += clsMain.SQLString(tongtien.ToString()) + ",";
                    sSQL += clsMain.SQLString(tongtien.ToString()) + ",";
                    sSQL += "GETDATE()" + ",";
                    sSQL += "GETDATE()" + ",";
                    sSQL += "GETDATE()" + ",";
                    sSQL += clsMain.SQLString(dt2.Rows[0]["MANHANVIEN"].ToString()) + "," + 1 + ",";
                    sSQL += clsMain.SQLString(cls_ConfigCashier.idShop) + ",";
                    sSQL += clsMain.SQLString(cls_ConfigCashier.idWarehouse) + ",";
                    sSQL += clsMain.SQLString(cls_ConfigCashier.idCashier) + ",";
                    //25/03/2017 THEM MA KHACH HANG MUA ONLINE
                    sSQL += clsMain.SQLString(dtBN.Rows[0]["ID_KHTT"].ToString()) + ",";
                    //
                    sSQL += clsMain.SQLString("0") + ",";
                    sSQL += clsMain.SQLString("0") + ",";
                    sSQL += clsMain.SQLString(0.ToString()) + ",";
                    sSQL += clsMain.SQLString(0.ToString()) + ",";
                    sSQL += clsMain.SQLString(0.ToString()) + ",";
                    sSQL += clsMain.SQLString(0.ToString()) + ",";
                    sSQL += clsMain.SQLString(tongtien.ToString()) + ")";
                    clsMain.ExecuteSQL(sSQL);
                    //Màn hình bếp -tạo bàn fastfood
                    string maban_fas = "";
                    if (clsUserManagement.CheckActive("3"))
                    {
                        sSQL = "Select MA_BAN From BAN Where TEN_BAN='FastFood'";
                        DataTable dtb = clsMain.ReturnDataTable(sSQL);
                        if (dtb.Rows.Count > 0)
                        {
                            maban_fas = dtb.Rows[0]["MA_BAN"].ToString();
                        }
                        else
                        {
                            sSQL = "Select MA_KHUVUC From KHUVUC Where MA_CUAHANG=" + clsMain.SQLString(cls_ConfigCashier.idShop);
                            dtb = clsMain.ReturnDataTable(sSQL);
                            string sql = "";
                            sql += "INSERT INTO BAN(TEN_BAN,MA_KHUVUC,SOGHE,Width,Height,LocaY,LocaX,SUDUNG,TRANG,ISMO_BAN)" + "\n";
                            sql += "Values(";
                            sql += "'FastFood'" + ",";
                            sql += clsMain.SQLString(dtb.Rows[0]["MA_KHUVUC"].ToString()) + ",";
                            sql += clsMain.SQLString(4.ToString()) + ",";
                            sql += clsMain.SQLString(110.ToString()) + ",";
                            sql += clsMain.SQLString(80.ToString()) + ",";
                            sql += clsMain.SQLString(3.ToString()) + ",";
                            sql += clsMain.SQLString(3.ToString()) + ",";
                            sql += clsMain.SQLBit(true) + ",";
                            sql += clsMain.SQLString(5.ToString()) + ",";
                            sql += clsMain.SQLBit(true) + ")";
                            clsMain.ExecuteSQL(sql);

                            sSQL = "Select MA_BAN From BAN Where TEN_BAN='FastFood'";
                            dtb = clsMain.ReturnDataTable(sSQL);
                            if (dtb.Rows.Count > 0)
                            {
                                maban_fas = dtb.Rows[0]["MA_BAN"].ToString();
                            }
                        }
                    }

                    //Kiểm tra combo -add thanh phần
                    sSQL = "";
                    sSQL += "Select A.MABIENNHAN,A.MAHANGHOA AS MA_HANGHOA,B.TEN_HANGHOA,A.SOLUONG,A.DONGIA AS GIABAN1,A.NGUOITAO,B.IS_COMBO,B.IS_INBEP,B.MA_BEP,'' AS COMBOCON,ISNULL(A.CHIETKHAU,0) AS CHIETKHAU," + "\n";
                    sSQL += "CASE ISNULL(A.CHIETKHAU,0) WHEN 0 THEN DONGIA WHEN 100 THEN 0 ELSE (DONGIA/(1 - A.CHIETKHAU/100)) END AS GIABAN" + "\n";
                    sSQL += "From BN_HANGHOA A,HANGHOA B" + "\n";
                    sSQL += "where A.MAHANGHOA=B.MA_HANGHOA AND MABIENNHAN=" + clsMain.SQLString(dt.Rows[0]["BIENNHAN"].ToString());
                    DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                    DataTable dtHH = clsMain.ReturnDataTable(sSQL);
                    dtHH.Rows.Clear();
                    n = dt1.Rows.Count;
                    foreach (DataRow dr in dt1.Rows)
                    {
                        if (!bool.Parse(dr["IS_COMBO"].ToString()))
                        {
                            dtHH.Rows.Add(dr["MABIENNHAN"].ToString(), dr["MA_HANGHOA"].ToString(), dr["TEN_HANGHOA"].ToString(),
                               dr["SOLUONG"].ToString(), dr["GIABAN1"].ToString(), dr["NGUOITAO"].ToString(), dr["IS_COMBO"].ToString(),
                               dr["IS_INBEP"].ToString(), dr["MA_BEP"].ToString(), dr["COMBOCON"].ToString(), dr["CHIETKHAU"].ToString(),
                               dr["GIABAN"].ToString());
                        }
                        else
                        {
                            dtHH.Rows.Add(dr["MABIENNHAN"].ToString(), dr["MA_HANGHOA"].ToString(), dr["TEN_HANGHOA"].ToString(),
                               dr["SOLUONG"].ToString(), dr["GIABAN1"].ToString(), dr["NGUOITAO"].ToString(), dr["IS_COMBO"].ToString(),
                               dr["IS_INBEP"].ToString(), dr["MA_BEP"].ToString(), dr["COMBOCON"].ToString(), dr["CHIETKHAU"].ToString(),
                               dr["GIABAN"].ToString());

                            sSQL = "";
                            sSQL += "Select A.MA_HANGHOA,B.TEN_HANGHOA,A.SOLUONG,A.GIA,B.IS_INBEP,B.MA_BEP" + "\n";
                            sSQL += "From COMBO A,HANGHOA B" + "\n";
                            sSQL += "where A.MA_HANGHOA=B.MA_HANGHOA AND MA_COMBO=" + clsMain.SQLString(dr["MA_HANGHOA"].ToString());
                            dt2 = clsMain.ReturnDataTable(sSQL);
                            for (int i = 0; i < dt2.Rows.Count; i++)
                            {
                                dtHH.Rows.Add(dr["MABIENNHAN"].ToString(), dt2.Rows[i]["MA_HANGHOA"].ToString(), dt2.Rows[i]["TEN_HANGHOA"].ToString(), dt2.Rows[i]["SOLUONG"].ToString(), 0, dr["NGUOITAO"].ToString(), 0, bool.Parse(dt2.Rows[i]["IS_INBEP"].ToString()), dt2.Rows[i]["MA_BEP"].ToString(), "1", dr["CHIETKHAU"].ToString(), 0);
                            }
                        }
                    }
                    //ct_hoadon
                    DataTable dtCombo = new DataTable();
                    decimal sl_combo = 0;
                    for (int i = 0; i < dtHH.Rows.Count; i++)
                    {
                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        dmHangHoa.STT = (i + 1).ToString();
                        dmHangHoa.Ten_HoaDon = "PHIẾU THANH TOÁN";
                        dmHangHoa.MA_HANGHOA = dtHH.Rows[i]["MA_HANGHOA"].ToString();
                        dmHangHoa.TEN_HANGHOA = dtHH.Rows[i]["TEN_HANGHOA"].ToString();
                        dmHangHoa.SOLUONG = decimal.Parse(dtHH.Rows[i]["SOLUONG"].ToString());
                        dmHangHoa.GIABAN = decimal.Parse(dtHH.Rows[i]["GIABAN"].ToString());
                        dmHangHoa.THUE = 0;
                        dmHangHoa.Thanh_Tien = decimal.Parse(dtHH.Rows[i]["SOLUONG"].ToString()) * decimal.Parse(dtHH.Rows[i]["GIABAN"].ToString());
                        dmHangHoa.IS_INBEP = bool.Parse(dtHH.Rows[i]["IS_INBEP"].ToString());
                        dmHangHoa.Tong_Cong = string.Format("{0:#,###0}", tongtien);
                        dmHangHoa.Tien_TraKhach = "0";
                        dmHangHoa.Tien_KhachTra = string.Format("{0:#,###0}", tongtien);
                        dmHangHoa.Giam_Gia = dtHH.Rows[i]["CHIETKHAU"].ToString() + "%";
                        double GiaBan_Thuc = (double)dtHH.Rows[i]["GIABAN1"];
                        dmHangHoa.Phiphucvu = "0";
                        dmHangHoa.Tiendatcoc = "0";

                        dmHangHoa.Ma_HoaDon = idBill;
                        dmHangHoa.Ma_Quay = cls_ConfigCashier.nameCashier;
                        dmHangHoa.MaQuay = cls_ConfigCashier.nameCashier;
                        dmHangHoa.TenNV = TenNV;
                        dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                        dmHangHoa.ngayHD = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                        if (dtHH.Rows[i]["COMBOCON"].ToString() == "1")
                            dmHangHoa.IS_COMBO = true;
                        listDM_HangHoa.Add(dmHangHoa);

                        sSQL = "";
                        sSQL += "Insert Into CT_HOADON( MA_HOADON, MA_HANGHOA, SOLUONG, GIABAN, THUE,IS_COMBO,GIABAN_THAT)" + "\n";
                        sSQL += "Values(";
                        sSQL += clsMain.SQLString(idBill) + ",";
                        sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                        sSQL += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                        sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                        sSQL += clsMain.SQLString(dmHangHoa.THUE.ToString()) + ",";
                        sSQL += clsMain.SQLBit(dmHangHoa.IS_COMBO) + ",";
                        sSQL += clsMain.SQLString(GiaBan_Thuc.ToString ()) + ")";
                        clsMain.ExecuteSQL(sSQL);

                        //UPDATE Table BN_HANGHOA
                        sSQL = "";
                        sSQL += "Update BN_HANGHOA Set LAY_HANGHOA=1" + "\n";
                        sSQL += "Where MABIENNHAN=" + clsMain.SQLString(dtHH.Rows[i]["MABIENNHAN"].ToString()) + "\n";
                        sSQL += "And MAHANGHOA=" + clsMain.SQLString(dmHangHoa.MA_HANGHOA) + "\n";
                        clsMain.ExecuteSQL(sSQL);

                        if (maban_fas != "")
                        {
                            sSQL = "";
                            sSQL += "Insert into DISPLAY_ORDER (MA_HANGHOA,MA_HOADON, MA_BAN, GIOORDER, SOLUONG, YEUCAU,FLAG)";
                            sSQL += "Values (";
                            sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                            sSQL += clsMain.SQLString(idBill) + ",";
                            sSQL += clsMain.SQLString(maban_fas) + ",";
                            sSQL += "GETDATE()" + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                            sSQL += clsMain.SQLStringUnicode("") + ",";
                            sSQL += clsMain.SQLBit(true) + ")";
                            clsMain.ExecuteSQL(sSQL);
                        }

                        if (!flag_IsCombo)
                        {
                            sSQL = "Select MA_HANGHOA From HANGHOA Where IS_COMBO=1 AND MA_HANGHOA=" + clsMain.SQLString(dmHangHoa.MA_HANGHOA);
                            dtCombo = clsMain.ReturnDataTable(sSQL);
                            if (dtCombo.Rows.Count > 0)
                            {
                                flag_IsCombo = true;
                                sl_combo = dmHangHoa.SOLUONG;
                                sSQL = "";
                                sSQL += "Select Count(*) From COMBO Where MA_COMBO=" + clsMain.SQLString(dmHangHoa.MA_HANGHOA);
                                DataTable dtC = clsMain.ReturnDataTable(sSQL);
                                if (dtC.Rows.Count > 0)
                                {
                                    dem = int.Parse(dtC.Rows[0][0].ToString()) + 1;
                                }
                            }
                        }

                        if (flag_IsCombo)
                        {
                            if (dmHangHoa.IS_COMBO)
                            {
                                decimal sl_HangCombo = sl_combo * dmHangHoa.SOLUONG;

                                sSQL = "";
                                sSQL += "Insert Into CT_HOADON_COMBO( MA_HOADON,MA_COMBO,MA_HANGHOA, SOLUONG)" + "\n";
                                sSQL += "Values(";
                                sSQL += clsMain.SQLString(idBill) + ",";
                                sSQL += clsMain.SQLString(dtCombo.Rows[0]["MA_HANGHOA"].ToString()) + ",";
                                sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                                sSQL += clsMain.SQLString(sl_HangCombo.ToString()) + ")";
                                clsMain.ExecuteSQL(sSQL);
                            }
                        }
                        dem = dem - 1;
                        if (dem == 0)
                            flag_IsCombo = false;
                    }
                    //in bill
                    while (dtHH.Rows.Count > 0)
                    {
                        List<DM_HangHoa> listDM_HangHoa_bep = new List<DM_HangHoa>();
                        int j = 0;
                        string maBEP = "";
                        for (int i = 0; i < dtHH.Rows.Count; i++)
                        {
                            DM_HangHoa dmHangHoa = new DM_HangHoa();
                            maBEP = dtHH.Rows[i]["MA_HANGHOA"].ToString();
                            dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", DateTime .Now);

                            dmHangHoa.Ma_HoaDon = idBill;
                            dmHangHoa.Ten_HoaDon = "Order Bếp";
                            dmHangHoa.MaQuay = cls_ConfigCashier.nameCashier;
                            dmHangHoa.TenNV = TenNV;
                            dmHangHoa.STT = (++j).ToString();
                            dmHangHoa.MA_HANGHOA = dtHH.Rows[i]["MA_HANGHOA"].ToString();
                            dmHangHoa.TEN_HANGHOA = dtHH.Rows[i]["TEN_HANGHOA"].ToString();
                            dmHangHoa.SOLUONG = decimal.Parse(dtHH.Rows[i]["SOLUONG"].ToString());
                            dmHangHoa.THUE = 0;
                            dmHangHoa.GIABAN = decimal.Parse(dtHH.Rows[i]["GIABAN"].ToString());
                            if (dtHH.Rows[i]["COMBOCON"].ToString() == "1")
                                dmHangHoa.IS_COMBO = true;
                            dmHangHoa.IS_INBEP = bool.Parse(dtHH.Rows[i]["IS_INBEP"].ToString());
                            dmHangHoa.GHICHU = "";

                            if (dmHangHoa.IS_INBEP)//nếu hàng hóa được in bếp mới cho in xuống bếp
                                listDM_HangHoa_bep.Add(dmHangHoa);
                            if (i != 0)
                            {
                                dtHH.Rows.RemoveAt(i);
                                i--;
                            }

                        }

                        dtHH.Rows.RemoveAt(0);// Delete rows(0)
                        // Printer Order 
                        convert cvrt1 = new convert();
                        DataTable Orderbill = cvrt1.ConvertToDataTable(listDM_HangHoa_bep);
                        if (Orderbill.Rows.Count > 0)
                        {
                            string print = "";
                            print = GetIdKitchen(maBEP);
                            if (print == null)
                                XtraMessageBox.Show("Chưa chọn máy in cho mặt hàng", "Thông báo", MessageBoxButtons.OK);
                            KP_Report.Frm_Report1 frm = new KP_Report.Frm_Report1();
                            if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                                frm.ReportName = "rpt_Order_FF_58";
                            else
                                frm.ReportName = "rpt_Order_FF";
                            frm.DataSource = Orderbill;
                            frm.WindowState = FormWindowState.Minimized;
                            frm.PrinterName = print;
                            frm.IsPrint = true;
                            frm.ShowDialog();
                            frm.Dispose();
                        }
                    }
                    // printer bill
                    convert cvrt = new convert();
                    DataTable bill = cvrt.ConvertToDataTable(listDM_HangHoa);
                    Frm_Report1 frm1 = new KP_Report.Frm_Report1();
                    if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                        frm1.ReportName = "rpt_inhoadon_Bill_58";
                    else
                        frm1.ReportName = "rpt_inhoadon_Bill";

                    frm1.DataSource = bill;
                    frm1.WindowState = FormWindowState.Minimized;
                    frm1.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                    frm1.IsPrint = true;
                    frm1.ShowDialog();
                    frm1.Dispose();


                    //Công nợ hóa đơn khách  hàng
                    //CN_MaKH = clsMain.ReturnDataTable("Select MA From NHACUNGCAP where ID = 'NK00000000'").Rows[0][0].ToString();
                    //cls_KHTT.CongNoKhachHangHoaDon(CN_MaKH, idBill, decimal.Parse(lbTotalMoneyAll.Text), decimal.Parse(lbTotalMoneyAll.Text) - CN_SoTienTra, CN_ngayHenTra);

                    sSQL = string.Format("Exec SP_TRUKHO_DINHLUONG @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 1);
                    clsMain.ExecuteSQL(sSQL);

                    sSQL = string.Format("Exec SP_TRUKHO_HOADON @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 1);
                    clsMain.ExecuteSQL(sSQL);

                    //cho biet Ma_hanghoa va DONGIA cua Combo cho cac hang hoa thuoc ve Combo
                    sSQL = string.Format("Exec SP_CT_HOADON_UPDATE @MA_HOADON='{0}'", idBill);
                    clsMain.ExecuteSQL(sSQL);

                    return;
                }
            }
        }

        
    }
}