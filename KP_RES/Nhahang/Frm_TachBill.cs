﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES
{
    public partial class Frm_TachBill : DevExpress.XtraEditors.XtraForm
    {
        public DataTable dt_ban_old;
        public DataTable dt_ban_new;
        public DataTable dt_ban_thanhtoan;
        DataTable divide_Table1 = new DataTable();
        DataTable divide_Table2 = new DataTable();
        bool flag_sole = false; // nếu như hủy món có số lượng là số lẻ
        decimal SoLuong_HuyMon = 0;
        public Frm_TachBill()
        {
            InitializeComponent();
            AddColumnDatatable();
        }

        private void Frm_TachBill_Load(object sender, EventArgs e)
        {
            gr_divide_Table_1.DataSource = dt_ban_old;
            if (dt_ban_old.Rows.Count > 0)
            {
                List<DM_ListDisheTableOne> lisHangHoa = new List<DM_ListDisheTableOne>();
                for (int i = 0; i < dt_ban_old.Rows.Count; i++)
                {
                    DM_ListDisheTableOne dmHangHoa = new DM_ListDisheTableOne();
                    dmHangHoa._MA_HANGHOA = dt_ban_old.Rows[i]["MA_HANGHOA"].ToString();
                    dmHangHoa._TEN_HANGHOA = dt_ban_old.Rows[i]["TEN_HANGHOA"].ToString();
                    dmHangHoa._SOLUONG = decimal.Parse(dt_ban_old.Rows[i]["SOLUONG"].ToString());
                    dmHangHoa._GIABAN = decimal.Parse(dt_ban_old.Rows[i]["GIABAN"].ToString());
                    dmHangHoa._Thanh_Tien = decimal.Parse(dt_ban_old.Rows[i]["Thanh_Tien"].ToString());
                    dmHangHoa._GHICHU = dt_ban_old.Rows[i]["GHICHU"].ToString();
                    dmHangHoa._IS_COMBO = bool.Parse(dt_ban_old.Rows[i]["IS_COMBO"].ToString());
                    lisHangHoa.Add(dmHangHoa);
                }
                convert cvrt = new convert();
                divide_Table1 = cvrt.ConvertToDataTable(lisHangHoa);
                gr_divide_Table_1.DataSource = divide_Table1;
                gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.RowCount - 1;
            }
        }

        private void btn_Payments_Click(object sender, EventArgs e)
        {
            SendKeys.Send("{Enter}");
        }

        private void btn_Thoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gv_divide_Table_1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == _SSTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
        }

        private void gv_divide_Table_2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == __STT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
        }

        private void btn_DowDivide_Click(object sender, EventArgs e)
        {
            try
            {
                gv_divide_Table_1.Focus();
                gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.FocusedRowHandle + 1;
                btn_UpDivide.Enabled = true;
                if (gv_divide_Table_1.FocusedRowHandle == gv_divide_Table_1.RowCount - 1)
                    btn_DowDivide.Enabled = false;
            }
            catch
            {
            }
        }

        private void btn_UpDivide_Click(object sender, EventArgs e)
        {
            try
            {
                gv_divide_Table_1.Focus();
                gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.FocusedRowHandle - 1;
                btn_DowDivide.Enabled = true;
                if (gv_divide_Table_1.FocusedRowHandle == 0)
                    btn_UpDivide.Enabled = false;
            }
            catch
            {
            }
        }

        private void btn_ToRightAll_Click(object sender, EventArgs e)
        {
            try
            {
                while (gv_divide_Table_1.RowCount > 0)
                    ThrowTable1ToTable2();
            }
            catch
            {
            }
        }

        private void btn_ToLeftAll_Click(object sender, EventArgs e)
        {
            try
            {
                while (gv_divide_Table_2.RowCount > 0)
                    Throw_Table2ToTable1();
            }
            catch
            {
            }
        }

        private void btn_ToRight_Click(object sender, EventArgs e)
        {
            try
            {
                ThrowTable1ToTable2();
            }
            catch
            {
            }
        }

        private void btn_Toleft_Click(object sender, EventArgs e)
        {
            try
            {
                Throw_Table2ToTable1();
            }
            catch
            {
            }
        }

        private void btn_ESCThroughtTable_Click(object sender, EventArgs e)
        {
            try
            {
                ExitDivideTable();
            }
            catch
            {
            }
        }

        private void ExitDivideTable()
        {
            this.Close();
            //dt_ban_thanhtoan = dt_ban_old;
        }
        public void ThrowTable1ToTable2()
        {
            decimal sl = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
            if (SoLuong_HuyMon > sl)
            {
                XtraMessageBox.Show("Số lượng nhập lớn hơn số lượng trên order", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (gv_divide_Table_1.RowCount <= 0)
                return;
            bool checkProductOnGv = false;
            if (divide_Table2.Rows.Count > 0)
            {
                for (int i = 0; i < divide_Table2.Rows.Count; i++)
                {
                    string maHH = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString();
                    if (divide_Table2.Rows[i]["__MA_HANGHOA"].ToString() == maHH)
                    {
                        if (decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString()) <= 1)
                        {
                            if (flag_sole)
                                divide_Table2.Rows[i]["__SOLUONG"] = decimal.Parse(divide_Table2.Rows[i]["__SOLUONG"].ToString()) + SoLuong_HuyMon;
                            else
                                divide_Table2.Rows[i]["__SOLUONG"] = decimal.Parse(divide_Table2.Rows[i]["__SOLUONG"].ToString()) + decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                        }
                        else
                        {
                            if (flag_sole)
                                divide_Table2.Rows[i]["__SOLUONG"] = decimal.Parse(divide_Table2.Rows[i]["__SOLUONG"].ToString()) + SoLuong_HuyMon;
                            else
                                divide_Table2.Rows[i]["__SOLUONG"] = decimal.Parse(divide_Table2.Rows[i]["__SOLUONG"].ToString()) + 1;
                        }
                        divide_Table2.Rows[i]["__Thanh_Tien"] = decimal.Parse(divide_Table2.Rows[i]["__GIABAN"].ToString()) * decimal.Parse(divide_Table2.Rows[i]["__SOLUONG"].ToString());
                        checkProductOnGv = true;
                    }
                }
            }

            if (checkProductOnGv == false)
            {
                DataRow dr = divide_Table2.NewRow();
                dr[0] = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString();
                dr[1] = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_TEN_HANGHOA"]).ToString();

                if (decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString()) <= 1)
                {
                    if (flag_sole)
                        dr[2] = SoLuong_HuyMon;
                    else
                        dr[2] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                }
                else
                {
                    if (flag_sole)
                        dr[2] = SoLuong_HuyMon;
                    else
                        dr[2] = 1;
                }
                dr[3] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_GIABAN"]).ToString());
                dr[4] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_MA_BEP"]).ToString());
                dr[5] = decimal.Parse(dr[2].ToString()) * decimal.Parse(dr[3].ToString());
                dr[6] = false;
                divide_Table2.Rows.Add(dr);
            }
            gr_divide_Table_2.DataSource = divide_Table2;
            gv_divide_Table_2.FocusedRowHandle = gv_divide_Table_2.RowCount - 1;

            if (decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString()) <= 1)
            {
                if (flag_sole)
                {
                    gv_divide_Table_1.SetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"], sl - SoLuong_HuyMon);
                    if (decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString()) == 0)
                        gv_divide_Table_1.DeleteSelectedRows();
                }
                else
                    gv_divide_Table_1.DeleteSelectedRows();
            }
            else
            {
                if (flag_sole)
                    gv_divide_Table_1.SetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"], sl - SoLuong_HuyMon);
                else
                    gv_divide_Table_1.SetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"], sl - 1);
            }
            gv_divide_Table_1.SetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_Thanh_Tien"], decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString()) * decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_GIABAN"]).ToString()));
            flag_sole = false;
        }

        private void Throw_Table2ToTable1()
        {
            if (gv_divide_Table_2.RowCount <= 0)
                return;
            bool checkProductOnGv = false;
            if (divide_Table1.Rows.Count > 0)
            {
                for (int i = 0; i < divide_Table1.Rows.Count; i++)
                {
                    string maHH = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                    if (divide_Table1.Rows[i]["_MA_HANGHOA"].ToString() == maHH)
                    {
                        if (decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString()) < 1)
                            divide_Table1.Rows[i]["_SOLUONG"] = decimal.Parse(divide_Table1.Rows[i]["_SOLUONG"].ToString()) + decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                        else
                            divide_Table1.Rows[i]["_SOLUONG"] = decimal.Parse(divide_Table1.Rows[i]["_SOLUONG"].ToString()) + 1;
                        divide_Table1.Rows[i]["_Thanh_Tien"] = decimal.Parse(divide_Table1.Rows[i]["_GIABAN"].ToString()) * decimal.Parse(divide_Table1.Rows[i]["_SOLUONG"].ToString());
                        checkProductOnGv = true;
                    }
                }
            }
            if (checkProductOnGv == false)
            {
                DataRow dr = divide_Table1.NewRow();
                dr[0] = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                dr[1] = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__TEN_HANGHOA"]).ToString();
                if (decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString()) < 1)
                    dr[2] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                else
                    dr[2] = 1;
                dr[3] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__GIABAN"]).ToString());
                dr[4] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__MA_BEP"]).ToString());
                dr[5] = decimal.Parse(dr[2].ToString()) * decimal.Parse(dr[3].ToString());
                divide_Table1.Rows.Add(dr);
            }
            gr_divide_Table_1.DataSource = divide_Table1;
            gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.RowCount - 1;
            if (decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString()) <= 1)
            {
                gv_divide_Table_2.DeleteSelectedRows();
            }
            else
            {
                decimal sl = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                gv_divide_Table_2.SetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"], sl - 1);
            }
            gv_divide_Table_2.SetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__Thanh_Tien"], decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString()) * decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__GIABAN"]).ToString()));
        }

        private void AddColumnDatatable()
        {
            //add colum tach ban
            divide_Table1.Columns.Add("_MA_HANGHOA", Type.GetType("System.String"));
            divide_Table1.Columns.Add("_TEN_HANGHOA", Type.GetType("System.String"));
            divide_Table1.Columns.Add("_SOLUONG", Type.GetType("System.Double"));
            divide_Table1.Columns.Add("_GIABAN", Type.GetType("System.Double"));
            divide_Table1.Columns.Add("_MA_BEP", typeof(Int32));
            divide_Table1.Columns.Add("_Thanh_Tien", Type.GetType("System.Double"));
            //add colum tach ban
            divide_Table2.Columns.Add("__MA_HANGHOA", Type.GetType("System.String"));
            divide_Table2.Columns.Add("__TEN_HANGHOA", Type.GetType("System.String"));
            divide_Table2.Columns.Add("__SOLUONG", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("__GIABAN", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("__MA_BEP", typeof(Int32));
            divide_Table2.Columns.Add("__Thanh_Tien", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("__IS_COMBO", typeof(bool));
        }

        private void btn_NhapSL_Click(object sender, EventArgs e)
        {
            Frm_Chontheosoluong frm = new Frm_Chontheosoluong();
            frm.Mode = 1;
            frm.ShowDialog();
            if (frm.bThoat == false)
            {
                SoLuong_HuyMon = frm.tmp_SoLuong;
                flag_sole = true;
                ThrowTable1ToTable2();
            }
            frm.Dispose();
        }

        private void btn_OkThrough_Click(object sender, EventArgs e)
        {
            DataTable dt1 = (DataTable)gr_divide_Table_1.DataSource;
            DataTable dt2 = (DataTable)gr_divide_Table_2.DataSource;
            if (dt2 != null)
            {
                if (dt2.Rows.Count != 0)
                {
                    dt_ban_new = dt1;
                    dt_ban_thanhtoan = dt2;
                }
            }
            else
                dt_ban_thanhtoan = null;
            this.Close();
        }
    }
}