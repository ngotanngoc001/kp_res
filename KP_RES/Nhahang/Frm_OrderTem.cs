﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;

namespace KP_RES
{
    public partial class Frm_OrderTem : DevExpress.XtraEditors.XtraForm
    {
        public DataTable dtGridviewProduct = new DataTable();
        public DataTable dtGridviewProduct1 = new DataTable();

        public Frm_OrderTem(DataTable dtGridview)
        {
            InitializeComponent();
            dtGridviewProduct = dtGridview;
            LoaddataGridView();
        }

        private void LoaddataGridView()
        {
            dtGridviewProduct1 = dtGridviewProduct;

            gridView1.Focus();
            gridControl1.DataSource = dtGridviewProduct1;
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
                return true;
            else
                return false;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            btnLuu.Focus();
            dtGridviewProduct = dtGridviewProduct1;
            this.Close();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_NhanVienPayment_Load(object sender, EventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            gridView1.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView1.Columns["TEN_HANGHOA"].Caption = rm.GetString("tenhanghoa", culture);
            //gridView1.Columns["sTenNhanVien"].Caption = rm.GetString("nhanvien", culture);
            btnLuu.Text = rm.GetString("luu",culture);
            //btnThoat.Text = rm.GetString("thoat", culture);
        }

    }
}