﻿namespace KP_RES
{
    partial class Frm_Danhsachdatban
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Danhsachdatban));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MA_BAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_BAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HOADON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIENDATCOC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIODAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TRANGTHAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SODIENTHOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btn_TrangThai = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gridControl1);
            this.panelControl1.Controls.Add(this.gridControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(2, 79);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 326);
            this.panelControl1.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.AllowDrop = true;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Right;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(908, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(110, 322);
            this.gridControl1.TabIndex = 5;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.MA_BAN,
            this.TEN_BAN});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            // 
            // MA_BAN
            // 
            this.MA_BAN.Caption = "MA_BAN";
            this.MA_BAN.FieldName = "MA_BAN";
            this.MA_BAN.Name = "MA_BAN";
            // 
            // TEN_BAN
            // 
            this.TEN_BAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TEN_BAN.AppearanceCell.Options.UseFont = true;
            this.TEN_BAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_BAN.AppearanceHeader.Options.UseFont = true;
            this.TEN_BAN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_BAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_BAN.Caption = "DS Bàn";
            this.TEN_BAN.FieldName = "TEN_BAN";
            this.TEN_BAN.Name = "TEN_BAN";
            this.TEN_BAN.OptionsColumn.AllowEdit = false;
            this.TEN_BAN.OptionsColumn.AllowFocus = false;
            this.TEN_BAN.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TEN_BAN.OptionsColumn.FixedWidth = true;
            this.TEN_BAN.OptionsFilter.AllowAutoFilter = false;
            this.TEN_BAN.OptionsFilter.AllowFilter = false;
            this.TEN_BAN.Visible = true;
            this.TEN_BAN.VisibleIndex = 0;
            this.TEN_BAN.Width = 130;
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Left;
            gridLevelNode2.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gridControl2.Location = new System.Drawing.Point(2, 2);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(862, 322);
            this.gridControl2.TabIndex = 4;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MA_HOADON,
            this.TIENDATCOC,
            this.GIODAT,
            this.TRANGTHAI,
            this.TEN,
            this.SODIENTHOAI});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.OptionsFilter.AllowAutoFilter = false;
            this.STT.OptionsFilter.AllowFilter = false;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 45;
            // 
            // MA_HOADON
            // 
            this.MA_HOADON.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MA_HOADON.AppearanceCell.Options.UseFont = true;
            this.MA_HOADON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA_HOADON.AppearanceHeader.Options.UseFont = true;
            this.MA_HOADON.AppearanceHeader.Options.UseTextOptions = true;
            this.MA_HOADON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_HOADON.Caption = "Hóa đơn";
            this.MA_HOADON.FieldName = "MA_HOADON";
            this.MA_HOADON.Name = "MA_HOADON";
            this.MA_HOADON.OptionsColumn.AllowEdit = false;
            this.MA_HOADON.OptionsColumn.AllowFocus = false;
            this.MA_HOADON.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.MA_HOADON.OptionsColumn.FixedWidth = true;
            this.MA_HOADON.OptionsFilter.AllowAutoFilter = false;
            this.MA_HOADON.OptionsFilter.AllowFilter = false;
            this.MA_HOADON.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.False;
            this.MA_HOADON.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.MA_HOADON.OptionsFilter.ImmediateUpdateAutoFilter = false;
            this.MA_HOADON.Visible = true;
            this.MA_HOADON.VisibleIndex = 1;
            this.MA_HOADON.Width = 135;
            // 
            // TIENDATCOC
            // 
            this.TIENDATCOC.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TIENDATCOC.AppearanceCell.Options.UseFont = true;
            this.TIENDATCOC.AppearanceCell.Options.UseTextOptions = true;
            this.TIENDATCOC.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIENDATCOC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TIENDATCOC.AppearanceHeader.Options.UseFont = true;
            this.TIENDATCOC.AppearanceHeader.Options.UseTextOptions = true;
            this.TIENDATCOC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIENDATCOC.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TIENDATCOC.Caption = "Tiền đặt cọc";
            this.TIENDATCOC.DisplayFormat.FormatString = "{0:#,###0}";
            this.TIENDATCOC.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.TIENDATCOC.FieldName = "TIENDATCOC";
            this.TIENDATCOC.Name = "TIENDATCOC";
            this.TIENDATCOC.OptionsColumn.AllowEdit = false;
            this.TIENDATCOC.OptionsColumn.AllowFocus = false;
            this.TIENDATCOC.Visible = true;
            this.TIENDATCOC.VisibleIndex = 2;
            this.TIENDATCOC.Width = 116;
            // 
            // GIODAT
            // 
            this.GIODAT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.GIODAT.AppearanceCell.Options.UseFont = true;
            this.GIODAT.AppearanceCell.Options.UseTextOptions = true;
            this.GIODAT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIODAT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIODAT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIODAT.AppearanceHeader.Options.UseFont = true;
            this.GIODAT.AppearanceHeader.Options.UseTextOptions = true;
            this.GIODAT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIODAT.Caption = "Giờ đặt";
            this.GIODAT.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm:ss";
            this.GIODAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GIODAT.FieldName = "GIODAT";
            this.GIODAT.Name = "GIODAT";
            this.GIODAT.OptionsColumn.AllowEdit = false;
            this.GIODAT.OptionsColumn.AllowFocus = false;
            this.GIODAT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.GIODAT.OptionsColumn.FixedWidth = true;
            this.GIODAT.Visible = true;
            this.GIODAT.VisibleIndex = 4;
            this.GIODAT.Width = 176;
            // 
            // TRANGTHAI
            // 
            this.TRANGTHAI.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TRANGTHAI.AppearanceCell.Options.UseFont = true;
            this.TRANGTHAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TRANGTHAI.AppearanceHeader.Options.UseFont = true;
            this.TRANGTHAI.AppearanceHeader.Options.UseTextOptions = true;
            this.TRANGTHAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TRANGTHAI.Caption = "Trạng thái";
            this.TRANGTHAI.FieldName = "TRANGTHAI";
            this.TRANGTHAI.Name = "TRANGTHAI";
            this.TRANGTHAI.OptionsColumn.AllowEdit = false;
            this.TRANGTHAI.OptionsColumn.AllowFocus = false;
            this.TRANGTHAI.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TRANGTHAI.OptionsColumn.FixedWidth = true;
            this.TRANGTHAI.OptionsFilter.AllowAutoFilter = false;
            this.TRANGTHAI.OptionsFilter.AllowFilter = false;
            this.TRANGTHAI.Visible = true;
            this.TRANGTHAI.VisibleIndex = 6;
            this.TRANGTHAI.Width = 110;
            // 
            // TEN
            // 
            this.TEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TEN.AppearanceCell.Options.UseFont = true;
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.Caption = "Người đặt";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowEdit = false;
            this.TEN.OptionsColumn.AllowFocus = false;
            this.TEN.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TEN.OptionsColumn.FixedWidth = true;
            this.TEN.OptionsFilter.AllowAutoFilter = false;
            this.TEN.OptionsFilter.AllowFilter = false;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 3;
            this.TEN.Width = 138;
            // 
            // SODIENTHOAI
            // 
            this.SODIENTHOAI.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.SODIENTHOAI.AppearanceCell.Options.UseFont = true;
            this.SODIENTHOAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SODIENTHOAI.AppearanceHeader.Options.UseFont = true;
            this.SODIENTHOAI.AppearanceHeader.Options.UseTextOptions = true;
            this.SODIENTHOAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SODIENTHOAI.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SODIENTHOAI.Caption = "Điện thoại";
            this.SODIENTHOAI.FieldName = "SODIENTHOAI";
            this.SODIENTHOAI.Name = "SODIENTHOAI";
            this.SODIENTHOAI.OptionsColumn.AllowEdit = false;
            this.SODIENTHOAI.OptionsColumn.AllowFocus = false;
            this.SODIENTHOAI.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.SODIENTHOAI.OptionsFilter.AllowAutoFilter = false;
            this.SODIENTHOAI.OptionsFilter.AllowFilter = false;
            this.SODIENTHOAI.Visible = true;
            this.SODIENTHOAI.VisibleIndex = 5;
            this.SODIENTHOAI.Width = 140;
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.btn_TrangThai);
            this.groupControl1.Controls.Add(this.btnDong);
            this.groupControl1.Controls.Add(this.panelControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupControl1.Size = new System.Drawing.Size(1024, 407);
            this.groupControl1.TabIndex = 7;
            this.groupControl1.Text = "Danh sách đặt bàn";
            // 
            // btn_TrangThai
            // 
            this.btn_TrangThai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TrangThai.Appearance.Options.UseFont = true;
            this.btn_TrangThai.Image = ((System.Drawing.Image)(resources.GetObject("btn_TrangThai.Image")));
            this.btn_TrangThai.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_TrangThai.Location = new System.Drawing.Point(685, 31);
            this.btn_TrangThai.Name = "btn_TrangThai";
            this.btn_TrangThai.Size = new System.Drawing.Size(181, 42);
            this.btn_TrangThai.TabIndex = 7;
            this.btn_TrangThai.Text = "Mở/Đóng bàn";
            this.btn_TrangThai.Click += new System.EventHandler(this.btn_TrangThai_Click);
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = ((System.Drawing.Image)(resources.GetObject("btnDong.Image")));
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(910, 31);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(109, 42);
            this.btnDong.TabIndex = 6;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // Frm_Danhsachdatban
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 407);
            this.Controls.Add(this.groupControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximumSize = new System.Drawing.Size(1200, 407);
            this.MinimumSize = new System.Drawing.Size(896, 407);
            this.Name = "Frm_Danhsachdatban";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FRM_DATBAN";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.SimpleButton btn_TrangThai;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_BAN;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HOADON;
        private DevExpress.XtraGrid.Columns.GridColumn GIODAT;
        private DevExpress.XtraGrid.Columns.GridColumn TRANGTHAI;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraGrid.Columns.GridColumn SODIENTHOAI;
        private DevExpress.XtraGrid.Columns.GridColumn TIENDATCOC;
        private DevExpress.XtraGrid.Columns.GridColumn MA_BAN;
    }
}