﻿namespace KP_RES 
{
    partial class Frm_Nanghangbuffet 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl19 = new DevExpress.XtraEditors.PanelControl();
            this.pnYeuCau = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl22 = new DevExpress.XtraEditors.PanelControl();
            this.btnYCT_Len = new DevExpress.XtraEditors.SimpleButton();
            this.btnYCT_Xuong = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).BeginInit();
            this.panelControl19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).BeginInit();
            this.panelControl22.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(13, 44);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(83, 19);
            this.lblTEN.TabIndex = 0;
            this.lblTEN.Text = "Chọn buffet";
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant_300x15;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(496, 35);
            this.panel2.TabIndex = 7;
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDong.Location = new System.Drawing.Point(13, 423);
            this.btnDong.Margin = new System.Windows.Forms.Padding(4);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(466, 80);
            this.btnDong.TabIndex = 5;
            this.btnDong.Text = "&Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // panelControl19
            // 
            this.panelControl19.Controls.Add(this.pnYeuCau);
            this.panelControl19.Controls.Add(this.panelControl22);
            this.panelControl19.Location = new System.Drawing.Point(13, 71);
            this.panelControl19.Name = "panelControl19";
            this.panelControl19.Size = new System.Drawing.Size(466, 344);
            this.panelControl19.TabIndex = 15;
            // 
            // pnYeuCau
            // 
            this.pnYeuCau.AutoScroll = true;
            this.pnYeuCau.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnYeuCau.Location = new System.Drawing.Point(2, 2);
            this.pnYeuCau.Name = "pnYeuCau";
            this.pnYeuCau.Size = new System.Drawing.Size(429, 340);
            this.pnYeuCau.TabIndex = 1;
            // 
            // panelControl22
            // 
            this.panelControl22.Controls.Add(this.btnYCT_Len);
            this.panelControl22.Controls.Add(this.btnYCT_Xuong);
            this.panelControl22.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl22.Location = new System.Drawing.Point(431, 2);
            this.panelControl22.Name = "panelControl22";
            this.panelControl22.Size = new System.Drawing.Size(33, 340);
            this.panelControl22.TabIndex = 0;
            // 
            // btnYCT_Len
            // 
            this.btnYCT_Len.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnYCT_Len.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnYCT_Len.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnYCT_Len.Location = new System.Drawing.Point(2, 267);
            this.btnYCT_Len.Name = "btnYCT_Len";
            this.btnYCT_Len.Size = new System.Drawing.Size(29, 71);
            this.btnYCT_Len.TabIndex = 7;
            this.btnYCT_Len.Text = ">";
            this.btnYCT_Len.Click += new System.EventHandler(this.btnYCT_Xuong_Click);
            // 
            // btnYCT_Xuong
            // 
            this.btnYCT_Xuong.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnYCT_Xuong.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnYCT_Xuong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnYCT_Xuong.Location = new System.Drawing.Point(2, 2);
            this.btnYCT_Xuong.Name = "btnYCT_Xuong";
            this.btnYCT_Xuong.Size = new System.Drawing.Size(29, 71);
            this.btnYCT_Xuong.TabIndex = 8;
            this.btnYCT_Xuong.Text = "<";
            this.btnYCT_Xuong.Click += new System.EventHandler(this.btnYCT_Len_Click);
            // 
            // Frm_Nanghangbuffet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 519);
            this.ControlBox = false;
            this.Controls.Add(this.panelControl19);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lblTEN);
            this.Controls.Add(this.btnDong);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Frm_Nanghangbuffet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nâng hạng bufet";
            this.Load += new System.EventHandler(this.Frm_Nanghangbuffet_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).EndInit();
            this.panelControl19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).EndInit();
            this.panelControl22.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.PanelControl panelControl19;
        private System.Windows.Forms.FlowLayoutPanel pnYeuCau;
        private DevExpress.XtraEditors.PanelControl panelControl22;
        private DevExpress.XtraEditors.SimpleButton btnYCT_Len;
        private DevExpress.XtraEditors.SimpleButton btnYCT_Xuong;




    }
}