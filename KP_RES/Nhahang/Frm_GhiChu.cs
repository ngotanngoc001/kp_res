﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES
{
    public partial class Frm_GhiChu : DevExpress.XtraEditors.XtraForm
    {
        public String sGhiChu = "";
        public String smode= "";
       

        public Frm_GhiChu(String GhiChu)
        {
            InitializeComponent();
            sGhiChu = GhiChu;

            txtGhiChu.Text = sGhiChu;
            txtGhiChu.Focus();
            us_keyboard1.SetTextbox(txtGhiChu);
        }

        private bool checkinput()
        {
            if (txtGhiChu.Text.Trim() == "")
            {
                XtraMessageBox.Show("Nhập ghi chú", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!checkinput())
                return;

            sGhiChu = txtGhiChu.Text;
            this.Close();
        }

        private void Frm_Giaohang_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnLuu_Click(sender, e);
            }
        }

        private void Frm_GhiChu_Load(object sender, EventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            lblGhiChu.Text = rm.GetString("ghichu",culture);
            this.Text = rm.GetString("ghichuhoadon", culture);
            if (smode == "1")
            {
                lblGhiChu.Text ="Mã hóa đơn" ;
                this.Text = "Nhập mã hóa đơn đợi";
            }
        }

        private void Frm_GhiChu_FormClosing(object sender, FormClosingEventArgs e)
        {
        }
    }
}