﻿namespace KP_RES
{
    partial class Frm_Manhinhhai_Nhahang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Manhinhhai_Nhahang));
            this.pn_Right = new System.Windows.Forms.Panel();
            this.pn_View = new System.Windows.Forms.Panel();
            this.gr_SelectProduct = new DevExpress.XtraGrid.GridControl();
            this.gv_SelectProduct = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SOTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.GIABAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Thanh_Tien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_BEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.pn_chensubBottom = new System.Windows.Forms.Panel();
            this.pn_ThanhToan = new System.Windows.Forms.Panel();
            this.pnThanhToan = new DevExpress.XtraEditors.PanelControl();
            this.pnlThanhTien = new DevExpress.XtraEditors.PanelControl();
            this.lbThanhTien = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.pnlGiamGia = new DevExpress.XtraEditors.PanelControl();
            this.lbGiamGia = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.pnlTongCong = new DevExpress.XtraEditors.PanelControl();
            this.lbTongCong = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pnlBottom1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pn_banner = new System.Windows.Forms.Panel();
            this.lbNumber = new System.Windows.Forms.Label();
            this.lbprice = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pnlTop1 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pn_chenRight = new System.Windows.Forms.Panel();
            this.pn_ChenRightRight = new System.Windows.Forms.Panel();
            this.pn_Left = new System.Windows.Forms.Panel();
            this.pnVideo = new System.Windows.Forms.Panel();
            this.WindowsMediaPlayer = new AxWMPLib.AxWindowsMediaPlayer();
            this.pnlLeft = new System.Windows.Forms.Panel();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.pn_Right.SuspendLayout();
            this.pn_View.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gr_SelectProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_SelectProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            this.pn_ThanhToan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnThanhToan)).BeginInit();
            this.pnThanhToan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlThanhTien)).BeginInit();
            this.pnlThanhTien.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlGiamGia)).BeginInit();
            this.pnlGiamGia.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTongCong)).BeginInit();
            this.pnlTongCong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.pn_banner.SuspendLayout();
            this.pnlTop1.SuspendLayout();
            this.pn_Left.SuspendLayout();
            this.pnVideo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WindowsMediaPlayer)).BeginInit();
            this.SuspendLayout();
            // 
            // pn_Right
            // 
            this.pn_Right.Controls.Add(this.pn_View);
            this.pn_Right.Controls.Add(this.pn_chensubBottom);
            this.pn_Right.Controls.Add(this.pn_ThanhToan);
            this.pn_Right.Controls.Add(this.pnlBottom1);
            this.pn_Right.Controls.Add(this.panel2);
            this.pn_Right.Controls.Add(this.pn_banner);
            this.pn_Right.Controls.Add(this.panel4);
            this.pn_Right.Controls.Add(this.pnlTop1);
            this.pn_Right.Controls.Add(this.panel3);
            this.pn_Right.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn_Right.Location = new System.Drawing.Point(452, 0);
            this.pn_Right.Name = "pn_Right";
            this.pn_Right.Size = new System.Drawing.Size(560, 581);
            this.pn_Right.TabIndex = 3;
            // 
            // pn_View
            // 
            this.pn_View.BackColor = System.Drawing.Color.LightBlue;
            this.pn_View.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pn_View.Controls.Add(this.gr_SelectProduct);
            this.pn_View.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_View.Location = new System.Drawing.Point(0, 162);
            this.pn_View.Name = "pn_View";
            this.pn_View.Size = new System.Drawing.Size(560, 288);
            this.pn_View.TabIndex = 0;
            // 
            // gr_SelectProduct
            // 
            this.gr_SelectProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gr_SelectProduct.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gr_SelectProduct.Location = new System.Drawing.Point(0, 0);
            this.gr_SelectProduct.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.gr_SelectProduct.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gr_SelectProduct.MainView = this.gv_SelectProduct;
            this.gr_SelectProduct.Name = "gr_SelectProduct";
            this.gr_SelectProduct.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemMemoEdit1});
            this.gr_SelectProduct.Size = new System.Drawing.Size(558, 286);
            this.gr_SelectProduct.TabIndex = 20;
            this.gr_SelectProduct.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv_SelectProduct});
            // 
            // gv_SelectProduct
            // 
            this.gv_SelectProduct.Appearance.FocusedRow.BackColor = System.Drawing.Color.Red;
            this.gv_SelectProduct.Appearance.FocusedRow.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(1)), true);
            this.gv_SelectProduct.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gv_SelectProduct.Appearance.FocusedRow.Options.UseFont = true;
            this.gv_SelectProduct.Appearance.HeaderPanel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.gv_SelectProduct.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gv_SelectProduct.Appearance.Row.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.gv_SelectProduct.Appearance.Row.Options.UseFont = true;
            this.gv_SelectProduct.ColumnPanelRowHeight = 50;
            this.gv_SelectProduct.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SOTT,
            this.TEN_HANGHOA,
            this.SOLUONG,
            this.GIABAN,
            this.Thanh_Tien,
            this.MA_HANGHOA,
            this.GHICHU,
            this.MA_BEP,
            this.THUE});
            this.gv_SelectProduct.GridControl = this.gr_SelectProduct;
            this.gv_SelectProduct.Name = "gv_SelectProduct";
            this.gv_SelectProduct.OptionsView.RowAutoHeight = true;
            this.gv_SelectProduct.OptionsView.ShowGroupPanel = false;
            this.gv_SelectProduct.OptionsView.ShowIndicator = false;
            this.gv_SelectProduct.RowHeight = 60;
            this.gv_SelectProduct.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gv_SelectProduct.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gv_SelectProduct_CustomDrawCell);
            this.gv_SelectProduct.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gv_SelectProduct_RowCellStyle);
            // 
            // SOTT
            // 
            this.SOTT.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 14F);
            this.SOTT.AppearanceCell.Options.UseFont = true;
            this.SOTT.AppearanceCell.Options.UseTextOptions = true;
            this.SOTT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOTT.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOTT.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SOTT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.SOTT.AppearanceHeader.Options.UseFont = true;
            this.SOTT.AppearanceHeader.Options.UseTextOptions = true;
            this.SOTT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOTT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOTT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SOTT.Caption = "TT";
            this.SOTT.FieldName = "SOTT";
            this.SOTT.Name = "SOTT";
            this.SOTT.OptionsColumn.AllowEdit = false;
            this.SOTT.OptionsColumn.AllowFocus = false;
            this.SOTT.OptionsColumn.FixedWidth = true;
            this.SOTT.Visible = true;
            this.SOTT.VisibleIndex = 0;
            this.SOTT.Width = 35;
            // 
            // TEN_HANGHOA
            // 
            this.TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 14F);
            this.TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceCell.Options.UseTextOptions = true;
            this.TEN_HANGHOA.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TEN_HANGHOA.Caption = "Tên Hàng Hóa";
            this.TEN_HANGHOA.ColumnEdit = this.repositoryItemMemoEdit1;
            this.TEN_HANGHOA.FieldName = "TEN_HANGHOA";
            this.TEN_HANGHOA.Name = "TEN_HANGHOA";
            this.TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this.TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this.TEN_HANGHOA.Visible = true;
            this.TEN_HANGHOA.VisibleIndex = 1;
            this.TEN_HANGHOA.Width = 184;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // SOLUONG
            // 
            this.SOLUONG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 14F);
            this.SOLUONG.AppearanceCell.Options.UseFont = true;
            this.SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SOLUONG.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SOLUONG.Caption = "SL";
            this.SOLUONG.ColumnEdit = this.repositoryItemCalcEdit1;
            this.SOLUONG.FieldName = "SOLUONG";
            this.SOLUONG.Name = "SOLUONG";
            this.SOLUONG.OptionsColumn.AllowEdit = false;
            this.SOLUONG.OptionsColumn.AllowFocus = false;
            this.SOLUONG.OptionsColumn.FixedWidth = true;
            this.SOLUONG.Visible = true;
            this.SOLUONG.VisibleIndex = 2;
            this.SOLUONG.Width = 35;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // GIABAN
            // 
            this.GIABAN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 14F);
            this.GIABAN.AppearanceCell.Options.UseFont = true;
            this.GIABAN.AppearanceCell.Options.UseTextOptions = true;
            this.GIABAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIABAN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GIABAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.GIABAN.AppearanceHeader.Options.UseFont = true;
            this.GIABAN.AppearanceHeader.Options.UseTextOptions = true;
            this.GIABAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIABAN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GIABAN.Caption = "Đơn giá";
            this.GIABAN.DisplayFormat.FormatString = "{0:#,###0}";
            this.GIABAN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GIABAN.FieldName = "GIABAN";
            this.GIABAN.Name = "GIABAN";
            this.GIABAN.OptionsColumn.AllowEdit = false;
            this.GIABAN.OptionsColumn.AllowFocus = false;
            this.GIABAN.OptionsColumn.FixedWidth = true;
            this.GIABAN.Visible = true;
            this.GIABAN.VisibleIndex = 3;
            // 
            // Thanh_Tien
            // 
            this.Thanh_Tien.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 14F);
            this.Thanh_Tien.AppearanceCell.Options.UseFont = true;
            this.Thanh_Tien.AppearanceCell.Options.UseTextOptions = true;
            this.Thanh_Tien.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Thanh_Tien.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Thanh_Tien.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Thanh_Tien.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.Thanh_Tien.AppearanceHeader.Options.UseFont = true;
            this.Thanh_Tien.AppearanceHeader.Options.UseTextOptions = true;
            this.Thanh_Tien.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Thanh_Tien.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Thanh_Tien.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Thanh_Tien.Caption = "Thành Tiền";
            this.Thanh_Tien.DisplayFormat.FormatString = "{0:#,###0}";
            this.Thanh_Tien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.Thanh_Tien.FieldName = "Thanh_Tien";
            this.Thanh_Tien.Name = "Thanh_Tien";
            this.Thanh_Tien.OptionsColumn.AllowEdit = false;
            this.Thanh_Tien.OptionsColumn.AllowFocus = false;
            this.Thanh_Tien.OptionsColumn.FixedWidth = true;
            this.Thanh_Tien.Visible = true;
            this.Thanh_Tien.VisibleIndex = 4;
            // 
            // MA_HANGHOA
            // 
            this.MA_HANGHOA.Caption = "Mã Hàng Hóa";
            this.MA_HANGHOA.FieldName = "MA_HANGHOA";
            this.MA_HANGHOA.Name = "MA_HANGHOA";
            this.MA_HANGHOA.OptionsColumn.AllowEdit = false;
            this.MA_HANGHOA.OptionsColumn.AllowFocus = false;
            // 
            // GHICHU
            // 
            this.GHICHU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 14F);
            this.GHICHU.AppearanceCell.Options.UseFont = true;
            this.GHICHU.AppearanceCell.Options.UseTextOptions = true;
            this.GHICHU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GHICHU.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GHICHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.GHICHU.AppearanceHeader.Options.UseFont = true;
            this.GHICHU.AppearanceHeader.Options.UseTextOptions = true;
            this.GHICHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GHICHU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GHICHU.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GHICHU.Caption = "Ghi Chú";
            this.GHICHU.ColumnEdit = this.repositoryItemMemoEdit1;
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.Name = "GHICHU";
            this.GHICHU.Visible = true;
            this.GHICHU.VisibleIndex = 5;
            this.GHICHU.Width = 150;
            // 
            // MA_BEP
            // 
            this.MA_BEP.Caption = "Máy In Bếp";
            this.MA_BEP.FieldName = "MA_BEP";
            this.MA_BEP.Name = "MA_BEP";
            // 
            // THUE
            // 
            this.THUE.Caption = "VAT";
            this.THUE.FieldName = "THUE";
            this.THUE.Name = "THUE";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // pn_chensubBottom
            // 
            this.pn_chensubBottom.BackColor = System.Drawing.Color.Gray;
            this.pn_chensubBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pn_chensubBottom.Location = new System.Drawing.Point(0, 450);
            this.pn_chensubBottom.Name = "pn_chensubBottom";
            this.pn_chensubBottom.Size = new System.Drawing.Size(560, 14);
            this.pn_chensubBottom.TabIndex = 3;
            // 
            // pn_ThanhToan
            // 
            this.pn_ThanhToan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pn_ThanhToan.Controls.Add(this.pnThanhToan);
            this.pn_ThanhToan.Controls.Add(this.panelControl2);
            this.pn_ThanhToan.Controls.Add(this.panelControl1);
            this.pn_ThanhToan.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pn_ThanhToan.Location = new System.Drawing.Point(0, 464);
            this.pn_ThanhToan.Name = "pn_ThanhToan";
            this.pn_ThanhToan.Size = new System.Drawing.Size(560, 103);
            this.pn_ThanhToan.TabIndex = 1;
            // 
            // pnThanhToan
            // 
            this.pnThanhToan.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnThanhToan.Controls.Add(this.pnlThanhTien);
            this.pnThanhToan.Controls.Add(this.panelControl6);
            this.pnThanhToan.Controls.Add(this.pnlGiamGia);
            this.pnThanhToan.Controls.Add(this.panelControl5);
            this.pnThanhToan.Controls.Add(this.pnlTongCong);
            this.pnThanhToan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnThanhToan.Location = new System.Drawing.Point(64, 0);
            this.pnThanhToan.Name = "pnThanhToan";
            this.pnThanhToan.Size = new System.Drawing.Size(465, 101);
            this.pnThanhToan.TabIndex = 35;
            // 
            // pnlThanhTien
            // 
            this.pnlThanhTien.Appearance.BackColor = System.Drawing.Color.White;
            this.pnlThanhTien.Appearance.Options.UseBackColor = true;
            this.pnlThanhTien.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlThanhTien.Controls.Add(this.lbThanhTien);
            this.pnlThanhTien.Controls.Add(this.labelControl3);
            this.pnlThanhTien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlThanhTien.Location = new System.Drawing.Point(0, 64);
            this.pnlThanhTien.Name = "pnlThanhTien";
            this.pnlThanhTien.Size = new System.Drawing.Size(465, 37);
            this.pnlThanhTien.TabIndex = 43;
            // 
            // lbThanhTien
            // 
            this.lbThanhTien.Appearance.Font = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbThanhTien.Appearance.ForeColor = System.Drawing.Color.Green;
            this.lbThanhTien.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbThanhTien.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbThanhTien.Location = new System.Drawing.Point(449, 0);
            this.lbThanhTien.Margin = new System.Windows.Forms.Padding(4);
            this.lbThanhTien.Name = "lbThanhTien";
            this.lbThanhTien.Size = new System.Drawing.Size(16, 36);
            this.lbThanhTien.TabIndex = 42;
            this.lbThanhTien.Text = "0";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Green;
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelControl3.Location = new System.Drawing.Point(0, 0);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(159, 36);
            this.labelControl3.TabIndex = 41;
            this.labelControl3.Text = "Thành Tiền";
            // 
            // panelControl6
            // 
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl6.Location = new System.Drawing.Point(0, 62);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(465, 2);
            this.panelControl6.TabIndex = 41;
            // 
            // pnlGiamGia
            // 
            this.pnlGiamGia.Appearance.BackColor = System.Drawing.Color.White;
            this.pnlGiamGia.Appearance.Options.UseBackColor = true;
            this.pnlGiamGia.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlGiamGia.Controls.Add(this.lbGiamGia);
            this.pnlGiamGia.Controls.Add(this.labelControl1);
            this.pnlGiamGia.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlGiamGia.Location = new System.Drawing.Point(0, 32);
            this.pnlGiamGia.Name = "pnlGiamGia";
            this.pnlGiamGia.Size = new System.Drawing.Size(465, 30);
            this.pnlGiamGia.TabIndex = 44;
            // 
            // lbGiamGia
            // 
            this.lbGiamGia.Appearance.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGiamGia.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbGiamGia.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbGiamGia.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbGiamGia.Location = new System.Drawing.Point(453, 0);
            this.lbGiamGia.Margin = new System.Windows.Forms.Padding(4);
            this.lbGiamGia.Name = "lbGiamGia";
            this.lbGiamGia.Size = new System.Drawing.Size(12, 27);
            this.lbGiamGia.TabIndex = 41;
            this.lbGiamGia.Text = "0";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl1.Location = new System.Drawing.Point(61, 0);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(92, 25);
            this.labelControl1.TabIndex = 40;
            this.labelControl1.Text = "Giảm Giá";
            // 
            // panelControl5
            // 
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl5.Location = new System.Drawing.Point(0, 30);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(465, 2);
            this.panelControl5.TabIndex = 2;
            // 
            // pnlTongCong
            // 
            this.pnlTongCong.Appearance.BackColor = System.Drawing.Color.White;
            this.pnlTongCong.Appearance.Options.UseBackColor = true;
            this.pnlTongCong.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlTongCong.Controls.Add(this.lbTongCong);
            this.pnlTongCong.Controls.Add(this.labelControl2);
            this.pnlTongCong.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTongCong.Location = new System.Drawing.Point(0, 0);
            this.pnlTongCong.Name = "pnlTongCong";
            this.pnlTongCong.Size = new System.Drawing.Size(465, 30);
            this.pnlTongCong.TabIndex = 43;
            // 
            // lbTongCong
            // 
            this.lbTongCong.Appearance.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTongCong.Appearance.ForeColor = System.Drawing.Color.MediumBlue;
            this.lbTongCong.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbTongCong.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lbTongCong.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbTongCong.Location = new System.Drawing.Point(359, 0);
            this.lbTongCong.Margin = new System.Windows.Forms.Padding(4);
            this.lbTongCong.Name = "lbTongCong";
            this.lbTongCong.Size = new System.Drawing.Size(106, 27);
            this.lbTongCong.TabIndex = 40;
            this.lbTongCong.Text = "Tổng Cộng";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.MediumBlue;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl2.Location = new System.Drawing.Point(61, 0);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(105, 25);
            this.labelControl2.TabIndex = 39;
            this.labelControl2.Text = "Tổng Cộng";
            // 
            // panelControl2
            // 
            this.panelControl2.Appearance.BackColor = System.Drawing.Color.White;
            this.panelControl2.Appearance.Options.UseBackColor = true;
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(529, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(29, 101);
            this.panelControl2.TabIndex = 42;
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.White;
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(64, 101);
            this.panelControl1.TabIndex = 41;
            // 
            // pnlBottom1
            // 
            this.pnlBottom1.BackColor = System.Drawing.Color.Gray;
            this.pnlBottom1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom1.Location = new System.Drawing.Point(0, 567);
            this.pnlBottom1.Name = "pnlBottom1";
            this.pnlBottom1.Size = new System.Drawing.Size(560, 14);
            this.pnlBottom1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gray;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 148);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(560, 14);
            this.panel2.TabIndex = 4;
            // 
            // pn_banner
            // 
            this.pn_banner.BackColor = System.Drawing.Color.White;
            this.pn_banner.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pn_banner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pn_banner.Controls.Add(this.lbNumber);
            this.pn_banner.Controls.Add(this.lbprice);
            this.pn_banner.Controls.Add(this.lbName);
            this.pn_banner.Dock = System.Windows.Forms.DockStyle.Top;
            this.pn_banner.Location = new System.Drawing.Point(0, 55);
            this.pn_banner.Name = "pn_banner";
            this.pn_banner.Size = new System.Drawing.Size(560, 93);
            this.pn_banner.TabIndex = 4;
            // 
            // lbNumber
            // 
            this.lbNumber.AutoSize = true;
            this.lbNumber.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNumber.ForeColor = System.Drawing.Color.Blue;
            this.lbNumber.Location = new System.Drawing.Point(43, 53);
            this.lbNumber.Name = "lbNumber";
            this.lbNumber.Size = new System.Drawing.Size(127, 31);
            this.lbNumber.TabIndex = 8;
            this.lbNumber.Text = "Số Lượng";
            // 
            // lbprice
            // 
            this.lbprice.AutoSize = true;
            this.lbprice.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbprice.ForeColor = System.Drawing.Color.Blue;
            this.lbprice.Location = new System.Drawing.Point(270, 53);
            this.lbprice.Name = "lbprice";
            this.lbprice.Size = new System.Drawing.Size(49, 31);
            this.lbprice.TabIndex = 7;
            this.lbprice.Text = "giá";
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbName.ForeColor = System.Drawing.Color.Blue;
            this.lbName.Location = new System.Drawing.Point(5, 2);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(59, 31);
            this.lbName.TabIndex = 6;
            this.lbName.Text = "Tên";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Gray;
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 50);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(560, 5);
            this.panel4.TabIndex = 5;
            // 
            // pnlTop1
            // 
            this.pnlTop1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTop1.Controls.Add(this.panel1);
            this.pnlTop1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop1.Location = new System.Drawing.Point(0, 14);
            this.pnlTop1.Name = "pnlTop1";
            this.pnlTop1.Size = new System.Drawing.Size(560, 36);
            this.pnlTop1.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::KP_RES.Properties.Resources.KingPos_for_Restaurant;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(434, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(124, 34);
            this.panel1.TabIndex = 5;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Gray;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(560, 14);
            this.panel3.TabIndex = 9;
            // 
            // pn_chenRight
            // 
            this.pn_chenRight.BackColor = System.Drawing.Color.Gray;
            this.pn_chenRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn_chenRight.Location = new System.Drawing.Point(440, 0);
            this.pn_chenRight.Name = "pn_chenRight";
            this.pn_chenRight.Size = new System.Drawing.Size(12, 581);
            this.pn_chenRight.TabIndex = 2;
            // 
            // pn_ChenRightRight
            // 
            this.pn_ChenRightRight.BackColor = System.Drawing.Color.Gray;
            this.pn_ChenRightRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn_ChenRightRight.Location = new System.Drawing.Point(1012, 0);
            this.pn_ChenRightRight.Name = "pn_ChenRightRight";
            this.pn_ChenRightRight.Size = new System.Drawing.Size(12, 581);
            this.pn_ChenRightRight.TabIndex = 3;
            // 
            // pn_Left
            // 
            this.pn_Left.BackColor = System.Drawing.Color.LightCyan;
            this.pn_Left.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pn_Left.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pn_Left.Controls.Add(this.pnVideo);
            this.pn_Left.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_Left.Location = new System.Drawing.Point(0, 0);
            this.pn_Left.Name = "pn_Left";
            this.pn_Left.Size = new System.Drawing.Size(440, 581);
            this.pn_Left.TabIndex = 2;
            // 
            // pnVideo
            // 
            this.pnVideo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnVideo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnVideo.Controls.Add(this.WindowsMediaPlayer);
            this.pnVideo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnVideo.Location = new System.Drawing.Point(0, 0);
            this.pnVideo.Name = "pnVideo";
            this.pnVideo.Size = new System.Drawing.Size(436, 577);
            this.pnVideo.TabIndex = 5;
            // 
            // WindowsMediaPlayer
            // 
            this.WindowsMediaPlayer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WindowsMediaPlayer.Enabled = true;
            this.WindowsMediaPlayer.Location = new System.Drawing.Point(0, 0);
            this.WindowsMediaPlayer.Name = "WindowsMediaPlayer";
            this.WindowsMediaPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("WindowsMediaPlayer.OcxState")));
            this.WindowsMediaPlayer.Size = new System.Drawing.Size(434, 575);
            this.WindowsMediaPlayer.TabIndex = 3;
            // 
            // pnlLeft
            // 
            this.pnlLeft.BackColor = System.Drawing.Color.Gray;
            this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeft.Location = new System.Drawing.Point(0, 0);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(12, 581);
            this.pnlLeft.TabIndex = 3;
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.Color.Gray;
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(12, 567);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(428, 14);
            this.pnlBottom.TabIndex = 4;
            // 
            // pnlTop
            // 
            this.pnlTop.BackColor = System.Drawing.Color.Gray;
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(12, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(428, 14);
            this.pnlTop.TabIndex = 4;
            // 
            // timer2
            // 
            this.timer2.Interval = 8000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Frm_Manhinhhai_Nhahang
            // 
            this.Appearance.Font = new System.Drawing.Font("Tahoma", 8.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 581);
            this.ControlBox = false;
            this.Controls.Add(this.pnlTop);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.pnlLeft);
            this.Controls.Add(this.pn_Left);
            this.Controls.Add(this.pn_chenRight);
            this.Controls.Add(this.pn_Right);
            this.Controls.Add(this.pn_ChenRightRight);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Manhinhhai_Nhahang";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Màn Hình Hai Nhà Hàng";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frm_Manhinhhai_Nhahang_Load);
            this.pn_Right.ResumeLayout(false);
            this.pn_View.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gr_SelectProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_SelectProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            this.pn_ThanhToan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnThanhToan)).EndInit();
            this.pnThanhToan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlThanhTien)).EndInit();
            this.pnlThanhTien.ResumeLayout(false);
            this.pnlThanhTien.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlGiamGia)).EndInit();
            this.pnlGiamGia.ResumeLayout(false);
            this.pnlGiamGia.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTongCong)).EndInit();
            this.pnlTongCong.ResumeLayout(false);
            this.pnlTongCong.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.pn_banner.ResumeLayout(false);
            this.pn_banner.PerformLayout();
            this.pnlTop1.ResumeLayout(false);
            this.pn_Left.ResumeLayout(false);
            this.pnVideo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WindowsMediaPlayer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pn_Right;
        private System.Windows.Forms.Panel pn_View;
        private System.Windows.Forms.Panel pn_ThanhToan;
        private System.Windows.Forms.Panel pn_chenRight;
        private System.Windows.Forms.Panel pn_ChenRightRight;
        private System.Windows.Forms.Panel pn_banner;
        private System.Windows.Forms.Panel pn_chensubBottom;
        private DevExpress.XtraEditors.PanelControl pnThanhToan;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl pnlThanhTien;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private System.Windows.Forms.Panel pn_Left;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnVideo;
        private AxWMPLib.AxWindowsMediaPlayer WindowsMediaPlayer;
        private DevExpress.XtraGrid.GridControl gr_SelectProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView gv_SelectProduct;
        private DevExpress.XtraGrid.Columns.GridColumn SOTT;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn SOLUONG;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn GIABAN;
        private DevExpress.XtraGrid.Columns.GridColumn Thanh_Tien;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;
        private DevExpress.XtraGrid.Columns.GridColumn MA_BEP;
        private DevExpress.XtraGrid.Columns.GridColumn THUE;
        private System.Windows.Forms.Label lbNumber;
        private System.Windows.Forms.Label lbprice;
        private System.Windows.Forms.Label lbName;
        private DevExpress.XtraEditors.PanelControl pnlGiamGia;
        private DevExpress.XtraEditors.LabelControl lbGiamGia;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl pnlTongCong;
        private DevExpress.XtraEditors.LabelControl lbTongCong;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl lbThanhTien;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.Panel pnlBottom1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlTop1;
        private System.Windows.Forms.Panel pnlLeft;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Timer timer2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;


    }
}