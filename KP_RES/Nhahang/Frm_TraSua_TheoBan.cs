﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Threading;
using KP_UserManagement;
using System.IO;
using KP_Terminal;
using DevExpress.XtraGrid.Views.Grid;
using System.Net.Mail;
using KP_Report;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;

namespace KP_RES
{
    public partial class Frm_TraSua_TheoBan : DevExpress.XtraEditors.XtraForm
    {
        int funtions = 1;//Funtion 1 Sơ đồ bàn, Funtion 2 Mở bàn, 3 yêu cầu thêm, 4 send Order,5 Gép bàn , 6 Tách bàn, 7 Payments, 8 buy id Product, 9 Đặt bàn, 10 ket ca,11 Chuyển bàn.
        int sendOrder = 0;
        string maBan = "";
        string tenBan = "";
        string maBanGhep = "";
        string maMon = "";
        DataTable dtLuoi = new DataTable("HANGHOA");
        DataTable dtBan = new DataTable();
        int cPageKV = 1;
        int cPageNH = 1;
        int cPageMon = 1;
        int cPageBan = 1;
        int maKV = 1;
        int maNH = 1;
        DataTable divide_Table1 = new DataTable();
        DataTable divide_Table2 = new DataTable();
        bool exitReprint = false;
        bool Flag_soluong = false; //true:so luong tren lưới dc bắt đầu lại,False:sô lượng đang tạo +thêm chuỗi.
        string sSQL_DL = "";
        string sSQL_Yeucauthem = "";
        int sokhach = 0;
        bool flag_sole = false; // nếu như hủy món có số lượng là số lẻ
        decimal SoLuong_HuyMon = 0;
        string GhiChu = "";
        public delegate void GetString(DataTable dtshow, int ctrol, int iFocusRow);
        public GetString MyGetData;
        public int contrDelegate = 0;
        public int iFocusRow = 0;
        int sTEM = 1;
        int sTEM_THEMMON = 1;
        int sTT = 1;
        int sMaCB = 1;
        int sMaCB_THEMMON = 1;
        bool sButtonMonThem = false;
        bool isYeuCauThem = false;
        string Kytu_Combo = "   *  ";
        string Kytu_Monthem = "       +  ";
        String sTenMon = "";
        int ssMaCB = 1;
        string sMaHH_THEMMON = "";
        public Frm_TraSua_TheoBan()
        {
            InitializeComponent();
            cls_KP_RES.LoadBangCauHinh();
            LoadPermission();
            AddColumnDatatable();
            lb_Quay.Text = cls_ConfigCashier.nameCashier;
            lb_Kho.Text = cls_ConfigCashier.nameWarehouse;
            lbBan.Text = "";
            lbMon.Text = "";
            lbCaBan.Text = DateTime.Now.ToString("dd-MM-yyyy");
            lb_NhanVien.Text = clsGlobal.gsNameOfUserLogin;
            lbtimeby.Text = cls_ConfigCashier.sTenCB + ": " + cls_ConfigCashier.sGioBD + " --> " + cls_ConfigCashier.sGioKT;
        }

        string _cb = "";
        string _hcb = "";
        string _mb = "";
        string _sdb = "";
        string _dsb = "";
        string _dsm = "";
        string _gb = "";
        string _hgb = "";
        string _gbd = "";
        string _tb = "";
        string _htb = "";
        string _ngayht = "";
        string _dsmonmuontach = "";
        string _dsmoncanhuy = "";
        string _kc = "";
        string _hkc = "";
        string _thongbao = "";
        string _giaban1 = "";
        string _giaban2 = "";
        string _tgvaocuabannaykxd = "";
        string _ngaydatbankhonghople = "";
        string _tgdenkhonghople = "";
        string _bannaydaconguoidat = "";
        string _manvrong = "";
        string _chuachonmayinchomathang = "";
        string _thanhtoankhongthanhcong = "";
        string _huyhdcothelamhtmatondinh = "";
        string _chuakhaibaodinhluong = "";
        string _chuachonmayinchomhnay = "";
        string _ngaydbkhonghople = "";
        string _chuachontgden = "";
        string _banphainhapdayduthongtin = "";
        string _chuanhapttxacnhan = "";
        string _emailkhople = "";
        string _tiendatcoclakieuso = "";
        string _xacnhandatbanthanhcong = "";
        string _bandangmokhongthelaylaihd = "";
        string _Orderinlailan = "";
        string _daylahhtheothoigia = "";
        string _cobansaptoigiovao = "";
        string _khongcohhthaythe = "";
        string _hhkhongthuoccombo = "";
        string _dong = "";
        string _tachbanloi = "";
        string _loiupdatehh = "";
        string _bgphailabdmtkg = "";
        string _ktlaphdnay = "";
        private void Frm_TraSua_TheoBan_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            _loiupdatehh = rm.GetString("loiupdatehh", culture);
            _bgphailabdmtkg = rm.GetString("bgphailabdmtkg", culture);
            _tachbanloi = rm.GetString("tachbanloi", culture);
            btn_ThuNgan.Text = rm.GetString("thungan", culture);
            btn_NghiepVu.Text = rm.GetString("nghiepvu", culture);
            btn_Chuyenban.Text = rm.GetString("chuyenban", culture);
            _cb = rm.GetString("chuyenban", culture);
            _hcb = rm.GetString("huychuyenban", culture);
            btn_SoDoBan.Text = rm.GetString("moban", culture);
            _mb = rm.GetString("moban", culture);
            _sdb = rm.GetString("sodoban", culture);
            _dsb = rm.GetString("danhsachban", culture);
            _dsm = rm.GetString("danhsachmon", culture);
            btn_GuiOrder.Text = rm.GetString("guiorder", culture);
            btn_YeuCauThem.Text = rm.GetString("yeucauthem", culture);
            btn_GhepBan.Text = rm.GetString("ghepban", culture);
            _gb = rm.GetString("ghepban", culture);
            _hgb = rm.GetString("huyghepban", culture);
            _gbd = rm.GetString("ghepbanden", culture);
            btn_Tachban.Text = rm.GetString("tachban", culture);
            _tb = rm.GetString("tachban", culture);
            _htb = rm.GetString("huytachban", culture);
            _ngayht = rm.GetString("ngayht", culture);
            lbCaBan.Text = DateTime.Now.ToString("dd-MM-yyyy");
            btn_ThanhToan.Text = rm.GetString("F1thanhtoan", culture);
            btn_TamTinh.Text = rm.GetString("intamtinh", culture);
            btn_BanGia2.Text = rm.GetString("giaban2", culture);
            _giaban1 = rm.GetString("giaban1", culture);
            _giaban2 = rm.GetString("giaban2", culture);
            btn_InLaiHoaDon.Text = rm.GetString("inlaihoadon", culture);
            btn_DatBan.Text = rm.GetString("datban", culture);
            btn_KetCa.Text = rm.GetString("ketca", culture);
            _kc = rm.GetString("ketca", culture);
            _dong = rm.GetString("dong", culture);
            _hkc = rm.GetString("huyketca", culture);
            btn_LayLaiHoaDon.Text = rm.GetString("laylaihoadon", culture);
            btn_HuyBan.Text = rm.GetString("huyban", culture);
            btn_HuyMon.Text = rm.GetString("huymon", culture);
            btn_InlaiOrder.Text = rm.GetString("inlaiorder", culture);
            btn_MoKet.Text = rm.GetString("moket", culture);
            btn_Dong.Text = rm.GetString("dong", culture);
            lb_phuthu.Text = rm.GetString("phuthu", culture);
            lb_giamgia.Text = rm.GetString("giamgia", culture);
            lb_tongcong.Text = rm.GetString("tongcong", culture);
            btnComboOption.Text = rm.GetString("tuychon", culture);
            btn_xoa.Text = rm.GetString("xoa", culture);
            btn_XoaTatCa.Text = rm.GetString("xoatatca", culture);
            gv_SelectProduct.Columns["SOTT"].Caption = rm.GetString("stt", culture);
            gv_SelectProduct.Columns["TEN_HANGHOA"].Caption = rm.GetString("tenhanghoa", culture);
            gv_SelectProduct.Columns["SOLUONG"].Caption = rm.GetString("soluong", culture);
            gv_SelectProduct.Columns["GIABAN"].Caption = rm.GetString("dongia", culture);
            //gv_SelectProduct.Columns["Thanh_Tien"].Caption = rm.GetString("thanhtien", culture);
            lb_soluong.Text = rm.GetString("soluong1", culture);
            lb_KhuVuc.Text = rm.GetString("khuvuc", culture);
            lb_Ban.Text = rm.GetString("ban", culture);
            lb_TenKH.Text = rm.GetString("tenkh", culture);
            _dsmonmuontach = rm.GetString("dsmonmuontach", culture);
            gvBillPaymented.Columns["SSTT"].Caption = rm.GetString("stt", culture);
            gvBillPaymented.Columns["MA_HOADON"].Caption = rm.GetString("mahoadon", culture);
            gvBillPaymented.Columns["TEN_KHUVUC"].Caption = rm.GetString("khuvuc", culture);
            gvBillPaymented.Columns["TEN_BAN"].Caption = rm.GetString("ban", culture);
            gvBillPaymented.Columns["NGAYTAO"].Caption = rm.GetString("ngaytao", culture);
            gvBillPaymented.Columns["TONGTIEN"].Caption = rm.GetString("tongtien", culture);
            gv_divide_Table_1.Columns["_SSTT"].Caption = rm.GetString("stt", culture);
            gv_divide_Table_1.Columns["_TEN_HANGHOA"].Caption = rm.GetString("tenhanghoa", culture);
            gv_divide_Table_1.Columns["_SOLUONG"].Caption = rm.GetString("soluong", culture);
            gv_divide_Table_1.Columns["_GIABAN"].Caption = rm.GetString("dongia", culture);
            gv_divide_Table_1.Columns["_Thanh_Tien"].Caption = rm.GetString("thanhtien", culture);
            gv_divide_Table_2.Columns["__STT"].Caption = rm.GetString("stt", culture);
            gv_divide_Table_2.Columns["__TEN_HANGHOA"].Caption = rm.GetString("tenhanghoa", culture);
            gv_divide_Table_2.Columns["__SOLUONG"].Caption = rm.GetString("soluong", culture);
            gv_divide_Table_2.Columns["__GIABAN"].Caption = rm.GetString("dongia", culture);
            gv_divide_Table_2.Columns["__Thanh_Tien"].Caption = rm.GetString("thanhtien", culture);
            _dsmoncanhuy = rm.GetString("dsmoncanhuy", culture);
            pnInforTable.Text = rm.GetString("thongtinbandat", culture);
            lb_TenKH2.Text = rm.GetString("khachhang", culture);
            lb_dienthoai2.Text = rm.GetString("dienthoai", culture);
            lb_ngay2.Text = rm.GetString("ngay", culture);
            lb_Diachi.Text = rm.GetString("diachi", culture);
            lb_Tiendatcoc.Text = rm.GetString("tiendatcoc", culture);
            lb_Gio.Text = rm.GetString("gio", culture);
            chkEmail.Text = rm.GetString("guiemail", culture);
            btn_SelectTable.Text = rm.GetString("chonban", culture);
            btn_SelectDishes.Text = rm.GetString("chonmon", culture);
            btnSendEmail.Text = rm.GetString("guiemail", culture);
            btnRemove.Text = rm.GetString("huyban", culture);
            btn_Danhsachdatban.Text = rm.GetString("danhsachdb", culture);
            btnMoBanNgay.Text = rm.GetString("mobanngay", culture);
            btnMobansau.Text = rm.GetString("mobansau", culture);
            btn_Close.Text = rm.GetString("dong", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _tgvaocuabannaykxd = rm.GetString("tgvaocuabannaykxd", culture);
            _ngaydatbankhonghople = rm.GetString("ngaydatbankhonghople", culture);
            _tgdenkhonghople = rm.GetString("tgdenkhonghople", culture);
            _bannaydaconguoidat = rm.GetString("bannaydaconguoidat", culture);
            _manvrong = rm.GetString("manvrong", culture);
            _chuachonmayinchomathang = rm.GetString("chuachonmayinchomathang", culture);
            _thanhtoankhongthanhcong = rm.GetString("thanhtoankhongthanhcong", culture);
            _huyhdcothelamhtmatondinh = rm.GetString("huyhdcothelamhtmatondinh", culture);
            _chuakhaibaodinhluong = rm.GetString("chuakhaibaodinhluong", culture);
            _chuachonmayinchomhnay = rm.GetString("chuachonmayinchomhnay", culture);
            _ngaydbkhonghople = rm.GetString("ngaydbkhonghople", culture);
            _chuachontgden = rm.GetString("chuachontgden", culture);
            _banphainhapdayduthongtin = rm.GetString("banphainhapdayduthongtin", culture);
            _chuanhapttxacnhan = rm.GetString("chuanhapttxacnhan", culture);
            _emailkhople = rm.GetString("emailkhople", culture);
            _tiendatcoclakieuso = rm.GetString("tiendatcoclakieuso", culture);
            _xacnhandatbanthanhcong = rm.GetString("xacnhandatbanthanhcong", culture);
            _bandangmokhongthelaylaihd = rm.GetString("bandangmokhongthelaylaihd", culture);
            _Orderinlailan = rm.GetString("Orderinlailan", culture);
            _daylahhtheothoigia = rm.GetString("daylahhtheothoigia", culture);
            _cobansaptoigiovao = rm.GetString("cobansaptoigiovao", culture);
            _khongcohhthaythe = rm.GetString("khongcohhthaythe", culture);
            _hhkhongthuoccombo = rm.GetString("hhkhongthuoccombo", culture);
        }

        private void Frm_TraSua_TheoBan_Load(object sender, EventArgs e)
        {
            SetBackgroudOnClick(btn_ThuNgan);
            SetDefautlBackgroud(btn_NghiepVu);
            SetDefautlBackgroud(btn_DatThu);
            pnThuNgan.BringToFront();
            pnShowHang.BringToFront();
            pnBtnDeleteGv.Visible = false;
            pnUpDowGv.Visible = false;
            btnComboOption.Visible = false;
            txtID.Visible = false;
            LoadCombo();
            GetKhuVuc();
            GetSoDoBan();
        }

        private void Frm_TraSua_TheoBan_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                if (funtions == 1 && maBan != "" && gv_SelectProduct.RowCount > 0)
                {
                    btn_Payment_Click(sender, e);
                }
            }
            else if (e.KeyCode == Keys.Escape)
            {
                btn_logout_Click(null, null);
            }
        }

        private void LoadPermission()
        {
            //30/08/2016 Thong them cau hinh co dung chuc nang in tam tinh thay khong
            string sIntamtinh = clsUserManagement.CheckCauhinh("INTAMTINH");
            bool bIntamtinh = false;
            if (sIntamtinh != "Error")
            {
                bIntamtinh = sIntamtinh == "1" ? true : false;
            }
            btn_ThanhToan.Enabled = clsUserManagement.AllowAdd("25121701");
            btn_TamTinh.Enabled = bIntamtinh && clsUserManagement.AllowAdd("25121701");
            btn_InLaiHoaDon.Enabled = clsUserManagement.AllowEdit("25121701");
            btn_LayLaiHoaDon.Enabled = clsUserManagement.AllowView("25101801");
            btn_HuyBan.Enabled = clsUserManagement.AllowDelete("25121701");
            btn_HuyMon.Enabled = clsUserManagement.AllowDelete("25121701");
        }

        private void LoadCombo()
        {
            string sSQL = "";
            DataTable dt = new DataTable();
            sSQL = string.Format("Exec SelectHangHoa_Nhahang @maQUAY={0}", cls_ConfigCashier.idCashier);
            dt = clsMain.ReturnDataTable(sSQL);
            txtID.Properties.DataSource = dt;
            txtID.EditValue = txtID.Properties.GetDataSourceValue(txtID.Properties.ValueMember, 0);
        }

        private void AddColumnDatatable()
        {
            //add column dt luoi
            dtLuoi.Columns.Add("MA_HANGHOA", typeof(Int32));
            dtLuoi.Columns.Add("TEN_HANGHOA", Type.GetType("System.String"));
            dtLuoi.Columns.Add("SOLUONG", Type.GetType("System.Double"));
            dtLuoi.Columns.Add("GIABAN", Type.GetType("System.Double"));
            dtLuoi.Columns.Add("GHICHU", Type.GetType("System.String"));
            dtLuoi.Columns.Add("MA_BEP", typeof(Int32));
            dtLuoi.Columns.Add("THUE", Type.GetType("System.Double"));
            dtLuoi.Columns.Add("Thanh_Tien", Type.GetType("System.Double"));
            dtLuoi.Columns.Add("TRANGTHAI", typeof(bool));
            dtLuoi.Columns.Add("IS_COMBO", typeof(bool));
            dtLuoi.Columns.Add("IS_INBEP", typeof(bool));
            dtLuoi.Columns.Add("IS_KHUYENMAI", typeof(bool));
            dtLuoi.Columns.Add("CHIETKHAU", Type.GetType("System.Double"));
            dtLuoi.Columns.Add("MONTHEM", typeof(bool));
            dtLuoi.Columns.Add("TEM", typeof(Int32));
            dtLuoi.Columns.Add("MACP", Type.GetType("System.String"));
            dtLuoi.Columns.Add("MACB", typeof(Int32));
            dtLuoi.Columns.Add("INTEM", typeof(bool));
            dtLuoi.Columns.Add("COMBO_CHINH", Type.GetType("System.String"));
            //add colum tach ban
            divide_Table1.Columns.Add("_MA_HANGHOA", typeof(Int32));
            divide_Table1.Columns.Add("_TEN_HANGHOA", Type.GetType("System.String"));
            divide_Table1.Columns.Add("_SOLUONG", Type.GetType("System.Double"));
            divide_Table1.Columns.Add("_GIABAN", Type.GetType("System.Double"));
            divide_Table1.Columns.Add("_MA_BEP", typeof(Int32));
            divide_Table1.Columns.Add("_Thanh_Tien", Type.GetType("System.Double"));
            divide_Table1.Columns.Add("_IS_COMBO", typeof(bool));
            divide_Table1.Columns.Add("_IS_INBEP", typeof(bool));
            divide_Table1.Columns.Add("_MONTHEM", typeof(bool));
            divide_Table1.Columns.Add("_TEM", typeof(Int32));
            divide_Table1.Columns.Add("_MACP", Type.GetType("System.String"));
            divide_Table1.Columns.Add("_MACB", typeof(Int32));
            divide_Table1.Columns.Add("_INTEM", typeof(bool));
            divide_Table1.Columns.Add("_COMBO_CHINH", Type.GetType("System.String"));
            divide_Table1.Columns.Add("_THUE", Type.GetType("System.Double"));
            divide_Table1.Columns.Add("_CHIETKHAU", Type.GetType("System.Double"));
            divide_Table1.Columns.Add("_TRANGTHAI", typeof(bool));
            divide_Table1.Columns.Add("_IS_XULY", typeof(bool));
            //add colum tach ban
            divide_Table2.Columns.Add("__MA_HANGHOA", typeof(Int32));
            divide_Table2.Columns.Add("__TEN_HANGHOA", Type.GetType("System.String"));
            divide_Table2.Columns.Add("__SOLUONG", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("__GIABAN", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("__MA_BEP", typeof(Int32));
            divide_Table2.Columns.Add("__Thanh_Tien", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("__IS_COMBO", typeof(bool));
            divide_Table2.Columns.Add("__IS_INBEP", typeof(bool));
            divide_Table2.Columns.Add("__MONTHEM", typeof(bool));
            divide_Table2.Columns.Add("__TEM", typeof(Int32));
            divide_Table2.Columns.Add("__MACP", Type.GetType("System.String"));
            divide_Table2.Columns.Add("__MACB", typeof(Int32));
            divide_Table2.Columns.Add("__INTEM", typeof(bool));
            divide_Table2.Columns.Add("__COMBO_CHINH", Type.GetType("System.String"));
            divide_Table2.Columns.Add("__THUE", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("__CHIETKHAU", Type.GetType("System.Double"));
            divide_Table2.Columns.Add("__TRANGTHAI", typeof(bool));
            divide_Table2.Columns.Add("__IS_XULY", typeof(bool));
        }

        private void SetBackgroudOnClick(SimpleButton _btn)
        {
            if (cls_KP_RES.iHinhanh == 1)
            {
                if (_btn.BackgroundImageLayout == ImageLayout.Stretch)
                {
                    return;
                }
            }
            _btn.Appearance.BackColor = Color.YellowGreen;
            _btn.Appearance.BackColor2 = Color.YellowGreen;
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private void SetDefautlBackgroud(SimpleButton _btn)
        {
            if (cls_KP_RES.iHinhanh == 1)
            {
                if (_btn.BackgroundImageLayout == ImageLayout.Stretch)
                {
                    return;
                }
            }
            _btn.Appearance.BackColor = Color.White;
            if (_btn.Appearance.Options.UseBackColor == true)
                _btn.Appearance.Options.UseBackColor = false;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == false)
                _btn.LookAndFeel.UseDefaultLookAndFeel = true;
        }

        private void GetKhuVuc()
        {
            try
            {
                DataTable dtKhuVuc = new DataTable();
                dtKhuVuc.Rows.Clear();
                string SQLKV = string.Format("Exec SelectKhuVuc @PageNumber={0},@PageSize={1}, @maCH={2}, @maQUAY={3}", cPageKV, 7, cls_ConfigCashier.idShop, cls_ConfigCashier.idCashier);
                dtKhuVuc = clsMain.ReturnDataTable(SQLKV);

                while (pnKV_Mon.Controls.Count > 0)
                {
                    pnKV_Mon.Controls[0].Dispose();
                }
                pnKV_Mon.Controls.Clear();
                int locateY = 2;
                int locateX = 3;
                for (int i = 0; i < dtKhuVuc.Rows.Count; i++)
                {
                    SimpleButton btnKV = new SimpleButton();
                    btnKV.Name = dtKhuVuc.Rows[i]["MA_KHUVUC"].ToString();
                    if (i == 0)
                    {
                        maKV = int.Parse(dtKhuVuc.Rows[0]["MA_KHUVUC"].ToString());
                        lbKhuVuc.Text = "" + dtKhuVuc.Rows[0]["TEN_KHUVUC"].ToString() + ">";
                        SetBackgroudOnClick(btnKV);
                    }
                    btnKV.Text = dtKhuVuc.Rows[i]["TEN_KHUVUC"].ToString();
                    try
                    {
                        byte[] tam = new byte[((byte[])dtKhuVuc.Rows[i]["HINHANH"]).LongLength];
                        tam = (byte[])dtKhuVuc.Rows[i]["HINHANH"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        if (cls_KP_RES.iHinhanh == 1)
                        {
                            btnKV.BackgroundImage = bm;
                            btnKV.BackgroundImageLayout = ImageLayout.Stretch;
                            btnKV.Appearance.BackColor = Color.Transparent;
                            btnKV.Appearance.Options.UseBackColor = true;
                            btnKV.ButtonStyle = BorderStyles.NoBorder;
                            btnKV.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                        }
                        else
                        {
                            btnKV.Image = bm;
                        }
                    }
                    catch
                    {
                    }
                    btnKV.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btnKV.ImageLocation = ImageLocation.TopCenter;
                    btnKV.RightToLeft = RightToLeft.No;
                    btnKV.Size = new Size(125, 80);
                    if (i != 0)
                    {
                        locateY += int.Parse(btnKV.Location.Y.ToString()) + 85;
                    }
                    btnKV.Location = new Point(locateX, locateY);
                    btnKV.Click += new EventHandler(btnKV_Click);
                    pnKV_Mon.Controls.Add(btnKV);
                }
            }
            catch
            {
            }
        }

        private void GetSoDoBan()
        {
            try
            {
                dtBan.Clear();
                string sql = string.Format("Exec SelectTable @PageNumber={0},@maKV={1}", cPageBan, maKV);
                dtBan = clsMain.ReturnDataTable(sql);

                while (pnTable_Mon.Controls.Count > 0)
                {
                    pnTable_Mon.Controls[0].Dispose();
                }
                pnTable_Mon.Controls.Clear();
                for (int i = 0; i < dtBan.Rows.Count; i++)
                {
                    SimpleButton btnBan_Mon = new SimpleButton();
                    btnBan_Mon.Name = dtBan.Rows[i]["MA_BAN"].ToString();
                    if (dtBan.Rows[i]["SOGHE"].ToString() != "0")
                    {
                        btnBan_Mon.Text = dtBan.Rows[i]["TEN_BAN"].ToString() + " - " + dtBan.Rows[i]["SOGHE"].ToString() + "  Ghế";
                    }
                    else
                    {
                        btnBan_Mon.Text = dtBan.Rows[i]["TEN_BAN"].ToString();
                    }
                    try
                    {
                        byte[] tam = new byte[((byte[])dtBan.Rows[i]["HINHANH"]).LongLength];
                        tam = (byte[])dtBan.Rows[i]["HINHANH"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        if (cls_KP_RES.iHinhanh == 1)
                        {
                            btnBan_Mon.BackgroundImage = bm;
                            btnBan_Mon.BackgroundImageLayout = ImageLayout.Stretch;
                            btnBan_Mon.Appearance.BackColor = Color.Transparent;
                            btnBan_Mon.Appearance.Options.UseBackColor = true;
                            btnBan_Mon.ButtonStyle = BorderStyles.NoBorder;
                            btnBan_Mon.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                        }
                        else
                        {
                            btnBan_Mon.Image = bm;
                        }
                    }
                    catch
                    {
                    }
                    btnBan_Mon.Width = int.Parse(dtBan.Rows[i]["Width"].ToString());
                    btnBan_Mon.Height = int.Parse(dtBan.Rows[i]["Height"].ToString());
                    btnBan_Mon.Location = new Point(int.Parse(dtBan.Rows[i]["LocaX"].ToString()), int.Parse(dtBan.Rows[i]["LocaY"].ToString()));

                    try
                    {
                        if (bool.Parse(dtBan.Rows[i]["ISBANDAT"].ToString()) == true)
                        {
                            SetBackgroudOnClick2(btnBan_Mon);
                        }
                    }
                    catch
                    {
                    }
                    try
                    {
                        if (bool.Parse(dtBan.Rows[i]["ISMO_BAN"].ToString()) == true)
                        {
                            SetBackgroudOnClick1(btnBan_Mon);
                            DataTable dataInPut = getGioVaoBan(btnBan_Mon.Name.ToString());
                            if (dataInPut.Rows.Count > 0)
                            {
                                btnBan_Mon.Text += "\n Giờ vào: " + "\n" + cls_KP_RES.DateToString_Full_NoSecond((DateTime)dataInPut.Rows[0][0]);
                            }
                            string idHD = GetMaHoaDon(btnBan_Mon.Name.ToString());
                            DataTable tb = DemSoBanTrongBill(idHD);
                            if (tb.Rows.Count > 1)
                            {
                                btnBan_Mon.Text += " \n GB " + idHD.Substring(idHD.IndexOf("B") + 1, 4);
                            }
                        }
                    }
                    catch
                    {
                    }
                    try
                    {
                        if (bool.Parse(dtBan.Rows[i]["SEE_PAYMENTS"].ToString()) == true)
                        {
                            SetBackgroudOnClick3(btnBan_Mon);
                        }
                    }
                    catch
                    {
                        dtBan.Rows[i]["SEE_PAYMENTS"] = 0;
                        clsMain.ExecuteSQL("update ban set SEE_PAYMENTS=0 where SEE_PAYMENTS is null");
                    }

                    btnBan_Mon.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btnBan_Mon.ImageLocation = ImageLocation.TopCenter;
                    btnBan_Mon.RightToLeft = RightToLeft.No;
                    btnBan_Mon.Click += new EventHandler(btnBan_Mon_Click);
                    pnTable_Mon.Controls.Add(btnBan_Mon);
                }
            }
            catch
            {
            }
        }

        private void SetBackgroudOnClick2(SimpleButton _btn)
        {
            if (cls_KP_RES.iHinhanh == 1)
            {
                if (_btn.BackgroundImageLayout == ImageLayout.Stretch)
                {
                    return;
                }
            }
            _btn.Appearance.BackColor = Color.FromArgb(0, 173, 239);
            _btn.Appearance.BackColor2 = Color.FromArgb(0, 173, 239);
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private void SetBackgroudOnClick3(SimpleButton _btn)
        {
            if (cls_KP_RES.iHinhanh == 1)
            {
                if (_btn.BackgroundImageLayout == ImageLayout.Stretch)
                {
                    return;
                }
            }
            _btn.Appearance.BackColor = Color.FromArgb(214, 223, 35);
            _btn.Appearance.BackColor2 = Color.FromArgb(214, 223, 35);
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;

        }

        private DataTable getGioVaoBan(string maBan)
        {
            string sSQL = "";
            sSQL += "Select GIOVAO" + "\n";
            sSQL += "From HOADON hd,CT_MOBAN ct" + "\n";
            sSQL += "Where hd.MA_HOADON = ct.MA_HOADON And hd.ISPAYMENT = 0 And hd.ISBANDAT=0" + "\n";
            sSQL += "And ct.MA_BAN = " + clsMain.SQLString(maBan);
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                return dt;
            }
            else
            {
                sSQL = "";
                sSQL += "Select GIOVAO" + "\n";
                sSQL += "From HOADON hd,CT_MOBAN ct" + "\n";
                sSQL += "Where hd.MA_HOADON = ct.MA_HOADON And hd.ISPAYMENT = 0 And hd.ISBANDAT=1" + "\n";
                sSQL += "And ct.MA_BAN = " + clsMain.SQLString(maBan) + "\n";
                sSQL += "Order by GIOVAO ASC";
                dt = clsMain.ReturnDataTable(sSQL);
                return dt;
            }
        }

        private void SetBackgroudOnClick1(SimpleButton _btn)
        {
            if (cls_KP_RES.iHinhanh == 1)
            {
                if (_btn.BackgroundImageLayout == ImageLayout.Stretch)
                {
                    return;
                }
            }
            _btn.Appearance.BackColor = Color.FromArgb(229, 137, 45);
            _btn.Appearance.BackColor2 = Color.FromArgb(229, 137, 45);
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private string GetMaHoaDon(string maBan)
        {
            string sSQL = "";
            sSQL += "Select a.MA_HOADON " + "\n";
            sSQL += "From CT_MOBAN as a,HOADON as b,BAN c" + "\n";
            sSQL += "Where a.MA_HOADON = b.MA_HOADON And a.MA_BAN=c.MA_BAN And b.ISBANDAT=0" + "\n";
            sSQL += "And ISMO_BAN=1 And b.ISPAYMENT = 0 And a.MA_BAN = " + clsMain.SQLString(maBan) + "\n";
            sSQL += "Order by MA_HOADON desc";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0]["MA_HOADON"].ToString();
            }
            else
            {
                sSQL = "";
                sSQL += "Select a.MA_HOADON " + "\n";
                sSQL += "From CT_MOBAN as a,HOADON as b,BAN c" + "\n";
                sSQL += "Where a.MA_HOADON = b.MA_HOADON And a.MA_BAN=c.MA_BAN And b.ISBANDAT=1" + "\n";
                sSQL += "And ISMO_BAN=1 And b.ISPAYMENT = 0 And a.MA_BAN = " + clsMain.SQLString(maBan) + "\n";
                sSQL += "Order by GIOVAO ASC";
                dt = clsMain.ReturnDataTable(sSQL);
                if (dt.Rows.Count > 0)
                {
                    return dt.Rows[0]["MA_HOADON"].ToString();
                }
                else
                {
                    return "";
                }
            }
        }

        private DataTable DemSoBanTrongBill(string maBill)
        {
            DataTable dttabless;
            string sql = "select MA_BAN from CT_MOBAN where MA_HOADON =" + clsMain.SQLString(maBill);
            dttabless = clsMain.ReturnDataTable(sql);
            return dttabless;
        }

        private void btn_UpKV_Click(object sender, EventArgs e)
        {
            if (funtions == 1 || funtions == 11 || funtions == 6)
            {
                if (cPageKV > 1)
                {
                    cPageKV -= 1;
                    GetKhuVuc();
                    GetSoDoBan();
                }
            }
            else if (funtions == 2)
            {
                if (cPageNH > 1)
                {
                    cPageNH -= 1;
                    GetNhomhang();
                    GetListMon();
                }
            }
        }

        private void btn_DowKV_Click(object sender, EventArgs e)
        {
            if (funtions == 1 || funtions == 11 || funtions == 6)
            {
                if (cPageKV < 20)
                {
                    cPageKV += 1;
                    GetKhuVuc();
                    GetSoDoBan();
                }
            }
            else if (funtions == 2)
            {
                if (cPageNH < 20)
                {
                    cPageNH += 1;
                    GetNhomhang();
                    GetListMon();
                }
            }
        }

        private void btn_NextToRight_Click(object sender, EventArgs e)
        {
            if (funtions == 1 || funtions == 5 || funtions == 6 || funtions == 11)
            {
                if (pnTable_Mon.HorizontalScroll.Visible)
                {
                    try
                    {
                        pnTable_Mon.AutoScrollPosition = new Point(pnTable_Mon.HorizontalScroll.Value + pnTable_Mon.HorizontalScroll.Maximum / 10, 0);
                        return;
                    }
                    catch
                    {
                        pnTable_Mon.HorizontalScroll.Value = pnTable_Mon.HorizontalScroll.Maximum;
                        return;
                    }
                }
                if (cPageBan == 5)
                    return;
                cPageBan = cPageBan + 1;
                lbPageTable.Text = cPageBan.ToString();
                GetSoDoBan();
            }
            else if (funtions == 2)
            {
                if (cPageMon == 20)
                    return;
                cPageMon = cPageMon + 1;
                lbPageTable.Text = cPageMon.ToString();
                GetListMon();
            }
        }

        private void btn_NextToLeft_Click(object sender, EventArgs e)
        {

            if (funtions == 1 || funtions == 5 || funtions == 6 || funtions == 11)
            {
                if (pnTable_Mon.HorizontalScroll.Visible)
                {
                    try
                    {
                        pnTable_Mon.AutoScrollPosition = new Point(pnTable_Mon.HorizontalScroll.Value - pnTable_Mon.HorizontalScroll.Maximum / 10, 0);
                        return;
                    }
                    catch
                    {
                        pnTable_Mon.HorizontalScroll.Value = pnTable_Mon.HorizontalScroll.Minimum;
                        return;
                    }
                }
                if (cPageBan == 1)
                    return;
                cPageBan = cPageBan - 1;
                lbPageTable.Text = cPageBan.ToString();
                GetSoDoBan();
            }
            else if (funtions == 2)
            {
                if (cPageMon == 1)
                    return;
                cPageMon = cPageMon - 1;
                lbPageTable.Text = cPageMon.ToString();
                GetListMon();
            }
        }

        private void GetNhomhang()
        {
            try
            {
                DataTable dtNhomhang = new DataTable();
                dtNhomhang.Rows.Clear();
                string SQLMON = string.Format("exec SelectDish @PageNumber={0},@PageSize={1},@maQUAY={2}", cPageNH, 7, int.Parse(cls_ConfigCashier.idCashier));
                dtNhomhang = clsMain.ReturnDataTable(SQLMON);

                while (pnKV_Mon.Controls.Count > 0)
                {
                    pnKV_Mon.Controls[0].Dispose();
                }
                pnKV_Mon.Controls.Clear();
                int locateY = 2;
                int locateX = 3;
                for (int i = 0; i < dtNhomhang.Rows.Count; i++)
                {
                    SimpleButton btnKV = new SimpleButton();
                    btnKV.Name = dtNhomhang.Rows[i]["MA_NHOMHANG"].ToString();
                    if (i == 0)
                    {
                        maNH = int.Parse(dtNhomhang.Rows[0]["MA_NHOMHANG"].ToString());
                        SetBackgroudOnClick(btnKV);
                    }
                    btnKV.Text = "" + dtNhomhang.Rows[i]["TEN_NHOMHANG"].ToString();
                    try
                    {
                        byte[] tam = new byte[((byte[])dtNhomhang.Rows[i]["HINHANH"]).LongLength];
                        tam = (byte[])dtNhomhang.Rows[i]["HINHANH"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        if (cls_KP_RES.iHinhanh == 1)
                        {
                            btnKV.BackgroundImage = bm;
                            btnKV.BackgroundImageLayout = ImageLayout.Stretch;
                            btnKV.Appearance.BackColor = Color.Transparent;
                            btnKV.Appearance.Options.UseBackColor = true;
                            btnKV.ButtonStyle = BorderStyles.NoBorder;
                            btnKV.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                        }
                        else
                        {
                            btnKV.Image = bm;
                        }
                    }
                    catch
                    {
                    }
                    btnKV.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btnKV.ImageLocation = ImageLocation.TopCenter;
                    btnKV.RightToLeft = RightToLeft.No;
                    btnKV.Size = new Size(125, 80);
                    if (i != 0)
                    {
                        locateY += int.Parse(btnKV.Location.Y.ToString()) + 85;
                    }
                    btnKV.Location = new Point(locateX, locateY);
                    btnKV.Click += new EventHandler(btnKV_Click);
                    pnKV_Mon.Controls.Add(btnKV);
                }
            }
            catch
            {
            }
        }

        private void GetListMon()
        {
            DataTable dtMon = new DataTable();
            dtMon.Clear();
            int width = (pnTable_Mon.Width / 110);
            int height = (pnTable_Mon.Height / 80);
            int sizeMon = width * height;
            string sql = string.Format("Exec SelectMON @PageNumber={0},@PageSize={1}, @maNH={2}, @maKV={3},@maCH={4}", cPageMon, sizeMon, maNH, maKV, cls_ConfigCashier.idShop);
            dtMon = clsMain.ReturnDataTable(sql);

            while (pnTable_Mon.Controls.Count > 0)
            {
                pnTable_Mon.Controls[0].Dispose();
            }
            pnTable_Mon.Controls.Clear();
            int count = (pnTable_Mon.Width / 120) + 1;
            int line = 0;
            int locateY = 2;
            int locateX = 2;
            int x = 0;
            int y = 0;
            for (int i = 0; i < dtMon.Rows.Count; i++)
            {
                line++;
                SimpleButton btnBan_Mon = new SimpleButton();
                btnBan_Mon.Name = dtMon.Rows[i]["MA_HANGHOA"].ToString();
                btnBan_Mon.Text = dtMon.Rows[i]["TEN_HANGHOA"].ToString() + "\n" + "Giá:" + string.Format("{0:0,0 }", double.Parse(dtMon.Rows[i]["GIABAN"].ToString())) + "";
                btnBan_Mon.Tag = dtMon.Rows[i]["TEN_HANGHOA"].ToString() + " - " + "Giá:" + string.Format("{0:0,0 }", double.Parse(dtMon.Rows[i]["GIABAN"].ToString())) + "";
                try
                {
                    byte[] tam = new byte[((byte[])dtMon.Rows[i]["HINHANH"]).LongLength];
                    tam = (byte[])dtMon.Rows[i]["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    if (cls_KP_RES.iHinhanh == 1)
                    {
                        btnBan_Mon.BackgroundImage = bm;
                        btnBan_Mon.BackgroundImageLayout = ImageLayout.Stretch;
                        btnBan_Mon.Appearance.BackColor = Color.Transparent;
                        btnBan_Mon.Appearance.Options.UseBackColor = true;
                        btnBan_Mon.ButtonStyle = BorderStyles.NoBorder;
                        btnBan_Mon.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                    }
                    else
                    {
                        btnBan_Mon.Image = bm;
                    }

                }
                catch
                {
                }
                btnBan_Mon.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btnBan_Mon.ImageLocation = ImageLocation.TopCenter;
                btnBan_Mon.RightToLeft = RightToLeft.No;
                if (cls_KP_RES.iHinhanh == 1)
                {
                    btnBan_Mon.Size = new Size(168, 130);
                    if (this.Width < 1100)
                    {
                        if (line == 4)
                        {
                            locateY += int.Parse(btnBan_Mon.Location.Y.ToString()) + 133;
                            locateX = 2;
                            line = 1;
                        }
                        else
                        {
                            if (i != 0)
                            {
                                locateX += int.Parse(btnBan_Mon.Location.X.ToString()) + 171;
                            }
                        }
                    }
                    else
                    {
                        if (line == 6)
                        {
                            locateY += int.Parse(btnBan_Mon.Location.Y.ToString()) + 133;
                            locateX = 2;
                            line = 1;
                        }
                        else
                        {
                            if (i != 0)
                            {
                                locateX += int.Parse(btnBan_Mon.Location.X.ToString()) + 171;
                            }
                        }
                    }
                }
                else
                {
                    btnBan_Mon.Size = new Size(110, 80);
                    if (line == count)
                    {
                        locateY += int.Parse(btnBan_Mon.Location.Y.ToString()) + 85;
                        locateX = 2;
                        line = 1;
                    }
                    else
                    {
                        if (i != 0)
                        {
                            if (this.Width < 1100)
                            {
                                locateX += int.Parse(btnBan_Mon.Location.X.ToString()) + 132;
                            }
                            else
                            {
                                locateX += int.Parse(btnBan_Mon.Location.X.ToString()) + 124;
                            }
                        }
                    }
                }
                btnBan_Mon.Location = new Point(locateX, locateY);
                btnBan_Mon.Click += new EventHandler(btnBan_Mon_Click);
                pnTable_Mon.Controls.Add(btnBan_Mon);
            }
        }

        private void btnKV_Click(object sender, EventArgs e)
        {
            try
            {
                cPageBan = 1;
                cPageMon = 1;
                lbPageTable.Text = cPageBan.ToString();
                btnComboOption.Visible = false;
                foreach (SimpleButton btnKV in pnKV_Mon.Controls)
                {
                    if (btnKV.Name == this.ActiveControl.Name)
                    {
                        SetBackgroudOnClick(btnKV);
                        if (funtions == 1)
                        {
                            lbKhuVuc.Text = btnKV.Text + "  >  ";
                            lbMon.Text = "";
                            lbBan.Text = "";
                            maBan = "";
                            maKV = int.Parse(btnKV.Name.ToString());
                            GetSoDoBan();
                        }
                        else if (funtions == 2)
                        {
                            maNH = int.Parse(btnKV.Name.ToString());
                            GetListMon();
                        }
                        else if (funtions == 6)
                        {
                            funtions = 1;
                            lbKhuVuc.Text = btnKV.Text + "  >  ";
                            lbMon.Text = "";
                            lbBan.Text = "";
                            maKV = int.Parse(btnKV.Name.ToString());
                            GetSoDoBan();
                            funtions = 6;
                        }
                        else if (funtions == 11)
                        {
                            funtions = 1;
                            lbKhuVuc.Text = btnKV.Text + "  >  ";
                            lbMon.Text = "";
                            lbBan.Text = "";
                            maKV = int.Parse(btnKV.Name.ToString());
                            GetSoDoBan();
                            funtions = 11;
                        }
                        else if (selectDishes == 2)
                        {
                            funtions = 1;
                            lbKhuVuc.Text = btnKV.Text + "  >  ";
                            lbMon.Text = "";
                            lbBan.Text = "";
                            maBan = "";
                            maKV = int.Parse(btnKV.Name.ToString());
                            GetSoDoBan();
                            funtions = 9;
                        }
                        else if (selectDishes == 1)
                        {
                            funtions = 2;
                            maNH = int.Parse(btnKV.Name.ToString());
                            GetListMon();
                            funtions = 9;
                        }
                    }
                    else
                    {
                        SetDefautlBackgroud(btnKV);
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK);
            }
        }
        //Funtion 1 Sơ đồ bàn, Funtion 2 Mở bàn, 3 yêu cầu thêm, 4 send Order,5 Gép bàn , 6 Tách bàn, 7 Payments, 8 buy id Product, 9 Đặt bàn, 10 ket ca,11 Chuyển bàn.
        private void btnBan_Mon_Click(object sender, EventArgs e)
        {
            try
            {
                // 9 Đặt bàn
                if (funtions == 9)
                {
                    if (selectDishes == 0)
                    {
                        return;
                    }
                    foreach (SimpleButton btnBan in pnTable_Mon.Controls)
                    {
                        if (btnBan.Name == this.ActiveControl.Name)
                        {
                            if (selectDishes == 1)
                            {
                                sendOrder = 1;
                                maMon = btnBan.Name;
                                if (selectTable == 1)
                                {
                                    selectTable = 0;
                                    dtLuoi.Rows.Clear();
                                    gr_SelectProduct.RefreshDataSource();
                                }
                                GetDishGoToBuy(maMon, 0);
                            }
                            else
                            {
                                if (selectDishes == 2)
                                {
                                    maBan = btnBan.Name;
                                    if (CheckOpenTable(maBan) == false)
                                    {
                                        if (CheckHaveISBANDAT(int.Parse(maBan)) == true)// research find table check
                                        {
                                            if (SelectTablesCheckt(maBan) == true)// kiểm tra time of this table have ok or no 
                                            {
                                                SelectCheckTable(maBan); // kiểm tra bàn này đã chọn đặt chưa ?
                                                SetBackgroudOnClick(btnBan);
                                            }
                                            else
                                            {
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            SelectCheckTable(maBan);
                                            SetBackgroudOnClick(btnBan);
                                        }
                                    }
                                    else
                                    {
                                        if (XacNhanBanDat(maBan) == true)
                                        {
                                            SelectCheckTable(maBan);
                                            SetBackgroudOnClick(btnBan);
                                        }
                                        else
                                        {
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    us_keyboard2.SetTextbox(txtDBTenKH);
                    txtTenKhacHang.Focus();
                    return;
                }

                btnComboOption.Visible = false;
                pnBtnDeleteGv.Visible = false;
                foreach (SimpleButton btnBan_Mon in pnTable_Mon.Controls)
                {
                    if (btnBan_Mon.Name == this.ActiveControl.Name)
                    {
                        SetBackgroudOnClick(btnBan_Mon);
                        //1 Sơ đồ bàn
                        if (funtions == 1)
                        {
                            SetBackgroudOnClick(btn_ThuNgan);
                            SetDefautlBackgroud(btn_NghiepVu);
                            SetDefautlBackgroud(btn_DatThu);
                            pnThuNgan.BringToFront();
                            sendOrder = 0;
                            maBan = btnBan_Mon.Name;
                            if (btnBan_Mon.Text.IndexOf("\n") > 0)
                            {
                                tenBan = btnBan_Mon.Text.Substring(0, btnBan_Mon.Text.IndexOf("\n"));
                                lbBan.Text = btnBan_Mon.Text.Substring(0, btnBan_Mon.Text.IndexOf("\n"));
                            }
                            else
                            {
                                tenBan = btnBan_Mon.Text;
                                lbBan.Text = btnBan_Mon.Text;
                            }
                            lbMon.Text = "";
                            lbTotal.Text = "0";
                            if (CheckOpenTable(maBan) == true)
                            {
                                selectTable = 1;
                                lbTotal.Text = "0";
                                string maHOADON = GetMaHoaDon(maBan);
                                GetDishOfTable(maHOADON);
                                sTEM = LayTemLonNhat() + 1;
                                sMaCB = LayMACBLonNhat() + 1;

                            }
                            else
                            {
                                dtLuoi.Rows.Clear();
                                gr_SelectProduct.DataSource = dtLuoi;
                                sTEM = 1;
                                sMaCB = 1;
                            }
                        }
                        //2 Mở bàn
                        else if (funtions == 2)
                        {
                            sendOrder = 1;
                            if (selectTable == 1)
                            {
                                selectTable = 0;
                                dtLuoi.Rows.Clear();
                                gr_SelectProduct.DataSource = dtLuoi;
                            }
                            maMon = btnBan_Mon.Name;
                            lbMon.Text = " >   " + btnBan_Mon.Tag;
                            GetDishGoToBuy(maMon, maKV);
                        }
                        //5 Ghép bàn
                        else if (funtions == 5)
                        {
                            maBanGhep = btnBan_Mon.Name;
                            if (maBanGhep == maBan)
                            {
                                return;
                            }
                            string idHD = GetMaHoaDon(maBan);
                            string idHD_BanGhep = GetMaHoaDon(maBanGhep);
                            if (idHD == idHD_BanGhep)
                            {
                                return;
                            }
                            if (idHD_BanGhep == "")
                            {
                                string sqlBAN = "Update BAN SET ISMO_BAN=1 where MA_BAN =" + int.Parse(maBanGhep);
                                clsMain.ExecuteSQL(sqlBAN);
                                string sqlCTMOBAN = "Insert into CT_MOBAN(MA_HOADON, MA_BAN)";
                                sqlCTMOBAN += " values(" + "'" + idHD + "'," + int.Parse(maBanGhep) + ")";
                                clsMain.ExecuteSQL(sqlCTMOBAN);
                            }
                            else
                            {
                                string sqlSelect = "";
                                string sSQL = "";
                                int ssMaCB = 0;
                                bool KTCB = false;
                                int sMaCB_TiepTheo = 0;
                                int sMaCB_HienTai = 0;
                                string sMaHH = "";
                                sqlSelect = "Select cthd.MA_HANGHOA, hh.TEN_HANGHOA, cthd.SOLUONG, cthd.GIABAN, cthd.THUE, Isnull(cthd.TRANGTHAI,0) as TRANGTHAI, Isnull(cthd.Is_Combo,0) as Is_Combo,cthd.CHIETKHAU,cthd.TEM,cthd.MACP,cthd.Number,cthd.MACB " + "\n";
                                sqlSelect += "From CT_HOADON as cthd inner join HANGHOA as hh on cthd.MA_HANGHOA=hh.MA_HANGHOA where cthd.MA_HOADON ='" + idHD_BanGhep + "'";
                                DataTable disheJoinTable = clsMain.ReturnDataTable(sqlSelect);

                                sqlSelect = "Select cthd.MA_HANGHOA, hh.TEN_HANGHOA, cthd.SOLUONG, cthd.GIABAN, cthd.THUE, Isnull(cthd.TRANGTHAI,0) as TRANGTHAI, Isnull(cthd.Is_Combo,0) as Is_Combo,cthd.CHIETKHAU,cthd.TEM,cthd.MACP,cthd.Number,cthd.MACB " + "\n";
                                sqlSelect += "From CT_HOADON as cthd inner join HANGHOA as hh on cthd.MA_HANGHOA=hh.MA_HANGHOA where cthd.MA_HOADON ='" + idHD + "'";
                                DataTable disheTable = clsMain.ReturnDataTable(sqlSelect);

                                //cập nhật MACB disheJoinTable
                                if (disheTable.Rows.Count > 0)
                                    sMaCB_TiepTheo = int.Parse(disheTable.Rows[disheTable.Rows.Count - 1]["MACB"].ToString()) + 1;
                                if (disheJoinTable.Rows.Count > 0)
                                {
                                    sMaCB_HienTai = int.Parse(disheJoinTable.Rows[0]["MACB"].ToString());
                                    foreach (DataRow dr in disheJoinTable.Rows)
                                    {
                                        if (int.Parse(dr["MACB"].ToString()) == sMaCB_HienTai)
                                        {
                                            dr["MACB"] = sMaCB_TiepTheo;
                                        }
                                        else
                                        {
                                            sMaCB_HienTai = int.Parse(dr["MACB"].ToString());
                                            sMaCB_TiepTheo += 1;
                                            dr["MACB"] = sMaCB_TiepTheo;
                                        }
                                    }
                                }
                                if (disheJoinTable.Rows.Count > 0)
                                {
                                    for (int i = 0; i < disheJoinTable.Rows.Count; i++)
                                    {
                                        
                                        //món phải là combo?
                                        DataTable dtCB = GetProductInCombo(disheJoinTable.Rows[i]["MA_HANGHOA"].ToString());
                                        if (dtCB.Rows.Count > 0)
                                        {
                                            ssMaCB = int.Parse(disheJoinTable.Rows[i]["MACB"].ToString());
                                            //combo có món thêm?
                                            KTCB = KT_COMBO_OPTION_MONTHEM(disheJoinTable.Rows[i]["MA_HANGHOA"].ToString());
                                            sMaHH = disheJoinTable.Rows[i]["MA_HANGHOA"].ToString();
                                        }
                                        
                                        //có combo
                                        if (dtCB.Rows.Count > 0 || dtCB.Rows.Count == 0 & int.Parse(disheJoinTable.Rows[i]["MACB"].ToString()) == ssMaCB)
                                        {
                                            //món đã có trên bàn?
                                            bool KTHH_In_Table = ChecBishInTable(idHD, disheJoinTable.Rows[i]["MA_HANGHOA"].ToString(), bool.Parse(disheJoinTable.Rows[i]["Is_Combo"].ToString()), sMaHH);
                                            //combo có món thêm
                                            if (KTCB == true)
                                            {
                                                sSQL = "Insert into CT_HOADON(MA_HOADON,MA_HANGHOA,SOLUONG,GIABAN,GIABAN_THAT,THUE,TRANGTHAI,IS_COMBO,COMBO_CHINH,CHIETKHAU,TEM,MACP,Number,MACB)";
                                                sSQL += "Values (";
                                                sSQL += clsMain.SQLString(idHD) + ",";
                                                sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["MA_HANGHOA"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["SOLUONG"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["GIABAN"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["GIABAN"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["THUE"].ToString()) + ",";
                                                sSQL += clsMain.SQLBit(bool.Parse(disheJoinTable.Rows[i]["TRANGTHAI"].ToString())) + ",";
                                                sSQL += clsMain.SQLBit(bool.Parse(disheJoinTable.Rows[i]["Is_Combo"].ToString())) + ",";
                                                sSQL += clsMain.SQLString(sMaHH) + ",";
                                                sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["CHIETKHAU"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["TEM"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["MACP"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheTable.Rows[0]["Number"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["MACB"].ToString()) + ")";
                                                clsMain.ExecuteSQL(sSQL);
                                            }
                                            //combo ko có món thêm
                                            else
                                            {
                                                //thêm dòng mới
                                                if (KTHH_In_Table == false)
                                                {
                                                    sSQL = "Insert into CT_HOADON(MA_HOADON,MA_HANGHOA,SOLUONG,GIABAN,GIABAN_THAT,THUE,TRANGTHAI,IS_COMBO,COMBO_CHINH,CHIETKHAU,TEM,MACP,Number,MACB)";
                                                    sSQL += "Values (";
                                                    sSQL += clsMain.SQLString(idHD) + ",";
                                                    sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["MA_HANGHOA"].ToString()) + ",";
                                                    sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["SOLUONG"].ToString()) + ",";
                                                    sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["GIABAN"].ToString()) + ",";
                                                    sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["GIABAN"].ToString()) + ",";
                                                    sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["THUE"].ToString()) + ",";
                                                    sSQL += clsMain.SQLBit(bool.Parse(disheJoinTable.Rows[i]["TRANGTHAI"].ToString())) + ",";
                                                    sSQL += clsMain.SQLBit(bool.Parse(disheJoinTable.Rows[i]["Is_Combo"].ToString())) + ",";
                                                    sSQL += clsMain.SQLString(sMaHH) + ",";
                                                    sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["CHIETKHAU"].ToString()) + ",";
                                                    sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["TEM"].ToString()) + ",";
                                                    sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["MACP"].ToString()) + ",";
                                                    sSQL += clsMain.SQLString(disheTable.Rows[0]["Number"].ToString()) + ",";
                                                    sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["MACB"].ToString()) + ")";
                                                    clsMain.ExecuteSQL(sSQL);
                                                }
                                                //cập nhật số lượng
                                                else
                                                {
                                                    sSQL = "UPDATE CT_HOADON SET " + "\n";
                                                    sSQL += "SOLUONG +=" + clsMain.SQLString(disheJoinTable.Rows[i]["SOLUONG"].ToString()) + "," + "\n";
                                                    sSQL += "CHIETKHAU =" + clsMain.SQLString(disheJoinTable.Rows[i]["CHIETKHAU"].ToString()) + "\n";
                                                    sSQL += "WHERE MA_HOADON =" + clsMain.SQLString(idHD) + "\n";
                                                    sSQL += "AND Isnull(Is_Combo,0)=" + clsMain.SQLBit(bool.Parse(disheJoinTable.Rows[i]["Is_Combo"].ToString())) + "\n";
                                                    sSQL += "AND MA_HANGHOA =" + clsMain.SQLString(disheJoinTable.Rows[i]["MA_HANGHOA"].ToString()) + "\n";
                                                    if (dtCB.Rows.Count > 0)
                                                        sSQL += "AND MACP =" + clsMain.SQLString("CB") + "\n";
                                                    else
                                                        sSQL += "AND MACP =" + clsMain.SQLString("CBC") + "\n";
                                                    sSQL += "AND COMBO_CHINH =" + clsMain.SQLString(sMaHH) + "\n";
                                                    clsMain.ExecuteSQL(sSQL);
                                                }
                                            }
                                        }

                                        //ko có combo
                                        else
                                        {
                                            sMaHH = "";
                                            //món đã có trên bàn?
                                            bool KTHH_In_Table = ChecBishInTable(idHD, disheJoinTable.Rows[i]["MA_HANGHOA"].ToString(), bool.Parse(disheJoinTable.Rows[i]["Is_Combo"].ToString()), sMaHH);
                                            //thêm dòng mới
                                            if ((KTHH_In_Table == false)
                                            || (KTHH_In_Table == true & int.Parse(disheJoinTable.Rows[i]["TEM"].ToString()) != 0 & disheJoinTable.Rows[i]["MACP"].ToString() == "C")
                                            || (KTHH_In_Table == true & int.Parse(disheJoinTable.Rows[i]["TEM"].ToString()) != 0 & disheJoinTable.Rows[i]["MACP"].ToString() == "P"))
                                            {
                                                sSQL = "Insert into CT_HOADON(MA_HOADON,MA_HANGHOA,SOLUONG,GIABAN,GIABAN_THAT,THUE,TRANGTHAI,IS_COMBO,COMBO_CHINH,CHIETKHAU,TEM,MACP,Number,MACB)";
                                                sSQL += "Values (";
                                                sSQL += clsMain.SQLString(idHD) + ",";
                                                sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["MA_HANGHOA"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["SOLUONG"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["GIABAN"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["GIABAN"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["THUE"].ToString()) + ",";
                                                sSQL += clsMain.SQLBit(bool.Parse(disheJoinTable.Rows[i]["TRANGTHAI"].ToString())) + ",";
                                                sSQL += clsMain.SQLBit(bool.Parse(disheJoinTable.Rows[i]["Is_Combo"].ToString())) + ",";
                                                sSQL += clsMain.SQLString(sMaHH) + ",";
                                                sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["CHIETKHAU"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["TEM"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["MACP"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheTable.Rows[0].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheJoinTable.Rows[i]["MACB"].ToString()) + ")";
                                                clsMain.ExecuteSQL(sSQL);
                                            }
                                            //cập nhật số lượng
                                            else
                                            {
                                                sSQL = "UPDATE CT_HOADON SET " + "\n";
                                                sSQL += "SOLUONG +=" + clsMain.SQLString(disheJoinTable.Rows[i]["SOLUONG"].ToString()) + "," + "\n";
                                                sSQL += "CHIETKHAU =" + clsMain.SQLString(disheJoinTable.Rows[i]["CHIETKHAU"].ToString()) + "\n";
                                                sSQL += "WHERE MA_HOADON =" + clsMain.SQLString(idHD) + "\n";
                                                sSQL += "AND Isnull(Is_Combo,0)=" + clsMain.SQLBit(bool.Parse(disheJoinTable.Rows[i]["Is_Combo"].ToString())) + "\n";
                                                sSQL += "AND MA_HANGHOA =" + clsMain.SQLString(disheJoinTable.Rows[i]["MA_HANGHOA"].ToString()) + "\n";
                                                sSQL += "AND MACP =" + clsMain.SQLString("C");
                                                clsMain.ExecuteSQL(sSQL);
                                            }
                                        }
                                    }
                                }
                                string sqlCT_MOBAN = "update CT_MOBAN set MA_HOADON ='" + idHD + "' where MA_HOADON ='" + idHD_BanGhep + "'";
                                clsMain.ExecuteSQL(sqlCT_MOBAN);
                                string deleteCT = "Delete FROM CT_HOADON  where MA_HOADON ='" + idHD_BanGhep + "'";
                                clsMain.ExecuteSQL(deleteCT);
                                string deleteBill = "Delete FROM HOADON  where MA_HOADON ='" + idHD_BanGhep + "'";
                                clsMain.ExecuteSQL(deleteBill);
                            }
                            ExitDivideTable();
                        }
                        //6 Tách bàn
                        else if (funtions == 6)
                        {
                            maBanGhep = btnBan_Mon.Name;
                            if (maBanGhep == maBan)
                            {
                                return;
                            }
                            txtmaKVBanGhep.Text = lbKhuVuc.Text;
                            txtmaBanGhep.Text = btnBan_Mon.Text;
                            if (dtLuoi.Rows.Count > 0 && maBanGhep != "")
                            {
                                List<DM_ListDisheTableOne> lisHangHoa = new List<DM_ListDisheTableOne>();
                                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                                {
                                    DM_ListDisheTableOne dmHangHoa = new DM_ListDisheTableOne();
                                    dmHangHoa._MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                                    dmHangHoa._TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                                    dmHangHoa._SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                                    dmHangHoa._GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                                    dmHangHoa._Thanh_Tien = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"]).ToString());
                                    dmHangHoa._GHICHU = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"]).ToString();
                                    dmHangHoa._IS_COMBO = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["IS_COMBO"]).ToString());
                                    dmHangHoa._MA_BEP = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_BEP"]).ToString());
                                    dmHangHoa._MONTHEM = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MONTHEM"]).ToString());
                                    dmHangHoa._IS_INBEP = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["IS_INBEP"]).ToString());
                                    dmHangHoa._TEM = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEM"]).ToString());
                                    dmHangHoa._MACP = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MACP"]).ToString();
                                    dmHangHoa._MACB = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MACB"]).ToString());
                                    dmHangHoa._INTEM = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["INTEM"]).ToString());
                                    dmHangHoa._COMBO_CHINH = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["COMBO_CHINH"]).ToString();
                                    dmHangHoa._THUE = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["THUE"]).ToString());
                                    dmHangHoa._CHIETKHAU = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["CHIETKHAU"]).ToString());
                                    dmHangHoa._TRANGTHAI = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TRANGTHAI"]).ToString());
                                    dmHangHoa._IS_XULY = false;
                                    lisHangHoa.Add(dmHangHoa);
                                }
                                convert cvrt = new convert();
                                divide_Table1 = cvrt.ConvertToDataTable(lisHangHoa);
                                gr_divide_Table_1.DataSource = divide_Table1;
                                gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.RowCount - 1;

                                pnInforTablecheck.Visible = true;
                                pnTachban.BringToFront();
                                int w = (panelControl14.Parent.Width - 110) / 2;
                                panelControl14.Width = w;
                                pn_TableShare.Width = w;
                                panelControl15.Width = w;
                                pn_TableTow.Width = w;
                            }
                        }
                        //11 Chuyển bàn
                        else if (funtions == 11)
                        {
                            maBanGhep = btnBan_Mon.Name;
                            if (maBanGhep == maBan)
                            {
                                return;
                            }
                            txtmaKVBanGhep.Text = lbKhuVuc.Text;
                            txtmaBanGhep.Text = btnBan_Mon.Text;
                            string sLocal_maBan = maBan;
                            string sLocal_maBanGhep = maBanGhep;

                            string sSQL = "";
                            int ssMaCB = 0;
                            bool KTCB = false;
                            int sMaCB_TiepTheo = 0;
                            int sMaCB_HienTai = 0;
                            string sMaHH = "";
                            if (CheckOpenTable(maBanGhep))
                            {
                                string maHD = GetMaHoaDon(maBan);
                                string maHD_banghep = GetMaHoaDon(maBanGhep);
                                sSQL = "";
                                sSQL = "Select cthd.MA_HANGHOA, hh.TEN_HANGHOA, cthd.SOLUONG, cthd.GIABAN, cthd.THUE, Isnull(cthd.TRANGTHAI,0) as TRANGTHAI, Isnull(cthd.Is_Combo,0) as Is_Combo,cthd.CHIETKHAU,cthd.TEM,cthd.MACP,cthd.Number,cthd.MACB " + "\n";
                                sSQL += "From CT_HOADON as cthd inner join HANGHOA as hh on cthd.MA_HANGHOA=hh.MA_HANGHOA where cthd.MA_HOADON ='" + maHD_banghep + "'";
                                DataTable disheJoinTable = clsMain.ReturnDataTable(sSQL);

                                sSQL = "Select cthd.MA_HANGHOA, hh.TEN_HANGHOA, cthd.SOLUONG, cthd.GIABAN, cthd.THUE, Isnull(cthd.TRANGTHAI,0) as TRANGTHAI, Isnull(cthd.Is_Combo,0) as Is_Combo,cthd.CHIETKHAU,cthd.TEM,cthd.MACP,cthd.Number,cthd.MACB " + "\n";
                                sSQL += "From CT_HOADON as cthd inner join HANGHOA as hh on cthd.MA_HANGHOA=hh.MA_HANGHOA where cthd.MA_HOADON ='" + maHD + "'";
                                DataTable disheTable = clsMain.ReturnDataTable(sSQL);

                                //cập nhật MACB disheTable
                                if (disheJoinTable.Rows.Count > 0)
                                    sMaCB_TiepTheo = int.Parse(disheJoinTable.Rows[disheJoinTable.Rows.Count - 1]["MACB"].ToString()) + 1;
                                if (disheTable.Rows.Count > 0)
                                {
                                    sMaCB_HienTai = int.Parse(disheTable.Rows[0]["MACB"].ToString());
                                    foreach (DataRow dr in disheTable.Rows)
                                    {
                                        if (int.Parse(dr["MACB"].ToString()) == sMaCB_HienTai)
                                        {
                                            dr["MACB"] = sMaCB_TiepTheo;
                                        }
                                        else
                                        {
                                            sMaCB_HienTai = int.Parse(dr["MACB"].ToString());
                                            sMaCB_TiepTheo += 1;
                                            dr["MACB"] = sMaCB_TiepTheo;
                                        }
                                    }
                                }

                                for (int i = 0; i < disheTable.Rows.Count; i++)
                                {
                                    
                                    //món phải là combo?
                                    DataTable dtCB = GetProductInCombo(disheTable.Rows[i]["MA_HANGHOA"].ToString());
                                    if (dtCB.Rows.Count > 0)
                                    {
                                        ssMaCB = int.Parse(disheTable.Rows[i]["MACB"].ToString());
                                        //combo có món thêm?
                                        KTCB = KT_COMBO_OPTION_MONTHEM(disheTable.Rows[i]["MA_HANGHOA"].ToString());
                                        sMaHH = disheTable.Rows[i]["MA_HANGHOA"].ToString();
                                    }
                                    
                                    //có combo
                                    if (dtCB.Rows.Count > 0 || dtCB.Rows.Count == 0 & int.Parse(disheTable.Rows[i]["MACB"].ToString()) == ssMaCB)
                                    {
                                        //món đã có trên bàn?
                                        bool KTHH_In_Table = ChecBishInTable(maHD_banghep, disheTable.Rows[i]["MA_HANGHOA"].ToString(), bool.Parse(disheTable.Rows[i]["Is_Combo"].ToString()), sMaHH);
                                        //combo có món thêm
                                        if (KTCB == true)
                                        {
                                            sSQL = "Insert into CT_HOADON(MA_HOADON,MA_HANGHOA,SOLUONG,GIABAN,GIABAN_THAT,THUE,TRANGTHAI,IS_COMBO,COMBO_CHINH,CHIETKHAU,TEM,MACP,Number,MACB)";
                                            sSQL += "Values (";
                                            sSQL += clsMain.SQLString(maHD_banghep) + ",";
                                            sSQL += clsMain.SQLString(disheTable.Rows[i]["MA_HANGHOA"].ToString()) + ",";
                                            sSQL += clsMain.SQLString(disheTable.Rows[i]["SOLUONG"].ToString()) + ",";
                                            sSQL += clsMain.SQLString(disheTable.Rows[i]["GIABAN"].ToString()) + ",";
                                            sSQL += clsMain.SQLString(disheTable.Rows[i]["GIABAN"].ToString()) + ",";
                                            sSQL += clsMain.SQLString(disheTable.Rows[i]["THUE"].ToString()) + ",";
                                            sSQL += clsMain.SQLBit(bool.Parse(disheTable.Rows[i]["TRANGTHAI"].ToString())) + ",";
                                            sSQL += clsMain.SQLBit(bool.Parse(disheTable.Rows[i]["Is_Combo"].ToString())) + ",";
                                            sSQL += clsMain.SQLString(sMaHH) + ",";
                                            sSQL += clsMain.SQLString(disheTable.Rows[i]["CHIETKHAU"].ToString()) + ",";
                                            sSQL += clsMain.SQLString(disheTable.Rows[i]["TEM"].ToString()) + ",";
                                            sSQL += clsMain.SQLString(disheTable.Rows[i]["MACP"].ToString()) + ",";
                                            sSQL += clsMain.SQLString(disheJoinTable.Rows[0]["Number"].ToString()) + ",";
                                            sSQL += clsMain.SQLString(disheTable.Rows[i]["MACB"].ToString()) + ")";
                                            clsMain.ExecuteSQL(sSQL);
                                        }
                                        //combo ko có món thêm
                                        else
                                        {
                                            //thêm dòng mới
                                            if (KTHH_In_Table == false)
                                            {
                                                sSQL = "Insert into CT_HOADON(MA_HOADON,MA_HANGHOA,SOLUONG,GIABAN,GIABAN_THAT,THUE,TRANGTHAI,IS_COMBO,COMBO_CHINH,CHIETKHAU,TEM,MACP,Number,MACB)";
                                                sSQL += "Values (";
                                                sSQL += clsMain.SQLString(maHD_banghep) + ",";
                                                sSQL += clsMain.SQLString(disheTable.Rows[i]["MA_HANGHOA"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheTable.Rows[i]["SOLUONG"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheTable.Rows[i]["GIABAN"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheTable.Rows[i]["GIABAN"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheTable.Rows[i]["THUE"].ToString()) + ",";
                                                sSQL += clsMain.SQLBit(bool.Parse(disheTable.Rows[i]["TRANGTHAI"].ToString())) + ",";
                                                sSQL += clsMain.SQLBit(bool.Parse(disheTable.Rows[i]["Is_Combo"].ToString())) + ",";
                                                sSQL += clsMain.SQLString(sMaHH) + ",";
                                                sSQL += clsMain.SQLString(disheTable.Rows[i]["CHIETKHAU"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheTable.Rows[i]["TEM"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheTable.Rows[i]["MACP"].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheJoinTable.Rows[0].ToString()) + ",";
                                                sSQL += clsMain.SQLString(disheTable.Rows[i]["MACB"].ToString()) + ")";
                                                clsMain.ExecuteSQL(sSQL);
                                            }
                                            //cập nhật số lượng
                                            else
                                            {
                                                sSQL = "UPDATE CT_HOADON SET " + "\n";
                                                sSQL += "SOLUONG +=" + clsMain.SQLString(disheTable.Rows[i]["SOLUONG"].ToString()) + "," + "\n";
                                                sSQL += "CHIETKHAU =" + clsMain.SQLString(disheTable.Rows[i]["CHIETKHAU"].ToString()) + "\n";
                                                sSQL += "WHERE MA_HOADON =" + clsMain.SQLString(maHD_banghep) + "\n";
                                                sSQL += "AND Isnull(Is_Combo,0)=" + clsMain.SQLBit(bool.Parse(disheTable.Rows[i]["Is_Combo"].ToString())) + "\n";
                                                sSQL += "AND MA_HANGHOA =" + clsMain.SQLString(disheTable.Rows[i]["MA_HANGHOA"].ToString()) + "\n";
                                                if (dtCB.Rows.Count > 0)
                                                    sSQL += "AND MACP =" + clsMain.SQLString("CB") + "\n";
                                                else
                                                    sSQL += "AND MACP =" + clsMain.SQLString("CBC") + "\n";
                                                sSQL += "AND COMBO_CHINH =" + clsMain.SQLString(sMaHH) + "\n";
                                                clsMain.ExecuteSQL(sSQL);
                                            }
                                        }
                                    }

                                    //ko có combo
                                    else
                                    {
                                        sMaHH = "";
                                        //món đã có trên bàn?
                                        bool KTHH_In_Table = ChecBishInTable(maHD_banghep, disheTable.Rows[i]["MA_HANGHOA"].ToString(), bool.Parse(disheTable.Rows[i]["Is_Combo"].ToString()), sMaHH);
                                        //thêm dòng mới
                                        if ((KTHH_In_Table == false)
                                        || (KTHH_In_Table == true & int.Parse(disheTable.Rows[i]["TEM"].ToString()) != 0 & disheTable.Rows[i]["MACP"].ToString() == "C")
                                        || (KTHH_In_Table == true & int.Parse(disheTable.Rows[i]["TEM"].ToString()) != 0 & disheTable.Rows[i]["MACP"].ToString() == "P"))
                                        {
                                            sSQL = "Insert into CT_HOADON(MA_HOADON,MA_HANGHOA,SOLUONG,GIABAN,GIABAN_THAT,THUE,TRANGTHAI,IS_COMBO,COMBO_CHINH,CHIETKHAU,TEM,MACP,Number,MACB)";
                                            sSQL += "Values (";
                                            sSQL += clsMain.SQLString(maHD_banghep) + ",";
                                            sSQL += clsMain.SQLString(disheTable.Rows[i]["MA_HANGHOA"].ToString()) + ",";
                                            sSQL += clsMain.SQLString(disheTable.Rows[i]["SOLUONG"].ToString()) + ",";
                                            sSQL += clsMain.SQLString(disheTable.Rows[i]["GIABAN"].ToString()) + ",";
                                            sSQL += clsMain.SQLString(disheTable.Rows[i]["GIABAN"].ToString()) + ",";
                                            sSQL += clsMain.SQLString(disheTable.Rows[i]["THUE"].ToString()) + ",";
                                            sSQL += clsMain.SQLBit(bool.Parse(disheTable.Rows[i]["TRANGTHAI"].ToString())) + ",";
                                            sSQL += clsMain.SQLBit(bool.Parse(disheTable.Rows[i]["Is_Combo"].ToString())) + ",";
                                            sSQL += clsMain.SQLString(sMaHH) + ",";
                                            sSQL += clsMain.SQLString(disheTable.Rows[i]["CHIETKHAU"].ToString()) + ",";
                                            sSQL += clsMain.SQLString(disheTable.Rows[i]["TEM"].ToString()) + ",";
                                            sSQL += clsMain.SQLString(disheTable.Rows[i]["MACP"].ToString()) + ",";
                                            sSQL += clsMain.SQLString(disheJoinTable.Rows[0]["Number"].ToString()) + ",";
                                            sSQL += clsMain.SQLString(disheTable.Rows[i]["MACB"].ToString()) + ")";
                                            clsMain.ExecuteSQL(sSQL);
                                        }
                                        //cập nhật số lượng
                                        else
                                        {
                                            sSQL = "UPDATE CT_HOADON SET " + "\n";
                                            sSQL += "SOLUONG +=" + clsMain.SQLString(disheTable.Rows[i]["SOLUONG"].ToString()) + "," + "\n";
                                            sSQL += "CHIETKHAU =" + clsMain.SQLString(disheTable.Rows[i]["CHIETKHAU"].ToString()) + "\n";
                                            sSQL += "WHERE MA_HOADON =" + clsMain.SQLString(maHD_banghep) + "\n";
                                            sSQL += "AND Isnull(Is_Combo,0)=" + clsMain.SQLBit(bool.Parse(disheTable.Rows[i]["Is_Combo"].ToString())) + "\n";
                                            sSQL += "AND MA_HANGHOA =" + clsMain.SQLString(disheTable.Rows[i]["MA_HANGHOA"].ToString()) + "\n";
                                            sSQL += "AND MACP =" + clsMain.SQLString("C");
                                            clsMain.ExecuteSQL(sSQL);
                                        }
                                    }
                                }

                                sSQL = "";
                                sSQL += "SELECT * FROM BAN WHERE MA_BAN =" + int.Parse(maBanGhep) + "";
                                DataTable dt_ban = clsMain.ReturnDataTable(sSQL);
                                sSQL = "";
                                sSQL += "Update CT_HOADON SET Number =" + dt_ban.Rows[0]["TEN_BAN"].ToString() + " where MA_HOADON='" + maHD + "'";
                                clsMain.ExecuteSQL(sSQL);
                                sSQL = "";
                                sSQL += " Delete CT_HOADON where MA_HOADON='" + maHD + "'";
                                clsMain.ExecuteSQL(sSQL);
                                sSQL = "";
                                sSQL += " Delete CT_MOBAN where MA_HOADON='" + maHD + "'";
                                clsMain.ExecuteSQL(sSQL);
                                sSQL = "";
                                sSQL += " Delete HOADON where MA_HOADON ='" + maHD + "'";
                                clsMain.ExecuteSQL(sSQL);
                                sSQL = "";
                                sSQL += "update BAN SET ISMO_BAN=0 where MA_BAN =" + int.Parse(maBan) + "";
                                clsMain.ExecuteSQL(sSQL);
                                sSQL = "";
                                sSQL += "Update  DISPLAY_ORDER Set MA_BAN=" + clsMain.SQLString(maBanGhep) + " ,MA_HOADON=" + clsMain.SQLString(maHD_banghep) + " Where MA_HOADON =" + clsMain.SQLString(maHD) + " And MA_BAN=" + clsMain.SQLString(maBan) + "\n";
                                clsMain.ExecuteSQL(sSQL);
                            }
                            else
                            {
                                string maHD = GetMaHoaDon(maBan);

                                sSQL = "";
                                sSQL += "SELECT * FROM BAN WHERE MA_BAN =" + int.Parse(maBanGhep) + "";
                                DataTable dt_ban = clsMain.ReturnDataTable(sSQL);
                                sSQL = "";
                                sSQL += "Update CT_HOADON SET Number =" + dt_ban.Rows[0]["TEN_BAN"].ToString() + " where MA_HOADON='" + maHD + "'";
                                clsMain.ExecuteSQL(sSQL);
                                sSQL = "";
                                sSQL += "Update BAN SET ISMO_BAN=1 where MA_BAN =" + int.Parse(maBanGhep) + "";
                                clsMain.ExecuteSQL(sSQL);
                                sSQL = "";
                                sSQL += " Update CT_MOBAN SET MA_BAN =" + int.Parse(maBanGhep) + " where MA_HOADON='" + maHD + "'";
                                clsMain.ExecuteSQL(sSQL);
                                sSQL = "";
                                sSQL += "Update BAN SET ISMO_BAN=0 where MA_BAN =" + int.Parse(maBan) + "";
                                clsMain.ExecuteSQL(sSQL);
                                sSQL = "";
                                sSQL += "Update  DISPLAY_ORDER Set MA_BAN=" + clsMain.SQLString(maBanGhep) + " Where MA_HOADON =" + clsMain.SQLString(maHD) + " And MA_BAN=" + clsMain.SQLString(maBan) + "\n";
                                clsMain.ExecuteSQL(sSQL);
                            }
                            ExitDivideTable();
                            if (cls_ConfigCashier.sInbep_Chuyenban == "1")
                            {
                                string bangoc = "";
                                string banchuyen = "";
                                DataTable dtTenban = new DataTable();

                                sSQL = "";
                                sSQL += "Select TEN_BAN" + "\n";
                                sSQL += "From BAN" + "\n";
                                sSQL += "Where MA_BAN=" + clsMain.SQLString(sLocal_maBan);
                                dtTenban = clsMain.ReturnDataTable(sSQL);
                                bangoc = dtTenban.Rows[0]["TEN_BAN"].ToString();

                                sSQL = "";
                                sSQL += "Select TEN_BAN" + "\n";
                                sSQL += "From BAN" + "\n";
                                sSQL += "Where MA_BAN=" + clsMain.SQLString(sLocal_maBanGhep);
                                dtTenban = clsMain.ReturnDataTable(sSQL);
                                banchuyen = dtTenban.Rows[0]["TEN_BAN"].ToString();

                                sSQL = "";
                                sSQL += "Select MAYINBEP From DM_BEP Where MA_CUAHANG=" + clsMain.SQLString(cls_ConfigCashier.idShop);
                                DataTable dtbep = clsMain.ReturnDataTable(sSQL);
                                string print = "";
                                for (int i = 0; i < dtbep.Rows.Count; i++)
                                {
                                    print = dtbep.Rows[i]["MAYINBEP"].ToString();
                                    DataTable dt = new DataTable();
                                    dt.Columns.Add("TITLE");
                                    dt.Columns.Add("BANGOC");
                                    dt.Columns.Add("BANCHUYEN");
                                    dt.Columns.Add("GIUA");
                                    dt.Rows.Add("Chuyển Bàn", bangoc, banchuyen, "chuyển qua");
                                    Frm_Report1 frm = new Frm_Report1();
                                    frm.ReportName = "rpt_Chuyenban";
                                    frm.DataSource = dt;
                                    frm.WindowState = FormWindowState.Minimized;
                                    frm.PrinterName = print;
                                    frm.IsPrint = true;
                                    frm.ShowDialog();
                                    frm.Dispose();
                                }
                            }
                        }
                    }
                    else
                    {
                        SetDefautlBackgroud(btnBan_Mon);

                        if (funtions == 1)
                        {
                            for (int i = 0; i < dtBan.Rows.Count; i++)
                            {
                                if (bool.Parse(dtBan.Rows[i]["ISBANDAT"].ToString()) == true && btnBan_Mon.Name == dtBan.Rows[i]["MA_BAN"].ToString())
                                {
                                    SetBackgroudOnClick2(btnBan_Mon);
                                }
                                if (bool.Parse(dtBan.Rows[i]["ISMO_BAN"].ToString()) == true && btnBan_Mon.Name == dtBan.Rows[i]["MA_BAN"].ToString())
                                {
                                    SetBackgroudOnClick1(btnBan_Mon);
                                }
                                if (bool.Parse(dtBan.Rows[i]["SEE_PAYMENTS"].ToString()) == true && btnBan_Mon.Name == dtBan.Rows[i]["MA_BAN"].ToString())
                                {
                                    SetBackgroudOnClick3(btnBan_Mon);
                                }
                            }
                        }
                    }
                }


                if (funtions == 1)
                {
                    string mHD = GetMaHoaDon(maBan);
                    if (mHD != null && mHD != "")
                    {
                        DataTable dtcheck = clsMain.ReturnDataTable("select MA_BAN from CT_MOBAN where MA_HOADON='" + mHD + "'");
                        if (dtcheck.Rows.Count > 1)
                        {
                            for (int i = 0; i < dtcheck.Rows.Count; i++)
                            {
                                foreach (SimpleButton btnTable in pnTable_Mon.Controls)
                                {
                                    if (btnTable.Name == dtcheck.Rows[i]["MA_BAN"].ToString())
                                    {
                                        SetBackgroudOnClick(btnTable);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK);
            }
        }

        private bool CheckOpenTable(string idTable)
        {
            string sql = "select ISMO_BAN from BAN where MA_BAN =" + int.Parse(idTable) + "";
            DataTable checkTable = clsMain.ReturnDataTable(sql);
            if (checkTable.Rows.Count > 0 && bool.Parse(checkTable.Rows[0]["ISMO_BAN"].ToString()) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void GetDishOfTable(string maHoaDon)
        {
            dtLuoi.Rows.Clear();
            string sSQL = "";
            sSQL += "Select cthd.MA_HANGHOA, hh.TEN_HANGHOA,cthd.SOLUONG,cthd.GIABAN,'a' as GHICHU,hh.MA_BEP,ISNULL(cthd.THUE,0) as THUE,";
            sSQL += "0 as Thanh_Tien,ISNULL(TRANGTHAI,0) AS TRANGTHAI,ISNULL(TRANGTHAI,0) as IS_COMBO,hh.IS_INBEP,'' as IS_KHUYENMAI, ISNULL(cthd.CHIETKHAU,0) AS CHIETKHAU, ";
            sSQL += "ISNULL(hh.MONTHEM,0) AS MONTHEM, cthd.TEM, cthd.MACP, cthd.MACB, ISNULL(hh.INTEM,0) AS INTEM, cthd.Number, cthd.COMBO_CHINH" + "\n";
            sSQL += "From CT_HOADON as cthd inner join HANGHOA as hh on cthd.MA_HANGHOA=hh.MA_HANGHOA " + "\n";
            sSQL += "Where cthd.MA_HOADON ='" + maHoaDon + "'";
            dtLuoi = clsMain.ReturnDataTable(sSQL);

            for (int i = 0; i < dtLuoi.Rows.Count; i++)
            {
                if (dtLuoi.Rows[i]["MACP"].ToString() == "P")
                {
                    dtLuoi.Rows[i]["TEN_HANGHOA"] = Kytu_Monthem + dtLuoi.Rows[i]["TEN_HANGHOA"].ToString();
                }
                else if (dtLuoi.Rows[i]["MACP"].ToString() == "CBC")
                {
                    dtLuoi.Rows[i]["TEN_HANGHOA"] = Kytu_Combo + dtLuoi.Rows[i]["TEN_HANGHOA"].ToString();
                }
            }
            DataView dv = dtLuoi.DefaultView;
            dv.Sort = "MACB ASC, TEM ASC";
            dtLuoi = dv.ToTable();

            gr_SelectProduct.DataSource = dtLuoi;
            if (gv_SelectProduct.RowCount > 0)
                TotalMoney();
            else
                lbTotal.Text = "0";
            gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.RowCount - 1;
        }

        private void GetDishGoToBuy(string maHangHoa, int idKV)
        {
            try
            {
                string sql = "";
                DataTable dtHH;
                bool CongCombo = false;
                sql = string.Format("SELECT MA_HANGHOA, TEN_HANGHOA, ISNULL(MONTHEM,0) AS MONTHEM, ISNULL(INTEM,0) AS INTEM FROM HANGHOA WHERE MA_HANGHOA='" + maHangHoa + "'");
                dtHH = clsMain.ReturnDataTable(sql);
                bool sMONTHEM = bool.Parse(dtHH.Rows[0]["MONTHEM"].ToString());
                bool sINTEM = bool.Parse(dtHH.Rows[0]["INTEM"].ToString());

                sql = string.Format("SELECT MA_HANGHOA, TEN_HANGHOA FROM HANGHOA A INNER JOIN NHOMHANG B ON A.MA_NHOMHANG=B.MA_NHOMHANG AND B.MONTHEM=1 WHERE MA_HANGHOA='" + maHangHoa + "'");
                dtHH = clsMain.ReturnDataTable(sql);

                if (dtHH.Rows.Count > 0 && sButtonMonThem == true)
                {
                    string sMaHangHoaChonTrenLuoi = gv_SelectProduct.GetRowCellValue(iFocusRow, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                    string sql_HangHoaChonTrenLuoi = string.Format("SELECT MA_HANGHOA, TEN_HANGHOA, ISNULL(MONTHEM,0) AS MONTHEM FROM HANGHOA WHERE MA_HANGHOA='" + sMaHangHoaChonTrenLuoi + "'");
                    DataTable dtHH_HangHoaChonTrenLuoi = clsMain.ReturnDataTable(sql_HangHoaChonTrenLuoi);

                    bool sMONTHEM_HangHoaChonTrenLuoi = bool.Parse(dtHH_HangHoaChonTrenLuoi.Rows[0]["MONTHEM"].ToString());
                    if (sMONTHEM_HangHoaChonTrenLuoi == true)
                    {
                        sTEM = int.Parse(gv_SelectProduct.GetRowCellValue(iFocusRow, gv_SelectProduct.Columns["TEM"]).ToString());
                        sMaCB = int.Parse(gv_SelectProduct.GetRowCellValue(iFocusRow, gv_SelectProduct.Columns["MACB"]).ToString());
                    }
                }

                sql = string.Format("Exec SelectByMON_TRASUA @MAHANG={0},@MAVACH='{1}',@maKV={2},@maCH={3}", maHangHoa, maHangHoa, idKV, cls_ConfigCashier.idShop);
                dtHH = clsMain.ReturnDataTable(sql);
                if (dtHH.Rows.Count < 1)
                {
                    return;
                }
                //sửa định lượng
                if (bool.Parse(dtHH.Rows[0]["SUADINHLUONG"].ToString()))
                {
                    bool flag_Exist = false; // flag_Exist=false: mặt hàng này chưa bán và ngược lại
                    if (gv_SelectProduct.RowCount > 0)
                    {
                        for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                        {
                            if (gv_SelectProduct.GetRowCellValue(i, "MA_HANGHOA").ToString() == maHangHoa)
                            {
                                flag_Exist = true;
                                break;
                            }
                        }
                    }
                    if (!flag_Exist)
                    {
                        sql = "Select MANGUYENLIEU" + "\n";
                        sql += "From DINHLUONG" + "\n";
                        sql += "Where MATHANHPHAM=" + clsMain.SQLString(dtHH.Rows[0]["MA_HANGHOA"].ToString());
                        DataTable dtDL = clsMain.ReturnDataTable(sql);
                        if (dtDL.Rows.Count > 0)
                        {
                            Frm_Suadinhluong frm = new Frm_Suadinhluong(dtHH.Rows[0]["MA_HANGHOA"].ToString());
                            frm.ShowDialog();
                            sSQL_DL += frm.tmp_sSQL;
                            sSQL_DL += "\n";
                            sSQL_Yeucauthem += frm.tmp_sSQL_Yeucauthem;
                            sSQL_Yeucauthem += "\n";
                            frm.Dispose();
                        }
                        else
                        {
                            XtraMessageBox.Show(_chuakhaibaodinhluong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                }
                //sửa giá
                decimal Giaban_Cur = 0;  //giá bán thực của hàng hóa.
                decimal SoLuong_Cur = 0;
                if (bool.Parse(dtHH.Rows[0]["SUAGIA"].ToString()))
                {
                    bool flag_Exist = false; // flag_Exist=false: mặt hàng này chưa bán và ngược lại
                    //if (gv_SelectProduct.RowCount > 0)
                    //{
                    //    for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                    //    {
                    //        if (gv_SelectProduct.GetRowCellValue(i, "MA_HANGHOA").ToString() == maHangHoa)
                    //        {
                    //            flag_Exist = true;
                    //            break;
                    //        }
                    //    }
                    //}
                    if (!flag_Exist)
                    {
                        Frm_Chontheothoigia frm = new Frm_Chontheothoigia();
                        frm.tmp_Thoigia = decimal.Parse(dtHH.Rows[0]["GIABAN"].ToString());
                        frm.ShowDialog();
                        if (frm.bThoat == true)
                        {
                            return;
                        }
                        Giaban_Cur = frm.tmp_Thoigia;
                        SoLuong_Cur = frm.tmp_SoLuong;
                        frm.Dispose();
                    }
                }
                else
                {
                    SoLuong_Cur = 1;
                    Giaban_Cur = decimal.Parse(dtHH.Rows[0]["GIABAN"].ToString());
                }
                //lấy dữ liệu hàng hóa mới chọn
                DM_HangHoa hanghoa = new DM_HangHoa();
                hanghoa.MA_HANGHOA = dtHH.Rows[0]["MA_HANGHOA"].ToString();
                hanghoa.TEN_HANGHOA = dtHH.Rows[0]["TEN_HANGHOA"].ToString();
                hanghoa.SOLUONG = SoLuong_Cur;
                hanghoa.MA_BEP = dtHH.Rows[0]["MA_BEP"].ToString();
                hanghoa.GIABAN = Giaban_Cur;
                hanghoa.GHICHU = dtHH.Rows[0]["GHICHU"].ToString();
                hanghoa.THUE = double.Parse(dtHH.Rows[0]["THUE"].ToString());
                hanghoa.TRANGTHAI = false;
                hanghoa.MONTHEM = bool.Parse(dtHH.Rows[0]["MONTHEM"].ToString());
                hanghoa.IS_COMBO = bool.Parse(dtHH.Rows[0]["IS_COMBO"].ToString());
                hanghoa.INTEM = bool.Parse(dtHH.Rows[0]["INTEM"].ToString());
                //kiem tra co tren luoi chua
                bool testID = false;  // bien ktra trùng tên khi addrow hàng hóa
                if (dtLuoi.Rows.Count < 1)
                {
                    testID = false;
                }
                else
                {
                    for (int i = 0; i < dtLuoi.Rows.Count; i++)
                    {
                        if (dtLuoi.Rows[i]["MA_HANGHOA"].ToString() == hanghoa.MA_HANGHOA && "cb" != dtLuoi.Rows[i]["GHICHU"].ToString() && !bool.Parse(dtLuoi.Rows[i]["IS_KHUYENMAI"].ToString()))
                        {
                            var dtCombo = GetProductInCombo(hanghoa.MA_HANGHOA);
                            if (dtCombo.Rows.Count > 0)
                            {
                                if (KT_COMBO_OPTION_MONTHEM(hanghoa.MA_HANGHOA) == false)
                                {
                                    iFocusRow = i;
                                    dtLuoi.Rows[i]["SOLUONG"] = double.Parse(dtLuoi.Rows[i]["SOLUONG"].ToString()) + 1;
                                    for (int j = 0; j < dtCombo.Rows.Count; j++)
                                    {
                                        double sl = double.Parse(dtCombo.Rows[j]["SOLUONG"].ToString());
                                        sl += double.Parse(dtLuoi.Rows[i + j + 1]["SOLUONG"].ToString());
                                        dtLuoi.Rows[i + j + 1]["SOLUONG"] = sl;
                                    }
                                    testID = true;
                                    CongCombo = true;
                                    break;
                                }
                                else
                                {
                                    DataRow dr = dtLuoi.NewRow();
                                    dr[0] = hanghoa.MA_HANGHOA;
                                    dr[2] = hanghoa.SOLUONG;
                                    dr[3] = hanghoa.GIABAN;
                                    dr[4] = hanghoa.GHICHU;
                                    dr[5] = hanghoa.MA_BEP;
                                    dr[6] = hanghoa.THUE;
                                    dr["CHIETKHAU"] = 0;
                                    dr["TRANGTHAI"] = false;//hàng hóa bình thường,được xóa hay thêm giảm
                                    dr["IS_COMBO"] = false;//hàng hóa bình thường
                                    dr["IS_INBEP"] = bool.Parse(dtHH.Rows[0]["IS_INBEP"].ToString());
                                    dr["IS_KHUYENMAI"] = false;
                                    dr["MONTHEM"] = bool.Parse(dtHH.Rows[0]["MONTHEM"].ToString());
                                    dr["MACB"] = sMaCB;
                                    dr["INTEM"] = bool.Parse(dtHH.Rows[0]["INTEM"].ToString());
                                    if (sButtonMonThem == false)
                                    {
                                        dr[1] = hanghoa.TEN_HANGHOA;

                                        if (hanghoa.IS_COMBO == false)
                                        {
                                            if (sINTEM == true)
                                                dr["TEM"] = sTEM;
                                            else
                                                dr["TEM"] = 0;
                                            dr["MACP"] = "C";
                                            //dr["STT"] = sTT;
                                        }
                                        else
                                        {
                                            dr["TEM"] = 0;
                                            dr["MACP"] = "CB";
                                            //dr["STT"] = "CB";
                                        }

                                    }
                                    else if (sButtonMonThem == true)
                                    {
                                        dr[1] = Kytu_Monthem + hanghoa.TEN_HANGHOA;
                                        dr["TEM"] = sTEM;
                                        dr["MACP"] = "P";
                                        dr["COMBO_CHINH"] = sMaHH_THEMMON;
                                    }
                                    dtLuoi.Rows.Add(dr);
                                    for (int j = 0; j < dtCombo.Rows.Count; j++)
                                    {
                                        DataRow drr = dtLuoi.NewRow();
                                        string sql_combo = string.Format("SELECT MA_HANGHOA, TEN_HANGHOA, ISNULL(MONTHEM,0) AS MONTHEM, ISNULL(INTEM,0) AS INTEM FROM HANGHOA WHERE MA_HANGHOA='" + dtCombo.Rows[j]["MA_HANGHOA"].ToString() + "'");
                                        DataTable dtHH_combo = clsMain.ReturnDataTable(sql_combo);
                                        bool sMONTHEM_combo = bool.Parse(dtHH_combo.Rows[0]["MONTHEM"].ToString());
                                        bool sINTEM_combo = bool.Parse(dtHH_combo.Rows[0]["INTEM"].ToString());
                                        drr[0] = int.Parse(dtCombo.Rows[j]["MA_HANGHOA"].ToString());
                                        drr[2] = dtCombo.Rows[j]["SOLUONG"].ToString();
                                        drr[3] = 0;
                                        drr[4] = "cb";
                                        drr[5] = dtCombo.Rows[j]["MA_BEP"].ToString();
                                        drr[6] = 0;
                                        drr["CHIETKHAU"] = 0;
                                        drr["TRANGTHAI"] = true;//hàng hóa thuộc Combo,không được xóa hay thêm giảm
                                        drr["IS_COMBO"] = true;//hàng hóa thuộc Combo
                                        drr["IS_INBEP"] = bool.Parse(dtHH.Rows[0]["IS_INBEP"].ToString());
                                        drr["IS_KHUYENMAI"] = false;
                                        drr["MONTHEM"] = bool.Parse(dtCombo.Rows[j]["MONTHEM"].ToString());
                                        drr["INTEM"] = bool.Parse(dtCombo.Rows[j]["INTEM"].ToString());
                                        drr["MACB"] = sMaCB;
                                        if (sButtonMonThem == false)
                                        {
                                            drr[1] = Kytu_Combo + dtCombo.Rows[j]["TEN_HANGHOA"].ToString();
                                            if (sINTEM_combo == true)
                                            {
                                                drr["TEM"] = sTEM;
                                                sTEM += 1;
                                            }
                                            else
                                                drr["TEM"] = 0;
                                            drr["MACP"] = "CBC";
                                            //drr["STT"] = sTT;
                                            sTT += 1;
                                        }
                                        else if (sButtonMonThem == true)
                                        {
                                            drr[1] = Kytu_Monthem + hanghoa.TEN_HANGHOA;
                                            drr["TEM"] = sTEM;
                                            drr["MACP"] = "P";
                                            dr["COMBO_CHINH"] = sMaHH_THEMMON;
                                        }
                                        dtLuoi.Rows.Add(drr);
                                    }
                                    iFocusRow = i;
                                    testID = true;
                                    break;
                                }
                            }
                            else
                            {
                                if (sButtonMonThem == true & int.Parse(dtLuoi.Rows[i]["TEM"].ToString()) == sTEM & !bool.Parse(dtHH.Rows[0]["SUAGIA"].ToString())
                                    || sButtonMonThem == false & sMONTHEM == false & sINTEM == false & dtLuoi.Rows[i]["MACP"].ToString() == "C" & !bool.Parse(dtHH.Rows[0]["SUAGIA"].ToString())
                                    || sButtonMonThem == false & sMONTHEM == false & sINTEM == false & dtLuoi.Rows[i]["MACP"].ToString() == "CBC" & !bool.Parse(dtHH.Rows[0]["SUAGIA"].ToString()))
                                {
                                    if (dtLuoi.Rows[i]["MACP"].ToString() == "C" || dtLuoi.Rows[i]["MACP"].ToString() == "CBC")
                                        iFocusRow = i;

                                    dtLuoi.Rows[i]["SOLUONG"] = double.Parse(dtLuoi.Rows[i]["SOLUONG"].ToString()) + 1;
                                    testID = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            testID = false;
                        }
                    }
                }
                //Hàng hóa chưa được order trong bàn nên thêm dòng mới
                if (testID == false)//Hàng hóa chưa được order trong bàn nên thêm dòng mới
                {
                    DataRow dr = dtLuoi.NewRow();
                    dr[0] = hanghoa.MA_HANGHOA;
                    dr[2] = hanghoa.SOLUONG;
                    dr[3] = hanghoa.GIABAN;
                    dr[4] = hanghoa.GHICHU;
                    dr[5] = hanghoa.MA_BEP;
                    dr[6] = hanghoa.THUE;
                    dr["CHIETKHAU"] = 0;
                    dr["TRANGTHAI"] = false;//hàng hóa bình thường,được xóa hay thêm giảm
                    dr["IS_COMBO"] = false;//hàng hóa bình thường
                    dr["IS_INBEP"] = bool.Parse(dtHH.Rows[0]["IS_INBEP"].ToString());
                    dr["IS_KHUYENMAI"] = false;
                    dr["MONTHEM"] = bool.Parse(dtHH.Rows[0]["MONTHEM"].ToString());
                    dr["MACB"] = sMaCB;
                    dr["INTEM"] = bool.Parse(dtHH.Rows[0]["INTEM"].ToString());
                    if (sButtonMonThem == false)
                    {
                        dr[1] = hanghoa.TEN_HANGHOA;

                        if (hanghoa.IS_COMBO == false)
                        {
                            if (sINTEM == true)
                                dr["TEM"] = sTEM;
                            else
                                dr["TEM"] = 0;
                            dr["MACP"] = "C";
                            //dr["STT"] = sTT;
                        }
                        else
                        {
                            dr["TEM"] = 0;
                            dr["MACP"] = "CB";
                            //dr["STT"] = "CB";
                        }
                    }
                    else if (sButtonMonThem == true)
                    {
                        dr[1] = Kytu_Monthem + hanghoa.TEN_HANGHOA;
                        dr["TEM"] = sTEM;
                        dr["MACP"] = "P";
                        dr["COMBO_CHINH"] = sMaHH_THEMMON;
                    }
                    dtLuoi.Rows.Add(dr);
                    // Combo 
                    var dtCombo = GetProductInCombo(hanghoa.MA_HANGHOA);
                    int iNumberCombo = 0;
                    iNumberCombo = dtCombo.Rows.Count;
                    if (dtCombo.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtCombo.Rows.Count; i++)
                        {
                            DataRow drr = dtLuoi.NewRow();
                            string sql_combo = string.Format("SELECT MA_HANGHOA, TEN_HANGHOA, ISNULL(MONTHEM,0) AS MONTHEM, ISNULL(INTEM,0) AS INTEM FROM HANGHOA WHERE MA_HANGHOA='" + dtCombo.Rows[i]["MA_HANGHOA"].ToString() + "'");
                            DataTable dtHH_combo = clsMain.ReturnDataTable(sql_combo);
                            bool sMONTHEM_combo = bool.Parse(dtHH_combo.Rows[0]["MONTHEM"].ToString());
                            bool sINTEM_combo = bool.Parse(dtHH_combo.Rows[0]["INTEM"].ToString());
                            drr[0] = int.Parse(dtCombo.Rows[i]["MA_HANGHOA"].ToString());
                            drr[2] = dtCombo.Rows[i]["SOLUONG"].ToString();
                            drr[3] = 0;
                            drr[4] = "cb";
                            drr[5] = dtCombo.Rows[i]["MA_BEP"].ToString();
                            drr[6] = 0;
                            drr["CHIETKHAU"] = 0;
                            drr["TRANGTHAI"] = true;//hàng hóa thuộc Combo,không được xóa hay thêm giảm
                            drr["IS_COMBO"] = true;//hàng hóa thuộc Combo
                            drr["IS_INBEP"] = bool.Parse(dtHH.Rows[0]["IS_INBEP"].ToString());
                            drr["IS_KHUYENMAI"] = false;
                            drr["MONTHEM"] = bool.Parse(dtCombo.Rows[i]["MONTHEM"].ToString());
                            drr["MACB"] = sMaCB;
                            drr["INTEM"] = bool.Parse(dtCombo.Rows[i]["INTEM"].ToString());
                            if (sButtonMonThem == false)
                            {
                                drr[1] = Kytu_Combo + dtCombo.Rows[i]["TEN_HANGHOA"].ToString();
                                if (sINTEM_combo == true)
                                {
                                    drr["TEM"] = sTEM;
                                    sTEM += 1;
                                }
                                else
                                    drr["TEM"] = 0;
                                drr["MACP"] = "CBC";
                                //drr["STT"] = sTT;
                                sTT += 1;
                            }
                            else if (sButtonMonThem == true)
                            {
                                drr[1] = Kytu_Monthem + hanghoa.TEN_HANGHOA;
                                drr["TEM"] = sTEM;
                                drr["MACP"] = "P";
                                dr["COMBO_CHINH"] = sMaHH_THEMMON;
                            }
                            dtLuoi.Rows.Add(drr);
                        }
                    }
                    if (sButtonMonThem == false)
                        iFocusRow = dtLuoi.Rows.Count - 1 - iNumberCombo;
                }

                DataView dv = dtLuoi.DefaultView;
                dv.Sort = "MACB ASC, TEM ASC";
                dtLuoi = dv.ToTable();


                gr_SelectProduct.DataSource = dtLuoi;
                if (gv_SelectProduct.RowCount > 0)
                {
                    TotalMoney();
                }
                gv_SelectProduct.Focus();
                gv_SelectProduct.FocusedRowHandle = iFocusRow;

                if (sButtonMonThem == false)
                {
                    if (hanghoa.IS_COMBO == false)
                    {
                        sTT += 1;
                    }
                    if (sINTEM == true)
                    {
                        sTEM += 1;
                    }
                    if (CongCombo == false)
                    {
                        sMaCB += 1;
                    }

                }
            }
            catch (Exception ex)
            {
                //XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK);
            }
        }

        private DataTable GetProductInCombo(string maHangHoa)
        {
            string sSQL = "";
            sSQL += "Select cb.MA_COMBO, hh.MA_HANGHOA, hh.TEN_HANGHOA, cb.SOLUONG,HH.MA_BEP, ISNULL(HH.MONTHEM,0) AS MONTHEM, ISNULL(HH.INTEM,0) AS INTEM" + "\n";
            sSQL += "From COMBO cb inner join HANGHOA hh on cb.MA_HANGHOA = hh.MA_HANGHOA" + "\n";
            sSQL += "Where MA_COMBO = " + maHangHoa + "";
            var dtCombo = clsMain.ReturnDataTable(sSQL);
            return dtCombo;
        }

        private DataTable DanhSachSpKM(string maHangHoa)
        {
            string sSQL = "";
            sSQL += "SELECT MA_NHOMHANG" + "\n";
            sSQL += "FROM HANGHOA" + "\n";
            sSQL += "WHERE MA_HANGHOA=" + clsMain.SQLString(maHangHoa);
            DataTable dtNH = clsMain.ReturnDataTable(sSQL);

            sSQL = "";
            sSQL += "Select DISTINCT A.MAKHUYENMAI, MALOAICTKM, THUTUUUTIEN" + "\n";
            sSQL += "From DM_KHUYENMAI A,CT_KHUYENMAI B" + "\n";
            sSQL += "Where A.MAKHUYENMAI=B.MAKHUYENMAI" + "\n";
            sSQL += "AND (CONVERT(DATE,TUNGAY,103) < CONVERT(DATE," + clsMain.SQLString(String.Format("{0:yyyyMMdd}", DateTime.Now)) + ") \n";
            sSQL += "AND CONVERT(DATE,DENNGAY,103) > CONVERT(DATE," + clsMain.SQLString(String.Format("{0:yyyyMMdd}", DateTime.Now)) + ") \n";
            sSQL += "OR (convert(datetime,THOIGIANBATDAU,108) < " + clsMain.SQLString(String.Format("{0:HH:mm:ss}", DateTime.Now)) + "\n";
            sSQL += "AND convert(DATE, TUNGAY, 103) = " + clsMain.SQLString(String.Format("{0:yyyyMMdd}", DateTime.Now)) + ") \n";
            sSQL += "OR (convert(datetime,THOIGIANKETTHUC,108) > " + clsMain.SQLString(String.Format("{0:HH:mm:ss}", DateTime.Now)) + "\n";
            sSQL += "AND convert(DATE, DENNGAY, 103) = " + clsMain.SQLString(String.Format("{0:yyyyMMdd}", DateTime.Now)) + ")) \n";
            sSQL += "AND(HANGMUA=" + clsMain.SQLString(maHangHoa) + "\n";
            sSQL += "OR NHOMHANG_HANGHOA = " + clsMain.SQLString(dtNH.Rows[0]["MA_NHOMHANG"].ToString()) + "\n";
            sSQL += "OR NHOMHANG_HANGHOA = " + clsMain.SQLString(maHangHoa) + ")";
            var dtKM = clsMain.ReturnDataTable(sSQL);
            return dtKM;
        }

        DataTable checkVAT = new DataTable();
        private void TotalMoney()
        {
            if (checkVAT.Rows.Count <= 0)
            {
                string sSQL = "Select GIATRI From CAUHINH Where TEN='GIABANTRUOCTHUE' ";
                checkVAT = clsMain.ReturnDataTable(sSQL);
            }
            bool kt = false;
            if (checkVAT.Rows[0]["GIATRI"].ToString() == "1")
                kt = true;
            double total = 0;
            try
            {
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    double thanhTien = 0, vat = 0;
                    double sl = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                    double gia = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                    double CK = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["CHIETKHAU"]).ToString());
                    thanhTien = (sl * gia) - (sl * gia) * CK / 100;
                    if (kt == true)
                    {
                        vat = (double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["THUE"]).ToString()) / 100) * thanhTien;
                    }
                    total += thanhTien + vat;
                    gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"], thanhTien + vat);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            lbTotal.Text = string.Format("{0:#,###0}", total);
        }

        private void txtID_Click(object sender, EventArgs e)
        {
            txtID.SelectAll();
        }

        private void txtID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(txtID.Text))
                {
                    txtID.Focus();
                    return;
                }
                if (funtions == 2)
                {
                    if (txtID.SelectedText != "")
                    {
                        string smahanghoa = txtID.EditValue.ToString();
                        string stenhanghoa = txtID.Text.ToString();
                        sendOrder = 1;
                        lbMon.Text = " >   " + stenhanghoa;
                        GetDishGoToBuy(smahanghoa, maKV);
                        txtID.Text = "";
                        txtID.Focus();
                    }
                    else
                    {
                        string sSQL = "";
                        sSQL += "Exec SP_Select_Hanghoa_Mavach " + clsMain.SQLString(txtID.Text);
                        DataTable dtMon = clsMain.ReturnDataTable(sSQL);
                        if (dtMon.Rows.Count > 0)
                        {
                            sendOrder = 1;
                            lbMon.Text = " >   " + dtMon.Rows[0]["TEN_HANGHOA"].ToString();
                            GetDishGoToBuy(dtMon.Rows[0]["MA_HANGHOA"].ToString(), maKV);
                            txtID.Text = "";
                            txtID.Focus();
                        }
                        else
                        {
                            txtID.Text = "";
                            txtID.Focus();
                        }
                    }
                }
            }
        }

        private void btn_DowLast_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                iFocusRow = gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.RowCount - 1;
                btn_UpFirst.Enabled = true;
                btn_up.Enabled = true;
                btn_Dow.Enabled = false;
                btn_DowLast.Enabled = false;
                iFocusRow = gv_SelectProduct.FocusedRowHandle;
                lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
            }
            catch
            {
            }
        }

        private void btn_Dow_Click(object sender, EventArgs e)
        {
            try
            {
                if (gv_SelectProduct.FocusedRowHandle < gv_SelectProduct.RowCount)
                {
                    gv_SelectProduct.Focus();
                    gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.FocusedRowHandle + 1;
                    btn_up.Enabled = true;
                    btn_UpFirst.Enabled = true;
                    lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
                    if (gv_SelectProduct.FocusedRowHandle == gv_SelectProduct.RowCount - 1)
                    {
                        btn_Dow.Enabled = false;
                        btn_DowLast.Enabled = false;
                    }
                    iFocusRow = gv_SelectProduct.FocusedRowHandle;
                }
            }
            catch
            {
            }
        }

        private void btn_up_Click(object sender, EventArgs e)
        {
            try
            {
                if (gv_SelectProduct.FocusedRowHandle > 0)
                {
                    gv_SelectProduct.Focus();
                    iFocusRow = gv_SelectProduct.FocusedRowHandle = gv_SelectProduct.FocusedRowHandle - 1;
                    btn_Dow.Enabled = true;
                    btn_DowLast.Enabled = true;
                    lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
                    if (gv_SelectProduct.FocusedRowHandle == 0)
                    {
                        btn_up.Enabled = false;
                        btn_UpFirst.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_UpFirst_Click(object sender, EventArgs e)
        {
            try
            {
                gv_SelectProduct.Focus();
                iFocusRow = gv_SelectProduct.FocusedRowHandle = 0;
                btn_UpFirst.Enabled = false;
                btn_up.Enabled = false;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
                iFocusRow = gv_SelectProduct.FocusedRowHandle;
                lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();
            }
            catch
            {
            }
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            try
            {
                bool sMONTHEM = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MONTHEM"]).ToString());
                string sMaHangHoaChonTrenLuoi = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                DataTable dt_combo = GetProductInCombo(sMaHangHoaChonTrenLuoi);
                bool sTrangThai = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString());
                bool sINTEM = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["INTEM"]).ToString());
                if (KT_COMBO_OPTION_MONTHEM(sMaHangHoaChonTrenLuoi) || sMONTHEM || sINTEM)
                {
                    return;
                }
                else
                {
                    if (!sTrangThai)
                    {
                        if (!Flag_soluong)
                        {
                            if (lbSoluong.Text.Length < 3)
                                lbSoluong.Text += btn0.Text;
                        }
                        int i = gv_SelectProduct.FocusedRowHandle;
                        int soluong = int.Parse(lbSoluong.Text);
                        if (dt_combo.Rows.Count > 0)
                        {
                            gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"], soluong + 1);
                            dtLuoi.Rows[i]["SOLUONG"] = soluong;
                            for (int j = 0; j < dt_combo.Rows.Count; j++)
                            {
                                int sl = int.Parse(dt_combo.Rows[j]["SOLUONG"].ToString());
                                dtLuoi.Rows[i + j + 1]["SOLUONG"] = soluong * sl;
                            }
                        }
                        else
                            gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"], soluong);
                        TotalMoney();
                    }
                }
            }
            catch
            {
            }
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            try
            {
                string sMaHangHoaChonTrenLuoi = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                DataTable dt_combo = GetProductInCombo(sMaHangHoaChonTrenLuoi);
                bool sTrangThai = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString());
                bool sMONTHEM = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MONTHEM"]).ToString());
                bool sINTEM = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["INTEM"]).ToString());
                if (KT_COMBO_OPTION_MONTHEM(sMaHangHoaChonTrenLuoi) || sMONTHEM || sINTEM)
                {
                    return;
                }
                else
                {
                    SimpleButton btn = (SimpleButton)sender;
                    if (!sTrangThai)
                    {
                        if (Flag_soluong)
                        {
                            lbSoluong.Text = btn.Text;
                            Flag_soluong = false;
                        }
                        else
                        {
                            if (lbSoluong.Text.Length < 3)
                                lbSoluong.Text += btn.Text;
                        }

                        int i = gv_SelectProduct.FocusedRowHandle;
                        int soluong = int.Parse(lbSoluong.Text);
                        if (dt_combo.Rows.Count > 0)
                        {
                            gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"], soluong + 1);
                            dtLuoi.Rows[i]["SOLUONG"] = soluong;
                            for (int j = 0; j < dt_combo.Rows.Count; j++)
                            {
                                int sl = int.Parse(dt_combo.Rows[j]["SOLUONG"].ToString());
                                dtLuoi.Rows[i + j + 1]["SOLUONG"] = soluong * sl;
                            }
                        }
                        else
                            gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"], soluong);
                        TotalMoney();
                    }
                }
            }
            catch
            {
            }
        }

        private void btn_tru_Click(object sender, EventArgs e)
        {
            try
            {
                string sMaHangHoaChonTrenLuoi = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                DataTable dt_combo = GetProductInCombo(sMaHangHoaChonTrenLuoi);
                bool sTrangThai = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString());
                bool sMONTHEM = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MONTHEM"]).ToString());
                bool sINTEM = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["INTEM"]).ToString());
                if (KT_COMBO_OPTION_MONTHEM(sMaHangHoaChonTrenLuoi) || sMONTHEM || sINTEM)
                {
                    return;
                }
                else
                {
                    if (!sTrangThai)
                    {
                        int i = gv_SelectProduct.FocusedRowHandle;
                        int soluong = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                        if (soluong > 1)
                        {
                            Flag_soluong = true;
                            if (dt_combo.Rows.Count > 0)
                            {
                                gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"], soluong + 1);
                                dtLuoi.Rows[i]["SOLUONG"] = soluong - 1;
                                for (int j = 0; j < dt_combo.Rows.Count; j++)
                                {
                                    int sl_trenluoi = int.Parse(dtLuoi.Rows[i + j + 1]["SOLUONG"].ToString());
                                    int sl_combo = int.Parse(dt_combo.Rows[j]["SOLUONG"].ToString());
                                    dtLuoi.Rows[i + j + 1]["SOLUONG"] = sl_trenluoi - sl_combo;
                                }
                            }
                            else
                                gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"], soluong - 1);
                            lbSoluong.Text = (soluong - 1).ToString();
                        }
                        TotalMoney();
                    }
                }
            }
            catch
            {

            }
        }

        private void btn_cong_Click(object sender, EventArgs e)
        {
            try
            {
                string sMaHangHoaChonTrenLuoi = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                DataTable dt_combo = GetProductInCombo(sMaHangHoaChonTrenLuoi);
                bool sTrangThai = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString());
                bool sMONTHEM = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MONTHEM"]).ToString());
                bool sINTEM = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["INTEM"]).ToString());
                if (KT_COMBO_OPTION_MONTHEM(sMaHangHoaChonTrenLuoi) || sMONTHEM || sINTEM)
                {
                    return;
                }
                else
                {
                    if (!sTrangThai)
                    {
                        int i = gv_SelectProduct.FocusedRowHandle;
                        int soluong = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                        if (soluong < 999)
                        {
                            Flag_soluong = true;
                            if (dt_combo.Rows.Count > 0)
                            {
                                gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"], soluong + 1);
                                dtLuoi.Rows[i]["SOLUONG"] = soluong + 1;
                                for (int j = 0; j < dt_combo.Rows.Count; j++)
                                {
                                    int sl_trenluoi = int.Parse(dtLuoi.Rows[i + j + 1]["SOLUONG"].ToString());
                                    int sl_combo = int.Parse(dt_combo.Rows[j]["SOLUONG"].ToString());
                                    dtLuoi.Rows[i + j + 1]["SOLUONG"] = sl_trenluoi + sl_combo;
                                }
                            }
                            else
                                gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"], soluong + 1);
                            lbSoluong.Text = (soluong + 1).ToString();
                            TotalMoney();
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            pnBtnDeleteGv.Visible = false;
            btnComboOption.Visible = false;
        }

        private void btn_xoa_Click(object sender, EventArgs e)
        {
            try
            {
                if (!bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
                {
                    DataTable dtCombo = GetProductInCombo(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                    DataTable dtKM = DanhSachSpKM(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                    int MaCB = int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MACB"]).ToString());
                    bool sMONTHEM = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MONTHEM"]).ToString());
                    sTEM = int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TEM"]).ToString());

                    int sSoDongXoa = 0;
                    if (dtCombo.Rows.Count > 0 || dtKM.Rows.Count > 0)
                    {

                        for (int i = 0; i < dtLuoi.Rows.Count; i++)
                        {
                            if (int.Parse(dtLuoi.Rows[i]["MACB"].ToString()) == MaCB)
                            {
                                sSoDongXoa += 1;
                            }
                        }
                        for (int i = 0; i < sSoDongXoa - 1; i++)
                        {
                            gv_SelectProduct.DeleteRow(gv_SelectProduct.FocusedRowHandle + 1);
                        }
                    }
                    else
                    {
                        if (sMONTHEM == true)
                        {
                            sSoDongXoa = 0;
                            for (int i = 0; i < dtLuoi.Rows.Count; i++)
                            {
                                if (int.Parse(dtLuoi.Rows[i]["TEM"].ToString()) == sTEM)
                                {
                                    sSoDongXoa += 1;
                                }
                            }
                            for (int i = 0; i < sSoDongXoa - 1; i++)
                            {
                                gv_SelectProduct.DeleteRow(gv_SelectProduct.FocusedRowHandle + 1);
                            }
                        }
                    }
                    gv_SelectProduct.DeleteSelectedRows();
                    btnComboOption.Visible = false;
                    TotalMoney();
                    if (gv_SelectProduct.RowCount <= 0)
                        pnBtnDeleteGv.Visible = false;
                    else
                        lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();

                    if (dtLuoi.Rows.Count > 0)
                    {
                        sTEM = LayTemLonNhat() + 1;
                        sMaCB = LayMACBLonNhat() + 1;
                        //DanhSoThuTu();
                    }
                    else
                    {
                        //sTEM = 1;
                        sTT = 1;
                        //sMaCB = 1;
                        SetGiaoDienMacDinh();
                    }
                }
            }
            catch (Exception)
            {
            } 
        }

        private void btn_XoaTatCa_Click(object sender, EventArgs e)
        {
            try
            {
                dtLuoi.Rows.Clear();
                DataTable s = new DataTable();
                s = dtLuoi;
                sSQL_DL = "";
                sSQL_Yeucauthem = "";
                TotalMoney();
                pnBtnDeleteGv.Visible = false;
                btnComboOption.Visible = false;
                //sTEM = 1;
                sTT = 1;
                //sMaCB = 1;
                SetGiaoDienMacDinh();
            }
            catch (Exception)
            {
            }
            
        }

        private void gv_SelectProduct_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == SOTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
        }

        private void gv_SelectProduct_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gv_SelectProduct.FocusedRowHandle >= 0)
            {
                clsKP_Terminal.ShowCustomerDisplayOneLine(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString() + " : " + (int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["GIABAN"]).ToString())).ToString("N0") + " VND");
            }
            else
            {
                clsKP_Terminal.ShowCustomerDisplayOneLine("Welcome to " + cls_ConfigCashier.nameShop);
            }
            XuLyGv();
        }

        private void gv_SelectProduct_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            maMon = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
            sTenMon = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
            bool sMONTHEM = bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MONTHEM"]).ToString());
            string sMACP = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MACP"]).ToString();
            ssMaCB = int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MACB"]).ToString());
            if (sMONTHEM == true && sMACP == "C" || sMONTHEM == true && sMACP == "CBC" || sMONTHEM == false && sMACP == "P")
            {
                sTEM_THEMMON = int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TEM"]).ToString());
                sMaCB_THEMMON = int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MACB"]).ToString());
                if (sMONTHEM == true && sMACP == "C")
                    sMaHH_THEMMON = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
            }
            if (sendOrder != 1)
                return;
            XuLyGv();
            pnBtnDeleteGv.Visible = true;
            Flag_soluong = true;
            lbSoluong.Text = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["SOLUONG"]).ToString();

            if (bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
                btnComboOption.Visible = true;
            else
                btnComboOption.Visible = false;

            string yeucau = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["GHICHU"]).ToString();
            if (yeucau != "a")
                txtYeuCauThem.Text = yeucau;
            else
                txtYeuCauThem.Text = "";
        }

        private void XuLyGv()
        {
            if (pnBtnDeleteGv.Visible)
            {
                if (gv_SelectProduct.RowCount > 5)
                {
                    pnUpDowGv.Visible = true;
                }
                else
                {
                    pnUpDowGv.Visible = false;
                }
            }
            else
            {
                if (gv_SelectProduct.RowCount > 8)
                {
                    pnUpDowGv.Visible = true;
                }
                else
                {
                    pnUpDowGv.Visible = false;
                }
            }
        }

        int iTudong_load_sodoban = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            string hour, minute, second;
            second = DateTime.Now.Second.ToString();
            hour = DateTime.Now.Hour.ToString();
            minute = DateTime.Now.Minute.ToString();
            if (second.Length < 2)
                lbgiay.Text = "0" + second;
            else
                lbgiay.Text = second;
            if (minute.Length < 2)
                lbGio.Text = hour + ":0" + minute;
            else
                lbGio.Text = hour + ":" + minute;

            if (iTudong_load_sodoban < 20)
            {
                iTudong_load_sodoban++;
            }
            else
            {
                if (funtions == 1)
                {
                    GetSoDoBan();
                }
                iTudong_load_sodoban = 0;

            }
            if (MyGetData != null)
            {
                MyGetData(dtLuoi, contrDelegate, iFocusRow);
            }
            timer1.Enabled = true;
        }

        bool flagdatban = false;
        bool _flag_chuyenban = false;
        string _mahoadon_chuyenban = "";
        private void timer2_Tick(object sender, EventArgs e)
        {
            timer2.Enabled = false;
            try
            {
                if (flagdatban)
                    return;
                string sSQL = "";
                sSQL += "Select Distinct HD.MA_HOADON,DATEDIFF(minute, GETDATE(),GIOVAO) as PHUT,ISMO_BAN" + "\n";
                sSQL += "From HOADON HD,CT_MOBAN CT,BAN B" + "\n";
                sSQL += "Where HD.ISBANDAT=1 AND ISPAYMENT = 0" + "\n";
                sSQL += "AND HD.MA_HOADON=CT.MA_HOADON AND CT.MA_BAN=B.MA_BAN AND ISMO_BAN=0";
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                if (dt.Rows.Count > 0)
                {
                    sSQL = "Select TEN,GIATRI From CAUHINH Where TEN='NHACNHO_DATBAN'";
                    DataTable dtCauhinh = clsMain.ReturnDataTable(sSQL);

                    int n = dt.Rows.Count;
                    for (int i = 0; i < n; i++)
                    {
                        if (int.Parse(dt.Rows[i]["PHUT"].ToString()) <= int.Parse(dtCauhinh.Rows[i]["GIATRI"].ToString()) && int.Parse(dt.Rows[i]["PHUT"].ToString()) >= 0)
                        {
                            flagdatban = true;
                            if (DialogResult.Yes == (XtraMessageBox.Show(_cobansaptoigiovao, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Information)))
                            {
                                Frm_Danhsachdatban frm = new Frm_Danhsachdatban();
                                frm.ShowDialog();

                                _flag_chuyenban = Frm_Danhsachdatban.flag_Chuyenban;
                                _mahoadon_chuyenban = Frm_Danhsachdatban.mahoadon;
                                if (_flag_chuyenban)
                                {
                                    btn_DatBan_Click(sender, e);
                                    sSQL = "";
                                    sSQL += "Select TEN,hd.SODIENTHOAI,DIACHI,TIENDATCOC,NGAYDAT,GIOVAO" + "\n";
                                    sSQL += "From HOADON hd,NHACUNGCAP n" + "\n";
                                    sSQL += "Where hd.MA_KHACHHANG=n.MA" + "\n";
                                    sSQL += "And hd.MA_HOADON=" + clsMain.SQLString(_mahoadon_chuyenban);
                                    DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                                    if (dt1.Rows.Count > 0)
                                    {
                                        txtDBTenKH.Text = dt1.Rows[0]["TEN"].ToString();
                                        txtDienThoai.Text = dt1.Rows[0]["SODIENTHOAI"].ToString();
                                        txtAddress.Text = dt1.Rows[0]["DIACHI"].ToString();
                                        txtPartMoneyPayments.EditValue = dt1.Rows[0]["TIENDATCOC"].ToString();
                                        txtngay.EditValue = DateTime.Parse(dt1.Rows[0]["NGAYDAT"].ToString());
                                        cbHH.Text = DateTime.Parse(dt1.Rows[0]["GIOVAO"].ToString()).Hour.ToString().Length < 2 ? "0" + DateTime.Parse(dt1.Rows[0]["GIOVAO"].ToString()).Hour.ToString() : DateTime.Parse(dt1.Rows[0]["GIOVAO"].ToString()).Hour.ToString();
                                        cbMM.Text = DateTime.Parse(dt1.Rows[0]["GIOVAO"].ToString()).Minute.ToString().Length < 2 ? "0" + DateTime.Parse(dt1.Rows[0]["GIOVAO"].ToString()).Minute.ToString() : DateTime.Parse(dt1.Rows[0]["GIOVAO"].ToString()).Minute.ToString();
                                        KhoamoControl_Chuyenban(true);
                                    }
                                }
                                else
                                {
                                    GetSoDoBan();
                                    flagdatban = false;
                                }

                            }
                            else
                                flagdatban = false;
                        }
                    }
                }
            }
            catch
            {
            }
            timer2.Enabled = true;
        }

        public void KhoamoControl_Chuyenban(bool flag)
        {
            txtDBTenKH.Properties.ReadOnly = txtDienThoai.Properties.ReadOnly = txtngay.Properties.ReadOnly = flag;
            txtAddress.Properties.ReadOnly = txtPartMoneyPayments.Properties.ReadOnly = flag;
            cbHH.Properties.ReadOnly = cbMM.Properties.ReadOnly = chkEmail.Properties.ReadOnly = flag;
            btn_SelectDishes.Enabled = btn_Danhsachdatban.Enabled = !flag;
        }

        private void btn_ThuNgan_Click(object sender, EventArgs e)
        {
            if (funtions == 9 || exitReprint == true)
                return;
            SetBackgroudOnClick(btn_ThuNgan);
            SetDefautlBackgroud(btn_NghiepVu);
            SetDefautlBackgroud(btn_DatThu);
            pnThuNgan.BringToFront();
        }

        private void btn_NghiepVu_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
                return;
            SetBackgroudOnClick(btn_NghiepVu);
            SetDefautlBackgroud(btn_ThuNgan);
            SetDefautlBackgroud(btn_DatThu);
            pnNghiepVu.BringToFront();
        }

        private void btn_DatThu_Click(object sender, EventArgs e)
        {
            if (funtions != 1 || exitReprint == true)
                return;
            SetBackgroudOnClick(btn_DatThu);
            SetDefautlBackgroud(btn_ThuNgan);
            SetDefautlBackgroud(btn_NghiepVu);
            pnDacThu.BringToFront();
        }

        private void btn_logout_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnMoKet_Click(object sender, EventArgs e)
        {
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }
            KP_Terminal.clsKP_Terminal.OpenCashDrawer(false);
        }

        private void btnKetKa_Click(object sender, EventArgs e)
        {
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }
            Frm_Ketca frm = new Frm_Ketca();
            frm.Width = this.Width;
            frm.Height = this.Height;
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btn_HuyBan_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
                return;
            if (maBan == "")
                return;
            if (CheckOpenTable(maBan) == false)
                return;

            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }

            string maHoaDon = GetMaHoaDon(maBan);
            DataTable table = GetTableInBill(maHoaDon);
            string sSQL = "";
            if (table.Rows.Count > 0)
            {
                sSQL = "";
                sSQL += "EXEC SP_InsertHuyBan " + clsMain.SQLString(maHoaDon) + "," + clsMain.SQLString(table.Rows[0]["MA_BAN"].ToString()) + "," + clsMain.SQLString(clsGlobal.gsUserID) + "";
                clsMain.ExecuteSQL(sSQL);

                sSQL = "";
                sSQL += "Delete From CT_HOADON Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + "\n";
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    sSQL += "Delete From CT_MOBAN Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + " And MA_BAN=" + clsMain.SQLString(table.Rows[i]["MA_BAN"].ToString()) + "\n";

                    sSQL += "Update  DISPLAY_ORDER Set FLAG=0 Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + " And MA_BAN=" + clsMain.SQLString(table.Rows[i]["MA_BAN"].ToString()) + "\n";

                    sSQL += "Update BAN SET ISMO_BAN=0, SEE_PAYMENTS = 0 Where MA_BAN =" + clsMain.SQLString(table.Rows[i]["MA_BAN"].ToString()) + "\n";
                }
                sSQL += "Delete From HOADON Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + "\n";
                sSQL += "Delete From DINHLUONG_DIEUCHINH Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + "\n";


                clsMain.ExecuteSQL(sSQL);
            }
            else
            {
                sSQL = "";
                sSQL += "Update BAN SET ISMO_BAN=0, SEE_PAYMENTS = 0 Where MA_BAN =" + clsMain.SQLString(maBan) + "\n";
                clsMain.ExecuteSQL(sSQL);
            }

            //in bill hủy bàn
            string sTen_ban = "";
            sSQL = "";
            sSQL += "Select TEN_BAN From BAN Where MA_BAN=" + clsMain.SQLString(maBan) + "\n";
            DataTable dtb = clsMain.ReturnDataTable(sSQL);
            if (dtb.Rows.Count > 0)
            {
                sTen_ban = dtb.Rows[0]["TEN_BAN"].ToString();
            }

            while (gv_SelectProduct.RowCount > 0)
            {
                List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                int j = 0;
                string maBEP = "";
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    DM_HangHoa dmHangHoa = new DM_HangHoa();
                    maBEP = gv_SelectProduct.GetRowCellValue(0, gv_SelectProduct.Columns["MA_BEP"]).ToString();
                    if (maBEP == gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_BEP"]).ToString())
                    {
                        dmHangHoa.Ma_HoaDon = maHoaDon;
                        dmHangHoa.Ten_HoaDon = "Hủy Bàn";
                        dmHangHoa.MaQuay = cls_ConfigCashier.nameCashier;
                        dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                        dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                        dmHangHoa.TEN_BAN = sTen_ban;
                        dmHangHoa.STT = (++j).ToString();
                        dmHangHoa.MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                        dmHangHoa.TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                        dmHangHoa.SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                        dmHangHoa.GHICHU = "Hủy Món";
                        dmHangHoa.IS_INBEP = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["IS_INBEP"]).ToString());

                        if (dmHangHoa.IS_INBEP)
                            listDM_HangHoa.Add(dmHangHoa);

                        if (i != 0)
                        {
                            gv_SelectProduct.DeleteRow(i);
                            i--;
                        }
                    }
                }
                gv_SelectProduct.DeleteRow(0);
                string print = GetIdKitchen(maBEP);
                if (print == null)
                    XtraMessageBox.Show(_chuachonmayinchomhnay, _thongbao, MessageBoxButtons.OK);
                convert cvrt = new convert();
                DataTable Orderbill = cvrt.ConvertToDataTable(listDM_HangHoa);
                if (Orderbill.Rows.Count > 0)
                {
                    Frm_Report1 frm1 = new Frm_Report1();
                    if (cls_KP_RES.Mode == 3)
                    {
                        frm1.ReportName = "rpt_Order_1";
                    }
                    else
                    {
                        frm1.ReportName = "rpt_Order";
                    }
                    frm1.DataSource = Orderbill;
                    frm1.WindowState = FormWindowState.Minimized;
                    frm1.PrinterName = print;
                    frm1.IsPrint = true;
                    frm1.ShowDialog();
                    frm1.Dispose();
                }
            }
            ExitDivideTable();
            sTEM = 1;
            sMaCB = 1;
        }

        private DataTable GetTableInBill(string idBill)
        {
            string sql = "select MA_BAN from CT_MOBAN where MA_HOADON='" + idBill + "'";
            DataTable retTables = clsMain.ReturnDataTable(sql);
            return retTables;
        }

        private void btnSuaGia_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
                return;
            string sMaHoaDon = GetMaHoaDon(maBan);
            if (sMaHoaDon == "")
                return;
            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }
            string sSQL = "SELECT * FROM BAN WHERE MA_BAN =" + int.Parse(maBan) + "";
            DataTable dt_ban = clsMain.ReturnDataTable(sSQL);

            DM_HangHoa dmHangHoa = new DM_HangHoa();
            dmHangHoa.MA_HANGHOA = gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
            dmHangHoa.TEN_HANGHOA = gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
            dmHangHoa.SOLUONG = decimal.Parse(gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["SOLUONG"]).ToString());
            dmHangHoa.GIABAN = decimal.Parse(gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["GIABAN"]).ToString());
            dmHangHoa.Thanh_Tien = decimal.Parse(gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["Thanh_Tien"]).ToString());
            dmHangHoa.GHICHU = gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["GHICHU"]).ToString();
            dmHangHoa.IS_COMBO = bool.Parse(gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["IS_COMBO"]).ToString());
            dmHangHoa.MA_BEP = gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["MA_BEP"]).ToString();
            dmHangHoa.MONTHEM = bool.Parse(gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["MONTHEM"]).ToString());
            dmHangHoa.IS_INBEP = bool.Parse(gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["IS_INBEP"]).ToString());
            dmHangHoa.TEM = int.Parse(gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["TEM"]).ToString());
            dmHangHoa.MACP = gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["MACP"]).ToString();
            dmHangHoa.Number = dt_ban.Rows[0]["TEN_BAN"].ToString();
            dmHangHoa.MACB = int.Parse(gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["MACB"]).ToString());
            dmHangHoa.INTEM = bool.Parse(gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["INTEM"]).ToString());
            dmHangHoa.COMBO_CHINH = gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["COMBO_CHINH"]).ToString();
            dmHangHoa.THUE = double.Parse(gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["THUE"]).ToString());
            dmHangHoa.CHIETKHAU = decimal.Parse(gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["CHIETKHAU"]).ToString());
            dmHangHoa.TRANGTHAI = bool.Parse(gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["TRANGTHAI"]).ToString());

            DataTable dt = new DataTable();
            dt.Columns.Add("MA_HOADON");
            dt.Columns.Add("MA_HANGHOA");
            dt.Columns.Add("TEN_HANGHOA");
            dt.Columns.Add("SOLUONG");
            dt.Columns.Add("GIABAN");
            dt.Columns.Add("IS_COMBO");
            dt.Columns.Add("TEM");
            dt.Columns.Add("MACP");
            dt.Columns.Add("Number");
            dt.Columns.Add("MACB");
            dt.Columns.Add("COMBO_CHINH");
            dt.Columns.Add("THUE");
            dt.Columns.Add("CHIETKHAU");
            dt.Columns.Add("TRANGTHAI");
            dt.Rows.Add(sMaHoaDon, dmHangHoa.MA_HANGHOA, dmHangHoa.TEN_HANGHOA, dmHangHoa.SOLUONG, dmHangHoa.GIABAN, dmHangHoa.IS_COMBO,
                dmHangHoa.TEM, dmHangHoa.MACP, dmHangHoa.Number, dmHangHoa.MACB, dmHangHoa.COMBO_CHINH
                , dmHangHoa.THUE, dmHangHoa.CHIETKHAU, dmHangHoa.TRANGTHAI);

            bool bUpdataGia = false;
            Frm_SuaGia_TraSua frm1 = new Frm_SuaGia_TraSua();
            frm1.dtHanghoa = dt;
            frm1.ShowDialog();
            bUpdataGia = frm1.bCoSua;
            frm1.Dispose();
            if (bUpdataGia)
            {
                ExitDivideTable();
            }
        }

        private void btnChietkhau_Click(object sender, EventArgs e)
        {
            if (gv_SelectProduct.RowCount <= 0 || maMon == null || maMon == "")
                return;

            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }

            decimal Giamoi = 0;
            Frm_Nhapchietkhau frm = new Frm_Nhapchietkhau();
            frm.ShowDialog();
            Giamoi = frm.tmp_Thoigia;
            frm.Dispose();
            if (Giamoi <= 100 && Giamoi >= 0)
            {
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    if (gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString() == maMon
                        & int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MACB"]).ToString()) == ssMaCB)
                    {
                        gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["CHIETKHAU"], Giamoi.ToString());
                    }
                }
                if (gv_SelectProduct.RowCount > 0)
                {
                    TotalMoney();
                }
            }
        }

        private void btn_SoDoBan_Click(object sender, EventArgs e)
        {
            if (funtions == 1 && maBan != "")
            {
                funtions = 2;
                btn_SoDoBan.Text = _sdb;
                sSQL_DL = "";
                sSQL_Yeucauthem = "";
                dtLuoi.Rows.Clear();
                gr_SelectProduct.DataSource = dtLuoi;
                lbTotal.Text = "0";
                txtID.Visible = true;
                txtID.Focus();
                pnShowHang.BringToFront();
                SetBackgroudOnClick(btn_SoDoBan);
                SetDefaultBackgroundBtnInPn(pnThuNgan);
                SetDefaultBackgroundBtnInPn(pnNghiepVu);
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    if (btn.Name == btn_SoDoBan.Name || btn.Name == btn_GuiOrder.Name || btn.Name == btn_YeuCauThem.Name || btn.Name == btn_MonThem.Name)
                    {
                        btn.Visible = true;
                    }
                    else
                    {
                        btn.Visible = false;
                    }
                }
                //8/11/2016 them so khach theo yeu cau huy viet nam
                if (cls_KP_RES.Mode == 4)
                {
                    if (!CheckOpenTable(maBan))
                    {
                        Frm_Soluongkhach frmTemp = new Frm_Soluongkhach();
                        frmTemp.ShowDialog();
                        sokhach = int.Parse(frmTemp.tmp_Thoigia.ToString());
                        frmTemp.Dispose();
                    }
                }
                GetNhomhang();
                GetListMon();
            }
            else if (funtions == 2)
            {
                funtions = 1;
                maBan = "";
                sSQL_DL = "";
                sSQL_Yeucauthem = "";
                dtLuoi.Rows.Clear();
                gr_SelectProduct.DataSource = dtLuoi;
                lbBan.Text = "";
                lbMon.Text = "";
                lbTotal.Text = "0";
                btn_SoDoBan.Text = _mb;
                txtID.Visible = false;
                pnShowHang.BringToFront();
                SetDefautlBackgroud(btn_SoDoBan);
                SetDefaultBackgroundBtnInPn(pnThuNgan);
                SetDefaultBackgroundBtnInPn(pnNghiepVu);
                GetKhuVuc();
                GetSoDoBan();
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    btn.Visible = true;
                }
            }
        }

        private void SetDefaultBackgroundBtnInPn(PanelControl pn2)
        {
            foreach (SimpleButton bt in pn2.Controls)
            {
                SetDefautlBackgroud(bt);
            }
        }

        private void PrintMaVachFastFood(String sIdBill, String tenBan)
        {
            if (cls_ConfigCashier.sIn_Mavach_Fastfood != "0")
            {
                DataView myDV = (DataView)gv_SelectProduct.DataSource;
                DataTable myDT = myDV.ToTable().Copy();
                DataTable myDT1 = myDV.ToTable().Copy();

                myDT1.Clear();

                foreach (DataRow dr in myDT.Rows)
                {
                    myDT1.Rows.Add(dr["MA_HANGHOA"].ToString(), dr["TEN_HANGHOA"].ToString(), dr["SOLUONG"].ToString(), dr["GIABAN"].ToString(), dr["GHICHU"].ToString()
                        , dr["MA_BEP"].ToString(), dr["THUE"].ToString(), dr["Thanh_Tien"].ToString(), Boolean.Parse(dr["TRANGTHAI"].ToString()), Boolean.Parse(dr["IS_COMBO"].ToString())
                        , Boolean.Parse(dr["IS_INBEP"].ToString()), Boolean.Parse(dr["IS_KHUYENMAI"].ToString()), dr["CHIETKHAU"].ToString(), dr["MONTHEM"].ToString(), dr["TEM"].ToString()
                        , dr["MACP"].ToString(), dr["MACB"].ToString(), dr["INTEM"].ToString());

                }


                myDT.Clear();
                foreach (DataRow dr in myDT1.Rows)
                {
                    if (!dr["TEM"].ToString().Equals("0"))
                        myDT.Rows.Add(dr.ItemArray);
                }

                String nameTable = "";
                String pTem = "";
                int TongSoMon = 0;
                int iMon = 0;
                string sNumber = "";
                foreach (DataRow dr in myDT.Rows)
                {
                    if (dr["MACP"].ToString() == "C" || dr["MACP"].ToString() == "CBC")
                        TongSoMon += 1;
                }

                Frm_Report1 frmTmp1;
                DataTable dt1 = new DataTable();
                dt1.Columns.Add("MA_HOADON");
                dt1.Columns.Add("TEN_HANGHOA");
                dt1.Columns.Add("NGAY_HOADON");
                dt1.Columns.Add("DON_GIA");
                dt1.Columns.Add("YEUCAUTHEM");
                dt1.Columns.Add("BAN");
                dt1.Columns.Add("SOLUONG");
                dt1.Columns.Add("Number");

                if (myDT.Rows.Count == 0)
                    return;
                else
                    pTem = myDT.Rows[0]["TEM"].ToString();
                for (int i = 0; i < myDT.Rows.Count; i++)
                {
                    if (myDT.Rows[i]["MACP"].ToString() == "C" || myDT.Rows[i]["MACP"].ToString() == "CBC")
                    {
                        iMon += 1;
                        sNumber = "(" + tenBan + ") " + iMon + "/" + TongSoMon;
                    }

                    if (!myDT.Rows[i]["TEM"].ToString().Equals("0"))
                    {
                        if (pTem == myDT.Rows[i]["TEM"].ToString())
                        {
                            dt1.Rows.Add(sIdBill, myDT.Rows[i]["TEN_HANGHOA"].ToString().Trim() + "   x " + myDT.Rows[i]["SOLUONG"].ToString(), cls_KP_RES.DateToString_Full_NoSecond(DateTime.Now), int.Parse(myDT.Rows[i]["GIABAN"].ToString()).ToString("N0"), myDT.Rows[i]["GHICHU"].ToString().Length > 2 ? myDT.Rows[i]["GHICHU"].ToString() : "", nameTable, myDT.Rows[i]["SOLUONG"].ToString(), sNumber);
                        }
                        else
                        {
                            if (!myDT.Rows[i]["TEM"].ToString().Equals("0"))
                            {
                                frmTmp1 = new Frm_Report1();
                                frmTmp1.ReportName = "rpt_MaVach_Fastfood";
                                frmTmp1.DataSource = dt1;
                                frmTmp1.WindowState = FormWindowState.Minimized;
                                frmTmp1.PrinterName = KP_Terminal.clsKP_Terminal.Mayinmavach;
                                frmTmp1.IsPrint = true;
                                frmTmp1.ShowDialog();
                                frmTmp1.Dispose();

                                dt1.Clear();
                                dt1.Rows.Add(sIdBill, myDT.Rows[i]["TEN_HANGHOA"].ToString().Trim() + "   x " + myDT.Rows[i]["SOLUONG"].ToString(), cls_KP_RES.DateToString_Full_NoSecond(DateTime.Now), int.Parse(myDT.Rows[i]["GIABAN"].ToString()).ToString("N0"), myDT.Rows[i]["GHICHU"].ToString().Length > 2 ? myDT.Rows[i]["GHICHU"].ToString() : "", nameTable, myDT.Rows[i]["SOLUONG"].ToString(), sNumber);

                                pTem = myDT.Rows[i]["TEM"].ToString();
                            }
                        }
                    }
                    else
                    {
                        dt1.Clear();
                        pTem = "1";
                    }
                }

                if (dt1.Rows.Count > 0)
                {
                    frmTmp1 = new Frm_Report1();
                    frmTmp1.ReportName = "rpt_MaVach_Fastfood";
                    frmTmp1.DataSource = dt1;
                    frmTmp1.WindowState = FormWindowState.Minimized;
                    frmTmp1.PrinterName = KP_Terminal.clsKP_Terminal.Mayinmavach;
                    frmTmp1.IsPrint = true;
                    frmTmp1.ShowDialog();
                    frmTmp1.Dispose();
                }
            }
        }

        private void PrintMaVachFastFood1(String sIdBill, String maBan)
        {
            if (cls_ConfigCashier.sIn_Mavach_Fastfood != "0")
            {
                String nameTable = clsMain.ReturnDataTable("Select TEN_BAN From BAN Where MA_BAN = " + clsMain.SQLString(maBan) + "").Rows[0][0].ToString();
                DataView dv = (DataView)gv_SelectProduct.DataSource;
                DataTable dt = dv.ToTable().Copy();
                foreach (DataRow dr in dt.Rows)
                {
                    for (int i = 0; i < int.Parse(dr["SOLUONG"].ToString()); i++)
                    {
                        DataTable dt1 = new DataTable();
                        dt1.Columns.Add("MA_HOADON");
                        dt1.Columns.Add("TEN_HANGHOA");
                        dt1.Columns.Add("NGAY_HOADON");
                        dt1.Columns.Add("DON_GIA");
                        dt1.Columns.Add("YEUCAUTHEM");
                        dt1.Columns.Add("BAN");
                        dt1.Rows.Add(sIdBill, dr["TEN_HANGHOA"].ToString(), cls_KP_RES.DateToString_Full_NoSecond(DateTime.Now), int.Parse(dr["GIABAN"].ToString()).ToString("N0"), dr["GHICHU"].ToString().Length > 1 ? dr["GHICHU"].ToString() : "", nameTable);

                        Frm_Report1 frmTmp1 = new Frm_Report1();
                        frmTmp1.ReportName = "rpt_MaVach_Fastfood";
                        frmTmp1.DataSource = dt1;
                        frmTmp1.WindowState = FormWindowState.Minimized;
                        frmTmp1.PrinterName = KP_Terminal.clsKP_Terminal.Mayinmavach;
                        frmTmp1.IsPrint = true;
                        frmTmp1.ShowDialog();
                        frmTmp1.Dispose();
                    }
                }
            }
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
                return true;
            else
                return false;
        }

        private void btn_OrderSent_Click(object sender, EventArgs e)
        {
            if (funtions != 2)
                return;
            if (sendOrder != 1)
                return;
            if (string.IsNullOrEmpty(clsGlobal.gsMaNVOfUserLogin))
            {
                XtraMessageBox.Show(_manvrong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                pnBtnDeleteGv.Visible = false;
                btnComboOption.Visible = false;
                string sSQL = "";
                if (CheckOpenTable(maBan) == false)
                {
                    string maHoaDon = GetNewCodeBill();
                    //bàn
                    sSQL += "Update BAN SET ISMO_BAN=1 Where MA_BAN = " + clsMain.SQLString(maBan) + "\n";
                    //hóa đơn
                    sSQL += "Insert Into HOADON(MA_HOADON,SOLUONGKHACH, MANHANVIEN, NGAYTAO, GIOVAO)" + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(maHoaDon) + ",";
                    sSQL += clsMain.SQLString(sokhach.ToString()) + ",";
                    sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                    sSQL += "GETDATE(),GETDATE())" + "\n";
                    //ct mở bàn
                    sSQL += "Insert Into CT_MOBAN(MA_HOADON, MA_BAN)" + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(maHoaDon) + ",";
                    sSQL += clsMain.SQLString(maBan) + ")" + "\n";

                    clsMain.ExecuteSQL(sSQL);
                }

                string nameTable = clsMain.ReturnDataTable("Select TEN_BAN From BAN Where MA_BAN = " + clsMain.SQLString(maBan) + "").Rows[0][0].ToString();
                string maHDB = GetMaHoaDon(maBan);
                //31/08/2017 fastfood sau khi in bill thì trên lưới có bao nhiêu hàng mỗi hàng sẻ in tem qua máy in tem
                PrintMaVachFastFood(maHDB, nameTable);

                
                while (gv_SelectProduct.RowCount > 0)
                {
                    List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                    int j = 0;
                    string maBEP = "";
                    int ssMaCB = 0;
                    bool KTCB = false;
                    string sMaHH = "";
                    for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                    {
                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        maBEP = gv_SelectProduct.GetRowCellValue(0, gv_SelectProduct.Columns["MA_BEP"]).ToString();

                        if (maBEP == gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_BEP"]).ToString())
                        {
                            dmHangHoa.Ma_HoaDon = maHDB;
                            dmHangHoa.Ten_HoaDon = "Order Bếp";
                            dmHangHoa.TEN_BAN = nameTable;
                            dmHangHoa.MaQuay = cls_ConfigCashier.nameCashier;
                            dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                            dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                            dmHangHoa.STT = (++j).ToString();
                            dmHangHoa.MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                            dmHangHoa.TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                            dmHangHoa.SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                            dmHangHoa.THUE = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["THUE"]).ToString());
                            dmHangHoa.GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                            dmHangHoa.TRANGTHAI = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TRANGTHAI"]).ToString());
                            dmHangHoa.IS_COMBO = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["IS_COMBO"]).ToString());
                            dmHangHoa.IS_INBEP = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["IS_INBEP"]).ToString());
                            dmHangHoa.CHIETKHAU = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["CHIETKHAU"]).ToString());                           
                            dmHangHoa.MONTHEM = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MONTHEM"]).ToString());
                            dmHangHoa.TEM = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEM"]).ToString());
                            dmHangHoa.Number = nameTable;
                            dmHangHoa.MACP = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MACP"]).ToString();
                            dmHangHoa.MACB = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MACB"]).ToString());
                            dmHangHoa.COMBO_CHINH = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["COMBO_CHINH"]).ToString();
                            string repuest = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"]).ToString();
                            if (repuest == "a")
                                dmHangHoa.GHICHU = "";
                            else
                                dmHangHoa.GHICHU = repuest;
                            //Insert vào bảng DINHLUONG_DIEUCHINH
                            if (sSQL_DL != "")
                            {
                                sSQL_DL = sSQL_DL.Replace("TEMP_MA_HOADON", clsMain.SQLString(maHDB));
                                string[] arr = sSQL_DL.Split('*');
                                for (int z = 0; z < arr.Length; z++)
                                {
                                    string[] arr1 = arr[z].Split('=');
                                    if (arr1[0].Contains(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString()))
                                    {
                                        clsMain.ExecuteSQL(arr1[1]);
                                    }
                                }
                                arr = sSQL_Yeucauthem.Split('*');
                                for (int z = 0; z < arr.Length; z++)
                                {
                                    string[] arr1 = arr[z].Split('=');
                                    if (arr1[0].Contains(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString()))
                                    {
                                        dmHangHoa.GHICHU = arr1[1];
                                    }
                                }
                            }
                            //nếu hàng hóa được in bếp mới cho in xuống bếp
                            if (dmHangHoa.IS_INBEP)
                            {
                                listDM_HangHoa.Add(dmHangHoa);
                            }
                            //CT hóa đơn
                            sSQL = "";
                            
                            //món phải là combo
                            DataTable dtCB = GetProductInCombo(dmHangHoa.MA_HANGHOA);
                            if (dtCB.Rows.Count > 0)
                            {
                                ssMaCB = dmHangHoa.MACB;
                                //combo có món thêm
                                KTCB = KT_COMBO_OPTION_MONTHEM(dmHangHoa.MA_HANGHOA);
                                sMaHH = dmHangHoa.MA_HANGHOA;
                            }
                            
                            //có combo
                            if (dtCB.Rows.Count > 0 || dtCB.Rows.Count == 0 & dmHangHoa.MACB == ssMaCB)
                            {
                                //món đã có trên bàn chưa?
                                bool KTHH_In_Table = ChecBishInTable(maHDB, dmHangHoa.MA_HANGHOA, dmHangHoa.IS_COMBO, sMaHH); 
                                //combo có món thêm
                                if (KTCB == true)
                                {
                                    sSQL = "Insert into CT_HOADON(MA_HOADON,MA_HANGHOA,SOLUONG,GIABAN,GIABAN_THAT,THUE,TRANGTHAI,IS_COMBO,COMBO_CHINH,CHIETKHAU,TEM,MACP,Number,MACB)";
                                    sSQL += "Values (";
                                    sSQL += clsMain.SQLString(maHDB) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.THUE.ToString()) + ",";
                                    sSQL += clsMain.SQLBit(dmHangHoa.TRANGTHAI) + ",";
                                    sSQL += clsMain.SQLBit(dmHangHoa.IS_COMBO) + ",";
                                    sSQL += clsMain.SQLString(sMaHH) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.CHIETKHAU.ToString()) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.TEM.ToString()) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.MACP) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.Number) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.MACB.ToString()) + ")";
                                    clsMain.ExecuteSQL(sSQL);
                                }
                                //combo ko có món thêm
                                else
                                {
                                    //thêm dòng mới
                                    if (KTHH_In_Table == false)
                                    {
                                        sSQL = "Insert into CT_HOADON(MA_HOADON,MA_HANGHOA,SOLUONG,GIABAN,GIABAN_THAT,THUE,TRANGTHAI,IS_COMBO,COMBO_CHINH,CHIETKHAU,TEM,MACP,Number,MACB)";
                                        sSQL += "Values (";
                                        sSQL += clsMain.SQLString(maHDB) + ",";
                                        sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                                        sSQL += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                                        sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                                        sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                                        sSQL += clsMain.SQLString(dmHangHoa.THUE.ToString()) + ",";
                                        sSQL += clsMain.SQLBit(dmHangHoa.TRANGTHAI) + ",";
                                        sSQL += clsMain.SQLBit(dmHangHoa.IS_COMBO) + ",";
                                        sSQL += clsMain.SQLString(sMaHH) + ",";
                                        sSQL += clsMain.SQLString(dmHangHoa.CHIETKHAU.ToString()) + ",";
                                        sSQL += clsMain.SQLString(dmHangHoa.TEM.ToString()) + ",";
                                        sSQL += clsMain.SQLString(dmHangHoa.MACP) + ",";
                                        sSQL += clsMain.SQLString(dmHangHoa.Number) + ",";
                                        sSQL += clsMain.SQLString(dmHangHoa.MACB.ToString()) + ")";
                                        clsMain.ExecuteSQL(sSQL);
                                    }
                                    //cập nhật số lượng
                                    else
                                    {
                                        sSQL = "UPDATE CT_HOADON SET " + "\n";
                                        sSQL += "SOLUONG +=" + clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + "," + "\n";
                                        sSQL += "CHIETKHAU =" + clsMain.SQLString(dmHangHoa.CHIETKHAU.ToString()) + "\n";
                                        sSQL += "WHERE MA_HOADON =" + clsMain.SQLString(maHDB) + "\n";
                                        sSQL += "AND Isnull(Is_Combo,0)=" + clsMain.SQLBit(dmHangHoa.IS_COMBO) + "\n";
                                        sSQL += "AND MA_HANGHOA =" + clsMain.SQLString(dmHangHoa.MA_HANGHOA) + "\n";
                                        if (dtCB.Rows.Count > 0)
                                            sSQL += "AND MACP =" + clsMain.SQLString("CB") + "\n";
                                        else
                                            sSQL += "AND MACP =" + clsMain.SQLString("CBC") + "\n";
                                        sSQL += "AND COMBO_CHINH =" + clsMain.SQLString(sMaHH) + "\n";
                                        clsMain.ExecuteSQL(sSQL);
                                    }
                                }
                            }
                            
                            //ko có combo
                            else
                            {
                                sMaHH = "";
                                //món đã có trên bàn chưa?
                                bool KTHH_In_Table = ChecBishInTable(maHDB, dmHangHoa.MA_HANGHOA, dmHangHoa.IS_COMBO, sMaHH); 
                                //thêm dòng mới
                                if ((KTHH_In_Table == false)
                                || (KTHH_In_Table == true & dmHangHoa.TEM != 0 & dmHangHoa.MACP == "C")
                                || (KTHH_In_Table == true & dmHangHoa.TEM != 0 & dmHangHoa.MACP == "P"))
                                {
                                    sSQL = "Insert into CT_HOADON(MA_HOADON,MA_HANGHOA,SOLUONG,GIABAN,GIABAN_THAT,THUE,TRANGTHAI,IS_COMBO,COMBO_CHINH,CHIETKHAU,TEM,MACP,Number,MACB)";
                                    sSQL += "Values (";
                                    sSQL += clsMain.SQLString(maHDB) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.THUE.ToString()) + ",";
                                    sSQL += clsMain.SQLBit(dmHangHoa.TRANGTHAI) + ",";
                                    sSQL += clsMain.SQLBit(dmHangHoa.IS_COMBO) + ",";
                                    if (dmHangHoa.TEM != 0)
                                        sSQL += clsMain.SQLString(dmHangHoa.COMBO_CHINH) + ",";
                                    else
                                        sSQL += clsMain.SQLString(sMaHH) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.CHIETKHAU.ToString()) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.TEM.ToString()) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.MACP) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.Number) + ",";
                                    sSQL += clsMain.SQLString(dmHangHoa.MACB.ToString()) + ")";
                                    clsMain.ExecuteSQL(sSQL);
                                }
                                //cập nhật số lượng
                                else
                                {
                                    sSQL = "UPDATE CT_HOADON SET " + "\n";
                                    sSQL += "SOLUONG +=" + clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + "," + "\n";
                                    sSQL += "CHIETKHAU =" + clsMain.SQLString(dmHangHoa.CHIETKHAU.ToString()) + "\n";
                                    sSQL += "WHERE MA_HOADON =" + clsMain.SQLString(maHDB) + "\n";
                                    sSQL += "AND Isnull(Is_Combo,0)=" + clsMain.SQLBit(dmHangHoa.IS_COMBO) + "\n";
                                    sSQL += "AND MA_HANGHOA =" + clsMain.SQLString(dmHangHoa.MA_HANGHOA) + "\n";
                                    sSQL += "AND MACP =" + clsMain.SQLString("C");
                                    clsMain.ExecuteSQL(sSQL);
                                }
                            }
                            
                            //Man hinh bep
                            if (dmHangHoa.IS_INBEP)
                            {
                                sSQL = "";
                                sSQL += "Insert into DISPLAY_ORDER (MA_HANGHOA,MA_HOADON, MA_BAN, GIOORDER, SOLUONG,NHANVIEN, YEUCAU,FLAG)";
                                sSQL += "Values (";
                                sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                                sSQL += clsMain.SQLString(maHDB) + ",";
                                sSQL += clsMain.SQLString(maBan) + ",";
                                sSQL += "GETDATE()" + ",";
                                sSQL += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                                sSQL += clsMain.SQLString(clsGlobal.gsUserID) + ",";
                                sSQL += clsMain.SQLStringUnicode(dmHangHoa.GHICHU) + ",";
                                sSQL += clsMain.SQLBit(true) + ")";
                                clsMain.ExecuteSQL(sSQL);
                            }
                            // Delete product selected 
                            if (i != 0)
                            {
                                gv_SelectProduct.DeleteRow(i);
                                i--;
                            }
                        }
                    }
                    // Printer Order 
                    gv_SelectProduct.DeleteRow(0);
                    convert cvrt = new convert();
                    DataTable Orderbill = cvrt.ConvertToDataTable(listDM_HangHoa);

                    if (Orderbill.Rows.Count > 0)
                    {
                        string print = GetIdKitchen(maBEP);
                        if (print == null)
                        {
                            XtraMessageBox.Show(_chuachonmayinchomathang, _thongbao, MessageBoxButtons.OK);
                            print = "";
                        }

                        //8/11/2016 căt từng món theo yêu cầu huy việt nam
                        if (cls_KP_RES.Mode == 4)
                        {
                            string sSoKhach = "1";
                            try
                            {
                                DataTable dtKhach = clsMain.ReturnDataTable("select SOLUONGKHACH from HOADON where MA_HOADON=" + clsMain.SQLString(Orderbill.Rows[0]["Ma_HoaDon"].ToString()));
                                sSoKhach = dtKhach.Rows[0][0].ToString();
                            }
                            catch
                            {
                            }
                            DataColumn dctemp = new DataColumn();
                            dctemp.DefaultValue = sSoKhach;
                            dctemp.ColumnName = "sokhach";
                            Orderbill.Columns.Add(dctemp);
                            for (int i = 0; i < Orderbill.Rows.Count; i++)
                            {
                                DataTable dtTmp1 = Orderbill.Copy();
                                dtTmp1.Clear();
                                dtTmp1.Rows.Add(Orderbill.Rows[i].ItemArray);

                                Frm_Report1 frmTmp1 = new Frm_Report1();
                                frmTmp1.ReportName = "rpt_Order_1_Cat1";
                                frmTmp1.DataSource = dtTmp1;
                                frmTmp1.WindowState = FormWindowState.Minimized;
                                frmTmp1.PrinterName = print;
                                frmTmp1.IsPrint = true;
                                frmTmp1.ShowDialog();
                                frmTmp1.Dispose();
                            }
                        }
                        else
                        {
                            Frm_Report1 frm = new Frm_Report1();
                            if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                            {
                                frm.ReportName = "rpt_Order_58";
                            }
                            else
                            {
                                if (cls_KP_RES.Mode == 3 || cls_KP_RES.Mode == 4)
                                {
                                    frm.ReportName = "rpt_Order_1";
                                }
                                else
                                {
                                    frm.ReportName = "rpt_Order";
                                }
                            }
                            frm.DataSource = Orderbill;
                            frm.WindowState = FormWindowState.Minimized;
                            frm.PrinterName = print;
                            frm.IsPrint = true;
                            frm.ShowDialog();
                            frm.Dispose();
                        }
                    }
                }

                funtions = 1;
                sendOrder = 0;
                maBan = "";
                sSQL_DL = "";
                sSQL_Yeucauthem = "";
                dtLuoi.Rows.Clear();
                gr_SelectProduct.DataSource = dtLuoi;
                maMon = "";
                lbMon.Text = "";
                lbBan.Text = "";
                lbTotal.Text = "0";
                btn_SoDoBan.Text = _mb;
                txtID.Visible = false;
                pnShowHang.BringToFront();
                SetDefautlBackgroud(btn_SoDoBan);
                SetDefaultBackgroundBtnInPn(pnThuNgan);
                SetDefaultBackgroundBtnInPn(pnNghiepVu);
                GetKhuVuc();
                GetSoDoBan();
                sTEM = 1;
                sMaCB = 1;
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    btn.Visible = true;
                }
            }
            catch (Exception ex)
            {
                txtID.Visible = false;
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool CheckHH_INTEM(string sMaHD, string sMaHH, bool IsCombo)
        {
            bool kq = false;
            string sSQL = "";
            sSQL += "Select MA_CTHOADON" + "\n";
            sSQL += "From CT_HOADON" + "\n";
            sSQL += "Where Isnull(Is_Combo,0)=" + clsMain.SQLBit(IsCombo) + "\n";
            sSQL += "And MA_HOADON =" + clsMain.SQLString(sMaHD) + "\n";
            sSQL += "And MA_HANGHOA =" + clsMain.SQLString(sMaHH) + "\n";
            sSQL += "And MACP =" + clsMain.SQLString("C") + "\n";
            DataTable dish = clsMain.ReturnDataTable(sSQL);
            if (dish.Rows.Count > 0)
                kq = true;
            else
                kq = false;
            return kq;
        }

        private string GetNewCodeBill()
        {
            string _mahoadon = "";
            string ngay = cls_ConfigCashier.idCashier + DateTime.Now.Day.ToString("00") + DateTime.Now.Month.ToString("00") + DateTime.Now.Year.ToString("0000");
            DataTable dtMA = clsMain.ReturnDataTable("select [dbo].[fc_NewcodeHoaDon] ('" + ngay + "')");
            _mahoadon = dtMA.Rows[0][0].ToString();
            return _mahoadon;
        }

        private bool ChecBishInTable(string _maHOADON, string maHH, bool IsCombo, string maCombo)
        {
            string sSQL = "";
            sSQL += "Select MA_CTHOADON" + "\n";
            sSQL += "From CT_HOADON " + "\n";
            sSQL += "Where Isnull(Is_Combo,0)=" + clsMain.SQLBit(IsCombo) + "\n";
            sSQL += "And MA_HOADON =" + clsMain.SQLString(_maHOADON) + "\n";
            sSQL += "And MA_HANGHOA =" + clsMain.SQLString(maHH) + "\n";
            if (maCombo != "")
                sSQL += "And COMBO_CHINH =" + clsMain.SQLString(maCombo) + "\n";
            else
                sSQL += "And COMBO_CHINH =" + clsMain.SQLString("0") + "\n";
            DataTable dish = clsMain.ReturnDataTable(sSQL);
            if (dish.Rows.Count > 0)
                return true;
            return false;
        }
        private bool ChecBishInTable2(DataTable dtCombo, string maHH, bool IsCombo, string maCombo)
        {
            string sSQL = "";
            sSQL = "__IS_COMBO =" + IsCombo + "\n";
            sSQL += "And __MA_HANGHOA =" + clsMain.SQLString(maHH) + "\n";
            sSQL += "And __COMBO_CHINH =" + clsMain.SQLString(maCombo) + "\n";
            DataRow[] results = dtCombo.Select(sSQL);
            DataTable dtFiltered = dtCombo.Clone();
            foreach (DataRow dr in results)
            {
                dtFiltered.ImportRow(dr);
            }

            if (dtFiltered.Rows.Count > 0)
                return true;
            return false;
        }
        private bool ChecBishInTable1(DataTable dtCombo, string maHH, bool IsCombo, string maCombo)
        {
            string sSQL = "";
            sSQL = "_IS_COMBO =" + IsCombo + "\n";
            sSQL += "And _MA_HANGHOA =" + clsMain.SQLString(maHH) + "\n";
            sSQL += "And _COMBO_CHINH =" + clsMain.SQLString(maCombo) + "\n";
            DataRow[] results = dtCombo.Select(sSQL);
            DataTable dtFiltered = dtCombo.Clone();
            foreach (DataRow dr in results)
            {
                dtFiltered.ImportRow(dr);
            }

            if (dtFiltered.Rows.Count > 0)
                return true;
            return false;
        }
        private string GetIdKitchen(string idKitchen)
        {
            string printKitchen = null;
            string sql = "Select MAYINBEP From DM_BEP Where MA_BEP =" + clsMain.SQLString(idKitchen);
            DataTable kitchen = clsMain.ReturnDataTable(sql);
            if (kitchen.Rows.Count > 0)
            {
                printKitchen = kitchen.Rows[0]["MAYINBEP"].ToString();
                return printKitchen;
            }
            else
                return printKitchen;
        }

        private void btn_YeuCauThem_Click(object sender, EventArgs e)
        {
            try
            {
                txtYeuCauThem.Text = "";
                string sMaCP = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MACP"]).ToString();
                if (sMaCP == "P" || sMaCP == "CB")
                    return;
                if (maMon == "" || funtions != 2 || sendOrder != 1)
                    return;
                if (isYeuCauThem == false)
                {
                    SetBackgroudOnClick(btn_YeuCauThem);
                    lbkvYeuCauThem.Text = lbKhuVuc.Text;
                    lbTableYeuCauThem.Text = lbBan.Text;
                    lbMonYeuCauThem.Text = lbMon.Text;
                    us_keyboard3.SetTextbox(txtYeuCauThem);
                    pnYeucauthem.BringToFront();
                    txtYeuCauThem.Focus();
                    SelectProduct();
                    sTEM = int.Parse(gv_SelectProduct.GetFocusedRowCellValue("TEM").ToString());
                    sMaCB = int.Parse(gv_SelectProduct.GetFocusedRowCellValue("MACB").ToString());
                    isYeuCauThem = true;
                    foreach (SimpleButton btn in pnThuNgan.Controls)
                    {
                        if (btn.Name != "btn_YeuCauThem")
                        {
                            btn.Enabled = false;
                        }
                    }
                }
                else if (isYeuCauThem == true)
                {
                    isYeuCauThem = false;
                    GetNhomhang();
                    GetListMon();
                    SetDefautlBackgroud(btn_YeuCauThem);
                    if (dtLuoi.Rows.Count > 0)
                    {
                        sTEM = LayTemLonNhat() + 1;
                        sMaCB = LayMACBLonNhat() + 1;
                    }
                    foreach (SimpleButton btn in pnThuNgan.Controls)
                    {
                        btn.Enabled = true;
                    }
                }
            }
            catch (Exception)
            {
                
            }
            
        }

        private void SelectProduct()
        {
            if (pnYeuCau.Controls.Count > 0)
                return;
            string sql = "SELECT  * FROM YEUCAUTHEM ORDER BY  YEUCAU ";
            DataTable requestAdd = clsMain.ReturnDataTable(sql);
            pnYeuCau.Controls.Clear();
            for (int i = 0; i < requestAdd.Rows.Count; i++)
            {
                SimpleButton btn = new SimpleButton();
                btn.Name = requestAdd.Rows[i]["id"].ToString();
                btn.Text = requestAdd.Rows[i]["YEUCAU"].ToString();
                btn.Click += new EventHandler(btn_Click);
                btn.Size = new Size(115, 68);
                pnYeuCau.Controls.Add(btn);
            }
        }

        private void btn_Click(object sender, EventArgs e)
        {
            foreach (SimpleButton btn in pnYeuCau.Controls)
            {
                if (btn.Name == this.ActiveControl.Name)
                {
                    txtYeuCauThem.Text += btn.Text + "," + " ";
                    break;
                }
            }
            txtYeuCauThem.Focus();
            int textLength = txtYeuCauThem.Text.Length;
            txtYeuCauThem.SelectionStart = textLength;
            txtYeuCauThem.SelectionLength = 0;
        }

        private void txtYeuCauThem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int sMaHangHoaChonTrenLuoi = int.Parse(gv_SelectProduct.GetRowCellValue(iFocusRow, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    int maHH = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                    if (maHH == sMaHangHoaChonTrenLuoi 
                        & int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MACB"]).ToString()) == ssMaCB)
                    {
                        gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"], txtYeuCauThem.Text);
                        break;
                    }
                }
                txtYeuCauThem.Text = "";
                pnShowHang.BringToFront();
                SetDefaultBackgroundBtnInPn(pnThuNgan);
                SetDefaultBackgroundBtnInPn(pnNghiepVu);

                isYeuCauThem = false;

                if (dtLuoi.Rows.Count > 0)
                {
                    sTEM = LayTemLonNhat() + 1;
                    sMaCB = LayMACBLonNhat() + 1;
                }
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    btn.Enabled = true;
                }
            }
        }

        private void btnYCT_Len_Click(object sender, EventArgs e)
        {
            try
            {
                pnYeuCau.AutoScrollPosition = new Point(0, pnYeuCau.VerticalScroll.Value - pnYeuCau.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnYCT_Xuong_Click(object sender, EventArgs e)
        {
            try
            {
                pnYeuCau.AutoScrollPosition = new Point(0, pnYeuCau.VerticalScroll.Value + pnYeuCau.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnChuyenban_Click(object sender, EventArgs e)
        {
            if (funtions == 1)
            {
                // Check Table Opened or table paymented
                if (maBan == "")
                    return;
                if (GetMaHoaDon(maBan) == "")
                    return;
                btn_Chuyenban.Text = _hcb;
                SetBackgroudOnClick(btn_Chuyenban);
                txtmaKVBanGoc.Text = lbKhuVuc.Text;
                txtmaBanGoc.Text = lbBan.Text;
                funtions = 11;
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    if (btn.Name == btn_Chuyenban.Name)
                    {
                        btn.Visible = true;
                    }
                    else
                    {
                        btn.Visible = false;
                    }
                }
            }
            else if (funtions == 11)
            {
                funtions = 1;
                btn_Chuyenban.Text = _cb;
                SetDefautlBackgroud(btn_Chuyenban);
                dtLuoi.Rows.Clear();
                gr_SelectProduct.DataSource = dtLuoi;
                maBan = "";
                maBanGhep = "";
                lbBan.Text = "";
                lbMon.Text = "";
                lbTotal.Text = "0";
                GetKhuVuc();
                GetSoDoBan();
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    btn.Visible = true;
                }
            }
        }

        private void ExitDivideTable()
        {
            sendOrder = 0;
            funtions = 1;
            exitReprint = false;
            cPageBan = 1;
            btn_SoDoBan.Text = _mb;
            btn_Tachban.Text = _tb;
            btn_Chuyenban.Text = _cb;
            btn_GhepBan.Text = _gb;
            maBan = "";
            maBanGhep = "";
            txtmaBanGhep.Text = "";
            txtmaKVBanGhep.Text = "";
            divide_Table1.Rows.Clear();
            divide_Table2.Rows.Clear();
            SetDefaultBackgroundBtnInPn(pnThuNgan);
            SetDefaultBackgroundBtnInPn(pnNghiepVu);
            foreach (SimpleButton btn in pnThuNgan.Controls)
            {
                btn.Visible = true;
            }
            foreach (SimpleButton btn in pnNghiepVu.Controls)
            {
                btn.Visible = true;
            }
            pnShowHang.BringToFront();
            GetKhuVuc();
            GetSoDoBan();
            string maHD = GetMaHoaDon(maBan);
            GetDishOfTable(maHD);
        }

        private void btn_GhepBan_Click(object sender, EventArgs e)
        {
            if (funtions == 1)
            {
                // Check Table Opened or table paymented
                if (maBan == "")
                    return;
                if (GetMaHoaDon(maBan) == "")
                    return;
                btn_GhepBan.Text = _hgb;
                SetBackgroudOnClick(btn_GhepBan);
                funtions = 5;
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    if (btn.Name == btn_GhepBan.Name)
                    {
                        btn.Visible = true;
                    }
                    else
                    {
                        btn.Visible = false;
                    }
                }
            }
            else if (funtions == 5)
            {
                funtions = 1;
                btn_GhepBan.Text = _gb;
                SetDefautlBackgroud(btn_GhepBan);
                dtLuoi.Rows.Clear();
                gr_SelectProduct.DataSource = dtLuoi;
                maBan = "";
                maBanGhep = "";
                lbBan.Text = "";
                lbMon.Text = "";
                lbTotal.Text = "0";
                GetKhuVuc();
                GetSoDoBan();
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    btn.Visible = true;
                }
            }
        }

        private void btn_TachBan_Click(object sender, EventArgs e)
        {
            if (funtions == 1)
            {
                // Check Table Opened or table paymented
                if (maBan == "")
                    return;
                if (GetMaHoaDon(maBan) == "")
                    return;
                btn_Tachban.Text = _htb;
                txtmaKVBanGoc.Text = lbKhuVuc.Text;
                txtmaBanGoc.Text = lbBan.Text;
                SetBackgroudOnClick(btn_Tachban);
                funtions = 6;
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    if (btn.Name == btn_Tachban.Name)
                    {
                        btn.Visible = true;
                    }
                    else
                    {
                        btn.Visible = false;
                    }
                }
            }
            else if (funtions == 6)
            {
                funtions = 1;
                btn_Tachban.Text = _tb;
                SetDefautlBackgroud(btn_Tachban);
                dtLuoi.Rows.Clear();
                gr_SelectProduct.DataSource = dtLuoi;
                maBan = "";
                maBanGhep = "";
                lbBan.Text = "";
                lbMon.Text = "";
                lbTotal.Text = "0";
                pnShowHang.BringToFront();
                GetKhuVuc();
                GetSoDoBan();
                foreach (SimpleButton btn in pnThuNgan.Controls)
                {
                    btn.Visible = true;
                }
            }
        }

        private void gv_divide_Table_1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == _SSTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
        }

        private void gv_divide_Table_2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == __STT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
            }
        }

        private void btn_DowDivide_Click(object sender, EventArgs e)
        {
            try
            {
                gv_divide_Table_1.Focus();
                gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.FocusedRowHandle + 1;
                btn_UpDivide.Enabled = true;
                if (gv_divide_Table_1.FocusedRowHandle == gv_divide_Table_1.RowCount - 1)
                    btn_DowDivide.Enabled = false;
            }
            catch
            {
            }
        }

        private void btn_UpDivide_Click(object sender, EventArgs e)
        {
            try
            {
                gv_divide_Table_1.Focus();
                gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.FocusedRowHandle - 1;
                btn_DowDivide.Enabled = true;
                if (gv_divide_Table_1.FocusedRowHandle == 0)
                    btn_UpDivide.Enabled = false;
            }
            catch
            {
            }
        }

        private void btn_ToRightAll_Click(object sender, EventArgs e)
        {
            try
            {
                while (gv_divide_Table_1.RowCount > 0)
                    ThrowTable1ToTable2();
                gv_divide_Table_2.FocusedRowHandle = 0;
            }
            catch
            {
            }
        }

        private void btn_ToLeftAll_Click(object sender, EventArgs e)
        {
            try
            {
                while (gv_divide_Table_2.RowCount > 0)
                    Throw_Table2ToTable1();
                gv_divide_Table_1.FocusedRowHandle = 0;
            }
            catch
            {
            }
        }

        private void btn_ToRight_Click(object sender, EventArgs e)
        {
            try
            {
                ThrowTable1ToTable2();
            }
            catch
            {
            }
        }

        private void btn_Toleft_Click(object sender, EventArgs e)
        {
            try
            {
                Throw_Table2ToTable1();
            }
            catch
            {
            }
        }

        public void ThrowTable1ToTable2()
        {
            try
            {
                string MaCP = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_MACP"]).ToString();
                bool sINTEM = bool.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_INTEM"]).ToString());
                int MaCB = int.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_MACB"]).ToString());
                bool KTCB = true;
                string sMaHH = "";
                decimal sl_combo = 0;
                decimal sSoLuong = 0;
                DataTable dtCB2 = new DataTable();
                if (MaCP == "C" || MaCP == "CB")
                {
                    //món bình thường
                    if (MaCP == "C" & sINTEM == false)
                    {
                        decimal sl = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                        if (SoLuong_HuyMon > sl)
                        {
                            XtraMessageBox.Show("Số lượng nhập lớn hơn số lượng trên order", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        if (gv_divide_Table_1.RowCount <= 0)
                            return;
                        bool checkProductOnGv = false;
                        // table2 có data
                        if (divide_Table2.Rows.Count > 0)
                        {
                            for (int i = 0; i < divide_Table2.Rows.Count; i++)
                            {
                                string maHH = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString();
                                if (divide_Table2.Rows[i]["__MA_HANGHOA"].ToString() == maHH & int.Parse(divide_Table2.Rows[i]["__MACB"].ToString()) == MaCB)
                                {
                                    if (decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString()) <= 1)
                                    {
                                        if (flag_sole)
                                            divide_Table2.Rows[i]["__SOLUONG"] = decimal.Parse(divide_Table2.Rows[i]["__SOLUONG"].ToString()) + SoLuong_HuyMon;
                                        else
                                            divide_Table2.Rows[i]["__SOLUONG"] = decimal.Parse(divide_Table2.Rows[i]["__SOLUONG"].ToString()) + decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                                    }
                                    else
                                    {
                                        if (flag_sole)
                                            divide_Table2.Rows[i]["__SOLUONG"] = decimal.Parse(divide_Table2.Rows[i]["__SOLUONG"].ToString()) + SoLuong_HuyMon;
                                        else
                                            divide_Table2.Rows[i]["__SOLUONG"] = decimal.Parse(divide_Table2.Rows[i]["__SOLUONG"].ToString()) + 1;
                                    }
                                    divide_Table2.Rows[i]["__Thanh_Tien"] = decimal.Parse(divide_Table2.Rows[i]["__GIABAN"].ToString()) * decimal.Parse(divide_Table2.Rows[i]["__SOLUONG"].ToString());
                                    checkProductOnGv = true;
                                }
                            }
                        }
                        //Hàng không có trên table2 hay table2 không có data
                        //Thêm món hủy vào table2
                        if (checkProductOnGv == false)
                        {
                            DataRow dr = divide_Table2.NewRow();
                            dr[0] = int.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString());
                            dr[1] = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_TEN_HANGHOA"]).ToString();

                            if (decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString()) <= 1)
                            {
                                if (flag_sole)
                                    dr[2] = SoLuong_HuyMon;
                                else
                                    dr[2] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                            }
                            else
                            {
                                if (flag_sole)
                                    dr[2] = SoLuong_HuyMon;
                                else
                                    dr[2] = 1;
                            }
                            dr[3] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_GIABAN"]).ToString());
                            dr[4] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_MA_BEP"]).ToString());
                            dr[5] = decimal.Parse(dr[2].ToString()) * decimal.Parse(dr[3].ToString());
                            dr[6] = bool.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_IS_COMBO"]).ToString());
                            dr[7] = bool.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_IS_INBEP"]).ToString());
                            dr[8] = bool.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_MONTHEM"]).ToString());
                            dr[9] = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_TEM"]).ToString();
                            dr[10] = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_MACP"]).ToString();
                            dr[11] = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_MACB"]).ToString();
                            dr[12] = bool.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_INTEM"]).ToString());
                            dr[13] = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_COMBO_CHINH"]).ToString();
                            dr[14] = double.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_THUE"]).ToString());
                            dr[15] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_CHIETKHAU"]).ToString());
                            dr[16] = bool.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_TRANGTHAI"]).ToString());
                            divide_Table2.Rows.Add(dr);
                        }
                        gr_divide_Table_2.DataSource = divide_Table2;
                        gv_divide_Table_2.FocusedRowHandle = gv_divide_Table_2.RowCount - 1;

                        //cập nhật table1
                        if (decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString()) <= 1)
                        {
                            if (flag_sole)
                            {
                                gv_divide_Table_1.SetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"], sl - SoLuong_HuyMon);
                                if (decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString()) == 0)
                                    gv_divide_Table_1.DeleteSelectedRows();
                            }
                            else
                                gv_divide_Table_1.DeleteSelectedRows();
                        }
                        else
                        {
                            if (flag_sole)
                                gv_divide_Table_1.SetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"], sl - SoLuong_HuyMon);
                            else
                                gv_divide_Table_1.SetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"], sl - 1);
                            gv_divide_Table_1.SetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_Thanh_Tien"], decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_SOLUONG"]).ToString()) * decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_GIABAN"]).ToString()));
                        }
                        flag_sole = false;
                        SoLuong_HuyMon = 0;
                    }
                    //món có combo hay món có món thêm hay in tem
                    else
                    {
                        for (int i = 0; i < divide_Table1.Rows.Count; i++)
                        {
                            bool checkProductOnGv = false;
                            bool KTHH_In_Table = false;
                            //Thêm món hủy vào table2
                            if (int.Parse(divide_Table1.Rows[i]["_MACB"].ToString()) == MaCB)
                            {
                                string ssMaCP = gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MACP"]).ToString();
                                //món phải là combo?
                                DataTable dtCB = GetProductInCombo(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString());
                                if (dtCB.Rows.Count > 0)
                                {
                                    //combo có món thêm?
                                    KTCB = KT_COMBO_OPTION_MONTHEM(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString());
                                    dtCB2 = dtCB.Copy();
                                    sMaHH = gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString();
                                }

                                //món đã có trên bàn chưa?
                                if (divide_Table2.Rows.Count > 0)
                                    KTHH_In_Table = ChecBishInTable2(divide_Table2, gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString(), bool.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_IS_COMBO"]).ToString()), sMaHH);

                                //combo không được gọi món thêm hay in tem
                                if (dtCB.Rows.Count > 0 & KTCB == false || dtCB.Rows.Count == 0 & ssMaCP == "CBC" & KTCB == false)
                                {
                                    if (ssMaCP == "CB")
                                    {
                                        sl_combo = decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                                        if (KTHH_In_Table == true)
                                        {
                                            if (divide_Table2.Rows.Count > 0)
                                            {
                                                for (int j = 0; j < divide_Table2.Rows.Count; j++)
                                                {
                                                    string maHH = gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString();
                                                    if (divide_Table2.Rows[j]["__MA_HANGHOA"].ToString() == maHH & int.Parse(divide_Table2.Rows[j]["__MACB"].ToString()) == MaCB & (divide_Table2.Rows[j]["__IS_XULY"].ToString() == "" || bool.Parse(divide_Table2.Rows[j]["__IS_XULY"].ToString()) == false))
                                                    {
                                                        if (sl_combo <= 1)
                                                        {
                                                            divide_Table2.Rows[j]["__SOLUONG"] = decimal.Parse(divide_Table2.Rows[j]["__SOLUONG"].ToString()) + decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                                                            //sl_combo = 0;
                                                        }
                                                        else
                                                        {
                                                            divide_Table2.Rows[j]["__SOLUONG"] = decimal.Parse(divide_Table2.Rows[j]["__SOLUONG"].ToString()) + 1;
                                                            //sl_combo -= 1;
                                                        }
                                                        checkProductOnGv = true;
                                                        divide_Table2.Rows[j]["__IS_XULY"] = true;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            // thêm dòng mới
                                            checkProductOnGv = false;
                                            sSoLuong = 1;
                                            if (sl_combo <= 1)
                                                sl_combo = 0;
                                            else
                                                sl_combo -= 1;
                                        }
                                    }
                                    else if (ssMaCP == "CBC")
                                    {
                                        if (KTHH_In_Table == true)
                                        {
                                            if (divide_Table2.Rows.Count > 0)
                                            {
                                                for (int j = 0; j < divide_Table2.Rows.Count; j++)
                                                {
                                                    string maHH = gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString();
                                                    if (divide_Table2.Rows[j]["__MA_HANGHOA"].ToString() == maHH & int.Parse(divide_Table2.Rows[j]["__MACB"].ToString()) == MaCB & (divide_Table2.Rows[j]["__IS_XULY"].ToString() == "" || bool.Parse(divide_Table2.Rows[j]["__IS_XULY"].ToString()) == false))
                                                    {
                                                        if (sl_combo <= 1)
                                                            divide_Table2.Rows[j]["__SOLUONG"] = decimal.Parse(divide_Table2.Rows[j]["__SOLUONG"].ToString()) + decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                                                        else
                                                        {
                                                            decimal sl_trenluoi = decimal.Parse(gv_divide_Table_2.GetRowCellValue(j, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                                                            decimal sl_trongcombo = decimal.Parse(dtCB2.Select("MA_HANGHOA=" + clsMain.SQLString(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString()))[0]["SOLUONG"].ToString());
                                                            divide_Table2.Rows[j]["__SOLUONG"] = sl_trenluoi + sl_trongcombo;
                                                        }
                                                        checkProductOnGv = true;
                                                        divide_Table2.Rows[j]["__IS_XULY"] = true;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            // thêm dòng mới
                                            checkProductOnGv = false;
                                            sSoLuong = decimal.Parse(dtCB2.Select("MA_HANGHOA=" + clsMain.SQLString(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString()))[0]["SOLUONG"].ToString());
                                        }
                                    }
                                    //combo chưa có trên bàn
                                    if (checkProductOnGv == false)
                                    {
                                        //tạo dòng mới
                                        DataRow dr = divide_Table2.NewRow();
                                        dr[0] = int.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString());
                                        dr[1] = gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_TEN_HANGHOA"]).ToString();
                                        dr[2] = sSoLuong;
                                        dr[3] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_GIABAN"]).ToString());
                                        dr[4] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MA_BEP"]).ToString());
                                        dr[5] = decimal.Parse(dr[2].ToString()) * decimal.Parse(dr[3].ToString());
                                        dr[6] = bool.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_IS_COMBO"]).ToString());
                                        dr[7] = bool.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_IS_INBEP"]).ToString());
                                        dr[8] = bool.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MONTHEM"]).ToString());
                                        dr[9] = gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_TEM"]).ToString();
                                        dr[10] = gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MACP"]).ToString();
                                        dr[11] = gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MACB"]).ToString();
                                        dr[12] = bool.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_INTEM"]).ToString());
                                        dr[13] = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_COMBO_CHINH"]).ToString();
                                        dr[14] = double.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_THUE"]).ToString());
                                        dr[15] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_CHIETKHAU"]).ToString());
                                        dr[16] = bool.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_TRANGTHAI"]).ToString());
                                        divide_Table2.Rows.Add(dr);
                                        sSoLuong = 0;
                                    }
                                }
                                // món hay combo được gọi món thêm hay in tem
                                else
                                {
                                    //tạo dòng mới
                                    DataRow dr = divide_Table2.NewRow();
                                    dr[0] = int.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString());
                                    dr[1] = gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_TEN_HANGHOA"]).ToString();
                                    dr[2] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                                    dr[3] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_GIABAN"]).ToString());
                                    dr[4] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MA_BEP"]).ToString());
                                    dr[5] = decimal.Parse(dr[2].ToString()) * decimal.Parse(dr[3].ToString());
                                    dr[6] = bool.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_IS_COMBO"]).ToString());
                                    dr[7] = bool.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_IS_INBEP"]).ToString());
                                    dr[8] = bool.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MONTHEM"]).ToString());
                                    dr[9] = gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_TEM"]).ToString();
                                    dr[10] = gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MACP"]).ToString();
                                    dr[11] = gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MACB"]).ToString();
                                    dr[12] = bool.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_INTEM"]).ToString());
                                    dr[13] = gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_COMBO_CHINH"]).ToString();
                                    dr[14] = double.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_THUE"]).ToString());
                                    dr[15] = decimal.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_CHIETKHAU"]).ToString());
                                    dr[16] = bool.Parse(gv_divide_Table_1.GetRowCellValue(gv_divide_Table_1.FocusedRowHandle, gv_divide_Table_1.Columns["_TRANGTHAI"]).ToString());
                                    divide_Table2.Rows.Add(dr);
                                }
                            }
                        }
                        for (int j = 0; j < divide_Table2.Rows.Count; j++)
                            divide_Table2.Rows[j]["__IS_XULY"] = false;
                        gr_divide_Table_2.DataSource = divide_Table2;
                        gv_divide_Table_2.FocusedRowHandle = gv_divide_Table_2.RowCount - 1;

                        //cập nhật table1
                        if (KTCB == false)
                        {
                            for (int i = 0; i < divide_Table1.Rows.Count; i++)
                            {
                                if (int.Parse(divide_Table1.Rows[i]["_MACB"].ToString()) == MaCB)
                                {
                                    string ssMaCP = gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MACP"]).ToString();
                                    if (ssMaCP == "CB")
                                    {
                                        sl_combo = decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                                        if (sl_combo <= 1)
                                        {
                                            XoaData_divide_Table1(MaCB);
                                        }
                                        else
                                        {
                                            gv_divide_Table_1.SetRowCellValue(i, gv_divide_Table_1.Columns["_SOLUONG"], sl_combo - 1);
                                            gv_divide_Table_1.SetRowCellValue(i, gv_divide_Table_1.Columns["_Thanh_Tien"], decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_SOLUONG"]).ToString()) * decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_GIABAN"]).ToString()));
                                        }
                                    }
                                    else if (ssMaCP == "CBC")
                                    {
                                        if (sl_combo <= 1)
                                        {
                                            XoaData_divide_Table1(MaCB);
                                        }
                                        else
                                        {
                                            decimal sl_trenluoi = decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                                            decimal sl_trongcombo = decimal.Parse(dtCB2.Select("MA_HANGHOA=" + clsMain.SQLString(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString()))[0]["SOLUONG"].ToString());
                                            gv_divide_Table_1.SetRowCellValue(i, gv_divide_Table_1.Columns["_SOLUONG"], sl_trenluoi - sl_trongcombo);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            XoaData_divide_Table1(MaCB);
                        }
                    }
                    DataView dv = divide_Table2.DefaultView;
                    dv.Sort = "__MACB ASC, __TEM ASC";
                    divide_Table2 = dv.ToTable();
                    gr_divide_Table_2.DataSource = divide_Table2;
                }
            }
            catch (Exception)
            {
            }
        }

        private void Throw_Table2ToTable1()
        {
            try
            {
                string MaCP = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__MACP"]).ToString();
                bool sINTEM = bool.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__INTEM"]).ToString());
                int MaCB = int.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__MACB"]).ToString());
                bool KTCB = true;
                string sMaHH = "";
                decimal sl_combo = 0;
                decimal sSoLuong = 0;
                DataTable dtCB2 = new DataTable();
                if (MaCP == "C" || MaCP == "CB")
                {
                    if (gv_divide_Table_2.RowCount <= 0)
                        return;
                    bool checkProductOnGv = false;
                    //món bình thường
                    if (MaCP == "C" & sINTEM == false)
                    {
                        // table1 có data
                        if (divide_Table1.Rows.Count > 0)
                        {
                            for (int i = 0; i < divide_Table1.Rows.Count; i++)
                            {
                                string maHH = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                                if (divide_Table1.Rows[i]["_MA_HANGHOA"].ToString() == maHH && int.Parse(divide_Table1.Rows[i]["_MACB"].ToString()) == MaCB)
                                {
                                    if (decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString()) <= 1)
                                        divide_Table1.Rows[i]["_SOLUONG"] = decimal.Parse(divide_Table1.Rows[i]["_SOLUONG"].ToString()) + decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                                    else
                                        divide_Table1.Rows[i]["_SOLUONG"] = decimal.Parse(divide_Table1.Rows[i]["_SOLUONG"].ToString()) + 1;
                                    divide_Table1.Rows[i]["_Thanh_Tien"] = decimal.Parse(divide_Table1.Rows[i]["_GIABAN"].ToString()) * decimal.Parse(divide_Table1.Rows[i]["_SOLUONG"].ToString());
                                    checkProductOnGv = true;
                                }
                            }
                        }
                        // hàng muốn chuyển từ table2 -> table1 ko có trong table1 nên tạo dòng mới
                        if (checkProductOnGv == false)
                        {
                            DataRow dr = divide_Table1.NewRow();
                            dr[0] = int.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString());
                            dr[1] = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__TEN_HANGHOA"]).ToString();
                            if (decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString()) <= 1)
                                dr[2] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                            else
                                dr[2] = 1;
                            dr[3] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__GIABAN"]).ToString());
                            dr[4] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__MA_BEP"]).ToString());
                            dr[5] = decimal.Parse(dr[2].ToString()) * decimal.Parse(dr[3].ToString());
                            dr[7] = bool.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__IS_COMBO"]).ToString());
                            dr[8] = bool.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__IS_INBEP"]).ToString());
                            dr[9] = bool.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__MONTHEM"]).ToString());
                            dr[10] = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__TEM"]).ToString();
                            dr[11] = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__MACP"]).ToString();
                            dr[12] = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__MACB"]).ToString();
                            dr[13] = bool.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__INTEM"]).ToString());
                            dr[14] = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__COMBO_CHINH"]).ToString();
                            dr[15] = double.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__THUE"]).ToString());
                            dr[16] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__CHIETKHAU"]).ToString());
                            dr[17] = bool.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__TRANGTHAI"]).ToString());
                            divide_Table1.Rows.Add(dr);
                        }
                        gr_divide_Table_1.DataSource = divide_Table1;
                        gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.RowCount - 1;
                        
                        //cập nhật table2
                        if (decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString()) <= 1)
                        {
                            gv_divide_Table_2.DeleteSelectedRows();
                        }
                        else
                        {
                            decimal sl = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                            gv_divide_Table_2.SetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"], sl - 1);
                            gv_divide_Table_2.SetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__Thanh_Tien"], decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__SOLUONG"]).ToString()) * decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__GIABAN"]).ToString()));
                        }
                        
                    }
                    //món có combo hay món có món thêm-in tem
                    else
                    {
                        for (int i = 0; i < divide_Table2.Rows.Count; i++)
                        {
                            checkProductOnGv = false;
                            bool KTHH_In_Table = false;
                            //Thêm món vào table1
                            if (int.Parse(divide_Table2.Rows[i]["__MACB"].ToString()) == MaCB)
                            {
                                string ssMaCP = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MACP"]).ToString();
                                //món phải là combo?
                                DataTable dtCB = GetProductInCombo(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString());
                                if (dtCB.Rows.Count > 0)
                                {
                                    //combo có món thêm?
                                    KTCB = KT_COMBO_OPTION_MONTHEM(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString());
                                    dtCB2 = dtCB.Copy();
                                    sMaHH = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                                }

                                //món đã có trên bàn chưa?
                                if (divide_Table1.Rows.Count > 0)
                                    KTHH_In_Table = ChecBishInTable1(divide_Table1, gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString(), bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__IS_COMBO"]).ToString()), sMaHH);
                                //combo không được gọi món thêm hay in tem
                                if (dtCB.Rows.Count > 0 & KTCB == false || dtCB.Rows.Count == 0 & ssMaCP == "CBC" & KTCB == false)
                                {
                                    if (ssMaCP == "CB")
                                    {
                                        sl_combo = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                                        if (KTHH_In_Table == true)
                                        {
                                            if (divide_Table1.Rows.Count > 0)
                                            {
                                                for (int j = 0; j < divide_Table1.Rows.Count; j++)
                                                {
                                                    string maHH = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                                                    if (divide_Table1.Rows[j]["_MA_HANGHOA"].ToString() == maHH & int.Parse(divide_Table1.Rows[j]["_MACB"].ToString()) == MaCB & (divide_Table1.Rows[j]["_IS_XULY"].ToString() == "" || bool.Parse(divide_Table1.Rows[j]["_IS_XULY"].ToString()) == false))
                                                    {
                                                        if (sl_combo <= 1)
                                                        {
                                                            divide_Table1.Rows[j]["_SOLUONG"] = decimal.Parse(divide_Table1.Rows[j]["_SOLUONG"].ToString()) + decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                                                            //sl_combo = 0;
                                                        }
                                                        else
                                                        {
                                                            divide_Table1.Rows[j]["_SOLUONG"] = decimal.Parse(divide_Table1.Rows[j]["_SOLUONG"].ToString()) + 1;
                                                            //sl_combo -= 1;
                                                        }
                                                        checkProductOnGv = true;
                                                        divide_Table1.Rows[j]["_IS_XULY"] = true;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            // thêm dòng mới
                                            checkProductOnGv = false;
                                            sSoLuong = 1;
                                            if (sl_combo <= 1)
                                                sl_combo = 0;
                                            else
                                                sl_combo -= 1;
                                        }
                                    }
                                    else if (ssMaCP == "CBC")
                                    {
                                        if (KTHH_In_Table == true)
                                        {
                                            if (divide_Table1.Rows.Count > 0)
                                            {
                                                for (int j = 0; j < divide_Table1.Rows.Count; j++)
                                                {
                                                    string maHH = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                                                    if (divide_Table1.Rows[j]["_MA_HANGHOA"].ToString() == maHH & int.Parse(divide_Table1.Rows[j]["_MACB"].ToString()) == MaCB & (divide_Table1.Rows[j]["_IS_XULY"].ToString() == "" || bool.Parse(divide_Table1.Rows[j]["_IS_XULY"].ToString()) == false))
                                                    {
                                                        if (sl_combo <= 1)
                                                            divide_Table1.Rows[j]["_SOLUONG"] = decimal.Parse(divide_Table1.Rows[j]["_SOLUONG"].ToString()) + decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                                                        else
                                                        {
                                                            decimal sl_trenluoi = decimal.Parse(gv_divide_Table_1.GetRowCellValue(j, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                                                            decimal sl_trongcombo = decimal.Parse(dtCB2.Select("MA_HANGHOA=" + clsMain.SQLString(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString()))[0]["SOLUONG"].ToString());
                                                            divide_Table1.Rows[j]["_SOLUONG"] = sl_trenluoi + sl_trongcombo;
                                                        }
                                                        checkProductOnGv = true;
                                                        divide_Table1.Rows[j]["_IS_XULY"] = true;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            // thêm dòng mới
                                            checkProductOnGv = false;
                                            sSoLuong = decimal.Parse(dtCB2.Select("MA_HANGHOA=" + clsMain.SQLString(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString()))[0]["SOLUONG"].ToString());
                                        }
                                    }
                                    //combo chưa có trên bàn
                                    if (checkProductOnGv == false)
                                    {
                                        //tạo dòng mới
                                        DataRow dr = divide_Table1.NewRow();
                                        dr[0] = int.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString());
                                        dr[1] = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__TEN_HANGHOA"]).ToString();
                                        dr[2] = sSoLuong;
                                        dr[3] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__GIABAN"]).ToString());
                                        dr[4] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_BEP"]).ToString());
                                        dr[5] = decimal.Parse(dr[2].ToString()) * decimal.Parse(dr[3].ToString());
                                        dr[7] = bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__IS_COMBO"]).ToString());
                                        dr[8] = bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__IS_INBEP"]).ToString());
                                        dr[9] = bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MONTHEM"]).ToString());
                                        dr[10] = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__TEM"]).ToString();
                                        dr[11] = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MACP"]).ToString();
                                        dr[12] = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MACB"]).ToString();
                                        dr[13] = bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__INTEM"]).ToString());
                                        dr[14] = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__COMBO_CHINH"]).ToString();
                                        dr[15] = double.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__THUE"]).ToString());
                                        dr[16] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__CHIETKHAU"]).ToString());
                                        dr[17] = bool.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__TRANGTHAI"]).ToString());
                                        divide_Table1.Rows.Add(dr);
                                        sSoLuong = 0;
                                    }
                                }
                                // món hay combo được gọi món thêm hay in tem
                                else
                                {
                                    //tạo dòng mới
                                    DataRow dr = divide_Table1.NewRow();
                                    dr[0] = int.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString());
                                    dr[1] = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__TEN_HANGHOA"]).ToString();
                                    dr[2] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                                    dr[3] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__GIABAN"]).ToString());
                                    dr[4] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_BEP"]).ToString());
                                    dr[5] = decimal.Parse(dr[2].ToString()) * decimal.Parse(dr[3].ToString());
                                    dr[7] = bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__IS_COMBO"]).ToString());
                                    dr[8] = bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__IS_INBEP"]).ToString());
                                    dr[9] = bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MONTHEM"]).ToString());
                                    dr[10] = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__TEM"]).ToString();
                                    dr[11] = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MACP"]).ToString();
                                    dr[12] = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MACB"]).ToString();
                                    dr[13] = bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__INTEM"]).ToString());
                                    dr[14] = gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__COMBO_CHINH"]).ToString();
                                    dr[15] = double.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__THUE"]).ToString());
                                    dr[16] = decimal.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__CHIETKHAU"]).ToString());
                                    dr[17] = bool.Parse(gv_divide_Table_2.GetRowCellValue(gv_divide_Table_2.FocusedRowHandle, gv_divide_Table_2.Columns["__TRANGTHAI"]).ToString());
                                    divide_Table1.Rows.Add(dr);
                                }
                            }
                        }
                        for (int j = 0; j < divide_Table1.Rows.Count; j++)
                            divide_Table1.Rows[j]["_IS_XULY"] = false;
                        gr_divide_Table_1.DataSource = divide_Table1;
                        gv_divide_Table_1.FocusedRowHandle = gv_divide_Table_1.RowCount - 1;

                        //cập nhật table2
                        if (KTCB == false)
                        {
                            for (int i = 0; i < divide_Table2.Rows.Count; i++)
                            {
                                if (int.Parse(divide_Table2.Rows[i]["__MACB"].ToString()) == MaCB)
                                {
                                    string ssMaCP = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MACP"]).ToString();
                                    if (ssMaCP == "CB")
                                    {
                                        sl_combo = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                                        if (sl_combo <= 1)
                                        {
                                            XoaData_divide_Table2(MaCB);
                                        }
                                        else
                                        {
                                            gv_divide_Table_2.SetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"], sl_combo - 1);
                                            gv_divide_Table_2.SetRowCellValue(i, gv_divide_Table_2.Columns["__Thanh_Tien"], decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString()) * decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__GIABAN"]).ToString()));
                                        }
                                    }
                                    else if (ssMaCP == "CBC")
                                    {
                                        if (sl_combo <= 1)
                                        {
                                            XoaData_divide_Table2(MaCB);
                                        }
                                        else
                                        {
                                            decimal sl_trenluoi = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                                            decimal sl_trongcombo = decimal.Parse(dtCB2.Select("MA_HANGHOA=" + clsMain.SQLString(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString()))[0]["SOLUONG"].ToString());
                                            gv_divide_Table_2.SetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"], sl_trenluoi - sl_trongcombo);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            XoaData_divide_Table2(MaCB);
                        }         
                    }

                    DataView dv = divide_Table1.DefaultView;
                    dv.Sort = "_MACB ASC, _TEM ASC";
                    divide_Table1 = dv.ToTable();
                    gr_divide_Table_1.DataSource = divide_Table1;
                }
            }
            catch (Exception)
            {

            }
        }

        private void btn_ESCThroughtTable_Click(object sender, EventArgs e)
        {
            try
            {
                ExitDivideTable();
            }
            catch
            {
            }
        }

        private void btn_OkThrough_Click(object sender, EventArgs e)
        {
            try
            {
                if (gv_divide_Table_1.RowCount <= 0 || gv_divide_Table_2.RowCount <= 0)
                    return;
                if (funtions == 1)
                {
                    string maHoaDon = GetMaHoaDon(maBan);
                    string sSQL = "";
                    for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                    {
                        string idDisher = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                        decimal soLuong = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                        int sMaCB = int.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MACB"]).ToString());
                        sSQL = "";
                        sSQL += "EXEC SP_InsertHuyMon " + clsMain.SQLString(maHoaDon) + ",";
                        sSQL += clsMain.SQLString(maBan) + ",";
                        sSQL += clsMain.SQLString(idDisher) + ",";
                        sSQL += clsMain.SQLString(soLuong.ToString()) + ",";
                        sSQL += clsMain.SQLString(clsGlobal.gsUserID);
                        clsMain.ExecuteSQL(sSQL);


                        sSQL = "";
                        sSQL += "Update CT_HOADON Set" + "\n";
                        sSQL += "SOLUONG= SOLUONG - " + soLuong + "\n";
                        sSQL += "Where MA_HOADON=" + clsMain.SQLString(maHoaDon) + "\n";
                        sSQL += "And MA_HANGHOA=" + clsMain.SQLString(idDisher) + "\n";
                        sSQL += "And MACB=" + clsMain.SQLString(sMaCB.ToString()) + "\n";
                        clsMain.ExecuteSQL(sSQL);
                        // Kiểm tra sau khi trừ nếu số lượng =0 thì delete hàng hóa đó khỏi CT HOADON và  DINHLUONG DIEUCHINH và  DISPLAY ORDER
                        sSQL = "";
                        sSQL += "Select MA_HOADON,MA_HANGHOA" + "\n";
                        sSQL += "From CT_HOADON" + "\n";
                        sSQL += "Where SOLUONG= 0" + "\n";
                        sSQL += "And MA_HOADON=" + clsMain.SQLString(maHoaDon) + "\n";
                        sSQL += "And MA_HANGHOA=" + clsMain.SQLString(idDisher);
                        sSQL += "And MACB=" + clsMain.SQLString(sMaCB.ToString()) + "\n";
                        DataTable dt = clsMain.ReturnDataTable(sSQL);
                        if (dt.Rows.Count > 0)
                        {
                            sSQL = "";
                            sSQL += "Select MA_HOADON" + "\n";
                            sSQL += "From DINHLUONG_DIEUCHINH" + "\n";
                            sSQL += "Where MA_HOADON=" + clsMain.SQLString(maHoaDon) + "\n";
                            sSQL += "And MATHANHPHAM=" + clsMain.SQLString(idDisher);
                            dt = clsMain.ReturnDataTable(sSQL);
                            sSQL = "";
                            if (dt.Rows.Count > 0)
                            {
                                sSQL += "Delete DINHLUONG_DIEUCHINH" + "\n";
                                sSQL += "Where MA_HOADON=" + clsMain.SQLString(maHoaDon) + "\n";
                                sSQL += "And MATHANHPHAM=" + clsMain.SQLString(idDisher) + "\n";
                            }
                            sSQL += "Delete from CT_HOADON" + "\n";
                            sSQL += "Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + "\n";
                            sSQL += "And MA_HANGHOA=" + clsMain.SQLString(idDisher) + "\n";
                            sSQL += "And MACB=" + clsMain.SQLString(sMaCB.ToString()) + "\n";

                            sSQL += "Update  DISPLAY_ORDER Set FLAG=0" + "\n";
                            sSQL += "Where MA_HOADON =" + clsMain.SQLString(maHoaDon) + "\n";
                            sSQL += "And MA_HANGHOA=" + clsMain.SQLString(idDisher) + "\n";
                            sSQL += "And MA_BAN=" + clsMain.SQLString(maBan) + "\n";
                            clsMain.ExecuteSQL(sSQL);
                        }
                        else
                        {
                            //code cho man hinh bep
                            sSQL = "";
                            sSQL += "Select *" + "\n";
                            sSQL += "From DISPLAY_ORDER" + "\n";
                            sSQL += "Where TRANGTHAI=0 AND FLAG=1" + "\n";
                            sSQL += "And MA_HOADON=" + clsMain.SQLString(maHoaDon) + "\n";
                            sSQL += "And MA_HANGHOA=" + clsMain.SQLString(idDisher) + "\n";
                            sSQL += "Order by GIOORDER Desc" + "\n";
                            DataTable dtBep = clsMain.ReturnDataTable(sSQL);
                            foreach (DataRow dr in dtBep.Rows)
                            {
                                if (decimal.Parse(dr["SOLUONG"].ToString()) > soLuong)
                                {
                                    sSQL = "";
                                    sSQL += "Update DISPLAY_ORDER Set" + "\n";
                                    sSQL += "SOLUONG= SOLUONG - " + soLuong + "\n";
                                    sSQL += "Where STT=" + clsMain.SQLString(dr["STT"].ToString()) + "\n";
                                    clsMain.ExecuteSQL(sSQL);
                                    break;
                                }
                                else if (decimal.Parse(dr["SOLUONG"].ToString()) == soLuong)
                                {
                                    sSQL += "Update  DISPLAY_ORDER Set FLAG=0" + "\n";
                                    sSQL += "Where STT=" + clsMain.SQLString(dr["STT"].ToString()) + "\n";
                                    clsMain.ExecuteSQL(sSQL);
                                    break;
                                }
                                else
                                {
                                    sSQL += "Update  DISPLAY_ORDER Set FLAG=0" + "\n";
                                    sSQL += "Where STT=" + clsMain.SQLString(dr["STT"].ToString()) + "\n";
                                    clsMain.ExecuteSQL(sSQL);
                                    soLuong = soLuong - decimal.Parse(dr["SOLUONG"].ToString());
                                }
                            }
                        }
                    }
                    // Printer Order 
                    string sTen_ban = "";
                    sSQL = "";
                    sSQL += "Select TEN_BAN From BAN Where MA_BAN=" + clsMain.SQLString(maBan) + "\n";
                    DataTable dtb = clsMain.ReturnDataTable(sSQL);
                    if (dtb.Rows.Count > 0)
                    {
                        sTen_ban = dtb.Rows[0]["TEN_BAN"].ToString();
                    }
                    while (gv_divide_Table_2.RowCount > 0)
                    {
                        List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                        int j = 0;
                        string maBEP = "";
                        for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                        {
                            DM_HangHoa dmHangHoa = new DM_HangHoa();
                            maBEP = gv_divide_Table_2.GetRowCellValue(0, gv_divide_Table_2.Columns["__MA_BEP"]).ToString();
                            if (maBEP == gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_BEP"]).ToString())
                            {
                                dmHangHoa.Ma_HoaDon = maHoaDon;
                                dmHangHoa.Ten_HoaDon = "Hủy Order Bếp";
                                dmHangHoa.MaQuay = cls_ConfigCashier.nameCashier;
                                dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                                dmHangHoa.Ngay = string.Format("{0:dd/MM/yyyy hh:mm:ss}", DateTime.Now);
                                dmHangHoa.TEN_BAN = sTen_ban;
                                dmHangHoa.STT = (++j).ToString();
                                dmHangHoa.MA_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                                dmHangHoa.TEN_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__TEN_HANGHOA"]).ToString();
                                dmHangHoa.SOLUONG = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                                dmHangHoa.IS_INBEP = bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__IS_INBEP"]).ToString());
                                dmHangHoa.GHICHU = "Hủy Món";

                                if (dmHangHoa.IS_INBEP)
                                    listDM_HangHoa.Add(dmHangHoa);

                                if (i != 0)
                                {
                                    gv_divide_Table_2.DeleteRow(i);
                                    i--;
                                }
                            }
                        }
                        gv_divide_Table_2.DeleteRow(0);
                        string print = GetIdKitchen(maBEP);
                        if (print == null)
                            XtraMessageBox.Show(_chuachonmayinchomhnay, _thongbao, MessageBoxButtons.OK);
                        convert cvrt = new convert();
                        DataTable Orderbill = cvrt.ConvertToDataTable(listDM_HangHoa);
                        if (Orderbill.Rows.Count > 0)
                        {


                            Frm_Report1 frm = new Frm_Report1();
                            if (cls_KP_RES.Mode == 3)
                                frm.ReportName = "rpt_Order_1";
                            else
                                frm.ReportName = "rpt_Order";
                            frm.DataSource = Orderbill;
                            frm.WindowState = FormWindowState.Minimized;
                            frm.PrinterName = print;
                            frm.IsPrint = true;
                            frm.ShowDialog();
                            frm.Dispose();
                        }
                    }
                    divide_Table1.Clear();
                    divide_Table2.Clear();
                }
                else if (funtions == 6)
                {
                    //bàn tách là bàn trống
                    int ssMaCB = 0;
                    bool KTCB = false;
                    string sMaHH = "";
                    if (CheckOpenTable(maBanGhep) == false)
                    {
                        string sql = "Update BAN SET ISMO_BAN=1 where MA_BAN =" + int.Parse(maBanGhep);
                        clsMain.ExecuteSQL(sql);

                        string maHoaDon = GetNewCodeBill();
                        string sqlInserter = "Insert Into HOADON(MA_HOADON,SOLUONGKHACH, MANHANVIEN, NGAYTAO, GIOVAO)" + "\n";
                        sqlInserter += "Values(";
                        sqlInserter += clsMain.SQLString(maHoaDon) + ",";
                        sqlInserter += clsMain.SQLString(sokhach.ToString()) + ",";
                        sqlInserter += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                        sqlInserter += "GETDATE(),GETDATE())" + "\n";
                        clsMain.ExecuteSQL(sqlInserter);

                        string sqlCTMOBAN = "Insert into CT_MOBAN(MA_HOADON, MA_BAN)";
                        sqlCTMOBAN += " values(" + "'" + maHoaDon + "'," + int.Parse(maBanGhep) + ")";
                        clsMain.ExecuteSQL(sqlCTMOBAN);

                        for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                        {
                            if (maHoaDon != null)
                            {
                                string sSQL = "SELECT * FROM BAN WHERE MA_BAN =" + int.Parse(maBanGhep) + "";
                                DataTable dt_ban = clsMain.ReturnDataTable(sSQL);
                                DM_HangHoa dmHangHoa = new DM_HangHoa();
                                dmHangHoa.MA_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                                dmHangHoa.TEN_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__TEN_HANGHOA"]).ToString();
                                dmHangHoa.SOLUONG = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                                dmHangHoa.THUE = double.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__THUE"]).ToString());
                                dmHangHoa.GIABAN = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__GIABAN"]).ToString());
                                dmHangHoa.TRANGTHAI = bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__TRANGTHAI"]).ToString());
                                dmHangHoa.IS_COMBO = bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__IS_COMBO"]).ToString());
                                dmHangHoa.IS_INBEP = bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__IS_INBEP"]).ToString());
                                dmHangHoa.CHIETKHAU = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__CHIETKHAU"]).ToString());
                                dmHangHoa.MONTHEM = bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MONTHEM"]).ToString());
                                dmHangHoa.TEM = int.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__TEM"]).ToString());
                                dmHangHoa.Number = dt_ban.Rows[0]["TEN_BAN"].ToString();
                                dmHangHoa.MACP = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MACP"]).ToString();
                                dmHangHoa.MACB = int.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MACB"]).ToString());

                                //món phải là combo
                                DataTable dtCB = GetProductInCombo(dmHangHoa.MA_HANGHOA);
                                if (dtCB.Rows.Count > 0)
                                {
                                    ssMaCB = dmHangHoa.MACB;
                                    //combo có món thêm
                                    KTCB = KT_COMBO_OPTION_MONTHEM(dmHangHoa.MA_HANGHOA);
                                    sMaHH = dmHangHoa.MA_HANGHOA;
                                }
                                //Thêm dòng mới
                                string sqlc = "";
                                sqlc = "Insert into CT_HOADON(MA_HOADON,MA_HANGHOA,SOLUONG,GIABAN,GIABAN_THAT,THUE,TRANGTHAI,IS_COMBO,COMBO_CHINH,CHIETKHAU,TEM,MACP,Number,MACB)";
                                sqlc += "Values (";
                                sqlc += clsMain.SQLString(maHoaDon) + ",";
                                sqlc += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                                sqlc += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                                sqlc += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                                sqlc += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                                sqlc += clsMain.SQLString(dmHangHoa.THUE.ToString()) + ",";
                                sqlc += clsMain.SQLBit(dmHangHoa.TRANGTHAI) + ",";
                                sqlc += clsMain.SQLBit(dmHangHoa.IS_COMBO) + ",";
                                if (dtCB.Rows.Count > 0 || dtCB.Rows.Count == 0 & dmHangHoa.MACB == ssMaCB)
                                    sqlc += clsMain.SQLString(sMaHH) + ",";
                                else
                                    sqlc += clsMain.SQLString("0") + ",";
                                sqlc += clsMain.SQLString(dmHangHoa.CHIETKHAU.ToString()) + ",";
                                sqlc += clsMain.SQLString(dmHangHoa.TEM.ToString()) + ",";
                                sqlc += clsMain.SQLString(dmHangHoa.MACP) + ",";
                                sqlc += clsMain.SQLString(dmHangHoa.Number) + ",";
                                sqlc += clsMain.SQLString(dmHangHoa.MACB.ToString()) + ")";
                                clsMain.ExecuteSQL(sqlc);
                            }
                            else
                            {
                                MessageBox.Show(_tachbanloi);
                            }
                        }
                    }
                    //bàn tách đang mở
                    else
                    {
                        //cập nhật MACB disheJoinTable
                        int sMaCB_TiepTheo = 0;
                        int sMaCB_HienTai = 0;
                        string maHDB = GetMaHoaDon(maBanGhep);
                        string sql = "SELECT * FROM CT_HOADON WHERE MA_HOADON=" + clsMain.SQLString(maHDB);
                        DataTable disheTable = clsMain.ReturnDataTable(sql);
                        if (disheTable.Rows.Count > 0)
                            sMaCB_TiepTheo = int.Parse(disheTable.Rows[disheTable.Rows.Count - 1]["MACB"].ToString()) + 1;
                        if (divide_Table2.Rows.Count > 0)
                        {
                            sMaCB_HienTai = int.Parse(divide_Table2.Rows[0]["__MACB"].ToString());
                            foreach (DataRow dr in divide_Table2.Rows)
                            {
                                if (int.Parse(dr["__MACB"].ToString()) == sMaCB_HienTai)
                                {
                                    dr["__MACB"] = sMaCB_TiepTheo;
                                }
                                else
                                {
                                    sMaCB_HienTai = int.Parse(dr["__MACB"].ToString());
                                    sMaCB_TiepTheo += 1;
                                    dr["__MACB"] = sMaCB_TiepTheo;
                                }
                            }
                        }
                        for (int i = 0; i < gv_divide_Table_2.RowCount; i++)
                        {
                            if (maHDB != null)
                            {
                                string sSQL = "SELECT * FROM BAN WHERE MA_BAN =" + int.Parse(maBanGhep) + "";
                                DataTable dt_ban = clsMain.ReturnDataTable(sSQL);
                                DM_HangHoa dmHangHoa = new DM_HangHoa();
                                dmHangHoa.MA_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MA_HANGHOA"]).ToString();
                                dmHangHoa.TEN_HANGHOA = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__TEN_HANGHOA"]).ToString();
                                dmHangHoa.SOLUONG = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__SOLUONG"]).ToString());
                                dmHangHoa.THUE = double.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__THUE"]).ToString());
                                dmHangHoa.GIABAN = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__GIABAN"]).ToString());
                                dmHangHoa.TRANGTHAI = bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__TRANGTHAI"]).ToString());
                                dmHangHoa.IS_COMBO = bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__IS_COMBO"]).ToString());
                                dmHangHoa.IS_INBEP = bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__IS_INBEP"]).ToString());
                                dmHangHoa.CHIETKHAU = decimal.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__CHIETKHAU"]).ToString());
                                dmHangHoa.MONTHEM = bool.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MONTHEM"]).ToString());
                                dmHangHoa.TEM = int.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__TEM"]).ToString());
                                dmHangHoa.Number = dt_ban.Rows[0]["TEN_BAN"].ToString();
                                dmHangHoa.MACP = gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MACP"]).ToString();
                                dmHangHoa.MACB = int.Parse(gv_divide_Table_2.GetRowCellValue(i, gv_divide_Table_2.Columns["__MACB"]).ToString());

                                //món phải là combo
                                DataTable dtCB = GetProductInCombo(dmHangHoa.MA_HANGHOA);
                                if (dtCB.Rows.Count > 0)
                                {
                                    ssMaCB = dmHangHoa.MACB;
                                    //combo có món thêm
                                    KTCB = KT_COMBO_OPTION_MONTHEM(dmHangHoa.MA_HANGHOA);
                                    sMaHH = dmHangHoa.MA_HANGHOA;
                                }
                                
                                string sqlc = "";
                                //có combo
                                if (dtCB.Rows.Count > 0 || dtCB.Rows.Count == 0 & dmHangHoa.MACB == ssMaCB)
                                {
                                    //món đã có trên bàn chưa?
                                    bool KTHH_In_Table = ChecBishInTable(maHDB, dmHangHoa.MA_HANGHOA, dmHangHoa.IS_COMBO, sMaHH);
                                    //combo có món thêm
                                    if (KTCB == true)
                                    {
                                        sqlc = "Insert into CT_HOADON(MA_HOADON,MA_HANGHOA,SOLUONG,GIABAN,GIABAN_THAT,THUE,TRANGTHAI,IS_COMBO,COMBO_CHINH,CHIETKHAU,TEM,MACP,Number,MACB)";
                                        sqlc += "Values (";
                                        sqlc += clsMain.SQLString(maHDB) + ",";
                                        sqlc += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                                        sqlc += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                                        sqlc += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                                        sqlc += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                                        sqlc += clsMain.SQLString(dmHangHoa.THUE.ToString()) + ",";
                                        sqlc += clsMain.SQLBit(dmHangHoa.TRANGTHAI) + ",";
                                        sqlc += clsMain.SQLBit(dmHangHoa.IS_COMBO) + ",";
                                        if (dtCB.Rows.Count > 0 || dtCB.Rows.Count == 0 & dmHangHoa.MACB == ssMaCB)
                                            sqlc += clsMain.SQLString(sMaHH) + ",";
                                        else
                                            sqlc += clsMain.SQLString("0") + ",";
                                        sqlc += clsMain.SQLString(dmHangHoa.CHIETKHAU.ToString()) + ",";
                                        sqlc += clsMain.SQLString(dmHangHoa.TEM.ToString()) + ",";
                                        sqlc += clsMain.SQLString(dmHangHoa.MACP) + ",";
                                        sqlc += clsMain.SQLString(dmHangHoa.Number) + ",";
                                        sqlc += clsMain.SQLString(dmHangHoa.MACB.ToString()) + ")";
                                        clsMain.ExecuteSQL(sqlc);
                                    }
                                    //combo ko có món thêm
                                    else
                                    {
                                        //thêm dòng mới
                                        if (KTHH_In_Table == false)
                                        {
                                            sqlc = "Insert into CT_HOADON(MA_HOADON,MA_HANGHOA,SOLUONG,GIABAN,GIABAN_THAT,THUE,TRANGTHAI,IS_COMBO,COMBO_CHINH,CHIETKHAU,TEM,MACP,Number,MACB)";
                                            sqlc += "Values (";
                                            sqlc += clsMain.SQLString(maHDB) + ",";
                                            sqlc += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                                            sqlc += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                                            sqlc += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                                            sqlc += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                                            sqlc += clsMain.SQLString(dmHangHoa.THUE.ToString()) + ",";
                                            sqlc += clsMain.SQLBit(dmHangHoa.TRANGTHAI) + ",";
                                            sqlc += clsMain.SQLBit(dmHangHoa.IS_COMBO) + ",";
                                            if (dtCB.Rows.Count > 0 || dtCB.Rows.Count == 0 & dmHangHoa.MACB == ssMaCB)
                                                sqlc += clsMain.SQLString(sMaHH) + ",";
                                            else
                                                sqlc += clsMain.SQLString("0") + ",";
                                            sqlc += clsMain.SQLString(dmHangHoa.CHIETKHAU.ToString()) + ",";
                                            sqlc += clsMain.SQLString(dmHangHoa.TEM.ToString()) + ",";
                                            sqlc += clsMain.SQLString(dmHangHoa.MACP) + ",";
                                            sqlc += clsMain.SQLString(dmHangHoa.Number) + ",";
                                            sqlc += clsMain.SQLString(dmHangHoa.MACB.ToString()) + ")";
                                            clsMain.ExecuteSQL(sqlc);
                                        }
                                        //cập nhật số lượng
                                        else
                                        {
                                            sqlc = "UPDATE CT_HOADON SET " + "\n";
                                            sqlc += "SOLUONG +=" + clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + "," + "\n";
                                            sqlc += "CHIETKHAU =" + clsMain.SQLString(dmHangHoa.CHIETKHAU.ToString()) + "\n";
                                            sqlc += "WHERE MA_HOADON =" + clsMain.SQLString(maHDB) + "\n";
                                            sqlc += "AND Isnull(Is_Combo,0)=" + clsMain.SQLBit(dmHangHoa.IS_COMBO) + "\n";
                                            sqlc += "AND MA_HANGHOA =" + clsMain.SQLString(dmHangHoa.MA_HANGHOA) + "\n";
                                            if (dtCB.Rows.Count > 0)
                                                sqlc += "AND MACP =" + clsMain.SQLString("CB") + "\n";
                                            else
                                                sqlc += "AND MACP =" + clsMain.SQLString("CBC") + "\n";
                                            sqlc += "AND COMBO_CHINH =" + clsMain.SQLString(sMaHH) + "\n";
                                            clsMain.ExecuteSQL(sqlc);
                                        }
                                    }
                                }
                                //ko có combo
                                else
                                {
                                    sMaHH = "";
                                    //món đã có trên bàn chưa?
                                    bool KTHH_In_Table = ChecBishInTable(maHDB, dmHangHoa.MA_HANGHOA, dmHangHoa.IS_COMBO, sMaHH);
                                    //thêm dòng mới
                                    if ((KTHH_In_Table == false)
                                    || (KTHH_In_Table == true & dmHangHoa.TEM != 0 & dmHangHoa.MACP == "C")
                                    || (KTHH_In_Table == true & dmHangHoa.TEM != 0 & dmHangHoa.MACP == "P"))
                                    {
                                        sqlc = "Insert into CT_HOADON(MA_HOADON,MA_HANGHOA,SOLUONG,GIABAN,GIABAN_THAT,THUE,TRANGTHAI,IS_COMBO,COMBO_CHINH,CHIETKHAU,TEM,MACP,Number,MACB)";
                                        sqlc += "Values (";
                                        sqlc += clsMain.SQLString(maHDB) + ",";
                                        sqlc += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                                        sqlc += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                                        sqlc += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                                        sqlc += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                                        sqlc += clsMain.SQLString(dmHangHoa.THUE.ToString()) + ",";
                                        sqlc += clsMain.SQLBit(dmHangHoa.TRANGTHAI) + ",";
                                        sqlc += clsMain.SQLBit(dmHangHoa.IS_COMBO) + ",";
                                        if (dtCB.Rows.Count > 0 || dtCB.Rows.Count == 0 & dmHangHoa.MACB == ssMaCB)
                                            sqlc += clsMain.SQLString(sMaHH) + ",";
                                        else
                                            sqlc += clsMain.SQLString("0") + ",";
                                        sqlc += clsMain.SQLString(dmHangHoa.CHIETKHAU.ToString()) + ",";
                                        sqlc += clsMain.SQLString(dmHangHoa.TEM.ToString()) + ",";
                                        sqlc += clsMain.SQLString(dmHangHoa.MACP) + ",";
                                        sqlc += clsMain.SQLString(dmHangHoa.Number) + ",";
                                        sqlc += clsMain.SQLString(dmHangHoa.MACB.ToString()) + ")";
                                        clsMain.ExecuteSQL(sqlc);
                                    }
                                    //cập nhật số lượng
                                    else
                                    {
                                        sqlc = "UPDATE CT_HOADON SET " + "\n";
                                        sqlc += "SOLUONG +=" + clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + "," + "\n";
                                        sqlc += "CHIETKHAU =" + clsMain.SQLString(dmHangHoa.CHIETKHAU.ToString()) + "\n";
                                        sqlc += "WHERE MA_HOADON =" + clsMain.SQLString(maHDB) + "\n";
                                        sqlc += "AND Isnull(Is_Combo,0)=" + clsMain.SQLBit(dmHangHoa.IS_COMBO) + "\n";
                                        sqlc += "AND MA_HANGHOA =" + clsMain.SQLString(dmHangHoa.MA_HANGHOA) + "\n";
                                        sqlc += "AND MACP =" + clsMain.SQLString("C");
                                        clsMain.ExecuteSQL(sqlc);
                                    }
                                }
                            }
                            else
                            {
                                throw new Exception(_tachbanloi);
                            }
                        }
                    }
                    //ReUpdate table 1
                    string idBill1 = GetMaHoaDon(maBan);
                    string sqldelte = "Delete CT_HOADON where MA_HOADON ='" + idBill1 + "'";
                    clsMain.ExecuteSQL(sqldelte);
                    for (int i = 0; i < gv_divide_Table_1.RowCount; i++)
                    {
                        if (idBill1 != null)
                        {
                            string sSQL = "SELECT * FROM BAN WHERE MA_BAN =" + int.Parse(maBan) + "";
                            DataTable dt_ban = clsMain.ReturnDataTable(sSQL);
                            DM_HangHoa dmHangHoa = new DM_HangHoa();
                            dmHangHoa.MA_HANGHOA = gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MA_HANGHOA"]).ToString();
                            dmHangHoa.TEN_HANGHOA = gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_TEN_HANGHOA"]).ToString();
                            dmHangHoa.SOLUONG = decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_SOLUONG"]).ToString());
                            dmHangHoa.THUE = double.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_THUE"]).ToString());
                            dmHangHoa.GIABAN = decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_GIABAN"]).ToString());
                            dmHangHoa.TRANGTHAI = bool.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_TRANGTHAI"]).ToString());
                            dmHangHoa.IS_COMBO = bool.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_IS_COMBO"]).ToString());
                            dmHangHoa.IS_INBEP = bool.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_IS_INBEP"]).ToString());
                            dmHangHoa.CHIETKHAU = decimal.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_CHIETKHAU"]).ToString());
                            dmHangHoa.MONTHEM = bool.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MONTHEM"]).ToString());
                            dmHangHoa.TEM = int.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_TEM"]).ToString());
                            dmHangHoa.Number = dt_ban.Rows[0]["TEN_BAN"].ToString();
                            dmHangHoa.MACP = gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MACP"]).ToString();
                            dmHangHoa.MACB = int.Parse(gv_divide_Table_1.GetRowCellValue(i, gv_divide_Table_1.Columns["_MACB"]).ToString());

                            //món phải là combo
                            DataTable dtCB = GetProductInCombo(dmHangHoa.MA_HANGHOA);
                            if (dtCB.Rows.Count > 0)
                            {
                                ssMaCB = dmHangHoa.MACB;
                                //combo có món thêm
                                KTCB = KT_COMBO_OPTION_MONTHEM(dmHangHoa.MA_HANGHOA);
                                sMaHH = dmHangHoa.MA_HANGHOA;
                            }
                            //Thêm dòng mới
                            string sqlc = "";
                            sqlc = "Insert into CT_HOADON(MA_HOADON,MA_HANGHOA,SOLUONG,GIABAN,GIABAN_THAT,THUE,TRANGTHAI,IS_COMBO,COMBO_CHINH,CHIETKHAU,TEM,MACP,Number,MACB)";
                            sqlc += "Values (";
                            sqlc += clsMain.SQLString(idBill1) + ",";
                            sqlc += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                            sqlc += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                            sqlc += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                            sqlc += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                            sqlc += clsMain.SQLString(dmHangHoa.THUE.ToString()) + ",";
                            sqlc += clsMain.SQLBit(dmHangHoa.TRANGTHAI) + ",";
                            sqlc += clsMain.SQLBit(dmHangHoa.IS_COMBO) + ",";
                            if (dtCB.Rows.Count > 0 || dtCB.Rows.Count == 0 & dmHangHoa.MACB == ssMaCB)
                                sqlc += clsMain.SQLString(sMaHH) + ",";
                            else
                                sqlc += clsMain.SQLString("0") + ",";
                            sqlc += clsMain.SQLString(dmHangHoa.CHIETKHAU.ToString()) + ",";
                            sqlc += clsMain.SQLString(dmHangHoa.TEM.ToString()) + ",";
                            sqlc += clsMain.SQLString(dmHangHoa.MACP) + ",";
                            sqlc += clsMain.SQLString(dmHangHoa.Number) + ",";
                            sqlc += clsMain.SQLString(dmHangHoa.MACB.ToString()) + ")";
                            clsMain.ExecuteSQL(sqlc);
                        }
                        else
                        {
                            XtraMessageBox.Show(_tachbanloi, _thongbao, MessageBoxButtons.OK);
                        }
                    }
                }
                divide_Table1.Rows.Clear();
                divide_Table2.Rows.Clear();
                ExitDivideTable();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString(), _thongbao, MessageBoxButtons.OK);
            }
        }

        private void btn_ExitDishe_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
                return;
            if (maBan == "")
                return;
            if (CheckOpenTable(maBan) == false)
                return;

            if (exitReprint == true)
            {
                SetDefautlBackgroud(btn_HuyMon);
                ExitDivideTable();
                divide_Table1.Clear();
                divide_Table2.Clear();
            }
            else
            {
                if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
                {
                    bool check;
                    Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                    fr.ShowDialog();
                    check = fr.kt;
                    fr.Dispose();
                    if (check == false)
                        return;
                }

                SetBackgroudOnClick(btn_HuyMon);
                pnInforTablecheck.Visible = false;
                pnTachban.BringToFront();
                int w = (pn_GroupTable.Width - 110) / 2;
                panelControl15.Width = w;
                pn_TableTow.Width = w;
                List<DM_ListDisheTableOne> lisHangHoa = new List<DM_ListDisheTableOne>();
                for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                {
                    DM_ListDisheTableOne dmHangHoa = new DM_ListDisheTableOne();
                    dmHangHoa._MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                    dmHangHoa._TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                    dmHangHoa._SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                    dmHangHoa._GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                    dmHangHoa._MA_BEP = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_BEP"]).ToString());
                    dmHangHoa._Thanh_Tien = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"]).ToString());
                    dmHangHoa._GHICHU = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"]).ToString();
                    dmHangHoa._MONTHEM = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MONTHEM"]).ToString());
                    dmHangHoa._IS_INBEP = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["IS_INBEP"]).ToString());
                    dmHangHoa._TEM = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEM"]).ToString());
                    dmHangHoa._MACP = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MACP"]).ToString();
                    dmHangHoa._MACB = int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MACB"]).ToString());
                    dmHangHoa._INTEM = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["INTEM"]).ToString());
                    dmHangHoa._COMBO_CHINH = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["COMBO_CHINH"]).ToString();
                    dmHangHoa._THUE = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["THUE"]).ToString());
                    dmHangHoa._CHIETKHAU = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["CHIETKHAU"]).ToString());
                    dmHangHoa._TRANGTHAI = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TRANGTHAI"]).ToString());
                    dmHangHoa._IS_XULY = false;
                    lisHangHoa.Add(dmHangHoa);
                }
                convert cvrt = new convert();
                divide_Table1 = cvrt.ConvertToDataTable(lisHangHoa);
                gr_divide_Table_1.DataSource = divide_Table1;
                gv_divide_Table_1.FocusedRowHandle = 0;

                foreach (SimpleButton btn in pnNghiepVu.Controls)
                {
                    if (btn.Name == btn_HuyMon.Name)
                    {
                        btn.Visible = true;
                    }
                    else
                    {
                        btn.Visible = false;
                    }
                }
                exitReprint = true;
            }
        }

        private void btnGiaBan2_Click(object sender, EventArgs e)
        {
            if (funtions != 1 || gv_SelectProduct.RowCount <= 0 || maBan == "")
                return;

            if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
            {
                bool check;
                Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                fr.ShowDialog();
                check = fr.kt;
                fr.Dispose();
                if (check == false)
                    return;
            }

            string smaMon = gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
            string maHD = GetMaHoaDon(maBan);

            //Kiểm tra nếu đang dùng bản giá theo khu hay cửa hàng thì return
            string sSQL = "select GIATRI from CAUHINH where TEN='APDUNGBANGGIATHEOKHUVUC'";
            DataTable dtcheck = clsMain.ReturnDataTable(sSQL);
            if (dtcheck.Rows.Count > 0)
            {
                if (dtcheck.Rows[0]["GIATRI"].ToString() != "0")
                {
                    MessageBox.Show("Đang dùng chương trình tính giá theo khu hay cửa hàng. Không áp dụng được giá bán 2", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }

            //Kiểm tra hàng hóa là thời giá hay ko,nếu đúng thì return
            sSQL = "Select ISNULL(SUAGIA,0) AS SUAGIA From HANGHOA Where MA_HANGHOA = " + clsMain.SQLString(smaMon);
            dtcheck = clsMain.ReturnDataTable(sSQL);
            if (dtcheck.Rows.Count > 0)
            {
                if (bool.Parse(dtcheck.Rows[0]["SUAGIA"].ToString()))
                {
                    MessageBox.Show(_daylahhtheothoigia, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }

            //lây giá bán 1,2
            sSQL = "select  ISNULL(GIABAN1,0) as GIABAN1,ISNULL(GIABAN2,0) as GIABAN2 from HANGHOA where MA_HANGHOA = " + clsMain.SQLString(smaMon);
            DataTable dtgiaban = clsMain.ReturnDataTable(sSQL);
            if (dtgiaban.Rows.Count > 0)
            {
                if (gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["GIABAN"]).ToString() == dtgiaban.Rows[0]["GIABAN1"].ToString())
                {
                    //gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"], dtgiaban.Rows[0]["GIABAN2"].ToString());
                    sSQL = "";
                    sSQL += "UPDATE CT_HOADON SET " + "\n";
                    sSQL += "GIABAN =" + clsMain.SQLString(dtgiaban.Rows[0]["GIABAN2"].ToString()) + "\n";
                    sSQL += "WHERE MA_HOADON =" + clsMain.SQLString(maHD) + "\n";
                    sSQL += "AND Isnull(Is_Combo,0)=" + clsMain.SQLBit(false) + "\n";
                    sSQL += "AND MA_HANGHOA =" + clsMain.SQLString(smaMon) + "\n";
                    sSQL += "AND GIABAN =" + clsMain.SQLString(gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["GIABAN"]).ToString()) + "\n";
                    clsMain.ExecuteSQL(sSQL);
                }
                else
                {
                    //gv_SelectProduct.SetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"], dtgiaban.Rows[0]["GIABAN1"].ToString());
                    sSQL = "";
                    sSQL += "UPDATE CT_HOADON SET " + "\n";
                    sSQL += "GIABAN =" + clsMain.SQLString(dtgiaban.Rows[0]["GIABAN1"].ToString()) + "\n";
                    sSQL += "WHERE MA_HOADON =" + clsMain.SQLString(maHD) + "\n";
                    sSQL += "AND Isnull(Is_Combo,0)=" + clsMain.SQLBit(false) + "\n";
                    sSQL += "AND MA_HANGHOA =" + clsMain.SQLString(smaMon) + "\n";
                    sSQL += "AND GIABAN =" + clsMain.SQLString(gv_SelectProduct.GetFocusedRowCellValue(gv_SelectProduct.Columns["GIABAN"]).ToString()) + "\n";
                    clsMain.ExecuteSQL(sSQL);
                }
                ExitDivideTable();
            }
        }

        private void btn_InlaiOrder_Click(object sender, EventArgs e)
        {
            if (funtions == 1)
            {
                // Check Table Opened or table paymented
                if (maBan == "")
                    return;
                if (gv_SelectProduct.RowCount <= 0)
                    return;
                string maHDB = GetMaHoaDon(maBan);
                if (maHDB == "")
                    return;

                string nameTable = clsMain.ReturnDataTable("Select TEN_BAN From BAN Where MA_BAN = " + clsMain.SQLString(maBan) + "").Rows[0][0].ToString();
                string solan = "";
                string sSQL = "";
                sSQL += "Select LANIN From CT_INLAI Where MA_HOADON=" + clsMain.SQLString(maHDB);
                DataTable dtinlai = clsMain.ReturnDataTable(sSQL);
                if (dtinlai.Rows.Count <= 0)
                {
                    sSQL = "";
                    sSQL += "INSERT INTO CT_INLAI(MA_HOADON,LANIN)" + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(maHDB) + ",";
                    sSQL += clsMain.SQLString(1.ToString()) + ")";
                    clsMain.ExecuteSQL(sSQL);
                    solan = "1";
                }
                else
                {
                    solan = (int.Parse(dtinlai.Rows[0]["LANIN"].ToString()) + 1).ToString();
                    sSQL = "";
                    sSQL += "UPDATE CT_INLAI SET" + "\n";
                    sSQL += "LANIN = " + clsMain.SQLString(solan) + "\n";
                    sSQL += "WHERE MA_HOADON=" + clsMain.SQLString(maHDB);
                    clsMain.ExecuteSQL(sSQL);
                }

                while (gv_SelectProduct.RowCount > 0)
                {
                    List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                    int j = 0;
                    string maBEP = "";
                    for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                    {
                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        maBEP = gv_SelectProduct.GetRowCellValue(0, gv_SelectProduct.Columns["MA_BEP"]).ToString();
                        dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                        if (maBEP == gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_BEP"]).ToString())
                        {
                            dmHangHoa.Ma_HoaDon = maHDB;
                            dmHangHoa.Ten_HoaDon = _Orderinlailan + " " + solan;
                            dmHangHoa.TEN_BAN = nameTable;
                            dmHangHoa.MaQuay = cls_ConfigCashier.nameCashier;
                            dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                            dmHangHoa.STT = (++j).ToString();
                            dmHangHoa.MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                            dmHangHoa.TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                            dmHangHoa.SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                            dmHangHoa.THUE = double.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["THUE"]).ToString());
                            dmHangHoa.GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                            dmHangHoa.IS_INBEP = bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["IS_INBEP"]).ToString());
                            string repuest = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GHICHU"]).ToString();
                            if (repuest == "a")
                                dmHangHoa.GHICHU = "";
                            else
                                dmHangHoa.GHICHU = repuest;

                            if (dmHangHoa.IS_INBEP)//nếu hàng hóa được in bếp mới cho in xuống bếp
                                listDM_HangHoa.Add(dmHangHoa);
                            // Delete product selected 
                            if (i != 0)
                            {
                                gv_SelectProduct.DeleteRow(i);
                                i--;
                            }
                        }
                    }
                    string print = GetIdKitchen(maBEP);
                    if (print == null)
                        XtraMessageBox.Show(_chuachonmayinchomhnay, _thongbao, MessageBoxButtons.OK);
                    gv_SelectProduct.DeleteRow(0);
                    // Printer Order 
                    convert cvrt = new convert();
                    DataTable Orderbill = cvrt.ConvertToDataTable(listDM_HangHoa);
                    if (Orderbill.Rows.Count > 0)
                    {
                        Frm_Report1 frm = new Frm_Report1();
                        if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                            frm.ReportName = "rpt_Order_58";
                        else
                        {
                            if (cls_KP_RES.Mode == 3)
                            {
                                frm.ReportName = "rpt_Order_1";
                            }
                            else
                            {
                                frm.ReportName = "rpt_Order";
                            }
                        }
                        frm.DataSource = Orderbill;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.PrinterName = print;
                        frm.IsPrint = true;
                        frm.ShowDialog();
                        frm.Dispose();
                    }
                }
                ExitDivideTable();
            }
        }

        private void btn_TamTinh_Click(object sender, EventArgs e)
        {
            try
            {
                btnComboOption.Visible = false;
                pnBtnDeleteGv.Visible = false;
                if (funtions != 1)
                    return;
                if (maBan == "")
                    return;
                if (gv_SelectProduct.RowCount <= 0)
                    return;

                //Lấy mã hóa don
                string idBill = GetMaHoaDon(maBan);
                if (idBill == "")
                    return;
                //Lấy tiền đặt cọc
                decimal Tiendatcoc = 0;
                DataTable dtMoney = clsMain.ReturnDataTable("Select ISNULL(TIENDATCOC,0) AS TIENDATCOC From HOADON Where MA_HOADON=" + clsMain.SQLString(idBill));
                if (dtMoney.Rows.Count > 0)
                {
                    Tiendatcoc = decimal.Parse(dtMoney.Rows[0]["TIENDATCOC"].ToString());
                }
                else
                {
                    Tiendatcoc = 0;
                }
                //Lấy danh sách hàng hóa
                DataTable _dt = null;
                _dt = gr_SelectProduct.DataSource as DataTable;
                //Gởi form thanh toán
                bool _check;
                clsKP_Terminal.ShowCustomerDisplayOneLine("Tổng cộng" + " : " + lbTotal.Text + " VND");
                Frm_Payment_Nhahang_Tamtinh frm = new Frm_Payment_Nhahang_Tamtinh(maBan, idBill, _dt, Tiendatcoc, tenBan);
                frm.ShowDialog();
                _check = frm.kt;
                frm.Dispose();
                // if payment successful
                if (_check == true)
                {
                    ExitDivideTable();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK);
            }
        }

        private void btn_Payment_Click(object sender, EventArgs e)
        {
            try
            {
                btnComboOption.Visible = false;
                pnBtnDeleteGv.Visible = false;
                if (funtions != 1)
                    return;
                if (maBan == "")
                    return;
                if (gv_SelectProduct.RowCount <= 0)
                    return;
                //Lấy mã hóa don
                string idBill = GetMaHoaDon(maBan);
                if (idBill == "")
                    return;
                //Lấy tiền đặt cọc
                decimal Tiendatcoc = 0;
                DataTable dtMoney = clsMain.ReturnDataTable("Select ISNULL(TIENDATCOC,0) AS TIENDATCOC From HOADON Where MA_HOADON=" + clsMain.SQLString(idBill));
                if (dtMoney.Rows.Count > 0)
                {
                    Tiendatcoc = decimal.Parse(dtMoney.Rows[0]["TIENDATCOC"].ToString());
                }
                else
                {
                    Tiendatcoc = 0;
                }
                //Lấy danh sách hàng hóa
                DataTable _dt = null;
                _dt = gr_SelectProduct.DataSource as DataTable;
                //Gởi form thanh toán
                bool _check;
                clsKP_Terminal.ShowCustomerDisplayOneLine("Tổng cộng" + " : " + lbTotal.Text + " VND");
                Frm_Payment_Nhahang frm = new Frm_Payment_Nhahang(maBan, idBill, _dt, Tiendatcoc, tenBan);
                frm.ShowDialog();
                _check = frm.bHoanTacThanhToan;
                frm.Dispose();
                // if payment successful
                if (_check == true)
                {
                    ExitDivideTable();
                    KP_Terminal.clsKP_Terminal.OpenCashDrawer(true);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK);
            }
        }

        private void btn_InLaiHoaDon_Click(object sender, EventArgs e)
        {
            pnBtnDeleteGv.Visible = false;
            btnComboOption.Visible = false;
            if (funtions != 1)
                return;

            if (exitReprint == true)
            {
                SetDefautlBackgroud(btn_InLaiHoaDon);
                ExitDivideTable();
            }
            else
            {
                if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
                {
                    bool check;
                    Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                    fr.ShowDialog();
                    check = fr.kt;
                    fr.Dispose();
                    if (check == false)
                        return;
                }

                string sSQL = "exec SP_DANHSACHHOADON";
                DataTable dts = clsMain.ReturnDataTable(sSQL);
                gr_billPaymented.DataSource = dts;
                gr_billPaymented.RefreshDataSource();

                SetBackgroudOnClick(btn_InLaiHoaDon);
                IN.Visible = true;
                HUY.Visible = false;
                LAYLAI.Visible = false;
                pnInlaihoadon.BringToFront();

                foreach (SimpleButton btn in pnNghiepVu.Controls)
                {
                    if (btn.Name != btn_InLaiHoaDon.Name)
                    {
                        btn.Visible = false;
                    }
                }
                exitReprint = true;
            }
        }

        private void btn_LayLaiHoaDon_Click(object sender, EventArgs e)
        {
            pnBtnDeleteGv.Visible = false;
            btnComboOption.Visible = false;
            if (funtions != 1)
                return;

            if (exitReprint == true)
            {
                SetDefautlBackgroud(btn_LayLaiHoaDon);
                ExitDivideTable();
            }
            else
            {
                if (cls_ConfigCashier.sDungmatkhaubanhang != "0")
                {
                    bool check;
                    Frm_ConfigFuntion fr = new Frm_ConfigFuntion();
                    fr.ShowDialog();
                    check = fr.kt;
                    fr.Dispose();
                    if (check == false)
                        return;
                }

                string sSQL = "exec SP_DANHSACHHOADON";
                DataTable dts = clsMain.ReturnDataTable(sSQL);
                gr_billPaymented.DataSource = dts;
                gr_billPaymented.RefreshDataSource();

                SetBackgroudOnClick(btn_LayLaiHoaDon);
                LAYLAI.Visible = true;
                HUY.Visible = false;
                IN.Visible = false;
                pnInlaihoadon.BringToFront();
                foreach (SimpleButton btn in pnNghiepVu.Controls)
                {
                    if (btn.Name != btn_LayLaiHoaDon.Name)
                    {
                        btn.Visible = false;
                    }
                }
                exitReprint = true;
            }
        }

        private void btn_upbill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = gvBillPaymented.FocusedRowHandle - 1;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
                if (gvBillPaymented.FocusedRowHandle == 0)
                {
                    btn_up.Enabled = false;
                    btn_UpFirst.Enabled = false;
                }
            }
            catch
            {
            }

        }

        private void btn_UpTopBill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = 0;
                btn_UpFirst.Enabled = false;
                btn_up.Enabled = false;
                btn_Dow.Enabled = true;
                btn_DowLast.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnDowBill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = gvBillPaymented.FocusedRowHandle + 1;
                btn_up.Enabled = true;
                btn_UpFirst.Enabled = true;
                if (gvBillPaymented.FocusedRowHandle == gvBillPaymented.RowCount - 1)
                {
                    btn_Dow.Enabled = false;
                    btn_DowLast.Enabled = false;
                }
            }
            catch
            {

            }
        }

        private void btnDowLastBill_Click(object sender, EventArgs e)
        {
            try
            {
                gvBillPaymented.Focus();
                gvBillPaymented.FocusedRowHandle = gvBillPaymented.RowCount - 1;
                btn_UpFirst.Enabled = true;
                btn_up.Enabled = true;
                btn_Dow.Enabled = false;
                btn_DowLast.Enabled = false;
            }
            catch
            {
            }
        }

        private void gvBillPaymented_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column == SSTT)
                {
                    e.DisplayText = Convert.ToString(e.RowHandle + 1);
                }
                lbTongSoRow.Text = gvBillPaymented.RowCount.ToString("00");
                labelControl45.Text = (gvBillPaymented.FocusedRowHandle + 1).ToString("00");
            }
        }

        string idBill = "";
        private void gvBillPaymented_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            try
            {
                idBill = gvBillPaymented.GetRowCellValue(gvBillPaymented.FocusedRowHandle, gvBillPaymented.Columns["MA_HOADON"]).ToString(); ;
                GetDishOfTable(idBill);
                GetDistcountABill(idBill);

                string sSQL;
                if (e.Column == IN)
                {
                    sSQL = "";
                    sSQL += "Insert into HOADON_INLAI(MA_HOADON,NHANVIEN_INLAI,GIO_INLAI)" + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(idBill) + ",";
                    sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                    sSQL += "GETDATE())";
                    clsMain.ExecuteSQL(sSQL);

                    sSQL = "";
                    sSQL += "Select ROW_NUMBER() over (order by (select 1)) as STT,'' as TenNV ,ISNULL(cthd.CHIETKHAU,0) AS CHIETKHAU,";
                    sSQL += "cthd.GIABAN, (cthd.SOLUONG * cthd.GIABAN) - (cthd.SOLUONG * cthd.GIABAN) * ISNULL(cthd.CHIETKHAU,0) / 100 as Thanh_Tien, hd.NGAYTAO, ISNULL(hd.GIORA,hd.NGAYTAO) as ngayHD,hh.TEN_HANGHOA, cthd.SOLUONG,";
                    sSQL += "hd.PHIPHUCVU as Phiphucvu,hd.MA_HOADON,TEN_QUAY as MA_QUAY, ISNULL(hd.TIENDATCOC,0) as TIENDATCOC,";
                    sSQL += "case when ISNULL(hd.NHANVIEN_RABILL,'')='' Then (hd.MANHANVIEN) Else (hd.NHANVIEN_RABILL) End as MANHANVIEN,";
                    sSQL += "ISNULL(cthd.TEM,0) AS TEM, ISNULL(cthd.MACP,'C') AS MACP, ISNULL(cthd.Number,0) AS Number," + "\n";
                    if (cls_KP_RES.Mode == 4)
                    {
                        sSQL += "hd.GIAMGIA as Giam_Gia1, hd.TONGTIEN as Tong_Cong, hd.TIEN_KHACHTRA, hd.TIEN_TRAKHACH, hd.TONGTIEN, N'PHIẾU IN LẠI' as Ten_HoaDon" + "\n";
                    }
                    else
                    {
                        sSQL += "hd.GIAMGIA as Giam_Gia1, hd.TONGTIEN as Tong_Cong, hd.TIEN_KHACHTRA, hd.TIEN_TRAKHACH, hd.TONGTIEN, N'PHIẾU THANH TOÁN \n (IN LẠI)' as Ten_HoaDon" + "\n";
                    }                    
                    sSQL += "From HANGHOA hh inner join CT_HOADON cthd on hh.MA_HANGHOA = cthd.MA_HANGHOA inner join HOADON hd on cthd.MA_HOADON = hd.MA_HOADON" + "\n";
                    sSQL += "inner join QUAY q on hd.MA_QUAY = q.MA_QUAY" + "\n";
                    sSQL += "Where hd.MA_HOADON ='" + idBill + "'";
                    DataTable dtReport = clsMain.ReturnDataTable(sSQL);
                    if (dtReport.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtReport.Rows.Count; i++)
                        {
                            if (dtReport.Rows[i]["MACP"].ToString() == "P")
                            {
                                dtReport.Rows[i]["TEN_HANGHOA"] = Kytu_Monthem + dtReport.Rows[i]["TEN_HANGHOA"].ToString();
                            }
                            else if (dtReport.Rows[i]["MACP"].ToString() == "CBC")
                            {
                                dtReport.Rows[i]["TEN_HANGHOA"] = Kytu_Combo + dtReport.Rows[i]["TEN_HANGHOA"].ToString();
                            }
                        }

                        string sTenNV = "";
                        string sTenBan = "";
                        sSQL = "Select TENNHANVIEN From DM_NHANVIEN Where MANHANVIEN=" + clsMain.SQLString(dtReport.Rows[0]["MANHANVIEN"].ToString());
                        DataTable dtTenNV = clsMain.ReturnDataTable(sSQL);
                        if (dtTenNV.Rows.Count > 0)
                        {
                            sTenNV = dtTenNV.Rows[0]["TENNHANVIEN"].ToString();
                        }
                        sSQL = "Select TEN_BAN From CT_MOBAN ct,BAN b Where ct.MA_BAN=b.MA_BAN And ct.MA_HOADON=" + clsMain.SQLString(idBill);
                        DataTable dtTenban = clsMain.ReturnDataTable(sSQL);
                        if (dtTenban.Rows.Count > 0)
                        {
                            sTenBan = dtTenban.Rows[0]["TEN_BAN"].ToString();
                        }
                        dtReport.Columns.Add("Giam_Gia", typeof(String));
                        dtReport.Columns.Add("TEN_BAN", typeof(String));
                        for (int i = 0; i < dtReport.Rows.Count; i++)
                        {
                            dtReport.Rows[i]["TEN_BAN"] = sTenBan;
                            dtReport.Rows[i]["TenNV"] = sTenNV;
                            if (double.Parse(dtReport.Rows[i]["Giam_Gia1"].ToString()) > 100)
                                dtReport.Rows[i]["Giam_Gia"] = string.Format("{0:#,###0}", dtReport.Rows[i]["Giam_Gia1"]);
                            else
                                dtReport.Rows[i]["Giam_Gia"] = dtReport.Rows[i]["Giam_Gia1"].ToString() + "%";
                        }

                        Frm_Report1 frm = new Frm_Report1();
                        if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                        {
                            if (cls_ConfigCashier.sIn_Number_Fastfood != "0")
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_ban_TraSua_58";
                            }
                            else
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_ban_58";
                            }
                            
                        }
                        else
                        {
                            if (cls_KP_RES.Mode == 3)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_ban_1";
                            }
                            else if (cls_KP_RES.Mode == 4)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_ban_pho";
                            }
                            else if (cls_KP_RES.Mode == 5)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_ban_2";
                            }
                            else if (cls_KP_RES.Mode == 9)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_ban_image";
                            }
                            else if (cls_ConfigCashier.sIn_Number_Fastfood != "0")
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_ban_TraSua";
                            }
                            else
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_ban";
                            }
                        }
                        frm.DataSource = dtReport;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                        frm.IsPrint = true;
                        frm.ShowDialog();
                        frm.Dispose();
                        dtLuoi.Rows.Clear();
                        lbTotal.Text = "0";
                        idBill = "";
                    }
                }
                else if (e.Column == LAYLAI)
                {
                    //Kiểm tra bàn có đang trống
                    DataTable dtTables = clsMain.ReturnDataTable("Select MA_BAN From CT_MOBAN where MA_HOADON =" + clsMain.SQLString(idBill));
                    if (dtTables.Rows.Count > 0)
                    {
                        DataTable dtCheckban;
                        for (int i = 0; i < dtTables.Rows.Count; i++)
                        {
                            dtCheckban = clsMain.ReturnDataTable("Select ISMO_BAN From BAN Where MA_BAN =" + clsMain.SQLString(dtTables.Rows[i]["MA_BAN"].ToString()));
                            if (dtCheckban.Rows.Count > 0)
                            {
                                if (bool.Parse(dtCheckban.Rows[0]["ISMO_BAN"].ToString()))
                                {
                                    XtraMessageBox.Show(_bandangmokhongthelaylaihd, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return;
                                }
                            }
                        }
                    }

                    //nhập lý do
                    Frm_GhiChu frm = new Frm_GhiChu(GhiChu);
                    frm.ShowDialog();
                    GhiChu = frm.sGhiChu;
                    frm.Dispose();
                    if (GhiChu.Trim() == "")
                    {
                        return;
                    }
                    sSQL = "";
                    sSQL += "EXEC SP_InsertDSLayLaiHD " + clsMain.SQLString(idBill) + "," + clsMain.SQLString(dtTables.Rows[0]["MA_BAN"].ToString()) + "," + clsMain.SQLString(clsGlobal.gsUserID) + "," + clsMain.SQLStringUnicode(GhiChu) + "";
                    clsMain.ExecuteSQL(sSQL);

                    //hóa đơn
                    sSQL = "";
                    sSQL = "Update HOADON set ISPAYMENT=0, GIAMGIA=" + 0 + ", TIEN_TRAKHACH =" + 0 + ", TIEN_KHACHTRA =" + 0 + ", TONGTIEN=" + 0 + "  where MA_HOADON=" + clsMain.SQLString(idBill);
                    clsMain.ExecuteSQL(sSQL);
                    //Bàn
                    if (dtTables.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtTables.Rows.Count; i++)
                        {
                            sSQL = "Update BAN SET ISMO_BAN=1 where MA_BAN =" + clsMain.SQLString(dtTables.Rows[i]["MA_BAN"].ToString());
                            clsMain.ExecuteSQL(sSQL);
                        }
                    }
                    //Kho
                    sSQL = string.Format("Exec SP_TRUKHO_DINHLUONG @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 0);
                    clsMain.ExecuteSQL(sSQL);
                    sSQL = string.Format("Exec SP_TRUKHO_HOADON @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 0);
                    clsMain.ExecuteSQL(sSQL);
                    // KHTT -TTNB
                    sSQL = "Select ISNULL(MA_KHACHHANG,'') AS MA_KHACHHANG,TONGTIEN,GIAMGIA From HOADON Where MA_HOADON =" + clsMain.SQLString(idBill);
                    DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                    if (dt1.Rows.Count > 0)
                    {
                        if (dt1.Rows[0]["MA_KHACHHANG"].ToString() != "")
                        {
                            DataTable dtKH = cls_KHTT.ReturnCauHinhKHTT();
                            if (dtKH.Rows[0]["GIATRI"].ToString() == "1")
                            {
                                int tem = 100 - int.Parse(dt1.Rows[0]["GIAMGIA"].ToString());
                                decimal tongtien = decimal.Parse(dt1.Rows[0]["TONGTIEN"].ToString()) / tem * 100;
                                cls_KHTT.TruDiemKhachHangThanThiet(dt1.Rows[0]["MA_KHACHHANG"].ToString(), tongtien);
                                cls_KHTT.TruTien_TTTNB(dt1.Rows[0]["MA_KHACHHANG"].ToString(), tongtien, "Lấy lại hóa đơn");
                            }
                            else
                            {
                                cls_KHTT.TruDiemKhachHangThanThiet(dt1.Rows[0]["MA_KHACHHANG"].ToString(), decimal.Parse(dt1.Rows[0]["TONGTIEN"].ToString()));
                                cls_KHTT.TruTien_TTTNB(dt1.Rows[0]["MA_KHACHHANG"].ToString(), decimal.Parse(dt1.Rows[0]["TONGTIEN"].ToString()), "Lấy lại hóa đơn");
                            }
                        }
                    }
                    //Công nợ
                    sSQL = "EXEC SP_CN_HUYHOADON '" + idBill + "'";
                    clsMain.ExecuteSQL(sSQL);

                    SetDefautlBackgroud(btn_LayLaiHoaDon);
                    ExitDivideTable();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK);
            }
        }

        private void GetDistcountABill(string maHoaDon)
        {
            string sql = " select TIENDATCOC, GIAMGIA, PHIPHUCVU from HOADON  where MA_HOADON ='" + maHoaDon + "'";
            var dtDistcount = clsMain.ReturnDataTable(sql);
            lbPhuThu.Text = string.Format("{0:#,###0}", double.Parse(dtDistcount.Rows[0]["PHIPHUCVU"].ToString()));
            lbGiamGia.Text = string.Format("{0:#,###0}", double.Parse(dtDistcount.Rows[0]["GIAMGIA"].ToString()));
        }

        int selectDishes = 0;
        int selectTable = 0;
        private void btn_DatBan_Click(object sender, EventArgs e)
        {
            if (funtions == 1)
            {
                funtions = 9;
                SetBackgroudOnClick(btn_DatBan);
                foreach (SimpleButton btn in pnNghiepVu.Controls)
                {
                    if (btn.Name == btn_DatBan.Name)
                    {
                        btn.Visible = true;
                    }
                    else
                    {
                        btn.Visible = false;
                    }
                }
                pnDatban.BringToFront();
                txtDBTenKH.Focus();
                us_keyboard2.SetTextbox(txtDBTenKH);
                txtngay.EditValue = clsGlobal.gdServerDate;
                cbHH.Text = "00";
                cbMM.Text = "00";
                selectTable = 1;
            }
            else if (funtions == 9)
            {
                //ExitDivideTable();
                selectDishes = 0;
                pnDatban.BringToFront();
                return;
            }
        }

        private void txtDBTenKH_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(txtDBTenKH);
        }

        private void txtAddress_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(txtAddress);
        }

        private void txtDienThoai_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(txtDienThoai);
        }

        private void txtEmail_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(txtEmail);
        }

        private void cbHH_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(cbHH);
        }

        private void cbMM_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(cbMM);
        }

        private void txtPartMoneyPayments_Click(object sender, EventArgs e)
        {
            us_keyboard2.SetTextbox(txtPartMoneyPayments);
        }

        private void txtngay_TextChanged(object sender, EventArgs e)
        {
            flCheckTable.Controls.Clear();
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            if (funtions != 1)
            {
                pnShowHang.BringToFront();
                txtDBTenKH.Text = "";
                txtDienThoai.Text = "";
                txtAddress.Text = "";
                txtPartMoneyPayments.Text = "";
                flCheckTable.Controls.Clear();
                KhoamoControl_Chuyenban(false);
                flagdatban = false;
                ExitDivideTable();
            }
        }

        private void btn_SelectTable_Click(object sender, EventArgs e)
        {
            if (txtngay.Text == "" || cbHH.Text == "")
                return;
            DateTime now = DateTime.Parse(DateTime.Now.ToShortDateString());
            DateTime x = DateTime.Parse(((DateTime)txtngay.EditValue).ToShortDateString());
            int outline = DateTime.Compare(x, now);
            if (outline < 0)
            {
                XtraMessageBox.Show(_ngaydbkhonghople, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                if (int.Parse(cbHH.Text) < int.Parse(DateTime.Now.Hour.ToString()) && outline == 0)
                {
                    XtraMessageBox.Show(_chuachontgden, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            funtions = 1;
            if (funtions == 1)
            {
                pnShowHang.BringToFront();
                btn_SoDoBan.Text = _sdb;
                GetKhuVuc();
                GetSoDoBan();
                foreach (SimpleButton btn in pnKV_Mon.Controls)
                {
                    if (btn.Name == maKV.ToString())
                    {
                        SetBackgroudOnClick(btn);
                    }
                }
                selectDishes = 2;
                funtions = 9;
            }

            //Frm_Danhsachban frm = new Frm_Danhsachban();
            //frm.selectDishes = 2;
            //frm.funtions = 9;
            //frm.txtngay = x;
            //frm.ShowDialog();
            //frm.Dispose();
        }

        private void btn_SelectDishes_Click(object sender, EventArgs e)
        {
            funtions = 2;
            pnShowHang.BringToFront();
            btn_SoDoBan.Text = _mb;
            GetNhomhang();
            GetListMon();
            foreach (SimpleButton btn in pnKV_Mon.Controls)
            {
                if (btn.Name == maNH.ToString())
                {
                    SetBackgroudOnClick(btn);
                }
            }
            funtions = 9;
            selectDishes = 1;
        }

        string idCheckTable = "";
        private void btnRemove_Click(object sender, EventArgs e)
        {
            foreach (SimpleButton btnBan in flCheckTable.Controls)
            {
                if (btnBan.Name == idCheckTable)//ActiveControl.Name
                {
                    flCheckTable.Controls.Remove(btnBan);
                    idCheckTable = "";
                }
            }
        }

        private void btnSendEmail_Click(object sender, EventArgs e)
        {

        }

        private void btnMoBanNgay_Click(object sender, EventArgs e)
        {
            if (_flag_chuyenban)
            {
                string sSQL = "";
                sSQL += "Delete CT_MOBAN Where MA_HOADON=" + clsMain.SQLString(_mahoadon_chuyenban);
                clsMain.ExecuteSQL(sSQL);
                foreach (SimpleButton btn in flCheckTable.Controls)
                {
                    string[] arr = btn.Text.Split('-');
                    sSQL = "";
                    sSQL += "Insert into CT_MOBAN(MA_HOADON, MA_BAN)" + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(_mahoadon_chuyenban) + ",";
                    sSQL += clsMain.SQLString(btn.Name) + ")";
                    clsMain.ExecuteSQL(sSQL);

                    sSQL = "";
                    sSQL += "Update BAN Set" + "\n";
                    sSQL += "ISMO_BAN=1" + "\n";
                    sSQL += "Where MA_BAN=" + clsMain.SQLString(btn.Name) + "\n";
                    clsMain.ExecuteSQL(sSQL);
                }
                //Reload Table
                funtions = 1;
                GetSoDoBan();
                funtions = 9;

                // Dispes funtion 9 Check Table
                if (funtions != 1)
                {
                    funtions = 1;
                    btn_SoDoBan.Text = _mb;
                    pnShowHang.BringToFront();
                    GetKhuVuc();
                    GetSoDoBan();
                    SetDefaultBackgroundBtnInPn(pnThuNgan);
                    SetDefaultBackgroundBtnInPn(pnNghiepVu);
                    foreach (SimpleButton btn in pnKV_Mon.Controls)
                    {
                        if (btn.Name == maKV.ToString())
                        {
                            SetBackgroudOnClick(btn);
                        }
                    }
                    foreach (SimpleButton btnb in pnTable_Mon.Controls)
                    {
                        if (btnb.Name == maBan.ToString())
                        {
                            SetBackgroudOnClick(btnb);
                        }
                    }
                    KhoamoControl_Chuyenban(false);
                    dtLuoi.Rows.Clear();
                    gr_SelectProduct.RefreshDataSource();
                    txtDBTenKH.Text = "";
                    txtDienThoai.Text = "";
                    txtAddress.Text = "";
                    txtPartMoneyPayments.Text = "";
                    flCheckTable.Controls.Clear();
                    flagdatban = false;
                }
            }
            else
            {
                int dem = 0;
                foreach (SimpleButton btn in flCheckTable.Controls)
                {
                    dem++;
                }
                if (funtions != 9 || txtDBTenKH.Text == "" || dem == 0 || txtngay.Text == "")
                {
                    XtraMessageBox.Show(_banphainhapdayduthongtin, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (txtDienThoai.Text == "" || txtAddress.Text == "")
                {
                    XtraMessageBox.Show(_chuanhapttxacnhan, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (chkEmail.Checked)
                {
                    if (!cls_KP_RES.isValidEmail(txtEmail.Text.Trim()))
                    {
                        XtraMessageBox.Show(_emailkhople, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                }
                else
                {
                    if (!cls_KP_RES.isValidEmail(txtEmail.Text.Trim()) && txtEmail.Text != "")
                    {
                        XtraMessageBox.Show(_emailkhople, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                string date = string.Format("{0:dd-MM-yyyy}", txtngay.EditValue);
                string time = cbHH.Text + ":" + cbMM.Text + ":00";
                string ngayDat = date + " " + time;
                //Creater id Bill
                if (txtPartMoneyPayments.Text == "")
                    txtPartMoneyPayments.Text = "0";
                int n;
                bool isNumeric = int.TryParse(txtPartMoneyPayments.Text.Replace(",", String.Empty), out n);
                if (isNumeric == false)
                {
                    XtraMessageBox.Show(_tiendatcoclakieuso, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                string maHoaDon = GetNewCodeBill();
                string sSQL = "";
                //update Check Table           
                //Update infor Table checked
                if (maHoaDon != null)
                {
                    DataTable KH;
                    KH = clsMain.ReturnDataTable("Select MA From NHACUNGCAP Where DIENTHOAI =" + clsMain.SQLString(txtDienThoai.Text) + " And LOAI = 0");
                    if (KH.Rows.Count <= 0)
                    {
                        sSQL += "Insert into NHACUNGCAP(TEN, DIACHI, EMAIL, DIENTHOAI)" + "\n";
                        sSQL += "values(";
                        sSQL += clsMain.SQLStringUnicode(txtDBTenKH.Text) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtAddress.Text) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtEmail.Text) + ",";
                        sSQL += clsMain.SQLString(txtDienThoai.Text) + ")";
                        clsMain.ExecuteSQL(sSQL);
                        KH = clsMain.ReturnDataTable("Select MA From NHACUNGCAP Where DIENTHOAI =" + clsMain.SQLString(txtDienThoai.Text) + "  And LOAI = 0");
                    }
                    else
                    {
                        sSQL += "UPDATE NHACUNGCAP SET " + "\n";
                        sSQL += "TEN=" + clsMain.SQLStringUnicode(txtDBTenKH.Text) + "," + "\n";
                        sSQL += "EMAIL=" + clsMain.SQLStringUnicode(txtEmail.Text) + "," + "\n";
                        sSQL += "DIACHI=" + clsMain.SQLStringUnicode(txtAddress.Text) + "\n";
                        sSQL += "Where MA=" + clsMain.SQLString(KH.Rows[0]["MA"].ToString());
                        clsMain.ExecuteSQL(sSQL);
                    }

                    sSQL = "";
                    sSQL += "Insert into HOADON(MA_HOADON, MANHANVIEN,NGAYTAO, NGAYDAT, TIENDATCOC, GIOVAO, SODIENTHOAI, MA_KHACHHANG, ISBANDAT) ";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(maHoaDon) + ",";
                    sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                    sSQL += "GETDATE()" + ",";
                    sSQL += clsMain.SQLString(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")) + ",";
                    sSQL += clsMain.SQLString(txtPartMoneyPayments.Text.Replace(",", "")) + ",";
                    sSQL += clsMain.SQLString(ngayDat) + ",";
                    sSQL += clsMain.SQLString(txtDienThoai.Text) + ",";
                    sSQL += clsMain.SQLString(KH.Rows[0]["MA"].ToString()) + ",";
                    sSQL += 1 + ")";
                    clsMain.ExecuteSQL(sSQL);
                    string nameTable = "";
                    foreach (SimpleButton btn in flCheckTable.Controls)
                    {
                        string[] arr = btn.Text.Split('-');
                        nameTable += arr[0] + " ";
                        sSQL = "";
                        sSQL += "Insert into CT_MOBAN(MA_HOADON, MA_BAN)" + "\n";
                        sSQL += "Values(";
                        sSQL += clsMain.SQLString(maHoaDon) + ",";
                        sSQL += clsMain.SQLString(btn.Name) + ")";
                        clsMain.ExecuteSQL(sSQL);

                        sSQL = "";
                        sSQL += "Update BAN Set" + "\n";
                        sSQL += "ISMO_BAN=1" + "\n";
                        sSQL += "Where MA_BAN=" + clsMain.SQLString(btn.Name) + "\n";
                        clsMain.ExecuteSQL(sSQL);
                    }
                    string NameDistrict = "";
                    DataTable dtKVV = clsMain.ReturnDataTable("Select TEN_KHUVUC From KHUVUC Where MA_KHUVUC=" + clsMain.SQLString(maKV.ToString()));
                    if (dtKVV.Rows.Count > 0)
                        NameDistrict = dtKVV.Rows[0]["TEN_KHUVUC"].ToString();

                    List<DM_HangHoa> listHH = new List<DM_HangHoa>();
                    int j = 1;
                    if (gv_SelectProduct.RowCount > 0)
                    {
                        for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                        {
                            DM_HangHoa dmHangHoa = new DM_HangHoa();
                            dmHangHoa.Ten_HoaDon = "Phiếu xác nhận đặt Bàn";
                            dmHangHoa.STT = (j++).ToString();
                            dmHangHoa.Ma_HoaDon = maHoaDon;
                            dmHangHoa.TenKhachHang = txtDBTenKH.Text;
                            dmHangHoa.DienThoai = txtDienThoai.Text;
                            dmHangHoa.GIOVAO = ngayDat;
                            dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", DateTime.Now);
                            dmHangHoa.MaBan = nameTable;
                            dmHangHoa.TienDatCoc = decimal.Parse(txtPartMoneyPayments.Text);
                            dmHangHoa.KhuVuc = NameDistrict;
                            dmHangHoa.MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                            dmHangHoa.TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                            dmHangHoa.SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                            dmHangHoa.GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                            dmHangHoa.Thanh_Tien = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"]).ToString());

                            dmHangHoa.Ma_Quay = "QCO";
                            dmHangHoa.TenNV = clsGlobal.gsNameOfUserLogin;
                            listHH.Add(dmHangHoa);

                            sSQL = "";
                            sSQL += "Insert Into CT_HOADON(MA_HOADON, MA_HANGHOA,SOLUONG,GIABAN_THAT,GIABAN)" + "\n";
                            sSQL += "values(";
                            sSQL += clsMain.SQLString(maHoaDon) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ")";
                            clsMain.ExecuteSQL(sSQL);
                        }
                    }
                    else
                    {

                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        dmHangHoa.Ten_HoaDon = "Phiếu xác nhận đặt Bàn";
                        dmHangHoa.Ma_HoaDon = maHoaDon;
                        dmHangHoa.TenKhachHang = txtDBTenKH.Text;
                        dmHangHoa.DienThoai = txtDienThoai.Text;
                        dmHangHoa.GIOVAO = ngayDat;
                        dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", DateTime.Now);
                        dmHangHoa.MaBan = nameTable;
                        dmHangHoa.TienDatCoc = decimal.Parse(txtPartMoneyPayments.Text);
                        dmHangHoa.KhuVuc = NameDistrict;
                        dmHangHoa.Ma_Quay = "QCO";
                        dmHangHoa.TenNV = clsGlobal.gsNameOfUserLogin;
                        listHH.Add(dmHangHoa);
                    }
                    //


                    convert cvrt = new convert();
                    DataTable TableCheck = cvrt.ConvertToDataTable(listHH);

                    #region Gửi Email cho khách hàng
                    if (chkEmail.Checked)
                    {
                        int dangfile = 0;
                        DataTable dtTG = new DataTable();

                        string sql = "select TAPTIN,EMAIL_GUI,MATKHAU,CHUDE,NOIDUNG,HOST,PORT,MA_CUAHANG from CHITIETMAIL";
                        dtTG = clsMain.ReturnDataTable(sql);
                        if (dtTG.Rows.Count > 0)
                        {
                            if (dtTG.Rows[0]["TAPTIN"].ToString() == "E")
                                dangfile = 0;
                            else
                                dangfile = 1000;
                        }

                        //kiểm tra nếu thư mục "Report" chưa tồn tại thì tạo mới
                        string directoryPath = @"C:\Report";
                        if (!System.IO.Directory.Exists(directoryPath))
                        {
                            System.IO.Directory.CreateDirectory(directoryPath);
                            DirectoryInfo theFolder = new DirectoryInfo(@"C:\Report");
                            theFolder.Attributes = FileAttributes.Hidden | FileAttributes.System;   //set thư mục Report ẩn
                        }

                        KP_Report.Frm_Report1 fm = new KP_Report.Frm_Report1();
                        fm.ReportName = "rpt_BillDatBan";
                        fm.DataSource = TableCheck;
                        fm.IsSave = true;
                        fm.Fl = dangfile;
                        fm.FileName = "BillDatBan" + maHoaDon;
                        fm.Show();

                        MailMessage msg = new MailMessage();
                        msg.From = new MailAddress(dtTG.Rows[0]["EMAIL_GUI"].ToString());
                        msg.To.Add(txtEmail.Text.Trim());
                        msg.Subject = dtTG.Rows[0]["CHUDE"].ToString();         // chủ đề email
                        msg.Body = dtTG.Rows[0]["NOIDUNG"].ToString();          // nội dung của email
                        msg.Priority = MailPriority.High;                       // email ưu tiên
                        msg.IsBodyHtml = true;

                        string bdb = "";
                        if (dangfile == 0)
                        {
                            bdb = @"C:\Report\BillDatBan" + maHoaDon + ".xlsx";
                        }
                        else
                        {
                            bdb = @"C:\Report\BillDatBan" + maHoaDon + ".pdf";
                        }
                        Attachment attbcbh = new Attachment(bdb);
                        msg.Attachments.Add(attbcbh);

                        SmtpClient client = new SmtpClient();
                        client.Host = dtTG.Rows[0]["HOST"].ToString();
                        client.Port = int.Parse(dtTG.Rows[0]["PORT"].ToString());   //cổng 587
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.UseDefaultCredentials = false;
                        client.Credentials = new System.Net.NetworkCredential(dtTG.Rows[0]["EMAIL_GUI"].ToString(), dtTG.Rows[0]["MATKHAU"].ToString());
                        client.EnableSsl = true;
                        client.Send(msg);
                    }
                    #endregion

                    KP_Report.Frm_Report1 frm = new KP_Report.Frm_Report1();
                    if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                        frm.ReportName = "rpt_BillDatBan_58";
                    else
                        frm.ReportName = "rpt_BillDatBan";
                    frm.DataSource = TableCheck;
                    frm.WindowState = FormWindowState.Minimized;
                    frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                    frm.IsPrint = true;
                    frm.ShowDialog();
                    frm.Dispose();


                    XtraMessageBox.Show(_xacnhandatbanthanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CheckedIsTable();

                    //Reload Table
                    funtions = 1;
                    GetSoDoBan();
                    funtions = 9;
                }
                // Dispes funtion 9 Check Table
                if (funtions != 1)
                {
                    funtions = 1;
                    btn_SoDoBan.Text = _mb;
                    pnShowHang.BringToFront();
                    GetKhuVuc();
                    GetSoDoBan();
                    SetDefaultBackgroundBtnInPn(pnThuNgan);
                    SetDefaultBackgroundBtnInPn(pnNghiepVu);
                    foreach (SimpleButton btn in pnKV_Mon.Controls)
                    {
                        if (btn.Name == maKV.ToString())
                        {
                            SetBackgroudOnClick(btn);
                        }
                    }
                    foreach (SimpleButton btnb in pnTable_Mon.Controls)
                    {
                        if (btnb.Name == maBan.ToString())
                        {
                            SetBackgroudOnClick(btnb);
                        }
                    }

                    dtLuoi.Rows.Clear();
                    gr_SelectProduct.RefreshDataSource();
                    txtDBTenKH.Text = "";
                    txtDienThoai.Text = "";
                    txtAddress.Text = "";
                    txtPartMoneyPayments.Text = "";
                    flCheckTable.Controls.Clear();
                }
            }
            ExitDivideTable();
        }

        private void btnMobansau_Click(object sender, EventArgs e)
        {
            if (_flag_chuyenban)
            {
                string sSQL = "";
                sSQL += "Delete CT_MOBAN Where MA_HOADON=" + clsMain.SQLString(_mahoadon_chuyenban);
                clsMain.ExecuteSQL(sSQL);
                foreach (SimpleButton btn in flCheckTable.Controls)
                {
                    string[] arr = btn.Text.Split('-');
                    sSQL = "";
                    sSQL += "Insert into CT_MOBAN(MA_HOADON, MA_BAN)" + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(_mahoadon_chuyenban) + ",";
                    sSQL += clsMain.SQLString(btn.Name) + ")";
                    clsMain.ExecuteSQL(sSQL);
                }
                //Reload Table
                funtions = 1;
                GetSoDoBan();
                funtions = 9;

                // Dispes funtion 9 Check Table
                if (funtions != 1)
                {
                    funtions = 1;
                    btn_SoDoBan.Text = _mb;
                    pnShowHang.BringToFront();
                    GetKhuVuc();
                    GetSoDoBan();
                    SetDefaultBackgroundBtnInPn(pnThuNgan);
                    SetDefaultBackgroundBtnInPn(pnNghiepVu);
                    foreach (SimpleButton btn in pnKV_Mon.Controls)
                    {
                        if (btn.Name == maKV.ToString())
                        {
                            SetBackgroudOnClick(btn);
                        }
                    }
                    foreach (SimpleButton btnb in pnTable_Mon.Controls)
                    {
                        if (btnb.Name == maBan.ToString())
                        {
                            SetBackgroudOnClick(btnb);
                        }
                    }
                    KhoamoControl_Chuyenban(false);
                    dtLuoi.Rows.Clear();
                    gr_SelectProduct.RefreshDataSource();
                    txtDBTenKH.Text = "";
                    txtDienThoai.Text = "";
                    txtAddress.Text = "";
                    txtPartMoneyPayments.Text = "";
                    flCheckTable.Controls.Clear();
                    flagdatban = false;
                }
            }
            else
            {
                string sSQL = "";
                int dem = 0;
                foreach (SimpleButton btn in flCheckTable.Controls)
                {
                    dem++;
                }
                if (funtions != 9 || txtDBTenKH.Text == "" || dem == 0 || txtngay.Text == "")
                {
                    XtraMessageBox.Show(_banphainhapdayduthongtin, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (txtDienThoai.Text == "" || txtAddress.Text == "")
                {
                    XtraMessageBox.Show(_chuanhapttxacnhan, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (chkEmail.Checked)
                {
                    if (!cls_KP_RES.isValidEmail(txtEmail.Text.Trim()))
                    {
                        XtraMessageBox.Show(_emailkhople, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                else
                {
                    if (!cls_KP_RES.isValidEmail(txtEmail.Text.Trim()) && txtEmail.Text != "")
                    {
                        XtraMessageBox.Show(_emailkhople, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                string date = string.Format("{0:dd-MM-yyyy}", txtngay.EditValue);
                string time = cbHH.Text + ":" + cbMM.Text + ":00";
                string ngayDat = date + " " + time;
                //Creater id Bill
                if (txtPartMoneyPayments.Text == "")
                    txtPartMoneyPayments.Text = "0";
                int n;
                bool isNumeric = int.TryParse(txtPartMoneyPayments.Text.Replace(",", String.Empty), out n);
                if (isNumeric == false)
                {
                    XtraMessageBox.Show(_tiendatcoclakieuso, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                string maHoaDon = GetNewCodeBill();
                //update Check Table           
                //Update infor Table checked
                if (maHoaDon != null)
                {
                    DataTable KH;
                    KH = clsMain.ReturnDataTable("Select MA From NHACUNGCAP Where DIENTHOAI =" + clsMain.SQLString(txtDienThoai.Text) + " And LOAI = 0");
                    if (KH.Rows.Count <= 0)
                    {
                        sSQL += "Insert into NHACUNGCAP(TEN, DIACHI,EMAIL, DIENTHOAI)" + "\n";
                        sSQL += "values(";
                        sSQL += clsMain.SQLStringUnicode(txtDBTenKH.Text) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtAddress.Text) + ",";
                        sSQL += clsMain.SQLStringUnicode(txtEmail.Text) + ",";
                        sSQL += clsMain.SQLString(txtDienThoai.Text) + ")";
                        clsMain.ExecuteSQL(sSQL);
                        KH = clsMain.ReturnDataTable("Select MA From NHACUNGCAP Where DIENTHOAI =" + clsMain.SQLString(txtDienThoai.Text) + "  And LOAI = 0");
                    }
                    else
                    {
                        sSQL += "UPDATE NHACUNGCAP SET " + "\n";
                        sSQL += "TEN=" + clsMain.SQLStringUnicode(txtDBTenKH.Text) + "," + "\n";
                        sSQL += "DIACHI=" + clsMain.SQLStringUnicode(txtAddress.Text) + "," + "\n";
                        sSQL += "EMAIL=" + clsMain.SQLStringUnicode(txtEmail.Text) + "\n";
                        sSQL += "Where MA=" + clsMain.SQLString(KH.Rows[0]["MA"].ToString());
                        clsMain.ExecuteSQL(sSQL);
                    }

                    sSQL = "";
                    sSQL += "Insert into HOADON(MA_HOADON, MANHANVIEN,NGAYTAO, NGAYDAT, TIENDATCOC, GIOVAO, SODIENTHOAI, MA_KHACHHANG, ISBANDAT) ";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(maHoaDon) + ",";
                    sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                    sSQL += "GETDATE()" + ",";
                    sSQL += clsMain.SQLString(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")) + ",";
                    sSQL += float.Parse(txtPartMoneyPayments.Text.Replace(",", "")) + ",";
                    sSQL += clsMain.SQLString(ngayDat) + ",";
                    sSQL += clsMain.SQLString(txtDienThoai.Text) + ",";
                    sSQL += clsMain.SQLString(KH.Rows[0]["MA"].ToString()) + ",";
                    sSQL += 1 + ")";
                    clsMain.ExecuteSQL(sSQL);
                    string nameTable = "";
                    foreach (SimpleButton btn in flCheckTable.Controls)
                    {
                        string[] arr = btn.Text.Split('-');
                        nameTable += arr[0] + " ";
                        sSQL = "";
                        sSQL += "Insert into CT_MOBAN(MA_HOADON, MA_BAN)" + "\n";
                        sSQL += "Values(";
                        sSQL += clsMain.SQLString(maHoaDon) + ",";
                        sSQL += clsMain.SQLString(btn.Name) + ")";
                        clsMain.ExecuteSQL(sSQL);
                    }
                    string NameDistrict = "";
                    DataTable dtKVV = clsMain.ReturnDataTable("Select TEN_KHUVUC From KHUVUC Where MA_KHUVUC=" + maKV + "");
                    if (dtKVV.Rows.Count > 0)
                        NameDistrict = dtKVV.Rows[0]["TEN_KHUVUC"].ToString();

                    List<DM_HangHoa> listHH = new List<DM_HangHoa>();
                    int j = 1;
                    if (gv_SelectProduct.RowCount > 0)
                    {
                        for (int i = 0; i < gv_SelectProduct.RowCount; i++)
                        {
                            DM_HangHoa dmHangHoa = new DM_HangHoa();
                            dmHangHoa.Ten_HoaDon = "Phiếu xác nhận đặt Bàn";
                            dmHangHoa.STT = (j++).ToString();
                            dmHangHoa.Ma_HoaDon = maHoaDon;
                            dmHangHoa.TenKhachHang = txtDBTenKH.Text;
                            dmHangHoa.DienThoai = txtDienThoai.Text;
                            dmHangHoa.GIOVAO = ngayDat;
                            dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", DateTime.Now);
                            dmHangHoa.MaBan = nameTable;
                            dmHangHoa.TienDatCoc = decimal.Parse(txtPartMoneyPayments.Text);
                            dmHangHoa.KhuVuc = NameDistrict;
                            dmHangHoa.MA_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                            dmHangHoa.TEN_HANGHOA = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEN_HANGHOA"]).ToString();
                            dmHangHoa.SOLUONG = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["SOLUONG"]).ToString());
                            dmHangHoa.GIABAN = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["GIABAN"]).ToString());
                            dmHangHoa.Thanh_Tien = decimal.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["Thanh_Tien"]).ToString());
                            dmHangHoa.Ma_Quay = "QCO";
                            dmHangHoa.TenNV = clsGlobal.gsNameOfUserLogin;
                            listHH.Add(dmHangHoa);

                            sSQL = "";
                            sSQL += "Insert Into CT_HOADON(MA_HOADON, MA_HANGHOA,SOLUONG,GIABAN_THAT,GIABAN)" + "\n";
                            sSQL += "values(";
                            sSQL += clsMain.SQLString(maHoaDon) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ")";
                            clsMain.ExecuteSQL(sSQL);
                        }
                    }
                    else
                    {

                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        dmHangHoa.Ten_HoaDon = "Phiếu xác nhận đặt Bàn";
                        dmHangHoa.Ma_HoaDon = maHoaDon;
                        dmHangHoa.TenKhachHang = txtDBTenKH.Text;
                        dmHangHoa.DienThoai = txtDienThoai.Text;
                        dmHangHoa.GIOVAO = ngayDat;
                        dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", DateTime.Now);
                        dmHangHoa.MaBan = nameTable;
                        dmHangHoa.TienDatCoc = decimal.Parse(txtPartMoneyPayments.Text);
                        dmHangHoa.KhuVuc = NameDistrict;
                        dmHangHoa.Ma_Quay = "QCO";
                        dmHangHoa.TenNV = clsGlobal.gsNameOfUserLogin;
                        listHH.Add(dmHangHoa);
                    }
                    //
                    convert cvrt = new convert();
                    DataTable TableCheck = cvrt.ConvertToDataTable(listHH);
                    #region Gửi Email cho khách hàng
                    if (chkEmail.Checked)
                    {
                        int dangfile = 0;
                        DataTable dtTG = new DataTable();

                        string sql = "select TAPTIN,EMAIL_GUI,MATKHAU,CHUDE,NOIDUNG,HOST,PORT,MA_CUAHANG from CHITIETMAIL";
                        dtTG = clsMain.ReturnDataTable(sql);
                        if (dtTG.Rows.Count > 0)
                        {
                            if (dtTG.Rows[0]["TAPTIN"].ToString() == "E")
                                dangfile = 0;
                            else
                                dangfile = 1000;
                        }

                        //kiểm tra nếu thư mục "Report" chưa tồn tại thì tạo mới
                        string directoryPath = @"C:\Report";
                        if (!System.IO.Directory.Exists(directoryPath))
                        {
                            System.IO.Directory.CreateDirectory(directoryPath);
                            DirectoryInfo theFolder = new DirectoryInfo(@"C:\Report");
                            theFolder.Attributes = FileAttributes.Hidden | FileAttributes.System;   //set thư mục Report ẩn
                        }

                        KP_Report.Frm_Report1 fm = new KP_Report.Frm_Report1();
                        fm.ReportName = "rpt_BillDatBan";
                        fm.DataSource = TableCheck;
                        fm.IsSave = true;
                        fm.Fl = dangfile;
                        fm.FileName = "BillDatBan" + maHoaDon;
                        fm.Show();

                        MailMessage msg = new MailMessage();
                        msg.From = new MailAddress(dtTG.Rows[0]["EMAIL_GUI"].ToString());
                        msg.To.Add(txtEmail.Text.Trim());
                        msg.Subject = dtTG.Rows[0]["CHUDE"].ToString();         // chủ đề email
                        msg.Body = dtTG.Rows[0]["NOIDUNG"].ToString();          // nội dung của email
                        msg.Priority = MailPriority.High;                       // email ưu tiên
                        msg.IsBodyHtml = true;

                        string bdb = "";
                        if (dangfile == 0)
                        {
                            bdb = @"C:\Report\BillDatBan" + maHoaDon + ".xlsx";
                        }
                        else
                        {
                            bdb = @"C:\Report\BillDatBan" + maHoaDon + ".pdf";
                        }
                        Attachment attbcbh = new Attachment(bdb);
                        msg.Attachments.Add(attbcbh);

                        SmtpClient client = new SmtpClient();
                        client.Host = dtTG.Rows[0]["HOST"].ToString();
                        client.Port = int.Parse(dtTG.Rows[0]["PORT"].ToString());   //cổng 587
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.UseDefaultCredentials = false;
                        client.Credentials = new System.Net.NetworkCredential(dtTG.Rows[0]["EMAIL_GUI"].ToString(), dtTG.Rows[0]["MATKHAU"].ToString());
                        client.EnableSsl = true;
                        client.Send(msg);
                    }
                    #endregion
                    KP_Report.Frm_Report1 frm = new KP_Report.Frm_Report1();
                    if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                        frm.ReportName = "rpt_BillDatBan_58";
                    else
                        frm.ReportName = "rpt_BillDatBan";
                    frm.DataSource = TableCheck;
                    frm.WindowState = FormWindowState.Minimized;
                    frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                    frm.IsPrint = true;
                    frm.ShowDialog();
                    frm.Dispose();
                    XtraMessageBox.Show(_xacnhandatbanthanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CheckedIsTable();

                    //Reload Table
                    funtions = 1;
                    GetSoDoBan();
                    funtions = 9;
                }
                // Dispes funtion 9 Check Table
                if (funtions != 1)
                {
                    funtions = 1;
                    btn_SoDoBan.Text = _mb;
                    pnShowHang.BringToFront();
                    GetKhuVuc();
                    GetSoDoBan();
                    SetDefaultBackgroundBtnInPn(pnThuNgan);
                    SetDefaultBackgroundBtnInPn(pnNghiepVu);
                    foreach (SimpleButton btn in pnKV_Mon.Controls)
                    {
                        if (btn.Name == maKV.ToString())
                        {
                            SetBackgroudOnClick(btn);
                        }
                    }
                    foreach (SimpleButton btnb in pnTable_Mon.Controls)
                    {
                        if (btnb.Name == maBan.ToString())
                        {
                            SetBackgroudOnClick(btnb);
                        }
                    }

                    dtLuoi.Rows.Clear();
                    gr_SelectProduct.RefreshDataSource();
                    txtDBTenKH.Text = "";
                    txtDienThoai.Text = "";
                    txtAddress.Text = "";
                    txtPartMoneyPayments.Text = "";
                    flCheckTable.Controls.Clear();
                }
            }
            ExitDivideTable();
        }

        private void btn_Danhsachdatban_Click(object sender, EventArgs e)
        {
            Frm_Danhsachdatban frm = new Frm_Danhsachdatban();
            frm.ShowDialog();
            _flag_chuyenban = Frm_Danhsachdatban.flag_Chuyenban;
            _mahoadon_chuyenban = Frm_Danhsachdatban.mahoadon;
            if (_flag_chuyenban)
            {
                string sSQL = "";
                sSQL += "Select TEN,HD.SODIENTHOAI,DIACHI,HD.TIENDATCOC,NGAYDAT,GIOVAO" + "\n";
                sSQL += "From HOADON hd,NHACUNGCAP n" + "\n";
                sSQL += "Where hd.MA_KHACHHANG=n.MA" + "\n";
                sSQL += "And hd.MA_HOADON=" + clsMain.SQLString(_mahoadon_chuyenban);
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                if (dt.Rows.Count > 0)
                {
                    txtDBTenKH.Text = dt.Rows[0]["TEN"].ToString();
                    txtDienThoai.Text = dt.Rows[0]["SODIENTHOAI"].ToString();
                    txtAddress.Text = dt.Rows[0]["DIACHI"].ToString();
                    txtPartMoneyPayments.EditValue = dt.Rows[0]["TIENDATCOC"].ToString();
                    txtngay.EditValue = DateTime.Parse(dt.Rows[0]["NGAYDAT"].ToString());
                    cbHH.Text = DateTime.Parse(dt.Rows[0]["GIOVAO"].ToString()).Hour.ToString().Length < 2 ? "0" + DateTime.Parse(dt.Rows[0]["GIOVAO"].ToString()).Hour.ToString() : DateTime.Parse(dt.Rows[0]["GIOVAO"].ToString()).Hour.ToString();
                    cbMM.Text = DateTime.Parse(dt.Rows[0]["GIOVAO"].ToString()).Minute.ToString().Length < 2 ? "0" + DateTime.Parse(dt.Rows[0]["GIOVAO"].ToString()).Minute.ToString() : DateTime.Parse(dt.Rows[0]["GIOVAO"].ToString()).Minute.ToString();

                    KhoamoControl_Chuyenban(true);
                }
            }
            else
            {
            }
        }

        // Select check Table have in flow funtion 9
        private void SelectCheckTable(string idTable)
        {
            //Checked table select 
            foreach (SimpleButton btnBan in flCheckTable.Controls)
            {
                if (btnBan.Name == this.ActiveControl.Name)
                {
                    if (btnBan.Name == maBan)
                        throw new Exception("Bàn này đã được chọn !");
                }
            }
            //Select Table
            string sql = "select MA_BAN, TEN_BAN, SOGHE, HINHANH from BAN where MA_BAN = " + idTable + "";
            DataTable dtCheckTable = clsMain.ReturnDataTable(sql);
            SimpleButton btnTables = new SimpleButton();
            btnTables.Name = dtCheckTable.Rows[0]["MA_BAN"].ToString();
            btnTables.Text = dtCheckTable.Rows[0]["TEN_BAN"].ToString() + " - " + dtCheckTable.Rows[0]["SOGHE"].ToString() + "  Ghế";
            btnTables.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            btnTables.ImageLocation = ImageLocation.TopCenter;
            try
            {
                byte[] tam = new byte[((byte[])dtCheckTable.Rows[0]["HINHANH"]).LongLength];
                tam = (byte[])dtCheckTable.Rows[0]["HINHANH"];
                MemoryStream ms = new MemoryStream(tam);
                Bitmap bm = new Bitmap(ms);
                if (cls_KP_RES.iHinhanh == 1)
                {
                    btnTables.BackgroundImage = bm;
                    btnTables.BackgroundImageLayout = ImageLayout.Stretch;
                    btnTables.Appearance.BackColor = Color.Transparent;
                    btnTables.Appearance.Options.UseBackColor = true;
                    btnTables.ButtonStyle = BorderStyles.NoBorder;
                    btnTables.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                }
                else
                {
                    btnTables.Image = bm;
                }
            }
            catch
            {
            }
            btnTables.RightToLeft = RightToLeft.No;
            btnTables.Size = new Size(110, 80);
            btnTables.Click += new EventHandler(btnTables_Click);
            flCheckTable.Controls.Add(btnTables);
        }

        private void btnTables_Click(object sender, EventArgs e)
        {
            if (funtions == 9)
            {
                foreach (SimpleButton btnTables in flCheckTable.Controls)
                {
                    if (btnTables.Name == this.ActiveControl.Name)
                    {
                        idCheckTable = btnTables.Name;
                        SetBackgroudOnClick(btnTables);
                    }
                    else
                        SetDefautlBackgroud(btnTables);
                }
            }
        }

        private bool XacNhanBanDat(string idTable)
        {
            DateTime now;
            string idBill = GetMaHoaDon(idTable);
            DataTable timeIn = clsMain.ReturnDataTable("Select GIOVAO from HOADON where MA_HOADON ='" + idBill + "'");
            if (timeIn.Rows.Count > 0)
                now = DateTime.Parse(timeIn.Rows[0]["GIOVAO"].ToString().Substring(0, 10));
            else
                throw new Exception(_tgvaocuabannaykxd);
            DateTime x = DateTime.Parse(txtngay.EditValue.ToString().Substring(0, 10));
            int outline = DateTime.Compare(now, x);
            if (outline == 1)
            {
                XtraMessageBox.Show(_ngaydatbankhonghople, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            else
            {
                if (cbHH.Text != "")
                    if ((int.Parse(cbHH.Text) - int.Parse(DateTime.Now.Hour.ToString())) <= 5 && outline == 0)
                    {
                        XtraMessageBox.Show(_tgdenkhonghople, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
            }
            return true;
        }

        // select table isbandat
        private bool SelectTablesCheckt(string idTable)
        {
            DateTime now;
            DataTable timeIn = clsMain.ReturnDataTable("Select HD.GIOVAO from HOADON HD inner join CT_MOBAN BM on HD.MA_HOADON = BM.MA_HOADON where BM.MA_BAN = " + idTable + " and HD.ISBANDAT = 1 ");
            if (timeIn.Rows.Count > 0)
            {
                for (int i = 0; i < timeIn.Rows.Count; i++)
                {
                    now = DateTime.Parse(timeIn.Rows[i]["GIOVAO"].ToString().Substring(0, 10));
                    DateTime x = DateTime.Parse(txtngay.EditValue.ToString().Substring(0, 10));
                    int outline = DateTime.Compare(now, x);
                    if (outline == 0)
                    {
                        XtraMessageBox.Show(_bannaydaconguoidat, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
            }
            return true;

        }

        private bool CheckHaveISBANDAT(int idTable)
        {
            string sqlIS = "Select ISBANDAT From BAN Where MA_BAN =" + idTable + " And ISBANDAT = 1";
            DataTable tableIS = clsMain.ReturnDataTable(sqlIS);
            if (tableIS.Rows.Count > 0)
                return true;
            return false;
        }

        private void CheckedIsTable()
        {
            try
            {
                string time1 = Convert.ToDateTime(DateTime.Now.ToShortDateString()).ToString("dd/MM/yyyy");
                DateTime now = DateTime.Parse(time1);
                DataTable dttt = clsMain.ReturnDataTable("Select mb.MA_BAN, convert(varchar(25), hd.GIOVAO , 120) as GIOVAO, convert(varchar(10),hd.GIOVAO , 101) as NgayDat from CT_MOBAN mb inner join HOADON hd on mb.MA_HOADON=hd.MA_HOADON  where ISBANDAT =1");
                if (dttt.Rows.Count <= 0)
                    return;
                for (int i = 0; i < dttt.Rows.Count; i++)
                {
                    string dd = dttt.Rows[i]["GIOVAO"].ToString();
                    if (dttt.Rows[i]["GIOVAO"].ToString() != "")
                    {
                        string sso = dttt.Rows[i]["NgayDat"].ToString();
                        DateTime x = DateTime.Parse(sso);
                        int outline = DateTime.Compare(now, x);
                        if ((int.Parse(dttt.Rows[i]["GIOVAO"].ToString().Substring(11, 2)) - int.Parse(DateTime.Now.Hour.ToString()) <= 5) && outline == 0)
                        {
                            string sql = "Update BAN SET ISBANDAT = 1 where MA_BAN =" + int.Parse(dttt.Rows[i]["MA_BAN"].ToString());
                            clsMain.ExecuteSQL(sql);
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void btnComboOption_Click(object sender, EventArgs e)
        {
            try
            {
                string co_mahanghoa = "";
                string co_tenhanghoa = "";
                string co_soluong = "";
                string co_phuthu = "";
                int sMaCB = int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MACB"]).ToString());
                if (bool.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["TRANGTHAI"]).ToString()))
                {
                    string maCombo = "";
                    int i = 0;
                    for (i = gv_SelectProduct.FocusedRowHandle - 1; i >= 0; i--)
                    {
                        if (!bool.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TRANGTHAI"]).ToString())
                            && int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MACB"]).ToString()) == sMaCB
                            && int.Parse(gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["TEM"]).ToString()) == 0
                            && gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MACP"]).ToString() == "CB")
                        {
                            maCombo = gv_SelectProduct.GetRowCellValue(i, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString();
                            break;
                        }
                    }

                    string sSQL = "";
                    sSQL += "Select MA_HANGHOA_OPTION as MA,B.TEN_HANGHOA as TEN,A.SOLUONG,GIATHEM" + "\n";
                    sSQL += "From COMBO_OPTION A,HANGHOA B" + "\n";
                    sSQL += "Where MA_HANGHOA_OPTION=b.MA_HANGHOA" + "\n";
                    sSQL += "AND a.MA_COMBO=" + clsMain.SQLString(maCombo) + "\n";
                    sSQL += "AND a.MA_HANGHOA=" + clsMain.SQLString(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                    DataTable dt = clsMain.ReturnDataTable(sSQL);
                    if (dt.Rows.Count > 0)
                    {
                        Frm_Combo_Option frm = new Frm_Combo_Option(maCombo, gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MA_HANGHOA"]).ToString());
                        frm.ShowDialog();
                        co_mahanghoa = frm.s_mahanghoa;
                        co_tenhanghoa = Kytu_Combo + frm.s_tenhanghoa;
                        co_soluong = (int.Parse(frm.s_soluong) * int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, "SOLUONG").ToString())).ToString();
                        co_phuthu = frm.s_phuthu;
                        if (co_mahanghoa != "")
                        {
                            float tt = float.Parse(gv_SelectProduct.GetRowCellValue(i, "GIABAN").ToString()) + float.Parse(co_phuthu);
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "MA_HANGHOA", co_mahanghoa);
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "TEN_HANGHOA", co_tenhanghoa);
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "SOLUONG", int.Parse(co_soluong));
                            gv_SelectProduct.SetRowCellValue(i, "GIABAN", tt);
                            gv_SelectProduct.SetRowCellValue(i, "Thanh_Tien", tt * int.Parse(gv_SelectProduct.GetRowCellValue(i, "SOLUONG").ToString()));
                        }
                    }
                    else if (dt.Rows.Count == 0)
                    {
                        sSQL = "";
                        sSQL += "Select a.MA_HANGHOA as MA,B.TEN_HANGHOA as TEN,A.SOLUONG,GIATHEM" + "\n";
                        sSQL += "From COMBO_OPTION A,HANGHOA B" + "\n";
                        sSQL += "Where a.MA_HANGHOA=b.MA_HANGHOA" + "\n";
                        sSQL += "AND a.MA_COMBO=" + clsMain.SQLString(maCombo) + "\n";
                        sSQL += "AND a.MA_HANGHOA_OPTION=" + clsMain.SQLString(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, "MA_HANGHOA").ToString());
                        dt = clsMain.ReturnDataTable(sSQL);
                        if (dt.Rows.Count > 0)
                        {
                            int sl = int.Parse(gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, "SOLUONG").ToString()) / int.Parse(dt.Rows[0]["SOLUONG"].ToString());
                            float tt = float.Parse(gv_SelectProduct.GetRowCellValue(i, "GIABAN").ToString()) - float.Parse(dt.Rows[0]["GIATHEM"].ToString());
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "MA_HANGHOA", dt.Rows[0]["MA"].ToString());
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "TEN_HANGHOA", Kytu_Combo + dt.Rows[0]["TEN"].ToString());
                            gv_SelectProduct.SetRowCellValue(gv_SelectProduct.FocusedRowHandle, "SOLUONG", sl);
                            gv_SelectProduct.SetRowCellValue(i, "GIABAN", tt);
                            gv_SelectProduct.SetRowCellValue(i, "Thanh_Tien", tt * int.Parse(gv_SelectProduct.GetRowCellValue(i, "SOLUONG").ToString()));
                        }
                        else
                        {
                            XtraMessageBox.Show(_khongcohhthaythe, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    else
                    {
                        XtraMessageBox.Show(_khongcohhthaythe, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                else
                {
                    XtraMessageBox.Show(_hhkhongthuoccombo, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch
            {
            }
        }

        private void btnThemHangHoa_Click(object sender, EventArgs e)
        {
            Frm_ThemHangHoa frm = new Frm_ThemHangHoa();
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btn_NhapSL_Click(object sender, EventArgs e)
        {
            Frm_Chontheosoluong frm = new Frm_Chontheosoluong();
            frm.Mode = 1;
            frm.ShowDialog();
            SoLuong_HuyMon = frm.tmp_SoLuong;
            frm.Dispose();
            flag_sole = true;
            ThrowTable1ToTable2();
        }

        private void gv_SelectProduct_RowClick(object sender, RowClickEventArgs e)
        {
            iFocusRow = gv_SelectProduct.FocusedRowHandle;
        }

        private void GetNhomhangMonThem()
        {
            try
            {
                DataTable dtNhomhang = new DataTable();
                dtNhomhang.Rows.Clear();
                string SQLMON = string.Format("exec SelectDishMonThem @PageNumber={0},@PageSize={1},@maQUAY={2}", cPageNH, 7, int.Parse(cls_ConfigCashier.idCashier));
                dtNhomhang = clsMain.ReturnDataTable(SQLMON);

                while (pnKV_Mon.Controls.Count > 0)
                {
                    pnKV_Mon.Controls[0].Dispose();
                }
                pnKV_Mon.Controls.Clear();
                int locateY = 2;
                int locateX = 3;
                for (int i = 0; i < dtNhomhang.Rows.Count; i++)
                {
                    SimpleButton btnKV = new SimpleButton();
                    btnKV.Name = dtNhomhang.Rows[i]["MA_NHOMHANG"].ToString();
                    if (i == 0)
                    {
                        maNH = int.Parse(dtNhomhang.Rows[0]["MA_NHOMHANG"].ToString());
                        SetBackgroudOnClick(btnKV);
                    }
                    btnKV.Text = "" + dtNhomhang.Rows[i]["TEN_NHOMHANG"].ToString();
                    try
                    {
                        byte[] tam = new byte[((byte[])dtNhomhang.Rows[i]["HINHANH"]).LongLength];
                        tam = (byte[])dtNhomhang.Rows[i]["HINHANH"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        //btnKV.Image = bm;
                        if (cls_KP_RES.iHinhanh == 1)
                        {
                            btnKV.BackgroundImage = bm;
                            btnKV.BackgroundImageLayout = ImageLayout.Stretch;
                            btnKV.Appearance.BackColor = Color.Transparent;
                            btnKV.Appearance.Options.UseBackColor = true;
                            btnKV.ButtonStyle = BorderStyles.NoBorder;
                            btnKV.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                        }
                        else
                        {
                            btnKV.Image = bm;
                        }
                    }
                    catch
                    {
                    }
                    btnKV.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btnKV.ImageLocation = ImageLocation.TopCenter;
                    btnKV.RightToLeft = RightToLeft.No;
                    btnKV.Size = new Size(125, 80);
                    if (i != 0)
                    {
                        locateY += int.Parse(btnKV.Location.Y.ToString()) + 85;
                    }
                    btnKV.Location = new Point(locateX, locateY);
                    btnKV.Click += new EventHandler(btnKV_Click);
                    pnKV_Mon.Controls.Add(btnKV);
                }
            }
            catch
            {
            }
        }

        private void btn_MonThem_Click(object sender, EventArgs e)
        {
            try
            {
                if (isYeuCauThem == true)
                    return;
                if (gv_SelectProduct.RowCount == 0)
                    sButtonMonThem = true;
                else
                {
                    bool sMONTHEM = bool.Parse(gv_SelectProduct.GetFocusedRowCellValue("MONTHEM").ToString());
                    string sMACP = gv_SelectProduct.GetRowCellValue(gv_SelectProduct.FocusedRowHandle, gv_SelectProduct.Columns["MACP"]).ToString();

                    if (sMONTHEM == false & sButtonMonThem == false)
                        //sButtonMonThem = true;
                        return;
                }

                pnBtnDeleteGv.Visible = false;
                btnComboOption.Visible = false;

                if (sButtonMonThem == false && funtions == 2)
                {

                    GetNhomhangMonThem();
                    GetListMon();
                    sButtonMonThem = true;
                    SetBackgroudOnClick(btn_MonThem);
                    //panelControl1.Visible = false;
                    sTEM = int.Parse(gv_SelectProduct.GetFocusedRowCellValue("TEM").ToString());
                    sMaCB = int.Parse(gv_SelectProduct.GetFocusedRowCellValue("MACB").ToString());
                    sMaHH_THEMMON = gv_SelectProduct.GetFocusedRowCellValue("MA_HANGHOA").ToString();
                    foreach (SimpleButton btn in pnThuNgan.Controls)
                    {
                        if (btn.Name != "btn_MonThem")
                        {
                            btn.Enabled = false;
                        }
                    }
                }
                else if (sButtonMonThem == true)
                {
                    GetNhomhang();
                    GetListMon();
                    sButtonMonThem = false;
                    SetDefautlBackgroud(btn_MonThem);
                    if (dtLuoi.Rows.Count > 0)
                    {
                        sTEM = LayTemLonNhat() + 1;
                        sMaCB = LayMACBLonNhat() + 1;
                    }
                    else
                        sTEM = 1;
                    foreach (SimpleButton btn in pnThuNgan.Controls)
                    {
                        btn.Enabled = true;
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        private void SetGiaoDienMacDinh()
        {

            GetNhomhang();
            GetListMon();
            sButtonMonThem = false;
            isYeuCauThem = false;
            foreach (SimpleButton btn in pnThuNgan.Controls)
            {
                btn.Enabled = true;
                SetDefautlBackgroud(btn);
            }
        }

        private bool KT_COMBO_OPTION_MONTHEM(string MACOMBO)
        {
            bool kq = false;
            DataTable dt_combo = GetProductInCombo(MACOMBO);
            string sSQL = "";
            for (int i = 0; i < dt_combo.Rows.Count; i++)
            {
                sSQL += "Select MA_HANGHOA_OPTION as MA,B.TEN_HANGHOA as TEN,A.SOLUONG,GIATHEM" + "\n";
                sSQL += "From COMBO_OPTION A,HANGHOA B" + "\n";
                sSQL += "Where MA_HANGHOA_OPTION=b.MA_HANGHOA" + "\n";
                sSQL += "AND a.MA_COMBO=" + clsMain.SQLString(MACOMBO) + "\n";
                sSQL += "AND a.MA_HANGHOA=" + clsMain.SQLString(dt_combo.Rows[i]["MA_HANGHOA"].ToString());
                DataTable dt_option = clsMain.ReturnDataTable(sSQL);

                if (dt_option.Rows.Count > 0 || bool.Parse(dt_combo.Rows[i]["MONTHEM"].ToString()) == true)
                {
                    kq = true;
                    break;
                }
                else
                {
                    kq = false;
                }
            }
            return kq;
        }

        private bool KiemTraCot(DataTable _dt)
        {
            bool kq = false;
            foreach (DataRow dr in _dt.Rows)
            {
                if (dtLuoi.Columns.Contains("Number"))
                {
                    kq = true;
                    break;
                }
            }
            return kq;
        }

        private int LayTemLonNhat()
        {
            int kq = 1;
            foreach (DataRow dr in dtLuoi.Rows)
            {
                if (int.Parse(dr["TEM"].ToString()) > kq)
                {
                    kq = int.Parse(dr["TEM"].ToString());
                }
            }
            return kq;
        }
        private int LayMACBLonNhat()
        {
            int kq = 1;
            foreach (DataRow dr in dtLuoi.Rows)
            {
                if (int.Parse(dr["MACB"].ToString()) > kq)
                {
                    kq = int.Parse(dr["MACB"].ToString());
                }
            }
            return kq;
        }

        private void XoaData_divide_Table1(int smaCB)
        {
            DataTable divide_Table3 = divide_Table1.Copy();
            divide_Table3.Clear();
            foreach (DataRow dr in divide_Table1.Rows)
            {
                if (int.Parse(dr["_MACB"].ToString()) != smaCB)
                {
                    divide_Table3.Rows.Add(dr.ItemArray);
                }
            }
            divide_Table1.Clear();
            divide_Table1 = divide_Table3;
            gr_divide_Table_1.DataSource = divide_Table1;
        }
        private void XoaData_divide_Table2(int smaCB)
        {
            DataTable divide_Table3 = divide_Table2.Copy();
            divide_Table3.Clear();
            foreach (DataRow dr in divide_Table2.Rows)
            {
                if (int.Parse(dr["__MACB"].ToString()) != smaCB)
                {
                    divide_Table3.Rows.Add(dr.ItemArray);
                }
            }
            divide_Table2.Clear();
            divide_Table2 = divide_Table3;
            gr_divide_Table_2.DataSource = divide_Table2;
        }

        private void txtEmail_Click(object sender, MouseEventArgs e)
        {

        }
    }

}