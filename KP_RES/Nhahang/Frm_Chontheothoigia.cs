﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using KP_Terminal;
using System.IO.Ports;
using System.Text.RegularExpressions;

namespace KP_RES
{
    public partial class Frm_Chontheothoigia : DevExpress.XtraEditors.XtraForm
    {
        public bool kt = false;
        bool[] ctltxt;
        public decimal tmp_Thoigia;
        public decimal tmp_SoLuong;
        public bool bThoat = false;
        public int Mode = 0; // 0 sửa giá, 1 hủy món

        public Frm_Chontheothoigia()
        {
            InitializeComponent();
        }

        string _thongbao = "";
        string _bkdnq15kt = "";
        string _cntg = "";
        string _cngia = "";
        string _cntgEmpty = "";
        private void Frm_ConfigFuntion_Load(object sender, EventArgs e)
        {
            if (Mode == 1)
            {
                lb_nhapgia.Visible = false;
                txtGIA.Visible = false;
            }
            int dem = 1;
            foreach (Control ctl in panelControl2.Controls)
            {
                if (ctl is TextEdit)
                {
                    ctl.Tag = dem;
                    dem += 1;
                }
                ctl.Click += new EventHandler(ctl_Click);
            }
            ctltxt = new bool[dem];

            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            this.Text = rm.GetString("nhapgia", culture);
            lb_nhapgia.Text = rm.GetString("nhapgia", culture);
            btn_thoat.Text = rm.GetString("thoat", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _bkdnq15kt = rm.GetString("bkdnq15kt", culture);
            _cntg = rm.GetString("cntg", culture);
            //labelControl1.Text = rm.GetString("cngia", culture);
            _cntgEmpty = rm.GetString("cntgEmpty", culture);

            txtGIA.Text = tmp_Thoigia.ToString();
            txtSoLuong.Focus();
        }

        private void btn_000_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("000");
        }

        private void btn_00_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber(".");
        }

        private void btn_0_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("0");
        }

        private void btn_1_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("1");
        }

        private void btn_2_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("2");
        }

        private void btn_3_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("3");
        }

        private void btn_4_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("4");
        }

        private void btn_5_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("5");
        }

        private void btn_6_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("6");
        }

        private void btn_7_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("7");
        }

        private void btn_8_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("8");
        }

        private void btn_9_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("9");
        }

        private void btn_ESC_Click(object sender, EventArgs e)
        {
            if (iControl == 0)
            {
                txtSoLuong.EditValue = 0;
            }
            else
            {
                txtGIA.EditValue = 0;
            }
           
        }

        private void btn_Payments_Click(object sender, EventArgs e)
        {

            if (Mode == 1)
            {
                tmp_SoLuong = decimal.Parse(txtSoLuong.Text);
                this.Close();
            }
            else
            {
                SendKeys.Send("{Enter}");
                txtGIA.Focus();
            }
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            DeleteTextBox();
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            MoveToLeft();
        }

        private void btnMoveRightNumber_Click(object sender, EventArgs e)
        {
            MoveToRight();
        }

        private void btn_Thoat_Click(object sender, EventArgs e)
        {
            this.Close();
            bThoat = true;
        }

        private void txtConfig_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtSoLuong.Text == "" || txtSoLuong.Text == "0")
                {
                    MessageBox.Show(_cntgEmpty, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtSoLuong.Focus();
                    return;
                }
                if (txtGIA.Text == "" || txtGIA.Text == "0")
                {
                    MessageBox.Show(_cntg, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtGIA.Focus();
                    return;
                }
                tmp_Thoigia = Convert.ToDecimal(txtGIA.Text);
                tmp_SoLuong = Convert.ToDecimal(txtSoLuong.Text);
                this.Close();
                bThoat = false;
            }
        }

        int iControl = 0;
        private void InputKeyboardNumber(string sInput)
        {
            if (iControl == 0)
            {
                if (txtSoLuong.Text.Length >= 15)
                {
                    int iPositon = txtGIA.SelectionStart;
                    XtraMessageBox.Show(_bkdnq15kt, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtSoLuong.SelectionStart = txtSoLuong.Text.Length;
                    txtSoLuong.SelectionStart = iPositon;
                    return;
                }
                if (txtSoLuong.Text == "0")
                {
                    if (sInput == "0" || sInput == "00" || sInput == "000")
                        return;
                }
                if (txtSoLuong.SelectedText == txtSoLuong.Text)
                {
                    txtSoLuong.Text = "0";
                }
                int iLenght, iPosition;
                iLenght = txtSoLuong.Text.Length;
                iPosition = txtSoLuong.SelectionStart;
                if (iPosition == 0)
                {
                    if (sInput == "0" || sInput == "00" || sInput == "000")
                        return;
                }
                if (txtSoLuong.Text == "0")
                { //17/08 khang thay đổi để nhập số 0.##
                    if (txtSoLuong.Text == "0" && sInput == ".")
                    {
                        iPosition = 2;
                        txtSoLuong.Text = "0" + sInput;
                    }
                    else
                        txtSoLuong.Text = sInput;
                }
                else
                {
                    txtSoLuong.Text = txtSoLuong.Text.Insert(iPosition, sInput);
                }
                if (sInput.Length == 1)
                {
                    if (txtSoLuong.Text.Length - iLenght == 1)
                    {
                        txtSoLuong.SelectionStart = iPosition + 1;
                    }
                    else
                    {
                        txtSoLuong.SelectionStart = iPosition + 2;
                    }
                }
                else if (sInput.Length == 2)
                {
                    if (txtSoLuong.Text.Length - iLenght == 2)
                    {
                        txtSoLuong.SelectionStart = iPosition + 2;
                    }
                    else
                    {
                        txtSoLuong.SelectionStart = iPosition + 3;
                    }
                }
                else if (sInput.Length == 3)
                {
                    if (txtSoLuong.Text.Length - iLenght == 3)
                    {
                        txtSoLuong.SelectionStart = iPosition + 3;
                    }
                    else
                    {
                        txtSoLuong.SelectionStart = iPosition + 4;
                    }
                }
            }
            else if (iControl == 1)
            {
                if (txtGIA.Text.Length >= 15)
                {
                    int iPositon = txtGIA.SelectionStart;
                    XtraMessageBox.Show(_bkdnq15kt, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtGIA.SelectionStart = txtGIA.Text.Length;
                    txtGIA.SelectionStart = iPositon;
                    return;
                }
                if (txtGIA.Text == "0")
                {
                    if (sInput == "0" || sInput == "00" || sInput == "000")
                        return;
                }
                if (txtGIA.SelectedText == txtGIA.Text)
                {
                    txtGIA.Text = "0";
                }
                int iLenght, iPosition;
                iLenght = txtGIA.Text.Length;
                iPosition = txtGIA.SelectionStart;
                if (iPosition == 0)
                {
                    if (sInput == "0" || sInput == "00" || sInput == "000")
                        return;
                }
                if (txtGIA.Text == "0")
                {
                    txtGIA.Text = sInput;
                }
                else
                {
                    txtGIA.Text = txtGIA.Text.Insert(iPosition, sInput);
                }
                if (sInput.Length == 1)
                {
                    if (txtGIA.Text.Length - iLenght == 1)
                    {
                        txtGIA.SelectionStart = iPosition + 1;
                    }
                    else
                    {
                        txtGIA.SelectionStart = iPosition + 2;
                    }
                }
                else if (sInput.Length == 2)
                {
                    if (txtGIA.Text.Length - iLenght == 2)
                    {
                        txtGIA.SelectionStart = iPosition + 2;
                    }
                    else
                    {
                        txtGIA.SelectionStart = iPosition + 3;
                    }
                }
                else if (sInput.Length == 3)
                {
                    if (txtGIA.Text.Length - iLenght == 3)
                    {
                        txtGIA.SelectionStart = iPosition + 3;
                    }
                    else
                    {
                        txtGIA.SelectionStart = iPosition + 4;
                    }
                }
            }

        }

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                if (txt.Name == txtSoLuong.Name)
                {
                    iControl = 0;
                }
                else
                {
                    iControl = 1;
                }
                if (!ctltxt[int.Parse(txt.Tag.ToString())])
                {
                    txt.SelectAll();
                    ctltxt[int.Parse(txt.Tag.ToString())] = true;
                }
                else
                {
                    ctltxt[int.Parse(txt.Tag.ToString())] = false;
                }
                
            }
            catch
            {
            }
        }

        private void DeleteTextBox()
        {
            if (iControl == 0)
            {
                int iPosition = txtSoLuong.SelectionStart;
                int iLenght = txtSoLuong.Text.Length;
                if (iPosition > 1)
                {
                    if (txtSoLuong.Text[iPosition - 1].ToString() == ",")
                    {
                        txtSoLuong.Text = txtSoLuong.Text.Remove(iPosition - 2, 1);

                        txtSoLuong.SelectionStart = iPosition - 2;

                    }
                    else
                    {
                        txtSoLuong.Text = txtSoLuong.Text.Remove(iPosition - 1, 1);
                        if (iLenght - txtSoLuong.Text.Length == 1)
                        {
                            txtSoLuong.SelectionStart = iPosition - 1;
                        }
                        else
                        {
                            txtSoLuong.SelectionStart = iPosition - 2;
                        }
                    }
                }
                else if (iPosition == 1)
                {
                    if (iLenght >= 2)
                    {
                        txtSoLuong.Text = txtSoLuong.Text.Remove(iPosition - 1, 1);
                        txtSoLuong.SelectionStart = iPosition - 1;
                        if (txtGIA.Text == "0")
                            txtSoLuong.SelectionStart = 1;
                    }
                    else
                    {
                        txtSoLuong.Text = "0";
                        txtSoLuong.SelectionStart = 1;
                    }
                }
            }
            else
            {
                int iPosition = txtGIA.SelectionStart;
                int iLenght = txtGIA.Text.Length;
                if (iPosition > 1)
                {
                    if (txtGIA.Text[iPosition - 1].ToString() == ",")
                    {
                        txtGIA.Text = txtGIA.Text.Remove(iPosition - 2, 1);

                        txtGIA.SelectionStart = iPosition - 2;

                    }
                    else
                    {
                        txtGIA.Text = txtGIA.Text.Remove(iPosition - 1, 1);
                        if (iLenght - txtGIA.Text.Length == 1)
                        {
                            txtGIA.SelectionStart = iPosition - 1;
                        }
                        else
                        {
                            txtGIA.SelectionStart = iPosition - 2;
                        }
                    }
                }
                else if (iPosition == 1)
                {
                    if (iLenght >= 2)
                    {
                        txtGIA.Text = txtGIA.Text.Remove(iPosition - 1, 1);
                        txtGIA.SelectionStart = iPosition - 1;
                        if (txtGIA.Text == "0")
                            txtGIA.SelectionStart = 1;
                    }
                    else
                    {
                        txtGIA.Text = "0";
                        txtGIA.SelectionStart = 1;
                    }
                }
            }
            
        }

        private void MoveToLeft()
        {
            if (iControl == 0)
            {
                int iPosition = txtSoLuong.SelectionStart;
                if (iPosition > 1)
                {
                    if (txtSoLuong.Text[iPosition - 1].ToString() == ",")
                    {
                        txtSoLuong.SelectionStart = iPosition - 2;
                    }
                    else
                    {
                        txtSoLuong.SelectionStart = iPosition - 1;
                    }
                }
                else
                {
                    txtSoLuong.SelectionStart = 0;
                }
            }
            else
            {
                int iPosition = txtGIA.SelectionStart;
                if (iPosition > 1)
                {
                    if (txtGIA.Text[iPosition - 1].ToString() == ",")
                    {
                        txtGIA.SelectionStart = iPosition - 2;
                    }
                    else
                    {
                        txtGIA.SelectionStart = iPosition - 1;
                    }
                }
                else
                {
                    txtGIA.SelectionStart = 0;

                }
            }

            
        }

        private void MoveToRight()
        {
            if (iControl == 0)
            {
                int iPosition = txtSoLuong.SelectionStart;
                if (iPosition < txtSoLuong.Text.Length - 1)
                {
                    if (txtSoLuong.Text[iPosition + 1].ToString() == ",")
                    {
                        txtSoLuong.SelectionStart = iPosition + 2;
                    }
                    else
                    {
                        txtSoLuong.SelectionStart = iPosition + 1;
                    }
                }
                else
                {
                    txtSoLuong.SelectionStart = txtSoLuong.Text.Length;

                }
            }
            else
            {
                int iPosition = txtGIA.SelectionStart;
                if (iPosition < txtGIA.Text.Length - 1)
                {
                    if (txtGIA.Text[iPosition + 1].ToString() == ",")
                    {
                        txtGIA.SelectionStart = iPosition + 2;
                    }
                    else
                    {
                        txtGIA.SelectionStart = iPosition + 1;
                    }
                }
                else
                {
                    txtGIA.SelectionStart = txtGIA.Text.Length;

                }
            }
            
        }

        private void btnLaytrongluong_Click(object sender, EventArgs e)
        {
            string sTrongluong = "1";
            try
            {
                string a = clsKP_Terminal.ReadDataCOM1(false);
                a = a.TrimEnd();
                string b = "";
                if (a.Length >= 8)
                {
                    b = a.Substring(a.Length - 8).Replace("kg", "").Trim();
                }
                else
                {
                    b = a.Replace("kg", "").Trim();
                }
                string c = b.Replace("g", "").Trim();
                sTrongluong = c;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            txtSoLuong.Text = sTrongluong;
            txtGIA.Focus();
        }

        private void Frm_Chontheothoigia_FormClosing(object sender, FormClosingEventArgs e)
        {
            bThoat = true;
        }

        private void txtSoLuong_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{Enter}");
                txtGIA.Focus();
            }
        }
    }
}