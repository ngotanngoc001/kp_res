﻿namespace KP_RES
{
    partial class Frm_GhiChu_TenKhach
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_GhiChu_TenKhach));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pnlKeyBoardText = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton14 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton13 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton12 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnP = new DevExpress.XtraEditors.SimpleButton();
            this.btnZ = new DevExpress.XtraEditors.SimpleButton();
            this.btnY = new DevExpress.XtraEditors.SimpleButton();
            this.btnShift = new DevExpress.XtraEditors.SimpleButton();
            this.btnX = new DevExpress.XtraEditors.SimpleButton();
            this.btnW = new DevExpress.XtraEditors.SimpleButton();
            this.btnV = new DevExpress.XtraEditors.SimpleButton();
            this.btnU = new DevExpress.XtraEditors.SimpleButton();
            this.btnT = new DevExpress.XtraEditors.SimpleButton();
            this.btnS = new DevExpress.XtraEditors.SimpleButton();
            this.btnF = new DevExpress.XtraEditors.SimpleButton();
            this.btnE = new DevExpress.XtraEditors.SimpleButton();
            this.btnD = new DevExpress.XtraEditors.SimpleButton();
            this.btnC = new DevExpress.XtraEditors.SimpleButton();
            this.btnB = new DevExpress.XtraEditors.SimpleButton();
            this.btnA = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoveRigthText = new DevExpress.XtraEditors.SimpleButton();
            this.btnM = new DevExpress.XtraEditors.SimpleButton();
            this.btnR = new DevExpress.XtraEditors.SimpleButton();
            this.btnQ = new DevExpress.XtraEditors.SimpleButton();
            this.btnOkText = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteText = new DevExpress.XtraEditors.SimpleButton();
            this.btnL = new DevExpress.XtraEditors.SimpleButton();
            this.btnI = new DevExpress.XtraEditors.SimpleButton();
            this.btnH = new DevExpress.XtraEditors.SimpleButton();
            this.btnG = new DevExpress.XtraEditors.SimpleButton();
            this.btnMoveLeftText = new DevExpress.XtraEditors.SimpleButton();
            this.btnK = new DevExpress.XtraEditors.SimpleButton();
            this.btnJ = new DevExpress.XtraEditors.SimpleButton();
            this.btnO = new DevExpress.XtraEditors.SimpleButton();
            this.btnN = new DevExpress.XtraEditors.SimpleButton();
            this.btnEscText = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.lblGhiChu = new DevExpress.XtraEditors.LabelControl();
            this.txtGhiChu = new DevExpress.XtraEditors.MemoEdit();
            this.us_keyboard1 = new KP_RES.Us_keyboard();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKeyBoardText)).BeginInit();
            this.pnlKeyBoardText.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChu.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pnlKeyBoardText);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 117);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(650, 214);
            this.panelControl1.TabIndex = 4;
            // 
            // pnlKeyBoardText
            // 
            this.pnlKeyBoardText.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlKeyBoardText.Controls.Add(this.panelControl3);
            this.pnlKeyBoardText.Controls.Add(this.simpleButton14);
            this.pnlKeyBoardText.Controls.Add(this.simpleButton13);
            this.pnlKeyBoardText.Controls.Add(this.simpleButton12);
            this.pnlKeyBoardText.Controls.Add(this.simpleButton11);
            this.pnlKeyBoardText.Controls.Add(this.simpleButton10);
            this.pnlKeyBoardText.Controls.Add(this.simpleButton7);
            this.pnlKeyBoardText.Controls.Add(this.simpleButton8);
            this.pnlKeyBoardText.Controls.Add(this.simpleButton9);
            this.pnlKeyBoardText.Controls.Add(this.simpleButton4);
            this.pnlKeyBoardText.Controls.Add(this.simpleButton5);
            this.pnlKeyBoardText.Controls.Add(this.simpleButton6);
            this.pnlKeyBoardText.Controls.Add(this.simpleButton3);
            this.pnlKeyBoardText.Controls.Add(this.simpleButton2);
            this.pnlKeyBoardText.Controls.Add(this.simpleButton1);
            this.pnlKeyBoardText.Controls.Add(this.btnP);
            this.pnlKeyBoardText.Controls.Add(this.btnZ);
            this.pnlKeyBoardText.Controls.Add(this.btnY);
            this.pnlKeyBoardText.Controls.Add(this.btnShift);
            this.pnlKeyBoardText.Controls.Add(this.btnX);
            this.pnlKeyBoardText.Controls.Add(this.btnW);
            this.pnlKeyBoardText.Controls.Add(this.btnV);
            this.pnlKeyBoardText.Controls.Add(this.btnU);
            this.pnlKeyBoardText.Controls.Add(this.btnT);
            this.pnlKeyBoardText.Controls.Add(this.btnS);
            this.pnlKeyBoardText.Controls.Add(this.btnF);
            this.pnlKeyBoardText.Controls.Add(this.btnE);
            this.pnlKeyBoardText.Controls.Add(this.btnD);
            this.pnlKeyBoardText.Controls.Add(this.btnC);
            this.pnlKeyBoardText.Controls.Add(this.btnB);
            this.pnlKeyBoardText.Controls.Add(this.btnA);
            this.pnlKeyBoardText.Controls.Add(this.btnMoveRigthText);
            this.pnlKeyBoardText.Controls.Add(this.btnM);
            this.pnlKeyBoardText.Controls.Add(this.btnR);
            this.pnlKeyBoardText.Controls.Add(this.btnQ);
            this.pnlKeyBoardText.Controls.Add(this.btnOkText);
            this.pnlKeyBoardText.Controls.Add(this.btnDeleteText);
            this.pnlKeyBoardText.Controls.Add(this.btnL);
            this.pnlKeyBoardText.Controls.Add(this.btnI);
            this.pnlKeyBoardText.Controls.Add(this.btnH);
            this.pnlKeyBoardText.Controls.Add(this.btnG);
            this.pnlKeyBoardText.Controls.Add(this.btnMoveLeftText);
            this.pnlKeyBoardText.Controls.Add(this.btnK);
            this.pnlKeyBoardText.Controls.Add(this.btnJ);
            this.pnlKeyBoardText.Controls.Add(this.btnO);
            this.pnlKeyBoardText.Controls.Add(this.btnN);
            this.pnlKeyBoardText.Controls.Add(this.btnEscText);
            this.pnlKeyBoardText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlKeyBoardText.Location = new System.Drawing.Point(2, 2);
            this.pnlKeyBoardText.Name = "pnlKeyBoardText";
            this.pnlKeyBoardText.Size = new System.Drawing.Size(646, 210);
            this.pnlKeyBoardText.TabIndex = 93;
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.us_keyboard1);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(646, 210);
            this.panelControl3.TabIndex = 118;
            // 
            // simpleButton14
            // 
            this.simpleButton14.AllowFocus = false;
            this.simpleButton14.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton14.Appearance.Options.UseFont = true;
            this.simpleButton14.Location = new System.Drawing.Point(464, 156);
            this.simpleButton14.Name = "simpleButton14";
            this.simpleButton14.Size = new System.Drawing.Size(45, 45);
            this.simpleButton14.TabIndex = 117;
            this.simpleButton14.Text = "/";
            // 
            // simpleButton13
            // 
            this.simpleButton13.AllowFocus = false;
            this.simpleButton13.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton13.Appearance.Options.UseFont = true;
            this.simpleButton13.Location = new System.Drawing.Point(363, 156);
            this.simpleButton13.Name = "simpleButton13";
            this.simpleButton13.Size = new System.Drawing.Size(45, 45);
            this.simpleButton13.TabIndex = 116;
            this.simpleButton13.Text = ",";
            // 
            // simpleButton12
            // 
            this.simpleButton12.AllowFocus = false;
            this.simpleButton12.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton12.Appearance.Options.UseFont = true;
            this.simpleButton12.Location = new System.Drawing.Point(414, 156);
            this.simpleButton12.Name = "simpleButton12";
            this.simpleButton12.Size = new System.Drawing.Size(45, 45);
            this.simpleButton12.TabIndex = 115;
            this.simpleButton12.Text = ".";
            // 
            // simpleButton11
            // 
            this.simpleButton11.AllowFocus = false;
            this.simpleButton11.Appearance.BackColor = System.Drawing.Color.GreenYellow;
            this.simpleButton11.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton11.Appearance.Options.UseBackColor = true;
            this.simpleButton11.Appearance.Options.UseFont = true;
            this.simpleButton11.Location = new System.Drawing.Point(183, 65);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(298, 45);
            this.simpleButton11.TabIndex = 114;
            // 
            // simpleButton10
            // 
            this.simpleButton10.AllowFocus = false;
            this.simpleButton10.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton10.Appearance.Options.UseFont = true;
            this.simpleButton10.Location = new System.Drawing.Point(515, 5);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(45, 45);
            this.simpleButton10.TabIndex = 113;
            this.simpleButton10.Text = "0";
            // 
            // simpleButton7
            // 
            this.simpleButton7.AllowFocus = false;
            this.simpleButton7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton7.Appearance.Options.UseFont = true;
            this.simpleButton7.Location = new System.Drawing.Point(413, 5);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(45, 45);
            this.simpleButton7.TabIndex = 112;
            this.simpleButton7.Text = "8";
            // 
            // simpleButton8
            // 
            this.simpleButton8.AllowFocus = false;
            this.simpleButton8.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton8.Appearance.Options.UseFont = true;
            this.simpleButton8.Location = new System.Drawing.Point(464, 5);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(45, 45);
            this.simpleButton8.TabIndex = 111;
            this.simpleButton8.Text = "9";
            // 
            // simpleButton9
            // 
            this.simpleButton9.AllowFocus = false;
            this.simpleButton9.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton9.Appearance.Options.UseFont = true;
            this.simpleButton9.Location = new System.Drawing.Point(362, 5);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(45, 45);
            this.simpleButton9.TabIndex = 110;
            this.simpleButton9.Text = "7";
            // 
            // simpleButton4
            // 
            this.simpleButton4.AllowFocus = false;
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.Location = new System.Drawing.Point(260, 5);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(45, 45);
            this.simpleButton4.TabIndex = 109;
            this.simpleButton4.Text = "5";
            // 
            // simpleButton5
            // 
            this.simpleButton5.AllowFocus = false;
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.Location = new System.Drawing.Point(311, 5);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(45, 45);
            this.simpleButton5.TabIndex = 108;
            this.simpleButton5.Text = "6";
            // 
            // simpleButton6
            // 
            this.simpleButton6.AllowFocus = false;
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.Location = new System.Drawing.Point(209, 5);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(45, 45);
            this.simpleButton6.TabIndex = 107;
            this.simpleButton6.Text = "4";
            // 
            // simpleButton3
            // 
            this.simpleButton3.AllowFocus = false;
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Location = new System.Drawing.Point(107, 4);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(45, 45);
            this.simpleButton3.TabIndex = 106;
            this.simpleButton3.Text = "2";
            // 
            // simpleButton2
            // 
            this.simpleButton2.AllowFocus = false;
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Location = new System.Drawing.Point(158, 4);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(45, 45);
            this.simpleButton2.TabIndex = 105;
            this.simpleButton2.Text = "3";
            // 
            // simpleButton1
            // 
            this.simpleButton1.AllowFocus = false;
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Location = new System.Drawing.Point(56, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(45, 45);
            this.simpleButton1.TabIndex = 104;
            this.simpleButton1.Text = "1";
            // 
            // btnP
            // 
            this.btnP.AllowFocus = false;
            this.btnP.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnP.Appearance.Options.UseFont = true;
            this.btnP.Location = new System.Drawing.Point(464, 56);
            this.btnP.Name = "btnP";
            this.btnP.Size = new System.Drawing.Size(45, 45);
            this.btnP.TabIndex = 78;
            this.btnP.Text = "P";
            // 
            // btnZ
            // 
            this.btnZ.AllowFocus = false;
            this.btnZ.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnZ.Appearance.Options.UseFont = true;
            this.btnZ.Location = new System.Drawing.Point(6, 156);
            this.btnZ.Name = "btnZ";
            this.btnZ.Size = new System.Drawing.Size(45, 45);
            this.btnZ.TabIndex = 103;
            this.btnZ.Text = "Z";
            // 
            // btnY
            // 
            this.btnY.AllowFocus = false;
            this.btnY.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnY.Appearance.Options.UseFont = true;
            this.btnY.Location = new System.Drawing.Point(260, 56);
            this.btnY.Name = "btnY";
            this.btnY.Size = new System.Drawing.Size(45, 45);
            this.btnY.TabIndex = 102;
            this.btnY.Text = "Y";
            // 
            // btnShift
            // 
            this.btnShift.AllowFocus = false;
            this.btnShift.Appearance.BackColor = System.Drawing.Color.GreenYellow;
            this.btnShift.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShift.Appearance.Options.UseBackColor = true;
            this.btnShift.Appearance.Options.UseFont = true;
            this.btnShift.Location = new System.Drawing.Point(79, 65);
            this.btnShift.Name = "btnShift";
            this.btnShift.Size = new System.Drawing.Size(96, 45);
            this.btnShift.TabIndex = 101;
            this.btnShift.Text = "Shift";
            // 
            // btnX
            // 
            this.btnX.AllowFocus = false;
            this.btnX.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnX.Appearance.Options.UseFont = true;
            this.btnX.Location = new System.Drawing.Point(57, 156);
            this.btnX.Name = "btnX";
            this.btnX.Size = new System.Drawing.Size(45, 45);
            this.btnX.TabIndex = 100;
            this.btnX.Text = "X";
            // 
            // btnW
            // 
            this.btnW.AllowFocus = false;
            this.btnW.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnW.Appearance.Options.UseFont = true;
            this.btnW.Location = new System.Drawing.Point(56, 56);
            this.btnW.Name = "btnW";
            this.btnW.Size = new System.Drawing.Size(45, 45);
            this.btnW.TabIndex = 96;
            this.btnW.Text = "W";
            // 
            // btnV
            // 
            this.btnV.AllowFocus = false;
            this.btnV.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnV.Appearance.Options.UseFont = true;
            this.btnV.Location = new System.Drawing.Point(159, 156);
            this.btnV.Name = "btnV";
            this.btnV.Size = new System.Drawing.Size(45, 45);
            this.btnV.TabIndex = 95;
            this.btnV.Text = "V";
            // 
            // btnU
            // 
            this.btnU.AllowFocus = false;
            this.btnU.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnU.Appearance.Options.UseFont = true;
            this.btnU.Location = new System.Drawing.Point(311, 56);
            this.btnU.Name = "btnU";
            this.btnU.Size = new System.Drawing.Size(45, 45);
            this.btnU.TabIndex = 94;
            this.btnU.Text = "U";
            // 
            // btnT
            // 
            this.btnT.AllowFocus = false;
            this.btnT.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnT.Appearance.Options.UseFont = true;
            this.btnT.Location = new System.Drawing.Point(209, 56);
            this.btnT.Name = "btnT";
            this.btnT.Size = new System.Drawing.Size(45, 45);
            this.btnT.TabIndex = 93;
            this.btnT.Text = "T";
            // 
            // btnS
            // 
            this.btnS.AllowFocus = false;
            this.btnS.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnS.Appearance.Options.UseFont = true;
            this.btnS.Location = new System.Drawing.Point(77, 106);
            this.btnS.Name = "btnS";
            this.btnS.Size = new System.Drawing.Size(45, 45);
            this.btnS.TabIndex = 92;
            this.btnS.Text = "S";
            // 
            // btnF
            // 
            this.btnF.AllowFocus = false;
            this.btnF.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnF.Appearance.Options.UseFont = true;
            this.btnF.Location = new System.Drawing.Point(181, 106);
            this.btnF.Name = "btnF";
            this.btnF.Size = new System.Drawing.Size(45, 45);
            this.btnF.TabIndex = 91;
            this.btnF.Text = "F";
            // 
            // btnE
            // 
            this.btnE.AllowFocus = false;
            this.btnE.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnE.Appearance.Options.UseFont = true;
            this.btnE.Location = new System.Drawing.Point(107, 56);
            this.btnE.Name = "btnE";
            this.btnE.Size = new System.Drawing.Size(45, 45);
            this.btnE.TabIndex = 90;
            this.btnE.Text = "E";
            // 
            // btnD
            // 
            this.btnD.AllowFocus = false;
            this.btnD.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnD.Appearance.Options.UseFont = true;
            this.btnD.Location = new System.Drawing.Point(129, 106);
            this.btnD.Name = "btnD";
            this.btnD.Size = new System.Drawing.Size(45, 45);
            this.btnD.TabIndex = 89;
            this.btnD.Text = "D";
            // 
            // btnC
            // 
            this.btnC.AllowFocus = false;
            this.btnC.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnC.Appearance.Options.UseFont = true;
            this.btnC.Location = new System.Drawing.Point(108, 156);
            this.btnC.Name = "btnC";
            this.btnC.Size = new System.Drawing.Size(45, 45);
            this.btnC.TabIndex = 88;
            this.btnC.Text = "C";
            // 
            // btnB
            // 
            this.btnB.AllowFocus = false;
            this.btnB.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnB.Appearance.Options.UseFont = true;
            this.btnB.Location = new System.Drawing.Point(210, 156);
            this.btnB.Name = "btnB";
            this.btnB.Size = new System.Drawing.Size(45, 45);
            this.btnB.TabIndex = 87;
            this.btnB.Text = "B";
            // 
            // btnA
            // 
            this.btnA.AllowFocus = false;
            this.btnA.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA.Appearance.Options.UseFont = true;
            this.btnA.Location = new System.Drawing.Point(25, 106);
            this.btnA.Name = "btnA";
            this.btnA.Size = new System.Drawing.Size(45, 45);
            this.btnA.TabIndex = 86;
            this.btnA.Text = "A";
            // 
            // btnMoveRigthText
            // 
            this.btnMoveRigthText.AllowFocus = false;
            this.btnMoveRigthText.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveRigthText.Appearance.Options.UseFont = true;
            this.btnMoveRigthText.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveRigthText.Image")));
            this.btnMoveRigthText.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveRigthText.Location = new System.Drawing.Point(566, 56);
            this.btnMoveRigthText.Name = "btnMoveRigthText";
            this.btnMoveRigthText.Size = new System.Drawing.Size(45, 45);
            this.btnMoveRigthText.TabIndex = 85;
            // 
            // btnM
            // 
            this.btnM.AllowFocus = false;
            this.btnM.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnM.Appearance.Options.UseFont = true;
            this.btnM.Location = new System.Drawing.Point(312, 156);
            this.btnM.Name = "btnM";
            this.btnM.Size = new System.Drawing.Size(45, 45);
            this.btnM.TabIndex = 84;
            this.btnM.Text = "M";
            // 
            // btnR
            // 
            this.btnR.AllowFocus = false;
            this.btnR.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnR.Appearance.Options.UseFont = true;
            this.btnR.Location = new System.Drawing.Point(158, 56);
            this.btnR.Name = "btnR";
            this.btnR.Size = new System.Drawing.Size(45, 45);
            this.btnR.TabIndex = 81;
            this.btnR.Text = "R";
            // 
            // btnQ
            // 
            this.btnQ.AllowFocus = false;
            this.btnQ.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQ.Appearance.Options.UseFont = true;
            this.btnQ.Location = new System.Drawing.Point(5, 56);
            this.btnQ.Name = "btnQ";
            this.btnQ.Size = new System.Drawing.Size(45, 45);
            this.btnQ.TabIndex = 80;
            this.btnQ.Text = "Q";
            // 
            // btnOkText
            // 
            this.btnOkText.AllowFocus = false;
            this.btnOkText.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOkText.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.btnOkText.Appearance.Options.UseFont = true;
            this.btnOkText.Appearance.Options.UseForeColor = true;
            this.btnOkText.Location = new System.Drawing.Point(515, 105);
            this.btnOkText.Name = "btnOkText";
            this.btnOkText.Size = new System.Drawing.Size(95, 96);
            this.btnOkText.TabIndex = 66;
            this.btnOkText.Text = "OK";
            // 
            // btnDeleteText
            // 
            this.btnDeleteText.AllowFocus = false;
            this.btnDeleteText.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteText.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btnDeleteText.Appearance.Options.UseFont = true;
            this.btnDeleteText.Appearance.Options.UseForeColor = true;
            this.btnDeleteText.Location = new System.Drawing.Point(566, 5);
            this.btnDeleteText.Name = "btnDeleteText";
            this.btnDeleteText.Size = new System.Drawing.Size(45, 45);
            this.btnDeleteText.TabIndex = 79;
            this.btnDeleteText.Text = "Del";
            // 
            // btnL
            // 
            this.btnL.AllowFocus = false;
            this.btnL.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnL.Appearance.Options.UseFont = true;
            this.btnL.Location = new System.Drawing.Point(441, 107);
            this.btnL.Name = "btnL";
            this.btnL.Size = new System.Drawing.Size(45, 45);
            this.btnL.TabIndex = 74;
            this.btnL.Text = "L";
            // 
            // btnI
            // 
            this.btnI.AllowFocus = false;
            this.btnI.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnI.Appearance.Options.UseFont = true;
            this.btnI.Location = new System.Drawing.Point(362, 56);
            this.btnI.Name = "btnI";
            this.btnI.Size = new System.Drawing.Size(45, 45);
            this.btnI.TabIndex = 77;
            this.btnI.Text = "I";
            // 
            // btnH
            // 
            this.btnH.AllowFocus = false;
            this.btnH.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnH.Appearance.Options.UseFont = true;
            this.btnH.Location = new System.Drawing.Point(285, 106);
            this.btnH.Name = "btnH";
            this.btnH.Size = new System.Drawing.Size(45, 45);
            this.btnH.TabIndex = 76;
            this.btnH.Text = "H";
            // 
            // btnG
            // 
            this.btnG.AllowFocus = false;
            this.btnG.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnG.Appearance.Options.UseFont = true;
            this.btnG.Location = new System.Drawing.Point(233, 106);
            this.btnG.Name = "btnG";
            this.btnG.Size = new System.Drawing.Size(45, 45);
            this.btnG.TabIndex = 75;
            this.btnG.Text = "G";
            // 
            // btnMoveLeftText
            // 
            this.btnMoveLeftText.AllowFocus = false;
            this.btnMoveLeftText.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveLeftText.Appearance.Options.UseFont = true;
            this.btnMoveLeftText.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveLeftText.Image")));
            this.btnMoveLeftText.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveLeftText.Location = new System.Drawing.Point(515, 56);
            this.btnMoveLeftText.Name = "btnMoveLeftText";
            this.btnMoveLeftText.Size = new System.Drawing.Size(45, 45);
            this.btnMoveLeftText.TabIndex = 73;
            // 
            // btnK
            // 
            this.btnK.AllowFocus = false;
            this.btnK.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnK.Appearance.Options.UseFont = true;
            this.btnK.Location = new System.Drawing.Point(389, 106);
            this.btnK.Name = "btnK";
            this.btnK.Size = new System.Drawing.Size(45, 45);
            this.btnK.TabIndex = 72;
            this.btnK.Text = "K";
            // 
            // btnJ
            // 
            this.btnJ.AllowFocus = false;
            this.btnJ.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnJ.Appearance.Options.UseFont = true;
            this.btnJ.Location = new System.Drawing.Point(337, 106);
            this.btnJ.Name = "btnJ";
            this.btnJ.Size = new System.Drawing.Size(45, 45);
            this.btnJ.TabIndex = 71;
            this.btnJ.Text = "J";
            // 
            // btnO
            // 
            this.btnO.AllowFocus = false;
            this.btnO.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnO.Appearance.Options.UseFont = true;
            this.btnO.Location = new System.Drawing.Point(413, 56);
            this.btnO.Name = "btnO";
            this.btnO.Size = new System.Drawing.Size(45, 45);
            this.btnO.TabIndex = 70;
            this.btnO.Text = "O";
            // 
            // btnN
            // 
            this.btnN.AllowFocus = false;
            this.btnN.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnN.Appearance.Options.UseFont = true;
            this.btnN.Location = new System.Drawing.Point(261, 156);
            this.btnN.Name = "btnN";
            this.btnN.Size = new System.Drawing.Size(45, 45);
            this.btnN.TabIndex = 69;
            this.btnN.Text = "N";
            // 
            // btnEscText
            // 
            this.btnEscText.AllowFocus = false;
            this.btnEscText.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEscText.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnEscText.Appearance.Options.UseFont = true;
            this.btnEscText.Appearance.Options.UseForeColor = true;
            this.btnEscText.Location = new System.Drawing.Point(5, 5);
            this.btnEscText.Name = "btnEscText";
            this.btnEscText.Size = new System.Drawing.Size(45, 45);
            this.btnEscText.TabIndex = 67;
            this.btnEscText.Text = "ESC";
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.lblGhiChu);
            this.panelControl2.Controls.Add(this.txtGhiChu);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(650, 117);
            this.panelControl2.TabIndex = 13;
            // 
            // lblGhiChu
            // 
            this.lblGhiChu.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGhiChu.Location = new System.Drawing.Point(8, 18);
            this.lblGhiChu.Name = "lblGhiChu";
            this.lblGhiChu.Size = new System.Drawing.Size(44, 16);
            this.lblGhiChu.TabIndex = 4;
            this.lblGhiChu.Text = "Tên KH";
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.EditValue = "";
            this.txtGhiChu.Location = new System.Drawing.Point(81, 12);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txtGhiChu.Properties.Appearance.Options.UseFont = true;
            this.txtGhiChu.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGhiChu.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtGhiChu.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGhiChu.Size = new System.Drawing.Size(559, 94);
            this.txtGhiChu.TabIndex = 3;
            // 
            // us_keyboard1
            // 
            this.us_keyboard1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.us_keyboard1.Location = new System.Drawing.Point(-3, -1);
            this.us_keyboard1.Name = "us_keyboard1";
            this.us_keyboard1.Size = new System.Drawing.Size(650, 211);
            this.us_keyboard1.TabIndex = 0;
            // 
            // Frm_GhiChu_TenKhach
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 331);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_GhiChu_TenKhach";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tên Khách Hàng";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frm_GhiChu_TenKhach_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_Giaohang_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlKeyBoardText)).EndInit();
            this.pnlKeyBoardText.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChu.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl pnlKeyBoardText;
        private DevExpress.XtraEditors.SimpleButton btnP;
        private DevExpress.XtraEditors.SimpleButton btnZ;
        private DevExpress.XtraEditors.SimpleButton btnY;
        private DevExpress.XtraEditors.SimpleButton btnShift;
        private DevExpress.XtraEditors.SimpleButton btnX;
        private DevExpress.XtraEditors.SimpleButton btnW;
        private DevExpress.XtraEditors.SimpleButton btnV;
        private DevExpress.XtraEditors.SimpleButton btnU;
        private DevExpress.XtraEditors.SimpleButton btnT;
        private DevExpress.XtraEditors.SimpleButton btnS;
        private DevExpress.XtraEditors.SimpleButton btnF;
        private DevExpress.XtraEditors.SimpleButton btnE;
        private DevExpress.XtraEditors.SimpleButton btnD;
        private DevExpress.XtraEditors.SimpleButton btnC;
        private DevExpress.XtraEditors.SimpleButton btnB;
        private DevExpress.XtraEditors.SimpleButton btnA;
        private DevExpress.XtraEditors.SimpleButton btnMoveRigthText;
        private DevExpress.XtraEditors.SimpleButton btnM;
        private DevExpress.XtraEditors.SimpleButton btnR;
        private DevExpress.XtraEditors.SimpleButton btnQ;
        private DevExpress.XtraEditors.SimpleButton btnOkText;
        private DevExpress.XtraEditors.SimpleButton btnDeleteText;
        private DevExpress.XtraEditors.SimpleButton btnL;
        private DevExpress.XtraEditors.SimpleButton btnI;
        private DevExpress.XtraEditors.SimpleButton btnH;
        private DevExpress.XtraEditors.SimpleButton btnG;
        private DevExpress.XtraEditors.SimpleButton btnMoveLeftText;
        private DevExpress.XtraEditors.SimpleButton btnK;
        private DevExpress.XtraEditors.SimpleButton btnJ;
        private DevExpress.XtraEditors.SimpleButton btnO;
        private DevExpress.XtraEditors.SimpleButton btnN;
        private DevExpress.XtraEditors.SimpleButton btnEscText;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LabelControl lblGhiChu;
        private DevExpress.XtraEditors.SimpleButton simpleButton14;
        private DevExpress.XtraEditors.SimpleButton simpleButton13;
        private DevExpress.XtraEditors.SimpleButton simpleButton12;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private Us_keyboard us_keyboard1;
        private DevExpress.XtraEditors.MemoEdit txtGhiChu;
    }
}