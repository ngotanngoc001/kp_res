﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using KP_RES.Class;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Data.SqlClient;
using KP_Report;
using DevExpress.XtraGrid.Views.Grid;
using KP_Terminal;

namespace KP_RES 
{
    public partial class Frm_Payment_Nhahang : DevExpress.XtraEditors.XtraForm
    {
        public bool bHoanTacThanhToan = false;
        TextEdit txtInput=new TextEdit();//txt get input
        bool bShift = true;//true is upperText,false is lower text
        bool bText = false;//true is permit input,false is not permit
        bool bPrintBill = true;//true print bill.false is not print bill
        bool bVisaCard = true; //true is not payment visa,false were payment visa
        bool bVoucher = true; //true is not payment Voucher,false were payment Voucher
        string _sIdTable = "";
        string _sTenBan = "";
        string _sIdBill = "";
        string idVoucher = "";//ID Voucher
        string giatriVoucher = "";//ID Voucher
        decimal dMoneyVisa = 0;//total payment = visa
        decimal dMoneyVoucher = 0;//total payment = voucher
        decimal dMoneyInnerCard = 0;//total payment = inner card
        decimal dMoneyCash = 0;//total payment = Cash
        string soThe = "0";
        string ngayThe = "0";
        string GH_diachi = "";
        string GH_MaKH = "";
        string GH_TenKH = "";
        string GH_Dienthoai = "";
        bool GH_chk_diachikhac = false;
        decimal _dMoneygetFirst = 0;
        decimal _dMoneyHour = 0;
        decimal per_Discount = 0;
        decimal per_Extra = 0;
        DataTable dtGridviewProduct=new DataTable();//data list product sale 
        public bool CN_Flag = false;
        public string CN_MaKH = "";
        decimal CN_SoTienTra = 0;
        DateTime CN_ngayHenTra = DateTime.Now;
        public int checkPrinter = 0;
        string GhiChu = "";
        decimal Number = 0;
        string sTenKhach = "";
        bool sInTem = false;
        decimal dTienPhieuMuaHang = 0;
        string sGhiChuPhieuMuaHang = "";
        public Frm_Payment_Nhahang(string sIdTable, string sIdBill, DataTable dtGridview, decimal dMoneygetFirst,string sTenBan)
        {
            InitializeComponent();
            _sIdBill = sIdBill;
            _sIdTable = sIdTable;
            _sTenBan = sTenBan;
            dtGridviewProduct = dtGridview;
            _dMoneygetFirst = dMoneygetFirst;
            sInTem = KiemTraCot(dtGridview);
        }

        private void Payment_Nhahang_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F3)
            {
                btnNotPrintBill_Click(null, null);
            }
            if (e.KeyCode == Keys.F2)
            {
                btnPaymentText_Click(null, null);
            }
            if (e.KeyCode == Keys.F4)
            {
                btnPaymentTachBill_Click(null, null);
            }
            if (e.KeyCode == Keys.Escape)
            {
                btnCancelPayment_Click(null, null);
            }
            int iPosition = txtInput.SelectionStart;
            int iLenght = txtInput.Text.Length;
            if (e.KeyCode == Keys.Delete)
            {

                if (iLenght > 2)
                {
                    if (iPosition == 0)
                    {
                        if (txtInput.Text[1] == ',')
                            txtInput.Text = txtInput.Text.Remove(0, 2);
                        else
                            txtInput.Text = txtInput.Text.Remove(0, 1);

                        txtInput.SelectionStart = 0;
                        e.Handled = true;
                    }
                }
            }
            else if (e.KeyCode == Keys.Back)
            {

                if (iPosition == 1)
                {
                    txtInput.Text = txtInput.Text.Remove(iPosition - 1, 1);
                    txtInput.SelectionStart = iPosition - 1;
                }
                else if (iPosition == 2)
                {
                    if (txtInput.Text[1] == ',')
                    {
                        txtInput.Text = txtInput.Text.Remove(iPosition - 2, 2);
                        txtInput.SelectionStart = iPosition - 2;
                    }
                }
            }
        }

        private void Frm_Payment_Nhahang_FormClosing(object sender, FormClosingEventArgs e)
        {
            cls_KP_RES.iChietkhau_Manhinhhai = 0;
        }

        string _ngay = "";
        string _tienmat = "";
        string _thongbao = "";
        string _ttkdnq3l = "";
        string _lhstkt = "";
        string _makhttkd = "";
        string _tienthettnb = "";
        string _matttnbkd = "";
        string _mathevoucherktt = "";
        string _mathevoucherdsd = "";
        string _mathevouchercactive = "";
        string _mathevoucherdhhsd = "";
        string _vouchergiam = "";
        string _bkdnq15kt = "";
        string _bkdcnkhtt = "";
        string _bkdcnkhttnb = "";
        string _banchuanhapsothe = "";
        string _banchuanhapsotien = "";
        string _maantoankhongdung = "";
        string _tienthevisa = "";
        string _chuacobanff = "";
        string _chuatthettien = "";
        string _phieuMuaHangGiam = "Phiếu mua hàng giảm: ";
        private void Payment_Load(object sender, EventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            _ngay = rm.GetString("ngayht", culture);
            btn_ThuNgan.Text = rm.GetString("THANHTOAN", culture);
            btn_DatThu.Text = rm.GetString("datthu", culture);
            btnCongNo.Text = rm.GetString("congno", culture);
            btnMakhuyenmai.Text = rm.GetString("makhuyenmai", culture);
            btnServiceMoney.Text = rm.GetString("phiphucvu", culture);
            btnCardInternalPayment.Text = rm.GetString("thettnb", culture);
            btnNotPrintBill.Text = rm.GetString("f3khonginhoadon", culture);
            btnDiscount.Text = rm.GetString("chietkhau", culture);
            btnCardImtimateCustomer.Text = rm.GetString("khtt", culture);
            btnVisa.Text = rm.GetString("thevisa", culture);
            btnRetypePayment.Text = rm.GetString("nhaplaitientra", culture);
            btnGiaohang.Text = rm.GetString("giaohang", culture);
            btnCancelPayment.Text = rm.GetString("huythanhtoan", culture);
            //gvListProduct.Columns["STT"].Caption = rm.GetString("stt", culture);
            gvListProduct.Columns["TEN_HANGHOA"].Caption = rm.GetString("tenhanghoa", culture);
            gvListProduct.Columns["SOLUONG"].Caption = rm.GetString("soluong", culture);
            gvListProduct.Columns["GIABAN"].Caption = rm.GetString("dongia", culture);
            //gvListProduct.Columns["CHIETKHAU"].Caption = rm.GetString("ck", culture);
            //gvListProduct.Columns["Thanh_Tien"].Caption = rm.GetString("thanhtien", culture);
            lb_thanhtien.Text = rm.GetString("thanhtien", culture);
            lb_tiengio.Text = rm.GetString("tiengio", culture);
            lb_tongcong.Text = rm.GetString("tongcong", culture);
            gvListForiegnCurrency.Columns["STT1"].Caption = rm.GetString("stt", culture);
            gvListForiegnCurrency.Columns["TEN"].Caption = rm.GetString("nguyente", culture);
            gvListForiegnCurrency.Columns["TYGIA"].Caption = rm.GetString("tygia", culture);
            btnPaymentText.Text = rm.GetString("inhoadon", culture);
            btnPaymentMoney.Text = rm.GetString("inhoadon", culture);
            btnPaymentNumber.Text = rm.GetString("inhoadon", culture);
            lb_tygia.Text = rm.GetString("tygia1", culture);
            lb_loaitien.Text = rm.GetString("loaitien", culture);
            lb_tongthanhtoan.Text = rm.GetString("tongthanhtoan", culture);
            lb_chietkhau.Text = rm.GetString("chietkhau", culture);
            lb_phiphucvu.Text = rm.GetString("phiphucvu", culture);
            lb_tiendatcoc.Text = rm.GetString("tiendatcoc", culture);
            lb_TCthanhtoan.Text = rm.GetString("TCthanhtoan", culture);
            lb_tienkhachtra.Text = rm.GetString("tienkhachtra", culture);
            lb_tientralai.Text = rm.GetString("tientralai", culture);
            lb_nhapVoucher.Text = rm.GetString("nhapvoucher", culture);
            lbPayment.Text = rm.GetString("nhaptientra", culture);
            lb_nhapmaTTNB.Text = rm.GetString("nhapmaTTNB", culture);
            lb_nhaptienphucvu.Text = rm.GetString("nhaptienphucvu", culture);
            lb_nhaptienphucvu.Text = rm.GetString("nhaptienphucvu", culture);
            lb_nhapma_KHTT.Text = rm.GetString("nhapma_KHTT", culture);
            lb_nhapchietkhau.Text = rm.GetString("nhapchietkhau", culture);
            _tienmat = rm.GetString("tienmat", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _ttkdnq3l = rm.GetString("ttkdnq3l", culture);
            _lhstkt = rm.GetString("lhstkt", culture);
            _makhttkd = rm.GetString("makhttkd", culture);
            _tienthettnb = rm.GetString("tienthettnb", culture);
            _matttnbkd = rm.GetString("matttnbkd", culture);
            _mathevoucherktt = rm.GetString("mathevoucherktt", culture);
            _mathevoucherdsd = rm.GetString("mathevoucherdsd", culture);
            _mathevouchercactive = rm.GetString("mathevouchercactive", culture);
            _mathevoucherdhhsd = rm.GetString("mathevoucherdhhsd", culture);
            _maantoankhongdung = rm.GetString("maantoankhongdung", culture);
            _bkdcnkhtt = rm.GetString("bkdcnkhtt", culture);
            _vouchergiam = rm.GetString("vouchergiam", culture);
            _bkdnq15kt = rm.GetString("bkdnq15kt", culture);
            _bkdcnkhttnb = rm.GetString("bkdcnkhttnb", culture);
            _banchuanhapsothe = rm.GetString("banchuanhapsothe", culture);
            _banchuanhapsotien = rm.GetString("banchuanhapsotien", culture);
            _tienthevisa = rm.GetString("tienthevisa", culture);
            _chuacobanff = rm.GetString("chuacobanff", culture);
            _chuatthettien = rm.GetString("chuatthettien", culture);
            lb_thanhtoanbangthe.Text = rm.GetString("ttbttdvgnqt", culture);
            lb_thongtinbatbuoc.Text = rm.GetString("ttbb", culture);
            lb_thongtinthem.Text = rm.GetString("thongtinthem", culture);
            lb_sothe.Text = rm.GetString("sothe", culture);
            lb_sotien.Text = rm.GetString("sotien", culture);
            lb_ngayhethan.Text = rm.GetString("ngayhethan", culture);
            lb_tenchuthe.Text = rm.GetString("tenchuthe", culture);
            lb_quocgia.Text = rm.GetString("quocgia", culture);
            lb_diachichuthe.Text = rm.GetString("diachichuthe", culture);
            lb_tinhtp.Text = rm.GetString("tinhtp", culture);
            lb_maantoan.Text = rm.GetString("maantoan", culture);
            btnPaymentVisa.Text = rm.GetString("giaodich", culture);
            bntCancelVisa.Text = rm.GetString("huygiaodich", culture);
            labelControl21.Text = rm.GetString("congno", culture);
            labelControl16.Text = rm.GetString("makh", culture);
            labelControl3.Text = rm.GetString("dienthoai", culture);
            labelControl13.Text = rm.GetString("sotien2", culture);
            labelControl1.Text = rm.GetString("ngaytra", culture);
            //_chuachonmayin = rm.GetString("chuachonmayin", culture);
            //_hoadondaduocthanhtoan = rm.GetString("hoadondaduocthanhtoan", culture);
            //_ngaytranhohonngayhientai = rm.GetString("ngaytranhohonngayhientai", culture);
            //_vuotquahanmucchophep = rm.GetString("vuotquahanmucchophep", culture);
            //_vuotquasotiendatcoc = rm.GetString("vuotquasotiendatcoc", culture);
            //_khonghople = rm.GetString("khonghople", culture);
            //_khachhangquahanthanhtoan = rm.GetString("khachhangquahanthanhtoan", culture);
            btnKhachhang.Text = rm.GetString("khachhang", culture);
            btnGhichu.Text = rm.GetString("ghichu", culture);
            btnNhanvienthuchien.Text = rm.GetString("nhanvienthuchien", culture);


            SetBackgroudOnClick(btn_ThuNgan);

            LoadForiegnCurrency();
            LoadDataPrepare();
            LoadKeyboardMoney();
            LoadKeyboardNumber();
            LoadKeyboardText();
        }

        private void LoadForiegnCurrency()
        {
            DataTable dtForiegnCurrency = clsMain.ReturnDataTable("exec GETTYGIA");
            gcForeignCurrency.DataSource = dtForiegnCurrency;
            if (dtForiegnCurrency.Rows.Count > 0)
            {
                lbKindMoney.Text = dtForiegnCurrency.Rows[0]["MA"].ToString();
                lbExchangeRate.Text = dtForiegnCurrency.Rows[0]["TYGIA"].ToString();
            }
        }

        private void LoadDataPrepare()
        {
            lb_Quay.Text = cls_ConfigCashier.nameCashier;
            lb_Kho.Text = cls_ConfigCashier.nameWarehouse;
            lb_NhanVien.Text = clsUserManagement.ReturnNameOfUserLogin();
            lbCaBan.Text = DateTime.Now.ToString("dd-MM-yyyy");
            lbtimeby.Text = cls_ConfigCashier.sTenCB + ": " + cls_ConfigCashier.sGioBD + " --> " + cls_ConfigCashier.sGioKT;
            lbInforName.Text = _sTenBan == "" ? _sIdTable : _sTenBan;
            gcListProduct.DataSource = dtGridviewProduct;
            decimal fTotalMoney = 0;
            if (dtGridviewProduct.Rows.Count > 0)
            {
                for (int i = 0; i < dtGridviewProduct.Rows.Count; i++)
                {
                    fTotalMoney += decimal.Parse(dtGridviewProduct.Rows[i]["Thanh_Tien"].ToString());
                }
            }
            if (dtGridviewProduct.Rows.Count >= 7)
            {
                pnUpDownGridview.Visible = true;
            }
            lbTheMoney.Tag = fTotalMoney;
            lbTheMoney.Text = string.Format("{0:#,###0}", fTotalMoney);
            lbHourMoney.Tag = _dMoneyHour;
            lbHourMoney.Text = string.Format("{0:#,###0}", _dMoneyHour);
            lbMoneyGetFirst.Tag = _dMoneygetFirst;
            lbMoneyGetFirst.Text = string.Format("{0:#,###0}", _dMoneygetFirst);
            lbReplyCustomer.Text = "0";

            pnlForeignCurrency.Size = new Size((pnlBottomPayment.Width / 100) * 35, 294);
            pnlPhiPhuVu.Visible = pnlTTTNB.Visible = pnlKHTT.Visible = pnlChietKhau.Visible = pnlVoucher.Visible = pnlMagiamgia.Visible = false;
            pnlTienKhachTra.Visible = true;
            int iWidth = pnlMoneyReply.Width + pnlMoneyReply.Location.X - lbVNDReply.Width - pnlKeyboardAndShowForeignCurrency.Location.X;
            txtInnerCustomer.Size = txtDearCustomer.Size = txtInputMoney.Size = txtServiceMoney.Size = txtInputDiscount.Size = txtInputVoucher.Size = txtInputMagiamgia.Size = txtNhapPhieuMuaHang.Size = new Size(iWidth, 32);
            txtInnerCustomer.Location = txtDearCustomer.Location = txtInputMoney.Location = txtServiceMoney.Location = txtInputDiscount.Location = txtInputVoucher.Location = txtInputMagiamgia.Location = txtNhapPhieuMuaHang.Location = new Point(pnlKeyboardAndShowForeignCurrency.Location.X - pnlChietKhau.Location.X, 7);

            pnlKeyboardMoney.BringToFront();
            pnlShowInforPayment.BringToFront();
            txtInput = txtInputMoney;
            txtInput.KeyPress += new KeyPressEventHandler(txtInput_KeyPress);
            txtInput.Focus();
            txtInput.Text = lbTotalMoneyAll.Text;
            txtInput.SelectAll();
        }

        private void LoadKeyboardMoney()
        {
            int iWidth = (pnlKeyboardMoney.Width - 5) / 5;
            int iSurplusWidth = (pnlKeyboardMoney.Width - 5) % 5;
            int iHeight = (pnlKeyboardMoney.Height - 5) / 4;
            int iSurplusHeight = (pnlKeyboardMoney.Height - 5) % 4;
            int iAlignWidth = (10 + iSurplusWidth) / 2;
            int iAlignHeight = (10 + iSurplusHeight) / 2;
            //set size for button
            btnMoveLeftMoney.Size = btnMoveRightMoney.Size = btnDeleteMoney.Size = btnTranslateMoney.Size = btnEcsMoney.Size = btnOkMoney.Size = btn500.Size = btn1000.Size = btn2000.Size = btn5000.Size = btn10000.Size = btn20000.Size = btn50000.Size = btn100000.Size = btn200000.Size = btn500000.Size = new Size(iWidth - 5, iHeight - 5);
            btnPaymentMoney.Size = new Size((iWidth * 2 - 5), (iHeight * 2 - 5));
            //set location for button
            //dòng 1
            btn500.Location = new Point(iAlignWidth, iAlignHeight);
            btn1000.Location = new Point(btn500.Location.X + iWidth, btn500.Location.Y);
            btn2000.Location = new Point(btn1000.Location.X + iWidth, btn500.Location.Y);
            btn5000.Location = new Point(btn2000.Location.X + iWidth, btn500.Location.Y);
            btn10000.Location = new Point(btn5000.Location.X + iWidth, btn500.Location.Y);
            //dòng 2
            btn20000.Location = new Point(iAlignWidth, btn500.Location.Y + iHeight);
            btn50000.Location = new Point(btn20000.Location.X + iWidth, btn20000.Location.Y);
            btn100000.Location = new Point(btn50000.Location.X + iWidth, btn20000.Location.Y);
            btn200000.Location = new Point(btn100000.Location.X + iWidth, btn20000.Location.Y);
            btn500000.Location = new Point(btn200000.Location.X + iWidth, btn20000.Location.Y);
            //dòng 3
            btnMoveLeftMoney.Location = new Point(iAlignWidth, btn20000.Location.Y + iHeight);
            btnMoveRightMoney.Location = new Point(btnMoveLeftMoney.Location.X + iWidth, btnMoveLeftMoney.Location.Y);
            btnDeleteMoney.Location = new Point(btnMoveRightMoney.Location.X + iWidth, btnMoveLeftMoney.Location.Y);
            btnPaymentMoney.Location = new Point(btnDeleteMoney.Location.X + iWidth, btnMoveLeftMoney.Location.Y);
            //dòng 4
            btnTranslateMoney.Location = new Point(iAlignWidth, btnMoveLeftMoney.Location.Y + iHeight);
            btnEcsMoney.Location = new Point(btnTranslateMoney.Location.X + iWidth, btnTranslateMoney.Location.Y);
            btnOkMoney.Location = new Point(btnEcsMoney.Location.X + iWidth, btnTranslateMoney.Location.Y);
        }

        private void LoadKeyboardNumber()
        {
            int iWidth = (pnlKeyboardNumber.Width - 5) / 6;
            int iSurplusWidth = (pnlKeyboardNumber.Width - 5) % 6;
            int iHeight = (pnlKeyboardNumber.Height - 5) / 4;
            int iSurplusHeight = (pnlKeyboardNumber.Height - 5) % 4;
            int iAlignWidth = (10 + iSurplusWidth) / 2;
            int iAlignHeight = (10 + iSurplusHeight) / 2;
            //set size for button
            btnDeleteNumber.Size = btnMoveLeftNumber.Size = btnMoveRightNumber.Size = btn0.Size = btn00.Size = btn000.Size = btn1.Size = btn2.Size = btn3.Size = btn4.Size = btn5.Size = btn6.Size = btn7.Size = btn8.Size = btn9.Size = btnTranslateNumber.Size = new Size(iWidth - 5, iHeight - 5);
            btnPaymentNumber.Size = new Size(iWidth * 2 - 5, iHeight * 2 - 5);
            btnEscNumber.Size = new Size(iWidth - 5, iHeight * 2 - 5);
            btnOkNumber.Size = new Size(iWidth * 2 - 5, iHeight - 5);
            //set location for button
            //dòng 1
            btn7.Location = new Point(iAlignWidth, iAlignHeight);
            btn8.Location = new Point(btn7.Location.X + iWidth, btn7.Location.Y);
            btn9.Location = new Point(btn8.Location.X + iWidth, btn7.Location.Y);
            btnMoveLeftNumber.Location = new Point(btn9.Location.X + iWidth, btn7.Location.Y);
            btnMoveRightNumber.Location = new Point(btnMoveLeftNumber.Location.X + iWidth, btn7.Location.Y);
            btnDeleteNumber.Location = new Point(btnMoveRightNumber.Location.X + iWidth, btn7.Location.Y);
            //dòng 2
            btn4.Location = new Point(iAlignWidth, btn7.Location.Y + iHeight);
            btn5.Location = new Point(btn4.Location.X + iWidth, btn4.Location.Y);
            btn6.Location = new Point(btn5.Location.X + iWidth, btn4.Location.Y);
            btnEscNumber.Location = new Point(btn6.Location.X + iWidth, btn4.Location.Y);
            btnOkNumber.Location = new Point(btnEscNumber.Location.X + iWidth, btn4.Location.Y);
            //dòng 3
            btn1.Location = new Point(iAlignWidth, btn4.Location.Y + iHeight);
            btn2.Location = new Point(btn1.Location.X + iWidth, btn1.Location.Y);
            btn3.Location = new Point(btn2.Location.X + iWidth, btn1.Location.Y);
            btnPaymentNumber.Location = new Point(btnOkNumber.Location.X, btn1.Location.Y);
            //dòng 4
            btnTranslateNumber.Location = new Point(iAlignWidth, btn1.Location.Y + iHeight);
            btn0.Location = new Point(btnTranslateNumber.Location.X + iWidth, btnTranslateMoney.Location.Y);
            btn00.Location = new Point(btn0.Location.X + iWidth, btnTranslateMoney.Location.Y);
            btn000.Location = new Point(btn00.Location.X + iWidth, btnTranslateMoney.Location.Y);
        }

        private void LoadKeyboardText()
        {
            int iWidth = (pnlKeyBoardText.Width - 5) / 10;
            int iSurplusWidth = (pnlKeyBoardText.Width - 5) % 10;
            int iHeight = (pnlKeyBoardText.Height - 5) / 5;
            int iSurplusHeight = (pnlKeyBoardText.Height - 5) % 5;
            int iAlignWidth = (10 + iSurplusWidth) / 2;
            int iAlignHeight = (10 + iSurplusHeight) / 2;
            //set size for button
            btnQ.Size = btnW.Size = btnE.Size = btnR.Size = btnT.Size = btnY.Size = btnU.Size = btnI.Size = btnO.Size = btnP.Size = btnA.Size = btnS.Size = btnD.Size = btnF.Size = btnG.Size = btnH.Size = btnJ.Size = btnK.Size = btnL.Size = btnZ.Size = btnX.Size = btnC.Size = btnV.Size = btnB.Size = btnN.Size = btnM.Size = btnMoveLeftText.Size = btnMoveRigthText.Size = new Size(iWidth - 5, iHeight - 5);
            btnTranslateText.Size = new Size(iWidth * 2 - 5, iHeight - 5);
            btnShift.Size = btnEscText.Size = btnDeleteText.Size = new Size(iWidth * 3 - 5, iHeight - 5);
            btnPaymentText.Size = new Size((iWidth * 3 - 5), (iHeight * 2 - 5));        
            btnOkText.Size = new Size(iWidth * 2 - 5, iHeight * 2 - 5);
            //set location for button
            //dòng 1
            btnQ.Location = new Point(iAlignWidth, iAlignHeight);
            btnW.Location = new Point(btnQ.Location.X + iWidth, btnQ.Location.Y);
            btnE.Location = new Point(btnW.Location.X + iWidth, btnQ.Location.Y);
            btnR.Location = new Point(btnE.Location.X + iWidth, btnQ.Location.Y);
            btnT.Location = new Point(btnR.Location.X + iWidth, btnQ.Location.Y);
            btnY.Location = new Point(btnT.Location.X + iWidth, btnQ.Location.Y);
            btnU.Location = new Point(btnY.Location.X + iWidth, btnQ.Location.Y);
            btnI.Location = new Point(btnU.Location.X + iWidth, btnQ.Location.Y);
            btnO.Location = new Point(btnI.Location.X + iWidth, btnQ.Location.Y);
            btnP.Location = new Point(btnO.Location.X + iWidth, btnQ.Location.Y);
            //dòng 2
            btnA.Location = new Point(iAlignWidth + iWidth / 2, btnQ.Location.Y + iHeight);
            btnS.Location = new Point(btnA.Location.X + iWidth, btnA.Location.Y);
            btnD.Location = new Point(btnS.Location.X + iWidth, btnA.Location.Y);
            btnF.Location = new Point(btnD.Location.X + iWidth, btnA.Location.Y);
            btnG.Location = new Point(btnF.Location.X + iWidth, btnA.Location.Y);
            btnH.Location = new Point(btnG.Location.X + iWidth, btnA.Location.Y);
            btnJ.Location = new Point(btnH.Location.X + iWidth, btnA.Location.Y);
            btnK.Location = new Point(btnJ.Location.X + iWidth, btnA.Location.Y);
            btnL.Location = new Point(btnK.Location.X + iWidth, btnA.Location.Y);
            //dòng 3
            btnZ.Location = new Point(iAlignWidth, btnA.Location.Y + iHeight);
            btnX.Location = new Point(btnZ.Location.X + iWidth, btnZ.Location.Y);
            btnC.Location = new Point(btnX.Location.X + iWidth, btnZ.Location.Y);
            btnV.Location = new Point(btnC.Location.X + iWidth, btnZ.Location.Y);
            btnB.Location = new Point(btnV.Location.X + iWidth, btnZ.Location.Y);
            btnN.Location = new Point(btnB.Location.X + iWidth, btnZ.Location.Y);
            btnM.Location = new Point(btnN.Location.X + iWidth, btnZ.Location.Y);
            btnShift.Location = new Point(btnM.Location.X + iWidth, btnZ.Location.Y);
            //dòng 4
            btnMoveLeftText.Location = new Point(iAlignWidth, btnZ.Location.Y + iHeight);
            btnMoveRigthText.Location = new Point(btnMoveLeftText.Location.X + iWidth, btnMoveLeftText.Location.Y);
            btnDeleteText.Location = new Point(btnMoveRigthText.Location.X + iWidth, btnMoveLeftText.Location.Y);
            btnOkText.Location = new Point(btnDeleteText.Location.X + iWidth * 3, btnMoveLeftText.Location.Y);
            btnPaymentText.Location = new Point((btnOkText.Location.X + iWidth * 2) * (60 / 100), (btnMoveLeftText.Location.Y) * (60 / 100));
            //dòng 5
            btnTranslateText.Location = new Point(iAlignWidth, btnMoveLeftText.Location.Y + iHeight);
            btnEscText.Location = new Point(btnTranslateText.Location.X + iWidth * 2, btnTranslateText.Location.Y);
        }

        private void txtInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtInput.Text.Length > 15)
            {
                XtraMessageBox.Show(_bkdnq15kt, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
            }
        }

        private void gvListForiegnCurrency_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT1 && e.RowHandle >= 0)
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void gvListForiegnCurrency_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            lbKindMoney.Text = gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString();
            lbcurrencyService.Text = lbcurrencyGetMoney.Text = gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString();
            lbExchangeRate.Text = gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString();
        }

        private void btnUpForeignCurrency_Click(object sender, EventArgs e)
        {
            if (gvListForiegnCurrency.FocusedRowHandle > 0)
            {
                gvListForiegnCurrency.FocusedRowHandle = gvListForiegnCurrency.FocusedRowHandle - 1;
            }
        }

        private void btnDownForeignCurrency_Click(object sender, EventArgs e)
        {
            if (gvListForiegnCurrency.FocusedRowHandle < gvListForiegnCurrency.RowCount - 1)
            {
                gvListForiegnCurrency.FocusedRowHandle = gvListForiegnCurrency.FocusedRowHandle + 1;
            }
        }

        private void gvListProduct_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT && e.RowHandle >= 0)
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnFirstGridview_Click(object sender, EventArgs e)
        {
            gvListProduct.Focus();
            gvListProduct.FocusedRowHandle = 0;
        }

        private void btnLastGridview_Click(object sender, EventArgs e)
        {
            gvListProduct.Focus();
            gvListProduct.FocusedRowHandle = gvListProduct.RowCount - 1;
        }

        private void btnUpGridview_Click(object sender, EventArgs e)
        {
            if (gvListProduct.FocusedRowHandle > 0)
            {
                gvListProduct.Focus();
                gvListProduct.FocusedRowHandle--;
            }
        }

        private void btnDownGridview_Click(object sender, EventArgs e)
        {
            if (gvListProduct.FocusedRowHandle < gvListProduct.RowCount - 1)
            {
                gvListProduct.Focus();
                gvListProduct.FocusedRowHandle++;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            string hour, minute, second;
            second = DateTime.Now.Second.ToString();
            hour = DateTime.Now.Hour.ToString();
            minute = DateTime.Now.Minute.ToString();
            if (second.Length < 2)
                lbgiay.Text = "0" + second;
            else
                lbgiay.Text = second;
            if (minute.Length < 2)
                lbGio.Text = hour + ":0" + minute;
            else
                lbGio.Text = hour + ":" + minute;
            timer1.Enabled = true;
        }

        private void btn_ThuNgan_Click(object sender, EventArgs e)
        {
            SetBackgroudOnClick(btn_ThuNgan);
            SetDefautlBackgroud(btn_DatThu);
            pnlPayment.BringToFront();
        }

        private void btn_DatThu_Click(object sender, EventArgs e)
        {
            SetBackgroudOnClick(btn_DatThu);
            SetDefautlBackgroud(btn_ThuNgan);
            pnDacThu.BringToFront();
        }

        private void SetBackgroudOnClick(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.YellowGreen;
            _btn.Appearance.BackColor2 = Color.YellowGreen;
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;
        }

        private void SetDefautlBackgroud(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.White;
            if (_btn.Appearance.Options.UseBackColor == true)
                _btn.Appearance.Options.UseBackColor = false;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == false)
                _btn.LookAndFeel.UseDefaultLookAndFeel = true;
        }

        #region Text Click
        private void btnA_Click(object sender, EventArgs e)
        {
            InputKeyboardText("A");
        }

        private void btnQ_Click(object sender, EventArgs e)
        {
            InputKeyboardText("Q");
        }

        private void btnW_Click(object sender, EventArgs e)
        {
            InputKeyboardText("W");
        }

        private void btnE_Click(object sender, EventArgs e)
        {
            InputKeyboardText("E");
        }

        private void btnR_Click(object sender, EventArgs e)
        {
            InputKeyboardText("R");
        }

        private void btnT_Click(object sender, EventArgs e)
        {
            InputKeyboardText("T");
        }

        private void btnY_Click(object sender, EventArgs e)
        {
            InputKeyboardText("Y");
        }

        private void btnU_Click(object sender, EventArgs e)
        {
            InputKeyboardText("U");
        }

        private void btnI_Click(object sender, EventArgs e)
        {
            InputKeyboardText("I");
        }

        private void btnO_Click(object sender, EventArgs e)
        {
            InputKeyboardText("O");
        }

        private void btnP_Click(object sender, EventArgs e)
        {
            InputKeyboardText("P");
        }

        private void btnS_Click(object sender, EventArgs e)
        {
            InputKeyboardText("S");
        }

        private void btnD_Click(object sender, EventArgs e)
        {
            InputKeyboardText("D");
        }

        private void btnF_Click(object sender, EventArgs e)
        {
            InputKeyboardText("F");
        }

        private void btnG_Click(object sender, EventArgs e)
        {
            InputKeyboardText("G");
        }

        private void btnH_Click(object sender, EventArgs e)
        {
            InputKeyboardText("H");
        }

        private void btnJ_Click(object sender, EventArgs e)
        {
            InputKeyboardText("J");
        }

        private void btnK_Click(object sender, EventArgs e)
        {
            InputKeyboardText("K");
        }

        private void btnL_Click(object sender, EventArgs e)
        {
            InputKeyboardText("L");
        }

        private void btnM_Click(object sender, EventArgs e)
        {
            InputKeyboardText("M");
        }

        private void btnX_Click(object sender, EventArgs e)
        {
            InputKeyboardText("X");
        }

        private void btnC_Click(object sender, EventArgs e)
        {
            InputKeyboardText("C");
        }

        private void btnV_Click(object sender, EventArgs e)
        {
            InputKeyboardText("V");
        }

        private void btnB_Click(object sender, EventArgs e)
        {
            InputKeyboardText("B");
        }

        private void btnN_Click(object sender, EventArgs e)
        {
            InputKeyboardText("N");
        }

        private void btnZ_Click(object sender, EventArgs e)
        {
            InputKeyboardText("Z");
        }

        private void btnShift_Click(object sender, EventArgs e)
        {
            bShift = !bShift;
            if (bShift)
            {
                btnShift.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            }
            else
            {
                btnShift.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            }
        }

        private void btnTranslateText_Click(object sender, EventArgs e)
        {
            pnlKeyboardMoney.BringToFront();
            if (txtInput.SelectedText == txtInput.Text)
            {
                txtInput.Focus();
            }
            else
            {
                int iPosition = txtInput.SelectionStart;
                txtInput.Focus();
                txtInput.SelectionStart = txtInput.Text.Length;
                txtInput.SelectionStart = iPosition;
            }
        }

        private void btnMoveRigthText_Click(object sender, EventArgs e)
        {
            MoveToRight();
        }

        private void btnMoveLeftText_Click(object sender, EventArgs e)
        {
            MoveToLeft();
        }

        private void btnDeleteText_Click(object sender, EventArgs e)
        {
            DeleteTextBox();
        }

        private void btnEscText_Click(object sender, EventArgs e)
        {
            EscapeTextbox();
        }

        private void btnOkText_Click(object sender, EventArgs e)
        {
            OkTextBox();
        }

        private void btnPaymentText_Click(object sender, EventArgs e)
        {
            bPrintBill = true;
            PrintBill();
        }

        private void InputKeyboardText(string sInput)
        {
            if (bText)
            {
                if (txtInput.Text.Length > 15)
                {
                    int iPositon = txtInput.SelectionStart;
                    XtraMessageBox.Show(_bkdnq15kt, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtInput.SelectionStart = txtInput.Text.Length;
                    txtInput.SelectionStart = iPositon;
                    return;
                }
                if (bShift)
                    sInput = sInput.ToUpper();
                else
                    sInput = sInput.ToLower();
                int iPosition = txtInput.SelectionStart;
                txtInput.Text = txtInput.Text.Insert(iPosition, sInput);
                txtInput.SelectionStart = iPosition + 1;
            }
        }

        private void MoveToLeft()
        {
            int iPosition = txtInput.SelectionStart;
            if (iPosition > 1)
            {
                if (txtInput.Text[iPosition - 1].ToString() == ",")
                {
                    txtInput.SelectionStart = iPosition - 2;
                }
                else
                {
                    txtInput.SelectionStart = iPosition - 1;
                }
            }
            else
            {
                txtInput.SelectionStart = 0;

            }
        }

        private void MoveToRight()
        {
            int iPosition = txtInput.SelectionStart;
            if (iPosition < txtInput.Text.Length - 1)
            {
                if (txtInput.Text[iPosition + 1].ToString() == ",")
                {
                    txtInput.SelectionStart = iPosition + 2;
                }
                else
                {
                    txtInput.SelectionStart = iPosition + 1;
                }
            }
            else
            {
                txtInput.SelectionStart = txtInput.Text.Length;

            }
        }

        private void DeleteTextBox()
        {
            int iPosition = txtInput.SelectionStart;
            int iLenght = txtInput.Text.Length;
            if (iPosition > 1)
            {
                if (txtInput.Text[iPosition - 1].ToString() == ",")
                {
                    txtInput.Text = txtInput.Text.Remove(iPosition - 2, 1);

                    txtInput.SelectionStart = iPosition - 2;

                }
                else
                {
                    txtInput.Text = txtInput.Text.Remove(iPosition - 1, 1);
                    if (iLenght - txtInput.Text.Length == 1)
                    {
                        txtInput.SelectionStart = iPosition - 1;
                    }
                    else
                    {
                        txtInput.SelectionStart = iPosition - 2;
                    }
                }
            }
            else if (iPosition == 1)
            {
                if (iLenght >= 2)
                {
                    txtInput.Text = txtInput.Text.Remove(iPosition - 1, 1);
                    txtInput.SelectionStart = iPosition - 1;
                    if (txtInput.Text == "0")
                        txtInput.SelectionStart = 1;
                }
                else
                {
                    txtInput.Text = "0";
                    txtInput.SelectionStart = 1;
                }
            }
        }

        private void OkTextBox()
        {
            if (txtInput.Name == "txtInputMoney")
            {
                if (int.Parse(txtInput.Text.Replace(",", "")) == 0)
                {
                    if (float.Parse(lbTotalMoneyAll.Text) <= 0)
                    {
                        decimal MoneyInputAfterChange = decimal.Parse(txtInput.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                        lbFirst.Text = _tienmat + txtInput.Text + " " + gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString() + "  -->";
                        lbFirstMoney.Tag = MoneyInputAfterChange;
                        lbFirstMoney.Text = string.Format("{0:#,###0}", MoneyInputAfterChange) + "  VND";
                        txtInput.Text = "0";
                        txtInput.SelectionStart = 1;
                        pnlFirst.Visible = true;
                    }
                    else
                        return;
                }
                else if (!pnlFirst.Visible)
                {
                    decimal MoneyInputAfterChange = decimal.Parse(txtInput.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                    lbFirst.Text = _tienmat + txtInput.Text + " " + gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString() + "  -->";
                    lbFirstMoney.Tag = MoneyInputAfterChange;
                    lbFirstMoney.Text = string.Format("{0:#,###0}", MoneyInputAfterChange) + "  VND";
                    txtInput.Text = "0";
                    txtInput.SelectionStart = 1;
                    pnlFirst.Visible = true;
                }
                else if (!pnlSecond.Visible)
                {
                    decimal MoneyInputAfterChange = decimal.Parse(txtInput.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                    lbSecond.Text = _tienmat + txtInput.Text + " " + gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString() + "  -->";
                    lbSecondMoney.Tag = MoneyInputAfterChange;
                    lbSecondMoney.Text = string.Format("{0:#,###0}", MoneyInputAfterChange) + "  VND";
                    txtInput.Text = "0";
                    txtInput.SelectionStart = 1;
                    pnlSecond.Visible = true;
                }
                else if (!pnlThird.Visible)
                {
                    decimal MoneyInputAfterChange = decimal.Parse(txtInput.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                    lbThird.Text = _tienmat + txtInput.Text + " " + gvListForiegnCurrency.GetFocusedRowCellValue(MA).ToString() + "  -->";
                    lbThirdMoney.Tag = MoneyInputAfterChange;
                    lbThirdMoney.Text = string.Format("{0:#,###0}", MoneyInputAfterChange) + "  VND";
                    txtInput.Text = "0";
                    txtInput.SelectionStart = 1;
                    pnlThird.Visible = true;
                }
                else
                {
                    XtraMessageBox.Show(_ttkdnq3l, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ResetInputMoney();
                }
            }
            else if (txtInput.Name == "txtInputDiscount") 
            {
                if (int.Parse(txtInput.Text.Replace(",", "")) == 0)
                    return;
                if (decimal.Parse(txtInputDiscount.Text.Replace(",", "")) <= 100)
                {
                    lbDiscountPayment.Tag = (decimal.Parse(lbTotalMoneyPayment.Tag.ToString()) * decimal.Parse(txtInputDiscount.Text.Replace(",", ""))) / 100;
                    lbDiscountPayment.Text = txtInputDiscount.Text;
                    lbDiscount.Text = "        %";
                    per_Discount = decimal.Parse(txtInputDiscount.Text.Replace(",", ""));
                }
                else
                {
                    if (decimal.Parse(txtInputDiscount.Text.Replace(",", "")) > decimal.Parse(lbTotalMoneyPayment.Tag.ToString()))
                    {
                        XtraMessageBox.Show(_lhstkt, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtInput.Text = "0";
                        return;
                    }
                    lbDiscountPayment.Tag = decimal.Parse(txtInputDiscount.Text.Replace(",", ""));
                    lbDiscountPayment.Text = string.Format("{0:#,###0}", decimal.Parse(lbDiscountPayment.Tag.ToString()));
                    lbDiscount.Text = "   VND";
                    per_Discount = decimal.Parse(txtInputDiscount.Text.Replace(",", "")) * 100 / decimal.Parse(lbTotalMoneyPayment.Tag.ToString());
                }
                txtInput.Text = "0";
                InputGetMoneyFocus();
                txtInputMoney.Text = lbTotalMoneyAll.Text;
            }
            else if (txtInput.Name == "txtServiceMoney")
            {
                if (int.Parse(txtInput.Text.Replace(",", "")) == 0)
                    return;
                if (decimal.Parse(txtServiceMoney.Text.Replace(",", "")) <= 100)
                {
                    lbServiceMoney.Tag = (decimal.Parse(lbTotalMoneyPayment.Tag.ToString()) * (decimal.Parse(txtServiceMoney.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString()))) / 100;
                    lbServiceMoney.Text = txtServiceMoney.Text;
                    lbService.Text = "        %";
                    per_Extra = decimal.Parse(lbServiceMoney.Text.Replace(",", ""));
                }
                else
                {
                    lbServiceMoney.Tag = decimal.Parse(txtServiceMoney.Text.Replace(",", "")) * decimal.Parse(gvListForiegnCurrency.GetFocusedRowCellValue(TYGIA).ToString());
                    lbServiceMoney.Text = string.Format("{0:#,###0}", decimal.Parse(lbServiceMoney.Tag.ToString()));
                    lbService.Text = "   VND";
                    per_Extra = decimal.Parse(lbServiceMoney.Text.Replace(",", "")) * 100 / decimal.Parse(lbTotalMoneyPayment.Tag.ToString());
                }
                
                txtInput.Text = "0";
                InputGetMoneyFocus();
            }
            else if (txtInput.Name == "txtDearCustomer")
            {
                if (txtInput.Text == "")
                {
                    lbIDDearCustomerShow.Text = lbMark.Text = lbNameDearCustomer.Text = "";
                    pnlDearCustomerShow.Visible = false;
                    return;
                }
                if (cls_KHTT.CheckKhachHangThanThiet(txtInput.Text))
                {
                    lbIDDearCustomerShow.Text = txtInput.Text;
                    DataTable dtCustomer = cls_KHTT.ReturnKhachHangThanThiet(lbIDDearCustomerShow.Text);
                    
                    string[] categories = dtCustomer.Rows[0]["categories"].ToString().Split(',');
                    for (int i = 0; i < gvListProduct.RowCount; i++)
                    {
                        bool sTim = false;
                        string sql = "select MA_NHOMHANG from HANGHOA where MA_HANGHOA=" + clsMain.SQLString(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["MA_HANGHOA"]).ToString());
                        DataTable dt = clsMain.ReturnDataTable(sql );
                        foreach (string scategories in categories)
                        {
                            if (dt.Rows [0][0].ToString ()==scategories )
                            {
                                sTim = true;
                                break;
                            }
                        }
                        if (sTim == false)
                        {
                            dtCustomer.Rows[0]["CHIETKHAU"] = "0";
                            break;
                        }
                    }

                    lbNameDearCustomer.Text = dtCustomer.Rows[0]["TEN"].ToString();
                    lbMark.Text = dtCustomer.Rows[0]["SoDiem"].ToString();
                    pnlDearCustomerShow.Visible = true;
                    InputGetMoneyFocus();


                    lbDiscountPayment.Tag = (decimal.Parse(lbTotalMoneyPayment.Tag.ToString()) * decimal.Parse(dtCustomer.Rows[0]["CHIETKHAU"].ToString().Replace(",", ""))) / 100;
                    string[] temp = dtCustomer.Rows[0]["CHIETKHAU"].ToString().Split('.');
                    lbDiscountPayment.Text = temp[0];
                    lbDiscount.Text = "        %";
                    per_Discount = decimal.Parse(dtCustomer.Rows[0]["CHIETKHAU"].ToString().Replace(",", ""));
                    txtInputMoney.Text = lbTotalMoneyAll.Text;
                }
                else
                {
                    XtraMessageBox.Show(_makhttkd, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtInput.Focus();
                }
            }
            else if (txtInput.Name == "txtInnerCustomer")
            {
                if (txtInput.Text == "")
                {
                    return;
                }
                if (cls_KHTT.CheckThanhToanNoiBo(txtInput.Text))
                {

                    dMoneyInnerCard = cls_KHTT.ReturnMoneyMustPay(txtInput.Text, decimal.Parse(lbTotalMoneyAll.Tag.ToString()) - decimal.Parse(lbGetCustomer.Text.Replace(",", "")));
                    if (dMoneyInnerCard == 0)
                    {
                        return;
                    }
                    if (lbIdInnerCustomerShow.Text != "")
                        return;
                    lbIdInnerCustomerShow.Text = txtInput.Text;
                    DataTable dtCustomer = cls_KHTT.ReturnKhachHangThanThiet(lbIdInnerCustomerShow.Text);
                    lbNameInnerCustomer.Text = dtCustomer.Rows[0]["TEN"].ToString();
                    lbMoneySurplus.Text = string.Format("{0:#,###0}", decimal.Parse(dtCustomer.Rows[0]["SoDuTaiKhoan"].ToString())) + " VND";
                    pnlInnerCustomerShow.Visible = true;
                    if (!pnlFirst.Visible)
                    {

                        lbFirst.Text = _tienthettnb + string.Format("{0:#,###0}", dMoneyInnerCard) + " VND -->";
                        lbFirstMoney.Tag = dMoneyInnerCard;
                        lbFirstMoney.Text = string.Format("{0:#,###0}", dMoneyInnerCard) + "  VND";
                        pnlFirst.Visible = true;
                    }
                    else if (!pnlSecond.Visible)
                    {
                        lbSecond.Text = _tienthettnb + string.Format("{0:#,###0}", dMoneyInnerCard) + " VND -->";
                        lbSecondMoney.Tag = dMoneyInnerCard;
                        lbSecondMoney.Text = string.Format("{0:#,###0}", dMoneyInnerCard) + "  VND";
                        pnlSecond.Visible = true;
                    }
                    else if (!pnlThird.Visible)
                    {
                        lbThird.Text = _tienthettnb + string.Format("{0:#,###0}", dMoneyInnerCard) + " VND -->";
                        lbThirdMoney.Tag = dMoneyInnerCard;
                        lbThirdMoney.Text = string.Format("{0:#,###0}", dMoneyInnerCard) + "  VND";
                        pnlThird.Visible = true;
                    }
                    else
                    {
                        XtraMessageBox.Show(_ttkdnq3l, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ResetInputMoney();
                    }
                    InputGetMoneyFocus();
                }
                else
                {
                    XtraMessageBox.Show(_matttnbkd, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtInput.Focus();
                }
            }
            else if (txtInput.Name == "txtInputVoucher")
            {
                if (txtInput.Text == "")
                {
                    return;
                }
                if (cls_ConfigCashier.sAutodaybill == "1")
                {
                    DataTable dt = new DataTable();
                    dt = cls_Huyvietnam.getVoucher(txtInput.Text);
                    if (dt.Rows.Count == 0)
                    {
                        XtraMessageBox.Show(_mathevoucherktt, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if ( bool.Parse ( dt.Rows[0]["flaG_USED"].ToString ()) == true )
                    {
                        XtraMessageBox.Show(_mathevoucherdsd, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (DateTime.Parse(dt.Rows[0]["datE_START"].ToString()) > DateTime.Now || DateTime.Parse(dt.Rows[0]["datE_END"].ToString()) < DateTime.Now)
                    {
                        XtraMessageBox.Show(_mathevoucherdhhsd, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else
                    {
                        if (int.Parse(dt.Rows[0]["GIATRI_PHANTRAM"].ToString()) != 0)
                        {
                            bVoucher = false;
                            idVoucher = txtInput.Text;
                            giatriVoucher = dt.Rows[0]["GIATRI_PHANTRAM"].ToString();
                            dMoneyVoucher = decimal.Parse(dt.Rows[0]["GIATRI_PHANTRAM"].ToString().Replace(",", "")) * decimal.Parse(lbTotalMoneyAll.Tag.ToString()) / 100;
                            if (!pnlFirst.Visible)
                            {
                                lbFirst.Text = _vouchergiam + dt.Rows[0]["GIATRI_PHANTRAM"].ToString() + " %" + "  -->";
                                lbFirstMoney.Tag = dMoneyVoucher;
                                lbFirstMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                txtInput.Text = "0";
                                txtInput.SelectionStart = 1;
                                pnlFirst.Visible = true;
                            }
                            else if (!pnlSecond.Visible)
                            {
                                lbSecond.Text = _vouchergiam + dt.Rows[0]["GIATRI_PHANTRAM"].ToString() + " %" + "  -->";
                                lbSecondMoney.Tag = dMoneyVoucher;
                                lbSecondMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                txtInput.Text = "0";
                                txtInput.SelectionStart = 1;
                                pnlSecond.Visible = true;
                            }
                            else if (!pnlThird.Visible)
                            {
                                lbThird.Text = _vouchergiam + dt.Rows[0]["GIATRI_PHANTRAM"].ToString() + " %" + "  -->";
                                lbThirdMoney.Tag = dMoneyVoucher;
                                lbThirdMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                txtInput.Text = "0";
                                txtInput.SelectionStart = 1;
                                pnlThird.Visible = true;
                            }
                            else
                            {
                                XtraMessageBox.Show(_ttkdnq3l, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ResetInputMoney();
                            }

                            InputGetMoneyFocus();
                            txtInputMoney.Text = (-decimal.Parse(lbReplyCustomer.Text.Replace(",", ""))).ToString();
                            txtInputMoney.SelectAll();
                        }
                        else
                        {
                            bVoucher = false;
                            idVoucher = txtInput.Text;
                            giatriVoucher = dt.Rows[0]["GIATRI_TIEN"].ToString();
                            dMoneyVoucher = decimal.Parse(dt.Rows[0]["GIATRI_TIEN"].ToString().Replace(",", ""));
                            if (dMoneyVoucher > decimal.Parse(lbTotalMoneyAll.Tag.ToString()))
                                dMoneyVoucher = decimal.Parse(lbTotalMoneyAll.Tag.ToString());
                            if (!pnlFirst.Visible)
                            {
                                lbFirst.Text = "Voucher: Giảm " + string.Format("{0:#,###0}", decimal.Parse(dt.Rows[0]["GIATRI_TIEN"].ToString())) + " VND" + "  -->";
                                lbFirstMoney.Tag = dMoneyVoucher;
                                lbFirstMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                txtInput.Text = "0";
                                txtInput.SelectionStart = 1;
                                pnlFirst.Visible = true;
                            }
                            else if (!pnlSecond.Visible)
                            {
                                lbSecond.Text = _vouchergiam + string.Format("{0:#,###0}", decimal.Parse(dt.Rows[0]["GIATRI_TIEN"].ToString())) + " VND" + "  -->";
                                lbSecondMoney.Tag = dMoneyVoucher;
                                lbSecondMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                txtInput.Text = "0";
                                txtInput.SelectionStart = 1;
                                pnlSecond.Visible = true;
                            }
                            else if (!pnlThird.Visible)
                            {
                                lbThirdMoney.Text = _vouchergiam + string.Format("{0:#,###0}", decimal.Parse(dt.Rows[0]["GIATRI_TIEN"].ToString())) + " VND" + "  -->";
                                lbThirdMoney.Tag = dMoneyVoucher;
                                lbThirdMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                txtInput.Text = "0";
                                txtInput.SelectionStart = 1;
                                pnlThird.Visible = true;
                            }
                            else
                            {
                                XtraMessageBox.Show(_ttkdnq3l, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ResetInputMoney();
                            }

                            InputGetMoneyFocus();
                            txtInputMoney.Text = (-decimal.Parse(lbReplyCustomer.Text.Replace(",", ""))).ToString();
                            txtInputMoney.SelectAll();
                        }
                        txtInputMoney.Text = string.Format("{0:#,###0}", (decimal.Parse(lbTotalMoneyAll.Text) - dMoneyVoucher));
                    }
                }
                else 
                {
                    if (cls_KHTT.CheckTheVoucher(txtInput.Text) == 1)
                    {
                        XtraMessageBox.Show(_mathevoucherktt, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (cls_KHTT.CheckTheVoucher(txtInput.Text) == 2)
                    {
                        XtraMessageBox.Show(_mathevoucherdsd, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (cls_KHTT.CheckTheVoucher(txtInput.Text) == 3)
                    {
                        XtraMessageBox.Show(_mathevouchercactive, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (cls_KHTT.CheckTheVoucher(txtInput.Text) == 4)
                    {
                        XtraMessageBox.Show(_mathevoucherdhhsd, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (cls_KHTT.CheckTheVoucher(txtInput.Text) == 0)
                    {
                        string sSQL = "";
                        sSQL += "Select GIATRI_PHANTRAM,GIATRI_TIEN From VOUCHER_DETAIL Where MA =" + clsMain.SQLString(txtInput.Text);
                        DataTable dt = clsMain.ReturnDataTable(sSQL);
                        if (int.Parse(dt.Rows[0]["GIATRI_PHANTRAM"].ToString()) != 0)
                        {
                            bVoucher = false;
                            idVoucher = txtInput.Text;
                            giatriVoucher = dt.Rows[0]["GIATRI_PHANTRAM"].ToString();
                            dMoneyVoucher = decimal.Parse(dt.Rows[0]["GIATRI_PHANTRAM"].ToString().Replace(",", "")) * decimal.Parse(lbTotalMoneyAll.Tag.ToString()) / 100;
                            if (!pnlFirst.Visible)
                            {
                                lbFirst.Text = _vouchergiam + dt.Rows[0]["GIATRI_PHANTRAM"].ToString() + " %" + "  -->";
                                lbFirstMoney.Tag = dMoneyVoucher;
                                lbFirstMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                txtInput.Text = "0";
                                txtInput.SelectionStart = 1;
                                pnlFirst.Visible = true;
                            }
                            else if (!pnlSecond.Visible)
                            {
                                lbSecond.Text = _vouchergiam + dt.Rows[0]["GIATRI_PHANTRAM"].ToString() + " %" + "  -->";
                                lbSecondMoney.Tag = dMoneyVoucher;
                                lbSecondMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                txtInput.Text = "0";
                                txtInput.SelectionStart = 1;
                                pnlSecond.Visible = true;
                            }
                            else if (!pnlThird.Visible)
                            {
                                lbThird.Text = _vouchergiam + dt.Rows[0]["GIATRI_PHANTRAM"].ToString() + " %" + "  -->";
                                lbThirdMoney.Tag = dMoneyVoucher;
                                lbThirdMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                txtInput.Text = "0";
                                txtInput.SelectionStart = 1;
                                pnlThird.Visible = true;
                            }
                            else
                            {
                                XtraMessageBox.Show(_ttkdnq3l, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ResetInputMoney();
                            }

                            InputGetMoneyFocus();
                            txtInputMoney.Text = (-decimal.Parse(lbReplyCustomer.Text.Replace(",", ""))).ToString();
                            txtInputMoney.SelectAll();
                        }
                        else
                        {
                            bVoucher = false;
                            idVoucher = txtInput.Text;
                            giatriVoucher = dt.Rows[0]["GIATRI_TIEN"].ToString();
                            dMoneyVoucher = decimal.Parse(dt.Rows[0]["GIATRI_TIEN"].ToString().Replace(",", ""));
                            if (dMoneyVoucher > decimal.Parse(lbTotalMoneyAll.Tag.ToString()))
                                dMoneyVoucher = decimal.Parse(lbTotalMoneyAll.Tag.ToString());
                            if (!pnlFirst.Visible)
                            {
                                lbFirst.Text = "Voucher: Giảm " + string.Format("{0:#,###0}", decimal.Parse(dt.Rows[0]["GIATRI_TIEN"].ToString())) + " VND" + "  -->";
                                lbFirstMoney.Tag = dMoneyVoucher;
                                lbFirstMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                txtInput.Text = "0";
                                txtInput.SelectionStart = 1;
                                pnlFirst.Visible = true;
                            }
                            else if (!pnlSecond.Visible)
                            {
                                lbSecond.Text = _vouchergiam + string.Format("{0:#,###0}", decimal.Parse(dt.Rows[0]["GIATRI_TIEN"].ToString())) + " VND" + "  -->";
                                lbSecondMoney.Tag = dMoneyVoucher;
                                lbSecondMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                txtInput.Text = "0";
                                txtInput.SelectionStart = 1;
                                pnlSecond.Visible = true;
                            }
                            else if (!pnlThird.Visible)
                            {
                                lbThirdMoney.Text = _vouchergiam + string.Format("{0:#,###0}", decimal.Parse(dt.Rows[0]["GIATRI_TIEN"].ToString())) + " VND" + "  -->";
                                lbThirdMoney.Tag = dMoneyVoucher;
                                lbThirdMoney.Text = string.Format("{0:#,###0}", dMoneyVoucher) + "  VND";
                                txtInput.Text = "0";
                                txtInput.SelectionStart = 1;
                                pnlThird.Visible = true;
                            }
                            else
                            {
                                XtraMessageBox.Show(_ttkdnq3l, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ResetInputMoney();
                            }

                            InputGetMoneyFocus();
                            txtInputMoney.Text = (-decimal.Parse(lbReplyCustomer.Text.Replace(",", ""))).ToString();
                            txtInputMoney.SelectAll();
                        }
                        txtInputMoney.Text = string.Format("{0:#,###0}", (decimal.Parse(lbTotalMoneyAll.Text) - dMoneyVoucher));
                    }
                }
            }
            else if (txtInput.Name == "txtInputMagiamgia")
            {
                if (txtInput.Text == "")
                {
                    return;
                }
                if (cls_ConfigCashier.sAutodaybill == "1")
                {
                    DataTable dt = new DataTable();
                    dt = cls_Huyvietnam.getMaGiamGia(txtInput.Text);
                    if (dt.Rows.Count == 0)
                    {
                        XtraMessageBox.Show("Mã giảm giá không tồn tại", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (bool.Parse(dt.Rows[0]["flaG_USED"].ToString()) == true)
                    {
                        XtraMessageBox.Show("Mã giảm giá đã sữ dụng", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (DateTime.Parse(dt.Rows[0]["datE_START"].ToString()) > DateTime.Now || DateTime.Parse(dt.Rows[0]["datE_END"].ToString()) < DateTime.Now)
                    {
                        XtraMessageBox.Show("Mã giảm giá đã hết hạn sữ dụng", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else
                    {
                        if (decimal.Parse(dt.Rows[0]["GIATRI_PHANTRAM"].ToString().Replace(",", "")) != 0)
                        {
                            if (idVoucher != txtInput.Text)
                            {
                                idVoucher = txtInput.Text;
                                giatriVoucher = dt.Rows[0]["GIATRI_PHANTRAM"].ToString();
                                txtInputDiscount.Text = dt.Rows[0]["GIATRI_PHANTRAM"].ToString().Replace(",", "");
                            }
                            else
                            {
                                //if (bool.Parse(dt.Rows[0]["flaG_ONCE"].ToString())==false )
                                //{
                                //    idVoucher = txtInput.Text;
                                //    giatriVoucher =(decimal .Parse (giatriVoucher)+ decimal.Parse(dt.Rows[0]["GIATRI_PHANTRAM"].ToString().Replace(",", ""))).ToString ();
                                //    txtInputDiscount.Text = giatriVoucher.ToString().Replace(",", "");
                                //}
                            }
                        }
                        else
                        {
                            if (idVoucher != txtInput.Text)
                            {
                                idVoucher = txtInput.Text;
                                giatriVoucher = dt.Rows[0]["GIATRI_TIEN"].ToString();
                                txtInputDiscount.Text = dt.Rows[0]["GIATRI_TIEN"].ToString().Replace(",", "");
                            }
                            else
                            {
                                if (bool.Parse(dt.Rows[0]["flaG_ONCE"].ToString()) == false)
                                {
                                    idVoucher = txtInput.Text;
                                    giatriVoucher = (decimal.Parse(giatriVoucher) + decimal.Parse(dt.Rows[0]["GIATRI_TIEN"].ToString().Replace(",", ""))).ToString();
                                    txtInputDiscount.Text = giatriVoucher.ToString().Replace(",", "");
                                }
                            }
                        }

                        if (decimal.Parse(txtInputDiscount.Text.Replace(",", "")) <= 100)
                        {
                            lbDiscountPayment.Tag = (decimal.Parse(lbTotalMoneyPayment.Tag.ToString()) * decimal.Parse(txtInputDiscount.Text.Replace(",", ""))) / 100;
                            lbDiscountPayment.Text = txtInputDiscount.Text;
                            lbDiscount.Text = "        %";
                            per_Discount = decimal.Parse(txtInputDiscount.Text.Replace(",", ""));
                        }
                        else
                        {
                            if (decimal.Parse(txtInputDiscount.Text.Replace(",", "")) > decimal.Parse(lbTotalMoneyPayment.Tag.ToString()))
                            {
                                XtraMessageBox.Show(_lhstkt, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                txtInput.Text = "0";
                                return;
                            }
                            lbDiscountPayment.Tag = decimal.Parse(txtInputDiscount.Text.Replace(",", ""));
                            lbDiscountPayment.Text = string.Format("{0:#,###0}", decimal.Parse(lbDiscountPayment.Tag.ToString()));
                            lbDiscount.Text = "   VND";
                            per_Discount = decimal.Parse(txtInputDiscount.Text.Replace(",", "")) * 100 / decimal.Parse(lbTotalMoneyPayment.Tag.ToString());
                        }
                        txtInput.Text = "0";
                        InputGetMoneyFocus();
                        txtInputMoney.Text = lbTotalMoneyAll.Text;
                    }
                }
                else
                {
                   
                }
            }
            else if (txtInput.Name == "txtNhapPhieuMuaHang")
            {
                if (int.Parse(txtInput.Text.Replace(",", "")) == 0)
                    return;
                sGhiChuPhieuMuaHang = txtNhapPhieuMuaHang.Text.Replace(",", "");
                if (decimal.Parse(txtNhapPhieuMuaHang.Text.Replace(",", "")) <= 100)
                {
                    dTienPhieuMuaHang = (decimal.Parse(lbTotalMoneyPayment.Tag.ToString()) * decimal.Parse(txtNhapPhieuMuaHang.Text.Replace(",", ""))) / 100;
                    if (!pnlFirst.Visible)
                    {
                        lbFirst.Text = _phieuMuaHangGiam + txtNhapPhieuMuaHang.Text + " %" + "  -->";
                        lbFirstMoney.Tag = dTienPhieuMuaHang;
                        lbFirstMoney.Text = string.Format("{0:#,###0}", dTienPhieuMuaHang) + "  VND";
                        txtInput.Text = "0";
                        txtInput.SelectionStart = 1;
                        pnlFirst.Visible = true;
                    }
                    else if (!pnlSecond.Visible)
                    {
                        lbSecond.Text = _phieuMuaHangGiam + txtNhapPhieuMuaHang.Text + " %" + "  -->";
                        lbSecondMoney.Tag = dTienPhieuMuaHang;
                        lbSecondMoney.Text = string.Format("{0:#,###0}", dTienPhieuMuaHang) + "  VND";
                        txtInput.Text = "0";
                        txtInput.SelectionStart = 1;
                        pnlSecond.Visible = true;
                    }
                    else if (!pnlThird.Visible)
                    {
                        lbThird.Text = _phieuMuaHangGiam + txtNhapPhieuMuaHang.Text + " %" + "  -->";
                        lbThirdMoney.Tag = dTienPhieuMuaHang;
                        lbThirdMoney.Text = string.Format("{0:#,###0}", dTienPhieuMuaHang) + "  VND";
                        txtInput.Text = "0";
                        txtInput.SelectionStart = 1;
                        pnlThird.Visible = true;
                    }
                    else
                    {
                        XtraMessageBox.Show(_ttkdnq3l, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ResetInputMoney();
                    }

                    InputGetMoneyFocus();
                    txtInputMoney.Text = (-decimal.Parse(lbReplyCustomer.Text.Replace(",", ""))).ToString();
                    txtInputMoney.SelectAll();
                }
                else
                {

                    dTienPhieuMuaHang = decimal.Parse(txtNhapPhieuMuaHang.Text.Replace(",", ""));
                    if (dTienPhieuMuaHang > decimal.Parse(lbTotalMoneyAll.Tag.ToString()))
                        dTienPhieuMuaHang = decimal.Parse(lbTotalMoneyAll.Tag.ToString());
                    if (!pnlFirst.Visible)
                    {
                        lbFirst.Text = _phieuMuaHangGiam + string.Format("{0:#,###0}", txtNhapPhieuMuaHang.Text) + " VND" + "  -->";
                        lbFirstMoney.Tag = dTienPhieuMuaHang;
                        lbFirstMoney.Text = string.Format("{0:#,###0}", dTienPhieuMuaHang) + "  VND";
                        txtInput.Text = "0";
                        txtInput.SelectionStart = 1;
                        pnlFirst.Visible = true;
                    }
                    else if (!pnlSecond.Visible)
                    {
                        lbSecond.Text = _phieuMuaHangGiam + string.Format("{0:#,###0}", txtNhapPhieuMuaHang.Text) + " VND" + "  -->";
                        lbSecondMoney.Tag = dTienPhieuMuaHang;
                        lbSecondMoney.Text = string.Format("{0:#,###0}", dTienPhieuMuaHang) + "  VND";
                        txtInput.Text = "0";
                        txtInput.SelectionStart = 1;
                        pnlSecond.Visible = true;
                    }
                    else if (!pnlThird.Visible)
                    {
                        lbThirdMoney.Text = _phieuMuaHangGiam + string.Format("{0:#,###0}", txtNhapPhieuMuaHang.Text) + " VND" + "  -->";
                        lbThirdMoney.Tag = dTienPhieuMuaHang;
                        lbThirdMoney.Text = string.Format("{0:#,###0}", dTienPhieuMuaHang) + "  VND";
                        txtInput.Text = "0";
                        txtInput.SelectionStart = 1;
                        pnlThird.Visible = true;
                    }
                    else
                    {
                        XtraMessageBox.Show(_ttkdnq3l, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ResetInputMoney();
                    }

                    InputGetMoneyFocus();
                    txtInputMoney.Text = (-decimal.Parse(lbReplyCustomer.Text.Replace(",", ""))).ToString();
                    txtInputMoney.SelectAll();
                }

                //txtInput.Text = "0";
                //InputGetMoneyFocus();
                //txtInputMoney.Text = lbTotalMoneyAll.Text;
                txtInputMoney.Text = string.Format("{0:#,###0}", (decimal.Parse(lbTotalMoneyAll.Text) - dTienPhieuMuaHang));
            }
        }

        private void EscapeTextbox()
        {
            if (txtInput.Name == "txtInputMoney")
            {
                txtInputMoney.Text = "0";
                txtInputMoney.Focus();
                txtInputMoney.SelectionStart = 1;
            }
            else if (txtInput.Name == "txtInputDiscount")
            {
                txtInputDiscount.Text = "0";
                txtInputDiscount.Focus();
                txtInputDiscount.SelectionStart = 1;
            }
            else if (txtInput.Name == "txtInputVoucher")
            {
                txtInputVoucher.Text = "0";
                txtInputVoucher.Focus();
                txtInputVoucher.SelectionStart = 1;
            }
            else if (txtInput.Name == "txtInputMagiamgia")
            {
                txtInputMagiamgia.Text = "0";
                txtInputMagiamgia.Focus();
                txtInputMagiamgia.SelectionStart = 1;
            }
            else if (txtInput.Name == "txtDearCustomer")
            {
                txtDearCustomer.Text = "";
                txtDearCustomer.Focus();
                txtDearCustomer.SelectionStart = 0;
            }
            else if (txtInput.Name == "txtInnerCustomer")
            {
                txtInnerCustomer.Text = "";
                txtInnerCustomer.Focus();
                txtInnerCustomer.SelectionStart = 0;
            }
        }

        private void PrintBill()
        {
            if (pnlTienKhachTra.Visible)
            {
                if (!pnlFirst.Visible)
                {
                    OkTextBox();
                }
            }
            if (decimal.Parse(lbReplyCustomer.Tag.ToString()) < 0)
            {
                XtraMessageBox.Show(_chuatthettien, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dMoneyVoucher > decimal.Parse(lbTotalMoneyAll.Tag.ToString()))
            {
                dMoneyVoucher = decimal.Parse(lbTotalMoneyAll.Tag.ToString());
                dMoneyCash = decimal.Parse(lbTotalMoneyAll.Tag.ToString()) - dMoneyVoucher;
                dMoneyVisa = 0;
                dMoneyInnerCard = 0;
            }
            else if ((dMoneyVoucher + dMoneyVisa + dTienPhieuMuaHang) > decimal.Parse(lbTotalMoneyAll.Tag.ToString()))
            {
                dMoneyCash = decimal.Parse(lbTotalMoneyAll.Tag.ToString()) - (dMoneyVoucher + dMoneyVisa + dTienPhieuMuaHang);
                dMoneyInnerCard = 0;
            }
            else
            {
                dMoneyCash = decimal.Parse(lbTotalMoneyAll.Tag.ToString()) - (dMoneyVoucher + dMoneyVisa + dMoneyInnerCard + dTienPhieuMuaHang);
            }
            if (cls_ConfigCashier.sAutodaybill == "1")
            {
                //15/09/2018 huy viet nam
                //TTTNB
                if (lbIdInnerCustomerShow.Text != "" || dMoneyInnerCard > 0)
                {
                    string idBill = GetNewCodeBill();
                    string postCard = cls_Huyvietnam.postCard(lbIdInnerCustomerShow.Text, dMoneyInnerCard.ToString(), idBill, cls_ConfigCashier.sStore_ID);
                    if (postCard != "00")
                    {
                        XtraMessageBox.Show("Thanh toán thẻ nội bộ không thành công. Kiểm tra lại thẻ hoặc chọn phương thức thanh toán khác" + "\n" + postCard, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                //Voucher
                if (idVoucher != "")
                {
                    string idBill = GetNewCodeBill();
                    string postVoucher = cls_Huyvietnam.postVoucher(idVoucher, idBill, cls_ConfigCashier.sStore_ID);
                    if (postVoucher != "00")
                    {
                        XtraMessageBox.Show("Thanh toán Voucher không thành công. Kiểm tra lại hoặc chọn phương thức thanh toán khác" + "\n" + postVoucher, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            PrintAndSaveBill();
            bHoanTacThanhToan = true;
            this.Close();
        }

        private void PrintAndSaveBill()
        {
            string sSQL = "";
            decimal GiaBan_Thuc = 0;
            if (_sIdTable == "")
            {
                
                if (cls_ConfigCashier.sShow_Ghichu != "0" & sTenKhach == "")
                {
                    Frm_GhiChu_TenKhach frm = new Frm_GhiChu_TenKhach(sTenKhach);
                    frm.ShowDialog();
                    sTenKhach = frm.sGhiChu;
                    frm.Dispose();
                }

                if (cls_ConfigCashier.sIn_Number_Fastfood != "0" & sInTem == true)
                {
                    Frm_Chontheosoluong frm = new Frm_Chontheosoluong();
                    frm.Mode = 1;
                    frm.iAuto = 1;
                    frm.ShowDialog();
                    Number = frm.tmp_SoLuong;
                    frm.Dispose();
                }

                List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                string idBill = GetNewCodeBill();
                sSQL = "";
                sSQL += "Insert Into HOADON(MA_HOADON, MANHANVIEN, GIAMGIA, PHIPHUCVU, TIEN_TRAKHACH, TIEN_KHACHTRA,";
                sSQL += "TONGTIEN, NGAYTAO, GIOVAO, GIORA,NHANVIEN_RABILL,ISPAYMENT, MA_CUAHANG, MA_KHO, MA_QUAY,MA_KHACHHANG,";
                sSQL += "SOTHETT,NGAYTHETT,TIENTRATHETT,MONEYVISA,MONEYINNERCARD,MONEYVOUCHER,MONEYCASH,MONEYPHIEUMUAHANG,GHICHUPHIEUMUAHANG,GHICHU,GHICHU1)" + "\n";
                sSQL += "Values(";
                sSQL += clsMain.SQLString(idBill) + ",";
                sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                sSQL += clsMain.SQLString(lbDiscountPayment.Text.Replace(",", "")) + ",";
                sSQL += clsMain.SQLString(lbServiceMoney.Text.Replace(",", "")) + ",";
                sSQL += clsMain.SQLString(lbReplyCustomer.Text.Replace(",", "")) + ",";
                sSQL += clsMain.SQLString(lbGetCustomer.Text.Replace(",", "")) + ",";
                sSQL += clsMain.SQLString(lbTotalMoneyAll.Text.Replace(",", "")) + ",";
                sSQL += "GETDATE()" + ",";
                sSQL += "GETDATE()" + ",";
                sSQL += "GETDATE()" + ",";
                sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + "," + 1 + ",";
                sSQL += clsMain.SQLString(cls_ConfigCashier.idShop) + ",";
                sSQL += clsMain.SQLString(cls_ConfigCashier.idWarehouse) + ",";
                sSQL += clsMain.SQLString(cls_ConfigCashier.idCashier) + ",";
                if (CN_Flag)
                {
                    sSQL += clsMain.SQLString(CN_MaKH) + ",";
                }
                else
                {
                    if (lbIDDearCustomerShow.Text != "")
                        sSQL += "(SELECT MA FROM KHTT WHERE MATHE = " + clsMain.SQLString(lbIDDearCustomerShow.Text) + ")" + ",";
                    else if (lbIdInnerCustomerShow.Text != "")
                        sSQL += clsMain.SQLString(lbIdInnerCustomerShow.Text) + ",";
                    else
                        sSQL += clsMain.SQLString(GH_MaKH) + ",";
                }
                sSQL += clsMain.SQLString(soThe) + ",";
                sSQL += clsMain.SQLString(ngayThe) + ",";
                sSQL += clsMain.SQLString(dMoneyVisa.ToString()) + ",";
                sSQL += clsMain.SQLString(dMoneyVisa.ToString()) + ",";
                sSQL += clsMain.SQLString(dMoneyInnerCard.ToString()) + ",";
                sSQL += clsMain.SQLString(dMoneyVoucher.ToString()) + ",";
                sSQL += clsMain.SQLString(dMoneyCash.ToString()) + ",";
                sSQL += clsMain.SQLString(dTienPhieuMuaHang.ToString()) + ",";
                sSQL += clsMain.SQLString(sGhiChuPhieuMuaHang) + ",";
                sSQL += clsMain.SQLStringUnicode(sTenKhach ) + ",";
                sSQL += clsMain.SQLStringUnicode(GhiChu) + ")";
                clsMain.ExecuteSQL(sSQL);

                DataTable dt = new DataTable();
                decimal sl_combo = 0;
                bool flag_IsCombo = false;
                int dem = 0;
                //decimal iSLMonChinh = 1;
                for (int i = 0; i < gvListProduct.RowCount; i++)
                {
                    DM_HangHoa dmHangHoa = new DM_HangHoa();
                    //dmHangHoa.STT = gvListProduct.GetRowCellValue(i, gvListProduct.Columns["STT"]).ToString(); ;
                    dmHangHoa.MONTHEM = false;
                    dmHangHoa.TEM = 0;
                    dmHangHoa.Number = "0";
                    dmHangHoa.MACP = "C";
                    dmHangHoa.MACB = 0;
                    dmHangHoa.Ten_HoaDon = "PHIẾU THANH TOÁN";
                    if (GH_diachi != "")
                    {
                        dmHangHoa.DIACHIGIAOHANG = GH_diachi;
                        dmHangHoa.TENKH = GH_TenKH;
                        dmHangHoa.DIENTHOAI_KH = GH_Dienthoai;
                    }
                    if (sTenKhach != "")
                    {
                        dmHangHoa.TENKH = sTenKhach;
                    }  
                    dmHangHoa.MA_HANGHOA = gvListProduct.GetRowCellValue(i, gvListProduct.Columns["MA_HANGHOA"]).ToString();
                    dmHangHoa.TEN_HANGHOA = gvListProduct.GetRowCellValue(i, gvListProduct.Columns["TEN_HANGHOA"]).ToString();
                    dmHangHoa.SOLUONG = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["SOLUONG"]).ToString());
                    dmHangHoa.GIABAN = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["GIABAN"]).ToString());
                    dmHangHoa.THUE = double.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["THUE"]).ToString());
                    dmHangHoa.Thanh_Tien = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["Thanh_Tien"]).ToString());
                    dmHangHoa.IS_INBEP = bool.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["IS_INBEP"]).ToString());
                    dmHangHoa.CHIETKHAU = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["CHIETKHAU"]).ToString());
                    dmHangHoa.Tong_Cong_Tru_Voucher = decimal.Parse(lbTotalMoneyAll.Text) - dMoneyVoucher;
                    dmHangHoa.Tong_Cong = lbTotalMoneyAll.Text;
                    dmHangHoa.Tien_TraKhach = lbReplyCustomer.Text;
                    dmHangHoa.GhiChu = GhiChu;

                    if (lbGetCustomer.Text != "0")
                        dmHangHoa.Tien_KhachTra = lbGetCustomer.Text;
                    else
                    {
                        dmHangHoa.Tien_KhachTra = txtInputMoney.Text;
                        dmHangHoa.Tien_TraKhach = (double.Parse(txtInputMoney.Text.ToString()) - double.Parse(lbTotalMoneyAll.Text.ToString())).ToString();
                        lbReplyCustomer.Text = dmHangHoa.Tien_TraKhach;
                    }
                    if (double.Parse(lbServiceMoney.Text)> 100)
                        dmHangHoa.Phiphucvu = lbServiceMoney.Text;
                    else
                        dmHangHoa.Phiphucvu = lbServiceMoney.Text + "%";
                    dmHangHoa.Tiendatcoc = lbMoneyGetFirst.Text;
                    if (double.Parse(lbDiscountPayment.Text) > 100)
                        dmHangHoa.Giam_Gia = lbDiscountPayment.Text;
                    else
                        dmHangHoa.Giam_Gia = lbDiscountPayment.Text + "%";
                    dmHangHoa.VOUCHER = dMoneyVoucher;
                    dmHangHoa.Ma_HoaDon = idBill;
                    dmHangHoa.Ma_Quay = cls_ConfigCashier.nameCashier;
                    dmHangHoa.MaQuay = cls_ConfigCashier.nameCashier;
                    dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                    dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                    dmHangHoa.ngayHD = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                    dmHangHoa.IS_COMBO = bool.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["IS_COMBO"]).ToString());
                    
                    String sMaNhanVien = "0";
                    String sTenNhanVien = "";
                    try
                    {
                        sMaNhanVien = dtGridviewProduct.Select("MA_HANGHOA=" + clsMain.SQLString(dmHangHoa.MA_HANGHOA))[0]["sMaNhanVien"].ToString();
                        sTenNhanVien = dtGridviewProduct.Select("MA_HANGHOA=" + clsMain.SQLString(dmHangHoa.MA_HANGHOA))[0]["NhanVien"].ToString();
                    }
                    catch (Exception e)
                    {
                    }
                    dmHangHoa.TenNhanVien = sTenNhanVien;
                    if (cls_ConfigCashier.sIn_Number_Fastfood != "0" & sInTem == true)
                    {
                        dmHangHoa.MONTHEM = bool.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["MONTHEM"]).ToString());
                        dmHangHoa.TEM = int.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["TEM"]).ToString());
                        dmHangHoa.Number = Number.ToString();
                        dmHangHoa.MACP = gvListProduct.GetRowCellValue(i, gvListProduct.Columns["MACP"]).ToString();
                        dmHangHoa.MACB = int.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["MACB"]).ToString());
                    }

                    //if (dmHangHoa.MACP == "C")
                    //{
                    //    dmHangHoa.SOLUONG = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["SOLUONG"]).ToString());
                    //    iSLMonChinh = dmHangHoa.SOLUONG;
                    //}
                    //else
                    //{
                    //    dmHangHoa.SOLUONG = iSLMonChinh * decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["SOLUONG"]).ToString());
                    //}
                    dmHangHoa.GhiChu = GhiChu;
                    listDM_HangHoa.Add(dmHangHoa);
                    decimal sChietKhau = dmHangHoa.CHIETKHAU;
                    GiaBan_Thuc = dmHangHoa.GIABAN - dmHangHoa.GIABAN * per_Discount / 100 + dmHangHoa.GIABAN * per_Extra / 100;
                    GiaBan_Thuc -= GiaBan_Thuc * sChietKhau / 100;
                    sSQL = "";
                    sSQL += "Insert Into CT_HOADON( MA_HOADON, MA_HANGHOA, SOLUONG, GIABAN, THUE,IS_COMBO,GIABAN_THAT,CHIETKHAU,NHANVIEN,TEM,MACP,Number,MACB)" + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(idBill) + ",";
                    sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                    sSQL += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                    sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                    sSQL += clsMain.SQLString(dmHangHoa.THUE.ToString()) + ",";
                    sSQL += clsMain.SQLBit(dmHangHoa.IS_COMBO) + ",";
                    sSQL += clsMain.SQLString(GiaBan_Thuc.ToString() == "" ? dmHangHoa.GIABAN.ToString() : GiaBan_Thuc.ToString()) + ",";
                    sSQL += clsMain.SQLString(sChietKhau.ToString()) + ",";
                    sSQL += clsMain.SQLString(sMaNhanVien) + ",";
                    sSQL += clsMain.SQLString(dmHangHoa.TEM.ToString()) + ",";
                    sSQL += clsMain.SQLString(dmHangHoa.MACP) + ",";
                    sSQL += clsMain.SQLString(dmHangHoa.Number) + ",";
                    sSQL += clsMain.SQLString(dmHangHoa.MACB.ToString()) + ")";
                    clsMain.ExecuteSQL(sSQL);

                    if (!flag_IsCombo)
                    {
                        sSQL = "Select MA_HANGHOA From HANGHOA Where IS_COMBO=1 AND MA_HANGHOA=" + clsMain.SQLString(dmHangHoa.MA_HANGHOA);
                        dt = clsMain.ReturnDataTable(sSQL);
                        if (dt.Rows.Count > 0)
                        {
                            flag_IsCombo = true;
                            sl_combo = dmHangHoa.SOLUONG;
                            sSQL = "";
                            sSQL += "Select Count(*) From COMBO Where MA_COMBO=" + clsMain.SQLString(dmHangHoa.MA_HANGHOA);
                            DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                            if (dt1.Rows.Count > 0)
                            {
                                dem = int.Parse(dt1.Rows[0][0].ToString()) + 1;
                            }
                        }
                    }
                    if (flag_IsCombo)
                    {
                        if (dmHangHoa.IS_COMBO)
                        {
                            decimal sl_HangCombo = sl_combo * dmHangHoa.SOLUONG;

                            sSQL = "";
                            sSQL += "Insert Into CT_HOADON_COMBO( MA_HOADON,MA_COMBO,MA_HANGHOA, SOLUONG)" + "\n";
                            sSQL += "Values(";
                            sSQL += clsMain.SQLString(idBill) + ",";
                            sSQL += clsMain.SQLString(dt.Rows[0]["MA_HANGHOA"].ToString()) + ",";
                            sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                            sSQL += clsMain.SQLString(sl_HangCombo.ToString()) + ")";
                            clsMain.ExecuteSQL(sSQL);
                        }
                    }
                    dem = dem - 1;
                    if (dem == 0)
                        flag_IsCombo = false;
                }
                // print bill
                if (bPrintBill)
                {
                    convert cvrt = new convert();
                    DataTable bill = cvrt.ConvertToDataTable(listDM_HangHoa);
                    if (idVoucher != "")
                    {
                        DataColumn colidVoucher = new DataColumn();
                        colidVoucher.ColumnName = "idVoucher";
                        colidVoucher.DataType = System.Type.GetType("System.String");
                        colidVoucher.DefaultValue = idVoucher ;

                        DataColumn colgiatriVoucher = new DataColumn();
                        colgiatriVoucher.ColumnName = "giatriVoucher";
                        colgiatriVoucher.DataType = System.Type.GetType("System.String");
                        if(decimal.Parse (giatriVoucher.Replace (",","") )<=100)
                        {
                            colgiatriVoucher.DefaultValue = giatriVoucher + " %";
                        }
                        else 
                        {
                            colgiatriVoucher.DefaultValue = decimal.Parse(giatriVoucher.Replace(",", "")).ToString("N0") + " VND";
                        }
                        
                        bill.Columns.Add(colidVoucher);
                        bill.Columns.Add(colgiatriVoucher);
                    }
                    if (lbIdInnerCustomerShow.Text != "")
                    {
                        DataColumn colNameInnerCustomer = new DataColumn();
                        colNameInnerCustomer.ColumnName = "NameInnerCustomer";
                        colNameInnerCustomer.DataType = System.Type.GetType("System.String");
                        colNameInnerCustomer.DefaultValue = lbNameInnerCustomer.Text;

                        DataColumn colMoneySurplus = new DataColumn();
                        colMoneySurplus.ColumnName = "MoneySurplus";
                        colMoneySurplus.DataType = System.Type.GetType("System.String");
                        colMoneySurplus.DefaultValue = (decimal.Parse(lbMoneySurplus.Text.Replace(",", "").Replace("VND", "").Trim()) - decimal.Parse(dMoneyInnerCard.ToString ().Replace (",",""))).ToString("N0") + " VND";

                        bill.Columns.Add(colNameInnerCustomer);
                        bill.Columns.Add(colMoneySurplus);
                    }
                    if (lbIDDearCustomerShow.Text != "")
                    {
                        DataColumn colNameDearCustomer = new DataColumn();
                        colNameDearCustomer.ColumnName = "NameDearCustomer";
                        colNameDearCustomer.DataType = System.Type.GetType("System.String");
                        colNameDearCustomer.DefaultValue = lbNameDearCustomer.Text;

                        DataColumn colMark = new DataColumn();
                        colMark.ColumnName = "Mark";
                        colMark.DataType = System.Type.GetType("System.String");
                        colMark.DefaultValue = (decimal.Parse(lbMark.Text.Replace(",", "")) + ((dMoneyCash + dMoneyVoucher + dMoneyInnerCard + dMoneyVisa) / 10000)).ToString("F");
           
                        bill.Columns.Add(colNameDearCustomer);
                        bill.Columns.Add(colMark);
                    }

                    Frm_Report1 frm = new Frm_Report1();
                    if (GH_diachi == "")
                    {
                        if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                        {
                            if (cls_ConfigCashier.sIn_Number_Fastfood != "0" & sInTem == true)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_TraSua_58";
                            }
                            else
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_58";
                            }
                        }
                        else
                        {
                            if (cls_KP_RES.Mode == 8)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_thaomoc";
                            }
                            else if (cls_KP_RES.Mode == 9)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_image";
                            }
                            else if (cls_KP_RES.Mode == 10)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_MaVach";
                            }
                            else if (cls_ConfigCashier.sIn_Number_Fastfood != "0" & sInTem == true)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_TraSua";
                            }
                            else
                            {
                                frm.ReportName = "rpt_inhoadon_Bill";
                            }
                          
                        }
                    }
                    else
                    {
                        if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                        {
                            frm.ReportName = "rpt_inhoadon_Bill_giaohang_58";
                        }
                        else
                        {
                            if (cls_KP_RES.Mode == 6)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_giaohang_annhien";
                            }
                            else if (cls_KP_RES.Mode == 8)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_giaohang_thaomoc";
                            }
                            else if (cls_KP_RES.Mode == 9)
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_Giaohang_image";
                            }
                            else
                            {
                                frm.ReportName = "rpt_inhoadon_Bill_giaohang";
                            }
                        }
                    }
                    frm.DataSource = bill;
                    frm.WindowState = FormWindowState.Minimized;
                    frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                    frm.IsPrint = true;
                    frm.Copy = KP_Terminal.clsKP_Terminal.iCopy;
                    frm.ShowDialog();
                    frm.Dispose();
                }
                //TTTNB
                if (lbIdInnerCustomerShow.Text != "")
                {
                    cls_KHTT.UpdateInnerCustomer(lbIdInnerCustomerShow.Text, dMoneyInnerCard);
                }
                //KHTT
                if (lbIDDearCustomerShow.Text != "")
                {
                    DataTable dtKH = cls_KHTT.ReturnCauHinhKHTT();
                    if (dtKH.Rows[0]["GIATRI"].ToString() == "1")
                        cls_KHTT.CongDiemKhachHangThanThiet(lbIDDearCustomerShow.Text, decimal.Parse(lbTotalMoneyPayment.Tag.ToString()));
                    else
                        cls_KHTT.CongDiemKhachHangThanThiet(lbIDDearCustomerShow.Text, decimal.Parse(lbTotalMoneyAll.Tag.ToString()));
                }
                //Voucher
                if (idVoucher != "")
                {
                    sSQL = "";
                    sSQL += "Update VOUCHER_DETAIL" + "\n";
                    sSQL += "Set STATUS=1,NGAY_SUDUNG=GETDATE(),HOADON=" + clsMain.SQLString(idBill) + "\n";
                    sSQL += "Where MA=" + clsMain.SQLString(idVoucher) + "\n";
                 
                    sSQL += "Update HOADON" + "\n";
                    sSQL += "Set GHICHU1=ISNULL(GHICHU1,'')+ " + clsMain.SQLString(idVoucher) + "\n";
                    sSQL += "Where MA_HOADON=" + clsMain.SQLString(idBill);
                    clsMain.ExecuteSQL(sSQL);
                }
                //Công nợ
                if (CN_Flag)
                {
                    cls_KHTT.CongNoKhachHangHoaDon(CN_MaKH, idBill, decimal.Parse(lbTotalMoneyAll.Text), decimal.Parse(lbTotalMoneyAll.Text) - CN_SoTienTra, CN_ngayHenTra);
                }
                //Trừ kho
                sSQL = string.Format("Exec SP_TRUKHO_DINHLUONG @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 1);
                clsMain.ExecuteSQL(sSQL);
                sSQL = string.Format("Exec SP_TRUKHO_HOADON @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 1);
                clsMain.ExecuteSQL(sSQL);
                //cho biet Ma_hanghoa va DONGIA cua Combo cho cac hang hoa thuoc ve Combo
                sSQL = string.Format("Exec SP_CT_HOADON_UPDATE @MA_HOADON='{0}'", idBill);
                clsMain.ExecuteSQL(sSQL);

                //31/08/2017 fastfood sau khi in bill thì trên lưới có bao nhiêu hàng mỗi hàng sẻ in tem qua máy in tem
                PrintMaVachFastFood(idBill);
                //08/2017 fastfood cung can in bep
                PrintOrderBepFastFood(idBill);

                return;
            }
            if (_sIdTable != "")
            {
                if (tachBill == false)
                {
                    List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                    string idBill = _sIdBill;
                    sSQL = "";
                    sSQL += "Update HOADON set " + "\n";
                    sSQL += "MA_CUAHANG=" + clsMain.SQLString(cls_ConfigCashier.idShop) + ",";
                    sSQL += "MA_KHO=" + clsMain.SQLString(cls_ConfigCashier.idWarehouse) + ",";
                    sSQL += "MA_QUAY=" + clsMain.SQLString(cls_ConfigCashier.idCashier) + ",ISPAYMENT=1,";
                    sSQL += "GIAMGIA=" + clsMain.SQLString(lbDiscountPayment.Text.Replace(",", "")) + ",";
                    sSQL += "TIEN_TRAKHACH =" + clsMain.SQLString(lbReplyCustomer.Text.Replace(",", "")) + ",";
                    sSQL += "TIEN_KHACHTRA =" + clsMain.SQLString(lbGetCustomer.Text.Replace(",", "")) + ",";
                    sSQL += "TONGTIEN=" + clsMain.SQLString(lbTotalMoneyAll.Text.Replace(",", "")) + ",";
                    sSQL += "GIORA=" + clsMain.SQLString(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")) + ",";
                    sSQL += "NHANVIEN_RABILL=" + clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                    sSQL += "PHIPHUCVU=" + clsMain.SQLString(lbServiceMoney.Text.Replace(",", "")) + ",";
                    sSQL += "SOTHETT=" + clsMain.SQLString(soThe) + ",";
                    if (CN_Flag)
                    {
                        sSQL += "MA_KHACHHANG=" + clsMain.SQLString(CN_MaKH) + ",";
                    }
                    else
                    {
                        if (lbIDDearCustomerShow.Text != "")
                        {
                            sSQL += "MA_KHACHHANG = " + "(SELECT MA FROM KHTT WHERE MATHE = " + clsMain.SQLString(lbIDDearCustomerShow.Text) + ")" + ",";
                        }
                        else if (GH_MaKH != "")
                            sSQL += "MA_KHACHHANG=" + clsMain.SQLString(GH_MaKH) + ",";
                    }
                    sSQL += "NGAYTHETT = " + clsMain.SQLString(ngayThe) + ",";
                    sSQL += "TIENTRATHETT = " + clsMain.SQLString(dMoneyVisa.ToString()) + ",";
                    sSQL += "MONEYVISA = " + clsMain.SQLString(dMoneyVisa.ToString()) + ",";
                    sSQL += "MONEYINNERCARD = " + clsMain.SQLString(dMoneyInnerCard.ToString()) + ",";
                    sSQL += "MONEYVOUCHER = " + clsMain.SQLString(dMoneyVoucher.ToString()) + ",";
                    sSQL += "MONEYCASH = " + clsMain.SQLString(dMoneyCash.ToString()) + ",";
                    sSQL += "MONEYPHIEUMUAHANG = " + clsMain.SQLString(dTienPhieuMuaHang.ToString()) + ",";
                    sSQL += "GHICHUPHIEUMUAHANG = " + clsMain.SQLString(sGhiChuPhieuMuaHang) + ",";
                    sSQL += "GHICHU = " + clsMain.SQLStringUnicode(GhiChu) + "\n";
                    sSQL += "Where MA_HOADON = " + clsMain.SQLString(idBill);
                    clsMain.ExecuteSQL(sSQL);

                    DataTable dtTables = clsMain.ReturnDataTable("SELECT MA_BAN FROM CT_MOBAN WHERE MA_HOADON = '" + idBill + "'");
                    if (dtTables.Rows.Count > 0)
                    {
                        sSQL = "";
                        for (int i = 0; i < dtTables.Rows.Count; i++)
                        {
                            sSQL += "Update BAN SET ISMO_BAN=0, SEE_PAYMENTS = 0" + "\n";
                            sSQL += "Where MA_BAN =" + clsMain.SQLString(dtTables.Rows[i]["MA_BAN"].ToString()) + "\n";
                            clsMain.ExecuteSQL(sSQL);
                        }
                    }

                    //Bảng chi tiết hóa đơn
                    DataTable dt = new DataTable();
                    decimal sl_combo = 0;
                    bool flag_IsCombo = false;
                    int dem = 0;
                    for (int i = 0; i < gvListProduct.RowCount; i++)
                    {
                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        dmHangHoa.STT = (i + 1).ToString();
                        dmHangHoa.Ten_HoaDon = "PHIẾU THANH TOÁN";
                        if (cls_KP_RES.Mode == 4)
                        {
                            dmHangHoa.Ten_HoaDon = "PHIẾU THANH TOÁN";
                        }
                        else
                        {
                            dmHangHoa.Ten_HoaDon = "PHIẾU THANH TOÁN";
                        }
                        if (GH_diachi != "")
                        {
                            dmHangHoa.DIACHIGIAOHANG = GH_diachi;
                            dmHangHoa.TENKH = GH_TenKH;
                            dmHangHoa.DIENTHOAI_KH = GH_Dienthoai;
                        }
                        dmHangHoa.MA_HANGHOA = gvListProduct.GetRowCellValue(i, gvListProduct.Columns["MA_HANGHOA"]).ToString();
                        dmHangHoa.TEN_HANGHOA = gvListProduct.GetRowCellValue(i, gvListProduct.Columns["TEN_HANGHOA"]).ToString();
                        dmHangHoa.SOLUONG = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["SOLUONG"]).ToString());
                        dmHangHoa.GIABAN = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["GIABAN"]).ToString());
                        dmHangHoa.Thanh_Tien = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["Thanh_Tien"]).ToString());
                        dmHangHoa.CHIETKHAU = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["CHIETKHAU"]).ToString());
                        dmHangHoa.THUE = double.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["THUE"]).ToString());
                        dmHangHoa.Tong_Cong_Tru_Voucher = decimal.Parse(lbTotalMoneyAll.Text) - dMoneyVoucher;
                        dmHangHoa.Tong_Cong = lbTotalMoneyAll.Text;
                        dmHangHoa.Tien_TraKhach = lbReplyCustomer.Text;
                        dmHangHoa.TEN_BAN = _sTenBan;
                        if (lbGetCustomer.Text != "0")
                            dmHangHoa.Tien_KhachTra = lbGetCustomer.Text;
                        else
                        {
                            dmHangHoa.Tien_KhachTra = txtInputMoney.Text;
                            dmHangHoa.Tien_TraKhach = (double.Parse(txtInputMoney.Text.ToString()) - double.Parse(lbTotalMoneyAll.Text.ToString())).ToString();
                            lbReplyCustomer.Text = dmHangHoa.Tien_TraKhach;
                        }
                        if (double.Parse(lbServiceMoney.Text) > 100)
                            dmHangHoa.Phiphucvu = lbServiceMoney.Text;
                        else
                            dmHangHoa.Phiphucvu = lbServiceMoney.Text + "%";
                        dmHangHoa.Tiendatcoc = lbMoneyGetFirst.Text;
                        if (double.Parse(lbDiscountPayment.Text) > 100)
                            dmHangHoa.Giam_Gia = lbDiscountPayment.Text;
                        else
                            dmHangHoa.Giam_Gia = lbDiscountPayment.Text + "%";
                        dmHangHoa.VOUCHER = dMoneyVoucher;
                        dmHangHoa.Ma_HoaDon = idBill;
                        dmHangHoa.Ma_Quay = cls_ConfigCashier.nameCashier;
                        dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                        dmHangHoa.ngayHD = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                        dmHangHoa.IS_COMBO = bool.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["IS_COMBO"]).ToString());
                        dmHangHoa.GhiChu = GhiChu;
                        listDM_HangHoa.Add(dmHangHoa);

                        decimal sChietKhau = dmHangHoa.CHIETKHAU;
                        GiaBan_Thuc = dmHangHoa.GIABAN - dmHangHoa.GIABAN * per_Discount / 100 + dmHangHoa.GIABAN * per_Extra / 100;
                        GiaBan_Thuc -= GiaBan_Thuc * sChietKhau / 100;

                        sSQL = "";
                        sSQL += "Update CT_HOADON" + "\n";
                        sSQL += "Set GIABAN_THAT=" + clsMain.SQLString(GiaBan_Thuc.ToString() == "" ? dmHangHoa.GIABAN.ToString() : GiaBan_Thuc.ToString());
                        sSQL += "Where MA_HOADON=" + clsMain.SQLString(idBill) + "\n";
                        sSQL += "And GIABAN=" + clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + "\n";
                        sSQL += "And MA_HANGHOA=" + clsMain.SQLString(dmHangHoa.MA_HANGHOA) + "\n";
                        clsMain.ExecuteSQL(sSQL);
                        sSQL = "";
                        sSQL += "Update CT_HOADON" + "\n";
                        sSQL += "Set CHIETKHAU=" + clsMain.SQLString(dmHangHoa.CHIETKHAU.ToString());
                        sSQL += "Where MA_HOADON=" + clsMain.SQLString(idBill) + "\n";
                        sSQL += "And MA_HANGHOA=" + clsMain.SQLString(dmHangHoa.MA_HANGHOA);
                        clsMain.ExecuteSQL(sSQL);

                        if (!flag_IsCombo)
                        {
                            sSQL = "Select MA_HANGHOA From HANGHOA Where IS_COMBO=1 AND MA_HANGHOA=" + clsMain.SQLString(dmHangHoa.MA_HANGHOA);
                            dt = clsMain.ReturnDataTable(sSQL);
                            if (dt.Rows.Count > 0)
                            {
                                flag_IsCombo = true;
                                sl_combo = dmHangHoa.SOLUONG;
                                sSQL = "";
                                sSQL += "Select Count(*) From COMBO Where MA_COMBO=" + clsMain.SQLString(dmHangHoa.MA_HANGHOA);
                                DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                                if (dt1.Rows.Count > 0)
                                {
                                    dem = int.Parse(dt1.Rows[0][0].ToString()) + 1;
                                }
                            }
                        }
                        if (flag_IsCombo)
                        {
                            if (dmHangHoa.IS_COMBO)
                            {
                                decimal sl_HangCombo = sl_combo * dmHangHoa.SOLUONG;

                                sSQL = "";
                                sSQL += "Insert Into CT_HOADON_COMBO( MA_HOADON,MA_COMBO,MA_HANGHOA, SOLUONG)" + "\n";
                                sSQL += "Values(";
                                sSQL += clsMain.SQLString(idBill) + ",";
                                sSQL += clsMain.SQLString(dt.Rows[0]["MA_HANGHOA"].ToString()) + ",";
                                sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                                sSQL += clsMain.SQLString(sl_HangCombo.ToString()) + ")";
                                clsMain.ExecuteSQL(sSQL);
                            }
                        }
                        dem = dem - 1;
                        if (dem == 0)
                            flag_IsCombo = false;
                    }
                    //print bill
                    if (bPrintBill)
                    {
                        convert cvrt = new convert();
                        DataTable bill = cvrt.ConvertToDataTable(listDM_HangHoa);
                        if (idVoucher != "")
                        {
                            DataColumn colidVoucher = new DataColumn();
                            colidVoucher.ColumnName = "idVoucher";
                            colidVoucher.DataType = System.Type.GetType("System.String");
                            colidVoucher.DefaultValue = idVoucher;

                            DataColumn colgiatriVoucher = new DataColumn();
                            colgiatriVoucher.ColumnName = "giatriVoucher";
                            colgiatriVoucher.DataType = System.Type.GetType("System.String");
                            if (decimal .Parse(giatriVoucher.Replace(",", "")) <= 100)
                            {
                                colgiatriVoucher.DefaultValue = giatriVoucher + " %";
                            }
                            else
                            {
                                colgiatriVoucher.DefaultValue = decimal.Parse(giatriVoucher.Replace(",", "")).ToString("N0") + " VND";
                            }

                            bill.Columns.Add(colidVoucher);
                            bill.Columns.Add(colgiatriVoucher);
                        }
                        if (lbIdInnerCustomerShow.Text != "")
                        {
                            DataColumn colNameInnerCustomer = new DataColumn();
                            colNameInnerCustomer.ColumnName = "NameInnerCustomer";
                            colNameInnerCustomer.DataType = System.Type.GetType("System.String");
                            colNameInnerCustomer.DefaultValue = lbNameInnerCustomer.Text;

                            DataColumn colMoneySurplus = new DataColumn();
                            colMoneySurplus.ColumnName = "MoneySurplus";
                            colMoneySurplus.DataType = System.Type.GetType("System.String");
                            colMoneySurplus.DefaultValue = (decimal .Parse(lbMoneySurplus.Text.Replace(",", "").Replace("VND", "").Trim()) - decimal .Parse(dMoneyInnerCard.ToString().Replace(",", ""))).ToString("N0") + " VND";

                            bill.Columns.Add(colNameInnerCustomer);
                            bill.Columns.Add(colMoneySurplus);
                        }
                        if (lbIDDearCustomerShow.Text != "")
                        {
                            DataColumn colNameDearCustomer = new DataColumn();
                            colNameDearCustomer.ColumnName = "NameDearCustomer";
                            colNameDearCustomer.DataType = System.Type.GetType("System.String");
                            colNameDearCustomer.DefaultValue = lbNameDearCustomer.Text;

                            DataColumn colMark = new DataColumn();
                            colMark.ColumnName = "Mark";
                            colMark.DataType = System.Type.GetType("System.String");
                            colMark.DefaultValue = (decimal.Parse(lbMark.Text.Replace(",", "")) + ((dMoneyCash + dMoneyVoucher + dMoneyInnerCard + dMoneyVisa) / 10000)).ToString("F");

                            bill.Columns.Add(colNameDearCustomer);
                            bill.Columns.Add(colMark);
                        }

                        Frm_Report1 frm = new Frm_Report1();
                        if (GH_diachi == "")
                        {
                            if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                            {
                                if (cls_KP_RES.Mode == 9)
                                {
                                    frm.ReportName = "rpt_inhoadon_Bill_ban_58_Image";
                                }
                                else if (cls_ConfigCashier.sIn_Number_Fastfood != "0" & sInTem == true)
                                {
                                    frm.ReportName = "rpt_inhoadon_Bill_ban_TraSua_58";
                                }
                                else
                                {
                                    frm.ReportName = "rpt_inhoadon_Bill_ban_58";
                                }
                            }
                            else
                            {
                                if (cls_KP_RES.Mode == 3)
                                {
                                    frm.ReportName = "rpt_inhoadon_Bill_ban_1";
                                }
                                else if (cls_KP_RES.Mode == 4)
                                {
                                    frm.ReportName = "rpt_inhoadon_Bill_ban_pho";
                                }
                                else if (cls_KP_RES.Mode == 5)
                                {
                                    frm.ReportName = "rpt_inhoadon_Bill_ban_2";
                                }
                                else if (cls_KP_RES.Mode == 9)
                                {
                                    frm.ReportName = "rpt_inhoadon_Bill_ban_image";
                                }
                                else if (cls_ConfigCashier.sIn_Number_Fastfood != "0" & sInTem == true)
                                {
                                    frm.ReportName = "rpt_inhoadon_Bill_ban_TraSua";
                                }
                                else
                                {
                                    frm.ReportName = "rpt_inhoadon_Bill_ban";
                                }

                            }
                        }
                        else
                        {
                            if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                                frm.ReportName = "rpt_inhoadon_Bill_ban_giaohang_58";
                            else
                                frm.ReportName = "rpt_inhoadon_Bill_ban_giaohang";
                        }
                        frm.DataSource = bill;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                        frm.IsPrint = true;
                        frm.Copy = KP_Terminal.clsKP_Terminal.iCopy;
                        frm.ShowDialog();
                        frm.Dispose();
                        if (cls_KP_RES.Mode == 4)
                        {
                            DataTable bill1 = bill.Copy();
                            foreach (DataRow dr in bill1.Rows)
                            {
                                dr["Ten_HoaDon"] = "PHIẾU IN LẠI";
                            }

                            Frm_Report1 frm1 = new Frm_Report1();
                            if (GH_diachi == "")
                            {
                                if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                                {
                                    frm1.ReportName = "rpt_inhoadon_Bill_ban_58";
                                }
                                else
                                {
                                    if (cls_KP_RES.Mode == 3)
                                    {
                                        frm1.ReportName = "rpt_inhoadon_Bill_ban_1";
                                    }
                                    else if (cls_KP_RES.Mode == 4)
                                    {
                                        frm1.ReportName = "rpt_inhoadon_Bill_ban_pho";
                                    }
                                    else if (cls_KP_RES.Mode == 5)
                                    {
                                        frm.ReportName = "rpt_inhoadon_Bill_ban_2";
                                    }
                                    else if (cls_KP_RES.Mode == 9)
                                    {
                                        frm.ReportName = "rpt_inhoadon_Bill_ban_image";
                                    }
                                    else
                                    {
                                        frm1.ReportName = "rpt_inhoadon_Bill_ban";
                                    }

                                }
                            }
                            else
                            {
                                if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                                    frm1.ReportName = "rpt_inhoadon_Bill_ban_giaohang_58";
                                else
                                    frm1.ReportName = "rpt_inhoadon_Bill_ban_giaohang";
                            }
                            frm1.DataSource = bill1;
                            frm1.WindowState = FormWindowState.Minimized;
                            frm1.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                            frm1.IsPrint = true;
                            frm1.Copy = KP_Terminal.clsKP_Terminal.iCopy;
                            frm1.ShowDialog();
                            frm1.Dispose();
                        }
                    }
                    //TTTNB
                    if (lbIdInnerCustomerShow.Text != "")
                    {
                        cls_KHTT.UpdateInnerCustomer(lbIdInnerCustomerShow.Text, dMoneyInnerCard);
                    }
                    //KHTT
                    if (lbIDDearCustomerShow.Text != "")
                    {
                        DataTable dtKH = cls_KHTT.ReturnCauHinhKHTT();
                        if (dtKH.Rows[0]["GIATRI"].ToString() == "1")
                            cls_KHTT.CongDiemKhachHangThanThiet(lbIDDearCustomerShow.Text, decimal.Parse(lbTotalMoneyPayment.Tag.ToString()));
                        else
                            cls_KHTT.CongDiemKhachHangThanThiet(lbIDDearCustomerShow.Text, decimal.Parse(lbTotalMoneyAll.Tag.ToString()));
                    }
                    //Voucher
                    if (idVoucher != "")
                    {
                        sSQL = "";
                        sSQL += "Update VOUCHER_DETAIL" + "\n";
                        sSQL += "Set STATUS=1,NGAY_SUDUNG=GETDATE(),HOADON=" + clsMain.SQLString(idBill) + "\n";
                        sSQL += "Where MA=" + clsMain.SQLString(idVoucher) + "\n";

                        sSQL += "Update HOADON" + "\n";
                        sSQL += "Set GHICHU1=ISNULL(GHICHU1,'')+ " + clsMain.SQLString(idVoucher) + "\n";
                        sSQL += "Where MA_HOADON=" + clsMain.SQLString(idBill);
                        clsMain.ExecuteSQL(sSQL);
                    }
                    //Công nợ
                    if (CN_Flag)
                    {
                        cls_KHTT.CongNoKhachHangHoaDon(CN_MaKH, idBill, decimal.Parse(lbTotalMoneyAll.Text), decimal.Parse(lbTotalMoneyAll.Text) - CN_SoTienTra, CN_ngayHenTra);
                    }
                    //Trừ kho
                    sSQL = string.Format("Exec SP_TRUKHO_DINHLUONG @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 1);
                    clsMain.ExecuteSQL(sSQL);
                    sSQL = string.Format("Exec SP_TRUKHO_HOADON @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 1);
                    clsMain.ExecuteSQL(sSQL);
                    //cho biet Ma_hanghoa va DONGIA cua Combo cho cac hang hoa thuoc ve Combo
                    sSQL = string.Format("Exec SP_CT_HOADON_UPDATE @MA_HOADON='{0}'", idBill);
                    clsMain.ExecuteSQL(sSQL);

                    return;
                }
                // tách bill
                else
                {
                    // 1. Tạo hóa đơn thanh toán các hàng hóa tách bill
                    // Bảng hóa đơn
                    List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                    string idBill = GetNewCodeBill();
                    sSQL = "";
                    sSQL += "Insert Into HOADON(MA_HOADON, MANHANVIEN, GIAMGIA, PHIPHUCVU, TIEN_TRAKHACH, TIEN_KHACHTRA,";
                    sSQL += "TONGTIEN, NGAYTAO, GIOVAO, GIORA,NHANVIEN_RABILL,ISPAYMENT, MA_CUAHANG, MA_KHO, MA_QUAY,MA_KHACHHANG,";
                    sSQL += "SOTHETT,NGAYTHETT,TIENTRATHETT,MONEYVISA,MONEYINNERCARD,MONEYVOUCHER,MONEYCASH,GHICHU1)" + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(idBill) + ",";
                    sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + ",";
                    sSQL += clsMain.SQLString(lbDiscountPayment.Text.Replace(",", "")) + ",";
                    sSQL += clsMain.SQLString(lbServiceMoney.Text.Replace(",", "")) + ",";
                    sSQL += clsMain.SQLString(lbReplyCustomer.Text.Replace(",", "")) + ",";
                    sSQL += clsMain.SQLString(lbGetCustomer.Text.Replace(",", "")) + ",";
                    sSQL += clsMain.SQLString(lbTotalMoneyAll.Text.Replace(",", "")) + ",";
                    sSQL += "GETDATE()" + ",";
                    sSQL += "GETDATE()" + ",";
                    sSQL += "GETDATE()" + ",";
                    sSQL += clsMain.SQLString(clsGlobal.gsMaNVOfUserLogin) + "," + 1 + ",";
                    sSQL += clsMain.SQLString(cls_ConfigCashier.idShop) + ",";
                    sSQL += clsMain.SQLString(cls_ConfigCashier.idWarehouse) + ",";
                    sSQL += clsMain.SQLString(cls_ConfigCashier.idCashier) + ",";
                    if (CN_Flag)
                    {
                        sSQL += clsMain.SQLString(CN_MaKH) + ",";
                    }
                    else
                    {
                        if (lbIDDearCustomerShow.Text != "")
                            sSQL += "(SELECT MA FROM KHTT WHERE MATHE = " + clsMain.SQLString(lbIDDearCustomerShow.Text) + ")" + ",";
                        else
                            sSQL += clsMain.SQLString(GH_MaKH) + ",";
                    }
                    sSQL += clsMain.SQLString(soThe) + ",";
                    sSQL += clsMain.SQLString(ngayThe) + ",";
                    sSQL += clsMain.SQLString(dMoneyVisa.ToString()) + ",";
                    sSQL += clsMain.SQLString(dMoneyVisa.ToString()) + ",";
                    sSQL += clsMain.SQLString(dMoneyInnerCard.ToString()) + ",";
                    sSQL += clsMain.SQLString(dMoneyVoucher.ToString()) + ",";
                    sSQL += clsMain.SQLString(dMoneyCash.ToString()) + ",";
                    sSQL += clsMain.SQLStringUnicode(GhiChu) + ")";
                    clsMain.ExecuteSQL(sSQL);
                  
                    //ct mở bàn
                    sSQL = "";
                    sSQL += "Insert Into CT_MOBAN(MA_HOADON, MA_BAN)" + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(idBill) + ",";
                    sSQL += clsMain.SQLString(_sIdTable) + ")" + "\n";
                    clsMain.ExecuteSQL(sSQL);

                    //Bảng chi tiết hóa đơn
                    DataTable dt = new DataTable();
                    decimal sl_combo = 0;
                    bool flag_IsCombo = false;
                    int dem = 0;
                    for (int i = 0; i < gvListProduct.RowCount; i++)
                    {
                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        //dmHangHoa.STT = gvListProduct.GetRowCellValue(i, gvListProduct.Columns["STT"]).ToString(); ;
                        dmHangHoa.MONTHEM = false;
                        dmHangHoa.TEM = 0;
                        dmHangHoa.Number = "0";
                        dmHangHoa.MACP = "C";
                        dmHangHoa.MACB = 0;
                        dmHangHoa.Ten_HoaDon = "PHIẾU THANH TOÁN";
                        if (GH_diachi != "")
                        {
                            dmHangHoa.DIACHIGIAOHANG = GH_diachi;
                            dmHangHoa.TENKH = GH_TenKH;
                            dmHangHoa.DIENTHOAI_KH = GH_Dienthoai;
                        }
                        dmHangHoa.MA_HANGHOA = gvListProduct.GetRowCellValue(i, gvListProduct.Columns["MA_HANGHOA"]).ToString();
                        dmHangHoa.TEN_HANGHOA = gvListProduct.GetRowCellValue(i, gvListProduct.Columns["TEN_HANGHOA"]).ToString();
                        dmHangHoa.SOLUONG = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["SOLUONG"]).ToString());
                        dmHangHoa.GIABAN = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["GIABAN"]).ToString());
                        dmHangHoa.THUE = double.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["THUE"]).ToString());
                        dmHangHoa.Thanh_Tien = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["Thanh_Tien"]).ToString());
                        //dmHangHoa.IS_INBEP = bool.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["IS_INBEP"]).ToString());
                        dmHangHoa.CHIETKHAU = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["CHIETKHAU"]).ToString());
                        dmHangHoa.Tong_Cong_Tru_Voucher = decimal.Parse(lbTotalMoneyAll.Text) - dMoneyVoucher;
                        dmHangHoa.Tong_Cong = lbTotalMoneyAll.Text;
                        dmHangHoa.Tien_TraKhach = lbReplyCustomer.Text;
                        dmHangHoa.GhiChu = GhiChu;
                        dmHangHoa.TEN_BAN = _sTenBan;

                        if (lbGetCustomer.Text != "0")
                            dmHangHoa.Tien_KhachTra = lbGetCustomer.Text;
                        else
                        {
                            dmHangHoa.Tien_KhachTra = txtInputMoney.Text;
                            dmHangHoa.Tien_TraKhach = (double.Parse(txtInputMoney.Text.ToString()) - double.Parse(lbTotalMoneyAll.Text.ToString())).ToString();
                            lbReplyCustomer.Text = dmHangHoa.Tien_TraKhach;
                        }
                        if (double.Parse(lbServiceMoney.Text) > 100)
                            dmHangHoa.Phiphucvu = lbServiceMoney.Text;
                        else
                            dmHangHoa.Phiphucvu = lbServiceMoney.Text + "%";
                        dmHangHoa.Tiendatcoc = lbMoneyGetFirst.Text;
                        if (double.Parse(lbDiscountPayment.Text) > 100)
                            dmHangHoa.Giam_Gia = lbDiscountPayment.Text;
                        else
                            dmHangHoa.Giam_Gia = lbDiscountPayment.Text + "%";
                        dmHangHoa.VOUCHER = dMoneyVoucher;
                        dmHangHoa.Ma_HoaDon = idBill;
                        dmHangHoa.Ma_Quay = cls_ConfigCashier.nameCashier;
                        dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                        dmHangHoa.ngayHD = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                        dmHangHoa.IS_COMBO = bool.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["IS_COMBO"]).ToString());

                        String sMaNhanVien = "0";
                        String sTenNhanVien = "";
                        try
                        {
                            sMaNhanVien = dtGridviewProduct.Select("MA_HANGHOA=" + clsMain.SQLString(dmHangHoa.MA_HANGHOA))[0]["sMaNhanVien"].ToString();
                            sTenNhanVien = dtGridviewProduct.Select("MA_HANGHOA=" + clsMain.SQLString(dmHangHoa.MA_HANGHOA))[0]["NhanVien"].ToString();
                        }
                        catch (Exception e)
                        {
                        }
                        dmHangHoa.TenNhanVien = sTenNhanVien;
                        dmHangHoa.GhiChu = GhiChu;
                        listDM_HangHoa.Add(dmHangHoa);
                        decimal sChietKhau = dmHangHoa.CHIETKHAU;
                        GiaBan_Thuc = dmHangHoa.GIABAN - dmHangHoa.GIABAN * per_Discount / 100 + dmHangHoa.GIABAN * per_Extra / 100;
                        GiaBan_Thuc -= GiaBan_Thuc * sChietKhau / 100;
                        sSQL = "";
                        sSQL += "Insert Into CT_HOADON( MA_HOADON, MA_HANGHOA, SOLUONG, GIABAN, THUE,IS_COMBO,GIABAN_THAT,CHIETKHAU,NHANVIEN,TEM,MACP,Number,MACB)" + "\n";
                        sSQL += "Values(";
                        sSQL += clsMain.SQLString(idBill) + ",";
                        sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                        sSQL += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                        sSQL += clsMain.SQLString(dmHangHoa.GIABAN.ToString()) + ",";
                        sSQL += clsMain.SQLString(dmHangHoa.THUE.ToString()) + ",";
                        sSQL += clsMain.SQLBit(dmHangHoa.IS_COMBO) + ",";
                        sSQL += clsMain.SQLString(GiaBan_Thuc.ToString() == "" ? dmHangHoa.GIABAN.ToString() : GiaBan_Thuc.ToString()) + ",";
                        sSQL += clsMain.SQLString(sChietKhau.ToString()) + ",";
                        sSQL += clsMain.SQLString(sMaNhanVien) + ",";
                        sSQL += clsMain.SQLString(dmHangHoa.TEM.ToString()) + ",";
                        sSQL += clsMain.SQLString(dmHangHoa.MACP) + ",";
                        sSQL += clsMain.SQLString(dmHangHoa.Number) + ",";
                        sSQL += clsMain.SQLString(dmHangHoa.MACB.ToString()) + ")";
                        clsMain.ExecuteSQL(sSQL);

                        if (!flag_IsCombo)
                        {
                            sSQL = "Select MA_HANGHOA From HANGHOA Where IS_COMBO=1 AND MA_HANGHOA=" + clsMain.SQLString(dmHangHoa.MA_HANGHOA);
                            dt = clsMain.ReturnDataTable(sSQL);
                            if (dt.Rows.Count > 0)
                            {
                                flag_IsCombo = true;
                                sl_combo = dmHangHoa.SOLUONG;
                                sSQL = "";
                                sSQL += "Select Count(*) From COMBO Where MA_COMBO=" + clsMain.SQLString(dmHangHoa.MA_HANGHOA);
                                DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                                if (dt1.Rows.Count > 0)
                                {
                                    dem = int.Parse(dt1.Rows[0][0].ToString()) + 1;
                                }
                            }
                        }
                        if (flag_IsCombo)
                        {
                            if (dmHangHoa.IS_COMBO)
                            {
                                decimal sl_HangCombo = sl_combo * dmHangHoa.SOLUONG;

                                sSQL = "";
                                sSQL += "Insert Into CT_HOADON_COMBO( MA_HOADON,MA_COMBO,MA_HANGHOA, SOLUONG)" + "\n";
                                sSQL += "Values(";
                                sSQL += clsMain.SQLString(idBill) + ",";
                                sSQL += clsMain.SQLString(dt.Rows[0]["MA_HANGHOA"].ToString()) + ",";
                                sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                                sSQL += clsMain.SQLString(sl_HangCombo.ToString()) + ")";
                                clsMain.ExecuteSQL(sSQL);
                            }
                        }
                        dem = dem - 1;
                        if (dem == 0)
                            flag_IsCombo = false;
                    }

                    //print bill
                    if (bPrintBill)
                    {
                        convert cvrt = new convert();
                        DataTable bill = cvrt.ConvertToDataTable(listDM_HangHoa);
                        if (idVoucher != "")
                        {
                            DataColumn colidVoucher = new DataColumn();
                            colidVoucher.ColumnName = "idVoucher";
                            colidVoucher.DataType = System.Type.GetType("System.String");
                            colidVoucher.DefaultValue = idVoucher;

                            DataColumn colgiatriVoucher = new DataColumn();
                            colgiatriVoucher.ColumnName = "giatriVoucher";
                            colgiatriVoucher.DataType = System.Type.GetType("System.String");
                            if (decimal .Parse(giatriVoucher.Replace(",", "")) <= 100)
                            {
                                colgiatriVoucher.DefaultValue = giatriVoucher + " %";
                            }
                            else
                            {
                                colgiatriVoucher.DefaultValue = decimal .Parse(giatriVoucher.Replace(",", "")).ToString("N0") + " VND";
                            }

                            bill.Columns.Add(colidVoucher);
                            bill.Columns.Add(colgiatriVoucher);
                        }
                        if (lbIdInnerCustomerShow.Text != "")
                        {
                            DataColumn colNameInnerCustomer = new DataColumn();
                            colNameInnerCustomer.ColumnName = "NameInnerCustomer";
                            colNameInnerCustomer.DataType = System.Type.GetType("System.String");
                            colNameInnerCustomer.DefaultValue = lbNameInnerCustomer.Text;

                            DataColumn colMoneySurplus = new DataColumn();
                            colMoneySurplus.ColumnName = "MoneySurplus";
                            colMoneySurplus.DataType = System.Type.GetType("System.String");
                            
                            colMoneySurplus.DefaultValue = (decimal .Parse(lbMoneySurplus.Text.Replace(",", "").Replace("VND", "").Trim()) - decimal .Parse(dMoneyInnerCard.ToString().Replace(",", ""))).ToString("N0") + " VND";

                            bill.Columns.Add(colNameInnerCustomer);
                            bill.Columns.Add(colMoneySurplus);
                        }
                        if (lbIDDearCustomerShow.Text != "")
                        {
                            DataColumn colNameDearCustomer = new DataColumn();
                            colNameDearCustomer.ColumnName = "NameDearCustomer";
                            colNameDearCustomer.DataType = System.Type.GetType("System.String");
                            colNameDearCustomer.DefaultValue = lbNameDearCustomer.Text;

                            DataColumn colMark = new DataColumn();
                            colMark.ColumnName = "Mark";
                            colMark.DataType = System.Type.GetType("System.String");
                            colMark.DefaultValue = (decimal.Parse(lbMark.Text.Replace(",", "")) + ((dMoneyCash + dMoneyVoucher + dMoneyInnerCard + dMoneyVisa) / 10000)).ToString("F");

                            bill.Columns.Add(colNameDearCustomer);
                            bill.Columns.Add(colMark);
                        }

                        Frm_Report1 frm = new Frm_Report1();
                        if (GH_diachi == "")
                        {
                            if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                            {
                                if (cls_KP_RES.Mode == 9)
                                {
                                    frm.ReportName = "rpt_inhoadon_Bill_ban_58_Image";
                                }
                                else if (cls_ConfigCashier.sIn_Number_Fastfood != "0" & sInTem == true)
                                {
                                    frm.ReportName = "rpt_inhoadon_Bill_ban_TraSua_58";
                                }
                                else
                                {
                                    frm.ReportName = "rpt_inhoadon_Bill_ban_58";
                                }
                            }
                            else
                            {
                                if (cls_KP_RES.Mode == 3)
                                {
                                    frm.ReportName = "rpt_inhoadon_Bill_ban_1";
                                }
                                else if (cls_KP_RES.Mode == 4)
                                {
                                    frm.ReportName = "rpt_inhoadon_Bill_ban_pho";
                                }
                                else if (cls_KP_RES.Mode == 5)
                                {
                                    frm.ReportName = "rpt_inhoadon_Bill_ban_2";
                                }
                                else if (cls_KP_RES.Mode == 9)
                                {
                                    frm.ReportName = "rpt_inhoadon_Bill_ban_image";
                                }
                                else if (cls_ConfigCashier.sIn_Number_Fastfood != "0" & sInTem == true)
                                {
                                    frm.ReportName = "rpt_inhoadon_Bill_ban_TraSua";
                                }
                                else
                                {
                                    frm.ReportName = "rpt_inhoadon_Bill_ban";
                                }

                            }
                        }
                        else
                        {
                            if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                                frm.ReportName = "rpt_inhoadon_Bill_ban_giaohang_58";
                            else
                                frm.ReportName = "rpt_inhoadon_Bill_ban_giaohang";
                        }
                        frm.DataSource = bill;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                        frm.IsPrint = true;
                        frm.Copy = KP_Terminal.clsKP_Terminal.iCopy;
                        frm.ShowDialog();
                        frm.Dispose();
                        if (cls_KP_RES.Mode == 4)
                        {
                            DataTable bill1 = bill.Copy();
                            foreach (DataRow dr in bill1.Rows)
                            {
                                dr["Ten_HoaDon"] = "PHIẾU IN LẠI";
                            }

                            Frm_Report1 frm1 = new Frm_Report1();
                            if (GH_diachi == "")
                            {
                                if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                                {
                                    frm1.ReportName = "rpt_inhoadon_Bill_ban_58";
                                }
                                else
                                {
                                    if (cls_KP_RES.Mode == 3)
                                    {
                                        frm1.ReportName = "rpt_inhoadon_Bill_ban_1";
                                    }
                                    else if (cls_KP_RES.Mode == 4)
                                    {
                                        frm1.ReportName = "rpt_inhoadon_Bill_ban_pho";
                                    }
                                    else if (cls_KP_RES.Mode == 5)
                                    {
                                        frm.ReportName = "rpt_inhoadon_Bill_ban_2";
                                    }
                                    else if (cls_KP_RES.Mode == 9)
                                    {
                                        frm.ReportName = "rpt_inhoadon_Bill_ban_image";
                                    }
                                    else
                                    {
                                        frm1.ReportName = "rpt_inhoadon_Bill_ban";
                                    }

                                }
                            }
                            else
                            {
                                if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                                    frm1.ReportName = "rpt_inhoadon_Bill_ban_giaohang_58";
                                else
                                    frm1.ReportName = "rpt_inhoadon_Bill_ban_giaohang";
                            }
                            frm1.DataSource = bill1;
                            frm1.WindowState = FormWindowState.Minimized;
                            frm1.PrinterName = KP_Terminal.clsKP_Terminal.Mayinbill;
                            frm1.IsPrint = true;
                            frm1.Copy = KP_Terminal.clsKP_Terminal.iCopy;
                            frm1.ShowDialog();
                            frm1.Dispose();
                        }
                    }
                    //TTTNB
                    if (lbIdInnerCustomerShow.Text != "")
                    {
                        cls_KHTT.UpdateInnerCustomer(lbIdInnerCustomerShow.Text, dMoneyInnerCard);
                    }
                    //KHTT
                    if (lbIDDearCustomerShow.Text != "")
                    {
                        DataTable dtKH = cls_KHTT.ReturnCauHinhKHTT();
                        if (dtKH.Rows[0]["GIATRI"].ToString() == "1")
                            cls_KHTT.CongDiemKhachHangThanThiet(lbIDDearCustomerShow.Text, decimal.Parse(lbTotalMoneyPayment.Tag.ToString()));
                        else
                            cls_KHTT.CongDiemKhachHangThanThiet(lbIDDearCustomerShow.Text, decimal.Parse(lbTotalMoneyAll.Tag.ToString()));
                    }
                    if (idVoucher != "")
                    {
                        sSQL = "";
                        sSQL += "Update VOUCHER_DETAIL" + "\n";
                        sSQL += "Set STATUS=1,NGAY_SUDUNG=GETDATE(),HOADON=" + clsMain.SQLString(idBill) + "\n";
                        sSQL += "Where MA=" + clsMain.SQLString(idVoucher) + "\n";

                        sSQL += "Update HOADON" + "\n";
                        sSQL += "Set GHICHU1=ISNULL(GHICHU1,'')+ " + clsMain.SQLString(idVoucher) + "\n";
                        sSQL += "Where MA_HOADON=" + clsMain.SQLString(idBill);
                        clsMain.ExecuteSQL(sSQL);
                    }
                    //Công nợ
                    if (CN_Flag)
                    {
                        cls_KHTT.CongNoKhachHangHoaDon(CN_MaKH, idBill, decimal.Parse(lbTotalMoneyAll.Text), decimal.Parse(lbTotalMoneyAll.Text) - CN_SoTienTra, CN_ngayHenTra);
                    }
                    //Trừ kho
                    sSQL = string.Format("Exec SP_TRUKHO_DINHLUONG @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 1);
                    clsMain.ExecuteSQL(sSQL);
                    sSQL = string.Format("Exec SP_TRUKHO_HOADON @MA_HOADON='{0}', @IS_PAYMENT={1}", idBill, 1);
                    clsMain.ExecuteSQL(sSQL);
                    //cho biet Ma_hanghoa va DONGIA cua Combo cho cac hang hoa thuoc ve Combo
                    sSQL = string.Format("Exec SP_CT_HOADON_UPDATE @MA_HOADON='{0}'", idBill);
                    clsMain.ExecuteSQL(sSQL);
                    tachBill = false;

                    // 2. Xóa bill cũ trước khi tách bill
                    string idBill1 = _sIdBill;
                    string sqldelte = "DELETE CT_HOADON WHERE MA_HOADON ='" + idBill1 + "'";
                    clsMain.ExecuteSQL(sqldelte);
                    for (int i = 0; i < dt_ban_new.Rows.Count; i++)
                    {
                        DM_HangHoa dmHangHoa = new DM_HangHoa();
                        dmHangHoa.MA_HANGHOA = dt_ban_new.Rows[i]["_MA_HANGHOA"].ToString();
                        dmHangHoa.SOLUONG = decimal.Parse(dt_ban_new.Rows[i]["_SOLUONG"].ToString());
                        dmHangHoa.GIABAN = decimal.Parse(dt_ban_new.Rows[i]["_GIABAN"].ToString());
                        if (idBill1 != null)
                        {
                                string sqlc = "";
                                sqlc += "Insert into CT_HOADON( MA_HOADON, MA_HANGHOA,  SOLUONG, GIABAN,GIABAN_THAT, Is_Combo)" + "\n";
                                sqlc += "Values(";
                                sqlc += clsMain.SQLString(idBill1) + ",";
                                sqlc += clsMain.SQLString(dt_ban_new.Rows[i]["_MA_HANGHOA"].ToString()) + ",";
                                sqlc += clsMain.SQLString(dt_ban_new.Rows[i]["_SOLUONG"].ToString()) + ",";
                                sqlc += clsMain.SQLString(dt_ban_new.Rows[i]["_GIABAN"].ToString()) + ",";
                                sqlc += clsMain.SQLString(dt_ban_new.Rows[i]["_GIABAN"].ToString()) + ",";
                                sqlc += clsMain.SQLBit(bool.Parse(dt_ban_new.Rows[i]["_IS_COMBO"].ToString())) + ")";
                                clsMain.ExecuteSQL(sqlc);
                        }
                        else
                        {
                            XtraMessageBox.Show("Lỗi tách bill", _thongbao, MessageBoxButtons.OK);
                        }
                    }
                }
            }
        }

        private void PrintOrderBepFastFood(String sIdBill)
        {
            String sSQL = "";
            while (gvListProduct.RowCount > 0)
            {
                List<DM_HangHoa> listDM_HangHoa = new List<DM_HangHoa>();
                int j = 0;
                string maBEP = "";
                for (int i = 0; i < gvListProduct.RowCount; i++)
                {
                    DM_HangHoa dmHangHoa = new DM_HangHoa();
                    maBEP = gvListProduct.GetRowCellValue(0, gvListProduct.Columns["MA_BEP"]).ToString();

                    if (maBEP == gvListProduct.GetRowCellValue(i, gvListProduct.Columns["MA_BEP"]).ToString())
                    {
                        dmHangHoa.Ma_HoaDon = sIdBill;
                        dmHangHoa.Ten_HoaDon = "Order Bếp";
                        dmHangHoa.TEN_BAN = "FastFood";
                        dmHangHoa.MaQuay = cls_ConfigCashier.nameCashier;
                        dmHangHoa.Ngay = string.Format("{0:dd-MM-yyyy HH:mm:ss}", clsMain.ReturnDataTable("select GETDATE()").Rows[0][0]);
                        dmHangHoa.TenNV = KP_UserManagement.clsGlobal.gsNameOfUserLogin;
                        dmHangHoa.STT = (++j).ToString();
                        dmHangHoa.MA_HANGHOA = gvListProduct.GetRowCellValue(i, gvListProduct.Columns["MA_HANGHOA"]).ToString();
                        dmHangHoa.TEN_HANGHOA = gvListProduct.GetRowCellValue(i, gvListProduct.Columns["TEN_HANGHOA"]).ToString();
                        dmHangHoa.SOLUONG = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["SOLUONG"]).ToString());
                        dmHangHoa.THUE = double.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["THUE"]).ToString());
                        dmHangHoa.GIABAN = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["GIABAN"]).ToString());
                        //dmHangHoa.TRANGTHAI = bool.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["TRANGTHAI"]).ToString());
                        dmHangHoa.IS_COMBO = bool.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["IS_COMBO"]).ToString());
                        dmHangHoa.IS_INBEP = bool.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["IS_INBEP"]).ToString());
                        dmHangHoa.CHIETKHAU = decimal.Parse(gvListProduct.GetRowCellValue(i, gvListProduct.Columns["CHIETKHAU"]).ToString());
                        string repuest = gvListProduct.GetRowCellValue(i, gvListProduct.Columns["GHICHU"]).ToString();
                        if (repuest == "a")
                            dmHangHoa.GHICHU = "";
                        else
                            dmHangHoa.GHICHU = repuest;

                        if (GH_diachi != "")
                        {
                            dmHangHoa.DIACHIGIAOHANG = GH_diachi;
                            dmHangHoa.TENKH = GH_TenKH;
                            dmHangHoa.DIENTHOAI_KH = GH_Dienthoai;
                        }

                        if (dmHangHoa.IS_INBEP)
                            listDM_HangHoa.Add(dmHangHoa);

                        try
                        {
                            if (dmHangHoa.IS_INBEP)
                            {
                                sSQL = "";
                                sSQL += "Insert into DISPLAY_ORDER (MA_HANGHOA,MA_HOADON, MA_BAN, GIOORDER, SOLUONG,NHANVIEN, YEUCAU,FLAG)";
                                sSQL += "Values (";
                                sSQL += clsMain.SQLString(dmHangHoa.MA_HANGHOA) + ",";
                                sSQL += clsMain.SQLString(sIdBill) + ",";
                                sSQL += clsMain.SQLString("0") + ",";
                                sSQL += "GETDATE()" + ",";
                                sSQL += clsMain.SQLString(dmHangHoa.SOLUONG.ToString()) + ",";
                                sSQL += clsMain.SQLString(clsGlobal.gsUserID) + ",";
                                sSQL += clsMain.SQLStringUnicode(dmHangHoa.GHICHU) + ",";
                                sSQL += clsMain.SQLBit(true) + ")";
                                clsMain.ExecuteSQL(sSQL);
                            }
                        }
                        catch (Exception e)
                        {
 
                        }

                        if (i != 0)
                        {
                            gvListProduct.DeleteRow(i);
                            i--;
                        }
                    }
                }

                gvListProduct.DeleteRow(0);
                convert cvrt = new convert();
                DataTable Orderbill = cvrt.ConvertToDataTable(listDM_HangHoa);

                if (Orderbill.Rows.Count > 0)
                {
                    string print = GetIdKitchen(maBEP);
                    if (print == null)
                    {
                        XtraMessageBox.Show("Chưa chọn máy in", _thongbao, MessageBoxButtons.OK);
                        print = "";
                    }

                    //8/11/2016 căt từng món theo yêu cầu huy việt nam
                    if (cls_KP_RES.Mode == 4)
                    {
                        string sSoKhach = "1";
                        try
                        {
                            DataTable dtKhach = clsMain.ReturnDataTable("select SOLUONGKHACH from HOADON where MA_HOADON=" + clsMain.SQLString(Orderbill.Rows[0]["Ma_HoaDon"].ToString()));
                            sSoKhach = dtKhach.Rows[0][0].ToString();
                        }
                        catch
                        {
                        }
                        DataColumn dctemp = new DataColumn();
                        dctemp.DefaultValue = sSoKhach;
                        dctemp.ColumnName = "sokhach";
                        Orderbill.Columns.Add(dctemp);
                        for (int i = 0; i < Orderbill.Rows.Count; i++)
                        {
                            DataTable dtTmp1 = Orderbill.Copy();
                            dtTmp1.Clear();
                            dtTmp1.Rows.Add(Orderbill.Rows[i].ItemArray);

                            Frm_Report1 frmTmp1 = new Frm_Report1();
                            frmTmp1.ReportName = "rpt_Order_1_Cat1";
                            frmTmp1.DataSource = dtTmp1;
                            frmTmp1.WindowState = FormWindowState.Minimized;
                            frmTmp1.PrinterName = print;
                            frmTmp1.IsPrint = true;
                            frmTmp1.ShowDialog();
                            frmTmp1.Dispose();
                        }
                    }
                    else
                    {
                        Frm_Report1 frm = new Frm_Report1();
                        if (KP_Terminal.clsKP_Terminal.BillSize == "58")
                        {
                            frm.ReportName = "rpt_Order_58";
                        }
                        else
                        {
                            if (cls_KP_RES.Mode == 3 || cls_KP_RES.Mode == 4)
                            {
                                frm.ReportName = "rpt_Order_1";
                            }
                            else
                            {
                                frm.ReportName = "rpt_Order";
                            }
                        }
                        frm.DataSource = Orderbill;
                        frm.WindowState = FormWindowState.Minimized;
                        frm.PrinterName = print;
                        frm.IsPrint = true;
                        frm.ShowDialog();
                        frm.Dispose();
                    }
                }
            }
        }

        private void PrintMaVachFastFood(String sIdBill)
        {
            if (cls_ConfigCashier.sIn_Mavach_Fastfood != "0" & sInTem == true)
            {
                if (cls_ConfigCashier.sIn_Mavach_Fastfood_Giaohang != "0" & GH_TenKH == "")
                {
                    return;
                }
                
                DataView myDV = (DataView)gvListProduct.DataSource;
                DataTable myDT = myDV.ToTable().Copy();
                DataTable myDT1 = myDV.ToTable().Copy();
                myDT = TachOrder();
                myDT1.Clear();

                foreach (DataRow dr in myDT.Rows)
                {
                    myDT1.Rows.Add(dr["MA_HANGHOA"].ToString(), dr["TEN_HANGHOA"].ToString(), dr["SOLUONG"].ToString(), dr["GIABAN"].ToString(), dr["GHICHU"].ToString()
                        , dr["MA_BEP"].ToString(), dr["THUE"].ToString(), dr["Thanh_Tien"].ToString(), Boolean.Parse(dr["TRANGTHAI"].ToString()), Boolean.Parse(dr["IS_COMBO"].ToString())
                        , Boolean.Parse(dr["IS_INBEP"].ToString()), Boolean.Parse(dr["IS_KHUYENMAI"].ToString()), dr["CHIETKHAU"].ToString(), dr["MONTHEM"].ToString()
                        , dr["TEM"].ToString(), dr["MACP"].ToString(), dr["MACB"].ToString());

                }


                myDT.Clear();
                foreach (DataRow dr in myDT1.Rows)
                {
                    if (!dr["TEM"].ToString().Equals("0"))
                        myDT.Rows.Add(dr.ItemArray);
                }

                String nameTable = "";
                String pTem = "";
                int TongSoMon = 0;
                int iMon = 0;
                string sNumber = "";
                foreach (DataRow dr in myDT.Rows)
                {
                    if (dr["MACP"].ToString() == "C" || dr["MACP"].ToString() == "CBC")
                        TongSoMon += int.Parse(dr["SOLUONG"].ToString()); ;
                }

                Frm_Report1 frmTmp1;
                DataTable dt1 = new DataTable();
                dt1.Columns.Add("MA_HOADON");
                dt1.Columns.Add("TEN_HANGHOA");
                dt1.Columns.Add("NGAY_HOADON");
                dt1.Columns.Add("DON_GIA");
                dt1.Columns.Add("YEUCAUTHEM");
                dt1.Columns.Add("BAN");
                dt1.Columns.Add("SOLUONG");
                dt1.Columns.Add("Number");
                dt1.Columns.Add("TENKH");

                if (myDT.Rows.Count == 0)
                    return;
                else
                    pTem = myDT.Rows[0]["TEM"].ToString();
                for (int i = 0; i < myDT.Rows.Count; i++)
                {

                    if (myDT.Rows[i]["MACP"].ToString() == "C" || myDT.Rows[i]["MACP"].ToString() == "CBC")
                    {
                        iMon += 1;
                        sNumber = "(" + Number + ") " + iMon + "/" + TongSoMon;
                    }

                    if (!myDT.Rows[i]["TEM"].ToString().Equals("0"))
                    {
                        if (pTem == myDT.Rows[i]["TEM"].ToString())
                        {
                            dt1.Rows.Add(sIdBill, myDT.Rows[i]["TEN_HANGHOA"].ToString().Trim() + "   x " + myDT.Rows[i]["SOLUONG"].ToString(), cls_KP_RES.DateToString_Full_NoSecond(DateTime.Now), int.Parse(myDT.Rows[i]["GIABAN"].ToString()).ToString("N0"), myDT.Rows[i]["GHICHU"].ToString().Length > 2 ? myDT.Rows[i]["GHICHU"].ToString() : "", nameTable, myDT.Rows[i]["SOLUONG"].ToString(), sNumber, sTenKhach);
                        }
                        else
                        {
                            if (!myDT.Rows[i]["TEM"].ToString().Equals("0"))
                            {
                                do
                                {
                                    frmTmp1 = new Frm_Report1();
                                    frmTmp1.ReportName = "rpt_MaVach_Fastfood";
                                    frmTmp1.DataSource = dt1;
                                    frmTmp1.WindowState = FormWindowState.Minimized;
                                    frmTmp1.PrinterName = KP_Terminal.clsKP_Terminal.Mayinmavach;
                                    frmTmp1.IsPrint = true;
                                    frmTmp1.ShowDialog();
                                    frmTmp1.Dispose();
                                    dt1.Rows[0]["SOLUONG"] = int.Parse(dt1.Rows[0]["SOLUONG"].ToString()) - 1;
                                } while (int.Parse(dt1.Rows[0]["SOLUONG"].ToString()) > 0);
                                dt1.Clear();
                                dt1.Rows.Add(sIdBill, myDT.Rows[i]["TEN_HANGHOA"].ToString().Trim() + "   x " + myDT.Rows[i]["SOLUONG"].ToString(), cls_KP_RES.DateToString_Full_NoSecond(DateTime.Now), int.Parse(myDT.Rows[i]["GIABAN"].ToString()).ToString("N0"), myDT.Rows[i]["GHICHU"].ToString().Length > 2 ? myDT.Rows[i]["GHICHU"].ToString() : "", nameTable, myDT.Rows[i]["SOLUONG"].ToString(), sNumber, sTenKhach );

                                pTem = myDT.Rows[i]["TEM"].ToString();
                            }
                        }
                    }
                    else
                    {
                        dt1.Clear();
                        pTem = "1";
                    }

                }

                if (dt1.Rows.Count > 0)
                {
                    do
                    {
                        frmTmp1 = new Frm_Report1();
                        frmTmp1.ReportName = "rpt_MaVach_Fastfood";
                        frmTmp1.DataSource = dt1;
                        frmTmp1.WindowState = FormWindowState.Minimized;
                        frmTmp1.PrinterName = KP_Terminal.clsKP_Terminal.Mayinmavach;
                        frmTmp1.IsPrint = true;
                        frmTmp1.ShowDialog();
                        frmTmp1.Dispose();
                        dt1.Rows[0]["SOLUONG"] = int.Parse(dt1.Rows[0]["SOLUONG"].ToString()) - 1;
                    } while (int.Parse(dt1.Rows[0]["SOLUONG"].ToString()) > 0);
                }
            }
        }

        private DataTable TachOrder()
        {
            DataTable kq = new DataTable();
            DataView myDV = (DataView)gvListProduct.DataSource;
            DataTable myDT = myDV.ToTable().Copy();
            DataTable myDT1 = myDV.ToTable().Copy();
            myDT1.Clear();
            int iMaCBNext = LayMACBLonNhat(myDT) + 1;
            int iTEMNext = LayTEMLonNhat(myDT) + 1;
            myDT.Columns.Add("DA_DUYET", typeof(System.Int32));
            foreach (DataRow dr in myDT.Rows)
            {
                dr["DA_DUYET"] = 0;
            }
            for (int i = 0; i < myDT.Rows.Count; i++)
            {
                string sMaHH = "";
                int iSoLuong = 0;
                int sMaCB = int.Parse(myDT.Rows[i]["MACB"].ToString());
                int iSoLuongMonChinh = LaySoLuongLonNhat(sMaCB);
                if (bool.Parse(myDT.Rows[i]["INTEM"].ToString()) == true)
                {
                    while (int.Parse(myDT.Rows[i]["SOLUONG"].ToString()) > 1 & myDT.Rows[i]["MACP"].ToString() == "C")
                    {
                        for (int j = 0; j < myDT.Rows.Count; j++)
                        {
                            if (int.Parse(myDT.Rows[j]["MACB"].ToString()) == sMaCB)
                            {
                                if (myDT.Rows[j]["MACP"].ToString() == "C")
                                {
                                    myDT.Rows[i]["SOLUONG"] = int.Parse(myDT.Rows[i]["SOLUONG"].ToString()) - 1;
                                    myDT1.Rows.Add(myDT.Rows[j]["MA_HANGHOA"].ToString(), myDT.Rows[j]["TEN_HANGHOA"].ToString(), 1, myDT.Rows[j]["GIABAN"].ToString(), myDT.Rows[j]["GHICHU"].ToString()
                                    , myDT.Rows[j]["MA_BEP"].ToString(), myDT.Rows[j]["THUE"].ToString(), myDT.Rows[j]["Thanh_Tien"].ToString(), Boolean.Parse(myDT.Rows[j]["TRANGTHAI"].ToString()), Boolean.Parse(myDT.Rows[j]["IS_COMBO"].ToString())
                                    , Boolean.Parse(myDT.Rows[j]["IS_INBEP"].ToString()), Boolean.Parse(myDT.Rows[j]["IS_KHUYENMAI"].ToString()), myDT.Rows[j]["CHIETKHAU"].ToString(), myDT.Rows[j]["MONTHEM"].ToString()
                                    , iTEMNext, myDT.Rows[j]["MACP"].ToString(), "", iMaCBNext, myDT.Rows[j]["INTEM"].ToString());
                                }
                                else
                                {
                                    if (myDT.Rows[j]["DA_DUYET"].ToString() != "1")
                                    {
                                        iSoLuong = int.Parse(myDT.Rows[j]["SOLUONG"].ToString()) / iSoLuongMonChinh;
                                        myDT.Rows[j]["SOLUONG"] = iSoLuong;
                                        myDT.Rows[j]["DA_DUYET"] = 1;
                                    }
                                    else
                                        iSoLuong = int.Parse(myDT.Rows[j]["SOLUONG"].ToString());
                                    myDT1.Rows.Add(myDT.Rows[j]["MA_HANGHOA"].ToString(), myDT.Rows[j]["TEN_HANGHOA"].ToString(), iSoLuong, myDT.Rows[j]["GIABAN"].ToString(), myDT.Rows[j]["GHICHU"].ToString()
                                    , myDT.Rows[j]["MA_BEP"].ToString(), myDT.Rows[j]["THUE"].ToString(), myDT.Rows[j]["Thanh_Tien"].ToString(), Boolean.Parse(myDT.Rows[j]["TRANGTHAI"].ToString()), Boolean.Parse(myDT.Rows[j]["IS_COMBO"].ToString())
                                    , Boolean.Parse(myDT.Rows[j]["IS_INBEP"].ToString()), Boolean.Parse(myDT.Rows[j]["IS_KHUYENMAI"].ToString()), myDT.Rows[j]["CHIETKHAU"].ToString(), myDT.Rows[j]["MONTHEM"].ToString()
                                    , iTEMNext, myDT.Rows[j]["MACP"].ToString(), "", iMaCBNext, myDT.Rows[j]["INTEM"].ToString());
                                }
                            }
                        }
                        iMaCBNext += 1;
                        iTEMNext += 1;
                    }
                }
            }
            myDT.Merge(myDT1);
            //foreach (DataRow dr in myDT1.Rows)
            //    myDT.Rows.Add(dr);
            kq = myDT;
            return kq;
        }

        private int LayMACBLonNhat(DataTable myDT)
        {
            int kq = 1;
            foreach (DataRow dr in myDT.Rows)
            {
                if (int.Parse(dr["MACB"].ToString()) > kq)
                {
                    kq = int.Parse(dr["MACB"].ToString());
                }
            }
            return kq;
        }
        private int LayTEMLonNhat(DataTable myDT)
        {
            int kq = 1;
            foreach (DataRow dr in myDT.Rows)
            {
                if (int.Parse(dr["TEM"].ToString()) > kq)
                {
                    kq = int.Parse(dr["TEM"].ToString());
                }
            }
            return kq;
        }
        private string GetNewCodeBill()
        {
            string _mahoadon = "";
            string ngay = cls_ConfigCashier.idCashier + DateTime.Now.Day.ToString("00") + DateTime.Now.Month.ToString("00") + DateTime.Now.Year.ToString("0000");
            DataTable dtMA = clsMain.ReturnDataTable("select [dbo].[fc_NewcodeHoaDon] ('" + ngay + "')");
            _mahoadon = dtMA.Rows[0][0].ToString();
            return _mahoadon;
        }

        private string GetIdKitchen(string idKitchen)
        {
            string printKitchen = null;
            string sql = "Select MAYINBEP From DM_BEP A,HANGHOA B Where A.MA_BEP=B.MA_BEP AND B.IS_INBEP=1 AND B.MA_BEP =" + clsMain.SQLString(idKitchen);
            DataTable kitchen = clsMain.ReturnDataTable(sql);
            if (kitchen.Rows.Count > 0)
            {
                printKitchen = kitchen.Rows[0]["MAYINBEP"].ToString();
                return printKitchen;
            }
            else
                return printKitchen;
        }

        private void ResetInputMoney()
        {
            bVoucher = bVisaCard = true;
            lbThirdMoney.Tag = lbSecondMoney.Tag = lbFirstMoney.Tag = 0;
            lbFirst.Text = lbFirstMoney.Text = lbSecond.Text = lbSecondMoney.Text = lbThird.Text = lbThirdMoney.Text = string.Empty;
            pnlFirst.Visible = pnlSecond.Visible = pnlThird.Visible = false;

            lbNameInnerCustomer.Text = lbIdInnerCustomerShow.Text = lbMoneySurplus.Text = "";
            pnlInnerCustomerShow.Visible = false;
            txtInputMoney.Text = "0";
            txtInputMoney.SelectionStart = 1;
            txtDearCustomer.Text = txtInnerCustomer.Text = "";
            idVoucher = "";
            dMoneyInnerCard = 0;
            dMoneyVisa = 0;
            dMoneyVoucher = 0;
            dMoneyCash = 0;
        }

        private void InputGetMoneyFocus()
        {
            bText = false;
            txtInput = txtInputMoney;
            pnlVoucher.Visible = false;
            pnlMagiamgia .Visible = false;
            pnlChietKhau.Visible = false;
            pnlTienKhachTra.Visible = true;
            pnlKHTT.Visible = false;
            pnlTTTNB.Visible = false;
            pnlPhiPhuVu.Visible = false;
            pnlPhieuMuaHang.Visible = false;
            txtInput.Text = "0";
            txtInput.SelectionStart = 1;
            txtInput.Focus();
        }
        #endregion

        #region money click
        private void btn500_Click(object sender, EventArgs e)
        {
            PlusInputMoney(500);
        }

        private void btn1000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(1000);
        }

        private void btn2000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(2000);
        }

        private void btn5000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(5000);
        }

        private void btn10000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(10000);
        }

        private void btn20000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(20000);
        }

        private void btn50000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(50000);
        }

        private void btn100000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(100000);
        }

        private void btn200000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(200000);
        }

        private void btn500000_Click(object sender, EventArgs e)
        {
            PlusInputMoney(500000);
        }

        private void btnTranslateMoney_Click(object sender, EventArgs e)
        {
            pnlKeyboardNumber.BringToFront();
            if (txtInput.SelectedText == txtInput.Text)
            {
                txtInput.Focus();
            }
            else
            {
                int iPosition = txtInput.SelectionStart;
                txtInput.Focus();
                txtInput.SelectionStart = txtInput.Text.Length;
                txtInput.SelectionStart = iPosition;
            }
        }

        private void btnMoveLeftMoney_Click(object sender, EventArgs e)
        {
            MoveToLeft();
        }

        private void btnMoveRightMoney_Click(object sender, EventArgs e)
        {
            MoveToRight();
        }

        private void btnDeleteMoney_Click(object sender, EventArgs e)
        {
            DeleteTextBox();
        }

        private void btnOkMoney_Click(object sender, EventArgs e)
        {
            OkTextBox();
        }

        private void btnEcsMoney_Click(object sender, EventArgs e)
        {
            EscapeTextbox();
        }

        private void btnPaymentMoney_Click(object sender, EventArgs e)
        {
            bPrintBill = true;
            PrintBill();
        }

        private void PlusInputMoney(int iMoney)
        {
            if (!bText)
            {
                if (txtInput.Text.Length > 15)
                {
                    int iPositon = txtInput.SelectionStart;
                    XtraMessageBox.Show(_bkdnq15kt, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtInput.SelectionStart = txtInput.Text.Length;
                    txtInput.SelectionStart = iPositon;
                    return;
                }
                if (txtInput.SelectedText == txtInput.Text)
                {
                    txtInput.Text = "0";
                }
                decimal iTotal = decimal.Parse(txtInput.Text.Replace(",", "")) + iMoney;
                txtInput.Text = iTotal.ToString();
                txtInput.SelectionStart = txtInput.Text.Length;
            }
        }
        #endregion

        #region number click
        private void btn000_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("000");
        }

        private void btn00_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("00");
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("0");
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("1");
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("2");
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("3");
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("4");
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("5");
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("6");
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("7");
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("8");
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("9");
        }

        private void btnTranslateNumber_Click(object sender, EventArgs e)
        {
            pnlKeyBoardText.BringToFront();
            txtInput.SelectionStart = txtInput.Text.Length;
            if (txtInput.SelectedText == txtInput.Text)
            {
                txtInput.Focus();
            }
            else
            {
                int iPosition = txtInput.SelectionStart;

                txtInput.Focus();
                txtInput.SelectionStart = iPosition;
            }

        }
       
        private void btnMoveLeftNumber_Click(object sender, EventArgs e)
        {
            MoveToLeft();
        }

        private void btnMoveRightNumber_Click(object sender, EventArgs e)
        {
            MoveToRight();
        }

        private void btnDeleteNumber_Click(object sender, EventArgs e)
        {
            DeleteTextBox();
        }

        private void btnOkNumber_Click(object sender, EventArgs e)
        {
            OkTextBox();
        }

        private void btnEscNumber_Click(object sender, EventArgs e)
        {
            EscapeTextbox();
        }

        private void btnPaymentNumber_Click(object sender, EventArgs e)
        {
            bPrintBill = true;
            PrintBill();

        }

        private void InputKeyboardNumber(string sInput)
        {
            if (txtInput.Text.Length >= 15)
            {
                int iPositon = txtInput.SelectionStart;
                XtraMessageBox.Show(_bkdnq15kt, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtInput.SelectionStart = txtInput.Text.Length;
                txtInput.SelectionStart = iPositon;
                return;
            }
            if (txtInput.Text == "0")
            {
                if (sInput == "0" || sInput == "00" || sInput == "000")
                    return;
            }
            if (txtInput.SelectedText == txtInput.Text)
            {
                txtInput.Text = "0";
            }
            int iLenght, iPosition;
            iLenght = txtInput.Text.Length;
            iPosition = txtInput.SelectionStart;
            if (iPosition == 0)
            {
                if (sInput == "0" || sInput == "00" || sInput == "000")
                    return;
            }
            if (txtInput.Text == "0")
            {
                txtInput.Text = sInput;
            }
            else
            {
                txtInput.Text = txtInput.Text.Insert(iPosition, sInput);
            }
            if (sInput.Length == 1)
            {
                if (txtInput.Text.Length - iLenght == 1)
                {
                    txtInput.SelectionStart = iPosition + 1;
                }
                else
                {
                    txtInput.SelectionStart = iPosition + 2;
                }
            }
            else if (sInput.Length == 2)
            {
                if (txtInput.Text.Length - iLenght == 2)
                {
                    txtInput.SelectionStart = iPosition + 2;
                }
                else
                {
                    txtInput.SelectionStart = iPosition + 3;
                }
            }
            else if (sInput.Length == 3)
            {
                if (txtInput.Text.Length - iLenght == 3)
                {
                    txtInput.SelectionStart = iPosition + 3;
                }
                else
                {
                    txtInput.SelectionStart = iPosition + 4;
                }
            }
        }
        #endregion

        private void lbTotalMoneyAll_TextChanged(object sender, EventArgs e)
        {
            decimal fReplyCustomer = decimal.Parse(lbGetCustomer.Text.Replace(",", "")) - decimal.Parse(lbTotalMoneyAll.Text.Replace(",", ""));
            lbReplyCustomer.Tag = fReplyCustomer;
            if (int.Parse(lbReplyCustomer.Tag.ToString()) > 0)
            {
                lbReplyCustomer.Text = string.Format("{0:#,###0}", lbReplyCustomer.Tag);
            }
            else
            {
                lbReplyCustomer.Text = "0";
            }
        }

        private void lbTotalMoneyPayment_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalMoneyPayment();
        }

        private void lbDiscountPayment_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalMoneyPayment();
            //05/03/2017 them chiet khau man hinh hai
            try
            {
                cls_KP_RES.iChietkhau_Manhinhhai = int.Parse(lbDiscountPayment.Text);
            }
            catch
            {
            }
        }

        private void lbMoneyService_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalMoneyPayment();
        }

        private void lbMoneyGetFirst_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalMoneyPayment();
        }

        private void UpdateTotalMoneyPayment()
        {
            decimal fTotal = decimal.Parse(lbTotalMoneyPayment.Tag.ToString()) + decimal.Parse(lbServiceMoney.Tag.ToString()) - decimal.Parse(lbDiscountPayment.Tag.ToString()) - decimal.Parse(lbMoneyGetFirst.Tag.ToString());
            lbTotalMoneyAll.Tag = fTotal;
            lbTotalMoneyAll.Text = string.Format("{0:#,###0}", fTotal);
        }

        private void lbFirstMoney_TextChanged(object sender, EventArgs e)
        {
            if (!pnlSecond.Visible && !pnlThird.Visible)
                lbGetCustomer.Text = string.Format("{0:#,###0}", lbFirstMoney.Tag);
            else if (pnlSecond.Visible && !pnlThird.Visible)
                lbGetCustomer.Text = string.Format("{0:#,###0}", (decimal.Parse(lbFirstMoney.Tag.ToString()) + decimal.Parse(lbSecondMoney.Tag.ToString())));
            else if (pnlSecond.Visible && pnlThird.Visible)
                lbGetCustomer.Text = string.Format("{0:#,###0}", (decimal.Parse(lbFirstMoney.Tag.ToString()) + decimal.Parse(lbSecondMoney.Tag.ToString())) + decimal.Parse(lbThirdMoney.Tag.ToString()));

            decimal fReplyCustomer = decimal.Parse(lbGetCustomer.Text.Replace(",", "")) - decimal.Parse(lbTotalMoneyAll.Text.Replace(",", ""));
            lbReplyCustomer.Tag = fReplyCustomer;
            if (int.Parse(lbReplyCustomer.Tag.ToString()) > 0)
            {
                lbReplyCustomer.Text = string.Format("{0:#,###0}", lbReplyCustomer.Tag);
            }
            else
            {
                lbReplyCustomer.Text = "0";
            }
        }

        private void lbSecondMoney_TextChanged(object sender, EventArgs e)
        {
            if (!pnlThird.Visible)
                lbGetCustomer.Text = string.Format("{0:#,###0}", (decimal.Parse(lbFirstMoney.Tag.ToString()) + decimal.Parse(lbSecondMoney.Tag.ToString())));
            else
                lbGetCustomer.Text = string.Format("{0:#,###0}", (decimal.Parse(lbFirstMoney.Tag.ToString()) + decimal.Parse(lbSecondMoney.Tag.ToString())) + decimal.Parse(lbThirdMoney.Tag.ToString()));
            decimal fReplyCustomer = decimal.Parse(lbGetCustomer.Text.Replace(",", "")) - decimal.Parse(lbTotalMoneyAll.Text.Replace(",", ""));
            lbReplyCustomer.Tag = fReplyCustomer;
            if (int.Parse(lbReplyCustomer.Tag.ToString()) > 0)
            {
                lbReplyCustomer.Text = string.Format("{0:#,###0}", lbReplyCustomer.Tag);
            }
            else
            {
                lbReplyCustomer.Text = "0";
            }
        }

        private void lbThirdMoney_TextChanged(object sender, EventArgs e)
        {
            lbGetCustomer.Text = string.Format("{0:#,###0}", (decimal.Parse(lbFirstMoney.Tag.ToString()) + decimal.Parse(lbSecondMoney.Tag.ToString())) + decimal.Parse(lbThirdMoney.Tag.ToString()));
            decimal fReplyCustomer = decimal.Parse(lbGetCustomer.Text.Replace(",", "")) - decimal.Parse(lbTotalMoneyAll.Text.Replace(",", ""));
            lbReplyCustomer.Tag = fReplyCustomer;
            if (int.Parse(lbReplyCustomer.Tag.ToString()) > 0)
            {
                lbReplyCustomer.Text = string.Format("{0:#,###0}", lbReplyCustomer.Tag);
            }
            else
            {
                lbReplyCustomer.Text = "0";
            }
        }

        private void lbTheMoney_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalGridview();
        }

        private void lbHourMoney_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalGridview();
        }

        private void UpdateTotalGridview()
        {
            decimal fTotal = decimal.Parse(lbTheMoney.Tag.ToString()) + decimal.Parse(lbHourMoney.Tag.ToString());
            lbTotalMoney.Tag = fTotal;
            lbTotalMoney.Text = string.Format("{0:#,###0}", fTotal);
        }

        private void lbTotalMoney_TextChanged(object sender, EventArgs e)
        {
            lbTotalMoneyPayment.Tag = lbTotalMoney.Tag;
            lbTotalMoneyPayment.Text = lbTotalMoney.Text;
        }

        private void txtServiceMoney_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void txtInnerCustomer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void txtInputMoney_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void txtDearCustomer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void txtInputDiscount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void txtInputVoucher_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void txtInputMagiamgia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void btnCancelPayment_Click(object sender, EventArgs e)
        {
            bHoanTacThanhToan = false;
            this.Close();
            this.Dispose();
        }

        private void btnRetypePayment_Click(object sender, EventArgs e)
        {
            pnlShowInforPayment.BringToFront();
            bText = false;
            CN_Flag = false;

            txtInput = txtInputMoney;
            pnlVoucher.Visible = false;
            pnlMagiamgia.Visible = false;
            pnlChietKhau.Visible = false;
            pnlTienKhachTra.Visible = true;
            pnlKHTT.Visible = false;
            pnlTTTNB.Visible = false;
            pnlPhiPhuVu.Visible = false;
            pnlPhieuMuaHang.Visible = false;
            txtInput.Text = "0";
            txtInput.SelectionStart = 1;
            txtInput.Focus();
            ResetInputMoney();
        }

        private void btnDiscount_Click(object sender, EventArgs e)
        {
            pnlShowInforPayment.BringToFront();
            bText = false;
            txtInput = txtInputDiscount;
            pnlVoucher.Visible = false;
            pnlMagiamgia.Visible = false;
            pnlChietKhau.Visible = true;
            pnlTienKhachTra.Visible = false;
            pnlKHTT.Visible = false;
            pnlTTTNB.Visible = false;
            pnlPhiPhuVu.Visible = false;
            pnlPhieuMuaHang.Visible = false;
            txtInput.Text = "0";
            txtInput.Focus();
            txtInput.SelectionStart = 1;
        }

        private void btnServiceMoney_Click(object sender, EventArgs e)
        {
            pnlShowInforPayment.BringToFront();
            bText = false;
            txtInput = txtServiceMoney;
            pnlVoucher.Visible = false;
            pnlMagiamgia.Visible = false;
            pnlChietKhau.Visible = false;
            pnlTienKhachTra.Visible = false;
            pnlKHTT.Visible = false;
            pnlTTTNB.Visible = false;
            pnlPhiPhuVu.Visible = true;
            pnlPhieuMuaHang.Visible = false;
            txtInput.Text = "0";
            txtInput.Focus();
            txtInput.SelectionStart = 1;
        }

        private void btnNotPrintBill_Click(object sender, EventArgs e)
        {
            bPrintBill = false;
            PrintBill();
        }

        private void btnGiaohang_Click(object sender, EventArgs e)
        {
            if (GH_diachi != "")
            {
                Frm_Giaohang frm = new Frm_Giaohang(GH_MaKH, GH_chk_diachikhac, GH_diachi);
                frm.ShowDialog();
                GH_diachi = frm.sdiachi;
                GH_MaKH = frm.sMaKH;
                GH_TenKH = frm.sTenKH;
                GH_Dienthoai = frm.sDienthoai;
                GH_chk_diachikhac = frm.chkdiachikhac;
                frm.Dispose();
            }
            else
            {
                Frm_Giaohang frm = new Frm_Giaohang();
                frm.ShowDialog();
                GH_diachi = frm.sdiachi;
                GH_MaKH = frm.sMaKH;
                GH_TenKH = frm.sTenKH;
                GH_Dienthoai = frm.sDienthoai;
                GH_chk_diachikhac = frm.chkdiachikhac;
                frm.Dispose();
            }

            if (GH_diachi != "" & cls_ConfigCashier .sGiaban2_Giaohang !="0")
            {
                foreach (DataRow dr in dtGridviewProduct.Rows)
                {
                    string sSQL = "select ISNULL(GIABAN2,0) as GIABAN2 from HANGHOA where MA_HANGHOA = " + clsMain.SQLString(dr["MA_HANGHOA"].ToString ());
                    DataTable dtgiaban = clsMain.ReturnDataTable(sSQL);
                    dr["GIABAN"] = dtgiaban.Rows[0]["GIABAN2"];
                    dr["THANH_TIEN"] = float.Parse(dr["GIABAN"].ToString()) * float.Parse(dr["SOLUONG"].ToString());
                }
                Payment_Load(null, null);
            }
        }

        private void btnKhachhang_Click(object sender, EventArgs e)
        {
            btnGiaohang_Click(null, null);
        }

        private void btnGhichu_Click(object sender, EventArgs e)
        {
            Frm_GhiChu frm = new Frm_GhiChu(GhiChu);
            frm.ShowDialog();
            GhiChu = frm.sGhiChu;
            frm.Dispose();
        }

        private void btnNhanvienthuchien_Click(object sender, EventArgs e)
        {
            Frm_NhanVienPayment frm = new Frm_NhanVienPayment(dtGridviewProduct);
            frm.ShowDialog();
            dtGridviewProduct = frm.dtGridviewProduct;
            frm.Dispose();
        }

        private void btnMakhuyenmai_Click(object sender, EventArgs e)
        {
            pnlShowInforPayment.BringToFront();
            bText = true;

            txtInput = txtInputMagiamgia;
            pnlMagiamgia.Visible = true;
            pnlVoucher.Visible = false;
            pnlChietKhau.Visible = false;
            pnlTienKhachTra.Visible = false;
            pnlKHTT.Visible = false;
            pnlTTTNB.Visible = false;
            pnlPhiPhuVu.Visible = false;
            pnlPhieuMuaHang.Visible = false;
            txtInput.Focus();
            txtInput.SelectionStart = 1;
            txtInput.SelectAll();
        }

        private void btnVoucher_Click(object sender, EventArgs e)
        {
            if (bVoucher)
            {
                pnlShowInforPayment.BringToFront();
                bText = true;

                txtInput = txtInputVoucher;
                pnlVoucher.Visible = true;
                pnlMagiamgia.Visible = false ;
                pnlChietKhau.Visible = false;
                pnlTienKhachTra.Visible = false;
                pnlKHTT.Visible = false;
                pnlTTTNB.Visible = false;
                pnlPhiPhuVu.Visible = false;
                pnlPhieuMuaHang.Visible = false;
                txtInput.Focus();
                txtInput.SelectionStart = 1;
            }
        }

        private void btnCardInternalPayment_Click(object sender, EventArgs e)
        {
            if (!clsUserManagement.CheckActive("8"))
            {
                XtraMessageBox.Show(_bkdcnkhttnb, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            decimal dMustGetMoney = decimal.Parse(lbTotalMoneyAll.Tag.ToString()) - decimal.Parse(lbGetCustomer.Text.Replace(",", ""));
            if (dMustGetMoney <= 0)
                return;

            pnlShowInforPayment.BringToFront();
            bText = true;
            txtInput = txtInnerCustomer;
            pnlVoucher.Visible = false;
            pnlMagiamgia.Visible = false;
            pnlChietKhau.Visible = false;
            pnlTienKhachTra.Visible = false;
            pnlKHTT.Visible = false;
            pnlTTTNB.Visible = true;
            pnlPhiPhuVu.Visible = false;
            pnlPhieuMuaHang.Visible = false;
            txtInput.Text = lbIdInnerCustomerShow.Text;
            txtInput.Focus();
            txtInput.SelectionStart = txtInput.Text.Length;

            if (cls_ConfigCashier.sKHOANHAPTHE == "1")
            {
                AutoClosingMessageBox.Show("Quét mã thẻ khách hàng", "Thông báo", 500);
                string sMathe = "";
                for (int i = 0; i < 5; i++)
                {
                    if (sMathe.Trim() == "")
                    {
                        sMathe = clsKP_Terminal.ReadDataCOM2();
                    }
                    else
                    {
                        break;
                    }
                }
                txtInnerCustomer.Text = sMathe.Trim();
                txtInnerCustomer.Properties.ReadOnly = true;
                btnOkText_Click(null, null);
            }
        }

        private void btnCardImtimateCustomer_Click(object sender, EventArgs e)
        {
            if (!clsUserManagement.CheckActive("4"))
            {
                XtraMessageBox.Show(_bkdcnkhtt, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            pnlShowInforPayment.BringToFront();
            bText = true;
            txtInput = txtDearCustomer;
            pnlVoucher.Visible = false;
            pnlMagiamgia.Visible = false;
            pnlChietKhau.Visible = false;
            pnlTienKhachTra.Visible = false;
            pnlKHTT.Visible = true;
            pnlTTTNB.Visible = false;
            pnlPhiPhuVu.Visible = false;
            pnlPhieuMuaHang.Visible = false;
            txtInput.Text = lbIDDearCustomerShow.Text;
            txtInput.Focus();
            txtInput.SelectionStart = txtInput.Text.Length;
            
            if (cls_ConfigCashier.sKHOANHAPTHE == "1")
            {
                AutoClosingMessageBox.Show("Quét mã thẻ khách hàng", "Thông báo", 1000);
                string sMathe = "";
                for (int i = 0; i < 5; i++)
                {
                    if (sMathe.Trim() == "")
                    {
                        sMathe = clsKP_Terminal.ReadDataCOM2();
                    }
                    else
                    {
                        break;
                    }
                }
                txtDearCustomer.Text = sMathe.Trim();
                txtDearCustomer.Properties.ReadOnly = true;
                btnOkText_Click(null, null);
            }
        }

        private void btnCongNo_Click(object sender, EventArgs e)
        {
            txtCNMaKH.GotFocus += new EventHandler(txtCNMaKH_GotFocus);
            txtCNSoDT.GotFocus += new EventHandler(txtCNSoDT_GotFocus);
            txtCongNo_Tien.GotFocus += new EventHandler(txtCongNo_Tien_GotFocus);

            if (CN_Flag)
            {
                if (pnlFirst.Visible && lbFirst.Text.Contains("Công nợ:"))
                {
                    lbFirstMoney.Tag = 0;
                    lbFirstMoney.Text = string.Format("{0:#,###0}", 0) + "  VND";
                    pnlFirst.Visible = false;
                }
                else if (pnlSecond.Visible && lbSecond.Text.Contains("Công nợ:"))
                {
                    lbSecondMoney.Tag = 0;
                    lbSecondMoney.Text = string.Format("{0:#,###0}", 0) + "  VND";
                    pnlSecond.Visible = false;
                }
                else if (pnlThird.Visible && lbThird.Text.Contains("Công nợ:"))
                {
                    lbThirdMoney.Tag = 0;
                    lbThirdMoney.Text = string.Format("{0:#,###0}", 0) + "  VND";
                    pnlThird.Visible = false;
                }

                pnlCongNo.BringToFront();
                txtCNMaKH.Text = CN_MaKH;
                txtCongNo_Tien.Enabled = true;
                txtCongNo_Tien.Text = (decimal.Parse(lbTotalMoneyAll.Text) - decimal.Parse(lbGetCustomer.Text)).ToString();
                dtpNgayHenTra.EditValue = CN_ngayHenTra;
            }
            else
            {
                if (decimal.Parse(lbTotalMoneyAll.Text) <= decimal.Parse(lbGetCustomer.Text))
                {
                    XtraMessageBox.Show("Số tiền trả cho hóa đơn đã đủ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                pnlCongNo.BringToFront();
                ResetControlValue(pnlCongNo);
                txtCongNo_Tien.Enabled = false;
                dtpNgayHenTra.EditValue = DateTime.Now;
            }
            palCongNo_Noidung.Left = pnlCongNo.Width / 2 - palCongNo_Noidung.Width / 2;
        }

        private void btnCongNoSearch_Click(object sender, EventArgs e)
        {
            if (txtCNMaKH.Text != "" || txtCNSoDT.Text != "")
            {
                string sSQL = "";
                sSQL += "Select MA,ID,TEN,HANMUC_CONGNO" + "\n";
                sSQL += "From NHACUNGCAP" + "\n";
                sSQL += "Where (ISNULL(HANMUC_CONGNO,0)>0 OR ISNULL(TIENDATCOC,0)>0)" + "\n";
                if (txtCNMaKH.Text.Trim() != "")
                {
                    sSQL += "AND ID=" + clsMain.SQLString(txtCNMaKH.Text.Trim());
                }
                if (txtCNSoDT.Text.Trim() != "")
                {
                    sSQL += "AND DIENTHOAI=" + clsMain.SQLString(txtCNSoDT.Text.Trim());
                }
                DataTable dtCN = clsMain.ReturnDataTable(sSQL);
                if (dtCN.Rows.Count > 0)
                {
                    txtCongNo_Tien.Enabled = true;
                    txtCongNo_Tien.Text = (decimal.Parse(lbTotalMoneyAll.Text) - decimal.Parse(lbGetCustomer.Text)).ToString();
                    txtCongNo_Tien.Focus();
                    txtCongNo_Tien.SelectAll();
                }
                else
                    txtCongNo_Tien.Enabled = false;
            }
        }

        private void btnHuyCongNo_Click(object sender, EventArgs e)
        {
            pnlShowInforPayment.BringToFront();
            CN_Flag = false;
            ResetControlValue(pnlCongNo);
            InputGetMoneyFocus();
        }

        private void btnCongNoXacNhan_Click(object sender, EventArgs e)
        {
            try
            {
                if (DateTime.Compare(DateTime.Parse(DateTime.Now.ToShortDateString()), DateTime.Parse(DateTime.Parse(dtpNgayHenTra.EditValue.ToString()).ToShortDateString())) > 0)
                {
                    XtraMessageBox.Show("Ngày hẹn trả không được nhỏ hơn ngày hiện tại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (decimal.Parse(lbTotalMoneyAll.Text) <= decimal.Parse(lbGetCustomer.Text))
                {
                    XtraMessageBox.Show("Số tiền trả cho hóa đơn đã đủ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (txtCongNo_Tien.Text == "0")
                {
                    XtraMessageBox.Show("Chưa nhập số tiền", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                string sSQL = "";
                sSQL += "Select MA,ID,TEN,ISNULL(HANMUC_CONGNO,0)AS HANMUC_CONGNO,ISNULL(TIEN_CONGNO,0) AS TIEN_CONGNO,";
                sSQL += "ISNULL(TIENDATCOC,0)AS TIENDATCOC,ISNULL(ISDATCOC,0) AS ISDATCOC" + "\n";
                sSQL += "From NHACUNGCAP" + "\n";
                sSQL += "Where (ISNULL(HANMUC_CONGNO,0)>0 OR ISNULL(TIENDATCOC,0)>0)" + "\n";
                if (txtCNMaKH.Text.Trim() != "")
                {
                    sSQL += "AND ID=" + clsMain.SQLString(txtCNMaKH.Text.Trim());
                }
                if (txtCNSoDT.Text.Trim() != "")
                {
                    sSQL += "AND DIENTHOAI=" + clsMain.SQLString(txtCNSoDT.Text.Trim());
                }
                DataTable dtCN = clsMain.ReturnDataTable(sSQL);
                if (dtCN.Rows.Count > 0)
                {
                    if (!checkHanThanhToan(txtCNMaKH.Text.Trim(), txtCNSoDT.Text.Trim()))
                        return;

                    //xét điều kiện theo hạn mức công nợ
                    if (!bool.Parse(dtCN.Rows[0]["ISDATCOC"].ToString()))
                    {
                        if (float.Parse(dtCN.Rows[0]["HANMUC_CONGNO"].ToString()) >= (float.Parse(dtCN.Rows[0]["TIEN_CONGNO"].ToString()) + float.Parse(txtCongNo_Tien.Text)))
                        {
                            CN_Flag = true;
                            CN_MaKH = dtCN.Rows[0]["MA"].ToString();
                            if (decimal.Parse(txtCongNo_Tien.Text) > decimal.Parse(lbTotalMoneyAll.Text) - decimal.Parse(lbGetCustomer.Text))
                                CN_SoTienTra = decimal.Parse(lbTotalMoneyAll.Text) - decimal.Parse(lbGetCustomer.Text);
                            else
                                CN_SoTienTra = decimal.Parse(txtCongNo_Tien.Text);
                            CN_ngayHenTra = DateTime.Parse(dtpNgayHenTra.EditValue.ToString());
                            pnlShowInforPayment.BringToFront();

                            if (!pnlFirst.Visible)
                            {
                                lbFirst.Text = "Công nợ: " + string.Format("{0:#,###0}", CN_SoTienTra) + " VND -->";
                                lbFirstMoney.Tag = CN_SoTienTra;
                                lbFirstMoney.Text = string.Format("{0:#,###0}", CN_SoTienTra) + "  VND";
                                pnlFirst.Visible = true;
                            }
                            else if (!pnlSecond.Visible)
                            {
                                lbSecond.Text = "Công nợ: " + string.Format("{0:#,###0}", CN_SoTienTra) + " VND -->";
                                lbSecondMoney.Tag = CN_SoTienTra;
                                lbSecondMoney.Text = string.Format("{0:#,###0}", CN_SoTienTra) + "  VND";
                                pnlSecond.Visible = true;
                            }
                            else if (!pnlThird.Visible)
                            {
                                lbThird.Text = "Công nợ: " + string.Format("{0:#,###0}", CN_SoTienTra) + " VND -->";
                                lbThirdMoney.Tag = CN_SoTienTra;
                                lbThirdMoney.Text = string.Format("{0:#,###0}", CN_SoTienTra) + "  VND";
                                pnlThird.Visible = true;
                            }
                            else
                            {
                                XtraMessageBox.Show(_ttkdnq3l, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ResetInputMoney();
                            }

                            InputGetMoneyFocus();
                        }
                        else
                        {
                            XtraMessageBox.Show("Đã vượt hạn mức công nợ cho phép", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    else  //xét điều kiện theo tiền đặt cộc
                    {
                        if (float.Parse(dtCN.Rows[0]["TIENDATCOC"].ToString()) >= (float.Parse(dtCN.Rows[0]["TIEN_CONGNO"].ToString()) + float.Parse(txtCongNo_Tien.Text)))
                        {
                            CN_Flag = true;
                            CN_MaKH = dtCN.Rows[0]["MA"].ToString();
                            if (decimal.Parse(txtCongNo_Tien.Text) > decimal.Parse(lbTotalMoneyAll.Text) - decimal.Parse(lbGetCustomer.Text))
                                CN_SoTienTra = decimal.Parse(lbTotalMoneyAll.Text) - decimal.Parse(lbGetCustomer.Text);
                            else
                                CN_SoTienTra = decimal.Parse(txtCongNo_Tien.Text);
                            CN_ngayHenTra = DateTime.Parse(dtpNgayHenTra.EditValue.ToString());
                            pnlShowInforPayment.BringToFront();

                            if (!pnlFirst.Visible)
                            {
                                lbFirst.Text = "Công nợ: " + string.Format("{0:#,###0}", CN_SoTienTra) + " VND -->";
                                lbFirstMoney.Tag = CN_SoTienTra;
                                lbFirstMoney.Text = string.Format("{0:#,###0}", CN_SoTienTra) + "  VND";
                                pnlFirst.Visible = true;
                            }
                            else if (!pnlSecond.Visible)
                            {
                                lbSecond.Text = "Công nợ: " + string.Format("{0:#,###0}", CN_SoTienTra) + " VND -->";
                                lbSecondMoney.Tag = CN_SoTienTra;
                                lbSecondMoney.Text = string.Format("{0:#,###0}", CN_SoTienTra) + "  VND";
                                pnlSecond.Visible = true;
                            }
                            else if (!pnlThird.Visible)
                            {
                                lbThird.Text = "Công nợ: " + string.Format("{0:#,###0}", CN_SoTienTra) + " VND -->";
                                lbThirdMoney.Tag = CN_SoTienTra;
                                lbThirdMoney.Text = string.Format("{0:#,###0}", CN_SoTienTra) + "  VND";
                                pnlThird.Visible = true;
                            }
                            else
                            {
                                XtraMessageBox.Show(_ttkdnq3l, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ResetInputMoney();
                            }

                            InputGetMoneyFocus();
                        }
                        else
                        {
                            XtraMessageBox.Show("Đã vượt quá số tiền đặt cọc", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                }
                else
                {
                    XtraMessageBox.Show("Không hợp lệ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString(), "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool checkHanThanhToan(string _id, string _sdt)
        {
            string sSQL = "";
            sSQL += "Select MA,ID,TEN,ISNULL(HANMUC_CONGNO,0)AS HANMUC_CONGNO,ISNULL(TIEN_CONGNO,0) AS TIEN_CONGNO,";
            sSQL += "ISNULL(TIENDATCOC,0)AS TIENDATCOC,ISNULL(ISDATCOC,0) AS ISDATCOC,ISNULL(HANTHANHTOAN,0) as HANTHANHTOAN" + "\n";
            sSQL += "From NHACUNGCAP" + "\n";
            sSQL += "Where (ISNULL(HANMUC_CONGNO,0)>0 OR ISNULL(TIENDATCOC,0)>0)" + "\n";
            if (_id != "")
            {
                sSQL += "AND ID=" + clsMain.SQLString(_id);
            }
            if (_sdt != "")
            {
                sSQL += "AND DIENTHOAI=" + clsMain.SQLString(_sdt);
            }
            DataTable dtCN = clsMain.ReturnDataTable(sSQL);
            if (dtCN.Rows.Count > 0)
            {
                sSQL = "";
                sSQL += "Select DISTINCT TOP(1) NGAYLAPPHIEU " + "\n";
                sSQL += "FROM CN_CONGNO A,CHITIETTHANHTOAN B" + "\n";
                sSQL += "WHERE A.MA_HOADON=B.MA_HOADON AND SOTIENCONLAI>0" + "\n";
                sSQL += "AND MADOITUONG=" + clsMain.SQLString(dtCN.Rows[0]["MA"].ToString()) + "\n";
                sSQL += "ORDER BY NGAYLAPPHIEU ASC";
                DataTable dt1 = clsMain.ReturnDataTable(sSQL);
                if (dt1.Rows.Count > 0)
                {
                    if ((DateTime.Now - DateTime.Parse(dt1.Rows[0]["NGAYLAPPHIEU"].ToString())).TotalDays + 1 > double.Parse(dtCN.Rows[0]["HANTHANHTOAN"].ToString()))
                    {
                        XtraMessageBox.Show("Khách hàng đã quá hạn thanh toán", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                    else
                        return true;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                XtraMessageBox.Show("Không hợp lệ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }

        public static void ResetControlValue(Control parent)
        {

            foreach (Control c in parent.Controls)
            {
                if (c.Controls.Count > 0)
                {
                    switch (c.GetType().ToString())
                    {
                        case "DevExpress.XtraEditors.TextEdit":
                            ((TextEdit)c).Text = "";
                            break;
                        case "DevExpress.XtraEditors.DateEdit":
                            ((DateEdit)c).EditValue = clsMain.GetServerDate();
                            break;
                        case "DevExpress.XtraEditors.CheckEdit":
                            ((CheckEdit)c).Checked = true;
                            break;
                        case "DevExpress.XtraEditors.LookupEdit":
                            ((LookUpEdit)(c)).EditValue = ((LookUpEdit)c).Properties.GetDataSourceValue(((LookUpEdit)c).Properties.ValueMember, 0);
                            break;
                        case "DevExpress.XtraEditors.GridLookUpEdit":
                            ((GridLookUpEdit)(c)).EditValue = ((GridLookUpEdit)c).Properties.GetKeyValue(0);
                            break;
                        default:
                            ResetControlValue(c);
                            break;
                    }
                }
                else
                {
                    switch (c.GetType().ToString())
                    {
                        case "DevExpress.XtraEditors.CheckEdit":
                            ((CheckEdit)c).Checked = true;
                            break;

                        default:
                            break;
                    }
                }
            }
        }

        private void btnVisa_Click(object sender, EventArgs e)
        {
           
            txtIdCardVisa.GotFocus += new EventHandler(txtIdCardVisa_GotFocus);
            txtMoneyVisa.GotFocus += new EventHandler(txtMoneyVisa_GotFocus);
            txtNameVisa.GotFocus += new EventHandler(txtNameVisa_GotFocus);
            txtAddress.GotFocus += new EventHandler(txtAddress_GotFocus);
            txtCwCvc.GotFocus += new EventHandler(txtCwCvc_GotFocus);
            txtCity.GotFocus += new EventHandler(txtCity_GotFocus);
            txtIdCodeInput.GotFocus += new EventHandler(txtIdCodeInput_GotFocus);
            txtMonth.GotFocus += new EventHandler(txtMonth_GotFocus);
            txtYear.GotFocus += new EventHandler(txtYear_GotFocus);
            txtCountry.GotFocus += new EventHandler(txtCountry_GotFocus);

            if (bVisaCard)
            {
                pnlVisa.BringToFront();
                ResetControlValue(pnlVisa);
                if (txtIdCodeCreate.Text == "")
                {
                    txtIdCodeCreate.Text = CreateIdCode();
                }
                //10/05/2017 MAC DINH SO TIEN THANH TOAN
                txtMoneyVisa.Text = lbTotalMoneyAll.Text;
                txtMonth.Text = DateTime.Now.Day.ToString("00");
                txtYear.Text = DateTime.Now.Month.ToString("00");
                txtIdCardVisa.Text = "0000-0000-0000-0000";
            }
        }

        private void bntCancelVisa_Click(object sender, EventArgs e)
        {
            pnlShowInforPayment.BringToFront();
            ResetControlValue(pnlVisa);
            InputGetMoneyFocus();
        }

        private void btnRefeshIdCode_Click(object sender, EventArgs e)
        {

            txtIdCodeCreate.Text = CreateIdCode();
        }

        private bool CheckInputVisa()
        {
            if (txtIdCardVisa.Text == "")
            {
                XtraMessageBox.Show(_banchuanhapsothe, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtIdCardVisa.Focus();
                return false;
            }
            if (txtMoneyVisa.Text == "0")
            {
                XtraMessageBox.Show(_banchuanhapsotien, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMoneyVisa.Focus();
                return false;
            }
            return true;

        }

        private void btnPaymentVisa_Click(object sender, EventArgs e)
        {
            if (!CheckInputVisa())
            {
                return;
            }
            if (txtIdCodeInput.Text != "")
            {
                if (txtIdCodeInput.Text != txtIdCodeCreate.Text)
                {
                    XtraMessageBox.Show(_maantoankhongdung, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtIdCodeInput.Text = "";
                    txtIdCodeInput.Focus();
                    return;
                }
            }

            dMoneyVisa = decimal.Parse(txtMoneyVisa.Text.Replace(",", ""));

            if (!pnlFirst.Visible)
            {

                lbFirst.Text = _tienthevisa + string.Format("{0:#,###0}", dMoneyVisa) + " VND -->";
                lbFirstMoney.Tag = dMoneyVisa;
                lbFirstMoney.Text = string.Format("{0:#,###0}", dMoneyVisa) + "  VND";
                pnlFirst.Visible = true;
            }
            else
                if (!pnlSecond.Visible)
                {
                    lbSecond.Text = _tienthevisa + string.Format("{0:#,###0}", dMoneyVisa) + " VND -->";
                    lbSecondMoney.Tag = dMoneyVisa;
                    lbSecondMoney.Text = string.Format("{0:#,###0}", dMoneyVisa) + "  VND";
                    pnlSecond.Visible = true;

                }
                else
                    if (!pnlThird.Visible)
                    {
                        lbThird.Text = _tienthevisa + string.Format("{0:#,###0}", dMoneyVisa) + " VND -->";
                        lbThirdMoney.Tag = dMoneyVisa;
                        lbThirdMoney.Text = string.Format("{0:#,###0}", dMoneyVisa) + "  VND";
                        pnlThird.Visible = true;
                    }
                    else
                    {
                        XtraMessageBox.Show(_ttkdnq3l, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ResetInputMoney();
                    }
            soThe = txtIdCardVisa.Text;
            ngayThe = txtMonth.Text + " - " + txtYear.Text;

            InputGetMoneyFocus();
            pnlShowInforPayment.BringToFront();
            ResetControlValue(pnlVisa);
            InputGetMoneyFocus();
            bVisaCard = false;
        }

        private void txtIdCardVisa_GotFocus(object sender, EventArgs e)
        {
            bText = false;
            txtInput = txtIdCardVisa;
        }

        private void txtMoneyVisa_GotFocus(object sender, EventArgs e)
        {
            bText = false;
            txtInput = txtMoneyVisa;
        }

        private void txtMonth_GotFocus(object sender, EventArgs e)
        {
            bText = false;
            txtInput = txtMonth;
        }

        private void txtYear_GotFocus(object sender, EventArgs e)
        {
            bText = false;
            txtInput = txtYear;
        }

        private void txtNameVisa_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtNameVisa;
        }

        private void txtAddress_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtAddress;
        }

        private void txtCwCvc_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtCwCvc;
        }

        private void txtCity_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtCity;
        }

        private void txtIdCodeInput_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtIdCodeInput;
        }

        private void txtCountry_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtCountry;
        }

        private void txtCNSoDT_GotFocus(object sender, EventArgs e)
        {
            bText = false;
            txtInput = txtCNSoDT;
        }

        private void txtCNMaKH_GotFocus(object sender, EventArgs e)
        {
            bText = true;
            txtInput = txtCNMaKH;
        }

        private void txtCongNo_Tien_GotFocus(object sender, EventArgs e)
        {
            bText = false;
            txtInput = txtCongNo_Tien;
        }

        private string CreateIdCode()
        {
            string sRandom = "";
            int[] sRandomArr = new int[70];
            int iCount = 0;
            for (int i = 48; i <= 57; i++)
            {
                sRandomArr[iCount] = i;
                iCount++;
            }
            for (int i = 65; i <= 90; i++)
            {
                sRandomArr[iCount] = i;
                iCount++;
            }
            for (int i = 97; i <= 122; i++)
            {
                sRandomArr[iCount] = i;
                iCount++;
            }
            Random rd = new Random();
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);
            sRandom += Convert.ToString((char)sRandomArr[rd.Next(0, iCount - 1)]);

            return sRandom;

        }
        private bool KiemTraCot(DataTable _dt)
        {
            bool kq = false;
            foreach (DataRow dr in _dt.Rows)
            {
                if (_dt.Columns.Contains("MONTHEM"))
                {
                    kq = true;
                    break;
                }
            }
            return kq;
        }

        bool tachBill = false;
        DataTable dt_ban_thanhtoan;
        DataTable dt_ban_new;
        private void btnPaymentTachBill_Click(object sender, EventArgs e)
        {
            decimal fTotalMoney = 0;
            Frm_TachBill frm = new Frm_TachBill();
            frm.dt_ban_old = dtGridviewProduct;
            frm.ShowDialog();
            dt_ban_thanhtoan = frm.dt_ban_thanhtoan;
            dt_ban_new = frm.dt_ban_new;
            frm.Dispose();
            if (dt_ban_thanhtoan!= null)
            {
                List<DM_HangHoa> lisHangHoa = new List<DM_HangHoa>();
                for (int i = 0; i < dt_ban_thanhtoan.Rows.Count; i++)
                {
                    DM_HangHoa dmHangHoa = new DM_HangHoa();
                    dmHangHoa.MA_HANGHOA = dt_ban_thanhtoan.Rows[i]["__MA_HANGHOA"].ToString();
                    dmHangHoa.TEN_HANGHOA = dt_ban_thanhtoan.Rows[i]["__TEN_HANGHOA"].ToString();
                    dmHangHoa.SOLUONG = decimal.Parse(dt_ban_thanhtoan.Rows[i]["__SOLUONG"].ToString());
                    dmHangHoa.GIABAN = decimal.Parse(dt_ban_thanhtoan.Rows[i]["__GIABAN"].ToString());
                    dmHangHoa.Thanh_Tien = decimal.Parse(dt_ban_thanhtoan.Rows[i]["__Thanh_Tien"].ToString());
                    //dmHangHoa.GHICHU = dt_ban_thanhtoan.Rows[i]["__GHICHU"].ToString();
                    dmHangHoa.IS_COMBO = bool.Parse(dt_ban_thanhtoan.Rows[i]["__IS_COMBO"].ToString());
                    lisHangHoa.Add(dmHangHoa);
                }
                convert cvrt = new convert();
                dtGridviewProduct = cvrt.ConvertToDataTable(lisHangHoa);
                gcListProduct.DataSource = dtGridviewProduct;


                if (dtGridviewProduct.Rows.Count > 0)
                {
                    for (int i = 0; i < dtGridviewProduct.Rows.Count; i++)
                    {
                        fTotalMoney += decimal.Parse(dtGridviewProduct.Rows[i]["Thanh_Tien"].ToString());
                    }
                }
                if (dtGridviewProduct.Rows.Count >= 7)
                {
                    pnUpDownGridview.Visible = true;
                }
                lbTheMoney.Tag = fTotalMoney;
                lbTheMoney.Text = string.Format("{0:#,###0}", fTotalMoney);
                lbHourMoney.Tag = _dMoneyHour;
                lbHourMoney.Text = string.Format("{0:#,###0}", _dMoneyHour);
                lbMoneyGetFirst.Tag = _dMoneygetFirst;
                lbMoneyGetFirst.Text = string.Format("{0:#,###0}", _dMoneygetFirst);
                lbReplyCustomer.Text = "0";

                pnlKeyboardMoney.BringToFront();
                pnlShowInforPayment.BringToFront();
                txtInput = txtInputMoney;
                txtInput.KeyPress += new KeyPressEventHandler(txtInput_KeyPress);
                txtInput.Focus();
                txtInput.Text = lbTotalMoneyAll.Text;
                txtInput.SelectAll();
                tachBill = true;
            }
            
        }

        private void btnPhieuMuaHang_Click(object sender, EventArgs e)
        {
            pnlShowInforPayment.BringToFront();
            bText = false;
            txtInput = txtNhapPhieuMuaHang;
            pnlVoucher.Visible = false;
            pnlMagiamgia.Visible = false;
            pnlChietKhau.Visible = false;
            pnlTienKhachTra.Visible = false;
            pnlKHTT.Visible = false;
            pnlTTTNB.Visible = false;
            pnlPhiPhuVu.Visible = false;
            pnlPhieuMuaHang.Visible = true;
            txtInput.Text = "0";
            txtInput.Focus();
            txtInput.SelectionStart = 1;
        }

        private void txtNhapPhieuMuaHang_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                OkTextBox();
            }
        }

        private void lbPhieuMuaHangPayment_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalMoneyPayment();
        }

        private int LaySoLuongLonNhat(int iMACB)
        {
            int kq = 1;
            foreach (DataRow dr in dtGridviewProduct.Rows)
            {
                if (int.Parse(dr["MACB"].ToString()) == iMACB & dr["MACP"].ToString() == "C")
                {
                    kq = int.Parse(dr["SOLUONG"].ToString());
                }
            }
            return kq;
        }
     
    }
}