﻿namespace KP_RES
{
    partial class Frm_Danhsachban
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Danhsachban));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.pnShowHang = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.pnTable_Mon = new DevExpress.XtraEditors.XtraScrollableControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.lbPageTable = new DevExpress.XtraEditors.LabelControl();
            this.btn_NextRight = new DevExpress.XtraEditors.SimpleButton();
            this.btn_NextToLeft = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl37 = new DevExpress.XtraEditors.PanelControl();
            this.lbKhuVuc = new DevExpress.XtraEditors.LabelControl();
            this.lbBan = new DevExpress.XtraEditors.LabelControl();
            this.lbMon = new DevExpress.XtraEditors.LabelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pnKV_Mon = new DevExpress.XtraEditors.PanelControl();
            this.panelControl28 = new DevExpress.XtraEditors.PanelControl();
            this.btn_UpKV = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl27 = new DevExpress.XtraEditors.PanelControl();
            this.btn_DowKV = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnShowHang)).BeginInit();
            this.pnShowHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl37)).BeginInit();
            this.panelControl37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnKV_Mon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl28)).BeginInit();
            this.panelControl28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl27)).BeginInit();
            this.panelControl27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.pnShowHang);
            this.groupControl1.Controls.Add(this.panelControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupControl1.Size = new System.Drawing.Size(1000, 600);
            this.groupControl1.TabIndex = 7;
            this.groupControl1.Text = "Sơ đồ bàn";
            // 
            // pnShowHang
            // 
            this.pnShowHang.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnShowHang.Controls.Add(this.panelControl2);
            this.pnShowHang.Controls.Add(this.panelControl4);
            this.pnShowHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnShowHang.Location = new System.Drawing.Point(2, 27);
            this.pnShowHang.Name = "pnShowHang";
            this.pnShowHang.Size = new System.Drawing.Size(996, 526);
            this.pnShowHang.TabIndex = 2;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.pnTable_Mon);
            this.panelControl2.Controls.Add(this.panelControl9);
            this.panelControl2.Controls.Add(this.panelControl7);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(861, 526);
            this.panelControl2.TabIndex = 5;
            // 
            // pnTable_Mon
            // 
            this.pnTable_Mon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnTable_Mon.Location = new System.Drawing.Point(2, 47);
            this.pnTable_Mon.Name = "pnTable_Mon";
            this.pnTable_Mon.Size = new System.Drawing.Size(857, 432);
            this.pnTable_Mon.TabIndex = 2;
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.labelControl3);
            this.panelControl9.Controls.Add(this.labelControl4);
            this.panelControl9.Controls.Add(this.lbPageTable);
            this.panelControl9.Controls.Add(this.btn_NextRight);
            this.panelControl9.Controls.Add(this.btn_NextToLeft);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl9.Location = new System.Drawing.Point(2, 479);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(857, 45);
            this.panelControl9.TabIndex = 1;
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl3.Location = new System.Drawing.Point(423, 17);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(6, 13);
            this.labelControl3.TabIndex = 10;
            this.labelControl3.Text = "/";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl4.Location = new System.Drawing.Point(441, 17);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(14, 13);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "20";
            // 
            // lbPageTable
            // 
            this.lbPageTable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lbPageTable.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbPageTable.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbPageTable.Location = new System.Drawing.Point(404, 17);
            this.lbPageTable.Name = "lbPageTable";
            this.lbPageTable.Size = new System.Drawing.Size(7, 13);
            this.lbPageTable.TabIndex = 8;
            this.lbPageTable.Text = "1";
            // 
            // btn_NextRight
            // 
            this.btn_NextRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_NextRight.Image = global::KP_RES.Properties.Resources.up33_26;
            this.btn_NextRight.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_NextRight.Location = new System.Drawing.Point(724, 2);
            this.btn_NextRight.Name = "btn_NextRight";
            this.btn_NextRight.Size = new System.Drawing.Size(131, 41);
            this.btn_NextRight.TabIndex = 1;
            this.btn_NextRight.Text = "simpleButton16";
            this.btn_NextRight.Click += new System.EventHandler(this.btn_NextToRight_Click);
            // 
            // btn_NextToLeft
            // 
            this.btn_NextToLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_NextToLeft.Image = global::KP_RES.Properties.Resources.up44_26;
            this.btn_NextToLeft.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_NextToLeft.Location = new System.Drawing.Point(2, 2);
            this.btn_NextToLeft.Name = "btn_NextToLeft";
            this.btn_NextToLeft.Size = new System.Drawing.Size(131, 41);
            this.btn_NextToLeft.TabIndex = 0;
            this.btn_NextToLeft.Text = "simpleButton15";
            this.btn_NextToLeft.Click += new System.EventHandler(this.btn_NextToLeft_Click);
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.panelControl37);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(2, 2);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(857, 45);
            this.panelControl7.TabIndex = 0;
            // 
            // panelControl37
            // 
            this.panelControl37.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl37.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl37.Controls.Add(this.lbKhuVuc);
            this.panelControl37.Controls.Add(this.lbBan);
            this.panelControl37.Controls.Add(this.lbMon);
            this.panelControl37.Location = new System.Drawing.Point(2, 7);
            this.panelControl37.Name = "panelControl37";
            this.panelControl37.Size = new System.Drawing.Size(843, 31);
            this.panelControl37.TabIndex = 0;
            // 
            // lbKhuVuc
            // 
            this.lbKhuVuc.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbKhuVuc.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbKhuVuc.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbKhuVuc.Location = new System.Drawing.Point(531, 0);
            this.lbKhuVuc.Name = "lbKhuVuc";
            this.lbKhuVuc.Size = new System.Drawing.Size(130, 19);
            this.lbKhuVuc.TabIndex = 0;
            this.lbKhuVuc.Text = "Khu Vực 1         >";
            // 
            // lbBan
            // 
            this.lbBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbBan.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbBan.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbBan.Location = new System.Drawing.Point(661, 0);
            this.lbBan.Name = "lbBan";
            this.lbBan.Size = new System.Drawing.Size(82, 19);
            this.lbBan.TabIndex = 1;
            this.lbBan.Text = "    Bàn 1  >";
            // 
            // lbMon
            // 
            this.lbMon.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbMon.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lbMon.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbMon.Location = new System.Drawing.Point(743, 0);
            this.lbMon.Name = "lbMon";
            this.lbMon.Size = new System.Drawing.Size(100, 19);
            this.lbMon.TabIndex = 2;
            this.lbMon.Text = "     Lẩu Bò Tái";
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.pnKV_Mon);
            this.panelControl4.Controls.Add(this.panelControl28);
            this.panelControl4.Controls.Add(this.panelControl27);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl4.Location = new System.Drawing.Point(861, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(135, 526);
            this.panelControl4.TabIndex = 4;
            // 
            // pnKV_Mon
            // 
            this.pnKV_Mon.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnKV_Mon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnKV_Mon.Location = new System.Drawing.Point(2, 47);
            this.pnKV_Mon.Name = "pnKV_Mon";
            this.pnKV_Mon.Size = new System.Drawing.Size(131, 432);
            this.pnKV_Mon.TabIndex = 2;
            // 
            // panelControl28
            // 
            this.panelControl28.Controls.Add(this.btn_UpKV);
            this.panelControl28.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl28.Location = new System.Drawing.Point(2, 2);
            this.panelControl28.Name = "panelControl28";
            this.panelControl28.Size = new System.Drawing.Size(131, 45);
            this.panelControl28.TabIndex = 4;
            // 
            // btn_UpKV
            // 
            this.btn_UpKV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_UpKV.Image = ((System.Drawing.Image)(resources.GetObject("btn_UpKV.Image")));
            this.btn_UpKV.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_UpKV.Location = new System.Drawing.Point(2, 2);
            this.btn_UpKV.Name = "btn_UpKV";
            this.btn_UpKV.Size = new System.Drawing.Size(127, 41);
            this.btn_UpKV.TabIndex = 0;
            this.btn_UpKV.Text = "simpleButton17";
            this.btn_UpKV.Click += new System.EventHandler(this.btn_UpKV_Click);
            // 
            // panelControl27
            // 
            this.panelControl27.Controls.Add(this.btn_DowKV);
            this.panelControl27.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl27.Location = new System.Drawing.Point(2, 479);
            this.panelControl27.Name = "panelControl27";
            this.panelControl27.Size = new System.Drawing.Size(131, 45);
            this.panelControl27.TabIndex = 3;
            // 
            // btn_DowKV
            // 
            this.btn_DowKV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_DowKV.Image = ((System.Drawing.Image)(resources.GetObject("btn_DowKV.Image")));
            this.btn_DowKV.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_DowKV.Location = new System.Drawing.Point(2, 2);
            this.btn_DowKV.Name = "btn_DowKV";
            this.btn_DowKV.Size = new System.Drawing.Size(127, 41);
            this.btn_DowKV.TabIndex = 1;
            this.btn_DowKV.Text = "simpleButton18";
            this.btn_DowKV.Click += new System.EventHandler(this.btn_DowKV_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.btnDong);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(2, 553);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(996, 45);
            this.panelControl1.TabIndex = 3;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Dock = System.Windows.Forms.DockStyle.Right;
            this.simpleButton1.Image = global::KP_RES.Properties.Resources.ok_26;
            this.simpleButton1.Location = new System.Drawing.Point(726, 2);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(137, 41);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "Đồng ý";
            // 
            // btnDong
            // 
            this.btnDong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btnDong.Appearance.Options.UseFont = true;
            this.btnDong.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDong.Image = global::KP_RES.Properties.Resources.cancel_26;
            this.btnDong.Location = new System.Drawing.Point(863, 2);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(131, 41);
            this.btnDong.TabIndex = 0;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // Frm_Danhsachban
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 600);
            this.Controls.Add(this.groupControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximumSize = new System.Drawing.Size(1200, 760);
            this.MinimumSize = new System.Drawing.Size(896, 407);
            this.Name = "Frm_Danhsachban";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FRM_DATBAN";
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnShowHang)).EndInit();
            this.pnShowHang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.panelControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl37)).EndInit();
            this.panelControl37.ResumeLayout(false);
            this.panelControl37.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnKV_Mon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl28)).EndInit();
            this.panelControl28.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl27)).EndInit();
            this.panelControl27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.PanelControl pnShowHang;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl lbPageTable;
        private DevExpress.XtraEditors.SimpleButton btn_NextRight;
        private DevExpress.XtraEditors.SimpleButton btn_NextToLeft;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.PanelControl panelControl37;
        private DevExpress.XtraEditors.LabelControl lbKhuVuc;
        private DevExpress.XtraEditors.LabelControl lbBan;
        private DevExpress.XtraEditors.LabelControl lbMon;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl pnKV_Mon;
        private DevExpress.XtraEditors.PanelControl panelControl28;
        private DevExpress.XtraEditors.SimpleButton btn_UpKV;
        private DevExpress.XtraEditors.PanelControl panelControl27;
        private DevExpress.XtraEditors.SimpleButton btn_DowKV;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.XtraScrollableControl pnTable_Mon;
    }
}