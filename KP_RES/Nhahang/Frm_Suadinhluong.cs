﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES
{
    public partial class Frm_Suadinhluong : DevExpress.XtraEditors.XtraForm
    {
        bool[] ctltxt;
        public string tmp_sSQL = "";
        public string tmp_sSQL_Yeucauthem = "";
        string _mahanghoa = "";
        TextEdit txtText;

        private void Frm_Suadinhluong_Load(object sender, EventArgs e)
        {
            int dem = 1;
            foreach (PanelControl panel in palDinhluong.Controls)
            {
                foreach (Control ctl in panel.Controls)
                {
                    if (ctl is TextEdit)
                    {
                        ctl.Tag = dem;
                        dem += 1;
                    }
                    ctl.Click += new EventHandler(ctl_Click);
                }
                ctltxt = new bool[dem];
            }
            txtText.SelectAll();
        }
        DataTable dt;
        public void LoadDinhluong()
        {
            string sSQL = "";
            sSQL += "Select MATHANHPHAM,MANGUYENLIEU,SOLUONG,";
            sSQL += "(Select TEN_HANGHOA FROM HANGHOA WHERE MA_HANGHOA=MANGUYENLIEU) AS TEN_HANGHOA,";
            sSQL += "(Select TEN_DONVITINH FROM HANGHOA H,DONVITINH D WHERE H.MA_DONVITINH=D.MA_DONVITINH AND H.MA_HANGHOA=MANGUYENLIEU) AS DVT" + "\n";
            sSQL += "From DINHLUONG" + "\n";
            sSQL += "Where SUDUNG=1 AND MATHANHPHAM=" + clsMain.SQLString(_mahanghoa);
            dt = clsMain.ReturnDataTable(sSQL);
            int n = dt.Rows.Count;
            //TextEdit []txt = new TextEdit[n];
            if (n > 0)
            {
                #region tạo label
                PanelControl pal1 = new PanelControl();
                pal1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
                pal1.Height = 25;
                pal1.Width = palDinhluong.Width - 25;
                pal1.Dock = DockStyle.Top;

                LabelControl lb1 = new LabelControl();
                lb1.Name = "lbNguyenlieu";
                lb1.Text = _nguyenlieu;
                lb1.Left = 15;
                lb1.Top = 0;
                lb1.Font = new System.Drawing.Font("Tahoma", 12);
                pal1.Controls.Add(lb1);

                LabelControl lb2 = new LabelControl();
                lb2.Name = "lbDVT";
                lb2.Text = _dvt;
                lb2.Left = palDinhluong.Width - 155;
                lb2.Top = 0;
                lb2.Font = new System.Drawing.Font("Tahoma", 12);
                pal1.Controls.Add(lb2);

                LabelControl lb3 = new LabelControl();
                lb3.Name = "lbSL";
                lb3.Text = _sl;
                lb3.Left = palDinhluong.Width - 94;
                lb3.Top = 0;
                lb3.Font = new System.Drawing.Font("Tahoma", 12);
                pal1.Controls.Add(lb3);

                palDinhluong.Controls.Add(pal1);

                PanelControl pal2 = new PanelControl();
                pal2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
                pal2.Height = 2;
                pal2.Left = 0;
                //pal2.Width = palDinhluong.Width-1;
                pal2.Dock = DockStyle.Bottom;
                pal1.Controls.Add(pal2);

                #endregion
                for (int i = 0; i < n; i++)
                {
                    PanelControl pal = new PanelControl();
                    pal.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
                    pal.Height = 30;
                    pal.Width = palDinhluong.Width-25;
                    pal.Dock = DockStyle.Top;

                    LabelControl lb4 = new LabelControl();
                    lb4.Name = "lbNguyenlieu" + i.ToString();
                    lb4.Text = dt.Rows[i]["TEN_HANGHOA"].ToString();
                    lb4.Left = 4;
                    lb4.Top = 5;
                    lb4.Font = new System.Drawing.Font("Tahoma", 12);
                    pal.Controls.Add(lb4);

                    LabelControl lb5 = new LabelControl();
                    lb5.Name = "lbDVT" + i.ToString();
                    lb5.Text = dt.Rows[i]["DVT"].ToString();
                    lb5.Left = palDinhluong.Width - 150;
                    lb5.Top = 4;
                    lb5.Font = new System.Drawing.Font("Tahoma", 12);
                    pal.Controls.Add(lb5);

                    TextEdit txt = new TextEdit();
                    txt.Name = "txtSL" + i.ToString();
                    txt.Left = palDinhluong.Width - 87;
                    txt.Text = dt.Rows[i]["SOLUONG"].ToString();
                    txt.Top = 0;
                    txt.Width = 50;
                    txt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
                    txt.Font = new System.Drawing.Font("Tahoma", 12);
                    txt.Click += new System.EventHandler(txt_Click);
                    if(i == 0)
                        SetTextEdit(txt);
                    pal.Controls.Add(txt);

                    PanelControl pal3 = new PanelControl();
                    pal3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
                    pal3.Height = 2;
                    pal3.Left = 0;
                    //pal3.Width = palDinhluong.Width-10;
                    pal3.Dock = DockStyle.Bottom;
                    pal.Controls.Add(pal3);

                    palDinhluong.Controls.Add(pal);
                    pal.Dock = DockStyle.Top;
                }
            }
        }

        private void txt_Click(object sender, EventArgs e)
        {
            TextEdit txt =(TextEdit)sender;
            txt.SelectAll();
            SetTextEdit(txt);
        }

        public void SetTextEdit(TextEdit text)
        {
            txtText = text;
        }

       
        #region bàn phím số
        private void InputKeyboardNumber(string sInput)
        {
            if (txtText.Text.Length >= 6)
            {
                int iPositon = txtText.SelectionStart;
                XtraMessageBox.Show(_bkdnq6kt, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtText.SelectionStart = txtText.Text.Length;
                txtText.SelectionStart = iPositon;
                return;
            }
            if (txtText.Text.Contains("."))
            {
                if (sInput == ".")
                    return;
            }
            if (txtText.Text == "0")
            {
                if (sInput == "0" || sInput == "00" || sInput == "000")
                    return;
            }
            if (txtText.SelectedText == txtText.Text)
            {
                txtText.Text = "0";
            }
            int iLenght, iPosition;
            iLenght = txtText.Text.Length;
            iPosition = txtText.SelectionStart;
            if (iPosition == 0)
            {
                if (sInput == "0" || sInput == "00" || sInput == "000")
                    return;
            }

            if (txtText.Text == "0")
            {
                if (sInput == ".")
                {
                    iPosition = iPosition + 1;
                    txtText.Text = txtText.Text.Insert(iPosition, sInput);
                }
                else
                    txtText.Text = sInput;
            }
            else
            {
                txtText.Text = txtText.Text.Insert(iPosition, sInput);
            }
            if (sInput.Length == 1)
            {
                if (txtText.Text.Length - iLenght == 1)
                {
                    txtText.SelectionStart = iPosition + 1;
                }
                else
                {
                    txtText.SelectionStart = iPosition + 2;
                }
            }
            else
                if (sInput.Length == 2)
                {
                    if (txtText.Text.Length - iLenght == 2)
                    {
                        txtText.SelectionStart = iPosition + 2;
                    }
                    else
                    {
                        txtText.SelectionStart = iPosition + 3;
                    }
                }
                else
                    if (sInput.Length == 3)
                    {
                        if (txtText.Text.Length - iLenght == 3)
                        {
                            txtText.SelectionStart = iPosition + 3;
                        }
                        else
                        {
                            txtText.SelectionStart = iPosition + 4;
                        }
                    }


        }//input keyboard for number
        #endregion
        private void btn_7_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("7");
        }

        private void btn_8_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("8");
        }

        private void btn_9_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("9");
        }

        private void btn_ESC_Click(object sender, EventArgs e)
        {
            txtText.EditValue = 0;
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            DeleteTextBox();
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            MoveToLeft();
        }

        private void btnMoveRightNumber_Click(object sender, EventArgs e)
        {
            MoveToRight();
        }

        private void btn_6_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("6");
        }

        private void btn_5_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("5");
        }

        private void btn_4_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("4");
        }

        private void btn_1_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("1");
        }

        private void btn_2_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("2");
        }

        private void btn_3_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("3");
        }

        private void btn_000_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber(".");
        }

        private void btn_00_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("00");
        }

        private void btn_0_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("0");
        }

        private void btn_Thoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Payments_Click(object sender, EventArgs e)
        {
            int n = dt.Rows.Count;
            int i = 0;
            string[] SL = new string[n];
            foreach (PanelControl panel in palDinhluong.Controls)
            {
                foreach (Control  txt in panel.Controls)
                {
                    try
                    {
                        TextEdit txt1 = (TextEdit)txt;

                        if (txt.Text == "0" || txt.Text == "0." || txt.Text == "")
                        {
                            XtraMessageBox.Show(_cngt, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        else if (txt.Text.Length > 6)
                        {
                            XtraMessageBox.Show(_bkdnq6kt, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        else
                        {
                            SL[i] = txt.Text;
                            i += 1;
                        }

                    }
                    catch
                    {
                    }
                     
                
                }
            }

            tmp_sSQL = _mahanghoa + "=";
            tmp_sSQL_Yeucauthem = _mahanghoa + "=";
            for (i = 0; i < n; i++)
            {
                tmp_sSQL += "Insert into DINHLUONG_DIEUCHINH (MA_HOADON,MATHANHPHAM,MANGUYENLIEU,SOLUONG,SUDUNG,NGAYTAO,IS_ON)" + "\n";
                tmp_sSQL += "Values (";
                tmp_sSQL += "TEMP_MA_HOADON" + ",";
                tmp_sSQL += clsMain.SQLString(_mahanghoa) + ",";
                tmp_sSQL += clsMain.SQLString(dt.Rows[i]["MANGUYENLIEU"].ToString()) + ",";
                tmp_sSQL += clsMain.SQLString(SL[i].ToString()) + ",";
                tmp_sSQL += clsMain.SQLBit(true) + ",";
                tmp_sSQL += "GETDATE()" + ",";
                tmp_sSQL += clsMain.SQLBit(true) + ")" + "\n";

                tmp_sSQL_Yeucauthem += dt.Rows[i]["TEN_HANGHOA"].ToString() + ":" + SL[i].ToString() + " " + dt.Rows[i]["DVT"].ToString() + ",   \n";
            }
            tmp_sSQL += "*";
            tmp_sSQL_Yeucauthem += "*";
            this.Close();
        }

       

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                if (!ctltxt[int.Parse(txt.Tag.ToString())])
                {
                    txt.SelectAll();
                    ctltxt[int.Parse(txt.Tag.ToString())] = true;
                }
                else
                {
                    ctltxt[int.Parse(txt.Tag.ToString())] = false;
                }
            }
            catch
            {
            }
        }

        private void txtConfig_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void DeleteTextBox()
        {
            int iPosition = txtText.SelectionStart;
            int iLenght = txtText.Text.Length;
            if (iPosition > 1)
            {
                if (txtText.Text[iPosition - 1].ToString() == ",")
                {
                    txtText.Text = txtText.Text.Remove(iPosition - 2, 1);

                    txtText.SelectionStart = iPosition - 2;

                }
                else
                {
                    txtText.Text = txtText.Text.Remove(iPosition - 1, 1);
                    if (iLenght - txtText.Text.Length == 1)
                    {
                        txtText.SelectionStart = iPosition - 1;
                    }
                    else
                    {
                        txtText.SelectionStart = iPosition - 2;
                    }
                }
            }
            else if (iPosition == 1)
            {
                if (iLenght >= 2)
                {
                    txtText.Text = txtText.Text.Remove(iPosition - 1, 1);
                    txtText.SelectionStart = iPosition - 1;
                    if (txtText.Text == "0")
                        txtText.SelectionStart = 1;
                }
                else
                {
                    txtText.Text = "0";
                    txtText.SelectionStart = 1;
                }
            }
        }

        private void MoveToLeft()
        {
            int iPosition = txtText.SelectionStart;
            if (iPosition > 1)
            {
                if (txtText.Text[iPosition - 1].ToString() == ",")
                {
                    txtText.SelectionStart = iPosition - 2;
                }
                else
                {
                    txtText.SelectionStart = iPosition - 1;
                }
            }
            else
            {
                txtText.SelectionStart = 0;

            }
        }

        private void MoveToRight()
        {
            int iPosition = txtText.SelectionStart;
            if (iPosition < txtText.Text.Length - 1)
            {
                if (txtText.Text[iPosition + 1].ToString() == ",")
                {
                    txtText.SelectionStart = iPosition + 2;
                }
                else
                {
                    txtText.SelectionStart = iPosition + 1;
                }
            }
            else
            {
                txtText.SelectionStart = txtText.Text.Length;

            }
        }

        string _nguyenlieu = "";
        string _dvt = "";
        string _sl = "";
        string _thongbao = "";
        string _bkdnq6kt = "";
        string _cngt = "";
        public Frm_Suadinhluong(string mahanghoa)
        {
            InitializeComponent();
            _mahanghoa = mahanghoa;

            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            this.Text = rm.GetString("dieuchinhdinhluong", culture);
            btn_thoat.Text = rm.GetString("thoat", culture);
            _nguyenlieu = rm.GetString("nguyenlieu", culture);
            _dvt = rm.GetString("dvt1", culture);
            _sl = rm.GetString("soluong2", culture);
            _bkdnq6kt = rm.GetString("bkdnq6kt", culture);
            _cngt = rm.GetString("cngt", culture);
            _thongbao = rm.GetString("thongbao", culture);
            LoadDinhluong();

        }
    }
}