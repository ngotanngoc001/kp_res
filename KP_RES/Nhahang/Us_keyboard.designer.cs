﻿namespace KP_RES
{
    partial class Us_keyboard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_N = new DevExpress.XtraEditors.SimpleButton();
            this.btn_K = new DevExpress.XtraEditors.SimpleButton();
            this.btn_S = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Y = new DevExpress.XtraEditors.SimpleButton();
            this.btn_T = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Space = new DevExpress.XtraEditors.SimpleButton();
            this.btn_B = new DevExpress.XtraEditors.SimpleButton();
            this.btn_J = new DevExpress.XtraEditors.SimpleButton();
            this.btn_A = new DevExpress.XtraEditors.SimpleButton();
            this.btn_P = new DevExpress.XtraEditors.SimpleButton();
            this.btn_R = new DevExpress.XtraEditors.SimpleButton();
            this.btn_V = new DevExpress.XtraEditors.SimpleButton();
            this.btn_H = new DevExpress.XtraEditors.SimpleButton();
            this.btn_C = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Shift = new DevExpress.XtraEditors.SimpleButton();
            this.btn_G = new DevExpress.XtraEditors.SimpleButton();
            this.btn_X = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Z = new DevExpress.XtraEditors.SimpleButton();
            this.btn_F = new DevExpress.XtraEditors.SimpleButton();
            this.btn_O = new DevExpress.XtraEditors.SimpleButton();
            this.btn_M = new DevExpress.XtraEditors.SimpleButton();
            this.btn_I = new DevExpress.XtraEditors.SimpleButton();
            this.btn_L = new DevExpress.XtraEditors.SimpleButton();
            this.btn_D = new DevExpress.XtraEditors.SimpleButton();
            this.btn_E = new DevExpress.XtraEditors.SimpleButton();
            this.btn_U = new DevExpress.XtraEditors.SimpleButton();
            this.btn_W = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Q = new DevExpress.XtraEditors.SimpleButton();
            this.btn_0 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Cross = new DevExpress.XtraEditors.SimpleButton();
            this.btn_enter = new DevExpress.XtraEditors.SimpleButton();
            this.btn_delete = new DevExpress.XtraEditors.SimpleButton();
            this.btn_6 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_9 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_8 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_7 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_5 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_4 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_3 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_2 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ESC = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapsLock = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnDot = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btncheo = new DevExpress.XtraEditors.SimpleButton();
            this.btnNext = new DevExpress.XtraEditors.SimpleButton();
            this.btn_back = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_N
            // 
            this.btn_N.AllowFocus = false;
            this.btn_N.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_N.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_N.Appearance.Options.UseFont = true;
            this.btn_N.Location = new System.Drawing.Point(368, 162);
            this.btn_N.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_N.Name = "btn_N";
            this.btn_N.Size = new System.Drawing.Size(45, 45);
            this.btn_N.TabIndex = 156;
            this.btn_N.Text = "n";
            this.btn_N.Click += new System.EventHandler(this.btn_N_Click);
            // 
            // btn_K
            // 
            this.btn_K.AllowFocus = false;
            this.btn_K.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_K.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_K.Appearance.Options.UseFont = true;
            this.btn_K.Location = new System.Drawing.Point(433, 109);
            this.btn_K.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_K.Name = "btn_K";
            this.btn_K.Size = new System.Drawing.Size(45, 45);
            this.btn_K.TabIndex = 157;
            this.btn_K.Text = "k";
            this.btn_K.Click += new System.EventHandler(this.btn_K_Click);
            // 
            // btn_S
            // 
            this.btn_S.AllowFocus = false;
            this.btn_S.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_S.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_S.Appearance.Options.UseFont = true;
            this.btn_S.Location = new System.Drawing.Point(138, 109);
            this.btn_S.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_S.Name = "btn_S";
            this.btn_S.Size = new System.Drawing.Size(45, 45);
            this.btn_S.TabIndex = 158;
            this.btn_S.Text = "s";
            this.btn_S.Click += new System.EventHandler(this.btn_S_Click);
            // 
            // btn_Y
            // 
            this.btn_Y.AllowFocus = false;
            this.btn_Y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Y.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Y.Appearance.Options.UseFont = true;
            this.btn_Y.Location = new System.Drawing.Point(305, 58);
            this.btn_Y.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_Y.Name = "btn_Y";
            this.btn_Y.Size = new System.Drawing.Size(45, 45);
            this.btn_Y.TabIndex = 155;
            this.btn_Y.Text = "y";
            this.btn_Y.Click += new System.EventHandler(this.btn_Y_Click);
            // 
            // btn_T
            // 
            this.btn_T.AllowFocus = false;
            this.btn_T.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_T.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_T.Appearance.Options.UseFont = true;
            this.btn_T.Location = new System.Drawing.Point(257, 58);
            this.btn_T.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_T.Name = "btn_T";
            this.btn_T.Size = new System.Drawing.Size(45, 45);
            this.btn_T.TabIndex = 151;
            this.btn_T.Text = "t";
            this.btn_T.Click += new System.EventHandler(this.btn_T_Click);
            // 
            // btn_Space
            // 
            this.btn_Space.AllowFocus = false;
            this.btn_Space.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Space.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Space.Appearance.Options.UseFont = true;
            this.btn_Space.Location = new System.Drawing.Point(311, 162);
            this.btn_Space.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_Space.Name = "btn_Space";
            this.btn_Space.Size = new System.Drawing.Size(51, 45);
            this.btn_Space.TabIndex = 153;
            this.btn_Space.Click += new System.EventHandler(this.btn_Space_Click);
            // 
            // btn_B
            // 
            this.btn_B.AllowFocus = false;
            this.btn_B.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_B.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_B.Appearance.Options.UseFont = true;
            this.btn_B.Location = new System.Drawing.Point(259, 162);
            this.btn_B.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_B.Name = "btn_B";
            this.btn_B.Size = new System.Drawing.Size(45, 45);
            this.btn_B.TabIndex = 152;
            this.btn_B.Text = "b";
            this.btn_B.Click += new System.EventHandler(this.btn_B_Click);
            // 
            // btn_J
            // 
            this.btn_J.AllowFocus = false;
            this.btn_J.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_J.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_J.Appearance.Options.UseFont = true;
            this.btn_J.Location = new System.Drawing.Point(384, 109);
            this.btn_J.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_J.Name = "btn_J";
            this.btn_J.Size = new System.Drawing.Size(45, 45);
            this.btn_J.TabIndex = 154;
            this.btn_J.Text = "j";
            this.btn_J.Click += new System.EventHandler(this.btn_J_Click);
            // 
            // btn_A
            // 
            this.btn_A.AllowFocus = false;
            this.btn_A.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_A.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_A.Appearance.Options.UseFont = true;
            this.btn_A.Location = new System.Drawing.Point(89, 109);
            this.btn_A.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_A.Name = "btn_A";
            this.btn_A.Size = new System.Drawing.Size(45, 45);
            this.btn_A.TabIndex = 164;
            this.btn_A.Text = "a";
            this.btn_A.Click += new System.EventHandler(this.btn_A_Click);
            // 
            // btn_P
            // 
            this.btn_P.AllowFocus = false;
            this.btn_P.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_P.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_P.Appearance.Options.UseFont = true;
            this.btn_P.Location = new System.Drawing.Point(497, 58);
            this.btn_P.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_P.Name = "btn_P";
            this.btn_P.Size = new System.Drawing.Size(45, 45);
            this.btn_P.TabIndex = 165;
            this.btn_P.Text = "p";
            this.btn_P.Click += new System.EventHandler(this.btn_P_Click);
            // 
            // btn_R
            // 
            this.btn_R.AllowFocus = false;
            this.btn_R.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_R.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_R.Appearance.Options.UseFont = true;
            this.btn_R.Location = new System.Drawing.Point(209, 58);
            this.btn_R.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_R.Name = "btn_R";
            this.btn_R.Size = new System.Drawing.Size(45, 45);
            this.btn_R.TabIndex = 168;
            this.btn_R.Text = "r";
            this.btn_R.Click += new System.EventHandler(this.btn_R_Click);
            // 
            // btn_V
            // 
            this.btn_V.AllowFocus = false;
            this.btn_V.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_V.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_V.Appearance.Options.UseFont = true;
            this.btn_V.Location = new System.Drawing.Point(210, 162);
            this.btn_V.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_V.Name = "btn_V";
            this.btn_V.Size = new System.Drawing.Size(45, 45);
            this.btn_V.TabIndex = 163;
            this.btn_V.Text = "v";
            this.btn_V.Click += new System.EventHandler(this.btn_V_Click);
            // 
            // btn_H
            // 
            this.btn_H.AllowFocus = false;
            this.btn_H.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_H.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_H.Appearance.Options.UseFont = true;
            this.btn_H.Location = new System.Drawing.Point(334, 109);
            this.btn_H.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_H.Name = "btn_H";
            this.btn_H.Size = new System.Drawing.Size(45, 45);
            this.btn_H.TabIndex = 159;
            this.btn_H.Text = "h";
            this.btn_H.Click += new System.EventHandler(this.btn_H_Click);
            // 
            // btn_C
            // 
            this.btn_C.AllowFocus = false;
            this.btn_C.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_C.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_C.Appearance.Options.UseFont = true;
            this.btn_C.Location = new System.Drawing.Point(161, 162);
            this.btn_C.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_C.Name = "btn_C";
            this.btn_C.Size = new System.Drawing.Size(45, 45);
            this.btn_C.TabIndex = 138;
            this.btn_C.Text = "c";
            this.btn_C.Click += new System.EventHandler(this.btn_C_Click);
            // 
            // btn_Shift
            // 
            this.btn_Shift.AllowFocus = false;
            this.btn_Shift.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Shift.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Shift.Appearance.Options.UseFont = true;
            this.btn_Shift.Location = new System.Drawing.Point(5, 162);
            this.btn_Shift.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_Shift.Name = "btn_Shift";
            this.btn_Shift.Size = new System.Drawing.Size(52, 45);
            this.btn_Shift.TabIndex = 139;
            this.btn_Shift.Text = "Shift";
            this.btn_Shift.Click += new System.EventHandler(this.btn_Shift_Click);
            // 
            // btn_G
            // 
            this.btn_G.AllowFocus = false;
            this.btn_G.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_G.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_G.Appearance.Options.UseFont = true;
            this.btn_G.Location = new System.Drawing.Point(285, 109);
            this.btn_G.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_G.Name = "btn_G";
            this.btn_G.Size = new System.Drawing.Size(45, 45);
            this.btn_G.TabIndex = 141;
            this.btn_G.Text = "g";
            this.btn_G.Click += new System.EventHandler(this.btn_G_Click);
            // 
            // btn_X
            // 
            this.btn_X.AllowFocus = false;
            this.btn_X.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_X.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_X.Appearance.Options.UseFont = true;
            this.btn_X.Location = new System.Drawing.Point(112, 162);
            this.btn_X.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_X.Name = "btn_X";
            this.btn_X.Size = new System.Drawing.Size(45, 45);
            this.btn_X.TabIndex = 136;
            this.btn_X.Text = "x";
            this.btn_X.Click += new System.EventHandler(this.btn_X_Click);
            // 
            // btn_Z
            // 
            this.btn_Z.AllowFocus = false;
            this.btn_Z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Z.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Z.Appearance.Options.UseFont = true;
            this.btn_Z.Location = new System.Drawing.Point(63, 162);
            this.btn_Z.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_Z.Name = "btn_Z";
            this.btn_Z.Size = new System.Drawing.Size(45, 45);
            this.btn_Z.TabIndex = 137;
            this.btn_Z.Text = "z";
            this.btn_Z.Click += new System.EventHandler(this.btn_Z_Click);
            // 
            // btn_F
            // 
            this.btn_F.AllowFocus = false;
            this.btn_F.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_F.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_F.Appearance.Options.UseFont = true;
            this.btn_F.Location = new System.Drawing.Point(236, 109);
            this.btn_F.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_F.Name = "btn_F";
            this.btn_F.Size = new System.Drawing.Size(45, 45);
            this.btn_F.TabIndex = 133;
            this.btn_F.Text = "f";
            this.btn_F.Click += new System.EventHandler(this.btn_F_Click);
            // 
            // btn_O
            // 
            this.btn_O.AllowFocus = false;
            this.btn_O.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_O.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_O.Appearance.Options.UseFont = true;
            this.btn_O.Location = new System.Drawing.Point(449, 58);
            this.btn_O.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_O.Name = "btn_O";
            this.btn_O.Size = new System.Drawing.Size(45, 45);
            this.btn_O.TabIndex = 134;
            this.btn_O.Text = "o";
            this.btn_O.Click += new System.EventHandler(this.btn_O_Click);
            // 
            // btn_M
            // 
            this.btn_M.AllowFocus = false;
            this.btn_M.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_M.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_M.Appearance.Options.UseFont = true;
            this.btn_M.Location = new System.Drawing.Point(419, 162);
            this.btn_M.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_M.Name = "btn_M";
            this.btn_M.Size = new System.Drawing.Size(45, 45);
            this.btn_M.TabIndex = 135;
            this.btn_M.Text = "m";
            this.btn_M.Click += new System.EventHandler(this.btn_M_Click);
            // 
            // btn_I
            // 
            this.btn_I.AllowFocus = false;
            this.btn_I.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_I.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_I.Appearance.Options.UseFont = true;
            this.btn_I.Location = new System.Drawing.Point(401, 58);
            this.btn_I.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_I.Name = "btn_I";
            this.btn_I.Size = new System.Drawing.Size(45, 45);
            this.btn_I.TabIndex = 146;
            this.btn_I.Text = "i";
            this.btn_I.Click += new System.EventHandler(this.btn_I_Click);
            // 
            // btn_L
            // 
            this.btn_L.AllowFocus = false;
            this.btn_L.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_L.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_L.Appearance.Options.UseFont = true;
            this.btn_L.Location = new System.Drawing.Point(482, 109);
            this.btn_L.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_L.Name = "btn_L";
            this.btn_L.Size = new System.Drawing.Size(45, 45);
            this.btn_L.TabIndex = 149;
            this.btn_L.Text = "l";
            this.btn_L.Click += new System.EventHandler(this.btn_L_Click);
            // 
            // btn_D
            // 
            this.btn_D.AllowFocus = false;
            this.btn_D.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_D.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_D.Appearance.Options.UseFont = true;
            this.btn_D.Location = new System.Drawing.Point(187, 109);
            this.btn_D.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_D.Name = "btn_D";
            this.btn_D.Size = new System.Drawing.Size(45, 45);
            this.btn_D.TabIndex = 150;
            this.btn_D.Text = "d";
            this.btn_D.Click += new System.EventHandler(this.btn_D_Click);
            // 
            // btn_E
            // 
            this.btn_E.AllowFocus = false;
            this.btn_E.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_E.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_E.Appearance.Options.UseFont = true;
            this.btn_E.Location = new System.Drawing.Point(161, 58);
            this.btn_E.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_E.Name = "btn_E";
            this.btn_E.Size = new System.Drawing.Size(45, 45);
            this.btn_E.TabIndex = 145;
            this.btn_E.Text = "e";
            this.btn_E.Click += new System.EventHandler(this.btn_E_Click);
            // 
            // btn_U
            // 
            this.btn_U.AllowFocus = false;
            this.btn_U.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_U.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_U.Appearance.Options.UseFont = true;
            this.btn_U.Location = new System.Drawing.Point(353, 58);
            this.btn_U.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_U.Name = "btn_U";
            this.btn_U.Size = new System.Drawing.Size(45, 45);
            this.btn_U.TabIndex = 142;
            this.btn_U.Text = "u";
            this.btn_U.Click += new System.EventHandler(this.btn_U_Click);
            // 
            // btn_W
            // 
            this.btn_W.AllowFocus = false;
            this.btn_W.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_W.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_W.Appearance.Options.UseFont = true;
            this.btn_W.Location = new System.Drawing.Point(113, 58);
            this.btn_W.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_W.Name = "btn_W";
            this.btn_W.Size = new System.Drawing.Size(45, 45);
            this.btn_W.TabIndex = 143;
            this.btn_W.Text = "w";
            this.btn_W.Click += new System.EventHandler(this.btn_W_Click);
            // 
            // btn_Q
            // 
            this.btn_Q.AllowFocus = false;
            this.btn_Q.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Q.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Q.Appearance.Options.UseFont = true;
            this.btn_Q.Location = new System.Drawing.Point(65, 58);
            this.btn_Q.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_Q.Name = "btn_Q";
            this.btn_Q.Size = new System.Drawing.Size(45, 45);
            this.btn_Q.TabIndex = 144;
            this.btn_Q.Text = "q";
            this.btn_Q.Click += new System.EventHandler(this.btn_Q_Click);
            // 
            // btn_0
            // 
            this.btn_0.AllowFocus = false;
            this.btn_0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_0.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_0.Appearance.Options.UseFont = true;
            this.btn_0.Appearance.Options.UseTextOptions = true;
            this.btn_0.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_0.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.btn_0.Location = new System.Drawing.Point(465, 9);
            this.btn_0.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_0.Name = "btn_0";
            this.btn_0.Size = new System.Drawing.Size(43, 43);
            this.btn_0.TabIndex = 129;
            this.btn_0.Text = ")\r\n0";
            this.btn_0.Click += new System.EventHandler(this.btn_0_Click);
            // 
            // btn_Cross
            // 
            this.btn_Cross.AllowFocus = false;
            this.btn_Cross.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Cross.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Cross.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btn_Cross.Appearance.Options.UseFont = true;
            this.btn_Cross.Appearance.Options.UseForeColor = true;
            this.btn_Cross.Appearance.Options.UseTextOptions = true;
            this.btn_Cross.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_Cross.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.btn_Cross.Location = new System.Drawing.Point(511, 9);
            this.btn_Cross.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_Cross.Name = "btn_Cross";
            this.btn_Cross.Size = new System.Drawing.Size(43, 43);
            this.btn_Cross.TabIndex = 130;
            this.btn_Cross.Text = "_\r\n-";
            this.btn_Cross.Click += new System.EventHandler(this.btn_Cross_Click);
            // 
            // btn_enter
            // 
            this.btn_enter.AllowFocus = false;
            this.btn_enter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_enter.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_enter.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btn_enter.Appearance.Options.UseFont = true;
            this.btn_enter.Appearance.Options.UseForeColor = true;
            this.btn_enter.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_enter.Location = new System.Drawing.Point(554, 109);
            this.btn_enter.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_enter.Name = "btn_enter";
            this.btn_enter.Size = new System.Drawing.Size(92, 98);
            this.btn_enter.TabIndex = 132;
            this.btn_enter.Text = "Enter";
            this.btn_enter.Click += new System.EventHandler(this.btn_enter_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.AllowFocus = false;
            this.btn_delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_delete.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_delete.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btn_delete.Appearance.Options.UseFont = true;
            this.btn_delete.Appearance.Options.UseForeColor = true;
            this.btn_delete.Location = new System.Drawing.Point(603, 9);
            this.btn_delete.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(43, 43);
            this.btn_delete.TabIndex = 131;
            this.btn_delete.Text = "Del";
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_6
            // 
            this.btn_6.AllowFocus = false;
            this.btn_6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_6.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_6.Appearance.Options.UseFont = true;
            this.btn_6.Appearance.Options.UseTextOptions = true;
            this.btn_6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_6.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.btn_6.Location = new System.Drawing.Point(281, 9);
            this.btn_6.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_6.Name = "btn_6";
            this.btn_6.Size = new System.Drawing.Size(43, 43);
            this.btn_6.TabIndex = 125;
            this.btn_6.Text = "^\r\n6";
            this.btn_6.Click += new System.EventHandler(this.btn_6_Click);
            // 
            // btn_9
            // 
            this.btn_9.AllowFocus = false;
            this.btn_9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_9.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_9.Appearance.Options.UseFont = true;
            this.btn_9.Appearance.Options.UseTextOptions = true;
            this.btn_9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_9.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.btn_9.Location = new System.Drawing.Point(419, 9);
            this.btn_9.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_9.Name = "btn_9";
            this.btn_9.Size = new System.Drawing.Size(43, 43);
            this.btn_9.TabIndex = 128;
            this.btn_9.Text = "(\r\n9\r\n";
            this.btn_9.Click += new System.EventHandler(this.btn_9_Click);
            // 
            // btn_8
            // 
            this.btn_8.AllowFocus = false;
            this.btn_8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_8.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_8.Appearance.Options.UseFont = true;
            this.btn_8.Appearance.Options.UseTextOptions = true;
            this.btn_8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_8.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.btn_8.Location = new System.Drawing.Point(373, 9);
            this.btn_8.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_8.Name = "btn_8";
            this.btn_8.Size = new System.Drawing.Size(43, 43);
            this.btn_8.TabIndex = 127;
            this.btn_8.Text = "*\r\n8";
            this.btn_8.Click += new System.EventHandler(this.btn_8_Click);
            // 
            // btn_7
            // 
            this.btn_7.AllowFocus = false;
            this.btn_7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_7.Appearance.Options.UseFont = true;
            this.btn_7.Appearance.Options.UseTextOptions = true;
            this.btn_7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_7.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.btn_7.Location = new System.Drawing.Point(327, 9);
            this.btn_7.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_7.Name = "btn_7";
            this.btn_7.Size = new System.Drawing.Size(43, 43);
            this.btn_7.TabIndex = 126;
            this.btn_7.Text = "&&\r\n7";
            this.btn_7.Click += new System.EventHandler(this.btn_7_Click);
            // 
            // btn_5
            // 
            this.btn_5.AllowFocus = false;
            this.btn_5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_5.Appearance.Options.UseFont = true;
            this.btn_5.Appearance.Options.UseTextOptions = true;
            this.btn_5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_5.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.btn_5.Location = new System.Drawing.Point(235, 9);
            this.btn_5.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_5.Name = "btn_5";
            this.btn_5.Size = new System.Drawing.Size(43, 43);
            this.btn_5.TabIndex = 123;
            this.btn_5.Text = "%\r\n5";
            this.btn_5.Click += new System.EventHandler(this.btn_5_Click);
            // 
            // btn_4
            // 
            this.btn_4.AllowFocus = false;
            this.btn_4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_4.Appearance.Options.UseFont = true;
            this.btn_4.Appearance.Options.UseTextOptions = true;
            this.btn_4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_4.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.btn_4.Location = new System.Drawing.Point(189, 9);
            this.btn_4.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_4.Name = "btn_4";
            this.btn_4.Size = new System.Drawing.Size(43, 43);
            this.btn_4.TabIndex = 122;
            this.btn_4.Text = "$\r\n4";
            this.btn_4.Click += new System.EventHandler(this.btn_4_Click);
            // 
            // btn_3
            // 
            this.btn_3.AllowFocus = false;
            this.btn_3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_3.Appearance.Options.UseFont = true;
            this.btn_3.Appearance.Options.UseTextOptions = true;
            this.btn_3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.btn_3.Location = new System.Drawing.Point(143, 9);
            this.btn_3.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_3.Name = "btn_3";
            this.btn_3.Size = new System.Drawing.Size(43, 43);
            this.btn_3.TabIndex = 121;
            this.btn_3.Text = "#\r\n3";
            this.btn_3.Click += new System.EventHandler(this.btn_3_Click);
            // 
            // btn_2
            // 
            this.btn_2.AllowFocus = false;
            this.btn_2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_2.Appearance.Options.UseFont = true;
            this.btn_2.Appearance.Options.UseTextOptions = true;
            this.btn_2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.btn_2.Location = new System.Drawing.Point(97, 9);
            this.btn_2.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_2.Name = "btn_2";
            this.btn_2.Size = new System.Drawing.Size(43, 43);
            this.btn_2.TabIndex = 120;
            this.btn_2.Text = "@\r\n2";
            this.btn_2.Click += new System.EventHandler(this.btn_2_Click);
            // 
            // btn_1
            // 
            this.btn_1.AccessibleDescription = "";
            this.btn_1.AllowFocus = false;
            this.btn_1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_1.Appearance.Options.UseFont = true;
            this.btn_1.Appearance.Options.UseTextOptions = true;
            this.btn_1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.btn_1.Location = new System.Drawing.Point(51, 9);
            this.btn_1.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_1.Name = "btn_1";
            this.btn_1.Size = new System.Drawing.Size(43, 43);
            this.btn_1.TabIndex = 119;
            this.btn_1.Text = "!\r\n1";
            this.btn_1.Click += new System.EventHandler(this.btn_1_Click);
            // 
            // btn_ESC
            // 
            this.btn_ESC.AllowFocus = false;
            this.btn_ESC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_ESC.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ESC.Appearance.ForeColor = System.Drawing.Color.Maroon;
            this.btn_ESC.Appearance.Options.UseFont = true;
            this.btn_ESC.Appearance.Options.UseForeColor = true;
            this.btn_ESC.Appearance.Options.UseTextOptions = true;
            this.btn_ESC.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_ESC.Location = new System.Drawing.Point(5, 9);
            this.btn_ESC.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_ESC.Name = "btn_ESC";
            this.btn_ESC.Size = new System.Drawing.Size(43, 43);
            this.btn_ESC.TabIndex = 118;
            this.btn_ESC.Text = "ESC";
            this.btn_ESC.Click += new System.EventHandler(this.btn_ESC_Click);
            // 
            // btnCapsLock
            // 
            this.btnCapsLock.AllowFocus = false;
            this.btnCapsLock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCapsLock.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapsLock.Appearance.Options.UseFont = true;
            this.btnCapsLock.Appearance.Options.UseTextOptions = true;
            this.btnCapsLock.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnCapsLock.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCapsLock.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapsLock.Location = new System.Drawing.Point(5, 110);
            this.btnCapsLock.LookAndFeel.UseWindowsXPTheme = true;
            this.btnCapsLock.Name = "btnCapsLock";
            this.btnCapsLock.Size = new System.Drawing.Size(78, 44);
            this.btnCapsLock.TabIndex = 169;
            this.btnCapsLock.Text = "Caps Lock";
            this.btnCapsLock.Click += new System.EventHandler(this.btnCapsLock_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.AllowFocus = false;
            this.simpleButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Location = new System.Drawing.Point(5, 58);
            this.simpleButton2.LookAndFeel.UseWindowsXPTheme = true;
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(52, 45);
            this.simpleButton2.TabIndex = 170;
            this.simpleButton2.Text = "Tab";
            // 
            // btnDot
            // 
            this.btnDot.AllowFocus = false;
            this.btnDot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDot.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDot.Appearance.Options.UseFont = true;
            this.btnDot.Location = new System.Drawing.Point(469, 162);
            this.btnDot.Name = "btnDot";
            this.btnDot.Size = new System.Drawing.Size(38, 45);
            this.btnDot.TabIndex = 171;
            this.btnDot.Text = ".";
            this.btnDot.Click += new System.EventHandler(this.btnshift2_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.AllowFocus = false;
            this.simpleButton4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton4.Appearance.ForeColor = System.Drawing.Color.Black;
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.Appearance.Options.UseForeColor = true;
            this.simpleButton4.Appearance.Options.UseTextOptions = true;
            this.simpleButton4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleButton4.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.simpleButton4.Location = new System.Drawing.Point(557, 9);
            this.simpleButton4.LookAndFeel.UseWindowsXPTheme = true;
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(43, 43);
            this.simpleButton4.TabIndex = 172;
            this.simpleButton4.Text = "+\r\n=";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btncheo);
            this.panelControl1.Controls.Add(this.btnNext);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.btnDot);
            this.panelControl1.Controls.Add(this.btnCapsLock);
            this.panelControl1.Controls.Add(this.btn_N);
            this.panelControl1.Controls.Add(this.btn_K);
            this.panelControl1.Controls.Add(this.btn_S);
            this.panelControl1.Controls.Add(this.btn_Y);
            this.panelControl1.Controls.Add(this.btn_T);
            this.panelControl1.Controls.Add(this.simpleButton4);
            this.panelControl1.Controls.Add(this.btn_Space);
            this.panelControl1.Controls.Add(this.btn_A);
            this.panelControl1.Controls.Add(this.btn_P);
            this.panelControl1.Controls.Add(this.btn_B);
            this.panelControl1.Controls.Add(this.btn_R);
            this.panelControl1.Controls.Add(this.btn_H);
            this.panelControl1.Controls.Add(this.btn_Shift);
            this.panelControl1.Controls.Add(this.btn_G);
            this.panelControl1.Controls.Add(this.btn_X);
            this.panelControl1.Controls.Add(this.btn_V);
            this.panelControl1.Controls.Add(this.btn_Z);
            this.panelControl1.Controls.Add(this.btn_F);
            this.panelControl1.Controls.Add(this.btn_M);
            this.panelControl1.Controls.Add(this.btn_O);
            this.panelControl1.Controls.Add(this.btn_L);
            this.panelControl1.Controls.Add(this.btn_E);
            this.panelControl1.Controls.Add(this.btn_C);
            this.panelControl1.Controls.Add(this.btn_U);
            this.panelControl1.Controls.Add(this.btn_I);
            this.panelControl1.Controls.Add(this.btn_W);
            this.panelControl1.Controls.Add(this.btn_0);
            this.panelControl1.Controls.Add(this.btn_J);
            this.panelControl1.Controls.Add(this.btn_Cross);
            this.panelControl1.Controls.Add(this.btn_6);
            this.panelControl1.Controls.Add(this.btn_enter);
            this.panelControl1.Controls.Add(this.btn_delete);
            this.panelControl1.Controls.Add(this.btn_D);
            this.panelControl1.Controls.Add(this.btn_8);
            this.panelControl1.Controls.Add(this.btn_5);
            this.panelControl1.Controls.Add(this.btn_7);
            this.panelControl1.Controls.Add(this.btn_ESC);
            this.panelControl1.Controls.Add(this.btn_back);
            this.panelControl1.Controls.Add(this.btn_2);
            this.panelControl1.Controls.Add(this.btn_9);
            this.panelControl1.Controls.Add(this.btn_3);
            this.panelControl1.Controls.Add(this.btn_4);
            this.panelControl1.Controls.Add(this.btn_1);
            this.panelControl1.Controls.Add(this.btn_Q);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(650, 223);
            this.panelControl1.TabIndex = 173;
            // 
            // btncheo
            // 
            this.btncheo.AllowFocus = false;
            this.btncheo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btncheo.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncheo.Appearance.Options.UseFont = true;
            this.btncheo.Location = new System.Drawing.Point(512, 162);
            this.btncheo.Name = "btncheo";
            this.btncheo.Size = new System.Drawing.Size(38, 45);
            this.btncheo.TabIndex = 174;
            this.btncheo.Text = "/";
            this.btncheo.Click += new System.EventHandler(this.btncheo_Click);
            // 
            // btnNext
            // 
            this.btnNext.AllowFocus = false;
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.Appearance.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btnNext.Appearance.Options.UseFont = true;
            this.btnNext.Appearance.Options.UseForeColor = true;
            this.btnNext.Appearance.Options.UseTextOptions = true;
            this.btnNext.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnNext.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.btnNext.Image = global::KP_RES.Properties.Resources.right_26;
            this.btnNext.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnNext.Location = new System.Drawing.Point(602, 58);
            this.btnNext.LookAndFeel.UseWindowsXPTheme = true;
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(43, 45);
            this.btnNext.TabIndex = 173;
            this.btnNext.Text = "Back";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btn_back
            // 
            this.btn_back.AllowFocus = false;
            this.btn_back.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_back.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_back.Appearance.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btn_back.Appearance.Options.UseFont = true;
            this.btn_back.Appearance.Options.UseForeColor = true;
            this.btn_back.Appearance.Options.UseTextOptions = true;
            this.btn_back.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_back.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.btn_back.Image = global::KP_RES.Properties.Resources.left_26;
            this.btn_back.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_back.Location = new System.Drawing.Point(557, 58);
            this.btn_back.LookAndFeel.UseWindowsXPTheme = true;
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(43, 45);
            this.btn_back.TabIndex = 124;
            this.btn_back.Text = "Back";
            this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
            // 
            // Us_keyboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add(this.panelControl1);
            this.Name = "Us_keyboard";
            this.Size = new System.Drawing.Size(650, 223);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btn_N;
        private DevExpress.XtraEditors.SimpleButton btn_K;
        private DevExpress.XtraEditors.SimpleButton btn_S;
        private DevExpress.XtraEditors.SimpleButton btn_Y;
        private DevExpress.XtraEditors.SimpleButton btn_T;
        private DevExpress.XtraEditors.SimpleButton btn_Space;
        private DevExpress.XtraEditors.SimpleButton btn_B;
        private DevExpress.XtraEditors.SimpleButton btn_J;
        private DevExpress.XtraEditors.SimpleButton btn_A;
        private DevExpress.XtraEditors.SimpleButton btn_P;
        private DevExpress.XtraEditors.SimpleButton btn_R;
        private DevExpress.XtraEditors.SimpleButton btn_V;
        private DevExpress.XtraEditors.SimpleButton btn_H;
        private DevExpress.XtraEditors.SimpleButton btn_C;
        private DevExpress.XtraEditors.SimpleButton btn_Shift;
        private DevExpress.XtraEditors.SimpleButton btn_G;
        private DevExpress.XtraEditors.SimpleButton btn_X;
        private DevExpress.XtraEditors.SimpleButton btn_Z;
        private DevExpress.XtraEditors.SimpleButton btn_F;
        private DevExpress.XtraEditors.SimpleButton btn_O;
        private DevExpress.XtraEditors.SimpleButton btn_M;
        private DevExpress.XtraEditors.SimpleButton btn_I;
        private DevExpress.XtraEditors.SimpleButton btn_L;
        private DevExpress.XtraEditors.SimpleButton btn_D;
        private DevExpress.XtraEditors.SimpleButton btn_E;
        private DevExpress.XtraEditors.SimpleButton btn_U;
        private DevExpress.XtraEditors.SimpleButton btn_W;
        private DevExpress.XtraEditors.SimpleButton btn_Q;
        private DevExpress.XtraEditors.SimpleButton btn_0;
        private DevExpress.XtraEditors.SimpleButton btn_Cross;
        private DevExpress.XtraEditors.SimpleButton btn_enter;
        private DevExpress.XtraEditors.SimpleButton btn_delete;
        private DevExpress.XtraEditors.SimpleButton btn_6;
        private DevExpress.XtraEditors.SimpleButton btn_9;
        private DevExpress.XtraEditors.SimpleButton btn_8;
        private DevExpress.XtraEditors.SimpleButton btn_7;
        private DevExpress.XtraEditors.SimpleButton btn_back;
        private DevExpress.XtraEditors.SimpleButton btn_5;
        private DevExpress.XtraEditors.SimpleButton btn_4;
        private DevExpress.XtraEditors.SimpleButton btn_3;
        private DevExpress.XtraEditors.SimpleButton btn_2;
        private DevExpress.XtraEditors.SimpleButton btn_1;
        private DevExpress.XtraEditors.SimpleButton btn_ESC;
        private DevExpress.XtraEditors.SimpleButton btnCapsLock;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton btnDot;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnNext;
        private DevExpress.XtraEditors.SimpleButton btncheo;
    }
}
