﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;

namespace KP_RES
{
    public partial class Frm_NhanVienPayment : DevExpress.XtraEditors.XtraForm
    {
        public DataTable dtGridviewProduct = new DataTable();
        public DataTable dtGridviewProduct1 = new DataTable();


        public Frm_NhanVienPayment(DataTable dtGridview)
        {
            InitializeComponent();
            dtGridviewProduct = dtGridview;
            LoaddataGridView();
        }

        private void LoaddataGridView()
        {
            dtGridviewProduct1 = dtGridviewProduct;
            gridView1.Focus();

            DataTable myDT = clsMain.ReturnDataTable("SELECT MANHANVIEN AS MA, TENNHANVIEN AS TEN FROM DM_NHANVIEN WHERE SUDUNG = 1");
            if (myDT.Rows.Count > 0)
            {
                repositoryItemCheckedComboBoxEdit2.DataSource = myDT;
                string[] displayname = new string[] { "Mã", "Tên"};
                string[] hidename = new string[] { "MA", "TEN"};
                int[] width = new int[] { 100, 300};
                bool[] visible = new bool[] { true, true};

                for (int i = 0; i < displayname.Length; i++)
                {
                    LookUpColumnInfo info = new LookUpColumnInfo();
                    info.FieldName = hidename[i];
                    info.Caption = displayname[i];
                    info.Visible = visible[i];
                    info.Width = width[i];

                    repositoryItemCheckedComboBoxEdit2.Items.Add(info);
                }
            }
            DataColumn sMaNhanVien = new DataColumn();
            sMaNhanVien.ColumnName = "sMaNhanVien";
            sMaNhanVien.DataType = System.Type.GetType("System.String");
            sMaNhanVien.DefaultValue = "0";
            DataColumn sTenNhanVien = new DataColumn();
            sTenNhanVien.ColumnName = "sTenNhanVien";
            sTenNhanVien.DataType = System.Type.GetType("System.String");
            DataColumn NhanVien = new DataColumn();
            NhanVien.ColumnName = "NhanVien";
            NhanVien.DataType = System.Type.GetType("System.String");

            if (ContainColumn("sMaNhanVien", dtGridviewProduct1) == false)
                dtGridviewProduct1.Columns.Add(sMaNhanVien);
            if (ContainColumn("sTenNhanVien", dtGridviewProduct1) == false)
                dtGridviewProduct1.Columns.Add(sTenNhanVien);
            if (ContainColumn("NhanVien", dtGridviewProduct1) == false)
                dtGridviewProduct1.Columns.Add(NhanVien);

            gridControl1.DataSource = dtGridviewProduct1;
        }

        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void look_ten_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DevExpress.XtraEditors.CheckedComboBoxEdit editor = (sender as DevExpress.XtraEditors.CheckedComboBoxEdit);

                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "NhanVien", CheckedComboBox(editor));

                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "sMaNhanVien", editor.Properties.GetCheckedItems());
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "sTenNhanVien", editor.Properties.GetCheckedItems());
            }
            catch
            {

            }
        }

        public static String CheckedComboBox(CheckedComboBoxEdit sCbo)
        {
            String sStr = "";
            try
            {
                foreach (CheckedListBoxItem sItem in sCbo.Properties.Items)
                {
                    if (sItem.CheckState == CheckState.Checked)
                    {
                        if (sStr != "")
                            sStr += ", ";

                        sStr += sItem.ToString();
                    }
                }
            }
            catch (Exception)
            {
            }

            return sStr;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            dtGridviewProduct = dtGridviewProduct1;
            this.Close();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_NhanVienPayment_Load(object sender, EventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            gridView1.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView1.Columns["TEN_HANGHOA"].Caption = rm.GetString("tenhanghoa", culture);
            gridView1.Columns["sTenNhanVien"].Caption = rm.GetString("nhanvien", culture);
            btnLuu.Text = rm.GetString("luu",culture);
            btnThoat.Text = rm.GetString("thoat", culture);
        }


        


    }
}