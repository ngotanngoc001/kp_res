﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Metro.Navigation;
using System.IO;
using KP_UserManagement;
using KP_Terminal;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;

namespace KP_RES 
{
    public partial class Frm_Main1 : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Main1()
        {
            if (Process.GetProcessesByName("kingpos").Length > 1)
            {
                System.Environment.Exit(0);
            }
            InitializeComponent();
            this.defaultLookAndFeel1.LookAndFeel.SkinName = cls_KP_RES.LoadSkin();
            clsKP_Terminal.LoadMayIn();
            clsUserManagement.ShowFormDangNhap();
            LoadPermission();
            VisibleTileItem();
        }

        public void ExtractZipFile(string archiveFilenameIn, string password, string outFolder)
        {
            ZipFile zf = null;
            try
            {
                FileStream fs = File.OpenRead(archiveFilenameIn);
                zf = new ZipFile(fs);
                if (!String.IsNullOrEmpty(password))
                {
                    zf.Password = password;     // AES encrypted entries are handled automatically
                }
                foreach (ZipEntry zipEntry in zf)
                {
                    if (!zipEntry.IsFile)
                    {
                        continue;           // Ignore directories
                    }
                    String entryFileName = zipEntry.Name;
                    // to remove the folder from the entry:- entryFileName = Path.GetFileName(entryFileName);
                    // Optionally match entrynames against a selection list here to skip as desired.
                    // The unpacked length is available in the zipEntry.Size property.

                    byte[] buffer = new byte[4096];     // 4K is optimum
                    Stream zipStream = zf.GetInputStream(zipEntry);

                    // Manipulate the output filename here as desired.
                    String fullZipToPath = Path.Combine(outFolder, entryFileName);
                    string directoryName = Path.GetDirectoryName(fullZipToPath);
                    if (directoryName.Length > 0)
                        Directory.CreateDirectory(directoryName);

                    // Unzip file in buffered chunks. This is just as fast as unpacking to a buffer the full size
                    // of the file, but does not waste memory.
                    // The "using" will close the stream even if an exception occurs.
                    using (FileStream streamWriter = File.Create(fullZipToPath))
                    {
                        StreamUtils.Copy(zipStream, streamWriter, buffer);
                    }
                }
            }
            finally
            {
                if (zf != null)
                {
                    zf.IsStreamOwner = true; // Makes close also shut the underlying stream
                    zf.Close(); // Ensure we release resources
                }
            }
        }

        private void Frm_Main1_Load(object sender, EventArgs e)
        {
            itDangxuat.Text = "(" + clsGlobal.gsUserID.ToUpper() + ")" + "\n" + "Đăng xuất";
            cls_KP_RES.LoadLayout(tileControl1);
            if (File.Exists(Application.StartupPath + "\\KP_Inventory.exe"))
            {
                foreach (Process proc in Process.GetProcessesByName("KP_Inventory"))
                {
                    proc.Kill();
                }
                Process.Start(Application.StartupPath + "\\KP_Inventory.exe");
            }
        }

        private void Frm_Main1_FormClosing(object sender, FormClosingEventArgs e)
        {
            cls_KP_RES.SaveSkin(defaultLookAndFeel1.LookAndFeel.SkinName);
            cls_KP_RES.SaveLayout(tileControl1);
            clsUserManagement.FreeLogin(clsUserManagement .ReturnUserLogin ());
            foreach (Process proc in Process.GetProcessesByName("KP_Inventory"))
            {
                proc.Kill();
            }
        }

        private void Frm_Main1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
            if (e.KeyCode == Keys.F1)
            {
                this.WindowState =  FormWindowState.Minimized;
            }
        }

        private void timerGiaiphongbonho_Tick(object sender, EventArgs e)
        {
            GC.Collect();
            if (!clsUserManagement.CheckLogin(clsGlobal.gsUserID))
            {
                itDangxuat_ItemClick(null, null);
            }
        }

        private void itThunho_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itThunho.Checked)
            {
                this.WindowState = FormWindowState.Minimized;
            }
        }

        private void itThoat_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itThoat.Checked )
            {
                try
                {
                    foreach (Process proc in Process.GetProcessesByName("KP_Keyboard"))
                    {
                        proc.Kill();
                    }
                   
                    this.Close();
                }
                catch
                {
                    this.Close();
                }
            }
        }

        private void itHethong_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itHethong.Checked)
            {
                this.GoTo<Frm_Hethong>();
            }
        }

        private void itCauhinh_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itCauhinh.Checked)
            {
                this.GoTo<Frm_Cauhinh>();
            }
        }

        private void itDangxuat_ItemClick(object sender, TileItemEventArgs e)
        {
            cls_KP_RES.LoadDefaultLayout();
            if (itDangxuat.Checked)
            {
                Application.Restart();
            }
        }

        private void itTrogiup_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itTrogiup.Checked)
            {
                this.GoTo<Frm_Trogiup>();
            }
        }

        private void VisibleTileItem()
        {
            cls_KP_RES.RemoveTileItem(itKetoan );
            cls_KP_RES.RemoveTileItem(itHanhchinh);
            cls_KP_RES.RemoveTileItem(itGiamdoc);
            
        }

        private void LoadPermission()
        {
            itDangxuat.Checked = true;
            itTrogiup.Checked = true;
            itCauhinh.Checked = true;
            itHethong.Checked = true;
            itThoat.Checked = true;
            itThunho.Checked = true;

            //itKetoan.Checked = (clsUserManagement.AllowView("92") || clsUserManagement.AllowView("93") || clsUserManagement.AllowView("94") || clsUserManagement.AllowView("95") || clsUserManagement.AllowView("96") || clsUserManagement.AllowView("97") || clsUserManagement.AllowView("98") || clsUserManagement.AllowView("90") || clsUserManagement.AllowView("91") || clsUserManagement.AllowView("26071601") || clsUserManagement.AllowView("26071602")) && clsUserManagement.CheckActive("12");
            //itHanhchinh.Checked = (clsUserManagement.AllowView("24031601") || clsUserManagement.AllowView("24031602") || clsUserManagement.AllowView("24031603")) && clsUserManagement.CheckActive("16");
            


         

        }

        private void tileControl1_Paint(object sender, PaintEventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main1).Assembly);
            itDangxuat.Text = rm.GetString("dangxuat", culture);
            itTrogiup.Text = rm.GetString("trogiup", culture);
            itCauhinh.Text = rm.GetString("cauhinh", culture);
            itThoat.Text = rm.GetString("thoat", culture);
            itHethong.Text = rm.GetString("hethong", culture);
            itThunho.Text = rm.GetString("thunho", culture);
           
        }

        private void itKetoan_ItemClick(object sender, TileItemEventArgs e)
        {
             bool bKetoan= (clsUserManagement.AllowView("92") || clsUserManagement.AllowView("93") || clsUserManagement.AllowView("94") || clsUserManagement.AllowView("95") || clsUserManagement.AllowView("96") || clsUserManagement.AllowView("97") || clsUserManagement.AllowView("98") || clsUserManagement.AllowView("90") || clsUserManagement.AllowView("91") || clsUserManagement.AllowView("26071601") || clsUserManagement.AllowView("26071602")) && clsUserManagement.CheckActive("12");
            if (itKetoan.Checked && bKetoan)
            {
                this.GoTo<Frm_Phongketoan>();
            }
        }

        private void itHanhchinh_ItemClick(object sender, TileItemEventArgs e)
        {
            bool bHanhchinh = (clsUserManagement.AllowView("24031601") || clsUserManagement.AllowView("24031602") || clsUserManagement.AllowView("24031603")) && clsUserManagement.CheckActive("16");
            if (itHanhchinh.Checked && bHanhchinh )
            {
                this.GoTo<Frm_Phonghanhchinh>();
            }
        }

        private void itGiamdoc_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itGiamdoc.Checked)
            {
                this.GoTo<Frm_Phonggiamdoc>();
            }
        }

        private void itKho_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKho.Checked)
            {
                this.GoTo<Frm_CongtyKho>();
            }
        }

        private void itNhansu_ItemClick(object sender, TileItemEventArgs e)
        {
            if (itKho.Checked)
            {
                this.GoTo<Frm_Phongnhansu>();
            }
        }

      
      
        
    }
}