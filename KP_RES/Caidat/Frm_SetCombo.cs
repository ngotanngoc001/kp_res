﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Data.SqlClient;
using DevExpress.XtraEditors.Controls;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_SetCombo : DevExpress.XtraEditors.XtraForm
    {
        public static BindingSource BS = new BindingSource();
        int flag = 0;//flag=0:Combo chi tiết, flag=2:combo lưới , flag=3: combo Icon
        string smaNhomCombo = "";
        string smaCombo = "";
        string smaNhomhang = "";
        string smaHang = "";

        public Frm_SetCombo()
        {
            InitializeComponent();
            btnc_Click(null, null);
            LoadCombo();
            LoadNhomCombo();
            LoadDSCombo(smaNhomCombo,false);
            LoadGridCOMBO();
            LoadPermission();
        }

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                txt.SelectAll();
            }
            catch
            {
            }
        }

        private void Frm_NhomHanghoa_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void btnLenNhomhang_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlNhomhang.AutoScrollPosition = new Point(0, fpnlNhomhang.VerticalScroll.Value - fpnlNhomhang.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnXuongNhomhang_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlNhomhang.AutoScrollPosition = new Point(0, fpnlNhomhang.VerticalScroll.Value + fpnlNhomhang.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnLenHangHoa_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlHanghoa.AutoScrollPosition = new Point(0, fpnlHanghoa.VerticalScroll.Value - fpnlHanghoa.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnXuongHangHoa_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlHanghoa.AutoScrollPosition = new Point(0, fpnlHanghoa.VerticalScroll.Value + fpnlHanghoa.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            //LoadNhomCombo();
            //LoadDSCombo(smaNhomCombo);
            //if (fpnlNhomhang.Controls.Count > 0)
            //{
            //    NhomCombo_Click(fpnlNhomhang.Controls[0], null);
            //}
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        DataTable dtNhomhang = new DataTable();
        private void LoadNhomCombo()
        {
            SetRongControl();
            KhoaMoControl(true);

            string sSQL = "";
            sSQL += "Select MA_NHOMHANG As MA,TEN_NHOMHANG As TEN,GHICHU,SUDUNG,THUCDON,STT,HINHANH" + "\n";
            sSQL += "From NHOMHANG" + "\n";
            sSQL += "Order by SUDUNG DESC,STT" + "\n";
            dtNhomhang = clsMain.ReturnDataTable(sSQL);
            fpnlNhomhang.Controls.Clear();
            fpnlHanghoa.Controls.Clear();
            foreach (DataRow dr in dtNhomhang.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 125;
                btn.Height = 80;
                btn.Font = fpnlHanghoa.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                try
                {
                    btn.ImageLocation = ImageLocation.TopCenter;
                    byte[] tam = new byte[((byte[])dr["HINHANH"]).LongLength];
                    tam = (byte[])dr["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    btn.Image = bm;
                }
                catch
                {
                }
                btn.Name = "N" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(NhomCombo_Click);
                fpnlNhomhang.Controls.Add(btn);
            }
            if (fpnlNhomhang.VerticalScroll.Visible)
            {
                palNHOMHANG.Width = 152;
            }
            else
            {
                palNHOMHANG.Width = 135;
            }
        }

        private void NhomCombo_Click(object sender, EventArgs e)//nhóm hàng dùng để chọn Combo
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in fpnlNhomhang.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }
            //if(flag)//Load các combo
            LoadDSCombo(btn.Name.Substring(1, btn.Name.Length - 1),false);
            //else//Load hàng hóa ko phải Combo
            //    Loadhanghoa(smaNhomCombo);
            try
            {
                cboNhomhang.EditValue = int.Parse(btn.Name.Substring(1, btn.Name.Length - 1));
            }
            catch
            {
                cboNhomhang.EditValue = btn.Name.Substring(1, btn.Name.Length - 1);
            }
            smaNhomCombo = btn.Name.Substring(1, btn.Name.Length - 1);
            gridControl2.DataSource = null;//cho lưới chi tiết Combo rỗng
            txtGIABAN1.EditValue = 0;//cho giá Combo = 0
            txtGIABAN2.EditValue = 0;//cho giá Combo = 0
            txtGIABAN3.EditValue = 0;//cho giá Combo = 0
            txtGIABAN4.EditValue = 0;//cho giá Combo = 0
        }

        DataTable dtHanghoa = new DataTable();
        private void LoadDSCombo(string maNhomhang,bool flag)
        {
            if (!flag)
            {
                SetRongControl();
            }
            KhoaMoControl(true);

            string sSQL = "";
           // sSQL += "Select  MA_HANGHOA As MA,ID_HANGHOA,TEN_HANGHOA As TEN,HINHANH,THUE,GIABAN1,MA_NHOMHANG,MA_DONVITINH,IS_COMBO" + "\n";
            sSQL += "Select  MA_HANGHOA As MA,ID_HANGHOA,TEN_HANGHOA As TEN,HINHANH,THUE,GIANHAP,GIABAN1,GIABAN2,GIABAN3,GIABAN4,GHICHU,MAVACH,SUDUNG,SUAGIA,TONKHO,TONTOITHIEU,MA_THANHPHAN,SOLUONG,MA_NHOMHANG,MA_DONVITINH,MA_BEP,STT,IS_COMBO,IS_INBEP" + "\n";
            sSQL += "From HANGHOA" + "\n";
            sSQL += "Where IS_COMBO=1 AND MA_NHOMHANG=" + clsMain.SQLString(maNhomhang) + "\n";
            sSQL += "Order by SUDUNG DESC, STT" + "\n";
            dtHanghoa = clsMain.ReturnDataTable(sSQL);
            fpnlHanghoa.Controls.Clear();
            foreach (DataRow dr in dtHanghoa.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 110;
                btn.Height = 80;
                btn.Font = fpnlHanghoa.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                try
                {
                    btn.ImageLocation = ImageLocation.TopCenter;
                    byte[] tam = new byte[((byte[])dr["HINHANH"]).LongLength];
                    tam = (byte[])dr["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    //btn.Image = bm;
                    btn.BackgroundImage = bm;
                    btn.BackgroundImageLayout = ImageLayout.Stretch;
                    btn.Appearance.BackColor = Color.Transparent;
                    btn.Appearance.Options.UseBackColor = true;
                    btn.ButtonStyle = BorderStyles.NoBorder;
                    btn.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                }
                catch
                {
                }
                btn.Name = "H" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString() + "\n" + String.Format("{0:#,###0}", dr["GIABAN1"]);
                btn.Click += new EventHandler(Combo_Click);
                fpnlHanghoa.Controls.Add(btn);
            }
        }

        private void Combo_Click(object sender, EventArgs e)
        {
            KhoaMoControl(true);
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in fpnlHanghoa.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }
            try
            {
                smaCombo = btn.Name.Substring(1, btn.Name.Length - 1);
                cboNhomhang.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["MA_NHOMHANG"].ToString());
                txtTENCOMBO.Text = dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["TEN"].ToString();
                txtGIABAN1.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["GIABAN1"].ToString());
                txtGIABAN2.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["GIABAN2"].ToString());
                txtGIABAN3.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["GIABAN3"].ToString());
                txtGIABAN4.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["GIABAN4"].ToString());
                cboDonvitinh.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["MA_DONVITINH"].ToString());
                cboThue.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["THUE"].ToString());
                txtMAHANGHOA.Text = dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["ID_HANGHOA"].ToString();
                txtGHICHUHANGHOA.Text = dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["GHICHU"].ToString();
                txtMAVACH.Text = dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["MAVACH"].ToString();
                txtSTTHANGHOA.Text = dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["STT"].ToString();
                chkSUDUNG.Checked = bool.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["SUDUNG"].ToString());
                txtGIAMUA.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["GIANHAP"].ToString());
                chkMayinbep.Checked = bool.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["IS_INBEP"].ToString());
                cboBep.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["MA_BEP"].ToString());
            }
            catch
            {
            }

            LoadGridComboView(smaCombo);

            try
            {
                byte[] tam = new byte[((byte[])dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["HINHANH"]).LongLength];
                tam = (byte[])dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["HINHANH"];
                MemoryStream ms = new MemoryStream(tam);
                Bitmap bm = new Bitmap(ms);
                picHANGHOA.Image = bm;
                picHANGHOA.BackgroundImage = null;
            }
            catch
            {
                picHANGHOA.Image = null;
                picHANGHOA.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            }
        }

        private void LoadGridComboView(string ma)
        {
            KhoaMoControl1(true);

            string SQL = "SELECT c.MA_HANGHOA as MA,TEN_HANGHOA as TEN,C.SOLUONG,GIA as PRICE FROM COMBO C,HANGHOA H WHERE C.MA_HANGHOA=H.MA_HANGHOA AND MA_COMBO=" + clsMain.SQLString(ma);
            DataTable dt = clsMain.ReturnDataTable(SQL);
            BS.DataSource = dt;
            gridControl2.DataSource = BS;

            dtGridhang = dt;
            //gridControl3.Visible = true;
            //gridControl2.Visible = false;
        }
        
        private void LoadNhomhanghoa()
        {
            string sSQL = "";
            sSQL += "Select MA_NHOMHANG As MA,TEN_NHOMHANG As TEN,GHICHU,SUDUNG,THUCDON,STT,HINHANH" + "\n";
            sSQL += "From NHOMHANG" + "\n";
            sSQL += "Order by SUDUNG DESC,STT" + "\n";
            dtNhomhang = clsMain.ReturnDataTable(sSQL);
            fpnlNhomhang.Controls.Clear();
            fpnlHanghoa.Controls.Clear();
            foreach (DataRow dr in dtNhomhang.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 125;
                btn.Height = 80;
                btn.Font = fpnlHanghoa.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                try
                {
                    btn.ImageLocation = ImageLocation.TopCenter;
                    byte[] tam = new byte[((byte[])dr["HINHANH"]).LongLength];
                    tam = (byte[])dr["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    btn.Image = bm;
                }
                catch
                {
                }
                btn.Name = "N" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(Nhomhanghoa_Click);
                fpnlNhomhang.Controls.Add(btn);
            }
            if (fpnlNhomhang.VerticalScroll.Visible)
            {
                palNHOMHANG.Width = 152;
            }
            else
            {
                palNHOMHANG.Width = 135;
            }
        }

        private void Nhomhanghoa_Click(object sender, EventArgs e)//nhóm hàng chỉ dùng để chọn món
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in fpnlNhomhang.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }
            Loadhanghoa(btn.Name.Substring(1, btn.Name.Length - 1));
            smaNhomhang = btn.Name.Substring(1, btn.Name.Length - 1);
            //Loadhanghoa(smaNhomhang);
        }

        private void Loadhanghoa(string maNhomhang)
        {
            //SetRongControl();
            //KhoaMoControl(true);

            string sSQL = "";
            sSQL += "Select  MA_HANGHOA As MA,TEN_HANGHOA As TEN,HINHANH,GIABAN1,MA_NHOMHANG" + "\n";
            sSQL += "From HANGHOA" + "\n";
            sSQL += "Where IS_COMBO=0 AND MA_NHOMHANG=" + clsMain.SQLString(maNhomhang) + "\n";
            sSQL += "Order by SUDUNG DESC, STT" + "\n";
            dtHanghoa = clsMain.ReturnDataTable(sSQL);
            fpnlHanghoa.Controls.Clear();
            foreach (DataRow dr in dtHanghoa.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 130;
                btn.Height = 80;
                btn.Font = fpnlHanghoa.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                try
                {

                    btn.ImageLocation = ImageLocation.TopCenter;
                    byte[] tam = new byte[((byte[])dr["HINHANH"]).LongLength];
                    tam = (byte[])dr["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    //btn.Image = bm;
                    btn.BackgroundImage = bm;
                    btn.BackgroundImageLayout = ImageLayout.Stretch;
                    btn.Appearance.BackColor = Color.Transparent;
                    btn.Appearance.Options.UseBackColor = true;
                    btn.ButtonStyle = BorderStyles.NoBorder;
                    btn.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                }
                catch
                {
                }
                btn.Name = "H" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString() + "\n" + String.Format("{0:#,###0}", dr["GIABAN1"]);
                btn.Click += new EventHandler(Hanghoa_Click);
                fpnlHanghoa.Controls.Add(btn);
            }
        }

        DataTable dtGridhang = new DataTable();
        private void Hanghoa_Click(object sender, EventArgs e)
        {
            //KhoaMoControl(true);
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in fpnlHanghoa.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }

            smaHang = btn.Name.Substring(1, btn.Name.Length - 1);
            int n = dtGridhang.Rows.Count;
            if (flag_doimathang)
            {
                if (gridView2.GetRowCellValue(gridView2.FocusedRowHandle, MA).ToString() == smaHang)
                {
                    XtraMessageBox.Show(_trungmathang, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                Frm_SetCombo_Doimathang frm = new Frm_SetCombo_Doimathang(smaCombo, gridView2.GetRowCellValue(gridView2.FocusedRowHandle, MA).ToString(), int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, SOLUONG).ToString()), smaHang);
                frm.ShowDialog();
            }
            else
            {
                string SQL = "SELECT MA_HANGHOA as MA,TEN_HANGHOA as TEN,(1) as SOLUONG,GIABAN1 as PRICE FROM HANGHOA \n";
                SQL += "WHERE MA_HANGHOA=" + clsMain.SQLString(btn.Name.Substring(1, btn.Name.Length - 1));

                DataTable dt = clsMain.ReturnDataTable(SQL);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < n; i++)
                    {
                        if (dtGridhang.Rows[i]["MA"].ToString() == dt.Rows[0]["MA"].ToString())
                        {
                            XtraMessageBox.Show(_mathangnaydacotrods, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    for (int i = 0; i < n; i++)
                    {
                        if (string.IsNullOrEmpty(dtGridhang.Rows[i]["MA"].ToString()))
                            dtGridhang.Rows.RemoveAt(i);
                    }
                    dtGridhang.Rows.Add(dt.Rows[0]["MA"], dt.Rows[0]["TEN"], dt.Rows[0]["SOLUONG"], dt.Rows[0]["PRICE"]);
                    dtGridhang.Rows.Add(null, null, null, null);
                }
                BS.DataSource = dtGridhang;
                gridControl2.DataSource = BS;
                LoadGiaCombo();
            }
        }

        private void LoadCombo()
        {
            string sSQL = "";
            DataTable dt = new DataTable();
            sSQL += "Select MA_NHOMHANG As MA,TEN_NHOMHANG As TEN" + "\n";
            sSQL += "From NHOMHANG" + "\n";
            sSQL += "Where SUDUNG=1 ";
            sSQL += "Order by  TEN_NHOMHANG ";
            dt = clsMain.ReturnDataTable(sSQL);
            cboNhomhang.Properties.DataSource = dt;
            cboNhomhang.EditValue = cboNhomhang.Properties.GetDataSourceValue(cboNhomhang.Properties.ValueMember, 0);

            sSQL = "";
            sSQL += "Select MA_DONVITINH As MA,TEN_DONVITINH As TEN" + "\n";
            sSQL += "From DONVITINH" + "\n";
            sSQL += "Where SUDUNG=1 ";
            sSQL += "Order by  TEN_DONVITINH ";
            dt = clsMain.ReturnDataTable(sSQL);
            cboDonvitinh.Properties.DataSource = dt;
            cboDonvitinh.EditValue = cboDonvitinh.Properties.GetDataSourceValue(cboDonvitinh.Properties.ValueMember, 0);

            sSQL = "";
            sSQL += "Select A.MA_BEP As MA,A.TEN_BEP As TEN,A.MAYINBEP,A.MA_CUAHANG,B.TEN_CUAHANG" + "\n";
            sSQL += "From DM_BEP A, CUAHANG B" + "\n";
            sSQL += "Where A.MA_CUAHANG=B.MA_CUAHANG" + "\n";
            sSQL += "And A.SUDUNG=1 ";
            sSQL += "Order by  B.TEN_CUAHANG,A.TEN_BEP ";
            dt = clsMain.ReturnDataTable(sSQL);
            cboBep.Properties.DataSource = dt;
            cboBep.EditValue = cboBep.Properties.GetDataSourceValue(cboBep.Properties.ValueMember, 0);
        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//khóa
            {
                cboNhomhang.Properties.ReadOnly = true;
                txtTENCOMBO.Properties.ReadOnly = true;
                cboDonvitinh.Properties.ReadOnly = true;
                txtGHICHUHANGHOA.Properties.ReadOnly = true;
                txtSTTHANGHOA.Properties.ReadOnly = true;
                chkSUDUNG.Properties.ReadOnly = true;
                picHANGHOA.Enabled = false;
                cboThue.Properties.ReadOnly = true;
                txtGIAMUA.Properties.ReadOnly = true;
                txtGIABAN1.Properties.ReadOnly = true;
                txtGIABAN2.Properties.ReadOnly = true;
                txtGIABAN3.Properties.ReadOnly = true;
                txtGIABAN4.Properties.ReadOnly = true;
                txtMAHANGHOA.Properties.ReadOnly = true;
                btnAUTO.Enabled = false;
                cboBep.Properties.ReadOnly = true;
                chkMayinbep.Properties.ReadOnly = true;
                txtMAVACH.Properties.ReadOnly = true;
            }
            else//mở
            {
                cboNhomhang.Properties.ReadOnly = false ;
                txtTENCOMBO.Properties.ReadOnly = false;
                cboDonvitinh.Properties.ReadOnly = false;
                txtGHICHUHANGHOA.Properties.ReadOnly = false;
                txtSTTHANGHOA.Properties.ReadOnly = false;
                chkSUDUNG.Properties.ReadOnly = false;
                picHANGHOA.Enabled = true ;
                cboThue.Properties.ReadOnly = false;
                txtGIAMUA.Properties.ReadOnly = false;
                txtGIABAN1.Properties.ReadOnly = false;
                txtGIABAN2.Properties.ReadOnly = false;
                txtGIABAN3.Properties.ReadOnly = false;
                txtGIABAN4.Properties.ReadOnly = false;
                txtMAHANGHOA.Properties.ReadOnly = false;
                btnAUTO.Enabled = true ;
                cboBep.Properties.ReadOnly = false;
                chkMayinbep.Properties.ReadOnly = false;
                txtMAVACH.Properties.ReadOnly = false;
            }
        }

        private void KhoaMoControl1(bool tf)
        {
            if (tf == true)//khóa
            {
                gridView2.OptionsBehavior.ReadOnly = true;
            }
            else//mở
            {

                gridView2.OptionsBehavior.ReadOnly = false;
            }
        }

        private void SetRongControl()
        {
            smaCombo = "";
            cboNhomhang.EditValue = (smaNhomhang != "") ? int.Parse(smaNhomhang) : cboNhomhang.Properties.GetDataSourceValue(cboNhomhang.Properties.ValueMember, 0);
            cboDonvitinh.EditValue = cboDonvitinh.Properties.GetDataSourceValue(cboDonvitinh.Properties.ValueMember, 0);
            cboThue.EditValue = 0;
            txtMAHANGHOA.Text = "";
            txtTENCOMBO.Text = "";
            txtGHICHUHANGHOA.Text = "";
            txtSTTHANGHOA.EditValue = 1;
            chkSUDUNG.Checked = true;
            picHANGHOA.Image = null;
            picHANGHOA.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            txtGIAMUA.EditValue = 0;
            txtGIABAN1.EditValue = 0;
            txtGIABAN2.EditValue = 0;
            txtGIABAN3.EditValue = 0;
            txtGIABAN4.EditValue = 0;
            cboBep.EditValue = cboBep.Properties.GetDataSourceValue(cboBep.Properties.ValueMember, 0);
            chkMayinbep.Checked = false;
            txtMAVACH.Text = "";
        }

        private void LoadPermission()
        {
            btnThemCombo.Enabled = clsUserManagement.AllowAdd("6");
            btnSuaCombo.Enabled = clsUserManagement.AllowEdit("6");
            btnXoaCombo.Enabled = clsUserManagement.AllowDelete("6");
            btnLuuCombo.Enabled = btnThemCombo.Enabled || btnSuaCombo.Enabled;

            btnThemChitietCombo.Enabled = clsUserManagement.AllowAdd("6");
            btnSuaChitietCombo.Enabled = clsUserManagement.AllowEdit("6");
            btnHuyChitietCombo.Enabled = clsUserManagement.AllowDelete("6");
            btnLuuChitietCombo.Enabled = btnThemChitietCombo.Enabled || btnSuaChitietCombo.Enabled;
        }

        private void btnCapnhatGV_Click(object sender, EventArgs e)
        {
            LoadGridview();
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT1 & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;

            KhoaMoControl(true);
            smaCombo = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MA").ToString();
            cboNhomhang.EditValue = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MA_NHOMHANG").ToString();
            txtTENCOMBO.Text = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TEN").ToString();
            txtGIABAN1.Text = String.Format("{0:#,###0}", gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GIABAN1"));
            txtGIABAN2.Text = String.Format("{0:#,###0}", gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GIABAN2"));
            txtGIABAN3.Text = String.Format("{0:#,###0}", gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GIABAN3"));
            txtGIABAN4.Text = String.Format("{0:#,###0}", gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GIABAN4"));
            LoadGridComboView(smaCombo);

            try
            {
                byte[] tam = new byte[((byte[])gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "HINHANH")).LongLength];
                tam = (byte[])gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "HINHANH");
                MemoryStream ms = new MemoryStream(tam);
                Bitmap bm = new Bitmap(ms);
                picHANGHOA.Image = bm;
                picHANGHOA.BackgroundImage = null;
            }
            catch
            {
                picHANGHOA.Image = null;
                picHANGHOA.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            }
        }

        private void SetDulieuKhoiTao()
        {
            DataTable DTSOUCRE = new DataTable();
            DTSOUCRE.Columns.Add("MA", typeof(string));
            DTSOUCRE.Columns.Add("TEN", typeof(string));
            DTSOUCRE.Columns.Add("SOLUONG", typeof(float));
            DTSOUCRE.Columns.Add("PRICE", typeof(float));
            DTSOUCRE.Rows.Add(null, null, null, null);
            BS.DataSource = DTSOUCRE;
            gridControl2.DataSource = BS;
            dtGridhang = DTSOUCRE;
            //dtGridhang.Rows.Clear();
            
        }

        private void LoadGridCOMBO()
        {
            cboHANGHOA.Columns.Clear();
            DataTable dt = clsMain.ReturnDataTable("SELECT MA_HANGHOA,TEN_HANGHOA,GIABAN1 FROM HANGHOA h,DONVITINH d WHERE h.MA_DONVITINH=d.MA_DONVITINH AND IS_COMBO=0 AND MA_HANGHOA!=" + clsMain.SQLString(smaCombo) + " ORDER BY TEN_HANGHOA");
            if (dt.Rows.Count > 0)
            {
                cboHANGHOA.DataSource = dt;
                string[] displayname = new string[] { "Mã hàng", "Tên hàng", "GIABAN1"};
                string[] hidename = new string[] { "MA_HANGHOA", "TEN_HANGHOA", "GIABAN1" };
                int[] width = new int[] { 100, 300, 20 };
                bool[] visible = new bool[] { true, true, false};
                for (int i = 0; i < displayname.Length; i++)
                {
                    LookUpColumnInfo info = new LookUpColumnInfo();
                    info.FieldName = hidename[i];
                    info.Caption = displayname[i];
                    info.Visible = visible[i];
                    info.Width = width[i];
                    cboHANGHOA.Columns.Add(info);
                }
            }
        }

        private void cboHANGHOA_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DevExpress.XtraEditors.LookUpEdit editor = (sender as DevExpress.XtraEditors.LookUpEdit);
                gridView2.SetRowCellValue(gridView2.FocusedRowHandle, "MA", editor.GetColumnValue("MA_HANGHOA").ToString());
                gridView2.SetRowCellValue(gridView2.FocusedRowHandle, "TEN", editor.GetColumnValue("TEN_HANGHOA").ToString());
                gridView2.SetRowCellValue(gridView2.FocusedRowHandle, "SOLUONG", 1);
                gridView2.SetRowCellValue(gridView2.FocusedRowHandle, "PRICE", editor.GetColumnValue("GIABAN1").ToString());
                LoadGiaCombo();
                //DataTable dttest = dtGridhang;
                
                
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cboHANGHOA_Leave(object sender, EventArgs e)
        {
            DataTable dttmp = (DataTable)Frm_SetCombo.BS.DataSource;
            if (dttmp.Rows.Count == 1)
            {
                if (dttmp.Rows[0]["MA"].ToString() != "")
                    dttmp.Rows.Add(null, null, null, null);
            }
            else
            {
                if (dttmp.Rows[dttmp.Rows.Count - 1]["MA"].ToString() == "")
                {
                }
                else
                    dttmp.Rows.Add(null, null, null, null);
            }

            //if (gridView2.RowCount == 1)
            //{
            //    if (gridView2.GetRowCellValue(0, "MA").ToString() != "")
            //        gridView2.AddNewRow();
            //}
            //else
            //{
            //    if (gridView2.GetRowCellValue(gridView2.RowCount - 1, "MA").ToString() == "")
            //    {
            //    }
            //    else
            //        gridView2.AddNewRow();
            //}
        }

        private void gridView2_ShownEditor(object sender, EventArgs e)
        {
            BeginInvoke(new Action(() =>
            {
                gridView2.ActiveEditor.SelectAll();
            }));
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STTCombo & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void txtSOLUONGHANG_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                //sự kiện Keypress cho cột SL,Giá Nhập
                if (gridView2.FocusedRowHandle >= 0)
                {
                    if (string.IsNullOrEmpty(gridView2.GetFocusedRowCellValue("MA").ToString()))
                        e.Handled = true;
                    else
                    {
                        if (!char.IsNumber(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsPunctuation(e.KeyChar) & e.KeyChar != '-')
                            e.Handled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtGIA_EditValueChanging(object sender, ChangingEventArgs e)
        {
            try
            {
                float convertam;
                float convertam1;
                #region values = null,0,không phải số
                if (string.IsNullOrEmpty(e.NewValue.ToString()) || e.NewValue.Equals("") || float.Parse(e.NewValue.ToString()) == 0 || !float.TryParse(e.NewValue.ToString(), out convertam1))
                {
                    txtGIA.EditValueChanging -= txtGIA_EditValueChanging;
                    DataRowView dv = (DataRowView)BS.Current;
                    dv["PRICE"] = 0;
                    e.NewValue = 0;
                    LoadGiaCombo();

                    BS.ResetCurrentItem();
                }
                #endregion
                #region values la số # 0
                else if (float.TryParse(e.NewValue.ToString(), out convertam))
                {
                    //MessageBox.Show(gridView2.GetRowCellValue(0, PRICE).ToString());
                    if (float.Parse(e.NewValue.ToString()) != 0)
                    {
                        txtGIA.EditValueChanging -= txtGIA_EditValueChanging;
                        DataRowView dv = (DataRowView)BS.Current;

                        dv["PRICE"] = e.NewValue;
                        LoadGiaCombo();
                        //MessageBox.Show(gridView2.GetRowCellValue(0, PRICE).ToString());
                        BS.ResetCurrentItem();
                    }
                    //MessageBox.Show(gridView2.GetRowCellValue(0, PRICE).ToString());
                }
                #endregion
                txtGIA.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(txtGIA_EditValueChanging);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void LoadGiaCombo()
        {
            float GiaCombo = 0;
            int n = gridView2.RowCount;
            for (int i = 0; i < n; i++)
            {
                if (string.IsNullOrEmpty(gridView2.GetRowCellValue(i, "MA").ToString()))
                {
                }
                else
                {
                    GiaCombo = GiaCombo + (float.Parse(gridView2.GetRowCellValue(i, "SOLUONG").ToString()) * float.Parse(gridView2.GetRowCellValue(i, "PRICE").ToString()));
                }
            }
           // txtGIABAN1.Text = string.Format("{0:#,###0}", GiaCombo);
        }

        private void txtGIA_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                //sự kiện Keypress cho cột SL,Giá Nhập
                if (gridView2.FocusedRowHandle >= 0)
                {
                    if (string.IsNullOrEmpty(gridView2.GetFocusedRowCellValue("MA").ToString()))
                        e.Handled = true;
                    else
                    {
                        if (!char.IsNumber(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsPunctuation(e.KeyChar) & e.KeyChar != '-')
                            e.Handled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtSOLUONGHANG_EditValueChanging(object sender, ChangingEventArgs e)
        {
            try
            {
                float convertam;
                float convertam1;
                #region values = null,0,không phải số
                if (string.IsNullOrEmpty(e.NewValue.ToString()) || e.NewValue.Equals("") || float.Parse(e.NewValue.ToString()) == 0 || !float.TryParse(e.NewValue.ToString(), out convertam1))
                {
                    txtSOLUONGHANG.EditValueChanging -= txtSOLUONGHANG_EditValueChanging;
                    DataRowView dv = (DataRowView)BS.Current;
                    dv["SOLUONG"] = 1;
                    e.NewValue = 1;
                    LoadGiaCombo();

                    BS.ResetCurrentItem();
                }
                #endregion
                #region values la số # 0
                else if (float.TryParse(e.NewValue.ToString(), out convertam))
                {
                    if (float.Parse(e.NewValue.ToString()) != 0)
                    {
                        txtSOLUONGHANG.EditValueChanging -= txtSOLUONGHANG_EditValueChanging;
                        DataRowView dv = (DataRowView)BS.Current;

                        dv["SOLUONG"] = e.NewValue;
                        LoadGiaCombo();

                        BS.ResetCurrentItem();
                    }
                }
                #endregion
                txtSOLUONGHANG.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(txtSOLUONGHANG_EditValueChanging);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnLuuCombo_Click(object sender, EventArgs e)
        {
            if (!CheckInput1())
            {
                return;
            }
            string SQL = "";
            DataTable dt = new DataTable();
            Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(picHANGHOA.Image, typeof(Byte[]));
            SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
            SqlCommand SqlComMain = null;
            string sSQL = "";
            if (smaCombo == "" && btnThemCombo.Enabled)
            {
                sSQL += "Insert into HANGHOA (TEN_HANGHOA,HINHANH,ID_HANGHOA,THUE,GIANHAP,GIABAN1,GIABAN2,GIABAN3,GIABAN4,GHICHU,MAVACH,SUDUNG,SUAGIA,TONKHO,TONTOITHIEU,MA_THANHPHAN,SOLUONG,MA_NHOMHANG,MA_DONVITINH,MA_BEP,STT,IS_INBEP,IS_COMBO) Values (@TEN_HANGHOA,@HINHANH,@ID_HANGHOA,@THUE,@GIANHAP,@GIABAN1,@GIABAN2,@GIABAN3,@GIABAN4,@GHICHU,@MAVACH,@SUDUNG,@SUAGIA,@TONKHO,@TONTOITHIEU,@MA_THANHPHAN,@SOLUONG,@MA_NHOMHANG,@MA_DONVITINH,@MA_BEP,@STT,@IS_INBEP,@IS_COMBO)" + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);

                SqlCom.Parameters.Add(new SqlParameter("@TEN_HANGHOA", (object)txtTENCOMBO.Text));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlCom.Parameters.Add(new SqlParameter("@ID_HANGHOA", (object)txtMAHANGHOA.Text));
                SqlCom.Parameters.Add(new SqlParameter("@THUE", (object)cboThue.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIANHAP", (object)txtGIAMUA.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN1", (object)txtGIABAN1.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN2", (object)txtGIABAN2.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN3", (object)txtGIABAN3.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN4", (object)txtGIABAN4.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txtGHICHUHANGHOA.Text));
                SqlCom.Parameters.Add(new SqlParameter("@MAVACH", (object)txtMAVACH.Text));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSUDUNG.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@SUAGIA", (object)false));
                SqlCom.Parameters.Add(new SqlParameter("@TONKHO", (object)false));
                SqlCom.Parameters.Add(new SqlParameter("@TONTOITHIEU", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@MA_THANHPHAN", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@SOLUONG", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@MA_NHOMHANG", (object)cboNhomhang.EditValue));
                SqlCom.Parameters.Add(new SqlParameter("@MA_DONVITINH", (object)cboDonvitinh.EditValue));
                SqlCom.Parameters.Add(new SqlParameter("@MA_BEP", (object)cboBep.EditValue));
                SqlCom.Parameters.Add(new SqlParameter("@STT", (object)txtSTTHANGHOA.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@IS_INBEP", (object)chkMayinbep.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@IS_COMBO", (object)true ));
                SqlComMain = SqlCom;
            }
            else if (smaCombo != "" && btnSuaCombo.Enabled)
            {
                sSQL += "Update HANGHOA Set TEN_HANGHOA=@TEN_HANGHOA,ID_HANGHOA=@ID_HANGHOA,HINHANH=@HINHANH,THUE=@THUE,GIANHAP=@GIANHAP,GIABAN1=@GIABAN1,GIABAN2=@GIABAN2,GIABAN3=@GIABAN3,GIABAN4=@GIABAN4,GHICHU=@GHICHU,MAVACH=@MAVACH,SUDUNG=@SUDUNG,SUAGIA=@SUAGIA,TONKHO=@TONKHO,TONTOITHIEU=@TONTOITHIEU,MA_THANHPHAN=@MA_THANHPHAN,SOLUONG=@SOLUONG,MA_NHOMHANG=@MA_NHOMHANG,MA_DONVITINH=@MA_DONVITINH,MA_BEP=@MA_BEP,STT=@STT,IS_INBEP=@IS_INBEP,IS_COMBO=@IS_COMBO " + "\n";
                sSQL += "Where MA_HANGHOA=" + clsMain.SQLString(smaCombo) + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);

                SqlCom.Parameters.Add(new SqlParameter("@TEN_HANGHOA", (object)txtTENCOMBO.Text));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlCom.Parameters.Add(new SqlParameter("@ID_HANGHOA", (object)txtMAHANGHOA.Text));
                SqlCom.Parameters.Add(new SqlParameter("@THUE", (object)cboThue.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIANHAP", (object)txtGIAMUA.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN1", (object)txtGIABAN1.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN2", (object)txtGIABAN2.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN3", (object)txtGIABAN3.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN4", (object)txtGIABAN4.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txtGHICHUHANGHOA.Text));
                SqlCom.Parameters.Add(new SqlParameter("@MAVACH", (object)txtMAVACH.Text));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSUDUNG.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@SUAGIA", (object)false));
                SqlCom.Parameters.Add(new SqlParameter("@TONKHO", (object)false));
                SqlCom.Parameters.Add(new SqlParameter("@TONTOITHIEU", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@MA_THANHPHAN", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@SOLUONG", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@MA_NHOMHANG", (object)cboNhomhang.EditValue));
                SqlCom.Parameters.Add(new SqlParameter("@MA_DONVITINH", (object)cboDonvitinh.EditValue));
                SqlCom.Parameters.Add(new SqlParameter("@MA_BEP", (object)cboBep.EditValue));
                SqlCom.Parameters.Add(new SqlParameter("@STT", (object)txtSTTHANGHOA.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@IS_INBEP", (object)chkMayinbep.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@IS_COMBO", (object)true));
                SqlComMain = SqlCom;
            }
            else
            {
                SqlComMain = null;
            }

            Boolean bRunSQL = true;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                CN.Open();
                SqlComMain.ExecuteNonQuery();
                CN.Close();
                this.Cursor = Cursors.Default;
                bRunSQL = true;
            }
            catch
            {
                bRunSQL = false;
            }

            if (bRunSQL)
            {
                if (smaCombo == "")
                {
                    SQL = "Select max(MA_HANGHOA) From HANGHOA";
                    dt = clsMain.ReturnDataTable(SQL);
                    smaCombo = dt.Rows[0][0].ToString();
                }
                if (palHANGHOA.Visible && palNHOMHANG.Visible)//palnel đang mở là Combo chitiet
                {
                    LoadDSCombo(smaNhomCombo, true);
                    LoadCombo();
                }
                else if (palLIETKE.Visible)
                {
                    LoadGridview();
                }
                else 
                {
                    LoadComboIcon();
                }
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private Boolean CheckInput1()
        {
            if (txtTENCOMBO.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_nhap + lblTENHANGHOA.Text, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTENCOMBO.Focus();
                return false;
            }
            if (txtMAHANGHOA.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_nhapmahanghoa, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMAHANGHOA.Focus();
                return false;
            }
            if (smaCombo == "" && btnThemCombo.Enabled && txtMAHANGHOA.Text != "" && clsMain.ReturnDataTable("Select MA_HANGHOA From HANGHOA Where ID_HANGHOA=" + clsMain.SQLString(txtMAHANGHOA.Text)).Rows.Count > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_trungmahh, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMAHANGHOA.Focus();
                return false;
            }
            if (smaCombo != "" && btnSuaCombo.Enabled && txtMAHANGHOA.Text != "" && clsMain.ReturnDataTable("Select MA_HANGHOA From HANGHOA Where ID_HANGHOA=" + clsMain.SQLString(txtMAHANGHOA.Text) + "And MA_HANGHOA!= " + clsMain.SQLString(smaCombo)).Rows.Count > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_trungmahh, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMAHANGHOA.Focus();
                return false;
            }
            if (smaCombo == "" && btnThemCombo.Enabled && txtTENCOMBO.Text != "" && clsMain.ReturnDataTable("Select MA_HANGHOA From HANGHOA Where TEN_HANGHOA=" + clsMain.SQLStringUnicode(txtTENCOMBO.Text)).Rows.Count > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show(_trungtenhanghoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    txtTENCOMBO.Focus();
                    return false;
                }
            }
            if (smaCombo != "" && btnSuaCombo.Enabled && txtTENCOMBO.Text != "" && clsMain.ReturnDataTable("Select MA_HANGHOA From HANGHOA Where TEN_HANGHOA=" + clsMain.SQLStringUnicode(txtTENCOMBO.Text) + " And MA_HANGHOA!= " + clsMain.SQLString(smaCombo)).Rows.Count > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show(_trungtenhanghoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    txtTENCOMBO.Focus();
                    return false;
                }
            }
            return true;
        }
        private void btnHuyChitietCombo_Click(object sender, EventArgs e)
        {
            if (smaCombo == "" || txtTENCOMBO.Text == "")//Chưa chọn dòng để xóa
            {
                XtraMessageBox.Show(_chuachoncombo, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (gridView2.RowCount <= 0)//Không có dữ liệu để sữa
            {
                XtraMessageBox.Show(_khongcodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show(_xacnhanbanmuonxoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string SQL = string.Empty;
            SQL  = "UPDATE HANGHOA SET GIABAN1=0 WHERE MA_HANGHOA=" + clsMain.SQLString(smaCombo) + " \n";
            SQL += "DELETE COMBO WHERE MA_COMBO=" + clsMain.SQLString(smaCombo) + " \n";
            if (clsMain.ExecuteSQL(SQL))
            {
                smaCombo = "";
                txtGIABAN1.Text = "0";
                txtGIABAN2.Text = "0";
                txtGIABAN3.Text = "0";
                txtGIABAN4.Text = "0";
                if (palNHOMHANG.Visible && palHANGHOA.Visible)
                {
                    LoadDSCombo(smaNhomCombo,false);
                }
                else if (palLIETKE.Visible)
                {
                    LoadGridview();
                }
                gridControl2.DataSource = null;
                //LoadNhomCombo();
                //LoadDATAGridview();
            }
            else
            {
                XtraMessageBox.Show(_xoakhongthanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnThemCombo_Click(object sender, EventArgs e)
        {
            SetRongControl();
            SetRongCombo();
            btnAUTO_Click(null, null);
            btnAutoMaVach_Click(null, null);
            KhoaMoControl(false);
            cboNhomhang.Focus();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (smaNhomCombo == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl(false);
            cboNhomhang.Focus();
        }

        private void btnSuaChitietCombo_Click(object sender, EventArgs e)
        {
            if (!gridView2.OptionsBehavior.ReadOnly)
            {
                return;
            }
            if (txtTENCOMBO.Text.Trim() == "")
            {
                XtraMessageBox.Show(_chuachoncombo, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (gridView2.RowCount <= 0)//Không có dữ liệu để sữa
            {
                XtraMessageBox.Show(_khongcodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (palHANGHOA.Visible && palNHOMHANG.Visible)//palnel đang mở là Combo chitiet
                flag = 0;
            else if (palLIETKE.Visible) //palnel đang mở là Combo lưới
            {
                flag = 1;
                KhoaMoPanel(0);
            }
            else //palnel đang mở là Combo Icon
            {
                flag = 2;
                KhoaMoPanel(0);
            }
            KhoaMoControl(true);//Tên,nhóm Combo không được sửa nữa
            KhoaMoControl1(false);

            if(!string.IsNullOrEmpty(gridView2.GetRowCellValue(gridView2.RowCount-1,"MA").ToString()))
                dtGridhang.Rows.Add(null, null, null, null);

            BS.DataSource = dtGridhang;
            gridControl2.DataSource = BS;//đổ thêm một dòng rỗng cho lưới

            if (fpnlNhomhang.Controls.Count > 0)
            {

                Nhomhanghoa_Click(fpnlNhomhang.Controls[0], null);
            }
            flag_doimathang = false;
            LoadGridCOMBO();
            LoadNhomhanghoa();
            Loadhanghoa(smaNhomhang);
        }

        private void btnLuuChitietCombo_Click(object sender, EventArgs e)
        {
            if (gridView2.OptionsBehavior.ReadOnly)
            {
                return;
            }
            if (smaCombo == "" || txtTENCOMBO.Text == "")
            {
                XtraMessageBox.Show(_khongcocombo, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrEmpty(gridView2.GetRowCellValue(0, "MA").ToString()))
            {
                XtraMessageBox.Show(_khongcodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            string SQL = string.Empty;
            SQL = "SELECT DISTINCT MA_COMBO FROM COMBO WHERE MA_COMBO=" + clsMain.SQLString(smaCombo);
            DataTable dt = clsMain.ReturnDataTable(SQL);
            if (dt.Rows.Count > 0)
            {
                SQL = "DELETE COMBO WHERE MA_COMBO=" + clsMain.SQLString(smaCombo);
                clsMain.ExecuteSQL(SQL);
            }

            //SQL = "UPDATE HANGHOA SET GIABAN1=" + clsMain.SQLString(txtGIABAN1.Text.Replace(",", ""))+" ,GIABAN2=" + clsMain.SQLString(txtGIABAN1.Text.Replace(",", ""))+" ,GIABAN3=" + clsMain.SQLString(txtGIABAN3.Text.Replace(",", ""))+" ,GIABAN4=" + clsMain.SQLString(txtGIABAN4.Text.Replace(",", "")) + " WHERE MA_HANGHOA=" + clsMain.SQLString(smaCombo) + " \n";
            SQL = "";
            for (int i = 0; i < gridView2.RowCount; i++)
            {
                if (string.IsNullOrEmpty(gridView2.GetRowCellValue(i, "MA").ToString()))
                {
                }
                else
                {
                    SQL += "INSERT INTO COMBO (MA_COMBO,MA_HANGHOA,SOLUONG,GIA) \n";
                    SQL += "VALUES(";
                    SQL += clsMain.SQLString(smaCombo) + ",";
                    SQL += clsMain.SQLString(gridView2.GetRowCellValue(i, "MA").ToString()) + ",";
                    SQL += clsMain.SQLString(gridView2.GetRowCellValue(i, "SOLUONG").ToString()) + ",";
                    SQL += clsMain.SQLString(gridView2.GetRowCellValue(i, "PRICE").ToString()) + ") \n";
                }
            }
            if (clsMain.ExecuteSQL(SQL))
            {
                if (flag == 0)//chọn panel Combo chitiet
                {
                    KhoaMoPanel(0);
                }
                else if (flag == 1)//chọn panel Combo lưới
                {
                    KhoaMoPanel(1);
                }
                else    //chọn panel Combo Icon
                {
                    KhoaMoPanel(2);
                }

                if (palNHOMHANG.Visible && palHANGHOA.Visible)
                {
                    LoadNhomCombo();
                    LoadDSCombo(smaNhomCombo,false);
                }
                else if (palLIETKE.Visible)
                {
                    LoadGridview();
                }
                else
                {
                    LoadComboIcon();
                }

                LoadCombo();
                gridControl2.DataSource = null;
                txtGIABAN1.EditValue = 0;
                txtGIABAN2.EditValue = 0;
                txtGIABAN3.EditValue = 0;
                txtGIABAN4.EditValue = 0;
                //if (btnLIETKE.Text == "Xem chi tiết")
                //    LoadDATAGridview();
                XtraMessageBox.Show(_taocombothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void picHANGHOA_Click(object sender, EventArgs e)
        {
            if (picHANGHOA.Enabled)
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = Application.StartupPath + "\\Images";
                openFileDialog1.Filter = "Image Files|*.jpg;*.bmp;*.png;*.gif|All Files|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.Title = "Chọn ảnh";
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        picHANGHOA.ImageLocation = openFileDialog1.FileName;
                        picHANGHOA.BackgroundImage = null;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {
                        picHANGHOA.Image = null;
                        picHANGHOA.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void btnThemChitietCombo_Click(object sender, EventArgs e)
        {
            if (txtTENCOMBO.Text.Trim() == "")
            {
                XtraMessageBox.Show(_chuachoncombo, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (palHANGHOA.Visible && palNHOMHANG.Visible)//palnel đang mở là Combo chitiet
                flag = 0;
            else if (palLIETKE.Visible) //palnel đang mở là Combo lưới
            {
                flag = 1;
                KhoaMoPanel(0);
            }
            else //palnel đang mở là Combo Icon
            {
                flag = 2;
                KhoaMoPanel(0);
            }
            KhoaMoControl(true);//Tên,nhóm Combo không được sửa nữa
            KhoaMoControl1(false);
            SetDulieuKhoiTao();
            if (fpnlNhomhang.Controls.Count > 0)
            {
                Nhomhanghoa_Click(fpnlNhomhang.Controls[0], null);
            }
            flag_doimathang = false;
            LoadGridCOMBO();
            LoadNhomhanghoa();
            Loadhanghoa(smaNhomhang);
            txtGIABAN1.EditValue = 0;
            txtGIABAN2.EditValue = 0;
            txtGIABAN3.EditValue = 0;
            txtGIABAN4.EditValue = 0;
        }

        private void btnXoadong_Click(object sender, EventArgs e)
        {
            if (gridView2.OptionsBehavior.ReadOnly)
            {
                return;
            }
            if (gridView2.RowCount <= 1)//Không có dữ liệu để xóa
            {
                XtraMessageBox.Show(_khongcodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (gridView2.FocusedRowHandle < 0)
            {
                XtraMessageBox.Show(_chuachondongdexoa, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            int n = dtGridhang.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                if (dtGridhang.Rows[i]["MA"].ToString() == gridView2.GetFocusedRowCellValue("MA").ToString() && !string.IsNullOrEmpty(dtGridhang.Rows[i]["MA"].ToString()))
                {
                    dtGridhang.Rows.RemoveAt(i);
                    break;
                }
            }
            BS.DataSource = dtGridhang;
            gridControl2.DataSource = BS;
            LoadGiaCombo();
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            if (flag == 0)//chọn panel Combo chitiet
            {
                KhoaMoPanel(0);
            }
            else if (flag == 1)//chọn panel Combo lưới
            {
                KhoaMoPanel(1);
            }
            else    //chọn panel Combo Icon
            {
                KhoaMoPanel(2);
            }

            if (palNHOMHANG.Visible && palHANGHOA.Visible)//chọn panel Combo chi tiết
            {
                LoadNhomCombo();
                LoadDSCombo(smaNhomCombo,false);
            }
            else if (palLIETKE.Visible)//chọn panel Combo lưới
            {
                LoadGridview();
            }
            else    //chọn panel Combo Icon
            {
                LoadComboIcon();
            }

            LoadCombo();
            gridControl2.DataSource = null;
            txtGIABAN1.EditValue = 0;
            txtGIABAN2.EditValue = 0;
            txtGIABAN3.EditValue = 0;
            txtGIABAN4.EditValue = 0;
        }

        private void btnXoaCombo_Click(object sender, EventArgs e)
        {
            if (smaCombo == "" || txtTENCOMBO.Text == "")//Chưa chọn dòng để xóa
            {
                XtraMessageBox.Show(_chuachoncombo, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show(_xacnhanbanmuonxoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string SQL = string.Empty;
            SQL  = "DELETE HANGHOA WHERE MA_HANGHOA=" + clsMain.SQLString(smaCombo) + " \n";
            SQL += "DELETE COMBO WHERE MA_COMBO=" + clsMain.SQLString(smaCombo) + " \n";
            if (clsMain.ExecuteSQL(SQL))
            {
                smaCombo = "";
                txtGIABAN1.Text = "0";
                txtGIABAN2.Text = "0";
                txtGIABAN3.Text = "0";
                txtGIABAN4.Text = "0";
                gridControl2.DataSource = null;

                if (palHANGHOA.Visible && palNHOMHANG.Visible)//palnel đang mở là Combo chitiet
                {
                    LoadDSCombo(smaNhomCombo, false);
                    LoadCombo();
                }
                else if (palLIETKE.Visible)
                {
                    LoadGridview();
                }
                else
                {
                    LoadComboIcon();
                }
            }
            else
            {
                XtraMessageBox.Show(_xoakhongthanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void LoadGridview()
        {
            string SQL = string.Empty;
            SQL = "Select  MA_HANGHOA As MA,TEN_HANGHOA As TEN,GIABAN1,TEN_NHOMHANG,H.MA_NHOMHANG,MA_DONVITINH,H.HINHANH,IS_COMBO" + "\n";
            SQL += "From HANGHOA H,NHOMHANG N" + "\n";
            SQL += "Where H.MA_NHOMHANG=N.MA_NHOMHANG AND IS_COMBO=1 \n";
            DataTable dt = clsMain.ReturnDataTable(SQL);
            gridControl1.DataSource = dt;
            gridView1.Columns["TEN_NHOMHANG"].GroupIndex = 0;
            gridView1.ExpandAllGroups();
        }

        private void KhoaMoPanel(int tf)
        {
            if (tf == 0)//Combo chi tiết
            {
                palHANGHOA.Visible = true;
                palNHOMHANG.Visible = true;
                palLIETKE.Visible = false;
                palComboIcon.Visible = false;
            }
            else if (tf == 1)//Combo lưới
            {
                palLIETKE.Visible = true;
                palHANGHOA.Visible = false;
                palNHOMHANG.Visible = false;
                palComboIcon.Visible = false;
            }
            else //Combo Icon
            {
                palComboIcon.Visible = true;
                palLIETKE.Visible = false;
                palHANGHOA.Visible = false;
                palNHOMHANG.Visible = false;
            }
        }

        private void LoadComboIcon()
        {
            SetRongControl();
            KhoaMoControl(true);

            string SQL = "";
            SQL = "Select  MA_HANGHOA As MA,TEN_HANGHOA As TEN,GIABAN1,TEN_NHOMHANG,H.MA_NHOMHANG,MA_DONVITINH,H.HINHANH,IS_COMBO" + "\n";
            SQL += "From HANGHOA H,NHOMHANG N" + "\n";
            SQL += "Where H.MA_NHOMHANG=N.MA_NHOMHANG AND IS_COMBO=1 \n";
            dtHanghoa = clsMain.ReturnDataTable(SQL);
            fpnlCombo.Controls.Clear();
            foreach (DataRow dr in dtHanghoa.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 110;
                btn.Height = 80;
                btn.Font = fpnlCombo.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                try
                {
                    btn.ImageLocation = ImageLocation.TopCenter;
                    byte[] tam = new byte[((byte[])dr["HINHANH"]).LongLength];
                    tam = (byte[])dr["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    btn.Image = bm;
                }
                catch
                {
                }
                btn.Name = "H" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString() + "\n" + String.Format("{0:#,###0}", dr["GIABAN1"]);
                btn.Click += new EventHandler(ComboIcon_Click);
                fpnlCombo.Controls.Add(btn);
            }
        }

        private void ComboIcon_Click(object sender, EventArgs e)
        {
            KhoaMoControl(true);
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in fpnlCombo.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }

            smaCombo = btn.Name.Substring(1, btn.Name.Length - 1);
            cboNhomhang.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["MA_NHOMHANG"].ToString());
            txtTENCOMBO.Text = dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["TEN"].ToString();
            txtGIABAN1.Text = String.Format("{0:#,###0}", dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["GIABAN1"]);
            txtGIABAN2.Text = String.Format("{0:#,###0}", dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["GIABAN2"]);
            txtGIABAN3.Text = String.Format("{0:#,###0}", dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["GIABAN3"]);
            txtGIABAN4.Text = String.Format("{0:#,###0}", dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["GIABAN4"]);
            LoadGridComboView(smaCombo);

            try
            {
                byte[] tam = new byte[((byte[])dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["HINHANH"]).LongLength];
                tam = (byte[])dtHanghoa.Select("MA=" + clsMain.SQLString(smaCombo))[0]["HINHANH"];
                MemoryStream ms = new MemoryStream(tam);
                Bitmap bm = new Bitmap(ms);
                picHANGHOA.Image = bm;
                picHANGHOA.BackgroundImage = null;
            }
            catch
            {
                picHANGHOA.Image = null;
                picHANGHOA.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            }
        }

        private void btnComboChitiet_Click(object sender, EventArgs e)
        {
            KhoaMoPanel(0);
            SetRongCombo();
            LoadNhomCombo();
            LoadDSCombo(smaNhomCombo,false);
        }

        private void btnComboGrid_Click(object sender, EventArgs e)
        {
            KhoaMoPanel(1);
            SetRongCombo();
            LoadGridview();
        }

        private void btnComboIcon_Click(object sender, EventArgs e)
        {
            KhoaMoPanel(2);
            SetRongCombo();
            LoadComboIcon();
        }
       
        private void SetRongCombo()
        {
            smaCombo = "";
            txtTENCOMBO.Text = "";
            txtGIABAN1.EditValue = 0;
            txtGIABAN2.EditValue = 0;
            txtGIABAN3.EditValue = 0;
            txtGIABAN4.EditValue = 0;
            gridControl2.DataSource = null;
        }

        private void btnAUTO_Click(object sender, EventArgs e)
        {
            txtMAHANGHOA.Text = clsMain.ReturnDataTable("SELECT [dbo].[fc_NewCodeID_HANGHOA]  ()").Rows[0][0].ToString();
        }

        private void btnc_Click(object sender, EventArgs e)
        {
            panelControl12.Visible = true;
            panelControl15.Visible = false;
        }

        private bool checkinput()
        {
            if (gridView2.FocusedRowHandle >= 0)
                return true;
            else
                return false;
        }

        bool flag_doimathang = false;
        private void btnDoimathang_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0)
            {
                XtraMessageBox.Show(_khongcodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (fpnlNhomhang.Controls.Count > 0)
            {

                Nhomhanghoa_Click(fpnlNhomhang.Controls[0], null);
            }
            flag_doimathang = true;
            LoadGridCOMBO();
            LoadNhomhanghoa();
            Loadhanghoa(smaNhomhang);
        }

        private void btnNangcao_Click(object sender, EventArgs e)
        {
            panelControl12.Visible = false;
            panelControl15.Visible = true;
        }

        private void btnXemDS_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0)
            {
                XtraMessageBox.Show(_khongcodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            string sSQL = "";
            sSQL += "SELECT TEN_HANGHOA as TEN,B.SOLUONG,B.GIATHEM" + "\n";
            sSQL += "FROM HANGHOA A,COMBO_OPTION B" + "\n";
            sSQL += "WHERE A.MA_HANGHOA=B.MA_HANGHOA_OPTION" + "\n";
            sSQL += "AND B.MA_COMBO=" + clsMain.SQLString(smaCombo) + "\n";
            sSQL += "AND B.MA_HANGHOA=" + clsMain.SQLString(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, MA).ToString()) + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                Frm_SetCombo_XemDS frm = new Frm_SetCombo_XemDS(smaCombo, gridView2.GetRowCellValue(gridView2.FocusedRowHandle, MA).ToString());
                frm.ShowDialog();
            }
            else
            {
                XtraMessageBox.Show(_kocomathangnaothaythe, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHoanTat_Click(object sender, EventArgs e)
        {
            btnHuy_Click(sender, e);
        }


        string _nhap = "";
        string _thongbao = "";
        string _luukothanhcong = "";
        string _xacnhanbanmuonxoa = "";
        string _kocomathangnaothaythe = "";
        string _khongcodulieu = "";
        string _xoakhongthanhcong = "";
        string _trungmathang = "";
        string _mathangnaydacotrods = "";
        string _nhapmahanghoa = "";
        string _trungmahh = "";
        string _trungtenhanghoa = "";
        string _chuachoncombo = "";
        string _khongcocombo = "";
        string _taocombothanhcong = "";
        string _chuachondongdexoa = "";
        private void Frm_NhomHanghoa_Load(object sender, EventArgs e)
        {
            if (fpnlNhomhang.Controls.Count > 0)
            {
                NhomCombo_Click(fpnlNhomhang.Controls[0], null);
            }

            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            lbNhom.Text = rm.GetString("nhom", culture);
            lbMa.Text = rm.GetString("ma", culture);
            lblDVT.Text = rm.GetString("dvt1", culture);
            lbSTT.Text = rm.GetString("stt", culture);
            lbGhiChu.Text = rm.GetString("ghichu", culture);
            lbThue.Text = rm.GetString("thue", culture);
            lbGiaMua.Text = rm.GetString("giamua", culture);
           // lbGiaBan.Text = rm.GetString("giaban", culture);
            chkMayinbep.Text = rm.GetString("mayinbep", culture);
            lbMaVach.Text = rm.GetString("mavach", culture);
            chkSUDUNG.Text = rm.GetString("sudung", culture);
            btnLuuCombo.Text = rm.GetString("luu", culture);
            btnThemCombo.Text = rm.GetString("them", culture);
            btnSuaCombo.Text = rm.GetString("sua", culture);
            btnXoaCombo.Text = rm.GetString("xoa", culture);
            btnXoadong.Text = rm.GetString("xoadong", culture);
            btnHuy.Text = rm.GetString("huy", culture);
            btnLuuChitietCombo.Text = rm.GetString("luu", culture);
            btnSuaChitietCombo.Text = rm.GetString("sua", culture);
            btnHuyChitietCombo.Text = rm.GetString("xoa", culture);
            btnNghiepvu.Text = rm.GetString("nghiepvu", culture);
            btnNangcao.Text = rm.GetString("nangcao", culture);
            btnDoimathang.Text = rm.GetString("doimathang", culture);
            btnHoanTat.Text = rm.GetString("hoantat", culture);
            btnXemDS.Text = rm.GetString("xemtuychon", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _nhap = rm.GetString("nhap", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);
            _xacnhanbanmuonxoa = rm.GetString("xacnhanbanmuonxoa", culture);
            _kocomathangnaothaythe = rm.GetString("kocomathangnaothaythe", culture);
            _khongcodulieu = rm.GetString("kocodulieu", culture);
            _xoakhongthanhcong = rm.GetString("xoakhongthanhcong", culture);
            _trungmathang = rm.GetString("xoakhongthanhcong", culture);
            _mathangnaydacotrods = rm.GetString("mathangnaydacotrods", culture);
            _nhapmahanghoa = rm.GetString("nhapmahanghoa", culture);
            _trungmahh = rm.GetString("trungmahh", culture);
            _trungtenhanghoa = rm.GetString("trungtenhanghoa", culture);
            _chuachoncombo = rm.GetString("chuachoncombo", culture);
            _khongcocombo = rm.GetString("khongcocombo", culture);
            _taocombothanhcong = rm.GetString("taocombothanhcong", culture);
            _chuachondongdexoa = rm.GetString("chuachondongdexoa", culture);

            gridView2.Columns["STTCombo"].Caption = rm.GetString("stt", culture);
            gridView2.Columns["TEN"].Caption = rm.GetString("tenhanghoa", culture);
            gridView2.Columns["SOLUONG"].Caption = rm.GetString("soluong", culture);
            gridView2.Columns["PRICE"].Caption = rm.GetString("gia", culture);
        }

        private void btnAutoMaVach_Click(object sender, EventArgs e)
        {
            txtMAVACH.Text = clsMain.ReturnDataTable("SELECT [dbo].[fc_NewCodeID_MaVach]  ()").Rows[0][0].ToString();
        }
    }
}