﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Data.SqlClient;
using DevExpress.XtraEditors.Controls;
using System.Globalization;
using System.Resources;

using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

namespace KP_RES
{
    public partial class Frm_NhomHanghoa : DevExpress.XtraEditors.XtraForm
    {
        public static BindingSource BS = new BindingSource();
        string smaNhomhang = "";
        string smaHanghoa = "";

        public Frm_NhomHanghoa()
        {
            InitializeComponent();
            cls_KP_RES.LoadBangCauHinh();
            this.Text = rm.GetString("nhomhanghanghoa", culture);
            LoadCombo();
            LoaddataGridView();
            LoaddataGridView1(smaNhomhang);
            LoadPermission();
            btnChonNhomhang_Click(null, null);
        }

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                txt.SelectAll();
            }
            catch
            {
            }
        }

        private void Frm_NhomHanghoa_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void btnLenNhomhang_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlNhomhang.AutoScrollPosition = new Point(0, fpnlNhomhang.VerticalScroll.Value - fpnlNhomhang.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnXuongNhomhang_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlNhomhang.AutoScrollPosition = new Point(0, fpnlNhomhang.VerticalScroll.Value + fpnlNhomhang.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnLenHangHoa_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlHanghoa.AutoScrollPosition = new Point(0, fpnlHanghoa.VerticalScroll.Value - fpnlHanghoa.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnXuongHangHoa_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlHanghoa.AutoScrollPosition = new Point(0, fpnlHanghoa.VerticalScroll.Value + fpnlHanghoa.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
            LoaddataGridView1(smaNhomhang);
            if (fpnlNhomhang.Controls.Count > 0)
            {
                Nhomhang_Click(fpnlNhomhang.Controls[0], null);
            }
            LoadGridview();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnChonNhomhang_Click(object sender, EventArgs e)
        {
            pnlNhomhang.Visible = true;
            pnlNhomhang.Dock = DockStyle.Fill;
            pnlHanghoa.Visible = false;
            btnChonNhomhang.ForeColor = Color.Red;
            btnChonHanghoa.ForeColor = btnCapnhat.ForeColor;
        }

        private void btnChonHanghoa_Click(object sender, EventArgs e)
        {
            pnlHanghoa.Visible = true;
            pnlHanghoa.Dock = DockStyle.Fill;
            pnlNhomhang.Visible = false;
            btnChonHanghoa.ForeColor = Color.Red;
            btnChonNhomhang.ForeColor = btnCapnhat.ForeColor;
        }

        private void picNHOMHANG_Click(object sender, EventArgs e)
        {
            if (picNHOMHANG.Enabled)
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = Application.StartupPath + "\\Images";
                openFileDialog1.Filter = "Image Files|*.jpg;*.bmp;*.png;*.gif|All Files|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.Title = _chonanh;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        picNHOMHANG.ImageLocation = openFileDialog1.FileName;
                        picNHOMHANG.BackgroundImage = null;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {
                        picNHOMHANG.Image = null;
                        picNHOMHANG.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void picHANGHOA_Click(object sender, EventArgs e)
        {
            if (picHANGHOA.Enabled)
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = Application.StartupPath + "\\Images";
                openFileDialog1.Filter = "Image Files|*.jpg;*.bmp;*.png;*.gif|All Files|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.Title = "Chọn ảnh";
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        picHANGHOA.ImageLocation = openFileDialog1.FileName;
                        picHANGHOA.BackgroundImage = null;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {
                        picHANGHOA.Image = null;
                        picHANGHOA.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void btnThemNhomHang_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtTENNHOMHANG.Focus();
        }

        private void btnSuaNhomHang_Click(object sender, EventArgs e)
        {
            if (smaNhomhang == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl(false);
            txtTENNHOMHANG.Focus();
        }

        private void btnXoaNhomHang_Click(object sender, EventArgs e)
        {
            if (smaNhomhang == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show(_xacnhanbanmuonxoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From NHOMHANG" + "\n";
            sSQL += "Where MA_NHOMHANG=" + clsMain.SQLString(smaNhomhang) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
                LoadCombo();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLuuNhomHang_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(picNHOMHANG.Image, typeof(Byte[]));
            SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
            SqlCommand SqlComMain = null;
            string sSQL = "";
            if (smaNhomhang == "" && btnThemNhomHang.Enabled)
            {
                sSQL += "Insert into NHOMHANG (TEN_NHOMHANG,GHICHU,SUDUNG,THUCDON,STT,HINHANH,MONTHEM) Values (@TEN_NHOMHANG,@GHICHU,@SUDUNG,@THUCDON,@STT,@HINHANH,@MONTHEM)" + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);
                SqlCom.Parameters.Add(new SqlParameter("@TEN_NHOMHANG", (object)txtTENNHOMHANG.Text));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txtGHICHUNHOMHANG.Text));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSUDUNGNHOMHANG.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@THUCDON", (object)chkTHUCDONNHOMHANG.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@STT", (object)txtSTTNHOMHANG.Text));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlCom.Parameters.Add(new SqlParameter("@MONTHEM", (object)chkMONTHEM.Checked));

                SqlComMain = SqlCom;
            }
            else if (smaNhomhang != "" && btnSuaNhomHang.Enabled)
            {
                sSQL += "Update NHOMHANG Set TEN_NHOMHANG=@TEN_NHOMHANG,GHICHU=@GHICHU,SUDUNG=@SUDUNG,THUCDON=@THUCDON,STT=@STT,HINHANH=@HINHANH,MONTHEM=@MONTHEM" + "\n";
                sSQL += "Where MA_NHOMHANG=" + clsMain.SQLString(smaNhomhang) + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);
                SqlCom.Parameters.Add(new SqlParameter("@TEN_NHOMHANG", (object)txtTENNHOMHANG.Text));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txtGHICHUNHOMHANG.Text));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSUDUNGNHOMHANG.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@THUCDON", (object)chkTHUCDONNHOMHANG.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@STT", (object)txtSTTNHOMHANG.Text));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlCom.Parameters.Add(new SqlParameter("@MONTHEM", (object)chkMONTHEM_NH.Checked));
                
                SqlComMain = SqlCom;
            }
            else
            {
                SqlComMain = null;
            }

            Boolean bRunSQL = true;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                CN.Open();
                SqlComMain.ExecuteNonQuery();
                CN.Close();
                this.Cursor = Cursors.Default;
                bRunSQL = true;
            }
            catch
            {
                bRunSQL = false;
            }

            if (bRunSQL)
            {
                #region Update Sửa máy in bếp
                if (smaNhomhang != "" && btnSuaNhomHang.Enabled)
                {
                    sSQL = "";
                    if (chkSuamayinbep.Checked)
                    {
                        if (chkMayinbepNhom.Checked)
                        {
                            sSQL += "Update HANGHOA Set" + "\n";
                            sSQL += "IS_INBEP=1,";
                            sSQL += "MA_BEP=" + clsMain.SQLString(cboBepNhom.EditValue.ToString()) + " \n";
                            sSQL += "Where MA_NHOMHANG=" + clsMain.SQLString(smaNhomhang);
                        }
                        else
                        {
                            sSQL += "Update HANGHOA Set" + "\n";
                            sSQL += "IS_INBEP=0" + "\n";
                            sSQL += "Where MA_NHOMHANG=" + clsMain.SQLString(smaNhomhang);
                        }
                        clsMain.ExecuteSQL(sSQL);
                    }
                }
                #endregion
                LoaddataGridView();
                LoadCombo();
                btnThemNhomHang.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnThemHangHoa_Click(object sender, EventArgs e)
        {
            SetRongControl1();
            KhoaMoControl1(false);
            btnAUTO_Click(null, null);
            btnAutoMaVach_Click(null, null);
            cboNhomhang.Focus();
        }

        private void btnSuaHangHoa_Click(object sender, EventArgs e)
        {
            if (smaHanghoa == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl1(false);
            cboNhomhang.Focus();
        }

        private void btnXoaHangHoa_Click(object sender, EventArgs e)
        {
            if (smaHanghoa == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show(_xacnhanbanmuonxoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From HANGHOA" + "\n";
            sSQL += "Where MA_HANGHOA=" + clsMain.SQLString(smaHanghoa) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView1(smaNhomhang);
                LoadCombo();

            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLuuHangHoa_Click(object sender, EventArgs e)
        {
            if (!CheckInput1())
            {
                return;
            }
            Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(picHANGHOA.Image, typeof(Byte[]));
            SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
            SqlCommand SqlComMain = null;
            string sSQL = "";
            if (smaHanghoa == "" && btnThemHangHoa.Enabled)
            {
                sSQL += "Insert into HANGHOA (TEN_HANGHOA,HINHANH,ID_HANGHOA,THUE,GIANHAP,GIABAN1,GIABAN2,GIABAN3,GIABAN4,GHICHU,HANSUDUNG,MAVACH,SUDUNG,SUAGIA,TONKHO,TONTOITHIEU,MA_THANHPHAN,SOLUONG,MA_NHOMHANG,MA_DONVITINH,MA_BEP,STT,IS_INBEP,SUADINHLUONG,GIATHEOTRONGLUONG,PLU,MONTHEM,INTEM)" + "\n";
                sSQL += "Values (@TEN_HANGHOA,@HINHANH,@ID_HANGHOA,@THUE,@GIANHAP,@GIABAN1,@GIABAN2,@GIABAN3,@GIABAN4,@GHICHU,@HANSUDUNG,@MAVACH,@SUDUNG,@SUAGIA,@TONKHO,@TONTOITHIEU,@MA_THANHPHAN,@SOLUONG,@MA_NHOMHANG,@MA_DONVITINH,@MA_BEP,@STT,@IS_INBEP,@SUADINHLUONG,@GIATHEOTRONGLUONG,@PLU,@MONTHEM,@INTEM)" + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);

                SqlCom.Parameters.Add(new SqlParameter("@TEN_HANGHOA", (object)txtTENHANGHOA.Text));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlCom.Parameters.Add(new SqlParameter("@ID_HANGHOA", (object)txtMAHANGHOA.Text));
                SqlCom.Parameters.Add(new SqlParameter("@THUE", (object)cboThue.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIANHAP", (object)txtGIAMUA.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN1", (object)txtGIABAN1.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN2", (object)txtGIABAN2.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN3", (object)txtGIABAN3.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN4", (object)txtGIABAN4.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txtGHICHUHANGHOA.Text));
                SqlCom.Parameters.Add(new SqlParameter("@HANSUDUNG", (object)txtHSD.Text));
                SqlCom.Parameters.Add(new SqlParameter("@MAVACH", (object)txtMAVACH.Text));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSUDUNGHANGHOA.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@SUAGIA", (object)chkSUAGIA.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@TONKHO", (object)chkTONKHO.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@TONTOITHIEU", (object)txtTONTOITHIEU.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@MA_THANHPHAN", (object)cboTHANHPHAN.EditValue));
                SqlCom.Parameters.Add(new SqlParameter("@SOLUONG", (object)txtSOLUONG.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@MA_NHOMHANG", (object)cboNhomhang.EditValue));
                SqlCom.Parameters.Add(new SqlParameter("@MA_DONVITINH", (object)cboDonvitinh.EditValue));
                SqlCom.Parameters.Add(new SqlParameter("@MA_BEP", (object)cboBep.EditValue));
                SqlCom.Parameters.Add(new SqlParameter("@STT", (object)txtSTTHANGHOA.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@IS_INBEP", (object)chkMayinbep.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@SUADINHLUONG", (object)chkSuaDinhLuong.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@GIATHEOTRONGLUONG", (object)chkGiatheotrongluong.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@PLU", (object)cboPLU.EditValue));
                SqlCom.Parameters.Add(new SqlParameter("@MONTHEM", (object)chkMONTHEM.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@INTEM", (object)chkINTEM.Checked));

                SqlComMain = SqlCom;
            }
            else if ((smaHanghoa != "" && btnSuaHangHoa.Enabled))
            {
                sSQL += "Update HANGHOA Set" + "\n";
                sSQL += "TEN_HANGHOA=@TEN_HANGHOA,HINHANH=@HINHANH,ID_HANGHOA=@ID_HANGHOA,THUE=@THUE,GIANHAP=@GIANHAP,";
                sSQL += "GIABAN1=@GIABAN1,GIABAN2=@GIABAN2,GIABAN3=@GIABAN3,GIABAN4=@GIABAN4,GHICHU=@GHICHU,HANSUDUNG=@HANSUDUNG,MAVACH=@MAVACH,SUDUNG=@SUDUNG,SUAGIA=@SUAGIA,";
                sSQL += "TONKHO=@TONKHO,TONTOITHIEU=@TONTOITHIEU,MA_THANHPHAN=@MA_THANHPHAN,SOLUONG=@SOLUONG,MA_NHOMHANG=@MA_NHOMHANG,";
                sSQL += "MA_DONVITINH=@MA_DONVITINH,MA_BEP=@MA_BEP,STT=@STT,IS_INBEP=@IS_INBEP,SUADINHLUONG=@SUADINHLUONG,GIATHEOTRONGLUONG=@GIATHEOTRONGLUONG,PLU=@PLU,MONTHEM=@MONTHEM,INTEM=@INTEM" + "\n";
                sSQL += "Where MA_HANGHOA=" + clsMain.SQLString(smaHanghoa) + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);

                SqlCom.Parameters.Add(new SqlParameter("@TEN_HANGHOA", (object)txtTENHANGHOA.Text));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlCom.Parameters.Add(new SqlParameter("@ID_HANGHOA", (object)txtMAHANGHOA.Text));
                SqlCom.Parameters.Add(new SqlParameter("@THUE", (object)cboThue.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIANHAP", (object)txtGIAMUA.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN1", (object)txtGIABAN1.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN2", (object)txtGIABAN2.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN3", (object)txtGIABAN3.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN4", (object)txtGIABAN4.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txtGHICHUHANGHOA.Text));
                SqlCom.Parameters.Add(new SqlParameter("@HANSUDUNG", (object)txtHSD.Text));
                SqlCom.Parameters.Add(new SqlParameter("@MAVACH", (object)txtMAVACH.Text));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSUDUNGHANGHOA.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@SUAGIA", (object)chkSUAGIA.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@TONKHO", (object)chkTONKHO.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@TONTOITHIEU", (object)txtTONTOITHIEU.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@MA_THANHPHAN", (object)cboTHANHPHAN.EditValue));
                SqlCom.Parameters.Add(new SqlParameter("@SOLUONG", (object)txtSOLUONG.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@MA_NHOMHANG", (object)cboNhomhang.EditValue));
                SqlCom.Parameters.Add(new SqlParameter("@MA_DONVITINH", (object)cboDonvitinh.EditValue));
                SqlCom.Parameters.Add(new SqlParameter("@MA_BEP", (object)cboBep.EditValue));
                SqlCom.Parameters.Add(new SqlParameter("@STT", (object)txtSTTHANGHOA.Text.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@IS_INBEP", (object)chkMayinbep.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@SUADINHLUONG", (object)chkSuaDinhLuong.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@GIATHEOTRONGLUONG", (object)chkGiatheotrongluong.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@PLU", (object)cboPLU.EditValue));
                SqlCom.Parameters.Add(new SqlParameter("@MONTHEM", (object)chkMONTHEM.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@INTEM", (object)chkINTEM.Checked));

                SqlComMain = SqlCom;
            }
            else
            {
                SqlComMain = null;
            }

            Boolean bRunSQL = true;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                CN.Open();
                SqlComMain.ExecuteNonQuery();
                CN.Close();
                this.Cursor = Cursors.Default;
                bRunSQL = true;
            }
            catch
            {
                bRunSQL = false;
            }

            if (bRunSQL)
            {
                LoaddataGridView1(smaNhomhang);
                LoadCombo();
                if (btnLIETKE.Text == _trangchu)
                {
                    LoadGridview();
                }

                btnThemHangHoa.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Nhomhang_Click(object sender, EventArgs e)
        {
            btnChonNhomhang_Click(null, null);

            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in fpnlNhomhang.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }

            LoaddataGridView1(btn.Name.Substring(1, btn.Name.Length - 1));

            smaNhomhang = btn.Name.Substring(1, btn.Name.Length - 1);
            txtTENNHOMHANG.Text = dtNhomhang.Select("MA=" + clsMain.SQLString(smaNhomhang))[0]["TEN"].ToString();
            txtGHICHUNHOMHANG.Text = dtNhomhang.Select("MA=" + clsMain.SQLString(smaNhomhang))[0]["GHICHU"].ToString();
            txtSTTNHOMHANG.Text = dtNhomhang.Select("MA=" + clsMain.SQLString(smaNhomhang))[0]["STT"].ToString();
            chkSUDUNGNHOMHANG.Checked = bool.Parse(dtNhomhang.Select("MA=" + clsMain.SQLString(smaNhomhang))[0]["SUDUNG"].ToString());
            chkTHUCDONNHOMHANG.Checked = bool.Parse(dtNhomhang.Select("MA=" + clsMain.SQLString(smaNhomhang))[0]["THUCDON"].ToString());
            chkSuamayinbep.Checked = false;
            chkMONTHEM_NH.Checked = bool.Parse(dtNhomhang.Select("MA=" + clsMain.SQLString(smaNhomhang))[0]["MONTHEM"].ToString());
            
            try
            {
                byte[] tam = new byte[((byte[])dtNhomhang.Select("MA=" + clsMain.SQLString(smaNhomhang))[0]["HINHANH"]).LongLength];
                tam = (byte[])dtNhomhang.Select("MA=" + clsMain.SQLString(smaNhomhang))[0]["HINHANH"];
                MemoryStream ms = new MemoryStream(tam);
                Bitmap bm = new Bitmap(ms);
                picNHOMHANG.Image = bm;
                picNHOMHANG.BackgroundImage = null;
            }
            catch
            {
                picNHOMHANG.Image = null;
                picNHOMHANG.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            }
        }

        private void HangHoa_Click(object sender, EventArgs e)
        {
            try
            {
                SimpleButton btn = (SimpleButton)sender;
                btn.ForeColor = Color.Red;
                foreach (SimpleButton btn1 in fpnlHanghoa.Controls)
                {
                    if (btn1.Name != btn.Name)
                    {
                        btn1.ForeColor = btnCapnhat.ForeColor;
                    }
                }
                if (btn.Tag.ToString() == "True")
                {
                    return;
                }
                btnChonHanghoa_Click(null, null);

                smaHanghoa = btn.Name.Substring(1, btn.Name.Length - 1);
                try
                {
                    cboNhomhang.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["MA_NHOMHANG"].ToString());
                }
                catch
                {
                    cboNhomhang.EditValue = (dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["MA_NHOMHANG"].ToString());
                }
                cboDonvitinh.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["MA_DONVITINH"].ToString());
                cboThue.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["THUE"].ToString());
                txtMAHANGHOA.Text = dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["ID_HANGHOA"].ToString();
                txtTENHANGHOA.Text = dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["TEN"].ToString();
                txtGHICHUHANGHOA.Text = dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["GHICHU"].ToString();
                txtHSD.Text = dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["HANSUDUNG"].ToString();
                txtMAVACH.Text = dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["MAVACH"].ToString();
                txtSTTHANGHOA.Text = dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["STT"].ToString();
                chkSUDUNGHANGHOA.Checked = bool.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["SUDUNG"].ToString());
                chkSUAGIA.Checked = bool.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["SUAGIA"].ToString());
                chkSuaDinhLuong.Checked = bool.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["SUADINHLUONG"].ToString());
                chkTONKHO.Checked = bool.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["TONKHO"].ToString());
                txtGIAMUA.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["GIANHAP"].ToString());
                txtLoinhuan.EditValue = Math.Round((((double.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["GIABAN1"].ToString()) - double.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["GIANHAP"].ToString())) / double.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["GIANHAP"].ToString())) * 100),1);
                txtGIABAN1.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["GIABAN1"].ToString());
                txtGIABAN2.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["GIABAN2"].ToString());
                txtGIABAN3.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["GIABAN3"].ToString());
                txtGIABAN4.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["GIABAN4"].ToString());
                chkMayinbep.Checked = bool.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["IS_INBEP"].ToString());
                cboBep.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["MA_BEP"].ToString());
                cboTHANHPHAN.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["MA_THANHPHAN"].ToString());
                txtSOLUONG.Text = dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["SOLUONG"].ToString();
                txtTONTOITHIEU.Text = dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["TONTOITHIEU"].ToString();
                chkGiatheotrongluong.Checked = bool.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["GIATHEOTRONGLUONG"].ToString());
                cboPLU.EditValue = int.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["PLU"].ToString() == "" ? "0" : dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["PLU"].ToString());
                chkMONTHEM.Checked = bool.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["MONTHEM"].ToString());
                chkINTEM.Checked = bool.Parse(dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["INTEM"].ToString());
                try
                {
                    byte[] tam = new byte[((byte[])dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["HINHANH"]).LongLength];
                    tam = (byte[])dtHanghoa.Select("MA=" + clsMain.SQLString(smaHanghoa))[0]["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    picHANGHOA.Image = bm;
                    picHANGHOA.BackgroundImage = null;
                }
                catch
                {
                    picHANGHOA.Image = null;
                    picHANGHOA.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
                }
            }
            catch {
                
            }
        }

        DataTable dtNhomhang = new DataTable();
        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);

            string sSQL = "";
            sSQL += "Select MA_NHOMHANG As MA,TEN_NHOMHANG As TEN,GHICHU,SUDUNG,THUCDON,STT,HINHANH,MONTHEM" + "\n";
            sSQL += "From NHOMHANG" + "\n";
            sSQL += "Order by SUDUNG DESC,STT" + "\n";
            dtNhomhang = clsMain.ReturnDataTable(sSQL);
            fpnlNhomhang.Controls.Clear();
            fpnlHanghoa.Controls.Clear();
            foreach (DataRow dr in dtNhomhang.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 125;
                btn.Height = 80;
                btn.Font = fpnlHanghoa.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                try
                {
                    btn.ImageLocation = ImageLocation.TopCenter;
                    byte[] tam = new byte[((byte[])dr["HINHANH"]).LongLength];
                    tam = (byte[])dr["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    if (cls_KP_RES.iHinhanh == 1)
                    {
                        btn.BackgroundImage = bm;
                        btn.BackgroundImageLayout = ImageLayout.Stretch;
                        btn.Appearance.BackColor = Color.Transparent;
                        btn.Appearance.Options.UseBackColor = true;
                        btn.ButtonStyle = BorderStyles.NoBorder;
                        btn.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                    }
                    else
                    {
                        btn.Image = bm;
                    }
                }
                catch
                {
                }
                btn.Name = "N" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(Nhomhang_Click);
                fpnlNhomhang.Controls.Add(btn);
            }
            if (fpnlNhomhang.VerticalScroll.Visible)
            {
                palNHOMHANG.Width = 152;
            }
            else
            {
                palNHOMHANG.Width = 135;
            }
        }

        DataTable dtHanghoa = new DataTable();
        private void LoaddataGridView1(string maNhomhang)
        {
            SetRongControl1();
            KhoaMoControl1(true);

            string sSQL = "";
            sSQL += "Select MA_HANGHOA As MA,ID_HANGHOA,TEN_HANGHOA As TEN,HINHANH,THUE,GIANHAP,GIABAN1,GIABAN2,GIABAN3,GIABAN4,GHICHU,HANSUDUNG,MAVACH,";
            sSQL += "SUDUNG,SUAGIA,TONKHO,TONTOITHIEU,MA_THANHPHAN,SOLUONG,MA_NHOMHANG,MA_DONVITINH,MA_BEP,STT,IS_COMBO,IS_INBEP,";
            sSQL += "ISNULL(SUADINHLUONG,0) AS SUADINHLUONG,GIATHEOTRONGLUONG,PLU,MONTHEM,INTEM" + "\n";
            sSQL += "From HANGHOA" + "\n";
            sSQL += "Where MA_NHOMHANG=" + clsMain.SQLString(maNhomhang) + "\n";
            sSQL += "Order by SUDUNG DESC,STT" + "\n";
            dtHanghoa = clsMain.ReturnDataTable(sSQL);
            fpnlHanghoa.Controls.Clear();
            foreach (DataRow dr in dtHanghoa.Rows)
            {
                SimpleButton btn = new SimpleButton();
                if (cls_KP_RES.iHinhanh == 1)
                {
                    btn.Size = new Size(168, 130);
                }
                else
                {
                    btn.Size = new Size(110, 80);
                }
                btn.Font = fpnlHanghoa.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                try
                {
                    btn.ImageLocation = ImageLocation.TopCenter;
                    byte[] tam = new byte[((byte[])dr["HINHANH"]).LongLength];
                    tam = (byte[])dr["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    if (cls_KP_RES.iHinhanh == 1)
                    {
                      
                        btn.BackgroundImage = bm;
                        btn.BackgroundImageLayout = ImageLayout.Stretch;
                        btn.Appearance.BackColor = Color.Transparent;
                        btn.Appearance.Options.UseBackColor = true;
                        btn.ButtonStyle = BorderStyles.NoBorder;
                        btn.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                    }
                    else
                    {
                        btn.Image = bm;
                    }
                }
                catch
                {
                }
                btn.Tag = dr["IS_COMBO"].ToString();
                btn.Name = "H" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString() + "\n" + String.Format("{0:#,###0}", dr["GIABAN1"]);
                btn.Click += new EventHandler(HangHoa_Click);
                fpnlHanghoa.Controls.Add(btn);
            }
        }

        private void LoadCombo()
        {
            string sSQL = "";
            DataTable dt = new DataTable();
            sSQL += "Select MA_NHOMHANG As MA,TEN_NHOMHANG As TEN" + "\n";
            sSQL += "From NHOMHANG" + "\n";
            sSQL += "Where SUDUNG=1 ";
            sSQL += "Order by  TEN_NHOMHANG ";
            dt = clsMain.ReturnDataTable(sSQL);
            cboNhomhang.Properties.DataSource = dt;
            cboNhomhang.EditValue = cboNhomhang.Properties.GetDataSourceValue(cboNhomhang.Properties.ValueMember, 0);

            sSQL = "";
            sSQL += "Select MA_DONVITINH As MA,TEN_DONVITINH As TEN" + "\n";
            sSQL += "From DONVITINH" + "\n";
            sSQL += "Where SUDUNG=1 ";
            sSQL += "Order by  TEN_DONVITINH ";
            dt = clsMain.ReturnDataTable(sSQL);
            cboDonvitinh.Properties.DataSource = dt;
            cboDonvitinh.EditValue = cboDonvitinh.Properties.GetDataSourceValue(cboDonvitinh.Properties.ValueMember, 0);

            sSQL = "";
            sSQL += "Select A.MA_BEP As MA,A.TEN_BEP As TEN,A.MAYINBEP,A.MA_CUAHANG,B.TEN_CUAHANG" + "\n";
            sSQL += "From DM_BEP A, CUAHANG B" + "\n";
            sSQL += "Where A.MA_CUAHANG=B.MA_CUAHANG" + "\n";
            sSQL += "And A.SUDUNG=1 ";
            sSQL += "Order by  B.TEN_CUAHANG,A.TEN_BEP ";
            dt = clsMain.ReturnDataTable(sSQL);
            cboBep.Properties.DataSource = dt;
            cboBep.EditValue = cboBep.Properties.GetDataSourceValue(cboBep.Properties.ValueMember, 0);

            cboBepNhom.Properties.DataSource = dt;
            cboBepNhom.EditValue = cboBepNhom.Properties.GetDataSourceValue(cboBepNhom.Properties.ValueMember, 0);

            sSQL = "";
            sSQL += "Select A.MA_HANGHOA As MA,A.TEN_HANGHOA As TEN,B.TEN_DONVITINH As DONVITINH" + "\n";
            sSQL += "From HANGHOA A , DONVITINH B" + "\n";
            sSQL += "Where A.MA_DONVITINH=B.MA_DONVITINH ";
            sSQL += "And A.SUDUNG=1 ";
            sSQL += "Order by  A.TEN_HANGHOA ";
            dt = clsMain.ReturnDataTable(sSQL);
            dt.Rows.Add("0", "Không sử dụng");
            cboTHANHPHAN.Properties.DataSource = dt;
            cboTHANHPHAN.EditValue = 0;

            sSQL = "";
            sSQL += "EXEC SP_LIST_NUMBER" + "\n";
            dt = clsMain.ReturnDataTable(sSQL);
            cboPLU.Properties.DataSource = dt;
            cboPLU.EditValue = cboPLU.Properties.GetDataSourceValue(cboBep.Properties.ValueMember, 0);
        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTENNHOMHANG.Properties.ReadOnly = true;
                txtGHICHUNHOMHANG.Properties.ReadOnly = true;
                txtSTTNHOMHANG.Properties.ReadOnly = true;
                chkSUDUNGNHOMHANG.Properties.ReadOnly = true;
                chkTHUCDONNHOMHANG.Properties.ReadOnly = true;
                chkSuamayinbep.Properties.ReadOnly = true;
                picNHOMHANG.Enabled = false;
            }
            else//mở
            {
                txtTENNHOMHANG.Properties.ReadOnly = false;
                txtGHICHUNHOMHANG.Properties.ReadOnly = false;
                txtSTTNHOMHANG.Properties.ReadOnly = false;
                chkSUDUNGNHOMHANG.Properties.ReadOnly = false;
                chkTHUCDONNHOMHANG.Properties.ReadOnly = false;
                chkSuamayinbep.Properties.ReadOnly = false;
                picNHOMHANG.Enabled = true;
            }
        }

        private void KhoaMoControl1(bool tf)
        {
            if (tf == true)//khóa
            {
                cboNhomhang.Properties.ReadOnly = true;
                cboDonvitinh.Properties.ReadOnly = true;
                txtTENHANGHOA.Properties.ReadOnly = true;
                txtGHICHUHANGHOA.Properties.ReadOnly = true;
                txtHSD.Properties.ReadOnly = true;
                txtMAVACH.Properties.ReadOnly = true;
                txtSTTHANGHOA.Properties.ReadOnly = true;
                chkSUDUNGHANGHOA.Properties.ReadOnly = true;
                chkSUAGIA.Properties.ReadOnly = true;
                chkSuaDinhLuong.Properties.ReadOnly = true;
                chkMayinbep.Properties.ReadOnly = true;
                chkTONKHO.Properties.ReadOnly = true;
                picHANGHOA.Enabled = false;
                cboThue.Properties.ReadOnly = true;
                txtGIAMUA.Properties.ReadOnly = true;
                txtLoinhuan.Properties.ReadOnly = true;
                txtGIABAN1.Properties.ReadOnly = true;
                txtGIABAN2.Properties.ReadOnly = true;
                txtGIABAN3.Properties.ReadOnly = true;
                txtGIABAN4.Properties.ReadOnly = true;
                cboBep.Properties.ReadOnly = true;
                cboTHANHPHAN.Properties.ReadOnly = true;
                txtSOLUONG.Properties.ReadOnly = true;
                txtTONTOITHIEU.Properties.ReadOnly = true;
                txtMAHANGHOA.Properties.ReadOnly = true;
                btnAUTO.Enabled = false;
                btnAutoMaVach.Enabled = false;
                chkGiatheotrongluong.Properties.ReadOnly = true;
                cboPLU.Properties.ReadOnly = true;
            }
            else//mở
            {
                cboNhomhang.Properties.ReadOnly = false;
                cboDonvitinh.Properties.ReadOnly = false;
                txtTENHANGHOA.Properties.ReadOnly = false;
                txtGHICHUHANGHOA.Properties.ReadOnly = false;
                txtHSD.Properties.ReadOnly = false;
                txtMAVACH.Properties.ReadOnly = false;
                txtSTTHANGHOA.Properties.ReadOnly = false;
                chkSUDUNGHANGHOA.Properties.ReadOnly = false;
                chkSUAGIA.Properties.ReadOnly = false;
                chkSuaDinhLuong.Properties.ReadOnly = false;
                chkTONKHO.Properties.ReadOnly = false;
                chkMayinbep.Properties.ReadOnly = false;
                picHANGHOA.Enabled = true;
                cboThue.Properties.ReadOnly = false;
                txtGIAMUA.Properties.ReadOnly = false;
                txtLoinhuan.Properties.ReadOnly = false;
                txtGIABAN1.Properties.ReadOnly = false;
                txtGIABAN2.Properties.ReadOnly = false;
                txtGIABAN3.Properties.ReadOnly = false;
                txtGIABAN4.Properties.ReadOnly = false;
                cboBep.Properties.ReadOnly = false;
                cboTHANHPHAN.Properties.ReadOnly = false;
                txtSOLUONG.Properties.ReadOnly = false;
                txtTONTOITHIEU.Properties.ReadOnly = false;
                txtMAHANGHOA.Properties.ReadOnly = false;
                btnAUTO.Enabled = true;
                btnAutoMaVach.Enabled = true;
                chkGiatheotrongluong.Properties.ReadOnly = false;
                cboPLU.Properties.ReadOnly = false;
            }
        }

        private void SetRongControl()
        {
            smaNhomhang = "";
            txtTENNHOMHANG.Text = "";
            txtGHICHUNHOMHANG.Text = "";
            txtSTTNHOMHANG.Text = "1";
            chkSUDUNGNHOMHANG.Checked = true;
            chkTHUCDONNHOMHANG.Checked = true;
            picNHOMHANG.Image = null;
            picNHOMHANG.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
          
        }

        private void SetRongControl1()
        {
            smaHanghoa = "";
            try
            {
                cboNhomhang.EditValue = (smaNhomhang != "") ? int.Parse(smaNhomhang) : cboNhomhang.Properties.GetDataSourceValue(cboNhomhang.Properties.ValueMember, 0);
            }
            catch
            {
                cboNhomhang.EditValue = (smaNhomhang != "") ? (smaNhomhang) : cboNhomhang.Properties.GetDataSourceValue(cboNhomhang.Properties.ValueMember, 0);
            }
            cboDonvitinh.EditValue = cboDonvitinh.Properties.GetDataSourceValue(cboDonvitinh.Properties.ValueMember, 0);
            cboThue.EditValue = 0;
            txtMAHANGHOA.Text = "";
            txtTENHANGHOA.Text = "";
            txtGHICHUHANGHOA.Text = "";
            txtHSD.Text = "";
            txtMAVACH.Text = "";
            txtSTTHANGHOA.EditValue = 1;
            chkSUDUNGHANGHOA.Checked = true;
            chkSUAGIA.Checked = false;
            chkSuaDinhLuong.Checked = false;
            chkTONKHO.Checked = false;
            picHANGHOA.Image = null;
            picHANGHOA.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            txtGIAMUA.EditValue = 0;
            txtLoinhuan.EditValue = 0;
            txtGIABAN1.EditValue = 0;
            txtGIABAN2.EditValue = 0;
            txtGIABAN3.EditValue = 0;
            txtGIABAN4.EditValue = 0;
            cboBep.EditValue = cboBep.Properties.GetDataSourceValue(cboBep.Properties.ValueMember, 0);
            cboTHANHPHAN.EditValue = 0;
            txtSOLUONG.EditValue = 0;
            txtTONTOITHIEU.EditValue = 0;
            chkGiatheotrongluong.Checked = false ;
            cboPLU.EditValue = 0;
            chkMONTHEM.Checked = false;
            chkINTEM.Checked = false;
        }

        private void LoadPermission()
        {
            btnThemNhomHang.Enabled = clsUserManagement.AllowAdd("6");
            btnSuaNhomHang.Enabled = clsUserManagement.AllowEdit("6");
            btnXoaNhomHang.Enabled = clsUserManagement.AllowDelete("6");
            btnLuuNhomHang.Enabled = btnThemNhomHang.Enabled || btnSuaNhomHang.Enabled;

            btnThemHangHoa.Enabled = clsUserManagement.AllowAdd("6");
            btnSuaHangHoa.Enabled = clsUserManagement.AllowEdit("6");
            btnXoaHangHoa.Enabled = clsUserManagement.AllowDelete("6");
            btnLuuHangHoa.Enabled = btnThemHangHoa.Enabled || btnSuaHangHoa.Enabled;
        }

        private Boolean CheckInput()
        {
            if (txtTENNHOMHANG.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_nhap + lblTENNHOMHANG.Text, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTENNHOMHANG.Focus();
                return false;
            }
            if (smaNhomhang == "" && btnThemNhomHang.Enabled && txtTENNHOMHANG.Text != "" && clsMain.ReturnDataTable("Select MA_NHOMHANG From NHOMHANG Where TEN_NHOMHANG=" + clsMain.SQLStringUnicode(txtTENNHOMHANG.Text)).Rows.Count > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show(_trungtennhomhang, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    txtTENNHOMHANG.Focus();
                    return false;
                }
            }
            if (smaNhomhang != "" && btnSuaNhomHang.Enabled && txtTENNHOMHANG.Text != "" && clsMain.ReturnDataTable("Select MA_NHOMHANG From NHOMHANG Where TEN_NHOMHANG=" + clsMain.SQLStringUnicode(txtTENNHOMHANG.Text) + " And MA_NHOMHANG!= " + clsMain.SQLString(smaNhomhang)).Rows.Count > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show(_trungtennhomhang, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    txtTENNHOMHANG.Focus();
                    return false;
                }
            }
            return true;
        }

        private Boolean CheckInput1()
        {
            if (txtTENHANGHOA.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_nhap + lblTENHANGHOA.Text, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTENHANGHOA.Focus();
                return false;
            }
            if (txtMAHANGHOA.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_nhapmahanghoa, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMAHANGHOA.Focus();
                return false;
            }
            if (smaHanghoa == "" && btnThemHangHoa.Enabled && txtMAHANGHOA.Text != "" && clsMain.ReturnDataTable("Select MA_HANGHOA From HANGHOA Where ID_HANGHOA=" + clsMain.SQLString(txtMAHANGHOA.Text)).Rows.Count > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_trungmahh, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMAHANGHOA.Focus();
                return false;
            }
            if (smaHanghoa != "" && btnSuaHangHoa.Enabled && txtMAHANGHOA.Text != "" && clsMain.ReturnDataTable("Select MA_HANGHOA From HANGHOA Where ID_HANGHOA=" + clsMain.SQLString(txtMAHANGHOA.Text) + "And MA_HANGHOA!= " + clsMain.SQLString(smaHanghoa)).Rows.Count > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_trungmahh, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMAHANGHOA.Focus();
                return false;
            }
            if (smaHanghoa == "" && btnThemHangHoa.Enabled && txtTENHANGHOA.Text != "" && clsMain.ReturnDataTable("Select MA_HANGHOA From HANGHOA Where TEN_HANGHOA=" + clsMain.SQLStringUnicode(txtTENHANGHOA.Text)).Rows.Count > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show(_trungtenhanghoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    txtTENHANGHOA.Focus();
                    return false;
                }
            }
            if (smaHanghoa != "" && btnSuaHangHoa.Enabled && txtTENHANGHOA.Text != "" && clsMain.ReturnDataTable("Select MA_HANGHOA From HANGHOA Where TEN_HANGHOA=" + clsMain.SQLStringUnicode(txtTENHANGHOA.Text) + " And MA_HANGHOA!= " + clsMain.SQLString(smaHanghoa)).Rows.Count > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show(_trungtenhanghoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    txtTENHANGHOA.Focus();
                    return false;
                }
            }
            if (smaHanghoa == "" && btnThemHangHoa.Enabled && txtMAVACH.Text != "" && clsMain.ReturnDataTable("Select MA_HANGHOA From HANGHOA Where MAVACH=" + clsMain.SQLString(txtMAVACH.Text)).Rows.Count > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_trungmavach, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMAVACH.Focus();
                return false;
            }
            if (smaHanghoa != "" && btnSuaHangHoa.Enabled && txtMAVACH.Text != "" && clsMain.ReturnDataTable("Select MA_HANGHOA From HANGHOA Where MAVACH=" + clsMain.SQLString(txtMAVACH.Text) + "And MA_HANGHOA!= " + clsMain.SQLString(smaHanghoa)).Rows.Count > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_trungmavach, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMAVACH.Focus();
                return false;
            }
            if (smaHanghoa == "" && btnThemHangHoa.Enabled && cboPLU.Text != "" && cboPLU.EditValue != null  && clsMain.ReturnDataTable("Select MA_HANGHOA From HANGHOA Where PLU=" + clsMain.SQLString(cboPLU.Text)).Rows.Count > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Trùng mã PLU", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboPLU.Focus();
                return false;
            }
            if (smaHanghoa != "" && btnSuaHangHoa.Enabled && cboPLU.Text != "" && cboPLU.EditValue != null && clsMain.ReturnDataTable("Select MA_HANGHOA From HANGHOA Where PLU=" + clsMain.SQLString(cboPLU.Text) + "And MA_HANGHOA!= " + clsMain.SQLString(smaHanghoa)).Rows.Count > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_trungmavach, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMAVACH.Focus();
                return false;
            }
            return true;
        }

        private void btnAUTO_Click(object sender, EventArgs e)
        {
            txtMAHANGHOA.Text = clsMain.ReturnDataTable("SELECT [dbo].[fc_NewCodeID_HANGHOA]  ()").Rows[0][0].ToString();
        }

        private void btnLIETKE_Click(object sender, EventArgs e)
        {
            if (btnLIETKE.Text == _lietketatca)
            {
                palHANGHOA.Visible = false;
                palNHOMHANG.Visible = false;
                palLIETKE.Visible = true;
                btnLIETKE.Text = _trangchu;
                LoadGridview();
            }
            else if (btnLIETKE.Text == _trangchu)
            {
                palHANGHOA.Visible = true;
                palNHOMHANG.Visible = true;
                palLIETKE.Visible = false;
                btnLIETKE.Text = _lietketatca;
            }

        }

        private void LoadGridview()
        {
            string SQL = "";
            SQL += "SELECT MA_HANGHOA,ID_HANGHOA,TEN_HANGHOA,H.STT,H.GHICHU,H.MAVACH,TEN_DONVITINH,H.MA_DONVITINH,";
            SQL += "H.SUDUNG,H.SUAGIA,H.HINHANH,H.MA_NHOMHANG,TEN_NHOMHANG,THUE,GIANHAP,GIABAN1,GIABAN2,GIABAN3,GIABAN4,TONKHO,H.IS_INBEP,";
            SQL += "THUCDON,TONTOITHIEU,H.SOLUONG,H.MA_BEP,H.MA_THANHPHAN,ISNULL(SUADINHLUONG,0) AS SUADINHLUONG,H.GIATHEOTRONGLUONG,H.PLU, H.HANSUDUNG, H.MONTHEM,H.INTEM" + "\n";
            SQL += "FROM DONVITINH D,HANGHOA H,NHOMHANG N \n";
            SQL += "WHERE H.MA_NHOMHANG=N.MA_NHOMHANG and H.MA_DONVITINH=D.MA_DONVITINH AND H.SUDUNG=1 \n";
            SQL += "ORDER BY TEN_NHOMHANG,TEN_HANGHOA";
            DataTable dt = clsMain.ReturnDataTable(SQL);
            gridControl1.DataSource = dt;
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT1 & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.Focus();
                gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            try
            {
                if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                    return;
                btnChonHanghoa_Click(null, null);

                smaHanghoa = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MA_HANGHOA").ToString();
                cboNhomhang.EditValue = (gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MA_NHOMHANG").ToString());
                cboDonvitinh.EditValue = int.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MA_DONVITINH").ToString());
                cboThue.EditValue = int.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "THUE").ToString());
                txtMAHANGHOA.Text = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "ID_HANGHOA").ToString();
                txtTENHANGHOA.Text = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TEN_HANGHOA").ToString();
                txtGHICHUHANGHOA.Text = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GHICHU").ToString();
                txtHSD.Text = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "HANSUDUNG").ToString();
                txtMAVACH.Text = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MAVACH").ToString();
                txtSTTHANGHOA.Text = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "STT").ToString();
                chkSUDUNGHANGHOA.Checked = bool.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "SUDUNG").ToString());
                chkSUAGIA.Checked = bool.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "SUAGIA").ToString());
                chkSuaDinhLuong.Checked = bool.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "SUADINHLUONG").ToString());
                chkTONKHO.Checked = bool.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TONKHO").ToString());
                chkMayinbep.Checked = bool.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "IS_INBEP").ToString());
                txtGIAMUA.EditValue = int.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GIANHAP").ToString());
                txtLoinhuan.EditValue = Math.Round((((double.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GIABAN1").ToString()) - double.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GIANHAP").ToString())) / double.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GIANHAP").ToString())) * 100), 1);
                txtGIABAN1.EditValue = int.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GIABAN1").ToString());
                txtGIABAN2.EditValue = int.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GIABAN2").ToString());
                txtGIABAN3.EditValue = int.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GIABAN3").ToString());
                txtGIABAN4.EditValue = int.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GIABAN4").ToString());
                cboBep.EditValue = int.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MA_BEP").ToString());
                cboTHANHPHAN.EditValue = int.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MA_THANHPHAN").ToString());
                txtSOLUONG.Text = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "SOLUONG").ToString();
                txtTONTOITHIEU.Text = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TONTOITHIEU").ToString();
                chkGiatheotrongluong.Checked = bool.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GIATHEOTRONGLUONG").ToString());
                cboPLU.EditValue = int.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "PLU").ToString()==""?"0":gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "PLU").ToString());
                chkMONTHEM.Checked = bool.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MONTHEM").ToString());
                chkINTEM.Checked = bool.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "INTEM").ToString());
                try
                {
                    byte[] tam = new byte[((byte[])gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "HINHANH")).LongLength];
                    tam = (byte[])gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "HINHANH");
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    picHANGHOA.Image = bm;
                    picHANGHOA.BackgroundImage = null;
                }
                catch
                {
                    picHANGHOA.Image = null;
                    picHANGHOA.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
                }
            }
            catch
            {
            }

        }

        private void chkSuamayinbep_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSuamayinbep.Checked)
            {
                palMayinbep.Visible = true;
                cboBepNhom.Properties.DataSource = null;
                string sSQL = "";
                sSQL += "Select A.MA_BEP As MA,A.TEN_BEP As TEN,A.MAYINBEP,A.MA_CUAHANG,B.TEN_CUAHANG" + "\n";
                sSQL += "From DM_BEP A, CUAHANG B" + "\n";
                sSQL += "Where A.MA_CUAHANG=B.MA_CUAHANG" + "\n";
                sSQL += "And A.SUDUNG=1 ";
                sSQL += "Order by  B.TEN_CUAHANG,A.TEN_BEP ";
                DataTable dt = clsMain.ReturnDataTable(sSQL);

                cboBepNhom.Properties.DataSource = dt;
                cboBepNhom.EditValue = cboBepNhom.Properties.GetDataSourceValue(cboBepNhom.Properties.ValueMember, 0);
            }
            else
                palMayinbep.Visible = false;
        }

        private void pnlNhomhang_Paint(object sender, PaintEventArgs e)
        {

        }

        string _trungtennhomhang = "";
        string _nhap = "";
        string _lietketatca = "";
        string _trangchu = "";
        string _thongbao = "";
        string _luukothanhcong = "";
        string _xacnhanbanmuonxoa = "";
        string _nhapmahanghoa = "";
        string _trungmahh = "";
        string _trungtenhanghoa = "";
        string _trungmavach = "";
        string _chonanh = "";
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void Frm_NhomHanghoa_Load(object sender, EventArgs e)
        {
            if (fpnlNhomhang.Controls.Count > 0)
            {
                Nhomhang_Click(fpnlNhomhang.Controls[0], null);
            }
            foreach (Control ctl in pnlNhomhang.Controls)
            {
                ctl.Click += new EventHandler(ctl_Click);
            }
            foreach (Control ctl in pnlHanghoa.Controls)
            {
                ctl.Click += new EventHandler(ctl_Click);
            }


            this.Text = rm.GetString("nhomhanghanghoa", culture);
            btnChonNhomhang.Text = rm.GetString("nhomhang", culture);
            btnChonHanghoa.Text = rm.GetString("hanghoa", culture);
            lblTENNHOMHANG.Text = rm.GetString("ten", culture);
            lbSTT.Text = rm.GetString("stt", culture);
            lblGHICHU.Text = rm.GetString("ghichu", culture);
            chkSuamayinbep.Text = rm.GetString("suamayinbep", culture);
            chkMayinbepNhom.Text = rm.GetString("mayinbep", culture);
            chkSUDUNGNHOMHANG.Text = rm.GetString("sudung", culture);
            chkTHUCDONNHOMHANG.Text = rm.GetString("thucdon", culture);
            btnLuuNhomHang.Text = rm.GetString("luu", culture);
            btnThemNhomHang.Text = rm.GetString("them", culture);
            btnSuaNhomHang.Text = rm.GetString("sua", culture);
            btnXoaNhomHang.Text = rm.GetString("xoa", culture);
            lbNhom.Text = rm.GetString("nhom", culture);
            lbMa.Text = rm.GetString("ma", culture);
            lblTENHANGHOA.Text = rm.GetString("ten", culture);
            lblDVT.Text = rm.GetString("dvt1", culture);
            lbSTT1.Text = rm.GetString("stt", culture);
            lbGhiChu1.Text = rm.GetString("ghichu", culture);
            lbThue.Text = rm.GetString("thue", culture);
            lbGiaMua.Text = rm.GetString("giamua", culture);
            lbGiaBan1.Text = rm.GetString("giaban11", culture);
            lbGiaBan2.Text = rm.GetString("giaban21", culture);
            lbThanhPhan.Text = rm.GetString("thanhphan", culture);
            chkTONKHO.Text = rm.GetString("tonkho", culture);
            lblTONTOITHIEU.Text = rm.GetString("tontoithieu", culture);
            chkMayinbep.Text = rm.GetString("mayinbep", culture);
            lbMaVach.Text = rm.GetString("mavach", culture);
            chkSUDUNGHANGHOA.Text = rm.GetString("sudung", culture);
            chkSUAGIA.Text = rm.GetString("giatheothoivu", culture);
            chkSuaDinhLuong.Text = rm.GetString("suadinhluong", culture);
            btnLuuHangHoa.Text = rm.GetString("luu", culture);
            btnThemHangHoa.Text = rm.GetString("them", culture);
            btnSuaHangHoa.Text = rm.GetString("sua", culture);
            btnXoaHangHoa.Text = rm.GetString("xoa", culture);
            btnLIETKE.Text = rm.GetString("lietketatca", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _lietketatca = rm.GetString("lietketatca", culture);
            _trangchu = rm.GetString("trangchu", culture);
            _nhap = rm.GetString("nhap", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);
            _xacnhanbanmuonxoa = rm.GetString("xacnhanbanmuonxoa", culture);
            _nhapmahanghoa = rm.GetString("nhapmahanghoa", culture);
            _trungmahh = rm.GetString("trungmahh", culture);
            _trungtenhanghoa = rm.GetString("trungtenhanghoa", culture);
            _trungmavach = rm.GetString("trungmavach", culture);
            _chonanh = rm.GetString("chonanh", culture);

            gridView1.Columns["STT1"].Caption = rm.GetString("stt", culture);
            gridView1.Columns["ID_HANGHOA"].Caption = rm.GetString("ma", culture);
            gridView1.Columns["TEN_HANGHOA"].Caption = rm.GetString("tenhanghoa", culture);
            gridView1.Columns["TEN_DONVITINH"].Caption = rm.GetString("dvt1", culture);
            gridView1.Columns["THUE"].Caption = rm.GetString("thue", culture);
            gridView1.Columns["GIANHAP"].Caption = rm.GetString("gianhap", culture);
            gridView1.Columns["GIABAN1"].Caption = rm.GetString("giaban11", culture);
            gridView1.Columns["GIABAN2"].Caption = rm.GetString("giaban21", culture);
            gridView1.Columns["TONKHO"].Caption = rm.GetString("tonkho", culture);
            gridView1.Columns["THUCDON"].Caption = rm.GetString("thucdon", culture);
            gridView1.Columns["TEN_NHOMHANG"].Caption = rm.GetString("nhom", culture);
            gridView1.Columns["GHICHU"].Caption = rm.GetString("ghichu", culture);
            gridView1.GroupPanelText = rm.GetString("keothacotvaodaydenhom", culture);
        }

        private void btnFileNhomhang_Click(object sender, EventArgs e)
        {
            OpenFileDialog sOpenFile = new OpenFileDialog();
            sOpenFile.Filter = Filter();
            sOpenFile.FilterIndex = 1;
            sOpenFile.RestoreDirectory = true;
            sOpenFile.Multiselect = false;
            sOpenFile.Title = "Chọn file";
            if (sOpenFile.ShowDialog() == DialogResult.OK)
            {
                Excel.Application xlApp = new Excel.Application();
                Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(sOpenFile.FileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                Excel._Worksheet xlWorksheet = (Excel.Worksheet)xlWorkbook.Worksheets.get_Item(1);
                Excel.Range xlRange = xlWorksheet.UsedRange;

                for (int i = 2; i <= xlRange.Rows.Count; i++)
                {
                    try  
                    {
                        String sMa = (String)(xlRange.Cells[i, 1] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 1] as Excel.Range).Value2 : "";
                        String sTen = (String)(xlRange.Cells[i, 2] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 2] as Excel.Range).Value2.ToString() : "";
                        String sGhiChu = (String)(xlRange.Cells[i, 3] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 3] as Excel.Range).Value2.ToString() : "";
                        String sSuDung = "1";
                        String sThucDon = (String)(xlRange.Cells[i, 4] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 4] as Excel.Range).Value2.ToString() : "0";
                        String sSTT = "1";

                        if (sMa != "" && clsMain.ReturnDataTable("SELECT MA_NHOMHANG FROM NHOMHANG WHERE MA_NHOMHANG=" + clsMain.SQLString(sMa)).Rows.Count > 0)
                        {
                            UpdateFileNhomHang(sMa, sTen, sGhiChu, sSuDung, sThucDon, sSTT);
                        }
                        else if (sMa != "" && clsMain.ReturnDataTable("SELECT MA_NHOMHANG FROM NHOMHANG WHERE MA_NHOMHANG=" + clsMain.SQLString(sMa)).Rows.Count == 0)
                        {
                            SaveFileNhomHang(sMa, sTen, sGhiChu, sSuDung, sThucDon, sSTT);
                        }
                    }
                    catch (Exception ex)  
                    {
                        this.Cursor = Cursors.Default;
                    }
                }

                xlWorkbook.Close(true, null, null);
                xlApp.Quit();

                Marshal.ReleaseComObject(xlWorksheet);
                Marshal.ReleaseComObject(xlWorkbook);
                Marshal.ReleaseComObject(xlApp);

                LoaddataGridView();
                LoadCombo();

                XtraMessageBox.Show("Lưu thành công", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Cursor = Cursors.Default;
            }
        }

        private void SaveFileNhomHang(String sMa, String sTen, String sGhiChu, String sSuDung, String sThucDon, String sSTT)
        {
            try
            {
                SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
                SqlCommand SqlComMain = null;
                String sSQL = "";
                sSQL += "SET IDENTITY_INSERT NHOMHANG ON " + "\n";
                sSQL += "Insert into NHOMHANG(MA_NHOMHANG,TEN_NHOMHANG,GHICHU,SUDUNG,THUCDON,STT) Values(@MA_NHOMHANG,@TEN_NHOMHANG,@GHICHU,@SUDUNG,@THUCDON,@STT)" + "\n";
                sSQL += "SET IDENTITY_INSERT NHOMHANG OFF " + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);
                SqlCom.Parameters.Add(new SqlParameter("@MA_NHOMHANG", (object)sMa));
                SqlCom.Parameters.Add(new SqlParameter("@TEN_NHOMHANG", (object)sTen));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)sGhiChu));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)sSuDung));
                SqlCom.Parameters.Add(new SqlParameter("@THUCDON", (object)sThucDon));
                SqlCom.Parameters.Add(new SqlParameter("@STT", (object)sSTT));
                SqlComMain = SqlCom;

                Boolean bRunSQL = true;
                try {
                    this.Cursor = Cursors.WaitCursor;
                    CN.Open();
                    SqlComMain.ExecuteNonQuery();
                    CN.Close();
                    this.Cursor = Cursors.Default;
                    bRunSQL = true;
                }
                catch {
                    bRunSQL = false;
                }
            }
            catch (Exception e)
            {

            }
        }

        private void UpdateFileNhomHang(String sMa, String sTen, String sGhiChu, String sSuDung, String sThucDon, String sSTT)
        {
            try
            {
                SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
                SqlCommand SqlComMain = null;
                String sSQL = "";
                sSQL += "UPDATE NHOMHANG SET TEN_NHOMHANG = @TEN_NHOMHANG, GHICHU = @GHICHU,SUDUNG = @SUDUNG,THUCDON = @THUCDON,STT = @STT " + "\n";
                sSQL += "WHERE MA_NHOMHANG = " + clsMain.SQLString(sMa) + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);
                SqlCom.Parameters.Add(new SqlParameter("@TEN_NHOMHANG", (object)sTen));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)sGhiChu));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)sSuDung));
                SqlCom.Parameters.Add(new SqlParameter("@THUCDON", (object)sThucDon));
                SqlCom.Parameters.Add(new SqlParameter("@STT", (object)sSTT));
                SqlComMain = SqlCom;

                Boolean bRunSQL = true;
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    CN.Open();
                    SqlComMain.ExecuteNonQuery();
                    CN.Close();
                    this.Cursor = Cursors.Default;
                    bRunSQL = true;
                }
                catch
                {
                    bRunSQL = false;
                }
            }
            catch (Exception e)
            {

            }
        }

        private void btnFileHangHoa_Click(object sender, EventArgs e)
        {
            OpenFileDialog sOpenFile = new OpenFileDialog();
            sOpenFile.Filter = Filter();
            sOpenFile.FilterIndex = 1;
            sOpenFile.RestoreDirectory = true;
            sOpenFile.Multiselect = false;
            sOpenFile.Title = "Chọn file";
            if (sOpenFile.ShowDialog() == DialogResult.OK)
            {
                Excel.Application xlApp = new Excel.Application();
                Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(sOpenFile.FileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                Excel._Worksheet xlWorksheet = (Excel.Worksheet)xlWorkbook.Worksheets.get_Item(1);
                Excel.Range xlRange = xlWorksheet.UsedRange;

                for (int i = 2; i <= xlRange.Rows.Count; i++)
                {
                    try
                    {
                        String sMa = (String)(xlRange.Cells[i, 1] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 1] as Excel.Range).Value2.ToString() : "";
                        String sTen = (String)(xlRange.Cells[i, 2] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 2] as Excel.Range).Value2.ToString() : "";
                        String sMaNhomHang = (String)(xlRange.Cells[i, 3] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 3] as Excel.Range).Value2.ToString() : "";
                        String sMaDonViTinh = (String)(xlRange.Cells[i, 4] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 4] as Excel.Range).Value2.ToString() : "";
                        String sGiaNhap = (String)(xlRange.Cells[i, 5] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 5] as Excel.Range).Value2.ToString() : "0";
                        String sGiaBan1 = (String)(xlRange.Cells[i, 6] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 6] as Excel.Range).Value2.ToString() : "0";
                        String sGiaBan2 = (String)(xlRange.Cells[i, 7] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 7] as Excel.Range).Value2.ToString() : "0";
                        String sGhiChu = (String)(xlRange.Cells[i, 8] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 8] as Excel.Range).Value2.ToString() : "";
                        String sMaVach = (String)(xlRange.Cells[i, 9] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 9] as Excel.Range).Value2.ToString() : "";
                        String sSuDung = "1";
                        String sTonKho = (String)(xlRange.Cells[i, 10] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 10] as Excel.Range).Value2.ToString() : "0";
                        String sSoLuong = "0";
                        String sSTT = "1";


                        if (sMa != "" && clsMain.ReturnDataTable("SELECT MA_HANGHOA FROM HANGHOA WHERE ID_HANGHOA = " + clsMain.SQLString(sMa)).Rows.Count > 0)
                        {
                            UpdateFileHangHoa(sMa, sTen, sMaNhomHang, sGiaNhap, sGiaBan1, sGiaBan2, sGhiChu, sMaVach, sSuDung, sTonKho, sSoLuong, sSTT, sMaDonViTinh);
                        }
                        else if (sMaVach != "" && clsMain.ReturnDataTable("SELECT MA_HANGHOA FROM HANGHOA WHERE MAVACH = " + clsMain.SQLString(sMaVach)).Rows.Count > 0)
                        {
                            UpdateFileHangHoa1(sMa, sTen, sMaNhomHang, sGiaNhap, sGiaBan1, sGiaBan2, sGhiChu, sMaVach, sSuDung, sTonKho, sSoLuong, sSTT, sMaDonViTinh);
                        }
                        else if (sMa != "" && clsMain.ReturnDataTable("Select MA_HANGHOA From HANGHOA Where ID_HANGHOA = " + clsMain.SQLString(sMa)).Rows.Count == 0)
                            SaveFileHangHoa(sMa, sTen, sMaNhomHang, sGiaNhap, sGiaBan1, sGiaBan2, sGhiChu, sMaVach, sSuDung, sTonKho, sSoLuong, sSTT, sMaDonViTinh);
                    }
                    catch (Exception ex)
                    {
                        this.Cursor = Cursors.Default;
                    }
                }

                xlWorkbook.Close(true, null, null);
                xlApp.Quit();

                Marshal.ReleaseComObject(xlWorksheet);
                Marshal.ReleaseComObject(xlWorkbook);
                Marshal.ReleaseComObject(xlApp);

                LoaddataGridView1(smaNhomhang);
                LoadCombo();
                LoadGridview();

                XtraMessageBox.Show("Lưu thành công", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Cursor = Cursors.Default;
            }
        }

        private void SaveFileHangHoa(String sMa, String sTen, String sMaNhomHang, String sGiaNhap, String sGiaBan1, String sGiaBan2, String sGhiChu, String sMaVach, String sSuDung, String sTonKho, String sSoLuong, String sSTT, String sMaDonViTinh)
        {
            try
            {
                String sDVT = clsMain.ReturnDataTable("SELECT TOP 1 MA_DONVITINH FROM DONVITINH").Rows[0]["MA_DONVITINH"].ToString();
                String sBep = clsMain.ReturnDataTable("SELECT TOP 1 MA_BEP FROM DM_BEP").Rows[0]["MA_BEP"].ToString();

                SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
                SqlCommand SqlComMain = null;

                String sSQL = "";
                sSQL += "Insert into HANGHOA (TEN_HANGHOA,ID_HANGHOA,THUE,GIANHAP,GIABAN1,GIABAN2,GHICHU,MAVACH,SUDUNG,SUAGIA,TONKHO,TONTOITHIEU,MA_THANHPHAN,SOLUONG,MA_NHOMHANG,MA_DONVITINH,MA_BEP,STT,IS_INBEP,SUADINHLUONG)" + "\n";
                sSQL += "Values (@TEN_HANGHOA,@ID_HANGHOA,@THUE,@GIANHAP,@GIABAN1,@GIABAN2,@GHICHU,@MAVACH,@SUDUNG,@SUAGIA,@TONKHO,@TONTOITHIEU,@MA_THANHPHAN,@SOLUONG,@MA_NHOMHANG,@MA_DONVITINH,@MA_BEP,@STT,@IS_INBEP,@SUADINHLUONG)" + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);

                SqlCom.Parameters.Add(new SqlParameter("@ID_HANGHOA", (object)sMa));
                SqlCom.Parameters.Add(new SqlParameter("@TEN_HANGHOA", (object)sTen));
                SqlCom.Parameters.Add(new SqlParameter("@THUE", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@GIANHAP", (object)sGiaNhap.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN1", (object)sGiaBan1.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN2", (object)sGiaBan2.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)sGhiChu));
                SqlCom.Parameters.Add(new SqlParameter("@MAVACH", (object)sMaVach));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)sSuDung));
                SqlCom.Parameters.Add(new SqlParameter("@SUAGIA", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@TONKHO", (object)sTonKho));
                SqlCom.Parameters.Add(new SqlParameter("@TONTOITHIEU", (object)"1"));
                SqlCom.Parameters.Add(new SqlParameter("@MA_THANHPHAN", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@SOLUONG", (object)sSoLuong.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@MA_NHOMHANG", (object)sMaNhomHang));
                SqlCom.Parameters.Add(new SqlParameter("@MA_DONVITINH", (object)sMaDonViTinh));
                SqlCom.Parameters.Add(new SqlParameter("@STT", (object)sSTT.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@MA_BEP", (object)sBep));
                SqlCom.Parameters.Add(new SqlParameter("@IS_INBEP", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@SUADINHLUONG", (object)"0"));

                SqlComMain = SqlCom;

                Boolean bRunSQL = true;
                try {
                    this.Cursor = Cursors.WaitCursor;
                    CN.Open();
                    SqlComMain.ExecuteNonQuery();
                    CN.Close();
                    this.Cursor = Cursors.Default;
                    bRunSQL = true;
                }
                catch {
                    bRunSQL = false;
                }
            }
            catch (Exception e)
            {

            }
        }

        private void UpdateFileHangHoa(String sMa, String sTen, String sMaNhomHang, String sGiaNhap, String sGiaBan1, String sGiaBan2, String sGhiChu, String sMaVach, String sSuDung, String sTonKho, String sSoLuong, String sSTT, String sMaDonViTinh)
        {
            try
            {
                String sDVT = clsMain.ReturnDataTable("SELECT TOP 1 MA_DONVITINH FROM DONVITINH").Rows[0]["MA_DONVITINH"].ToString();
                String sBep = clsMain.ReturnDataTable("SELECT TOP 1 MA_BEP FROM DM_BEP").Rows[0]["MA_BEP"].ToString();

                SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
                SqlCommand SqlComMain = null;

                String sSQL = "";
                sSQL += "Update HANGHOA SET" + "\n";
                sSQL += "TEN_HANGHOA = @TEN_HANGHOA, THUE = @THUE, GIANHAP = @GIANHAP,";
                sSQL += "GIABAN1 = @GIABAN1, GIABAN2 = @GIABAN2, GHICHU = @GHICHU, MAVACH = @MAVACH, SUDUNG = @SUDUNG, SUAGIA = @SUAGIA,";
                sSQL += "TONKHO = @TONKHO, TONTOITHIEU = @TONTOITHIEU, MA_THANHPHAN = @MA_THANHPHAN, SOLUONG = @SOLUONG, MA_NHOMHANG = @MA_NHOMHANG,";
                sSQL += "MA_DONVITINH = @MA_DONVITINH, MA_BEP = @MA_BEP, STT = @STT, IS_INBEP = @IS_INBEP, SUADINHLUONG = @SUADINHLUONG" + "\n";
                sSQL += "WHERE ID_HANGHOA = " + clsMain.SQLString(sMa) + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);

                SqlCom.Parameters.Add(new SqlParameter("@TEN_HANGHOA", (object)sTen));
                SqlCom.Parameters.Add(new SqlParameter("@THUE", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@GIANHAP", (object)sGiaNhap.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN1", (object)sGiaBan1.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN2", (object)sGiaBan2.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)sGhiChu));
                SqlCom.Parameters.Add(new SqlParameter("@MAVACH", (object)sMaVach));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)sSuDung));
                SqlCom.Parameters.Add(new SqlParameter("@SUAGIA", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@TONKHO", (object)sTonKho));
                SqlCom.Parameters.Add(new SqlParameter("@TONTOITHIEU", (object)"1"));
                SqlCom.Parameters.Add(new SqlParameter("@MA_THANHPHAN", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@SOLUONG", (object)sSoLuong.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@MA_NHOMHANG", (object)sMaNhomHang));
                SqlCom.Parameters.Add(new SqlParameter("@MA_DONVITINH", (object)sMaDonViTinh));
                SqlCom.Parameters.Add(new SqlParameter("@STT", (object)sSTT.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@MA_BEP", (object)sBep));
                SqlCom.Parameters.Add(new SqlParameter("@IS_INBEP", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@SUADINHLUONG", (object)"0"));

                SqlComMain = SqlCom;

                Boolean bRunSQL = true;
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    CN.Open();
                    SqlComMain.ExecuteNonQuery();
                    CN.Close();
                    this.Cursor = Cursors.Default;
                    bRunSQL = true;
                }
                catch
                {
                    bRunSQL = false;
                }
            }
            catch (Exception e)
            {

            }
        }

        private void UpdateFileHangHoa1(String sMa, String sTen, String sMaNhomHang, String sGiaNhap, String sGiaBan1, String sGiaBan2, String sGhiChu, String sMaVach, String sSuDung, String sTonKho, String sSoLuong, String sSTT, String sMaDonViTinh)
        {
            try
            {
                String sDVT = clsMain.ReturnDataTable("SELECT TOP 1 MA_DONVITINH FROM DONVITINH").Rows[0]["MA_DONVITINH"].ToString();
                String sBep = clsMain.ReturnDataTable("SELECT TOP 1 MA_BEP FROM DM_BEP").Rows[0]["MA_BEP"].ToString();

                SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
                SqlCommand SqlComMain = null;

                String sSQL = "";
                sSQL += "Update HANGHOA SET" + "\n";
                sSQL += "TEN_HANGHOA = @TEN_HANGHOA, THUE = @THUE, GIANHAP = @GIANHAP,";
                sSQL += "GIABAN1 = @GIABAN1, GIABAN2 = @GIABAN2, GHICHU = @GHICHU, MAVACH = @MAVACH, SUDUNG = @SUDUNG, SUAGIA = @SUAGIA,";
                sSQL += "TONKHO = @TONKHO, TONTOITHIEU = @TONTOITHIEU, MA_THANHPHAN = @MA_THANHPHAN, SOLUONG = @SOLUONG, MA_NHOMHANG = @MA_NHOMHANG,";
                sSQL += "MA_DONVITINH = @MA_DONVITINH, MA_BEP = @MA_BEP, STT = @STT, IS_INBEP = @IS_INBEP, SUADINHLUONG = @SUADINHLUONG" + "\n";
                sSQL += "WHERE MAVACH = " + clsMain.SQLString(sMaVach) + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);

                SqlCom.Parameters.Add(new SqlParameter("@TEN_HANGHOA", (object)sTen));
                SqlCom.Parameters.Add(new SqlParameter("@THUE", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@GIANHAP", (object)sGiaNhap.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN1", (object)sGiaBan1.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GIABAN2", (object)sGiaBan2.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)sGhiChu));
                SqlCom.Parameters.Add(new SqlParameter("@MAVACH", (object)sMaVach));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)sSuDung));
                SqlCom.Parameters.Add(new SqlParameter("@SUAGIA", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@TONKHO", (object)sTonKho));
                SqlCom.Parameters.Add(new SqlParameter("@TONTOITHIEU", (object)"1"));
                SqlCom.Parameters.Add(new SqlParameter("@MA_THANHPHAN", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@SOLUONG", (object)sSoLuong.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@MA_NHOMHANG", (object)sMaNhomHang));
                SqlCom.Parameters.Add(new SqlParameter("@MA_DONVITINH", (object)sMaDonViTinh));
                SqlCom.Parameters.Add(new SqlParameter("@STT", (object)sSTT.Replace(",", "")));
                SqlCom.Parameters.Add(new SqlParameter("@MA_BEP", (object)sBep));
                SqlCom.Parameters.Add(new SqlParameter("@IS_INBEP", (object)"0"));
                SqlCom.Parameters.Add(new SqlParameter("@SUADINHLUONG", (object)"0"));

                SqlComMain = SqlCom;

                Boolean bRunSQL = true;
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    CN.Open();
                    SqlComMain.ExecuteNonQuery();
                    CN.Close();
                    this.Cursor = Cursors.Default;
                    bRunSQL = true;
                }
                catch
                {
                    bRunSQL = false;
                }
            }
            catch (Exception e)
            {

            }
        }


        public static String Filter()
        {
            String Filter = "";
            Filter += "Excel (*.xlsx)|*.xlsx|";
            Filter += "Excel 97-2003 (*.xls)|*.xls|";
            Filter += "Word Document (*.docx)|*.docx|";
            Filter += "Word 97-2003 Document (*.doc)|*.doc|";
            Filter += "PDF (*.pdf)|*.pdf|";
            Filter += "Plain Text (*.txt)|*.txt|";
            Filter += "XML Document (*.xml)|*.xml|";
            Filter += "Web Page (*.htm, *.html)|*.htm;*.html|";
            Filter += "PNG (*.png)|*.png|";
            Filter += "JPEG (*.jpeg;*.jpg;*.jpe)|*.jpeg;*.jpg;*.jpe|";
            Filter += "GIF (*.gif)|*.gif|";
            Filter += "All File|*.*";

            return Filter;
        }

        private void btnFileHangHoa_Mo_Click(object sender, EventArgs e)
        {
            string filename = Application.StartupPath + "\\FileHangHoa.xlsx";
            FileInfo fi = new FileInfo(filename);
            if (fi.Exists)
            {
                System.Diagnostics.Process.Start(filename);
            }
        }

        private void btnFileNhomhang_Mo_Click(object sender, EventArgs e)
        {
            string filename = Application.StartupPath + "\\FileNhomHang.xlsx";
            FileInfo fi = new FileInfo(filename);
            if (fi.Exists)
            {
                System.Diagnostics.Process.Start(filename);
            }
        }

        private void btnAutoMaVach_Click(object sender, EventArgs e)
        {
            txtMAVACH.Text = clsMain.ReturnDataTable("SELECT [dbo].[fc_NewCodeID_MaVach]  ()").Rows[0][0].ToString();
        }

        private void cboPLU_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboPLU.Properties.ReadOnly == false)
                {
                    txtMAVACH.Text = int.Parse(cboPLU.Text).ToString("00000");
                }
            }
            catch
            {
            }
        }

        private void txtLoinhuan_EditValueChanged(object sender, EventArgs e)
        {
            try 
            {
                double loinhuan = double.Parse(txtLoinhuan.Text);
                txtGIABAN1.EditValue = long.Parse ( txtGIAMUA.EditValue .ToString ().Replace (",","")) + long.Parse ( txtGIAMUA.EditValue .ToString ().Replace (",","")) * loinhuan /100; 
                //txtGIABAN1.EditValue = (long.Parse(txtGIAMUA.EditValue.ToString().Replace(",", ""))*100)/(100-loinhuan ); 
            }
            catch 
            {
            }
        }

        private void txtGIAMUA_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                double loinhuan = double.Parse(txtLoinhuan.Text);
                txtGIABAN1.EditValue = (long.Parse(txtGIAMUA.EditValue.ToString().Replace(",", "")) * 100) / (100 - loinhuan); 
            }
            catch
            {
            }
        }

       
       
    }
}