﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Dinhluong : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Dinhluong()
        {
            InitializeComponent();
            this.Text = rm.GetString("dinhluong", culture);
            LoadCombo();
            LoaddataGridView();
            LoadPermission();
        }

        private void Frm_Dinhluong_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        string sma = "";

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            cboThanhpham.EditValue = iThanhphan; 
            cboThanhpham .Focus();
            if (cboThanhpham.Text == "")
            {
                gridView2.ActiveFilterString = "[SUDUNG] = True";
            }
            else
            {
                gridView2.ActiveFilterString = "[SUDUNG] = True And [TENTHANHPHAM]=" + clsMain.SQLString(cboThanhpham.Text);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show(_xacnhanbanmuonxoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From DINHLUONG" + "\n";
            sSQL += "Where MADINHLUONG=" + clsMain.SQLString(sma) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl(false);
            cboThanhpham.Properties.ReadOnly = true;
            cboNguyenlieu.Properties.ReadOnly = true;
            cboThanhpham .Focus();
        }

        int iThanhphan = 0;
        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            //Lất số lượng nguyên liệu dành cho 1 đơn vị thành phẩm
            float sl = float.Parse(txtSOLUONG.Text) / float.Parse(txtSOLUONGTP.Text);
            string sSQL = "";
            if (sma == "" && btnThem.Enabled)
            {
                sSQL += "Insert into DINHLUONG (MATHANHPHAM,MANGUYENLIEU,SOLUONG,SUDUNG)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLString(cboThanhpham .EditValue .ToString ()) + ",";
                sSQL += clsMain.SQLString(cboNguyenlieu .EditValue .ToString ()) + ",";
                sSQL += clsMain.SQLString(sl.ToString()) + ",";
                sSQL += clsMain.SQLBit(chkSUDUNG.Checked) + ")";
            }
            else if ((sma != "" && btnSua.Enabled))
            {
                sSQL += "Update DINHLUONG Set " + "\n";
                sSQL += "MATHANHPHAM=" + clsMain.SQLString(cboThanhpham.EditValue.ToString()) + "," + "\n";
                sSQL += "MANGUYENLIEU=" + clsMain.SQLString(cboNguyenlieu.EditValue.ToString()) + "," + "\n";
                sSQL += "SOLUONG=" + clsMain.SQLString(sl.ToString()) + "," + "\n";
                sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNG.Checked) + "\n";
                sSQL += "Where MADINHLUONG=" + clsMain.SQLString(sma) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                iThanhphan =int.Parse ( cboThanhpham.EditValue.ToString ());
                LoaddataGridView();
                btnThem.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void cboThanhpham_EditValueChanged(object sender, EventArgs e)
        {
            LookUpEdit cboTemp = (LookUpEdit)sender;
            lblDVTTHANHPHAM.Text = "(" + cboTemp.GetColumnValue("DVT") + ")";
            lblDVTTHANHPHAM1.Text = "(" + cboTemp.GetColumnValue("DVT") + ")";
        }

        private void cboNguyenlieu_EditValueChanged(object sender, EventArgs e)
        {
            LookUpEdit cboTemp = (LookUpEdit)sender;
            lblDVTNGUYENLIEU.Text = "(" + cboTemp.GetColumnValue("DVT") + ")";
            lblDVTNGUYENLIEU1.Text = "(" + cboTemp.GetColumnValue("DVT") + ")";
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
            if (e.Column == CAN & e.RowHandle >= 0)
                e.DisplayText = "=";
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sma = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            cboThanhpham.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MATHANHPHAM").ToString());
            iThanhphan = int.Parse(cboThanhpham.EditValue.ToString());
            cboNguyenlieu.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MANGUYENLIEU").ToString());
            txtSOLUONG.EditValue  = double .Parse ( gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SOLUONG").ToString());
            txtSOLUONGTP.EditValue = 1;
            chkSUDUNG.Checked = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SUDUNG").ToString());
            KhoaMoControl(true);
        }

        private void LoadCombo()
        {
            string sSQL = "";
            sSQL += "Select  A.MA_HANGHOA As MA,A.TEN_HANGHOA As TEN,A.MA_DONVITINH,B.TEN_DONVITINH As DVT" + "\n";
            sSQL += "From HANGHOA A,DONVITINH B,NHOMHANG C" + "\n";
            sSQL += "Where A.MA_DONVITINH=B.MA_DONVITINH" + "\n";
            sSQL += "And A.MA_NHOMHANG=C.MA_NHOMHANG" + "\n";
            sSQL += "And A.SUDUNG=1" + "\n";
            sSQL += "And C.THUCDON=1" + "\n";
            sSQL += "And A.TONKHO=0" + "\n";
            sSQL += "Order by A.TEN_HANGHOA" + "\n";

            DataTable dt = clsMain.ReturnDataTable(sSQL);
            cboThanhpham.Properties.DataSource = dt;
            cboThanhpham.EditValue = cboThanhpham.Properties.GetDataSourceValue(cboThanhpham.Properties.ValueMember, 0);


            sSQL = "";
            sSQL += "Select  A.MA_HANGHOA As MA,A.TEN_HANGHOA As TEN,A.MA_DONVITINH,B.TEN_DONVITINH As DVT" + "\n";
            sSQL += "From HANGHOA A,DONVITINH B,NHOMHANG C" + "\n";
            sSQL += "Where A.MA_DONVITINH=B.MA_DONVITINH" + "\n";
            sSQL += "And A.MA_NHOMHANG=C.MA_NHOMHANG" + "\n";
            sSQL += "And A.SUDUNG=1" + "\n";
            sSQL += "And C.THUCDON=0" + "\n";
            sSQL += "Order by A.TEN_HANGHOA" + "\n";

            dt = clsMain.ReturnDataTable(sSQL);
            cboNguyenlieu.Properties.DataSource = dt;
            cboNguyenlieu.EditValue = cboNguyenlieu.Properties.GetDataSourceValue(cboNguyenlieu.Properties.ValueMember, 0);
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            string sSQL = "";
            sSQL += "Select MADINHLUONG As MA ,MATHANHPHAM ,(Select TEN_HANGHOA From HANGHOA Where MA_HANGHOA = MATHANHPHAM) As TENTHANHPHAM,(Select A.TEN_DONVITINH From DONVITINH A,HANGHOA B Where B.MA_DONVITINH=A.MA_DONVITINH And B.MA_HANGHOA = MATHANHPHAM) As DVTTHANHPHAM ,MANGUYENLIEU,(Select TEN_HANGHOA From HANGHOA Where MA_HANGHOA = MANGUYENLIEU) As TENNGUYENLIEU,(Select A.TEN_DONVITINH From DONVITINH A,HANGHOA B Where B.MA_DONVITINH=A.MA_DONVITINH And B.MA_HANGHOA = MANGUYENLIEU) As DVTNGUYENLIEU,SOLUONG,SUDUNG,0 As CAN" + "\n";
            sSQL += "From DINHLUONG" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            gridControl2.DataSource = dt;
            gridView2.ActiveFilterString = "[SUDUNG] = True";
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("41");
            btnSua.Enabled = clsUserManagement.AllowEdit("41");
            btnXoa.Enabled = clsUserManagement.AllowDelete("41");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//khóa
            {
                cboThanhpham.Properties.ReadOnly = true;
                cboNguyenlieu.Properties.ReadOnly = true;
                txtSOLUONG.Properties.ReadOnly = true;
                txtSOLUONGTP.Properties.ReadOnly = true;
                chkSUDUNG.Properties.ReadOnly = true;
            }
            else//mở
            {
                cboThanhpham.Properties.ReadOnly = false;
                cboNguyenlieu.Properties.ReadOnly = false;
                txtSOLUONG.Properties.ReadOnly = false;
                txtSOLUONGTP.Properties.ReadOnly = false;
                chkSUDUNG.Properties.ReadOnly = false;
            }
        }

        private void SetRongControl()
        {
            sma = "";
            cboThanhpham.EditValue = cboThanhpham.Properties.GetDataSourceValue(cboThanhpham.Properties.ValueMember, 0);
            cboNguyenlieu.EditValue = cboNguyenlieu.Properties.GetDataSourceValue(cboNguyenlieu.Properties.ValueMember, 0);
            txtSOLUONG.EditValue = 0;
            txtSOLUONGTP.EditValue = 0;
            chkSUDUNG.Checked = true;
        }

        private Boolean CheckInput()
        {
            if (txtSOLUONG.Text == "0" || txtSOLUONG.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_nhap + lblSoluong1.Text, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSOLUONG.Focus();
                return false;
            }
            if (txtSOLUONGTP.Text == "0" || txtSOLUONGTP.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_nhap + lblSoluong1.Text, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSOLUONGTP.Focus();
                return false;
            }
            if (sma == "" && btnThem.Enabled && clsMain.ReturnDataTable("Select MATHANHPHAM,MANGUYENLIEU From DINHLUONG Where MATHANHPHAM=" + clsMain.SQLString(cboThanhpham.EditValue.ToString()) + "And MANGUYENLIEU=" + clsMain.SQLString(cboNguyenlieu.EditValue.ToString())).Rows.Count > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_nguyenlieudanhap, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboNguyenlieu .Focus();
                return false;
            }
            return true;
        }

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                txt.SelectAll();
            }
            catch
            {
            }
        }

        string _nhap = "";
        string _thongbao = "";
        string _luukothanhcong = "";
        string _xacnhanbanmuonxoa = "";
        string _nguyenlieudanhap = "";
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void Frm_Dinhluong_Load(object sender, EventArgs e)
        {
            foreach (Control ctl in panelControl1.Controls)
            {
                ctl.Click += new EventHandler(ctl_Click);
            }

       
          
            this.Text = rm.GetString("dinhluong", culture);
            lblTEN.Text = rm.GetString("thanhpham", culture);
            lbSoLuong.Text = rm.GetString("soluong2", culture);
            lbNguyenLieu.Text = rm.GetString("nguyenlieu", culture);
            lblSoluong1.Text = rm.GetString("soluong2", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _nhap = rm.GetString("nhap", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);
            _xacnhanbanmuonxoa = rm.GetString("xacnhanbanmuonxoa", culture);
            _nguyenlieudanhap = rm.GetString("nguyenlieudanhap", culture);

            chkSUDUNG.Text = rm.GetString("sudung", culture);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);

            gridView2.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView2.Columns["MA"].Caption = rm.GetString("ma", culture);
            gridView2.Columns["TENTHANHPHAM"].Caption = rm.GetString("thanhpham", culture);
            gridView2.Columns["DVTTHANHPHAM"].Caption = rm.GetString("dvt", culture);
            gridView2.Columns["CAN"].Caption = rm.GetString("can", culture);
            gridView2.Columns["TENNGUYENLIEU"].Caption = rm.GetString("nguyenlieu", culture);
            gridView2.Columns["DVTNGUYENLIEU"].Caption = rm.GetString("dvt", culture);
            gridView2.Columns["SOLUONG"].Caption = rm.GetString("soluong2", culture);
            gridView2.Columns["SUDUNG"].Caption = rm.GetString("sudung", culture);
            gridView2.GroupPanelText = rm.GetString("ktcvddntimkiem", culture);
        }
    }
}