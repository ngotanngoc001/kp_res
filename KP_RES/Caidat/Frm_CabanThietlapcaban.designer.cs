﻿namespace KP_RES 
{
    partial class Frm_CabanThietlapcaban 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_CabanThietlapcaban));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.cboCuahang = new DevExpress.XtraEditors.LookUpEdit();
            this.lbCuaHang = new DevExpress.XtraEditors.LabelControl();
            this.chkSUDUNG1 = new DevExpress.XtraEditors.CheckEdit();
            this.lbGhiChu = new DevExpress.XtraEditors.LabelControl();
            this.txtGHICHU1 = new DevExpress.XtraEditors.TextEdit();
            this.lbNhanVien = new DevExpress.XtraEditors.LabelControl();
            this.cboQuay = new DevExpress.XtraEditors.LookUpEdit();
            this.cboNhanvien = new DevExpress.XtraEditors.LookUpEdit();
            this.cboCa = new DevExpress.XtraEditors.LookUpEdit();
            this.lbQuay = new DevExpress.XtraEditors.LabelControl();
            this.lbCaBan = new DevExpress.XtraEditors.LabelControl();
            this.btnLuu1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem1 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.timeBD = new DevExpress.XtraEditors.TimeEdit();
            this.timeKT = new DevExpress.XtraEditors.TimeEdit();
            this.lbGioKetThuc = new DevExpress.XtraEditors.LabelControl();
            this.lbGioBatDau = new DevExpress.XtraEditors.LabelControl();
            this.chkSUDUNG = new DevExpress.XtraEditors.CheckEdit();
            this.lblGHICHU = new DevExpress.XtraEditors.LabelControl();
            this.txtGHICHU = new DevExpress.XtraEditors.TextEdit();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.txtTEN = new DevExpress.XtraEditors.TextEdit();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIOBATDAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SUDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIOKETTHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_CUAHANG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_QUAY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENNHANVIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_CABAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SUDUNG1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL1 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboCuahang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHU1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboQuay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhanvien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timeBD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeKT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHU.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl6);
            this.panelControl1.Controls.Add(this.panelControl5);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 238);
            this.panelControl1.TabIndex = 0;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.cboCuahang);
            this.panelControl6.Controls.Add(this.lbCuaHang);
            this.panelControl6.Controls.Add(this.chkSUDUNG1);
            this.panelControl6.Controls.Add(this.lbGhiChu);
            this.panelControl6.Controls.Add(this.txtGHICHU1);
            this.panelControl6.Controls.Add(this.lbNhanVien);
            this.panelControl6.Controls.Add(this.cboQuay);
            this.panelControl6.Controls.Add(this.cboNhanvien);
            this.panelControl6.Controls.Add(this.cboCa);
            this.panelControl6.Controls.Add(this.lbQuay);
            this.panelControl6.Controls.Add(this.lbCaBan);
            this.panelControl6.Controls.Add(this.btnLuu1);
            this.panelControl6.Controls.Add(this.btnSua1);
            this.panelControl6.Controls.Add(this.btnXoa1);
            this.panelControl6.Controls.Add(this.btnThem1);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(500, 2);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(518, 234);
            this.panelControl6.TabIndex = 0;
            // 
            // cboCuahang
            // 
            this.cboCuahang.EnterMoveNextControl = true;
            this.cboCuahang.Location = new System.Drawing.Point(95, 3);
            this.cboCuahang.Name = "cboCuahang";
            this.cboCuahang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCuahang.Properties.Appearance.Options.UseFont = true;
            this.cboCuahang.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.cboCuahang.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.cboCuahang.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.cboCuahang.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.cboCuahang.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCuahang.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboCuahang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboCuahang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "MA", 40, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", 100, "TEN")});
            this.cboCuahang.Properties.DisplayMember = "TEN";
            this.cboCuahang.Properties.DropDownItemHeight = 40;
            this.cboCuahang.Properties.NullText = "";
            this.cboCuahang.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboCuahang.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.cboCuahang.Properties.ShowHeader = false;
            this.cboCuahang.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboCuahang.Properties.ValueMember = "MA";
            this.cboCuahang.Size = new System.Drawing.Size(248, 26);
            this.cboCuahang.TabIndex = 14;
            this.cboCuahang.EditValueChanged += new System.EventHandler(this.cboCuahang_EditValueChanged);
            // 
            // lbCuaHang
            // 
            this.lbCuaHang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCuaHang.Location = new System.Drawing.Point(6, 6);
            this.lbCuaHang.Margin = new System.Windows.Forms.Padding(4);
            this.lbCuaHang.Name = "lbCuaHang";
            this.lbCuaHang.Size = new System.Drawing.Size(68, 19);
            this.lbCuaHang.TabIndex = 13;
            this.lbCuaHang.Text = "Cửa hàng";
            // 
            // chkSUDUNG1
            // 
            this.chkSUDUNG1.EnterMoveNextControl = true;
            this.chkSUDUNG1.Location = new System.Drawing.Point(93, 163);
            this.chkSUDUNG1.Name = "chkSUDUNG1";
            this.chkSUDUNG1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNG1.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNG1.Properties.Caption = "Sử dụng";
            this.chkSUDUNG1.Size = new System.Drawing.Size(92, 24);
            this.chkSUDUNG1.TabIndex = 8;
            // 
            // lbGhiChu
            // 
            this.lbGhiChu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGhiChu.Location = new System.Drawing.Point(6, 134);
            this.lbGhiChu.Margin = new System.Windows.Forms.Padding(4);
            this.lbGhiChu.Name = "lbGhiChu";
            this.lbGhiChu.Size = new System.Drawing.Size(54, 19);
            this.lbGhiChu.TabIndex = 6;
            this.lbGhiChu.Text = "Ghi chú";
            // 
            // txtGHICHU1
            // 
            this.txtGHICHU1.EnterMoveNextControl = true;
            this.txtGHICHU1.Location = new System.Drawing.Point(95, 131);
            this.txtGHICHU1.Name = "txtGHICHU1";
            this.txtGHICHU1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHU1.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHU1.Size = new System.Drawing.Size(248, 26);
            this.txtGHICHU1.TabIndex = 7;
            // 
            // lbNhanVien
            // 
            this.lbNhanVien.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNhanVien.Location = new System.Drawing.Point(6, 102);
            this.lbNhanVien.Margin = new System.Windows.Forms.Padding(4);
            this.lbNhanVien.Name = "lbNhanVien";
            this.lbNhanVien.Size = new System.Drawing.Size(71, 19);
            this.lbNhanVien.TabIndex = 4;
            this.lbNhanVien.Text = "Nhân viên";
            // 
            // cboQuay
            // 
            this.cboQuay.EnterMoveNextControl = true;
            this.cboQuay.Location = new System.Drawing.Point(95, 35);
            this.cboQuay.Name = "cboQuay";
            this.cboQuay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboQuay.Properties.Appearance.Options.UseFont = true;
            this.cboQuay.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.cboQuay.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.cboQuay.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.cboQuay.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.cboQuay.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboQuay.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboQuay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboQuay.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", 40, "TEN"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN_KHO", 100, "TEN_KHO")});
            this.cboQuay.Properties.DisplayMember = "TEN";
            this.cboQuay.Properties.DropDownItemHeight = 40;
            this.cboQuay.Properties.NullText = "";
            this.cboQuay.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboQuay.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.cboQuay.Properties.ShowHeader = false;
            this.cboQuay.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboQuay.Properties.ValueMember = "MA";
            this.cboQuay.Size = new System.Drawing.Size(248, 26);
            this.cboQuay.TabIndex = 1;
            // 
            // cboNhanvien
            // 
            this.cboNhanvien.EnterMoveNextControl = true;
            this.cboNhanvien.Location = new System.Drawing.Point(95, 99);
            this.cboNhanvien.Name = "cboNhanvien";
            this.cboNhanvien.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhanvien.Properties.Appearance.Options.UseFont = true;
            this.cboNhanvien.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.cboNhanvien.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.cboNhanvien.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.cboNhanvien.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.cboNhanvien.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhanvien.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboNhanvien.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboNhanvien.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", 40, "MA"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", 100, "TEN")});
            this.cboNhanvien.Properties.DisplayMember = "TEN";
            this.cboNhanvien.Properties.DropDownItemHeight = 40;
            this.cboNhanvien.Properties.NullText = "";
            this.cboNhanvien.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboNhanvien.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.cboNhanvien.Properties.ShowHeader = false;
            this.cboNhanvien.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboNhanvien.Properties.ValueMember = "MA";
            this.cboNhanvien.Size = new System.Drawing.Size(248, 26);
            this.cboNhanvien.TabIndex = 5;
            // 
            // cboCa
            // 
            this.cboCa.EnterMoveNextControl = true;
            this.cboCa.Location = new System.Drawing.Point(95, 67);
            this.cboCa.Name = "cboCa";
            this.cboCa.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCa.Properties.Appearance.Options.UseFont = true;
            this.cboCa.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.cboCa.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.cboCa.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.cboCa.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.cboCa.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCa.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboCa.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboCa.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", 30, "TEN"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("GIOBATDAU", 40, "GIOBATDAU"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("GIOKETTHU", 40, "GIOKETTHU")});
            this.cboCa.Properties.DisplayMember = "TEN";
            this.cboCa.Properties.DropDownItemHeight = 40;
            this.cboCa.Properties.NullText = "";
            this.cboCa.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboCa.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.cboCa.Properties.ShowHeader = false;
            this.cboCa.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboCa.Properties.ValueMember = "MA";
            this.cboCa.Size = new System.Drawing.Size(248, 26);
            this.cboCa.TabIndex = 3;
            // 
            // lbQuay
            // 
            this.lbQuay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbQuay.Location = new System.Drawing.Point(6, 38);
            this.lbQuay.Margin = new System.Windows.Forms.Padding(4);
            this.lbQuay.Name = "lbQuay";
            this.lbQuay.Size = new System.Drawing.Size(37, 19);
            this.lbQuay.TabIndex = 0;
            this.lbQuay.Text = "Quầy";
            // 
            // lbCaBan
            // 
            this.lbCaBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCaBan.Location = new System.Drawing.Point(6, 70);
            this.lbCaBan.Margin = new System.Windows.Forms.Padding(4);
            this.lbCaBan.Name = "lbCaBan";
            this.lbCaBan.Size = new System.Drawing.Size(49, 19);
            this.lbCaBan.TabIndex = 2;
            this.lbCaBan.Text = "Ca bán";
            // 
            // btnLuu1
            // 
            this.btnLuu1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu1.Appearance.Options.UseFont = true;
            this.btnLuu1.Image = ((System.Drawing.Image)(resources.GetObject("btnLuu1.Image")));
            this.btnLuu1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu1.Location = new System.Drawing.Point(95, 194);
            this.btnLuu1.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu1.Name = "btnLuu1";
            this.btnLuu1.Size = new System.Drawing.Size(80, 35);
            this.btnLuu1.TabIndex = 9;
            this.btnLuu1.Text = "&Lưu";
            this.btnLuu1.Click += new System.EventHandler(this.btnLuu1_Click);
            // 
            // btnSua1
            // 
            this.btnSua1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua1.Appearance.Options.UseFont = true;
            this.btnSua1.Image = ((System.Drawing.Image)(resources.GetObject("btnSua1.Image")));
            this.btnSua1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua1.Location = new System.Drawing.Point(271, 194);
            this.btnSua1.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua1.Name = "btnSua1";
            this.btnSua1.Size = new System.Drawing.Size(80, 35);
            this.btnSua1.TabIndex = 11;
            this.btnSua1.Text = "&Sửa";
            this.btnSua1.Click += new System.EventHandler(this.btnSua1_Click);
            // 
            // btnXoa1
            // 
            this.btnXoa1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa1.Appearance.Options.UseFont = true;
            this.btnXoa1.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa1.Image")));
            this.btnXoa1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa1.Location = new System.Drawing.Point(359, 194);
            this.btnXoa1.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa1.Name = "btnXoa1";
            this.btnXoa1.Size = new System.Drawing.Size(86, 35);
            this.btnXoa1.TabIndex = 12;
            this.btnXoa1.Text = "&Xóa";
            this.btnXoa1.Click += new System.EventHandler(this.btnXoa1_Click);
            // 
            // btnThem1
            // 
            this.btnThem1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem1.Appearance.Options.UseFont = true;
            this.btnThem1.Image = ((System.Drawing.Image)(resources.GetObject("btnThem1.Image")));
            this.btnThem1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem1.Location = new System.Drawing.Point(183, 194);
            this.btnThem1.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem1.Name = "btnThem1";
            this.btnThem1.Size = new System.Drawing.Size(80, 35);
            this.btnThem1.TabIndex = 10;
            this.btnThem1.Text = "&Thêm";
            this.btnThem1.Click += new System.EventHandler(this.btnThem1_Click);
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.timeBD);
            this.panelControl5.Controls.Add(this.timeKT);
            this.panelControl5.Controls.Add(this.lbGioKetThuc);
            this.panelControl5.Controls.Add(this.lbGioBatDau);
            this.panelControl5.Controls.Add(this.chkSUDUNG);
            this.panelControl5.Controls.Add(this.lblGHICHU);
            this.panelControl5.Controls.Add(this.txtGHICHU);
            this.panelControl5.Controls.Add(this.lblTEN);
            this.panelControl5.Controls.Add(this.txtTEN);
            this.panelControl5.Controls.Add(this.btnThem);
            this.panelControl5.Controls.Add(this.btnXoa);
            this.panelControl5.Controls.Add(this.btnSua);
            this.panelControl5.Controls.Add(this.btnLuu);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl5.Location = new System.Drawing.Point(2, 2);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(498, 234);
            this.panelControl5.TabIndex = 0;
            // 
            // timeBD
            // 
            this.timeBD.EditValue = new System.DateTime(2014, 7, 15, 0, 0, 0, 0);
            this.timeBD.EnterMoveNextControl = true;
            this.timeBD.Location = new System.Drawing.Point(95, 67);
            this.timeBD.Name = "timeBD";
            this.timeBD.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeBD.Properties.Appearance.Options.UseFont = true;
            this.timeBD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.timeBD.Properties.DisplayFormat.FormatString = "HH:mm:ss";
            this.timeBD.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeBD.Properties.EditFormat.FormatString = "HH:mm:ss";
            this.timeBD.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeBD.Properties.Mask.EditMask = "HH:mm:ss";
            this.timeBD.Size = new System.Drawing.Size(248, 26);
            this.timeBD.TabIndex = 3;
            // 
            // timeKT
            // 
            this.timeKT.EditValue = new System.DateTime(2014, 7, 15, 0, 0, 0, 0);
            this.timeKT.EnterMoveNextControl = true;
            this.timeKT.Location = new System.Drawing.Point(95, 99);
            this.timeKT.Name = "timeKT";
            this.timeKT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeKT.Properties.Appearance.Options.UseFont = true;
            this.timeKT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.timeKT.Properties.DisplayFormat.FormatString = "HH:mm:ss";
            this.timeKT.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeKT.Properties.EditFormat.FormatString = "HH:mm:ss";
            this.timeKT.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeKT.Properties.Mask.EditMask = "HH:mm:ss";
            this.timeKT.Size = new System.Drawing.Size(248, 26);
            this.timeKT.TabIndex = 5;
            // 
            // lbGioKetThuc
            // 
            this.lbGioKetThuc.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGioKetThuc.Location = new System.Drawing.Point(6, 102);
            this.lbGioKetThuc.Margin = new System.Windows.Forms.Padding(4);
            this.lbGioKetThuc.Name = "lbGioKetThuc";
            this.lbGioKetThuc.Size = new System.Drawing.Size(85, 19);
            this.lbGioKetThuc.TabIndex = 4;
            this.lbGioKetThuc.Text = "Giờ kết thúc";
            // 
            // lbGioBatDau
            // 
            this.lbGioBatDau.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGioBatDau.Location = new System.Drawing.Point(6, 70);
            this.lbGioBatDau.Margin = new System.Windows.Forms.Padding(4);
            this.lbGioBatDau.Name = "lbGioBatDau";
            this.lbGioBatDau.Size = new System.Drawing.Size(82, 19);
            this.lbGioBatDau.TabIndex = 2;
            this.lbGioBatDau.Text = "Giờ bắt đầu";
            // 
            // chkSUDUNG
            // 
            this.chkSUDUNG.EnterMoveNextControl = true;
            this.chkSUDUNG.Location = new System.Drawing.Point(93, 163);
            this.chkSUDUNG.Name = "chkSUDUNG";
            this.chkSUDUNG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNG.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNG.Properties.Caption = "Sử dụng";
            this.chkSUDUNG.Size = new System.Drawing.Size(92, 24);
            this.chkSUDUNG.TabIndex = 8;
            // 
            // lblGHICHU
            // 
            this.lblGHICHU.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGHICHU.Location = new System.Drawing.Point(6, 134);
            this.lblGHICHU.Margin = new System.Windows.Forms.Padding(4);
            this.lblGHICHU.Name = "lblGHICHU";
            this.lblGHICHU.Size = new System.Drawing.Size(54, 19);
            this.lblGHICHU.TabIndex = 6;
            this.lblGHICHU.Text = "Ghi chú";
            // 
            // txtGHICHU
            // 
            this.txtGHICHU.EnterMoveNextControl = true;
            this.txtGHICHU.Location = new System.Drawing.Point(95, 131);
            this.txtGHICHU.Name = "txtGHICHU";
            this.txtGHICHU.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHU.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHU.Size = new System.Drawing.Size(248, 26);
            this.txtGHICHU.TabIndex = 7;
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(6, 38);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(78, 19);
            this.lblTEN.TabIndex = 0;
            this.lblTEN.Text = "Tên ca bán";
            // 
            // txtTEN
            // 
            this.txtTEN.EnterMoveNextControl = true;
            this.txtTEN.Location = new System.Drawing.Point(95, 35);
            this.txtTEN.Name = "txtTEN";
            this.txtTEN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTEN.Properties.Appearance.Options.UseFont = true;
            this.txtTEN.Size = new System.Drawing.Size(248, 26);
            this.txtTEN.TabIndex = 1;
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = ((System.Drawing.Image)(resources.GetObject("btnThem.Image")));
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(183, 194);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 35);
            this.btnThem.TabIndex = 10;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(359, 194);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(89, 35);
            this.btnXoa.TabIndex = 12;
            this.btnXoa.Text = "&Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnSua
            // 
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Image = ((System.Drawing.Image)(resources.GetObject("btnSua.Image")));
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(271, 194);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(80, 35);
            this.btnSua.TabIndex = 11;
            this.btnSua.Text = "&Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = ((System.Drawing.Image)(resources.GetObject("btnLuu.Image")));
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(95, 194);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 9;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(981, 238);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 494);
            this.panelControl2.TabIndex = 2;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 82);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 330);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 10);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 170);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 250);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(4);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 412);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Left;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 238);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(505, 494);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MA,
            this.TEN,
            this.GIOBATDAU,
            this.GHICHU,
            this.SUDUNG,
            this.GIOKETTHU,
            this.FILL});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.TEN, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 40;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA.Caption = "Mã";
            this.MA.DisplayFormat.FormatString = "000";
            this.MA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Visible = true;
            this.MA.VisibleIndex = 1;
            this.MA.Width = 50;
            // 
            // TEN
            // 
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.Caption = "Tên";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowEdit = false;
            this.TEN.OptionsColumn.AllowFocus = false;
            this.TEN.OptionsColumn.FixedWidth = true;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 2;
            this.TEN.Width = 120;
            // 
            // GIOBATDAU
            // 
            this.GIOBATDAU.AppearanceCell.Options.UseTextOptions = true;
            this.GIOBATDAU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIOBATDAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GIOBATDAU.AppearanceHeader.Options.UseFont = true;
            this.GIOBATDAU.AppearanceHeader.Options.UseTextOptions = true;
            this.GIOBATDAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIOBATDAU.Caption = "Bắt đầu";
            this.GIOBATDAU.FieldName = "GIOBATDAU";
            this.GIOBATDAU.Name = "GIOBATDAU";
            this.GIOBATDAU.OptionsColumn.AllowEdit = false;
            this.GIOBATDAU.OptionsColumn.AllowFocus = false;
            this.GIOBATDAU.OptionsColumn.FixedWidth = true;
            this.GIOBATDAU.Visible = true;
            this.GIOBATDAU.VisibleIndex = 3;
            this.GIOBATDAU.Width = 90;
            // 
            // GHICHU
            // 
            this.GHICHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GHICHU.AppearanceHeader.Options.UseFont = true;
            this.GHICHU.AppearanceHeader.Options.UseTextOptions = true;
            this.GHICHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GHICHU.Caption = "Ghi chú";
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.Name = "GHICHU";
            this.GHICHU.OptionsColumn.AllowEdit = false;
            this.GHICHU.OptionsColumn.AllowFocus = false;
            this.GHICHU.OptionsColumn.FixedWidth = true;
            this.GHICHU.Visible = true;
            this.GHICHU.VisibleIndex = 5;
            this.GHICHU.Width = 120;
            // 
            // SUDUNG
            // 
            this.SUDUNG.AppearanceCell.Options.UseTextOptions = true;
            this.SUDUNG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SUDUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SUDUNG.AppearanceHeader.Options.UseFont = true;
            this.SUDUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.SUDUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SUDUNG.Caption = "Sử dụng";
            this.SUDUNG.FieldName = "SUDUNG";
            this.SUDUNG.Name = "SUDUNG";
            this.SUDUNG.OptionsColumn.AllowEdit = false;
            this.SUDUNG.OptionsColumn.AllowFocus = false;
            this.SUDUNG.OptionsColumn.FixedWidth = true;
            this.SUDUNG.Visible = true;
            this.SUDUNG.VisibleIndex = 6;
            this.SUDUNG.Width = 90;
            // 
            // GIOKETTHU
            // 
            this.GIOKETTHU.AppearanceCell.Options.UseTextOptions = true;
            this.GIOKETTHU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIOKETTHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GIOKETTHU.AppearanceHeader.Options.UseFont = true;
            this.GIOKETTHU.AppearanceHeader.Options.UseTextOptions = true;
            this.GIOKETTHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIOKETTHU.Caption = "Kết thúc";
            this.GIOKETTHU.FieldName = "GIOKETTHU";
            this.GIOKETTHU.Name = "GIOKETTHU";
            this.GIOKETTHU.OptionsColumn.AllowEdit = false;
            this.GIOKETTHU.OptionsColumn.AllowFocus = false;
            this.GIOKETTHU.OptionsColumn.FixedWidth = true;
            this.GIOKETTHU.Visible = true;
            this.GIOKETTHU.VisibleIndex = 4;
            this.GIOKETTHU.Width = 90;
            // 
            // FILL
            // 
            this.FILL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FILL.AppearanceHeader.Options.UseFont = true;
            this.FILL.AppearanceHeader.Options.UseTextOptions = true;
            this.FILL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FILL.MinWidth = 10;
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.OptionsColumn.AllowMove = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 7;
            this.FILL.Width = 10;
            // 
            // gridControl1
            // 
            this.gridControl1.AllowDrop = true;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.RelationName = "Level1";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gridControl1.Location = new System.Drawing.Point(505, 238);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(476, 494);
            this.gridControl1.TabIndex = 3;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT1,
            this.TEN_CUAHANG,
            this.TEN_QUAY,
            this.UserID,
            this.TENNHANVIEN,
            this.TEN_CABAN,
            this.GHICHU1,
            this.SUDUNG1,
            this.FILL1});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 2;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.ShowCloseButton = false;
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.TEN_CUAHANG, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.TEN_QUAY, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.TEN_CABAN, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // STT1
            // 
            this.STT1.AppearanceCell.Options.UseTextOptions = true;
            this.STT1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT1.AppearanceHeader.Options.UseFont = true;
            this.STT1.AppearanceHeader.Options.UseTextOptions = true;
            this.STT1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT1.Caption = "STT";
            this.STT1.FieldName = "STT";
            this.STT1.Name = "STT1";
            this.STT1.OptionsColumn.AllowEdit = false;
            this.STT1.OptionsColumn.AllowFocus = false;
            this.STT1.OptionsColumn.AllowMove = false;
            this.STT1.OptionsColumn.FixedWidth = true;
            this.STT1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT1.Visible = true;
            this.STT1.VisibleIndex = 0;
            this.STT1.Width = 50;
            // 
            // TEN_CUAHANG
            // 
            this.TEN_CUAHANG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_CUAHANG.AppearanceHeader.Options.UseFont = true;
            this.TEN_CUAHANG.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_CUAHANG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_CUAHANG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_CUAHANG.Caption = "Cửa hàng";
            this.TEN_CUAHANG.FieldName = "TEN_CUAHANG";
            this.TEN_CUAHANG.Name = "TEN_CUAHANG";
            this.TEN_CUAHANG.OptionsColumn.AllowEdit = false;
            this.TEN_CUAHANG.OptionsColumn.AllowFocus = false;
            this.TEN_CUAHANG.OptionsColumn.FixedWidth = true;
            this.TEN_CUAHANG.Width = 150;
            // 
            // TEN_QUAY
            // 
            this.TEN_QUAY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TEN_QUAY.AppearanceHeader.Options.UseFont = true;
            this.TEN_QUAY.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_QUAY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_QUAY.Caption = "Quầy";
            this.TEN_QUAY.FieldName = "TEN_QUAY";
            this.TEN_QUAY.Name = "TEN_QUAY";
            this.TEN_QUAY.OptionsColumn.AllowEdit = false;
            this.TEN_QUAY.OptionsColumn.AllowFocus = false;
            this.TEN_QUAY.OptionsColumn.FixedWidth = true;
            // 
            // UserID
            // 
            this.UserID.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.UserID.AppearanceCell.Options.UseFont = true;
            this.UserID.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.UserID.AppearanceHeader.Options.UseFont = true;
            this.UserID.AppearanceHeader.Options.UseTextOptions = true;
            this.UserID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.UserID.Caption = "Tài khoản";
            this.UserID.FieldName = "UserID";
            this.UserID.Name = "UserID";
            this.UserID.OptionsColumn.AllowEdit = false;
            this.UserID.OptionsColumn.AllowFocus = false;
            this.UserID.OptionsColumn.FixedWidth = true;
            this.UserID.Visible = true;
            this.UserID.VisibleIndex = 2;
            this.UserID.Width = 92;
            // 
            // TENNHANVIEN
            // 
            this.TENNHANVIEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENNHANVIEN.AppearanceHeader.Options.UseFont = true;
            this.TENNHANVIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TENNHANVIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENNHANVIEN.Caption = "Nhân viên";
            this.TENNHANVIEN.FieldName = "TENNHANVIEN";
            this.TENNHANVIEN.Name = "TENNHANVIEN";
            this.TENNHANVIEN.OptionsColumn.AllowEdit = false;
            this.TENNHANVIEN.OptionsColumn.AllowFocus = false;
            this.TENNHANVIEN.OptionsColumn.FixedWidth = true;
            this.TENNHANVIEN.Visible = true;
            this.TENNHANVIEN.VisibleIndex = 3;
            this.TENNHANVIEN.Width = 150;
            // 
            // TEN_CABAN
            // 
            this.TEN_CABAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_CABAN.AppearanceHeader.Options.UseFont = true;
            this.TEN_CABAN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_CABAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_CABAN.Caption = "Ca bán";
            this.TEN_CABAN.FieldName = "TEN_CABAN";
            this.TEN_CABAN.Name = "TEN_CABAN";
            this.TEN_CABAN.OptionsColumn.AllowEdit = false;
            this.TEN_CABAN.OptionsColumn.AllowFocus = false;
            this.TEN_CABAN.OptionsColumn.FixedWidth = true;
            this.TEN_CABAN.Visible = true;
            this.TEN_CABAN.VisibleIndex = 1;
            this.TEN_CABAN.Width = 124;
            // 
            // GHICHU1
            // 
            this.GHICHU1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GHICHU1.AppearanceHeader.Options.UseFont = true;
            this.GHICHU1.AppearanceHeader.Options.UseTextOptions = true;
            this.GHICHU1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GHICHU1.Caption = "Ghi chú";
            this.GHICHU1.FieldName = "GHICHU";
            this.GHICHU1.Name = "GHICHU1";
            this.GHICHU1.OptionsColumn.AllowEdit = false;
            this.GHICHU1.OptionsColumn.AllowFocus = false;
            this.GHICHU1.OptionsColumn.FixedWidth = true;
            this.GHICHU1.Visible = true;
            this.GHICHU1.VisibleIndex = 4;
            this.GHICHU1.Width = 100;
            // 
            // SUDUNG1
            // 
            this.SUDUNG1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SUDUNG1.AppearanceHeader.Options.UseFont = true;
            this.SUDUNG1.AppearanceHeader.Options.UseTextOptions = true;
            this.SUDUNG1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SUDUNG1.Caption = "Sử dụng";
            this.SUDUNG1.FieldName = "SUDUNG";
            this.SUDUNG1.Name = "SUDUNG1";
            this.SUDUNG1.OptionsColumn.AllowEdit = false;
            this.SUDUNG1.OptionsColumn.AllowFocus = false;
            this.SUDUNG1.OptionsColumn.FixedWidth = true;
            this.SUDUNG1.Visible = true;
            this.SUDUNG1.VisibleIndex = 5;
            this.SUDUNG1.Width = 90;
            // 
            // FILL1
            // 
            this.FILL1.MinWidth = 10;
            this.FILL1.Name = "FILL1";
            this.FILL1.OptionsColumn.AllowEdit = false;
            this.FILL1.OptionsColumn.AllowFocus = false;
            this.FILL1.OptionsColumn.AllowMove = false;
            this.FILL1.Visible = true;
            this.FILL1.VisibleIndex = 6;
            this.FILL1.Width = 10;
            // 
            // Frm_CabanThietlapcaban
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 732);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_CabanThietlapcaban";
            this.Text = "Ca bán -Thiết lập ca bán";
            this.Load += new System.EventHandler(this.Frm_CabanThietlapcaban_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_CabanThietlapcaban_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboCuahang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHU1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboQuay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhanvien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timeBD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeKT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHU.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;
        private DevExpress.XtraGrid.Columns.GridColumn SUDUNG;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraGrid.Columns.GridColumn GIOBATDAU;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT1;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_CUAHANG;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_QUAY;
        private DevExpress.XtraGrid.Columns.GridColumn TENNHANVIEN;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_CABAN;
        private DevExpress.XtraGrid.Columns.GridColumn FILL1;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton btnLuu1;
        private DevExpress.XtraEditors.SimpleButton btnSua1;
        private DevExpress.XtraEditors.SimpleButton btnXoa1;
        private DevExpress.XtraEditors.SimpleButton btnThem1;
        private DevExpress.XtraGrid.Columns.GridColumn GIOKETTHU;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.TimeEdit timeBD;
        private DevExpress.XtraEditors.TimeEdit timeKT;
        private DevExpress.XtraEditors.LabelControl lbGioKetThuc;
        private DevExpress.XtraEditors.LabelControl lbGioBatDau;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNG;
        private DevExpress.XtraEditors.LabelControl lblGHICHU;
        private DevExpress.XtraEditors.TextEdit txtGHICHU;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.TextEdit txtTEN;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNG1;
        private DevExpress.XtraEditors.LabelControl lbGhiChu;
        private DevExpress.XtraEditors.TextEdit txtGHICHU1;
        private DevExpress.XtraEditors.LabelControl lbNhanVien;
        private DevExpress.XtraEditors.LookUpEdit cboQuay;
        private DevExpress.XtraEditors.LookUpEdit cboNhanvien;
        private DevExpress.XtraEditors.LookUpEdit cboCa;
        private DevExpress.XtraEditors.LabelControl lbQuay;
        private DevExpress.XtraEditors.LabelControl lbCaBan;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU1;
        private DevExpress.XtraGrid.Columns.GridColumn SUDUNG1;
        private DevExpress.XtraEditors.LookUpEdit cboCuahang;
        private DevExpress.XtraEditors.LabelControl lbCuaHang;
        private DevExpress.XtraGrid.Columns.GridColumn UserID;




    }
}