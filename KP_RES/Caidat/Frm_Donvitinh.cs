﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using System.Diagnostics;

using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Data.SqlClient;

namespace KP_RES 
{
    public partial class Frm_Donvitinh : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Donvitinh()
        {
            InitializeComponent();
            this.Text = rm.GetString("dvt", culture);
            LoaddataGridView();
            LoadPermission();
        }

        private void Frm_Donvitinh_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
            txtTEN.SelectAll();
        }

        string sma = "";

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtTEN.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show(_xacnhanbanmuonxoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From DONVITINH" + "\n";
            sSQL += "Where MA_DONVITINH=" + clsMain.SQLString(sma) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl(false);
            txtTEN.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            string sSQL = "";
            if (sma == "" && btnThem.Enabled)
            {
                sSQL += "Insert into DONVITINH (TEN_DONVITINH,GHICHU,SUDUNG)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLStringUnicode(txtTEN.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtGHICHU.Text) + ",";
                sSQL += clsMain.SQLBit(chkSUDUNG.Checked) + ")";
            }
            else if ((sma != "" && btnSua.Enabled))
            {
                sSQL += "Update DONVITINH Set " + "\n";
                sSQL += "TEN_DONVITINH=" + clsMain.SQLStringUnicode(txtTEN.Text) + "," + "\n";
                sSQL += "GHICHU=" + clsMain.SQLStringUnicode(txtGHICHU.Text) + "," + "\n";
                sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNG.Checked) + "\n";
                sSQL += "Where MA_DONVITINH=" + clsMain.SQLString(sma) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sma = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtTEN.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN").ToString();
            txtGHICHU.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GHICHU").ToString();
            chkSUDUNG.Checked = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SUDUNG").ToString());
            KhoaMoControl(true);
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            string sSQL = "";
            sSQL += "Select MA_DONVITINH As MA,TEN_DONVITINH As TEN,GHICHU,SUDUNG" + "\n";
            sSQL += "From DONVITINH" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            gridControl2.DataSource = dt;
            gridView2.ActiveFilterString = "[SUDUNG] = True";
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("7");
            btnSua.Enabled = clsUserManagement.AllowEdit("7");
            btnXoa.Enabled = clsUserManagement.AllowDelete("7");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTEN.Properties.ReadOnly = true;
                txtGHICHU.Properties.ReadOnly = true;
                chkSUDUNG.Properties.ReadOnly = true;
            }
            else//mở
            {
                txtTEN.Properties.ReadOnly = false;
                txtGHICHU.Properties.ReadOnly = false;
                chkSUDUNG.Properties.ReadOnly = false;
            }
        }

        private void SetRongControl()
        {
            sma = "";
            txtTEN.Text = "";
            txtGHICHU.Text = "";
            chkSUDUNG.Checked = true;
        }

        private Boolean CheckInput()
        {
            if (txtTEN.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_nhap + lblTEN.Text, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTEN.Focus();
                return false;
            }
            return true;
        }

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                txt.SelectAll();
            }
            catch
            {
            }
        }

        string _nhap = "";
        string _thongbao = "";
        string _luukothanhcong = "";
        string _xacnhanbanmuonxoa = "";
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void Frm_Donvitinh_Load(object sender, EventArgs e)
        {
            foreach (Control ctl in panelControl1.Controls)
            {
                ctl.Click += new EventHandler(ctl_Click);
            }
            this.Text = rm.GetString("dvt", culture);
            lblTEN.Text = rm.GetString("ten", culture);
            lblGHICHU.Text = rm.GetString("ghichu", culture);
            chkSUDUNG.Text = rm.GetString("sudung", culture);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _nhap = rm.GetString("nhap", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);
            _xacnhanbanmuonxoa = rm.GetString("xacnhanbanmuonxoa", culture);

            gridView2.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView2.Columns["MA"].Caption = rm.GetString("ma", culture);
            gridView2.Columns["TEN"].Caption = rm.GetString("ten", culture);
            gridView2.Columns["GHICHU"].Caption = rm.GetString("ghichu", culture);
            gridView2.Columns["SUDUNG"].Caption = rm.GetString("sudung", culture);
        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog sOpenFile = new OpenFileDialog();
            sOpenFile.Filter = Filter();
            sOpenFile.FilterIndex = 1;
            sOpenFile.RestoreDirectory = true;
            sOpenFile.Multiselect = false;
            sOpenFile.Title = "Chọn file";
            if (sOpenFile.ShowDialog() == DialogResult.OK)
            {
                Excel.Application xlApp = new Excel.Application();
                Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(sOpenFile.FileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                Excel._Worksheet xlWorksheet = (Excel.Worksheet)xlWorkbook.Worksheets.get_Item(1);
                Excel.Range xlRange = xlWorksheet.UsedRange;

                for (int i = 2; i <= xlRange.Rows.Count; i++)
                {
                    try
                    {
                        String sMa = (String)(xlRange.Cells[i, 1] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 1] as Excel.Range).Value2 : "";
                        String sTen = (String)(xlRange.Cells[i, 2] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 2] as Excel.Range).Value2.ToString() : "";
                        String sGhiChu = (String)(xlRange.Cells[i, 3] as Excel.Range).Value2 != null ? (String)(xlRange.Cells[i, 3] as Excel.Range).Value2.ToString() : "";
                        String sSuDung = "1";

                        if (sMa != "" && clsMain.ReturnDataTable("SELECT MA_DONVITINH FROM DONVITINH WHERE MA_DONVITINH = " + clsMain.SQLString(sMa)).Rows.Count > 0)
                        {
                            UpdateFile(sMa, sTen, sGhiChu, sSuDung);
                        }
                        else if (sMa != "" && clsMain.ReturnDataTable("SELECT MA_DONVITINH FROM DONVITINH WHERE MA_DONVITINH = " + clsMain.SQLString(sMa)).Rows.Count == 0)
                        {
                            SaveFile(sMa, sTen, sGhiChu, sSuDung);
                        }
                    }
                    catch (Exception ex)
                    {
                        this.Cursor = Cursors.Default;
                    }
                }

                xlWorkbook.Close(true, null, null);
                xlApp.Quit();

                Marshal.ReleaseComObject(xlWorksheet);
                Marshal.ReleaseComObject(xlWorkbook);
                Marshal.ReleaseComObject(xlApp);

                LoaddataGridView();

                XtraMessageBox.Show("Lưu thành công", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Cursor = Cursors.Default;
            }
        }

        private void SaveFile(String sMa, String sTen, String sGhiChu, String sSuDung)
        {
            try
            {
                SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
                SqlCommand SqlComMain = null;
                String sSQL = "";
                sSQL += "SET IDENTITY_INSERT DONVITINH ON " + "\n";
                sSQL += "Insert into DONVITINH(MA_DONVITINH,TEN_DONVITINH,GHICHU,SUDUNG) Values(@MA_DONVITINH,@TEN_DONVITINH,@GHICHU,@SUDUNG)" + "\n";
                sSQL += "SET IDENTITY_INSERT DONVITINH OFF " + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);
                SqlCom.Parameters.Add(new SqlParameter("@MA_DONVITINH", (object)sMa));
                SqlCom.Parameters.Add(new SqlParameter("@TEN_DONVITINH", (object)sTen));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)sGhiChu));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)sSuDung));
                SqlComMain = SqlCom;

                Boolean bRunSQL = true;
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    CN.Open();
                    SqlComMain.ExecuteNonQuery();
                    CN.Close();
                    this.Cursor = Cursors.Default;
                    bRunSQL = true;
                }
                catch
                {
                    bRunSQL = false;
                }
            }
            catch (Exception e)
            {

            }
        }

        private void UpdateFile(String sMa, String sTen, String sGhiChu, String sSuDung)
        {
            try
            {
                SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
                SqlCommand SqlComMain = null;
                String sSQL = "";
                sSQL += "UPDATE DONVITINH SET TEN_DONVITINH = @TEN_DONVITINH, GHICHU = @GHICHU,SUDUNG = @SUDUNG " + "\n";
                sSQL += "WHERE MA_DONVITINH = " + clsMain.SQLString(sMa) + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);
                SqlCom.Parameters.Add(new SqlParameter("@TEN_DONVITINH", (object)sTen));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)sGhiChu));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)sSuDung));
                SqlComMain = SqlCom;

                Boolean bRunSQL = true;
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    CN.Open();
                    SqlComMain.ExecuteNonQuery();
                    CN.Close();
                    this.Cursor = Cursors.Default;
                    bRunSQL = true;
                }
                catch
                {
                    bRunSQL = false;
                }
            }
            catch (Exception e)
            {

            }
        }

        public static String Filter()
        {
            String Filter = "";
            Filter += "Excel (*.xlsx)|*.xlsx|";
            Filter += "Excel 97-2003 (*.xls)|*.xls|";
            Filter += "Word Document (*.docx)|*.docx|";
            Filter += "Word 97-2003 Document (*.doc)|*.doc|";
            Filter += "PDF (*.pdf)|*.pdf|";
            Filter += "Plain Text (*.txt)|*.txt|";
            Filter += "XML Document (*.xml)|*.xml|";
            Filter += "Web Page (*.htm, *.html)|*.htm;*.html|";
            Filter += "PNG (*.png)|*.png|";
            Filter += "JPEG (*.jpeg;*.jpg;*.jpe)|*.jpeg;*.jpg;*.jpe|";
            Filter += "GIF (*.gif)|*.gif|";
            Filter += "All File|*.*";

            return Filter;
        }

        private void btnFile_Mo_Click(object sender, EventArgs e)
        {
            String filename = Application.StartupPath + "\\FileDonViTinh.xlsx";
            FileInfo fi = new FileInfo(filename);
            if (fi.Exists)
                Process.Start(filename);

        }
    }
}