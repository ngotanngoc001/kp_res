﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_TienteTygia : DevExpress.XtraEditors.XtraForm
    {
        public Frm_TienteTygia()
        {
            InitializeComponent();
            this.Text = rm.GetString("tientetygia", culture);
            LoaddataGridView();
            LoaddataGridView1();
            LoadPermission();
        }

        private void Frm_TienteTygia_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
            gridControl2.Width = (this.Width) / 2;
            panelControl5.Width = ((this.Width) / 2)-2;
        }

        string smaTiente = "";
        string smaTygia = "";
        int igrid = 1;

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                if (igrid == 1)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
                else
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                if (igrid == 1)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                    {
                        btnXuongit.Enabled = false;
                        btnXuongnhieu.Enabled = false;
                    }
                }
                else
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                    {
                        btnXuongit.Enabled = false;
                        btnXuongnhieu.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                if (igrid == 1)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                    if (gridView1.FocusedRowHandle == 0)
                    {
                        btnLenit.Enabled = false;
                        btnLennhieu.Enabled = false;
                    }
                }
                else
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                    if (gridView2.FocusedRowHandle == 0)
                    {
                        btnLenit.Enabled = false;
                        btnLennhieu.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                if (igrid == 1)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }
                else
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }
            }
            catch
            {
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtMA.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (smaTiente == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show(_xacnhanbanmuonxoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From TIENTE" + "\n";
            sSQL += "Where MA_TIENTE=" + clsMain.SQLString(smaTiente) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (smaTiente == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl(false);
            txtMA.Properties.ReadOnly = true;
            txtTEN.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            string sSQL = "";
            if (smaTiente == "" && btnThem.Enabled)
            {
                sSQL += "Insert into TIENTE (MA_TIENTE,TEN_TIENTE,GHICHU,STT,SUDUNG)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLStringUnicode(txtMA.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtTEN.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtGHICHU.Text) + ",";
                sSQL += clsMain.SQLString(txtSTT.Text) + ",";
                sSQL += clsMain.SQLBit(chkSUDUNG.Checked) + ")";
            }
            else if ((smaTiente != "" && btnSua.Enabled))
            {
                sSQL += "Update TIENTE Set " + "\n";
                sSQL += "TEN_TIENTE=" + clsMain.SQLStringUnicode(txtTEN.Text) + "," + "\n";
                sSQL += "GHICHU=" + clsMain.SQLStringUnicode(txtGHICHU.Text) + "," + "\n";
                sSQL += "STT=" + clsMain.SQLString(txtSTT.Text) + "," + "\n";
                sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNG.Checked) + "\n";
                sSQL += "Where MA_TIENTE=" + clsMain.SQLString(smaTiente) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
            LoaddataGridView1();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            igrid = 2;
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            smaTiente = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtMA.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtTEN.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN").ToString();
            txtSTT.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "THUTU").ToString();
            txtGHICHU.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GHICHU").ToString();
            chkSUDUNG.Checked = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SUDUNG").ToString());
            KhoaMoControl(true);

            gridView1.ActiveFilterString = "[TEN_TIENTE] = " +clsMain .SQLString ( gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN").ToString());
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            string sSQL = "";
            sSQL += "Select MA_TIENTE As MA,TEN_TIENTE As TEN,GHICHU,SUDUNG,TYGIA,STT As THUTU" + "\n";
            sSQL += "From TIENTE" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            gridControl2.DataSource = dt;
            //gridView2.ActiveFilterString = "[SUDUNG] = True";

            LoadCombo();
        }

        private void LoaddataGridView1()
        {
            SetRongControl1();
            KhoaMoControl1(true);
            gridView2.Focus();

            string sSQL = "";
            sSQL += "Select A.MA,A.NGAY,A.MA_TIENTE,A.TYGIA,A.NGUOITAO,B.TEN_TIENTE" + "\n";
            sSQL += "From TYGIA A,TIENTE B " + "\n";
            sSQL += "where A.MA_TIENTE=B.MA_TIENTE" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            gridControl1.DataSource = dt;
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("18");
            btnSua.Enabled = clsUserManagement.AllowEdit("18");
            btnXoa.Enabled = clsUserManagement.AllowDelete("18");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//khóa
            {
                txtMA.Properties.ReadOnly = true;
                txtTEN.Properties.ReadOnly = true;
                txtSTT.Properties.ReadOnly = true;
                txtGHICHU.Properties.ReadOnly = true;
                chkSUDUNG.Properties.ReadOnly = true;
            }
            else//mở
            {
                txtMA.Properties.ReadOnly = false;
                txtTEN.Properties.ReadOnly = false;
                txtSTT.Properties.ReadOnly = false;
                txtGHICHU.Properties.ReadOnly = false;
                chkSUDUNG.Properties.ReadOnly = false;
            }
        }

        private void SetRongControl()
        {
            smaTiente = "";
            txtMA.Text = "";
            txtTEN.Text = "";
            txtSTT.Text = "";
            txtGHICHU.Text = "";
            chkSUDUNG.Checked = true;
        }

        private Boolean CheckInput()
        {
            if (txtMA.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_nhap + lblMA.Text, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMA.Focus();
                return false;
            }
            if (txtTEN.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_nhap + lblTEN.Text, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTEN.Focus();
                return false;
            }
            return true;
        }

        private void btnThem1_Click(object sender, EventArgs e)
        {
            SetRongControl1();
            KhoaMoControl1(false);
            dtpNgay.Focus();
        }

        private void btnSua1_Click(object sender, EventArgs e)
        {
            if (smaTygia == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl1(false);
            dtpNgay.Focus();
        }

        private void btnXoa1_Click(object sender, EventArgs e)
        {
            if (smaTygia == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show(_xacnhanbanmuonxoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From TYGIA" + "\n";
            sSQL += "Where MA=" + clsMain.SQLString(smaTygia) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView1();
                LoaddataGridView();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLuu1_Click(object sender, EventArgs e)
        {
            if (!CheckInput1())
            {
                return;
            }
            string sSQL = "";
            if (smaTygia == "" && btnThem1.Enabled)
            {
                sSQL += "Insert into TYGIA (NGAY,MA_TIENTE,TYGIA,NGUOITAO)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLString(dtpNgay.Text) + ",";
                sSQL += clsMain.SQLString(cboTIENTE.EditValue.ToString()) + ",";
                sSQL += clsMain.SQLString(txtTIGIA.Text.Replace(",", "")) + ",";
                sSQL += clsMain.SQLString(clsGlobal.gsUserID) + ")";

                sSQL += "Update TIENTE Set " + "\n";
                sSQL += "TYGIA=" + clsMain.SQLString(txtTIGIA.Text.Replace(",", "")) + "\n";
                sSQL += "Where MA_TIENTE=" + clsMain.SQLString(cboTIENTE.EditValue.ToString()) + "\n";
            }
            else if ((smaTygia != "" && btnSua1.Enabled))
            {
                sSQL += "Update TYGIA Set " + "\n";
                sSQL += "NGAY=" + clsMain.SQLString(dtpNgay.Text) + "," + "\n";
                sSQL += "MA_TIENTE=" + clsMain.SQLString(cboTIENTE.EditValue.ToString()) + "," + "\n";
                sSQL += "TYGIA=" + clsMain.SQLString(txtTIGIA.Text.Replace(",", "")) + "," + "\n";
                sSQL += "NGUOITAO=" + clsMain.SQLStringUnicode(clsGlobal.gsUserID) + "\n";
                sSQL += "Where MA=" + clsMain.SQLString(smaTygia) + "\n";

                sSQL += "Update TIENTE Set " + "\n";
                sSQL += "TYGIA=" + clsMain.SQLString(txtTIGIA.Text.Replace(",", "")) + "\n";
                sSQL += "Where MA_TIENTE=" + clsMain.SQLString(cboTIENTE.EditValue.ToString()) + "\n";
            }



            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView1();
                LoaddataGridView();
                btnThem1.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT1 & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            igrid = 1;
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;

            smaTygia = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MA").ToString();
            dtpNgay.EditValue = DateTime.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "NGAY").ToString());
            cboTIENTE.EditValue = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MA_TIENTE").ToString();
            txtTIGIA.EditValue = int.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TYGIA").ToString());
            KhoaMoControl1(true);
        }

        private void KhoaMoControl1(bool tf)
        {
            if (tf == true)//khóa
            {
                cboTIENTE.Properties.ReadOnly = true;
                txtTIGIA.Properties.ReadOnly = true;
                dtpNgay.Properties.ReadOnly = true;
            }
            else//mở
            {
                cboTIENTE.Properties.ReadOnly = false;
                txtTIGIA.Properties.ReadOnly = false;
                dtpNgay.Properties.ReadOnly = false;
            }
        }

        private void SetRongControl1()
        {
            smaTygia = "";
            dtpNgay.EditValue = DateTime.Today;
            cboTIENTE.EditValue = cboTIENTE.Properties.GetDataSourceValue(cboTIENTE.Properties.ValueMember, 0);
            txtTIGIA.EditValue = "";
        }

        private void LoadCombo()
        {
            string sSQL = "";
            sSQL += "Select MA_TIENTE,TEN_TIENTE" + "\n";
            sSQL += "From TIENTE" + "\n";
            sSQL += "Where SUDUNG=1 ";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            cboTIENTE.Properties.DataSource = dt;
            cboTIENTE.EditValue = cboTIENTE.Properties.GetDataSourceValue(cboTIENTE.Properties.ValueMember, 0);
        }

        private Boolean CheckInput1()
        {
            if (txtTIGIA.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_nhap + lblTYGIA.Text, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTIGIA.Focus();
                return false;
            }
            return true;
        }

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                txt.SelectAll();
            }
            catch
            {
            }
        }

        string _nhap = "";
        string _thongbao = "";
        string _luukothanhcong = "";
        string _xacnhanbanmuonxoa = "";
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void Frm_TienteTygia_Load(object sender, EventArgs e)
        {
            foreach (Control ctl in panelControl5.Controls)
            {
                ctl.Click += new EventHandler(ctl_Click);
            }
            foreach (Control ctl in panelControl6.Controls)
            {
                ctl.Click += new EventHandler(ctl_Click);
            }

           
            this.Text = rm.GetString("tientetygia", culture);
            lblMA.Text = rm.GetString("matiente", culture);
            lblTEN.Text = rm.GetString("tentiente", culture);
            lbSTT.Text = rm.GetString("stt", culture);
            lbGhiChu.Text = rm.GetString("ghichu", culture);
            lbNgay.Text = rm.GetString("ngay", culture);
            lbTienTe.Text = rm.GetString("tiente", culture);
            lblTYGIA.Text = rm.GetString("tygia", culture);
            chkSUDUNG.Text = rm.GetString("sudung", culture);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
            btnLuu1.Text = rm.GetString("luu", culture);
            btnThem1.Text = rm.GetString("them", culture);
            btnSua1.Text = rm.GetString("sua", culture);
            btnXoa1.Text = rm.GetString("xoa", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _nhap = rm.GetString("nhap", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);
            _xacnhanbanmuonxoa = rm.GetString("xacnhanbanmuonxoa", culture);

            gridView2.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView2.Columns["MA"].Caption = rm.GetString("ma", culture);
            gridView2.Columns["TEN"].Caption = rm.GetString("ten", culture);
            gridView2.Columns["TYGIA"].Caption = rm.GetString("tygia", culture);
            gridView2.Columns["GHICHU"].Caption = rm.GetString("ghichu", culture);
            gridView2.Columns["THUTU"].Caption = rm.GetString("thutu", culture);
            gridView2.Columns["SUDUNG"].Caption = rm.GetString("sudung", culture);
            gridView2.GroupPanelText = rm.GetString("ktcvddntimkiem", culture);

            gridView1.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView1.Columns["NGAY"].Caption = rm.GetString("ngay", culture);
            gridView1.Columns["TEN_TIENTE"].Caption = rm.GetString("tiente", culture);
            gridView1.Columns["TYGIA"].Caption = rm.GetString("tygia", culture);
            gridView1.Columns["NGUOITAO"].Caption = rm.GetString("nguoitao", culture);
            gridView1.GroupPanelText = rm.GetString("ktcvddntimkiem", culture);
        }
    }
}