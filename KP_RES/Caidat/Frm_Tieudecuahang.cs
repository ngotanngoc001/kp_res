﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Tieudecuahang : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Tieudecuahang()
        {
            InitializeComponent();
           
        }
        public string sMacuahang { get; set; }

        private void LoadOption()
        {
            string sSQL = "Select HEADER,FOOTER From CUAHANG where MA_CUAHANG="+clsMain .SQLString (sMacuahang );
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            foreach (DataRow dr in dt.Rows)
            {
                txtHeader.Text = dr["HEADER"].ToString();
                txtFooter.Text = dr["FOOTER"].ToString();
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            string sSQL = "";
            sSQL += "Update CUAHANG Set " + "\n";
            sSQL += "HEADER=" + clsMain.SQLStringUnicode(txtHeader.Text) + "," + "\n";
            sSQL += "FOOTER=" + clsMain.SQLStringUnicode(txtFooter.Text) + "\n";
            sSQL += "Where MA_CUAHANG=" + clsMain.SQLString(sMacuahang) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luuthanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong,_thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        string _thongbao = "";
        string _luukothanhcong = "";
        string _luuthanhcong = "";
        private void Frm_Tieudecuahang_Load(object sender, EventArgs e)
        {

            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            //this.Text = rm.GetString("tieudebillve", culture);
            btnLuu.Text = rm.GetString("dongy", culture);
            btnBanphim.Text = rm.GetString("banphim", culture);
            btnDong.Text = rm.GetString("dong", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _luuthanhcong = rm.GetString("luuthanhcong", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);

            LoadOption();
        }
    }
}