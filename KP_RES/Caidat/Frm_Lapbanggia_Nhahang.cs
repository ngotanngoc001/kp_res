﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES
{
    public partial class Frm_Lapbanggia_Nhahang : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Lapbanggia_Nhahang()
        {
            InitializeComponent();
            this.Text = rm.GetString("lapbanggia", culture);
            LoaddataGridView();
            LoadPermission();
        }

        string sma = "";
        string sMaHangHoa = "";
        string sTenHangHoa = "";
        private void Frm_Lapbanggia_Nhahang_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtTEN.Focus();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để sữa
                return;

            KhoaMoControl(false);
            txtTEN.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để xóa
                return;

            if (!CheckDelete())
                return;

            if (XtraMessageBox.Show(_xacnhanbanmuonxoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;

            String sSQL = "";
            sSQL += "DELETE FROM THIETLAPBANGGIA" + "\n";
            sSQL += "WHERE MA_BANGGIA = " + clsMain.SQLString(sma) + "\n";

            sSQL += "DELETE FROM BANGGIA" + "\n";
            sSQL += "WHERE MABANGGIA = " + clsMain.SQLString(sma) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
                LoaddataGridView1();
            }
            else
                XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            string sSQL = "";
            if (sma == "" && btnThem.Enabled)
            {
                sSQL += "INSERT INTO BANGGIA(TENBANGGIA, GHICHU, SUDUNG, MODE)" + "\n";
                sSQL += "VALUES( ";
                sSQL += clsMain.SQLStringUnicode(txtTEN.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtGHICHU.Text) + ",";
                sSQL += clsMain.SQLBit(chkSUDUNG.Checked) + ",";
                sSQL += "0" + ")" + "\n";

                string mabanggia = clsMain.ReturnDataTable("Select IDENT_CURRENT('BANGGIA')+1 As MABANGGIA").Rows[0][0].ToString();
                string thoigian1 = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='THOIGIAN1'").Rows[0][0].ToString();
                string thoigian2 = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='THOIGIAN2'").Rows[0][0].ToString();
                string thoigian3 = clsMain.ReturnDataTable("Select GIATRI From CAUHINH where TEN='THOIGIAN3'").Rows[0][0].ToString();

                sSQL += "\n";
                sSQL += "DELETE FROM THIETLAPBANGGIA" + "\n";
                sSQL += "WHERE MA_BANGGIA = " + clsMain.SQLString(mabanggia) + "\n";

                foreach (DataRow dr in clsMain.ReturnDataTable("Select MA_HANGHOA,GIABAN1 From HANGHOA Where MA_NHOMHANG IN (Select MA_NHOMHANG From NHOMHANG Where THUCDON=1)").Rows)
                {
                    sSQL += "\n";
                    sSQL += "INSERT INTO THIETLAPBANGGIA(MA_BANGGIA,MA_HANGHOA,THOIGIAN1,GIABAN1,THOIGIAN2,GIABAN2,THOIGIAN3,GIABAN3)" + "\n";
                    sSQL += "VALUES( ";
                    sSQL += clsMain.SQLString(mabanggia) + ",";
                    sSQL += clsMain.SQLString(dr["MA_HANGHOA"].ToString()) + ",";
                    sSQL += clsMain.SQLString(thoigian1) + ",";
                    sSQL += clsMain.SQLString(dr["GIABAN1"].ToString()) + ",";
                    sSQL += clsMain.SQLString(thoigian2) + ",";
                    sSQL += clsMain.SQLString(dr["GIABAN1"].ToString()) + ",";
                    sSQL += clsMain.SQLString(thoigian3) + ",";
                    sSQL += clsMain.SQLString(dr["GIABAN1"].ToString()) + ")" + "\n";
                }
            }
            else if ((sma != "" && btnSua.Enabled))
            {
                sSQL += "UPDATE BANGGIA SET " + "\n";
                sSQL += "TENBANGGIA = " + clsMain.SQLStringUnicode(txtTEN.Text) + "," + "\n";
                sSQL += "GHICHU = " + clsMain.SQLStringUnicode(txtGHICHU.Text) + "," + "\n";
                sSQL += "SUDUNG = " + clsMain.SQLBit(chkSUDUNG.Checked) + "," + "\n";
                sSQL += "MODE = 0 " + "\n";
                sSQL += "WHERE MABANGGIA = " + clsMain.SQLString(sma) + "\n";

                sSQL += "\n";
                sSQL += "DELETE FROM THIETLAPBANGGIA" + "\n";
                sSQL += "WHERE MA_BANGGIA = " + clsMain.SQLString(sma) + "\n";

                DataView dvTemp = (DataView)gridView2.DataSource;
                foreach (DataRow dr in dvTemp.ToTable().Rows)
                {
                    sSQL += "\n";
                    sSQL += "INSERT INTO THIETLAPBANGGIA(MA_BANGGIA,MA_HANGHOA,THOIGIAN1,GIABAN1,THOIGIAN2,GIABAN2,THOIGIAN3,GIABAN3)" + "\n";
                    sSQL += "VALUES( ";
                    sSQL += clsMain.SQLString(sma) + ",";
                    sSQL += clsMain.SQLString(dr["MA_HANGHOA"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dr["THOIGIAN1"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dr["GIABAN1"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dr["THOIGIAN2"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dr["GIABAN2"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dr["THOIGIAN3"].ToString()) + ",";
                    sSQL += clsMain.SQLString(dr["GIABAN3"].ToString()) + ")" + "\n";
                }
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
                XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        bool bSaochep = false;
        string smacu = "";
        private void btnSaochep_Click(object sender, EventArgs e)
        {
            if (sma == "")
                XtraMessageBox.Show(_chonbanggianguondesaochep, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
            {
                XtraMessageBox.Show(_chonbanggiadichdesaochep, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                bSaochep = true;
                smacu = sma;
            }
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTEN.Properties.ReadOnly = true;
                txtGHICHU.Properties.ReadOnly = true;
                chkSUDUNG.Properties.ReadOnly = true;
                gridView2.OptionsBehavior.ReadOnly = true;
            }
            else//mở
            {
                txtTEN.Properties.ReadOnly = false;
                txtGHICHU.Properties.ReadOnly = false;
                chkSUDUNG.Properties.ReadOnly = false;
                gridView2.OptionsBehavior.ReadOnly = false;
            }
        }

        private void SetRongControl()
        {
            sma = "";
            txtTEN.Text = "";
            txtGHICHU.Text = "";
            chkSUDUNG.Checked = true;
        }

        private Boolean CheckInput()
        {
            if (txtTEN.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_nhap + lblTEN.Text, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTEN.Focus();
                return false;
            }
            return true;
        }

        private Boolean CheckDelete()
        {
            if (clsMain.ReturnDataTable("Select MA_KHUVUC From APDUNGBANGGIA Where THUHAI=" + clsMain.SQLString(sma) + " Or THUBA=" + clsMain.SQLString(sma) + " Or THUTU=" + clsMain.SQLString(sma) + " Or THUNAM=" + clsMain.SQLString(sma) + " Or THUSAU=" + clsMain.SQLString(sma) + " Or THUBAY=" + clsMain.SQLString(sma) + " Or CHUNHAT=" + clsMain.SQLString(sma)).Rows.Count > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_banggiadasudung, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }

        DataTable dtBanggia = new DataTable();
        private void LoaddataGridView()
        {
            try
            {
                SetRongControl();
                KhoaMoControl(true);

                String sSQL = "";
                sSQL += "SELECT MABANGGIA AS MA,TENBANGGIA AS TEN, GHICHU, SUDUNG" + "\n";
                sSQL += "FROM BANGGIA" + "\n";
                sSQL += "WHERE MABANGGIA IN (SELECT MA_BANGGIA FROM THIETLAPBANGGIA)" + "\n";
                sSQL += "AND MODE = 0" + "\n";
                dtBanggia = clsMain.ReturnDataTable(sSQL);

                fpnlBanggia.Controls.Clear();
                foreach (DataRow dr in dtBanggia.Rows)
                {
                    SimpleButton btn = new SimpleButton();
                    btn.Width = 110;
                    btn.Height = 80;
                    btn.Font = fpnlBanggia.Font;
                    btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    btn.ImageLocation = ImageLocation.TopCenter;
                    btn.Image = global::KP_RES.Properties.Resources.display_26;
                    btn.Name = "H" + dr["MA"].ToString();
                    btn.Text = dr["TEN"].ToString();
                    if (btn.Text.Length > 20)
                    {
                        btn.Width = 110 * 2;
                    }
                    btn.Click += new EventHandler(HangHoa_Click);
                    fpnlBanggia.Controls.Add(btn);
                }
            }
            catch
            {
            }
        }

        private void HangHoa_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in fpnlBanggia.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }
            sma = btn.Name.Substring(1, btn.Name.Length - 1);
            txtTEN.Text = dtBanggia.Select("MA=" + clsMain.SQLString(sma))[0]["TEN"].ToString();
            txtGHICHU.Text = dtBanggia.Select("MA=" + clsMain.SQLString(sma))[0]["GHICHU"].ToString();
            chkSUDUNG.Checked = bool.Parse(dtBanggia.Select("MA=" + clsMain.SQLString(sma))[0]["SUDUNG"].ToString());
            if (bSaochep)
            {
                Saochepbanggia(smacu, sma);
            }
            LoaddataGridView1();
            KhoaMoControl(true);
        }

        private void LoaddataGridView1()
        {
            try
            {
                string sSQL = "";
                sSQL += "Select  A.MA_HANGHOA,A.THOIGIAN1,A.GIABAN1,A.THOIGIAN2,A.GIABAN2,A.THOIGIAN3,A.GIABAN3,B.TEN_HANGHOA,C.TEN_DONVITINH" + "\n";
                sSQL += "From THIETLAPBANGGIA A, HANGHOA B , DONVITINH C" + "\n";
                sSQL += "Where A.MA_HANGHOA= B.MA_HANGHOA" + "\n";
                sSQL += "And B.MA_DONVITINH= C.MA_DONVITINH" + "\n";
                sSQL += "And MA_BANGGIA=" + clsMain.SQLString(sma) + "\n";
                DataTable dt = clsMain.ReturnDataTable(sSQL);

                DataColumn dcTHOIGIAN4 = new DataColumn();
                dcTHOIGIAN4.ColumnName = "THOIGIAN4";
                DateTime dti = DateTime.Parse(dt.Rows[0]["THOIGIAN1"].ToString());
                dcTHOIGIAN4.DefaultValue = dti.AddSeconds(-1.0).Hour.ToString("00") + ":" + dti.AddSeconds(-1.0).Minute.ToString("00") + ":" + dti.AddSeconds(-1.0).Second.ToString("00");
                dt.Columns.Add(dcTHOIGIAN4);

                DataTable dt1 = clsMain.ReturnDataTable("Select MA_HANGHOA,TEN_HANGHOA,GIABAN1,(Select TEN_DONVITINH From DONVITINH Where DONVITINH.MA_DONVITINH=HANGHOA.MA_DONVITINH) As TEN_DONVITINH From HANGHOA Where MA_NHOMHANG IN (Select MA_NHOMHANG From NHOMHANG Where THUCDON=1)  And MA_HANGHOA NOT IN (Select MA_HANGHOA From THIETLAPBANGGIA Where MA_BANGGIA=" + clsMain.SQLString(sma) + ")");
                foreach (DataRow dr in dt1.Rows)
                {
                    dt.Rows.Add(dr["MA_HANGHOA"].ToString(), dt.Rows[0]["THOIGIAN1"].ToString(), dr["GIABAN1"].ToString(), dt.Rows[0]["THOIGIAN2"].ToString(), dr["GIABAN1"].ToString(), dt.Rows[0]["THOIGIAN3"].ToString(), dr["GIABAN1"].ToString(), dr["TEN_HANGHOA"].ToString(), dr["TEN_DONVITINH"].ToString());
                }

                gridControl2.DataSource = dt;
            }
            catch
            {
                gridControl2.DataSource = null;
            }
        }

        private void Saochepbanggia(string manguon, string madich)
        {
            if (DevExpress.XtraEditors.XtraMessageBox.Show(_xacnhanbanmuonsaochepbg, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                bSaochep = false;
                return;
            }
            string sSQL = "";
            sSQL += "Select  A.MA_HANGHOA,A.GIABAN1,A.GIABAN2,A.GIABAN3" + "\n";
            sSQL += "From THIETLAPBANGGIA A" + "\n";
            sSQL += "Where MA_BANGGIA=" + clsMain.SQLString(manguon) + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            sSQL = "";

            foreach (DataRow dr in dt.Rows)
            {
                sSQL += "Update THIETLAPBANGGIA Set " + "\n";
                sSQL += "GIABAN1=" + clsMain.SQLString(dr["GIABAN1"].ToString()) + "," + "\n";
                sSQL += "GIABAN2=" + clsMain.SQLString(dr["GIABAN2"].ToString()) + "," + "\n";
                sSQL += "GIABAN3=" + clsMain.SQLString(dr["GIABAN3"].ToString()) + "\n";
                sSQL += "Where MA_BANGGIA=" + clsMain.SQLString(madich) + "\n";
                sSQL += "And  MA_HANGHOA=" + clsMain.SQLString(dr["MA_HANGHOA"].ToString()) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_saochepthanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_saochepkothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            bSaochep = false;
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("42");
            btnSua.Enabled = clsUserManagement.AllowEdit("42");
            btnXoa.Enabled = clsUserManagement.AllowDelete("42");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
            btnSaochep.Enabled = btnSua.Enabled;
        }


        string _nhap = "";
        string _thongbao = "";
        string _luukothanhcong = "";
        string _xacnhanbanmuonxoa = "";
        string _nguyenlieudanhap = "";
        string _saochepthanhcong = "";
        string _saochepkothanhcong = "";
        string _xacnhanbanmuonsaochepbg = "";
        string _banggiadasudung = "";
        string _chonbanggianguondesaochep = "";
        string _chonbanggiadichdesaochep = "";
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void Frm_Lapbanggia_Nhahang_Load(object sender, EventArgs e)
        {

            this.Text = rm.GetString("lapbanggia", culture);
            lblTEN.Text = rm.GetString("ten", culture);
            lblGHICHU.Text = rm.GetString("ghichu", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _nhap = rm.GetString("nhap", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);
            _xacnhanbanmuonxoa = rm.GetString("xacnhanbanmuonxoa", culture);
            _nguyenlieudanhap = rm.GetString("nguyenlieudanhap", culture);
            _saochepthanhcong = rm.GetString("saochepthanhcong", culture);
            _saochepkothanhcong = rm.GetString("saochepkothanhcong", culture);
            _xacnhanbanmuonsaochepbg = rm.GetString("xacnhanbanmuonsaochepbg", culture);
            _banggiadasudung = rm.GetString("banggiadasudung", culture);
            _chonbanggianguondesaochep = rm.GetString("chonbanggianguondesaochep", culture);
            _chonbanggiadichdesaochep = rm.GetString("chonbanggiadichdesaochep", culture);

            chkSUDUNG.Text = rm.GetString("sudung", culture);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
            btnSaochep.Text = rm.GetString("saochep", culture);

            gridView2.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView2.Columns["TEN_HANGHOA"].Caption = rm.GetString("hanghoa", culture);
            gridView2.Columns["TEN_DONVITINH"].Caption = rm.GetString("dvt1", culture);
            gridView2.Columns["THOIGIAN1"].Caption = rm.GetString("thoigian1", culture);
            gridView2.Columns["GIABAN1"].Caption = rm.GetString("giaban11", culture);
            gridView2.Columns["THOIGIAN2"].Caption = rm.GetString("thoigian2", culture);
            gridView2.Columns["GIABAN2"].Caption = rm.GetString("giaban21", culture);
            gridView2.Columns["THOIGIAN3"].Caption = rm.GetString("thoigian3", culture);
            gridView2.Columns["GIABAN3"].Caption = rm.GetString("giaban3", culture);
            gridView2.Columns["THOIGIAN4"].Caption = rm.GetString("thoigian4", culture);

        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            sMaHangHoa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA_HANGHOA").ToString();
            sTenHangHoa = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN_HANGHOA").ToString();
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            if (sMaHangHoa == "")
                return;

            if (XtraMessageBox.Show("Xác nhận bạn muốn xóa giá hàng hóa: " + sTenHangHoa, "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;

            String sSQL = "";
            sSQL += "DELETE FROM THIETLAPBANGGIA" + " ";
            sSQL += "WHERE MA_BANGGIA = " + clsMain.SQLString(sma) + " ";
            sSQL += "AND MA_HANGHOA=" + clsMain.SQLString(sMaHangHoa) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                XtraMessageBox.Show("Đã xóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}