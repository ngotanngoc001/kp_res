﻿namespace KP_RES 
{
    partial class Frm_Buffet 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Buffet));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.palHANGHOA = new DevExpress.XtraEditors.PanelControl();
            this.fpnlHanghoa = new System.Windows.Forms.FlowLayoutPanel();
            this.btnXuongHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.palNHOMHANG = new DevExpress.XtraEditors.PanelControl();
            this.fpnlNhomhang = new System.Windows.Forms.FlowLayoutPanel();
            this.btnXuongNhomhang = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenNhomhang = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pnlNhomhang = new DevExpress.XtraEditors.PanelControl();
            this.btnChonmon = new DevExpress.XtraEditors.SimpleButton();
            this.cbohanghoa = new DevExpress.XtraEditors.LookUpEdit();
            this.lbNhom = new DevExpress.XtraEditors.LabelControl();
            this.lbSTT = new DevExpress.XtraEditors.LabelControl();
            this.lblTENNHOMHANG = new DevExpress.XtraEditors.LabelControl();
            this.btnLuuBuffet = new DevExpress.XtraEditors.SimpleButton();
            this.txtTENBUFFET = new DevExpress.XtraEditors.TextEdit();
            this.btnSuaBuffet = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaBuffet = new DevExpress.XtraEditors.SimpleButton();
            this.lblGHICHU = new DevExpress.XtraEditors.LabelControl();
            this.btnThemBuffet = new DevExpress.XtraEditors.SimpleButton();
            this.chkSUDUNGBUFFET = new DevExpress.XtraEditors.CheckEdit();
            this.txtSTTBUFFET = new DevExpress.XtraEditors.TextEdit();
            this.txtGHICHUBUFFET = new DevExpress.XtraEditors.TextEdit();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.palBuffet = new DevExpress.XtraEditors.PanelControl();
            this.fpnlBuffet = new System.Windows.Forms.FlowLayoutPanel();
            this.btnXuongBuffet = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenBuffet = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palHANGHOA)).BeginInit();
            this.palHANGHOA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palNHOMHANG)).BeginInit();
            this.palNHOMHANG.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlNhomhang)).BeginInit();
            this.pnlNhomhang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbohanghoa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENBUFFET.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGBUFFET.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTTBUFFET.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUBUFFET.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palBuffet)).BeginInit();
            this.palBuffet.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.palHANGHOA);
            this.panelControl1.Controls.Add(this.palNHOMHANG);
            this.panelControl1.Controls.Add(this.panelControl4);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(913, 733);
            this.panelControl1.TabIndex = 0;
            // 
            // palHANGHOA
            // 
            this.palHANGHOA.Controls.Add(this.fpnlHanghoa);
            this.palHANGHOA.Controls.Add(this.btnXuongHangHoa);
            this.palHANGHOA.Controls.Add(this.btnLenHangHoa);
            this.palHANGHOA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palHANGHOA.Location = new System.Drawing.Point(2, 2);
            this.palHANGHOA.Name = "palHANGHOA";
            this.palHANGHOA.Size = new System.Drawing.Size(424, 729);
            this.palHANGHOA.TabIndex = 0;
            // 
            // fpnlHanghoa
            // 
            this.fpnlHanghoa.AutoScroll = true;
            this.fpnlHanghoa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpnlHanghoa.Font = new System.Drawing.Font("Tahoma", 12F);
            this.fpnlHanghoa.Location = new System.Drawing.Point(2, 37);
            this.fpnlHanghoa.Name = "fpnlHanghoa";
            this.fpnlHanghoa.Size = new System.Drawing.Size(420, 655);
            this.fpnlHanghoa.TabIndex = 18;
            // 
            // btnXuongHangHoa
            // 
            this.btnXuongHangHoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongHangHoa.Appearance.Options.UseFont = true;
            this.btnXuongHangHoa.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongHangHoa.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongHangHoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongHangHoa.Location = new System.Drawing.Point(2, 692);
            this.btnXuongHangHoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongHangHoa.Name = "btnXuongHangHoa";
            this.btnXuongHangHoa.Size = new System.Drawing.Size(420, 35);
            this.btnXuongHangHoa.TabIndex = 17;
            this.btnXuongHangHoa.Click += new System.EventHandler(this.btnXuongHangHoa_Click);
            // 
            // btnLenHangHoa
            // 
            this.btnLenHangHoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenHangHoa.Appearance.Options.UseFont = true;
            this.btnLenHangHoa.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenHangHoa.Image = ((System.Drawing.Image)(resources.GetObject("btnLenHangHoa.Image")));
            this.btnLenHangHoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenHangHoa.Location = new System.Drawing.Point(2, 2);
            this.btnLenHangHoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnLenHangHoa.Name = "btnLenHangHoa";
            this.btnLenHangHoa.Size = new System.Drawing.Size(420, 35);
            this.btnLenHangHoa.TabIndex = 16;
            this.btnLenHangHoa.Click += new System.EventHandler(this.btnLenHangHoa_Click);
            // 
            // palNHOMHANG
            // 
            this.palNHOMHANG.Controls.Add(this.fpnlNhomhang);
            this.palNHOMHANG.Controls.Add(this.btnXuongNhomhang);
            this.palNHOMHANG.Controls.Add(this.btnLenNhomhang);
            this.palNHOMHANG.Dock = System.Windows.Forms.DockStyle.Right;
            this.palNHOMHANG.Location = new System.Drawing.Point(426, 2);
            this.palNHOMHANG.Margin = new System.Windows.Forms.Padding(0);
            this.palNHOMHANG.Name = "palNHOMHANG";
            this.palNHOMHANG.Size = new System.Drawing.Size(135, 729);
            this.palNHOMHANG.TabIndex = 1;
            // 
            // fpnlNhomhang
            // 
            this.fpnlNhomhang.AutoScroll = true;
            this.fpnlNhomhang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpnlNhomhang.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fpnlNhomhang.Location = new System.Drawing.Point(2, 37);
            this.fpnlNhomhang.Margin = new System.Windows.Forms.Padding(0);
            this.fpnlNhomhang.Name = "fpnlNhomhang";
            this.fpnlNhomhang.Size = new System.Drawing.Size(131, 655);
            this.fpnlNhomhang.TabIndex = 17;
            // 
            // btnXuongNhomhang
            // 
            this.btnXuongNhomhang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongNhomhang.Appearance.Options.UseFont = true;
            this.btnXuongNhomhang.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongNhomhang.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongNhomhang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongNhomhang.Location = new System.Drawing.Point(2, 692);
            this.btnXuongNhomhang.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongNhomhang.Name = "btnXuongNhomhang";
            this.btnXuongNhomhang.Size = new System.Drawing.Size(131, 35);
            this.btnXuongNhomhang.TabIndex = 16;
            this.btnXuongNhomhang.Click += new System.EventHandler(this.btnXuongNhomhang_Click);
            // 
            // btnLenNhomhang
            // 
            this.btnLenNhomhang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenNhomhang.Appearance.Options.UseFont = true;
            this.btnLenNhomhang.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenNhomhang.Image = ((System.Drawing.Image)(resources.GetObject("btnLenNhomhang.Image")));
            this.btnLenNhomhang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenNhomhang.Location = new System.Drawing.Point(2, 2);
            this.btnLenNhomhang.Margin = new System.Windows.Forms.Padding(4);
            this.btnLenNhomhang.Name = "btnLenNhomhang";
            this.btnLenNhomhang.Size = new System.Drawing.Size(131, 35);
            this.btnLenNhomhang.TabIndex = 15;
            this.btnLenNhomhang.Click += new System.EventHandler(this.btnLenNhomhang_Click);
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.pnlNhomhang);
            this.panelControl4.Controls.Add(this.panelControl7);
            this.panelControl4.Controls.Add(this.palBuffet);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl4.Location = new System.Drawing.Point(561, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(350, 729);
            this.panelControl4.TabIndex = 0;
            // 
            // pnlNhomhang
            // 
            this.pnlNhomhang.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlNhomhang.Controls.Add(this.btnChonmon);
            this.pnlNhomhang.Controls.Add(this.cbohanghoa);
            this.pnlNhomhang.Controls.Add(this.lbNhom);
            this.pnlNhomhang.Controls.Add(this.lbSTT);
            this.pnlNhomhang.Controls.Add(this.lblTENNHOMHANG);
            this.pnlNhomhang.Controls.Add(this.btnLuuBuffet);
            this.pnlNhomhang.Controls.Add(this.txtTENBUFFET);
            this.pnlNhomhang.Controls.Add(this.btnSuaBuffet);
            this.pnlNhomhang.Controls.Add(this.btnXoaBuffet);
            this.pnlNhomhang.Controls.Add(this.lblGHICHU);
            this.pnlNhomhang.Controls.Add(this.btnThemBuffet);
            this.pnlNhomhang.Controls.Add(this.chkSUDUNGBUFFET);
            this.pnlNhomhang.Controls.Add(this.txtSTTBUFFET);
            this.pnlNhomhang.Controls.Add(this.txtGHICHUBUFFET);
            this.pnlNhomhang.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlNhomhang.Location = new System.Drawing.Point(99, 2);
            this.pnlNhomhang.Name = "pnlNhomhang";
            this.pnlNhomhang.Size = new System.Drawing.Size(249, 555);
            this.pnlNhomhang.TabIndex = 0;
            // 
            // btnChonmon
            // 
            this.btnChonmon.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonmon.Appearance.Options.UseFont = true;
            this.btnChonmon.Image = global::KP_RES.Properties.Resources.save_as_26;
            this.btnChonmon.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnChonmon.Location = new System.Drawing.Point(3, 201);
            this.btnChonmon.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonmon.Name = "btnChonmon";
            this.btnChonmon.Size = new System.Drawing.Size(162, 35);
            this.btnChonmon.TabIndex = 13;
            this.btnChonmon.Text = "&Chọn món";
            this.btnChonmon.Click += new System.EventHandler(this.btnChonmon_Click);
            // 
            // cbohanghoa
            // 
            this.cbohanghoa.EnterMoveNextControl = true;
            this.cbohanghoa.Location = new System.Drawing.Point(76, 95);
            this.cbohanghoa.Name = "cbohanghoa";
            this.cbohanghoa.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbohanghoa.Properties.Appearance.Options.UseFont = true;
            this.cbohanghoa.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbohanghoa.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbohanghoa.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cbohanghoa.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbohanghoa.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cbohanghoa.Properties.DisplayMember = "TEN";
            this.cbohanghoa.Properties.DropDownItemHeight = 40;
            this.cbohanghoa.Properties.NullText = "";
            this.cbohanghoa.Properties.ShowHeader = false;
            this.cbohanghoa.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbohanghoa.Properties.ValueMember = "MA";
            this.cbohanghoa.Size = new System.Drawing.Size(170, 26);
            this.cbohanghoa.TabIndex = 7;
            // 
            // lbNhom
            // 
            this.lbNhom.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNhom.Location = new System.Drawing.Point(3, 98);
            this.lbNhom.Margin = new System.Windows.Forms.Padding(4);
            this.lbNhom.Name = "lbNhom";
            this.lbNhom.Size = new System.Drawing.Size(73, 19);
            this.lbNhom.TabIndex = 6;
            this.lbNhom.Text = "Món chính";
            // 
            // lbSTT
            // 
            this.lbSTT.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSTT.Location = new System.Drawing.Point(3, 37);
            this.lbSTT.Margin = new System.Windows.Forms.Padding(4);
            this.lbSTT.Name = "lbSTT";
            this.lbSTT.Size = new System.Drawing.Size(29, 19);
            this.lbSTT.TabIndex = 2;
            this.lbSTT.Text = "STT";
            // 
            // lblTENNHOMHANG
            // 
            this.lblTENNHOMHANG.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTENNHOMHANG.Location = new System.Drawing.Point(3, 8);
            this.lblTENNHOMHANG.Margin = new System.Windows.Forms.Padding(4);
            this.lblTENNHOMHANG.Name = "lblTENNHOMHANG";
            this.lblTENNHOMHANG.Size = new System.Drawing.Size(27, 19);
            this.lblTENNHOMHANG.TabIndex = 0;
            this.lblTENNHOMHANG.Text = "Tên";
            // 
            // btnLuuBuffet
            // 
            this.btnLuuBuffet.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuBuffet.Appearance.Options.UseFont = true;
            this.btnLuuBuffet.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuuBuffet.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuuBuffet.Location = new System.Drawing.Point(3, 158);
            this.btnLuuBuffet.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuuBuffet.Name = "btnLuuBuffet";
            this.btnLuuBuffet.Size = new System.Drawing.Size(80, 35);
            this.btnLuuBuffet.TabIndex = 9;
            this.btnLuuBuffet.Text = "&Lưu";
            this.btnLuuBuffet.Click += new System.EventHandler(this.btnLuuBuffet_Click);
            // 
            // txtTENBUFFET
            // 
            this.txtTENBUFFET.EnterMoveNextControl = true;
            this.txtTENBUFFET.Location = new System.Drawing.Point(76, 5);
            this.txtTENBUFFET.Name = "txtTENBUFFET";
            this.txtTENBUFFET.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTENBUFFET.Properties.Appearance.Options.UseFont = true;
            this.txtTENBUFFET.Size = new System.Drawing.Size(170, 26);
            this.txtTENBUFFET.TabIndex = 1;
            // 
            // btnSuaBuffet
            // 
            this.btnSuaBuffet.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaBuffet.Appearance.Options.UseFont = true;
            this.btnSuaBuffet.Image = ((System.Drawing.Image)(resources.GetObject("btnSuaBuffet.Image")));
            this.btnSuaBuffet.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSuaBuffet.Location = new System.Drawing.Point(167, 158);
            this.btnSuaBuffet.Margin = new System.Windows.Forms.Padding(4);
            this.btnSuaBuffet.Name = "btnSuaBuffet";
            this.btnSuaBuffet.Size = new System.Drawing.Size(80, 35);
            this.btnSuaBuffet.TabIndex = 11;
            this.btnSuaBuffet.Text = "&Sửa";
            this.btnSuaBuffet.Click += new System.EventHandler(this.btnSuaNhomHang_Click);
            // 
            // btnXoaBuffet
            // 
            this.btnXoaBuffet.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaBuffet.Appearance.Options.UseFont = true;
            this.btnXoaBuffet.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoaBuffet.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoaBuffet.Location = new System.Drawing.Point(167, 201);
            this.btnXoaBuffet.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoaBuffet.Name = "btnXoaBuffet";
            this.btnXoaBuffet.Size = new System.Drawing.Size(80, 35);
            this.btnXoaBuffet.TabIndex = 12;
            this.btnXoaBuffet.Text = "&Xóa";
            this.btnXoaBuffet.Click += new System.EventHandler(this.btnXoaBuffet_Click);
            // 
            // lblGHICHU
            // 
            this.lblGHICHU.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGHICHU.Location = new System.Drawing.Point(3, 66);
            this.lblGHICHU.Margin = new System.Windows.Forms.Padding(4);
            this.lblGHICHU.Name = "lblGHICHU";
            this.lblGHICHU.Size = new System.Drawing.Size(54, 19);
            this.lblGHICHU.TabIndex = 4;
            this.lblGHICHU.Text = "Ghi chú";
            // 
            // btnThemBuffet
            // 
            this.btnThemBuffet.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemBuffet.Appearance.Options.UseFont = true;
            this.btnThemBuffet.Image = ((System.Drawing.Image)(resources.GetObject("btnThemBuffet.Image")));
            this.btnThemBuffet.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThemBuffet.Location = new System.Drawing.Point(85, 158);
            this.btnThemBuffet.Margin = new System.Windows.Forms.Padding(4);
            this.btnThemBuffet.Name = "btnThemBuffet";
            this.btnThemBuffet.Size = new System.Drawing.Size(80, 35);
            this.btnThemBuffet.TabIndex = 10;
            this.btnThemBuffet.Text = "&Thêm";
            this.btnThemBuffet.Click += new System.EventHandler(this.btnThemNhomHang_Click);
            // 
            // chkSUDUNGBUFFET
            // 
            this.chkSUDUNGBUFFET.EnterMoveNextControl = true;
            this.chkSUDUNGBUFFET.Location = new System.Drawing.Point(74, 127);
            this.chkSUDUNGBUFFET.Name = "chkSUDUNGBUFFET";
            this.chkSUDUNGBUFFET.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNGBUFFET.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNGBUFFET.Properties.Caption = "Sử dụng";
            this.chkSUDUNGBUFFET.Size = new System.Drawing.Size(92, 24);
            this.chkSUDUNGBUFFET.TabIndex = 8;
            // 
            // txtSTTBUFFET
            // 
            this.txtSTTBUFFET.EnterMoveNextControl = true;
            this.txtSTTBUFFET.Location = new System.Drawing.Point(76, 34);
            this.txtSTTBUFFET.Name = "txtSTTBUFFET";
            this.txtSTTBUFFET.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSTTBUFFET.Properties.Appearance.Options.UseFont = true;
            this.txtSTTBUFFET.Properties.Mask.BeepOnError = true;
            this.txtSTTBUFFET.Properties.Mask.EditMask = "d";
            this.txtSTTBUFFET.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSTTBUFFET.Size = new System.Drawing.Size(170, 26);
            this.txtSTTBUFFET.TabIndex = 3;
            // 
            // txtGHICHUBUFFET
            // 
            this.txtGHICHUBUFFET.EnterMoveNextControl = true;
            this.txtGHICHUBUFFET.Location = new System.Drawing.Point(76, 63);
            this.txtGHICHUBUFFET.Name = "txtGHICHUBUFFET";
            this.txtGHICHUBUFFET.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHUBUFFET.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHUBUFFET.Size = new System.Drawing.Size(170, 26);
            this.txtGHICHUBUFFET.TabIndex = 5;
            // 
            // panelControl7
            // 
            this.panelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl7.Controls.Add(this.btnCapnhat);
            this.panelControl7.Controls.Add(this.btnBanphim);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl7.Location = new System.Drawing.Point(99, 692);
            this.panelControl7.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(249, 35);
            this.panelControl7.TabIndex = 17;
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(0, 0);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(4);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(143, 35);
            this.btnCapnhat.TabIndex = 1;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(143, 0);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(4);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(106, 35);
            this.btnBanphim.TabIndex = 10;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // palBuffet
            // 
            this.palBuffet.Controls.Add(this.fpnlBuffet);
            this.palBuffet.Controls.Add(this.btnXuongBuffet);
            this.palBuffet.Controls.Add(this.btnLenBuffet);
            this.palBuffet.Dock = System.Windows.Forms.DockStyle.Left;
            this.palBuffet.Location = new System.Drawing.Point(2, 2);
            this.palBuffet.Margin = new System.Windows.Forms.Padding(0);
            this.palBuffet.Name = "palBuffet";
            this.palBuffet.Size = new System.Drawing.Size(97, 725);
            this.palBuffet.TabIndex = 18;
            // 
            // fpnlBuffet
            // 
            this.fpnlBuffet.AutoScroll = true;
            this.fpnlBuffet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpnlBuffet.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fpnlBuffet.Location = new System.Drawing.Point(2, 37);
            this.fpnlBuffet.Margin = new System.Windows.Forms.Padding(0);
            this.fpnlBuffet.Name = "fpnlBuffet";
            this.fpnlBuffet.Size = new System.Drawing.Size(93, 651);
            this.fpnlBuffet.TabIndex = 17;
            // 
            // btnXuongBuffet
            // 
            this.btnXuongBuffet.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongBuffet.Appearance.Options.UseFont = true;
            this.btnXuongBuffet.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongBuffet.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongBuffet.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongBuffet.Location = new System.Drawing.Point(2, 688);
            this.btnXuongBuffet.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongBuffet.Name = "btnXuongBuffet";
            this.btnXuongBuffet.Size = new System.Drawing.Size(93, 35);
            this.btnXuongBuffet.TabIndex = 16;
            this.btnXuongBuffet.Click += new System.EventHandler(this.btnXuongBuffet_Click);
            // 
            // btnLenBuffet
            // 
            this.btnLenBuffet.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenBuffet.Appearance.Options.UseFont = true;
            this.btnLenBuffet.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenBuffet.Image = ((System.Drawing.Image)(resources.GetObject("btnLenBuffet.Image")));
            this.btnLenBuffet.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenBuffet.Location = new System.Drawing.Point(2, 2);
            this.btnLenBuffet.Margin = new System.Windows.Forms.Padding(4);
            this.btnLenBuffet.Name = "btnLenBuffet";
            this.btnLenBuffet.Size = new System.Drawing.Size(93, 35);
            this.btnLenBuffet.TabIndex = 15;
            this.btnLenBuffet.Click += new System.EventHandler(this.btnLenBuffet_Click);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton5.Location = new System.Drawing.Point(65, 32);
            this.simpleButton5.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(80, 35);
            this.simpleButton5.TabIndex = 10;
            this.simpleButton5.Text = "&Lưu";
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton6.Location = new System.Drawing.Point(73, 84);
            this.simpleButton6.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(80, 35);
            this.simpleButton6.TabIndex = 11;
            this.simpleButton6.Text = "&Lưu";
            // 
            // simpleButton7
            // 
            this.simpleButton7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton7.Appearance.Options.UseFont = true;
            this.simpleButton7.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton7.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton7.Location = new System.Drawing.Point(81, 92);
            this.simpleButton7.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(80, 35);
            this.simpleButton7.TabIndex = 12;
            this.simpleButton7.Text = "&Lưu";
            // 
            // simpleButton8
            // 
            this.simpleButton8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton8.Appearance.Options.UseFont = true;
            this.simpleButton8.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton8.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton8.Location = new System.Drawing.Point(89, 100);
            this.simpleButton8.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(80, 35);
            this.simpleButton8.TabIndex = 13;
            this.simpleButton8.Text = "&Lưu";
            // 
            // simpleButton9
            // 
            this.simpleButton9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton9.Appearance.Options.UseFont = true;
            this.simpleButton9.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton9.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton9.Location = new System.Drawing.Point(114, 150);
            this.simpleButton9.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(80, 35);
            this.simpleButton9.TabIndex = 14;
            this.simpleButton9.Text = "&Lưu";
            // 
            // Frm_Buffet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 733);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Buffet";
            this.Text = "Cài đặt Buffet";
            this.Load += new System.EventHandler(this.Frm_Buffet_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Buffet_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palHANGHOA)).EndInit();
            this.palHANGHOA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palNHOMHANG)).EndInit();
            this.palNHOMHANG.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlNhomhang)).EndInit();
            this.pnlNhomhang.ResumeLayout(false);
            this.pnlNhomhang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbohanghoa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENBUFFET.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGBUFFET.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTTBUFFET.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUBUFFET.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palBuffet)).EndInit();
            this.palBuffet.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl palNHOMHANG;
        private DevExpress.XtraEditors.PanelControl palHANGHOA;
        private DevExpress.XtraEditors.SimpleButton btnXuongNhomhang;
        private DevExpress.XtraEditors.SimpleButton btnLenNhomhang;
        private System.Windows.Forms.FlowLayoutPanel fpnlHanghoa;
        private DevExpress.XtraEditors.SimpleButton btnXuongHangHoa;
        private DevExpress.XtraEditors.SimpleButton btnLenHangHoa;
        private System.Windows.Forms.FlowLayoutPanel fpnlNhomhang;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNGBUFFET;
        private DevExpress.XtraEditors.LabelControl lblGHICHU;
        private DevExpress.XtraEditors.TextEdit txtGHICHUBUFFET;
        private DevExpress.XtraEditors.LabelControl lblTENNHOMHANG;
        private DevExpress.XtraEditors.TextEdit txtTENBUFFET;
        private DevExpress.XtraEditors.SimpleButton btnLuuBuffet;
        private DevExpress.XtraEditors.SimpleButton btnSuaBuffet;
        private DevExpress.XtraEditors.SimpleButton btnXoaBuffet;
        private DevExpress.XtraEditors.SimpleButton btnThemBuffet;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.PanelControl pnlNhomhang;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.TextEdit txtSTTBUFFET;
        private DevExpress.XtraEditors.LabelControl lbSTT;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.PanelControl palBuffet;
        private System.Windows.Forms.FlowLayoutPanel fpnlBuffet;
        private DevExpress.XtraEditors.SimpleButton btnXuongBuffet;
        private DevExpress.XtraEditors.SimpleButton btnLenBuffet;
        private DevExpress.XtraEditors.LookUpEdit cbohanghoa;
        private DevExpress.XtraEditors.LabelControl lbNhom;
        private DevExpress.XtraEditors.SimpleButton btnChonmon;




    }
}