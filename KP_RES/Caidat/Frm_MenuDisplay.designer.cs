﻿namespace KP_RES 
{
    partial class Frm_MenuDisplay 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_MenuDisplay));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pn_Hinh = new DevExpress.XtraEditors.PanelControl();
            this.pn_xemvideo = new DevExpress.XtraEditors.PanelControl();
            this.WindowsMediaPlayer = new AxWMPLib.AxWindowsMediaPlayer();
            this.pn_cuon = new DevExpress.XtraEditors.PanelControl();
            this.pal_xuong = new DevExpress.XtraEditors.PanelControl();
            this.lbl_sotrang = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_tranglen = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_trangxuong = new DevExpress.XtraEditors.SimpleButton();
            this.pnCheckAll = new DevExpress.XtraEditors.PanelControl();
            this.chk_ChooseAll = new DevExpress.XtraEditors.CheckEdit();
            this.btn_bochonhinh = new DevExpress.XtraEditors.SimpleButton();
            this.btnChonTatCaHinh = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Them = new DevExpress.XtraEditors.SimpleButton();
            this.pn_CaiDat_Video = new DevExpress.XtraEditors.PanelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENHINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bnt_Play = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn_stop = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.CHON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAHINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.chk_chooseVideo = new DevExpress.XtraEditors.CheckEdit();
            this.btnbochonvideo = new DevExpress.XtraEditors.SimpleButton();
            this.btnChonTatCaVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btn_XoaVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ThemVideo = new DevExpress.XtraEditors.SimpleButton();
            this.pn_CuaHang = new DevExpress.XtraEditors.PanelControl();
            this.panel_Sandard = new DevExpress.XtraEditors.PanelControl();
            this.pnQuangCao = new DevExpress.XtraEditors.PanelControl();
            this.txt_SizeTenMon = new DevExpress.XtraEditors.TextEdit();
            this.txt_SizeDonGia = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.ck_6 = new DevExpress.XtraEditors.CheckEdit();
            this.chk_ShowHide = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.font_chu = new DevExpress.XtraEditors.FontEdit();
            this.ck_20 = new DevExpress.XtraEditors.CheckEdit();
            this.ck_10 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.memoNoiDung = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtThoiGianMenu = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtThoiGianHinh = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.pal_BUTTON = new DevExpress.XtraEditors.PanelControl();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnRefesh = new DevExpress.XtraEditors.SimpleButton();
            this.btnHuyBo = new DevExpress.XtraEditors.SimpleButton();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_Hinh)).BeginInit();
            this.pn_Hinh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_xemvideo)).BeginInit();
            this.pn_xemvideo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WindowsMediaPlayer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_cuon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pal_xuong)).BeginInit();
            this.pal_xuong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnCheckAll)).BeginInit();
            this.pnCheckAll.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk_ChooseAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_CaiDat_Video)).BeginInit();
            this.pn_CaiDat_Video.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnt_Play)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_stop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk_chooseVideo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_CuaHang)).BeginInit();
            this.pn_CuaHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel_Sandard)).BeginInit();
            this.panel_Sandard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnQuangCao)).BeginInit();
            this.pnQuangCao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_SizeTenMon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_SizeDonGia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck_6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_ShowHide.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.font_chu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck_20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck_10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoNoiDung.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThoiGianMenu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThoiGianHinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pal_BUTTON)).BeginInit();
            this.pal_BUTTON.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pn_Hinh);
            this.panelControl1.Controls.Add(this.pn_CaiDat_Video);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1354, 615);
            this.panelControl1.TabIndex = 0;
            // 
            // pn_Hinh
            // 
            this.pn_Hinh.Controls.Add(this.pn_xemvideo);
            this.pn_Hinh.Controls.Add(this.pn_cuon);
            this.pn_Hinh.Controls.Add(this.pal_xuong);
            this.pn_Hinh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_Hinh.Location = new System.Drawing.Point(2, 2);
            this.pn_Hinh.Name = "pn_Hinh";
            this.pn_Hinh.Size = new System.Drawing.Size(1350, 307);
            this.pn_Hinh.TabIndex = 56;
            // 
            // pn_xemvideo
            // 
            this.pn_xemvideo.Controls.Add(this.WindowsMediaPlayer);
            this.pn_xemvideo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_xemvideo.Location = new System.Drawing.Point(2, 36);
            this.pn_xemvideo.Name = "pn_xemvideo";
            this.pn_xemvideo.Size = new System.Drawing.Size(1346, 269);
            this.pn_xemvideo.TabIndex = 0;
            this.pn_xemvideo.Visible = false;
            // 
            // WindowsMediaPlayer
            // 
            this.WindowsMediaPlayer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WindowsMediaPlayer.Enabled = true;
            this.WindowsMediaPlayer.Location = new System.Drawing.Point(2, 2);
            this.WindowsMediaPlayer.Name = "WindowsMediaPlayer";
            this.WindowsMediaPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("WindowsMediaPlayer.OcxState")));
            this.WindowsMediaPlayer.Size = new System.Drawing.Size(1342, 265);
            this.WindowsMediaPlayer.TabIndex = 2;
            // 
            // pn_cuon
            // 
            this.pn_cuon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_cuon.Location = new System.Drawing.Point(2, 36);
            this.pn_cuon.Name = "pn_cuon";
            this.pn_cuon.Size = new System.Drawing.Size(1346, 269);
            this.pn_cuon.TabIndex = 56;
            // 
            // pal_xuong
            // 
            this.pal_xuong.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_xuong.Controls.Add(this.lbl_sotrang);
            this.pal_xuong.Controls.Add(this.bnt_tranglen);
            this.pal_xuong.Controls.Add(this.bnt_trangxuong);
            this.pal_xuong.Controls.Add(this.pnCheckAll);
            this.pal_xuong.Controls.Add(this.btn_bochonhinh);
            this.pal_xuong.Controls.Add(this.btnChonTatCaHinh);
            this.pal_xuong.Controls.Add(this.btn_Xoa);
            this.pal_xuong.Controls.Add(this.btn_Them);
            this.pal_xuong.Dock = System.Windows.Forms.DockStyle.Top;
            this.pal_xuong.Location = new System.Drawing.Point(2, 2);
            this.pal_xuong.Name = "pal_xuong";
            this.pal_xuong.Size = new System.Drawing.Size(1346, 34);
            this.pal_xuong.TabIndex = 53;
            // 
            // lbl_sotrang
            // 
            this.lbl_sotrang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_sotrang.Appearance.Options.UseFont = true;
            this.lbl_sotrang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_sotrang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.lbl_sotrang.Location = new System.Drawing.Point(903, 0);
            this.lbl_sotrang.Margin = new System.Windows.Forms.Padding(4);
            this.lbl_sotrang.Name = "lbl_sotrang";
            this.lbl_sotrang.Size = new System.Drawing.Size(443, 34);
            this.lbl_sotrang.TabIndex = 49;
            this.lbl_sotrang.Text = "1/5";
            // 
            // bnt_tranglen
            // 
            this.bnt_tranglen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_tranglen.Appearance.Options.UseFont = true;
            this.bnt_tranglen.Appearance.Options.UseTextOptions = true;
            this.bnt_tranglen.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bnt_tranglen.Dock = System.Windows.Forms.DockStyle.Left;
            this.bnt_tranglen.Image = global::KP_RES.Properties.Resources.up22_26;
            this.bnt_tranglen.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_tranglen.Location = new System.Drawing.Point(774, 0);
            this.bnt_tranglen.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_tranglen.Name = "bnt_tranglen";
            this.bnt_tranglen.Size = new System.Drawing.Size(129, 34);
            this.bnt_tranglen.TabIndex = 50;
            this.bnt_tranglen.Text = "Khu vực";
            this.bnt_tranglen.Click += new System.EventHandler(this.bnt_tranglen_Click);
            // 
            // bnt_trangxuong
            // 
            this.bnt_trangxuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_trangxuong.Appearance.Options.UseFont = true;
            this.bnt_trangxuong.Appearance.Options.UseTextOptions = true;
            this.bnt_trangxuong.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bnt_trangxuong.Dock = System.Windows.Forms.DockStyle.Left;
            this.bnt_trangxuong.Image = global::KP_RES.Properties.Resources.up11_26;
            this.bnt_trangxuong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_trangxuong.Location = new System.Drawing.Point(645, 0);
            this.bnt_trangxuong.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_trangxuong.Name = "bnt_trangxuong";
            this.bnt_trangxuong.Size = new System.Drawing.Size(129, 34);
            this.bnt_trangxuong.TabIndex = 50;
            this.bnt_trangxuong.Text = "Khu vực";
            this.bnt_trangxuong.Click += new System.EventHandler(this.bnt_trangxuong_Click);
            // 
            // pnCheckAll
            // 
            this.pnCheckAll.Controls.Add(this.chk_ChooseAll);
            this.pnCheckAll.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnCheckAll.Location = new System.Drawing.Point(516, 0);
            this.pnCheckAll.Name = "pnCheckAll";
            this.pnCheckAll.Size = new System.Drawing.Size(129, 34);
            this.pnCheckAll.TabIndex = 51;
            this.pnCheckAll.Visible = false;
            // 
            // chk_ChooseAll
            // 
            this.chk_ChooseAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk_ChooseAll.Location = new System.Drawing.Point(2, 2);
            this.chk_ChooseAll.Name = "chk_ChooseAll";
            this.chk_ChooseAll.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk_ChooseAll.Properties.Appearance.Options.UseFont = true;
            this.chk_ChooseAll.Properties.Caption = "Chọn Tất Cả";
            this.chk_ChooseAll.Size = new System.Drawing.Size(125, 24);
            this.chk_ChooseAll.TabIndex = 0;
            this.chk_ChooseAll.CheckedChanged += new System.EventHandler(this.chk_ChooseAll_CheckedChanged);
            // 
            // btn_bochonhinh
            // 
            this.btn_bochonhinh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_bochonhinh.Appearance.Options.UseFont = true;
            this.btn_bochonhinh.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_bochonhinh.Image = global::KP_RES.Properties.Resources.restart_26;
            this.btn_bochonhinh.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_bochonhinh.Location = new System.Drawing.Point(387, 0);
            this.btn_bochonhinh.Margin = new System.Windows.Forms.Padding(4);
            this.btn_bochonhinh.Name = "btn_bochonhinh";
            this.btn_bochonhinh.Size = new System.Drawing.Size(129, 34);
            this.btn_bochonhinh.TabIndex = 55;
            this.btn_bochonhinh.Text = "Bỏ chọn";
            this.btn_bochonhinh.Click += new System.EventHandler(this.btn_bochonhinh_Click);
            // 
            // btnChonTatCaHinh
            // 
            this.btnChonTatCaHinh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonTatCaHinh.Appearance.Options.UseFont = true;
            this.btnChonTatCaHinh.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnChonTatCaHinh.Image = ((System.Drawing.Image)(resources.GetObject("btnChonTatCaHinh.Image")));
            this.btnChonTatCaHinh.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnChonTatCaHinh.Location = new System.Drawing.Point(258, 0);
            this.btnChonTatCaHinh.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonTatCaHinh.Name = "btnChonTatCaHinh";
            this.btnChonTatCaHinh.Size = new System.Drawing.Size(129, 34);
            this.btnChonTatCaHinh.TabIndex = 57;
            this.btnChonTatCaHinh.Text = "Chọn tất cả";
            this.btnChonTatCaHinh.Click += new System.EventHandler(this.btnChonTatCaHinh_Click);
            // 
            // btn_Xoa
            // 
            this.btn_Xoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Xoa.Appearance.Options.UseFont = true;
            this.btn_Xoa.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_Xoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btn_Xoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_Xoa.Location = new System.Drawing.Point(129, 0);
            this.btn_Xoa.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Xoa.Name = "btn_Xoa";
            this.btn_Xoa.Size = new System.Drawing.Size(129, 34);
            this.btn_Xoa.TabIndex = 54;
            this.btn_Xoa.Text = "Xóa hình";
            this.btn_Xoa.Click += new System.EventHandler(this.btn_Xoa_Click);
            // 
            // btn_Them
            // 
            this.btn_Them.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Them.Appearance.Options.UseFont = true;
            this.btn_Them.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_Them.Image = global::KP_RES.Properties.Resources.slr_camera_26;
            this.btn_Them.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_Them.Location = new System.Drawing.Point(0, 0);
            this.btn_Them.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Them.Name = "btn_Them";
            this.btn_Them.Size = new System.Drawing.Size(129, 34);
            this.btn_Them.TabIndex = 51;
            this.btn_Them.Text = "Thêm hình";
            this.btn_Them.Click += new System.EventHandler(this.btn_Them_Click);
            // 
            // pn_CaiDat_Video
            // 
            this.pn_CaiDat_Video.Controls.Add(this.gridControl2);
            this.pn_CaiDat_Video.Controls.Add(this.panelControl3);
            this.pn_CaiDat_Video.Controls.Add(this.pn_CuaHang);
            this.pn_CaiDat_Video.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pn_CaiDat_Video.Location = new System.Drawing.Point(2, 309);
            this.pn_CaiDat_Video.Name = "pn_CaiDat_Video";
            this.pn_CaiDat_Video.Size = new System.Drawing.Size(1350, 304);
            this.pn_CaiDat_Video.TabIndex = 58;
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(2, 2);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.bnt_Play,
            this.btn_stop});
            this.gridControl2.Size = new System.Drawing.Size(695, 262);
            this.gridControl2.TabIndex = 55;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.TENHINH,
            this.gridColumn10,
            this.gridColumn11,
            this.CHON,
            this.MAHINH});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupFormat = "";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsView.ShowGroupedColumns = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.Default;
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 100;
            // 
            // TENHINH
            // 
            this.TENHINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TENHINH.AppearanceHeader.Options.UseFont = true;
            this.TENHINH.AppearanceHeader.Options.UseTextOptions = true;
            this.TENHINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENHINH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENHINH.Caption = "Tên";
            this.TENHINH.FieldName = "TENHINH";
            this.TENHINH.Name = "TENHINH";
            this.TENHINH.OptionsColumn.AllowEdit = false;
            this.TENHINH.OptionsColumn.AllowFocus = false;
            this.TENHINH.Visible = true;
            this.TENHINH.VisibleIndex = 1;
            this.TENHINH.Width = 383;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn10.AppearanceCell.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn10.AppearanceHeader.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "Xem ";
            this.gridColumn10.ColumnEdit = this.bnt_Play;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.FixedWidth = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            this.gridColumn10.Width = 100;
            // 
            // bnt_Play
            // 
            this.bnt_Play.AutoHeight = false;
            this.bnt_Play.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("bnt_Play.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.bnt_Play.Name = "bnt_Play";
            this.bnt_Play.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.bnt_Play.Click += new System.EventHandler(this.bnt_Play_Click);
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn11.AppearanceCell.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn11.AppearanceHeader.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "Thoát";
            this.gridColumn11.ColumnEdit = this.btn_stop;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.FixedWidth = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 3;
            this.gridColumn11.Width = 100;
            // 
            // btn_stop
            // 
            this.btn_stop.AutoHeight = false;
            this.btn_stop.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btn_stop.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // CHON
            // 
            this.CHON.AppearanceCell.Options.UseTextOptions = true;
            this.CHON.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHON.AppearanceHeader.Options.UseFont = true;
            this.CHON.AppearanceHeader.Options.UseTextOptions = true;
            this.CHON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHON.Caption = "Chọn";
            this.CHON.FieldName = "CHON";
            this.CHON.Name = "CHON";
            this.CHON.OptionsColumn.FixedWidth = true;
            this.CHON.Visible = true;
            this.CHON.VisibleIndex = 4;
            this.CHON.Width = 100;
            // 
            // MAHINH
            // 
            this.MAHINH.Caption = "gridColumn1";
            this.MAHINH.FieldName = "MAHINH";
            this.MAHINH.Name = "MAHINH";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.panelControl6);
            this.panelControl3.Controls.Add(this.btnbochonvideo);
            this.panelControl3.Controls.Add(this.btnChonTatCaVideo);
            this.panelControl3.Controls.Add(this.btn_XoaVideo);
            this.panelControl3.Controls.Add(this.btn_ThemVideo);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl3.Location = new System.Drawing.Point(2, 264);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(695, 38);
            this.panelControl3.TabIndex = 60;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.chk_chooseVideo);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl6.Location = new System.Drawing.Point(518, 2);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(127, 34);
            this.panelControl6.TabIndex = 55;
            this.panelControl6.Visible = false;
            // 
            // chk_chooseVideo
            // 
            this.chk_chooseVideo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk_chooseVideo.Location = new System.Drawing.Point(2, 2);
            this.chk_chooseVideo.Name = "chk_chooseVideo";
            this.chk_chooseVideo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk_chooseVideo.Properties.Appearance.Options.UseFont = true;
            this.chk_chooseVideo.Properties.Caption = "Chọn Tất Cả";
            this.chk_chooseVideo.Size = new System.Drawing.Size(123, 24);
            this.chk_chooseVideo.TabIndex = 0;
            this.chk_chooseVideo.CheckedChanged += new System.EventHandler(this.chk_chooseVideo_CheckedChanged);
            // 
            // btnbochonvideo
            // 
            this.btnbochonvideo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbochonvideo.Appearance.Options.UseFont = true;
            this.btnbochonvideo.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnbochonvideo.Image = global::KP_RES.Properties.Resources.restart_26;
            this.btnbochonvideo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnbochonvideo.Location = new System.Drawing.Point(389, 2);
            this.btnbochonvideo.Margin = new System.Windows.Forms.Padding(4);
            this.btnbochonvideo.Name = "btnbochonvideo";
            this.btnbochonvideo.Size = new System.Drawing.Size(129, 34);
            this.btnbochonvideo.TabIndex = 56;
            this.btnbochonvideo.Text = "Bỏ Chọn";
            this.btnbochonvideo.Click += new System.EventHandler(this.btnbochonvideo_Click);
            // 
            // btnChonTatCaVideo
            // 
            this.btnChonTatCaVideo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonTatCaVideo.Appearance.Options.UseFont = true;
            this.btnChonTatCaVideo.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnChonTatCaVideo.Image = ((System.Drawing.Image)(resources.GetObject("btnChonTatCaVideo.Image")));
            this.btnChonTatCaVideo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnChonTatCaVideo.Location = new System.Drawing.Point(260, 2);
            this.btnChonTatCaVideo.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonTatCaVideo.Name = "btnChonTatCaVideo";
            this.btnChonTatCaVideo.Size = new System.Drawing.Size(129, 34);
            this.btnChonTatCaVideo.TabIndex = 58;
            this.btnChonTatCaVideo.Text = "Chọn tất cả";
            this.btnChonTatCaVideo.Click += new System.EventHandler(this.btnChonTatCaVideo_Click);
            // 
            // btn_XoaVideo
            // 
            this.btn_XoaVideo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_XoaVideo.Appearance.Options.UseFont = true;
            this.btn_XoaVideo.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_XoaVideo.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btn_XoaVideo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_XoaVideo.Location = new System.Drawing.Point(131, 2);
            this.btn_XoaVideo.Margin = new System.Windows.Forms.Padding(4);
            this.btn_XoaVideo.Name = "btn_XoaVideo";
            this.btn_XoaVideo.Size = new System.Drawing.Size(129, 34);
            this.btn_XoaVideo.TabIndex = 54;
            this.btn_XoaVideo.Text = "Xóa Video";
            this.btn_XoaVideo.Click += new System.EventHandler(this.btn_XoaVideo_Click);
            // 
            // btn_ThemVideo
            // 
            this.btn_ThemVideo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ThemVideo.Appearance.Options.UseFont = true;
            this.btn_ThemVideo.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_ThemVideo.Image = global::KP_RES.Properties.Resources.slr_camera_26;
            this.btn_ThemVideo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_ThemVideo.Location = new System.Drawing.Point(2, 2);
            this.btn_ThemVideo.Margin = new System.Windows.Forms.Padding(4);
            this.btn_ThemVideo.Name = "btn_ThemVideo";
            this.btn_ThemVideo.Size = new System.Drawing.Size(129, 34);
            this.btn_ThemVideo.TabIndex = 52;
            this.btn_ThemVideo.Text = "Thêm Video";
            this.btn_ThemVideo.Click += new System.EventHandler(this.btn_ThemVideo_Click);
            // 
            // pn_CuaHang
            // 
            this.pn_CuaHang.Controls.Add(this.panel_Sandard);
            this.pn_CuaHang.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn_CuaHang.Location = new System.Drawing.Point(697, 2);
            this.pn_CuaHang.Name = "pn_CuaHang";
            this.pn_CuaHang.Size = new System.Drawing.Size(651, 300);
            this.pn_CuaHang.TabIndex = 3;
            // 
            // panel_Sandard
            // 
            this.panel_Sandard.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panel_Sandard.Controls.Add(this.pnQuangCao);
            this.panel_Sandard.Controls.Add(this.pal_BUTTON);
            this.panel_Sandard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Sandard.Location = new System.Drawing.Point(2, 2);
            this.panel_Sandard.Name = "panel_Sandard";
            this.panel_Sandard.Size = new System.Drawing.Size(647, 296);
            this.panel_Sandard.TabIndex = 3;
            // 
            // pnQuangCao
            // 
            this.pnQuangCao.Controls.Add(this.txt_SizeTenMon);
            this.pnQuangCao.Controls.Add(this.txt_SizeDonGia);
            this.pnQuangCao.Controls.Add(this.labelControl8);
            this.pnQuangCao.Controls.Add(this.labelControl7);
            this.pnQuangCao.Controls.Add(this.ck_6);
            this.pnQuangCao.Controls.Add(this.chk_ShowHide);
            this.pnQuangCao.Controls.Add(this.labelControl6);
            this.pnQuangCao.Controls.Add(this.font_chu);
            this.pnQuangCao.Controls.Add(this.ck_20);
            this.pnQuangCao.Controls.Add(this.ck_10);
            this.pnQuangCao.Controls.Add(this.labelControl4);
            this.pnQuangCao.Controls.Add(this.memoNoiDung);
            this.pnQuangCao.Controls.Add(this.labelControl5);
            this.pnQuangCao.Controls.Add(this.labelControl1);
            this.pnQuangCao.Controls.Add(this.txtThoiGianMenu);
            this.pnQuangCao.Controls.Add(this.labelControl3);
            this.pnQuangCao.Controls.Add(this.txtThoiGianHinh);
            this.pnQuangCao.Controls.Add(this.labelControl2);
            this.pnQuangCao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnQuangCao.Location = new System.Drawing.Point(0, 0);
            this.pnQuangCao.Name = "pnQuangCao";
            this.pnQuangCao.Size = new System.Drawing.Size(647, 261);
            this.pnQuangCao.TabIndex = 17;
            // 
            // txt_SizeTenMon
            // 
            this.txt_SizeTenMon.EnterMoveNextControl = true;
            this.txt_SizeTenMon.Location = new System.Drawing.Point(459, 37);
            this.txt_SizeTenMon.Name = "txt_SizeTenMon";
            this.txt_SizeTenMon.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SizeTenMon.Properties.Appearance.Options.UseFont = true;
            this.txt_SizeTenMon.Properties.Appearance.Options.UseTextOptions = true;
            this.txt_SizeTenMon.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txt_SizeTenMon.Properties.Mask.EditMask = "N0";
            this.txt_SizeTenMon.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt_SizeTenMon.Size = new System.Drawing.Size(182, 26);
            this.txt_SizeTenMon.TabIndex = 48;
            // 
            // txt_SizeDonGia
            // 
            this.txt_SizeDonGia.EnterMoveNextControl = true;
            this.txt_SizeDonGia.Location = new System.Drawing.Point(459, 5);
            this.txt_SizeDonGia.Name = "txt_SizeDonGia";
            this.txt_SizeDonGia.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SizeDonGia.Properties.Appearance.Options.UseFont = true;
            this.txt_SizeDonGia.Properties.Appearance.Options.UseTextOptions = true;
            this.txt_SizeDonGia.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txt_SizeDonGia.Properties.Mask.EditMask = "N0";
            this.txt_SizeDonGia.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt_SizeDonGia.Size = new System.Drawing.Size(182, 26);
            this.txt_SizeDonGia.TabIndex = 47;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl8.Location = new System.Drawing.Point(352, 40);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(107, 19);
            this.labelControl8.TabIndex = 46;
            this.labelControl8.Text = "Size Tên món";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl7.Location = new System.Drawing.Point(352, 8);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(107, 19);
            this.labelControl7.TabIndex = 45;
            this.labelControl7.Text = "Size Đơn giá";
            // 
            // ck_6
            // 
            this.ck_6.EditValue = true;
            this.ck_6.Location = new System.Drawing.Point(195, 185);
            this.ck_6.Name = "ck_6";
            this.ck_6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Document);
            this.ck_6.Properties.Appearance.Options.UseFont = true;
            this.ck_6.Properties.Caption = "6";
            this.ck_6.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.ck_6.Properties.RadioGroupIndex = 1;
            this.ck_6.Size = new System.Drawing.Size(41, 22);
            this.ck_6.TabIndex = 44;
            // 
            // chk_ShowHide
            // 
            this.chk_ShowHide.EditValue = true;
            this.chk_ShowHide.Location = new System.Drawing.Point(195, 221);
            this.chk_ShowHide.Name = "chk_ShowHide";
            this.chk_ShowHide.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk_ShowHide.Properties.Appearance.Options.UseFont = true;
            this.chk_ShowHide.Properties.Caption = "Hiện video";
            this.chk_ShowHide.Size = new System.Drawing.Size(123, 24);
            this.chk_ShowHide.TabIndex = 43;
            this.chk_ShowHide.CheckedChanged += new System.EventHandler(this.chk_ShowHide_CheckedChanged);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(4, 224);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(113, 19);
            this.labelControl6.TabIndex = 42;
            this.labelControl6.Text = "Ẩn / Hiện Video";
            // 
            // font_chu
            // 
            this.font_chu.Location = new System.Drawing.Point(197, 69);
            this.font_chu.Name = "font_chu";
            this.font_chu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.font_chu.Properties.Appearance.Options.UseFont = true;
            this.font_chu.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.font_chu.Properties.AppearanceDropDown.Options.UseFont = true;
            this.font_chu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.font_chu.Properties.Popup += new System.EventHandler(this.font_chu_Properties_Popup);
            this.font_chu.Size = new System.Drawing.Size(444, 26);
            this.font_chu.TabIndex = 41;
            this.font_chu.DrawItem += new DevExpress.XtraEditors.ListBoxDrawItemEventHandler(this.font_chu_DrawItem);
            // 
            // ck_20
            // 
            this.ck_20.Location = new System.Drawing.Point(331, 185);
            this.ck_20.Name = "ck_20";
            this.ck_20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Document);
            this.ck_20.Properties.Appearance.Options.UseFont = true;
            this.ck_20.Properties.Caption = "24";
            this.ck_20.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.ck_20.Properties.RadioGroupIndex = 1;
            this.ck_20.Size = new System.Drawing.Size(41, 22);
            this.ck_20.TabIndex = 40;
            this.ck_20.TabStop = false;
            // 
            // ck_10
            // 
            this.ck_10.Location = new System.Drawing.Point(260, 185);
            this.ck_10.Name = "ck_10";
            this.ck_10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Document);
            this.ck_10.Properties.Appearance.Options.UseFont = true;
            this.ck_10.Properties.Caption = "12";
            this.ck_10.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.ck_10.Properties.RadioGroupIndex = 1;
            this.ck_10.Size = new System.Drawing.Size(45, 22);
            this.ck_10.TabIndex = 39;
            this.ck_10.TabStop = false;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(4, 186);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(159, 19);
            this.labelControl4.TabIndex = 38;
            this.labelControl4.Text = "Số lượng món / Trang";
            // 
            // memoNoiDung
            // 
            this.memoNoiDung.Location = new System.Drawing.Point(197, 101);
            this.memoNoiDung.Name = "memoNoiDung";
            this.memoNoiDung.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.memoNoiDung.Properties.Appearance.Options.UseFont = true;
            this.memoNoiDung.Size = new System.Drawing.Size(444, 78);
            this.memoNoiDung.TabIndex = 37;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(4, 104);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(65, 19);
            this.labelControl5.TabIndex = 36;
            this.labelControl5.Text = "Nội dung";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(4, 72);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(62, 19);
            this.labelControl1.TabIndex = 35;
            this.labelControl1.Text = "Font chữ";
            // 
            // txtThoiGianMenu
            // 
            this.txtThoiGianMenu.EnterMoveNextControl = true;
            this.txtThoiGianMenu.Location = new System.Drawing.Point(197, 37);
            this.txtThoiGianMenu.Name = "txtThoiGianMenu";
            this.txtThoiGianMenu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThoiGianMenu.Properties.Appearance.Options.UseFont = true;
            this.txtThoiGianMenu.Properties.Appearance.Options.UseTextOptions = true;
            this.txtThoiGianMenu.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtThoiGianMenu.Properties.Mask.EditMask = "N0";
            this.txtThoiGianMenu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtThoiGianMenu.Size = new System.Drawing.Size(148, 26);
            this.txtThoiGianMenu.TabIndex = 28;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(4, 40);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(185, 19);
            this.labelControl3.TabIndex = 27;
            this.labelControl3.Text = "Thời gian đổi Menu (Giây)";
            // 
            // txtThoiGianHinh
            // 
            this.txtThoiGianHinh.EnterMoveNextControl = true;
            this.txtThoiGianHinh.Location = new System.Drawing.Point(197, 5);
            this.txtThoiGianHinh.Name = "txtThoiGianHinh";
            this.txtThoiGianHinh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThoiGianHinh.Properties.Appearance.Options.UseFont = true;
            this.txtThoiGianHinh.Properties.Appearance.Options.UseTextOptions = true;
            this.txtThoiGianHinh.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtThoiGianHinh.Properties.Mask.EditMask = "N0";
            this.txtThoiGianHinh.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtThoiGianHinh.Size = new System.Drawing.Size(148, 26);
            this.txtThoiGianHinh.TabIndex = 26;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl2.Location = new System.Drawing.Point(4, 8);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(185, 19);
            this.labelControl2.TabIndex = 25;
            this.labelControl2.Text = "Thời gian đổi hình (Giây)";
            // 
            // pal_BUTTON
            // 
            this.pal_BUTTON.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_BUTTON.Controls.Add(this.btnBanphim);
            this.pal_BUTTON.Controls.Add(this.btnRefesh);
            this.pal_BUTTON.Controls.Add(this.btnHuyBo);
            this.pal_BUTTON.Controls.Add(this.btnEdit);
            this.pal_BUTTON.Controls.Add(this.btnLuu);
            this.pal_BUTTON.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pal_BUTTON.Location = new System.Drawing.Point(0, 261);
            this.pal_BUTTON.Name = "pal_BUTTON";
            this.pal_BUTTON.Size = new System.Drawing.Size(647, 35);
            this.pal_BUTTON.TabIndex = 3;
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnBanphim.Appearance.Options.UseBackColor = true;
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Appearance.Options.UseForeColor = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(516, 0);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(4);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(129, 35);
            this.btnBanphim.TabIndex = 32;
            this.btnBanphim.Text = "7";
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnRefesh
            // 
            this.btnRefesh.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnRefesh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefesh.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnRefesh.Appearance.Options.UseBackColor = true;
            this.btnRefesh.Appearance.Options.UseFont = true;
            this.btnRefesh.Appearance.Options.UseForeColor = true;
            this.btnRefesh.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnRefesh.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnRefesh.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnRefesh.Location = new System.Drawing.Point(387, 0);
            this.btnRefesh.Margin = new System.Windows.Forms.Padding(4);
            this.btnRefesh.Name = "btnRefesh";
            this.btnRefesh.Size = new System.Drawing.Size(129, 35);
            this.btnRefesh.TabIndex = 33;
            this.btnRefesh.Click += new System.EventHandler(this.btnRefesh_Click);
            // 
            // btnHuyBo
            // 
            this.btnHuyBo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuyBo.Appearance.Options.UseFont = true;
            this.btnHuyBo.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnHuyBo.Image = ((System.Drawing.Image)(resources.GetObject("btnHuyBo.Image")));
            this.btnHuyBo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnHuyBo.Location = new System.Drawing.Point(258, 0);
            this.btnHuyBo.Margin = new System.Windows.Forms.Padding(4);
            this.btnHuyBo.Name = "btnHuyBo";
            this.btnHuyBo.Size = new System.Drawing.Size(129, 35);
            this.btnHuyBo.TabIndex = 32;
            this.btnHuyBo.Text = "&Hủy Bỏ";
            this.btnHuyBo.Click += new System.EventHandler(this.btnHuyBo_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Appearance.Options.UseFont = true;
            this.btnEdit.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(129, 0);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(4);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(129, 35);
            this.btnEdit.TabIndex = 34;
            this.btnEdit.Text = "&Sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnLuu.Image = ((System.Drawing.Image)(resources.GetObject("btnLuu.Image")));
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(0, 0);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(129, 35);
            this.btnLuu.TabIndex = 31;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // Frm_MenuDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 615);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_MenuDisplay";
            this.Text = "Cài đặt màn hình Menu Display";
            this.SizeChanged += new System.EventHandler(this.Frm_QuangCao_Nhahang_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_Hinh)).EndInit();
            this.pn_Hinh.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_xemvideo)).EndInit();
            this.pn_xemvideo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WindowsMediaPlayer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_cuon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pal_xuong)).EndInit();
            this.pal_xuong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnCheckAll)).EndInit();
            this.pnCheckAll.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk_ChooseAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_CaiDat_Video)).EndInit();
            this.pn_CaiDat_Video.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnt_Play)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_stop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk_chooseVideo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_CuaHang)).EndInit();
            this.pn_CuaHang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panel_Sandard)).EndInit();
            this.panel_Sandard.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnQuangCao)).EndInit();
            this.pnQuangCao.ResumeLayout(false);
            this.pnQuangCao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_SizeTenMon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_SizeDonGia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck_6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_ShowHide.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.font_chu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck_20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck_10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoNoiDung.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThoiGianMenu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThoiGianHinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pal_BUTTON)).EndInit();
            this.pal_BUTTON.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl pn_CuaHang;
        private DevExpress.XtraEditors.PanelControl panel_Sandard;
        private DevExpress.XtraEditors.PanelControl pal_xuong;
        private DevExpress.XtraEditors.SimpleButton bnt_tranglen;
        private DevExpress.XtraEditors.SimpleButton bnt_trangxuong;
        private DevExpress.XtraEditors.PanelControl pn_Hinh;
        private DevExpress.XtraEditors.SimpleButton btn_Them;
        private DevExpress.XtraEditors.PanelControl pn_cuon;
        private DevExpress.XtraEditors.PanelControl pn_CaiDat_Video;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btn_XoaVideo;
        private DevExpress.XtraEditors.SimpleButton btn_ThemVideo;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn TENHINH;
        private DevExpress.XtraGrid.Columns.GridColumn CHON;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn MAHINH;
        private DevExpress.XtraEditors.SimpleButton btn_Xoa;
        private DevExpress.XtraEditors.PanelControl pnCheckAll;
        private DevExpress.XtraEditors.CheckEdit chk_ChooseAll;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.CheckEdit chk_chooseVideo;
        private DevExpress.XtraEditors.SimpleButton btn_bochonhinh;
        private DevExpress.XtraEditors.SimpleButton btnbochonvideo;
        private DevExpress.XtraEditors.PanelControl pn_xemvideo;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit bnt_Play;
        private AxWMPLib.AxWindowsMediaPlayer WindowsMediaPlayer;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn_stop;
        private DevExpress.XtraEditors.PanelControl pal_BUTTON;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnRefesh;
        private DevExpress.XtraEditors.PanelControl pnQuangCao;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnHuyBo;
        private DevExpress.XtraEditors.TextEdit txtThoiGianMenu;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtThoiGianHinh;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.MemoEdit memoNoiDung;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton lbl_sotrang;
        private DevExpress.XtraEditors.FontEdit font_chu;
        private DevExpress.XtraEditors.CheckEdit ck_20;
        private DevExpress.XtraEditors.CheckEdit ck_10;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton btnChonTatCaHinh;
        private DevExpress.XtraEditors.SimpleButton btnChonTatCaVideo;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.CheckEdit chk_ShowHide;
        private DevExpress.XtraEditors.CheckEdit ck_6;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txt_SizeTenMon;
        private DevExpress.XtraEditors.TextEdit txt_SizeDonGia;




    }
}