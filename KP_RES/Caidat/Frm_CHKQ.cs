﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_CHKQ : DevExpress.XtraEditors.XtraForm
    {
        public Frm_CHKQ()
        {
            InitializeComponent();
            this.Text = rm.GetString("cuahangkhoquay", culture);
            LoadCombo();
            LoaddataGridView();
            LoadPermission();
        }

        string smaCuaHang = "";
        string smaKho = "";
        string smaQuay = "";

        private void Frm_CHKQ_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtTENCUAHANG.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (smaCuaHang == "" && smaKho == "" && smaQuay == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show( _xacnhanbanmuonxoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From CUAHANG" + "\n";
            sSQL += "Where MA_CUAHANG=" + clsMain.SQLString(smaCuaHang) + "\n";
            sSQL += "Delete From KHO" + "\n";
            sSQL += "Where MA_KHO=" + clsMain.SQLString(smaKho) + "\n";
            sSQL += "Delete From QUAY" + "\n";
            sSQL += "Where MA_QUAY=" + clsMain.SQLString(smaQuay) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoadCombo();
                LoaddataGridView();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (smaCuaHang == "" && smaKho == "" && smaQuay == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl(false);
            if (smaCuaHang != "")
            {
                txtTENCUAHANG.Focus();
            }
            else if (smaKho != "")
            {
                cboCUAHANG.Focus();
            }
            else if (smaQuay != "")
            {
                cboKHO.Focus();
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            string sSQL = "";
            if (txtTENCUAHANG.Text != "")
            {
                if (smaCuaHang == "" && btnThem.Enabled)
                {
                    sSQL += "Insert into CUAHANG (TEN_CUAHANG,GHICHU,SUDUNG)" + "\n";
                    sSQL += "Values ( ";
                    sSQL += clsMain.SQLStringUnicode(txtTENCUAHANG.Text) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtGHICHUCUAHANG.Text) + ",";
                    sSQL += clsMain.SQLBit(chkSUDUNGCUAHANG.Checked) + ")";
                }
                else if ((smaCuaHang != "" && btnSua.Enabled))
                {
                    sSQL += "Update CUAHANG Set " + "\n";
                    sSQL += "TEN_CUAHANG=" + clsMain.SQLStringUnicode(txtTENCUAHANG.Text) + "," + "\n";
                    sSQL += "GHICHU=" + clsMain.SQLStringUnicode(txtGHICHUCUAHANG.Text) + "," + "\n";
                    sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNGCUAHANG.Checked) + "\n";
                    sSQL += "Where MA_CUAHANG=" + clsMain.SQLString(smaCuaHang) + "\n";
                }
            }
            if (txtTENKHO.Text != "")
            {
                if (smaKho == "" && btnThem.Enabled)
                {
                    sSQL += "Insert into KHO (MA_CUAHANG,TEN_KHO,GHICHU,SUDUNG)" + "\n";
                    sSQL += "Values ( ";
                    sSQL += clsMain.SQLString(cboCUAHANG.EditValue.ToString()) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtTENKHO.Text) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtGHICHUKHO.Text) + ",";
                    sSQL += clsMain.SQLBit(chkSUDUNGKHO.Checked) + ")";
                }
                else if ((smaKho != "" && btnSua.Enabled))
                {
                    sSQL += "Update KHO Set " + "\n";
                    sSQL += "MA_CUAHANG=" + clsMain.SQLString(cboCUAHANG.EditValue.ToString()) + "," + "\n";
                    sSQL += "TEN_KHO=" + clsMain.SQLStringUnicode(txtTENKHO.Text) + "," + "\n";
                    sSQL += "GHICHU=" + clsMain.SQLStringUnicode(txtGHICHUKHO.Text) + "," + "\n";
                    sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNGKHO.Checked) + "\n";
                    sSQL += "Where MA_KHO=" + clsMain.SQLString(smaKho) + "\n";
                }
            }
            if (txtTENQUAY.Text != "")
            {
                if (smaQuay == "" && btnThem.Enabled)
                {
                    sSQL += "Insert into QUAY (MA_KHO,TEN_QUAY,GHICHU,SUDUNG)" + "\n";
                    sSQL += "Values ( ";
                    sSQL += clsMain.SQLString(cboKHO.EditValue.ToString()) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtTENQUAY.Text) + ",";
                    sSQL += clsMain.SQLStringUnicode(txtGHICHUQUAY.Text) + ",";
                    sSQL += clsMain.SQLBit(chkSUDUNGQUAY.Checked) + ")";
                }
                else if ((smaQuay != "" && btnSua.Enabled))
                {
                    sSQL += "Update QUAY Set " + "\n";
                    sSQL += "MA_KHO=" + clsMain.SQLString(cboKHO.EditValue.ToString()) + "," + "\n";
                    sSQL += "TEN_QUAY=" + clsMain.SQLStringUnicode(txtTENQUAY.Text) + "," + "\n";
                    sSQL += "GHICHU=" + clsMain.SQLStringUnicode(txtGHICHUQUAY.Text) + "," + "\n";
                    sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNGQUAY.Checked) + "\n";
                    sSQL += "Where MA_QUAY=" + clsMain.SQLString(smaQuay) + "\n";
                }
            }


            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
                LoadCombo();
                btnThem.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Cuahang_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pnlCuahang.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }
            pnlKho.Controls.Clear();
            pnlQuay.Controls.Clear();
            pnlKho.Padding = new Padding(btn .Location .X-3 , 0, 0, 0);
           
            LoaddataGridView1(btn.Name.Substring(1, btn.Name.Length - 1));

            smaCuaHang = btn.Name.Substring(1, btn.Name.Length - 1);
            txtTENCUAHANG.Text = dtCuahang.Select("MA=" + clsMain.SQLString(smaCuaHang))[0]["TEN"].ToString();
            txtGHICHUCUAHANG.Text = dtCuahang.Select("MA=" + clsMain.SQLString(smaCuaHang))[0]["GHICHU"].ToString();
            chkSUDUNGCUAHANG.Checked =bool .Parse(dtCuahang.Select("MA=" + clsMain.SQLString(smaCuaHang))[0]["SUDUNG"].ToString());

            smaKho = "";
            smaQuay = "";
            cboCUAHANG.EditValue = cboCUAHANG.Properties.GetDataSourceValue(cboCUAHANG.Properties.ValueMember, 0);
            cboKHO.EditValue = cboKHO.Properties.GetDataSourceValue(cboKHO.Properties.ValueMember, 0);
            txtTENKHO.Text = "";
            txtTENQUAY.Text = "";
            txtGHICHUQUAY.Text = "";
            txtGHICHUKHO.Text = "";
            chkSUDUNGKHO.Checked = true;
            chkSUDUNGQUAY.Checked = true;

        }

        private void Kho_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pnlKho.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }
            pnlQuay.Controls.Clear();
            pnlQuay.Padding = new Padding(btn.Location.X - 3, 0, 0, 0);

            LoaddataGridView2(btn.Name.Substring(1, btn.Name.Length - 1));

            smaKho = btn.Name.Substring(1, btn.Name.Length - 1);
            cboCUAHANG.EditValue = int.Parse (dtKHO.Select("MA=" + clsMain.SQLString(smaKho))[0]["MA_CUAHANG"].ToString());
            txtTENKHO.Text = dtKHO.Select("MA=" + clsMain.SQLString(smaKho))[0]["TEN"].ToString();
            txtGHICHUKHO.Text = dtKHO.Select("MA=" + clsMain.SQLString(smaKho))[0]["GHICHU"].ToString();
            chkSUDUNGKHO.Checked = bool.Parse(dtKHO.Select("MA=" + clsMain.SQLString(smaKho))[0]["SUDUNG"].ToString());

            smaCuaHang = "";
            smaQuay = "";
            txtTENCUAHANG.Text = "";
            txtGHICHUCUAHANG.Text = "";
            cboKHO.EditValue = cboKHO.Properties.GetDataSourceValue(cboKHO.Properties.ValueMember, 0);
            txtTENQUAY.Text = "";
            txtGHICHUQUAY.Text = "";
            chkSUDUNGCUAHANG.Checked = true;
            chkSUDUNGQUAY.Checked = true;
        }

        private void Quay_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pnlQuay.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }
         
            smaQuay = btn.Name.Substring(1, btn.Name.Length - 1);
            cboKHO.EditValue = int.Parse(dtQUAY.Select("MA=" + clsMain.SQLString(smaQuay))[0]["MA_KHO"].ToString());
            txtTENQUAY.Text = dtQUAY.Select("MA=" + clsMain.SQLString(smaQuay))[0]["TEN"].ToString();
            txtGHICHUQUAY.Text = dtQUAY.Select("MA=" + clsMain.SQLString(smaQuay))[0]["GHICHU"].ToString();
            chkSUDUNGQUAY.Checked = bool.Parse(dtQUAY.Select("MA=" + clsMain.SQLString(smaQuay))[0]["SUDUNG"].ToString());

            smaCuaHang = "";
            smaKho = "";
            txtTENCUAHANG.Text = "";
            txtGHICHUCUAHANG.Text = "";
            cboCUAHANG.EditValue = cboCUAHANG.Properties.GetDataSourceValue(cboCUAHANG.Properties.ValueMember, 0);
            txtTENKHO.Text = "";
            txtGHICHUKHO.Text = "";
            chkSUDUNGCUAHANG.Checked = true;
            chkSUDUNGKHO.Checked = true;
        }

        DataTable dtCuahang = new DataTable();
        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);

            string sSQL = "";
            sSQL += "Select MA_CUAHANG As MA,TEN_CUAHANG As TEN,GHICHU,SUDUNG" + "\n";
            sSQL += "From CUAHANG" + "\n";
            sSQL += "Order by SUDUNG DESC, TEN_CUAHANG" + "\n";
            dtCuahang = clsMain.ReturnDataTable(sSQL);
            pnlCuahang.Controls.Clear();
            pnlKho.Controls.Clear();
            pnlQuay.Controls.Clear();
            foreach (DataRow dr in dtCuahang.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 125;
                btn.Height = 80;
                btn.Font = pnlCuahang.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Image = global::KP_RES.Properties.Resources.geo_fence_32;
                btn.Name = "C" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(Cuahang_Click);
                pnlCuahang.Controls.Add(btn);
            }
        }

        DataTable dtKHO = new DataTable();
        private void LoaddataGridView1(string macuahang)
        {
            string sSQL = "";
            sSQL += "Select MA_CUAHANG,MA_KHO As MA,TEN_KHO As TEN,GHICHU,SUDUNG" + "\n";
            sSQL += "From KHO" + "\n";
            sSQL += "Where MA_CUAHANG="+ clsMain .SQLString (macuahang) + "\n";
            sSQL += "Order by SUDUNG DESC, TEN_KHO" + "\n";
            dtKHO = clsMain.ReturnDataTable(sSQL);
            pnlKho.Controls.Clear();
            foreach (DataRow dr in dtKHO.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 125;
                btn.Height = 80;
                btn.Font = pnlKho.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Image = global::KP_RES.Properties.Resources.tent_32;
                btn.Name = "K" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(Kho_Click);
                pnlKho.Controls.Add(btn);
            }
        }

        DataTable dtQUAY = new DataTable();
        private void LoaddataGridView2(string makho)
        {
            string sSQL = "";
            sSQL += "Select MA_KHO,MA_QUAY As MA,TEN_QUAY As TEN,GHICHU,SUDUNG" + "\n";
            sSQL += "From QUAY" + "\n";
            sSQL += "Where MA_KHO=" + clsMain.SQLString(makho) + "\n";
            sSQL += "Order by  SUDUNG DESC,TEN_QUAY" + "\n";
            dtQUAY = clsMain.ReturnDataTable(sSQL);
            pnlQuay.Controls.Clear();
            foreach (DataRow dr in dtQUAY.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 125;
                btn.Height = 80;
                btn.Font = pnlQuay.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Image = global::KP_RES.Properties.Resources.calculator_32;
                btn.Name = "Q" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(Quay_Click);
                pnlQuay.Controls.Add(btn);
            }
        }

        private void LoadCombo()
        {
            string sSQL = "";
            sSQL += "Select MA_CUAHANG As MA,TEN_CUAHANG As TEN" + "\n";
            sSQL += "From CUAHANG" + "\n";
            sSQL += "Where SUDUNG=1" + "\n";
            sSQL += "Order by  TEN_CUAHANG" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            cboCUAHANG.Properties.DataSource = dt;
            cboCUAHANG.EditValue = cboCUAHANG.Properties.GetDataSourceValue(cboCUAHANG.Properties.ValueMember, 0);

            sSQL = "";
            sSQL += "Select MA_KHO As MA,TEN_KHO As TEN" + "\n";
            sSQL += "From KHO" + "\n";
            sSQL += "Where SUDUNG=1" + "\n";
            sSQL += "Order by  TEN_KHO" + "\n";
            dt = clsMain.ReturnDataTable(sSQL);
            cboKHO.Properties.DataSource = dt;
            cboKHO.EditValue = cboKHO.Properties.GetDataSourceValue(cboKHO.Properties.ValueMember, 0);
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("4");
            btnSua.Enabled = clsUserManagement.AllowEdit("4");
            btnXoa.Enabled = clsUserManagement.AllowDelete("4");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
            btnTieude.Enabled = btnSua.Enabled;
        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTENCUAHANG.Properties.ReadOnly = true;
                txtGHICHUCUAHANG.Properties.ReadOnly = true;
                cboCUAHANG.Properties.ReadOnly = true;
                cboKHO.Properties.ReadOnly = true;
                txtTENKHO.Properties.ReadOnly = true;
                txtTENQUAY.Properties.ReadOnly = true;
                txtGHICHUQUAY.Properties.ReadOnly = true;
                txtGHICHUKHO.Properties.ReadOnly = true;
                chkSUDUNGCUAHANG.Properties.ReadOnly = true;
                chkSUDUNGKHO.Properties.ReadOnly = true;
                chkSUDUNGQUAY.Properties.ReadOnly = true;
            }
            else//mở
            {
                txtTENCUAHANG.Properties.ReadOnly = false;
                txtGHICHUCUAHANG.Properties.ReadOnly = false;
                cboCUAHANG.Properties.ReadOnly = false;
                cboKHO.Properties.ReadOnly = false;
                txtTENKHO.Properties.ReadOnly = false;
                txtTENQUAY.Properties.ReadOnly = false;
                txtGHICHUQUAY.Properties.ReadOnly = false;
                txtGHICHUKHO.Properties.ReadOnly = false;
                chkSUDUNGCUAHANG.Properties.ReadOnly = false;
                chkSUDUNGKHO.Properties.ReadOnly = false;
                chkSUDUNGQUAY.Properties.ReadOnly = false;
            }
        }

        private void SetRongControl()
        {
            smaCuaHang = "";
            smaKho = "";
            smaQuay = "";
            txtTENCUAHANG.Text = "";
            txtGHICHUCUAHANG.Text = "";
            cboCUAHANG.EditValue = cboCUAHANG.Properties.GetDataSourceValue(cboCUAHANG.Properties.ValueMember, 0);
            cboKHO.EditValue = cboKHO.Properties.GetDataSourceValue(cboKHO.Properties.ValueMember, 0);
            txtTENKHO.Text = "";
            txtTENQUAY.Text = "";
            txtGHICHUQUAY.Text = "";
            txtGHICHUKHO.Text = "";
            chkSUDUNGCUAHANG.Checked = true;
            chkSUDUNGKHO.Checked = true;
            chkSUDUNGQUAY.Checked = true;
        }

        private Boolean CheckInput()
        {
            if (txtTENCUAHANG.Text == "" && txtTENKHO.Text == "" && txtTENQUAY.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_chuanhapttdaydu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTENCUAHANG.Focus();
                return false;
            }
            return true;
        }

        private void btnTieude_Click(object sender, EventArgs e)
        {
            if (smaCuaHang == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            Frm_Tieudecuahang frm = new Frm_Tieudecuahang();
            frm.sMacuahang = smaCuaHang;
            frm.ShowDialog();
            frm.Dispose ();
        }

        string _chuanhapttdaydu = "";
        string _thongbao = "";
        string _luukothanhcong = "";
        string _xacnhanbanmuonxoa = "";
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void Frm_CHKQ_Load(object sender, EventArgs e)
        {
            this.Text = rm.GetString("cuahangkhoquay", culture);
            lblTEN.Text = rm.GetString("cuahang", culture);
            lblTenCH.Text = rm.GetString("cuahang", culture);
            lbGhiChu.Text = rm.GetString("ghichu", culture);
            lbGhiChu2.Text = rm.GetString("ghichu", culture);
            lbGhiChu3.Text = rm.GetString("ghichu", culture);
            lbKho.Text = rm.GetString("kho", culture);
            lbKho2.Text = rm.GetString("kho", culture);
            lbQuay.Text = rm.GetString("quay", culture);
            chkSUDUNGCUAHANG.Text = rm.GetString("sudung", culture);
            chkSUDUNGKHO.Text = rm.GetString("sudung", culture);
            chkSUDUNGQUAY.Text = rm.GetString("sudung", culture);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
            
            _thongbao = rm.GetString("thongbao", culture);
            _chuanhapttdaydu = rm.GetString("chuanhapttdaydu", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);
            _xacnhanbanmuonxoa = rm.GetString("xacnhanbanmuonxoa", culture);

        }

        private void btnTieudeVe_Click(object sender, EventArgs e)
        {

            if (smaCuaHang == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            Frm_Tieudeve frm = new Frm_Tieudeve();
            frm.sMacuahang = smaCuaHang;
            frm.ShowDialog();
            frm.Dispose();
        }
    }
}