﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Yeucauthem : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Yeucauthem()
        {
            InitializeComponent();
            this.Text = rm.GetString("yeucauthem", culture);
            LoaddataGridView();
            LoadPermission();
        }

        private void Frm_Yeucauthem_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        string sma = "";

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtTEN.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show(_xacnhanbanmuonxoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From YEUCAUTHEM" + "\n";
            sSQL += "Where ID=" + clsMain.SQLString(sma) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong,_thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl(false);
            txtTEN.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            string sSQL = "";
            if (sma == "" && btnThem.Enabled)
            {
                sSQL += "Insert into YEUCAUTHEM (YEUCAU)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLStringUnicode(txtTEN.Text) +")";
            }
            else if ((sma != "" && btnSua.Enabled))
            {
                sSQL += "Update YEUCAUTHEM Set " + "\n";
                sSQL += "YEUCAU=" + clsMain.SQLStringUnicode(txtTEN.Text)  + "\n";
                sSQL += "Where ID=" + clsMain.SQLString(sma) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void HangHoa_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in fpnlYeucauthem.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }
            sma = btn.Name.Substring(1, btn.Name.Length - 1);
            txtTEN.Text = btn.Text;
            KhoaMoControl(true);
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
          
            string sSQL = "";
            sSQL += "Select ID As MA,YEUCAU As TEN" + "\n";
            sSQL += "From YEUCAUTHEM" + "\n";
            sSQL += "Order by YEUCAU" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            fpnlYeucauthem.Controls.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 110;
                btn.Height = 80;
                btn.Font = fpnlYeucauthem.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Image = global::KP_RES.Properties.Resources.vegetarian_food_26;
                btn.Name = "H" + dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString() ;
                if (btn.Text.Length > 20)
                {
                    btn.Width = 110 * 2;
                }
                btn.Click += new EventHandler(HangHoa_Click);
                fpnlYeucauthem.Controls.Add(btn);
            }
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("40");
            btnSua.Enabled = clsUserManagement.AllowEdit("40");
            btnXoa.Enabled = clsUserManagement.AllowDelete("40");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTEN.Properties.ReadOnly = true;
            }
            else//mở
            {
                txtTEN.Properties.ReadOnly = false;
            }
        }

        private void SetRongControl()
        {
            sma = "";
            txtTEN.Text = "";
        }

        private Boolean CheckInput()
        {
            if (txtTEN.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_nhap + lblTEN.Text,_thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTEN.Focus();
                return false;
            }
            return true;
        }

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                txt.SelectAll();
            }
            catch
            {
            }
        }


        string _nhap = "";
        string _thongbao = "";
        string _luukothanhcong = "";
        string _xacnhanbanmuonxoa = "";
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void Frm_Yeucauthem_Load(object sender, EventArgs e)
        {
            foreach (Control ctl in panelControl1.Controls)
            {
                ctl.Click += new EventHandler(ctl_Click);
            }

           
            this.Text = rm.GetString("yeucauthem", culture);
            lblTEN.Text = rm.GetString("ten", culture);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _nhap = rm.GetString("nhap", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);
            _xacnhanbanmuonxoa = rm.GetString("xacnhanbanmuonxoa", culture);
        }
    }
}