﻿namespace KP_RES 
{
    partial class Frm_NCCKH 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.chkTienDatCoc = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtTienHanMuc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtHanThanhToan = new DevExpress.XtraEditors.TextEdit();
            this.cboCapDoDaiLy = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.cboLoaiDaiLy = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.btnCongNo = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtMA = new DevExpress.XtraEditors.TextEdit();
            this.cboGioitinh = new DevExpress.XtraEditors.LookUpEdit();
            this.lbGioiTinh = new DevExpress.XtraEditors.LabelControl();
            this.lbNgaySinh = new DevExpress.XtraEditors.LabelControl();
            this.dtpNgaysinh = new DevExpress.XtraEditors.DateEdit();
            this.lb_CMND = new DevExpress.XtraEditors.LabelControl();
            this.txtCMND = new DevExpress.XtraEditors.TextEdit();
            this.cboLoai = new DevExpress.XtraEditors.LookUpEdit();
            this.lbNCCKH = new DevExpress.XtraEditors.LabelControl();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.lbNguoiLienHe = new DevExpress.XtraEditors.LabelControl();
            this.txtNGUOILIENHE = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtEMAIL = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtWEBSITE = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtFAX = new DevExpress.XtraEditors.TextEdit();
            this.lbDienThoai = new DevExpress.XtraEditors.LabelControl();
            this.txtDIENTHOAI = new DevExpress.XtraEditors.TextEdit();
            this.lbDiaChi = new DevExpress.XtraEditors.LabelControl();
            this.txtDIACHI = new DevExpress.XtraEditors.TextEdit();
            this.chkSUDUNG = new DevExpress.XtraEditors.CheckEdit();
            this.lblGHICHU = new DevExpress.XtraEditors.LabelControl();
            this.txtGHICHU = new DevExpress.XtraEditors.TextEdit();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.txtTEN = new DevExpress.XtraEditors.TextEdit();
            this.txtDatCoc = new DevExpress.XtraEditors.TextEdit();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENLOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIACHI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DIENTHOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FAX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EMAIL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGUOILIENHE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGAYSINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MADAILY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LOAIDAILY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CAPDAILY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CHIETKHAU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HANMUC_CONGNO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HANTHANHTOAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ISDATCOC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIENDATCOC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SUDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WEBSITE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LOAI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTienDatCoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienHanMuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHanThanhToan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCapDoDaiLy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLoaiDaiLy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboGioitinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgaysinh.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgaysinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMND.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLoai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNGUOILIENHE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMAIL.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWEBSITE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFAX.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDIENTHOAI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDIACHI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHU.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatCoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.chkTienDatCoc);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.txtTienHanMuc);
            this.panelControl1.Controls.Add(this.labelControl10);
            this.panelControl1.Controls.Add(this.txtHanThanhToan);
            this.panelControl1.Controls.Add(this.cboCapDoDaiLy);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.cboLoaiDaiLy);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.btnCongNo);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.txtMA);
            this.panelControl1.Controls.Add(this.cboGioitinh);
            this.panelControl1.Controls.Add(this.lbGioiTinh);
            this.panelControl1.Controls.Add(this.lbNgaySinh);
            this.panelControl1.Controls.Add(this.dtpNgaysinh);
            this.panelControl1.Controls.Add(this.lb_CMND);
            this.panelControl1.Controls.Add(this.txtCMND);
            this.panelControl1.Controls.Add(this.cboLoai);
            this.panelControl1.Controls.Add(this.lbNCCKH);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnSua);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.lbNguoiLienHe);
            this.panelControl1.Controls.Add(this.txtNGUOILIENHE);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.txtEMAIL);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.txtWEBSITE);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.txtFAX);
            this.panelControl1.Controls.Add(this.lbDienThoai);
            this.panelControl1.Controls.Add(this.txtDIENTHOAI);
            this.panelControl1.Controls.Add(this.lbDiaChi);
            this.panelControl1.Controls.Add(this.txtDIACHI);
            this.panelControl1.Controls.Add(this.chkSUDUNG);
            this.panelControl1.Controls.Add(this.lblGHICHU);
            this.panelControl1.Controls.Add(this.txtGHICHU);
            this.panelControl1.Controls.Add(this.lblTEN);
            this.panelControl1.Controls.Add(this.txtTEN);
            this.panelControl1.Controls.Add(this.txtDatCoc);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1358, 270);
            this.panelControl1.TabIndex = 0;
            // 
            // chkTienDatCoc
            // 
            this.chkTienDatCoc.EnterMoveNextControl = true;
            this.chkTienDatCoc.Location = new System.Drawing.Point(712, 166);
            this.chkTienDatCoc.Name = "chkTienDatCoc";
            this.chkTienDatCoc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTienDatCoc.Properties.Appearance.Options.UseFont = true;
            this.chkTienDatCoc.Properties.Caption = "Đặt cọc";
            this.chkTienDatCoc.Size = new System.Drawing.Size(76, 24);
            this.chkTienDatCoc.TabIndex = 34;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(9, 168);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(64, 19);
            this.labelControl9.TabIndex = 30;
            this.labelControl9.Text = "Hạn mức";
            // 
            // txtTienHanMuc
            // 
            this.txtTienHanMuc.EnterMoveNextControl = true;
            this.txtTienHanMuc.Location = new System.Drawing.Point(88, 164);
            this.txtTienHanMuc.Name = "txtTienHanMuc";
            this.txtTienHanMuc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienHanMuc.Properties.Appearance.Options.UseFont = true;
            this.txtTienHanMuc.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTienHanMuc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTienHanMuc.Properties.DisplayFormat.FormatString = "N0";
            this.txtTienHanMuc.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTienHanMuc.Properties.EditFormat.FormatString = "N0";
            this.txtTienHanMuc.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTienHanMuc.Properties.Mask.EditMask = "N0";
            this.txtTienHanMuc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienHanMuc.Size = new System.Drawing.Size(248, 26);
            this.txtTienHanMuc.TabIndex = 31;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(343, 167);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(109, 19);
            this.labelControl10.TabIndex = 32;
            this.labelControl10.Text = "Hạn thanh toán";
            // 
            // txtHanThanhToan
            // 
            this.txtHanThanhToan.EnterMoveNextControl = true;
            this.txtHanThanhToan.Location = new System.Drawing.Point(459, 164);
            this.txtHanThanhToan.Name = "txtHanThanhToan";
            this.txtHanThanhToan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHanThanhToan.Properties.Appearance.Options.UseFont = true;
            this.txtHanThanhToan.Properties.Appearance.Options.UseTextOptions = true;
            this.txtHanThanhToan.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtHanThanhToan.Properties.DisplayFormat.FormatString = "N0";
            this.txtHanThanhToan.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtHanThanhToan.Properties.EditFormat.FormatString = "N0";
            this.txtHanThanhToan.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtHanThanhToan.Properties.Mask.EditMask = "N0";
            this.txtHanThanhToan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtHanThanhToan.Size = new System.Drawing.Size(248, 26);
            this.txtHanThanhToan.TabIndex = 33;
            // 
            // cboCapDoDaiLy
            // 
            this.cboCapDoDaiLy.EnterMoveNextControl = true;
            this.cboCapDoDaiLy.Location = new System.Drawing.Point(791, 132);
            this.cboCapDoDaiLy.Name = "cboCapDoDaiLy";
            this.cboCapDoDaiLy.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCapDoDaiLy.Properties.Appearance.Options.UseFont = true;
            this.cboCapDoDaiLy.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCapDoDaiLy.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboCapDoDaiLy.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboCapDoDaiLy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboCapDoDaiLy.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboCapDoDaiLy.Properties.DisplayMember = "TEN";
            this.cboCapDoDaiLy.Properties.DropDownItemHeight = 40;
            this.cboCapDoDaiLy.Properties.NullText = "";
            this.cboCapDoDaiLy.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboCapDoDaiLy.Properties.ShowHeader = false;
            this.cboCapDoDaiLy.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboCapDoDaiLy.Properties.ValueMember = "MA";
            this.cboCapDoDaiLy.Size = new System.Drawing.Size(248, 26);
            this.cboCapDoDaiLy.TabIndex = 29;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(714, 135);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(70, 19);
            this.labelControl2.TabIndex = 28;
            this.labelControl2.Text = "Cấp đại lý";
            // 
            // cboLoaiDaiLy
            // 
            this.cboLoaiDaiLy.EnterMoveNextControl = true;
            this.cboLoaiDaiLy.Location = new System.Drawing.Point(459, 132);
            this.cboLoaiDaiLy.Name = "cboLoaiDaiLy";
            this.cboLoaiDaiLy.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLoaiDaiLy.Properties.Appearance.Options.UseFont = true;
            this.cboLoaiDaiLy.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLoaiDaiLy.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboLoaiDaiLy.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboLoaiDaiLy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboLoaiDaiLy.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboLoaiDaiLy.Properties.DisplayMember = "TEN";
            this.cboLoaiDaiLy.Properties.DropDownItemHeight = 40;
            this.cboLoaiDaiLy.Properties.NullText = "";
            this.cboLoaiDaiLy.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboLoaiDaiLy.Properties.ShowHeader = false;
            this.cboLoaiDaiLy.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboLoaiDaiLy.Properties.ValueMember = "MA";
            this.cboLoaiDaiLy.Size = new System.Drawing.Size(248, 26);
            this.cboLoaiDaiLy.TabIndex = 27;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(343, 135);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(72, 19);
            this.labelControl6.TabIndex = 26;
            this.labelControl6.Text = "Loại đại lý";
            // 
            // btnCongNo
            // 
            this.btnCongNo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCongNo.Appearance.Options.UseFont = true;
            this.btnCongNo.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnCongNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnCongNo.Location = new System.Drawing.Point(445, 226);
            this.btnCongNo.Margin = new System.Windows.Forms.Padding(4);
            this.btnCongNo.Name = "btnCongNo";
            this.btnCongNo.Size = new System.Drawing.Size(107, 35);
            this.btnCongNo.TabIndex = 41;
            this.btnCongNo.Text = "&Công nợ";
            this.btnCongNo.Visible = false;
            this.btnCongNo.Click += new System.EventHandler(this.btnCongNo_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(9, 136);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(63, 19);
            this.labelControl1.TabIndex = 24;
            this.labelControl1.Text = "Mã đại lý";
            // 
            // txtMA
            // 
            this.txtMA.EnterMoveNextControl = true;
            this.txtMA.Location = new System.Drawing.Point(88, 132);
            this.txtMA.Name = "txtMA";
            this.txtMA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMA.Properties.Appearance.Options.UseFont = true;
            this.txtMA.Size = new System.Drawing.Size(248, 26);
            this.txtMA.TabIndex = 25;
            this.txtMA.Leave += new System.EventHandler(this.txtMA_Leave);
            // 
            // cboGioitinh
            // 
            this.cboGioitinh.EnterMoveNextControl = true;
            this.cboGioitinh.Location = new System.Drawing.Point(459, 100);
            this.cboGioitinh.Name = "cboGioitinh";
            this.cboGioitinh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboGioitinh.Properties.Appearance.Options.UseFont = true;
            this.cboGioitinh.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboGioitinh.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboGioitinh.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboGioitinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboGioitinh.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboGioitinh.Properties.DisplayMember = "TEN";
            this.cboGioitinh.Properties.DropDownItemHeight = 40;
            this.cboGioitinh.Properties.NullText = "";
            this.cboGioitinh.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboGioitinh.Properties.ShowHeader = false;
            this.cboGioitinh.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboGioitinh.Properties.ValueMember = "MA";
            this.cboGioitinh.Size = new System.Drawing.Size(248, 26);
            this.cboGioitinh.TabIndex = 21;
            // 
            // lbGioiTinh
            // 
            this.lbGioiTinh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGioiTinh.Location = new System.Drawing.Point(343, 103);
            this.lbGioiTinh.Margin = new System.Windows.Forms.Padding(4);
            this.lbGioiTinh.Name = "lbGioiTinh";
            this.lbGioiTinh.Size = new System.Drawing.Size(60, 19);
            this.lbGioiTinh.TabIndex = 20;
            this.lbGioiTinh.Text = "Giới tính";
            // 
            // lbNgaySinh
            // 
            this.lbNgaySinh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNgaySinh.Location = new System.Drawing.Point(9, 104);
            this.lbNgaySinh.Margin = new System.Windows.Forms.Padding(4);
            this.lbNgaySinh.Name = "lbNgaySinh";
            this.lbNgaySinh.Size = new System.Drawing.Size(67, 19);
            this.lbNgaySinh.TabIndex = 18;
            this.lbNgaySinh.Text = "Sinh nhật";
            // 
            // dtpNgaysinh
            // 
            this.dtpNgaysinh.EditValue = null;
            this.dtpNgaysinh.EnterMoveNextControl = true;
            this.dtpNgaysinh.Location = new System.Drawing.Point(88, 100);
            this.dtpNgaysinh.Name = "dtpNgaysinh";
            this.dtpNgaysinh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgaysinh.Properties.Appearance.Options.UseFont = true;
            this.dtpNgaysinh.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgaysinh.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dtpNgaysinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpNgaysinh.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgaysinh.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgaysinh.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtpNgaysinh.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtpNgaysinh.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtpNgaysinh.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.dtpNgaysinh.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpNgaysinh.Size = new System.Drawing.Size(248, 26);
            this.dtpNgaysinh.TabIndex = 19;
            // 
            // lb_CMND
            // 
            this.lb_CMND.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_CMND.Location = new System.Drawing.Point(714, 71);
            this.lb_CMND.Margin = new System.Windows.Forms.Padding(4);
            this.lb_CMND.Name = "lb_CMND";
            this.lb_CMND.Size = new System.Drawing.Size(44, 19);
            this.lb_CMND.TabIndex = 16;
            this.lb_CMND.Text = "CMND";
            // 
            // txtCMND
            // 
            this.txtCMND.EnterMoveNextControl = true;
            this.txtCMND.Location = new System.Drawing.Point(791, 68);
            this.txtCMND.Name = "txtCMND";
            this.txtCMND.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCMND.Properties.Appearance.Options.UseFont = true;
            this.txtCMND.Size = new System.Drawing.Size(248, 26);
            this.txtCMND.TabIndex = 17;
            // 
            // cboLoai
            // 
            this.cboLoai.EnterMoveNextControl = true;
            this.cboLoai.Location = new System.Drawing.Point(88, 4);
            this.cboLoai.Name = "cboLoai";
            this.cboLoai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLoai.Properties.Appearance.Options.UseFont = true;
            this.cboLoai.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLoai.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboLoai.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboLoai.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboLoai.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN")});
            this.cboLoai.Properties.DisplayMember = "TEN";
            this.cboLoai.Properties.NullText = "";
            this.cboLoai.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboLoai.Properties.ShowHeader = false;
            this.cboLoai.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboLoai.Properties.ValueMember = "MA";
            this.cboLoai.Size = new System.Drawing.Size(248, 26);
            this.cboLoai.TabIndex = 1;
            // 
            // lbNCCKH
            // 
            this.lbNCCKH.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNCCKH.Location = new System.Drawing.Point(9, 8);
            this.lbNCCKH.Margin = new System.Windows.Forms.Padding(4);
            this.lbNCCKH.Name = "lbNCCKH";
            this.lbNCCKH.Size = new System.Drawing.Size(57, 19);
            this.lbNCCKH.TabIndex = 0;
            this.lbNCCKH.Text = "NCC-KH";
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(86, 226);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 37;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnSua
            // 
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(262, 226);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(80, 35);
            this.btnSua.TabIndex = 39;
            this.btnSua.Text = "&Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(350, 226);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 35);
            this.btnXoa.TabIndex = 40;
            this.btnXoa.Text = "&Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = global::KP_RES.Properties.Resources.add_file_26;
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(174, 226);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 35);
            this.btnThem.TabIndex = 38;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // lbNguoiLienHe
            // 
            this.lbNguoiLienHe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNguoiLienHe.Location = new System.Drawing.Point(343, 71);
            this.lbNguoiLienHe.Margin = new System.Windows.Forms.Padding(4);
            this.lbNguoiLienHe.Name = "lbNguoiLienHe";
            this.lbNguoiLienHe.Size = new System.Drawing.Size(95, 19);
            this.lbNguoiLienHe.TabIndex = 14;
            this.lbNguoiLienHe.Text = "Người liên hệ";
            // 
            // txtNGUOILIENHE
            // 
            this.txtNGUOILIENHE.EnterMoveNextControl = true;
            this.txtNGUOILIENHE.Location = new System.Drawing.Point(459, 68);
            this.txtNGUOILIENHE.Name = "txtNGUOILIENHE";
            this.txtNGUOILIENHE.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNGUOILIENHE.Properties.Appearance.Options.UseFont = true;
            this.txtNGUOILIENHE.Size = new System.Drawing.Size(248, 26);
            this.txtNGUOILIENHE.TabIndex = 15;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(9, 72);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(39, 19);
            this.labelControl4.TabIndex = 12;
            this.labelControl4.Text = "Email";
            // 
            // txtEMAIL
            // 
            this.txtEMAIL.EnterMoveNextControl = true;
            this.txtEMAIL.Location = new System.Drawing.Point(88, 68);
            this.txtEMAIL.Name = "txtEMAIL";
            this.txtEMAIL.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEMAIL.Properties.Appearance.Options.UseFont = true;
            this.txtEMAIL.Size = new System.Drawing.Size(248, 26);
            this.txtEMAIL.TabIndex = 13;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(714, 38);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(55, 19);
            this.labelControl5.TabIndex = 10;
            this.labelControl5.Text = "Website";
            // 
            // txtWEBSITE
            // 
            this.txtWEBSITE.EnterMoveNextControl = true;
            this.txtWEBSITE.Location = new System.Drawing.Point(791, 36);
            this.txtWEBSITE.Name = "txtWEBSITE";
            this.txtWEBSITE.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWEBSITE.Properties.Appearance.Options.UseFont = true;
            this.txtWEBSITE.Size = new System.Drawing.Size(248, 26);
            this.txtWEBSITE.TabIndex = 11;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(343, 38);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(24, 19);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "Fax";
            // 
            // txtFAX
            // 
            this.txtFAX.EnterMoveNextControl = true;
            this.txtFAX.Location = new System.Drawing.Point(459, 36);
            this.txtFAX.Name = "txtFAX";
            this.txtFAX.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFAX.Properties.Appearance.Options.UseFont = true;
            this.txtFAX.Size = new System.Drawing.Size(248, 26);
            this.txtFAX.TabIndex = 9;
            // 
            // lbDienThoai
            // 
            this.lbDienThoai.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDienThoai.Location = new System.Drawing.Point(9, 39);
            this.lbDienThoai.Margin = new System.Windows.Forms.Padding(4);
            this.lbDienThoai.Name = "lbDienThoai";
            this.lbDienThoai.Size = new System.Drawing.Size(72, 19);
            this.lbDienThoai.TabIndex = 6;
            this.lbDienThoai.Text = "Điện thoại";
            // 
            // txtDIENTHOAI
            // 
            this.txtDIENTHOAI.EnterMoveNextControl = true;
            this.txtDIENTHOAI.Location = new System.Drawing.Point(88, 36);
            this.txtDIENTHOAI.Name = "txtDIENTHOAI";
            this.txtDIENTHOAI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDIENTHOAI.Properties.Appearance.Options.UseFont = true;
            this.txtDIENTHOAI.Size = new System.Drawing.Size(248, 26);
            this.txtDIENTHOAI.TabIndex = 7;
            // 
            // lbDiaChi
            // 
            this.lbDiaChi.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDiaChi.Location = new System.Drawing.Point(714, 7);
            this.lbDiaChi.Margin = new System.Windows.Forms.Padding(4);
            this.lbDiaChi.Name = "lbDiaChi";
            this.lbDiaChi.Size = new System.Drawing.Size(48, 19);
            this.lbDiaChi.TabIndex = 4;
            this.lbDiaChi.Text = "Địa chỉ";
            // 
            // txtDIACHI
            // 
            this.txtDIACHI.EnterMoveNextControl = true;
            this.txtDIACHI.Location = new System.Drawing.Point(791, 4);
            this.txtDIACHI.Name = "txtDIACHI";
            this.txtDIACHI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDIACHI.Properties.Appearance.Options.UseFont = true;
            this.txtDIACHI.Size = new System.Drawing.Size(248, 26);
            this.txtDIACHI.TabIndex = 5;
            // 
            // chkSUDUNG
            // 
            this.chkSUDUNG.EnterMoveNextControl = true;
            this.chkSUDUNG.Location = new System.Drawing.Point(86, 196);
            this.chkSUDUNG.Name = "chkSUDUNG";
            this.chkSUDUNG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNG.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNG.Properties.Caption = "Sử dụng";
            this.chkSUDUNG.Size = new System.Drawing.Size(92, 24);
            this.chkSUDUNG.TabIndex = 36;
            // 
            // lblGHICHU
            // 
            this.lblGHICHU.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGHICHU.Location = new System.Drawing.Point(714, 103);
            this.lblGHICHU.Margin = new System.Windows.Forms.Padding(4);
            this.lblGHICHU.Name = "lblGHICHU";
            this.lblGHICHU.Size = new System.Drawing.Size(54, 19);
            this.lblGHICHU.TabIndex = 22;
            this.lblGHICHU.Text = "Ghi chú";
            // 
            // txtGHICHU
            // 
            this.txtGHICHU.EnterMoveNextControl = true;
            this.txtGHICHU.Location = new System.Drawing.Point(791, 100);
            this.txtGHICHU.Name = "txtGHICHU";
            this.txtGHICHU.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHU.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHU.Size = new System.Drawing.Size(248, 26);
            this.txtGHICHU.TabIndex = 23;
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(343, 7);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(27, 19);
            this.lblTEN.TabIndex = 2;
            this.lblTEN.Text = "Tên";
            // 
            // txtTEN
            // 
            this.txtTEN.EnterMoveNextControl = true;
            this.txtTEN.Location = new System.Drawing.Point(459, 4);
            this.txtTEN.Name = "txtTEN";
            this.txtTEN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTEN.Properties.Appearance.Options.UseFont = true;
            this.txtTEN.Size = new System.Drawing.Size(248, 26);
            this.txtTEN.TabIndex = 3;
            // 
            // txtDatCoc
            // 
            this.txtDatCoc.EnterMoveNextControl = true;
            this.txtDatCoc.Location = new System.Drawing.Point(791, 164);
            this.txtDatCoc.Name = "txtDatCoc";
            this.txtDatCoc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDatCoc.Properties.Appearance.Options.UseFont = true;
            this.txtDatCoc.Properties.Appearance.Options.UseTextOptions = true;
            this.txtDatCoc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtDatCoc.Properties.DisplayFormat.FormatString = "N0";
            this.txtDatCoc.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtDatCoc.Properties.EditFormat.FormatString = "N0";
            this.txtDatCoc.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtDatCoc.Properties.Mask.EditMask = "N0";
            this.txtDatCoc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtDatCoc.Size = new System.Drawing.Size(248, 26);
            this.txtDatCoc.TabIndex = 35;
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 270);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1319, 381);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 50;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.ID,
            this.TEN,
            this.TENLOAI,
            this.DIACHI,
            this.DIENTHOAI,
            this.FAX,
            this.EMAIL,
            this.NGUOILIENHE,
            this.NGAYSINH,
            this.MADAILY,
            this.LOAIDAILY,
            this.CAPDAILY,
            this.CHIETKHAU,
            this.HANMUC_CONGNO,
            this.HANTHANHTOAN,
            this.ISDATCOC,
            this.TIENDATCOC,
            this.GHICHU,
            this.SUDUNG,
            this.FILL,
            this.WEBSITE,
            this.LOAI,
            this.MA});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.TEN, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.AllowSize = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 50;
            // 
            // ID
            // 
            this.ID.AppearanceCell.Options.UseTextOptions = true;
            this.ID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ID.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.ID.AppearanceHeader.Options.UseFont = true;
            this.ID.AppearanceHeader.Options.UseTextOptions = true;
            this.ID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ID.Caption = "Mã";
            this.ID.DisplayFormat.FormatString = "000";
            this.ID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.ID.FieldName = "MA";
            this.ID.Name = "ID";
            this.ID.OptionsColumn.AllowEdit = false;
            this.ID.OptionsColumn.AllowFocus = false;
            this.ID.OptionsColumn.FixedWidth = true;
            this.ID.Visible = true;
            this.ID.VisibleIndex = 1;
            this.ID.Width = 60;
            // 
            // TEN
            // 
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.Caption = "Tên";
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowEdit = false;
            this.TEN.OptionsColumn.AllowFocus = false;
            this.TEN.OptionsColumn.FixedWidth = true;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 3;
            this.TEN.Width = 180;
            // 
            // TENLOAI
            // 
            this.TENLOAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENLOAI.AppearanceHeader.Options.UseFont = true;
            this.TENLOAI.AppearanceHeader.Options.UseTextOptions = true;
            this.TENLOAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENLOAI.Caption = "NCC-KH";
            this.TENLOAI.FieldName = "TENLOAI";
            this.TENLOAI.Name = "TENLOAI";
            this.TENLOAI.OptionsColumn.AllowEdit = false;
            this.TENLOAI.OptionsColumn.AllowFocus = false;
            this.TENLOAI.OptionsColumn.FixedWidth = true;
            this.TENLOAI.Visible = true;
            this.TENLOAI.VisibleIndex = 2;
            this.TENLOAI.Width = 110;
            // 
            // DIACHI
            // 
            this.DIACHI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DIACHI.AppearanceHeader.Options.UseFont = true;
            this.DIACHI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIACHI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIACHI.Caption = "Địa chỉ";
            this.DIACHI.FieldName = "DIACHI";
            this.DIACHI.Name = "DIACHI";
            this.DIACHI.OptionsColumn.AllowEdit = false;
            this.DIACHI.OptionsColumn.AllowFocus = false;
            this.DIACHI.OptionsColumn.FixedWidth = true;
            this.DIACHI.Visible = true;
            this.DIACHI.VisibleIndex = 4;
            this.DIACHI.Width = 230;
            // 
            // DIENTHOAI
            // 
            this.DIENTHOAI.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DIENTHOAI.AppearanceHeader.Options.UseFont = true;
            this.DIENTHOAI.AppearanceHeader.Options.UseTextOptions = true;
            this.DIENTHOAI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DIENTHOAI.Caption = "Điện thoại";
            this.DIENTHOAI.FieldName = "DIENTHOAI";
            this.DIENTHOAI.Name = "DIENTHOAI";
            this.DIENTHOAI.OptionsColumn.AllowEdit = false;
            this.DIENTHOAI.OptionsColumn.AllowFocus = false;
            this.DIENTHOAI.OptionsColumn.FixedWidth = true;
            this.DIENTHOAI.Visible = true;
            this.DIENTHOAI.VisibleIndex = 5;
            this.DIENTHOAI.Width = 100;
            // 
            // FAX
            // 
            this.FAX.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.FAX.AppearanceHeader.Options.UseFont = true;
            this.FAX.AppearanceHeader.Options.UseTextOptions = true;
            this.FAX.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FAX.Caption = "Fax";
            this.FAX.FieldName = "FAX";
            this.FAX.Name = "FAX";
            this.FAX.OptionsColumn.AllowEdit = false;
            this.FAX.OptionsColumn.AllowFocus = false;
            this.FAX.OptionsColumn.FixedWidth = true;
            this.FAX.Visible = true;
            this.FAX.VisibleIndex = 6;
            this.FAX.Width = 100;
            // 
            // EMAIL
            // 
            this.EMAIL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.EMAIL.AppearanceHeader.Options.UseFont = true;
            this.EMAIL.AppearanceHeader.Options.UseTextOptions = true;
            this.EMAIL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.EMAIL.Caption = "Email";
            this.EMAIL.FieldName = "EMAIL";
            this.EMAIL.Name = "EMAIL";
            this.EMAIL.OptionsColumn.AllowEdit = false;
            this.EMAIL.OptionsColumn.AllowFocus = false;
            this.EMAIL.OptionsColumn.FixedWidth = true;
            this.EMAIL.Visible = true;
            this.EMAIL.VisibleIndex = 7;
            this.EMAIL.Width = 100;
            // 
            // NGUOILIENHE
            // 
            this.NGUOILIENHE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGUOILIENHE.AppearanceHeader.Options.UseFont = true;
            this.NGUOILIENHE.AppearanceHeader.Options.UseTextOptions = true;
            this.NGUOILIENHE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGUOILIENHE.Caption = "Người liên hệ";
            this.NGUOILIENHE.FieldName = "NGUOILIENHE";
            this.NGUOILIENHE.Name = "NGUOILIENHE";
            this.NGUOILIENHE.OptionsColumn.AllowEdit = false;
            this.NGUOILIENHE.OptionsColumn.AllowFocus = false;
            this.NGUOILIENHE.OptionsColumn.FixedWidth = true;
            this.NGUOILIENHE.Visible = true;
            this.NGUOILIENHE.VisibleIndex = 8;
            this.NGUOILIENHE.Width = 153;
            // 
            // NGAYSINH
            // 
            this.NGAYSINH.AppearanceCell.Options.UseTextOptions = true;
            this.NGAYSINH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYSINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.NGAYSINH.AppearanceHeader.Options.UseFont = true;
            this.NGAYSINH.AppearanceHeader.Options.UseTextOptions = true;
            this.NGAYSINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NGAYSINH.Caption = "Ngày sinh";
            this.NGAYSINH.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.NGAYSINH.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NGAYSINH.FieldName = "NGAYSINH";
            this.NGAYSINH.Name = "NGAYSINH";
            this.NGAYSINH.OptionsColumn.AllowEdit = false;
            this.NGAYSINH.OptionsColumn.AllowFocus = false;
            this.NGAYSINH.OptionsColumn.FixedWidth = true;
            this.NGAYSINH.Visible = true;
            this.NGAYSINH.VisibleIndex = 9;
            this.NGAYSINH.Width = 120;
            // 
            // MADAILY
            // 
            this.MADAILY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MADAILY.AppearanceHeader.Options.UseFont = true;
            this.MADAILY.AppearanceHeader.Options.UseTextOptions = true;
            this.MADAILY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MADAILY.Caption = "Mã đại lý";
            this.MADAILY.FieldName = "ID";
            this.MADAILY.Name = "MADAILY";
            this.MADAILY.OptionsColumn.AllowEdit = false;
            this.MADAILY.OptionsColumn.AllowFocus = false;
            this.MADAILY.OptionsColumn.FixedWidth = true;
            this.MADAILY.Visible = true;
            this.MADAILY.VisibleIndex = 10;
            this.MADAILY.Width = 100;
            // 
            // LOAIDAILY
            // 
            this.LOAIDAILY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.LOAIDAILY.AppearanceHeader.Options.UseFont = true;
            this.LOAIDAILY.AppearanceHeader.Options.UseTextOptions = true;
            this.LOAIDAILY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LOAIDAILY.Caption = "Loại đại lý";
            this.LOAIDAILY.FieldName = "LOAIDAILY1";
            this.LOAIDAILY.Name = "LOAIDAILY";
            this.LOAIDAILY.OptionsColumn.AllowEdit = false;
            this.LOAIDAILY.OptionsColumn.AllowFocus = false;
            this.LOAIDAILY.OptionsColumn.FixedWidth = true;
            this.LOAIDAILY.Visible = true;
            this.LOAIDAILY.VisibleIndex = 11;
            this.LOAIDAILY.Width = 120;
            // 
            // CAPDAILY
            // 
            this.CAPDAILY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CAPDAILY.AppearanceHeader.Options.UseFont = true;
            this.CAPDAILY.AppearanceHeader.Options.UseTextOptions = true;
            this.CAPDAILY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CAPDAILY.Caption = "Cấp đại lý";
            this.CAPDAILY.FieldName = "CAPDAILY";
            this.CAPDAILY.Name = "CAPDAILY";
            this.CAPDAILY.OptionsColumn.AllowEdit = false;
            this.CAPDAILY.OptionsColumn.AllowFocus = false;
            this.CAPDAILY.OptionsColumn.FixedWidth = true;
            this.CAPDAILY.Visible = true;
            this.CAPDAILY.VisibleIndex = 12;
            this.CAPDAILY.Width = 150;
            // 
            // CHIETKHAU
            // 
            this.CHIETKHAU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CHIETKHAU.AppearanceHeader.Options.UseFont = true;
            this.CHIETKHAU.AppearanceHeader.Options.UseTextOptions = true;
            this.CHIETKHAU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHIETKHAU.Caption = "Chiết khấu";
            this.CHIETKHAU.FieldName = "CHIETKHAU";
            this.CHIETKHAU.Name = "CHIETKHAU";
            this.CHIETKHAU.OptionsColumn.AllowEdit = false;
            this.CHIETKHAU.OptionsColumn.AllowFocus = false;
            this.CHIETKHAU.OptionsColumn.FixedWidth = true;
            this.CHIETKHAU.Visible = true;
            this.CHIETKHAU.VisibleIndex = 13;
            this.CHIETKHAU.Width = 100;
            // 
            // HANMUC_CONGNO
            // 
            this.HANMUC_CONGNO.AppearanceCell.Options.UseTextOptions = true;
            this.HANMUC_CONGNO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.HANMUC_CONGNO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HANMUC_CONGNO.AppearanceHeader.Options.UseFont = true;
            this.HANMUC_CONGNO.AppearanceHeader.Options.UseTextOptions = true;
            this.HANMUC_CONGNO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HANMUC_CONGNO.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.HANMUC_CONGNO.Caption = "Hạn mức công nợ";
            this.HANMUC_CONGNO.DisplayFormat.FormatString = "N0";
            this.HANMUC_CONGNO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.HANMUC_CONGNO.FieldName = "HANMUC_CONGNO";
            this.HANMUC_CONGNO.Name = "HANMUC_CONGNO";
            this.HANMUC_CONGNO.OptionsColumn.AllowEdit = false;
            this.HANMUC_CONGNO.OptionsColumn.AllowFocus = false;
            this.HANMUC_CONGNO.OptionsColumn.FixedWidth = true;
            this.HANMUC_CONGNO.Visible = true;
            this.HANMUC_CONGNO.VisibleIndex = 14;
            this.HANMUC_CONGNO.Width = 150;
            // 
            // HANTHANHTOAN
            // 
            this.HANTHANHTOAN.AppearanceCell.Options.UseTextOptions = true;
            this.HANTHANHTOAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.HANTHANHTOAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HANTHANHTOAN.AppearanceHeader.Options.UseFont = true;
            this.HANTHANHTOAN.AppearanceHeader.Options.UseTextOptions = true;
            this.HANTHANHTOAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HANTHANHTOAN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.HANTHANHTOAN.Caption = "Hạn thanh toán";
            this.HANTHANHTOAN.FieldName = "HANTHANHTOAN";
            this.HANTHANHTOAN.Name = "HANTHANHTOAN";
            this.HANTHANHTOAN.OptionsColumn.AllowEdit = false;
            this.HANTHANHTOAN.OptionsColumn.AllowFocus = false;
            this.HANTHANHTOAN.OptionsColumn.FixedWidth = true;
            this.HANTHANHTOAN.Visible = true;
            this.HANTHANHTOAN.VisibleIndex = 15;
            this.HANTHANHTOAN.Width = 100;
            // 
            // ISDATCOC
            // 
            this.ISDATCOC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ISDATCOC.AppearanceHeader.Options.UseFont = true;
            this.ISDATCOC.AppearanceHeader.Options.UseTextOptions = true;
            this.ISDATCOC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ISDATCOC.Caption = "Đặt cọc";
            this.ISDATCOC.FieldName = "ISDATCOC";
            this.ISDATCOC.Name = "ISDATCOC";
            this.ISDATCOC.OptionsColumn.AllowEdit = false;
            this.ISDATCOC.OptionsColumn.AllowFocus = false;
            this.ISDATCOC.OptionsColumn.FixedWidth = true;
            this.ISDATCOC.Visible = true;
            this.ISDATCOC.VisibleIndex = 16;
            this.ISDATCOC.Width = 100;
            // 
            // TIENDATCOC
            // 
            this.TIENDATCOC.AppearanceCell.Options.UseTextOptions = true;
            this.TIENDATCOC.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TIENDATCOC.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TIENDATCOC.AppearanceHeader.Options.UseFont = true;
            this.TIENDATCOC.AppearanceHeader.Options.UseTextOptions = true;
            this.TIENDATCOC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIENDATCOC.Caption = "Tiền đặt cọc";
            this.TIENDATCOC.DisplayFormat.FormatString = "N0";
            this.TIENDATCOC.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TIENDATCOC.FieldName = "TIENDATCOC";
            this.TIENDATCOC.Name = "TIENDATCOC";
            this.TIENDATCOC.OptionsColumn.AllowEdit = false;
            this.TIENDATCOC.OptionsColumn.AllowFocus = false;
            this.TIENDATCOC.OptionsColumn.FixedWidth = true;
            this.TIENDATCOC.Visible = true;
            this.TIENDATCOC.VisibleIndex = 17;
            this.TIENDATCOC.Width = 150;
            // 
            // GHICHU
            // 
            this.GHICHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GHICHU.AppearanceHeader.Options.UseFont = true;
            this.GHICHU.AppearanceHeader.Options.UseTextOptions = true;
            this.GHICHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GHICHU.Caption = "Ghi chú";
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.Name = "GHICHU";
            this.GHICHU.OptionsColumn.AllowEdit = false;
            this.GHICHU.OptionsColumn.AllowFocus = false;
            this.GHICHU.OptionsColumn.FixedWidth = true;
            this.GHICHU.Visible = true;
            this.GHICHU.VisibleIndex = 18;
            this.GHICHU.Width = 150;
            // 
            // SUDUNG
            // 
            this.SUDUNG.AppearanceCell.Options.UseTextOptions = true;
            this.SUDUNG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SUDUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SUDUNG.AppearanceHeader.Options.UseFont = true;
            this.SUDUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.SUDUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SUDUNG.Caption = "Sử dụng";
            this.SUDUNG.FieldName = "SUDUNG";
            this.SUDUNG.Name = "SUDUNG";
            this.SUDUNG.OptionsColumn.AllowEdit = false;
            this.SUDUNG.OptionsColumn.AllowFocus = false;
            this.SUDUNG.OptionsColumn.FixedWidth = true;
            this.SUDUNG.Visible = true;
            this.SUDUNG.VisibleIndex = 19;
            this.SUDUNG.Width = 90;
            // 
            // FILL
            // 
            this.FILL.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.FILL.AppearanceHeader.Options.UseFont = true;
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.OptionsColumn.AllowMove = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 20;
            this.FILL.Width = 20;
            // 
            // WEBSITE
            // 
            this.WEBSITE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.WEBSITE.AppearanceHeader.Options.UseFont = true;
            this.WEBSITE.AppearanceHeader.Options.UseTextOptions = true;
            this.WEBSITE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.WEBSITE.Caption = "Website";
            this.WEBSITE.FieldName = "WEBSITE";
            this.WEBSITE.Name = "WEBSITE";
            this.WEBSITE.OptionsColumn.AllowEdit = false;
            this.WEBSITE.OptionsColumn.AllowFocus = false;
            this.WEBSITE.OptionsColumn.FixedWidth = true;
            this.WEBSITE.Width = 100;
            // 
            // LOAI
            // 
            this.LOAI.Caption = "LOAI";
            this.LOAI.FieldName = "LOAI";
            this.LOAI.Name = "LOAI";
            // 
            // MA
            // 
            this.MA.Caption = "MA";
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1319, 270);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 381);
            this.panelControl2.TabIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 62);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 257);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 120);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 17);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 137);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 60);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = global::KP_RES.Properties.Resources.arrow2_26;
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 197);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 60);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = global::KP_RES.Properties.Resources.up22_26;
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 60);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 60);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = global::KP_RES.Properties.Resources.arrow1_26;
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 60);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 319);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 60);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 60);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // Frm_NCCKH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1358, 651);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_NCCKH";
            this.Text = "Nhà cung cấp - Khách hàng";
            this.Load += new System.EventHandler(this.Frm_NCCKH_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_NCCKH_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTienDatCoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienHanMuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHanThanhToan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCapDoDaiLy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLoaiDaiLy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboGioitinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgaysinh.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgaysinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMND.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboLoai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNGUOILIENHE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMAIL.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWEBSITE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFAX.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDIENTHOAI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDIACHI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHU.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatCoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn ID;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraGrid.Columns.GridColumn SUDUNG;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNG;
        private DevExpress.XtraEditors.LabelControl lblGHICHU;
        private DevExpress.XtraEditors.TextEdit txtGHICHU;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.TextEdit txtTEN;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraEditors.LabelControl lbNguoiLienHe;
        private DevExpress.XtraEditors.TextEdit txtNGUOILIENHE;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtEMAIL;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtWEBSITE;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtFAX;
        private DevExpress.XtraEditors.LabelControl lbDienThoai;
        private DevExpress.XtraEditors.TextEdit txtDIENTHOAI;
        private DevExpress.XtraEditors.LabelControl lbDiaChi;
        private DevExpress.XtraEditors.TextEdit txtDIACHI;
        private DevExpress.XtraGrid.Columns.GridColumn DIACHI;
        private DevExpress.XtraGrid.Columns.GridColumn DIENTHOAI;
        private DevExpress.XtraGrid.Columns.GridColumn FAX;
        private DevExpress.XtraGrid.Columns.GridColumn WEBSITE;
        private DevExpress.XtraGrid.Columns.GridColumn EMAIL;
        private DevExpress.XtraGrid.Columns.GridColumn NGUOILIENHE;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraGrid.Columns.GridColumn TENLOAI;
        private DevExpress.XtraEditors.LookUpEdit cboLoai;
        private DevExpress.XtraEditors.LabelControl lbNCCKH;
        private DevExpress.XtraGrid.Columns.GridColumn LOAI;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LookUpEdit cboGioitinh;
        private DevExpress.XtraEditors.LabelControl lbGioiTinh;
        private DevExpress.XtraEditors.LabelControl lbNgaySinh;
        private DevExpress.XtraEditors.DateEdit dtpNgaysinh;
        private DevExpress.XtraEditors.LabelControl lb_CMND;
        private DevExpress.XtraEditors.TextEdit txtCMND;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtMA;
        private DevExpress.XtraEditors.SimpleButton btnCongNo;
        private DevExpress.XtraGrid.Columns.GridColumn HANMUC_CONGNO;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn TIENDATCOC;
        private DevExpress.XtraGrid.Columns.GridColumn ISDATCOC;
        private DevExpress.XtraEditors.LookUpEdit cboCapDoDaiLy;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LookUpEdit cboLoaiDaiLy;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtTienHanMuc;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtHanThanhToan;
        private DevExpress.XtraEditors.CheckEdit chkTienDatCoc;
        private DevExpress.XtraEditors.TextEdit txtDatCoc;
        private DevExpress.XtraGrid.Columns.GridColumn NGAYSINH;
        private DevExpress.XtraGrid.Columns.GridColumn MADAILY;
        private DevExpress.XtraGrid.Columns.GridColumn LOAIDAILY;
        private DevExpress.XtraGrid.Columns.GridColumn CAPDAILY;
        private DevExpress.XtraGrid.Columns.GridColumn CHIETKHAU;
        private DevExpress.XtraGrid.Columns.GridColumn HANTHANHTOAN;




    }
}