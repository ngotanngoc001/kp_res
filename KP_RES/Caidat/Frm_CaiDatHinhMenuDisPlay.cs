﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using DevExpress.UserSkins;
using DevExpress.XtraEditors;
using System.IO;
using WMPLib;
using KP_UserManagement;
using System.Data.SqlClient;

namespace  KP_RES 
{
    public partial class Frm_CaiDatHinhMenuDisPlay : XtraForm
    {
        int soluonghanghoa;
        int width = 0;
        int height = 0;
        string[] FileName;
        string[] FilePath;
        int sohinhtheochieudai;
        int sohinhtheochieurong;
        int tonghinhtrentrang;
        int sotrang;
        int trang = 1;
        int manhom;
        int bienfocus;
        public Frm_CaiDatHinhMenuDisPlay()
        {
            InitializeComponent();
            LoadNhomHang();
        }

        private void LoadData(int manhom)
        {
            try
            {
                DataTable dthh = clsMain.ReturnDataTable("select HH.MA_HANGHOA from HANGHOA HH INNER JOIN NHOMHANG NH ON HH.MA_NHOMHANG = NH.MA_NHOMHANG WHERE HH.SUDUNG = 1 AND NH.THUCDON = 1 AND HH.MA_NHOMHANG = " + manhom);
                if (dthh.Rows.Count > 0)
                    soluonghanghoa = dthh.Rows.Count;
                width = Screen.PrimaryScreen.WorkingArea.Width - 140; //140 panel phai
                height = Screen.PrimaryScreen.WorkingArea.Height - 60; //2 panel chuyển trang
                sohinhtheochieudai = width / 140;
                sohinhtheochieurong = height / 150;
                tonghinhtrentrang = sohinhtheochieudai * sohinhtheochieurong;
                sotrang = soluonghanghoa / tonghinhtrentrang;
                if (soluonghanghoa % tonghinhtrentrang != 0)
                    sotrang++;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }//load dữ liệu ảnh,hiển thị

        private void LoadImage(int tonghinhtrentrang, int trang, int manhom)
        {
            try
            {
                pnHanghoa.Controls.Clear();
                int tang = 1;
                int x = 5, y = 10;
                string sqlhh = "exec sp_PHANTRANG_DISPLAY_MENU_NHAHANG " + (tonghinhtrentrang * (trang - 1)) + "," + (tonghinhtrentrang * trang) + "," + manhom;
                DataTable dthh = clsMain.ReturnDataTable(sqlhh);
                foreach (DataRow dr in dthh.Rows)
                {
                    Panel pn_Group = new Panel();
                    Panel pn_Hinh = new Panel();
                    Panel pn_Ten = new Panel();
                    LabelControl lbTen = new LabelControl();
                    PictureBox btnHinh = new PictureBox();

                    pn_Group.BorderStyle = BorderStyle.FixedSingle;
                    btnHinh.BorderStyle = BorderStyle.FixedSingle;
                    btnHinh.SizeMode = PictureBoxSizeMode.StretchImage;
                    lbTen.Font = new System.Drawing.Font("Tahoma", 10);

                    btnHinh.Dock = DockStyle.Fill;
                    lbTen.Dock = DockStyle.Fill;
                    pn_Hinh.Dock = DockStyle.Fill;
                    pn_Ten.Dock = DockStyle.Bottom;

                    pn_Group.Name = dr["STT"].ToString();
                    pn_Hinh.Name = dr["STT"].ToString();
                    pn_Ten.Name = dr["STT"].ToString();
                    lbTen.Tag = btnHinh.Tag = dr["MA_HANGHOA"].ToString();
                    btnHinh.Name = dr["STT"].ToString();
                    lbTen.Name = dr["STT"].ToString();

                    lbTen.Text = dr["TEN_HANGHOA"].ToString();
                    lbTen.AutoSizeMode = LabelAutoSizeMode.Vertical;
                    lbTen.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center; 

                    pn_Ten.Size = new Size(140, 20);
                    pn_Group.Size = new Size(140, 150);
                    pn_Group.Location = new Point(x, y);

                    if (tang < sohinhtheochieudai)
                    {
                        x += 140 + 5;
                        tang++;
                    }
                    else
                    {
                        x = 5;
                        y += 150 + 5;
                        tang = 1;
                    }

                    try
                    {
                        byte[] tam = new byte[((byte[])dr["HINHMENUDISLAY"]).LongLength];
                        tam = (byte[])dr["HINHMENUDISLAY"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        btnHinh.Image = bm;
                    }
                    catch
                    {
                    }
                    btnHinh.Click += new System.EventHandler(bnt_hinh_Click);
                    pn_Hinh.Controls.Add(btnHinh);
                    pn_Ten.Controls.Add(lbTen);

                    pn_Group.Controls.Add(pn_Hinh);
                    pn_Group.Controls.Add(pn_Ten);
                    pnHanghoa.Controls.Add(pn_Group);

                }
                lbl_sotrangduoi.Text = trang + "/" + sotrang;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }//load ảnh hiển thị 

        private void bnt_hinh_Click(object sender, EventArgs e)
        {
            PictureBox btn = (PictureBox)sender;
            foreach (Panel pn in pnHanghoa.Controls)
            {
                if (pn.Name == btn.Name)
                {
                    foreach (Panel pn1 in pn.Controls)
                    {
                        if (pn1.Name == btn.Name)
                        {
                            foreach (PictureBox pb in pn1.Controls)
                            {
                                if (pb.Name == btn.Name)
                                {
                                    OpenFileDialog openFileDialog1 = new OpenFileDialog();
                                    openFileDialog1.Filter = "Image Files|*.JPG;*.bmp;*.png;*.gif|All Files|*.*";
                                    openFileDialog1.FilterIndex = 1;
                                    openFileDialog1.RestoreDirectory = true;
                                    openFileDialog1.Multiselect = false;
                                    openFileDialog1.Title = "Chọn ảnh";
                                    if (openFileDialog1.ShowDialog() == DialogResult.OK)
                                    {
                                        FileName = openFileDialog1.SafeFileNames;
                                        FilePath = openFileDialog1.FileNames;
                                        for (int i = 0; i < FileName.Length; i++)
                                        {
                                            try
                                            {
                                                Image img = Image.FromFile(FilePath[i]);
                                                Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(img, typeof(Byte[]));
                                                SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
                                                SqlCommand SqlComMain = null;
                                                string sql = "UPDATE HANGHOA SET HINHMENUDISLAY = @HINHMENUDISLAY WHERE MA_HANGHOA = @MA_HANGHOA" + "\n";
                                                SqlCommand SqlCom = new SqlCommand(sql, CN);
                                                SqlCom.Parameters.Add(new SqlParameter("@MA_HANGHOA", (object)pb.Tag));
                                                SqlCom.Parameters.Add(new SqlParameter("@HINHMENUDISLAY", (object)imageData));
                                                SqlComMain = SqlCom;
                                                CN.Open();
                                                SqlComMain.ExecuteNonQuery();
                                                CN.Close();
                                                btn.Image = img;
                                            }
                                            catch
                                            {
                                                continue;
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        }
                    }
                    break;
                }
            }
        }//sự kiện click vào hình

        private void bnt_tranglen_Click(object sender, EventArgs e)
        {
            if (trang > 1)
            {
                trang--;
                LoadImage(tonghinhtrentrang, trang, manhom);
            }
        }//click vào nút lên

        private void bnt_trangxuong_Click(object sender, EventArgs e)
        {
            if (trang < sotrang)
            {
                trang++;
                LoadImage(tonghinhtrentrang, trang, manhom);
            }
        }//click vào nút xuống

        private void LoadNhomHang()
        {
            try
            {
                flpn_nhom.Controls.Clear();
                string sqlnhom = "SELECT Row_Number() over(order by TEN_NHOMHANG) as STT, MA_NHOMHANG, TEN_NHOMHANG, HINHANH FROM NHOMHANG WHERE THUCDON = 1 AND SUDUNG = 1";
                DataTable dtnhom = clsMain.ReturnDataTable(sqlnhom);
                foreach (DataRow dr in dtnhom.Rows)
                {
                    SimpleButton btn = new SimpleButton();
                    btn.Width = 110;
                    btn.Height = 74;
                    btn.Font = new System.Drawing.Font("Tahoma", 10);
                    btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                    try
                    {
                        btn.ImageLocation = ImageLocation.TopCenter;
                        byte[] tam = new byte[((byte[])dr["HINHANH"]).LongLength];
                        tam = (byte[])dr["HINHANH"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        btn.Image = bm;
                    }
                    catch
                    {
                    }
                    btn.Name = dr["MA_NHOMHANG"].ToString();
                    btn.Text = dr["TEN_NHOMHANG"].ToString();
                    btn.Tag = dr["STT"].ToString();
                    btn.Click += new EventHandler(Nhomhang_Click);
                    flpn_nhom.Controls.Add(btn);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }//load nhóm hàng

        private void Nhomhang_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in flpn_nhom.Controls)
            {                
                if (btn1.Name == btn.Name)
                {
                    trang = 1;
                    bienfocus = Convert.ToInt32(btn.Tag);
                    manhom =  int.Parse(btn.Name);
                    LoadData(manhom);
                    LoadImage(tonghinhtrentrang, trang, manhom);
                }
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = Color.Black;
                }
            }
        }

        private void Frm_CaiDatHinhMenuDisPlay_Load(object sender, EventArgs e)
        {
            if (flpn_nhom.Controls.Count > 0)
            {
                Nhomhang_Click(flpn_nhom.Controls[0], null);
            }
        }

        private void btnLenNhomhang_Click(object sender, EventArgs e)
        {
            try
            {
                flpn_nhom.AutoScrollPosition = new Point(0, flpn_nhom.VerticalScroll.Value - flpn_nhom.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnXuongNhomhang_Click(object sender, EventArgs e)
        {
            try
            {
                flpn_nhom.AutoScrollPosition = new Point(0, flpn_nhom.VerticalScroll.Value + flpn_nhom.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void bnt_refresh_Click(object sender, EventArgs e)
        {
            LoadNhomHang();
            Nhomhang_Click(flpn_nhom.Controls[bienfocus - 1], null);
        }
    }
}