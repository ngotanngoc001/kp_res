﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_CabanThietlapcaban : DevExpress.XtraEditors.XtraForm
    {
        public Frm_CabanThietlapcaban()
        {
            InitializeComponent();
            this.Text = rm.GetString("cbtlcb", culture);
            LoaddataGridView();
            LoaddataGridView1();
            LoadPermission();
        }

        private void Frm_CabanThietlapcaban_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
            gridControl2.Width = ((this.Width) / 2 );
            panelControl5.Width = ((this.Width) / 2) -2;
        }

        string smaCaban = "";
        string smaThietlap = "";
        int igrid = 1;

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                if (igrid == 1)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.RowCount - 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
                else
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                if (igrid == 1)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.FocusedRowHandle + 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    if (gridView1.FocusedRowHandle == gridView1.RowCount - 1)
                    {
                        btnXuongit.Enabled = false;
                        btnXuongnhieu.Enabled = false;
                    }
                }
                else
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                    btnLenit.Enabled = true;
                    btnLennhieu.Enabled = true;
                    if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                    {
                        btnXuongit.Enabled = false;
                        btnXuongnhieu.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                if (igrid == 1)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = gridView1.FocusedRowHandle - 1;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                    if (gridView1.FocusedRowHandle == 0)
                    {
                        btnLenit.Enabled = false;
                        btnLennhieu.Enabled = false;
                    }
                }
                else
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                    if (gridView2.FocusedRowHandle == 0)
                    {
                        btnLenit.Enabled = false;
                        btnLennhieu.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                if (igrid == 1)
                {
                    gridView1.Focus();
                    gridView1.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }
                else
                {
                    gridView2.Focus();
                    gridView2.FocusedRowHandle = 0;
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                    btnXuongit.Enabled = true;
                    btnXuongnhieu.Enabled = true;
                }
            }
            catch
            {
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtTEN.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (smaCaban == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show(_xacnhanbanmuonxoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From CABAN" + "\n";
            sSQL += "Where MA_CABAN=" + clsMain.SQLString(smaCaban) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (smaCaban == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl(false);
            txtTEN.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            string sSQL = "";
            if (smaCaban == "" && btnThem.Enabled)
            {
                sSQL += "Insert into CABAN (TEN_CABAN,GIOBATDAU,GIOKETTHU,GHICHU,SUDUNG)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLStringUnicode(txtTEN.Text) + ",";
                sSQL += clsMain.SQLString(timeBD.Text) + ",";
                sSQL += clsMain.SQLString(timeKT.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtGHICHU.Text) + ",";
                sSQL += clsMain.SQLBit(chkSUDUNG.Checked) + ")";
            }
            else if ((smaCaban != "" && btnSua.Enabled))
            {
                sSQL += "Update CABAN Set " + "\n";
                sSQL += "TEN_CABAN=" + clsMain.SQLStringUnicode(txtTEN.Text) + "," + "\n";
                sSQL += "GIOBATDAU=" + clsMain.SQLString(timeBD.Text) + "," + "\n";
                sSQL += "GIOKETTHU=" + clsMain.SQLString(timeKT.Text) + "," + "\n";
                sSQL += "GHICHU=" + clsMain.SQLStringUnicode(txtGHICHU.Text) + "," + "\n";
                sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNG.Checked) + "\n";
                sSQL += "Where MA_CABAN=" + clsMain.SQLString(smaCaban) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoaddataGridView();
            LoaddataGridView1();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            igrid = 2;
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            smaCaban = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            txtTEN.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN").ToString();
            timeBD.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GIOBATDAU").ToString();
            timeKT.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GIOKETTHU").ToString();
            txtGHICHU.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GHICHU").ToString();
            chkSUDUNG.Checked = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SUDUNG").ToString());
            KhoaMoControl(true);
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            string sSQL = "";
            sSQL += "Select MA_CABAN as MA,TEN_CABAN as TEN,GIOBATDAU,GIOKETTHU,GHICHU,SUDUNG" + "\n";
            sSQL += "From CABAN" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            gridControl2.DataSource = dt;
            gridView2.ActiveFilterString = "[SUDUNG] = True";

            LoadCombo();
        }

        private void LoaddataGridView1()
        {
            SetRongControl1();
            KhoaMoControl1(true);
            gridView2.Focus();

            string sSQL = "";
            sSQL += "Select A.MA_THIETLAP As MA,A.MA_QUAY,B.MA_CUAHANG,A.MA_CABAN,A.MA_NHANVIEN,A.GHICHU,A.SUDUNG,B.TEN_CUAHANG,D.TEN_QUAY ,E.TEN_CABAN,G.TENNHANVIEN,F.UserID " + "\n";
            sSQL += "From THIETLAPCABAN A,CUAHANG B,KHO C,QUAY D,CABAN E,SYS_USER F,DM_NHANVIEN G" + "\n";
            sSQL += "Where A.MA_QUAY = D.MA_QUAY " + "\n";
            sSQL += "And D.MA_KHO =C.MA_KHO " + "\n";
            sSQL += "And C.MA_CUAHANG =B.MA_CUAHANG " + "\n";
            sSQL += "And A.MA_CABAN =E.MA_CABAN " + "\n";
            sSQL += "And A.MA_NHANVIEN = F.UserID " + "\n";
            sSQL += "And F.MaNV =G.MANHANVIEN " + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            gridControl1.DataSource = dt;
            gridView1.ActiveFilterString = "[SUDUNG] = True";
            gridView1.Columns["TEN_CUAHANG"].GroupIndex = 0;
            gridView1.ExpandAllGroups();
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("33");
            btnSua.Enabled = clsUserManagement.AllowEdit("33");
            btnXoa.Enabled = clsUserManagement.AllowDelete("33");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTEN.Properties.ReadOnly = true;
                timeBD.Properties.ReadOnly = true;
                timeKT.Properties.ReadOnly = true;
                txtGHICHU.Properties.ReadOnly = true;
                chkSUDUNG.Properties.ReadOnly = true;
            }
            else//mở
            {
                txtTEN.Properties.ReadOnly = false;
                timeBD.Properties.ReadOnly = false;
                timeKT.Properties.ReadOnly = false;
                txtGHICHU.Properties.ReadOnly = false;
                chkSUDUNG.Properties.ReadOnly = false;
            }
        }

        private void SetRongControl()
        {
            smaCaban = "";
            txtTEN.Text = "";
            timeBD.EditValue = "00:00:00";
            timeKT.EditValue = "00:00:00";
            txtGHICHU.Text = "";
            chkSUDUNG.Checked = true;
        }

        private Boolean CheckInput()
        {
            if (txtTEN.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_nhap + lblTEN.Text,_thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTEN.Focus();
                return false;
            }
            return true;
        }

        private void btnThem1_Click(object sender, EventArgs e)
        {
            SetRongControl1();
            KhoaMoControl1(false);
            cboQuay.Focus();
        }

        private void btnSua1_Click(object sender, EventArgs e)
        {
            if (smaThietlap == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl1(false);
            cboQuay.Focus();
        }

        private void btnXoa1_Click(object sender, EventArgs e)
        {
            if (smaThietlap == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show(_xacnhanbanmuonxoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From THIETLAPCABAN" + "\n";
            sSQL += "Where MA_THIETLAP=" + clsMain.SQLString(smaThietlap) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView1();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private bool check()
        {
            bool flag = false;
            if (cboQuay.EditValue == null)
            {
                XtraMessageBox.Show(_kocodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                flag = true;
                return flag;
            }
            DataTable dt = clsMain.ReturnDataTable("SELECT distinct MA_QUAY,MA_CABAN from THIETLAPCABAN where MA_QUAY='" + cboQuay.EditValue.ToString() + "'");
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["MA_CABAN"].ToString() == cboCa.EditValue.ToString())
                {
                    XtraMessageBox.Show(_koduoctaotrungcatrongcungquay, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    flag = true;
                    break;
                }
                DataTable dtcaban = clsMain.ReturnDataTable("select GIOBATDAU,GIOKETTHU from CABAN where MA_CABAN='" + dr["MA_CABAN"].ToString() + "'");
                DataTable dtcathem = clsMain.ReturnDataTable("select GIOBATDAU,GIOKETTHU from CABAN where MA_CABAN='" + cboCa.EditValue.ToString() + "'");
                if (Convert.ToDateTime(dtcathem.Rows[0]["GIOBATDAU"].ToString()) > Convert.ToDateTime(dtcaban.Rows[0]["GIOBATDAU"].ToString()) &&
                    Convert.ToDateTime(dtcathem.Rows[0]["GIOBATDAU"].ToString()) < Convert.ToDateTime(dtcaban.Rows[0]["GIOKETTHU"].ToString())
                    ||Convert.ToDateTime(dtcathem.Rows[0]["GIOBATDAU"].ToString()) == Convert.ToDateTime(dtcaban.Rows[0]["GIOBATDAU"].ToString()) &&
                    Convert.ToDateTime(dtcathem.Rows[0]["GIOBATDAU"].ToString()) == Convert.ToDateTime(dtcaban.Rows[0]["GIOKETTHU"].ToString())
                    || Convert.ToDateTime(dtcathem.Rows[0]["GIOKETTHU"].ToString()) > Convert.ToDateTime(dtcaban.Rows[0]["GIOBATDAU"].ToString()) &&
                    Convert.ToDateTime(dtcathem.Rows[0]["GIOKETTHU"].ToString()) < Convert.ToDateTime(dtcaban.Rows[0]["GIOKETTHU"].ToString())
                    || Convert.ToDateTime(dtcathem.Rows[0]["GIOBATDAU"].ToString()) < Convert.ToDateTime(dtcaban.Rows[0]["GIOBATDAU"].ToString()) &&
                    Convert.ToDateTime(dtcathem.Rows[0]["GIOKETTHU"].ToString()) > Convert.ToDateTime(dtcaban.Rows[0]["GIOKETTHU"].ToString())
                     || Convert.ToDateTime(dtcathem.Rows[0]["GIOBATDAU"].ToString()) > Convert.ToDateTime(dtcaban.Rows[0]["GIOBATDAU"].ToString()) &&
                    Convert.ToDateTime(dtcathem.Rows[0]["GIOKETTHU"].ToString()) < Convert.ToDateTime(dtcaban.Rows[0]["GIOKETTHU"].ToString())
                    )
                {
                    XtraMessageBox.Show(_koduoctaocacotgtrungnhau, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    flag = true;
                    break;
                }

            }
            return flag;
        }
        private void btnLuu1_Click(object sender, EventArgs e)
        {
            bool kt = check();
            if (kt)
                return;
            string sSQL = "";
            if (smaThietlap == "" && btnThem1.Enabled)
            {
                sSQL += "Insert into THIETLAPCABAN (MA_QUAY,MA_CABAN,MA_NHANVIEN,GHICHU,SUDUNG)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLString(cboQuay.EditValue.ToString()) + ",";
                sSQL += clsMain.SQLString(cboCa.EditValue.ToString()) + ",";
                sSQL += clsMain.SQLString(cboNhanvien.EditValue.ToString()) + ",";
                sSQL += clsMain.SQLStringUnicode(txtGHICHU1.Text) + ",";
                sSQL += clsMain.SQLBit(chkSUDUNG1.Checked ) + ")";
            }
            else if ((smaThietlap != "" && btnSua1.Enabled))
            {
                sSQL += "Update THIETLAPCABAN Set " + "\n";
                sSQL += "MA_QUAY=" + clsMain.SQLString(cboQuay.EditValue.ToString()) + "," + "\n";
                sSQL += "MA_CABAN=" + clsMain.SQLString(cboCa.EditValue.ToString()) + "," + "\n";
                sSQL += "MA_NHANVIEN=" + clsMain.SQLString(cboNhanvien.EditValue.ToString()) + "," + "\n";
                sSQL += "GHICHU=" + clsMain.SQLStringUnicode(txtGHICHU1.Text) + "," + "\n";
                sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNG1.Checked) + "\n";
                sSQL += "Where MA_THIETLAP=" + clsMain.SQLString(smaThietlap) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView1();
                btnThem1.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong,_thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT1 & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            igrid = 1;
            if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                return;

            smaThietlap = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MA").ToString();
            cboCuahang.EditValue = int.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MA_CUAHANG").ToString());
            cboQuay.EditValue =int.Parse ( gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MA_QUAY").ToString());
            cboCa.EditValue =int.Parse ( gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MA_CABAN").ToString());
            cboNhanvien.EditValue = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MA_NHANVIEN").ToString();
            txtGHICHU1.Text = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GHICHU").ToString();
            chkSUDUNG1.Checked = bool.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "SUDUNG").ToString());
        }

        private void KhoaMoControl1(bool tf)
        {
            if (tf == true)//khóa
            {
                cboCuahang.Properties.ReadOnly = true;
                cboQuay.Properties.ReadOnly = true;
                cboCa.Properties.ReadOnly = true;
                cboNhanvien.Properties.ReadOnly = true;
                txtGHICHU1.Properties.ReadOnly = true;
                chkSUDUNG1.Properties.ReadOnly = true;
            }
            else//mở
            {
                cboCuahang.Properties.ReadOnly = false;
                cboQuay.Properties.ReadOnly = false;
                cboCa.Properties.ReadOnly = false;
                cboNhanvien.Properties.ReadOnly = false;
                txtGHICHU1.Properties.ReadOnly = false;
                chkSUDUNG1.Properties.ReadOnly = false;
            }
        }

        private void SetRongControl1()
        {
            smaThietlap = "";
            cboCuahang.EditValue = cboCuahang.Properties.GetDataSourceValue(cboCuahang.Properties.ValueMember, 0);
            cboQuay.EditValue = cboQuay.Properties.GetDataSourceValue(cboQuay.Properties.ValueMember, 0);
            cboCa.EditValue = cboCa.Properties.GetDataSourceValue(cboCa.Properties.ValueMember, 0);
            cboNhanvien.EditValue = cboNhanvien.Properties.GetDataSourceValue(cboNhanvien.Properties.ValueMember, 0);
            txtGHICHU1.EditValue = "";
            chkSUDUNG1.Checked = true;
        }

        private void LoadCombo()
        {
            string sSQL = "";
            DataTable dt = new DataTable();

            sSQL = "";
            sSQL += "Select MA_CUAHANG as MA,TEN_CUAHANG As TEN" + "\n";
            sSQL += "From CUAHANG " + "\n";
            sSQL += "Where SUDUNG=1 " + "\n";
            dt = clsMain.ReturnDataTable(sSQL);
            cboCuahang.Properties.DataSource = dt;
            cboCuahang.EditValue = cboCuahang.Properties.GetDataSourceValue(cboCuahang.Properties.ValueMember, 0);

            sSQL = "";
            sSQL += "Select MA_CABAN As MA,TEN_CABAN As TEN,GIOBATDAU,GIOKETTHU" + "\n";
            sSQL += "From CABAN" + "\n";
            sSQL += "Where SUDUNG=1 ";
            dt = clsMain.ReturnDataTable(sSQL);
            cboCa.Properties.DataSource = dt;
            cboCa.EditValue = cboCa.Properties.GetDataSourceValue(cboCa.Properties.ValueMember, 0);

            sSQL = "";
            sSQL += "Select A.UserID As MA,B.TENNHANVIEN As TEN " + "\n";
            sSQL += "From SYS_USER A,DM_NHANVIEN B" + "\n";
            sSQL += "Where A.MaNV =B.MANHANVIEN " + "\n";
            sSQL += "And A.Active =1" + "\n";
            dt = clsMain.ReturnDataTable(sSQL);
            cboNhanvien.Properties.DataSource = dt;
            cboNhanvien.EditValue = cboNhanvien.Properties.GetDataSourceValue(cboNhanvien.Properties.ValueMember, 0);
        }

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                txt.SelectAll();
            }
            catch
            {
            }
        }

        private void cboCuahang_EditValueChanged(object sender, EventArgs e)
        {
            string sSQL = "";
            DataTable dt = new DataTable();
            sSQL = "";
            sSQL += "Select A.MA_QUAY As MA,A.TEN_QUAY As TEN,B.TEN_KHO " + "\n";
            sSQL += "From QUAY A,KHO B" + "\n";
            sSQL += "Where A.MA_KHO =B.MA_KHO " + "\n";
            sSQL += "And B.MA_CUAHANG =" + clsMain.SQLString(cboCuahang.EditValue.ToString()) + "\n";
            sSQL += "And A.SUDUNG=1 " + "\n";
            dt = clsMain.ReturnDataTable(sSQL);
            cboQuay.Properties.DataSource = dt;
            cboQuay.EditValue = cboQuay.Properties.GetDataSourceValue(cboQuay.Properties.ValueMember, 0);
        }


        string _nhap = "";
        string _thongbao = "";
        string _luukothanhcong = "";
        string _xacnhanbanmuonxoa = "";
        string _koduoctaocacotgtrungnhau = "";
        string _koduoctaotrungcatrongcungquay = "";
        string _kocodulieu = "";
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);   
        private void Frm_CabanThietlapcaban_Load(object sender, EventArgs e)
        {
            foreach (Control ctl in panelControl5.Controls)
            {
                ctl.Click += new EventHandler(ctl_Click);
            }
            foreach (Control ctl in panelControl6.Controls)
            {
                ctl.Click += new EventHandler(ctl_Click);
            }


        
            this.Text = rm.GetString("cbtlcb", culture);
            lblTEN.Text = rm.GetString("tencaban", culture);
            lbGioBatDau.Text = rm.GetString("giobatdau", culture);
            lbGioKetThuc.Text = rm.GetString("gioketthuc", culture);
            lblGHICHU.Text = rm.GetString("ghichu", culture);
            lbCuaHang.Text = rm.GetString("cuahang", culture);
            lbQuay.Text = rm.GetString("quay", culture);
            lbCaBan.Text = rm.GetString("caban", culture);
            lbGhiChu.Text = rm.GetString("ghichu", culture);
            lbNhanVien.Text = rm.GetString("nhanvien", culture);
            chkSUDUNG.Text = rm.GetString("sudung", culture);
            chkSUDUNG1.Text = rm.GetString("sudung", culture);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
            btnLuu1.Text = rm.GetString("luu", culture);
            btnThem1.Text = rm.GetString("them", culture);
            btnSua1.Text = rm.GetString("sua", culture);
            btnXoa1.Text = rm.GetString("xoa", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _nhap = rm.GetString("nhap", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);
            _xacnhanbanmuonxoa = rm.GetString("xacnhanbanmuonxoa", culture);
            _koduoctaocacotgtrungnhau = rm.GetString("koduoctaocacotgtrungnhau", culture);
            _koduoctaotrungcatrongcungquay = rm.GetString("koduoctaotrungcatrongcungquay", culture);
            _kocodulieu = rm.GetString("kocodulieu", culture);

            gridView2.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView2.Columns["MA"].Caption = rm.GetString("ma", culture);
            gridView2.Columns["TEN"].Caption = rm.GetString("ten", culture);
            gridView2.Columns["GIOBATDAU"].Caption = rm.GetString("batdau", culture);
            gridView2.Columns["GIOKETTHU"].Caption = rm.GetString("ketthuc", culture);
            gridView2.Columns["GHICHU"].Caption = rm.GetString("ghichu", culture);
            gridView2.Columns["SUDUNG"].Caption = rm.GetString("sudung", culture);
            gridView2.GroupPanelText = rm.GetString("ktcvddntimkiem", culture);

            gridView1.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView1.Columns["TEN_CABAN"].Caption = rm.GetString("caban", culture);
            gridView1.Columns["UserID"].Caption = rm.GetString("taikhoan", culture);
            gridView1.Columns["TENNHANVIEN"].Caption = rm.GetString("nhanvien", culture);
            gridView1.Columns["GHICHU"].Caption = rm.GetString("ghichu", culture);
            gridView1.Columns["TEN_CUAHANG"].Caption = rm.GetString("cuahang", culture);
            gridView1.Columns["TEN_QUAY"].Caption = rm.GetString("quay", culture);
            gridView1.Columns["SUDUNG"].Caption = rm.GetString("sudung", culture);
            gridView1.GroupPanelText = rm.GetString("ktcvddntimkiem", culture);
        }
    }
}