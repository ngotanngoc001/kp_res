﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Globalization;
using System.Resources;
using System.Data.SqlClient;
namespace KP_RES 
{
    public partial class Frm_Tieudeve : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Tieudeve()
        {
            InitializeComponent();
           
        }
        public string sMacuahang { get; set; }

        private void LoadOption()
        {
            string sSQL = "Select HEADER_VE,FOOTER_VE, Image_Ve From CUAHANG where MA_CUAHANG="+clsMain .SQLString (sMacuahang );
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                txtHeader.Text = dt.Rows[0]["HEADER_VE"].ToString();
                txtFooter.Text = dt.Rows[0]["FOOTER_VE"].ToString();

                try
                {
                    byte[] tam = new byte[((byte[])dt.Rows[0]["Image_Ve"]).LongLength];
                    tam = (byte[])dt.Rows[0]["Image_Ve"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    picLogo.Image = bm;
                    picLogo.BackgroundImage = null;
                }
                catch
                {
                    picLogo.Image = null;
                    picLogo.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
                }

                 
                
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(picLogo.Image, typeof(Byte[]));
            SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
            SqlCommand SqlComMain = null;
            string sSQL = "";
            sSQL += "Update CUAHANG Set HEADER_VE=@HEADER_VE,FOOTER_VE=@FOOTER_VE,Image_Ve=@Image_Ve Where MA_CUAHANG=" + clsMain.SQLString(sMacuahang);

            SqlCommand SqlCom = new SqlCommand(sSQL, CN);
            SqlCom.Parameters.Add(new SqlParameter("@HEADER_VE", (object)txtHeader.Text));
            SqlCom.Parameters.Add(new SqlParameter("@FOOTER_VE", (object)txtFooter.Text));
            SqlCom.Parameters.Add(new SqlParameter("@Image_Ve", (object)imageData));
            SqlComMain = SqlCom;

            Boolean bRunSQL = true;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                CN.Open();
                SqlComMain.ExecuteNonQuery();
                CN.Close();
                this.Cursor = Cursors.Default;
                bRunSQL = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                bRunSQL = false;
            }

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        string _thongbao = "";
        string _luukothanhcong = "";
        string _luuthanhcong = "";
        private void Frm_Tieudecuahang_Load(object sender, EventArgs e)
        {

            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            //this.Text = rm.GetString("tieudebillve", culture);
            btnLuu.Text = rm.GetString("dongy", culture);
            btnBanphim.Text = rm.GetString("banphim", culture);
            btnDong.Text = rm.GetString("dong", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _luuthanhcong = rm.GetString("luuthanhcong", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);

            LoadOption();
        }

        private void picLogo_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "Image Files|*.jpg;*.bmp;*.png;*.gif|All Files|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.Title = "Chọn ảnh";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    picLogo.ImageLocation = openFileDialog1.FileName;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}