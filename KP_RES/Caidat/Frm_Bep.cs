﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_Bep : DevExpress.XtraEditors.XtraForm
    {
        public Frm_Bep()
        {
            InitializeComponent();
            this.Text = rm.GetString("bep", culture);
            LoadCombo();
            LoaddataGridView();
            LoadPermission();
        }

        private void Frm_Bep_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        string sma = "";

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            cboCUAHANG.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show(_xacnhanbanmuonxoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From DM_BEP" + "\n";
            sSQL += "Where MA_BEP=" + clsMain.SQLString(sma) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl(false);
            cboCUAHANG.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            string sSQL = "";
            if (sma == "" && btnThem.Enabled)
            {
                sSQL += "Insert into DM_BEP (MA_CUAHANG,TEN_BEP,MAYINBEP,IP,GHICHU,SUDUNG)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLString(cboCUAHANG.EditValue .ToString ()) + ",";
                sSQL += clsMain.SQLStringUnicode(txtTEN.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtMAYINBEP.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtIP.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtGHICHU.Text) + ",";
                sSQL += clsMain.SQLBit(chkSUDUNG.Checked) + ")";
            }
            else if ((sma != "" && btnSua.Enabled))
            {
                sSQL += "Update DM_BEP Set " + "\n";
                sSQL += "MA_CUAHANG=" + clsMain.SQLString(cboCUAHANG.EditValue.ToString()) + "," + "\n";
                sSQL += "TEN_BEP=" + clsMain.SQLStringUnicode(txtTEN.Text) + "," + "\n";
                sSQL += "MAYINBEP=" + clsMain.SQLStringUnicode(txtMAYINBEP.Text) + "," + "\n";
                sSQL += "IP=" + clsMain.SQLStringUnicode(txtIP.Text) + "," + "\n";
                sSQL += "GHICHU=" + clsMain.SQLStringUnicode(txtGHICHU.Text) + "," + "\n";
                sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNG.Checked) + "\n";
                sSQL += "Where MA_BEP=" + clsMain.SQLString(sma) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sma = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            cboCUAHANG.EditValue =int.Parse ( gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA_CUAHANG").ToString());
            txtTEN.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN").ToString();
            txtMAYINBEP.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MAYINBEP").ToString();
            txtIP.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "IP").ToString();
            txtGHICHU.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GHICHU").ToString();
            chkSUDUNG.Checked = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SUDUNG").ToString());
            KhoaMoControl(true);
        }

        private void LoadCombo()
        {
            string sSQL = "";
            sSQL += "Select MA_CUAHANG As MA,TEN_CUAHANG As TEN" + "\n";
            sSQL += "From CUAHANG" + "\n";
            sSQL += "Where SUDUNG=1 ";
            sSQL += "Order By TEN" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            cboCUAHANG.Properties.DataSource = dt;
            cboCUAHANG.EditValue = cboCUAHANG.Properties.GetDataSourceValue(cboCUAHANG.Properties.ValueMember, 0);
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            string sSQL = "";
            sSQL += "Select A.MA_BEP As MA,A.TEN_BEP As TEN,A.MAYINBEP,A.IP,A.GHICHU,A.SUDUNG,A.MA_CUAHANG,B.TEN_CUAHANG" + "\n";
            sSQL += "From DM_BEP A,CUAHANG B" + "\n";
            sSQL += "Where A.MA_CUAHANG=B.MA_CUAHANG" + "\n";
            sSQL += "Order by B.TEN_CUAHANG,A.TEN_BEP" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            gridControl2.DataSource = dt;
            gridView2.ActiveFilterString = "[SUDUNG] = True";
            gridView2.Columns["TEN_CUAHANG"].GroupIndex = 0;
            gridView2.ExpandAllGroups();
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("8");
            btnSua.Enabled = clsUserManagement.AllowEdit("8");
            btnXoa.Enabled = clsUserManagement.AllowDelete("8");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//khóa
            {
                cboCUAHANG.Properties.ReadOnly = true;
                txtTEN.Properties.ReadOnly = true;
                txtMAYINBEP.Properties.ReadOnly = true;
                txtIP.Properties.ReadOnly = true;
                txtGHICHU.Properties.ReadOnly = true;
                chkSUDUNG.Properties.ReadOnly = true;
            }
            else//mở
            {
                cboCUAHANG.Properties.ReadOnly = false;
                txtTEN.Properties.ReadOnly = false;
                txtMAYINBEP.Properties.ReadOnly = false;
                txtIP.Properties.ReadOnly = false;
                txtGHICHU.Properties.ReadOnly = false;
                chkSUDUNG.Properties.ReadOnly = false;
            }
        }

        private void SetRongControl()
        {
            sma = "";
            cboCUAHANG.EditValue = cboCUAHANG.Properties.GetDataSourceValue(cboCUAHANG.Properties.ValueMember, 0);
            txtTEN.Text = "";
            txtMAYINBEP.Text = "";
            txtIP.Text = "";
            txtGHICHU.Text = "";
            chkSUDUNG.Checked = true;
        }

        private Boolean CheckInput()
        {
            if (txtTEN.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_nhap + lblTEN.Text, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTEN.Focus();
                return false;
            }
            return true;
        }

        private void btnMayin_Click(object sender, EventArgs e)
        {
            if (txtMAYINBEP.Properties.ReadOnly == false)
            {
                PrintDialog printDialog1 = new PrintDialog();
                printDialog1.UseEXDialog = true;
                DialogResult result = printDialog1.ShowDialog(this);
                if (result == DialogResult.OK)
                {
                    txtMAYINBEP.Text = printDialog1.PrinterSettings.PrinterName;
                }
            }
        }

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                txt.SelectAll();
            }
            catch
            {
            }
        }

        string _nhap = "";
        string _thongbao = "";
        string _luukothanhcong = "";
        string _xacnhanbanmuonxoa = "";
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void Frm_Bep_Load(object sender, EventArgs e)
        {
            foreach (Control ctl in panelControl1.Controls)
            {
                ctl.Click += new EventHandler(ctl_Click);
            }

           
            this.Text = rm.GetString("bep", culture);
            lbCuaHang.Text = rm.GetString("cuahang", culture);
            lbMayIn.Text = rm.GetString("mayin", culture);
            lblTEN.Text = rm.GetString("ten", culture);
            lblGHICHU.Text = rm.GetString("ghichu", culture);
            chkSUDUNG.Text = rm.GetString("sudung", culture);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _nhap = rm.GetString("nhap", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);
            _xacnhanbanmuonxoa = rm.GetString("xacnhanbanmuonxoa", culture);

            gridView2.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView2.Columns["MA"].Caption = rm.GetString("ma", culture);
            gridView2.Columns["TEN"].Caption = rm.GetString("ten", culture);
            gridView2.Columns["TEN_CUAHANG"].Caption = rm.GetString("cuahang", culture);
            gridView2.Columns["MAYINBEP"].Caption = rm.GetString("mayinbep", culture);
            gridView2.Columns["GHICHU"].Caption = rm.GetString("ghichu", culture);
            gridView2.Columns["SUDUNG"].Caption = rm.GetString("sudung", culture);
            gridView2.GroupPanelText = rm.GetString("ktcvddntimkiem", culture);
        }
    }
}