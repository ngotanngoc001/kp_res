﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES
{
    public partial class Frm_SetCombo_Doimathang : DevExpress.XtraEditors.XtraForm
    {
        public bool kt = false;
        public static TextEdit _textbox;
        bool[] ctltxt;
        public decimal tmp_Thoigia;
        string _Ma_Combo;
        string _Ma_hanghoa;
        string _ma_hanghoathaythe;
        int _so_luong = 0;
        DataTable dtsl;
        DataTable dthhtt;

        public Frm_SetCombo_Doimathang(string Ma_Combo,string Ma_hanghoa,int so_luong,string ma_hanghoathaythe)
        {
            InitializeComponent();
            _Ma_Combo = Ma_Combo;
            _Ma_hanghoa = Ma_hanghoa;
            _ma_hanghoathaythe = ma_hanghoathaythe;
            _so_luong = so_luong;
            Loadhanghoa();
            SetTextbox(txtTiencongthem);
            
        }
        public void SetTextbox(TextEdit edit)
        {
            _textbox = edit;
        }
        private void Loadhanghoa()
        {
            string sSQL = "";
            sSQL += "SELECT MA_HANGHOA as MA,TEN_HANGHOA as TEN" + "\n";
            sSQL += "FROM HANGHOA" + "\n";
            sSQL += "WHERE MA_HANGHOA=" + clsMain.SQLString(_Ma_hanghoa);
            DataTable dthh = clsMain.ReturnDataTable(sSQL);

            sSQL = "";
            sSQL += "SELECT MA_HANGHOA as MA,TEN_HANGHOA as TEN" + "\n";
            sSQL += "FROM HANGHOA" + "\n";
            sSQL += "WHERE MA_HANGHOA=" + clsMain.SQLString(_ma_hanghoathaythe);
            dthhtt = clsMain.ReturnDataTable(sSQL);

            sSQL = "";
            sSQL += "SELECT SOLUONG" + "\n";
            sSQL += "FROM COMBO" + "\n";
            sSQL += "WHERE MA_COMBO=" + clsMain.SQLString(_Ma_Combo) + "\n";
            sSQL += "AND MA_HANGHOA=" + clsMain.SQLString(_Ma_hanghoa) + "\n";
            dtsl = clsMain.ReturnDataTable(sSQL);

            lbHanghoagoc.Text = dthh.Rows[0]["TEN"].ToString() + " ("+ _so_luong.ToString() +")";
            lbHanghoathaydoi.Text = dthhtt.Rows[0]["TEN"].ToString() + " (" + dtsl.Rows[0]["SOLUONG"].ToString() + ")";
        }
        #region bàn phím số
        private void InputKeyboardNumber(string sInput)
        {
            if (_textbox.Text.Length >= 15)
            {
                int iPositon = _textbox.SelectionStart;
                XtraMessageBox.Show("Bạn không được nhập quá 15 ký tự", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                _textbox.SelectionStart = _textbox.Text.Length;
                _textbox.SelectionStart = iPositon;
                return;
            }

            if (_textbox.Text == "0")
            {
                if (sInput == "0" || sInput == "00" || sInput == "000")
                    return;
            }
            if (_textbox.SelectedText == _textbox.Text)
            {
                _textbox.Text = "0";
            }
            int iLenght, iPosition;
            iLenght = _textbox.Text.Length;
            iPosition = _textbox.SelectionStart;
            if (iPosition == 0)
            {
                if (sInput == "0" || sInput == "00" || sInput == "000")
                    return;
            }

            if (_textbox.Text == "0")
            {
                _textbox.Text = sInput;
            }
            else
            {
                _textbox.Text = _textbox.Text.Insert(iPosition, sInput);
            }
            if (sInput.Length == 1)
            {
                if (_textbox.Text.Length - iLenght == 1)
                {
                    _textbox.SelectionStart = iPosition + 1;
                }
                else
                {
                    _textbox.SelectionStart = iPosition + 2;
                }
            }
            else
                if (sInput.Length == 2)
                {
                    if (_textbox.Text.Length - iLenght == 2)
                    {
                        _textbox.SelectionStart = iPosition + 2;
                    }
                    else
                    {
                        _textbox.SelectionStart = iPosition + 3;
                    }
                }
                else
                    if (sInput.Length == 3)
                    {
                        if (_textbox.Text.Length - iLenght == 3)
                        {
                            _textbox.SelectionStart = iPosition + 3;
                        }
                        else
                        {
                            _textbox.SelectionStart = iPosition + 4;
                        }
                    }


        }//input keyboard for number
        #endregion
        private void btn_7_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("7");
        }

        private void btn_8_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("8");
        }

        private void btn_9_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("9");
        }

        private void btn_ESC_Click(object sender, EventArgs e)
        {
            _textbox.EditValue = 0;
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            DeleteTextBox();
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            MoveToLeft();
        }

        private void btnMoveRightNumber_Click(object sender, EventArgs e)
        {
            MoveToRight();
        }

        private void btn_6_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("6");
        }

        private void btn_5_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("5");
        }

        private void btn_4_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("4");
        }

        private void btn_1_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("1");
        }

        private void btn_2_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("2");
        }

        private void btn_3_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("3");
        }

        private void btn_000_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("000");
        }

        private void btn_00_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("00");
        }

        private void btn_0_Click(object sender, EventArgs e)
        {
            InputKeyboardNumber("0");
        }

        private void btn_Thoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Payments_Click(object sender, EventArgs e)
        {
            SendKeys.Send("{Enter}");
        }

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                if (!ctltxt[int.Parse(txt.Tag.ToString())])
                {
                    txt.SelectAll();
                    ctltxt[int.Parse(txt.Tag.ToString())] = true;
                }
                else
                {
                    ctltxt[int.Parse(txt.Tag.ToString())] = false;
                }
            }
            catch
            {
            }
        }

        private void txtConfig_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if ( _textbox.Text == "")
                {
                    return;
                }
                string sSQL = "";
                sSQL += "Select MA_COMBO" + "\n";
                sSQL += "From COMBO_OPTION" + "\n";
                sSQL += "Where MA_COMBO=" + clsMain.SQLString(_Ma_Combo) + "\n";
                sSQL += "AND MA_HANGHOA=" + clsMain.SQLString(_Ma_hanghoa) + "\n";
                sSQL += "AND MA_HANGHOA_OPTION=" + clsMain.SQLString(_ma_hanghoathaythe) + "\n";
                DataTable dt = clsMain.ReturnDataTable(sSQL);

                sSQL = "";
                if (dt.Rows.Count > 0)
                {
                    sSQL += "Update COMBO_OPTION Set" + "\n";
                    sSQL += "SOLUONG=" + clsMain.SQLString(txtSL.Text) + ",";
                    sSQL += "GIATHEM=" + clsMain.SQLString(txtTiencongthem.Text.Replace(",","")) + "\n";
                    sSQL += "Where MA_COMBO=" + clsMain.SQLString(_Ma_Combo) + "\n";
                    sSQL += "AND MA_HANGHOA=" + clsMain.SQLString(_Ma_hanghoa) + "\n";
                    sSQL += "AND MA_HANGHOA_OPTION=" + clsMain.SQLString(_ma_hanghoathaythe) + "\n";
                }
                else
                {
                    sSQL += "Insert Into COMBO_OPTION (MA_COMBO,MA_HANGHOA,MA_HANGHOA_OPTION,SOLUONG,GIATHEM) " + "\n";
                    sSQL += "Values(";
                    sSQL += clsMain.SQLString(_Ma_Combo) + ",";
                    sSQL += clsMain.SQLString(_Ma_hanghoa) + ",";
                    sSQL += clsMain.SQLString(_ma_hanghoathaythe) + ",";
                    sSQL += clsMain.SQLString(txtSL.Text) + ",";
                    sSQL += clsMain.SQLString(txtTiencongthem.Text.Replace(",", "")) + ")";
                }
                clsMain.ExecuteSQL(sSQL);
                this.Close();
            }
        }

        private void DeleteTextBox()
        {
            int iPosition = _textbox.SelectionStart;
            int iLenght = _textbox.Text.Length;
            if (iPosition > 1)
            {
                if (_textbox.Text[iPosition - 1].ToString() == ",")
                {
                    _textbox.Text = _textbox.Text.Remove(iPosition - 2, 1);

                    _textbox.SelectionStart = iPosition - 2;

                }
                else
                {
                    _textbox.Text = _textbox.Text.Remove(iPosition - 1, 1);
                    if (iLenght - _textbox.Text.Length == 1)
                    {
                        _textbox.SelectionStart = iPosition - 1;
                    }
                    else
                    {
                        _textbox.SelectionStart = iPosition - 2;
                    }
                }
            }
            else if (iPosition == 1)
            {
                if (iLenght >= 2)
                {
                    _textbox.Text = _textbox.Text.Remove(iPosition - 1, 1);
                    _textbox.SelectionStart = iPosition - 1;
                    if (_textbox.Text == "0")
                        _textbox.SelectionStart = 1;
                }
                else
                {
                    _textbox.Text = "0";
                    _textbox.SelectionStart = 1;
                }
            }
        }

        private void MoveToLeft()
        {
            int iPosition = _textbox.SelectionStart;
            if (iPosition > 1)
            {
                if (_textbox.Text[iPosition - 1].ToString() == ",")
                {
                    _textbox.SelectionStart = iPosition - 2;
                }
                else
                {
                    _textbox.SelectionStart = iPosition - 1;
                }
            }
            else
            {
                _textbox.SelectionStart = 0;

            }
        }

        private void MoveToRight()
        {
            int iPosition = _textbox.SelectionStart;
            if (iPosition < _textbox.Text.Length - 1)
            {
                if (_textbox.Text[iPosition + 1].ToString() == ",")
                {
                    _textbox.SelectionStart = iPosition + 2;
                }
                else
                {
                    _textbox.SelectionStart = iPosition + 1;
                }
            }
            else
            {
                _textbox.SelectionStart = _textbox.Text.Length;
            }
        }

        private void txtGIA_Click(object sender, EventArgs e)
        {
            SetTextbox(txtTiencongthem);
        }

        private void txtSL_Click(object sender, EventArgs e)
        {
            SetTextbox(txtSL);
        }


        private void Frm_ConfigFuntion_Load(object sender, EventArgs e)
        {
            int dem = 1;
            foreach (Control ctl in panelControl2.Controls)
            {
                if (ctl is TextEdit)
                {
                    ctl.Tag = dem;
                    dem += 1;
                }
                ctl.Click += new EventHandler(ctl_Click);
            }
            ctltxt = new bool[dem];

            _textbox.SelectAll();


            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            this.Text = rm.GetString("doimathang", culture);
            btn_thoat.Text = rm.GetString("thoat", culture);
            lbHanghoagoc1.Text = rm.GetString("hanghoagoc", culture);
            lbHanghoathaydoi1.Text = rm.GetString("hanghoathaydoi", culture);
            lbSoLuong.Text = rm.GetString("soluong2", culture);
            lbTienCongThem.Text = rm.GetString("tiencongthem", culture);
        }

        private void txtSL_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int sl = int.Parse(txtSL.Text) * int.Parse(dtsl.Rows[0]["SOLUONG"].ToString());
                lbHanghoathaydoi.Text = dthhtt.Rows[0]["TEN"].ToString() + " (" + sl.ToString() + ")";
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.ToString(), "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}