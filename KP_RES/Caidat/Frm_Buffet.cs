﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Data.SqlClient;
using DevExpress.XtraEditors.Controls;
using System.Globalization;
using System.Resources;

using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using DevExpress.Utils;

namespace KP_RES
{
    public partial class Frm_Buffet : DevExpress.XtraEditors.XtraForm
    {

        bool bSetBufffet = false;
        public Frm_Buffet()
        {
            InitializeComponent();
            LoadPermission();
            cls_KP_RES.LoadBangCauHinh();
            LoadCombo();
            LoaddataGridView();
        }

        private void Frm_Buffet_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void LoadPermission()
        {
            btnThemBuffet.Enabled = clsUserManagement.AllowAdd("6");
            btnSuaBuffet.Enabled = clsUserManagement.AllowEdit("6");
            btnXoaBuffet.Enabled = clsUserManagement.AllowDelete("6");
            btnLuuBuffet.Enabled = btnThemBuffet.Enabled || btnSuaBuffet.Enabled;
        }

        private void LoadCombo()
        {
            string sSQL = "";
            DataTable dt = new DataTable();
            sSQL += "Select MA_HANGHOA As MA,TEN_HANGHOA As TEN" + "\n";
            sSQL += "From HANGHOA" + "\n";
            sSQL += "Where SUDUNG=1 ";
            sSQL += "Order by  TEN_HANGHOA ";
            dt = clsMain.ReturnDataTable(sSQL);

            dt.Rows.Add("0", "Không sử dụng");
            cbohanghoa.Properties.DataSource = dt;
            cbohanghoa.EditValue = cbohanghoa.Properties.GetDataSourceValue(cbohanghoa.Properties.ValueMember, 0);
        }

        string smaBuffet = "";
        DataTable dtBuffet = new DataTable();
        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);

            string sSQL = "";
            sSQL += "Update BUFFET_CT Set " + "\n";
            sSQL += "MA_NHOMHANG=(Select MA_NHOMHANG From HANGHOA Where HANGHOA.MA_HANGHOA= BUFFET_CT.MA_HANGHOA)" + "\n";
            sSQL += "Where MA_NHOMHANG="+clsMain .SQLString (" ") + "\n";
            sSQL += "Select MA_BUFFET As MA,TEN_BUFFET As TEN,GHICHU,SUDUNG,MONCHINH,STT" + "\n";
            sSQL += "From BUFFET" + "\n";
            sSQL += "Order by SUDUNG DESC,STT" + "\n";
            dtBuffet = clsMain.ReturnDataTable(sSQL);
            fpnlBuffet.Controls.Clear();
            fpnlNhomhang.Controls.Clear();
            fpnlHanghoa.Controls.Clear();
            foreach (DataRow dr in dtBuffet.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 86;
                btn.Height = 80;
                btn.Font = fpnlHanghoa.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.Name = dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(Buffet_Click);
                fpnlBuffet.Controls.Add(btn);
            }
            if (fpnlBuffet.VerticalScroll.Visible)
            {
                palBuffet.Width = 97;
            }
            else
            {
                palBuffet.Width = 97;
            }
        }

        private void Buffet_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in fpnlBuffet.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }

            smaBuffet = btn.Name;
            txtTENBUFFET.Text = dtBuffet.Select("MA=" + clsMain.SQLString(smaBuffet))[0]["TEN"].ToString();
            txtGHICHUBUFFET.Text = dtBuffet.Select("MA=" + clsMain.SQLString(smaBuffet))[0]["GHICHU"].ToString();
            txtSTTBUFFET.Text = dtBuffet.Select("MA=" + clsMain.SQLString(smaBuffet))[0]["STT"].ToString();
            chkSUDUNGBUFFET.Checked = bool.Parse(dtBuffet.Select("MA=" + clsMain.SQLString(smaBuffet))[0]["SUDUNG"].ToString());
            cbohanghoa.EditValue = dtBuffet.Select("MA=" + clsMain.SQLString(smaBuffet))[0]["MONCHINH"].ToString();
            KhoaMoControl(true);

            LoaddataGridView1(btn.Name);
        }

        private void SetRongControl()
        {
            smaBuffet = "";
            txtTENBUFFET.Text = "";
            txtGHICHUBUFFET.Text = "";
            txtSTTBUFFET.Text = "1";
            chkSUDUNGBUFFET.Checked = true;
            cbohanghoa.EditValue = cbohanghoa.Properties.GetDataSourceValue(cbohanghoa.Properties.ValueMember, 0);

        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//khóa
            {
                txtTENBUFFET.Properties.ReadOnly = true;
                txtGHICHUBUFFET.Properties.ReadOnly = true;
                txtSTTBUFFET.Properties.ReadOnly = true;
                chkSUDUNGBUFFET.Properties.ReadOnly = true;
                cbohanghoa.Properties.ReadOnly = true;
            }
            else//mở
            {
                txtTENBUFFET.Properties.ReadOnly = false;
                txtGHICHUBUFFET.Properties.ReadOnly = false;
                txtSTTBUFFET.Properties.ReadOnly = false;
                chkSUDUNGBUFFET.Properties.ReadOnly = false;
                cbohanghoa.Properties.ReadOnly = false ;
            }
        }

        private void btnThemNhomHang_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            txtTENBUFFET.Focus();
        }

        private void btnSuaNhomHang_Click(object sender, EventArgs e)
        {
            if (smaBuffet == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl(false);
            txtTENBUFFET.Focus();
        }

        private void btnXoaBuffet_Click(object sender, EventArgs e)
        {
            if (smaBuffet == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show(_xacnhanbanmuonxoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From BUFFET" + "\n";
            sSQL += "Where MA_BUFFET=" + clsMain.SQLString(smaBuffet) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLuuBuffet_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            string sSQL = "";
            if (smaBuffet == "" && btnThemBuffet.Enabled)
            {
                sSQL += "Insert into BUFFET (TEN_BUFFET,STT,MONCHINH,GHICHU,SUDUNG)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLStringUnicode(txtTENBUFFET.Text) + ",";
                sSQL += clsMain.SQLString (txtSTTBUFFET.Text) + ",";
                sSQL += clsMain.SQLString(cbohanghoa.EditValue.ToString ()) + ",";
                sSQL += clsMain.SQLStringUnicode(txtGHICHUBUFFET.Text) + ",";
                sSQL += clsMain.SQLBit(chkSUDUNGBUFFET.Checked) + ")";
            }
            else if ((smaBuffet != "" && btnSuaBuffet .Enabled))
            {
                sSQL += "Update BUFFET Set " + "\n";
                sSQL += "TEN_BUFFET=" + clsMain.SQLStringUnicode(txtTENBUFFET.Text) + "," + "\n";
                sSQL += "STT=" + clsMain.SQLString(txtSTTBUFFET.Text) + "," + "\n";
                sSQL += "MONCHINH=" + clsMain.SQLString(cbohanghoa.EditValue.ToString()) + "," + "\n";
                sSQL += "GHICHU=" + clsMain.SQLStringUnicode(txtGHICHUBUFFET.Text) + "," + "\n";
                sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNGBUFFET.Checked) + "\n";
                sSQL += "Where MA_BUFFET=" + clsMain.SQLString(smaBuffet) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
                btnThemBuffet .Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private Boolean CheckInput()
        {
            if (txtTENBUFFET.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_nhap + lblTENNHOMHANG.Text, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTENBUFFET.Focus();
                return false;
            }
            if (smaBuffet == "" && btnThemBuffet.Enabled && txtTENBUFFET.Text != "" && clsMain.ReturnDataTable("Select MA_BUFFET From BUFFET Where TEN_BUFFET=" + clsMain.SQLStringUnicode(txtTENBUFFET.Text)).Rows.Count > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show(_trungtennhomhang, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    txtTENBUFFET.Focus();
                    return false;
                }
            }
            if (smaBuffet != "" && btnSuaBuffet.Enabled && txtTENBUFFET.Text != "" && clsMain.ReturnDataTable("Select MA_BUFFET From BUFFET Where TEN_BUFFET=" + clsMain.SQLStringUnicode(txtTENBUFFET.Text) + " And MA_BUFFET!= " + clsMain.SQLString(smaBuffet)).Rows.Count > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show(_trungtennhomhang, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    txtTENBUFFET.Focus();
                    return false;
                }
            }
            return true;
        }

        private void btnLenBuffet_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlBuffet.AutoScrollPosition = new Point(0, fpnlBuffet.VerticalScroll.Value - fpnlBuffet.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnXuongBuffet_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlBuffet.AutoScrollPosition = new Point(0, fpnlBuffet.VerticalScroll.Value + fpnlBuffet.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        DataTable dtNhomhang = new DataTable();
        private void LoaddataGridView1(string maBuffet)
        {
            string sSQL = "";
            sSQL += "Select NHOMHANG.MA_NHOMHANG As MA,NHOMHANG.TEN_NHOMHANG As TEN,NHOMHANG.HINHANH, " + "\n";
            sSQL += "(Select TOP 1 MA_BUFFET From BUFFET_CT Where BUFFET_CT.MA_NHOMHANG=NHOMHANG.MA_NHOMHANG And BUFFET_CT.MA_BUFFET="+ clsMain .SQLString (smaBuffet )+" ) as MA_BUFFET" + "\n";
            sSQL += "From NHOMHANG " + "\n";
            sSQL += "Order by NHOMHANG.SUDUNG DESC,NHOMHANG.STT" + "\n";
            dtNhomhang = clsMain.ReturnDataTable(sSQL);
            fpnlNhomhang.Controls.Clear();
            fpnlHanghoa.Controls.Clear();
            foreach (DataRow dr in dtNhomhang.Rows)
            {
                Panel palGroup = new Panel();
                Panel palBtn = new Panel();
                Panel palChk = new Panel();

                CheckEdit chk = new CheckEdit();

                SimpleButton btn = new SimpleButton();
                btn.Font = new Font("Tahoma", 10);
                btn.Appearance.TextOptions.WordWrap = WordWrap.Wrap;
                btn.Name = dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(Nhomhang_Click);

                chk.Name = "chk" + dr["MA"].ToString();
                chk.Text = dr["TEN"].ToString();
                chk.Properties.ReadOnly = true;

                chk.Checked = dr["MA_BUFFET"].ToString() != "" ? true : false;
                if (dr["MA_BUFFET"].ToString() != "")
                    SetBackgroudOnClick(btn);
                else
                    SetDefautlBackgroud(btn);

                palGroup.Name = "G" + dr["MA"].ToString();
                palBtn.Name = "B" + dr["MA"].ToString();
                palChk.Name = "C" + dr["MA"].ToString();

                palGroup.Width = 125;
                palGroup.Height = 80;

                palChk.Width = 20;

                palChk.Dock = DockStyle.Right;
                palBtn.Dock = DockStyle.Fill;
                btn.Dock = DockStyle.Fill;
                chk.Dock = DockStyle.Fill;

                palChk.Controls.Add(chk);
                palBtn.Controls.Add(btn);
                palGroup.Controls.Add(palBtn);
                palGroup.Controls.Add(palChk);

                fpnlNhomhang.Controls.Add(palGroup);
            }
            if (fpnlNhomhang.VerticalScroll.Visible)
            {
                palNHOMHANG.Width = 152;
            }
            else
            {
                palNHOMHANG.Width = 135;
            }
        }

        private void SetBackgroudOnClick(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.YellowGreen;
            _btn.Appearance.BackColor2 = Color.YellowGreen;
            if (_btn.Appearance.Options.UseBackColor == false)
                _btn.Appearance.Options.UseBackColor = true;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == true)
                _btn.LookAndFeel.UseDefaultLookAndFeel = false;

        }

        private void SetDefautlBackgroud(SimpleButton _btn)
        {
            _btn.Appearance.BackColor = Color.Empty;
            if (_btn.Appearance.Options.UseBackColor == true)
                _btn.Appearance.Options.UseBackColor = false;
            _btn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
            if (_btn.LookAndFeel.UseDefaultLookAndFeel == false)
                _btn.LookAndFeel.UseDefaultLookAndFeel = true;
        }

        private void Nhomhang_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            LoaddataGridView2(btn.Name);
        }

        private void btnLenNhomhang_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlNhomhang.AutoScrollPosition = new Point(0, fpnlNhomhang.VerticalScroll.Value - fpnlNhomhang.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnXuongNhomhang_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlNhomhang.AutoScrollPosition = new Point(0, fpnlNhomhang.VerticalScroll.Value + fpnlNhomhang.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        DataTable dtHanghoa = new DataTable();
        private void LoaddataGridView2(string maNhomhang)
        {
            string sSQL = "";
            sSQL += "Select HANGHOA.MA_HANGHOA As MA,HANGHOA.TEN_HANGHOA As TEN,HANGHOA.HINHANH," + "\n";
            sSQL += "(Select top 1  MA_BUFFET From BUFFET_CT Where BUFFET_CT.MA_HANGHOA=HANGHOA.MA_HANGHOA And BUFFET_CT.MA_BUFFET=" + clsMain.SQLString(smaBuffet) + " ) as MA_BUFFET" + "\n";
            sSQL += "From HANGHOA" + "\n";
            sSQL += "Where MA_NHOMHANG=" + clsMain.SQLString(maNhomhang) + "\n";
            sSQL += "Order by HANGHOA.SUDUNG DESC,HANGHOA.STT" + "\n";
            dtHanghoa = clsMain.ReturnDataTable(sSQL);
            fpnlHanghoa.Controls.Clear();
            foreach (DataRow dr in dtHanghoa.Rows)
            {
                Panel palGroup = new Panel();
                Panel palBtn = new Panel();
                Panel palChk = new Panel();

                CheckEdit chk = new CheckEdit();

                SimpleButton btn = new SimpleButton();
                btn.Font = new Font("Tahoma", 10);
                btn.Appearance.TextOptions.WordWrap = WordWrap.Wrap;
                btn.Name = dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(HangHoa_Click);

                chk.Name = "chk" + dr["MA"].ToString();
                chk.Text = dr["TEN"].ToString();
                chk.Properties.ReadOnly = true;

                chk.Checked = dr["MA_BUFFET"].ToString() != "" ? true : false;
                if (dr["MA_BUFFET"].ToString() != "")
                    SetBackgroudOnClick(btn);
                else
                    SetDefautlBackgroud(btn);

                palGroup.Name = "G" + dr["MA"].ToString();
                palBtn.Name = "B" + dr["MA"].ToString();
                palChk.Name = "C" + dr["MA"].ToString();

                palGroup.Width = 168;
                palGroup.Height = 130;

                palChk.Width = 20;

                palChk.Dock = DockStyle.Right;
                palBtn.Dock = DockStyle.Fill;
                btn.Dock = DockStyle.Fill;
                chk.Dock = DockStyle.Fill;

                palChk.Controls.Add(chk);
                palBtn.Controls.Add(btn);
                palGroup.Controls.Add(palBtn);
                palGroup.Controls.Add(palChk);

                fpnlHanghoa.Controls.Add(palGroup);
            }
        }

        private void HangHoa_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            if (!bSetBufffet)
            {
                return;
            }

            foreach (Panel pn in fpnlHanghoa.Controls)
            {
                if (pn.Name == "G" + btn.Name)
                {
                    foreach (Panel pn1 in pn.Controls)
                    {
                        if (pn1.Name.Substring(0, 1) == "C")
                        {
                            foreach (CheckEdit chk1 in pn1.Controls)
                            {
                                if (btn.Appearance.BackColor == btnLenHangHoa.Appearance.BackColor)
                                {
                                    chk1.Checked = true;
                                    SetBackgroudOnClick(btn);
                                    String sSQL = "";
                                    sSQL += "Insert into BUFFET_CT (MA_BUFFET,MA_NHOMHANG,MA_HANGHOA)" + "\n";
                                    sSQL += "Values ( ";
                                    sSQL += clsMain.SQLString(smaBuffet) + ",";
                                    sSQL += clsMain.SQLString(" ") + ",";
                                    sSQL += clsMain.SQLString(btn.Name) + ")";
                                    clsMain.ExecuteSQL(sSQL);
                                }
                                else
                                {
                                    chk1.Checked = false;
                                    SetDefautlBackgroud(btn);
                                    String sSQL = "";
                                    sSQL += "Delete From BUFFET_CT" + "\n";
                                    sSQL += "Where MA_BUFFET=" + clsMain.SQLString(smaBuffet) + "\n";
                                    sSQL += "And MA_HANGHOA=" + clsMain.SQLString(btn.Name) + "\n";
                                    clsMain.ExecuteSQL(sSQL);
                                }
                                return;
                            }
                        }
                    }
                }
            }
        }

        private void btnLenHangHoa_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlHanghoa.AutoScrollPosition = new Point(0, fpnlHanghoa.VerticalScroll.Value - fpnlHanghoa.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnXuongHangHoa_Click(object sender, EventArgs e)
        {
            try
            {
                fpnlHanghoa.AutoScrollPosition = new Point(0, fpnlHanghoa.VerticalScroll.Value + fpnlHanghoa.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnChonmon_Click(object sender, EventArgs e)
        {
            if (bSetBufffet == false)
            {
                bSetBufffet = true;
                SetBackgroudOnClick(btnChonmon);
            }
            else
            {
                bSetBufffet = false;
                SetDefautlBackgroud(btnChonmon);
            }

            LoaddataGridView();

        }

        string _trungtennhomhang = "";
        string _nhap = "";
        string _lietketatca = "";
        string _trangchu = "";
        string _thongbao = "";
        string _luukothanhcong = "";
        string _xacnhanbanmuonxoa = "";
        string _nhapmahanghoa = "";
        string _trungmahh = "";
        string _trungtenhanghoa = "";
        string _trungmavach = "";
        string _chonanh = "";
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void Frm_Buffet_Load(object sender, EventArgs e)
        {
            foreach (Control ctl in palBuffet.Controls)
            {
                ctl.Click += new EventHandler(ctl_Click);
            }
         
            lblTENNHOMHANG.Text = rm.GetString("ten", culture);
            lbSTT.Text = rm.GetString("stt", culture);
            lblGHICHU.Text = rm.GetString("ghichu", culture);
          
            btnLuuBuffet.Text = rm.GetString("luu", culture);
            btnThemBuffet.Text = rm.GetString("them", culture);
            btnSuaBuffet.Text = rm.GetString("sua", culture);
            btnXoaBuffet.Text = rm.GetString("xoa", culture);
            
            _thongbao = rm.GetString("thongbao", culture);
            _lietketatca = rm.GetString("lietketatca", culture);
            _trangchu = rm.GetString("trangchu", culture);
            _nhap = rm.GetString("nhap", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);
            _xacnhanbanmuonxoa = rm.GetString("xacnhanbanmuonxoa", culture);
            _nhapmahanghoa = rm.GetString("nhapmahanghoa", culture);
            _trungmahh = rm.GetString("trungmahh", culture);
            _trungtenhanghoa = rm.GetString("trungtenhanghoa", culture);
            _trungmavach = rm.GetString("trungmavach", culture);
            _chonanh = rm.GetString("chonanh", culture);
        }

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                txt.SelectAll();
            }
            catch
            {
            }
        }

     

    }
}