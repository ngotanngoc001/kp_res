﻿namespace KP_RES 
{
    partial class Frm_NhomHanghoa 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_NhomHanghoa));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.palLIETKE = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ID_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_DONVITINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_NHOMHANG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAVACH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PLU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIANHAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIABAN1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIABAN2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TONKHO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.THUCDON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MONTHEM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GHICHU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_NHOMHANG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_DONVITINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SUDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SUAGIA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HINHANH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IS_COMBO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SUADINHLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IS_INBEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIATHEOTRONGLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl13 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl14 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.palHANGHOA = new DevExpress.XtraEditors.PanelControl();
            this.fpnlHanghoa = new System.Windows.Forms.FlowLayoutPanel();
            this.btnXuongHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.palNHOMHANG = new DevExpress.XtraEditors.PanelControl();
            this.fpnlNhomhang = new System.Windows.Forms.FlowLayoutPanel();
            this.btnXuongNhomhang = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenNhomhang = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pnlHanghoa = new DevExpress.XtraEditors.PanelControl();
            this.palControl = new DevExpress.XtraEditors.PanelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtLoinhuan = new DevExpress.XtraEditors.TextEdit();
            this.txtGIABAN4 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtGIABAN3 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.chkINTEM = new DevExpress.XtraEditors.CheckEdit();
            this.chkMONTHEM = new DevExpress.XtraEditors.CheckEdit();
            this.txtHSD = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.cboPLU = new DevExpress.XtraEditors.LookUpEdit();
            this.panelControl17 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.chkGiatheotrongluong = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl16 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.btnAutoMaVach = new DevExpress.XtraEditors.SimpleButton();
            this.btnFileHangHoa_Mo = new DevExpress.XtraEditors.SimpleButton();
            this.btnFileHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.chkSuaDinhLuong = new DevExpress.XtraEditors.CheckEdit();
            this.chkMayinbep = new DevExpress.XtraEditors.CheckEdit();
            this.btnAUTO = new DevExpress.XtraEditors.SimpleButton();
            this.lbMa = new DevExpress.XtraEditors.LabelControl();
            this.txtMAHANGHOA = new DevExpress.XtraEditors.TextEdit();
            this.chkSUAGIA = new DevExpress.XtraEditors.CheckEdit();
            this.txtMAVACH = new DevExpress.XtraEditors.TextEdit();
            this.lbMaVach = new DevExpress.XtraEditors.LabelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.txtGIABAN2 = new DevExpress.XtraEditors.TextEdit();
            this.lbGiaBan2 = new DevExpress.XtraEditors.LabelControl();
            this.txtGIABAN1 = new DevExpress.XtraEditors.TextEdit();
            this.lbGiaBan1 = new DevExpress.XtraEditors.LabelControl();
            this.txtGIAMUA = new DevExpress.XtraEditors.TextEdit();
            this.lbGiaMua = new DevExpress.XtraEditors.LabelControl();
            this.lbThue = new DevExpress.XtraEditors.LabelControl();
            this.lblTONTOITHIEU = new DevExpress.XtraEditors.LabelControl();
            this.txtTONTOITHIEU = new DevExpress.XtraEditors.TextEdit();
            this.chkSUDUNGHANGHOA = new DevExpress.XtraEditors.CheckEdit();
            this.txtSOLUONG = new DevExpress.XtraEditors.TextEdit();
            this.cboTHANHPHAN = new DevExpress.XtraEditors.LookUpEdit();
            this.lbThanhPhan = new DevExpress.XtraEditors.LabelControl();
            this.btnLuuHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnSuaHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.chkTONKHO = new DevExpress.XtraEditors.CheckEdit();
            this.cboNhomhang = new DevExpress.XtraEditors.LookUpEdit();
            this.lbNhom = new DevExpress.XtraEditors.LabelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.picHANGHOA = new System.Windows.Forms.PictureBox();
            this.txtGHICHUHANGHOA = new DevExpress.XtraEditors.TextEdit();
            this.lbSTT1 = new DevExpress.XtraEditors.LabelControl();
            this.lblTENHANGHOA = new DevExpress.XtraEditors.LabelControl();
            this.txtTENHANGHOA = new DevExpress.XtraEditors.TextEdit();
            this.txtSTTHANGHOA = new DevExpress.XtraEditors.TextEdit();
            this.lbGhiChu1 = new DevExpress.XtraEditors.LabelControl();
            this.cboBep = new DevExpress.XtraEditors.LookUpEdit();
            this.lblDVT = new DevExpress.XtraEditors.LabelControl();
            this.cboDonvitinh = new DevExpress.XtraEditors.LookUpEdit();
            this.cboThue = new DevExpress.XtraEditors.ComboBoxEdit();
            this.pnlNhomhang = new DevExpress.XtraEditors.PanelControl();
            this.chkMONTHEM_NH = new DevExpress.XtraEditors.CheckEdit();
            this.btnFileNhomhang_Mo = new DevExpress.XtraEditors.SimpleButton();
            this.btnFileNhomhang = new DevExpress.XtraEditors.SimpleButton();
            this.palMayinbep = new DevExpress.XtraEditors.PanelControl();
            this.chkMayinbepNhom = new DevExpress.XtraEditors.CheckEdit();
            this.cboBepNhom = new DevExpress.XtraEditors.LookUpEdit();
            this.chkSuamayinbep = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.picNHOMHANG = new System.Windows.Forms.PictureBox();
            this.lbSTT = new DevExpress.XtraEditors.LabelControl();
            this.lblTENNHOMHANG = new DevExpress.XtraEditors.LabelControl();
            this.btnLuuNhomHang = new DevExpress.XtraEditors.SimpleButton();
            this.txtTENNHOMHANG = new DevExpress.XtraEditors.TextEdit();
            this.btnSuaNhomHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaNhomHang = new DevExpress.XtraEditors.SimpleButton();
            this.lblGHICHU = new DevExpress.XtraEditors.LabelControl();
            this.btnThemNhomHang = new DevExpress.XtraEditors.SimpleButton();
            this.chkSUDUNGNHOMHANG = new DevExpress.XtraEditors.CheckEdit();
            this.chkTHUCDONNHOMHANG = new DevExpress.XtraEditors.CheckEdit();
            this.txtSTTNHOMHANG = new DevExpress.XtraEditors.TextEdit();
            this.txtGHICHUNHOMHANG = new DevExpress.XtraEditors.TextEdit();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnLIETKE = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.btnChonNhomhang = new DevExpress.XtraEditors.SimpleButton();
            this.btnChonHanghoa = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palLIETKE)).BeginInit();
            this.palLIETKE.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).BeginInit();
            this.panelControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.palHANGHOA)).BeginInit();
            this.palHANGHOA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palNHOMHANG)).BeginInit();
            this.palNHOMHANG.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHanghoa)).BeginInit();
            this.pnlHanghoa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palControl)).BeginInit();
            this.palControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLoinhuan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkINTEM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMONTHEM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHSD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboPLU.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGiatheotrongluong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSuaDinhLuong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMayinbep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMAHANGHOA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUAGIA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMAVACH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIAMUA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTONTOITHIEU.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGHANGHOA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSOLUONG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTHANHPHAN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTONKHO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhomhang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHANGHOA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUHANGHOA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENHANGHOA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTTHANGHOA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboBep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDonvitinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboThue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlNhomhang)).BeginInit();
            this.pnlNhomhang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMONTHEM_NH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.palMayinbep)).BeginInit();
            this.palMayinbep.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMayinbepNhom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboBepNhom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSuamayinbep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picNHOMHANG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENNHOMHANG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGNHOMHANG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTHUCDONNHOMHANG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTTNHOMHANG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUNHOMHANG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.palLIETKE);
            this.panelControl1.Controls.Add(this.palHANGHOA);
            this.panelControl1.Controls.Add(this.palNHOMHANG);
            this.panelControl1.Controls.Add(this.panelControl4);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1008, 729);
            this.panelControl1.TabIndex = 0;
            // 
            // palLIETKE
            // 
            this.palLIETKE.Controls.Add(this.panelControl2);
            this.palLIETKE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palLIETKE.Location = new System.Drawing.Point(2, 2);
            this.palLIETKE.Name = "palLIETKE";
            this.palLIETKE.Size = new System.Drawing.Size(519, 725);
            this.palLIETKE.TabIndex = 19;
            this.palLIETKE.Visible = false;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridControl1);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(515, 721);
            this.panelControl2.TabIndex = 6;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(472, 717);
            this.gridControl1.TabIndex = 3;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT1,
            this.ID_HANGHOA,
            this.TEN_HANGHOA,
            this.TEN_DONVITINH,
            this.TEN_NHOMHANG,
            this.MAVACH,
            this.PLU,
            this.THUE,
            this.GIANHAP,
            this.GIABAN1,
            this.GIABAN2,
            this.TONKHO,
            this.THUCDON,
            this.MONTHEM,
            this.GHICHU,
            this.MA_NHOMHANG,
            this.MA_HANGHOA,
            this.MA_DONVITINH,
            this.STT,
            this.SUDUNG,
            this.SUAGIA,
            this.HINHANH,
            this.IS_COMBO,
            this.SUADINHLUONG,
            this.IS_INBEP,
            this.GIATHEOTRONGLUONG});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView1.GroupRowHeight = 30;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.ShowCloseButton = false;
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsPrint.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.TEN_HANGHOA, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // STT1
            // 
            this.STT1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.STT1.AppearanceCell.Options.UseFont = true;
            this.STT1.AppearanceCell.Options.UseTextOptions = true;
            this.STT1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT1.AppearanceHeader.Options.UseFont = true;
            this.STT1.AppearanceHeader.Options.UseTextOptions = true;
            this.STT1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT1.Caption = "STT";
            this.STT1.FieldName = "STT1";
            this.STT1.Name = "STT1";
            this.STT1.OptionsColumn.AllowEdit = false;
            this.STT1.OptionsColumn.AllowFocus = false;
            this.STT1.OptionsColumn.FixedWidth = true;
            this.STT1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.STT1.Visible = true;
            this.STT1.VisibleIndex = 0;
            this.STT1.Width = 54;
            // 
            // ID_HANGHOA
            // 
            this.ID_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.ID_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.ID_HANGHOA.AppearanceCell.Options.UseTextOptions = true;
            this.ID_HANGHOA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ID_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.ID_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.ID_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.ID_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ID_HANGHOA.Caption = "Mã";
            this.ID_HANGHOA.DisplayFormat.FormatString = "{0:000}";
            this.ID_HANGHOA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ID_HANGHOA.FieldName = "ID_HANGHOA";
            this.ID_HANGHOA.Name = "ID_HANGHOA";
            this.ID_HANGHOA.OptionsColumn.AllowEdit = false;
            this.ID_HANGHOA.OptionsColumn.AllowFocus = false;
            this.ID_HANGHOA.OptionsColumn.FixedWidth = true;
            this.ID_HANGHOA.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.ID_HANGHOA.Visible = true;
            this.ID_HANGHOA.VisibleIndex = 1;
            this.ID_HANGHOA.Width = 77;
            // 
            // TEN_HANGHOA
            // 
            this.TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_HANGHOA.Caption = "Tên hàng hóa";
            this.TEN_HANGHOA.FieldName = "TEN_HANGHOA";
            this.TEN_HANGHOA.Name = "TEN_HANGHOA";
            this.TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this.TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this.TEN_HANGHOA.OptionsColumn.FixedWidth = true;
            this.TEN_HANGHOA.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.TEN_HANGHOA.Visible = true;
            this.TEN_HANGHOA.VisibleIndex = 2;
            this.TEN_HANGHOA.Width = 240;
            // 
            // TEN_DONVITINH
            // 
            this.TEN_DONVITINH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TEN_DONVITINH.AppearanceCell.Options.UseFont = true;
            this.TEN_DONVITINH.AppearanceCell.Options.UseTextOptions = true;
            this.TEN_DONVITINH.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_DONVITINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_DONVITINH.AppearanceHeader.Options.UseFont = true;
            this.TEN_DONVITINH.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_DONVITINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_DONVITINH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_DONVITINH.Caption = "ĐVT";
            this.TEN_DONVITINH.FieldName = "TEN_DONVITINH";
            this.TEN_DONVITINH.Name = "TEN_DONVITINH";
            this.TEN_DONVITINH.OptionsColumn.AllowEdit = false;
            this.TEN_DONVITINH.OptionsColumn.AllowFocus = false;
            this.TEN_DONVITINH.OptionsColumn.FixedWidth = true;
            this.TEN_DONVITINH.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.TEN_DONVITINH.Visible = true;
            this.TEN_DONVITINH.VisibleIndex = 3;
            // 
            // TEN_NHOMHANG
            // 
            this.TEN_NHOMHANG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TEN_NHOMHANG.AppearanceCell.Options.UseFont = true;
            this.TEN_NHOMHANG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_NHOMHANG.AppearanceHeader.Options.UseFont = true;
            this.TEN_NHOMHANG.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_NHOMHANG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_NHOMHANG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_NHOMHANG.Caption = "Nhóm";
            this.TEN_NHOMHANG.FieldName = "TEN_NHOMHANG";
            this.TEN_NHOMHANG.Name = "TEN_NHOMHANG";
            this.TEN_NHOMHANG.OptionsColumn.AllowEdit = false;
            this.TEN_NHOMHANG.OptionsColumn.AllowFocus = false;
            this.TEN_NHOMHANG.OptionsColumn.FixedWidth = true;
            this.TEN_NHOMHANG.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.TEN_NHOMHANG.Visible = true;
            this.TEN_NHOMHANG.VisibleIndex = 4;
            this.TEN_NHOMHANG.Width = 150;
            // 
            // MAVACH
            // 
            this.MAVACH.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MAVACH.AppearanceCell.Options.UseFont = true;
            this.MAVACH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MAVACH.AppearanceHeader.Options.UseFont = true;
            this.MAVACH.AppearanceHeader.Options.UseTextOptions = true;
            this.MAVACH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MAVACH.Caption = "Mã vạch";
            this.MAVACH.FieldName = "MAVACH";
            this.MAVACH.Name = "MAVACH";
            this.MAVACH.OptionsColumn.AllowEdit = false;
            this.MAVACH.OptionsColumn.AllowFocus = false;
            this.MAVACH.OptionsColumn.FixedWidth = true;
            this.MAVACH.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.MAVACH.Visible = true;
            this.MAVACH.VisibleIndex = 5;
            this.MAVACH.Width = 120;
            // 
            // PLU
            // 
            this.PLU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.PLU.AppearanceCell.Options.UseFont = true;
            this.PLU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.PLU.AppearanceHeader.Options.UseFont = true;
            this.PLU.AppearanceHeader.Options.UseTextOptions = true;
            this.PLU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PLU.Caption = "PLU";
            this.PLU.FieldName = "PLU";
            this.PLU.Name = "PLU";
            this.PLU.OptionsColumn.AllowEdit = false;
            this.PLU.OptionsColumn.AllowFocus = false;
            this.PLU.OptionsColumn.FixedWidth = true;
            this.PLU.Visible = true;
            this.PLU.VisibleIndex = 6;
            // 
            // THUE
            // 
            this.THUE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.THUE.AppearanceCell.Options.UseFont = true;
            this.THUE.AppearanceCell.Options.UseTextOptions = true;
            this.THUE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.THUE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THUE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THUE.AppearanceHeader.Options.UseFont = true;
            this.THUE.AppearanceHeader.Options.UseTextOptions = true;
            this.THUE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THUE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.THUE.Caption = "Thuế";
            this.THUE.DisplayFormat.FormatString = "{0:#,###0}%";
            this.THUE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.THUE.FieldName = "THUE";
            this.THUE.Name = "THUE";
            this.THUE.OptionsColumn.AllowEdit = false;
            this.THUE.OptionsColumn.AllowFocus = false;
            this.THUE.OptionsColumn.FixedWidth = true;
            this.THUE.Visible = true;
            this.THUE.VisibleIndex = 7;
            this.THUE.Width = 65;
            // 
            // GIANHAP
            // 
            this.GIANHAP.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.GIANHAP.AppearanceCell.Options.UseFont = true;
            this.GIANHAP.AppearanceCell.Options.UseTextOptions = true;
            this.GIANHAP.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIANHAP.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIANHAP.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIANHAP.AppearanceHeader.Options.UseFont = true;
            this.GIANHAP.AppearanceHeader.Options.UseTextOptions = true;
            this.GIANHAP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIANHAP.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIANHAP.Caption = "Giá nhập";
            this.GIANHAP.DisplayFormat.FormatString = "{0:#,###0}";
            this.GIANHAP.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GIANHAP.FieldName = "GIANHAP";
            this.GIANHAP.Name = "GIANHAP";
            this.GIANHAP.OptionsColumn.AllowEdit = false;
            this.GIANHAP.OptionsColumn.AllowFocus = false;
            this.GIANHAP.OptionsColumn.FixedWidth = true;
            this.GIANHAP.Visible = true;
            this.GIANHAP.VisibleIndex = 8;
            this.GIANHAP.Width = 110;
            // 
            // GIABAN1
            // 
            this.GIABAN1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.GIABAN1.AppearanceCell.Options.UseFont = true;
            this.GIABAN1.AppearanceCell.Options.UseTextOptions = true;
            this.GIABAN1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIABAN1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIABAN1.AppearanceHeader.Options.UseFont = true;
            this.GIABAN1.AppearanceHeader.Options.UseTextOptions = true;
            this.GIABAN1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIABAN1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN1.Caption = "Giá bán 1";
            this.GIABAN1.DisplayFormat.FormatString = "{0:#,###0}";
            this.GIABAN1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GIABAN1.FieldName = "GIABAN1";
            this.GIABAN1.Name = "GIABAN1";
            this.GIABAN1.OptionsColumn.AllowEdit = false;
            this.GIABAN1.OptionsColumn.AllowFocus = false;
            this.GIABAN1.OptionsColumn.FixedWidth = true;
            this.GIABAN1.Visible = true;
            this.GIABAN1.VisibleIndex = 9;
            this.GIABAN1.Width = 110;
            // 
            // GIABAN2
            // 
            this.GIABAN2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.GIABAN2.AppearanceCell.Options.UseFont = true;
            this.GIABAN2.AppearanceCell.Options.UseTextOptions = true;
            this.GIABAN2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIABAN2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIABAN2.AppearanceHeader.Options.UseFont = true;
            this.GIABAN2.AppearanceHeader.Options.UseTextOptions = true;
            this.GIABAN2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIABAN2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN2.Caption = "Giá bán 2";
            this.GIABAN2.DisplayFormat.FormatString = "{0:#,###0}";
            this.GIABAN2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GIABAN2.FieldName = "GIABAN2";
            this.GIABAN2.Name = "GIABAN2";
            this.GIABAN2.OptionsColumn.AllowEdit = false;
            this.GIABAN2.OptionsColumn.AllowFocus = false;
            this.GIABAN2.OptionsColumn.FixedWidth = true;
            this.GIABAN2.Visible = true;
            this.GIABAN2.VisibleIndex = 10;
            this.GIABAN2.Width = 110;
            // 
            // TONKHO
            // 
            this.TONKHO.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TONKHO.AppearanceCell.Options.UseFont = true;
            this.TONKHO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TONKHO.AppearanceHeader.Options.UseFont = true;
            this.TONKHO.AppearanceHeader.Options.UseTextOptions = true;
            this.TONKHO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TONKHO.Caption = "Tồn kho";
            this.TONKHO.FieldName = "TONKHO";
            this.TONKHO.Name = "TONKHO";
            this.TONKHO.OptionsColumn.AllowEdit = false;
            this.TONKHO.OptionsColumn.AllowFocus = false;
            this.TONKHO.OptionsColumn.FixedWidth = true;
            this.TONKHO.Visible = true;
            this.TONKHO.VisibleIndex = 11;
            this.TONKHO.Width = 106;
            // 
            // THUCDON
            // 
            this.THUCDON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.THUCDON.AppearanceHeader.Options.UseFont = true;
            this.THUCDON.AppearanceHeader.Options.UseTextOptions = true;
            this.THUCDON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.THUCDON.Caption = "Thực đơn";
            this.THUCDON.FieldName = "THUCDON";
            this.THUCDON.Name = "THUCDON";
            this.THUCDON.OptionsColumn.AllowEdit = false;
            this.THUCDON.OptionsColumn.AllowFocus = false;
            this.THUCDON.OptionsColumn.FixedWidth = true;
            this.THUCDON.Visible = true;
            this.THUCDON.VisibleIndex = 12;
            this.THUCDON.Width = 124;
            // 
            // MONTHEM
            // 
            this.MONTHEM.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MONTHEM.AppearanceCell.Options.UseFont = true;
            this.MONTHEM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MONTHEM.AppearanceHeader.Options.UseFont = true;
            this.MONTHEM.AppearanceHeader.Options.UseTextOptions = true;
            this.MONTHEM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MONTHEM.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MONTHEM.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MONTHEM.Caption = "Món thêm";
            this.MONTHEM.FieldName = "MONTHEM";
            this.MONTHEM.Name = "MONTHEM";
            this.MONTHEM.OptionsColumn.AllowEdit = false;
            this.MONTHEM.OptionsColumn.AllowFocus = false;
            this.MONTHEM.OptionsColumn.FixedWidth = true;
            this.MONTHEM.Visible = true;
            this.MONTHEM.VisibleIndex = 13;
            this.MONTHEM.Width = 106;
            // 
            // GHICHU
            // 
            this.GHICHU.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.GHICHU.AppearanceCell.Options.UseFont = true;
            this.GHICHU.AppearanceCell.Options.UseTextOptions = true;
            this.GHICHU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GHICHU.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GHICHU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GHICHU.AppearanceHeader.Options.UseFont = true;
            this.GHICHU.AppearanceHeader.Options.UseTextOptions = true;
            this.GHICHU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GHICHU.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GHICHU.Caption = "Ghi chú";
            this.GHICHU.FieldName = "GHICHU";
            this.GHICHU.Name = "GHICHU";
            this.GHICHU.OptionsColumn.AllowEdit = false;
            this.GHICHU.OptionsColumn.AllowFocus = false;
            this.GHICHU.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.GHICHU.Visible = true;
            this.GHICHU.VisibleIndex = 14;
            this.GHICHU.Width = 227;
            // 
            // MA_NHOMHANG
            // 
            this.MA_NHOMHANG.Caption = "MA_NHOMHANG";
            this.MA_NHOMHANG.FieldName = "MA_NHOMHANG";
            this.MA_NHOMHANG.Name = "MA_NHOMHANG";
            // 
            // MA_HANGHOA
            // 
            this.MA_HANGHOA.Caption = "MA_HANGHOA";
            this.MA_HANGHOA.FieldName = "MA_HANGHOA";
            this.MA_HANGHOA.Name = "MA_HANGHOA";
            // 
            // MA_DONVITINH
            // 
            this.MA_DONVITINH.Caption = "MA_DONVITINH";
            this.MA_DONVITINH.FieldName = "MA_DONVITINH";
            this.MA_DONVITINH.Name = "MA_DONVITINH";
            // 
            // STT
            // 
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            // 
            // SUDUNG
            // 
            this.SUDUNG.Caption = "SUDUNG";
            this.SUDUNG.FieldName = "SUDUNG";
            this.SUDUNG.Name = "SUDUNG";
            // 
            // SUAGIA
            // 
            this.SUAGIA.Caption = "SUAGIA";
            this.SUAGIA.FieldName = "SUAGIA";
            this.SUAGIA.Name = "SUAGIA";
            // 
            // HINHANH
            // 
            this.HINHANH.Caption = "HINHANH";
            this.HINHANH.FieldName = "HINHANH";
            this.HINHANH.Name = "HINHANH";
            // 
            // IS_COMBO
            // 
            this.IS_COMBO.Caption = "IS_COMBO";
            this.IS_COMBO.FieldName = "IS_COMBO";
            this.IS_COMBO.Name = "IS_COMBO";
            // 
            // SUADINHLUONG
            // 
            this.SUADINHLUONG.Caption = "SUADINHLUONG";
            this.SUADINHLUONG.FieldName = "SUADINHLUONG";
            this.SUADINHLUONG.Name = "SUADINHLUONG";
            // 
            // IS_INBEP
            // 
            this.IS_INBEP.Caption = "IS_INBEP";
            this.IS_INBEP.FieldName = "IS_INBEP";
            this.IS_INBEP.Name = "IS_INBEP";
            // 
            // GIATHEOTRONGLUONG
            // 
            this.GIATHEOTRONGLUONG.Caption = "GIATHEOTRONGLUONG";
            this.GIATHEOTRONGLUONG.FieldName = "GIATHEOTRONGLUONG";
            this.GIATHEOTRONGLUONG.Name = "GIATHEOTRONGLUONG";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.panelControl13);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl3.Location = new System.Drawing.Point(474, 2);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(39, 717);
            this.panelControl3.TabIndex = 7;
            // 
            // panelControl13
            // 
            this.panelControl13.AutoSize = true;
            this.panelControl13.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl13.Controls.Add(this.panelControl14);
            this.panelControl13.Controls.Add(this.btnXuongit);
            this.panelControl13.Controls.Add(this.btnXuongnhieu);
            this.panelControl13.Controls.Add(this.btnLenit);
            this.panelControl13.Controls.Add(this.btnLennhieu);
            this.panelControl13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl13.Location = new System.Drawing.Point(2, 2);
            this.panelControl13.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl13.Name = "panelControl13";
            this.panelControl13.Size = new System.Drawing.Size(35, 713);
            this.panelControl13.TabIndex = 14;
            // 
            // panelControl14
            // 
            this.panelControl14.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl14.Location = new System.Drawing.Point(0, 160);
            this.panelControl14.Name = "panelControl14";
            this.panelControl14.Size = new System.Drawing.Size(35, 393);
            this.panelControl14.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 553);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 633);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // palHANGHOA
            // 
            this.palHANGHOA.Controls.Add(this.fpnlHanghoa);
            this.palHANGHOA.Controls.Add(this.btnXuongHangHoa);
            this.palHANGHOA.Controls.Add(this.btnLenHangHoa);
            this.palHANGHOA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palHANGHOA.Location = new System.Drawing.Point(2, 2);
            this.palHANGHOA.Name = "palHANGHOA";
            this.palHANGHOA.Size = new System.Drawing.Size(519, 725);
            this.palHANGHOA.TabIndex = 0;
            // 
            // fpnlHanghoa
            // 
            this.fpnlHanghoa.AutoScroll = true;
            this.fpnlHanghoa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpnlHanghoa.Font = new System.Drawing.Font("Tahoma", 12F);
            this.fpnlHanghoa.Location = new System.Drawing.Point(2, 37);
            this.fpnlHanghoa.Name = "fpnlHanghoa";
            this.fpnlHanghoa.Size = new System.Drawing.Size(515, 651);
            this.fpnlHanghoa.TabIndex = 18;
            // 
            // btnXuongHangHoa
            // 
            this.btnXuongHangHoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongHangHoa.Appearance.Options.UseFont = true;
            this.btnXuongHangHoa.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongHangHoa.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongHangHoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongHangHoa.Location = new System.Drawing.Point(2, 688);
            this.btnXuongHangHoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongHangHoa.Name = "btnXuongHangHoa";
            this.btnXuongHangHoa.Size = new System.Drawing.Size(515, 35);
            this.btnXuongHangHoa.TabIndex = 17;
            this.btnXuongHangHoa.Click += new System.EventHandler(this.btnXuongHangHoa_Click);
            // 
            // btnLenHangHoa
            // 
            this.btnLenHangHoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenHangHoa.Appearance.Options.UseFont = true;
            this.btnLenHangHoa.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenHangHoa.Image = ((System.Drawing.Image)(resources.GetObject("btnLenHangHoa.Image")));
            this.btnLenHangHoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenHangHoa.Location = new System.Drawing.Point(2, 2);
            this.btnLenHangHoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnLenHangHoa.Name = "btnLenHangHoa";
            this.btnLenHangHoa.Size = new System.Drawing.Size(515, 35);
            this.btnLenHangHoa.TabIndex = 16;
            this.btnLenHangHoa.Click += new System.EventHandler(this.btnLenHangHoa_Click);
            // 
            // palNHOMHANG
            // 
            this.palNHOMHANG.Controls.Add(this.fpnlNhomhang);
            this.palNHOMHANG.Controls.Add(this.btnXuongNhomhang);
            this.palNHOMHANG.Controls.Add(this.btnLenNhomhang);
            this.palNHOMHANG.Dock = System.Windows.Forms.DockStyle.Right;
            this.palNHOMHANG.Location = new System.Drawing.Point(521, 2);
            this.palNHOMHANG.Margin = new System.Windows.Forms.Padding(0);
            this.palNHOMHANG.Name = "palNHOMHANG";
            this.palNHOMHANG.Size = new System.Drawing.Size(135, 725);
            this.palNHOMHANG.TabIndex = 1;
            // 
            // fpnlNhomhang
            // 
            this.fpnlNhomhang.AutoScroll = true;
            this.fpnlNhomhang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpnlNhomhang.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fpnlNhomhang.Location = new System.Drawing.Point(2, 37);
            this.fpnlNhomhang.Margin = new System.Windows.Forms.Padding(0);
            this.fpnlNhomhang.Name = "fpnlNhomhang";
            this.fpnlNhomhang.Size = new System.Drawing.Size(131, 651);
            this.fpnlNhomhang.TabIndex = 17;
            // 
            // btnXuongNhomhang
            // 
            this.btnXuongNhomhang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongNhomhang.Appearance.Options.UseFont = true;
            this.btnXuongNhomhang.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongNhomhang.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongNhomhang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongNhomhang.Location = new System.Drawing.Point(2, 688);
            this.btnXuongNhomhang.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongNhomhang.Name = "btnXuongNhomhang";
            this.btnXuongNhomhang.Size = new System.Drawing.Size(131, 35);
            this.btnXuongNhomhang.TabIndex = 16;
            this.btnXuongNhomhang.Click += new System.EventHandler(this.btnXuongNhomhang_Click);
            // 
            // btnLenNhomhang
            // 
            this.btnLenNhomhang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenNhomhang.Appearance.Options.UseFont = true;
            this.btnLenNhomhang.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenNhomhang.Image = ((System.Drawing.Image)(resources.GetObject("btnLenNhomhang.Image")));
            this.btnLenNhomhang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenNhomhang.Location = new System.Drawing.Point(2, 2);
            this.btnLenNhomhang.Margin = new System.Windows.Forms.Padding(4);
            this.btnLenNhomhang.Name = "btnLenNhomhang";
            this.btnLenNhomhang.Size = new System.Drawing.Size(131, 35);
            this.btnLenNhomhang.TabIndex = 15;
            this.btnLenNhomhang.Click += new System.EventHandler(this.btnLenNhomhang_Click);
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.pnlHanghoa);
            this.panelControl4.Controls.Add(this.pnlNhomhang);
            this.panelControl4.Controls.Add(this.panelControl7);
            this.panelControl4.Controls.Add(this.panelControl12);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl4.Location = new System.Drawing.Point(656, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(350, 725);
            this.panelControl4.TabIndex = 0;
            // 
            // pnlHanghoa
            // 
            this.pnlHanghoa.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlHanghoa.Controls.Add(this.palControl);
            this.pnlHanghoa.Location = new System.Drawing.Point(0, 0);
            this.pnlHanghoa.Name = "pnlHanghoa";
            this.pnlHanghoa.Size = new System.Drawing.Size(346, 681);
            this.pnlHanghoa.TabIndex = 0;
            // 
            // palControl
            // 
            this.palControl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palControl.Controls.Add(this.labelControl5);
            this.palControl.Controls.Add(this.txtLoinhuan);
            this.palControl.Controls.Add(this.txtGIABAN4);
            this.palControl.Controls.Add(this.labelControl3);
            this.palControl.Controls.Add(this.txtGIABAN3);
            this.palControl.Controls.Add(this.labelControl4);
            this.palControl.Controls.Add(this.chkINTEM);
            this.palControl.Controls.Add(this.chkMONTHEM);
            this.palControl.Controls.Add(this.txtHSD);
            this.palControl.Controls.Add(this.labelControl2);
            this.palControl.Controls.Add(this.cboPLU);
            this.palControl.Controls.Add(this.panelControl17);
            this.palControl.Controls.Add(this.labelControl1);
            this.palControl.Controls.Add(this.chkGiatheotrongluong);
            this.palControl.Controls.Add(this.panelControl16);
            this.palControl.Controls.Add(this.panelControl15);
            this.palControl.Controls.Add(this.btnAutoMaVach);
            this.palControl.Controls.Add(this.btnFileHangHoa_Mo);
            this.palControl.Controls.Add(this.btnFileHangHoa);
            this.palControl.Controls.Add(this.chkSuaDinhLuong);
            this.palControl.Controls.Add(this.chkMayinbep);
            this.palControl.Controls.Add(this.btnAUTO);
            this.palControl.Controls.Add(this.lbMa);
            this.palControl.Controls.Add(this.txtMAHANGHOA);
            this.palControl.Controls.Add(this.chkSUAGIA);
            this.palControl.Controls.Add(this.txtMAVACH);
            this.palControl.Controls.Add(this.lbMaVach);
            this.palControl.Controls.Add(this.panelControl6);
            this.palControl.Controls.Add(this.panelControl5);
            this.palControl.Controls.Add(this.panelControl11);
            this.palControl.Controls.Add(this.panelControl10);
            this.palControl.Controls.Add(this.txtGIABAN2);
            this.palControl.Controls.Add(this.lbGiaBan2);
            this.palControl.Controls.Add(this.txtGIABAN1);
            this.palControl.Controls.Add(this.lbGiaBan1);
            this.palControl.Controls.Add(this.txtGIAMUA);
            this.palControl.Controls.Add(this.lbGiaMua);
            this.palControl.Controls.Add(this.lbThue);
            this.palControl.Controls.Add(this.lblTONTOITHIEU);
            this.palControl.Controls.Add(this.txtTONTOITHIEU);
            this.palControl.Controls.Add(this.chkSUDUNGHANGHOA);
            this.palControl.Controls.Add(this.txtSOLUONG);
            this.palControl.Controls.Add(this.cboTHANHPHAN);
            this.palControl.Controls.Add(this.lbThanhPhan);
            this.palControl.Controls.Add(this.btnLuuHangHoa);
            this.palControl.Controls.Add(this.btnSuaHangHoa);
            this.palControl.Controls.Add(this.btnXoaHangHoa);
            this.palControl.Controls.Add(this.btnThemHangHoa);
            this.palControl.Controls.Add(this.chkTONKHO);
            this.palControl.Controls.Add(this.cboNhomhang);
            this.palControl.Controls.Add(this.lbNhom);
            this.palControl.Controls.Add(this.panelControl9);
            this.palControl.Controls.Add(this.txtGHICHUHANGHOA);
            this.palControl.Controls.Add(this.lbSTT1);
            this.palControl.Controls.Add(this.lblTENHANGHOA);
            this.palControl.Controls.Add(this.txtTENHANGHOA);
            this.palControl.Controls.Add(this.txtSTTHANGHOA);
            this.palControl.Controls.Add(this.lbGhiChu1);
            this.palControl.Controls.Add(this.cboBep);
            this.palControl.Controls.Add(this.lblDVT);
            this.palControl.Controls.Add(this.cboDonvitinh);
            this.palControl.Controls.Add(this.cboThue);
            this.palControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palControl.Location = new System.Drawing.Point(0, 0);
            this.palControl.Name = "palControl";
            this.palControl.Size = new System.Drawing.Size(346, 681);
            this.palControl.TabIndex = 29;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(288, 194);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(16, 19);
            this.labelControl5.TabIndex = 109;
            this.labelControl5.Text = "%";
            // 
            // txtLoinhuan
            // 
            this.txtLoinhuan.EnterMoveNextControl = true;
            this.txtLoinhuan.Location = new System.Drawing.Point(306, 191);
            this.txtLoinhuan.Name = "txtLoinhuan";
            this.txtLoinhuan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLoinhuan.Properties.Appearance.Options.UseFont = true;
            this.txtLoinhuan.Properties.Appearance.Options.UseTextOptions = true;
            this.txtLoinhuan.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtLoinhuan.Size = new System.Drawing.Size(38, 26);
            this.txtLoinhuan.TabIndex = 108;
            this.txtLoinhuan.EditValueChanged += new System.EventHandler(this.txtLoinhuan_EditValueChanged);
            // 
            // txtGIABAN4
            // 
            this.txtGIABAN4.EnterMoveNextControl = true;
            this.txtGIABAN4.Location = new System.Drawing.Point(245, 255);
            this.txtGIABAN4.Name = "txtGIABAN4";
            this.txtGIABAN4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGIABAN4.Properties.Appearance.Options.UseFont = true;
            this.txtGIABAN4.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGIABAN4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGIABAN4.Properties.DisplayFormat.FormatString = "N0";
            this.txtGIABAN4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN4.Properties.EditFormat.FormatString = "N0";
            this.txtGIABAN4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN4.Properties.Mask.EditMask = "N0";
            this.txtGIABAN4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGIABAN4.Size = new System.Drawing.Size(99, 26);
            this.txtGIABAN4.TabIndex = 107;
            this.txtGIABAN4.ToolTip = "GIÁ NỘI BỘ";
            this.txtGIABAN4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl3.Location = new System.Drawing.Point(171, 258);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(63, 19);
            this.labelControl3.TabIndex = 106;
            this.labelControl3.Text = "Giá bán4";
            this.labelControl3.ToolTip = "GIÁ NỘI BỘ";
            this.labelControl3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // txtGIABAN3
            // 
            this.txtGIABAN3.EnterMoveNextControl = true;
            this.txtGIABAN3.Location = new System.Drawing.Point(71, 255);
            this.txtGIABAN3.Name = "txtGIABAN3";
            this.txtGIABAN3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGIABAN3.Properties.Appearance.Options.UseFont = true;
            this.txtGIABAN3.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGIABAN3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGIABAN3.Properties.DisplayFormat.FormatString = "N0";
            this.txtGIABAN3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN3.Properties.EditFormat.FormatString = "N0";
            this.txtGIABAN3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN3.Properties.Mask.EditMask = "N0";
            this.txtGIABAN3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGIABAN3.Size = new System.Drawing.Size(99, 26);
            this.txtGIABAN3.TabIndex = 105;
            this.txtGIABAN3.ToolTip = "GIÁ NHÂN VIÊN";
            this.txtGIABAN3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl4.Location = new System.Drawing.Point(1, 258);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(63, 19);
            this.labelControl4.TabIndex = 104;
            this.labelControl4.Text = "Giá bán3";
            this.labelControl4.ToolTip = "GIÁ NHÂN VIÊN";
            this.labelControl4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // chkINTEM
            // 
            this.chkINTEM.EnterMoveNextControl = true;
            this.chkINTEM.Location = new System.Drawing.Point(264, 531);
            this.chkINTEM.Name = "chkINTEM";
            this.chkINTEM.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkINTEM.Properties.Appearance.Options.UseFont = true;
            this.chkINTEM.Properties.Caption = "In tem";
            this.chkINTEM.Size = new System.Drawing.Size(78, 24);
            this.chkINTEM.TabIndex = 103;
            // 
            // chkMONTHEM
            // 
            this.chkMONTHEM.EnterMoveNextControl = true;
            this.chkMONTHEM.Location = new System.Drawing.Point(136, 531);
            this.chkMONTHEM.Name = "chkMONTHEM";
            this.chkMONTHEM.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkMONTHEM.Properties.Appearance.Options.UseFont = true;
            this.chkMONTHEM.Properties.Caption = "Món thêm";
            this.chkMONTHEM.Size = new System.Drawing.Size(96, 24);
            this.chkMONTHEM.TabIndex = 102;
            // 
            // txtHSD
            // 
            this.txtHSD.EnterMoveNextControl = true;
            this.txtHSD.Location = new System.Drawing.Point(62, 469);
            this.txtHSD.Name = "txtHSD";
            this.txtHSD.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHSD.Properties.Appearance.Options.UseFont = true;
            this.txtHSD.Size = new System.Drawing.Size(282, 26);
            this.txtHSD.TabIndex = 101;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(4, 472);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(31, 19);
            this.labelControl2.TabIndex = 100;
            this.labelControl2.Text = "HSD";
            // 
            // cboPLU
            // 
            this.cboPLU.EnterMoveNextControl = true;
            this.cboPLU.Location = new System.Drawing.Point(256, 438);
            this.cboPLU.Name = "cboPLU";
            this.cboPLU.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPLU.Properties.Appearance.Options.UseFont = true;
            this.cboPLU.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPLU.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboPLU.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboPLU.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboPLU.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NUM", 30, "Mã")});
            this.cboPLU.Properties.DisplayMember = "NUM";
            this.cboPLU.Properties.DropDownItemHeight = 40;
            this.cboPLU.Properties.NullText = "";
            this.cboPLU.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboPLU.Properties.ShowHeader = false;
            this.cboPLU.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboPLU.Properties.ValueMember = "NUM";
            this.cboPLU.Size = new System.Drawing.Size(88, 26);
            this.cboPLU.TabIndex = 99;
            this.cboPLU.EditValueChanged += new System.EventHandler(this.cboPLU_EditValueChanged);
            // 
            // panelControl17
            // 
            this.panelControl17.Location = new System.Drawing.Point(1, 499);
            this.panelControl17.Name = "panelControl17";
            this.panelControl17.Size = new System.Drawing.Size(344, 2);
            this.panelControl17.TabIndex = 98;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(225, 442);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 19);
            this.labelControl1.TabIndex = 97;
            this.labelControl1.Text = "PLU";
            // 
            // chkGiatheotrongluong
            // 
            this.chkGiatheotrongluong.EnterMoveNextControl = true;
            this.chkGiatheotrongluong.Location = new System.Drawing.Point(0, 439);
            this.chkGiatheotrongluong.Name = "chkGiatheotrongluong";
            this.chkGiatheotrongluong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGiatheotrongluong.Properties.Appearance.Options.UseFont = true;
            this.chkGiatheotrongluong.Properties.Caption = "Tính giá theo trọng lượng";
            this.chkGiatheotrongluong.Size = new System.Drawing.Size(209, 24);
            this.chkGiatheotrongluong.TabIndex = 95;
            // 
            // panelControl16
            // 
            this.panelControl16.Location = new System.Drawing.Point(1, 432);
            this.panelControl16.Name = "panelControl16";
            this.panelControl16.Size = new System.Drawing.Size(344, 2);
            this.panelControl16.TabIndex = 94;
            // 
            // panelControl15
            // 
            this.panelControl15.Location = new System.Drawing.Point(1, 396);
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(344, 2);
            this.panelControl15.TabIndex = 93;
            // 
            // btnAutoMaVach
            // 
            this.btnAutoMaVach.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAutoMaVach.Appearance.Options.UseFont = true;
            this.btnAutoMaVach.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnAutoMaVach.Location = new System.Drawing.Point(292, 401);
            this.btnAutoMaVach.Margin = new System.Windows.Forms.Padding(4);
            this.btnAutoMaVach.Name = "btnAutoMaVach";
            this.btnAutoMaVach.Size = new System.Drawing.Size(51, 26);
            this.btnAutoMaVach.TabIndex = 92;
            this.btnAutoMaVach.Text = "Auto";
            this.btnAutoMaVach.Click += new System.EventHandler(this.btnAutoMaVach_Click);
            // 
            // btnFileHangHoa_Mo
            // 
            this.btnFileHangHoa_Mo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFileHangHoa_Mo.Appearance.Options.UseFont = true;
            this.btnFileHangHoa_Mo.Image = global::KP_RES.Properties.Resources.open_in_browser_26;
            this.btnFileHangHoa_Mo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnFileHangHoa_Mo.Location = new System.Drawing.Point(112, 599);
            this.btnFileHangHoa_Mo.Margin = new System.Windows.Forms.Padding(4);
            this.btnFileHangHoa_Mo.Name = "btnFileHangHoa_Mo";
            this.btnFileHangHoa_Mo.Size = new System.Drawing.Size(57, 35);
            this.btnFileHangHoa_Mo.TabIndex = 91;
            this.btnFileHangHoa_Mo.Click += new System.EventHandler(this.btnFileHangHoa_Mo_Click);
            // 
            // btnFileHangHoa
            // 
            this.btnFileHangHoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFileHangHoa.Appearance.Options.UseFont = true;
            this.btnFileHangHoa.Image = global::KP_RES.Properties.Resources.save_as_26;
            this.btnFileHangHoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnFileHangHoa.Location = new System.Drawing.Point(7, 599);
            this.btnFileHangHoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnFileHangHoa.Name = "btnFileHangHoa";
            this.btnFileHangHoa.Size = new System.Drawing.Size(103, 35);
            this.btnFileHangHoa.TabIndex = 90;
            this.btnFileHangHoa.Text = "&Import";
            this.btnFileHangHoa.Click += new System.EventHandler(this.btnFileHangHoa_Click);
            // 
            // chkSuaDinhLuong
            // 
            this.chkSuaDinhLuong.EnterMoveNextControl = true;
            this.chkSuaDinhLuong.Location = new System.Drawing.Point(0, 505);
            this.chkSuaDinhLuong.Name = "chkSuaDinhLuong";
            this.chkSuaDinhLuong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSuaDinhLuong.Properties.Appearance.Options.UseFont = true;
            this.chkSuaDinhLuong.Properties.Caption = "Sửa định lượng";
            this.chkSuaDinhLuong.Size = new System.Drawing.Size(130, 24);
            this.chkSuaDinhLuong.TabIndex = 89;
            // 
            // chkMayinbep
            // 
            this.chkMayinbep.EnterMoveNextControl = true;
            this.chkMayinbep.Location = new System.Drawing.Point(0, 366);
            this.chkMayinbep.Name = "chkMayinbep";
            this.chkMayinbep.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkMayinbep.Properties.Appearance.Options.UseFont = true;
            this.chkMayinbep.Properties.Caption = "Máy in bếp";
            this.chkMayinbep.Size = new System.Drawing.Size(104, 24);
            this.chkMayinbep.TabIndex = 88;
            // 
            // btnAUTO
            // 
            this.btnAUTO.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAUTO.Appearance.Options.UseFont = true;
            this.btnAUTO.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnAUTO.Location = new System.Drawing.Point(172, 39);
            this.btnAUTO.Margin = new System.Windows.Forms.Padding(4);
            this.btnAUTO.Name = "btnAUTO";
            this.btnAUTO.Size = new System.Drawing.Size(51, 26);
            this.btnAUTO.TabIndex = 87;
            this.btnAUTO.Text = "Auto";
            this.btnAUTO.Click += new System.EventHandler(this.btnAUTO_Click);
            // 
            // lbMa
            // 
            this.lbMa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMa.Location = new System.Drawing.Point(4, 42);
            this.lbMa.Margin = new System.Windows.Forms.Padding(4);
            this.lbMa.Name = "lbMa";
            this.lbMa.Size = new System.Drawing.Size(20, 19);
            this.lbMa.TabIndex = 85;
            this.lbMa.Text = "Mã";
            // 
            // txtMAHANGHOA
            // 
            this.txtMAHANGHOA.EnterMoveNextControl = true;
            this.txtMAHANGHOA.Location = new System.Drawing.Point(62, 39);
            this.txtMAHANGHOA.Name = "txtMAHANGHOA";
            this.txtMAHANGHOA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMAHANGHOA.Properties.Appearance.Options.UseFont = true;
            this.txtMAHANGHOA.Size = new System.Drawing.Size(108, 26);
            this.txtMAHANGHOA.TabIndex = 86;
            // 
            // chkSUAGIA
            // 
            this.chkSUAGIA.EnterMoveNextControl = true;
            this.chkSUAGIA.Location = new System.Drawing.Point(136, 505);
            this.chkSUAGIA.Name = "chkSUAGIA";
            this.chkSUAGIA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUAGIA.Properties.Appearance.Options.UseFont = true;
            this.chkSUAGIA.Properties.Caption = "Giá theo thời vụ";
            this.chkSUAGIA.Size = new System.Drawing.Size(140, 24);
            this.chkSUAGIA.TabIndex = 79;
            // 
            // txtMAVACH
            // 
            this.txtMAVACH.EnterMoveNextControl = true;
            this.txtMAVACH.Location = new System.Drawing.Point(105, 401);
            this.txtMAVACH.Name = "txtMAVACH";
            this.txtMAVACH.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMAVACH.Properties.Appearance.Options.UseFont = true;
            this.txtMAVACH.Size = new System.Drawing.Size(180, 26);
            this.txtMAVACH.TabIndex = 77;
            // 
            // lbMaVach
            // 
            this.lbMaVach.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMaVach.Location = new System.Drawing.Point(4, 404);
            this.lbMaVach.Margin = new System.Windows.Forms.Padding(4);
            this.lbMaVach.Name = "lbMaVach";
            this.lbMaVach.Size = new System.Drawing.Size(57, 19);
            this.lbMaVach.TabIndex = 76;
            this.lbMaVach.Text = "Mã vạch";
            // 
            // panelControl6
            // 
            this.panelControl6.Location = new System.Drawing.Point(2, 359);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(344, 2);
            this.panelControl6.TabIndex = 73;
            // 
            // panelControl5
            // 
            this.panelControl5.Location = new System.Drawing.Point(1, 321);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(344, 2);
            this.panelControl5.TabIndex = 69;
            // 
            // panelControl11
            // 
            this.panelControl11.Location = new System.Drawing.Point(1, 284);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(344, 2);
            this.panelControl11.TabIndex = 64;
            // 
            // panelControl10
            // 
            this.panelControl10.Location = new System.Drawing.Point(1, 186);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(344, 2);
            this.panelControl10.TabIndex = 55;
            // 
            // txtGIABAN2
            // 
            this.txtGIABAN2.EnterMoveNextControl = true;
            this.txtGIABAN2.Location = new System.Drawing.Point(245, 223);
            this.txtGIABAN2.Name = "txtGIABAN2";
            this.txtGIABAN2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGIABAN2.Properties.Appearance.Options.UseFont = true;
            this.txtGIABAN2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGIABAN2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGIABAN2.Properties.DisplayFormat.FormatString = "N0";
            this.txtGIABAN2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN2.Properties.EditFormat.FormatString = "N0";
            this.txtGIABAN2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN2.Properties.Mask.EditMask = "N0";
            this.txtGIABAN2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGIABAN2.Size = new System.Drawing.Size(99, 26);
            this.txtGIABAN2.TabIndex = 63;
            this.txtGIABAN2.ToolTip = "GIÁ GIAO HÀNG";
            this.txtGIABAN2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // lbGiaBan2
            // 
            this.lbGiaBan2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGiaBan2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbGiaBan2.Location = new System.Drawing.Point(171, 226);
            this.lbGiaBan2.Margin = new System.Windows.Forms.Padding(4);
            this.lbGiaBan2.Name = "lbGiaBan2";
            this.lbGiaBan2.Size = new System.Drawing.Size(63, 19);
            this.lbGiaBan2.TabIndex = 62;
            this.lbGiaBan2.Text = "Giá bán2";
            this.lbGiaBan2.ToolTip = "GIÁ GIAO HÀNG";
            this.lbGiaBan2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // txtGIABAN1
            // 
            this.txtGIABAN1.EnterMoveNextControl = true;
            this.txtGIABAN1.Location = new System.Drawing.Point(71, 223);
            this.txtGIABAN1.Name = "txtGIABAN1";
            this.txtGIABAN1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGIABAN1.Properties.Appearance.Options.UseFont = true;
            this.txtGIABAN1.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGIABAN1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGIABAN1.Properties.DisplayFormat.FormatString = "N0";
            this.txtGIABAN1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN1.Properties.EditFormat.FormatString = "N0";
            this.txtGIABAN1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN1.Properties.Mask.EditMask = "N0";
            this.txtGIABAN1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGIABAN1.Size = new System.Drawing.Size(99, 26);
            this.txtGIABAN1.TabIndex = 61;
            this.txtGIABAN1.ToolTip = "GIÁ MẶC ĐỊNH";
            this.txtGIABAN1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // lbGiaBan1
            // 
            this.lbGiaBan1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGiaBan1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbGiaBan1.Location = new System.Drawing.Point(1, 226);
            this.lbGiaBan1.Margin = new System.Windows.Forms.Padding(4);
            this.lbGiaBan1.Name = "lbGiaBan1";
            this.lbGiaBan1.Size = new System.Drawing.Size(63, 19);
            this.lbGiaBan1.TabIndex = 60;
            this.lbGiaBan1.Text = "Giá bán1";
            this.lbGiaBan1.ToolTip = "GIÁ MẶC ĐỊNH";
            this.lbGiaBan1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // txtGIAMUA
            // 
            this.txtGIAMUA.EnterMoveNextControl = true;
            this.txtGIAMUA.Location = new System.Drawing.Point(186, 191);
            this.txtGIAMUA.Name = "txtGIAMUA";
            this.txtGIAMUA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGIAMUA.Properties.Appearance.Options.UseFont = true;
            this.txtGIAMUA.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGIAMUA.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGIAMUA.Properties.DisplayFormat.FormatString = "N0";
            this.txtGIAMUA.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIAMUA.Properties.EditFormat.FormatString = "N0";
            this.txtGIAMUA.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIAMUA.Properties.Mask.EditMask = "N0";
            this.txtGIAMUA.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGIAMUA.Size = new System.Drawing.Size(99, 26);
            this.txtGIAMUA.TabIndex = 59;
            this.txtGIAMUA.EditValueChanged += new System.EventHandler(this.txtGIAMUA_EditValueChanged);
            // 
            // lbGiaMua
            // 
            this.lbGiaMua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGiaMua.Location = new System.Drawing.Point(116, 194);
            this.lbGiaMua.Margin = new System.Windows.Forms.Padding(4);
            this.lbGiaMua.Name = "lbGiaMua";
            this.lbGiaMua.Size = new System.Drawing.Size(59, 19);
            this.lbGiaMua.TabIndex = 58;
            this.lbGiaMua.Text = "Giá mua";
            // 
            // lbThue
            // 
            this.lbThue.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbThue.Location = new System.Drawing.Point(4, 194);
            this.lbThue.Margin = new System.Windows.Forms.Padding(4);
            this.lbThue.Name = "lbThue";
            this.lbThue.Size = new System.Drawing.Size(36, 19);
            this.lbThue.TabIndex = 56;
            this.lbThue.Text = "Thuế";
            // 
            // lblTONTOITHIEU
            // 
            this.lblTONTOITHIEU.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTONTOITHIEU.Location = new System.Drawing.Point(91, 332);
            this.lblTONTOITHIEU.Margin = new System.Windows.Forms.Padding(4);
            this.lblTONTOITHIEU.Name = "lblTONTOITHIEU";
            this.lblTONTOITHIEU.Size = new System.Drawing.Size(91, 19);
            this.lblTONTOITHIEU.TabIndex = 71;
            this.lblTONTOITHIEU.Text = "Tồn tối thiểu";
            // 
            // txtTONTOITHIEU
            // 
            this.txtTONTOITHIEU.EnterMoveNextControl = true;
            this.txtTONTOITHIEU.Location = new System.Drawing.Point(189, 329);
            this.txtTONTOITHIEU.Name = "txtTONTOITHIEU";
            this.txtTONTOITHIEU.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTONTOITHIEU.Properties.Appearance.Options.UseFont = true;
            this.txtTONTOITHIEU.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTONTOITHIEU.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTONTOITHIEU.Properties.Mask.EditMask = "N0";
            this.txtTONTOITHIEU.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTONTOITHIEU.Size = new System.Drawing.Size(155, 26);
            this.txtTONTOITHIEU.TabIndex = 72;
            // 
            // chkSUDUNGHANGHOA
            // 
            this.chkSUDUNGHANGHOA.EnterMoveNextControl = true;
            this.chkSUDUNGHANGHOA.Location = new System.Drawing.Point(0, 531);
            this.chkSUDUNGHANGHOA.Name = "chkSUDUNGHANGHOA";
            this.chkSUDUNGHANGHOA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNGHANGHOA.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNGHANGHOA.Properties.Caption = "Sử dụng";
            this.chkSUDUNGHANGHOA.Size = new System.Drawing.Size(92, 24);
            this.chkSUDUNGHANGHOA.TabIndex = 78;
            // 
            // txtSOLUONG
            // 
            this.txtSOLUONG.EnterMoveNextControl = true;
            this.txtSOLUONG.Location = new System.Drawing.Point(271, 289);
            this.txtSOLUONG.Name = "txtSOLUONG";
            this.txtSOLUONG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSOLUONG.Properties.Appearance.Options.UseFont = true;
            this.txtSOLUONG.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSOLUONG.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSOLUONG.Size = new System.Drawing.Size(73, 26);
            this.txtSOLUONG.TabIndex = 68;
            // 
            // cboTHANHPHAN
            // 
            this.cboTHANHPHAN.EnterMoveNextControl = true;
            this.cboTHANHPHAN.Location = new System.Drawing.Point(91, 289);
            this.cboTHANHPHAN.Name = "cboTHANHPHAN";
            this.cboTHANHPHAN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTHANHPHAN.Properties.Appearance.Options.UseFont = true;
            this.cboTHANHPHAN.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTHANHPHAN.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboTHANHPHAN.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboTHANHPHAN.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboTHANHPHAN.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DONVITINH", "Đơn vị tính")});
            this.cboTHANHPHAN.Properties.DisplayMember = "TEN";
            this.cboTHANHPHAN.Properties.DropDownItemHeight = 40;
            this.cboTHANHPHAN.Properties.NullText = "";
            this.cboTHANHPHAN.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboTHANHPHAN.Properties.ShowHeader = false;
            this.cboTHANHPHAN.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboTHANHPHAN.Properties.ValueMember = "MA";
            this.cboTHANHPHAN.Size = new System.Drawing.Size(174, 26);
            this.cboTHANHPHAN.TabIndex = 66;
            // 
            // lbThanhPhan
            // 
            this.lbThanhPhan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbThanhPhan.Location = new System.Drawing.Point(3, 295);
            this.lbThanhPhan.Margin = new System.Windows.Forms.Padding(4);
            this.lbThanhPhan.Name = "lbThanhPhan";
            this.lbThanhPhan.Size = new System.Drawing.Size(85, 19);
            this.lbThanhPhan.TabIndex = 65;
            this.lbThanhPhan.Text = "Thành phần";
            // 
            // btnLuuHangHoa
            // 
            this.btnLuuHangHoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuHangHoa.Appearance.Options.UseFont = true;
            this.btnLuuHangHoa.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuuHangHoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuuHangHoa.Location = new System.Drawing.Point(7, 561);
            this.btnLuuHangHoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuuHangHoa.Name = "btnLuuHangHoa";
            this.btnLuuHangHoa.Size = new System.Drawing.Size(80, 35);
            this.btnLuuHangHoa.TabIndex = 80;
            this.btnLuuHangHoa.Text = "&Lưu";
            this.btnLuuHangHoa.Click += new System.EventHandler(this.btnLuuHangHoa_Click);
            // 
            // btnSuaHangHoa
            // 
            this.btnSuaHangHoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaHangHoa.Appearance.Options.UseFont = true;
            this.btnSuaHangHoa.Image = ((System.Drawing.Image)(resources.GetObject("btnSuaHangHoa.Image")));
            this.btnSuaHangHoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSuaHangHoa.Location = new System.Drawing.Point(173, 561);
            this.btnSuaHangHoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnSuaHangHoa.Name = "btnSuaHangHoa";
            this.btnSuaHangHoa.Size = new System.Drawing.Size(80, 35);
            this.btnSuaHangHoa.TabIndex = 82;
            this.btnSuaHangHoa.Text = "&Sửa";
            this.btnSuaHangHoa.Click += new System.EventHandler(this.btnSuaHangHoa_Click);
            // 
            // btnXoaHangHoa
            // 
            this.btnXoaHangHoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaHangHoa.Appearance.Options.UseFont = true;
            this.btnXoaHangHoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoaHangHoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoaHangHoa.Location = new System.Drawing.Point(256, 561);
            this.btnXoaHangHoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoaHangHoa.Name = "btnXoaHangHoa";
            this.btnXoaHangHoa.Size = new System.Drawing.Size(80, 35);
            this.btnXoaHangHoa.TabIndex = 83;
            this.btnXoaHangHoa.Text = "&Xóa";
            this.btnXoaHangHoa.Click += new System.EventHandler(this.btnXoaHangHoa_Click);
            // 
            // btnThemHangHoa
            // 
            this.btnThemHangHoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemHangHoa.Appearance.Options.UseFont = true;
            this.btnThemHangHoa.Image = ((System.Drawing.Image)(resources.GetObject("btnThemHangHoa.Image")));
            this.btnThemHangHoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThemHangHoa.Location = new System.Drawing.Point(90, 561);
            this.btnThemHangHoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnThemHangHoa.Name = "btnThemHangHoa";
            this.btnThemHangHoa.Size = new System.Drawing.Size(80, 35);
            this.btnThemHangHoa.TabIndex = 81;
            this.btnThemHangHoa.Text = "&Thêm";
            this.btnThemHangHoa.Click += new System.EventHandler(this.btnThemHangHoa_Click);
            // 
            // chkTONKHO
            // 
            this.chkTONKHO.EnterMoveNextControl = true;
            this.chkTONKHO.Location = new System.Drawing.Point(0, 327);
            this.chkTONKHO.Name = "chkTONKHO";
            this.chkTONKHO.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTONKHO.Properties.Appearance.Options.UseFont = true;
            this.chkTONKHO.Properties.Caption = "Tồn kho";
            this.chkTONKHO.Size = new System.Drawing.Size(92, 24);
            this.chkTONKHO.TabIndex = 70;
            // 
            // cboNhomhang
            // 
            this.cboNhomhang.EnterMoveNextControl = true;
            this.cboNhomhang.Location = new System.Drawing.Point(62, 10);
            this.cboNhomhang.Name = "cboNhomhang";
            this.cboNhomhang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhomhang.Properties.Appearance.Options.UseFont = true;
            this.cboNhomhang.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhomhang.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboNhomhang.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboNhomhang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboNhomhang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboNhomhang.Properties.DisplayMember = "TEN";
            this.cboNhomhang.Properties.DropDownItemHeight = 40;
            this.cboNhomhang.Properties.NullText = "";
            this.cboNhomhang.Properties.ShowHeader = false;
            this.cboNhomhang.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboNhomhang.Properties.ValueMember = "MA";
            this.cboNhomhang.Size = new System.Drawing.Size(282, 26);
            this.cboNhomhang.TabIndex = 46;
            // 
            // lbNhom
            // 
            this.lbNhom.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNhom.Location = new System.Drawing.Point(4, 13);
            this.lbNhom.Margin = new System.Windows.Forms.Padding(4);
            this.lbNhom.Name = "lbNhom";
            this.lbNhom.Size = new System.Drawing.Size(43, 19);
            this.lbNhom.TabIndex = 45;
            this.lbNhom.Text = "Nhóm";
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.picHANGHOA);
            this.panelControl9.Location = new System.Drawing.Point(226, 38);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(118, 114);
            this.panelControl9.TabIndex = 84;
            // 
            // picHANGHOA
            // 
            this.picHANGHOA.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            this.picHANGHOA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picHANGHOA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picHANGHOA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picHANGHOA.Location = new System.Drawing.Point(2, 2);
            this.picHANGHOA.Name = "picHANGHOA";
            this.picHANGHOA.Size = new System.Drawing.Size(114, 110);
            this.picHANGHOA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picHANGHOA.TabIndex = 23;
            this.picHANGHOA.TabStop = false;
            this.picHANGHOA.Click += new System.EventHandler(this.picHANGHOA_Click);
            // 
            // txtGHICHUHANGHOA
            // 
            this.txtGHICHUHANGHOA.EnterMoveNextControl = true;
            this.txtGHICHUHANGHOA.Location = new System.Drawing.Point(62, 155);
            this.txtGHICHUHANGHOA.Name = "txtGHICHUHANGHOA";
            this.txtGHICHUHANGHOA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHUHANGHOA.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHUHANGHOA.Size = new System.Drawing.Size(282, 26);
            this.txtGHICHUHANGHOA.TabIndex = 54;
            // 
            // lbSTT1
            // 
            this.lbSTT1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSTT1.Location = new System.Drawing.Point(4, 129);
            this.lbSTT1.Margin = new System.Windows.Forms.Padding(4);
            this.lbSTT1.Name = "lbSTT1";
            this.lbSTT1.Size = new System.Drawing.Size(29, 19);
            this.lbSTT1.TabIndex = 51;
            this.lbSTT1.Text = "STT";
            // 
            // lblTENHANGHOA
            // 
            this.lblTENHANGHOA.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTENHANGHOA.Location = new System.Drawing.Point(4, 71);
            this.lblTENHANGHOA.Margin = new System.Windows.Forms.Padding(4);
            this.lblTENHANGHOA.Name = "lblTENHANGHOA";
            this.lblTENHANGHOA.Size = new System.Drawing.Size(27, 19);
            this.lblTENHANGHOA.TabIndex = 47;
            this.lblTENHANGHOA.Text = "Tên";
            // 
            // txtTENHANGHOA
            // 
            this.txtTENHANGHOA.EnterMoveNextControl = true;
            this.txtTENHANGHOA.Location = new System.Drawing.Point(62, 68);
            this.txtTENHANGHOA.Name = "txtTENHANGHOA";
            this.txtTENHANGHOA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTENHANGHOA.Properties.Appearance.Options.UseFont = true;
            this.txtTENHANGHOA.Size = new System.Drawing.Size(162, 26);
            this.txtTENHANGHOA.TabIndex = 48;
            // 
            // txtSTTHANGHOA
            // 
            this.txtSTTHANGHOA.EnterMoveNextControl = true;
            this.txtSTTHANGHOA.Location = new System.Drawing.Point(62, 126);
            this.txtSTTHANGHOA.Name = "txtSTTHANGHOA";
            this.txtSTTHANGHOA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSTTHANGHOA.Properties.Appearance.Options.UseFont = true;
            this.txtSTTHANGHOA.Properties.Mask.EditMask = "d";
            this.txtSTTHANGHOA.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSTTHANGHOA.Size = new System.Drawing.Size(162, 26);
            this.txtSTTHANGHOA.TabIndex = 52;
            // 
            // lbGhiChu1
            // 
            this.lbGhiChu1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGhiChu1.Location = new System.Drawing.Point(4, 158);
            this.lbGhiChu1.Margin = new System.Windows.Forms.Padding(4);
            this.lbGhiChu1.Name = "lbGhiChu1";
            this.lbGhiChu1.Size = new System.Drawing.Size(54, 19);
            this.lbGhiChu1.TabIndex = 53;
            this.lbGhiChu1.Text = "Ghi chú";
            // 
            // cboBep
            // 
            this.cboBep.EnterMoveNextControl = true;
            this.cboBep.Location = new System.Drawing.Point(105, 366);
            this.cboBep.Name = "cboBep";
            this.cboBep.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBep.Properties.Appearance.Options.UseFont = true;
            this.cboBep.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBep.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboBep.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboBep.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboBep.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN_CUAHANG", "TEN_CUAHANG"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MAYINBEP", "Máy in")});
            this.cboBep.Properties.DisplayMember = "TEN";
            this.cboBep.Properties.DropDownItemHeight = 40;
            this.cboBep.Properties.NullText = "";
            this.cboBep.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboBep.Properties.ShowHeader = false;
            this.cboBep.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboBep.Properties.ValueMember = "MA";
            this.cboBep.Size = new System.Drawing.Size(239, 26);
            this.cboBep.TabIndex = 75;
            // 
            // lblDVT
            // 
            this.lblDVT.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDVT.Location = new System.Drawing.Point(4, 100);
            this.lblDVT.Margin = new System.Windows.Forms.Padding(4);
            this.lblDVT.Name = "lblDVT";
            this.lblDVT.Size = new System.Drawing.Size(31, 19);
            this.lblDVT.TabIndex = 49;
            this.lblDVT.Text = "ĐVT";
            // 
            // cboDonvitinh
            // 
            this.cboDonvitinh.EnterMoveNextControl = true;
            this.cboDonvitinh.Location = new System.Drawing.Point(62, 97);
            this.cboDonvitinh.Name = "cboDonvitinh";
            this.cboDonvitinh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDonvitinh.Properties.Appearance.Options.UseFont = true;
            this.cboDonvitinh.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDonvitinh.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboDonvitinh.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboDonvitinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboDonvitinh.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboDonvitinh.Properties.DisplayMember = "TEN";
            this.cboDonvitinh.Properties.DropDownItemHeight = 40;
            this.cboDonvitinh.Properties.NullText = "";
            this.cboDonvitinh.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboDonvitinh.Properties.ShowHeader = false;
            this.cboDonvitinh.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboDonvitinh.Properties.ValueMember = "MA";
            this.cboDonvitinh.Size = new System.Drawing.Size(162, 26);
            this.cboDonvitinh.TabIndex = 50;
            // 
            // cboThue
            // 
            this.cboThue.EnterMoveNextControl = true;
            this.cboThue.Location = new System.Drawing.Point(71, 191);
            this.cboThue.Name = "cboThue";
            this.cboThue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboThue.Properties.Appearance.Options.UseFont = true;
            this.cboThue.Properties.Appearance.Options.UseTextOptions = true;
            this.cboThue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cboThue.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cboThue.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboThue.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.cboThue.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cboThue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboThue.Properties.DropDownItemHeight = 40;
            this.cboThue.Properties.Items.AddRange(new object[] {
            "0",
            "5",
            "10"});
            this.cboThue.Size = new System.Drawing.Size(40, 26);
            this.cboThue.TabIndex = 57;
            // 
            // pnlNhomhang
            // 
            this.pnlNhomhang.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlNhomhang.Controls.Add(this.chkMONTHEM_NH);
            this.pnlNhomhang.Controls.Add(this.btnFileNhomhang_Mo);
            this.pnlNhomhang.Controls.Add(this.btnFileNhomhang);
            this.pnlNhomhang.Controls.Add(this.palMayinbep);
            this.pnlNhomhang.Controls.Add(this.chkSuamayinbep);
            this.pnlNhomhang.Controls.Add(this.panelControl8);
            this.pnlNhomhang.Controls.Add(this.lbSTT);
            this.pnlNhomhang.Controls.Add(this.lblTENNHOMHANG);
            this.pnlNhomhang.Controls.Add(this.btnLuuNhomHang);
            this.pnlNhomhang.Controls.Add(this.txtTENNHOMHANG);
            this.pnlNhomhang.Controls.Add(this.btnSuaNhomHang);
            this.pnlNhomhang.Controls.Add(this.btnXoaNhomHang);
            this.pnlNhomhang.Controls.Add(this.lblGHICHU);
            this.pnlNhomhang.Controls.Add(this.btnThemNhomHang);
            this.pnlNhomhang.Controls.Add(this.chkSUDUNGNHOMHANG);
            this.pnlNhomhang.Controls.Add(this.chkTHUCDONNHOMHANG);
            this.pnlNhomhang.Controls.Add(this.txtSTTNHOMHANG);
            this.pnlNhomhang.Controls.Add(this.txtGHICHUNHOMHANG);
            this.pnlNhomhang.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlNhomhang.Location = new System.Drawing.Point(2, 37);
            this.pnlNhomhang.Name = "pnlNhomhang";
            this.pnlNhomhang.Size = new System.Drawing.Size(346, 261);
            this.pnlNhomhang.TabIndex = 0;
            this.pnlNhomhang.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlNhomhang_Paint);
            // 
            // chkMONTHEM_NH
            // 
            this.chkMONTHEM_NH.EnterMoveNextControl = true;
            this.chkMONTHEM_NH.Location = new System.Drawing.Point(233, 95);
            this.chkMONTHEM_NH.Name = "chkMONTHEM_NH";
            this.chkMONTHEM_NH.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkMONTHEM_NH.Properties.Appearance.Options.UseFont = true;
            this.chkMONTHEM_NH.Properties.Caption = "Món thêm";
            this.chkMONTHEM_NH.Size = new System.Drawing.Size(99, 24);
            this.chkMONTHEM_NH.TabIndex = 103;
            // 
            // btnFileNhomhang_Mo
            // 
            this.btnFileNhomhang_Mo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFileNhomhang_Mo.Appearance.Options.UseFont = true;
            this.btnFileNhomhang_Mo.Image = global::KP_RES.Properties.Resources.open_in_browser_26;
            this.btnFileNhomhang_Mo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnFileNhomhang_Mo.Location = new System.Drawing.Point(112, 169);
            this.btnFileNhomhang_Mo.Margin = new System.Windows.Forms.Padding(4);
            this.btnFileNhomhang_Mo.Name = "btnFileNhomhang_Mo";
            this.btnFileNhomhang_Mo.Size = new System.Drawing.Size(57, 35);
            this.btnFileNhomhang_Mo.TabIndex = 95;
            this.btnFileNhomhang_Mo.Click += new System.EventHandler(this.btnFileNhomhang_Mo_Click);
            // 
            // btnFileNhomhang
            // 
            this.btnFileNhomhang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFileNhomhang.Appearance.Options.UseFont = true;
            this.btnFileNhomhang.Image = global::KP_RES.Properties.Resources.save_as_26;
            this.btnFileNhomhang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnFileNhomhang.Location = new System.Drawing.Point(6, 169);
            this.btnFileNhomhang.Margin = new System.Windows.Forms.Padding(4);
            this.btnFileNhomhang.Name = "btnFileNhomhang";
            this.btnFileNhomhang.Size = new System.Drawing.Size(103, 35);
            this.btnFileNhomhang.TabIndex = 93;
            this.btnFileNhomhang.Text = "&Import";
            this.btnFileNhomhang.Click += new System.EventHandler(this.btnFileNhomhang_Click);
            // 
            // palMayinbep
            // 
            this.palMayinbep.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palMayinbep.Controls.Add(this.chkMayinbepNhom);
            this.palMayinbep.Controls.Add(this.cboBepNhom);
            this.palMayinbep.Location = new System.Drawing.Point(2, 241);
            this.palMayinbep.Name = "palMayinbep";
            this.palMayinbep.Size = new System.Drawing.Size(342, 37);
            this.palMayinbep.TabIndex = 92;
            this.palMayinbep.Visible = false;
            // 
            // chkMayinbepNhom
            // 
            this.chkMayinbepNhom.EnterMoveNextControl = true;
            this.chkMayinbepNhom.Location = new System.Drawing.Point(1, 5);
            this.chkMayinbepNhom.Name = "chkMayinbepNhom";
            this.chkMayinbepNhom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkMayinbepNhom.Properties.Appearance.Options.UseFont = true;
            this.chkMayinbepNhom.Properties.Caption = "Máy in bếp";
            this.chkMayinbepNhom.Size = new System.Drawing.Size(101, 24);
            this.chkMayinbepNhom.TabIndex = 90;
            // 
            // cboBepNhom
            // 
            this.cboBepNhom.EnterMoveNextControl = true;
            this.cboBepNhom.Location = new System.Drawing.Point(103, 5);
            this.cboBepNhom.Name = "cboBepNhom";
            this.cboBepNhom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBepNhom.Properties.Appearance.Options.UseFont = true;
            this.cboBepNhom.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBepNhom.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboBepNhom.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboBepNhom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboBepNhom.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN_CUAHANG", "TEN_CUAHANG"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MAYINBEP", "Máy in")});
            this.cboBepNhom.Properties.DisplayMember = "TEN";
            this.cboBepNhom.Properties.DropDownItemHeight = 40;
            this.cboBepNhom.Properties.NullText = "";
            this.cboBepNhom.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboBepNhom.Properties.ShowHeader = false;
            this.cboBepNhom.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboBepNhom.Properties.ValueMember = "MA";
            this.cboBepNhom.Size = new System.Drawing.Size(237, 26);
            this.cboBepNhom.TabIndex = 89;
            // 
            // chkSuamayinbep
            // 
            this.chkSuamayinbep.EnterMoveNextControl = true;
            this.chkSuamayinbep.Location = new System.Drawing.Point(5, 211);
            this.chkSuamayinbep.Name = "chkSuamayinbep";
            this.chkSuamayinbep.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSuamayinbep.Properties.Appearance.Options.UseFont = true;
            this.chkSuamayinbep.Properties.Caption = "Sửa máy in bếp";
            this.chkSuamayinbep.Size = new System.Drawing.Size(192, 24);
            this.chkSuamayinbep.TabIndex = 91;
            this.chkSuamayinbep.CheckedChanged += new System.EventHandler(this.chkSuamayinbep_CheckedChanged);
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.picNHOMHANG);
            this.panelControl8.Location = new System.Drawing.Point(225, 5);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(118, 88);
            this.panelControl8.TabIndex = 12;
            // 
            // picNHOMHANG
            // 
            this.picNHOMHANG.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            this.picNHOMHANG.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picNHOMHANG.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picNHOMHANG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picNHOMHANG.Location = new System.Drawing.Point(2, 2);
            this.picNHOMHANG.Name = "picNHOMHANG";
            this.picNHOMHANG.Size = new System.Drawing.Size(114, 84);
            this.picNHOMHANG.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picNHOMHANG.TabIndex = 23;
            this.picNHOMHANG.TabStop = false;
            this.picNHOMHANG.Click += new System.EventHandler(this.picNHOMHANG_Click);
            // 
            // lbSTT
            // 
            this.lbSTT.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSTT.Location = new System.Drawing.Point(3, 37);
            this.lbSTT.Margin = new System.Windows.Forms.Padding(4);
            this.lbSTT.Name = "lbSTT";
            this.lbSTT.Size = new System.Drawing.Size(29, 19);
            this.lbSTT.TabIndex = 2;
            this.lbSTT.Text = "STT";
            // 
            // lblTENNHOMHANG
            // 
            this.lblTENNHOMHANG.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTENNHOMHANG.Location = new System.Drawing.Point(3, 8);
            this.lblTENNHOMHANG.Margin = new System.Windows.Forms.Padding(4);
            this.lblTENNHOMHANG.Name = "lblTENNHOMHANG";
            this.lblTENNHOMHANG.Size = new System.Drawing.Size(27, 19);
            this.lblTENNHOMHANG.TabIndex = 0;
            this.lblTENNHOMHANG.Text = "Tên";
            // 
            // btnLuuNhomHang
            // 
            this.btnLuuNhomHang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuNhomHang.Appearance.Options.UseFont = true;
            this.btnLuuNhomHang.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuuNhomHang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuuNhomHang.Location = new System.Drawing.Point(6, 126);
            this.btnLuuNhomHang.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuuNhomHang.Name = "btnLuuNhomHang";
            this.btnLuuNhomHang.Size = new System.Drawing.Size(80, 35);
            this.btnLuuNhomHang.TabIndex = 8;
            this.btnLuuNhomHang.Text = "&Lưu";
            this.btnLuuNhomHang.Click += new System.EventHandler(this.btnLuuNhomHang_Click);
            // 
            // txtTENNHOMHANG
            // 
            this.txtTENNHOMHANG.EnterMoveNextControl = true;
            this.txtTENNHOMHANG.Location = new System.Drawing.Point(61, 5);
            this.txtTENNHOMHANG.Name = "txtTENNHOMHANG";
            this.txtTENNHOMHANG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTENNHOMHANG.Properties.Appearance.Options.UseFont = true;
            this.txtTENNHOMHANG.Size = new System.Drawing.Size(162, 26);
            this.txtTENNHOMHANG.TabIndex = 1;
            // 
            // btnSuaNhomHang
            // 
            this.btnSuaNhomHang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaNhomHang.Appearance.Options.UseFont = true;
            this.btnSuaNhomHang.Image = ((System.Drawing.Image)(resources.GetObject("btnSuaNhomHang.Image")));
            this.btnSuaNhomHang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSuaNhomHang.Location = new System.Drawing.Point(172, 126);
            this.btnSuaNhomHang.Margin = new System.Windows.Forms.Padding(4);
            this.btnSuaNhomHang.Name = "btnSuaNhomHang";
            this.btnSuaNhomHang.Size = new System.Drawing.Size(80, 35);
            this.btnSuaNhomHang.TabIndex = 10;
            this.btnSuaNhomHang.Text = "&Sửa";
            this.btnSuaNhomHang.Click += new System.EventHandler(this.btnSuaNhomHang_Click);
            // 
            // btnXoaNhomHang
            // 
            this.btnXoaNhomHang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaNhomHang.Appearance.Options.UseFont = true;
            this.btnXoaNhomHang.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoaNhomHang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoaNhomHang.Location = new System.Drawing.Point(255, 126);
            this.btnXoaNhomHang.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoaNhomHang.Name = "btnXoaNhomHang";
            this.btnXoaNhomHang.Size = new System.Drawing.Size(80, 35);
            this.btnXoaNhomHang.TabIndex = 11;
            this.btnXoaNhomHang.Text = "&Xóa";
            this.btnXoaNhomHang.Click += new System.EventHandler(this.btnXoaNhomHang_Click);
            // 
            // lblGHICHU
            // 
            this.lblGHICHU.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGHICHU.Location = new System.Drawing.Point(3, 66);
            this.lblGHICHU.Margin = new System.Windows.Forms.Padding(4);
            this.lblGHICHU.Name = "lblGHICHU";
            this.lblGHICHU.Size = new System.Drawing.Size(54, 19);
            this.lblGHICHU.TabIndex = 4;
            this.lblGHICHU.Text = "Ghi chú";
            // 
            // btnThemNhomHang
            // 
            this.btnThemNhomHang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemNhomHang.Appearance.Options.UseFont = true;
            this.btnThemNhomHang.Image = ((System.Drawing.Image)(resources.GetObject("btnThemNhomHang.Image")));
            this.btnThemNhomHang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThemNhomHang.Location = new System.Drawing.Point(89, 126);
            this.btnThemNhomHang.Margin = new System.Windows.Forms.Padding(4);
            this.btnThemNhomHang.Name = "btnThemNhomHang";
            this.btnThemNhomHang.Size = new System.Drawing.Size(80, 35);
            this.btnThemNhomHang.TabIndex = 9;
            this.btnThemNhomHang.Text = "&Thêm";
            this.btnThemNhomHang.Click += new System.EventHandler(this.btnThemNhomHang_Click);
            // 
            // chkSUDUNGNHOMHANG
            // 
            this.chkSUDUNGNHOMHANG.EnterMoveNextControl = true;
            this.chkSUDUNGNHOMHANG.Location = new System.Drawing.Point(28, 95);
            this.chkSUDUNGNHOMHANG.Name = "chkSUDUNGNHOMHANG";
            this.chkSUDUNGNHOMHANG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNGNHOMHANG.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNGNHOMHANG.Properties.Caption = "Sử dụng";
            this.chkSUDUNGNHOMHANG.Size = new System.Drawing.Size(92, 24);
            this.chkSUDUNGNHOMHANG.TabIndex = 6;
            // 
            // chkTHUCDONNHOMHANG
            // 
            this.chkTHUCDONNHOMHANG.EnterMoveNextControl = true;
            this.chkTHUCDONNHOMHANG.Location = new System.Drawing.Point(126, 95);
            this.chkTHUCDONNHOMHANG.Name = "chkTHUCDONNHOMHANG";
            this.chkTHUCDONNHOMHANG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTHUCDONNHOMHANG.Properties.Appearance.Options.UseFont = true;
            this.chkTHUCDONNHOMHANG.Properties.Caption = "Thực đơn";
            this.chkTHUCDONNHOMHANG.Size = new System.Drawing.Size(92, 24);
            this.chkTHUCDONNHOMHANG.TabIndex = 7;
            // 
            // txtSTTNHOMHANG
            // 
            this.txtSTTNHOMHANG.EnterMoveNextControl = true;
            this.txtSTTNHOMHANG.Location = new System.Drawing.Point(61, 34);
            this.txtSTTNHOMHANG.Name = "txtSTTNHOMHANG";
            this.txtSTTNHOMHANG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSTTNHOMHANG.Properties.Appearance.Options.UseFont = true;
            this.txtSTTNHOMHANG.Properties.Mask.BeepOnError = true;
            this.txtSTTNHOMHANG.Properties.Mask.EditMask = "d";
            this.txtSTTNHOMHANG.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSTTNHOMHANG.Size = new System.Drawing.Size(162, 26);
            this.txtSTTNHOMHANG.TabIndex = 3;
            // 
            // txtGHICHUNHOMHANG
            // 
            this.txtGHICHUNHOMHANG.EnterMoveNextControl = true;
            this.txtGHICHUNHOMHANG.Location = new System.Drawing.Point(61, 63);
            this.txtGHICHUNHOMHANG.Name = "txtGHICHUNHOMHANG";
            this.txtGHICHUNHOMHANG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHUNHOMHANG.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHUNHOMHANG.Size = new System.Drawing.Size(162, 26);
            this.txtGHICHUNHOMHANG.TabIndex = 5;
            // 
            // panelControl7
            // 
            this.panelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl7.Controls.Add(this.btnCapnhat);
            this.panelControl7.Controls.Add(this.btnBanphim);
            this.panelControl7.Controls.Add(this.btnLIETKE);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl7.Location = new System.Drawing.Point(2, 688);
            this.panelControl7.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(346, 35);
            this.panelControl7.TabIndex = 17;
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(130, 0);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(4);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(110, 35);
            this.btnCapnhat.TabIndex = 1;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(240, 0);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(4);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(106, 35);
            this.btnBanphim.TabIndex = 10;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnLIETKE
            // 
            this.btnLIETKE.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLIETKE.Appearance.Options.UseFont = true;
            this.btnLIETKE.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnLIETKE.Image = global::KP_RES.Properties.Resources.View_Details_26;
            this.btnLIETKE.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLIETKE.Location = new System.Drawing.Point(0, 0);
            this.btnLIETKE.Margin = new System.Windows.Forms.Padding(4);
            this.btnLIETKE.Name = "btnLIETKE";
            this.btnLIETKE.Size = new System.Drawing.Size(130, 35);
            this.btnLIETKE.TabIndex = 42;
            this.btnLIETKE.Text = "Liệt kê tất cả";
            this.btnLIETKE.Click += new System.EventHandler(this.btnLIETKE_Click);
            // 
            // panelControl12
            // 
            this.panelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl12.Controls.Add(this.btnChonNhomhang);
            this.panelControl12.Controls.Add(this.btnChonHanghoa);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl12.Location = new System.Drawing.Point(2, 2);
            this.panelControl12.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(346, 35);
            this.panelControl12.TabIndex = 18;
            // 
            // btnChonNhomhang
            // 
            this.btnChonNhomhang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonNhomhang.Appearance.Options.UseFont = true;
            this.btnChonNhomhang.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnChonNhomhang.Image = global::KP_RES.Properties.Resources.list_26;
            this.btnChonNhomhang.Location = new System.Drawing.Point(0, 0);
            this.btnChonNhomhang.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonNhomhang.Name = "btnChonNhomhang";
            this.btnChonNhomhang.Size = new System.Drawing.Size(173, 35);
            this.btnChonNhomhang.TabIndex = 1;
            this.btnChonNhomhang.Text = "Nhóm hàng";
            this.btnChonNhomhang.Click += new System.EventHandler(this.btnChonNhomhang_Click);
            // 
            // btnChonHanghoa
            // 
            this.btnChonHanghoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonHanghoa.Appearance.Options.UseFont = true;
            this.btnChonHanghoa.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnChonHanghoa.Image = global::KP_RES.Properties.Resources.vegetarian_food_26;
            this.btnChonHanghoa.Location = new System.Drawing.Point(173, 0);
            this.btnChonHanghoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonHanghoa.Name = "btnChonHanghoa";
            this.btnChonHanghoa.Size = new System.Drawing.Size(173, 35);
            this.btnChonHanghoa.TabIndex = 10;
            this.btnChonHanghoa.Text = "Hàng hóa";
            this.btnChonHanghoa.Click += new System.EventHandler(this.btnChonHanghoa_Click);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton5.Location = new System.Drawing.Point(65, 32);
            this.simpleButton5.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(80, 35);
            this.simpleButton5.TabIndex = 10;
            this.simpleButton5.Text = "&Lưu";
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton6.Location = new System.Drawing.Point(73, 84);
            this.simpleButton6.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(80, 35);
            this.simpleButton6.TabIndex = 11;
            this.simpleButton6.Text = "&Lưu";
            // 
            // simpleButton7
            // 
            this.simpleButton7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton7.Appearance.Options.UseFont = true;
            this.simpleButton7.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton7.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton7.Location = new System.Drawing.Point(81, 92);
            this.simpleButton7.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(80, 35);
            this.simpleButton7.TabIndex = 12;
            this.simpleButton7.Text = "&Lưu";
            // 
            // simpleButton8
            // 
            this.simpleButton8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton8.Appearance.Options.UseFont = true;
            this.simpleButton8.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton8.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton8.Location = new System.Drawing.Point(89, 100);
            this.simpleButton8.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(80, 35);
            this.simpleButton8.TabIndex = 13;
            this.simpleButton8.Text = "&Lưu";
            // 
            // simpleButton9
            // 
            this.simpleButton9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton9.Appearance.Options.UseFont = true;
            this.simpleButton9.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton9.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton9.Location = new System.Drawing.Point(114, 150);
            this.simpleButton9.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(80, 35);
            this.simpleButton9.TabIndex = 14;
            this.simpleButton9.Text = "&Lưu";
            // 
            // Frm_NhomHanghoa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_NhomHanghoa";
            this.Text = "Nhóm hàng - Hàng hóa";
            this.Load += new System.EventHandler(this.Frm_NhomHanghoa_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_NhomHanghoa_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palLIETKE)).EndInit();
            this.palLIETKE.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).EndInit();
            this.panelControl13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.palHANGHOA)).EndInit();
            this.palHANGHOA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palNHOMHANG)).EndInit();
            this.palNHOMHANG.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlHanghoa)).EndInit();
            this.pnlHanghoa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palControl)).EndInit();
            this.palControl.ResumeLayout(false);
            this.palControl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLoinhuan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkINTEM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMONTHEM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHSD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboPLU.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGiatheotrongluong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSuaDinhLuong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMayinbep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMAHANGHOA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUAGIA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMAVACH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIAMUA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTONTOITHIEU.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGHANGHOA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSOLUONG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTHANHPHAN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTONKHO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhomhang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picHANGHOA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUHANGHOA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENHANGHOA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTTHANGHOA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboBep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDonvitinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboThue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlNhomhang)).EndInit();
            this.pnlNhomhang.ResumeLayout(false);
            this.pnlNhomhang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMONTHEM_NH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.palMayinbep)).EndInit();
            this.palMayinbep.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkMayinbepNhom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboBepNhom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSuamayinbep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picNHOMHANG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENNHOMHANG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGNHOMHANG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTHUCDONNHOMHANG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTTNHOMHANG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUNHOMHANG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl palNHOMHANG;
        private DevExpress.XtraEditors.PanelControl palHANGHOA;
        private DevExpress.XtraEditors.SimpleButton btnXuongNhomhang;
        private DevExpress.XtraEditors.SimpleButton btnLenNhomhang;
        private System.Windows.Forms.FlowLayoutPanel fpnlHanghoa;
        private DevExpress.XtraEditors.SimpleButton btnXuongHangHoa;
        private DevExpress.XtraEditors.SimpleButton btnLenHangHoa;
        private System.Windows.Forms.FlowLayoutPanel fpnlNhomhang;
        private DevExpress.XtraEditors.CheckEdit chkTHUCDONNHOMHANG;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNGNHOMHANG;
        private DevExpress.XtraEditors.LabelControl lblGHICHU;
        private DevExpress.XtraEditors.TextEdit txtGHICHUNHOMHANG;
        private DevExpress.XtraEditors.LabelControl lblTENNHOMHANG;
        private DevExpress.XtraEditors.TextEdit txtTENNHOMHANG;
        private DevExpress.XtraEditors.SimpleButton btnLuuNhomHang;
        private DevExpress.XtraEditors.SimpleButton btnSuaNhomHang;
        private DevExpress.XtraEditors.SimpleButton btnXoaNhomHang;
        private DevExpress.XtraEditors.SimpleButton btnThemNhomHang;
        private DevExpress.XtraEditors.PanelControl pnlHanghoa;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.PanelControl pnlNhomhang;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.TextEdit txtSTTNHOMHANG;
        private DevExpress.XtraEditors.LabelControl lbSTT;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private System.Windows.Forms.PictureBox picNHOMHANG;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.SimpleButton btnChonNhomhang;
        private DevExpress.XtraEditors.SimpleButton btnChonHanghoa;
        private DevExpress.XtraEditors.SimpleButton btnLIETKE;
        private DevExpress.XtraEditors.PanelControl palLIETKE;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT1;
        private DevExpress.XtraGrid.Columns.GridColumn ID_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_DONVITINH;
        private DevExpress.XtraGrid.Columns.GridColumn THUE;
        private DevExpress.XtraGrid.Columns.GridColumn GIANHAP;
        private DevExpress.XtraGrid.Columns.GridColumn GIABAN1;
        private DevExpress.XtraGrid.Columns.GridColumn GIABAN2;
        private DevExpress.XtraGrid.Columns.GridColumn TONKHO;
        private DevExpress.XtraGrid.Columns.GridColumn THUCDON;
        private DevExpress.XtraGrid.Columns.GridColumn MA_NHOMHANG;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_NHOMHANG;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl13;
        private DevExpress.XtraEditors.PanelControl panelControl14;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraGrid.Columns.GridColumn MA_DONVITINH;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn GHICHU;
        private DevExpress.XtraGrid.Columns.GridColumn MAVACH;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn SUDUNG;
        private DevExpress.XtraGrid.Columns.GridColumn SUAGIA;
        private DevExpress.XtraGrid.Columns.GridColumn HINHANH;
        private DevExpress.XtraEditors.PanelControl palControl;
        private DevExpress.XtraEditors.SimpleButton btnAUTO;
        private DevExpress.XtraEditors.LabelControl lbMa;
        private DevExpress.XtraEditors.TextEdit txtMAHANGHOA;
        private DevExpress.XtraEditors.CheckEdit chkSUAGIA;
        private DevExpress.XtraEditors.TextEdit txtMAVACH;
        private DevExpress.XtraEditors.LabelControl lbMaVach;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.TextEdit txtGIABAN2;
        private DevExpress.XtraEditors.LabelControl lbGiaBan2;
        private DevExpress.XtraEditors.TextEdit txtGIABAN1;
        private DevExpress.XtraEditors.LabelControl lbGiaBan1;
        private DevExpress.XtraEditors.TextEdit txtGIAMUA;
        private DevExpress.XtraEditors.LabelControl lbGiaMua;
        private DevExpress.XtraEditors.LabelControl lbThue;
        private DevExpress.XtraEditors.LabelControl lblTONTOITHIEU;
        private DevExpress.XtraEditors.TextEdit txtTONTOITHIEU;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNGHANGHOA;
        private DevExpress.XtraEditors.TextEdit txtSOLUONG;
        private DevExpress.XtraEditors.LookUpEdit cboTHANHPHAN;
        private DevExpress.XtraEditors.LabelControl lbThanhPhan;
        private DevExpress.XtraEditors.SimpleButton btnLuuHangHoa;
        private DevExpress.XtraEditors.SimpleButton btnSuaHangHoa;
        private DevExpress.XtraEditors.SimpleButton btnXoaHangHoa;
        private DevExpress.XtraEditors.SimpleButton btnThemHangHoa;
        private DevExpress.XtraEditors.CheckEdit chkTONKHO;
        private DevExpress.XtraEditors.LookUpEdit cboNhomhang;
        private DevExpress.XtraEditors.LabelControl lbNhom;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private System.Windows.Forms.PictureBox picHANGHOA;
        private DevExpress.XtraEditors.TextEdit txtGHICHUHANGHOA;
        private DevExpress.XtraEditors.LabelControl lbSTT1;
        private DevExpress.XtraEditors.LabelControl lblTENHANGHOA;
        private DevExpress.XtraEditors.TextEdit txtTENHANGHOA;
        private DevExpress.XtraEditors.TextEdit txtSTTHANGHOA;
        private DevExpress.XtraEditors.LabelControl lbGhiChu1;
        private DevExpress.XtraEditors.LookUpEdit cboBep;
        private DevExpress.XtraEditors.LabelControl lblDVT;
        private DevExpress.XtraEditors.LookUpEdit cboDonvitinh;
        private DevExpress.XtraEditors.ComboBoxEdit cboThue;
        private DevExpress.XtraGrid.Columns.GridColumn IS_COMBO;
        private DevExpress.XtraEditors.CheckEdit chkMayinbep;
        private DevExpress.XtraEditors.PanelControl palMayinbep;
        private DevExpress.XtraEditors.CheckEdit chkMayinbepNhom;
        private DevExpress.XtraEditors.LookUpEdit cboBepNhom;
        private DevExpress.XtraEditors.CheckEdit chkSuamayinbep;
        private DevExpress.XtraEditors.CheckEdit chkSuaDinhLuong;
        private DevExpress.XtraGrid.Columns.GridColumn SUADINHLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn IS_INBEP;
        private DevExpress.XtraEditors.SimpleButton btnFileNhomhang;
        private DevExpress.XtraEditors.SimpleButton btnFileHangHoa;
        private DevExpress.XtraEditors.SimpleButton btnFileHangHoa_Mo;
        private DevExpress.XtraEditors.SimpleButton btnFileNhomhang_Mo;
        private DevExpress.XtraEditors.SimpleButton btnAutoMaVach;
        private DevExpress.XtraEditors.PanelControl panelControl15;
        private DevExpress.XtraEditors.PanelControl panelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckEdit chkGiatheotrongluong;
        private DevExpress.XtraEditors.PanelControl panelControl16;
        private DevExpress.XtraEditors.LookUpEdit cboPLU;
        private DevExpress.XtraGrid.Columns.GridColumn PLU;
        private DevExpress.XtraGrid.Columns.GridColumn GIATHEOTRONGLUONG;
        private DevExpress.XtraEditors.TextEdit txtHSD;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.CheckEdit chkMONTHEM;
        private DevExpress.XtraGrid.Columns.GridColumn MONTHEM;
        private DevExpress.XtraEditors.CheckEdit chkMONTHEM_NH;
        private DevExpress.XtraEditors.CheckEdit chkINTEM;
        private DevExpress.XtraEditors.TextEdit txtGIABAN4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtGIABAN3;
        private DevExpress.XtraEditors.TextEdit txtLoinhuan;
        private DevExpress.XtraEditors.LabelControl labelControl5;




    }
}