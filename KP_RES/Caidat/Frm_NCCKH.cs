﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES 
{
    public partial class Frm_NCCKH : DevExpress.XtraEditors.XtraForm
    {
        public Frm_NCCKH()
        {
            InitializeComponent();
            this.Text = rm.GetString("ncckh", culture);
            LoadCombo();
            LoaddataGridView();
            LoadPermission();
        }

        string sma = "";

        private void Frm_NCCKH_Paint(object sender, PaintEventArgs e)
        {
            LoadPermission();
        }

        private void btnXuongnhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.RowCount - 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                btnXuongit.Enabled = false;
                btnXuongnhieu.Enabled = false;
            }
            catch
            {
            }
        }

        private void btnXuongit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle + 1;
                btnLenit.Enabled = true;
                btnLennhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == gridView2.RowCount - 1)
                {
                    btnXuongit.Enabled = false;
                    btnXuongnhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLenit_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = gridView2.FocusedRowHandle - 1;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
                if (gridView2.FocusedRowHandle == 0)
                {
                    btnLenit.Enabled = false;
                    btnLennhieu.Enabled = false;
                }
            }
            catch
            {
            }
        }

        private void btnLennhieu_Click(object sender, EventArgs e)
        {
            try
            {
                gridView2.Focus();
                gridView2.FocusedRowHandle = 0;
                btnLenit.Enabled = false;
                btnLennhieu.Enabled = false;
                btnXuongit.Enabled = true;
                btnXuongnhieu.Enabled = true;
            }
            catch
            {
            }
        }

        private void btnCapnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            cboLoai.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (txtMA.Text == "NK00000000")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Khách hàng mặc định không được phép xóa", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show(_xacnhanbanmuonxoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From NHACUNGCAP" + "\n";
            sSQL += "Where MA=" + clsMain.SQLString(sma) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            if (txtMA.Text == "NK00000000")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Khách hàng mặc định không được phép sửa", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            KhoaMoControl(false);
            txtMA.Properties.ReadOnly = true;
            cboLoai.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            string sSQL = "";
            if (sma == "" && btnThem.Enabled)
            {
                sSQL += "Insert into NHACUNGCAP (ID,LOAI,TEN,DIACHI,DIENTHOAI,FAX,EMAIL,WEBSITE,NGUOILIENHE,GHICHU,CMND,NGAYSINH,GIOITINH,LOAIDAILY,CAPDO,TIENDATCOC,ISDATCOC,HANTHANHTOAN,HANMUC_CONGNO,SUDUNG)" + "\n";
                sSQL += "Values ( ";
                sSQL += clsMain.SQLString(txtMA.Text.ToString()) + ",";
                sSQL += clsMain.SQLString(cboLoai.EditValue.ToString()) + ",";
                sSQL += clsMain.SQLStringUnicode(txtTEN.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtDIACHI.Text) + ",";
                sSQL += clsMain.SQLString(txtDIENTHOAI.Text) + ",";
                sSQL += clsMain.SQLString(txtFAX.Text) + ",";
                sSQL += clsMain.SQLString(txtEMAIL.Text) + ",";
                sSQL += clsMain.SQLString(txtWEBSITE.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtNGUOILIENHE.Text) + ",";
                sSQL += clsMain.SQLStringUnicode(txtGHICHU.Text) + ",";
                sSQL += clsMain.SQLString(txtCMND.Text) + ",";
                sSQL += clsMain.SQLString(dtpNgaysinh.Text) + ",";
                sSQL += clsMain.SQLString(cboGioitinh.EditValue.ToString()) + ",";
                sSQL += clsMain.SQLString(cboLoaiDaiLy.EditValue.ToString()) + ",";
                sSQL += clsMain.SQLString(cboCapDoDaiLy.EditValue.ToString()) + ",";

                sSQL += clsMain.SQLString(txtDatCoc.Text .Replace(",","")) + ",";
                sSQL += clsMain.SQLBit(chkTienDatCoc.Checked) + ",";
                sSQL += clsMain.SQLString(txtHanThanhToan.Text.Replace(",", "")) + ",";
                sSQL += clsMain.SQLString(txtTienHanMuc.Text.Replace(",", "")) + ",";

                sSQL += clsMain.SQLBit(chkSUDUNG.Checked) + ")";
            }
            else if ((sma != "" && btnSua.Enabled))
            {
                sSQL += "Update NHACUNGCAP Set " + "\n";
                sSQL += "LOAI=" + clsMain.SQLString(cboLoai.EditValue.ToString()) + ",";
                sSQL += "TEN=" + clsMain.SQLStringUnicode(txtTEN.Text) + "," + "\n";
                sSQL += "DIACHI=" + clsMain.SQLStringUnicode(txtDIACHI.Text) + "," + "\n";
                sSQL += "DIENTHOAI=" + clsMain.SQLString(txtDIENTHOAI.Text) + "," + "\n";
                sSQL += "FAX=" + clsMain.SQLString(txtFAX.Text) + "," + "\n";
                sSQL += "EMAIL=" + clsMain.SQLString(txtEMAIL.Text) + "," + "\n";
                sSQL += "WEBSITE=" + clsMain.SQLString(txtWEBSITE.Text) + "," + "\n";
                sSQL += "NGUOILIENHE=" + clsMain.SQLStringUnicode(txtNGUOILIENHE.Text) + "," + "\n";
                sSQL += "GHICHU=" + clsMain.SQLStringUnicode(txtGHICHU.Text) + "," + "\n";
                sSQL += "CMND=" + clsMain.SQLString(txtCMND.Text) + "," + "\n";
                sSQL += "NGAYSINH=" + clsMain.SQLString(dtpNgaysinh.Text) + "," + "\n";
                sSQL += "LOAIDAILY=" + clsMain.SQLString(cboLoaiDaiLy.EditValue.ToString()) + "," + "\n";
                sSQL += "CAPDO=" + clsMain.SQLString(cboCapDoDaiLy.EditValue.ToString()) + "," + "\n";
                sSQL += "GIOITINH=" + clsMain.SQLString(cboGioitinh.EditValue.ToString()) + "," + "\n";

                sSQL += "HANMUC_CONGNO=" + clsMain.SQLString(txtTienHanMuc.Text.Replace(",", "")) + "," + "\n";
                sSQL += "HANTHANHTOAN=" + clsMain.SQLString(txtHanThanhToan.Text.Replace(",", "")) + "," + "\n";
                sSQL += "ISDATCOC=" + clsMain.SQLBit(chkTienDatCoc.Checked) + "," + "\n";
                sSQL += "TIENDATCOC=" + clsMain.SQLString(txtDatCoc.Text.Replace(",", "")) + "," + "\n";


                sSQL += "SUDUNG=" + clsMain.SQLBit(chkSUDUNG.Checked) + "\n";
                sSQL += "Where MA=" + clsMain.SQLString(sma) + "\n";
            }

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoaddataGridView();
                btnThem.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView2.RowCount == 0 || gridView2.FocusedRowHandle < 0)
                return;

            sma = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString();
            cboLoai.EditValue = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "LOAI").ToString()) ? "1" : "0";
            txtMA.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "ID").ToString();
            txtTEN.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN").ToString();
            txtDIACHI.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DIACHI").ToString();
            txtDIENTHOAI.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "DIENTHOAI").ToString();
            txtFAX.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "FAX").ToString();
            txtEMAIL.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "EMAIL").ToString();
            txtWEBSITE.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "WEBSITE").ToString();
            txtNGUOILIENHE.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGUOILIENHE").ToString();
            txtGHICHU.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GHICHU").ToString();
            chkSUDUNG.Checked = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "SUDUNG").ToString());
            txtCMND.Text = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "CMND").ToString();
            dtpNgaysinh .EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAYSINH").ToString() ==""? DateTime .Now   : DateTime .Parse ( gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAYSINH").ToString());
            cboGioitinh.EditValue =gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "NGAYSINH").ToString() ==""?"1": bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "GIOITINH").ToString()) ? "1" : "0";
            cboLoaiDaiLy.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "LOAIDAILY").ToString());
            cboCapDoDaiLy.EditValue = int.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "CAPDO").ToString());
            txtTienHanMuc.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "HANMUC_CONGNO");
            txtHanThanhToan.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "HANTHANHTOAN");
            txtDatCoc.EditValue = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TIENDATCOC");
            chkTienDatCoc.Checked = bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "ISDATCOC").ToString());

            KhoaMoControl(true);
        }

        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);
            gridView2.Focus();

            string sSQL = "";
            sSQL += "Select MA,ID,TEN,LOAI,(Case When LOAI=1 Then N'Nhà cung cấp' Else N'Khách hàng' End) As TENLOAI,";
            sSQL += "ISNULL(HANMUC_CONGNO,0) AS HANMUC_CONGNO,ISNULL(HANTHANHTOAN,0) AS HANTHANHTOAN,";
            sSQL += "ISNULL(TIENDATCOC,0) AS TIENDATCOC,ISNULL(ISDATCOC,0) AS ISDATCOC,";
            sSQL += "DIACHI,DIENTHOAI,FAX,EMAIL,WEBSITE,NGUOILIENHE,GHICHU,SUDUNG,CMND,NGAYSINH,GIOITINH,LOAIDAILY,CAPDO,(SELECT A.TEN FROM LOAIDAILY A WHERE A.MA=LOAIDAILY)AS LOAIDAILY1,(SELECT A.TEN FROM CAPDODAILY A WHERE A.MA=CAPDO)AS CAPDAILY,(SELECT A.CHIETKHAU FROM CAPDODAILY A WHERE A.MA=CAPDO)AS CHIETKHAU" + "\n";
            sSQL += "From NHACUNGCAP" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            gridControl2.DataSource = dt;
            gridView2.ActiveFilterString = "[SUDUNG] = True";
            //gridView2.Columns["TENLOAI"].GroupIndex = 0;
            //gridView2.ExpandAllGroups();
        }

        private void LoadCombo()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("MA");
            dt.Columns.Add("TEN");
            dt.Rows.Add("1", "Nhà cung cấp");
            dt.Rows.Add("0", "Khách hàng");
            cboLoai.Properties.DataSource = dt;
            cboLoai.EditValue = cboLoai.Properties.GetDataSourceValue(cboLoai.Properties.ValueMember, 0);

            DataTable dt1 = new DataTable();
            dt1.Columns.Add("MA");
            dt1.Columns.Add("TEN");
            dt1.Rows.Add("1", "Nam");
            dt1.Rows.Add("0", "Nữ");
            cboGioitinh.Properties.DataSource = dt1;
            cboGioitinh.EditValue = cboGioitinh.Properties.GetDataSourceValue(cboGioitinh.Properties.ValueMember, 0);

            DataTable dt2 = new DataTable();
            dt2 = clsMain.ReturnDataTable("SELECT MA,TEN FROM LOAIDAILY WHERE SUDUNG = 1 ORDER BY TEN");
            cboLoaiDaiLy.Properties.DataSource = dt2;
            cboLoaiDaiLy.EditValue = cboLoaiDaiLy.Properties.GetDataSourceValue(cboLoaiDaiLy.Properties.ValueMember, 0);

            dt2 = new DataTable();
            dt2 = clsMain.ReturnDataTable("SELECT MA,TEN FROM CAPDODAILY WHERE SUDUNG = 1 ORDER BY TEN");
            cboCapDoDaiLy.Properties.DataSource = dt2;
            cboCapDoDaiLy.EditValue = cboCapDoDaiLy.Properties.GetDataSourceValue(cboCapDoDaiLy.Properties.ValueMember, 0);
        }

        private void LoadPermission()
        {
            btnThem.Enabled = clsUserManagement.AllowAdd("9");
            btnSua.Enabled = clsUserManagement.AllowEdit("9");
            btnXoa.Enabled = clsUserManagement.AllowDelete("9");
            btnLuu.Enabled = btnThem.Enabled || btnSua.Enabled;
        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//khóa
            {
                cboLoai.Properties.ReadOnly = true;
                txtMA.Properties.ReadOnly = true;
                txtTEN.Properties.ReadOnly = true;
                txtDIACHI.Properties.ReadOnly = true;
                txtDIENTHOAI.Properties.ReadOnly = true;
                txtFAX.Properties.ReadOnly = true;
                txtEMAIL.Properties.ReadOnly = true;
                txtWEBSITE.Properties.ReadOnly = true;
                txtNGUOILIENHE.Properties.ReadOnly = true;
                txtGHICHU.Properties.ReadOnly = true;
                chkSUDUNG.Properties.ReadOnly = true;
                txtCMND.Properties.ReadOnly = true;
                dtpNgaysinh.Properties.ReadOnly = true;
                cboGioitinh.Properties.ReadOnly = true;
                cboLoaiDaiLy.Properties.ReadOnly = true;
                cboCapDoDaiLy.Properties.ReadOnly = true;
                txtHanThanhToan.Properties.ReadOnly = true;
                txtTienHanMuc.Properties.ReadOnly = true;
                chkTienDatCoc.Properties.ReadOnly = true;
                txtDatCoc.Properties.ReadOnly = true;
            }
            else//mở
            {
                cboLoai.Properties.ReadOnly = false;
                txtMA.Properties.ReadOnly = false;
                txtTEN.Properties.ReadOnly = false;
                txtDIACHI.Properties.ReadOnly = false;
                txtDIENTHOAI.Properties.ReadOnly = false;
                txtFAX.Properties.ReadOnly = false;
                txtEMAIL.Properties.ReadOnly = false;
                txtWEBSITE.Properties.ReadOnly = false;
                txtNGUOILIENHE.Properties.ReadOnly = false;
                txtGHICHU.Properties.ReadOnly = false;
                chkSUDUNG.Properties.ReadOnly = false;
                txtCMND.Properties.ReadOnly = false;
                dtpNgaysinh.Properties.ReadOnly = false;
                cboGioitinh.Properties.ReadOnly = false;
                cboLoaiDaiLy.Properties.ReadOnly = false;
                cboCapDoDaiLy.Properties.ReadOnly = false;
                txtHanThanhToan.Properties.ReadOnly = false;
                txtTienHanMuc.Properties.ReadOnly = false;
                chkTienDatCoc.Properties.ReadOnly = false;
                txtDatCoc.Properties.ReadOnly = false;

            }
        }

        private void SetRongControl()
        {
            sma = "";
            cboLoai.EditValue = cboLoai.Properties.GetDataSourceValue(cboLoai.Properties.ValueMember, 0);
            cboGioitinh.EditValue = cboGioitinh.Properties.GetDataSourceValue(cboGioitinh.Properties.ValueMember, 0);
            cboLoaiDaiLy.EditValue = cboLoaiDaiLy.Properties.GetDataSourceValue(cboLoaiDaiLy.Properties.ValueMember, 0);
            cboCapDoDaiLy.EditValue = cboCapDoDaiLy.Properties.GetDataSourceValue(cboCapDoDaiLy.Properties.ValueMember, 0);
            txtMA.Text = "";
            txtTEN.Text = "";
            txtDIACHI.Text = "";
            txtDIENTHOAI.Text = "";
            txtFAX.Text = "";
            txtEMAIL.Text = "";
            txtWEBSITE.Text = "";
            txtNGUOILIENHE.Text = "";
            txtGHICHU.Text = "";
            chkSUDUNG.Checked = true;
            txtCMND.Text = "";
            dtpNgaysinh.EditValue = DateTime.Now;
            txtHanThanhToan.Text = "";
            txtTienHanMuc.Text = "";
            chkTienDatCoc.Checked = false ;
            txtDatCoc.Text = "";
            DataTable dtMaKH = clsMain.ReturnDataTable("select [dbo].[fc_NewCodeID_NHACUNGCAP]  ()");
            if (dtMaKH.Rows.Count > 0)
            {
                txtMA.Text = dtMaKH.Rows[0][0].ToString();
            }
        }

        private Boolean CheckInput()
        {
            if (txtMA.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Chưa nhập mã", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMA.Focus();
                return false;
            }
            if (txtTEN.Text == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_nhap + lblTEN.Text, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTEN.Focus();
                return false;
            }
            if (sma == "")
            {
                string sSQL = "Select * From NHACUNGCAP Where ID=" + clsMain.SQLString(txtMA.Text.Trim());
                DataTable dtcheck = clsMain.ReturnDataTable(sSQL);
                if (dtcheck.Rows.Count > 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Mã đã tồn tại", _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtMA.Focus();
                    return false;
                }
            }
            return true;
        }

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                txt.SelectAll();
            }
            catch
            {
            }
        }

        string _nhap = "";
        string _thongbao = "";
        string _luukothanhcong = "";
        string _xacnhanbanmuonxoa = "";
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void Frm_NCCKH_Load(object sender, EventArgs e)
        {
            foreach (Control ctl in panelControl1.Controls)
            {
                ctl.Click += new EventHandler(ctl_Click);
            }
          
            this.Text = rm.GetString("ncckh", culture);
            lbNCCKH.Text = rm.GetString("ncckh1", culture);
            lbDienThoai.Text = rm.GetString("dienthoai", culture);
            lb_CMND.Text = rm.GetString("cmnd", culture);
            lblTEN.Text = rm.GetString("ten", culture);
            lbNguoiLienHe.Text = rm.GetString("nguoilienhe", culture);
            lbNgaySinh.Text = rm.GetString("ngaysinh", culture);
            lbDiaChi.Text = rm.GetString("diachi", culture);
            lblGHICHU.Text = rm.GetString("ghichu", culture);
            lbGioiTinh.Text = rm.GetString("gioitinh", culture);
            chkSUDUNG.Text = rm.GetString("sudung", culture);
            btnLuu.Text = rm.GetString("luu", culture);
            btnThem.Text = rm.GetString("them", culture);
            btnSua.Text = rm.GetString("sua", culture);
            btnXoa.Text = rm.GetString("xoa", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _nhap = rm.GetString("nhap", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);
            _xacnhanbanmuonxoa = rm.GetString("xacnhanbanmuonxoa", culture);

            gridView2.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView2.Columns["MA"].Caption = rm.GetString("ma", culture);
            gridView2.Columns["TEN"].Caption = rm.GetString("ten", culture);
            gridView2.Columns["NGAYSINH"].Caption = rm.GetString("ngaysinh", culture);
            gridView2.Columns["DIACHI"].Caption = rm.GetString("diachi", culture);
            gridView2.Columns["DIENTHOAI"].Caption = rm.GetString("dienthoai", culture);
            gridView2.Columns["NGUOILIENHE"].Caption = rm.GetString("nguoilienhe", culture);
            gridView2.Columns["GHICHU"].Caption = rm.GetString("ghichu", culture);
            gridView2.Columns["SUDUNG"].Caption = rm.GetString("sudung", culture);
            gridView2.GroupPanelText = rm.GetString("ktcvddntimkiem", culture);
        }

        private void txtMA_Leave(object sender, EventArgs e)
        {
            if (txtMA.Text.Trim() == "" && txtMA.Properties.ReadOnly == false)
            {
                DataTable dtMaKH = clsMain.ReturnDataTable("select [dbo].[fc_NewCodeID_NHACUNGCAP]  ()");
                if (dtMaKH.Rows.Count > 0)
                {
                    txtMA.Text = dtMaKH.Rows[0][0].ToString();
                }
            }
        }

        private void btnCongNo_Click(object sender, EventArgs e)
        {
            if (sma == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            Frm_CongNoHanMuc frm = new Frm_CongNoHanMuc(sma, gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN").ToString(), bool.Parse(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "ISDATCOC").ToString()), gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "HANMUC_CONGNO").ToString(), gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "HANTHANHTOAN").ToString(), gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TIENDATCOC").ToString());
            frm.ShowDialog();
            frm.Dispose();
        }
    }
}