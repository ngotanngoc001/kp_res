﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KP_UserManagement;
using System.Globalization;
using System.Resources;

namespace KP_RES
{
    public partial class Frm_SetCombo_XemDS : DevExpress.XtraEditors.XtraForm
    {
        public bool kt = false;
        public static TextEdit _textbox;
        bool[] ctltxt;
        public decimal tmp_Thoigia;
        string _Ma_Combo;
        string _Ma_hanghoa;

        public Frm_SetCombo_XemDS(string Ma_Combo, string Ma_hanghoa)
        {
            InitializeComponent();
            _Ma_Combo = Ma_Combo;
            _Ma_hanghoa = Ma_hanghoa;
            Loadhanghoa();
        }

        private void Loadhanghoa()
        {
            string sSQL = "";
            sSQL += "SELECT MA_HANGHOA as MA,TEN_HANGHOA as TEN" + "\n";
            sSQL += "FROM HANGHOA" + "\n";
            sSQL += "WHERE MA_HANGHOA=" + clsMain.SQLString(_Ma_Combo);
            DataTable dtCombo = clsMain.ReturnDataTable(sSQL);

            sSQL = "";
            sSQL += "SELECT MA_HANGHOA as MA,TEN_HANGHOA as TEN" + "\n";
            sSQL += "FROM HANGHOA" + "\n";
            sSQL += "WHERE MA_HANGHOA=" + clsMain.SQLString(_Ma_hanghoa);
            DataTable dthh = clsMain.ReturnDataTable(sSQL);

            lbCombo.Text = dtCombo.Rows[0]["TEN"].ToString();
            lbMathang.Text = dthh.Rows[0]["TEN"].ToString();

            sSQL = "";
            sSQL += "SELECT B.MA_HANGHOA_OPTION AS MA,TEN_HANGHOA as TEN,B.SOLUONG,B.GIATHEM" + "\n";
            sSQL += "FROM HANGHOA A,COMBO_OPTION B" + "\n";
            sSQL += "WHERE A.MA_HANGHOA=B.MA_HANGHOA_OPTION" + "\n";
            sSQL += "AND B.MA_COMBO=" + clsMain.SQLString(_Ma_Combo) + "\n";
            sSQL += "AND B.MA_HANGHOA=" + clsMain.SQLString(_Ma_hanghoa) + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            gridControl2.DataSource = dt;
        }

        private void btn_Thoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void bntXoa_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0)
            {
                XtraMessageBox.Show(_khongcodulieu, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (DialogResult.Yes == (XtraMessageBox.Show(_banmuonxoamhthaythe + gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "TEN").ToString(), "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information)))
            {
                string sSQL = "";
                sSQL += "DELETE COMBO_OPTION" + "\n";
                sSQL += "WHERE MA_COMBO=" + clsMain.SQLString(_Ma_Combo) + "\n";
                sSQL += "AND MA_HANGHOA=" + clsMain.SQLString(_Ma_hanghoa) + "\n";
                sSQL += "AND MA_HANGHOA_OPTION=" + clsMain.SQLString(gridView2.GetRowCellValue(gridView2.FocusedRowHandle, "MA").ToString());
                clsMain.ExecuteSQL(sSQL);

                sSQL = "";
                sSQL += "SELECT B.MA_HANGHOA_OPTION AS MA,TEN_HANGHOA as TEN,B.SOLUONG,B.GIATHEM" + "\n";
                sSQL += "FROM HANGHOA A,COMBO_OPTION B" + "\n";
                sSQL += "WHERE A.MA_HANGHOA=B.MA_HANGHOA_OPTION" + "\n";
                sSQL += "AND B.MA_COMBO=" + clsMain.SQLString(_Ma_Combo) + "\n";
                sSQL += "AND B.MA_HANGHOA=" + clsMain.SQLString(_Ma_hanghoa) + "\n";
                DataTable dt = clsMain.ReturnDataTable(sSQL);
                gridControl2.DataSource = dt;
            }
            else
                return; 
        }


        string _nhap = "";
        string _thongbao = "";
        string _luukothanhcong = "";
        string _xacnhanbanmuonxoa = "";
        string _khongcodulieu = "";
        string _banmuonxoamhthaythe = "";
        private void Frm_SetCombo_XemDS_Load(object sender, EventArgs e)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
            ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
            this.Text = rm.GetString("xemtuychon", culture);
            bntXoa.Text = rm.GetString("xoa", culture);
            btn_thoat.Text = rm.GetString("thoat", culture);
            lbTenCombo.Text = rm.GetString("tencombo", culture);
            lbMatHang1.Text = rm.GetString("mathang1", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _nhap = rm.GetString("nhap", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);
            _xacnhanbanmuonxoa = rm.GetString("xacnhanbanmuonxoa", culture);
            _khongcodulieu = rm.GetString("kocodulieu", culture);
            _banmuonxoamhthaythe = rm.GetString("banmuonxoamhthaythe", culture);

            gridView2.Columns["STT"].Caption = rm.GetString("stt", culture);
            gridView2.Columns["TEN"].Caption = rm.GetString("mathangthaythe", culture);
            gridView2.Columns["SOLUONG"].Caption = rm.GetString("soluong", culture);
            gridView2.Columns["GIATHEM"].Caption = rm.GetString("tiencongthem", culture);
        }
    }
}