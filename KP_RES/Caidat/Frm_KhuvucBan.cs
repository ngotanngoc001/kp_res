﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Globalization;
using System.Resources;
using DevExpress.XtraEditors.Controls;

namespace KP_RES 
{
    public partial class Frm_KhuvucBan : DevExpress.XtraEditors.XtraForm
    {
        string smaCuahang = "";
        string smaKhuvuc = "";
        string smaBan = "";
        string trang = "1";
        private Point MouseDownLocation;

        public Frm_KhuvucBan()
        {
            InitializeComponent();
            cls_KP_RES.LoadBangCauHinh();
            this.Text = rm.GetString("khuvucban", culture);
            LoadCombo();
            LoaddataGridView();
            LoadPermission();
        }

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                txt.SelectAll();
            }
            catch
            {
            }
        }

        private void Frm_KhuvucBan_Paint(object sender, PaintEventArgs e)
        {
            panelControl16.BackColor = panelControl1.BackColor;
            panelControl2.BackColor = panelControl1.BackColor;
            LoadPermission();
        }

        private void Frm_KhuvucBan_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                if (this.ActiveControl.Parent == pal_ban)
                {
                    if (this.ActiveControl.Location.Y == 0)
                        return;
                    this.ActiveControl.Location = new Point(this.ActiveControl.Location.X, this.ActiveControl.Location.Y - 1);
                }
            }
            if (e.KeyCode == Keys.Down)
            {
                if (this.ActiveControl.Parent == pal_ban)
                {
                    this.ActiveControl.Location = new Point(this.ActiveControl.Location.X, this.ActiveControl.Location.Y + 1);
                }
            }
            if (e.KeyCode == Keys.Left)
            {
                if (this.ActiveControl.Parent == pal_ban)
                {
                    if (this.ActiveControl.Location.X == 0)
                        return;
                    this.ActiveControl.Location = new Point(this.ActiveControl.Location.X - 1, this.ActiveControl.Location.Y);
                }
            }
            if (e.KeyCode == Keys.Right)
            {
                if (this.ActiveControl.Parent == pal_ban)
                {
                    this.ActiveControl.Location = new Point(this.ActiveControl.Location.X + 1, this.ActiveControl.Location.Y);
                }
            }
        }

        private void btnChonKhuvuc_Click(object sender, EventArgs e)
        {
            tab_Option.SelectedTabPage = tab_khuvuc;
            btnChonKhuvuc.ForeColor = Color.Red;
            btnChonBan.ForeColor = btnCapnhat.ForeColor;
        }

        private void btnChonBan_Click(object sender, EventArgs e)
        {
            tab_Option.SelectedTabPage = tab_ban;
            btnChonBan.ForeColor = Color.Red;
            btnChonKhuvuc.ForeColor = btnCapnhat.ForeColor;
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void bnt_capnhat_Click(object sender, EventArgs e)
        {
            LoadCombo();
            LoaddataGridView();
            if (pal_cuahang.Controls.Count > 0)
            {
                btCuahang_Click(pal_cuahang.Controls[0], null);
            }
            if (pal_khuvuc.Controls.Count > 0)
            {
                btKhuvuc_Click(pal_khuvuc.Controls[0], null);
            }
        }

        private void bnt_Len_Click(object sender, EventArgs e)
        {
            try
            {
                pal_khuvuc.AutoScrollPosition = new Point(0, pal_khuvuc.VerticalScroll.Value - pal_khuvuc.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btn_Xuong_Click(object sender, EventArgs e)
        {
            try
            {
                pal_khuvuc.AutoScrollPosition = new Point(0, pal_khuvuc.VerticalScroll.Value + pal_khuvuc.VerticalScroll.Maximum / 10);
            }
            catch
            {
            }
        }

        private void btnThemKhuvuc_Click(object sender, EventArgs e)
        {
            SetRongControl();
            KhoaMoControl(false);
            cboCUAHANG.Focus();
        }

        private void btnSuaKhuvuc_Click(object sender, EventArgs e)
        {
            if (smaKhuvuc == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl(false);
            cboCUAHANG.Focus();
        }

        private void btnXoaKhuvuc_Click(object sender, EventArgs e)
        {
            if (smaKhuvuc == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show(_xacnhanbanmuonxoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From KHUVUC" + "\n";
            sSQL += "Where MA_KHUVUC=" + clsMain.SQLString(smaKhuvuc) + "\n";

            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;

            if (bRunSQL)
            {
                LoadCombo();
                LoaddataGridView();
                btnThemKhuvuc.Focus();
                btCuahang_Click(pal_cuahang.Controls[0], null);
                btKhuvuc_Click(pal_khuvuc.Controls[0], null);
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong,_thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLuuKhuvuc_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(picKhuvuc.Image, typeof(Byte[]));
            SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
            SqlCommand SqlComMain = null;
            string sSQL = "";
            if (smaKhuvuc == "" && btnThemKhuvuc.Enabled)
            {
                sSQL += "Insert into KHUVUC (TEN_KHUVUC,MA_CUAHANG,GHICHU,SUDUNG,STT,HINHANH) Values (@TEN_KHUVUC,@MA_CUAHANG,@GHICHU,@SUDUNG,@STT,@HINHANH)" + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);
                SqlCom.Parameters.Add(new SqlParameter("@TEN_KHUVUC", (object)txt_khuvuc.Text));
                SqlCom.Parameters.Add(new SqlParameter("@MA_CUAHANG", (object)cboCUAHANG.EditValue.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txt_ghichu.Text));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSUDUNG.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@STT", (object)txtSTT.Text));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlComMain = SqlCom;
            }
            else if ((smaKhuvuc != "" && btnSuaKhuvuc.Enabled))
            {
                sSQL += "Update KHUVUC Set TEN_KHUVUC=@TEN_KHUVUC,MA_CUAHANG=@MA_CUAHANG,GHICHU=@GHICHU,SUDUNG=@SUDUNG,STT=@STT,HINHANH=@HINHANH " + "\n";
                sSQL += "Where MA_KHUVUC=" + clsMain.SQLString(smaKhuvuc) + "\n";
                SqlCommand SqlCom = new SqlCommand(sSQL, CN);
                SqlCom.Parameters.Add(new SqlParameter("@TEN_KHUVUC", (object)txt_khuvuc.Text));
                SqlCom.Parameters.Add(new SqlParameter("@MA_CUAHANG", (object)cboCUAHANG.EditValue.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)txt_ghichu.Text));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSUDUNG.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@STT", (object)txtSTT.Text));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlComMain = SqlCom;
            }
            else
            {
                SqlComMain = null;
            }

            Boolean bRunSQL = true;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                CN.Open();
                SqlComMain.ExecuteNonQuery();
                CN.Close();
                this.Cursor = Cursors.Default;
                bRunSQL = true;
            }
            catch
            {
                bRunSQL = false;
            }

            if (bRunSQL)
            {
                LoadCombo();
                LoaddataGridView();
                btnThemKhuvuc.Focus();
                try
                {
                    btCuahang_Click(pal_cuahang.Controls[0], null);
                    btKhuvuc_Click(pal_khuvuc.Controls[0], null);
                }
                catch
                {
                }
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bnt_tranglen_Click(object sender, EventArgs e)
        {
            string[] arr;
            arr = lbl_sotrang.Text.Split('/');
            if (Convert.ToInt32(arr[0]) == 1)
                return;
            else
            {
                trang = (Convert.ToInt32(arr[0]) - 1).ToString();
                lbl_sotrang.Text = trang + "/5";
                lbl_sotrang1.Text = trang + "/5";
                pal_ban.Controls.Clear();
                LoaddataGridView1(smaKhuvuc, trang);
            }
        }

        private void bnt_trangxuong_Click(object sender, EventArgs e)
        {
            string[] arr;
            arr = lbl_sotrang.Text.Split('/');
            if (Convert.ToInt32(arr[0]) == 5)
                return;
            else
            {
                trang = (Convert.ToInt32(arr[0]) + 1).ToString();
                lbl_sotrang.Text = trang + "/5";
                lbl_sotrang1.Text = trang + "/5";
                pal_ban.Controls.Clear();
                LoaddataGridView1(smaKhuvuc, trang);
            }
        }

        private void pic_hinhnen_Click(object sender, EventArgs e)
        {
            if (pic_hinhnen.Enabled)
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = Application.StartupPath + "\\Images";
                openFileDialog1.Filter = "Image Files|*.jpg;*.bmp;*.png;*.gif|All Files|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.Title = "Chọn ảnh";
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        pic_hinhnen.ImageLocation = openFileDialog1.FileName;
                        pic_hinhnen.BackgroundImage = null;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {
                        pic_hinhnen.Image = null;
                        pic_hinhnen.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26; 
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void picAnhban_Click(object sender, EventArgs e)
        {
            if (picAnhban.Enabled)
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = Application.StartupPath + "\\Images";
                openFileDialog1.Filter = "Image Files|*.jpg;*.bmp;*.png;*.gif|All Files|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.Title = _chonanh;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        picAnhban.ImageLocation = openFileDialog1.FileName;
                        picAnhban.BackgroundImage = null;
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {
                        picAnhban.Image = null;
                        picAnhban.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26; 
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void picKhuvuc_Click(object sender, EventArgs e)
        {
            if (picKhuvuc.Enabled)
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = Application.StartupPath + "\\Images";
                openFileDialog1.Filter = "Image Files|*.jpg;*.bmp;*.png;*.gif|All Files|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.Title = "Chọn ảnh";
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        picKhuvuc.ImageLocation = openFileDialog1.FileName;
                        picKhuvuc.BackgroundImage = null ; 
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {
                        picKhuvuc.Image = null;
                        picKhuvuc.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26; 
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void bnt_themnhanh_Click(object sender, EventArgs e)
        {
            pal_themnhanh.Visible =! pal_themnhanh.Visible;
            cbb_kichthuocban.Text = _kho1;
            txt_soban.Focus();
        }

        private void bnt_themban_Click(object sender, EventArgs e)
        {
            SetRongControl1();
            KhoaMoControl1(false);
            cboKhuvuc.Focus();
        }

        private void bnt_suaban_Click(object sender, EventArgs e)
        {
            if (smaBan == "")//Chưa chọn dòng để sữa
            {
                return;
            }
            KhoaMoControl1(false);
            cboKhuvuc.Focus();
        }

        private void bnt_xoaban_Click(object sender, EventArgs e)
        {
            if (smaBan == "")//Chưa chọn dòng để xóa
            {
                return;
            }
            if (XtraMessageBox.Show(_xacnhanbanmuonxoa, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sSQL = "";
            sSQL += "Delete From BAN" + "\n";
            sSQL += "Where MA_BAN=" + clsMain.SQLString (smaBan) + "\n";
            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sSQL);
            this.Cursor = Cursors.Default;
            if (bRunSQL)
            {
                pal_ban.Controls.Clear();
                LoaddataGridView1(smaKhuvuc, trang);
            }
            else
            {
                XtraMessageBox.Show(_xoakhongthanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bnt_luuban_Click(object sender, EventArgs e)
        {
            if (!CheckInput_ban())
            {
                return;
            }
            float width = 110;
            float height = 80;
            if (cbb_kichthuoc.Text.Equals(_kho2))
                width = width * 2 ;
            else if (cbb_kichthuoc.Text.Equals(_kho3))
                height = height * 2 ;
            else if (cbb_kichthuoc.Text.Equals(_kho4))
            {
                width = width * 2 ;
                height = height * 2 ;
            }
            Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(pic_hinhnen.Image, typeof(Byte[]));
            SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
            SqlCommand SqlComMain = null;
            string sql = "";
            if (smaBan == "" && bnt_themban.Enabled )
            {
                sql += "INSERT INTO BAN(TEN_BAN,MA_KHUVUC,SOGHE,Width,Height,LocaY,LocaX,SUDUNG,HINHANH,TRANG) values ";
                sql += "(@TEN_BAN,@MA_KHUVUC,@SOGHE,@Width,@Height,@LocaY,@LocaX,@SUDUNG,@HINHANH,@TRANG)" + "\n";
                SqlCommand SqlCom = new SqlCommand(sql, CN);
                SqlCom.Parameters.Add(new SqlParameter("@TEN_BAN", (object)txt_ban.Text));
                SqlCom.Parameters.Add(new SqlParameter("@MA_KHUVUC", (object)cboKhuvuc.EditValue.ToString ()));
                SqlCom.Parameters.Add(new SqlParameter("@SOGHE", (object)txt_soghe.Text));
                SqlCom.Parameters.Add(new SqlParameter("@Width", (object)width));
                SqlCom.Parameters.Add(new SqlParameter("@Height", (object)height));
                SqlCom.Parameters.Add(new SqlParameter("@LocaY", (object)3));
                SqlCom.Parameters.Add(new SqlParameter("@LocaX", (object)3));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSudungban.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlCom.Parameters.Add(new SqlParameter("@TRANG", (object)trang));
                SqlComMain = SqlCom;
            }
            else if(smaBan != "" && bnt_suaban.Enabled)
            {
                sql += "UPDATE BAN SET TEN_BAN=@TEN_BAN,MA_KHUVUC=@MA_KHUVUC,SOGHE=@SOGHE,Width=@Width,Height=@Height,SUDUNG=@SUDUNG,HINHANH=@HINHANH"+ "\n";
                sql += "Where MA_BAN=" + clsMain.SQLString(smaBan) + "\n";
                SqlCommand SqlCom = new SqlCommand(sql, CN);
                SqlCom.Parameters.Add(new SqlParameter("@TEN_BAN", (object)txt_ban.Text));
                SqlCom.Parameters.Add(new SqlParameter("@MA_KHUVUC", (object)cboKhuvuc.EditValue.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@SOGHE", (object)txt_soghe.Text));
                SqlCom.Parameters.Add(new SqlParameter("@Width", (object)width));
                SqlCom.Parameters.Add(new SqlParameter("@Height", (object)height));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSUDUNG.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlComMain = SqlCom;
            }
            Boolean bRunSQL = true;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                CN.Open();
                SqlComMain.ExecuteNonQuery();
                CN.Close();
                this.Cursor = Cursors.Default;
                bRunSQL = true;
            }
            catch
            {
                bRunSQL = false;
            }
            if (bRunSQL)
            {
                pal_ban.Controls.Clear();
                bnt_themban.Focus();
                LoaddataGridView1(smaKhuvuc, trang);
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLuusodoban_Click(object sender, EventArgs e)
        {
            string sql = "";
            foreach(SimpleButton btn in pal_ban .Controls )
            {
                sql += "UPDATE BAN SET " + "\n";
                sql += "LocaX=" + btn.Location.X + ",";
                sql += "LocaY=" + btn.Location.Y + "\n";
                sql += "Where MA_BAN =" + clsMain.SQLString(btn.Tag.ToString()) + "\n"; ;
            }
            this.Cursor = Cursors.WaitCursor;
            Boolean bRunSQL = clsMain.ExecuteSQL(sql);
            this.Cursor = Cursors.Default;
            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_capnhatthanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_capnhatthatbai, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        
        private void bnt_default_Click(object sender, EventArgs e)
        {
            Sapxepbanmacdinh(true);
        }

        private void btnLuuthemnhanh_Click(object sender, EventArgs e)
        {
            float width = 110;
            float height = 80;
            if (cbb_kichthuoc.Text.Equals(_kho2))
            {
                width *= 2;
            }
            else if (cbb_kichthuoc.Text.Equals(_kho3))
            {
                height *= 2;
            }
            else if (cbb_kichthuoc.Text.Equals(_kho4))
            {
                width *= 2;
                height *= 2;
            }

            if (txt_soban.Text == "")
            {
                XtraMessageBox.Show(_nhapsoban, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txt_soban.Focus();
                return;
            }
            if (Convert.ToInt32(txt_soban.Text) <= 0)
            {
                XtraMessageBox.Show(_sobanphailonhon0, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txt_soban.Focus();
                return;
            }

            Boolean bRunSQL = true;
            Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(picAnhban.Image, typeof(Byte[]));
            SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
            SqlCommand SqlCom = null;
            string sql = "";
            int n = Convert.ToInt32(txt_soban.Text);
            int tenban = 1;

            /*
            String sqlTONTAI = "select COUNT(MA_BAN) AS SOBAN from BAN where MA_KHUVUC=" +clsMain .SQLString ( smaKhuvuc);
            DataTable dtTONTAI = clsMain.ReturnDataTable(sqlTONTAI);
            if (dtTONTAI.Rows.Count > 0)
                tenban = Convert.ToInt32(dtTONTAI.Rows[0]["SOBAN"].ToString()) + 1;
            */
            tenban = Convert.ToInt32(txtBatDau.Text);

            for (int i = 0; i < n; i++)
            {
                tenban = tenban + i;
                sql = "INSERT INTO BAN(TEN_BAN,MA_KHUVUC,SOGHE,Width,Height,LocaY,LocaX,SUDUNG,HINHANH,TRANG) values ";
                sql += "(@TEN_BAN,@MA_KHUVUC,@SOGHE,@Width,@Height,@LocaY,@LocaX,@SUDUNG,@HINHANH,@TRANG)" + "\n";
                SqlCom = new SqlCommand(sql, CN);
                SqlCom.Parameters.Add(new SqlParameter("@TEN_BAN", (object) txtTenBanNhieu.Text + " " + tenban.ToString()));
                SqlCom.Parameters.Add(new SqlParameter("@MA_KHUVUC", (object)smaKhuvuc));
                SqlCom.Parameters.Add(new SqlParameter("@SOGHE", (object)txt_sogheban.Text));
                SqlCom.Parameters.Add(new SqlParameter("@Width", (object)width));
                SqlCom.Parameters.Add(new SqlParameter("@Height", (object)height));
                SqlCom.Parameters.Add(new SqlParameter("@LocaY", (object)3));
                SqlCom.Parameters.Add(new SqlParameter("@LocaX", (object)3));
                SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)chkSudungban.Checked));
                SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
                SqlCom.Parameters.Add(new SqlParameter("@TRANG", (object)trang));
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    CN.Open();
                    SqlCom.ExecuteNonQuery();
                    SqlCom = null;
                    CN.Close();
                    this.Cursor = Cursors.Default;
                    bRunSQL = true;
                    tenban = tenban - i;
                }
                catch
                {
                    bRunSQL = false;
                }
            }
            if (bRunSQL)
            {
                Sapxepbanmacdinh(false);
                pal_themnhanh.Visible = false;
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(_luukothanhcong,_thongbao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Sapxepbanmacdinh(bool ShowMessage)
        {
            bool dongy = true;
            if (ShowMessage)
            {
                if (DialogResult.Yes == (XtraMessageBox.Show(_quayvemacdinhban, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Information)))
                {
                    dongy = true;
                }
                else
                {
                    dongy = false;
                }
            }
            if (dongy)
            {
                string sqldefault = "";
                for (int h = 1; h <= 5; h++)
                {
                    string sql = "select MA_BAN from BAN where MA_KHUVUC=" + clsMain.SQLString(smaKhuvuc) + "and SUDUNG=1 and TRANG=" + clsMain.SQLString(h.ToString());
                    DataTable dt = clsMain.ReturnDataTable(sql);
                    int wid = pal_ban.Width / 115;
                    int du = pal_ban.Width - (wid * 115);
                    int hei = pal_ban.Height / 85;
                    if (dt.Rows.Count > 0)
                    {
                        int count = dt.Rows.Count;
                        int tempX = 1;
                        int tempY = 1;
                        int x = 2;
                        int y = 2;
                        for (int i = 0; i < count; i++)
                        {
                            if (tempX > wid)
                            {
                                y += 85;
                                x = 2;
                                tempX = 1;
                                tempY += 1;
                            }
                            if (tempY <= hei)
                                sqldefault += "update BAN set width=110,height=80, LocaX=" + x + ",LocaY=" + y + " where MA_BAN=" + clsMain.SQLString(dt.Rows[i]["MA_BAN"].ToString()) + "\n";
                            else
                            {
                                int pt;//nhảy sang trang pt nếu bàn đầy ở trang h trừ khi h=5
                                if (h < 5)
                                    pt = h + 1;
                                else
                                    pt = h;
                                sqldefault += "update BAN set width=110,height=80, LocaX=" + (pal_ban.Width - 115) + ",LocaY=" + (pal_ban.Height - 85) + ",TRANG="
                                                + clsMain.SQLString(pt.ToString()) + " where MA_BAN=" + clsMain.SQLString(dt.Rows[i]["MA_BAN"].ToString()) + "\n";
                            }
                                
                            x += 115 + (du / wid);
                            tempX += 1;
                        }
                    }
                }
                this.Cursor = Cursors.WaitCursor;
                Boolean bRunSQL = clsMain.ExecuteSQL(sqldefault);
                this.Cursor = Cursors.Default;
                if (bRunSQL)
                {
                    pal_ban.Controls.Clear();
                    LoaddataGridView1(smaKhuvuc, trang);
                }
                else
                {
                    XtraMessageBox.Show(_luukothanhcong, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

        }

        private void bnt_ban_len_Click(object sender, EventArgs e)
        {
            foreach (SimpleButton btn in pal_ban.Controls)
            {
                if (btn.Tag.ToString () == smaBan)
                {
                    btn.Location = new Point (btn.Location.X,btn .Location .Y -1);
                    break;
                }
            }
        }

        private void bnt_ban_len_MouseDown(object sender, MouseEventArgs e)
        {
            foreach (SimpleButton btn in pal_ban.Controls)
            {
                if (btn.Tag.ToString() == smaBan)
                {
                    btn.Location = new Point(btn.Location.X, btn.Location.Y - 1);
                    break;
                }
            }
            timer1.Start();
        }

        private void bnt_ban_len_MouseUp(object sender, MouseEventArgs e)
        {
            timer1.Stop();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string second;
            second = DateTime.Now.Second.ToString();
            int a = Convert.ToInt32(second);
            if (a % 1 == 0)
                bnt_ban_len_Click(sender, e);
        }

        private void bnt_ban_xuong_Click(object sender, EventArgs e)
        {
            foreach (SimpleButton btn in pal_ban.Controls)
            {
                if (btn.Tag.ToString() == smaBan)
                {
                    btn.Location = new Point(btn.Location.X, btn.Location.Y + 1);
                    break;
                }
            }
        }

        private void bnt_ban_xuong_MouseDown(object sender, MouseEventArgs e)
        {
            foreach (SimpleButton btn in pal_ban.Controls)
            {
                if (btn.Tag.ToString() == smaBan)
                {
                    btn.Location = new Point(btn.Location.X, btn.Location.Y + 1);
                    break;
                }
            }
            timer2.Start();
        }

        private void bnt_ban_xuong_MouseUp(object sender, MouseEventArgs e)
        {
            timer2.Stop();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            string second;
            second = DateTime.Now.Second.ToString();
            int a = Convert.ToInt32(second);
            if (a % 1 == 0)
                bnt_ban_xuong_Click(sender, e);
        }

        private void bnt_ban_trai_Click(object sender, EventArgs e)
        {
            foreach (SimpleButton btn in pal_ban.Controls)
            {
                if (btn.Tag.ToString() == smaBan)
                {
                    btn.Location = new Point(btn.Location.X-1, btn.Location.Y);
                    break;
                }
            }
        }

        private void bnt_ban_trai_MouseDown(object sender, MouseEventArgs e)
        {
            foreach (SimpleButton btn in pal_ban.Controls)
            {
                if (btn.Tag.ToString() == smaBan)
                {
                    btn.Location = new Point(btn.Location.X - 1, btn.Location.Y);
                    break;
                }
            }
            timer3.Start();
        }

        private void bnt_ban_trai_MouseUp(object sender, MouseEventArgs e)
        {
            timer3.Stop();
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            string second;
            second = DateTime.Now.Second.ToString();
            int a = Convert.ToInt32(second);
            if (a % 1 == 0)
                bnt_ban_trai_Click(sender, e);
        }

        private void bnt_ban_phai_Click(object sender, EventArgs e)
        {
            foreach (SimpleButton btn in pal_ban.Controls)
            {
                if (btn.Tag.ToString() == smaBan)
                {
                    btn.Location = new Point(btn.Location.X + 1, btn.Location.Y);
                    break;
                }
            }
        }

        private void bnt_ban_phai_MouseDown(object sender, MouseEventArgs e)
        {
            foreach (SimpleButton btn in pal_ban.Controls)
            {
                if (btn.Tag.ToString() == smaBan)
                {
                    btn.Location = new Point(btn.Location.X + 1, btn.Location.Y);
                    break;
                }
            }
            timer4.Start();
        }

        private void bnt_ban_phai_MouseUp(object sender, MouseEventArgs e)
        {
            timer4.Stop();
        }

        private void timer4_Tick(object sender, EventArgs e)
        {
            string second;
            second = DateTime.Now.Second.ToString();
            int a = Convert.ToInt32(second);
            if (a % 1 == 0)
                bnt_ban_phai_Click(sender, e);
        }

        private void btCuahang_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pal_cuahang.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }

            smaCuahang = btn.Tag.ToString();
            
            LoaddataGridView2(smaKhuvuc);
        }

        private void btKhuvuc_Click(object sender, EventArgs e)
        {
            btnChonKhuvuc_Click(null, null);

            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pal_khuvuc.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }

            smaKhuvuc = btn.Tag.ToString();
            cboCUAHANG.EditValue = int.Parse(dtKhuvuc.Select("MA=" + clsMain.SQLString(smaKhuvuc))[0]["MA_CUAHANG"].ToString());
            txt_khuvuc.Text = dtKhuvuc.Select("MA=" + clsMain.SQLString(smaKhuvuc))[0]["TEN"].ToString();
            txtSTT.Text = dtKhuvuc.Select("MA=" + clsMain.SQLString(smaKhuvuc))[0]["STT"].ToString();
            txt_ghichu.Text = dtKhuvuc.Select("MA=" + clsMain.SQLString(smaKhuvuc))[0]["GHICHU"].ToString();
            chkSUDUNG.Checked = bool.Parse(dtKhuvuc.Select("MA=" + clsMain.SQLString(smaKhuvuc))[0]["SUDUNG"].ToString());
            try
            {
                byte[] tam = new byte[((byte[])dtKhuvuc.Select("MA=" + clsMain.SQLString(smaKhuvuc))[0]["HINHANH"]).LongLength];
                tam = (byte[])dtKhuvuc.Select("MA=" + clsMain.SQLString(smaKhuvuc))[0]["HINHANH"];
                MemoryStream ms = new MemoryStream(tam);
                Bitmap bm = new Bitmap(ms);
                picKhuvuc.Image = bm;
                picKhuvuc.BackgroundImage = null; 
            }
            catch
            {
                picKhuvuc.Image = null;
                picKhuvuc.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26; 
            }
            pal_ban.Controls.Clear();
            LoaddataGridView1(smaKhuvuc, trang);
        }

        private void btban_Click(object sender, EventArgs e)
        {
            btnChonBan_Click(null, null);

            SimpleButton btn = (SimpleButton)sender;
            btn.ForeColor = Color.Red;
            foreach (SimpleButton btn1 in pal_ban.Controls)
            {
                if (btn1.Name != btn.Name)
                {
                    btn1.ForeColor = btnCapnhat.ForeColor;
                }
            }

            smaBan = btn.Tag.ToString();
            cboKhuvuc.EditValue = int.Parse(dtBan.Select("MA=" + clsMain.SQLString(smaBan))[0]["MA_KHUVUC"].ToString());
            txt_ban.Text = dtBan.Select("MA=" + clsMain.SQLString(smaBan))[0]["TEN"].ToString();
            txt_soghe.Text = dtBan.Select("MA=" + clsMain.SQLString(smaBan))[0]["SOGHE"].ToString();
            if (Convert.ToInt32(dtBan.Select("MA=" + clsMain.SQLString(smaBan))[0]["Width"].ToString()) == 110 & Convert.ToInt32(dtBan.Select("MA=" + clsMain.SQLString(smaBan))[0]["Height"].ToString()) == 80)
            {
                cbb_kichthuoc.Text = _kho1;
            }
            else if (Convert.ToInt32(dtBan.Select("MA=" + clsMain.SQLString(smaBan))[0]["Width"].ToString()) == 220 & Convert.ToInt32(dtBan.Select("MA=" + clsMain.SQLString(smaBan))[0]["Height"].ToString()) == 80)
            {
                cbb_kichthuoc.Text = _kho2;
            }
            else if (Convert.ToInt32(dtBan.Select("MA=" + clsMain.SQLString(smaBan))[0]["Width"].ToString()) == 110 & Convert.ToInt32(dtBan.Select("MA=" + clsMain.SQLString(smaBan))[0]["Height"].ToString()) == 160)
            {
                cbb_kichthuoc.Text = _kho3;
            }
            else
            {
                cbb_kichthuoc.Text = _kho4;
            }
            chkSudungban.Checked = bool.Parse(dtBan.Select("MA=" + clsMain.SQLString(smaBan))[0]["SUDUNG"].ToString());
            try
            {
                byte[] tam = new byte[((byte[])dtBan.Select("MA=" + clsMain.SQLString(smaBan))[0]["HINHANH"]).LongLength];
                tam = (byte[])dtBan.Select("MA=" + clsMain.SQLString(smaBan))[0]["HINHANH"];
                MemoryStream ms = new MemoryStream(tam);
                Bitmap bm = new Bitmap(ms);
                pic_hinhnen.Image = bm;
                pic_hinhnen.BackgroundImage = null;
            }
            catch
            {
                pic_hinhnen.Image = null;
                pic_hinhnen.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26; 
            }
        }

        private void btban_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void btban_MouseMove(object sender, MouseEventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                btn.Left = e.X + btn.Left - MouseDownLocation.X;
                btn.Top = e.Y + btn.Top - MouseDownLocation.Y;
            }
        }

        private void btban_MouseUp(object sender, MouseEventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            if (btn.Left <= 0)
                btn.Left = 0;
            if (btn.Top <= 0)
                btn.Top = 0;
            if (btn.Left > pal_ban.Width - btn.Width)
                btn.Left = pal_ban.Width - btn.Size.Width;
            if (btn.Top > pal_ban.Height - btn.Height)
                btn.Top = pal_ban.Height - btn.Size.Height;
        }  

        private void SetRongControl()
        {
            smaKhuvuc = "";
            cboCUAHANG.EditValue = (smaCuahang != "") ? int.Parse(smaCuahang) : cboCUAHANG.Properties.GetDataSourceValue(cboCUAHANG.Properties.ValueMember, 0); 
            txt_khuvuc.Text = "";
            txt_ghichu.Text = "";
            txtSTT.Text = "1";
            chkSUDUNG.Checked = true;
            picKhuvuc.Image = null;
            picKhuvuc.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26; 
        }

        private void SetRongControl1()
        {
            smaBan = "";
            cboKhuvuc.EditValue = (smaKhuvuc != "") ? int.Parse(smaKhuvuc) : cboKhuvuc.Properties.GetDataSourceValue(cboKhuvuc.Properties.ValueMember, 0);
            txt_ban.Text = "";
            txt_soghe.Text = "1";
            cbb_kichthuoc.Text = _kho1;
            chkSudungban.Checked = true;
            pic_hinhnen.Image = null;
            pic_hinhnen.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26; 
        }

        private void KhoaMoControl(bool tf)
        {
            if (tf == true)//mở
            {
                cboCUAHANG.Properties.ReadOnly = true;
                txt_khuvuc.Properties.ReadOnly = true;
                txt_ghichu.Properties.ReadOnly = true;
                txtSTT.Properties.ReadOnly = true;
                chkSUDUNG.Properties.ReadOnly = true;
                picKhuvuc.Enabled  = false;
            }
            else//đóng
            {
                cboCUAHANG.Properties.ReadOnly = false;
                txt_khuvuc.Properties.ReadOnly = false;
                txt_ghichu.Properties.ReadOnly = false;
                txtSTT.Properties.ReadOnly = false;
                chkSUDUNG.Properties.ReadOnly = false;
                picKhuvuc.Enabled = true ;
            }
        }

        private void KhoaMoControl1(bool tf)
        {
            if (tf == true)//mở
            {
                cboKhuvuc.Properties.ReadOnly = true;
                txt_ban.Properties.ReadOnly = true;
                txt_soghe.Properties.ReadOnly = true;
                cbb_kichthuoc.Properties.ReadOnly = true;
                chkSudungban.Properties.ReadOnly = true;
                pic_hinhnen.Enabled = false ;
            }
            else//đóng
            {
                cboKhuvuc.Properties.ReadOnly = false;
                txt_ban.Properties.ReadOnly = false;
                txt_soghe.Properties.ReadOnly = false;
                cbb_kichthuoc.Properties.ReadOnly = false;
                chkSudungban.Properties.ReadOnly = false;
                pic_hinhnen.Enabled = true  ;
            }
        }

        private void LoadCombo()
        {
            string sSQL = "";
            sSQL += "Select MA_CUAHANG As MA,TEN_CUAHANG As TEN" + "\n";
            sSQL += "From CUAHANG" + "\n";
            sSQL += "Where SUDUNG=1 ";
            sSQL += "Order By TEN" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            cboCUAHANG.Properties.DataSource = dt;
            cboCUAHANG.EditValue = cboCUAHANG.Properties.GetDataSourceValue(cboCUAHANG.Properties.ValueMember, 0);


            sSQL = "";
            sSQL += "Select MA_KHUVUC As MA,TEN_KHUVUC As TEN" + "\n";
            sSQL += "From KHUVUC" + "\n";
            sSQL += "Where SUDUNG=1 ";
            sSQL += "Order By TEN" + "\n";
            dt = clsMain.ReturnDataTable(sSQL);
            cboKhuvuc.Properties.DataSource = dt;
            cboKhuvuc.EditValue = cboKhuvuc.Properties.GetDataSourceValue(cboKhuvuc.Properties.ValueMember, 0);

            
        }

        private void LoadPermission()
        {
            btnThemKhuvuc.Enabled = clsUserManagement.AllowAdd("5");
            btnLuuKhuvuc.Enabled = clsUserManagement.AllowEdit("5");
            btnXoaKhuvuc.Enabled = clsUserManagement.AllowDelete("5");
            btnLuuKhuvuc.Enabled = btnThemKhuvuc.Enabled || btnSuaKhuvuc.Enabled;

            bnt_themban.Enabled = clsUserManagement.AllowAdd("5");
            bnt_suaban.Enabled = clsUserManagement.AllowEdit("5");
            bnt_xoaban.Enabled = clsUserManagement.AllowDelete("5");
            bnt_luuban.Enabled = bnt_themban.Enabled || bnt_suaban.Enabled;
            btnLuusodoban.Enabled = bnt_luuban.Enabled;
            bnt_macdinh.Enabled = bnt_luuban.Enabled;
        }

        DataTable dtKhuvuc = new DataTable();
        private void LoaddataGridView()
        {
            SetRongControl();
            KhoaMoControl(true);

            string sSQL = "";

            sSQL += "Select MA_CUAHANG as MA,TEN_CUAHANG as TEN" + "\n";
            sSQL += "From CUAHANG" + "\n";
            sSQL += "Order By TEN_CUAHANG" + "\n";
            DataTable dtCuahang = new DataTable();
            dtCuahang = clsMain.ReturnDataTable(sSQL);
            pal_cuahang.Controls.Clear();
            foreach (DataRow dr in dtCuahang.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 102;
                btn.Height = 80;
                btn.Font = pal_cuahang.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Name = "N" + dr["MA"].ToString();
                btn.Tag = dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Click += new EventHandler(btCuahang_Click);
                pal_cuahang.Controls.Add(btn);
            }
        }

        DataTable dtBan = new DataTable();
        private void LoaddataGridView1(string makhuvuc, string trang)
        {
            SetRongControl1();
            KhoaMoControl1(true);

            string sqlban = "select MA_BAN As MA,TEN_BAN As TEN,MA_KHUVUC,SOGHE,Width,Height,LocaY,LocaX,SUDUNG,HINHANH,TRANG from BAN where MA_KHUVUC=" +clsMain .SQLString (makhuvuc) + "and TRANG=" + clsMain.SQLString (trang)+"\n";
            sqlban += "order by MA DESC";
            dtBan = clsMain.ReturnDataTable(sqlban);
            pal_ban.Controls.Clear();
            foreach (DataRow dr in dtBan.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Font = pal_ban.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Name = "N" + dr["MA"].ToString();
                btn.Tag = dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                btn.Size = new Size(Convert.ToInt32(dr["Width"].ToString()), Convert.ToInt32(dr["Height"].ToString ()));
                btn.Location = new Point(Convert.ToInt32(dr["LocaX"].ToString()), Convert.ToInt32(dr["LocaY"].ToString ()));
                try
                {
                    byte[] tam = new byte[((byte[])dr["HINHANH"]).LongLength];
                    tam = (byte[])dr["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    if (cls_KP_RES.iHinhanh == 1)
                    {
                        btn.BackgroundImage = bm;
                        btn.BackgroundImageLayout = ImageLayout.Stretch;
                        btn.Appearance.BackColor = Color.Transparent;
                        btn.Appearance.Options.UseBackColor = true;
                        btn.ButtonStyle = BorderStyles.NoBorder;
                        btn.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                    }
                    else
                    {
                        btn.Image = bm;
                    }
                }
                catch
                {
                }
                btn.Click += new System.EventHandler(btban_Click);
                btn.MouseDown += new System.Windows.Forms.MouseEventHandler(btban_MouseDown);
                btn.MouseMove += new System.Windows.Forms.MouseEventHandler(btban_MouseMove);
                btn.MouseUp += new System.Windows.Forms.MouseEventHandler(btban_MouseUp);
                pal_ban.Controls.Add(btn);
            }
        }

        private void LoaddataGridView2(string macuahang)
        {
            string sSQL = "";
            sSQL += "Select MA_KHUVUC As MA,TEN_KHUVUC As TEN,MA_CUAHANG,GHICHU,SUDUNG,STT,HINHANH" + "\n";
            sSQL += "From KHUVUC" + "\n";
            sSQL += "Where MODE=0" + "\n";
            sSQL += "And MA_CUAHANG="+clsMain .SQLString (smaCuahang) + "\n";
            sSQL += "Order by SUDUNG DESC,MA_CUAHANG,STT,TEN_KHUVUC" + "\n";
            dtKhuvuc = clsMain.ReturnDataTable(sSQL);
            pal_khuvuc.Controls.Clear();
            pal_ban.Controls.Clear();
            foreach (DataRow dr in dtKhuvuc.Rows)
            {
                SimpleButton btn = new SimpleButton();
                btn.Width = 125;
                btn.Height = 80;
                btn.Font = pal_khuvuc.Font;
                btn.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                btn.ImageLocation = ImageLocation.TopCenter;
                btn.Name = "N" + dr["MA"].ToString();
                btn.Tag = dr["MA"].ToString();
                btn.Text = dr["TEN"].ToString();
                try
                {
                    byte[] tam = new byte[((byte[])dr["HINHANH"]).LongLength];
                    tam = (byte[])dr["HINHANH"];
                    MemoryStream ms = new MemoryStream(tam);
                    Bitmap bm = new Bitmap(ms);
                    if (cls_KP_RES.iHinhanh == 1)
                    {
                        btn.BackgroundImage = bm;
                        btn.BackgroundImageLayout = ImageLayout.Stretch;
                        btn.Appearance.BackColor = Color.Transparent;
                        btn.Appearance.Options.UseBackColor = true;
                        btn.ButtonStyle = BorderStyles.NoBorder;
                        btn.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
                    }
                    else
                    {
                        btn.Image = bm;
                    }
                }
                catch
                {
                }
                btn.Click += new EventHandler(btKhuvuc_Click);
                pal_khuvuc.Controls.Add(btn);
            }
            if (pal_khuvuc.VerticalScroll.Visible)
            {
                panel.Width = 152;
            }
            else
            {
                panel.Width = 135;
            }
        }

        private Boolean CheckInput()
        {
            if (txt_khuvuc.Text == "")
            {
                XtraMessageBox.Show(_nhapkhuvuc, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txt_khuvuc.Focus();
                return false;
            }
            if (smaKhuvuc == "" && btnThemKhuvuc.Enabled && txt_khuvuc.Text != "" && clsMain.ReturnDataTable("Select MA_KHUVUC From KHUVUC Where SUDUNG=1 And TEN_KHUVUC=" + clsMain.SQLStringUnicode(txt_khuvuc.Text) + " And MA_CUAHANG=" + clsMain.SQLStringUnicode(cboCUAHANG.EditValue.ToString())).Rows.Count > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show(_trungtenkhuvuc, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    txt_khuvuc.Focus();
                    return false;
                }
            }
            if (smaKhuvuc != "" && btnThemKhuvuc.Enabled && txt_khuvuc.Text != "" && clsMain.ReturnDataTable("Select MA_KHUVUC From KHUVUC Where SUDUNG=1 And TEN_KHUVUC=" + clsMain.SQLStringUnicode(txt_khuvuc.Text) + " And MA_KHUVUC != " + clsMain.SQLString(smaKhuvuc) + " And MA_CUAHANG=" + clsMain.SQLStringUnicode(cboCUAHANG.EditValue.ToString())).Rows.Count > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show(_trungtenkhuvuc, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    txt_khuvuc.Focus();
                    return false;
                }
            }
            return true;
        }

        private Boolean CheckInput_ban()
        {
            if (txt_ban.Text == "")
            {
                XtraMessageBox.Show(_nhaptenban, _thongbao, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txt_ban.Focus();
                return false;
            }
            if (smaBan == "" && bnt_themban.Enabled && txt_ban.Text != "" && clsMain.ReturnDataTable("Select MA_BAN From BAN Where SUDUNG=1 And TEN_BAN=" + clsMain.SQLStringUnicode(txt_ban.Text) + " And MA_KHUVUC=" + clsMain.SQLStringUnicode(cboKhuvuc.EditValue.ToString())).Rows.Count > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show(_trungtenban, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    txt_ban.Focus();
                    return false;
                }
            }
            if (smaBan != "" && bnt_themban.Enabled && txt_ban.Text != "" && clsMain.ReturnDataTable("Select MA_BAN From BAN Where SUDUNG=1 And TEN_BAN=" + clsMain.SQLStringUnicode(txt_ban.Text) + " And MA_BAN != " + clsMain.SQLString(smaBan) + " And MA_KHUVUC=" + clsMain.SQLStringUnicode(cboKhuvuc.EditValue.ToString())).Rows.Count > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show(_trungtenban, _thongbao, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    txt_ban.Focus();
                    return false;
                }
            }
            return true;
        }

        string _nhap = "";
        string _thongbao = "";
        string _luukothanhcong = "";
        string _xacnhanbanmuonxoa = "";
        string _trungtenban = "";
        string _nhaptenban = "";
        string _chonanh = "";
        string _xoakhongthanhcong = "";
        string _capnhatthanhcong = "";
        string _capnhatthatbai = "";
        string _nhapsoban = "";
        string _sobanphailonhon0 = "";
        string _quayvemacdinhban = "";
        string _nhapkhuvuc = "";
        string _trungtenkhuvuc = "";
        string _kho1 = "";
        string _kho2 = "";
        string _kho3 = "";
        string _kho4 = "";
        CultureInfo culture = CultureInfo.CreateSpecificCulture(clsUserManagement.ReturnLanguage());
        ResourceManager rm = new ResourceManager("KP_RES.Language.KP_RES", typeof(Frm_Main).Assembly);
        private void Frm_KhuvucBan_Load(object sender, EventArgs e)
        {
            btnChonKhuvuc_Click(null, null);
            if (pal_cuahang.Controls.Count > 0)
            {
                btCuahang_Click(pal_cuahang.Controls[0], null);
            }
            if (pal_khuvuc.Controls.Count > 0)
            {
                btKhuvuc_Click(pal_khuvuc.Controls[0], null);
            }
            if (pal_khuvuc.VerticalScroll.Visible)
            {
                panel.Width = 152;
            }
            else
            {
                panel.Width = 135;
            }
            foreach (Control ctl in panel_Sandard.Controls)
            {
                ctl.Click += new EventHandler(ctl_Click);
            }
            foreach (Control ctl in pal_themnhanh.Controls)
            {
                ctl.Click += new EventHandler(ctl_Click);
            }
            foreach (Control ctl in panelControl16.Controls)
            {
                ctl.Click += new EventHandler(ctl_Click);
            }


         
            this.Text = rm.GetString("khuvucban", culture);
            btnChonKhuvuc.Text = rm.GetString("khuvuc", culture);
            btnChonBan.Text = rm.GetString("ban", culture);
            lbKhuVuc.Text = rm.GetString("khuvuc", culture);
            lbTenBan.Text = rm.GetString("tenban", culture);
            lbSoGhe.Text = rm.GetString("soghe", culture);
            lbKichThuoc.Text = rm.GetString("kichthuoc", culture);
            chkSudungban.Text = rm.GetString("sudung", culture);
            lbSoBan.Text = rm.GetString("soban", culture);
            lbSoGhe2.Text = rm.GetString("soghe", culture);
            lbKichThuoc1.Text = rm.GetString("kichthuoc", culture);
            btnLuuthemnhanh.Text = rm.GetString("luu", culture);
            btnLuusodoban.Text = rm.GetString("luusodoban", culture);
            bnt_macdinh.Text = rm.GetString("sapxepmacdinh", culture);
            lbCuaHang.Text = rm.GetString("cuahang", culture);
            lblTen.Text = rm.GetString("ten", culture);
            lbSTT.Text = rm.GetString("stt", culture);
            lbGhiChu.Text = rm.GetString("ghichu", culture);
            _thongbao = rm.GetString("thongbao", culture);
            _nhap = rm.GetString("nhap", culture);
            _luukothanhcong = rm.GetString("luukothanhcong", culture);
            _xacnhanbanmuonxoa = rm.GetString("xacnhanbanmuonxoa", culture);
            _trungtenban = rm.GetString("trungtenban", culture);
            _nhaptenban = rm.GetString("nhaptenban", culture);
            _chonanh = rm.GetString("chonanh", culture);
            _xoakhongthanhcong = rm.GetString("xoakhongthanhcong", culture);
            _capnhatthanhcong = rm.GetString("capnhatthanhcong", culture);
            _capnhatthatbai = rm.GetString("capnhatthatbai", culture);
            _nhapsoban = rm.GetString("nhapsoban", culture);
            _sobanphailonhon0 = rm.GetString("sobanphailonhon0", culture);
            _quayvemacdinhban = rm.GetString("quayvemacdinhban", culture);
            _nhapkhuvuc = rm.GetString("nhapkhuvuc", culture);
            _trungtenkhuvuc = rm.GetString("trungtenkhuvuc", culture);
            _kho1 = rm.GetString("kho1", culture);
            _kho2 = rm.GetString("kho2", culture);
            _kho3 = rm.GetString("kho3", culture);
            _kho4 = rm.GetString("kho4", culture);

            chkSUDUNG.Text = rm.GetString("sudung", culture);
            btnLuuKhuvuc.Text = rm.GetString("luu", culture);
            btnThemKhuvuc.Text = rm.GetString("them", culture);
            btnSuaKhuvuc.Text = rm.GetString("sua", culture);
            btnXoaKhuvuc.Text = rm.GetString("xoa", culture);
            bnt_luuban.Text = rm.GetString("luu", culture);
            bnt_themban.Text = rm.GetString("them", culture);
            bnt_suaban.Text = rm.GetString("sua", culture);
            bnt_xoaban.Text = rm.GetString("xoa", culture);
            bnt_themnhanh.Text = rm.GetString("themnhieuban", culture);

            cbb_kichthuoc.Properties.Items.Insert(0, _kho1);
            cbb_kichthuoc.Properties.Items.Insert(1, _kho2);
            cbb_kichthuoc.Properties.Items.Insert(2, _kho3);
            cbb_kichthuoc.Properties.Items.Insert(3, _kho4);
        }
    }
}