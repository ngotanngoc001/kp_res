﻿namespace KP_RES 
{
    partial class Frm_Dinhluong 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Dinhluong));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lblDVTTHANHPHAM1 = new DevExpress.XtraEditors.LabelControl();
            this.lbSoLuong = new DevExpress.XtraEditors.LabelControl();
            this.txtSOLUONGTP = new DevExpress.XtraEditors.TextEdit();
            this.lblDVTNGUYENLIEU1 = new DevExpress.XtraEditors.LabelControl();
            this.cboNguyenlieu = new DevExpress.XtraEditors.LookUpEdit();
            this.lblSoluong1 = new DevExpress.XtraEditors.LabelControl();
            this.lblDVTNGUYENLIEU = new DevExpress.XtraEditors.LabelControl();
            this.lbNguyenLieu = new DevExpress.XtraEditors.LabelControl();
            this.lblDVTTHANHPHAM = new DevExpress.XtraEditors.LabelControl();
            this.cboThanhpham = new DevExpress.XtraEditors.LookUpEdit();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.chkSUDUNG = new DevExpress.XtraEditors.CheckEdit();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.txtSOLUONG = new DevExpress.XtraEditors.TextEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENTHANHPHAM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DVTTHANHPHAM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENNGUYENLIEU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DVTNGUYENLIEU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SUDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MATHANHPHAM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MANGUYENLIEU = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSOLUONGTP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNguyenlieu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboThanhpham.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSOLUONG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.lblDVTTHANHPHAM1);
            this.panelControl1.Controls.Add(this.lbSoLuong);
            this.panelControl1.Controls.Add(this.txtSOLUONGTP);
            this.panelControl1.Controls.Add(this.lblDVTNGUYENLIEU1);
            this.panelControl1.Controls.Add(this.cboNguyenlieu);
            this.panelControl1.Controls.Add(this.lblSoluong1);
            this.panelControl1.Controls.Add(this.lblDVTNGUYENLIEU);
            this.panelControl1.Controls.Add(this.lbNguyenLieu);
            this.panelControl1.Controls.Add(this.lblDVTTHANHPHAM);
            this.panelControl1.Controls.Add(this.cboThanhpham);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnSua);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.chkSUDUNG);
            this.panelControl1.Controls.Add(this.lblTEN);
            this.panelControl1.Controls.Add(this.txtSOLUONG);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1221, 109);
            this.panelControl1.TabIndex = 0;
            // 
            // lblDVTTHANHPHAM1
            // 
            this.lblDVTTHANHPHAM1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDVTTHANHPHAM1.Location = new System.Drawing.Point(463, 37);
            this.lblDVTTHANHPHAM1.Margin = new System.Windows.Forms.Padding(4);
            this.lblDVTTHANHPHAM1.Name = "lblDVTTHANHPHAM1";
            this.lblDVTTHANHPHAM1.Size = new System.Drawing.Size(27, 19);
            this.lblDVTTHANHPHAM1.TabIndex = 10;
            this.lblDVTTHANHPHAM1.Text = "(...)";
            // 
            // lbSoLuong
            // 
            this.lbSoLuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSoLuong.Location = new System.Drawing.Point(320, 8);
            this.lbSoLuong.Margin = new System.Windows.Forms.Padding(4);
            this.lbSoLuong.Name = "lbSoLuong";
            this.lbSoLuong.Size = new System.Drawing.Size(64, 19);
            this.lbSoLuong.TabIndex = 2;
            this.lbSoLuong.Text = "Số lượng";
            // 
            // txtSOLUONGTP
            // 
            this.txtSOLUONGTP.EnterMoveNextControl = true;
            this.txtSOLUONGTP.Location = new System.Drawing.Point(388, 5);
            this.txtSOLUONGTP.Name = "txtSOLUONGTP";
            this.txtSOLUONGTP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSOLUONGTP.Properties.Appearance.Options.UseFont = true;
            this.txtSOLUONGTP.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSOLUONGTP.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSOLUONGTP.Properties.EditFormat.FormatString = "G";
            this.txtSOLUONGTP.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSOLUONGTP.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.None;
            this.txtSOLUONGTP.Size = new System.Drawing.Size(106, 26);
            this.txtSOLUONGTP.TabIndex = 3;
            // 
            // lblDVTNGUYENLIEU1
            // 
            this.lblDVTNGUYENLIEU1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDVTNGUYENLIEU1.Location = new System.Drawing.Point(981, 37);
            this.lblDVTNGUYENLIEU1.Margin = new System.Windows.Forms.Padding(4);
            this.lblDVTNGUYENLIEU1.Name = "lblDVTNGUYENLIEU1";
            this.lblDVTNGUYENLIEU1.Size = new System.Drawing.Size(27, 19);
            this.lblDVTNGUYENLIEU1.TabIndex = 12;
            this.lblDVTNGUYENLIEU1.Text = "(...)";
            // 
            // cboNguyenlieu
            // 
            this.cboNguyenlieu.EnterMoveNextControl = true;
            this.cboNguyenlieu.Location = new System.Drawing.Point(616, 5);
            this.cboNguyenlieu.Name = "cboNguyenlieu";
            this.cboNguyenlieu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNguyenlieu.Properties.Appearance.Options.UseFont = true;
            this.cboNguyenlieu.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNguyenlieu.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboNguyenlieu.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboNguyenlieu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboNguyenlieu.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tiền tệ", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DVT", "DVT")});
            this.cboNguyenlieu.Properties.DisplayMember = "TEN";
            this.cboNguyenlieu.Properties.DropDownItemHeight = 40;
            this.cboNguyenlieu.Properties.NullText = "";
            this.cboNguyenlieu.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboNguyenlieu.Properties.ShowHeader = false;
            this.cboNguyenlieu.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboNguyenlieu.Properties.ValueMember = "MA";
            this.cboNguyenlieu.Size = new System.Drawing.Size(220, 26);
            this.cboNguyenlieu.TabIndex = 5;
            this.cboNguyenlieu.EditValueChanged += new System.EventHandler(this.cboNguyenlieu_EditValueChanged);
            // 
            // lblSoluong1
            // 
            this.lblSoluong1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoluong1.Location = new System.Drawing.Point(842, 8);
            this.lblSoluong1.Margin = new System.Windows.Forms.Padding(4);
            this.lblSoluong1.Name = "lblSoluong1";
            this.lblSoluong1.Size = new System.Drawing.Size(64, 19);
            this.lblSoluong1.TabIndex = 6;
            this.lblSoluong1.Text = "Số lượng";
            // 
            // lblDVTNGUYENLIEU
            // 
            this.lblDVTNGUYENLIEU.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDVTNGUYENLIEU.Location = new System.Drawing.Point(806, 37);
            this.lblDVTNGUYENLIEU.Margin = new System.Windows.Forms.Padding(4);
            this.lblDVTNGUYENLIEU.Name = "lblDVTNGUYENLIEU";
            this.lblDVTNGUYENLIEU.Size = new System.Drawing.Size(27, 19);
            this.lblDVTNGUYENLIEU.TabIndex = 11;
            this.lblDVTNGUYENLIEU.Text = "(...)";
            // 
            // lbNguyenLieu
            // 
            this.lbNguyenLieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNguyenLieu.Location = new System.Drawing.Point(528, 8);
            this.lbNguyenLieu.Margin = new System.Windows.Forms.Padding(4);
            this.lbNguyenLieu.Name = "lbNguyenLieu";
            this.lbNguyenLieu.Size = new System.Drawing.Size(84, 19);
            this.lbNguyenLieu.TabIndex = 4;
            this.lbNguyenLieu.Text = "Nguyên liệu";
            // 
            // lblDVTTHANHPHAM
            // 
            this.lblDVTTHANHPHAM.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDVTTHANHPHAM.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblDVTTHANHPHAM.Location = new System.Drawing.Point(281, 37);
            this.lblDVTTHANHPHAM.Margin = new System.Windows.Forms.Padding(4);
            this.lblDVTTHANHPHAM.Name = "lblDVTTHANHPHAM";
            this.lblDVTTHANHPHAM.Size = new System.Drawing.Size(27, 19);
            this.lblDVTTHANHPHAM.TabIndex = 9;
            this.lblDVTTHANHPHAM.Text = "(...)";
            // 
            // cboThanhpham
            // 
            this.cboThanhpham.EnterMoveNextControl = true;
            this.cboThanhpham.Location = new System.Drawing.Point(120, 5);
            this.cboThanhpham.Name = "cboThanhpham";
            this.cboThanhpham.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboThanhpham.Properties.Appearance.Options.UseFont = true;
            this.cboThanhpham.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboThanhpham.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboThanhpham.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboThanhpham.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboThanhpham.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tiền tệ", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DVT", "DVT")});
            this.cboThanhpham.Properties.DisplayMember = "TEN";
            this.cboThanhpham.Properties.DropDownItemHeight = 40;
            this.cboThanhpham.Properties.NullText = "";
            this.cboThanhpham.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboThanhpham.Properties.ShowHeader = false;
            this.cboThanhpham.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboThanhpham.Properties.ValueMember = "MA";
            this.cboThanhpham.Size = new System.Drawing.Size(194, 26);
            this.cboThanhpham.TabIndex = 1;
            this.cboThanhpham.EditValueChanged += new System.EventHandler(this.cboThanhpham_EditValueChanged);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = ((System.Drawing.Image)(resources.GetObject("btnLuu.Image")));
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(120, 68);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 13;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnSua
            // 
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Image = ((System.Drawing.Image)(resources.GetObject("btnSua.Image")));
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(296, 68);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(80, 35);
            this.btnSua.TabIndex = 15;
            this.btnSua.Text = "&Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(384, 68);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(84, 35);
            this.btnXoa.TabIndex = 16;
            this.btnXoa.Text = "&Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = ((System.Drawing.Image)(resources.GetObject("btnThem.Image")));
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(208, 68);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 35);
            this.btnThem.TabIndex = 14;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // chkSUDUNG
            // 
            this.chkSUDUNG.EnterMoveNextControl = true;
            this.chkSUDUNG.Location = new System.Drawing.Point(118, 37);
            this.chkSUDUNG.Name = "chkSUDUNG";
            this.chkSUDUNG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNG.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNG.Properties.Caption = "Sử dụng";
            this.chkSUDUNG.Size = new System.Drawing.Size(92, 24);
            this.chkSUDUNG.TabIndex = 8;
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(6, 8);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(90, 19);
            this.lblTEN.TabIndex = 0;
            this.lblTEN.Text = "Thành phẩm";
            // 
            // txtSOLUONG
            // 
            this.txtSOLUONG.EnterMoveNextControl = true;
            this.txtSOLUONG.Location = new System.Drawing.Point(910, 5);
            this.txtSOLUONG.Name = "txtSOLUONG";
            this.txtSOLUONG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSOLUONG.Properties.Appearance.Options.UseFont = true;
            this.txtSOLUONG.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSOLUONG.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSOLUONG.Properties.EditFormat.FormatString = "G";
            this.txtSOLUONG.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSOLUONG.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.None;
            this.txtSOLUONG.Size = new System.Drawing.Size(101, 26);
            this.txtSOLUONG.TabIndex = 7;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1182, 109);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 581);
            this.panelControl2.TabIndex = 2;
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSize = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Controls.Add(this.btnXuongit);
            this.panelControl3.Controls.Add(this.btnXuongnhieu);
            this.panelControl3.Controls.Add(this.btnLenit);
            this.panelControl3.Controls.Add(this.btnLennhieu);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 82);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 417);
            this.panelControl3.TabIndex = 14;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 160);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(35, 97);
            this.panelControl4.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 257);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 337);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 80);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 0);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 499);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(0);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(0, 109);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1182, 581);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.MA,
            this.TENTHANHPHAM,
            this.DVTTHANHPHAM,
            this.CAN,
            this.TENNGUYENLIEU,
            this.DVTNGUYENLIEU,
            this.SOLUONG,
            this.SUDUNG,
            this.FILL,
            this.MATHANHPHAM,
            this.MANGUYENLIEU});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupPanelText = "Kéo thả cột vào đây để nhóm. Ctrl + F để tìm kiếm";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.AllowCellMerge = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFooter = true;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.TENTHANHPHAM, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView2_RowCellClick);
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.AllowFocus = false;
            this.STT.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 60;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA.Caption = "Mã";
            this.MA.DisplayFormat.FormatString = "000";
            this.MA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Visible = true;
            this.MA.VisibleIndex = 1;
            this.MA.Width = 100;
            // 
            // TENTHANHPHAM
            // 
            this.TENTHANHPHAM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TENTHANHPHAM.AppearanceHeader.Options.UseFont = true;
            this.TENTHANHPHAM.AppearanceHeader.Options.UseTextOptions = true;
            this.TENTHANHPHAM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENTHANHPHAM.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENTHANHPHAM.Caption = "Thành phẩm";
            this.TENTHANHPHAM.FieldName = "TENTHANHPHAM";
            this.TENTHANHPHAM.Name = "TENTHANHPHAM";
            this.TENTHANHPHAM.OptionsColumn.AllowEdit = false;
            this.TENTHANHPHAM.OptionsColumn.AllowFocus = false;
            this.TENTHANHPHAM.OptionsColumn.FixedWidth = true;
            this.TENTHANHPHAM.Visible = true;
            this.TENTHANHPHAM.VisibleIndex = 2;
            this.TENTHANHPHAM.Width = 200;
            // 
            // DVTTHANHPHAM
            // 
            this.DVTTHANHPHAM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DVTTHANHPHAM.AppearanceHeader.Options.UseFont = true;
            this.DVTTHANHPHAM.AppearanceHeader.Options.UseTextOptions = true;
            this.DVTTHANHPHAM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DVTTHANHPHAM.Caption = "Đơn vị tính";
            this.DVTTHANHPHAM.FieldName = "DVTTHANHPHAM";
            this.DVTTHANHPHAM.Name = "DVTTHANHPHAM";
            this.DVTTHANHPHAM.OptionsColumn.AllowEdit = false;
            this.DVTTHANHPHAM.OptionsColumn.AllowFocus = false;
            this.DVTTHANHPHAM.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.DVTTHANHPHAM.OptionsColumn.FixedWidth = true;
            this.DVTTHANHPHAM.Visible = true;
            this.DVTTHANHPHAM.VisibleIndex = 3;
            this.DVTTHANHPHAM.Width = 100;
            // 
            // CAN
            // 
            this.CAN.AppearanceCell.Options.UseTextOptions = true;
            this.CAN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CAN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.CAN.AppearanceHeader.Options.UseFont = true;
            this.CAN.AppearanceHeader.Options.UseTextOptions = true;
            this.CAN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CAN.Caption = "Cần";
            this.CAN.FieldName = "CAN";
            this.CAN.Name = "CAN";
            this.CAN.OptionsColumn.AllowEdit = false;
            this.CAN.OptionsColumn.AllowFocus = false;
            this.CAN.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.CAN.OptionsColumn.FixedWidth = true;
            this.CAN.Visible = true;
            this.CAN.VisibleIndex = 4;
            // 
            // TENNGUYENLIEU
            // 
            this.TENNGUYENLIEU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TENNGUYENLIEU.AppearanceHeader.Options.UseFont = true;
            this.TENNGUYENLIEU.AppearanceHeader.Options.UseTextOptions = true;
            this.TENNGUYENLIEU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENNGUYENLIEU.Caption = "Nguyên liệu";
            this.TENNGUYENLIEU.FieldName = "TENNGUYENLIEU";
            this.TENNGUYENLIEU.Name = "TENNGUYENLIEU";
            this.TENNGUYENLIEU.OptionsColumn.AllowEdit = false;
            this.TENNGUYENLIEU.OptionsColumn.AllowFocus = false;
            this.TENNGUYENLIEU.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.TENNGUYENLIEU.OptionsColumn.FixedWidth = true;
            this.TENNGUYENLIEU.Visible = true;
            this.TENNGUYENLIEU.VisibleIndex = 5;
            this.TENNGUYENLIEU.Width = 200;
            // 
            // DVTNGUYENLIEU
            // 
            this.DVTNGUYENLIEU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.DVTNGUYENLIEU.AppearanceHeader.Options.UseFont = true;
            this.DVTNGUYENLIEU.AppearanceHeader.Options.UseTextOptions = true;
            this.DVTNGUYENLIEU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DVTNGUYENLIEU.Caption = "Đơn vị tính";
            this.DVTNGUYENLIEU.FieldName = "DVTNGUYENLIEU";
            this.DVTNGUYENLIEU.Name = "DVTNGUYENLIEU";
            this.DVTNGUYENLIEU.OptionsColumn.AllowEdit = false;
            this.DVTNGUYENLIEU.OptionsColumn.AllowFocus = false;
            this.DVTNGUYENLIEU.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.DVTNGUYENLIEU.OptionsColumn.FixedWidth = true;
            this.DVTNGUYENLIEU.Visible = true;
            this.DVTNGUYENLIEU.VisibleIndex = 6;
            this.DVTNGUYENLIEU.Width = 100;
            // 
            // SOLUONG
            // 
            this.SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.Caption = "Số lượng";
            this.SOLUONG.FieldName = "SOLUONG";
            this.SOLUONG.Name = "SOLUONG";
            this.SOLUONG.OptionsColumn.AllowEdit = false;
            this.SOLUONG.OptionsColumn.AllowFocus = false;
            this.SOLUONG.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.SOLUONG.OptionsColumn.FixedWidth = true;
            this.SOLUONG.Visible = true;
            this.SOLUONG.VisibleIndex = 7;
            this.SOLUONG.Width = 85;
            // 
            // SUDUNG
            // 
            this.SUDUNG.AppearanceCell.Options.UseTextOptions = true;
            this.SUDUNG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SUDUNG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SUDUNG.AppearanceHeader.Options.UseFont = true;
            this.SUDUNG.AppearanceHeader.Options.UseTextOptions = true;
            this.SUDUNG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SUDUNG.Caption = "Sử dụng";
            this.SUDUNG.FieldName = "SUDUNG";
            this.SUDUNG.Name = "SUDUNG";
            this.SUDUNG.OptionsColumn.AllowEdit = false;
            this.SUDUNG.OptionsColumn.AllowFocus = false;
            this.SUDUNG.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.SUDUNG.OptionsColumn.FixedWidth = true;
            this.SUDUNG.Visible = true;
            this.SUDUNG.VisibleIndex = 8;
            this.SUDUNG.Width = 100;
            // 
            // FILL
            // 
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.FILL.OptionsColumn.AllowMove = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 9;
            this.FILL.Width = 20;
            // 
            // MATHANHPHAM
            // 
            this.MATHANHPHAM.Caption = "MATHANHPHAM";
            this.MATHANHPHAM.FieldName = "MATHANHPHAM";
            this.MATHANHPHAM.Name = "MATHANHPHAM";
            // 
            // MANGUYENLIEU
            // 
            this.MANGUYENLIEU.Caption = "MANGUYENLIEU";
            this.MANGUYENLIEU.FieldName = "MANGUYENLIEU";
            this.MANGUYENLIEU.Name = "MANGUYENLIEU";
            // 
            // Frm_Dinhluong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1221, 690);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Dinhluong";
            this.Text = "Định lượng";
            this.Load += new System.EventHandler(this.Frm_Dinhluong_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_Dinhluong_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSOLUONGTP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNguyenlieu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboThanhpham.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSOLUONG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNG;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.TextEdit txtSOLUONG;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn TENTHANHPHAM;
        private DevExpress.XtraGrid.Columns.GridColumn TENNGUYENLIEU;
        private DevExpress.XtraGrid.Columns.GridColumn SUDUNG;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl lblSoluong1;
        private DevExpress.XtraEditors.LabelControl lblDVTNGUYENLIEU;
        private DevExpress.XtraEditors.LabelControl lbNguyenLieu;
        private DevExpress.XtraEditors.LabelControl lblDVTTHANHPHAM;
        private DevExpress.XtraEditors.LookUpEdit cboThanhpham;
        private DevExpress.XtraGrid.Columns.GridColumn CAN;
        private DevExpress.XtraGrid.Columns.GridColumn SOLUONG;
        private DevExpress.XtraGrid.Columns.GridColumn MATHANHPHAM;
        private DevExpress.XtraGrid.Columns.GridColumn MANGUYENLIEU;
        private DevExpress.XtraGrid.Columns.GridColumn DVTTHANHPHAM;
        private DevExpress.XtraGrid.Columns.GridColumn DVTNGUYENLIEU;
        private DevExpress.XtraEditors.LookUpEdit cboNguyenlieu;
        private DevExpress.XtraEditors.LabelControl lblDVTNGUYENLIEU1;
        private DevExpress.XtraEditors.LabelControl lblDVTTHANHPHAM1;
        private DevExpress.XtraEditors.LabelControl lbSoLuong;
        private DevExpress.XtraEditors.TextEdit txtSOLUONGTP;




    }
}