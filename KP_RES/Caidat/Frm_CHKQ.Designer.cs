﻿namespace KP_RES 
{
    partial class Frm_CHKQ 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_CHKQ));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnTieudeVe = new DevExpress.XtraEditors.SimpleButton();
            this.btnTieude = new DevExpress.XtraEditors.SimpleButton();
            this.cboCUAHANG = new DevExpress.XtraEditors.LookUpEdit();
            this.cboKHO = new DevExpress.XtraEditors.LookUpEdit();
            this.chkSUDUNGQUAY = new DevExpress.XtraEditors.CheckEdit();
            this.chkSUDUNGKHO = new DevExpress.XtraEditors.CheckEdit();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.lbKho2 = new DevExpress.XtraEditors.LabelControl();
            this.txtGHICHUQUAY = new DevExpress.XtraEditors.TextEdit();
            this.lbGhiChu2 = new DevExpress.XtraEditors.LabelControl();
            this.txtTENKHO = new DevExpress.XtraEditors.TextEdit();
            this.lbGhiChu3 = new DevExpress.XtraEditors.LabelControl();
            this.txtTENQUAY = new DevExpress.XtraEditors.TextEdit();
            this.lbGhiChu = new DevExpress.XtraEditors.LabelControl();
            this.txtGHICHUCUAHANG = new DevExpress.XtraEditors.TextEdit();
            this.lbQuay = new DevExpress.XtraEditors.LabelControl();
            this.lbKho = new DevExpress.XtraEditors.LabelControl();
            this.chkSUDUNGCUAHANG = new DevExpress.XtraEditors.CheckEdit();
            this.lblTenCH = new DevExpress.XtraEditors.LabelControl();
            this.txtGHICHUKHO = new DevExpress.XtraEditors.TextEdit();
            this.lblTEN = new DevExpress.XtraEditors.LabelControl();
            this.txtTENCUAHANG = new DevExpress.XtraEditors.TextEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.pnlQuay = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.pnlKho = new System.Windows.Forms.FlowLayoutPanel();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pnlCuahang = new System.Windows.Forms.FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboCUAHANG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboKHO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGQUAY.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGKHO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUQUAY.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENKHO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENQUAY.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUCUAHANG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGCUAHANG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUKHO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENCUAHANG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnTieudeVe);
            this.panelControl1.Controls.Add(this.btnTieude);
            this.panelControl1.Controls.Add(this.cboCUAHANG);
            this.panelControl1.Controls.Add(this.cboKHO);
            this.panelControl1.Controls.Add(this.chkSUDUNGQUAY);
            this.panelControl1.Controls.Add(this.chkSUDUNGKHO);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnSua);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.lbKho2);
            this.panelControl1.Controls.Add(this.txtGHICHUQUAY);
            this.panelControl1.Controls.Add(this.lbGhiChu2);
            this.panelControl1.Controls.Add(this.txtTENKHO);
            this.panelControl1.Controls.Add(this.lbGhiChu3);
            this.panelControl1.Controls.Add(this.txtTENQUAY);
            this.panelControl1.Controls.Add(this.lbGhiChu);
            this.panelControl1.Controls.Add(this.txtGHICHUCUAHANG);
            this.panelControl1.Controls.Add(this.lbQuay);
            this.panelControl1.Controls.Add(this.lbKho);
            this.panelControl1.Controls.Add(this.chkSUDUNGCUAHANG);
            this.panelControl1.Controls.Add(this.lblTenCH);
            this.panelControl1.Controls.Add(this.txtGHICHUKHO);
            this.panelControl1.Controls.Add(this.lblTEN);
            this.panelControl1.Controls.Add(this.txtTENCUAHANG);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1020, 174);
            this.panelControl1.TabIndex = 0;
            // 
            // btnTieudeVe
            // 
            this.btnTieudeVe.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTieudeVe.Appearance.Options.UseFont = true;
            this.btnTieudeVe.Image = global::KP_RES.Properties.Resources.check_book_26;
            this.btnTieudeVe.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnTieudeVe.Location = new System.Drawing.Point(584, 132);
            this.btnTieudeVe.Margin = new System.Windows.Forms.Padding(4);
            this.btnTieudeVe.Name = "btnTieudeVe";
            this.btnTieudeVe.Size = new System.Drawing.Size(147, 35);
            this.btnTieudeVe.TabIndex = 24;
            this.btnTieudeVe.Text = "&Tiêu đề vé";
            this.btnTieudeVe.Click += new System.EventHandler(this.btnTieudeVe_Click);
            // 
            // btnTieude
            // 
            this.btnTieude.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTieude.Appearance.Options.UseFont = true;
            this.btnTieude.Image = global::KP_RES.Properties.Resources.check_book_26;
            this.btnTieude.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnTieude.Location = new System.Drawing.Point(429, 132);
            this.btnTieude.Margin = new System.Windows.Forms.Padding(4);
            this.btnTieude.Name = "btnTieude";
            this.btnTieude.Size = new System.Drawing.Size(147, 35);
            this.btnTieude.TabIndex = 23;
            this.btnTieude.Text = "&Tiêu đề bill";
            this.btnTieude.Click += new System.EventHandler(this.btnTieude_Click);
            // 
            // cboCUAHANG
            // 
            this.cboCUAHANG.EnterMoveNextControl = true;
            this.cboCUAHANG.Location = new System.Drawing.Point(414, 5);
            this.cboCUAHANG.Name = "cboCUAHANG";
            this.cboCUAHANG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCUAHANG.Properties.Appearance.Options.UseFont = true;
            this.cboCUAHANG.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCUAHANG.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboCUAHANG.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboCUAHANG.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboCUAHANG.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboCUAHANG.Properties.DisplayMember = "TEN";
            this.cboCUAHANG.Properties.DropDownItemHeight = 40;
            this.cboCUAHANG.Properties.NullText = "";
            this.cboCUAHANG.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboCUAHANG.Properties.ShowHeader = false;
            this.cboCUAHANG.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboCUAHANG.Properties.ValueMember = "MA";
            this.cboCUAHANG.Size = new System.Drawing.Size(248, 26);
            this.cboCUAHANG.TabIndex = 6;
            // 
            // cboKHO
            // 
            this.cboKHO.EnterMoveNextControl = true;
            this.cboKHO.Location = new System.Drawing.Point(749, 5);
            this.cboKHO.Name = "cboKHO";
            this.cboKHO.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboKHO.Properties.Appearance.Options.UseFont = true;
            this.cboKHO.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboKHO.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboKHO.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboKHO.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboKHO.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "TEN", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboKHO.Properties.DisplayMember = "TEN";
            this.cboKHO.Properties.DropDownItemHeight = 40;
            this.cboKHO.Properties.NullText = "";
            this.cboKHO.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboKHO.Properties.ShowHeader = false;
            this.cboKHO.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboKHO.Properties.ValueMember = "MA";
            this.cboKHO.Size = new System.Drawing.Size(248, 26);
            this.cboKHO.TabIndex = 13;
            // 
            // chkSUDUNGQUAY
            // 
            this.chkSUDUNGQUAY.EnterMoveNextControl = true;
            this.chkSUDUNGQUAY.Location = new System.Drawing.Point(749, 101);
            this.chkSUDUNGQUAY.Name = "chkSUDUNGQUAY";
            this.chkSUDUNGQUAY.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNGQUAY.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNGQUAY.Properties.Caption = "Sử dụng";
            this.chkSUDUNGQUAY.Size = new System.Drawing.Size(92, 24);
            this.chkSUDUNGQUAY.TabIndex = 18;
            // 
            // chkSUDUNGKHO
            // 
            this.chkSUDUNGKHO.EnterMoveNextControl = true;
            this.chkSUDUNGKHO.Location = new System.Drawing.Point(414, 101);
            this.chkSUDUNGKHO.Name = "chkSUDUNGKHO";
            this.chkSUDUNGKHO.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNGKHO.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNGKHO.Properties.Caption = "Sử dụng";
            this.chkSUDUNGKHO.Size = new System.Drawing.Size(92, 24);
            this.chkSUDUNGKHO.TabIndex = 11;
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(77, 132);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(80, 35);
            this.btnLuu.TabIndex = 19;
            this.btnLuu.Text = "&Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnSua
            // 
            this.btnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Appearance.Options.UseFont = true;
            this.btnSua.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnSua.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSua.Location = new System.Drawing.Point(253, 132);
            this.btnSua.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(80, 35);
            this.btnSua.TabIndex = 21;
            this.btnSua.Text = "&Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Appearance.Options.UseFont = true;
            this.btnXoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoa.Location = new System.Drawing.Point(341, 132);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(80, 35);
            this.btnXoa.TabIndex = 22;
            this.btnXoa.Text = "&Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Image = global::KP_RES.Properties.Resources.add_file_26;
            this.btnThem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThem.Location = new System.Drawing.Point(165, 132);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(80, 35);
            this.btnThem.TabIndex = 20;
            this.btnThem.Text = "&Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // lbKho2
            // 
            this.lbKho2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbKho2.Location = new System.Drawing.Point(668, 8);
            this.lbKho2.Margin = new System.Windows.Forms.Padding(4);
            this.lbKho2.Name = "lbKho2";
            this.lbKho2.Size = new System.Drawing.Size(27, 19);
            this.lbKho2.TabIndex = 12;
            this.lbKho2.Text = "Kho";
            // 
            // txtGHICHUQUAY
            // 
            this.txtGHICHUQUAY.EnterMoveNextControl = true;
            this.txtGHICHUQUAY.Location = new System.Drawing.Point(749, 69);
            this.txtGHICHUQUAY.Name = "txtGHICHUQUAY";
            this.txtGHICHUQUAY.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHUQUAY.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHUQUAY.Size = new System.Drawing.Size(248, 26);
            this.txtGHICHUQUAY.TabIndex = 17;
            // 
            // lbGhiChu2
            // 
            this.lbGhiChu2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGhiChu2.Location = new System.Drawing.Point(332, 72);
            this.lbGhiChu2.Margin = new System.Windows.Forms.Padding(4);
            this.lbGhiChu2.Name = "lbGhiChu2";
            this.lbGhiChu2.Size = new System.Drawing.Size(54, 19);
            this.lbGhiChu2.TabIndex = 9;
            this.lbGhiChu2.Text = "Ghi chú";
            // 
            // txtTENKHO
            // 
            this.txtTENKHO.EnterMoveNextControl = true;
            this.txtTENKHO.Location = new System.Drawing.Point(414, 37);
            this.txtTENKHO.Name = "txtTENKHO";
            this.txtTENKHO.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTENKHO.Properties.Appearance.Options.UseFont = true;
            this.txtTENKHO.Size = new System.Drawing.Size(248, 26);
            this.txtTENKHO.TabIndex = 8;
            // 
            // lbGhiChu3
            // 
            this.lbGhiChu3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGhiChu3.Location = new System.Drawing.Point(668, 72);
            this.lbGhiChu3.Margin = new System.Windows.Forms.Padding(4);
            this.lbGhiChu3.Name = "lbGhiChu3";
            this.lbGhiChu3.Size = new System.Drawing.Size(54, 19);
            this.lbGhiChu3.TabIndex = 16;
            this.lbGhiChu3.Text = "Ghi chú";
            // 
            // txtTENQUAY
            // 
            this.txtTENQUAY.EnterMoveNextControl = true;
            this.txtTENQUAY.Location = new System.Drawing.Point(749, 37);
            this.txtTENQUAY.Name = "txtTENQUAY";
            this.txtTENQUAY.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTENQUAY.Properties.Appearance.Options.UseFont = true;
            this.txtTENQUAY.Size = new System.Drawing.Size(248, 26);
            this.txtTENQUAY.TabIndex = 15;
            // 
            // lbGhiChu
            // 
            this.lbGhiChu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGhiChu.Location = new System.Drawing.Point(5, 72);
            this.lbGhiChu.Margin = new System.Windows.Forms.Padding(4);
            this.lbGhiChu.Name = "lbGhiChu";
            this.lbGhiChu.Size = new System.Drawing.Size(54, 19);
            this.lbGhiChu.TabIndex = 2;
            this.lbGhiChu.Text = "Ghi chú";
            // 
            // txtGHICHUCUAHANG
            // 
            this.txtGHICHUCUAHANG.EnterMoveNextControl = true;
            this.txtGHICHUCUAHANG.Location = new System.Drawing.Point(77, 69);
            this.txtGHICHUCUAHANG.Name = "txtGHICHUCUAHANG";
            this.txtGHICHUCUAHANG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHUCUAHANG.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHUCUAHANG.Size = new System.Drawing.Size(248, 26);
            this.txtGHICHUCUAHANG.TabIndex = 3;
            // 
            // lbQuay
            // 
            this.lbQuay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbQuay.Location = new System.Drawing.Point(668, 40);
            this.lbQuay.Margin = new System.Windows.Forms.Padding(4);
            this.lbQuay.Name = "lbQuay";
            this.lbQuay.Size = new System.Drawing.Size(37, 19);
            this.lbQuay.TabIndex = 14;
            this.lbQuay.Text = "Quầy";
            // 
            // lbKho
            // 
            this.lbKho.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbKho.Location = new System.Drawing.Point(332, 40);
            this.lbKho.Margin = new System.Windows.Forms.Padding(4);
            this.lbKho.Name = "lbKho";
            this.lbKho.Size = new System.Drawing.Size(27, 19);
            this.lbKho.TabIndex = 7;
            this.lbKho.Text = "Kho";
            // 
            // chkSUDUNGCUAHANG
            // 
            this.chkSUDUNGCUAHANG.EnterMoveNextControl = true;
            this.chkSUDUNGCUAHANG.Location = new System.Drawing.Point(77, 101);
            this.chkSUDUNGCUAHANG.Name = "chkSUDUNGCUAHANG";
            this.chkSUDUNGCUAHANG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNGCUAHANG.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNGCUAHANG.Properties.Caption = "Sử dụng";
            this.chkSUDUNGCUAHANG.Size = new System.Drawing.Size(92, 24);
            this.chkSUDUNGCUAHANG.TabIndex = 4;
            // 
            // lblTenCH
            // 
            this.lblTenCH.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenCH.Location = new System.Drawing.Point(332, 8);
            this.lblTenCH.Margin = new System.Windows.Forms.Padding(4);
            this.lblTenCH.Name = "lblTenCH";
            this.lblTenCH.Size = new System.Drawing.Size(68, 19);
            this.lblTenCH.TabIndex = 5;
            this.lblTenCH.Text = "Cửa hàng";
            // 
            // txtGHICHUKHO
            // 
            this.txtGHICHUKHO.EnterMoveNextControl = true;
            this.txtGHICHUKHO.Location = new System.Drawing.Point(414, 69);
            this.txtGHICHUKHO.Name = "txtGHICHUKHO";
            this.txtGHICHUKHO.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHUKHO.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHUKHO.Size = new System.Drawing.Size(248, 26);
            this.txtGHICHUKHO.TabIndex = 10;
            // 
            // lblTEN
            // 
            this.lblTEN.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTEN.Location = new System.Drawing.Point(5, 40);
            this.lblTEN.Margin = new System.Windows.Forms.Padding(4);
            this.lblTEN.Name = "lblTEN";
            this.lblTEN.Size = new System.Drawing.Size(68, 19);
            this.lblTEN.TabIndex = 0;
            this.lblTEN.Text = "Cửa hàng";
            // 
            // txtTENCUAHANG
            // 
            this.txtTENCUAHANG.EnterMoveNextControl = true;
            this.txtTENCUAHANG.Location = new System.Drawing.Point(77, 37);
            this.txtTENCUAHANG.Name = "txtTENCUAHANG";
            this.txtTENCUAHANG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTENCUAHANG.Properties.Appearance.Options.UseFont = true;
            this.txtTENCUAHANG.Size = new System.Drawing.Size(248, 26);
            this.txtTENCUAHANG.TabIndex = 1;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.btnXuongit);
            this.panelControl2.Controls.Add(this.btnXuongnhieu);
            this.panelControl2.Controls.Add(this.btnLenit);
            this.panelControl2.Controls.Add(this.btnLennhieu);
            this.panelControl2.Controls.Add(this.btnBanphim);
            this.panelControl2.Controls.Add(this.btnCapnhat);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(981, 174);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 537);
            this.panelControl2.TabIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 242);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(35, 53);
            this.panelControl3.TabIndex = 20;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(2, 295);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 18;
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(2, 375);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 16;
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(2, 162);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 19;
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(2, 82);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 17;
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(2, 455);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(4);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(35, 80);
            this.btnBanphim.TabIndex = 9;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhat.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(2, 2);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(4);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhat.TabIndex = 0;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.panelControl7);
            this.panelControl5.Controls.Add(this.panelControl6);
            this.panelControl5.Controls.Add(this.panelControl4);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(0, 174);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(981, 537);
            this.panelControl5.TabIndex = 4;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.pnlQuay);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl7.Location = new System.Drawing.Point(2, 288);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(977, 247);
            this.panelControl7.TabIndex = 0;
            // 
            // pnlQuay
            // 
            this.pnlQuay.AutoScroll = true;
            this.pnlQuay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlQuay.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlQuay.Location = new System.Drawing.Point(2, 2);
            this.pnlQuay.Name = "pnlQuay";
            this.pnlQuay.Padding = new System.Windows.Forms.Padding(336, 0, 0, 0);
            this.pnlQuay.Size = new System.Drawing.Size(973, 243);
            this.pnlQuay.TabIndex = 2;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.pnlKho);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl6.Location = new System.Drawing.Point(2, 145);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(977, 143);
            this.panelControl6.TabIndex = 0;
            // 
            // pnlKho
            // 
            this.pnlKho.AutoScroll = true;
            this.pnlKho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlKho.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlKho.Location = new System.Drawing.Point(2, 2);
            this.pnlKho.Name = "pnlKho";
            this.pnlKho.Padding = new System.Windows.Forms.Padding(168, 0, 0, 0);
            this.pnlKho.Size = new System.Drawing.Size(973, 139);
            this.pnlKho.TabIndex = 1;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.pnlCuahang);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(2, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(977, 143);
            this.panelControl4.TabIndex = 0;
            // 
            // pnlCuahang
            // 
            this.pnlCuahang.AutoScroll = true;
            this.pnlCuahang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCuahang.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlCuahang.Location = new System.Drawing.Point(2, 2);
            this.pnlCuahang.Name = "pnlCuahang";
            this.pnlCuahang.Size = new System.Drawing.Size(973, 139);
            this.pnlCuahang.TabIndex = 0;
            // 
            // Frm_CHKQ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 711);
            this.Controls.Add(this.panelControl5);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_CHKQ";
            this.Text = "Cửa hàng - Kho - Quầy";
            this.Load += new System.EventHandler(this.Frm_CHKQ_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_CHKQ_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboCUAHANG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboKHO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGQUAY.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGKHO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUQUAY.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENKHO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENQUAY.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUCUAHANG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNGCUAHANG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUKHO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENCUAHANG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNGCUAHANG;
        private DevExpress.XtraEditors.LabelControl lblTenCH;
        private DevExpress.XtraEditors.TextEdit txtGHICHUKHO;
        private DevExpress.XtraEditors.LabelControl lblTEN;
        private DevExpress.XtraEditors.TextEdit txtTENCUAHANG;
        private DevExpress.XtraEditors.LabelControl lbKho2;
        private DevExpress.XtraEditors.TextEdit txtGHICHUQUAY;
        private DevExpress.XtraEditors.LabelControl lbGhiChu2;
        private DevExpress.XtraEditors.TextEdit txtTENKHO;
        private DevExpress.XtraEditors.LabelControl lbGhiChu3;
        private DevExpress.XtraEditors.TextEdit txtTENQUAY;
        private DevExpress.XtraEditors.LabelControl lbGhiChu;
        private DevExpress.XtraEditors.TextEdit txtGHICHUCUAHANG;
        private DevExpress.XtraEditors.LabelControl lbQuay;
        private DevExpress.XtraEditors.LabelControl lbKho;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNGQUAY;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNGKHO;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.LookUpEdit cboCUAHANG;
        private DevExpress.XtraEditors.LookUpEdit cboKHO;
        private System.Windows.Forms.FlowLayoutPanel pnlQuay;
        private System.Windows.Forms.FlowLayoutPanel pnlKho;
        private System.Windows.Forms.FlowLayoutPanel pnlCuahang;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnTieude;
        private DevExpress.XtraEditors.SimpleButton btnTieudeVe;




    }
}