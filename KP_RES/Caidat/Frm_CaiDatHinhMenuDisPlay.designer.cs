﻿namespace  KP_RES 
{
    partial class Frm_CaiDatHinhMenuDisPlay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_CaiDatHinhMenuDisPlay));
            this.pnTren = new DevExpress.XtraEditors.PanelControl();
            this.bnt_tranglen = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_refresh = new DevExpress.XtraEditors.SimpleButton();
            this.pnNgoai = new DevExpress.XtraEditors.PanelControl();
            this.pnGiua = new DevExpress.XtraEditors.PanelControl();
            this.pnHanghoa = new DevExpress.XtraEditors.PanelControl();
            this.pnDuoi = new DevExpress.XtraEditors.PanelControl();
            this.bnt_trangxuong = new DevExpress.XtraEditors.SimpleButton();
            this.lbl_sotrangduoi = new DevExpress.XtraEditors.SimpleButton();
            this.pnPhai = new DevExpress.XtraEditors.PanelControl();
            this.flpn_nhom = new System.Windows.Forms.FlowLayoutPanel();
            this.btnLenNhomhang = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongNhomhang = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pnTren)).BeginInit();
            this.pnTren.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnNgoai)).BeginInit();
            this.pnNgoai.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnGiua)).BeginInit();
            this.pnGiua.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnHanghoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnDuoi)).BeginInit();
            this.pnDuoi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnPhai)).BeginInit();
            this.pnPhai.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnTren
            // 
            this.pnTren.Controls.Add(this.bnt_tranglen);
            this.pnTren.Controls.Add(this.bnt_refresh);
            this.pnTren.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTren.Location = new System.Drawing.Point(2, 2);
            this.pnTren.Name = "pnTren";
            this.pnTren.Size = new System.Drawing.Size(905, 35);
            this.pnTren.TabIndex = 0;
            // 
            // bnt_tranglen
            // 
            this.bnt_tranglen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_tranglen.Appearance.Options.UseFont = true;
            this.bnt_tranglen.Appearance.Options.UseTextOptions = true;
            this.bnt_tranglen.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bnt_tranglen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bnt_tranglen.Image = global::KP_RES.Properties.Resources.up22_26;
            this.bnt_tranglen.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_tranglen.Location = new System.Drawing.Point(142, 2);
            this.bnt_tranglen.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_tranglen.Name = "bnt_tranglen";
            this.bnt_tranglen.Size = new System.Drawing.Size(761, 31);
            this.bnt_tranglen.TabIndex = 52;
            this.bnt_tranglen.Text = "Khu vực";
            this.bnt_tranglen.Click += new System.EventHandler(this.bnt_tranglen_Click);
            // 
            // bnt_refresh
            // 
            this.bnt_refresh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_refresh.Appearance.Options.UseFont = true;
            this.bnt_refresh.Dock = System.Windows.Forms.DockStyle.Left;
            this.bnt_refresh.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.bnt_refresh.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.bnt_refresh.Location = new System.Drawing.Point(2, 2);
            this.bnt_refresh.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_refresh.Name = "bnt_refresh";
            this.bnt_refresh.Size = new System.Drawing.Size(140, 31);
            this.bnt_refresh.TabIndex = 53;
            this.bnt_refresh.Text = "Cập nhật";
            this.bnt_refresh.Click += new System.EventHandler(this.bnt_refresh_Click);
            // 
            // pnNgoai
            // 
            this.pnNgoai.Controls.Add(this.pnGiua);
            this.pnNgoai.Controls.Add(this.pnDuoi);
            this.pnNgoai.Controls.Add(this.pnTren);
            this.pnNgoai.Controls.Add(this.pnPhai);
            this.pnNgoai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnNgoai.Location = new System.Drawing.Point(0, 0);
            this.pnNgoai.Name = "pnNgoai";
            this.pnNgoai.Size = new System.Drawing.Size(1049, 612);
            this.pnNgoai.TabIndex = 1;
            // 
            // pnGiua
            // 
            this.pnGiua.Controls.Add(this.pnHanghoa);
            this.pnGiua.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnGiua.Location = new System.Drawing.Point(2, 37);
            this.pnGiua.Name = "pnGiua";
            this.pnGiua.Size = new System.Drawing.Size(905, 538);
            this.pnGiua.TabIndex = 1;
            // 
            // pnHanghoa
            // 
            this.pnHanghoa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnHanghoa.Location = new System.Drawing.Point(2, 2);
            this.pnHanghoa.Name = "pnHanghoa";
            this.pnHanghoa.Size = new System.Drawing.Size(901, 534);
            this.pnHanghoa.TabIndex = 1;
            // 
            // pnDuoi
            // 
            this.pnDuoi.Controls.Add(this.bnt_trangxuong);
            this.pnDuoi.Controls.Add(this.lbl_sotrangduoi);
            this.pnDuoi.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnDuoi.Location = new System.Drawing.Point(2, 575);
            this.pnDuoi.Name = "pnDuoi";
            this.pnDuoi.Size = new System.Drawing.Size(905, 35);
            this.pnDuoi.TabIndex = 2;
            // 
            // bnt_trangxuong
            // 
            this.bnt_trangxuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_trangxuong.Appearance.Options.UseFont = true;
            this.bnt_trangxuong.Appearance.Options.UseTextOptions = true;
            this.bnt_trangxuong.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bnt_trangxuong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bnt_trangxuong.Image = global::KP_RES.Properties.Resources.up11_26;
            this.bnt_trangxuong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_trangxuong.Location = new System.Drawing.Point(142, 2);
            this.bnt_trangxuong.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_trangxuong.Name = "bnt_trangxuong";
            this.bnt_trangxuong.Size = new System.Drawing.Size(761, 31);
            this.bnt_trangxuong.TabIndex = 52;
            this.bnt_trangxuong.Text = "Khu vực";
            this.bnt_trangxuong.Click += new System.EventHandler(this.bnt_trangxuong_Click);
            // 
            // lbl_sotrangduoi
            // 
            this.lbl_sotrangduoi.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_sotrangduoi.Appearance.Options.UseFont = true;
            this.lbl_sotrangduoi.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbl_sotrangduoi.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.lbl_sotrangduoi.Location = new System.Drawing.Point(2, 2);
            this.lbl_sotrangduoi.Margin = new System.Windows.Forms.Padding(4);
            this.lbl_sotrangduoi.Name = "lbl_sotrangduoi";
            this.lbl_sotrangduoi.Size = new System.Drawing.Size(140, 31);
            this.lbl_sotrangduoi.TabIndex = 51;
            this.lbl_sotrangduoi.Text = "1/5";
            // 
            // pnPhai
            // 
            this.pnPhai.Controls.Add(this.flpn_nhom);
            this.pnPhai.Controls.Add(this.btnLenNhomhang);
            this.pnPhai.Controls.Add(this.btnXuongNhomhang);
            this.pnPhai.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnPhai.Location = new System.Drawing.Point(907, 2);
            this.pnPhai.Name = "pnPhai";
            this.pnPhai.Size = new System.Drawing.Size(140, 608);
            this.pnPhai.TabIndex = 0;
            // 
            // flpn_nhom
            // 
            this.flpn_nhom.AutoScroll = true;
            this.flpn_nhom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpn_nhom.Location = new System.Drawing.Point(2, 37);
            this.flpn_nhom.Name = "flpn_nhom";
            this.flpn_nhom.Size = new System.Drawing.Size(136, 538);
            this.flpn_nhom.TabIndex = 19;
            // 
            // btnLenNhomhang
            // 
            this.btnLenNhomhang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenNhomhang.Appearance.Options.UseFont = true;
            this.btnLenNhomhang.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenNhomhang.Image = ((System.Drawing.Image)(resources.GetObject("btnLenNhomhang.Image")));
            this.btnLenNhomhang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenNhomhang.Location = new System.Drawing.Point(2, 2);
            this.btnLenNhomhang.Margin = new System.Windows.Forms.Padding(4);
            this.btnLenNhomhang.Name = "btnLenNhomhang";
            this.btnLenNhomhang.Size = new System.Drawing.Size(136, 35);
            this.btnLenNhomhang.TabIndex = 18;
            this.btnLenNhomhang.Click += new System.EventHandler(this.btnLenNhomhang_Click);
            // 
            // btnXuongNhomhang
            // 
            this.btnXuongNhomhang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongNhomhang.Appearance.Options.UseFont = true;
            this.btnXuongNhomhang.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongNhomhang.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongNhomhang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongNhomhang.Location = new System.Drawing.Point(2, 575);
            this.btnXuongNhomhang.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongNhomhang.Name = "btnXuongNhomhang";
            this.btnXuongNhomhang.Size = new System.Drawing.Size(136, 31);
            this.btnXuongNhomhang.TabIndex = 17;
            this.btnXuongNhomhang.Click += new System.EventHandler(this.btnXuongNhomhang_Click);
            // 
            // Frm_CaiDatHinhMenuDisPlay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1049, 612);
            this.Controls.Add(this.pnNgoai);
            this.Name = "Frm_CaiDatHinhMenuDisPlay";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cài đặt hình cho Menu Display";
            this.Load += new System.EventHandler(this.Frm_CaiDatHinhMenuDisPlay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pnTren)).EndInit();
            this.pnTren.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnNgoai)).EndInit();
            this.pnNgoai.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnGiua)).EndInit();
            this.pnGiua.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnHanghoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnDuoi)).EndInit();
            this.pnDuoi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnPhai)).EndInit();
            this.pnPhai.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnTren;
        private DevExpress.XtraEditors.PanelControl pnNgoai;
        private DevExpress.XtraEditors.PanelControl pnDuoi;
        private DevExpress.XtraEditors.PanelControl pnGiua;
        private DevExpress.XtraEditors.SimpleButton bnt_tranglen;
        private DevExpress.XtraEditors.SimpleButton bnt_trangxuong;
        private DevExpress.XtraEditors.SimpleButton lbl_sotrangduoi;
        private DevExpress.XtraEditors.SimpleButton bnt_refresh;
        private DevExpress.XtraEditors.PanelControl pnHanghoa;
        private DevExpress.XtraEditors.PanelControl pnPhai;
        private DevExpress.XtraEditors.SimpleButton btnXuongNhomhang;
        private DevExpress.XtraEditors.SimpleButton btnLenNhomhang;
        private System.Windows.Forms.FlowLayoutPanel flpn_nhom;



    }
}
