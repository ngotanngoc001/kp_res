﻿namespace KP_RES 
{
    partial class Frm_SetCombo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_SetCombo));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.palComboIcon = new DevExpress.XtraEditors.PanelControl();
            this.fpnlCombo = new System.Windows.Forms.FlowLayoutPanel();
            this.btnXuongCombo = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenCombo = new DevExpress.XtraEditors.SimpleButton();
            this.palLIETKE = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GIABAN1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_NHOMHANG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_NHOMHANG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA_HANGHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SUDUNG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FILL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HINHANH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl13 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl14 = new DevExpress.XtraEditors.PanelControl();
            this.btnXuongit = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuongnhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenit = new DevExpress.XtraEditors.SimpleButton();
            this.btnLennhieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhatGV = new DevExpress.XtraEditors.SimpleButton();
            this.palHANGHOA = new DevExpress.XtraEditors.PanelControl();
            this.fpnlHanghoa = new System.Windows.Forms.FlowLayoutPanel();
            this.btnXuongHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenHangHoa = new DevExpress.XtraEditors.SimpleButton();
            this.palNHOMHANG = new DevExpress.XtraEditors.PanelControl();
            this.fpnlNhomhang = new System.Windows.Forms.FlowLayoutPanel();
            this.btnXuongNhomhang = new DevExpress.XtraEditors.SimpleButton();
            this.btnLenNhomhang = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.palSetCombo = new DevExpress.XtraEditors.PanelControl();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.btnThemChitietCombo = new DevExpress.XtraEditors.SimpleButton();
            this.btnHuyChitietCombo = new DevExpress.XtraEditors.SimpleButton();
            this.btnHuy = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuuChitietCombo = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoadong = new DevExpress.XtraEditors.SimpleButton();
            this.btnSuaChitietCombo = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.btnHoanTat = new DevExpress.XtraEditors.SimpleButton();
            this.btnXemDS = new DevExpress.XtraEditors.SimpleButton();
            this.btnDoimathang = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STTCombo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cboHANGHOA = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.SOLUONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtSOLUONGHANG = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.PRICE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtGIA = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.btnComboGrid = new DevExpress.XtraEditors.SimpleButton();
            this.btnComboChitiet = new DevExpress.XtraEditors.SimpleButton();
            this.btnComboIcon = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.txtGIABAN2 = new DevExpress.XtraEditors.TextEdit();
            this.lbGiaBan2 = new DevExpress.XtraEditors.LabelControl();
            this.txtGIABAN4 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtGIABAN3 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.btnAutoMaVach = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.txtMAVACH = new DevExpress.XtraEditors.TextEdit();
            this.lbMaVach = new DevExpress.XtraEditors.LabelControl();
            this.chkMayinbep = new DevExpress.XtraEditors.CheckEdit();
            this.cboBep = new DevExpress.XtraEditors.LookUpEdit();
            this.chkSUDUNG = new DevExpress.XtraEditors.CheckEdit();
            this.btnAUTO = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuuCombo = new DevExpress.XtraEditors.SimpleButton();
            this.lbMa = new DevExpress.XtraEditors.LabelControl();
            this.btnXoaCombo = new DevExpress.XtraEditors.SimpleButton();
            this.btnSuaCombo = new DevExpress.XtraEditors.SimpleButton();
            this.txtMAHANGHOA = new DevExpress.XtraEditors.TextEdit();
            this.btnThemCombo = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.txtGIAMUA = new DevExpress.XtraEditors.TextEdit();
            this.lbGiaMua = new DevExpress.XtraEditors.LabelControl();
            this.lbThue = new DevExpress.XtraEditors.LabelControl();
            this.txtGHICHUHANGHOA = new DevExpress.XtraEditors.TextEdit();
            this.lbSTT = new DevExpress.XtraEditors.LabelControl();
            this.txtSTTHANGHOA = new DevExpress.XtraEditors.TextEdit();
            this.lbGhiChu = new DevExpress.XtraEditors.LabelControl();
            this.lblDVT = new DevExpress.XtraEditors.LabelControl();
            this.cboDonvitinh = new DevExpress.XtraEditors.LookUpEdit();
            this.cboThue = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.picHANGHOA = new System.Windows.Forms.PictureBox();
            this.lbNhom = new DevExpress.XtraEditors.LabelControl();
            this.txtGIABAN1 = new DevExpress.XtraEditors.TextEdit();
            this.cboNhomhang = new DevExpress.XtraEditors.LookUpEdit();
            this.txtTENCOMBO = new DevExpress.XtraEditors.TextEdit();
            this.lblTENHANGHOA = new DevExpress.XtraEditors.LabelControl();
            this.lbGiaBan = new DevExpress.XtraEditors.LabelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.btnNangcao = new DevExpress.XtraEditors.SimpleButton();
            this.btnNghiepvu = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapnhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palComboIcon)).BeginInit();
            this.palComboIcon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palLIETKE)).BeginInit();
            this.palLIETKE.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).BeginInit();
            this.panelControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.palHANGHOA)).BeginInit();
            this.palHANGHOA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palNHOMHANG)).BeginInit();
            this.palNHOMHANG.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palSetCombo)).BeginInit();
            this.palSetCombo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            this.panelControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboHANGHOA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSOLUONGHANG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMAVACH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMayinbep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboBep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMAHANGHOA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIAMUA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUHANGHOA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTTHANGHOA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDonvitinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboThue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHANGHOA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhomhang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENCOMBO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.palComboIcon);
            this.panelControl1.Controls.Add(this.palLIETKE);
            this.panelControl1.Controls.Add(this.palHANGHOA);
            this.panelControl1.Controls.Add(this.palNHOMHANG);
            this.panelControl1.Controls.Add(this.panelControl4);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(913, 749);
            this.panelControl1.TabIndex = 0;
            // 
            // palComboIcon
            // 
            this.palComboIcon.Controls.Add(this.fpnlCombo);
            this.palComboIcon.Controls.Add(this.btnXuongCombo);
            this.palComboIcon.Controls.Add(this.btnLenCombo);
            this.palComboIcon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palComboIcon.Location = new System.Drawing.Point(2, 2);
            this.palComboIcon.Name = "palComboIcon";
            this.palComboIcon.Size = new System.Drawing.Size(424, 745);
            this.palComboIcon.TabIndex = 1;
            this.palComboIcon.Visible = false;
            // 
            // fpnlCombo
            // 
            this.fpnlCombo.AutoScroll = true;
            this.fpnlCombo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpnlCombo.Font = new System.Drawing.Font("Tahoma", 12F);
            this.fpnlCombo.Location = new System.Drawing.Point(2, 37);
            this.fpnlCombo.Name = "fpnlCombo";
            this.fpnlCombo.Size = new System.Drawing.Size(420, 671);
            this.fpnlCombo.TabIndex = 18;
            // 
            // btnXuongCombo
            // 
            this.btnXuongCombo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongCombo.Appearance.Options.UseFont = true;
            this.btnXuongCombo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongCombo.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongCombo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongCombo.Location = new System.Drawing.Point(2, 708);
            this.btnXuongCombo.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongCombo.Name = "btnXuongCombo";
            this.btnXuongCombo.Size = new System.Drawing.Size(420, 35);
            this.btnXuongCombo.TabIndex = 17;
            // 
            // btnLenCombo
            // 
            this.btnLenCombo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenCombo.Appearance.Options.UseFont = true;
            this.btnLenCombo.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenCombo.Image = ((System.Drawing.Image)(resources.GetObject("btnLenCombo.Image")));
            this.btnLenCombo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenCombo.Location = new System.Drawing.Point(2, 2);
            this.btnLenCombo.Margin = new System.Windows.Forms.Padding(4);
            this.btnLenCombo.Name = "btnLenCombo";
            this.btnLenCombo.Size = new System.Drawing.Size(420, 35);
            this.btnLenCombo.TabIndex = 16;
            // 
            // palLIETKE
            // 
            this.palLIETKE.Controls.Add(this.panelControl2);
            this.palLIETKE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palLIETKE.Location = new System.Drawing.Point(2, 2);
            this.palLIETKE.Name = "palLIETKE";
            this.palLIETKE.Size = new System.Drawing.Size(424, 745);
            this.palLIETKE.TabIndex = 19;
            this.palLIETKE.Visible = false;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridControl1);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(420, 741);
            this.panelControl2.TabIndex = 6;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(377, 737);
            this.gridControl1.TabIndex = 3;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT1,
            this.TEN_HANGHOA,
            this.GIABAN1,
            this.TEN_NHOMHANG,
            this.MA_NHOMHANG,
            this.MA_HANGHOA,
            this.STT,
            this.SUDUNG,
            this.FILL,
            this.HINHANH});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = "Kéo thả cột vào đây để nhóm";
            this.gridView1.GroupRowHeight = 30;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsPrint.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.TEN_HANGHOA, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // STT1
            // 
            this.STT1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.STT1.AppearanceCell.Options.UseFont = true;
            this.STT1.AppearanceCell.Options.UseTextOptions = true;
            this.STT1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STT1.AppearanceHeader.Options.UseFont = true;
            this.STT1.AppearanceHeader.Options.UseTextOptions = true;
            this.STT1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT1.Caption = "STT";
            this.STT1.FieldName = "STT1";
            this.STT1.Name = "STT1";
            this.STT1.OptionsColumn.AllowEdit = false;
            this.STT1.OptionsColumn.AllowFocus = false;
            this.STT1.OptionsColumn.FixedWidth = true;
            this.STT1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.STT1.Visible = true;
            this.STT1.VisibleIndex = 0;
            this.STT1.Width = 54;
            // 
            // TEN_HANGHOA
            // 
            this.TEN_HANGHOA.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TEN_HANGHOA.AppearanceCell.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_HANGHOA.AppearanceHeader.Options.UseFont = true;
            this.TEN_HANGHOA.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_HANGHOA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_HANGHOA.Caption = "Tên Combo";
            this.TEN_HANGHOA.FieldName = "TEN";
            this.TEN_HANGHOA.Name = "TEN_HANGHOA";
            this.TEN_HANGHOA.OptionsColumn.AllowEdit = false;
            this.TEN_HANGHOA.OptionsColumn.AllowFocus = false;
            this.TEN_HANGHOA.OptionsColumn.FixedWidth = true;
            this.TEN_HANGHOA.Visible = true;
            this.TEN_HANGHOA.VisibleIndex = 1;
            this.TEN_HANGHOA.Width = 240;
            // 
            // GIABAN1
            // 
            this.GIABAN1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.GIABAN1.AppearanceCell.Options.UseFont = true;
            this.GIABAN1.AppearanceCell.Options.UseTextOptions = true;
            this.GIABAN1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GIABAN1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.GIABAN1.AppearanceHeader.Options.UseFont = true;
            this.GIABAN1.AppearanceHeader.Options.UseTextOptions = true;
            this.GIABAN1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GIABAN1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GIABAN1.Caption = "Giá Combo";
            this.GIABAN1.DisplayFormat.FormatString = "{0:#,###0}";
            this.GIABAN1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GIABAN1.FieldName = "GIABAN1";
            this.GIABAN1.Name = "GIABAN1";
            this.GIABAN1.OptionsColumn.AllowEdit = false;
            this.GIABAN1.OptionsColumn.AllowFocus = false;
            this.GIABAN1.OptionsColumn.FixedWidth = true;
            this.GIABAN1.Visible = true;
            this.GIABAN1.VisibleIndex = 3;
            this.GIABAN1.Width = 110;
            // 
            // TEN_NHOMHANG
            // 
            this.TEN_NHOMHANG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TEN_NHOMHANG.AppearanceCell.Options.UseFont = true;
            this.TEN_NHOMHANG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN_NHOMHANG.AppearanceHeader.Options.UseFont = true;
            this.TEN_NHOMHANG.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN_NHOMHANG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN_NHOMHANG.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN_NHOMHANG.Caption = "Nhóm";
            this.TEN_NHOMHANG.FieldName = "TEN_NHOMHANG";
            this.TEN_NHOMHANG.Name = "TEN_NHOMHANG";
            this.TEN_NHOMHANG.OptionsColumn.AllowEdit = false;
            this.TEN_NHOMHANG.OptionsColumn.AllowFocus = false;
            this.TEN_NHOMHANG.OptionsColumn.FixedWidth = true;
            this.TEN_NHOMHANG.Visible = true;
            this.TEN_NHOMHANG.VisibleIndex = 2;
            this.TEN_NHOMHANG.Width = 150;
            // 
            // MA_NHOMHANG
            // 
            this.MA_NHOMHANG.Caption = "MA_NHOMHANG";
            this.MA_NHOMHANG.FieldName = "MA_NHOMHANG";
            this.MA_NHOMHANG.Name = "MA_NHOMHANG";
            // 
            // MA_HANGHOA
            // 
            this.MA_HANGHOA.Caption = "MA_HANGHOA";
            this.MA_HANGHOA.FieldName = "MA";
            this.MA_HANGHOA.Name = "MA_HANGHOA";
            // 
            // STT
            // 
            this.STT.Caption = "STT";
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            // 
            // SUDUNG
            // 
            this.SUDUNG.Caption = "SUDUNG";
            this.SUDUNG.FieldName = "SUDUNG";
            this.SUDUNG.Name = "SUDUNG";
            // 
            // FILL
            // 
            this.FILL.Name = "FILL";
            this.FILL.OptionsColumn.AllowEdit = false;
            this.FILL.OptionsColumn.AllowFocus = false;
            this.FILL.Visible = true;
            this.FILL.VisibleIndex = 4;
            this.FILL.Width = 200;
            // 
            // HINHANH
            // 
            this.HINHANH.Caption = "HINHANH";
            this.HINHANH.FieldName = "HINHANH";
            this.HINHANH.Name = "HINHANH";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.panelControl13);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl3.Location = new System.Drawing.Point(379, 2);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(39, 737);
            this.panelControl3.TabIndex = 7;
            // 
            // panelControl13
            // 
            this.panelControl13.AutoSize = true;
            this.panelControl13.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl13.Controls.Add(this.panelControl14);
            this.panelControl13.Controls.Add(this.btnXuongit);
            this.panelControl13.Controls.Add(this.btnXuongnhieu);
            this.panelControl13.Controls.Add(this.btnLenit);
            this.panelControl13.Controls.Add(this.btnLennhieu);
            this.panelControl13.Controls.Add(this.btnCapnhatGV);
            this.panelControl13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl13.Location = new System.Drawing.Point(2, 2);
            this.panelControl13.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl13.Name = "panelControl13";
            this.panelControl13.Size = new System.Drawing.Size(35, 733);
            this.panelControl13.TabIndex = 14;
            // 
            // panelControl14
            // 
            this.panelControl14.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl14.Location = new System.Drawing.Point(0, 240);
            this.panelControl14.Name = "panelControl14";
            this.panelControl14.Size = new System.Drawing.Size(35, 333);
            this.panelControl14.TabIndex = 15;
            // 
            // btnXuongit
            // 
            this.btnXuongit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongit.Appearance.Options.UseFont = true;
            this.btnXuongit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongit.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongit.Image")));
            this.btnXuongit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongit.Location = new System.Drawing.Point(0, 573);
            this.btnXuongit.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongit.Name = "btnXuongit";
            this.btnXuongit.Size = new System.Drawing.Size(35, 80);
            this.btnXuongit.TabIndex = 12;
            this.btnXuongit.Click += new System.EventHandler(this.btnXuongit_Click);
            // 
            // btnXuongnhieu
            // 
            this.btnXuongnhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongnhieu.Appearance.Options.UseFont = true;
            this.btnXuongnhieu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongnhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnXuongnhieu.Image")));
            this.btnXuongnhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongnhieu.Location = new System.Drawing.Point(0, 653);
            this.btnXuongnhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnXuongnhieu.Name = "btnXuongnhieu";
            this.btnXuongnhieu.Size = new System.Drawing.Size(35, 80);
            this.btnXuongnhieu.TabIndex = 1;
            this.btnXuongnhieu.Click += new System.EventHandler(this.btnXuongnhieu_Click);
            // 
            // btnLenit
            // 
            this.btnLenit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenit.Appearance.Options.UseFont = true;
            this.btnLenit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenit.Image = ((System.Drawing.Image)(resources.GetObject("btnLenit.Image")));
            this.btnLenit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenit.Location = new System.Drawing.Point(0, 160);
            this.btnLenit.Margin = new System.Windows.Forms.Padding(0);
            this.btnLenit.Name = "btnLenit";
            this.btnLenit.Size = new System.Drawing.Size(35, 80);
            this.btnLenit.TabIndex = 13;
            this.btnLenit.Click += new System.EventHandler(this.btnLenit_Click);
            // 
            // btnLennhieu
            // 
            this.btnLennhieu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLennhieu.Appearance.Options.UseFont = true;
            this.btnLennhieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLennhieu.Image = ((System.Drawing.Image)(resources.GetObject("btnLennhieu.Image")));
            this.btnLennhieu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLennhieu.Location = new System.Drawing.Point(0, 80);
            this.btnLennhieu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLennhieu.Name = "btnLennhieu";
            this.btnLennhieu.Size = new System.Drawing.Size(35, 80);
            this.btnLennhieu.TabIndex = 11;
            this.btnLennhieu.Click += new System.EventHandler(this.btnLennhieu_Click);
            // 
            // btnCapnhatGV
            // 
            this.btnCapnhatGV.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhatGV.Appearance.Options.UseFont = true;
            this.btnCapnhatGV.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapnhatGV.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhatGV.Location = new System.Drawing.Point(0, 0);
            this.btnCapnhatGV.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapnhatGV.Name = "btnCapnhatGV";
            this.btnCapnhatGV.Size = new System.Drawing.Size(35, 80);
            this.btnCapnhatGV.TabIndex = 16;
            this.btnCapnhatGV.Click += new System.EventHandler(this.btnCapnhatGV_Click);
            // 
            // palHANGHOA
            // 
            this.palHANGHOA.Controls.Add(this.fpnlHanghoa);
            this.palHANGHOA.Controls.Add(this.btnXuongHangHoa);
            this.palHANGHOA.Controls.Add(this.btnLenHangHoa);
            this.palHANGHOA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palHANGHOA.Location = new System.Drawing.Point(2, 2);
            this.palHANGHOA.Name = "palHANGHOA";
            this.palHANGHOA.Size = new System.Drawing.Size(424, 745);
            this.palHANGHOA.TabIndex = 0;
            // 
            // fpnlHanghoa
            // 
            this.fpnlHanghoa.AutoScroll = true;
            this.fpnlHanghoa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpnlHanghoa.Font = new System.Drawing.Font("Tahoma", 12F);
            this.fpnlHanghoa.Location = new System.Drawing.Point(2, 37);
            this.fpnlHanghoa.Name = "fpnlHanghoa";
            this.fpnlHanghoa.Size = new System.Drawing.Size(420, 671);
            this.fpnlHanghoa.TabIndex = 18;
            // 
            // btnXuongHangHoa
            // 
            this.btnXuongHangHoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongHangHoa.Appearance.Options.UseFont = true;
            this.btnXuongHangHoa.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongHangHoa.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongHangHoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongHangHoa.Location = new System.Drawing.Point(2, 708);
            this.btnXuongHangHoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongHangHoa.Name = "btnXuongHangHoa";
            this.btnXuongHangHoa.Size = new System.Drawing.Size(420, 35);
            this.btnXuongHangHoa.TabIndex = 17;
            this.btnXuongHangHoa.Click += new System.EventHandler(this.btnXuongHangHoa_Click);
            // 
            // btnLenHangHoa
            // 
            this.btnLenHangHoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenHangHoa.Appearance.Options.UseFont = true;
            this.btnLenHangHoa.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenHangHoa.Image = ((System.Drawing.Image)(resources.GetObject("btnLenHangHoa.Image")));
            this.btnLenHangHoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenHangHoa.Location = new System.Drawing.Point(2, 2);
            this.btnLenHangHoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnLenHangHoa.Name = "btnLenHangHoa";
            this.btnLenHangHoa.Size = new System.Drawing.Size(420, 35);
            this.btnLenHangHoa.TabIndex = 16;
            this.btnLenHangHoa.Click += new System.EventHandler(this.btnLenHangHoa_Click);
            // 
            // palNHOMHANG
            // 
            this.palNHOMHANG.Controls.Add(this.fpnlNhomhang);
            this.palNHOMHANG.Controls.Add(this.btnXuongNhomhang);
            this.palNHOMHANG.Controls.Add(this.btnLenNhomhang);
            this.palNHOMHANG.Dock = System.Windows.Forms.DockStyle.Right;
            this.palNHOMHANG.Location = new System.Drawing.Point(426, 2);
            this.palNHOMHANG.Margin = new System.Windows.Forms.Padding(0);
            this.palNHOMHANG.Name = "palNHOMHANG";
            this.palNHOMHANG.Size = new System.Drawing.Size(135, 745);
            this.palNHOMHANG.TabIndex = 1;
            // 
            // fpnlNhomhang
            // 
            this.fpnlNhomhang.AutoScroll = true;
            this.fpnlNhomhang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpnlNhomhang.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fpnlNhomhang.Location = new System.Drawing.Point(2, 37);
            this.fpnlNhomhang.Margin = new System.Windows.Forms.Padding(0);
            this.fpnlNhomhang.Name = "fpnlNhomhang";
            this.fpnlNhomhang.Size = new System.Drawing.Size(131, 671);
            this.fpnlNhomhang.TabIndex = 17;
            // 
            // btnXuongNhomhang
            // 
            this.btnXuongNhomhang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuongNhomhang.Appearance.Options.UseFont = true;
            this.btnXuongNhomhang.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXuongNhomhang.Image = global::KP_RES.Properties.Resources.up11_26;
            this.btnXuongNhomhang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnXuongNhomhang.Location = new System.Drawing.Point(2, 708);
            this.btnXuongNhomhang.Margin = new System.Windows.Forms.Padding(4);
            this.btnXuongNhomhang.Name = "btnXuongNhomhang";
            this.btnXuongNhomhang.Size = new System.Drawing.Size(131, 35);
            this.btnXuongNhomhang.TabIndex = 16;
            this.btnXuongNhomhang.Click += new System.EventHandler(this.btnXuongNhomhang_Click);
            // 
            // btnLenNhomhang
            // 
            this.btnLenNhomhang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLenNhomhang.Appearance.Options.UseFont = true;
            this.btnLenNhomhang.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLenNhomhang.Image = ((System.Drawing.Image)(resources.GetObject("btnLenNhomhang.Image")));
            this.btnLenNhomhang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnLenNhomhang.Location = new System.Drawing.Point(2, 2);
            this.btnLenNhomhang.Margin = new System.Windows.Forms.Padding(4);
            this.btnLenNhomhang.Name = "btnLenNhomhang";
            this.btnLenNhomhang.Size = new System.Drawing.Size(131, 35);
            this.btnLenNhomhang.TabIndex = 15;
            this.btnLenNhomhang.Click += new System.EventHandler(this.btnLenNhomhang_Click);
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.palSetCombo);
            this.panelControl4.Controls.Add(this.panelControl7);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl4.Location = new System.Drawing.Point(561, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(350, 745);
            this.panelControl4.TabIndex = 0;
            // 
            // palSetCombo
            // 
            this.palSetCombo.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.palSetCombo.Controls.Add(this.panelControl10);
            this.palSetCombo.Controls.Add(this.panelControl11);
            this.palSetCombo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palSetCombo.Location = new System.Drawing.Point(2, 2);
            this.palSetCombo.Name = "palSetCombo";
            this.palSetCombo.Size = new System.Drawing.Size(346, 706);
            this.palSetCombo.TabIndex = 74;
            // 
            // panelControl10
            // 
            this.panelControl10.Controls.Add(this.panelControl12);
            this.panelControl10.Controls.Add(this.panelControl15);
            this.panelControl10.Controls.Add(this.gridControl2);
            this.panelControl10.Controls.Add(this.panelControl5);
            this.panelControl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl10.Location = new System.Drawing.Point(0, 394);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(346, 312);
            this.panelControl10.TabIndex = 66;
            // 
            // panelControl12
            // 
            this.panelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl12.Controls.Add(this.btnThemChitietCombo);
            this.panelControl12.Controls.Add(this.btnHuyChitietCombo);
            this.panelControl12.Controls.Add(this.btnHuy);
            this.panelControl12.Controls.Add(this.btnLuuChitietCombo);
            this.panelControl12.Controls.Add(this.btnXoadong);
            this.panelControl12.Controls.Add(this.btnSuaChitietCombo);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl12.Location = new System.Drawing.Point(2, 272);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(342, 91);
            this.panelControl12.TabIndex = 59;
            // 
            // btnThemChitietCombo
            // 
            this.btnThemChitietCombo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemChitietCombo.Appearance.Options.UseFont = true;
            this.btnThemChitietCombo.Appearance.Options.UseTextOptions = true;
            this.btnThemChitietCombo.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnThemChitietCombo.Image = ((System.Drawing.Image)(resources.GetObject("btnThemChitietCombo.Image")));
            this.btnThemChitietCombo.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnThemChitietCombo.Location = new System.Drawing.Point(6, 7);
            this.btnThemChitietCombo.Margin = new System.Windows.Forms.Padding(4);
            this.btnThemChitietCombo.Name = "btnThemChitietCombo";
            this.btnThemChitietCombo.Size = new System.Drawing.Size(80, 76);
            this.btnThemChitietCombo.TabIndex = 13;
            this.btnThemChitietCombo.Text = "&Set Combo";
            this.btnThemChitietCombo.Click += new System.EventHandler(this.btnThemChitietCombo_Click);
            // 
            // btnHuyChitietCombo
            // 
            this.btnHuyChitietCombo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuyChitietCombo.Appearance.Options.UseFont = true;
            this.btnHuyChitietCombo.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnHuyChitietCombo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnHuyChitietCombo.Location = new System.Drawing.Point(255, 48);
            this.btnHuyChitietCombo.Margin = new System.Windows.Forms.Padding(4);
            this.btnHuyChitietCombo.Name = "btnHuyChitietCombo";
            this.btnHuyChitietCombo.Size = new System.Drawing.Size(80, 35);
            this.btnHuyChitietCombo.TabIndex = 12;
            this.btnHuyChitietCombo.Text = "&Xóa";
            this.btnHuyChitietCombo.Click += new System.EventHandler(this.btnHuyChitietCombo_Click);
            // 
            // btnHuy
            // 
            this.btnHuy.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuy.Appearance.Options.UseFont = true;
            this.btnHuy.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnHuy.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnHuy.Location = new System.Drawing.Point(215, 7);
            this.btnHuy.Margin = new System.Windows.Forms.Padding(4);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(120, 35);
            this.btnHuy.TabIndex = 55;
            this.btnHuy.Text = "&Hủy";
            this.btnHuy.Click += new System.EventHandler(this.btnHuy_Click);
            // 
            // btnLuuChitietCombo
            // 
            this.btnLuuChitietCombo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuChitietCombo.Appearance.Options.UseFont = true;
            this.btnLuuChitietCombo.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuuChitietCombo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuuChitietCombo.Location = new System.Drawing.Point(89, 48);
            this.btnLuuChitietCombo.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuuChitietCombo.Name = "btnLuuChitietCombo";
            this.btnLuuChitietCombo.Size = new System.Drawing.Size(80, 35);
            this.btnLuuChitietCombo.TabIndex = 9;
            this.btnLuuChitietCombo.Text = "&Lưu";
            this.btnLuuChitietCombo.Click += new System.EventHandler(this.btnLuuChitietCombo_Click);
            // 
            // btnXoadong
            // 
            this.btnXoadong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoadong.Appearance.Options.UseFont = true;
            this.btnXoadong.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoadong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoadong.Location = new System.Drawing.Point(89, 7);
            this.btnXoadong.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoadong.Name = "btnXoadong";
            this.btnXoadong.Size = new System.Drawing.Size(120, 35);
            this.btnXoadong.TabIndex = 54;
            this.btnXoadong.Text = "&Xóa dòng";
            this.btnXoadong.Click += new System.EventHandler(this.btnXoadong_Click);
            // 
            // btnSuaChitietCombo
            // 
            this.btnSuaChitietCombo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaChitietCombo.Appearance.Options.UseFont = true;
            this.btnSuaChitietCombo.Image = ((System.Drawing.Image)(resources.GetObject("btnSuaChitietCombo.Image")));
            this.btnSuaChitietCombo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSuaChitietCombo.Location = new System.Drawing.Point(172, 48);
            this.btnSuaChitietCombo.Margin = new System.Windows.Forms.Padding(4);
            this.btnSuaChitietCombo.Name = "btnSuaChitietCombo";
            this.btnSuaChitietCombo.Size = new System.Drawing.Size(80, 35);
            this.btnSuaChitietCombo.TabIndex = 53;
            this.btnSuaChitietCombo.Text = "&Sửa";
            this.btnSuaChitietCombo.Click += new System.EventHandler(this.btnSuaChitietCombo_Click);
            // 
            // panelControl15
            // 
            this.panelControl15.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl15.Controls.Add(this.btnHoanTat);
            this.panelControl15.Controls.Add(this.btnXemDS);
            this.panelControl15.Controls.Add(this.btnDoimathang);
            this.panelControl15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl15.Location = new System.Drawing.Point(2, 181);
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(342, 91);
            this.panelControl15.TabIndex = 60;
            // 
            // btnHoanTat
            // 
            this.btnHoanTat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHoanTat.Appearance.Options.UseFont = true;
            this.btnHoanTat.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnHoanTat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnHoanTat.Location = new System.Drawing.Point(194, 6);
            this.btnHoanTat.Margin = new System.Windows.Forms.Padding(4);
            this.btnHoanTat.Name = "btnHoanTat";
            this.btnHoanTat.Size = new System.Drawing.Size(135, 35);
            this.btnHoanTat.TabIndex = 56;
            this.btnHoanTat.Text = "&Hoàn tất";
            this.btnHoanTat.Click += new System.EventHandler(this.btnHoanTat_Click);
            // 
            // btnXemDS
            // 
            this.btnXemDS.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemDS.Appearance.Options.UseFont = true;
            this.btnXemDS.Image = global::KP_RES.Properties.Resources.search_26;
            this.btnXemDS.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXemDS.Location = new System.Drawing.Point(17, 50);
            this.btnXemDS.Margin = new System.Windows.Forms.Padding(4);
            this.btnXemDS.Name = "btnXemDS";
            this.btnXemDS.Size = new System.Drawing.Size(135, 35);
            this.btnXemDS.TabIndex = 55;
            this.btnXemDS.Text = "&Xem tùy chọn";
            this.btnXemDS.Click += new System.EventHandler(this.btnXemDS_Click);
            // 
            // btnDoimathang
            // 
            this.btnDoimathang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDoimathang.Appearance.Options.UseFont = true;
            this.btnDoimathang.Image = global::KP_RES.Properties.Resources.edit_file_26;
            this.btnDoimathang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnDoimathang.Location = new System.Drawing.Point(17, 6);
            this.btnDoimathang.Margin = new System.Windows.Forms.Padding(4);
            this.btnDoimathang.Name = "btnDoimathang";
            this.btnDoimathang.Size = new System.Drawing.Size(135, 35);
            this.btnDoimathang.TabIndex = 54;
            this.btnDoimathang.Text = "&Đổi mặt hàng";
            this.btnDoimathang.Click += new System.EventHandler(this.btnDoimathang_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Top;
            gridLevelNode1.RelationName = "Level1";
            gridLevelNode2.RelationName = "Level2";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1,
            gridLevelNode2});
            this.gridControl2.Location = new System.Drawing.Point(2, 2);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.cboHANGHOA,
            this.txtSOLUONGHANG,
            this.txtGIA});
            this.gridControl2.Size = new System.Drawing.Size(342, 179);
            this.gridControl2.TabIndex = 8;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STTCombo,
            this.MA,
            this.TEN,
            this.SOLUONG,
            this.PRICE});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            this.gridView2.ShownEditor += new System.EventHandler(this.gridView2_ShownEditor);
            // 
            // STTCombo
            // 
            this.STTCombo.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.STTCombo.AppearanceCell.Options.UseFont = true;
            this.STTCombo.AppearanceCell.Options.UseTextOptions = true;
            this.STTCombo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTCombo.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STTCombo.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.STTCombo.AppearanceHeader.Options.UseFont = true;
            this.STTCombo.AppearanceHeader.Options.UseTextOptions = true;
            this.STTCombo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STTCombo.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STTCombo.Caption = "STT";
            this.STTCombo.FieldName = "STTCombo";
            this.STTCombo.Name = "STTCombo";
            this.STTCombo.OptionsColumn.AllowEdit = false;
            this.STTCombo.OptionsColumn.AllowFocus = false;
            this.STTCombo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.STTCombo.OptionsColumn.AllowMove = false;
            this.STTCombo.OptionsColumn.AllowSize = false;
            this.STTCombo.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.STTCombo.OptionsColumn.FixedWidth = true;
            this.STTCombo.OptionsFilter.AllowAutoFilter = false;
            this.STTCombo.OptionsFilter.AllowFilter = false;
            this.STTCombo.Visible = true;
            this.STTCombo.VisibleIndex = 0;
            this.STTCombo.Width = 45;
            // 
            // MA
            // 
            this.MA.AppearanceCell.Options.UseTextOptions = true;
            this.MA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.MA.AppearanceHeader.Options.UseFont = true;
            this.MA.AppearanceHeader.Options.UseTextOptions = true;
            this.MA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MA.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MA.Caption = "Mã";
            this.MA.FieldName = "MA";
            this.MA.Name = "MA";
            this.MA.OptionsColumn.AllowEdit = false;
            this.MA.OptionsColumn.AllowFocus = false;
            this.MA.OptionsColumn.FixedWidth = true;
            this.MA.Width = 60;
            // 
            // TEN
            // 
            this.TEN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TEN.AppearanceCell.Options.UseFont = true;
            this.TEN.AppearanceCell.Options.UseTextOptions = true;
            this.TEN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.TEN.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TEN.AppearanceHeader.Options.UseFont = true;
            this.TEN.AppearanceHeader.Options.UseTextOptions = true;
            this.TEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TEN.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TEN.Caption = "Hàng hóa";
            this.TEN.ColumnEdit = this.cboHANGHOA;
            this.TEN.FieldName = "TEN";
            this.TEN.Name = "TEN";
            this.TEN.OptionsColumn.AllowMove = false;
            this.TEN.OptionsColumn.AllowSize = false;
            this.TEN.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.TEN.OptionsFilter.AllowAutoFilter = false;
            this.TEN.OptionsFilter.AllowFilter = false;
            this.TEN.Visible = true;
            this.TEN.VisibleIndex = 1;
            this.TEN.Width = 240;
            // 
            // cboHANGHOA
            // 
            this.cboHANGHOA.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboHANGHOA.Appearance.Options.UseFont = true;
            this.cboHANGHOA.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboHANGHOA.AppearanceDropDown.Options.UseFont = true;
            this.cboHANGHOA.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboHANGHOA.AppearanceDropDownHeader.Options.UseFont = true;
            this.cboHANGHOA.AutoHeight = false;
            this.cboHANGHOA.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboHANGHOA.DisplayMember = "TEN_HANGHOA";
            this.cboHANGHOA.MaxLength = 30;
            this.cboHANGHOA.Name = "cboHANGHOA";
            this.cboHANGHOA.NullText = "";
            this.cboHANGHOA.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.cboHANGHOA.ShowHeader = false;
            this.cboHANGHOA.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboHANGHOA.ValueMember = "TEN_HANGHOA";
            this.cboHANGHOA.EditValueChanged += new System.EventHandler(this.cboHANGHOA_EditValueChanged);
            this.cboHANGHOA.Leave += new System.EventHandler(this.cboHANGHOA_Leave);
            // 
            // SOLUONG
            // 
            this.SOLUONG.AppearanceCell.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SOLUONG.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.SOLUONG.AppearanceHeader.Options.UseFont = true;
            this.SOLUONG.AppearanceHeader.Options.UseTextOptions = true;
            this.SOLUONG.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SOLUONG.Caption = "SL";
            this.SOLUONG.ColumnEdit = this.txtSOLUONGHANG;
            this.SOLUONG.DisplayFormat.FormatString = "n";
            this.SOLUONG.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.SOLUONG.FieldName = "SOLUONG";
            this.SOLUONG.Name = "SOLUONG";
            this.SOLUONG.OptionsColumn.AllowMove = false;
            this.SOLUONG.OptionsColumn.AllowSize = false;
            this.SOLUONG.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.SOLUONG.OptionsColumn.FixedWidth = true;
            this.SOLUONG.OptionsFilter.AllowAutoFilter = false;
            this.SOLUONG.OptionsFilter.AllowFilter = false;
            this.SOLUONG.Visible = true;
            this.SOLUONG.VisibleIndex = 2;
            this.SOLUONG.Width = 60;
            // 
            // txtSOLUONGHANG
            // 
            this.txtSOLUONGHANG.AutoHeight = false;
            this.txtSOLUONGHANG.MaxLength = 3;
            this.txtSOLUONGHANG.Name = "txtSOLUONGHANG";
            this.txtSOLUONGHANG.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.txtSOLUONGHANG_EditValueChanging);
            this.txtSOLUONGHANG.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSOLUONGHANG_KeyPress);
            // 
            // PRICE
            // 
            this.PRICE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.PRICE.AppearanceCell.Options.UseFont = true;
            this.PRICE.AppearanceCell.Options.UseTextOptions = true;
            this.PRICE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.PRICE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PRICE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.PRICE.AppearanceHeader.Options.UseFont = true;
            this.PRICE.AppearanceHeader.Options.UseTextOptions = true;
            this.PRICE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PRICE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PRICE.Caption = "Giá";
            this.PRICE.ColumnEdit = this.txtGIA;
            this.PRICE.DisplayFormat.FormatString = "{0:#,###0}";
            this.PRICE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.PRICE.FieldName = "PRICE";
            this.PRICE.Name = "PRICE";
            this.PRICE.OptionsColumn.AllowMove = false;
            this.PRICE.OptionsColumn.AllowSize = false;
            this.PRICE.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.PRICE.OptionsFilter.AllowAutoFilter = false;
            this.PRICE.OptionsFilter.AllowFilter = false;
            this.PRICE.Width = 87;
            // 
            // txtGIA
            // 
            this.txtGIA.AutoHeight = false;
            this.txtGIA.MaxLength = 9;
            this.txtGIA.Name = "txtGIA";
            this.txtGIA.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.txtGIA_EditValueChanging);
            this.txtGIA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGIA_KeyPress);
            // 
            // panelControl5
            // 
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl5.Controls.Add(this.btnComboGrid);
            this.panelControl5.Controls.Add(this.btnComboChitiet);
            this.panelControl5.Controls.Add(this.btnComboIcon);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl5.Location = new System.Drawing.Point(2, 294);
            this.panelControl5.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(342, 16);
            this.panelControl5.TabIndex = 58;
            this.panelControl5.Visible = false;
            // 
            // btnComboGrid
            // 
            this.btnComboGrid.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnComboGrid.Appearance.Options.UseFont = true;
            this.btnComboGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnComboGrid.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnComboGrid.Location = new System.Drawing.Point(140, 2);
            this.btnComboGrid.Margin = new System.Windows.Forms.Padding(4);
            this.btnComboGrid.Name = "btnComboGrid";
            this.btnComboGrid.Size = new System.Drawing.Size(99, 12);
            this.btnComboGrid.TabIndex = 56;
            this.btnComboGrid.Text = "Combo(lưới)";
            this.btnComboGrid.Click += new System.EventHandler(this.btnComboGrid_Click);
            // 
            // btnComboChitiet
            // 
            this.btnComboChitiet.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnComboChitiet.Appearance.Options.UseFont = true;
            this.btnComboChitiet.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnComboChitiet.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnComboChitiet.Location = new System.Drawing.Point(2, 2);
            this.btnComboChitiet.Margin = new System.Windows.Forms.Padding(4);
            this.btnComboChitiet.Name = "btnComboChitiet";
            this.btnComboChitiet.Size = new System.Drawing.Size(138, 12);
            this.btnComboChitiet.TabIndex = 59;
            this.btnComboChitiet.Text = "Danh mục Combo";
            this.btnComboChitiet.Click += new System.EventHandler(this.btnComboChitiet_Click);
            // 
            // btnComboIcon
            // 
            this.btnComboIcon.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnComboIcon.Appearance.Options.UseFont = true;
            this.btnComboIcon.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnComboIcon.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnComboIcon.Location = new System.Drawing.Point(239, 2);
            this.btnComboIcon.Margin = new System.Windows.Forms.Padding(4);
            this.btnComboIcon.Name = "btnComboIcon";
            this.btnComboIcon.Size = new System.Drawing.Size(101, 12);
            this.btnComboIcon.TabIndex = 57;
            this.btnComboIcon.Text = "Combo(Icon)";
            this.btnComboIcon.Click += new System.EventHandler(this.btnComboIcon_Click);
            // 
            // panelControl11
            // 
            this.panelControl11.Controls.Add(this.txtGIABAN2);
            this.panelControl11.Controls.Add(this.lbGiaBan2);
            this.panelControl11.Controls.Add(this.txtGIABAN4);
            this.panelControl11.Controls.Add(this.labelControl3);
            this.panelControl11.Controls.Add(this.txtGIABAN3);
            this.panelControl11.Controls.Add(this.labelControl4);
            this.panelControl11.Controls.Add(this.btnAutoMaVach);
            this.panelControl11.Controls.Add(this.panelControl9);
            this.panelControl11.Controls.Add(this.txtMAVACH);
            this.panelControl11.Controls.Add(this.lbMaVach);
            this.panelControl11.Controls.Add(this.chkMayinbep);
            this.panelControl11.Controls.Add(this.cboBep);
            this.panelControl11.Controls.Add(this.chkSUDUNG);
            this.panelControl11.Controls.Add(this.btnAUTO);
            this.panelControl11.Controls.Add(this.btnLuuCombo);
            this.panelControl11.Controls.Add(this.lbMa);
            this.panelControl11.Controls.Add(this.btnXoaCombo);
            this.panelControl11.Controls.Add(this.btnSuaCombo);
            this.panelControl11.Controls.Add(this.txtMAHANGHOA);
            this.panelControl11.Controls.Add(this.btnThemCombo);
            this.panelControl11.Controls.Add(this.panelControl6);
            this.panelControl11.Controls.Add(this.txtGIAMUA);
            this.panelControl11.Controls.Add(this.lbGiaMua);
            this.panelControl11.Controls.Add(this.lbThue);
            this.panelControl11.Controls.Add(this.txtGHICHUHANGHOA);
            this.panelControl11.Controls.Add(this.lbSTT);
            this.panelControl11.Controls.Add(this.txtSTTHANGHOA);
            this.panelControl11.Controls.Add(this.lbGhiChu);
            this.panelControl11.Controls.Add(this.lblDVT);
            this.panelControl11.Controls.Add(this.cboDonvitinh);
            this.panelControl11.Controls.Add(this.cboThue);
            this.panelControl11.Controls.Add(this.panelControl8);
            this.panelControl11.Controls.Add(this.lbNhom);
            this.panelControl11.Controls.Add(this.txtGIABAN1);
            this.panelControl11.Controls.Add(this.cboNhomhang);
            this.panelControl11.Controls.Add(this.txtTENCOMBO);
            this.panelControl11.Controls.Add(this.lblTENHANGHOA);
            this.panelControl11.Controls.Add(this.lbGiaBan);
            this.panelControl11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl11.Location = new System.Drawing.Point(0, 0);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(346, 394);
            this.panelControl11.TabIndex = 0;
            // 
            // txtGIABAN2
            // 
            this.txtGIABAN2.EnterMoveNextControl = true;
            this.txtGIABAN2.Location = new System.Drawing.Point(245, 213);
            this.txtGIABAN2.Name = "txtGIABAN2";
            this.txtGIABAN2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGIABAN2.Properties.Appearance.Options.UseFont = true;
            this.txtGIABAN2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGIABAN2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGIABAN2.Properties.DisplayFormat.FormatString = "N0";
            this.txtGIABAN2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN2.Properties.EditFormat.FormatString = "N0";
            this.txtGIABAN2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN2.Properties.Mask.EditMask = "N0";
            this.txtGIABAN2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGIABAN2.Size = new System.Drawing.Size(99, 26);
            this.txtGIABAN2.TabIndex = 113;
            this.txtGIABAN2.ToolTip = "GIÁ GIAO HÀNG";
            this.txtGIABAN2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // lbGiaBan2
            // 
            this.lbGiaBan2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGiaBan2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbGiaBan2.Location = new System.Drawing.Point(172, 216);
            this.lbGiaBan2.Margin = new System.Windows.Forms.Padding(4);
            this.lbGiaBan2.Name = "lbGiaBan2";
            this.lbGiaBan2.Size = new System.Drawing.Size(63, 19);
            this.lbGiaBan2.TabIndex = 112;
            this.lbGiaBan2.Text = "Giá bán2";
            this.lbGiaBan2.ToolTip = "GIÁ GIAO HÀNG";
            this.lbGiaBan2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // txtGIABAN4
            // 
            this.txtGIABAN4.EnterMoveNextControl = true;
            this.txtGIABAN4.Location = new System.Drawing.Point(246, 243);
            this.txtGIABAN4.Name = "txtGIABAN4";
            this.txtGIABAN4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGIABAN4.Properties.Appearance.Options.UseFont = true;
            this.txtGIABAN4.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGIABAN4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGIABAN4.Properties.DisplayFormat.FormatString = "N0";
            this.txtGIABAN4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN4.Properties.EditFormat.FormatString = "N0";
            this.txtGIABAN4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN4.Properties.Mask.EditMask = "N0";
            this.txtGIABAN4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGIABAN4.Size = new System.Drawing.Size(99, 26);
            this.txtGIABAN4.TabIndex = 111;
            this.txtGIABAN4.ToolTip = "GIÁ NỘI BỘ";
            this.txtGIABAN4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl3.Location = new System.Drawing.Point(172, 246);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(63, 19);
            this.labelControl3.TabIndex = 110;
            this.labelControl3.Text = "Giá bán4";
            this.labelControl3.ToolTip = "GIÁ NỘI BỘ";
            this.labelControl3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // txtGIABAN3
            // 
            this.txtGIABAN3.EnterMoveNextControl = true;
            this.txtGIABAN3.Location = new System.Drawing.Point(72, 243);
            this.txtGIABAN3.Name = "txtGIABAN3";
            this.txtGIABAN3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGIABAN3.Properties.Appearance.Options.UseFont = true;
            this.txtGIABAN3.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGIABAN3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGIABAN3.Properties.DisplayFormat.FormatString = "N0";
            this.txtGIABAN3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN3.Properties.EditFormat.FormatString = "N0";
            this.txtGIABAN3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIABAN3.Properties.Mask.EditMask = "N0";
            this.txtGIABAN3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGIABAN3.Size = new System.Drawing.Size(99, 26);
            this.txtGIABAN3.TabIndex = 109;
            this.txtGIABAN3.ToolTip = "GIÁ NHÂN VIÊN";
            this.txtGIABAN3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl4.Location = new System.Drawing.Point(2, 246);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(63, 19);
            this.labelControl4.TabIndex = 108;
            this.labelControl4.Text = "Giá bán3";
            this.labelControl4.ToolTip = "GIÁ NHÂN VIÊN";
            this.labelControl4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // btnAutoMaVach
            // 
            this.btnAutoMaVach.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAutoMaVach.Appearance.Options.UseFont = true;
            this.btnAutoMaVach.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnAutoMaVach.Location = new System.Drawing.Point(293, 304);
            this.btnAutoMaVach.Margin = new System.Windows.Forms.Padding(4);
            this.btnAutoMaVach.Name = "btnAutoMaVach";
            this.btnAutoMaVach.Size = new System.Drawing.Size(51, 26);
            this.btnAutoMaVach.TabIndex = 29;
            this.btnAutoMaVach.Text = "Auto";
            this.btnAutoMaVach.Click += new System.EventHandler(this.btnAutoMaVach_Click);
            // 
            // panelControl9
            // 
            this.panelControl9.Location = new System.Drawing.Point(1, 271);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(344, 2);
            this.panelControl9.TabIndex = 19;
            // 
            // txtMAVACH
            // 
            this.txtMAVACH.EnterMoveNextControl = true;
            this.txtMAVACH.Location = new System.Drawing.Point(112, 304);
            this.txtMAVACH.Name = "txtMAVACH";
            this.txtMAVACH.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMAVACH.Properties.Appearance.Options.UseFont = true;
            this.txtMAVACH.Size = new System.Drawing.Size(180, 26);
            this.txtMAVACH.TabIndex = 23;
            // 
            // lbMaVach
            // 
            this.lbMaVach.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMaVach.Location = new System.Drawing.Point(4, 307);
            this.lbMaVach.Margin = new System.Windows.Forms.Padding(4);
            this.lbMaVach.Name = "lbMaVach";
            this.lbMaVach.Size = new System.Drawing.Size(57, 19);
            this.lbMaVach.TabIndex = 22;
            this.lbMaVach.Text = "Mã vạch";
            // 
            // chkMayinbep
            // 
            this.chkMayinbep.EnterMoveNextControl = true;
            this.chkMayinbep.Location = new System.Drawing.Point(1, 275);
            this.chkMayinbep.Name = "chkMayinbep";
            this.chkMayinbep.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkMayinbep.Properties.Appearance.Options.UseFont = true;
            this.chkMayinbep.Properties.Caption = "Máy in bếp";
            this.chkMayinbep.Size = new System.Drawing.Size(105, 24);
            this.chkMayinbep.TabIndex = 20;
            // 
            // cboBep
            // 
            this.cboBep.EnterMoveNextControl = true;
            this.cboBep.Location = new System.Drawing.Point(112, 275);
            this.cboBep.Name = "cboBep";
            this.cboBep.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBep.Properties.Appearance.Options.UseFont = true;
            this.cboBep.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBep.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboBep.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboBep.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboBep.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN_CUAHANG", "TEN_CUAHANG"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MAYINBEP", "Máy in")});
            this.cboBep.Properties.DisplayMember = "TEN";
            this.cboBep.Properties.DropDownItemHeight = 40;
            this.cboBep.Properties.NullText = "";
            this.cboBep.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboBep.Properties.ShowHeader = false;
            this.cboBep.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboBep.Properties.ValueMember = "MA";
            this.cboBep.Size = new System.Drawing.Size(232, 26);
            this.cboBep.TabIndex = 21;
            // 
            // chkSUDUNG
            // 
            this.chkSUDUNG.EnterMoveNextControl = true;
            this.chkSUDUNG.Location = new System.Drawing.Point(112, 329);
            this.chkSUDUNG.Name = "chkSUDUNG";
            this.chkSUDUNG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSUDUNG.Properties.Appearance.Options.UseFont = true;
            this.chkSUDUNG.Properties.Caption = "Sử dụng";
            this.chkSUDUNG.Size = new System.Drawing.Size(84, 24);
            this.chkSUDUNG.TabIndex = 24;
            // 
            // btnAUTO
            // 
            this.btnAUTO.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAUTO.Appearance.Options.UseFont = true;
            this.btnAUTO.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnAUTO.Location = new System.Drawing.Point(174, 34);
            this.btnAUTO.Margin = new System.Windows.Forms.Padding(4);
            this.btnAUTO.Name = "btnAUTO";
            this.btnAUTO.Size = new System.Drawing.Size(51, 26);
            this.btnAUTO.TabIndex = 4;
            this.btnAUTO.Text = "Auto";
            this.btnAUTO.Click += new System.EventHandler(this.btnAUTO_Click);
            // 
            // btnLuuCombo
            // 
            this.btnLuuCombo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuCombo.Appearance.Options.UseFont = true;
            this.btnLuuCombo.Image = global::KP_RES.Properties.Resources.save_26;
            this.btnLuuCombo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnLuuCombo.Location = new System.Drawing.Point(10, 356);
            this.btnLuuCombo.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuuCombo.Name = "btnLuuCombo";
            this.btnLuuCombo.Size = new System.Drawing.Size(80, 35);
            this.btnLuuCombo.TabIndex = 25;
            this.btnLuuCombo.Text = "&Lưu";
            this.btnLuuCombo.Click += new System.EventHandler(this.btnLuuCombo_Click);
            // 
            // lbMa
            // 
            this.lbMa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMa.Location = new System.Drawing.Point(4, 37);
            this.lbMa.Margin = new System.Windows.Forms.Padding(4);
            this.lbMa.Name = "lbMa";
            this.lbMa.Size = new System.Drawing.Size(20, 19);
            this.lbMa.TabIndex = 2;
            this.lbMa.Text = "Mã";
            // 
            // btnXoaCombo
            // 
            this.btnXoaCombo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaCombo.Appearance.Options.UseFont = true;
            this.btnXoaCombo.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btnXoaCombo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnXoaCombo.Location = new System.Drawing.Point(259, 356);
            this.btnXoaCombo.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoaCombo.Name = "btnXoaCombo";
            this.btnXoaCombo.Size = new System.Drawing.Size(80, 35);
            this.btnXoaCombo.TabIndex = 28;
            this.btnXoaCombo.Text = "&Xóa";
            this.btnXoaCombo.Click += new System.EventHandler(this.btnXoaCombo_Click);
            // 
            // btnSuaCombo
            // 
            this.btnSuaCombo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaCombo.Appearance.Options.UseFont = true;
            this.btnSuaCombo.Image = ((System.Drawing.Image)(resources.GetObject("btnSuaCombo.Image")));
            this.btnSuaCombo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSuaCombo.Location = new System.Drawing.Point(176, 356);
            this.btnSuaCombo.Margin = new System.Windows.Forms.Padding(4);
            this.btnSuaCombo.Name = "btnSuaCombo";
            this.btnSuaCombo.Size = new System.Drawing.Size(80, 35);
            this.btnSuaCombo.TabIndex = 27;
            this.btnSuaCombo.Text = "&Sửa";
            this.btnSuaCombo.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // txtMAHANGHOA
            // 
            this.txtMAHANGHOA.EnterMoveNextControl = true;
            this.txtMAHANGHOA.Location = new System.Drawing.Point(62, 34);
            this.txtMAHANGHOA.Name = "txtMAHANGHOA";
            this.txtMAHANGHOA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMAHANGHOA.Properties.Appearance.Options.UseFont = true;
            this.txtMAHANGHOA.Size = new System.Drawing.Size(108, 26);
            this.txtMAHANGHOA.TabIndex = 3;
            // 
            // btnThemCombo
            // 
            this.btnThemCombo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemCombo.Appearance.Options.UseFont = true;
            this.btnThemCombo.Image = ((System.Drawing.Image)(resources.GetObject("btnThemCombo.Image")));
            this.btnThemCombo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnThemCombo.Location = new System.Drawing.Point(93, 356);
            this.btnThemCombo.Margin = new System.Windows.Forms.Padding(4);
            this.btnThemCombo.Name = "btnThemCombo";
            this.btnThemCombo.Size = new System.Drawing.Size(80, 35);
            this.btnThemCombo.TabIndex = 26;
            this.btnThemCombo.Text = "&Thêm";
            this.btnThemCombo.Click += new System.EventHandler(this.btnThemCombo_Click);
            // 
            // panelControl6
            // 
            this.panelControl6.Location = new System.Drawing.Point(1, 179);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(344, 2);
            this.panelControl6.TabIndex = 12;
            // 
            // txtGIAMUA
            // 
            this.txtGIAMUA.EnterMoveNextControl = true;
            this.txtGIAMUA.Location = new System.Drawing.Point(245, 184);
            this.txtGIAMUA.Name = "txtGIAMUA";
            this.txtGIAMUA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGIAMUA.Properties.Appearance.Options.UseFont = true;
            this.txtGIAMUA.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGIAMUA.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGIAMUA.Properties.DisplayFormat.FormatString = "N0";
            this.txtGIAMUA.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIAMUA.Properties.EditFormat.FormatString = "N0";
            this.txtGIAMUA.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGIAMUA.Properties.Mask.EditMask = "N0";
            this.txtGIAMUA.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGIAMUA.Size = new System.Drawing.Size(99, 26);
            this.txtGIAMUA.TabIndex = 16;
            // 
            // lbGiaMua
            // 
            this.lbGiaMua.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGiaMua.Location = new System.Drawing.Point(172, 187);
            this.lbGiaMua.Margin = new System.Windows.Forms.Padding(4);
            this.lbGiaMua.Name = "lbGiaMua";
            this.lbGiaMua.Size = new System.Drawing.Size(59, 19);
            this.lbGiaMua.TabIndex = 15;
            this.lbGiaMua.Text = "Giá mua";
            // 
            // lbThue
            // 
            this.lbThue.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbThue.Location = new System.Drawing.Point(2, 187);
            this.lbThue.Margin = new System.Windows.Forms.Padding(4);
            this.lbThue.Name = "lbThue";
            this.lbThue.Size = new System.Drawing.Size(36, 19);
            this.lbThue.TabIndex = 14;
            this.lbThue.Text = "Thuế";
            // 
            // txtGHICHUHANGHOA
            // 
            this.txtGHICHUHANGHOA.EnterMoveNextControl = true;
            this.txtGHICHUHANGHOA.Location = new System.Drawing.Point(62, 150);
            this.txtGHICHUHANGHOA.Name = "txtGHICHUHANGHOA";
            this.txtGHICHUHANGHOA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGHICHUHANGHOA.Properties.Appearance.Options.UseFont = true;
            this.txtGHICHUHANGHOA.Size = new System.Drawing.Size(282, 26);
            this.txtGHICHUHANGHOA.TabIndex = 11;
            // 
            // lbSTT
            // 
            this.lbSTT.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSTT.Location = new System.Drawing.Point(4, 124);
            this.lbSTT.Margin = new System.Windows.Forms.Padding(4);
            this.lbSTT.Name = "lbSTT";
            this.lbSTT.Size = new System.Drawing.Size(29, 19);
            this.lbSTT.TabIndex = 8;
            this.lbSTT.Text = "STT";
            // 
            // txtSTTHANGHOA
            // 
            this.txtSTTHANGHOA.EnterMoveNextControl = true;
            this.txtSTTHANGHOA.Location = new System.Drawing.Point(62, 121);
            this.txtSTTHANGHOA.Name = "txtSTTHANGHOA";
            this.txtSTTHANGHOA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSTTHANGHOA.Properties.Appearance.Options.UseFont = true;
            this.txtSTTHANGHOA.Properties.Mask.EditMask = "d";
            this.txtSTTHANGHOA.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSTTHANGHOA.Size = new System.Drawing.Size(162, 26);
            this.txtSTTHANGHOA.TabIndex = 9;
            // 
            // lbGhiChu
            // 
            this.lbGhiChu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGhiChu.Location = new System.Drawing.Point(4, 153);
            this.lbGhiChu.Margin = new System.Windows.Forms.Padding(4);
            this.lbGhiChu.Name = "lbGhiChu";
            this.lbGhiChu.Size = new System.Drawing.Size(54, 19);
            this.lbGhiChu.TabIndex = 10;
            this.lbGhiChu.Text = "Ghi chú";
            // 
            // lblDVT
            // 
            this.lblDVT.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDVT.Location = new System.Drawing.Point(4, 95);
            this.lblDVT.Margin = new System.Windows.Forms.Padding(4);
            this.lblDVT.Name = "lblDVT";
            this.lblDVT.Size = new System.Drawing.Size(31, 19);
            this.lblDVT.TabIndex = 6;
            this.lblDVT.Text = "ĐVT";
            // 
            // cboDonvitinh
            // 
            this.cboDonvitinh.EnterMoveNextControl = true;
            this.cboDonvitinh.Location = new System.Drawing.Point(62, 92);
            this.cboDonvitinh.Name = "cboDonvitinh";
            this.cboDonvitinh.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDonvitinh.Properties.Appearance.Options.UseFont = true;
            this.cboDonvitinh.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDonvitinh.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboDonvitinh.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboDonvitinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboDonvitinh.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboDonvitinh.Properties.DisplayMember = "TEN";
            this.cboDonvitinh.Properties.DropDownItemHeight = 40;
            this.cboDonvitinh.Properties.NullText = "";
            this.cboDonvitinh.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.cboDonvitinh.Properties.ShowHeader = false;
            this.cboDonvitinh.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboDonvitinh.Properties.ValueMember = "MA";
            this.cboDonvitinh.Size = new System.Drawing.Size(162, 26);
            this.cboDonvitinh.TabIndex = 7;
            // 
            // cboThue
            // 
            this.cboThue.EnterMoveNextControl = true;
            this.cboThue.Location = new System.Drawing.Point(72, 184);
            this.cboThue.Name = "cboThue";
            this.cboThue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboThue.Properties.Appearance.Options.UseFont = true;
            this.cboThue.Properties.Appearance.Options.UseTextOptions = true;
            this.cboThue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cboThue.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cboThue.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboThue.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.cboThue.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cboThue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboThue.Properties.DropDownItemHeight = 40;
            this.cboThue.Properties.Items.AddRange(new object[] {
            "0",
            "5",
            "10"});
            this.cboThue.Size = new System.Drawing.Size(99, 26);
            this.cboThue.TabIndex = 13;
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.picHANGHOA);
            this.panelControl8.Location = new System.Drawing.Point(228, 34);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(118, 113);
            this.panelControl8.TabIndex = 25;
            // 
            // picHANGHOA
            // 
            this.picHANGHOA.BackgroundImage = global::KP_RES.Properties.Resources.slr_camera_26;
            this.picHANGHOA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picHANGHOA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picHANGHOA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picHANGHOA.Location = new System.Drawing.Point(2, 2);
            this.picHANGHOA.Name = "picHANGHOA";
            this.picHANGHOA.Size = new System.Drawing.Size(114, 109);
            this.picHANGHOA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picHANGHOA.TabIndex = 23;
            this.picHANGHOA.TabStop = false;
            this.picHANGHOA.Click += new System.EventHandler(this.picHANGHOA_Click);
            // 
            // lbNhom
            // 
            this.lbNhom.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNhom.Location = new System.Drawing.Point(4, 8);
            this.lbNhom.Margin = new System.Windows.Forms.Padding(4);
            this.lbNhom.Name = "lbNhom";
            this.lbNhom.Size = new System.Drawing.Size(43, 19);
            this.lbNhom.TabIndex = 0;
            this.lbNhom.Text = "Nhóm";
            // 
            // txtGIABAN1
            // 
            this.txtGIABAN1.EditValue = "0";
            this.txtGIABAN1.EnterMoveNextControl = true;
            this.txtGIABAN1.Location = new System.Drawing.Point(72, 213);
            this.txtGIABAN1.Name = "txtGIABAN1";
            this.txtGIABAN1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGIABAN1.Properties.Appearance.Options.UseFont = true;
            this.txtGIABAN1.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGIABAN1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGIABAN1.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtGIABAN1.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.txtGIABAN1.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.txtGIABAN1.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.txtGIABAN1.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.txtGIABAN1.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black;
            this.txtGIABAN1.Properties.AppearanceReadOnly.Options.UseForeColor = true;
            this.txtGIABAN1.Properties.DisplayFormat.FormatString = "{0:#,###0}";
            this.txtGIABAN1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtGIABAN1.Properties.Mask.BeepOnError = true;
            this.txtGIABAN1.Properties.Mask.EditMask = "d";
            this.txtGIABAN1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGIABAN1.Size = new System.Drawing.Size(99, 26);
            this.txtGIABAN1.TabIndex = 18;
            // 
            // cboNhomhang
            // 
            this.cboNhomhang.EnterMoveNextControl = true;
            this.cboNhomhang.Location = new System.Drawing.Point(62, 5);
            this.cboNhomhang.Name = "cboNhomhang";
            this.cboNhomhang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhomhang.Properties.Appearance.Options.UseFont = true;
            this.cboNhomhang.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNhomhang.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboNhomhang.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.cboNhomhang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboNhomhang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MA", "Mã", 30, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TEN", "Tên", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.cboNhomhang.Properties.DisplayMember = "TEN";
            this.cboNhomhang.Properties.DropDownItemHeight = 40;
            this.cboNhomhang.Properties.NullText = "";
            this.cboNhomhang.Properties.ShowHeader = false;
            this.cboNhomhang.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboNhomhang.Properties.ValueMember = "MA";
            this.cboNhomhang.Size = new System.Drawing.Size(282, 26);
            this.cboNhomhang.TabIndex = 1;
            // 
            // txtTENCOMBO
            // 
            this.txtTENCOMBO.EnterMoveNextControl = true;
            this.txtTENCOMBO.Location = new System.Drawing.Point(62, 63);
            this.txtTENCOMBO.Name = "txtTENCOMBO";
            this.txtTENCOMBO.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTENCOMBO.Properties.Appearance.Options.UseFont = true;
            this.txtTENCOMBO.Properties.ReadOnly = true;
            this.txtTENCOMBO.Size = new System.Drawing.Size(163, 26);
            this.txtTENCOMBO.TabIndex = 5;
            // 
            // lblTENHANGHOA
            // 
            this.lblTENHANGHOA.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTENHANGHOA.Location = new System.Drawing.Point(4, 66);
            this.lblTENHANGHOA.Margin = new System.Windows.Forms.Padding(4);
            this.lblTENHANGHOA.Name = "lblTENHANGHOA";
            this.lblTENHANGHOA.Size = new System.Drawing.Size(51, 19);
            this.lblTENHANGHOA.TabIndex = 4;
            this.lblTENHANGHOA.Text = "Combo";
            // 
            // lbGiaBan
            // 
            this.lbGiaBan.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGiaBan.Location = new System.Drawing.Point(2, 213);
            this.lbGiaBan.Margin = new System.Windows.Forms.Padding(4);
            this.lbGiaBan.Name = "lbGiaBan";
            this.lbGiaBan.Size = new System.Drawing.Size(63, 19);
            this.lbGiaBan.TabIndex = 17;
            this.lbGiaBan.Text = "Giá bán1";
            // 
            // panelControl7
            // 
            this.panelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl7.Controls.Add(this.btnNangcao);
            this.panelControl7.Controls.Add(this.btnNghiepvu);
            this.panelControl7.Controls.Add(this.btnCapnhat);
            this.panelControl7.Controls.Add(this.btnBanphim);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl7.Location = new System.Drawing.Point(2, 708);
            this.panelControl7.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(346, 35);
            this.panelControl7.TabIndex = 17;
            // 
            // btnNangcao
            // 
            this.btnNangcao.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNangcao.Appearance.Options.UseFont = true;
            this.btnNangcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNangcao.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnNangcao.Location = new System.Drawing.Point(98, 0);
            this.btnNangcao.Margin = new System.Windows.Forms.Padding(4);
            this.btnNangcao.Name = "btnNangcao";
            this.btnNangcao.Size = new System.Drawing.Size(98, 35);
            this.btnNangcao.TabIndex = 12;
            this.btnNangcao.Tag = "";
            this.btnNangcao.Text = "Nâng cao";
            this.btnNangcao.Click += new System.EventHandler(this.btnNangcao_Click);
            // 
            // btnNghiepvu
            // 
            this.btnNghiepvu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNghiepvu.Appearance.Options.UseFont = true;
            this.btnNghiepvu.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnNghiepvu.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnNghiepvu.Location = new System.Drawing.Point(0, 0);
            this.btnNghiepvu.Margin = new System.Windows.Forms.Padding(4);
            this.btnNghiepvu.Name = "btnNghiepvu";
            this.btnNghiepvu.Size = new System.Drawing.Size(98, 35);
            this.btnNghiepvu.TabIndex = 11;
            this.btnNghiepvu.Tag = "";
            this.btnNghiepvu.Text = "Nghiệp vụ";
            this.btnNghiepvu.Click += new System.EventHandler(this.btnc_Click);
            // 
            // btnCapnhat
            // 
            this.btnCapnhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapnhat.Appearance.Options.UseFont = true;
            this.btnCapnhat.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCapnhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapnhat.Image")));
            this.btnCapnhat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCapnhat.Location = new System.Drawing.Point(196, 0);
            this.btnCapnhat.Margin = new System.Windows.Forms.Padding(4);
            this.btnCapnhat.Name = "btnCapnhat";
            this.btnCapnhat.Size = new System.Drawing.Size(75, 35);
            this.btnCapnhat.TabIndex = 1;
            this.btnCapnhat.Click += new System.EventHandler(this.btnCapnhat_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnBanphim.Image = ((System.Drawing.Image)(resources.GetObject("btnBanphim.Image")));
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(271, 0);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(4);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(75, 35);
            this.btnBanphim.TabIndex = 10;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton5.Location = new System.Drawing.Point(65, 32);
            this.simpleButton5.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(80, 35);
            this.simpleButton5.TabIndex = 10;
            this.simpleButton5.Text = "&Lưu";
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton6.Location = new System.Drawing.Point(73, 84);
            this.simpleButton6.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(80, 35);
            this.simpleButton6.TabIndex = 11;
            this.simpleButton6.Text = "&Lưu";
            // 
            // simpleButton7
            // 
            this.simpleButton7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton7.Appearance.Options.UseFont = true;
            this.simpleButton7.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton7.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton7.Location = new System.Drawing.Point(81, 92);
            this.simpleButton7.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(80, 35);
            this.simpleButton7.TabIndex = 12;
            this.simpleButton7.Text = "&Lưu";
            // 
            // simpleButton8
            // 
            this.simpleButton8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton8.Appearance.Options.UseFont = true;
            this.simpleButton8.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton8.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton8.Location = new System.Drawing.Point(89, 100);
            this.simpleButton8.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(80, 35);
            this.simpleButton8.TabIndex = 13;
            this.simpleButton8.Text = "&Lưu";
            // 
            // simpleButton9
            // 
            this.simpleButton9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton9.Appearance.Options.UseFont = true;
            this.simpleButton9.Image = global::KP_RES.Properties.Resources.save_26;
            this.simpleButton9.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton9.Location = new System.Drawing.Point(114, 150);
            this.simpleButton9.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(80, 35);
            this.simpleButton9.TabIndex = 14;
            this.simpleButton9.Text = "&Lưu";
            // 
            // Frm_SetCombo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 749);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_SetCombo";
            this.Text = "Set Combo";
            this.Load += new System.EventHandler(this.Frm_NhomHanghoa_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Frm_NhomHanghoa_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palComboIcon)).EndInit();
            this.palComboIcon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palLIETKE)).EndInit();
            this.palLIETKE.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).EndInit();
            this.panelControl13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.palHANGHOA)).EndInit();
            this.palHANGHOA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palNHOMHANG)).EndInit();
            this.palNHOMHANG.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.palSetCombo)).EndInit();
            this.palSetCombo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            this.panelControl15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboHANGHOA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSOLUONGHANG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            this.panelControl11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMAVACH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMayinbep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboBep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSUDUNG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMAHANGHOA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIAMUA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGHICHUHANGHOA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTTHANGHOA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDonvitinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboThue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picHANGHOA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGIABAN1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNhomhang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTENCOMBO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl palNHOMHANG;
        private DevExpress.XtraEditors.PanelControl palHANGHOA;
        private DevExpress.XtraEditors.SimpleButton btnXuongNhomhang;
        private DevExpress.XtraEditors.SimpleButton btnLenNhomhang;
        private System.Windows.Forms.FlowLayoutPanel fpnlHanghoa;
        private DevExpress.XtraEditors.SimpleButton btnXuongHangHoa;
        private DevExpress.XtraEditors.SimpleButton btnLenHangHoa;
        private System.Windows.Forms.FlowLayoutPanel fpnlNhomhang;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.SimpleButton btnCapnhat;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.PanelControl palLIETKE;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT1;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn GIABAN1;
        private DevExpress.XtraGrid.Columns.GridColumn MA_NHOMHANG;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_NHOMHANG;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl13;
        private DevExpress.XtraEditors.PanelControl panelControl14;
        private DevExpress.XtraEditors.SimpleButton btnXuongit;
        private DevExpress.XtraEditors.SimpleButton btnXuongnhieu;
        private DevExpress.XtraEditors.SimpleButton btnLenit;
        private DevExpress.XtraEditors.SimpleButton btnLennhieu;
        private DevExpress.XtraEditors.SimpleButton btnCapnhatGV;
        private DevExpress.XtraGrid.Columns.GridColumn MA_HANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn SUDUNG;
        private DevExpress.XtraEditors.PanelControl palSetCombo;
        private DevExpress.XtraEditors.LabelControl lbGiaBan;
        private DevExpress.XtraEditors.LabelControl lblTENHANGHOA;
        private DevExpress.XtraEditors.TextEdit txtTENCOMBO;
        private DevExpress.XtraEditors.TextEdit txtGIABAN1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn MA;
        private DevExpress.XtraGrid.Columns.GridColumn TEN;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboHANGHOA;
        private DevExpress.XtraGrid.Columns.GridColumn SOLUONG;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtSOLUONGHANG;
        private DevExpress.XtraGrid.Columns.GridColumn PRICE;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtGIA;
        private DevExpress.XtraGrid.Columns.GridColumn STTCombo;
        private DevExpress.XtraEditors.SimpleButton btnLuuChitietCombo;
        private DevExpress.XtraEditors.SimpleButton btnHuyChitietCombo;
        private DevExpress.XtraEditors.SimpleButton btnThemChitietCombo;
        private DevExpress.XtraEditors.LookUpEdit cboNhomhang;
        private DevExpress.XtraEditors.LabelControl lbNhom;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraEditors.SimpleButton btnThemCombo;
        private DevExpress.XtraEditors.SimpleButton btnXoaCombo;
        private DevExpress.XtraEditors.SimpleButton btnLuuCombo;
        private DevExpress.XtraEditors.SimpleButton btnSuaCombo;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.SimpleButton btnSuaChitietCombo;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private System.Windows.Forms.PictureBox picHANGHOA;
        private DevExpress.XtraEditors.SimpleButton btnXoadong;
        private DevExpress.XtraEditors.SimpleButton btnHuy;
        private DevExpress.XtraEditors.SimpleButton btnComboGrid;
        private DevExpress.XtraEditors.SimpleButton btnComboIcon;
        private DevExpress.XtraGrid.Columns.GridColumn FILL;
        private DevExpress.XtraGrid.Columns.GridColumn HINHANH;
        private DevExpress.XtraEditors.PanelControl palComboIcon;
        private System.Windows.Forms.FlowLayoutPanel fpnlCombo;
        private DevExpress.XtraEditors.SimpleButton btnXuongCombo;
        private DevExpress.XtraEditors.SimpleButton btnLenCombo;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.SimpleButton btnComboChitiet;
        private DevExpress.XtraEditors.LabelControl lbMa;
        private DevExpress.XtraEditors.TextEdit txtMAHANGHOA;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.TextEdit txtGIAMUA;
        private DevExpress.XtraEditors.LabelControl lbGiaMua;
        private DevExpress.XtraEditors.LabelControl lbThue;
        private DevExpress.XtraEditors.TextEdit txtGHICHUHANGHOA;
        private DevExpress.XtraEditors.LabelControl lbSTT;
        private DevExpress.XtraEditors.TextEdit txtSTTHANGHOA;
        private DevExpress.XtraEditors.LabelControl lbGhiChu;
        private DevExpress.XtraEditors.LabelControl lblDVT;
        private DevExpress.XtraEditors.LookUpEdit cboDonvitinh;
        private DevExpress.XtraEditors.ComboBoxEdit cboThue;
        private DevExpress.XtraEditors.SimpleButton btnAUTO;
        private DevExpress.XtraEditors.CheckEdit chkSUDUNG;
        private DevExpress.XtraEditors.CheckEdit chkMayinbep;
        private DevExpress.XtraEditors.LookUpEdit cboBep;
        private DevExpress.XtraEditors.TextEdit txtMAVACH;
        private DevExpress.XtraEditors.LabelControl lbMaVach;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.SimpleButton btnNghiepvu;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.PanelControl panelControl15;
        private DevExpress.XtraEditors.SimpleButton btnXemDS;
        private DevExpress.XtraEditors.SimpleButton btnDoimathang;
        private DevExpress.XtraEditors.SimpleButton btnNangcao;
        private DevExpress.XtraEditors.SimpleButton btnHoanTat;
        private DevExpress.XtraEditors.SimpleButton btnAutoMaVach;
        private DevExpress.XtraEditors.TextEdit txtGIABAN4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtGIABAN3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtGIABAN2;
        private DevExpress.XtraEditors.LabelControl lbGiaBan2;




    }
}