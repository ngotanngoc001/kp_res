﻿namespace KP_RES 
{
    partial class  Frm_Manhinhhai 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Manhinhhai));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pn_Hinh = new DevExpress.XtraEditors.PanelControl();
            this.pn_xemvideo = new DevExpress.XtraEditors.PanelControl();
            this.WindowsMediaPlayer = new AxWMPLib.AxWindowsMediaPlayer();
            this.pn_cuon = new DevExpress.XtraEditors.PanelControl();
            this.pal_xuong = new DevExpress.XtraEditors.PanelControl();
            this.lbl_sotrang = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_tranglen = new DevExpress.XtraEditors.SimpleButton();
            this.bnt_trangxuong = new DevExpress.XtraEditors.SimpleButton();
            this.btn_bochonhinh = new DevExpress.XtraEditors.SimpleButton();
            this.btnChonTatCaHinh = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Them = new DevExpress.XtraEditors.SimpleButton();
            this.pn_CaiDat_Video = new DevExpress.XtraEditors.PanelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TENHINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bnt_Play = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn_stop = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.CHON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAHINH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.btnbochonvideo = new DevExpress.XtraEditors.SimpleButton();
            this.btnChonTatCaVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btn_XoaVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ThemVideo = new DevExpress.XtraEditors.SimpleButton();
            this.pnChon = new DevExpress.XtraEditors.PanelControl();
            this.btnHuyAnh = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.btn_Huy = new DevExpress.XtraEditors.SimpleButton();
            this.btn_chuyen = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl14 = new DevExpress.XtraEditors.PanelControl();
            this.btnRefesh = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.pn_CuaHang = new DevExpress.XtraEditors.PanelControl();
            this.panel_Sandard = new DevExpress.XtraEditors.PanelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.pn_ChiTietQuay = new DevExpress.XtraEditors.PanelControl();
            this.gvChiTietQuay = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnChonTatCaQuay = new DevExpress.XtraEditors.SimpleButton();
            this.pnQuay = new DevExpress.XtraEditors.PanelControl();
            this.gvDSQUAY = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.cb_CuaHang = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MA_CUAHANG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TEN_CUAHANG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.pal_BUTTON = new DevExpress.XtraEditors.PanelControl();
            this.btn_bochonquay = new DevExpress.XtraEditors.SimpleButton();
            this.btnChonTatCaChiTiet = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_Hinh)).BeginInit();
            this.pn_Hinh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_xemvideo)).BeginInit();
            this.pn_xemvideo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WindowsMediaPlayer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_cuon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pal_xuong)).BeginInit();
            this.pal_xuong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_CaiDat_Video)).BeginInit();
            this.pn_CaiDat_Video.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnt_Play)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_stop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnChon)).BeginInit();
            this.pnChon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_CuaHang)).BeginInit();
            this.pn_CuaHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel_Sandard)).BeginInit();
            this.panel_Sandard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pn_ChiTietQuay)).BeginInit();
            this.pn_ChiTietQuay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvChiTietQuay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnQuay)).BeginInit();
            this.pnQuay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvDSQUAY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cb_CuaHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pal_BUTTON)).BeginInit();
            this.pal_BUTTON.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pn_Hinh);
            this.panelControl1.Controls.Add(this.pn_CaiDat_Video);
            this.panelControl1.Controls.Add(this.pnChon);
            this.panelControl1.Controls.Add(this.pn_CuaHang);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1350, 741);
            this.panelControl1.TabIndex = 0;
            // 
            // pn_Hinh
            // 
            this.pn_Hinh.Controls.Add(this.pn_xemvideo);
            this.pn_Hinh.Controls.Add(this.pn_cuon);
            this.pn_Hinh.Controls.Add(this.pal_xuong);
            this.pn_Hinh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_Hinh.Location = new System.Drawing.Point(2, 2);
            this.pn_Hinh.Name = "pn_Hinh";
            this.pn_Hinh.Size = new System.Drawing.Size(901, 433);
            this.pn_Hinh.TabIndex = 56;
            // 
            // pn_xemvideo
            // 
            this.pn_xemvideo.Controls.Add(this.WindowsMediaPlayer);
            this.pn_xemvideo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_xemvideo.Location = new System.Drawing.Point(2, 37);
            this.pn_xemvideo.Name = "pn_xemvideo";
            this.pn_xemvideo.Size = new System.Drawing.Size(897, 394);
            this.pn_xemvideo.TabIndex = 0;
            this.pn_xemvideo.Visible = false;
            // 
            // WindowsMediaPlayer
            // 
            this.WindowsMediaPlayer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WindowsMediaPlayer.Enabled = true;
            this.WindowsMediaPlayer.Location = new System.Drawing.Point(2, 2);
            this.WindowsMediaPlayer.Name = "WindowsMediaPlayer";
            this.WindowsMediaPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("WindowsMediaPlayer.OcxState")));
            this.WindowsMediaPlayer.Size = new System.Drawing.Size(893, 390);
            this.WindowsMediaPlayer.TabIndex = 2;
            // 
            // pn_cuon
            // 
            this.pn_cuon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_cuon.Location = new System.Drawing.Point(2, 37);
            this.pn_cuon.Name = "pn_cuon";
            this.pn_cuon.Size = new System.Drawing.Size(897, 394);
            this.pn_cuon.TabIndex = 56;
            // 
            // pal_xuong
            // 
            this.pal_xuong.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_xuong.Controls.Add(this.lbl_sotrang);
            this.pal_xuong.Controls.Add(this.bnt_tranglen);
            this.pal_xuong.Controls.Add(this.bnt_trangxuong);
            this.pal_xuong.Controls.Add(this.btn_bochonhinh);
            this.pal_xuong.Controls.Add(this.btnChonTatCaHinh);
            this.pal_xuong.Controls.Add(this.btn_Xoa);
            this.pal_xuong.Controls.Add(this.btn_Them);
            this.pal_xuong.Dock = System.Windows.Forms.DockStyle.Top;
            this.pal_xuong.Location = new System.Drawing.Point(2, 2);
            this.pal_xuong.Name = "pal_xuong";
            this.pal_xuong.Size = new System.Drawing.Size(897, 35);
            this.pal_xuong.TabIndex = 53;
            // 
            // lbl_sotrang
            // 
            this.lbl_sotrang.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_sotrang.Appearance.Options.UseFont = true;
            this.lbl_sotrang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_sotrang.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.lbl_sotrang.Location = new System.Drawing.Point(774, 0);
            this.lbl_sotrang.Margin = new System.Windows.Forms.Padding(4);
            this.lbl_sotrang.Name = "lbl_sotrang";
            this.lbl_sotrang.Size = new System.Drawing.Size(123, 35);
            this.lbl_sotrang.TabIndex = 49;
            this.lbl_sotrang.Text = "1/5";
            // 
            // bnt_tranglen
            // 
            this.bnt_tranglen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_tranglen.Appearance.Options.UseFont = true;
            this.bnt_tranglen.Appearance.Options.UseTextOptions = true;
            this.bnt_tranglen.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bnt_tranglen.Dock = System.Windows.Forms.DockStyle.Left;
            this.bnt_tranglen.Image = global::KP_RES.Properties.Resources.up22_26;
            this.bnt_tranglen.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_tranglen.Location = new System.Drawing.Point(645, 0);
            this.bnt_tranglen.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_tranglen.Name = "bnt_tranglen";
            this.bnt_tranglen.Size = new System.Drawing.Size(129, 35);
            this.bnt_tranglen.TabIndex = 50;
            this.bnt_tranglen.Text = "Khu vực";
            this.bnt_tranglen.Click += new System.EventHandler(this.bnt_tranglen_Click);
            // 
            // bnt_trangxuong
            // 
            this.bnt_trangxuong.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_trangxuong.Appearance.Options.UseFont = true;
            this.bnt_trangxuong.Appearance.Options.UseTextOptions = true;
            this.bnt_trangxuong.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bnt_trangxuong.Dock = System.Windows.Forms.DockStyle.Left;
            this.bnt_trangxuong.Image = global::KP_RES.Properties.Resources.up11_26;
            this.bnt_trangxuong.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnt_trangxuong.Location = new System.Drawing.Point(516, 0);
            this.bnt_trangxuong.Margin = new System.Windows.Forms.Padding(4);
            this.bnt_trangxuong.Name = "bnt_trangxuong";
            this.bnt_trangxuong.Size = new System.Drawing.Size(129, 35);
            this.bnt_trangxuong.TabIndex = 50;
            this.bnt_trangxuong.Text = "Khu vực";
            this.bnt_trangxuong.Click += new System.EventHandler(this.bnt_trangxuong_Click);
            // 
            // btn_bochonhinh
            // 
            this.btn_bochonhinh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_bochonhinh.Appearance.Options.UseFont = true;
            this.btn_bochonhinh.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_bochonhinh.Image = global::KP_RES.Properties.Resources.restart_26;
            this.btn_bochonhinh.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_bochonhinh.Location = new System.Drawing.Point(387, 0);
            this.btn_bochonhinh.Margin = new System.Windows.Forms.Padding(4);
            this.btn_bochonhinh.Name = "btn_bochonhinh";
            this.btn_bochonhinh.Size = new System.Drawing.Size(129, 35);
            this.btn_bochonhinh.TabIndex = 55;
            this.btn_bochonhinh.Text = "Bỏ chọn";
            this.btn_bochonhinh.Click += new System.EventHandler(this.btn_bochonhinh_Click);
            // 
            // btnChonTatCaHinh
            // 
            this.btnChonTatCaHinh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonTatCaHinh.Appearance.Options.UseFont = true;
            this.btnChonTatCaHinh.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnChonTatCaHinh.Image = ((System.Drawing.Image)(resources.GetObject("btnChonTatCaHinh.Image")));
            this.btnChonTatCaHinh.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnChonTatCaHinh.Location = new System.Drawing.Point(258, 0);
            this.btnChonTatCaHinh.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonTatCaHinh.Name = "btnChonTatCaHinh";
            this.btnChonTatCaHinh.Size = new System.Drawing.Size(129, 35);
            this.btnChonTatCaHinh.TabIndex = 56;
            this.btnChonTatCaHinh.Text = "Chọn tất cả";
            this.btnChonTatCaHinh.Click += new System.EventHandler(this.btnChonTatCa_Click);
            // 
            // btn_Xoa
            // 
            this.btn_Xoa.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Xoa.Appearance.Options.UseFont = true;
            this.btn_Xoa.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_Xoa.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btn_Xoa.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_Xoa.Location = new System.Drawing.Point(129, 0);
            this.btn_Xoa.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Xoa.Name = "btn_Xoa";
            this.btn_Xoa.Size = new System.Drawing.Size(129, 35);
            this.btn_Xoa.TabIndex = 54;
            this.btn_Xoa.Text = "Xóa hình";
            this.btn_Xoa.Click += new System.EventHandler(this.btn_Xoa_Click);
            // 
            // btn_Them
            // 
            this.btn_Them.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Them.Appearance.Options.UseFont = true;
            this.btn_Them.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_Them.Image = global::KP_RES.Properties.Resources.slr_camera_26;
            this.btn_Them.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_Them.Location = new System.Drawing.Point(0, 0);
            this.btn_Them.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Them.Name = "btn_Them";
            this.btn_Them.Size = new System.Drawing.Size(129, 35);
            this.btn_Them.TabIndex = 51;
            this.btn_Them.Text = "Thêm hình";
            this.btn_Them.Click += new System.EventHandler(this.btn_Them_Click);
            // 
            // pn_CaiDat_Video
            // 
            this.pn_CaiDat_Video.Controls.Add(this.gridControl2);
            this.pn_CaiDat_Video.Controls.Add(this.panelControl3);
            this.pn_CaiDat_Video.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pn_CaiDat_Video.Location = new System.Drawing.Point(2, 435);
            this.pn_CaiDat_Video.Name = "pn_CaiDat_Video";
            this.pn_CaiDat_Video.Size = new System.Drawing.Size(901, 304);
            this.pn_CaiDat_Video.TabIndex = 58;
            // 
            // gridControl2
            // 
            this.gridControl2.AllowDrop = true;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(2, 2);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.bnt_Play,
            this.btn_stop});
            this.gridControl2.Size = new System.Drawing.Size(897, 265);
            this.gridControl2.TabIndex = 55;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView2.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView2.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.GroupRow.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 30;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.TENHINH,
            this.gridColumn10,
            this.gridColumn11,
            this.CHON,
            this.MAHINH});
            this.gridView2.FooterPanelHeight = 30;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupFormat = "";
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.ShowCloseButton = false;
            this.gridView2.OptionsView.ShowGroupedColumns = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.RowHeight = 30;
            this.gridView2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.Default;
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            // 
            // STT
            // 
            this.STT.AppearanceCell.Options.UseTextOptions = true;
            this.STT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT.AppearanceHeader.Options.UseFont = true;
            this.STT.AppearanceHeader.Options.UseTextOptions = true;
            this.STT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowMove = false;
            this.STT.OptionsColumn.FixedWidth = true;
            this.STT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            this.STT.Width = 100;
            // 
            // TENHINH
            // 
            this.TENHINH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TENHINH.AppearanceHeader.Options.UseFont = true;
            this.TENHINH.AppearanceHeader.Options.UseTextOptions = true;
            this.TENHINH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TENHINH.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TENHINH.Caption = "Tên";
            this.TENHINH.FieldName = "TENHINH";
            this.TENHINH.Name = "TENHINH";
            this.TENHINH.OptionsColumn.AllowEdit = false;
            this.TENHINH.OptionsColumn.AllowFocus = false;
            this.TENHINH.Visible = true;
            this.TENHINH.VisibleIndex = 1;
            this.TENHINH.Width = 495;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn10.AppearanceCell.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn10.AppearanceHeader.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "Xem ";
            this.gridColumn10.ColumnEdit = this.bnt_Play;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.FixedWidth = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            this.gridColumn10.Width = 100;
            // 
            // bnt_Play
            // 
            this.bnt_Play.AutoHeight = false;
            this.bnt_Play.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("bnt_Play.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.bnt_Play.Name = "bnt_Play";
            this.bnt_Play.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.bnt_Play.Click += new System.EventHandler(this.bnt_Play_Click);
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn11.AppearanceCell.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn11.AppearanceHeader.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "Thoát";
            this.gridColumn11.ColumnEdit = this.btn_stop;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.FixedWidth = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 3;
            this.gridColumn11.Width = 100;
            // 
            // btn_stop
            // 
            this.btn_stop.AutoHeight = false;
            this.btn_stop.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btn_stop.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // CHON
            // 
            this.CHON.AppearanceCell.Options.UseTextOptions = true;
            this.CHON.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHON.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHON.AppearanceHeader.Options.UseFont = true;
            this.CHON.AppearanceHeader.Options.UseTextOptions = true;
            this.CHON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CHON.Caption = "Chọn";
            this.CHON.FieldName = "CHON";
            this.CHON.Name = "CHON";
            this.CHON.OptionsColumn.FixedWidth = true;
            this.CHON.Visible = true;
            this.CHON.VisibleIndex = 4;
            this.CHON.Width = 100;
            // 
            // MAHINH
            // 
            this.MAHINH.Caption = "gridColumn1";
            this.MAHINH.FieldName = "MAHINH";
            this.MAHINH.Name = "MAHINH";
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.btnbochonvideo);
            this.panelControl3.Controls.Add(this.btnChonTatCaVideo);
            this.panelControl3.Controls.Add(this.btn_XoaVideo);
            this.panelControl3.Controls.Add(this.btn_ThemVideo);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl3.Location = new System.Drawing.Point(2, 267);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(897, 35);
            this.panelControl3.TabIndex = 60;
            // 
            // btnbochonvideo
            // 
            this.btnbochonvideo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbochonvideo.Appearance.Options.UseFont = true;
            this.btnbochonvideo.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnbochonvideo.Image = global::KP_RES.Properties.Resources.restart_26;
            this.btnbochonvideo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnbochonvideo.Location = new System.Drawing.Point(387, 0);
            this.btnbochonvideo.Margin = new System.Windows.Forms.Padding(4);
            this.btnbochonvideo.Name = "btnbochonvideo";
            this.btnbochonvideo.Size = new System.Drawing.Size(129, 35);
            this.btnbochonvideo.TabIndex = 56;
            this.btnbochonvideo.Text = "Bỏ chọn";
            this.btnbochonvideo.Click += new System.EventHandler(this.btnbochonvideo_Click);
            // 
            // btnChonTatCaVideo
            // 
            this.btnChonTatCaVideo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonTatCaVideo.Appearance.Options.UseFont = true;
            this.btnChonTatCaVideo.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnChonTatCaVideo.Image = ((System.Drawing.Image)(resources.GetObject("btnChonTatCaVideo.Image")));
            this.btnChonTatCaVideo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnChonTatCaVideo.Location = new System.Drawing.Point(258, 0);
            this.btnChonTatCaVideo.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonTatCaVideo.Name = "btnChonTatCaVideo";
            this.btnChonTatCaVideo.Size = new System.Drawing.Size(129, 35);
            this.btnChonTatCaVideo.TabIndex = 57;
            this.btnChonTatCaVideo.Text = "Chọn tất cả";
            this.btnChonTatCaVideo.Click += new System.EventHandler(this.btnChonTatCaVideo_Click);
            // 
            // btn_XoaVideo
            // 
            this.btn_XoaVideo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_XoaVideo.Appearance.Options.UseFont = true;
            this.btn_XoaVideo.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_XoaVideo.Image = global::KP_RES.Properties.Resources.delete_file_26;
            this.btn_XoaVideo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_XoaVideo.Location = new System.Drawing.Point(129, 0);
            this.btn_XoaVideo.Margin = new System.Windows.Forms.Padding(4);
            this.btn_XoaVideo.Name = "btn_XoaVideo";
            this.btn_XoaVideo.Size = new System.Drawing.Size(129, 35);
            this.btn_XoaVideo.TabIndex = 54;
            this.btn_XoaVideo.Text = "Xóa video";
            this.btn_XoaVideo.Click += new System.EventHandler(this.btn_XoaVideo_Click);
            // 
            // btn_ThemVideo
            // 
            this.btn_ThemVideo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ThemVideo.Appearance.Options.UseFont = true;
            this.btn_ThemVideo.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_ThemVideo.Image = global::KP_RES.Properties.Resources.slr_camera_26;
            this.btn_ThemVideo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_ThemVideo.Location = new System.Drawing.Point(0, 0);
            this.btn_ThemVideo.Margin = new System.Windows.Forms.Padding(4);
            this.btn_ThemVideo.Name = "btn_ThemVideo";
            this.btn_ThemVideo.Size = new System.Drawing.Size(129, 35);
            this.btn_ThemVideo.TabIndex = 52;
            this.btn_ThemVideo.Text = "Thêm video";
            this.btn_ThemVideo.Click += new System.EventHandler(this.btn_ThemVideo_Click);
            // 
            // pnChon
            // 
            this.pnChon.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnChon.Controls.Add(this.btnHuyAnh);
            this.pnChon.Controls.Add(this.panelControl8);
            this.pnChon.Controls.Add(this.btn_Huy);
            this.pnChon.Controls.Add(this.btn_chuyen);
            this.pnChon.Controls.Add(this.panelControl14);
            this.pnChon.Controls.Add(this.btnRefesh);
            this.pnChon.Controls.Add(this.btnBanphim);
            this.pnChon.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnChon.Location = new System.Drawing.Point(903, 2);
            this.pnChon.Name = "pnChon";
            this.pnChon.Size = new System.Drawing.Size(45, 737);
            this.pnChon.TabIndex = 60;
            // 
            // btnHuyAnh
            // 
            this.btnHuyAnh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuyAnh.Appearance.Options.UseFont = true;
            this.btnHuyAnh.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnHuyAnh.Image = global::KP_RES.Properties.Resources.up44_26;
            this.btnHuyAnh.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnHuyAnh.Location = new System.Drawing.Point(0, 327);
            this.btnHuyAnh.Margin = new System.Windows.Forms.Padding(4);
            this.btnHuyAnh.Name = "btnHuyAnh";
            this.btnHuyAnh.Size = new System.Drawing.Size(45, 128);
            this.btnHuyAnh.TabIndex = 61;
            this.btnHuyAnh.ToolTipTitle = "Xóa ảnh";
            this.btnHuyAnh.Click += new System.EventHandler(this.btnHuyAnh_Click);
            // 
            // panelControl8
            // 
            this.panelControl8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl8.Location = new System.Drawing.Point(0, 292);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(45, 35);
            this.panelControl8.TabIndex = 60;
            // 
            // btn_Huy
            // 
            this.btn_Huy.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Huy.Appearance.Options.UseFont = true;
            this.btn_Huy.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_Huy.Image = ((System.Drawing.Image)(resources.GetObject("btn_Huy.Image")));
            this.btn_Huy.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_Huy.Location = new System.Drawing.Point(0, 164);
            this.btn_Huy.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Huy.Name = "btn_Huy";
            this.btn_Huy.Size = new System.Drawing.Size(45, 128);
            this.btn_Huy.TabIndex = 55;
            this.btn_Huy.ToolTipTitle = "Xóa hết ảnh trong quầy";
            this.btn_Huy.Click += new System.EventHandler(this.btn_Huy_Click);
            // 
            // btn_chuyen
            // 
            this.btn_chuyen.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_chuyen.Appearance.Options.UseFont = true;
            this.btn_chuyen.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_chuyen.Image = global::KP_RES.Properties.Resources.fast_forward_26;
            this.btn_chuyen.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_chuyen.Location = new System.Drawing.Point(0, 36);
            this.btn_chuyen.Margin = new System.Windows.Forms.Padding(4);
            this.btn_chuyen.Name = "btn_chuyen";
            this.btn_chuyen.Size = new System.Drawing.Size(45, 128);
            this.btn_chuyen.TabIndex = 54;
            this.btn_chuyen.ToolTipTitle = "chuyển về máy trạm";
            this.btn_chuyen.Click += new System.EventHandler(this.btn_chuyen_Click);
            // 
            // panelControl14
            // 
            this.panelControl14.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl14.Location = new System.Drawing.Point(0, 0);
            this.panelControl14.Name = "panelControl14";
            this.panelControl14.Size = new System.Drawing.Size(45, 36);
            this.panelControl14.TabIndex = 65;
            // 
            // btnRefesh
            // 
            this.btnRefesh.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnRefesh.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefesh.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnRefesh.Appearance.Options.UseBackColor = true;
            this.btnRefesh.Appearance.Options.UseFont = true;
            this.btnRefesh.Appearance.Options.UseForeColor = true;
            this.btnRefesh.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnRefesh.Image = global::KP_RES.Properties.Resources.refresh_26;
            this.btnRefesh.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnRefesh.Location = new System.Drawing.Point(0, 599);
            this.btnRefesh.Margin = new System.Windows.Forms.Padding(4);
            this.btnRefesh.Name = "btnRefesh";
            this.btnRefesh.Size = new System.Drawing.Size(45, 69);
            this.btnRefesh.TabIndex = 57;
            this.btnRefesh.Click += new System.EventHandler(this.btnRefesh_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanphim.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnBanphim.Appearance.Options.UseBackColor = true;
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Appearance.Options.UseForeColor = true;
            this.btnBanphim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBanphim.Image = global::KP_RES.Properties.Resources.keyboard_32x32;
            this.btnBanphim.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBanphim.Location = new System.Drawing.Point(0, 668);
            this.btnBanphim.Margin = new System.Windows.Forms.Padding(4);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(45, 69);
            this.btnBanphim.TabIndex = 56;
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // pn_CuaHang
            // 
            this.pn_CuaHang.Controls.Add(this.panel_Sandard);
            this.pn_CuaHang.Controls.Add(this.pal_BUTTON);
            this.pn_CuaHang.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn_CuaHang.Location = new System.Drawing.Point(948, 2);
            this.pn_CuaHang.Name = "pn_CuaHang";
            this.pn_CuaHang.Size = new System.Drawing.Size(400, 737);
            this.pn_CuaHang.TabIndex = 3;
            // 
            // panel_Sandard
            // 
            this.panel_Sandard.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panel_Sandard.Controls.Add(this.panelControl5);
            this.panel_Sandard.Controls.Add(this.panelControl4);
            this.panel_Sandard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Sandard.Location = new System.Drawing.Point(2, 2);
            this.panel_Sandard.Name = "panel_Sandard";
            this.panel_Sandard.Size = new System.Drawing.Size(396, 698);
            this.panel_Sandard.TabIndex = 3;
            // 
            // panelControl5
            // 
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl5.Controls.Add(this.pn_ChiTietQuay);
            this.panelControl5.Controls.Add(this.panelControl12);
            this.panelControl5.Controls.Add(this.pnQuay);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(0, 34);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(396, 664);
            this.panelControl5.TabIndex = 61;
            // 
            // pn_ChiTietQuay
            // 
            this.pn_ChiTietQuay.Controls.Add(this.gvChiTietQuay);
            this.pn_ChiTietQuay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_ChiTietQuay.Location = new System.Drawing.Point(0, 291);
            this.pn_ChiTietQuay.Name = "pn_ChiTietQuay";
            this.pn_ChiTietQuay.Size = new System.Drawing.Size(396, 373);
            this.pn_ChiTietQuay.TabIndex = 58;
            // 
            // gvChiTietQuay
            // 
            this.gvChiTietQuay.AllowDrop = true;
            this.gvChiTietQuay.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.RelationName = "Level1";
            this.gvChiTietQuay.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gvChiTietQuay.Location = new System.Drawing.Point(2, 2);
            this.gvChiTietQuay.MainView = this.gridView3;
            this.gvChiTietQuay.Name = "gvChiTietQuay";
            this.gvChiTietQuay.Size = new System.Drawing.Size(392, 369);
            this.gvChiTietQuay.TabIndex = 58;
            this.gvChiTietQuay.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView3.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView3.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView3.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView3.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView3.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView3.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView3.Appearance.GroupRow.Options.UseFont = true;
            this.gridView3.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView3.Appearance.Row.Options.UseFont = true;
            this.gridView3.ColumnPanelRowHeight = 30;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT2,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn1,
            this.gridColumn8,
            this.gridColumn9});
            this.gridView3.FooterPanelHeight = 30;
            this.gridView3.GridControl = this.gvChiTietQuay;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFind.ShowCloseButton = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.RowHeight = 30;
            this.gridView3.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView3.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView3_CustomDrawCell);
            // 
            // STT2
            // 
            this.STT2.AppearanceCell.Options.UseTextOptions = true;
            this.STT2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT2.AppearanceHeader.Options.UseFont = true;
            this.STT2.AppearanceHeader.Options.UseTextOptions = true;
            this.STT2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT2.Caption = "STT";
            this.STT2.Name = "STT2";
            this.STT2.OptionsColumn.AllowEdit = false;
            this.STT2.OptionsColumn.AllowFocus = false;
            this.STT2.OptionsColumn.AllowMove = false;
            this.STT2.OptionsColumn.FixedWidth = true;
            this.STT2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT2.Visible = true;
            this.STT2.VisibleIndex = 0;
            this.STT2.Width = 50;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "Tên";
            this.gridColumn6.FieldName = "TENHINH";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 223;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn7.AppearanceCell.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "Loại";
            this.gridColumn7.FieldName = "LOAI";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.FixedWidth = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 2;
            this.gridColumn7.Width = 100;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.FieldName = "MA_QUAY";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridColumn8.AppearanceCell.Options.UseFont = true;
            this.gridColumn8.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridColumn8.AppearanceHeader.Options.UseFont = true;
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "Chọn";
            this.gridColumn8.FieldName = "CHON";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.FixedWidth = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 3;
            this.gridColumn8.Width = 60;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "gridColumn9";
            this.gridColumn9.FieldName = "MAHINH";
            this.gridColumn9.Name = "gridColumn9";
            // 
            // panelControl12
            // 
            this.panelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl12.Controls.Add(this.simpleButton1);
            this.panelControl12.Controls.Add(this.btnChonTatCaQuay);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl12.Location = new System.Drawing.Point(0, 256);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(396, 35);
            this.panelControl12.TabIndex = 62;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Dock = System.Windows.Forms.DockStyle.Left;
            this.simpleButton1.Image = global::KP_RES.Properties.Resources.restart_26;
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton1.Location = new System.Drawing.Point(197, 0);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(197, 35);
            this.simpleButton1.TabIndex = 57;
            this.simpleButton1.Text = "Bỏ chọn";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btnChonTatCaQuay
            // 
            this.btnChonTatCaQuay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonTatCaQuay.Appearance.Options.UseFont = true;
            this.btnChonTatCaQuay.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnChonTatCaQuay.Image = ((System.Drawing.Image)(resources.GetObject("btnChonTatCaQuay.Image")));
            this.btnChonTatCaQuay.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnChonTatCaQuay.Location = new System.Drawing.Point(0, 0);
            this.btnChonTatCaQuay.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonTatCaQuay.Name = "btnChonTatCaQuay";
            this.btnChonTatCaQuay.Size = new System.Drawing.Size(197, 35);
            this.btnChonTatCaQuay.TabIndex = 58;
            this.btnChonTatCaQuay.Text = "Chọn tất cả";
            this.btnChonTatCaQuay.Click += new System.EventHandler(this.btnChonTatCaQuay_Click);
            // 
            // pnQuay
            // 
            this.pnQuay.Controls.Add(this.gvDSQUAY);
            this.pnQuay.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnQuay.Location = new System.Drawing.Point(0, 0);
            this.pnQuay.Name = "pnQuay";
            this.pnQuay.Size = new System.Drawing.Size(396, 256);
            this.pnQuay.TabIndex = 59;
            // 
            // gvDSQUAY
            // 
            this.gvDSQUAY.AllowDrop = true;
            this.gvDSQUAY.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode3.RelationName = "Level1";
            this.gvDSQUAY.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode3});
            this.gvDSQUAY.Location = new System.Drawing.Point(2, 2);
            this.gvDSQUAY.MainView = this.gridView1;
            this.gvDSQUAY.Name = "gvDSQUAY";
            this.gvDSQUAY.Size = new System.Drawing.Size(392, 252);
            this.gvDSQUAY.TabIndex = 56;
            this.gvDSQUAY.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gridView1.FooterPanelHeight = 30;
            this.gridView1.GridControl = this.gvDSQUAY;
            this.gridView1.GroupFormat = "";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.ShowCloseButton = false;
            this.gridView1.OptionsView.ShowGroupedColumns = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 30;
            this.gridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.Default;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.Click += new System.EventHandler(this.gridView1_Click);
            // 
            // STT1
            // 
            this.STT1.AppearanceCell.Options.UseTextOptions = true;
            this.STT1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STT1.AppearanceHeader.Options.UseFont = true;
            this.STT1.AppearanceHeader.Options.UseTextOptions = true;
            this.STT1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.STT1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.STT1.Caption = "STT";
            this.STT1.Name = "STT1";
            this.STT1.OptionsColumn.AllowEdit = false;
            this.STT1.OptionsColumn.AllowFocus = false;
            this.STT1.OptionsColumn.AllowMove = false;
            this.STT1.OptionsColumn.FixedWidth = true;
            this.STT1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAVE", "")});
            this.STT1.Visible = true;
            this.STT1.VisibleIndex = 0;
            this.STT1.Width = 50;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn2.Caption = "Tên";
            this.gridColumn2.FieldName = "TEN_QUAY";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 276;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "Chọn";
            this.gridColumn3.FieldName = "CHON";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.FixedWidth = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 60;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "gridColumn1";
            this.gridColumn4.FieldName = "MA_QUAY";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.cb_CuaHang);
            this.panelControl4.Controls.Add(this.labelControl1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(396, 34);
            this.panelControl4.TabIndex = 60;
            // 
            // cb_CuaHang
            // 
            this.cb_CuaHang.EditValue = " ";
            this.cb_CuaHang.Location = new System.Drawing.Point(83, 4);
            this.cb_CuaHang.Name = "cb_CuaHang";
            this.cb_CuaHang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cb_CuaHang.Properties.Appearance.Options.UseFont = true;
            this.cb_CuaHang.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cb_CuaHang.Properties.AppearanceDisabled.Options.UseFont = true;
            this.cb_CuaHang.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cb_CuaHang.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cb_CuaHang.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cb_CuaHang.Properties.AppearanceFocused.Options.UseFont = true;
            this.cb_CuaHang.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cb_CuaHang.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.cb_CuaHang.Properties.AutoComplete = false;
            this.cb_CuaHang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_CuaHang.Properties.DisplayMember = "TEN_CUAHANG";
            this.cb_CuaHang.Properties.NullText = "";
            this.cb_CuaHang.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.cb_CuaHang.Properties.PopupFormSize = new System.Drawing.Size(272, 0);
            this.cb_CuaHang.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.FrameResize;
            this.cb_CuaHang.Properties.PopupSizeable = false;
            this.cb_CuaHang.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cb_CuaHang.Properties.ValueMember = "MA_CUAHANG";
            this.cb_CuaHang.Properties.View = this.gridLookUpEdit1View;
            this.cb_CuaHang.Size = new System.Drawing.Size(305, 26);
            this.cb_CuaHang.TabIndex = 37;
            this.cb_CuaHang.EditValueChanged += new System.EventHandler(this.cb_CuaHang_EditValueChanged);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 14F);
            this.gridLookUpEdit1View.Appearance.FocusedRow.Options.UseFont = true;
            this.gridLookUpEdit1View.Appearance.SelectedRow.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridLookUpEdit1View.Appearance.SelectedRow.Options.UseFont = true;
            this.gridLookUpEdit1View.ColumnPanelRowHeight = 30;
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.MA_CUAHANG,
            this.TEN_CUAHANG});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowColumnHeaders = false;
            this.gridLookUpEdit1View.OptionsView.ShowDetailButtons = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.RowHeight = 30;
            // 
            // MA_CUAHANG
            // 
            this.MA_CUAHANG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.MA_CUAHANG.AppearanceCell.Options.UseFont = true;
            this.MA_CUAHANG.AppearanceCell.Options.UseTextOptions = true;
            this.MA_CUAHANG.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MA_CUAHANG.Caption = "MACH";
            this.MA_CUAHANG.FieldName = "MA_CUAHANG";
            this.MA_CUAHANG.Name = "MA_CUAHANG";
            this.MA_CUAHANG.OptionsColumn.AllowEdit = false;
            this.MA_CUAHANG.OptionsColumn.AllowFocus = false;
            this.MA_CUAHANG.OptionsColumn.FixedWidth = true;
            this.MA_CUAHANG.Visible = true;
            this.MA_CUAHANG.VisibleIndex = 0;
            this.MA_CUAHANG.Width = 60;
            // 
            // TEN_CUAHANG
            // 
            this.TEN_CUAHANG.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.TEN_CUAHANG.AppearanceCell.Options.UseFont = true;
            this.TEN_CUAHANG.Caption = "Tên Cửa Hàng";
            this.TEN_CUAHANG.FieldName = "TEN_CUAHANG";
            this.TEN_CUAHANG.Name = "TEN_CUAHANG";
            this.TEN_CUAHANG.OptionsColumn.AllowEdit = false;
            this.TEN_CUAHANG.OptionsColumn.AllowFocus = false;
            this.TEN_CUAHANG.Visible = true;
            this.TEN_CUAHANG.VisibleIndex = 1;
            this.TEN_CUAHANG.Width = 340;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(8, 7);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(68, 19);
            this.labelControl1.TabIndex = 36;
            this.labelControl1.Text = "Cửa hàng";
            // 
            // pal_BUTTON
            // 
            this.pal_BUTTON.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pal_BUTTON.Controls.Add(this.btn_bochonquay);
            this.pal_BUTTON.Controls.Add(this.btnChonTatCaChiTiet);
            this.pal_BUTTON.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pal_BUTTON.Location = new System.Drawing.Point(2, 700);
            this.pal_BUTTON.Name = "pal_BUTTON";
            this.pal_BUTTON.Size = new System.Drawing.Size(396, 35);
            this.pal_BUTTON.TabIndex = 2;
            // 
            // btn_bochonquay
            // 
            this.btn_bochonquay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_bochonquay.Appearance.Options.UseFont = true;
            this.btn_bochonquay.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_bochonquay.Image = global::KP_RES.Properties.Resources.restart_26;
            this.btn_bochonquay.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btn_bochonquay.Location = new System.Drawing.Point(197, 0);
            this.btn_bochonquay.Margin = new System.Windows.Forms.Padding(4);
            this.btn_bochonquay.Name = "btn_bochonquay";
            this.btn_bochonquay.Size = new System.Drawing.Size(197, 35);
            this.btn_bochonquay.TabIndex = 57;
            this.btn_bochonquay.Text = "Bỏ chọn";
            this.btn_bochonquay.Click += new System.EventHandler(this.btn_bochonquay_Click);
            // 
            // btnChonTatCaChiTiet
            // 
            this.btnChonTatCaChiTiet.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonTatCaChiTiet.Appearance.Options.UseFont = true;
            this.btnChonTatCaChiTiet.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnChonTatCaChiTiet.Image = ((System.Drawing.Image)(resources.GetObject("btnChonTatCaChiTiet.Image")));
            this.btnChonTatCaChiTiet.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnChonTatCaChiTiet.Location = new System.Drawing.Point(0, 0);
            this.btnChonTatCaChiTiet.Margin = new System.Windows.Forms.Padding(4);
            this.btnChonTatCaChiTiet.Name = "btnChonTatCaChiTiet";
            this.btnChonTatCaChiTiet.Size = new System.Drawing.Size(197, 35);
            this.btnChonTatCaChiTiet.TabIndex = 58;
            this.btnChonTatCaChiTiet.Text = "Chọn tất cả";
            this.btnChonTatCaChiTiet.Click += new System.EventHandler(this.btnChonTatCaChiTiet_Click);
            // 
            // Frm_Manhinhhai
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 741);
            this.Controls.Add(this.panelControl1);
            this.Name = "Frm_Manhinhhai";
            this.Text = "Cài đặt màn hình hai";
            this.SizeChanged += new System.EventHandler(this.Frm_QuangCao_Video_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_Hinh)).EndInit();
            this.pn_Hinh.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_xemvideo)).EndInit();
            this.pn_xemvideo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WindowsMediaPlayer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_cuon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pal_xuong)).EndInit();
            this.pal_xuong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_CaiDat_Video)).EndInit();
            this.pn_CaiDat_Video.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnt_Play)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_stop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnChon)).EndInit();
            this.pnChon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_CuaHang)).EndInit();
            this.pn_CuaHang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panel_Sandard)).EndInit();
            this.panel_Sandard.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pn_ChiTietQuay)).EndInit();
            this.pn_ChiTietQuay.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvChiTietQuay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnQuay)).EndInit();
            this.pnQuay.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvDSQUAY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cb_CuaHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pal_BUTTON)).EndInit();
            this.pal_BUTTON.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl pal_BUTTON;
        private DevExpress.XtraEditors.PanelControl pn_CuaHang;
        private DevExpress.XtraEditors.PanelControl panel_Sandard;
        private DevExpress.XtraEditors.PanelControl pal_xuong;
        private DevExpress.XtraEditors.SimpleButton bnt_tranglen;
        private DevExpress.XtraEditors.SimpleButton lbl_sotrang;
        private DevExpress.XtraEditors.SimpleButton bnt_trangxuong;
        private DevExpress.XtraEditors.PanelControl pn_Hinh;
        private DevExpress.XtraEditors.SimpleButton btn_Them;
        private DevExpress.XtraEditors.PanelControl pn_cuon;
        private DevExpress.XtraEditors.PanelControl pn_CaiDat_Video;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btn_XoaVideo;
        private DevExpress.XtraEditors.SimpleButton btn_ThemVideo;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn TENHINH;
        private DevExpress.XtraGrid.Columns.GridColumn CHON;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn MAHINH;
        private DevExpress.XtraEditors.PanelControl pnChon;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.GridLookUpEdit cb_CuaHang;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn MA_CUAHANG;
        private DevExpress.XtraGrid.Columns.GridColumn TEN_CUAHANG;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraGrid.GridControl gvDSQUAY;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn STT1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.SimpleButton btn_Huy;
        private DevExpress.XtraEditors.SimpleButton btn_chuyen;
        private DevExpress.XtraEditors.SimpleButton btn_Xoa;
        private DevExpress.XtraEditors.SimpleButton btnRefesh;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.SimpleButton btn_bochonhinh;
        private DevExpress.XtraEditors.SimpleButton btnbochonvideo;
        private DevExpress.XtraEditors.SimpleButton btn_bochonquay;
        private DevExpress.XtraEditors.SimpleButton btnHuyAnh;
        private DevExpress.XtraEditors.PanelControl pn_xemvideo;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit bnt_Play;
        private AxWMPLib.AxWindowsMediaPlayer WindowsMediaPlayer;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn_stop;
        private DevExpress.XtraEditors.PanelControl pn_ChiTietQuay;
        private DevExpress.XtraGrid.GridControl gvChiTietQuay;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn STT2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.PanelControl pnQuay;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.PanelControl panelControl14;
        private DevExpress.XtraEditors.SimpleButton btnChonTatCaHinh;
        private DevExpress.XtraEditors.SimpleButton btnChonTatCaVideo;
        private DevExpress.XtraEditors.SimpleButton btnChonTatCaQuay;
        private DevExpress.XtraEditors.SimpleButton btnChonTatCaChiTiet;




    }
}