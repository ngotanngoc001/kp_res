﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using System.IO;
using DevExpress.Metro.Navigation;
using KP_UserManagement;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace KP_RES 
{
    public partial class  Frm_Manhinhhai : DevExpress.XtraEditors.XtraForm
    {
        string []smaHinh;
        string[] smavideo;
        int trang = 1;
        DataTable dtHinh = new DataTable();
        int _tonghinhtrentrang;
        int _sohinhtheochieudai;
        int sotrang = 1;
        bool[] chooseList;
        string[] FileName;
        string[] FileNameHave;
        string[] FileNameHaveVideo;
        string[] FilePath;
        int soluonghinh;
        bool delete = true;
        bool play = true;

        public  Frm_Manhinhhai()
        {
            InitializeComponent();
            LoadData();
            LoadImage(_tonghinhtrentrang, trang);
            LoadVideo(false);
            LoadCuaHang();
        }

        private void LoadData()
        {
            try
            {
                int width =0;
                int height = 0;
                int widthScreen = Screen.PrimaryScreen.WorkingArea.Width;
                int heightScreen = Screen.PrimaryScreen.WorkingArea.Height;
                
                width = pn_cuon.Width;
                height = pn_cuon.Height;
               soluonghinh =int.Parse( clsMain.ReturnDataTable("select count(h.MAQC) from QC_HINHANH h inner join QC_QUANGCAO qc on h.MAQC=qc.MAQC where qc.MODE=0 and LOAI=0 ").Rows[0][0].ToString());
                FileNameHave = new string[soluonghinh];
                chooseList = new bool[soluonghinh];
                smaHinh=new string[soluonghinh];
                
                for (int i = 0; i < soluonghinh ; i++)
                {
                    chooseList[i] = false;
                    smaHinh[i] = "";
                }
                pn_cuon.Controls.Clear();
                int sohinhtheochieudai = width / 100;
                _sohinhtheochieudai = sohinhtheochieudai;
                int sohinhtheochieurong = height / 160;
                int tonghinhtrentrang = sohinhtheochieudai * sohinhtheochieurong;
                sotrang = soluonghinh / tonghinhtrentrang;
                if (soluonghinh % tonghinhtrentrang != 0)
                    sotrang++;
                
                _tonghinhtrentrang = tonghinhtrentrang;
               
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }//load dữ liệu ảnh,hiển thị

        private void LoadCuaHang()
        {
            string sql = "select MA_CUAHANG,TEN_CUAHANG from CUAHANG where SUDUNG=1";
            DataTable dtCH = clsMain.ReturnDataTable(sql);
            cb_CuaHang.Properties.DataSource = dtCH;
            if (dtCH.Rows.Count > 0)
            {
                cb_CuaHang.EditValue = dtCH.Rows[0][0].ToString();
                LoadQuay(cb_CuaHang.EditValue.ToString(),false);
            }

        }//load danh sách cửa hàng

        private void LoadQuay(string smaCH,bool choose)
        {
            string sql = "select MA_QUAY,TEN_QUAY,cast('"+choose+"' as bit) as CHON from QUAY q inner join KHO k on q.MA_KHO=k.MA_KHO inner join CUAHANG ch on ch.MA_CUAHANG=k.MA_CUAHANG where ch.MA_CUAHANG='" + smaCH + "'";
            DataTable dtQuay = clsMain.ReturnDataTable(sql);
            gvDSQUAY.DataSource = dtQuay;
          
            
        }//load quầy theo cửa hàn

        private void LoadChitietQuay(string sMaQuay,bool choose)
        {
            string sql = "select h.MAHINH,q.MA_QUAY,q.TEN_QUAY,h.TENHINH,cast('" + choose + "' as bit) as CHON,(case when h.LOAI=1 then N'Video' else N'Ảnh' end) as LOAI from CUAHANG ch inner join KHO k on ch.MA_CUAHANG=k.MA_CUAHANG inner join QUAY q on k.MA_KHO=q.MA_KHO inner join QC_HINHANH_QUAY qh on q.MA_QUAY=qh.MA_QUAY inner join QC_HINHANH h on qh.MAHINH=h.MAHINH inner join QC_QUANGCAO qc on qc.MAQC=h.MAQC where qc.MODE=0 and q.MA_QUAY='" + sMaQuay + "'";
            DataTable dtQuay = clsMain.ReturnDataTable(sql);
            gvChiTietQuay.DataSource = dtQuay;
        }

        private void LoadImage(int tonghinhtrentrang, int trang)
        {
            try
            {
                pn_cuon.Controls.Clear();
                int tang = 1;
                int x =  5, y =  5;
                string sqlhinh = "exec sp_CHONHINHANH_NS " + (tonghinhtrentrang * (trang - 1)) + "," + (tonghinhtrentrang * trang);
                dtHinh = clsMain.ReturnDataTable(sqlhinh);
                foreach (DataRow dr in dtHinh.Rows)
                {
                    FileNameHave[int.Parse(dr["STTHINH"].ToString())-1] = dr["TENHINH"].ToString();
                    Panel pn_Group = new Panel();
                    Panel pn_Hinh = new Panel();
                    Panel pn_Ten = new Panel();
                    Panel pn_Chon = new Panel();
                    CheckEdit checkChon = new CheckEdit();
                    LabelControl lbTen = new LabelControl();
                    PictureBox btn = new PictureBox();
                    pn_Group.BorderStyle = BorderStyle.FixedSingle;
                    checkChon.Font = btn.Font = btn_Xoa.Font;
                    lbTen.Font = new System.Drawing.Font("Tahoma", 10);
                    checkChon.Enabled = btn.Enabled = true;
                    btn.SizeMode = PictureBoxSizeMode.StretchImage;
                    btn.BorderStyle = BorderStyle.FixedSingle;
                    btn.Dock = DockStyle.Fill;
                    checkChon.Dock = DockStyle.Fill;
                    lbTen.Dock = DockStyle.Fill;
                    pn_Hinh.Dock = DockStyle.Fill;
                    pn_Ten.Dock = DockStyle.Bottom;
                    pn_Chon.Dock = DockStyle.Bottom;
                    checkChon.Text = "Chọn";
                    lbTen.Name= checkChon.Name = btn.Name = dr["STTHINH"].ToString();
                    lbTen.Tag= checkChon.Tag = btn.Tag = dr["MAHINH"].ToString();
                    pn_Group.Name = "G" + dr["STTHINH"].ToString();
                    pn_Hinh.Name = "H" + dr["STTHINH"].ToString();
                    pn_Ten.Name = "T" + dr["STTHINH"].ToString();
                    pn_Chon.Name = "C" + dr["STTHINH"].ToString();
                    checkChon.Checked = chooseList[int.Parse(dr["STTHINH"].ToString())-1];
                    lbTen.Text = dr["TENHINH"].ToString();

                     pn_Chon.Size = new Size(90, 23);
                     pn_Ten.Size = new Size(90, 20);
                    pn_Group.Size = new Size(90, 150);
                    pn_Group.Location = new Point(x, y);
                    if (tang < _sohinhtheochieudai)
                    {
                        x += 100;
                        tang++;
                    }
                    else
                    {
                        x =  5;
                        y += 160;
                        tang = 1;
                    }

                    try
                    {
                        byte[] tam = new byte[((byte[])dr["URL"]).LongLength];
                        tam = (byte[])dr["URL"];
                        MemoryStream ms = new MemoryStream(tam);
                        Bitmap bm = new Bitmap(ms);
                        btn.Image = bm;
                    }
                    catch
                    {
                    }
                    btn.Click += new System.EventHandler(bnt_hinh_Click);
                    checkChon.CheckedChanged += new System.EventHandler(checkChon_CheckedChanged);
                    lbTen.Click += new System.EventHandler(bnt_hinh_Click);
               
                    pn_Hinh.Controls.Add(btn);
                    pn_Ten.Controls.Add(lbTen);
                    pn_Chon.Controls.Add(checkChon);

                    pn_Group.Controls.Add(pn_Hinh);
                    pn_Group.Controls.Add(pn_Ten);
                    pn_Group.Controls.Add(pn_Chon);
                    pn_cuon.Controls.Add(pn_Group);
                  
                }
                lbl_sotrang.Text = trang + "/" + sotrang;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }//load ảnh hiển thị 

        private void LoadVideo(bool choose)
        {
            string sql = "select MAHINH,TENHINH,cast('"+choose+"' as bit) as CHON from QC_HINHANH h inner join QC_QUANGCAO qc on h.MAQC=qc.MAQC where qc.MODE=0 and LOAI=1";
            DataTable dt = clsMain.ReturnDataTable(sql);
            gridControl2.DataSource = dt;
            FileNameHaveVideo = new string[dt.Rows.Count];
            smavideo = new string[dt.Rows.Count];
            for(int i=0;i<dt.Rows.Count;i++)
            {
                FileNameHaveVideo[i]=dt.Rows[i]["TENHINH"].ToString();
                if (!choose)
                {
                    smavideo[i] = "";
                }
                else
                {
                    smavideo[i] = dt.Rows[i]["MAHINH"].ToString();
                }
            }
        }//load tất cả video

        private bool checktenhinh(string name)
        {
            if (FileNameHave.Length != 0)
            {
                for (int i = 0; i < FileNameHave.Length; i++)
                {
                    if (name == FileNameHave[i])
                        return false;

                }
            }
            return true;
        }//kiểm tra 1 tên đã có trong danh sách hình chưa

        private bool checktenvideo(string name)
        {
            if (FileNameHaveVideo.Length != 0)
            {
                for (int i = 0; i < FileNameHaveVideo.Length; i++)
                {
                    if (name == FileNameHaveVideo[i])
                        return false;

                }
            }
            return true;
        }//kiểm tra 1 tên đã có trong danh sách video chưa

        private void btnRefesh_Click(object sender, EventArgs e)
        {
            LoadData();
            LoadImage(_tonghinhtrentrang, trang);
            LoadVideo(false);
            LoadCuaHang();
            
        }//refresh lại dữ liêu

        private void bnt_tranglen_Click(object sender, EventArgs e)
        {
            if (trang > 1)
            {
                trang--;
                LoadImage(_tonghinhtrentrang,trang);
            }
        }//click vào nút lên

        private void bnt_trangxuong_Click(object sender, EventArgs e)
        {
            if (trang < sotrang)
            {
                trang++;
                LoadImage(_tonghinhtrentrang, trang);
            }
        }//click vào nút xuống

        private void bnt_hinh_Click(object sender, EventArgs e)
        {
            Control btn = (Control)sender;
            //int i=1;
            chooseList[int.Parse(btn.Name) - 1] = !chooseList[int.Parse(btn.Name) - 1];
            foreach (Panel pn in pn_cuon.Controls)
            {
                if (pn.Name == ("G" + btn.Name))
                {
                    foreach (Panel pn1 in pn.Controls)
                    {
                        if (pn1.Name == ("C" + btn.Name))
                        {
                            foreach (CheckEdit chk in pn1.Controls)
                            {
                                chk.Checked = chooseList[int.Parse(btn.Name) - 1];
                                //break;
                            }
                            break;
                        }

                    }
                    break;
                }

            }
           
        }//sự kiện click vào hình

        private void pn_cuon_SizeChanged(object sender, EventArgs e)
        {
            LoadData();
            LoadImage(_tonghinhtrentrang, 1);
            trang = 1;
        }//xử ly sự kiện pannel chứa ảnh thay đổi kích thước

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }//click bàn phím
       
        private void btn_Them_Click(object sender, EventArgs e)
        {
            string smaQC = "";
            for (int i = 0; i < soluonghinh; i++)
            {
                chooseList[i] = false;
                smaHinh[i] = "";
            }
            DataTable dt = clsMain.ReturnDataTable("select MAQC from QC_QUANGCAO Where MODE=0");
            if (dt.Rows.Count > 0)
            {

                smaQC = dt.Rows[0][0].ToString();
            }
            else
            {
                string sqlinsert = "insert QC_QUANGCAO(VITRI,RUNTIMEIMAGE,TEXTCHU,RUNTIMETEXT,MODE) values('0','1','','1','0')";
                clsMain.ExecuteSQL(sqlinsert);
                dt = clsMain.ReturnDataTable("select MAQC from QC_QUANGCAO Where MODE=0");
                smaQC = dt.Rows[0][0].ToString();
            }
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = Application.StartupPath + "\\Images";
            openFileDialog1.Filter = "Image Files|*.JPG;*.bmp;*.png;*.gif|All Files|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.Multiselect = true;

            openFileDialog1.Title = "Chọn ảnh";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                
                FileName = openFileDialog1.SafeFileNames;

                FilePath = openFileDialog1.FileNames;

                this.Cursor = Cursors.WaitCursor;

                for (int i = 0; i <FileName.Length ; i++)
                {
                    //picter.ImageLocation = FilePath[i];
                    FileInfo fileInfo = new FileInfo(FilePath[i]);
                    long size = fileInfo.Length;
                    if (size > 500000)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Giới hạn dung lượng hình phải nhỏ hơn 500kb", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        continue;
                    }

                    if (!checktenhinh(FileName[i]))
                    {
                        XtraMessageBox.Show("Hình "+FileName[i].ToUpper()+" đã có trong danh sách ảnh", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        continue;
                    }
                    try
                    {
                        Image img = Image.FromFile(FilePath[i]);
                   
                        Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(img, typeof(Byte[]));
                        SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
                        SqlCommand SqlComMain = null;
                        string sql = "";
                        int stt = 1;
                        DataTable dtSTT = clsMain.ReturnDataTable("select top(1) h.STT from QC_HINHANH h inner join QC_QUANGCAO qc on h.MAQC=qc.MAQC where qc.MODE=0 and LOAI=0 order by STT desc");
                        if (dtSTT.Rows.Count > 0)
                        {
                            stt = int.Parse(dtSTT.Rows[0][0].ToString()) + 1;
                        }
                        sql += "INSERT INTO QC_HINHANH( TENHINH,URL,MAQC,SUDUNG,GHICHU,STT,LOAI) values ";
                        sql += "(@TENHINH,@URL,@MAQC,@SUDUNG,@GHICHU,@STT,@LOAI)" + "\n";
                        SqlCommand SqlCom = new SqlCommand(sql, CN);
                        SqlCom.Parameters.Add(new SqlParameter("@TENHINH", (object)FileName[i]));
                        SqlCom.Parameters.Add(new SqlParameter("@URL", (object)imageData));
                        SqlCom.Parameters.Add(new SqlParameter("@MAQC", (object)smaQC));
                        SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)true));
                        SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)""));
                        SqlCom.Parameters.Add(new SqlParameter("@STT", (object)stt));
                        SqlCom.Parameters.Add(new SqlParameter("@LOAI", (object)0));
                        SqlComMain = SqlCom;
                        CN.Open();
                        SqlComMain.ExecuteNonQuery();
                        CN.Close();
                    }
                    catch
                    {
                        continue;
                    }
                }
                this.Cursor = Cursors.Default;
                LoadData();
                 trang = sotrang;
                LoadImage(_tonghinhtrentrang, sotrang);
               // btn_bochonhinh_Click(null, null);
            }
               
        }//thêm ảnh vào danh sách ảnh

        private bool checkChoose()//hàm kiểm tra xem có hình nào được chọn không
        {
            for (int i = 0; i < soluonghinh; i++)
            {
                if (smaHinh[i] != "")
                    return true;
            }
            return false;
        }

        private void btn_Xoa_Click(object sender, EventArgs e)
        {
            try
            {
                string chuoi = "";
                if (!checkChoose())
                    throw new Exception("Bạn Chưa chọn hình cần xóa");
                for (int i = 0; i < soluonghinh; i++)
                {
                    if (smaHinh[i] != "")
                        chuoi += smaHinh[i]+",";
                }
                if (chuoi != "")
                    chuoi = chuoi.Remove(chuoi.Length-1);
                string sql = "delete from QC_HINHANH where MAHINH IN (" + chuoi + ")";
               
                if (clsMain.ExecuteSQL(sql))
                {
                    
                    LoadData();
                    if (trang > sotrang)
                    {
                        LoadImage(_tonghinhtrentrang, sotrang);
                        trang = sotrang;
                    }
                    else
                    {
                        LoadImage(_tonghinhtrentrang, trang);
                    }
                    btn_bochonhinh_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }//xóa ảnh khỏi danh sách

        private void gridView2_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }//stt lưới

        private void btn_ThemVideo_Click(object sender, EventArgs e)
        {
            try
            {
                string smaQC = "";

                DataTable dt = clsMain.ReturnDataTable("select MAQC from QC_QUANGCAO Where MODE=0");
                if (dt.Rows.Count > 0)
                {

                    smaQC = dt.Rows[0][0].ToString();
                }
                else
                {
                    string sqlinsert = "insert QC_QUANGCAO(VITRI,RUNTIMEIMAGE,TEXTCHU,RUNTIMETEXT,MODE) values('0','1','','1','0')";
                    clsMain.ExecuteSQL(sqlinsert);
                    dt = clsMain.ReturnDataTable("select MAQC from QC_QUANGCAO Where MODE=0");
                    smaQC = dt.Rows[0][0].ToString();
                }

                OpenFileDialog opnFileDlg = new OpenFileDialog();
                
                opnFileDlg.Filter = "(mp3,wav,mp4,mov,wmv,mpg,avi,3gp,flv)|*.mp3;*.wav;*.mp4;*.3gp;*.avi;*.mov;*.flv;*.wmv;*.mpg|all files|*.*";
                opnFileDlg.FilterIndex = 1;
                opnFileDlg.RestoreDirectory = true;
                opnFileDlg.Multiselect = false;

                opnFileDlg.Title = "Chọn video";
                if (opnFileDlg.ShowDialog() == DialogResult.OK)
                {
                    FileName = opnFileDlg.SafeFileNames;
                    FilePath = opnFileDlg.FileNames;
                    this.Cursor = Cursors.WaitCursor;

                    for (int i = 0; i <= FileName.Length - 1; i++)
                    {
                        if(!checktenvideo(FileName[i]))
                        {
                            throw new Exception("video " + FileName[i] .ToUpper()+ " đã có trong dữ liệu,bạn không cần thêm nữa");
                        }
                        FileStream stream = File.OpenRead(FilePath[i]);
                        if (stream.Length > 210000000)
                            throw new Exception("kích thước video không được lớn hơn 200MB,vui lòng chọn video khác");
                        byte[] fileBytes = new byte[stream.Length];
                        
                        stream.Read(fileBytes, 0, fileBytes.Length);
                        stream.Close();


                        SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
                        SqlCommand SqlComMain = null;
                        string sql = "";
                        int stt = 1;
                        DataTable dtSTT = clsMain.ReturnDataTable("select top(1) h.STT from QC_HINHANH h inner join QC_QUANGCAO qc on h.MAQC=qc.MAQC where qc.MODE=0 and LOAI=1 order by STT desc");
                        if (dtSTT.Rows.Count > 0)
                        {
                            stt = int.Parse(dtSTT.Rows[0][0].ToString()) + 1;
                        }
                        sql += "INSERT INTO QC_HINHANH( TENHINH,URL,MAQC,SUDUNG,GHICHU,STT,LOAI) values ";
                        sql += "(@TENHINH,@URL,@MAQC,@SUDUNG,@GHICHU,@STT,@LOAI)" + "\n";
                        SqlCommand SqlCom = new SqlCommand(sql, CN);
                        SqlCom.Parameters.Add(new SqlParameter("@TENHINH", (object)FileName[i]));
                        SqlCom.Parameters.Add(new SqlParameter("@URL", (object)fileBytes));
                        SqlCom.Parameters.Add(new SqlParameter("@MAQC", (object)smaQC));
                        SqlCom.Parameters.Add(new SqlParameter("@SUDUNG", (object)true));
                        SqlCom.Parameters.Add(new SqlParameter("@GHICHU", (object)""));
                        SqlCom.Parameters.Add(new SqlParameter("@STT", (object)stt));
                        SqlCom.Parameters.Add(new SqlParameter("@LOAI", (object)1));
                        SqlComMain = SqlCom;
                        CN.Open();
                        SqlComMain.ExecuteNonQuery();
                        CN.Close();
                    }
                    LoadVideo(false);
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }//thêm video vào danh sách

        private bool checkchon()
        {
            for (int i = 0; i < gridView2.RowCount; i++)
            {
                gridView2.FocusedRowHandle = i;
                if (bool.Parse(gridView2.GetFocusedRowCellValue("CHON").ToString()) == true)
                {
                    return true;
                }
            }
            return false;
        }//kiểm tra xem có video nào được chọn hay chưa

        private void setronglistvideo()
        {
            for (int i = 0; i < gridView2.RowCount; i++)
            {
                gridView2.FocusedRowHandle = i;
                smavideo[i] = "";
            }
        } //bỏ chọn video

        private void listchonvideo()
        {
            for (int i = 0; i < gridView2.RowCount; i++)
            {
                gridView2.FocusedRowHandle = i;
                if (bool.Parse(gridView2.GetFocusedRowCellValue("CHON").ToString()) == true)
                {
                    smavideo[i]= gridView2.GetFocusedRowCellValue("MAHINH").ToString();
                }
            }
          
        }//lấy tất cả mã video đã dc chọn

        private void btn_XoaVideo_Click(object sender, EventArgs e)
        {
            try
            {
                string videoIDlist = "";
                if (gridView2.RowCount == 0)
                {
                    return;
                }
                if (!checkchon())
                {
                    throw new Exception("Bạn chưa chọn video cần xóa");
                }
                this.Cursor = Cursors.WaitCursor;
                listchonvideo();
                for (int i = 0; i < smavideo.Length; i++)
                {
                    if (smavideo[i] != "")
                        videoIDlist += smavideo[i]+",";
                }
                if (videoIDlist != "")
                {
                    videoIDlist = videoIDlist.Remove(videoIDlist.Length - 1);
                    clsMain.ExecuteSQL("delete from QC_HINHANH where LOAI=1 and MAHINH in("+videoIDlist+")");
                    LoadVideo(false);
                }
               
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }//xóa video

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT1 & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        } //stt lưới

        private void cb_CuaHang_EditValueChanged(object sender, EventArgs e)
        {
            LoadQuay(cb_CuaHang.EditValue.ToString(),false);
            LoadChitietQuay("",false);
        }//sự kiện thay đổi cbo cửa hàng

        private bool checkChooseQuay()
        {
            for (int i = 0; i < gridView1.RowCount; i++)
            {
                gridView1.FocusedRowHandle = i;
                if(bool.Parse(gridView1.GetFocusedRowCellValue(CHON).ToString()))
                    return true;
            }
            return false;
        }//kiểm tra xem có quầy nào được chọn hay không

        private bool checkChooseanhquay()
        {
            for (int i = 0; i < gridView3.RowCount; i++)
            {
                gridView3.FocusedRowHandle = i;
                if (bool.Parse(gridView3.GetFocusedRowCellValue(CHON).ToString()))
                    return true;
            }
            return false;
        }//kiểm tra xem có quầy nào được chọn hay không

        private void xoavideoimage()
        {
            string smaquay = "";
            for (int i = 0; i < gridView1.RowCount; i++)
            {
                gridView1.FocusedRowHandle = i;
                if (bool.Parse(gridView1.GetFocusedRowCellValue(CHON).ToString()))
                    smaquay += gridView1.GetFocusedRowCellValue("MA_QUAY").ToString() + ",";
            }
            if (smaquay != "")
            {
                smaquay = smaquay.Remove(smaquay.Length - 1);
                string sql = "delete from QC_HINHANH_QUAY where MA_QUAY in(" + smaquay + ")";
                clsMain.ExecuteSQL(sql);
                    
            }
        }//hàm xóa các video,hinh trong các quầy đã được chọn

        private void xoavideoimage1()
        {
            for (int i = 0; i < gridView3.RowCount; i++)
            {
                gridView3.FocusedRowHandle = i;
                if (bool.Parse(gridView3.GetFocusedRowCellValue(CHON).ToString()))
                {
                    string sql = "delete from QC_HINHANH_QUAY where MA_QUAY =" + gridView3.GetFocusedRowCellValue("MA_QUAY").ToString() + " and MAHINH=" + gridView3.GetFocusedRowCellValue("MAHINH").ToString() + "";
                    clsMain.ExecuteSQL(sql);
                    
                }
            }
          
        }//hàm xóa các video,ảnh đã được chọn

        private void btn_Huy_Click(object sender, EventArgs e)
        {
            int vitri = gridView1.FocusedRowHandle;
            try
            {

                if (gridView1.RowCount == 0)
                    throw new Exception("Bạn Chưa Chọn Quầy Cần Hủy");
                if (!checkChooseQuay())
                    throw new Exception("Bạn Chưa Chọn quầy");
                xoavideoimage();
                gridView1.FocusedRowHandle = vitri;
                if (gridView1.FocusedRowHandle >= 0)
                    LoadChitietQuay(gridView1.GetFocusedRowCellValue("MA_QUAY").ToString(), false);
                simpleButton1_Click(null, null);
                throw new Exception("xóa thành công!");
            }
            catch (Exception ex)
            {
                gridView1.FocusedRowHandle = vitri;
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                gridView1.FocusedRowHandle = vitri;
            }
        }//click vào nút xóa tất cả video

        private bool checkTonTai(string maquay, string mahinhvieo)
        {
            DataTable dtcheck = clsMain.ReturnDataTable("select MA_QUAY from QC_HINHANH_QUAY where  MA_QUAY='"+maquay+"' and MAHINH='"+mahinhvieo+"'");
            if (dtcheck.Rows.Count > 0)
                return true;
            return false;
        }//hàm kiểm tra xem hình anh ,video đã có trong quầy này chưa,true là có false là chưa

        private void btn_chuyen_Click(object sender, EventArgs e)
        {
            int vitri=gridView1.FocusedRowHandle;
            try
            {
                string sql = "";
                if (gridView1.RowCount == 0)
                    throw new Exception("Bạn Chưa Chọn Quầy Cần Thêm");
                if (!checkChooseQuay())
                    throw new Exception("Bạn Chưa Chọn quầy");
                if(checkChoose()==false && checkchon()==false)
                    throw new Exception("Bạn Chưa Chọn video,hình ảnh nào để thêm");
                setronglistvideo();
                listchonvideo();
                for (int i = 0; i < gridView1.RowCount; i++)//duyệt quầy
                {
                    gridView1.FocusedRowHandle = i;
                    if (bool.Parse(gridView1.GetFocusedRowCellValue(CHON).ToString()))//nếu quầy được chọn
                    {
                        for (int j = 0; j < smaHinh.Length; j++)//thêm hình cho quầy
                        {
                            if (smaHinh[j] != "")
                            {
                                if (!checkTonTai(gridView1.GetFocusedRowCellValue("MA_QUAY").ToString(), smaHinh[j]))
                                    sql += "insert into QC_HINHANH_QUAY(MA_QUAY,MAHINH,DOWN) values (" + gridView1.GetFocusedRowCellValue("MA_QUAY").ToString() + "," + smaHinh[j] + ",0);";
                            }
                        }
                        for (int k = 0; k < smavideo.Length; k++)//thêm video cho quầy
                        {
                            if (smavideo[k] != "")
                            {
                                if (!checkTonTai(gridView1.GetFocusedRowCellValue("MA_QUAY").ToString(), smavideo[k]))
                                    sql += "insert into QC_HINHANH_QUAY(MA_QUAY,MAHINH,DOWN) values (" + gridView1.GetFocusedRowCellValue("MA_QUAY").ToString() + "," + smavideo[k] + ",0);";
                            }
                        }
                    }
                }
                clsMain.ExecuteSQL(sql);
                gridView1.FocusedRowHandle = vitri;
                if (gridView1.FocusedRowHandle >= 0)
                    LoadChitietQuay(gridView1.GetFocusedRowCellValue("MA_QUAY").ToString(), false);
                btn_bochonhinh_Click(null, null);
                btnbochonvideo_Click(null, null);
                simpleButton1_Click(null, null);
                btn_bochonquay_Click(null, null);
                throw new Exception("Thêm thành công!");
            }
            catch (Exception ex)
            {
                gridView1.FocusedRowHandle = vitri;
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }//chuyển video ,ảnh vào quầy

        private void LoadALLMAHINH()
        {
            DataTable dtMH = clsMain.ReturnDataTable("Select MAHINH from QC_HINHANH h inner join QC_QUANGCAO qc on h.MAQC=qc.MAQC where qc.MODE=0 and h.LOAI=0");
            for (int i = 0; i < dtMH.Rows.Count; i++)
            {
                smaHinh[i] = dtMH.Rows[i][0].ToString();
            }
        }//lấy tất cả mã hình

        private void btn_bochonhinh_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < soluonghinh; i++)
            {
                chooseList[i] = false;
                smaHinh[i] = "";

            }
            LoadImage(_tonghinhtrentrang, trang);
            
        }//bỏ chọn hình

        private void btnbochonvideo_Click(object sender, EventArgs e)
        {
            LoadVideo(false);
            
        }//bỏ chọn video

        private void btn_bochonquay_Click(object sender, EventArgs e)
        {
            if(gridView3.RowCount>0)
                LoadChitietQuay(gridView3.GetFocusedRowCellValue("MA_QUAY").ToString(), false);
           
        }//bỏ chọn quầy

        private void gridView3_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == STT2 & e.RowHandle >= 0)
                e.DisplayText = Convert.ToString(e.RowHandle + 1);
        }

        private void btnHuyAnh_Click(object sender, EventArgs e)
        {
            int vitri = gridView1.FocusedRowHandle;
            try
            {

                if (gridView3.RowCount == 0)
                    throw new Exception("Bạn Chưa Chọn Ảnh Cần Hủy");
                if (!checkChooseanhquay())
                    throw new Exception("Bạn Chưa Chọn Ảnh Cần Hủy");
                xoavideoimage1();
                if (gridView1.FocusedRowHandle >= 0)
                    LoadChitietQuay(gridView1.GetFocusedRowCellValue("MA_QUAY").ToString(), false);
                throw new Exception("xóa thành công!");
            }
            catch (Exception ex)
            {
                gridView1.FocusedRowHandle = vitri;
                XtraMessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        
        private void bnt_Play_Click(object sender, EventArgs e)
        {
            try
            {
                pn_cuon.Visible = false;
                //pn_xemvideo.Visible = !pn_xemvideo.Visible;

                if (play)
                {
                    play = false;
                    this.Cursor = Cursors.WaitCursor;
                    DataTable dt = clsMain.ReturnDataTable("Select TENHINH,URL from QC_HINHANH  where LOAI=1 and MAHINH='" + gridView2.GetFocusedRowCellValue("MAHINH").ToString() + "'");

                    //Begins the process of writing the byte array back to a file
                    byte[] fileBytes = new byte[((byte[])dt.Rows[0]["URL"]).LongLength];
                    fileBytes = (byte[])dt.Rows[0]["URL"];
                    Stream file = File.OpenWrite(Application.StartupPath + "/QCXEM/" + dt.Rows[0]["TENHINH"].ToString());
                    file.Write(fileBytes, 0, fileBytes.Length);
                    file.Close();
                    WindowsMediaPlayer.URL = Application.StartupPath + "/QCXEM/" + dt.Rows[0]["TENHINH"].ToString();
                    WindowsMediaPlayer.Ctlcontrols.play();
                    this.Cursor = Cursors.Default;
                    play = true;
                }

                pn_xemvideo.Visible = true;
            }
            catch
            {
                this.Cursor = Cursors.Default;
                play = true;
            }
        }
        
        private void btn_stop_Click(object sender, EventArgs e)
        {
            if (pn_xemvideo.Visible && delete==true)
            {
                delete = false;
                this.Cursor = Cursors.WaitCursor; 
                WindowsMediaPlayer.Ctlcontrols.stop();
                WindowsMediaPlayer.close();
                DirectoryInfo folder = new DirectoryInfo(Application.StartupPath + "/QCXEM");
                FileInfo []files = folder.GetFiles("*.*");
                foreach (FileInfo f in files)
                {

                   f.Delete();

                }
                this.Cursor = Cursors.Default;
                delete = true;
            }
            pn_xemvideo.Visible = false;
            pn_cuon.Visible = true;
            
        }

        private void checkChon_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit btn = (CheckEdit)sender;
            chooseList[int.Parse(btn.Name) - 1] = btn.Checked;
            if (chooseList[int.Parse(btn.Name) - 1])
                smaHinh[int.Parse(btn.Name) - 1] = btn.Tag.ToString();
            else
                smaHinh[int.Parse(btn.Name) - 1] = "";
        }

        private void Frm_QuangCao_Video_SizeChanged(object sender, EventArgs e)
        {
            if (pn_cuon.Height <= 0)
                return;
            LoadData();
            LoadImage(_tonghinhtrentrang, trang);
            LoadVideo(false);
            LoadCuaHang();
          
        }

        private void gridView1_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle >= 0)
                LoadChitietQuay(gridView1.GetFocusedRowCellValue("MA_QUAY").ToString(), false);
            else
            {
                gvChiTietQuay.DataSource = null;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            LoadQuay(cb_CuaHang.EditValue.ToString(), false);
            
        }

        private void btnChonTatCa_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < soluonghinh; i++)
            {
                chooseList[i] = true;

            }
            LoadALLMAHINH();

            LoadImage(_tonghinhtrentrang, trang);
        }

        private void btnChonTatCaVideo_Click(object sender, EventArgs e)
        {
            LoadVideo(true);
        }

        private void btnChonTatCaQuay_Click(object sender, EventArgs e)
        {
            LoadQuay(cb_CuaHang.EditValue.ToString(), true);
        }

        private void btnChonTatCaChiTiet_Click(object sender, EventArgs e)
        {
            if (gridView3.RowCount > 0)
                LoadChitietQuay(gridView3.GetFocusedRowCellValue("MA_QUAY").ToString(),true);
        }
    }
}