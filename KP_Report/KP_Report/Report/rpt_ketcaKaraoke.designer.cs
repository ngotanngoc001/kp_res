﻿namespace KP_Report
{
    partial class rpt_ketcaKaraoke
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbTheTTNB = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbThe = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbTienvoucher = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbTienmat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbPhuthu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbTiengio = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbTienChietKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbTongTienHang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbTongDoanhThu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbTenCa = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbTenQuay = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_Header = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_ngay = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.lb_TenNV = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_NameBill = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 8F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel24,
            this.lbTheTTNB,
            this.xrLabel27,
            this.xrLabel22,
            this.lbThe,
            this.xrLabel25,
            this.xrLabel20,
            this.lbTienvoucher,
            this.xrLabel23,
            this.xrLabel18,
            this.lbTienmat,
            this.xrLabel21,
            this.xrLabel14,
            this.lbPhuthu,
            this.xrLabel19,
            this.xrLabel6,
            this.lbTiengio,
            this.xrLabel17,
            this.xrLabel10,
            this.xrLabel8,
            this.lbTienChietKhau,
            this.xrLabel13,
            this.xrLine4,
            this.xrLabel7,
            this.lbTongTienHang,
            this.xrLine3,
            this.xrLabel1,
            this.lbTongDoanhThu,
            this.xrLabel16,
            this.lbTenCa,
            this.xrLabel12,
            this.lbTenQuay,
            this.xrLabel2,
            this.lb_Header,
            this.lb_ngay,
            this.xrLine2,
            this.lb_TenNV,
            this.lb_NameBill,
            this.xrLabel3,
            this.xrLabel4,
            this.xrLine1});
            this.ReportHeader.HeightF = 403.625F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLabel24
            // 
            this.xrLabel24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel24.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(8.691724F, 364.6616F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(110.4624F, 17.79001F);
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UseForeColor = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "- Thẻ TTNB:";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbTheTTNB
            // 
            this.lbTheTTNB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTheTTNB.ForeColor = System.Drawing.Color.Blue;
            this.lbTheTTNB.LocationFloat = new DevExpress.Utils.PointFloat(124.9921F, 364.6616F);
            this.lbTheTTNB.Name = "lbTheTTNB";
            this.lbTheTTNB.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTheTTNB.SizeF = new System.Drawing.SizeF(120.458F, 17.79001F);
            this.lbTheTTNB.StylePriority.UseBackColor = false;
            this.lbTheTTNB.StylePriority.UseFont = false;
            this.lbTheTTNB.StylePriority.UseForeColor = false;
            this.lbTheTTNB.StylePriority.UseTextAlignment = false;
            this.lbTheTTNB.Text = "lbTheTTNB";
            this.lbTheTTNB.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel27.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(249.9921F, 364.6616F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(40.617F, 17.78998F);
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.StylePriority.UseForeColor = false;
            this.xrLabel27.Text = "VND";
            // 
            // xrLabel22
            // 
            this.xrLabel22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(8.691724F, 346.8715F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(110.4624F, 17.79001F);
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UseForeColor = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "- Thẻ Visa:";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbThe
            // 
            this.lbThe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbThe.ForeColor = System.Drawing.Color.Blue;
            this.lbThe.LocationFloat = new DevExpress.Utils.PointFloat(124.9921F, 346.8715F);
            this.lbThe.Name = "lbThe";
            this.lbThe.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbThe.SizeF = new System.Drawing.SizeF(120.458F, 17.79001F);
            this.lbThe.StylePriority.UseBackColor = false;
            this.lbThe.StylePriority.UseFont = false;
            this.lbThe.StylePriority.UseForeColor = false;
            this.lbThe.StylePriority.UseTextAlignment = false;
            this.lbThe.Text = "lbThe";
            this.lbThe.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel25.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(250F, 346.8715F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(40.617F, 17.78998F);
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UseForeColor = false;
            this.xrLabel25.Text = "VND";
            // 
            // xrLabel20
            // 
            this.xrLabel20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(8.691724F, 329.0815F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(110.4624F, 17.79001F);
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseForeColor = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "- Tiền voucher:";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbTienvoucher
            // 
            this.lbTienvoucher.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTienvoucher.ForeColor = System.Drawing.Color.Blue;
            this.lbTienvoucher.LocationFloat = new DevExpress.Utils.PointFloat(124.9921F, 329.0815F);
            this.lbTienvoucher.Name = "lbTienvoucher";
            this.lbTienvoucher.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTienvoucher.SizeF = new System.Drawing.SizeF(120F, 17.79F);
            this.lbTienvoucher.StylePriority.UseBackColor = false;
            this.lbTienvoucher.StylePriority.UseFont = false;
            this.lbTienvoucher.StylePriority.UseForeColor = false;
            this.lbTienvoucher.StylePriority.UseTextAlignment = false;
            this.lbTienvoucher.Text = "lbTienvoucher";
            this.lbTienvoucher.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(249.9921F, 329.0815F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(40.1669F, 17.78998F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseForeColor = false;
            this.xrLabel23.Text = "VND";
            // 
            // xrLabel18
            // 
            this.xrLabel18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(8.691724F, 311.2916F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(110.4624F, 17.79001F);
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseForeColor = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "- Tiền mặt:";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbTienmat
            // 
            this.lbTienmat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTienmat.ForeColor = System.Drawing.Color.Blue;
            this.lbTienmat.LocationFloat = new DevExpress.Utils.PointFloat(124.9921F, 311.2914F);
            this.lbTienmat.Name = "lbTienmat";
            this.lbTienmat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTienmat.SizeF = new System.Drawing.SizeF(120F, 17.79F);
            this.lbTienmat.StylePriority.UseBackColor = false;
            this.lbTienmat.StylePriority.UseFont = false;
            this.lbTienmat.StylePriority.UseForeColor = false;
            this.lbTienmat.StylePriority.UseTextAlignment = false;
            this.lbTienmat.Text = "lbTienmat";
            this.lbTienmat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(249.5412F, 311.2916F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(40.1669F, 17.78998F);
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseForeColor = false;
            this.xrLabel21.Text = "VND";
            // 
            // xrLabel14
            // 
            this.xrLabel14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(2.845796F, 235.5851F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(116.3083F, 17.78999F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseForeColor = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "Phụ thu:";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbPhuthu
            // 
            this.lbPhuthu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPhuthu.ForeColor = System.Drawing.Color.Blue;
            this.lbPhuthu.LocationFloat = new DevExpress.Utils.PointFloat(124.9921F, 235.5851F);
            this.lbPhuthu.Name = "lbPhuthu";
            this.lbPhuthu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbPhuthu.SizeF = new System.Drawing.SizeF(120F, 17.79F);
            this.lbPhuthu.StylePriority.UseFont = false;
            this.lbPhuthu.StylePriority.UseForeColor = false;
            this.lbPhuthu.StylePriority.UseTextAlignment = false;
            this.lbPhuthu.Text = "lbPhuthu";
            this.lbPhuthu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(249.9921F, 217.7917F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(39.70805F, 17.79169F);
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UseForeColor = false;
            this.xrLabel19.Text = "VND";
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(2.845796F, 200F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(116.3083F, 17.78999F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseForeColor = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Tiền giờ:";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbTiengio
            // 
            this.lbTiengio.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTiengio.ForeColor = System.Drawing.Color.Blue;
            this.lbTiengio.LocationFloat = new DevExpress.Utils.PointFloat(124.9921F, 200F);
            this.lbTiengio.Name = "lbTiengio";
            this.lbTiengio.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTiengio.SizeF = new System.Drawing.SizeF(120F, 17.79F);
            this.lbTiengio.StylePriority.UseFont = false;
            this.lbTiengio.StylePriority.UseForeColor = false;
            this.lbTiengio.StylePriority.UseTextAlignment = false;
            this.lbTiengio.Text = "lbTiengio";
            this.lbTiengio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(249.9921F, 200F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(39.70805F, 17.79169F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseForeColor = false;
            this.xrLabel17.Text = "VND";
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(249.9921F, 235.5834F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(39.70805F, 17.79169F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseForeColor = false;
            this.xrLabel10.Text = "VND";
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(2.845796F, 253.3767F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(116.3083F, 17.78999F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseForeColor = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "Tiền chiết khấu:";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbTienChietKhau
            // 
            this.lbTienChietKhau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTienChietKhau.ForeColor = System.Drawing.Color.Blue;
            this.lbTienChietKhau.LocationFloat = new DevExpress.Utils.PointFloat(124.9921F, 253.3767F);
            this.lbTienChietKhau.Name = "lbTienChietKhau";
            this.lbTienChietKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTienChietKhau.SizeF = new System.Drawing.SizeF(120F, 17.78999F);
            this.lbTienChietKhau.StylePriority.UseFont = false;
            this.lbTienChietKhau.StylePriority.UseForeColor = false;
            this.lbTienChietKhau.StylePriority.UseTextAlignment = false;
            this.lbTienChietKhau.Text = "lbTienChietKhau";
            this.lbTienChietKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(249.9921F, 253.3751F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(40.17488F, 17.79169F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseForeColor = false;
            this.xrLabel13.Text = "VND";
            // 
            // xrLine4
            // 
            this.xrLine4.ForeColor = System.Drawing.Color.Blue;
            this.xrLine4.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(2.845788F, 283.0016F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(286.8543F, 10.5F);
            this.xrLine4.StylePriority.UseForeColor = false;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(2.845828F, 217.7934F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(116.3083F, 17.78996F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseForeColor = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Tổng tiền hàng:";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbTongTienHang
            // 
            this.lbTongTienHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTongTienHang.ForeColor = System.Drawing.Color.Blue;
            this.lbTongTienHang.LocationFloat = new DevExpress.Utils.PointFloat(124.9921F, 217.7934F);
            this.lbTongTienHang.Name = "lbTongTienHang";
            this.lbTongTienHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTongTienHang.SizeF = new System.Drawing.SizeF(120.458F, 17.78999F);
            this.lbTongTienHang.StylePriority.UseFont = false;
            this.lbTongTienHang.StylePriority.UseForeColor = false;
            this.lbTongTienHang.StylePriority.UseTextAlignment = false;
            this.lbTongTienHang.Text = "lbTongTienHang";
            this.lbTongTienHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLine3
            // 
            this.xrLine3.ForeColor = System.Drawing.Color.Blue;
            this.xrLine3.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(2.845796F, 91.08318F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(286.8543F, 10.5F);
            this.xrLine3.StylePriority.UseForeColor = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(250F, 293.5016F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(40.1669F, 17.78998F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.Text = "VND";
            // 
            // lbTongDoanhThu
            // 
            this.lbTongDoanhThu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTongDoanhThu.ForeColor = System.Drawing.Color.Blue;
            this.lbTongDoanhThu.LocationFloat = new DevExpress.Utils.PointFloat(124.9921F, 293.5016F);
            this.lbTongDoanhThu.Name = "lbTongDoanhThu";
            this.lbTongDoanhThu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTongDoanhThu.SizeF = new System.Drawing.SizeF(120F, 17.79F);
            this.lbTongDoanhThu.StylePriority.UseBackColor = false;
            this.lbTongDoanhThu.StylePriority.UseFont = false;
            this.lbTongDoanhThu.StylePriority.UseForeColor = false;
            this.lbTongDoanhThu.StylePriority.UseTextAlignment = false;
            this.lbTongDoanhThu.Text = "lbTongDoanhThu";
            this.lbTongDoanhThu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(2.845828F, 293.5016F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(116.3083F, 17.78998F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseForeColor = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "Tổng Doanh Thu:";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbTenCa
            // 
            this.lbTenCa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenCa.ForeColor = System.Drawing.Color.Blue;
            this.lbTenCa.LocationFloat = new DevExpress.Utils.PointFloat(62.95567F, 124.5831F);
            this.lbTenCa.Name = "lbTenCa";
            this.lbTenCa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTenCa.SizeF = new System.Drawing.SizeF(226.7445F, 17.79001F);
            this.lbTenCa.StylePriority.UseFont = false;
            this.lbTenCa.StylePriority.UseForeColor = false;
            this.lbTenCa.Text = "lb_mahoadon";
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(2.845796F, 124.5831F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(58.99999F, 17.79001F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseForeColor = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "Tên Ca   :";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbTenQuay
            // 
            this.lbTenQuay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenQuay.ForeColor = System.Drawing.Color.Blue;
            this.lbTenQuay.LocationFloat = new DevExpress.Utils.PointFloat(66.70961F, 142.3732F);
            this.lbTenQuay.Name = "lbTenQuay";
            this.lbTenQuay.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTenQuay.SizeF = new System.Drawing.SizeF(222.9905F, 17.79166F);
            this.lbTenQuay.StylePriority.UseFont = false;
            this.lbTenQuay.StylePriority.UseForeColor = false;
            this.lbTenQuay.Text = "lbTenQuay";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(2.845788F, 142.3731F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(63.86382F, 17.79169F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseForeColor = false;
            this.xrLabel2.Text = "Tên Quầy:";
            // 
            // lb_Header
            // 
            this.lb_Header.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Header.ForeColor = System.Drawing.Color.Blue;
            this.lb_Header.LocationFloat = new DevExpress.Utils.PointFloat(2.845828F, 10.00001F);
            this.lb_Header.Multiline = true;
            this.lb_Header.Name = "lb_Header";
            this.lb_Header.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_Header.SizeF = new System.Drawing.SizeF(286.8543F, 19.79168F);
            this.lb_Header.StylePriority.UseFont = false;
            this.lb_Header.StylePriority.UseForeColor = false;
            this.lb_Header.StylePriority.UseTextAlignment = false;
            this.lb_Header.Text = "Điện Máy Thiên Hòa";
            this.lb_Header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // lb_ngay
            // 
            this.lb_ngay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ngay.ForeColor = System.Drawing.Color.Blue;
            this.lb_ngay.LocationFloat = new DevExpress.Utils.PointFloat(62.9557F, 106.7915F);
            this.lb_ngay.Name = "lb_ngay";
            this.lb_ngay.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_ngay.SizeF = new System.Drawing.SizeF(226.7444F, 17.79163F);
            this.lb_ngay.StylePriority.UseFont = false;
            this.lb_ngay.StylePriority.UseForeColor = false;
            this.lb_ngay.Text = "lb_ngay";
            // 
            // xrLine2
            // 
            this.xrLine2.ForeColor = System.Drawing.Color.Blue;
            this.xrLine2.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(2.845828F, 185.2498F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(286.8543F, 10.5F);
            this.xrLine2.StylePriority.UseForeColor = false;
            // 
            // lb_TenNV
            // 
            this.lb_TenNV.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_TenNV.ForeColor = System.Drawing.Color.Blue;
            this.lb_TenNV.LocationFloat = new DevExpress.Utils.PointFloat(66.70961F, 160.1648F);
            this.lb_TenNV.Name = "lb_TenNV";
            this.lb_TenNV.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_TenNV.SizeF = new System.Drawing.SizeF(222.9905F, 17.78999F);
            this.lb_TenNV.StylePriority.UseFont = false;
            this.lb_TenNV.StylePriority.UseForeColor = false;
            this.lb_TenNV.Text = "lb_TenNV";
            // 
            // lb_NameBill
            // 
            this.lb_NameBill.Font = new System.Drawing.Font("Tahoma", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_NameBill.ForeColor = System.Drawing.Color.Blue;
            this.lb_NameBill.LocationFloat = new DevExpress.Utils.PointFloat(2.845828F, 67.87491F);
            this.lb_NameBill.Multiline = true;
            this.lb_NameBill.Name = "lb_NameBill";
            this.lb_NameBill.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_NameBill.SizeF = new System.Drawing.SizeF(288.3626F, 23.20828F);
            this.lb_NameBill.StylePriority.UseFont = false;
            this.lb_NameBill.StylePriority.UseForeColor = false;
            this.lb_NameBill.StylePriority.UseTextAlignment = false;
            this.lb_NameBill.Text = " Kết Ca";
            this.lb_NameBill.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(2.849611F, 160.1648F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(63.86F, 17.79F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseForeColor = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Họ Tên    :";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(2.845828F, 106.7915F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(58.99995F, 17.79163F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseForeColor = false;
            this.xrLabel4.Text = "Ngày :";
            // 
            // xrLine1
            // 
            this.xrLine1.ForeColor = System.Drawing.Color.Blue;
            this.xrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 393.125F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(286.8543F, 10.5F);
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // ReportFooter
            // 
            this.ReportFooter.HeightF = 0.8749326F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // rpt_ketcaKaraoke
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.ReportFooter});
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.Margins = new System.Drawing.Printing.Margins(0, 29, 0, 8);
            this.PageHeight = 678;
            this.PageWidth = 323;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ShowPrintMarginsWarning = false;
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel lb_ngay;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel lb_TenNV;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel lb_Header;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel lbTongDoanhThu;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel lbTenCa;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel lbTenQuay;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel lbTongTienHang;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel lbTienChietKhau;
        private DevExpress.XtraReports.UI.XRLabel lb_NameBill;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel lbPhuthu;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel lbTiengio;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel lbTheTTNB;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel lbThe;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel lbTienvoucher;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel lbTienmat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
    }
}
