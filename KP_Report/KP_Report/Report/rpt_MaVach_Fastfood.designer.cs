﻿namespace KP_Report
{
    partial class rpt_MaVach_Fastfood
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.HANGHOA = new DevExpress.XtraReports.UI.XRTableCell();
            this.YEUCAUTHEM = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAY = new DevExpress.XtraReports.UI.XRLabel();
            this.GIA = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.Number = new DevExpress.XtraReports.UI.XRLabel();
            this.TENCONGTY = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.SODIENTHOAI = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail.Dpi = 254F;
            this.Detail.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Detail.HeightF = 32F;
            this.Detail.LockedInUserDesigner = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTable6.Dpi = 254F;
            this.xrTable6.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(5.000018F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable6.SizeF = new System.Drawing.SizeF(381.5F, 32F);
            this.xrTable6.StylePriority.UseBorders = false;
            this.xrTable6.StylePriority.UseFont = false;
            this.xrTable6.StylePriority.UseTextAlignment = false;
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.HANGHOA});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // HANGHOA
            // 
            this.HANGHOA.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.HANGHOA.CanGrow = false;
            this.HANGHOA.Dpi = 254F;
            this.HANGHOA.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.HANGHOA.Multiline = true;
            this.HANGHOA.Name = "HANGHOA";
            this.HANGHOA.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 5, 0, 0, 254F);
            this.HANGHOA.StylePriority.UseBorders = false;
            this.HANGHOA.StylePriority.UseFont = false;
            this.HANGHOA.StylePriority.UsePadding = false;
            this.HANGHOA.StylePriority.UseTextAlignment = false;
            this.HANGHOA.Text = "Trà sữa trân châu\r\nTrà sữa trân châu\r\nTrà sữa trân châu";
            this.HANGHOA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.HANGHOA.Weight = 2.9195216111786118D;
            // 
            // YEUCAUTHEM
            // 
            this.YEUCAUTHEM.CanGrow = false;
            this.YEUCAUTHEM.Dpi = 254F;
            this.YEUCAUTHEM.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Italic);
            this.YEUCAUTHEM.LocationFloat = new DevExpress.Utils.PointFloat(5.000018F, 0F);
            this.YEUCAUTHEM.Name = "YEUCAUTHEM";
            this.YEUCAUTHEM.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.YEUCAUTHEM.SizeF = new System.Drawing.SizeF(381.5F, 40.11258F);
            this.YEUCAUTHEM.StylePriority.UseFont = false;
            this.YEUCAUTHEM.StylePriority.UsePadding = false;
            this.YEUCAUTHEM.StylePriority.UseTextAlignment = false;
            this.YEUCAUTHEM.Text = "Ít đường, lạnh , pho mai,Ít đường, lạnh , pho mai";
            this.YEUCAUTHEM.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // NGAY
            // 
            this.NGAY.CanGrow = false;
            this.NGAY.Dpi = 254F;
            this.NGAY.Font = new System.Drawing.Font("Arial", 6F);
            this.NGAY.LocationFloat = new DevExpress.Utils.PointFloat(195.0895F, 31.00566F);
            this.NGAY.Name = "NGAY";
            this.NGAY.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAY.SizeF = new System.Drawing.SizeF(191.4105F, 24.51183F);
            this.NGAY.StylePriority.UseFont = false;
            this.NGAY.StylePriority.UsePadding = false;
            this.NGAY.StylePriority.UseTextAlignment = false;
            this.NGAY.Text = "31/08/2017 13 :56";
            this.NGAY.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // GIA
            // 
            this.GIA.Dpi = 254F;
            this.GIA.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Bold);
            this.GIA.LocationFloat = new DevExpress.Utils.PointFloat(228.9084F, 41.57087F);
            this.GIA.Name = "GIA";
            this.GIA.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GIA.SizeF = new System.Drawing.SizeF(157.5917F, 27.15767F);
            this.GIA.StylePriority.UseFont = false;
            this.GIA.StylePriority.UsePadding = false;
            this.GIA.StylePriority.UseTextAlignment = false;
            this.GIA.Text = "Tổng: 300,000";
            this.GIA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // formattingRule1
            // 
            this.formattingRule1.Name = "formattingRule1";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.Number,
            this.TENCONGTY,
            this.NGAY});
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 58.74666F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // Number
            // 
            this.Number.Dpi = 254F;
            this.Number.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.Number.LocationFloat = new DevExpress.Utils.PointFloat(207.6808F, 0F);
            this.Number.Name = "Number";
            this.Number.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.Number.SizeF = new System.Drawing.SizeF(178.8192F, 31.00564F);
            this.Number.StylePriority.UseFont = false;
            this.Number.StylePriority.UsePadding = false;
            this.Number.StylePriority.UseTextAlignment = false;
            this.Number.Text = "(520) 10/10";
            this.Number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TENCONGTY
            // 
            this.TENCONGTY.Dpi = 254F;
            this.TENCONGTY.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.TENCONGTY.LocationFloat = new DevExpress.Utils.PointFloat(5.000099F, 0F);
            this.TENCONGTY.Name = "TENCONGTY";
            this.TENCONGTY.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENCONGTY.SizeF = new System.Drawing.SizeF(202.6807F, 55.51749F);
            this.TENCONGTY.StylePriority.UseFont = false;
            this.TENCONGTY.StylePriority.UsePadding = false;
            this.TENCONGTY.StylePriority.UseTextAlignment = false;
            this.TENCONGTY.Text = "TRA SUA CHAN TEA";
            this.TENCONGTY.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.SODIENTHOAI,
            this.YEUCAUTHEM,
            this.GIA});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 68.72853F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // SODIENTHOAI
            // 
            this.SODIENTHOAI.Dpi = 254F;
            this.SODIENTHOAI.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Bold);
            this.SODIENTHOAI.LocationFloat = new DevExpress.Utils.PointFloat(5.000113F, 41.57087F);
            this.SODIENTHOAI.Name = "SODIENTHOAI";
            this.SODIENTHOAI.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.SODIENTHOAI.SizeF = new System.Drawing.SizeF(223.9083F, 27.15767F);
            this.SODIENTHOAI.StylePriority.UseFont = false;
            this.SODIENTHOAI.StylePriority.UsePadding = false;
            this.SODIENTHOAI.StylePriority.UseTextAlignment = false;
            this.SODIENTHOAI.Text = "Phone:090.555.1111";
            this.SODIENTHOAI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // rpt_MaVach_Fastfood
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.ReportFooter});
            this.Dpi = 254F;
            this.Font = new System.Drawing.Font("Times New Roman", 15F, System.Drawing.FontStyle.Bold);
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
            this.PageHeight = 300;
            this.PageWidth = 398;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.ShowPrintMarginsWarning = false;
            this.ShowPrintStatusDialog = false;
            this.SnapGridSize = 5F;
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell HANGHOA;
        private DevExpress.XtraReports.UI.XRLabel GIA;
        private DevExpress.XtraReports.UI.XRLabel YEUCAUTHEM;
        private DevExpress.XtraReports.UI.XRLabel NGAY;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel TENCONGTY;
        private DevExpress.XtraReports.UI.XRLabel SODIENTHOAI;
        private DevExpress.XtraReports.UI.XRLabel Number;
    }
}
