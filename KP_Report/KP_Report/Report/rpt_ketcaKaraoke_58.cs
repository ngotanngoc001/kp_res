﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using System.Data;
using KP_UserManagement;
using System.IO;
using System.Windows.Forms;


namespace KP_Report
{
    /// <summary>
    /// rpt_ketcaKaraoke
    /// </summary>
    public partial class rpt_ketcaKaraoke_58 : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// checkBill
        /// </summary>
        public int checkBill=0;

        /// <summary>
        /// rpt_ketcaKaraoke
        /// </summary>
        public rpt_ketcaKaraoke_58()
        {
            InitializeComponent();  
            Setheader();            
        }
        /// <summary>
        /// BindData
        /// </summary>
        public void BindData()
        {
            lb_ngay.Text = string.Format("{0:dd/MM/yyyy HH:mm:ss}", Convert.ToDateTime(clsMain.ReturnDataTable("Select GETDATE()").Rows[0][0].ToString()));
            lbTenQuay.DataBindings.Add("Text", DataSource, "tenQuay");
            lb_TenNV.DataBindings.Add("Text", DataSource, "tenNV");
            lbTenCa.DataBindings.Add("Text", DataSource, "tenCaBan");
            lbTiengio.DataBindings.Add("Text", DataSource, "tiengio", "{0:#,###0}");
            lbTongTienHang.DataBindings.Add("Text", DataSource, "tongTienBan", "{0:#,###0}");
            lbPhuthu.DataBindings.Add("Text", DataSource, "phuthu", "{0:#,###0}");
            lbTienChietKhau.DataBindings.Add("Text", DataSource, "giamgia", "{0:#,###0}");            
            lbTongDoanhThu.DataBindings.Add("Text", DataSource, "tongTien", "{0:#,###0}");
            lbTienmat.DataBindings.Add("Text", DataSource, "tienmat", "{0:#,###0}");
            lbThe.DataBindings.Add("Text", DataSource, "tienthe", "{0:#,###0}");
            lbTheTTNB.DataBindings.Add("Text", DataSource, "TTNB", "{0:#,###0}");
            lbTienvoucher.DataBindings.Add("Text", DataSource, "voucher", "{0:#,###0}");    
        } 
        private void Setheader()
        {
            StreamReader str = new StreamReader(Application.StartupPath + "/systeminfo.txt");
            string sMacuahang = "";
            string line;
            while ((line = str.ReadLine()) != null)
            {
                string[] value = line.Split(',');
                sMacuahang = value[0].ToString();
            }

            string sSQL = "";
            sSQL += "Select MA_CUAHANG,	TEN_CUAHANG, GHICHU, SUDUNG, Header, Footer" + "\n";
            sSQL += "From CUAHANG" + "\n";
            sSQL += "Where MA_CUAHANG  =" + clsMain.SQLString(sMacuahang) + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            lb_Header.Text = dt.Rows[0]["Header"].ToString();
        }      
    }
}
