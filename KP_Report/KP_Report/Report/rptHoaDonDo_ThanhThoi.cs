﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using System.Drawing.Imaging;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using DevExpress.XtraPrinting;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptHoaDonDo_ThanhThoi : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptHoaDonDo_ThanhThoi()
        {
            InitializeComponent();
        }
        /// <summary>
        /// data nguon
        /// </summary>
        public static DataTable dt = new DataTable();
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            dt = (DataTable)DataSource;
            DataTable dtdulieu = new DataTable();
            dtdulieu.Columns.Add("STT", typeof(string));
            dtdulieu.Columns.Add("TENHANGHOA", typeof(string));
            dtdulieu.Columns.Add("DVT", typeof(string));
            dtdulieu.Columns.Add("SOLUONG", typeof(string));
            dtdulieu.Columns.Add("DONGIA", typeof(string));
            dtdulieu.Columns.Add("THANHTIEN", typeof(string));

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["TENHANGHOA"].ToString().Length <= 33)
                {
                    DataRow dr = dtdulieu.NewRow();
                    dr["STT"] = dt.Rows[i]["STT"].ToString();
                    dr["TENHANGHOA"] = dt.Rows[i]["TENHANGHOA"].ToString();
                    dr["DVT"] = dt.Rows[i]["DVT"].ToString();
                    dr["SOLUONG"] = dt.Rows[i]["SOLUONG"].ToString();
                    dr["DONGIA"] = dt.Rows[i]["DONGIA"].ToString();
                    dr["THANHTIEN"] = dt.Rows[i]["THANHTIEN"].ToString();
                    dtdulieu.Rows.Add(dr);
                }
                else
                {
                    string tenhang = dt.Rows[i]["TENHANGHOA"].ToString();
                    int bienlap = tenhang.Length / 33 + 1;
                    string tenhangcat = "";
                    string tenhangconlai = "";
                    int sdu = 0;
                    for (int j = 0; j < bienlap; j++)
                    {
                        if (j == 0) // lần đầu cắt ghi đầy đủ giá trị
                        {
                            int indexcuoi = 32;
                            do
                            {
                                if (tenhang.Substring(indexcuoi, 1) == " ")//substring bắt đầu từ 0, = "" cắt chuỗi khỏi đứt chữ
                                {
                                    DataRow dr = dtdulieu.NewRow();
                                    tenhangcat = tenhang.Substring(0, indexcuoi);
                                    tenhangconlai = tenhang.Substring(indexcuoi + 1);
                                    dr["STT"] = dt.Rows[i]["STT"].ToString();
                                    dr["TENHANGHOA"] = tenhangcat;
                                    dr["DVT"] = dt.Rows[i]["DVT"].ToString();
                                    dr["SOLUONG"] = dt.Rows[i]["SOLUONG"].ToString();
                                    dr["DONGIA"] = dt.Rows[i]["DONGIA"].ToString();
                                    dr["THANHTIEN"] = dt.Rows[i]["THANHTIEN"].ToString();
                                    dtdulieu.Rows.Add(dr);
                                }
                                else
                                {
                                    sdu += 1;
                                }
                                indexcuoi -= 1;
                            }
                            while (tenhangcat == "");
                            tenhangcat = "";
                        }
                        else
                        {
                            int indexcuoi = 32;
                            do
                            {
                                if (tenhangconlai.Length > 33)
                                {
                                    if (tenhangconlai.Substring(indexcuoi, 1) == " ")//substring bắt đầu từ 0
                                    {
                                        DataRow dr = dtdulieu.NewRow();
                                        tenhangcat = tenhangconlai.Substring(0, indexcuoi);
                                        tenhangconlai = tenhangconlai.Substring(indexcuoi + 1);
                                        dr["STT"] = "";
                                        dr["TENHANGHOA"] = tenhangcat;
                                        dr["SOLUONG"] = "";
                                        dr["DVT"] = "";
                                        dr["DONGIA"] = "";
                                        dr["THANHTIEN"] = "";
                                        dtdulieu.Rows.Add(dr);
                                    }
                                    else
                                    {
                                        sdu += 1;
                                    }
                                }
                                else
                                {
                                    DataRow dr = dtdulieu.NewRow();
                                    tenhangcat = tenhangconlai;
                                    dr["STT"] = "";
                                    dr["TENHANGHOA"] = tenhangcat;
                                    dr["DVT"] = "";
                                    dr["SOLUONG"] = "";
                                    dr["DONGIA"] = "";
                                    dr["THANHTIEN"] = "";
                                    dtdulieu.Rows.Add(dr);
                                }
                                indexcuoi -= 1;
                            }
                            while (tenhangcat == "");
                            tenhangcat = "";
                        }
                        if (sdu >= 20)
                        {
                            bienlap += 1;
                            sdu = 0;
                        }
                    }
                }
            }
            if (dtdulieu.Rows.Count < 12)//nếu dữ liệu chưa đủ 12 dòng add thêm dòng trống (vì hóa đơn đỏ tối đa 13 dòng)
            {
                int sodongtrong = 12 - dtdulieu.Rows.Count;
                for (int i = 0; i < sodongtrong; i++)
                {
                    DataRow dr = dtdulieu.NewRow();
                    dr["STT"] = "";
                    dr["TENHANGHOA"] = "";
                    dr["DVT"] = "";
                    dr["SOLUONG"] = "";
                    dr["DONGIA"] = "";
                    dr["THANHTIEN"] = "";
                    dtdulieu.Rows.Add(dr);
                }
            }
            //ghi dữ liệu
            //STT
            xrTableCell1.Text = dtdulieu.Rows[0]["STT"].ToString();
            xrTableCell7.Text = dtdulieu.Rows[1]["STT"].ToString();
            xrTableCell13.Text = dtdulieu.Rows[2]["STT"].ToString();
            xrTableCell19.Text = dtdulieu.Rows[3]["STT"].ToString();
            xrTableCell25.Text = dtdulieu.Rows[4]["STT"].ToString();
            xrTableCell31.Text = dtdulieu.Rows[5]["STT"].ToString();
            xrTableCell37.Text = dtdulieu.Rows[6]["STT"].ToString();
            xrTableCell43.Text = dtdulieu.Rows[7]["STT"].ToString();
            xrTableCell49.Text = dtdulieu.Rows[8]["STT"].ToString();
            xrTableCell55.Text = dtdulieu.Rows[9]["STT"].ToString();
            xrTableCell61.Text = dtdulieu.Rows[10]["STT"].ToString();
            xrTableCell67.Text = dtdulieu.Rows[11]["STT"].ToString();
            //Ten hàng hóa
            xrTableCell2.Text = dtdulieu.Rows[0]["TENHANGHOA"].ToString();
            xrTableCell8.Text = dtdulieu.Rows[1]["TENHANGHOA"].ToString();
            xrTableCell14.Text = dtdulieu.Rows[2]["TENHANGHOA"].ToString();
            xrTableCell20.Text = dtdulieu.Rows[3]["TENHANGHOA"].ToString();
            xrTableCell26.Text = dtdulieu.Rows[4]["TENHANGHOA"].ToString();
            xrTableCell32.Text = dtdulieu.Rows[5]["TENHANGHOA"].ToString();
            xrTableCell38.Text = dtdulieu.Rows[6]["TENHANGHOA"].ToString();
            xrTableCell44.Text = dtdulieu.Rows[7]["TENHANGHOA"].ToString();
            xrTableCell50.Text = dtdulieu.Rows[8]["TENHANGHOA"].ToString();
            xrTableCell56.Text = dtdulieu.Rows[9]["TENHANGHOA"].ToString();
            xrTableCell62.Text = dtdulieu.Rows[10]["TENHANGHOA"].ToString();
            xrTableCell68.Text = dtdulieu.Rows[11]["TENHANGHOA"].ToString();
            //Don vi tính
            xrTableCell3.Text = dtdulieu.Rows[0]["DVT"].ToString();
            xrTableCell9.Text = dtdulieu.Rows[1]["DVT"].ToString();
            xrTableCell15.Text = dtdulieu.Rows[2]["DVT"].ToString();
            xrTableCell21.Text = dtdulieu.Rows[3]["DVT"].ToString();
            xrTableCell27.Text = dtdulieu.Rows[4]["DVT"].ToString();
            xrTableCell33.Text = dtdulieu.Rows[5]["DVT"].ToString();
            xrTableCell39.Text = dtdulieu.Rows[6]["DVT"].ToString();
            xrTableCell45.Text = dtdulieu.Rows[7]["DVT"].ToString();
            xrTableCell51.Text = dtdulieu.Rows[8]["DVT"].ToString();
            xrTableCell57.Text = dtdulieu.Rows[9]["DVT"].ToString();
            xrTableCell63.Text = dtdulieu.Rows[10]["DVT"].ToString();
            xrTableCell69.Text = dtdulieu.Rows[11]["DVT"].ToString();
            //So luong
            xrTableCell4.Text = dtdulieu.Rows[0]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[0]["SOLUONG"].ToString()))).Replace(",", ".");
            xrTableCell10.Text = dtdulieu.Rows[1]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[1]["SOLUONG"].ToString()))).Replace(",", ".");
            xrTableCell16.Text = dtdulieu.Rows[2]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[2]["SOLUONG"].ToString()))).Replace(",", ".");
            xrTableCell22.Text = dtdulieu.Rows[3]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[3]["SOLUONG"].ToString()))).Replace(",", ".");
            xrTableCell28.Text = dtdulieu.Rows[4]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[4]["SOLUONG"].ToString()))).Replace(",", ".");
            xrTableCell34.Text = dtdulieu.Rows[5]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[5]["SOLUONG"].ToString()))).Replace(",", ".");
            xrTableCell40.Text = dtdulieu.Rows[6]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[6]["SOLUONG"].ToString()))).Replace(",", ".");
            xrTableCell46.Text = dtdulieu.Rows[7]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[7]["SOLUONG"].ToString()))).Replace(",", ".");
            xrTableCell52.Text = dtdulieu.Rows[8]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[8]["SOLUONG"].ToString()))).Replace(",", ".");
            xrTableCell58.Text = dtdulieu.Rows[9]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[9]["SOLUONG"].ToString()))).Replace(",", ".");
            xrTableCell64.Text = dtdulieu.Rows[10]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[10]["SOLUONG"].ToString()))).Replace(",", ".");
            xrTableCell70.Text = dtdulieu.Rows[11]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[11]["SOLUONG"].ToString()))).Replace(",", ".");
            //Đơn giá
            xrTableCell5.Text = dtdulieu.Rows[0]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[0]["DONGIA"].ToString()))).Replace(",", ".");
            xrTableCell11.Text = dtdulieu.Rows[1]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[1]["DONGIA"].ToString()))).Replace(",", ".");
            xrTableCell17.Text = dtdulieu.Rows[2]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[2]["DONGIA"].ToString()))).Replace(",", ".");
            xrTableCell23.Text = dtdulieu.Rows[3]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[3]["DONGIA"].ToString()))).Replace(",", ".");
            xrTableCell29.Text = dtdulieu.Rows[4]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[4]["DONGIA"].ToString()))).Replace(",", ".");
            xrTableCell35.Text = dtdulieu.Rows[5]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[5]["DONGIA"].ToString()))).Replace(",", ".");
            xrTableCell41.Text = dtdulieu.Rows[6]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[6]["DONGIA"].ToString()))).Replace(",", ".");
            xrTableCell47.Text = dtdulieu.Rows[7]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[7]["DONGIA"].ToString()))).Replace(",", ".");
            xrTableCell53.Text = dtdulieu.Rows[8]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[8]["DONGIA"].ToString()))).Replace(",", ".");
            xrTableCell59.Text = dtdulieu.Rows[9]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[9]["DONGIA"].ToString()))).Replace(",", ".");
            xrTableCell65.Text = dtdulieu.Rows[10]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[10]["DONGIA"].ToString()))).Replace(",", ".");
            xrTableCell71.Text = dtdulieu.Rows[11]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[11]["DONGIA"].ToString()))).Replace(",", ".");
            //Thành tiền
            xrTableCell6.Text = dtdulieu.Rows[0]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[0]["THANHTIEN"].ToString()))).Replace(",", ".");
            xrTableCell12.Text = dtdulieu.Rows[1]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[1]["THANHTIEN"].ToString()))).Replace(",", ".");
            xrTableCell18.Text = dtdulieu.Rows[2]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[2]["THANHTIEN"].ToString()))).Replace(",", ".");
            xrTableCell24.Text = dtdulieu.Rows[3]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[3]["THANHTIEN"].ToString()))).Replace(",", ".");
            xrTableCell30.Text = dtdulieu.Rows[4]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[4]["THANHTIEN"].ToString()))).Replace(",", ".");
            xrTableCell36.Text = dtdulieu.Rows[5]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[5]["THANHTIEN"].ToString()))).Replace(",", ".");
            xrTableCell42.Text = dtdulieu.Rows[6]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[6]["THANHTIEN"].ToString()))).Replace(",", ".");
            xrTableCell48.Text = dtdulieu.Rows[7]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[7]["THANHTIEN"].ToString()))).Replace(",", ".");
            xrTableCell54.Text = dtdulieu.Rows[8]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[8]["THANHTIEN"].ToString()))).Replace(",", ".");
            xrTableCell60.Text = dtdulieu.Rows[9]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[9]["THANHTIEN"].ToString()))).Replace(",", ".");
            xrTableCell66.Text = dtdulieu.Rows[10]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[10]["THANHTIEN"].ToString()))).Replace(",", ".");
            xrTableCell72.Text = dtdulieu.Rows[11]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[11]["THANHTIEN"].ToString()))).Replace(",", ".");

            VAT.Text = Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dt.Rows[0]["VAT"].ToString()))).Replace(",", ".");
            txt_thanhtientongcong.Text = Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dt.Rows[0]["THANHTIENTONG"].ToString()))).Replace(",", ".");
            txt_tongtienthue.Text = Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dt.Rows[0]["TIENVATTONG"].ToString()))).Replace(",", ".");
            txt_tongtiensauthue.Text = Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dt.Rows[0]["TONGCONG"].ToString()))).Replace(",", ".");

            txt_tennguoimua.DataBindings.Add("Text", DataSource, "HOTENGUOIMUA");
            txt_mst.DataBindings.Add("Text", DataSource, "MST");
            txt_httt.DataBindings.Add("Text", DataSource, "HTTT");
            txt_ngay.DataBindings.Add("Text", DataSource, "NGAY");
            txt_thang.DataBindings.Add("Text", DataSource, "THANG");
            txt_nam.DataBindings.Add("Text", DataSource, "NAM");
            //Nếu tên đơn vị mua dài hơn 65 ký tự sẽ ghi xuống ô MST, phía sau 
            string tendonvi = dt.Rows[0]["DONVIMUA"].ToString();
            if (tendonvi.Length <= 65)
            {
                txt_donvi.Text = tendonvi;
            }
            else
            {
                int index = 65;
                do
                {
                    if (tendonvi.Substring(index, 1) == " ")
                    {
                        txt_donvi.Text = tendonvi.Substring(0, index);
                        txt_tendonvidai.Text = tendonvi.Substring(index + 1);
                    }
                    index -= 1;
                }
                while (txt_donvi.Text == "");
            }
            //Nếu địa chỉ dài hơn 85 ký tự sẽ ghi xuống ô txt_diachi2
            string diachi = dt.Rows[0]["DIACHI"].ToString();
            if (diachi.Length <= 85)
            {
                txt_diachi1.Text = diachi;
                txt_diachi2.Text = "";
                txt_sotaikhoan.Text = dt.Rows[0]["SOTK"].ToString();
            }
            else
            {
                int index = 85;
                do
                {
                    if (diachi.Substring(index, 1) == " ")
                    {
                        txt_diachi1.Text = diachi.Substring(0, index);
                        txt_diachi2.Text = diachi.Substring(index + 1);
                        txt_sotaikhoan.Text = dt.Rows[0]["SOTK"].ToString();
                    }
                    index -= 1;
                }
                while (txt_diachi1.Text == "");
            }

            string tongtien = Math.Round(decimal.Parse(dt.Rows[0]["TONGCONG"].ToString())).ToString();
            string chuoichutien = ChuyenSo(tongtien) + "đồng";

            if (chuoichutien.Length <= 60)
            {
                txt_doctien1.Text = chuoichutien;
                txt_doctien2.Text = "";
            }
            else
            {
                int index = 60;
                do
                {
                    if (chuoichutien.Substring(index, 1) == " ")
                    {
                        txt_doctien1.Text = chuoichutien.Substring(0, index);
                        txt_doctien2.Text = chuoichutien.Substring(index + 1);
                    }
                    index -= 1;
                }
                while (txt_doctien1.Text == "");
            }
        }
        /// <summary>
        /// ChuyenSo
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string ChuyenSo(string number)
        {
            string[] dv = { "", "mươi", "trăm", "nghìn", "triệu", "tỉ" };
            string[] cs = { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
            string doc;
            int i, j, k, n, len, found, ddv, rd;

            len = number.Length;
            number += "ss";
            doc = "";
            found = 0;
            ddv = 0;
            rd = 0;

            i = 0;
            while (i < len)
            {
                //So chu so o hang dang duyet
                n = (len - i + 2) % 3 + 1;

                //Kiem tra so 0
                found = 0;
                for (j = 0; j < n; j++)
                {
                    if (number[i + j] != '0')
                    {
                        found = 1;
                        break;
                    }
                }

                //Duyet n chu so
                if (found == 1)
                {
                    rd = 1;
                    for (j = 0; j < n; j++)
                    {
                        ddv = 1;
                        switch (number[i + j])
                        {
                            case '0':
                                if (n - j == 3) doc += cs[0] + " ";
                                if (n - j == 2)
                                {
                                    if (number[i + j + 1] != '0') doc += "lẻ ";
                                    ddv = 0;
                                }
                                break;
                            case '1':
                                if (n - j == 3) doc += cs[1] + " ";
                                if (n - j == 2)
                                {
                                    doc += "mười ";
                                    ddv = 0;
                                }
                                if (n - j == 1)
                                {
                                    if (i + j == 0) k = 0;
                                    else k = i + j - 1;

                                    if (number[k] != '1' && number[k] != '0')
                                        doc += "mốt ";
                                    else
                                        doc += cs[1] + " ";
                                }
                                break;
                            case '5':
                                if (i + j == len - 1)
                                    doc += "lăm ";
                                else
                                    doc += cs[5] + " ";
                                break;
                            default:
                                doc += cs[(int)number[i + j] - 48] + " ";
                                break;
                        }

                        //Doc don vi nho
                        if (ddv == 1)
                        {
                            doc += dv[n - j - 1] + " ";
                        }
                    }
                }


                //Doc don vi lon
                if (len - i - n > 0)
                {
                    if ((len - i - n) % 9 == 0)
                    {
                        if (rd == 1)
                            for (k = 0; k < (len - i - n) / 9; k++)
                                doc += "tỉ ";
                        rd = 0;
                    }
                    else
                        if (found != 0) doc += dv[((len - i - n + 1) % 9) / 3 + 2] + " ";
                }

                i += n;
            }

            if (len == 1)
                if (number[0] == '0' || number[0] == '5') return cs[(int)number[0] - 48];
            string c = doc[0].ToString();

            doc = doc.Remove(0, 1);
            doc = doc.Insert(0, c.ToUpper());
            return doc;
        }

        private void rptHoaDonDo_KingPro_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            PrintingSystem.AddCommandHandler(new ExportToImageCommandHandler());
        }
        /// <summary>
        /// ExportToImageCommandHandler
        /// </summary>
        public class ExportToImageCommandHandler : DevExpress.XtraPrinting.ICommandHandler
        {
            /// <summary>
            /// HandleCommand
            /// </summary>
            /// <param name="command"></param>
            /// <param name="args"></param>
            /// <param name="control"></param>
            /// <param name="handled"></param>
            public virtual void HandleCommand(PrintingSystemCommand command, object[] args, IPrintControl control, ref bool handled)
            {
                if (!CanHandleCommand(command, control)) return;
                rptHoaDonDo_ThanhThoi report = new rptHoaDonDo_ThanhThoi();
                DataTable dtdulieu = new DataTable();
                dtdulieu.Columns.Add("STT", typeof(string));
                dtdulieu.Columns.Add("TENHANGHOA", typeof(string));
                dtdulieu.Columns.Add("DVT", typeof(string));
                dtdulieu.Columns.Add("SOLUONG", typeof(string));
                dtdulieu.Columns.Add("DONGIA", typeof(string));
                dtdulieu.Columns.Add("THANHTIEN", typeof(string));

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["TENHANGHOA"].ToString().Length <= 33)
                    {
                        DataRow dr = dtdulieu.NewRow();
                        dr["STT"] = dt.Rows[i]["STT"].ToString();
                        dr["TENHANGHOA"] = dt.Rows[i]["TENHANGHOA"].ToString();
                        dr["DVT"] = dt.Rows[i]["DVT"].ToString();
                        dr["SOLUONG"] = dt.Rows[i]["SOLUONG"].ToString();
                        dr["DONGIA"] = dt.Rows[i]["DONGIA"].ToString();
                        dr["THANHTIEN"] = dt.Rows[i]["THANHTIEN"].ToString();
                        dtdulieu.Rows.Add(dr);
                    }
                    else
                    {
                        string tenhang = dt.Rows[i]["TENHANGHOA"].ToString();
                        int bienlap = tenhang.Length / 33 + 1;
                        string tenhangcat = "";
                        string tenhangconlai = "";
                        int sdu = 0;
                        for (int j = 0; j < bienlap; j++)
                        {
                            if (j == 0) // lần đầu cắt ghi đầy đủ giá trị
                            {
                                int indexcuoi = 32;
                                do
                                {
                                    if (tenhang.Substring(indexcuoi, 1) == " ")//substring bắt đầu từ 0, = "" cắt chuỗi khỏi đứt chữ
                                    {
                                        DataRow dr = dtdulieu.NewRow();
                                        tenhangcat = tenhang.Substring(0, indexcuoi);
                                        tenhangconlai = tenhang.Substring(indexcuoi + 1);
                                        dr["STT"] = dt.Rows[i]["STT"].ToString();
                                        dr["TENHANGHOA"] = tenhangcat;
                                        dr["DVT"] = dt.Rows[i]["DVT"].ToString();
                                        dr["SOLUONG"] = dt.Rows[i]["SOLUONG"].ToString();
                                        dr["DONGIA"] = dt.Rows[i]["DONGIA"].ToString();
                                        dr["THANHTIEN"] = dt.Rows[i]["THANHTIEN"].ToString();
                                        dtdulieu.Rows.Add(dr);
                                    }
                                    else
                                    {
                                        sdu += 1;
                                    }
                                    indexcuoi -= 1;
                                }
                                while (tenhangcat == "");
                                tenhangcat = "";
                            }
                            else
                            {
                                int indexcuoi = 32;
                                do
                                {
                                    if (tenhangconlai.Length > 33)
                                    {
                                        if (tenhangconlai.Substring(indexcuoi, 1) == " ")//substring bắt đầu từ 0
                                        {
                                            DataRow dr = dtdulieu.NewRow();
                                            tenhangcat = tenhangconlai.Substring(0, indexcuoi);
                                            tenhangconlai = tenhangconlai.Substring(indexcuoi + 1);
                                            dr["STT"] = "";
                                            dr["TENHANGHOA"] = tenhangcat;
                                            dr["SOLUONG"] = "";
                                            dr["DVT"] = "";
                                            dr["DONGIA"] = "";
                                            dr["THANHTIEN"] = "";
                                            dtdulieu.Rows.Add(dr);
                                        }
                                        else
                                        {
                                            sdu += 1;
                                        }
                                    }
                                    else
                                    {
                                        DataRow dr = dtdulieu.NewRow();
                                        tenhangcat = tenhangconlai;
                                        dr["STT"] = "";
                                        dr["TENHANGHOA"] = tenhangcat;
                                        dr["DVT"] = "";
                                        dr["SOLUONG"] = "";
                                        dr["DONGIA"] = "";
                                        dr["THANHTIEN"] = "";
                                        dtdulieu.Rows.Add(dr);
                                    }
                                    indexcuoi -= 1;
                                }
                                while (tenhangcat == "");
                                tenhangcat = "";
                            }

                            if (sdu >= 20)
                            {
                                bienlap += 1;
                                sdu = 0;
                            }
                        }
                    }
                }
                if (dtdulieu.Rows.Count < 12)//nếu dữ liệu chưa đủ 12 dòng add thêm dòng trống (vì hóa đơn đỏ tối đa 13 dòng)
                {
                    int sodongtrong = 12 - dtdulieu.Rows.Count;
                    for (int i = 0; i < sodongtrong; i++)
                    {
                        DataRow dr = dtdulieu.NewRow();
                        dr["STT"] = "";
                        dr["TENHANGHOA"] = "";
                        dr["DVT"] = "";
                        dr["SOLUONG"] = "";
                        dr["DONGIA"] = "";
                        dr["THANHTIEN"] = "";
                        dtdulieu.Rows.Add(dr);
                    }
                }
                //ghi dữ liệu
                //STT
                report.xrTableCell1.Text = dtdulieu.Rows[0]["STT"].ToString();
                report.xrTableCell7.Text = dtdulieu.Rows[1]["STT"].ToString();
                report.xrTableCell13.Text = dtdulieu.Rows[2]["STT"].ToString();
                report.xrTableCell19.Text = dtdulieu.Rows[3]["STT"].ToString();
                report.xrTableCell25.Text = dtdulieu.Rows[4]["STT"].ToString();
                report.xrTableCell31.Text = dtdulieu.Rows[5]["STT"].ToString();
                report.xrTableCell37.Text = dtdulieu.Rows[6]["STT"].ToString();
                report.xrTableCell43.Text = dtdulieu.Rows[7]["STT"].ToString();
                report.xrTableCell49.Text = dtdulieu.Rows[8]["STT"].ToString();
                report.xrTableCell55.Text = dtdulieu.Rows[9]["STT"].ToString();
                report.xrTableCell61.Text = dtdulieu.Rows[10]["STT"].ToString();
                report.xrTableCell67.Text = dtdulieu.Rows[11]["STT"].ToString();
                //Ten hàng hóa
                report.xrTableCell2.Text = dtdulieu.Rows[0]["TENHANGHOA"].ToString();
                report.xrTableCell8.Text = dtdulieu.Rows[1]["TENHANGHOA"].ToString();
                report.xrTableCell14.Text = dtdulieu.Rows[2]["TENHANGHOA"].ToString();
                report.xrTableCell20.Text = dtdulieu.Rows[3]["TENHANGHOA"].ToString();
                report.xrTableCell26.Text = dtdulieu.Rows[4]["TENHANGHOA"].ToString();
                report.xrTableCell32.Text = dtdulieu.Rows[5]["TENHANGHOA"].ToString();
                report.xrTableCell38.Text = dtdulieu.Rows[6]["TENHANGHOA"].ToString();
                report.xrTableCell44.Text = dtdulieu.Rows[7]["TENHANGHOA"].ToString();
                report.xrTableCell50.Text = dtdulieu.Rows[8]["TENHANGHOA"].ToString();
                report.xrTableCell56.Text = dtdulieu.Rows[9]["TENHANGHOA"].ToString();
                report.xrTableCell62.Text = dtdulieu.Rows[10]["TENHANGHOA"].ToString();
                report.xrTableCell68.Text = dtdulieu.Rows[11]["TENHANGHOA"].ToString();
                //Don vi tính
                report.xrTableCell3.Text = dtdulieu.Rows[0]["DVT"].ToString();
                report.xrTableCell9.Text = dtdulieu.Rows[1]["DVT"].ToString();
                report.xrTableCell15.Text = dtdulieu.Rows[2]["DVT"].ToString();
                report.xrTableCell21.Text = dtdulieu.Rows[3]["DVT"].ToString();
                report.xrTableCell27.Text = dtdulieu.Rows[4]["DVT"].ToString();
                report.xrTableCell33.Text = dtdulieu.Rows[5]["DVT"].ToString();
                report.xrTableCell39.Text = dtdulieu.Rows[6]["DVT"].ToString();
                report.xrTableCell45.Text = dtdulieu.Rows[7]["DVT"].ToString();
                report.xrTableCell51.Text = dtdulieu.Rows[8]["DVT"].ToString();
                report.xrTableCell57.Text = dtdulieu.Rows[9]["DVT"].ToString();
                report.xrTableCell63.Text = dtdulieu.Rows[10]["DVT"].ToString();
                report.xrTableCell69.Text = dtdulieu.Rows[11]["DVT"].ToString();
                //So luong
                report.xrTableCell4.Text = dtdulieu.Rows[0]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[0]["SOLUONG"].ToString()))).Replace(",", ".");
                report.xrTableCell10.Text = dtdulieu.Rows[1]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[1]["SOLUONG"].ToString()))).Replace(",", ".");
                report.xrTableCell16.Text = dtdulieu.Rows[2]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[2]["SOLUONG"].ToString()))).Replace(",", ".");
                report.xrTableCell22.Text = dtdulieu.Rows[3]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[3]["SOLUONG"].ToString()))).Replace(",", ".");
                report.xrTableCell28.Text = dtdulieu.Rows[4]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[4]["SOLUONG"].ToString()))).Replace(",", ".");
                report.xrTableCell34.Text = dtdulieu.Rows[5]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[5]["SOLUONG"].ToString()))).Replace(",", ".");
                report.xrTableCell40.Text = dtdulieu.Rows[6]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[6]["SOLUONG"].ToString()))).Replace(",", ".");
                report.xrTableCell46.Text = dtdulieu.Rows[7]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[7]["SOLUONG"].ToString()))).Replace(",", ".");
                report.xrTableCell52.Text = dtdulieu.Rows[8]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[8]["SOLUONG"].ToString()))).Replace(",", ".");
                report.xrTableCell58.Text = dtdulieu.Rows[9]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[9]["SOLUONG"].ToString()))).Replace(",", ".");
                report.xrTableCell64.Text = dtdulieu.Rows[10]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[10]["SOLUONG"].ToString()))).Replace(",", ".");
                report.xrTableCell70.Text = dtdulieu.Rows[11]["SOLUONG"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[11]["SOLUONG"].ToString()))).Replace(",", ".");
                //Đơn giá
                report.xrTableCell5.Text = dtdulieu.Rows[0]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[0]["DONGIA"].ToString()))).Replace(",", ".");
                report.xrTableCell11.Text = dtdulieu.Rows[1]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[1]["DONGIA"].ToString()))).Replace(",", ".");
                report.xrTableCell17.Text = dtdulieu.Rows[2]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[2]["DONGIA"].ToString()))).Replace(",", ".");
                report.xrTableCell23.Text = dtdulieu.Rows[3]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[3]["DONGIA"].ToString()))).Replace(",", ".");
                report.xrTableCell29.Text = dtdulieu.Rows[4]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[4]["DONGIA"].ToString()))).Replace(",", ".");
                report.xrTableCell35.Text = dtdulieu.Rows[5]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[5]["DONGIA"].ToString()))).Replace(",", ".");
                report.xrTableCell41.Text = dtdulieu.Rows[6]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[6]["DONGIA"].ToString()))).Replace(",", ".");
                report.xrTableCell47.Text = dtdulieu.Rows[7]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[7]["DONGIA"].ToString()))).Replace(",", ".");
                report.xrTableCell53.Text = dtdulieu.Rows[8]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[8]["DONGIA"].ToString()))).Replace(",", ".");
                report.xrTableCell59.Text = dtdulieu.Rows[9]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[9]["DONGIA"].ToString()))).Replace(",", ".");
                report.xrTableCell65.Text = dtdulieu.Rows[10]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[10]["DONGIA"].ToString()))).Replace(",", ".");
                report.xrTableCell71.Text = dtdulieu.Rows[11]["DONGIA"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[11]["DONGIA"].ToString()))).Replace(",", ".");
                //Thành tiền
                report.xrTableCell6.Text = dtdulieu.Rows[0]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[0]["THANHTIEN"].ToString()))).Replace(",", ".");
                report.xrTableCell12.Text = dtdulieu.Rows[1]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[1]["THANHTIEN"].ToString()))).Replace(",", ".");
                report.xrTableCell18.Text = dtdulieu.Rows[2]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[2]["THANHTIEN"].ToString()))).Replace(",", ".");
                report.xrTableCell24.Text = dtdulieu.Rows[3]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[3]["THANHTIEN"].ToString()))).Replace(",", ".");
                report.xrTableCell30.Text = dtdulieu.Rows[4]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[4]["THANHTIEN"].ToString()))).Replace(",", ".");
                report.xrTableCell36.Text = dtdulieu.Rows[5]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[5]["THANHTIEN"].ToString()))).Replace(",", ".");
                report.xrTableCell42.Text = dtdulieu.Rows[6]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[6]["THANHTIEN"].ToString()))).Replace(",", ".");
                report.xrTableCell48.Text = dtdulieu.Rows[7]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[7]["THANHTIEN"].ToString()))).Replace(",", ".");
                report.xrTableCell54.Text = dtdulieu.Rows[8]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[8]["THANHTIEN"].ToString()))).Replace(",", ".");
                report.xrTableCell60.Text = dtdulieu.Rows[9]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[9]["THANHTIEN"].ToString()))).Replace(",", ".");
                report.xrTableCell66.Text = dtdulieu.Rows[10]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[10]["THANHTIEN"].ToString()))).Replace(",", ".");
                report.xrTableCell72.Text = dtdulieu.Rows[11]["THANHTIEN"].ToString() == "" ? "" : Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dtdulieu.Rows[11]["THANHTIEN"].ToString()))).Replace(",", ".");

                report.VAT.Text = Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dt.Rows[0]["VAT"].ToString()))).Replace(",", ".");
                report.txt_thanhtientongcong.Text = Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dt.Rows[0]["THANHTIENTONG"].ToString()))).Replace(",", ".");
                report.txt_tongtienthue.Text = Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dt.Rows[0]["TIENVATTONG"].ToString()))).Replace(",", ".");
                report.txt_tongtiensauthue.Text = Convert.ToString(string.Format("{0:#,###0}", decimal.Parse(dt.Rows[0]["TONGCONG"].ToString()))).Replace(",", ".");

                report.txt_tennguoimua.Text = dt.Rows[0]["HOTENGUOIMUA"].ToString();
                report.txt_mst.Text = dt.Rows[0]["MST"].ToString();
                report.txt_httt.Text = dt.Rows[0]["HTTT"].ToString();
                report.txt_ngay.Text = dt.Rows[0]["NGAY"].ToString();
                report.txt_thang.Text = dt.Rows[0]["THANG"].ToString();
                report.txt_nam.Text = dt.Rows[0]["NAM"].ToString();

                //Nếu tên đơn vị mua dài hơn 65 ký tự sẽ ghi xuống ô MST, phía sau 
                string tendonvi = dt.Rows[0]["DONVIMUA"].ToString();
                if (tendonvi.Length <= 65)
                {
                    report.txt_donvi.Text = tendonvi;
                }
                else
                {
                    int index = 65;
                    do
                    {
                        if (tendonvi.Substring(index, 1) == " ")
                        {
                            report.txt_donvi.Text = tendonvi.Substring(0, index);
                            report.txt_tendonvidai.Text = tendonvi.Substring(index + 1);
                        }
                        index -= 1;
                    }
                    while (report.txt_donvi.Text == "");
                }
                //Nếu địa chỉ dài hơn 85 ký tự sẽ ghi xuống ô số tài khoản, ô STK không sử dụng
                string diachi = dt.Rows[0]["DIACHI"].ToString();
                if (diachi.Length <= 85)
                {
                    report.txt_diachi1.Text = diachi;
                    report.txt_diachi2.Text = "";
                    report.txt_sotaikhoan.Text = dt.Rows[0]["SOTK"].ToString();
                }
                else
                {
                    int index = 85;
                    do
                    {
                        if (diachi.Substring(index, 1) == " ")
                        {
                            report.txt_diachi1.Text = diachi.Substring(0, index);
                            report.txt_diachi2.Text = diachi.Substring(index + 1);
                            report.txt_sotaikhoan.Text = dt.Rows[0]["SOTK"].ToString();
                        }
                        index -= 1;
                    }
                    while (report.txt_diachi1.Text == "");
                }

                string tongtien = Math.Round(decimal.Parse(dt.Rows[0]["TONGCONG"].ToString())).ToString();
                string chuoichutien = ChuyenSo(tongtien) + "đồng";

                if (chuoichutien.Length <= 60)
                {
                    report.txt_doctien1.Text = chuoichutien;
                    report.txt_doctien2.Text = "";
                }
                else
                {
                    int index = 60;
                    do
                    {
                        if (chuoichutien.Substring(index, 1) == " ")
                        {
                            report.txt_doctien1.Text = chuoichutien.Substring(0, index);
                            report.txt_doctien2.Text = chuoichutien.Substring(index + 1);
                        }
                        index -= 1;
                    }
                    while (report.txt_doctien1.Text == "");
                }

                report.lb_hoadongiatrigiatang.Visible = false;
                report.lb_vat.Visible = false;
                report.lb_giaonguoimua.Visible = false;
                report.lb_customer.Visible = false;
                report.lb_mauso.Visible = false;
                report.lb_kyhieu.Visible = false;
                report.lb_so.Visible = false;
                report.lb_ngay.Visible = false;
                report.lb_tennguoimua.Visible = false;
                report.lb_tendonvi.Visible = false;
                report.lb_mst.Visible = false;
                report.lb_diachi.Visible = false;
                report.lb_httt.Visible = false;
                report.lb_sotaikhoan.Visible = false;
                report.lb_congtienhang.Visible = false;
                report.lb_thuesuat.Visible = false;
                report.lb_tienthue.Visible = false;
                report.lb_tongcongtienthanhtoan.Visible = false;
                report.lb_tienchu.Visible = false;
                report.lb_stt.Visible = false;
                report.lb_tenhang.Visible = false;
                report.lb_dvt.Visible = false;
                report.lb_soluong.Visible = false;
                report.lb_dongia.Visible = false;
                report.lb_thanhtien.Visible = false;
                report.lb_nguoimuahang.Visible = false;
                report.lb_nguoibanhang.Visible = false;
                report.lb_nguoibanky.Visible = false;
                report.lb_nguoimuaky.Visible = false;

                if (command == PrintingSystemCommand.Print) //(print có dấu ?) bật form chọn máy in
                    report.PrintDialog();
                else
                    report.Print();
                handled = true;
            }
            /// <summary>
            /// CanHandleCommand
            /// </summary>
            /// <param name="command"></param>
            /// <param name="control"></param>
            /// <returns></returns>
            public virtual bool CanHandleCommand(PrintingSystemCommand command, IPrintControl control)
            {
                // This handler is used for the ExportGraphic command.
                //return command == PrintingSystemCommand.Print; //Print (print có dấu ?)
                //return command == PrintingSystemCommand.PrintDirect; //Quick Print (print ko dấu ?)
                return command == PrintingSystemCommand.PrintDirect || command == PrintingSystemCommand.Print;
            }
        }
    }
}
