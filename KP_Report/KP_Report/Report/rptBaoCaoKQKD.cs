﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using System.Drawing.Imaging;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBaoCaoKQKD : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBaoCaoKQKD()
        {
            InitializeComponent();
            Setheader();
        }
        private void ConvertHinh(ref PictureBox pic, Byte[] values)
        {
            MemoryStream ms = new MemoryStream(values);
            Bitmap bm = new Bitmap(ms);
            pic.Image = bm;
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            MAVACH.DataBindings.Add("Text", DataSource, "MAVACH");
            TENHANGHOA.DataBindings.Add("Text", DataSource, "TEN_HANGHOA");
            DVT.DataBindings.Add("Text", DataSource, "TEN_DONVITINH");
            SL.DataBindings.Add("Text", DataSource, "SOLUONGBAN", "{0:#,###0}");
            GIABAN.DataBindings.Add("Text", DataSource, "DONGIA", "{0:#,###0.00}");
            GIAVON.DataBindings.Add("Text", DataSource, "GIAVON", "{0:#,###0.00}");
            GIATHUC.DataBindings.Add("Text", DataSource, "GIABAN_THAT", "{0:#,###0.00}");
            LAI.DataBindings.Add("Text", DataSource, "LAI", "{0:#,###0.00}");
            TONGLAI.DataBindings.Add("Text", DataSource, "TONGTIENLAI", "{0:#,###0.00}");
            LO.DataBindings.Add("Text", DataSource, "LO", "{0:#,###0.00}");
            TONGLO.DataBindings.Add("Text", DataSource, "TONGTIENLO", "{0:#,###0.00}");
            
            SUMSL.DataBindings.Add("Text", DataSource, "SOLUONGBAN");
            SUMSL.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");
            SUMTHANHTIEN.DataBindings.Add("Text", DataSource, "TONGTIENLAI");
            SUMTHANHTIEN.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0.00}");
            SUMTONGLO.DataBindings.Add("Text", DataSource, "TONGTIENLO");
            SUMTONGLO.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0.00}");

            DataTable dtSource = (DataTable)DataSource;
            if (dtSource.Rows[0]["TYPE"].ToString() == "NH")
            {
                txt_tieude.Text = "( Nhóm: " + dtSource.Rows[0]["NAMETYPE"].ToString()+" )";
            }
            else if (dtSource.Rows[0]["TYPE"].ToString() == "CH")
            {
                txt_tieude.Text = "(  Cửa hàng: " + dtSource.Rows[0]["NAMETYPE"].ToString() + " )";
            }
            else if (dtSource.Rows[0]["TYPE"].ToString() == "K")
            {
                txt_tieude.Text = "(  Kho: " + dtSource.Rows[0]["NAMETYPE"].ToString() + " )";
            }
            else if (dtSource.Rows[0]["TYPE"].ToString() == "Q")
            {
                txt_tieude.Text = "(  Quầy: " + dtSource.Rows[0]["NAMETYPE"].ToString() + " )";
            }
            else if (dtSource.Rows[0]["TYPE"].ToString() == "NV")
            {
                txt_tieude.Text = "(  Nhân viên: " + dtSource.Rows[0]["NAMETYPE"].ToString() + " )";
            }
            else if (dtSource.Rows[0]["TYPE"].ToString() == "MH")
            {
                txt_tieude.Text = "(  Mặt hàng: " + dtSource.Rows[0]["NAMETYPE"].ToString() + " )";
            }
            txt_tieude1.Text = "( Từ ngày: " + dtSource.Rows[0]["TUNGAY"].ToString() + " - Đến ngày: " + dtSource.Rows[0]["DENNGAY"].ToString() + " )";
        }
        int i = 0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
        

    }
}
