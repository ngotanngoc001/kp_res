﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using System.Drawing.Imaging;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBienBanGiaoNhan : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBienBanGiaoNhan()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            TEN.DataBindings.Add("Text", DataSource, "TEN");
            COQUANCAP.DataBindings.Add("Text", DataSource, "COQUANCAP");
            SOHIEU.DataBindings.Add("Text", DataSource, "SOHIEU");
            BANCHINH.DataBindings.Add("Text", DataSource, "BANCHINH");
            BANPHU.DataBindings.Add("Text", DataSource, "BANPHU");
            NGAYCAP.DataBindings.Add("Text", DataSource, "NGAYCAP");
            NGAYHETHAN.DataBindings.Add("Text", DataSource, "NGAYHETHAN");

            SO_KYHIEU.DataBindings.Add("Text", DataSource, "SO_KYHIEU");
            BENGIAO.DataBindings.Add("Text", DataSource, "BENGIAO");
            CV_BENGIAO.DataBindings.Add("Text", DataSource, "CV_BENGIAO");
            BENNHAN.DataBindings.Add("Text", DataSource, "BENNHAN");
            CV_BENNHAN.DataBindings.Add("Text", DataSource, "CV_BENNHAN");
            NGAYNHAN.DataBindings.Add("Text", DataSource, "NGAYNHAN", "{0:dd/MM/yyyy}");
            NOIDUNG.DataBindings.Add("Text", DataSource, "NOIDUNG");
        }

        int i = 0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
