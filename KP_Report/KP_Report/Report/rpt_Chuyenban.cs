﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using System.Data;
using KP_Report;
using KP_UserManagement;
using System.IO;
using System.Windows.Forms;


namespace KP_Report
{
    public partial class rpt_Chuyenban : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// rpt_Order
        /// </summary>
        public rpt_Chuyenban()
        {
            InitializeComponent();                      
            Setheader();            
            

        }
        /// <summary>
        /// BindData
        /// </summary>
        public void BindData()
        {
            //lb_NameBill.DataBindings.Add("Text", DataSource, "Ten_HoaDon");
    
  
            //header
            lb_banchuyen.DataBindings.Add("Text", DataSource, "BANCHUYEN");
            
            lb_bangoc.DataBindings.Add("Text", DataSource, "BANGOC");
            lb_NameBill.DataBindings.Add("Text", DataSource, "TITLE");
            lbTachBan.DataBindings.Add("Text", DataSource, "GIUA");
        } 
        
         private void Setheader()
        {
            StreamReader str = new StreamReader(Application.StartupPath + "/systeminfo.txt");
            string sMacuahang = "";
            string line;
            while ((line = str.ReadLine()) != null)
            {
                string[] value = line.Split(',');
                sMacuahang = value[0].ToString();
            }

            string sSQL = "";
            sSQL += "Select MA_CUAHANG,	TEN_CUAHANG,	GHICHU,	SUDUNG,	Header,	Footer" + "\n";
            sSQL += "From CUAHANG" + "\n";
            sSQL += "Where MA_CUAHANG  =" + clsMain.SQLString(sMacuahang) + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            lb_Header.Text = dt.Rows[0]["Header"].ToString();
            //lbFooter.Text = dt.Rows[0]["Footer"].ToString();  
        }      
    }
}
