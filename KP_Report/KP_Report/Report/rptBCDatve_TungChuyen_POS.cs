﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBCDatve_TungChuyen_POS : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBCDatve_TungChuyen_POS()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        int i=0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            TENTUYEN1.DataBindings.Add("Text", DataSource, "TENTUYEN");
            TENDOITUONG1.DataBindings.Add("Text", DataSource, "TENDOITUONG");
            NGAYDI1.DataBindings.Add("Text", DataSource, "NGAYDI", "{0:dd/MM/yyyy}");
            GIODI1.DataBindings.Add("Text", DataSource, "GIODI");
            MABARCODE.DataBindings.Add("Text", DataSource, "MABARCODE");
            TENHANHKHACH.DataBindings.Add("Text", DataSource, "TENHANHKHACH");
          //  DONGIA.DataBindings.Add("Text", DataSource, "THANHTIEN", "{0:n0}");
            DATHANHTOAN.DataBindings.Add("Text", DataSource, "DATHANHTOAN", "{0:n0}");
            DIENTHOAI.DataBindings.Add("Text", DataSource, "DIENTHOAI");
            LOAIXE.DataBindings.Add("Text", DataSource, "SOXE");
            SOKHACH.DataBindings.Add("Text", DataSource, "SOGHE");
            PHIEUTHU.DataBindings.Add("Text", DataSource, "HTTT");
            NhanVien.DataBindings.Add("Text", DataSource, "NGUOITAO");
            Ngay.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy HH:mm}");
            TONGKHACH.DataBindings.Add("Text", DataSource, "SOGHE");
            TONGKHACH.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
           // TONGTIEN.DataBindings.Add("Text", DataSource, "THANHTIEN");
           // TONGTIEN.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
           // TONGDATHANHTOAN.DataBindings.Add("Text", DataSource, "DATHANHTOAN");
           // TONGDATHANHTOAN.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
        }
    }
}
