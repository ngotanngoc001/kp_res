﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptTinhhinhhang2 : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptTinhhinhhang2()
        {
            InitializeComponent();
            GroupField grf = new GroupField("TENPHIEU");
            GroupHeader1.GroupFields.Add(grf);
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }

        int i ;
        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i = 0;
        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            TENTUYEN.DataBindings.Add("Text", DataSource, "TENTUYEN");
            NGAYDI.DataBindings.Add("Text", DataSource, "NGAYDI", "{0:dd/MM/yyyy}");
            GIODI.DataBindings.Add("Text", DataSource, "GIODI");
            TENDOITUONG.DataBindings.Add("Text", DataSource, "TENDOITUONG");
            TENNGUOIGUI.DataBindings.Add("Text", DataSource, "TENNGUOIGUI");
            DIENTHOAINGUOIGUI.DataBindings.Add("Text", DataSource, "DIENTHOAINGUOIGUI");
            GHICHU.DataBindings.Add("Text", DataSource, "GHICHU");
            BIENSO.DataBindings.Add("Text", DataSource, "BIENSO");
            TONGSOXE.DataBindings.Add("Text", DataSource, "DONGIA");
            TONGSOXE.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Count, "{0:n0}");
            TENPHIEU.DataBindings.Add("Text", DataSource, "TENPHIEU");
        }
     
    }
}
