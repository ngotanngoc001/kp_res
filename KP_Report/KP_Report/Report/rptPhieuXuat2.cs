﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptPhieuXuat2 : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptPhieuXuat2()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            stt.DataBindings.Add("Text", DataSource, "STT");
            MA.DataBindings.Add("Text", DataSource, "MA_HANGHOA");
            TENHANGHOA.DataBindings.Add("Text", DataSource, "TEN_HANGHOA");
            DVT.DataBindings.Add("Text", DataSource, "TEN_DONVITINH");
            SLTONCUOIKY.DataBindings.Add("Text", DataSource, "SLTONCUOIKY", "{0:#,###0.00}");
            SL.DataBindings.Add("Text", DataSource, "SOLUONG", "{0:#,###0.00}");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:#,###0.00}");
            THANHTIEN.DataBindings.Add("Text", DataSource, "THANHTIEN", "{0:#,###0.00}");

            txt_sltontongcong.DataBindings.Add("Text", DataSource, "SLTONCUOIKY");
            txt_sltontongcong.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0.00}");
            txt_sltongcong.DataBindings.Add("Text", DataSource, "SLTONG", "{0:#,###0.00}");
            txt_thanhtientongcong.DataBindings.Add("Text", DataSource, "THANHTIENTONG", "{0:#,###0.00}");
            txt_tongtienthue.DataBindings.Add("Text", DataSource, "TIENVATTONG", "{0:#,###0.00}");
            txt_tongtiensauthue.DataBindings.Add("Text", DataSource, "TONGCONGSAUTHUE", "{0:#,###0.00}");
            txt_tengiaodich.DataBindings.Add("Text", DataSource, "TENDT");
            txt_diachi.DataBindings.Add("Text", DataSource, "DIACHIDT");
            txt_sdt.DataBindings.Add("Text", DataSource, "DIENTHOAIDT");
            txt_tenkho.DataBindings.Add("Text", DataSource, "TENKHO");
            txt_ghichu.DataBindings.Add("Text", DataSource, "GHICHU");
            txt_phuongthuc.DataBindings.Add("Text", DataSource, "TENPT");
            txt_sophieu.DataBindings.Add("Text", DataSource, "SOPHIEU");
            txt_ngayxuat.DataBindings.Add("Text", DataSource, "NGAYCHUNGTU", "{0:dd/MM/yyyy}");
           
        }
    }
}
