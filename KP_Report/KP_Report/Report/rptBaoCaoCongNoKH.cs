﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using System.Drawing.Imaging;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;


namespace KP_Report
{
    /// <summary>
    /// rptBaoCaoCongNoKH
    /// </summary>
    public partial class rptBaoCaoCongNoKH : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// rptBaoCaoCongNoKH
        /// </summary>
        public rptBaoCaoCongNoKH()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// ConvertHinh
        /// </summary>
        /// <param name="pic"></param>
        /// <param name="values"></param>
        public static void ConvertHinh(ref PictureBox pic, Byte[] values)
        {
            MemoryStream ms = new MemoryStream(values);
            Bitmap bm = new Bitmap(ms);
            pic.Image = bm;
        }
        private void Setheader()
        {

            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,EMAIL,SOTAIKHOAN,MASOTHUE,GIAMDOC,KETOAN,THUKHO" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
            // txt_ketoantruong.Text = dt.Rows[0]["KETOAN"].ToString();
            txt_giamdoc.Text = dt.Rows[0]["GIAMDOC"].ToString();
            // txt_footerreport.Text = dt.Rows[0]["FOOTERREPORT"].ToString();
            //copy copy
            //txtTencongty1.Text = dt.Rows[0]["TENCONGTY"].ToString();
            //txtDiachi1.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            //txtDienthoai1.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
            //txt_ketoantruong1.Text = dt.Rows[0]["KETOAN"].ToString();
            //txt_giamdoc1.Text = dt.Rows[0]["GIAMDOC"].ToString();
        }
        ///
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            DataTable dtSource = (DataTable)DataSource;

            //Chi.DataBindings.Add("Text", DataSource, "SoTienChi", "{0:0,0}");
            DT_HOTEN.DataBindings.Add("Text", DataSource, "DT_HOTEN");

            TongTienNo.DataBindings.Add("Text", DataSource, "TongTienNo", "{0:#,#}");
            DaThanhToan.DataBindings.Add("Text", DataSource, "DaThanhToan", "{0:#,#}");
            NoConLai.DataBindings.Add("Text", DataSource, "NoConLai", "{0:#,#}");
            //KemTheo.DataBindings.Add("Text", DataSource, "KemTheo");

            ngay1.Text = dtSource.Rows[0]["Ngay"].ToString();
            thang1.Text = dtSource.Rows[0]["Thang"].ToString();
            if (dtSource.Rows[0]["TuNgay"].ToString() == "")
            {
                xrTable9.Visible = false;
            }
            else
            {
                xrTable9.Visible = true;
                tuNgay.Text = dtSource.Rows[0]["TuNgay"].ToString();
                denNgay.Text = dtSource.Rows[0]["DenNgay"].ToString();
            }
            nam1.Text = dtSource.Rows[0]["Nam"].ToString();
            NguoiLap.Text = dtSource.Rows[0]["NguoiLap"].ToString();

            TongNo.DataBindings.Add("Text", DataSource, "TongTienNo");
            TongNo.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,#}");

            TongThanhToan.DataBindings.Add("Text", DataSource, "DaThanhToan");
            TongThanhToan.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,#}");

            TongNoConLai.DataBindings.Add("Text", DataSource, "NoConLai");
            TongNoConLai.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,#}");



            //  TongChi.DataBindings.Add("Text", DataSource, "NoChi");
            //TongChi.Text = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum , "{0:n0}"); 
            //TongThu.Text = 
            //TongDu.Text = 


        }
        int i = 0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
