﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace KP_Report
{
    /// <summary>
    /// Report Ve
    /// </summary>
    public partial class rptVeXeMay : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report Ve
        /// </summary>
        public rptVeXeMay()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            MABARCODE.DataBindings.Add("Text", DataSource, "MABARCODE");
            MABARCODE1.DataBindings.Add("Text", DataSource, "MABARCODE");
            TENTUYEN.DataBindings.Add("Text", DataSource, "TENTUYEN");
            TENTUYEN1.DataBindings.Add("Text", DataSource, "TENTUYEN");
            TENDOITUONG.DataBindings.Add("Text", DataSource, "TENDOITUONG");
            TENDOITUONG1.DataBindings.Add("Text", DataSource, "TENDOITUONG");
            BIENSO.DataBindings.Add("Text", DataSource, "BIENSO");
            BIENSO1.DataBindings.Add("Text", DataSource, "BIENSO");
            TENHANHKHACH.DataBindings.Add("Text", DataSource, "TENHANHKHACH");
            TENHANHKHACH1.DataBindings.Add("Text", DataSource, "TENHANHKHACH");
            CMND.DataBindings.Add("Text", DataSource, "CMND");
            NGAYDI.DataBindings.Add("Text", DataSource, "NGAYDI", "{0:dd/MM/yyyy}");
            NGAYDI1.DataBindings.Add("Text", DataSource, "NGAYDI", "{0:dd/MM/yyyy}");
            GIODI.DataBindings.Add("Text", DataSource, "GIODI");
            GIODI1.DataBindings.Add("Text", DataSource, "GIODI");
            NGUOITAO.DataBindings.Add("Text", DataSource, "NGUOITAO");
            NGAYTAO.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy HH : mm}");
            NGUOITAO1.DataBindings.Add("Text", DataSource, "NGUOITAO");
            NGAYTAO1.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy HH : mm}");
            DataTable dt = (DataTable)DataSource;
            MABARCODE2.Text =  dt.Rows[0]["MABARCODE"].ToString() ;
        }
    }
}
