﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using System.Data;
using KP_Report;
using KP_UserManagement;
using System.IO;
using System.Windows.Forms;

namespace KP_Report
{
    public partial class rpt_inhoadonKhachSan : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// rpt_inhoadonKhachSan
        /// </summary>
        public rpt_inhoadonKhachSan()
        {
            InitializeComponent();                      
            Setheader();                        

        }
        /// <summary>
        /// BindData
        /// </summary>
        public void BindData()
        {
            DataTable dtSource = (DataTable)DataSource;
            lb_NameBill.DataBindings.Add("Text", DataSource, "Ten_HoaDon");
           // cl_STT.DataBindings.Add("Text", DataSource, "STT");
            cl_HangHoa.DataBindings.Add("Text", DataSource, "TEN_HANGHOA");
            cl_giaban.DataBindings.Add("Text", DataSource, "GIABAN", "{0:#,###0}");
            cl_soluong.DataBindings.Add("Text", DataSource, "SOLUONG");
            cl_thanhtien.DataBindings.Add("Text", DataSource, "Thanh_Tien", "{0:#,###0}");
            lbTiendatcoc.DataBindings.Add("Text", DataSource, "Tiendatcoc", "{0:#,###0}");
            lbPhihucvu.DataBindings.Add("Text", DataSource, "Phiphucvu", "{0:#,###0}");
            lb_tongcong.DataBindings.Add("Text", DataSource, "Tong_Cong" , "{0:#,###0}");
            lb_tienkhachtra.DataBindings.Add("Text", DataSource, "Tien_KhachTra", "{0:#,###0}");
            lb_conlai.DataBindings.Add("Text", DataSource, "Tien_TraKhach", "{0:#,###0}");

           if (dtSource.Rows.Count > 0)
            {
                if (decimal.Parse(dtSource.Rows[0]["Giam_Gia"].ToString()) > 100)
                {
                    lb_giamgia.Text = string.Format("{0:#,###0}", decimal.Parse(dtSource.Rows[0]["Giam_Gia"].ToString()));
                }
                else
                {
                    lb_giamgia.Text = dtSource.Rows[0]["Giam_Gia"].ToString() + " %";
                }
            }
            lb_Tenban.DataBindings.Add("Text", DataSource, "TEN_BAN");
            lb_Quay.DataBindings.Add("Text", DataSource, "Ma_Quay");
            lb_mahoadon.DataBindings.Add("Text", DataSource, "Ma_HoaDon");
            lb_MaHD.DataBindings.Add("Text", DataSource, "So_HoaDon","*{0}*");
            lb_nhanvien.DataBindings.Add("Text", DataSource, "TenNV");
            lb_ngay.DataBindings.Add("Text", DataSource, "ngayHD", "{0:dd-MM-yyyy HH:mm:ss}");
            //Karaoke
            lbTongthanhtien.DataBindings.Add("Text", DataSource, "Thanh_Tien");
            lbTongthanhtien.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");
            lbTiengio.DataBindings.Add("Text", DataSource, "TienGio", "{0:#,###0}");
            lbNgayvao.DataBindings.Add("Text", DataSource, "CheckIn", "{0:dd/MM/yyyy}");
            lbNgayra.DataBindings.Add("Text", DataSource, "CheckOut", "{0:dd/MM/yyyy}");
            lbGiovao.DataBindings.Add("Text", DataSource, "CheckIn", "{0:HH:mm}");
            lbGiora.DataBindings.Add("Text", DataSource, "CheckOut", "{0:HH:mm}");
            lbGiaithich.DataBindings.Add("Text", DataSource, "GIAITHICH");
        } 

         private void Setheader()
        {
            StreamReader str = new StreamReader(Application.StartupPath + "/systeminfo.txt");
            string sMacuahang = "";
            string line;
            while ((line = str.ReadLine()) != null)
            {
                string[] value = line.Split(',');
                sMacuahang = value[0].ToString();
            }

            string sSQL = "";
            sSQL += "Select MA_CUAHANG,	TEN_CUAHANG,	GHICHU,	SUDUNG,	Header,	Footer" + "\n";
            sSQL += "From CUAHANG" + "\n";
            sSQL += "Where MA_CUAHANG  =" + clsMain.SQLString(sMacuahang) + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            lb_Header.Text = dt.Rows[0]["Header"].ToString();
            lbFooter.Text = dt.Rows[0]["Footer"].ToString();      
        }
    }
}
