﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptDS_Hoadon : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptDS_Hoadon()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            MA_Hoadon.DataBindings.Add("Text", DataSource, "MA_HOADON");
            Nhanvienban.DataBindings.Add("Text", DataSource, "TenNV");
            Ngayban.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy}");
            Tongtien.DataBindings.Add("Text", DataSource, "TONGTIEN", "{0:#,###0}");
            MONEYCASH.DataBindings.Add("Text", DataSource, "MONEYCASH", "{0:#,###0}");
            MONEYVOUCHER.DataBindings.Add("Text", DataSource, "MONEYVOUCHER", "{0:#,###0}");
            MONEYVISA.DataBindings.Add("Text", DataSource, "MONEYVISA", "{0:#,###0}");
            MONEYINNERCARD.DataBindings.Add("Text", DataSource, "MONEYINNERCARD", "{0:#,###0}");
            
            TONGCONG.DataBindings.Add("Text", DataSource, "TONGTIEN");
            TONGCONG.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");
            TONGMONEYCASH.DataBindings.Add("Text", DataSource, "MONEYCASH");
            TONGMONEYCASH.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");
            TONGMONEYVOUCHER.DataBindings.Add("Text", DataSource, "MONEYVOUCHER");
            TONGMONEYVOUCHER.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");
            TONGMONEYVISA.DataBindings.Add("Text", DataSource, "MONEYVISA");
            TONGMONEYVISA.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");
            TONGMONEYINNERCARD.DataBindings.Add("Text", DataSource, "MONEYINNERCARD");
            TONGMONEYINNERCARD.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");

            DataTable dtSource = (DataTable)DataSource;
            txt_tieude1.Text = "(" + dtSource.Rows[0]["FILTER"].ToString()  + " )";
            txt_tieude.Text = "( Từ ngày: " + dtSource.Rows[0]["TUNGAY"].ToString() + " - Đến ngày: " + dtSource.Rows[0]["DENNGAY"].ToString() + " )";
        }

        int i = 0;
        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i = 0;
        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
