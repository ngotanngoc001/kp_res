﻿namespace KP_Report
{
    /// <summary>
    /// rptVerapphim_Bill1
    /// </summary>
    partial class rptVerapphim_Bill1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.BarCode.Code128Generator code128Generator1 = new DevExpress.XtraPrinting.BarCode.Code128Generator();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_ngay = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_TenCty = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_MaSoTheu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_Quay = new DevExpress.XtraReports.UI.XRLabel();
            this.Logo = new System.Windows.Forms.Panel();
            this.winControlContainer1 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.MABARCODE = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_Header = new DevExpress.XtraReports.UI.XRLabel();
            this.TENPHIM = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.BATDAU = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYCHIEU = new DevExpress.XtraReports.UI.XRLabel();
            this.SOGHE = new DevExpress.XtraReports.UI.XRLabel();
            this.TENPHONG = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine6 = new DevExpress.XtraReports.UI.XRLine();
            this.DONGIA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.LOAIVE = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine7 = new DevExpress.XtraReports.UI.XRLine();
            this.NGUOITAO = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYTAO = new DevExpress.XtraReports.UI.XRLabel();
            this.MABARCODE2 = new DevExpress.XtraReports.UI.XRBarCode();
            this.MABARCODE1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbFooter = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 22F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1});
            this.ReportFooter.HeightF = 31.25F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(7.508636F, 0F);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(268.3542F, 31.25F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrLabel4.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(148.3326F, 58.625F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(52F, 17.79163F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseForeColor = false;
            this.xrLabel4.Text = "Ký hiệu:";
            // 
            // lb_ngay
            // 
            this.lb_ngay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ngay.ForeColor = System.Drawing.Color.Blue;
            this.lb_ngay.LocationFloat = new DevExpress.Utils.PointFloat(200.818F, 58.62494F);
            this.lb_ngay.Name = "lb_ngay";
            this.lb_ngay.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_ngay.SizeF = new System.Drawing.SizeF(75.04474F, 17.79163F);
            this.lb_ngay.StylePriority.UseFont = false;
            this.lb_ngay.StylePriority.UseForeColor = false;
            this.lb_ngay.Text = "AB/16T";
            // 
            // lb_TenCty
            // 
            this.lb_TenCty.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_TenCty.ForeColor = System.Drawing.Color.Blue;
            this.lb_TenCty.LocationFloat = new DevExpress.Utils.PointFloat(7.508501F, 13.54167F);
            this.lb_TenCty.Multiline = true;
            this.lb_TenCty.Name = "lb_TenCty";
            this.lb_TenCty.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_TenCty.SizeF = new System.Drawing.SizeF(268.3543F, 19.79168F);
            this.lb_TenCty.StylePriority.UseFont = false;
            this.lb_TenCty.StylePriority.UseForeColor = false;
            this.lb_TenCty.StylePriority.UseTextAlignment = false;
            this.lb_TenCty.Text = "VÉ XEM PHIM";
            this.lb_TenCty.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // lb_MaSoTheu
            // 
            this.lb_MaSoTheu.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_MaSoTheu.ForeColor = System.Drawing.Color.Blue;
            this.lb_MaSoTheu.LocationFloat = new DevExpress.Utils.PointFloat(7.508469F, 33.33335F);
            this.lb_MaSoTheu.Multiline = true;
            this.lb_MaSoTheu.Name = "lb_MaSoTheu";
            this.lb_MaSoTheu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_MaSoTheu.SizeF = new System.Drawing.SizeF(268.3543F, 25.29158F);
            this.lb_MaSoTheu.StylePriority.UseFont = false;
            this.lb_MaSoTheu.StylePriority.UseForeColor = false;
            this.lb_MaSoTheu.StylePriority.UseTextAlignment = false;
            this.lb_MaSoTheu.Text = "Liên 2 : Khách hàng";
            this.lb_MaSoTheu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrLabel15.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(7.508501F, 58.62494F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(52.90821F, 17.79169F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseForeColor = false;
            this.xrLabel15.Text = "Mẫu số:";
            // 
            // lb_Quay
            // 
            this.lb_Quay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Quay.ForeColor = System.Drawing.Color.Blue;
            this.lb_Quay.LocationFloat = new DevExpress.Utils.PointFloat(60.4167F, 58.62494F);
            this.lb_Quay.Name = "lb_Quay";
            this.lb_Quay.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_Quay.SizeF = new System.Drawing.SizeF(87.91591F, 17.79166F);
            this.lb_Quay.StylePriority.UseFont = false;
            this.lb_Quay.StylePriority.UseForeColor = false;
            this.lb_Quay.Text = "01/VE2/002";
            // 
            // Logo
            // 
            this.Logo.Location = new System.Drawing.Point(0, 0);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(258, 12);
            this.Logo.TabIndex = 0;
            // 
            // winControlContainer1
            // 
            this.winControlContainer1.LocationFloat = new DevExpress.Utils.PointFloat(7.508501F, 1.041667F);
            this.winControlContainer1.Name = "winControlContainer1";
            this.winControlContainer1.SizeF = new System.Drawing.SizeF(268.3543F, 12.50002F);
            this.winControlContainer1.WinControl = this.Logo;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrLabel2.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(7.508532F, 76.41662F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(52.90821F, 17.79169F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseForeColor = false;
            this.xrLabel2.Text = "Số:";
            // 
            // MABARCODE
            // 
            this.MABARCODE.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MABARCODE.ForeColor = System.Drawing.Color.Blue;
            this.MABARCODE.LocationFloat = new DevExpress.Utils.PointFloat(60.4167F, 76.41662F);
            this.MABARCODE.Name = "MABARCODE";
            this.MABARCODE.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MABARCODE.SizeF = new System.Drawing.SizeF(87.91591F, 17.79166F);
            this.MABARCODE.StylePriority.UseFont = false;
            this.MABARCODE.StylePriority.UseForeColor = false;
            this.MABARCODE.Text = "100000001";
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrLabel8.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(148.3326F, 76.41662F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(52F, 17.79163F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseForeColor = false;
            this.xrLabel8.Text = "MST:";
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(200.818F, 76.41662F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(75.04474F, 17.79163F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseForeColor = false;
            this.xrLabel12.Text = "0312742744";
            // 
            // lb_Header
            // 
            this.lb_Header.Font = new System.Drawing.Font("Tahoma", 8.5F);
            this.lb_Header.ForeColor = System.Drawing.Color.Blue;
            this.lb_Header.LocationFloat = new DevExpress.Utils.PointFloat(7.508501F, 94.20831F);
            this.lb_Header.Multiline = true;
            this.lb_Header.Name = "lb_Header";
            this.lb_Header.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_Header.SizeF = new System.Drawing.SizeF(268.3541F, 41.95826F);
            this.lb_Header.StylePriority.UseFont = false;
            this.lb_Header.StylePriority.UseForeColor = false;
            this.lb_Header.StylePriority.UseTextAlignment = false;
            this.lb_Header.Text = "CÔNG TY TNHH RẠP CHIẾU PHIM THỂ THAO VÀ GIẢI TRÍ NGÔI SAO ĐÀ LẠT\r\nĐC: Quảng trườn" +
                "g Lâm Viên, P10, TP.Đà Lạt, Lâm Đồng\r\nĐT: 08 7300 8881";
            this.lb_Header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // TENPHIM
            // 
            this.TENPHIM.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Bold);
            this.TENPHIM.ForeColor = System.Drawing.Color.Blue;
            this.TENPHIM.LocationFloat = new DevExpress.Utils.PointFloat(7.508532F, 146.6666F);
            this.TENPHIM.Multiline = true;
            this.TENPHIM.Name = "TENPHIM";
            this.TENPHIM.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TENPHIM.SizeF = new System.Drawing.SizeF(268.3542F, 31.25F);
            this.TENPHIM.StylePriority.UseFont = false;
            this.TENPHIM.StylePriority.UseForeColor = false;
            this.TENPHIM.StylePriority.UseTextAlignment = false;
            this.TENPHIM.Text = "Người sói : hồi kết ";
            this.TENPHIM.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLine4
            // 
            this.xrLine4.ForeColor = System.Drawing.Color.Blue;
            this.xrLine4.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(7.508469F, 136.1666F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(268.3542F, 10.50002F);
            this.xrLine4.StylePriority.UseForeColor = false;
            // 
            // BATDAU
            // 
            this.BATDAU.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.BATDAU.ForeColor = System.Drawing.Color.Blue;
            this.BATDAU.LocationFloat = new DevExpress.Utils.PointFloat(7.508564F, 183.7083F);
            this.BATDAU.Multiline = true;
            this.BATDAU.Name = "BATDAU";
            this.BATDAU.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.BATDAU.SizeF = new System.Drawing.SizeF(52.90817F, 22.91667F);
            this.BATDAU.StylePriority.UseFont = false;
            this.BATDAU.StylePriority.UseForeColor = false;
            this.BATDAU.StylePriority.UseTextAlignment = false;
            this.BATDAU.Text = "15:00";
            this.BATDAU.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // NGAYCHIEU
            // 
            this.NGAYCHIEU.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.NGAYCHIEU.ForeColor = System.Drawing.Color.Blue;
            this.NGAYCHIEU.LocationFloat = new DevExpress.Utils.PointFloat(72.47156F, 183.7083F);
            this.NGAYCHIEU.Multiline = true;
            this.NGAYCHIEU.Name = "NGAYCHIEU";
            this.NGAYCHIEU.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.NGAYCHIEU.SizeF = new System.Drawing.SizeF(95.14909F, 22.91667F);
            this.NGAYCHIEU.StylePriority.UseFont = false;
            this.NGAYCHIEU.StylePriority.UseForeColor = false;
            this.NGAYCHIEU.StylePriority.UseTextAlignment = false;
            this.NGAYCHIEU.Text = "31/12/2015";
            this.NGAYCHIEU.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // SOGHE
            // 
            this.SOGHE.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.SOGHE.ForeColor = System.Drawing.Color.Blue;
            this.SOGHE.LocationFloat = new DevExpress.Utils.PointFloat(175.4159F, 183.7083F);
            this.SOGHE.Multiline = true;
            this.SOGHE.Name = "SOGHE";
            this.SOGHE.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.SOGHE.SizeF = new System.Drawing.SizeF(59.44699F, 22.91667F);
            this.SOGHE.StylePriority.UseFont = false;
            this.SOGHE.StylePriority.UseForeColor = false;
            this.SOGHE.StylePriority.UseTextAlignment = false;
            this.SOGHE.Text = "B08";
            this.SOGHE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // TENPHONG
            // 
            this.TENPHONG.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.TENPHONG.ForeColor = System.Drawing.Color.Blue;
            this.TENPHONG.LocationFloat = new DevExpress.Utils.PointFloat(234.8629F, 183.7083F);
            this.TENPHONG.Multiline = true;
            this.TENPHONG.Name = "TENPHONG";
            this.TENPHONG.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TENPHONG.SizeF = new System.Drawing.SizeF(29.5414F, 22.91667F);
            this.TENPHONG.StylePriority.UseFont = false;
            this.TENPHONG.StylePriority.UseForeColor = false;
            this.TENPHONG.StylePriority.UseTextAlignment = false;
            this.TENPHONG.Text = "03";
            this.TENPHONG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(7.508564F, 206.6249F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(61.08138F, 17.79166F);
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseForeColor = false;
            this.xrLabel20.Text = "Giờ(Time)";
            // 
            // xrLabel21
            // 
            this.xrLabel21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(81.40364F, 206.6249F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(68.06577F, 17.79166F);
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseForeColor = false;
            this.xrLabel21.Text = "Ngày(Date)";
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(150.7696F, 206.6249F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(61.81613F, 17.79166F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseForeColor = false;
            this.xrLabel23.Text = "Ghế(Seat)";
            // 
            // xrLabel24
            // 
            this.xrLabel24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel24.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(212.5857F, 206.6249F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(68.9677F, 17.79166F);
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UseForeColor = false;
            this.xrLabel24.Text = "Rạp(Screen)";
            // 
            // xrLine6
            // 
            this.xrLine6.ForeColor = System.Drawing.Color.Blue;
            this.xrLine6.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine6.LocationFloat = new DevExpress.Utils.PointFloat(7.508564F, 224.4166F);
            this.xrLine6.Name = "xrLine6";
            this.xrLine6.SizeF = new System.Drawing.SizeF(268.3542F, 10.5F);
            this.xrLine6.StylePriority.UseForeColor = false;
            // 
            // DONGIA
            // 
            this.DONGIA.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.DONGIA.ForeColor = System.Drawing.Color.Blue;
            this.DONGIA.LocationFloat = new DevExpress.Utils.PointFloat(200.0051F, 234.9166F);
            this.DONGIA.Multiline = true;
            this.DONGIA.Name = "DONGIA";
            this.DONGIA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DONGIA.SizeF = new System.Drawing.SizeF(75.85762F, 22.91666F);
            this.DONGIA.StylePriority.UseFont = false;
            this.DONGIA.StylePriority.UseForeColor = false;
            this.DONGIA.StylePriority.UseTextAlignment = false;
            this.DONGIA.Text = "45,000";
            this.DONGIA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(199.4299F, 257.8333F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(76.43274F, 17.79166F);
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UseForeColor = false;
            this.xrLabel26.Text = "Giá vé(Price)";
            // 
            // LOAIVE
            // 
            this.LOAIVE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.LOAIVE.ForeColor = System.Drawing.Color.Blue;
            this.LOAIVE.LocationFloat = new DevExpress.Utils.PointFloat(7.508469F, 239F);
            this.LOAIVE.Name = "LOAIVE";
            this.LOAIVE.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.LOAIVE.SizeF = new System.Drawing.SizeF(188.9214F, 36.62491F);
            this.LOAIVE.StylePriority.UseFont = false;
            this.LOAIVE.StylePriority.UseForeColor = false;
            this.LOAIVE.StylePriority.UseTextAlignment = false;
            this.LOAIVE.Text = "VÉ MỜI VIP CÓ COMBO";
            this.LOAIVE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel28.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(7.508564F, 275.6249F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(268.3541F, 17.79166F);
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.StylePriority.UseForeColor = false;
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = "Đã gồm VAT 5% ";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLine7
            // 
            this.xrLine7.ForeColor = System.Drawing.Color.Blue;
            this.xrLine7.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine7.LocationFloat = new DevExpress.Utils.PointFloat(7.508469F, 293.4166F);
            this.xrLine7.Name = "xrLine7";
            this.xrLine7.SizeF = new System.Drawing.SizeF(268.3542F, 10.5F);
            this.xrLine7.StylePriority.UseForeColor = false;
            // 
            // NGUOITAO
            // 
            this.NGUOITAO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGUOITAO.ForeColor = System.Drawing.Color.Blue;
            this.NGUOITAO.LocationFloat = new DevExpress.Utils.PointFloat(7.508564F, 303.9166F);
            this.NGUOITAO.Name = "NGUOITAO";
            this.NGUOITAO.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.NGUOITAO.SizeF = new System.Drawing.SizeF(167.9073F, 17.79166F);
            this.NGUOITAO.StylePriority.UseFont = false;
            this.NGUOITAO.StylePriority.UseForeColor = false;
            this.NGUOITAO.StylePriority.UseTextAlignment = false;
            this.NGUOITAO.Text = "Nguyễn Văn Thông";
            this.NGUOITAO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // NGAYTAO
            // 
            this.NGAYTAO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYTAO.ForeColor = System.Drawing.Color.Blue;
            this.NGAYTAO.LocationFloat = new DevExpress.Utils.PointFloat(175.4159F, 303.9166F);
            this.NGAYTAO.Name = "NGAYTAO";
            this.NGAYTAO.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.NGAYTAO.SizeF = new System.Drawing.SizeF(100.4467F, 17.79166F);
            this.NGAYTAO.StylePriority.UseFont = false;
            this.NGAYTAO.StylePriority.UseForeColor = false;
            this.NGAYTAO.StylePriority.UseTextAlignment = false;
            this.NGAYTAO.Text = "21/12/2012 20:30";
            this.NGAYTAO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // MABARCODE2
            // 
            this.MABARCODE2.AutoModule = true;
            this.MABARCODE2.Font = new System.Drawing.Font("Tahoma", 6F);
            this.MABARCODE2.LocationFloat = new DevExpress.Utils.PointFloat(20.40091F, 339.4999F);
            this.MABARCODE2.Name = "MABARCODE2";
            this.MABARCODE2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.MABARCODE2.ShowText = false;
            this.MABARCODE2.SizeF = new System.Drawing.SizeF(244.0034F, 35.3652F);
            this.MABARCODE2.StylePriority.UseFont = false;
            this.MABARCODE2.StylePriority.UsePadding = false;
            this.MABARCODE2.StylePriority.UseTextAlignment = false;
            this.MABARCODE2.Symbology = code128Generator1;
            this.MABARCODE2.Text = "*4715062017B0016*";
            this.MABARCODE2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // MABARCODE1
            // 
            this.MABARCODE1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MABARCODE1.ForeColor = System.Drawing.Color.Blue;
            this.MABARCODE1.LocationFloat = new DevExpress.Utils.PointFloat(7.508627F, 321.7082F);
            this.MABARCODE1.Name = "MABARCODE1";
            this.MABARCODE1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MABARCODE1.SizeF = new System.Drawing.SizeF(268.354F, 17.79166F);
            this.MABARCODE1.StylePriority.UseFont = false;
            this.MABARCODE1.StylePriority.UseForeColor = false;
            this.MABARCODE1.StylePriority.UseTextAlignment = false;
            this.MABARCODE1.Text = "231100000001";
            this.MABARCODE1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lbFooter
            // 
            this.lbFooter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFooter.ForeColor = System.Drawing.Color.Blue;
            this.lbFooter.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 374.8651F);
            this.lbFooter.Multiline = true;
            this.lbFooter.Name = "lbFooter";
            this.lbFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbFooter.SizeF = new System.Drawing.SizeF(268.354F, 17.79166F);
            this.lbFooter.StylePriority.UseFont = false;
            this.lbFooter.StylePriority.UseForeColor = false;
            this.lbFooter.StylePriority.UseTextAlignment = false;
            this.lbFooter.Text = "Cảm Ơn Quý Khách\r\nChúc Quý Khách Xem Phim Vui Vẻ";
            this.lbFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel7,
            this.lbFooter,
            this.MABARCODE1,
            this.MABARCODE2,
            this.NGAYTAO,
            this.NGUOITAO,
            this.xrLine7,
            this.xrLabel28,
            this.LOAIVE,
            this.xrLabel26,
            this.DONGIA,
            this.xrLine6,
            this.xrLabel24,
            this.xrLabel23,
            this.xrLabel21,
            this.xrLabel20,
            this.TENPHONG,
            this.SOGHE,
            this.NGAYCHIEU,
            this.BATDAU,
            this.xrLine4,
            this.TENPHIM,
            this.lb_Header,
            this.xrLabel12,
            this.xrLabel8,
            this.MABARCODE,
            this.xrLabel2,
            this.winControlContainer1,
            this.lb_Quay,
            this.xrLabel15,
            this.lb_MaSoTheu,
            this.lb_TenCty,
            this.lb_ngay,
            this.xrLabel4});
            this.ReportHeader.HeightF = 418.6984F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 392.6567F);
            this.xrLabel7.Multiline = true;
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(268.354F, 17.79166F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseForeColor = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // rptVerapphim_Bill1
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.ReportFooter});
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.Margins = new System.Drawing.Printing.Margins(20, 79, 0, 22);
            this.PageHeight = 678;
            this.PageWidth = 400;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ShowPrintMarginsWarning = false;
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel lb_ngay;
        private DevExpress.XtraReports.UI.XRLabel lb_TenCty;
        private DevExpress.XtraReports.UI.XRLabel lb_MaSoTheu;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel lb_Quay;
        private System.Windows.Forms.Panel Logo;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel MABARCODE;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel lb_Header;
        private DevExpress.XtraReports.UI.XRLabel TENPHIM;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLabel BATDAU;
        private DevExpress.XtraReports.UI.XRLabel NGAYCHIEU;
        private DevExpress.XtraReports.UI.XRLabel SOGHE;
        private DevExpress.XtraReports.UI.XRLabel TENPHONG;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLine xrLine6;
        private DevExpress.XtraReports.UI.XRLabel DONGIA;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel LOAIVE;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLine xrLine7;
        private DevExpress.XtraReports.UI.XRLabel NGUOITAO;
        private DevExpress.XtraReports.UI.XRLabel NGAYTAO;
        private DevExpress.XtraReports.UI.XRBarCode MABARCODE2;
        private DevExpress.XtraReports.UI.XRLabel MABARCODE1;
        private DevExpress.XtraReports.UI.XRLabel lbFooter;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
    }
}
