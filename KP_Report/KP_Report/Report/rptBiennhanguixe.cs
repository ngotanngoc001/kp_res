﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBiennhanguixe : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBiennhanguixe()
        {
            InitializeComponent();
           Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            MAPHIEU.DataBindings.Add("Text", DataSource, "MAPHIEU");
            MAPHIEU1.DataBindings.Add("Text", DataSource, "MAPHIEU", "*{0}*");
            TENNGUOIGUI.DataBindings.Add("Text", DataSource, "TENNGUOIGUI");
            CMNDNGUOIGUI.DataBindings.Add("Text", DataSource, "CMNDNGUOIGUI");
            DIENTHOAINGUOIGUI.DataBindings.Add("Text", DataSource, "DIENTHOAINGUOIGUI");
            TENNGUOINHAN.DataBindings.Add("Text", DataSource, "TENNGUOINHAN");
            CMNDNGUOINHAN.DataBindings.Add("Text", DataSource, "CMNDNGUOINHAN");
            DIENTHOAINGUOINHAN.DataBindings.Add("Text", DataSource, "DIENTHOAINGUOINHAN");
            TENTUYEN.DataBindings.Add("Text", DataSource, "TENTUYEN");
            TENDOITUONG.DataBindings.Add("Text", DataSource, "TENDOITUONG");
            NGAYDI.DataBindings.Add("Text", DataSource, "NGAYDI", "{0:dd/MM/yyyy}");
            GIODI.DataBindings.Add("Text", DataSource, "GIODI");
            GHICHU.DataBindings.Add("Text", DataSource, "GHICHU");
            BIENSO.DataBindings.Add("Text", DataSource, "BIENSO");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:#,###0}");
            PHUTHU.DataBindings.Add("Text", DataSource, "PHUTHU", "{0:#,###0}");
            DATHANHTOAN.DataBindings.Add("Text", DataSource, "DATHANHTOAN");
            NGUOITAO.DataBindings.Add("Text", DataSource, "NGUOITAO");
            NGAYTAO1.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy}");
        }
       
    }
}
