﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using System.Drawing.Printing;
namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptKetquachieuphim : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptKetquachieuphim()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            byte[] tam = new byte[((byte[])dt.Rows[0]["HINHANH"]).LongLength];
            tam = (byte[])dt.Rows[0]["HINHANH"];
            System.IO.MemoryStream ms = new System.IO.MemoryStream(tam);
            Bitmap bm = new Bitmap(ms);
            //logo.Image = bm;
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            dtSource= (DataTable)DataSource;
            txt_tieude.Text = "(" + dtSource.Rows[0]["TYPE"].ToString() + ": " + dtSource.Rows[0]["NAMETYPE"].ToString() + " )";
            txt_tieude1.Text = "( Từ ngày: " + dtSource.Rows[0]["TUNGAY"].ToString() + " - Đến ngày: " + dtSource.Rows[0]["DENNGAY"].ToString() + " )";
        }

        DataTable dtSource = new DataTable();
        private void rptKetquachieuphim_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrTable3.Controls.Clear();
            xrTable1.Controls.Clear();
            xrTable2.Controls.Clear();
            int cellsInRow = dtSource.Columns.Count - 5;
            int rowsCount = dtSource.Rows.Count;
            float rowHeight = xrTableRow2.HeightF;
            string stenphim = dtSource.Rows[0]["PHIM"].ToString();
            string sNgay = dtSource.Rows[0]["NGAY"].ToString();

            foreach (DataRow dr in dtSource.Rows)
            {
                XRTableRow row = new XRTableRow();
                row.HeightF = rowHeight;
                XRTableRow row1 = new XRTableRow();
                row1.HeightF = rowHeight;
                XRTableRow row2 = new XRTableRow();
                row2.HeightF = rowHeight;
                XRTableRow row3 = new XRTableRow();
                row3.HeightF = rowHeight;

                foreach (DataColumn dc in dtSource.Columns)
                {
                    if (dc.ColumnName != "MALICHCHIEU" && dc.ColumnName != "TUNGAY" && dc.ColumnName != "DENNGAY" && dc.ColumnName != "TYPE" && dc.ColumnName != "NAMETYPE" && dc.ColumnName != "PHIM1" && dc.ColumnName != "DOANHTHUTB" && dc.ColumnName != "OCCTB")
                    {
                        XRTableCell cell = new XRTableCell();
                        cell.HeightF = rowHeight;
                        cell.CanGrow = true;
                        XRTableCell cell1 = new XRTableCell();
                        cell1.HeightF = rowHeight;
                        cell1.CanGrow = true;
                        XRTableCell cell2 = new XRTableCell();
                        cell2.HeightF = rowHeight;
                        cell2.CanGrow = true;
                        XRTableCell cell3 = new XRTableCell();
                        cell3.HeightF = rowHeight;
                        cell3.CanGrow = true;

                        cell.Text = dr[dc].ToString();
                        cell.Font = xrPageInfo3.Font;
                        if (dc.ColumnName == "TONGTIEN")
                        {
                            int itemp = int.Parse(cell.Text);
                            cell.Text = itemp.ToString("n0");
                            cell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
                        }
                        else
                        {
                            cell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        }
                        row.Cells.Add(cell);

                        if (dtSource.Rows.IndexOf(dr) == 0)
                        {
                            cell1.Text = dc.Caption;
                            cell1.Font = txtDienthoai.Font;
                            cell1.Multiline = true;
                            cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                            row1.Cells.Add(cell1);
                        }
                        if (dtSource.Rows.IndexOf(dr)==dtSource .Rows .Count -1)
                        {
                            cell1.Text = dc.Namespace;
                            cell1.Font = txtDienthoai.Font;
                            if (dc.ColumnName == "TONGTIEN")
                            {
                                cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
                            }
                            else
                            {
                                cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                            }
                            row1.Cells.Add(cell1);
                        }

                        float TongGhe = 0f;
                        float TongVe = 0f;

                        if (stenphim != dr["PHIM"].ToString() || sNgay != dr["NGAY"].ToString())
                        {
                            dtSource.DefaultView.RowFilter = "PHIM=" + clsMain.SQLString(stenphim) + " AND NGAY=" + clsMain.SQLString(sNgay);
                            DataTable dt = dtSource.DefaultView.ToTable();
                            if (dc.ColumnName == "NGAY" || dc.ColumnName == "PHIM" || dc.ColumnName == "NHAPHATHANH")
                            {
                                cell2.Text = dt.Rows[0][dc.ColumnName].ToString();
                                cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                            }
                            else if (dc.ColumnName == "PHONG")
                            {
                                cell2.Text = "";
                                cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                            }
                            else if (dc.ColumnName == "XUATCHIEU")
                            {
                                cell2.Text = dt.Rows.Count.ToString();
                                cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                            }
                            else
                            {
                                float sum = 0f;
                                foreach (DataRow drrow in dt.Rows)
                                {
                                    sum += float.Parse(drrow[dc.ColumnName].ToString());
                                }

                                if (dc.ColumnName == "OCC")
                                {
                                    TongGhe = 0f;
                                    TongVe = 0f;
                                    foreach (DataRow drrow in dt.Rows)
                                    {
                                        TongGhe += float.Parse(drrow["SOLUONGGHE"].ToString());
                                    }

                                    foreach (DataRow drrow in dt.Rows)
                                    {
                                        TongVe += float.Parse(drrow["TONGVE"].ToString());
                                    }

                                    cell2.Text = (Math.Round((TongVe / TongGhe) * 100, 2)).ToString();
                                    cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                                }
                                else if (dc.ColumnName == "TONGTIEN")
                                {
                                    cell2.Text = sum.ToString("N0");
                                    cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
                                }
                                else
                                {
                                    cell2.Text = sum.ToString();
                                    cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                                }
                            }
                            cell2.Font = txtDienthoai.Font;
                            row2.Cells.Add(cell2);
                        }

                        if (dtSource.Rows.IndexOf(dr) == dtSource.Rows.Count - 1)
                        {
                            dtSource.DefaultView.RowFilter = "PHIM=" + clsMain.SQLString(dr["PHIM"].ToString()) + " AND NGAY=" + clsMain.SQLString(dr["NGAY"].ToString());
                            DataTable dt = dtSource.DefaultView.ToTable();
                            if (dc.ColumnName == "NGAY" || dc.ColumnName == "PHIM" || dc.ColumnName == "NHAPHATHANH")
                            {
                                cell3.Text = dt.Rows[0][dc.ColumnName].ToString();
                                cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                            }
                            else if (dc.ColumnName == "PHONG")
                            {
                                cell3.Text = "";
                                cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                            }
                            else if (dc.ColumnName == "XUATCHIEU")
                            {
                                cell3.Text = dt.Rows.Count.ToString();
                                cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                            }
                            else
                            {
                                float sum = 0;
                                foreach (DataRow drrow in dt.Rows)
                                {
                                    sum += float.Parse(drrow[dc.ColumnName].ToString());
                                }

                                if (dc.ColumnName == "OCC")
                                {
                                    TongGhe = 0f;
                                    TongVe = 0f;
                                    foreach (DataRow drrow in dt.Rows)
                                    {
                                        TongGhe += float.Parse(drrow["SOLUONGGHE"].ToString());
                                    }

                                    foreach (DataRow drrow in dt.Rows)
                                    {
                                        TongVe += float.Parse(drrow["TONGVE"].ToString());
                                    }

                                    cell3.Text = (Math.Round((TongVe / TongGhe) * 100,2)).ToString();
                                    cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                                }
                                else if (dc.ColumnName == "TONGTIEN")
                                {
                                    cell3.Text = sum.ToString("N0");
                                    cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
                                }
                                else
                                {
                                    cell3.Text = sum.ToString();
                                    cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                                }
                            }
                            cell3.Font = txtDienthoai.Font;
                            row3.Cells.Add(cell3);
                        }
                    }
                }
                if (stenphim != dr["PHIM"].ToString() || sNgay != dr["NGAY"].ToString())
                {
                    xrTable2.Rows.Add(row2);
                    stenphim = dr["PHIM"].ToString();
                    sNgay = dr["NGAY"].ToString();
                }
                xrTable2.Rows.Add(row);
                if (dtSource.Rows.IndexOf(dr) == dtSource.Rows.Count - 1)
                {
                    xrTable2.Rows.Add(row3);
                }
                if (dtSource.Rows.IndexOf(dr) == 0)
                {
                    xrTable1.Rows.Add(row1);
                }
                if (dtSource.Rows.IndexOf(dr) ==dtSource .Rows .Count -1)
                {
                    xrTable3.Rows.Add(row1);
                }
            }
        }
    }
}
