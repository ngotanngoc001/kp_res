﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace KP_Report
{
    /// <summary>
    /// Report Ve
    /// </summary>
    public partial class rptMavachhang : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report Ve
        /// </summary>
        public rptMavachhang()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
           TENTUYEN.DataBindings.Add("Text", DataSource, "TENTUYEN");
           MAPHIEU.DataBindings.Add("Text", DataSource, "MABARCODE", "*{0}*");
           SOLUONG.DataBindings.Add("Text", DataSource, "SOLUONG");
        }
    }
}
