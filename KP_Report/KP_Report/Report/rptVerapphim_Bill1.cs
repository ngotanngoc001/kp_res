﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using System.Data;
using KP_UserManagement;
using System.IO;
using System.Windows.Forms;


namespace KP_Report
{
    public partial class rptVerapphim_Bill1 : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// checkBill
        /// </summary>
        public int checkBill=0;
        /// <summary>
        /// rptVerapphim_Bill1
        /// </summary>
        public rptVerapphim_Bill1()
        {
            InitializeComponent();                      
            Setheader();            
            

        }
        /// <summary>
        /// BindData
        /// </summary>
        public void BindData()
        {
            DataTable dt = (DataTable)DataSource;
            foreach (DataRow dr in dt.Rows)
            {
                try
                {
                    MABARCODE.Text =  Right_str(dr["MABARCODE"].ToString(), 7);
                }
                catch
                {
                    MABARCODE.Text = dr["MABARCODE"].ToString();
                }
                TENPHIM.Text  = dr["TENPHIM"].ToString();
                TENPHONG.Text  = dr["TENPHONG"].ToString();
                SOGHE.Text = dr["SOGHE"].ToString();
                DONGIA.Text  = string.Format("{0:#,###0}", dr["DONGIA"]);
                NGAYCHIEU.Text  = string.Format("{0:dd/MM/yyyy}", dr["NGAYCHIEU"]);
                BATDAU.Text  = string.Format("{0:HH:mm}", dr["BATDAU"]);
                NGAYTAO.Text = string.Format("{0:dd/MM/yyyy HH:mm}", dr["NGAYTAO"]);
                NGUOITAO.Text = dr["NGUOITAO"].ToString();
                LOAIVE.Text = dr["TENLOAIVE"].ToString();
                MABARCODE1.Text = MABARCODE2.Text = dr["MABARCODE"].ToString();
            }     
            
        } 

         private void Setheader()
        {
            StreamReader str = new StreamReader(Application.StartupPath + "/systeminfo.txt");
            string sMacuahang = "";
            string line;
            while ((line = str.ReadLine()) != null)
            {
                string[] value = line.Split(',');
                sMacuahang = value[0].ToString();
            }

            string sSQL = "";
            sSQL += "Select MA_CUAHANG,	TEN_CUAHANG,	GHICHU,	SUDUNG,	Header,	Footer" + "\n";
            sSQL += "From CUAHANG" + "\n";
            sSQL += "Where MA_CUAHANG  =" + clsMain.SQLString(sMacuahang) + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            lb_Header.Text = dt.Rows[0]["Header"].ToString();
            lbFooter.Text = dt.Rows[0]["Footer"].ToString();

           
        }

         private string Right_str(string value, int length)
         {
             return value.Substring(value.Length - length);
         }
    }
}
