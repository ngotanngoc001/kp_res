﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using System.Drawing.Imaging;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptHoaDonDo : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptHoaDonDo()
        {
            InitializeComponent();
            Setheader();
        }
        private void ConvertHinh(ref PictureBox pic, Byte[] values)
        {
            MemoryStream ms = new MemoryStream(values);
            Bitmap bm = new Bitmap(ms);
            pic.Image = bm;
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txt_masothue.Text = "MST (Tax Code) : " + dt.Rows[0]["MASOTHUE"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txt_SoTK.Text = "Số Tài Khoản (Account No.) : " + dt.Rows[0]["SOTAIKHOAN"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString() + " - Email : " + dt.Rows[0]["EMAIL"].ToString();
            txt_mauso.Text = dt.Rows[0]["MAUSOPHIEUHD"].ToString();
            txt_kyhieu.Text = dt.Rows[0]["KYHIEU"].ToString();
            if (dt.Rows[0]["HINHANH"] != DBNull.Value)
            {
                byte[] tam = new byte[((byte[])dt.Rows[0]["HINHANH"]).LongLength];
                tam = (byte[])dt.Rows[0]["HINHANH"];
                ConvertHinh(ref this.ptb_logo, tam);
            }
            else
                ptb_logo.ImageLocation = Application.StartupPath + "/noavatar.gif";
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            stt.DataBindings.Add("Text", DataSource, "STT");
            TENHANGHOA.DataBindings.Add("Text", DataSource, "HH_TENHANGHOA");
            DVT.DataBindings.Add("Text", DataSource, "HH_DVT");
            SL.DataBindings.Add("Text", DataSource, "SOLUONG", "{0:#,###0.00}");
            VAT.DataBindings.Add("Text", DataSource, "VAT", "{0:#,###0}");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:#,###0.00}");
            THANHTIEN.DataBindings.Add("Text", DataSource, "THANHTIEN", "{0:#,###0.00}");

            txt_thanhtientongcong.DataBindings.Add("Text", DataSource, "THANHTIENTONG", "{0:#,###0.00}");
            txt_tongtienthue.DataBindings.Add("Text", DataSource, "TIENVATTONG", "{0:#,###0.00}");
            txt_tongtiensauthue.DataBindings.Add("Text", DataSource, "TONGCONG", "{0:#,###0.00}");

            txt_tennguoimua.DataBindings.Add("Text", DataSource, "HOTENGUOIMUA");
            txt_donvi.DataBindings.Add("Text", DataSource, "DONVIMUA");
            txt_mst.DataBindings.Add("Text", DataSource, "MST");
            txt_sotaikhoan.DataBindings.Add("Text", DataSource, "SOTK");
            txt_diachi.DataBindings.Add("Text", DataSource, "DIACHI");
            txt_httt.DataBindings.Add("Text", DataSource, "HTTT");
            txt_ngay.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy}");
        }
    }
}
