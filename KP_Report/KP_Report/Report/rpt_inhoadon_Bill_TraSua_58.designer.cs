﻿namespace KP_Report
{
    /// <summary>
    /// rpt_inhoadon
    /// </summary>
    partial class rpt_inhoadon_Bill_TraSua_58
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt_inhoadon_Bill_TraSua_58));
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.tblMain = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cl_HangHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.cl_soluong = new DevExpress.XtraReports.UI.XRTableCell();
            this.cl_giaban = new DevExpress.XtraReports.UI.XRTableCell();
            this.CHIETKHAU = new DevExpress.XtraReports.UI.XRTableCell();
            this.cl_thanhtien = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.lb_ngay = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.lb_Quay = new DevExpress.XtraReports.UI.XRLabel();
            this.Number = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_nhanvien = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_mahoadon = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_ban = new DevExpress.XtraReports.UI.XRLabel();
            this.logo = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lb_Header = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_NameBill = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbTiendatcoc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbPhihucvu = new DevExpress.XtraReports.UI.XRLabel();
            this.lbFooter = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_tienkhachtra = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_conlai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.lb_giamgia = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_tongcong = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_MaHD = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.tblMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblMain});
            this.Detail.HeightF = 16.66667F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // tblMain
            // 
            this.tblMain.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.tblMain.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.tblMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblMain.ForeColor = System.Drawing.Color.Blue;
            this.tblMain.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.tblMain.Name = "tblMain";
            this.tblMain.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.tblMain.SizeF = new System.Drawing.SizeF(204.6043F, 16.66667F);
            this.tblMain.StylePriority.UseBorderDashStyle = false;
            this.tblMain.StylePriority.UseBorders = false;
            this.tblMain.StylePriority.UseFont = false;
            this.tblMain.StylePriority.UseForeColor = false;
            this.tblMain.StylePriority.UseTextAlignment = false;
            this.tblMain.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cl_HangHoa,
            this.cl_soluong,
            this.cl_giaban,
            this.CHIETKHAU,
            this.cl_thanhtien});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // cl_HangHoa
            // 
            this.cl_HangHoa.Name = "cl_HangHoa";
            this.cl_HangHoa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cl_HangHoa.StylePriority.UsePadding = false;
            this.cl_HangHoa.StylePriority.UseTextAlignment = false;
            this.cl_HangHoa.Text = "my á";
            this.cl_HangHoa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.cl_HangHoa.Weight = 1.0264543286095642D;
            // 
            // cl_soluong
            // 
            this.cl_soluong.Name = "cl_soluong";
            this.cl_soluong.StylePriority.UseTextAlignment = false;
            this.cl_soluong.Text = "100";
            this.cl_soluong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cl_soluong.Weight = 0.31253822401941639D;
            // 
            // cl_giaban
            // 
            this.cl_giaban.Name = "cl_giaban";
            this.cl_giaban.StylePriority.UseTextAlignment = false;
            this.cl_giaban.Text = "700,000";
            this.cl_giaban.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cl_giaban.Weight = 0.42771744945491857D;
            // 
            // CHIETKHAU
            // 
            this.CHIETKHAU.Name = "CHIETKHAU";
            this.CHIETKHAU.StylePriority.UseTextAlignment = false;
            this.CHIETKHAU.Text = "50%";
            this.CHIETKHAU.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CHIETKHAU.Weight = 0.20637686960519669D;
            // 
            // cl_thanhtien
            // 
            this.cl_thanhtien.Name = "cl_thanhtien";
            this.cl_thanhtien.StylePriority.UseTextAlignment = false;
            this.cl_thanhtien.Text = "5,000,000";
            this.cl_thanhtien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cl_thanhtien.Weight = 0.49937787335555978D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 22F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.xrLine1,
            this.lb_ngay,
            this.xrLabel1,
            this.xrLine2,
            this.lb_Quay,
            this.Number,
            this.lb_nhanvien,
            this.xrLabel15,
            this.xrLabel5,
            this.xrLabel3,
            this.lb_mahoadon,
            this.xrLabel4,
            this.lb_ban,
            this.logo,
            this.lb_Header,
            this.lb_NameBill});
            this.ReportHeader.HeightF = 263.9484F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrTable1
            // 
            this.xrTable1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.ForeColor = System.Drawing.Color.Blue;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0.001716614F, 231.6667F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(203.6618F, 18.74997F);
            this.xrTable1.StylePriority.UseBorderDashStyle = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseForeColor = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell5,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Hàng Hóa";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.52731827108341356D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.Text = "SL";
            this.xrTableCell2.Weight = 0.13050813442121093D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.Text = "Giá Bán";
            this.xrTableCell5.Weight = 0.2557413678297728D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.Text = "Thành Tiền";
            this.xrTableCell3.Weight = 0.34773861186163757D;
            // 
            // xrLine1
            // 
            this.xrLine1.BorderWidth = 1;
            this.xrLine1.ForeColor = System.Drawing.Color.Blue;
            this.xrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 221.1667F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(206.4128F, 10.50002F);
            this.xrLine1.StylePriority.UseBorderWidth = false;
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // lb_ngay
            // 
            this.lb_ngay.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ngay.ForeColor = System.Drawing.Color.Blue;
            this.lb_ngay.LocationFloat = new DevExpress.Utils.PointFloat(37.79529F, 185.5834F);
            this.lb_ngay.Name = "lb_ngay";
            this.lb_ngay.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_ngay.SizeF = new System.Drawing.SizeF(82.08894F, 17.79161F);
            this.lb_ngay.StylePriority.UseFont = false;
            this.lb_ngay.StylePriority.UseForeColor = false;
            this.lb_ngay.StylePriority.UseTextAlignment = false;
            this.lb_ngay.Text = "23-05-2015 10:13:13";
            this.lb_ngay.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(119.8842F, 150F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(77.11576F, 17.79167F);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "No.";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine2
            // 
            this.xrLine2.ForeColor = System.Drawing.Color.Blue;
            this.xrLine2.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 250.4167F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(204.6043F, 10.5F);
            this.xrLine2.StylePriority.UseForeColor = false;
            // 
            // lb_Quay
            // 
            this.lb_Quay.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Quay.ForeColor = System.Drawing.Color.Blue;
            this.lb_Quay.LocationFloat = new DevExpress.Utils.PointFloat(37.79529F, 150F);
            this.lb_Quay.Name = "lb_Quay";
            this.lb_Quay.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_Quay.SizeF = new System.Drawing.SizeF(82.08895F, 17.79167F);
            this.lb_Quay.StylePriority.UseFont = false;
            this.lb_Quay.StylePriority.UseForeColor = false;
            this.lb_Quay.StylePriority.UseTextAlignment = false;
            this.lb_Quay.Text = "xrLabel6";
            this.lb_Quay.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // Number
            // 
            this.Number.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Number.Font = new System.Drawing.Font("Tahoma", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Number.ForeColor = System.Drawing.Color.Blue;
            this.Number.LocationFloat = new DevExpress.Utils.PointFloat(119.8845F, 167.5482F);
            this.Number.Name = "Number";
            this.Number.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Number.SizeF = new System.Drawing.SizeF(77.11554F, 53.6185F);
            this.Number.StylePriority.UseBorders = false;
            this.Number.StylePriority.UseFont = false;
            this.Number.StylePriority.UseForeColor = false;
            this.Number.StylePriority.UseTextAlignment = false;
            this.Number.Text = "100";
            this.Number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lb_nhanvien
            // 
            this.lb_nhanvien.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_nhanvien.ForeColor = System.Drawing.Color.Blue;
            this.lb_nhanvien.LocationFloat = new DevExpress.Utils.PointFloat(24.80488F, 167.7917F);
            this.lb_nhanvien.Name = "lb_nhanvien";
            this.lb_nhanvien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_nhanvien.SizeF = new System.Drawing.SizeF(95.07935F, 17.79166F);
            this.lb_nhanvien.StylePriority.UseFont = false;
            this.lb_nhanvien.StylePriority.UseForeColor = false;
            this.lb_nhanvien.StylePriority.UseTextAlignment = false;
            this.lb_nhanvien.Text = "xrLabel6";
            this.lb_nhanvien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(0F, 150F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(37.79528F, 17.79168F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseForeColor = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "Quầy:";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(0.001716614F, 167.7917F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(24.80315F, 17.79169F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseForeColor = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "NV:";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(0.001716614F, 203.375F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(43.30709F, 17.79169F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseForeColor = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Số HD:";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lb_mahoadon
            // 
            this.lb_mahoadon.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_mahoadon.ForeColor = System.Drawing.Color.Blue;
            this.lb_mahoadon.LocationFloat = new DevExpress.Utils.PointFloat(43.3088F, 203.375F);
            this.lb_mahoadon.Name = "lb_mahoadon";
            this.lb_mahoadon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_mahoadon.SizeF = new System.Drawing.SizeF(76.57544F, 17.79169F);
            this.lb_mahoadon.StylePriority.UseFont = false;
            this.lb_mahoadon.StylePriority.UseForeColor = false;
            this.lb_mahoadon.StylePriority.UseTextAlignment = false;
            this.lb_mahoadon.Text = "lb_mahoadon";
            this.lb_mahoadon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 185.5833F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(37.79528F, 17.79163F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseForeColor = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Ngày:";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lb_ban
            // 
            this.lb_ban.Font = new System.Drawing.Font("Times New Roman", 7F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ban.ForeColor = System.Drawing.Color.Blue;
            this.lb_ban.LocationFloat = new DevExpress.Utils.PointFloat(7.518387F, 111F);
            this.lb_ban.Name = "lb_ban";
            this.lb_ban.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_ban.SizeF = new System.Drawing.SizeF(185.7749F, 27.11508F);
            this.lb_ban.StylePriority.UseFont = false;
            this.lb_ban.StylePriority.UseForeColor = false;
            this.lb_ban.StylePriority.UseTextAlignment = false;
            this.lb_ban.Text = "(Hóa đơn bán lẻ có giá trị xuất hóa đơn GTGT trong ngày)";
            this.lb_ban.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lb_ban.Visible = false;
            // 
            // logo
            // 
            this.logo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.logo.Image = ((System.Drawing.Image)(resources.GetObject("logo.Image")));
            this.logo.LocationFloat = new DevExpress.Utils.PointFloat(50.00169F, 5F);
            this.logo.Name = "logo";
            this.logo.SizeF = new System.Drawing.SizeF(80F, 66F);
            this.logo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            this.logo.StylePriority.UseBorders = false;
            // 
            // lb_Header
            // 
            this.lb_Header.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Header.ForeColor = System.Drawing.Color.Blue;
            this.lb_Header.LocationFloat = new DevExpress.Utils.PointFloat(3.602008E-05F, 77.05033F);
            this.lb_Header.Multiline = true;
            this.lb_Header.Name = "lb_Header";
            this.lb_Header.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_Header.SizeF = new System.Drawing.SizeF(193.2917F, 14F);
            this.lb_Header.StylePriority.UseFont = false;
            this.lb_Header.StylePriority.UseForeColor = false;
            this.lb_Header.StylePriority.UseTextAlignment = false;
            this.lb_Header.Text = "Điện Máy Thiên Hòa";
            this.lb_Header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lb_NameBill
            // 
            this.lb_NameBill.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_NameBill.ForeColor = System.Drawing.Color.Blue;
            this.lb_NameBill.LocationFloat = new DevExpress.Utils.PointFloat(0.001728964F, 95F);
            this.lb_NameBill.Multiline = true;
            this.lb_NameBill.Name = "lb_NameBill";
            this.lb_NameBill.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_NameBill.SizeF = new System.Drawing.SizeF(193.2916F, 16F);
            this.lb_NameBill.StylePriority.UseFont = false;
            this.lb_NameBill.StylePriority.UseForeColor = false;
            this.lb_NameBill.StylePriority.UseTextAlignment = false;
            this.lb_NameBill.Text = "Hóa Đơn Bán Lẻ\r\n";
            this.lb_NameBill.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel8,
            this.lbTiendatcoc,
            this.xrLabel2,
            this.lbPhihucvu,
            this.lbFooter,
            this.xrLine5,
            this.xrLabel11,
            this.lb_tienkhachtra,
            this.xrLabel7,
            this.lb_conlai,
            this.xrLabel10,
            this.xrLine3,
            this.lb_giamgia,
            this.xrLabel9,
            this.lb_tongcong,
            this.lb_MaHD});
            this.ReportFooter.HeightF = 142.3929F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 29.29166F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(67.84496F, 12F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseForeColor = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "Tiền đặt cọc :";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbTiendatcoc
            // 
            this.lbTiendatcoc.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTiendatcoc.ForeColor = System.Drawing.Color.Blue;
            this.lbTiendatcoc.LocationFloat = new DevExpress.Utils.PointFloat(100.8085F, 29.29166F);
            this.lbTiendatcoc.Name = "lbTiendatcoc";
            this.lbTiendatcoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTiendatcoc.SizeF = new System.Drawing.SizeF(92.48158F, 12F);
            this.lbTiendatcoc.StylePriority.UseFont = false;
            this.lbTiendatcoc.StylePriority.UseForeColor = false;
            this.lbTiendatcoc.StylePriority.UseTextAlignment = false;
            this.lbTiendatcoc.Text = "0";
            this.lbTiendatcoc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 17.29166F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(67.06594F, 12F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseForeColor = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Phí phục vụ :";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbPhihucvu
            // 
            this.lbPhihucvu.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPhihucvu.ForeColor = System.Drawing.Color.Blue;
            this.lbPhihucvu.LocationFloat = new DevExpress.Utils.PointFloat(100.8085F, 17.29166F);
            this.lbPhihucvu.Name = "lbPhihucvu";
            this.lbPhihucvu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbPhihucvu.SizeF = new System.Drawing.SizeF(92.48154F, 12F);
            this.lbPhihucvu.StylePriority.UseFont = false;
            this.lbPhihucvu.StylePriority.UseForeColor = false;
            this.lbPhihucvu.StylePriority.UseTextAlignment = false;
            this.lbPhihucvu.Text = "0";
            this.lbPhihucvu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lbFooter
            // 
            this.lbFooter.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFooter.ForeColor = System.Drawing.Color.Blue;
            this.lbFooter.LocationFloat = new DevExpress.Utils.PointFloat(2.39404F, 82.31544F);
            this.lbFooter.Multiline = true;
            this.lbFooter.Name = "lbFooter";
            this.lbFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbFooter.SizeF = new System.Drawing.SizeF(193.2917F, 21.12493F);
            this.lbFooter.StylePriority.UseFont = false;
            this.lbFooter.StylePriority.UseForeColor = false;
            this.lbFooter.StylePriority.UseTextAlignment = false;
            this.lbFooter.Text = "Trân trọng cảm ơn quý khách đã đến mua hàng  ";
            this.lbFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLine5
            // 
            this.xrLine5.ForeColor = System.Drawing.Color.Blue;
            this.xrLine5.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(2.39404F, 77.02376F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(206.4128F, 5.291672F);
            this.xrLine5.StylePriority.UseForeColor = false;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 65.02377F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(80.41F, 12F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseForeColor = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "Tiền thối lại :";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lb_tienkhachtra
            // 
            this.lb_tienkhachtra.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_tienkhachtra.ForeColor = System.Drawing.Color.Blue;
            this.lb_tienkhachtra.LocationFloat = new DevExpress.Utils.PointFloat(100.8101F, 53.02375F);
            this.lb_tienkhachtra.Name = "lb_tienkhachtra";
            this.lb_tienkhachtra.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_tienkhachtra.SizeF = new System.Drawing.SizeF(92.48321F, 12F);
            this.lb_tienkhachtra.StylePriority.UseFont = false;
            this.lb_tienkhachtra.StylePriority.UseForeColor = false;
            this.lb_tienkhachtra.StylePriority.UseTextAlignment = false;
            this.lb_tienkhachtra.Text = "0";
            this.lb_tienkhachtra.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 53.02375F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(98.41438F, 12F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseForeColor = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Tổng tiền khách trả :";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lb_conlai
            // 
            this.lb_conlai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_conlai.ForeColor = System.Drawing.Color.Blue;
            this.lb_conlai.LocationFloat = new DevExpress.Utils.PointFloat(100.8101F, 65.02377F);
            this.lb_conlai.Name = "lb_conlai";
            this.lb_conlai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_conlai.SizeF = new System.Drawing.SizeF(92.48322F, 12F);
            this.lb_conlai.StylePriority.UseFont = false;
            this.lb_conlai.StylePriority.UseForeColor = false;
            this.lb_conlai.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:#.00}";
            this.lb_conlai.Summary = xrSummary1;
            this.lb_conlai.Text = "0";
            this.lb_conlai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 41.02376F);
            this.xrLabel10.Multiline = true;
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(100.031F, 12F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseForeColor = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "Tổng tiền thanh toán :";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine3
            // 
            this.xrLine3.ForeColor = System.Drawing.Color.Blue;
            this.xrLine3.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(2.395662F, 0F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(204.0171F, 5.291668F);
            this.xrLine3.StylePriority.UseForeColor = false;
            // 
            // lb_giamgia
            // 
            this.lb_giamgia.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_giamgia.ForeColor = System.Drawing.Color.Blue;
            this.lb_giamgia.LocationFloat = new DevExpress.Utils.PointFloat(100.8101F, 5.291685F);
            this.lb_giamgia.Name = "lb_giamgia";
            this.lb_giamgia.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_giamgia.SizeF = new System.Drawing.SizeF(92.48161F, 12F);
            this.lb_giamgia.StylePriority.UseFont = false;
            this.lb_giamgia.StylePriority.UseForeColor = false;
            this.lb_giamgia.StylePriority.UseTextAlignment = false;
            this.lb_giamgia.Text = "0";
            this.lb_giamgia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.291653F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(58F, 12F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseForeColor = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "Giảm Giá :";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lb_tongcong
            // 
            this.lb_tongcong.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_tongcong.ForeColor = System.Drawing.Color.Blue;
            this.lb_tongcong.LocationFloat = new DevExpress.Utils.PointFloat(100.8101F, 41.02376F);
            this.lb_tongcong.Name = "lb_tongcong";
            this.lb_tongcong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_tongcong.SizeF = new System.Drawing.SizeF(92.48154F, 12F);
            this.lb_tongcong.StylePriority.UseFont = false;
            this.lb_tongcong.StylePriority.UseForeColor = false;
            this.lb_tongcong.StylePriority.UseTextAlignment = false;
            xrSummary2.FormatString = "{0:#.00}";
            this.lb_tongcong.Summary = xrSummary2;
            this.lb_tongcong.Text = "0";
            this.lb_tongcong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lb_MaHD
            // 
            this.lb_MaHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_MaHD.ForeColor = System.Drawing.Color.Black;
            this.lb_MaHD.LocationFloat = new DevExpress.Utils.PointFloat(2.395662F, 103.4403F);
            this.lb_MaHD.Name = "lb_MaHD";
            this.lb_MaHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_MaHD.SizeF = new System.Drawing.SizeF(193.2916F, 16.0358F);
            this.lb_MaHD.StylePriority.UseFont = false;
            this.lb_MaHD.StylePriority.UseForeColor = false;
            this.lb_MaHD.StylePriority.UseTextAlignment = false;
            this.lb_MaHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // rpt_inhoadon_Bill_TraSua_58
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.ReportFooter});
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.Margins = new System.Drawing.Printing.Margins(17, 100, 0, 22);
            this.PageHeight = 800;
            this.PageWidth = 326;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ShowPrintMarginsWarning = false;
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this.tblMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel lb_NameBill;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel lb_Header;
        private DevExpress.XtraReports.UI.XRLabel lbFooter;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLabel lb_MaHD;
        private DevExpress.XtraReports.UI.XRPictureBox logo;
        private DevExpress.XtraReports.UI.XRLabel lb_ban;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel lb_ngay;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel lb_Quay;
        private DevExpress.XtraReports.UI.XRLabel Number;
        private DevExpress.XtraReports.UI.XRLabel lb_nhanvien;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lb_mahoadon;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRTable tblMain;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell cl_HangHoa;
        private DevExpress.XtraReports.UI.XRTableCell cl_soluong;
        private DevExpress.XtraReports.UI.XRTableCell cl_giaban;
        private DevExpress.XtraReports.UI.XRTableCell CHIETKHAU;
        private DevExpress.XtraReports.UI.XRTableCell cl_thanhtien;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel lbTiendatcoc;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lbPhihucvu;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel lb_tienkhachtra;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel lb_conlai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel lb_giamgia;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel lb_tongcong;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
    }
}
