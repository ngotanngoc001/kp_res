﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using System.IO;
using System.Windows.Forms;

namespace KP_Report
{
    /// <summary>
    /// Report Ve
    /// </summary>
    public partial class rptVerapphim_Inlai : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report Ve
        /// </summary>
        public rptVerapphim_Inlai()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            //StreamReader str = new StreamReader(Application.StartupPath + "/systeminfo.txt");
            //string sMacuahang = "";
            //string line;
            //while ((line = str.ReadLine()) != null)
            //{
            //    string[] value = line.Split(',');
            //    //Class.ConfigCashier.idShop = value[0].ToString();
            //    //Class.ConfigCashier.nameShop = value[1].ToString();
            //    //Class.ConfigCashier.idCashier = value[2].ToString();
            //    //Class.ConfigCashier.nameCashier = value[3].ToString();
            //    //Class.ConfigCashier.idWarehouse = value[4].ToString();
            //    //Class.ConfigCashier.nameWarehouse = value[5].ToString();
            //    sMacuahang = value[0].ToString();
            //}
            //string sSQL = "";
            //sSQL += "Select MA_CUAHANG,TEN_CUAHANG,GHICHU,SUDUNG,Header,Footer" + "\n";
            //sSQL += "From CUAHANG" + "\n";
            //sSQL += "Where MA_CUAHANG  =" + clsMain .SQLString (sMacuahang )+ "\n";
            //DataTable dt = clsMain.ReturnDataTable(sSQL);
            //txtTencongty.Text = dt.Rows[0]["Header"].ToString();
            //txtTencongty1.Text = dt.Rows[0]["Header"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            DataTable dt = (DataTable)DataSource;
            foreach (DataRow dr in dt.Rows)
            {
                try
                {
                    MABARCODE.Text = MABARCODE1.Text = Right_str(dr["MABARCODE"].ToString(), 7);
                }
                catch
                {
                    MABARCODE.Text = MABARCODE1.Text = dr["MABARCODE"].ToString();
                }
                TENPHIM.Text = TENPHIM1.Text = dr["TENPHIM"].ToString();
                TENPHONG.Text = TENPHONG1.Text = dr["TENPHONG"].ToString();
                SOGHE.Text = SOGHE1.Text = dr["SOGHE"].ToString();
                DONGIA.Text = DONGIA1.Text = string.Format("{0:#,###0}", dr["DONGIA"]);
                NGAYCHIEU.Text = NGAYCHIEU1.Text = string.Format("{0:dd/MM/yyyy}", dr["NGAYCHIEU"]);
                BATDAU.Text = BATDAU1.Text = string.Format("{0:HH:mm}", dr["BATDAU"]);
                NGAYTAO.Text = NGAYTAO1.Text = string.Format("{0:dd/MM/yyyy HH:mm}", dr["NGAYTAO"]);
                NGUOITAO.Text = NGUOITAO1.Text = dr["MABARCODE"].ToString() + "  " + dr["NGUOITAO"].ToString();
                LOAIVE.Text = LOAIVE1.Text = dr["TENLOAIVE"].ToString();
                LANIN.Text = LANIN1.Text ="IL:"+ dr["LANIN"].ToString();
            }
        }
        private string Right_str(string value, int length)
        {
            return value.Substring(value.Length - length);
        }

      
    }
}
