﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.NativeBricks;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBiennhan1 : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBiennhan1()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            MAPHIEU.DataBindings.Add("Text", DataSource, "MAPHIEU");
            MAPHIEU1.DataBindings.Add("Text", DataSource, "MAPHIEU");
            NGAYTAO.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy}");
            TENHANHKHACH.DataBindings.Add("Text", DataSource, "TENHANHKHACH");
            DIENTHOAI.DataBindings.Add("Text", DataSource, "DIENTHOAI");
            TENTUYEN.DataBindings.Add("Text", DataSource, "TENPHIM");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:#,###0}");
            NGAYDI.DataBindings.Add("Text", DataSource, "NGAYCHIEU", "{0:dd/MM/yyyy}");
            GIODI.DataBindings.Add("Text", DataSource, "BATDAU");
            SOGHE.DataBindings.Add("Text", DataSource, "SOGHE");
            NGUOITAO.DataBindings.Add("Text", DataSource, "NGUOITAO");
            NGAYTAO1.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy HH : mm}");
            TONGTIEN.DataBindings.Add("Text", DataSource, "TONGGIAVE", "{0:#,###0}");
            DataTable dt = (DataTable)DataSource;
            j = dt.Rows.Count;
        }

        int i=0;
        int j = 0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (i == j)
            {
                i = 0;
            }
            i++;
            STT.Text = i.ToString();
        }
    }
}
