﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using System.IO;
using System.Windows.Forms;

namespace KP_Report
{
    /// <summary>
    /// Report Ve
    /// </summary>
    public partial class rptVe : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report Ve
        /// </summary>
        public rptVe()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            StreamReader str = new StreamReader(Application.StartupPath + "/systeminfo.txt");
            string sMacuahang = "";
            string line;
            while ((line = str.ReadLine()) != null)
            {
                string[] value = line.Split(',');
                //Class.ConfigCashier.idShop = value[0].ToString();
                //Class.ConfigCashier.nameShop = value[1].ToString();
                //Class.ConfigCashier.idCashier = value[2].ToString();
                //Class.ConfigCashier.nameCashier = value[3].ToString();
                //Class.ConfigCashier.idWarehouse = value[4].ToString();
                //Class.ConfigCashier.nameWarehouse = value[5].ToString();
                sMacuahang = value[0].ToString();
            }

            string sSQL = "";
            sSQL += "Select MA_CUAHANG,	TEN_CUAHANG,	GHICHU,	SUDUNG,	Header,	Footer" + "\n";
            sSQL += "From CUAHANG" + "\n";
            sSQL += "Where MA_CUAHANG  =" + clsMain.SQLString(sMacuahang) + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            //string sSQL = "";
            //sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            //sSQL += "From SYS_CONFIGREPORT" + "\n";
            //DataTable dt = clsMain.ReturnDataTable(sSQL);

            txtTencongty.Text = dt.Rows[0]["Header"].ToString();
            //txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            //txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
           // txtTencongty1.Text = dt.Rows[0]["Header"].ToString();
            //txtDienthoai1.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            MABARCODE.DataBindings.Add("Text", DataSource, "MA_VEBAN");
            MABARCODE1.DataBindings.Add("Text", DataSource, "MA_VEBAN");
            TENTUYEN.DataBindings.Add("Text", DataSource, "TEN_TROCHOI");
            TENTUYEN1.DataBindings.Add("Text", DataSource, "TEN_TROCHOI");
            SOGHE.DataBindings.Add("Text", DataSource, "GIABAN", "{0:#,###0}");
            SOGHE1.DataBindings.Add("Text", DataSource, "GIABAN" ,"{0:#,###0}");
            NGAYDI.DataBindings.Add("Text", DataSource, "NGAYBAN", "{0:dd/MM/yyyy}");
            NGAYDI1.DataBindings.Add("Text", DataSource, "NGAYBAN", "{0:dd/MM/yyyy}");
            GIODI.DataBindings.Add("Text", DataSource, "NGAYBAN", "{0:HH : mm}");
            GIODI1.DataBindings.Add("Text", DataSource, "NGAYBAN", "{0:HH : mm}");
            NGUOITAO.DataBindings.Add("Text", DataSource, "TENNHANVIEN");
            NGAYTAO.DataBindings.Add("Text", DataSource, "NGAYBAN", "{0:dd/MM/yyyy HH : mm}");
            NGUOITAO1.DataBindings.Add("Text", DataSource, "TENNHANVIEN");
            NGAYTAO1.DataBindings.Add("Text", DataSource, "NGAYBAN", "{0:dd/MM/yyyy HH : mm}");
          
        }
    }
}
