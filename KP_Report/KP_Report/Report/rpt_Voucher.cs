﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rpt_Voucher : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rpt_Voucher()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            ID.DataBindings.Add("Text", DataSource, "ID");
            NAME.DataBindings.Add("Text", DataSource, "NAME");
            NAME_STYLE.DataBindings.Add("Text", DataSource, "NAME_STYLE");
            ACTIVE_DATE.DataBindings.Add("Text", DataSource, "ACTIVE_DATE", "{0:dd/MM/yyyy}");
            EXP_DATE.DataBindings.Add("Text", DataSource, "EXP_DATE", "{0:dd/MM/yyyy}");
            VALUE_PERCENT.DataBindings.Add("Text", DataSource, "VALUE_PERCENT", "{0:#,###0}");
            VALUE_AMOUNT.DataBindings.Add("Text", DataSource, "VALUE_AMOUNT", "{0:#,###0}");
            DES.DataBindings.Add("Text", DataSource, "DES");
            
            DataTable dtSource = (DataTable)DataSource;
            txt_tieude.Text = dtSource.Rows[0]["HEARDER"].ToString();
        }

        int i = 0;
        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i = 0;
        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
