﻿namespace KP_Report
{
    partial class rptVe1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.BarCode.Code128Generator code128Generator1 = new DevExpress.XtraPrinting.BarCode.Code128Generator();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.TENLOAIVE = new DevExpress.XtraReports.UI.XRLabel();
            this.MABARCODE2 = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.TENHANHKHACH1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.TENHANHKHACH = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYTAO1 = new DevExpress.XtraReports.UI.XRLabel();
            this.NGUOITAO1 = new DevExpress.XtraReports.UI.XRLabel();
            this.LOAIGHE = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYTAO = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.CMND = new DevExpress.XtraReports.UI.XRLabel();
            this.GIODI1 = new DevExpress.XtraReports.UI.XRLabel();
            this.SOGHE1 = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYDI1 = new DevExpress.XtraReports.UI.XRLabel();
            this.NGUOITAO = new DevExpress.XtraReports.UI.XRLabel();
            this.MABARCODE1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TENTUYEN1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TENDOITUONG1 = new DevExpress.XtraReports.UI.XRLabel();
            this.SOGHE = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.GIODI = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYDI = new DevExpress.XtraReports.UI.XRLabel();
            this.TENDOITUONG = new DevExpress.XtraReports.UI.XRLabel();
            this.TENTUYEN = new DevExpress.XtraReports.UI.XRLabel();
            this.MABARCODE = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.BorderWidth = 0;
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel5,
            this.xrLabel3,
            this.xrLabel1,
            this.xrLabel13,
            this.xrLabel11,
            this.xrLabel9,
            this.xrLabel7,
            this.xrLabel12,
            this.xrLabel10,
            this.xrLabel8,
            this.xrLabel6,
            this.TENLOAIVE,
            this.MABARCODE2,
            this.xrLabel4,
            this.TENHANHKHACH1,
            this.xrLabel2,
            this.TENHANHKHACH,
            this.NGAYTAO1,
            this.NGUOITAO1,
            this.LOAIGHE,
            this.xrLabel47,
            this.xrLabel46,
            this.NGAYTAO,
            this.xrLabel43,
            this.CMND,
            this.GIODI1,
            this.SOGHE1,
            this.NGAYDI1,
            this.NGUOITAO,
            this.MABARCODE1,
            this.TENTUYEN1,
            this.TENDOITUONG1,
            this.SOGHE,
            this.xrLabel31,
            this.GIODI,
            this.NGAYDI,
            this.TENDOITUONG,
            this.TENTUYEN,
            this.MABARCODE});
            this.Detail.Dpi = 254F;
            this.Detail.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Detail.HeightF = 610F;
            this.Detail.LockedInUserDesigner = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.Detail.StylePriority.UseBorderWidth = false;
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.CanGrow = false;
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(1015.396F, 291.0284F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(150.8116F, 44.13254F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UsePadding = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Loại";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.CanGrow = false;
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(1015.396F, 368.4261F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(150.8116F, 44.13254F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UsePadding = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Tên";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.CanGrow = false;
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(1015.396F, 480.8691F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(150.8116F, 44.13254F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UsePadding = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "CMND";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.CanGrow = false;
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(535.8748F, 480.8675F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(150.8116F, 44.13254F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UsePadding = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "Xe";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel11
            // 
            this.xrLabel11.CanGrow = false;
            this.xrLabel11.Dpi = 254F;
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(535.8748F, 422F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(150.8115F, 44.13251F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UsePadding = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "Giờ";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.CanGrow = false;
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(535.8748F, 368.426F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(150.8115F, 44.13251F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UsePadding = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "Ngày";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.CanGrow = false;
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(535.8748F, 308.4243F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(150.8115F, 44.13254F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UsePadding = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Tuyến";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel12
            // 
            this.xrLabel12.CanGrow = false;
            this.xrLabel12.Dpi = 254F;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(35.87499F, 480.8675F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(150.8116F, 44.13254F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UsePadding = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "Xe";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.CanGrow = false;
            this.xrLabel10.Dpi = 254F;
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(35.87499F, 422F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(150.8115F, 44.13251F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UsePadding = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "Giờ";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.CanGrow = false;
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(35.87499F, 368.426F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(150.8115F, 44.13251F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UsePadding = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "Ngày";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.CanGrow = false;
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(35.87499F, 308.4243F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(150.8115F, 44.13254F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UsePadding = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Tuyến";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // TENLOAIVE
            // 
            this.TENLOAIVE.CanGrow = false;
            this.TENLOAIVE.Dpi = 254F;
            this.TENLOAIVE.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TENLOAIVE.LocationFloat = new DevExpress.Utils.PointFloat(1166.207F, 335.1609F);
            this.TENLOAIVE.Name = "TENLOAIVE";
            this.TENLOAIVE.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENLOAIVE.SizeF = new System.Drawing.SizeF(262.877F, 44.13254F);
            this.TENLOAIVE.StylePriority.UseFont = false;
            this.TENLOAIVE.StylePriority.UsePadding = false;
            this.TENLOAIVE.StylePriority.UseTextAlignment = false;
            this.TENLOAIVE.Text = "(Thong thuong)";
            this.TENLOAIVE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // MABARCODE2
            // 
            this.MABARCODE2.AutoModule = true;
            this.MABARCODE2.BarCodeOrientation = DevExpress.XtraPrinting.BarCode.BarCodeOrientation.RotateLeft;
            this.MABARCODE2.Dpi = 254F;
            this.MABARCODE2.LocationFloat = new DevExpress.Utils.PointFloat(1370.647F, 157.3617F);
            this.MABARCODE2.Module = 5.08F;
            this.MABARCODE2.Name = "MABARCODE2";
            this.MABARCODE2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.MABARCODE2.ShowText = false;
            this.MABARCODE2.SizeF = new System.Drawing.SizeF(73.98F, 367.64F);
            this.MABARCODE2.StylePriority.UsePadding = false;
            this.MABARCODE2.StylePriority.UseTextAlignment = false;
            this.MABARCODE2.Symbology = code128Generator1;
            this.MABARCODE2.Text = "*1234567*";
            this.MABARCODE2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel4
            // 
            this.xrLabel4.CanGrow = false;
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(542.6875F, 207.9583F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(76.72867F, 44.13255F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UsePadding = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Ghế";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // TENHANHKHACH1
            // 
            this.TENHANHKHACH1.CanGrow = false;
            this.TENHANHKHACH1.Dpi = 254F;
            this.TENHANHKHACH1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.TENHANHKHACH1.LocationFloat = new DevExpress.Utils.PointFloat(186.6865F, 252.0909F);
            this.TENHANHKHACH1.Name = "TENHANHKHACH1";
            this.TENHANHKHACH1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENHANHKHACH1.SizeF = new System.Drawing.SizeF(299.4375F, 47.99994F);
            this.TENHANHKHACH1.StylePriority.UseFont = false;
            this.TENHANHKHACH1.StylePriority.UsePadding = false;
            this.TENHANHKHACH1.StylePriority.UseTextAlignment = false;
            this.TENHANHKHACH1.Text = "GIANG THI ANH MINH";
            this.TENHANHKHACH1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.CanGrow = false;
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(35.87499F, 255.9583F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(150.8115F, 44.13255F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UsePadding = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Tên";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // TENHANHKHACH
            // 
            this.TENHANHKHACH.CanGrow = false;
            this.TENHANHKHACH.Dpi = 254F;
            this.TENHANHKHACH.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.TENHANHKHACH.LocationFloat = new DevExpress.Utils.PointFloat(1069.958F, 379.2934F);
            this.TENHANHKHACH.Name = "TENHANHKHACH";
            this.TENHANHKHACH.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENHANHKHACH.SizeF = new System.Drawing.SizeF(300.6886F, 97.70657F);
            this.TENHANHKHACH.StylePriority.UseFont = false;
            this.TENHANHKHACH.StylePriority.UsePadding = false;
            this.TENHANHKHACH.StylePriority.UseTextAlignment = false;
            this.TENHANHKHACH.Text = "GIANG THI ANH MINH";
            this.TENHANHKHACH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // NGAYTAO1
            // 
            this.NGAYTAO1.CanGrow = false;
            this.NGAYTAO1.Dpi = 254F;
            this.NGAYTAO1.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Italic);
            this.NGAYTAO1.LocationFloat = new DevExpress.Utils.PointFloat(261.8118F, 537.9891F);
            this.NGAYTAO1.Name = "NGAYTAO1";
            this.NGAYTAO1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYTAO1.SizeF = new System.Drawing.SizeF(235.6258F, 44.13269F);
            this.NGAYTAO1.StylePriority.UseFont = false;
            this.NGAYTAO1.StylePriority.UsePadding = false;
            this.NGAYTAO1.StylePriority.UseTextAlignment = false;
            this.NGAYTAO1.Text = "21/12/2012 13 : 30";
            this.NGAYTAO1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // NGUOITAO1
            // 
            this.NGUOITAO1.CanGrow = false;
            this.NGUOITAO1.Dpi = 254F;
            this.NGUOITAO1.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Italic);
            this.NGUOITAO1.LocationFloat = new DevExpress.Utils.PointFloat(35.87499F, 537.9891F);
            this.NGUOITAO1.Name = "NGUOITAO1";
            this.NGUOITAO1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGUOITAO1.SizeF = new System.Drawing.SizeF(246F, 44.13281F);
            this.NGUOITAO1.StylePriority.UseFont = false;
            this.NGUOITAO1.StylePriority.UsePadding = false;
            this.NGUOITAO1.StylePriority.UseTextAlignment = false;
            this.NGUOITAO1.Text = "Phan Bá Long ";
            this.NGUOITAO1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // LOAIGHE
            // 
            this.LOAIGHE.CanGrow = false;
            this.LOAIGHE.Dpi = 254F;
            this.LOAIGHE.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.LOAIGHE.LocationFloat = new DevExpress.Utils.PointFloat(1166.207F, 291.0283F);
            this.LOAIGHE.Name = "LOAIGHE";
            this.LOAIGHE.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.LOAIGHE.SizeF = new System.Drawing.SizeF(204.4376F, 44.13257F);
            this.LOAIGHE.StylePriority.UseFont = false;
            this.LOAIGHE.StylePriority.UsePadding = false;
            this.LOAIGHE.StylePriority.UseTextAlignment = false;
            this.LOAIGHE.Text = "Ghế thường";
            this.LOAIGHE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel47
            // 
            this.xrLabel47.Dpi = 254F;
            this.xrLabel47.Font = new System.Drawing.Font("Times New Roman", 9.5F, System.Drawing.FontStyle.Bold);
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(14.99999F, 115.3617F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(485.35F, 42F);
            this.xrLabel47.StylePriority.UseFont = false;
            this.xrLabel47.StylePriority.UsePadding = false;
            this.xrLabel47.StylePriority.UseTextAlignment = false;
            this.xrLabel47.Text = "THẺ HÀNH KHÁCH LÊN XE";
            this.xrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel46
            // 
            this.xrLabel46.Dpi = 254F;
            this.xrLabel46.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(553.2708F, 115.3617F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(785.6252F, 52.07007F);
            this.xrLabel46.StylePriority.UseFont = false;
            this.xrLabel46.StylePriority.UsePadding = false;
            this.xrLabel46.StylePriority.UseTextAlignment = false;
            this.xrLabel46.Text = "THẺ HÀNH KHÁCH LÊN XE";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // NGAYTAO
            // 
            this.NGAYTAO.CanGrow = false;
            this.NGAYTAO.Dpi = 254F;
            this.NGAYTAO.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Italic);
            this.NGAYTAO.LocationFloat = new DevExpress.Utils.PointFloat(1107.77F, 537.9891F);
            this.NGAYTAO.Name = "NGAYTAO";
            this.NGAYTAO.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYTAO.SizeF = new System.Drawing.SizeF(262.8749F, 44.13263F);
            this.NGAYTAO.StylePriority.UseFont = false;
            this.NGAYTAO.StylePriority.UsePadding = false;
            this.NGAYTAO.StylePriority.UseTextAlignment = false;
            this.NGAYTAO.Text = "21/12/2012 13 : 30";
            this.NGAYTAO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel43
            // 
            this.xrLabel43.CanGrow = false;
            this.xrLabel43.Dpi = 254F;
            this.xrLabel43.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(1069.958F, 537.9891F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(37.81213F, 44.13263F);
            this.xrLabel43.StylePriority.UseFont = false;
            this.xrLabel43.StylePriority.UsePadding = false;
            this.xrLabel43.StylePriority.UseTextAlignment = false;
            this.xrLabel43.Text = "-";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // CMND
            // 
            this.CMND.CanGrow = false;
            this.CMND.Dpi = 254F;
            this.CMND.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.CMND.LocationFloat = new DevExpress.Utils.PointFloat(1166.207F, 477F);
            this.CMND.Name = "CMND";
            this.CMND.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.CMND.SizeF = new System.Drawing.SizeF(204.4376F, 47.99997F);
            this.CMND.StylePriority.UseFont = false;
            this.CMND.StylePriority.UsePadding = false;
            this.CMND.StylePriority.UseTextAlignment = false;
            this.CMND.Text = "2507438701";
            this.CMND.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // GIODI1
            // 
            this.GIODI1.CanGrow = false;
            this.GIODI1.Dpi = 254F;
            this.GIODI1.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.GIODI1.LocationFloat = new DevExpress.Utils.PointFloat(686.6864F, 422F);
            this.GIODI1.Name = "GIODI1";
            this.GIODI1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.GIODI1.SizeF = new System.Drawing.SizeF(299.4376F, 44.13257F);
            this.GIODI1.StylePriority.UseFont = false;
            this.GIODI1.StylePriority.UsePadding = false;
            this.GIODI1.StylePriority.UseTextAlignment = false;
            this.GIODI1.Text = "06 : 00";
            this.GIODI1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // SOGHE1
            // 
            this.SOGHE1.CanGrow = false;
            this.SOGHE1.Dpi = 254F;
            this.SOGHE1.Font = new System.Drawing.Font("Times New Roman", 33F, System.Drawing.FontStyle.Bold);
            this.SOGHE1.LocationFloat = new DevExpress.Utils.PointFloat(686.6864F, 157.3617F);
            this.SOGHE1.Name = "SOGHE1";
            this.SOGHE1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.SOGHE1.SizeF = new System.Drawing.SizeF(338.4166F, 142.7291F);
            this.SOGHE1.StylePriority.UseFont = false;
            this.SOGHE1.StylePriority.UsePadding = false;
            this.SOGHE1.StylePriority.UseTextAlignment = false;
            this.SOGHE1.Text = "H22";
            this.SOGHE1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // NGAYDI1
            // 
            this.NGAYDI1.CanGrow = false;
            this.NGAYDI1.Dpi = 254F;
            this.NGAYDI1.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.NGAYDI1.LocationFloat = new DevExpress.Utils.PointFloat(686.6864F, 368.426F);
            this.NGAYDI1.Name = "NGAYDI1";
            this.NGAYDI1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYDI1.SizeF = new System.Drawing.SizeF(299.4376F, 44.13257F);
            this.NGAYDI1.StylePriority.UseFont = false;
            this.NGAYDI1.StylePriority.UsePadding = false;
            this.NGAYDI1.StylePriority.UseTextAlignment = false;
            this.NGAYDI1.Text = "21/12/2012";
            this.NGAYDI1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // NGUOITAO
            // 
            this.NGUOITAO.CanGrow = false;
            this.NGUOITAO.Dpi = 254F;
            this.NGUOITAO.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Italic);
            this.NGUOITAO.LocationFloat = new DevExpress.Utils.PointFloat(661.7505F, 537.9891F);
            this.NGUOITAO.Name = "NGUOITAO";
            this.NGUOITAO.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGUOITAO.SizeF = new System.Drawing.SizeF(408.2075F, 44.12994F);
            this.NGUOITAO.StylePriority.UseFont = false;
            this.NGUOITAO.StylePriority.UsePadding = false;
            this.NGUOITAO.StylePriority.UseTextAlignment = false;
            this.NGUOITAO.Text = "Phan Bá Long";
            this.NGUOITAO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // MABARCODE1
            // 
            this.MABARCODE1.CanGrow = false;
            this.MABARCODE1.Dpi = 254F;
            this.MABARCODE1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.MABARCODE1.LocationFloat = new DevExpress.Utils.PointFloat(542.6875F, 157.3616F);
            this.MABARCODE1.Name = "MABARCODE1";
            this.MABARCODE1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.MABARCODE1.SizeF = new System.Drawing.SizeF(158.5808F, 44.13252F);
            this.MABARCODE1.StylePriority.UseFont = false;
            this.MABARCODE1.StylePriority.UsePadding = false;
            this.MABARCODE1.StylePriority.UseTextAlignment = false;
            this.MABARCODE1.Text = "2543648";
            this.MABARCODE1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TENTUYEN1
            // 
            this.TENTUYEN1.CanGrow = false;
            this.TENTUYEN1.Dpi = 254F;
            this.TENTUYEN1.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.TENTUYEN1.LocationFloat = new DevExpress.Utils.PointFloat(686.6863F, 308.4242F);
            this.TENTUYEN1.Name = "TENTUYEN1";
            this.TENTUYEN1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENTUYEN1.SizeF = new System.Drawing.SizeF(328.7096F, 60.0018F);
            this.TENTUYEN1.StylePriority.UseFont = false;
            this.TENTUYEN1.StylePriority.UsePadding = false;
            this.TENTUYEN1.StylePriority.UseTextAlignment = false;
            this.TENTUYEN1.Text = "Bến phà-Trung tâm asa";
            this.TENTUYEN1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TENDOITUONG1
            // 
            this.TENDOITUONG1.CanGrow = false;
            this.TENDOITUONG1.Dpi = 254F;
            this.TENDOITUONG1.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.TENDOITUONG1.LocationFloat = new DevExpress.Utils.PointFloat(686.6864F, 480.8675F);
            this.TENDOITUONG1.Name = "TENDOITUONG1";
            this.TENDOITUONG1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENDOITUONG1.SizeF = new System.Drawing.SizeF(299.4376F, 44.13257F);
            this.TENDOITUONG1.StylePriority.UseFont = false;
            this.TENDOITUONG1.StylePriority.UsePadding = false;
            this.TENDOITUONG1.StylePriority.UseTextAlignment = false;
            this.TENDOITUONG1.Text = "Thringving2";
            this.TENDOITUONG1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // SOGHE
            // 
            this.SOGHE.CanGrow = false;
            this.SOGHE.Dpi = 254F;
            this.SOGHE.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Bold);
            this.SOGHE.LocationFloat = new DevExpress.Utils.PointFloat(186.6865F, 172.8257F);
            this.SOGHE.Name = "SOGHE";
            this.SOGHE.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.SOGHE.SizeF = new System.Drawing.SizeF(267.6875F, 79.2652F);
            this.SOGHE.StylePriority.UseFont = false;
            this.SOGHE.StylePriority.UsePadding = false;
            this.SOGHE.StylePriority.UseTextAlignment = false;
            this.SOGHE.Text = "H22";
            this.SOGHE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel31
            // 
            this.xrLabel31.CanGrow = false;
            this.xrLabel31.Dpi = 254F;
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(35.87499F, 207.9583F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(150.8115F, 44.13257F);
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.StylePriority.UsePadding = false;
            this.xrLabel31.StylePriority.UseTextAlignment = false;
            this.xrLabel31.Text = "Ghế";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // GIODI
            // 
            this.GIODI.CanGrow = false;
            this.GIODI.Dpi = 254F;
            this.GIODI.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.GIODI.LocationFloat = new DevExpress.Utils.PointFloat(186.6865F, 422F);
            this.GIODI.Name = "GIODI";
            this.GIODI.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.GIODI.SizeF = new System.Drawing.SizeF(299.4376F, 44.13257F);
            this.GIODI.StylePriority.UseFont = false;
            this.GIODI.StylePriority.UsePadding = false;
            this.GIODI.StylePriority.UseTextAlignment = false;
            this.GIODI.Text = "06 : 00";
            this.GIODI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // NGAYDI
            // 
            this.NGAYDI.CanGrow = false;
            this.NGAYDI.Dpi = 254F;
            this.NGAYDI.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.NGAYDI.LocationFloat = new DevExpress.Utils.PointFloat(186.6865F, 368.426F);
            this.NGAYDI.Name = "NGAYDI";
            this.NGAYDI.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYDI.SizeF = new System.Drawing.SizeF(299.4376F, 44.13257F);
            this.NGAYDI.StylePriority.UseFont = false;
            this.NGAYDI.StylePriority.UsePadding = false;
            this.NGAYDI.StylePriority.UseTextAlignment = false;
            this.NGAYDI.Text = "21/12/2012";
            this.NGAYDI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // TENDOITUONG
            // 
            this.TENDOITUONG.CanGrow = false;
            this.TENDOITUONG.Dpi = 254F;
            this.TENDOITUONG.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.TENDOITUONG.LocationFloat = new DevExpress.Utils.PointFloat(186.6865F, 480.8675F);
            this.TENDOITUONG.Name = "TENDOITUONG";
            this.TENDOITUONG.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENDOITUONG.SizeF = new System.Drawing.SizeF(299.4376F, 44.13254F);
            this.TENDOITUONG.StylePriority.UseFont = false;
            this.TENDOITUONG.StylePriority.UsePadding = false;
            this.TENDOITUONG.StylePriority.UseTextAlignment = false;
            this.TENDOITUONG.Text = "Thringving2";
            this.TENDOITUONG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // TENTUYEN
            // 
            this.TENTUYEN.CanGrow = false;
            this.TENTUYEN.Dpi = 254F;
            this.TENTUYEN.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.TENTUYEN.LocationFloat = new DevExpress.Utils.PointFloat(186.6865F, 308.4242F);
            this.TENTUYEN.Name = "TENTUYEN";
            this.TENTUYEN.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENTUYEN.SizeF = new System.Drawing.SizeF(313.6635F, 60.00183F);
            this.TENTUYEN.StylePriority.UseFont = false;
            this.TENTUYEN.StylePriority.UsePadding = false;
            this.TENTUYEN.StylePriority.UseTextAlignment = false;
            this.TENTUYEN.Text = "Bến phà-Trung tâm asa";
            this.TENTUYEN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // MABARCODE
            // 
            this.MABARCODE.CanGrow = false;
            this.MABARCODE.Dpi = 254F;
            this.MABARCODE.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.MABARCODE.LocationFloat = new DevExpress.Utils.PointFloat(35.87499F, 157.3617F);
            this.MABARCODE.Name = "MABARCODE";
            this.MABARCODE.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.MABARCODE.SizeF = new System.Drawing.SizeF(171.6865F, 44.13252F);
            this.MABARCODE.StylePriority.UseFont = false;
            this.MABARCODE.StylePriority.UsePadding = false;
            this.MABARCODE.StylePriority.UseTextAlignment = false;
            this.MABARCODE.Text = "2543648";
            this.MABARCODE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 10F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 10F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // formattingRule1
            // 
            this.formattingRule1.Name = "formattingRule1";
            // 
            // rptVe1
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Dpi = 254F;
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(10, 10, 10, 10);
            this.PageHeight = 630;
            this.PageWidth = 1500;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.ShowPrintMarginsWarning = false;
            this.ShowPrintStatusDialog = false;
            this.SnapGridSize = 5F;
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel GIODI;
        private DevExpress.XtraReports.UI.XRLabel NGAYDI;
        private DevExpress.XtraReports.UI.XRLabel TENDOITUONG;
        private DevExpress.XtraReports.UI.XRLabel TENTUYEN;
        private DevExpress.XtraReports.UI.XRLabel MABARCODE;
        private DevExpress.XtraReports.UI.XRLabel SOGHE;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel SOGHE1;
        private DevExpress.XtraReports.UI.XRLabel NGAYDI1;
        private DevExpress.XtraReports.UI.XRLabel NGUOITAO;
        private DevExpress.XtraReports.UI.XRLabel MABARCODE1;
        private DevExpress.XtraReports.UI.XRLabel TENTUYEN1;
        private DevExpress.XtraReports.UI.XRLabel TENDOITUONG1;
        private DevExpress.XtraReports.UI.XRLabel NGAYTAO;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel CMND;
        private DevExpress.XtraReports.UI.XRLabel GIODI1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        private DevExpress.XtraReports.UI.XRLabel LOAIGHE;
        private DevExpress.XtraReports.UI.XRLabel NGAYTAO1;
        private DevExpress.XtraReports.UI.XRLabel NGUOITAO1;
        private DevExpress.XtraReports.UI.XRLabel TENHANHKHACH;
        private DevExpress.XtraReports.UI.XRLabel TENHANHKHACH1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRBarCode MABARCODE2;
        private DevExpress.XtraReports.UI.XRLabel TENLOAIVE;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
    }
}
