﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptLichchieuphim2 : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptLichchieuphim2()
        {
            InitializeComponent();
            GroupField grf = new GroupField("TENDIADIEM");
            GroupHeader3.GroupFields.Add(grf);
            GroupField grf1 = new GroupField("TENPHIM");
            GroupHeader2.GroupFields.Add(grf1);
            GroupField grf2 = new GroupField("NGAYCHIEU");
            GroupHeader1.GroupFields.Add(grf2);
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            byte[] tam = new byte[((byte[])dt.Rows[0]["HINHANH"]).LongLength];
            tam = (byte[])dt.Rows[0]["HINHANH"];
            System.IO.MemoryStream ms = new System.IO.MemoryStream(tam);
            Bitmap bm = new Bitmap(ms);
            logo.Image = bm;
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        int i ;
        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i = 0;
        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            DataTable dtSource = (DataTable)DataSource;

            byte[] tam = new byte[((byte[])dtSource.Rows[0]["IMAGE"]).LongLength];
            tam = (byte[])dtSource.Rows[0]["IMAGE"];
            System.IO.MemoryStream ms = new System.IO.MemoryStream(tam);
            Bitmap bm = new Bitmap(ms);
            xrPictureBox1.Image = bm;

            txt_tieude.Text = "(" + dtSource.Rows[0]["TYPE"].ToString() + ": " + dtSource.Rows[0]["NAMETYPE"].ToString() + " )";
            txt_tieude1.Text = "( Từ ngày: " + dtSource.Rows[0]["TUNGAY"].ToString() + " - Đến ngày: " + dtSource.Rows[0]["DENNGAY"].ToString() + " )";
        }
    }
}
