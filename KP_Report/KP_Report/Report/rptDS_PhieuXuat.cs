﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptDS_PhieuXuat : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptDS_PhieuXuat()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            MAPHIEU.DataBindings.Add("Text", DataSource, "MA_PHIEU");
            NGAYCHUNGTU.DataBindings.Add("Text", DataSource, "NGAYCHUNGTU", "{0:dd/MM/yyyy}");
            NGUOITAO.DataBindings.Add("Text", DataSource, "NGUOITAO");
            NGAYTAO.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy}");
            KHONHAP.DataBindings.Add("Text", DataSource, "KHO");
            PHUONGTHUC.DataBindings.Add("Text", DataSource, "PT_TEN");
            NHACUNGCAP.DataBindings.Add("Text", DataSource, "NHACUNGCAP");
            TONGTIEN.DataBindings.Add("Text", DataSource, "TONGTIEN", "{0:#,###0}");
            GHICHU.DataBindings.Add("Text", DataSource, "GHICHU");
            TONGCONG.DataBindings.Add("Text", DataSource, "TONGTIEN");
            TONGCONG.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");
            DataTable dtSource = (DataTable)DataSource;
           
            txt_tieude.Text = "( Từ ngày: " + dtSource.Rows[0]["TUNGAY"].ToString() + " - Đến ngày: " + dtSource.Rows[0]["DENNGAY"].ToString() + " )";
        }

        int i = 0;
        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i = 0;
        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
