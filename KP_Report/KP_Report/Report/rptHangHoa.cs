﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptHangHoa : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptHangHoa()
        {
            InitializeComponent();
            GroupField grf = new GroupField("TEN_NHOMHANG");
            GroupHeader1.GroupFields.Add(grf);
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            NHOMHANG.DataBindings.Add("Text", DataSource, "TEN_NHOMHANG");
            MA_HANGHOA.DataBindings.Add("Text", DataSource, "MA_HANGHOA");
            TENHANGHOA.DataBindings.Add("Text", DataSource, "TEN_HANGHOA");
            DVT.DataBindings.Add("Text", DataSource, "TEN_DONVITINH");
            VAT.DataBindings.Add("Text", DataSource, "THUE", "{0:#,###0}%");
            GIANHAP.DataBindings.Add("Text", DataSource, "GIANHAP", "{0:#,###0}");
            GIABAN1.DataBindings.Add("Text", DataSource, "GIABAN1", "{0:#,###0}");
            GIABAN2.DataBindings.Add("Text", DataSource, "GIABAN2", "{0:#,###0}");
            chkTONKHO.DataBindings.Add("Checked", DataSource, "TONKHO");
            chkTHUCDON.DataBindings.Add("Checked", DataSource, "THUCDON");
            DataTable dtSource = (DataTable)DataSource;
            if (dtSource.Rows[0]["FILTER"].ToString() != "")
            {
                txt_tieude.Text = "(" + dtSource.Rows[0]["FILTER"].ToString() + ")";
            }
        }

        int i = 0;
        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i = 0;
        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
