﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using System.Data;
using KP_Report;
using KP_UserManagement;
using System.IO;
using System.Windows.Forms;


namespace KP_Report
{
    /// <summary>
    /// rpt_BillDatBan
    /// </summary>
    public partial class rpt_BillDatBan : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// checkBill
        /// </summary>
        public int checkBill=0;
        /// <summary>
        /// rpt_BillDatBan
        /// </summary>
        public rpt_BillDatBan()
        {
            InitializeComponent();                      
            Setheader();            
        }
        /// <summary>
        /// BindData
        /// </summary>
        public void BindData()
        {
            lb_NameBill.DataBindings.Add("Text", DataSource, "Ten_HoaDon");
                    
            cl_STT.DataBindings.Add("Text", DataSource, "STT");
            cl_HangHoa.DataBindings.Add("Text", DataSource, "TEN_HANGHOA");
            cl_giaban.DataBindings.Add("Text", DataSource, "GIABAN", "{0:#,###0}");
            cl_soluong.DataBindings.Add("Text", DataSource, "SOLUONG");
            //cl_vat.DataBindings.Add("Text", DataSource, "HH_VAT", "VAT: {0} %");
            cl_thanhtien.DataBindings.Add("Text", DataSource, "Thanh_Tien", "{0:#,###0}");
            lb_MoneySetFirst.DataBindings.Add("Text", DataSource, "TienDatCoc", "{0:#,###0}");
            

            //header
            lb_Quay.DataBindings.Add("Text", DataSource, "Ma_Quay");
            lb_mahoadon.DataBindings.Add("Text", DataSource, "Ma_HoaDon");           
            lb_nhanvien.DataBindings.Add("Text", DataSource, "TenNV");
            

            lb_TenKH.DataBindings.Add("Text", DataSource, "TenKhachHang");
            lb_Phone.DataBindings.Add("Text", DataSource, "DienThoai");
            lb_KhuVuc.DataBindings.Add("Text", DataSource, "KhuVuc");
            lb_idTables.DataBindings.Add("Text", DataSource, "MABAN");
            lb_Timeto.DataBindings.Add("Text", DataSource, "GIOVAO");
            lb_ngay.DataBindings.Add("Text", DataSource, "Ngay");
            
        } 

         private void Setheader()
        {
            StreamReader str = new StreamReader(Application.StartupPath + "/systeminfo.txt");
            string sMacuahang = "";
            string line;
            while ((line = str.ReadLine()) != null)
            {
                string[] value = line.Split(',');
                sMacuahang = value[0].ToString();
            }

            string sSQL = "";
            sSQL += "Select MA_CUAHANG,	TEN_CUAHANG,	GHICHU,	SUDUNG,	Header,	Footer" + "\n";
            sSQL += "From CUAHANG" + "\n";
            sSQL += "Where MA_CUAHANG  =" + clsMain.SQLString(sMacuahang) + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            lb_Header.Text = dt.Rows[0]["Header"].ToString();
            lbFooter.Text = dt.Rows[0]["Footer"].ToString();       
        }      
    }
}
