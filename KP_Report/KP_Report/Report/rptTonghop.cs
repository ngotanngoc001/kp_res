﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptTonghop : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptTonghop()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
       
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            TUNGAY.DataBindings.Add("Text", DataSource, "TUNGAY", "{0:dd/MM/yyyy}");
            DENNGAY.DataBindings.Add("Text", DataSource, "DENNGAY", "{0:dd/MM/yyyy}");
            TENTUYEN1.DataBindings.Add("Text", DataSource, "TENTUYEN1");
            TENDOITUONG1.DataBindings.Add("Text", DataSource, "TENDOITUONG1");
            TENKHUVUC1.DataBindings.Add("Text", DataSource, "TENKHUVUC1");
            NHANVIEN1.DataBindings.Add("Text", DataSource, "NHANVIEN1");
            VEBAN1.DataBindings.Add("Text", DataSource, "VEBAN1");
            TIENBAN1.DataBindings.Add("Text", DataSource, "TIENBAN1","{0:n0}");
            VEHUY1.DataBindings.Add("Text", DataSource, "VEHUY1");
            TIENHUY1.DataBindings.Add("Text", DataSource, "TIENHUY1", "{0:n0}");
            PHUTHU1.DataBindings.Add("Text", DataSource, "PHUTHU1", "{0:n0}");
            VEBAN2.DataBindings.Add("Text", DataSource, "VEBAN2");
            TIENBAN2.DataBindings.Add("Text", DataSource, "TIENBAN2", "{0:n0}");
            VEHUY2.DataBindings.Add("Text", DataSource, "VEHUY2");
            TIENHUY2.DataBindings.Add("Text", DataSource, "TIENHUY2", "{0:n0}");
            PHUTHU2.DataBindings.Add("Text", DataSource, "PHUTHU2", "{0:n0}");
            GIAMGIA.DataBindings.Add("Text", DataSource, "GIAMGIA", "{0:n0}");
            DataTable dt = (DataTable)DataSource;
            try
            {
                int iSODUCUOI1 = int.Parse(dt.Rows[0]["TIENBAN1"].ToString()) + int.Parse(dt.Rows[0]["PHUTHU1"].ToString()) - int.Parse(dt.Rows[0]["TIENHUY1"].ToString());
                int iSODUCUOI2 = int.Parse(dt.Rows[0]["TIENBAN2"].ToString()) + int.Parse(dt.Rows[0]["PHUTHU2"].ToString()) - int.Parse(dt.Rows[0]["TIENHUY2"].ToString());
                int iGIAMGIA = int.Parse(dt.Rows[0]["GIAMGIA"].ToString());
                SODUCUOI1.Text = iSODUCUOI1.ToString("n0");
                SODUCUOI2.Text = iSODUCUOI2.ToString("n0");
                TONG.Text = (iSODUCUOI1 + iSODUCUOI2 - iGIAMGIA).ToString("n0");
            }
            catch
            {
                SODUCUOI1.Text = "0";
                SODUCUOI2.Text = "0";
                TONG.Text = "0";
            }
        }
    }
}
