﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptVeKhuvuichoi : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptVeKhuvuichoi()
        {
            InitializeComponent();
            Setheader();

        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            //logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Đ/c:" + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Đt:" + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax:" + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            MAPHIEU.DataBindings.Add("Text", DataSource, "MA_VEBAN", "*{0}*");
            TENKhuvuichoi.DataBindings.Add("Text", DataSource, "TEN_TROCHOI");
            NGAYTAO.DataBindings.Add("Text", DataSource, "NGAYBAN", "{0:HH : mm}");
            DONGIA.DataBindings.Add("Text", DataSource, "GIABAN", "{0:#,###0}");
            NGUOITAO.DataBindings.Add("Text", DataSource, "TENNHANVIEN");
            NGAYTAO1.DataBindings.Add("Text", DataSource, "NGAYBAN", "{0:dd/MM/yyyy HH : mm}");
        }
       
    }
}
