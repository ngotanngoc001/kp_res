﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using System.Drawing.Imaging;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBaoCao_CongVanDen : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBaoCao_CongVanDen()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            SO_KYHIEU.DataBindings.Add("Text", DataSource, "SO_KYHIEU");
            NGAYBANHANH.DataBindings.Add("Text", DataSource, "NGAYBANHANH","{0:dd/MM/yyyy}");
            CQBH.DataBindings.Add("Text", DataSource, "CQBH");
            LOAIVB.DataBindings.Add("Text", DataSource, "LOAIVB");
            NOIDUNG.DataBindings.Add("Text", DataSource, "NOIDUNG");
            NGAYDEN.DataBindings.Add("Text", DataSource, "NGAYDEN", "{0:dd/MM/yyyy}");
            SOVB.DataBindings.Add("Text", DataSource, "SOVB");
            NHANVIENXULY.DataBindings.Add("Text", DataSource, "NHANVIENXULY");
            TRANGTHAI.DataBindings.Add("Text", DataSource, "TRANGTHAI");
            DIENGIAI.DataBindings.Add("Text", DataSource, "DIENGIAI");
            
            DataTable dtSource = (DataTable)DataSource;
            txtTitle.Text = dtSource.Rows[0]["HEARDER"].ToString();
        }

        int i = 0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
