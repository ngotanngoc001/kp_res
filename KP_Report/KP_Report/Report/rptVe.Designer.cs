﻿namespace KP_Report
{
    partial class rptVe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.NGUOITAO1 = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYTAO1 = new DevExpress.XtraReports.UI.XRLabel();
            this.txtTencongty1 = new DevExpress.XtraReports.UI.XRLabel();
            this.txtTencongty = new DevExpress.XtraReports.UI.XRLabel();
            this.TENCHATLUONGPHIM1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TENCHATLUONGPHIM = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYTAO = new DevExpress.XtraReports.UI.XRLabel();
            this.NGUOITAO = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYDI1 = new DevExpress.XtraReports.UI.XRLabel();
            this.GIODI1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.SOGHE1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.TENTUYEN1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.GIODI = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYDI = new DevExpress.XtraReports.UI.XRLabel();
            this.SOGHE = new DevExpress.XtraReports.UI.XRLabel();
            this.TENTUYEN = new DevExpress.XtraReports.UI.XRLabel();
            this.MABARCODE1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.MABARCODE = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.NGUOITAO1,
            this.NGAYTAO1,
            this.txtTencongty1,
            this.txtTencongty,
            this.TENCHATLUONGPHIM1,
            this.TENCHATLUONGPHIM,
            this.xrLabel43,
            this.NGAYTAO,
            this.NGUOITAO,
            this.NGAYDI1,
            this.GIODI1,
            this.xrLabel38,
            this.xrLabel37,
            this.xrLabel36,
            this.xrLabel23,
            this.xrLabel21,
            this.SOGHE1,
            this.xrLabel25,
            this.TENTUYEN1,
            this.xrLabel28,
            this.xrLabel30,
            this.xrLabel26,
            this.GIODI,
            this.NGAYDI,
            this.SOGHE,
            this.TENTUYEN,
            this.MABARCODE1,
            this.xrLabel31,
            this.xrLabel15,
            this.MABARCODE,
            this.xrLabel7});
            this.Detail.Dpi = 254F;
            this.Detail.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Detail.HeightF = 630F;
            this.Detail.LockedInUserDesigner = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.StylePriority.UseTextAlignment = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // NGUOITAO1
            // 
            this.NGUOITAO1.CanGrow = false;
            this.NGUOITAO1.Dpi = 254F;
            this.NGUOITAO1.Font = new System.Drawing.Font("Arial", 8F);
            this.NGUOITAO1.LocationFloat = new DevExpress.Utils.PointFloat(54.4F, 498.3F);
            this.NGUOITAO1.Name = "NGUOITAO1";
            this.NGUOITAO1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGUOITAO1.SizeF = new System.Drawing.SizeF(225.5832F, 44.13269F);
            this.NGUOITAO1.StylePriority.UseFont = false;
            this.NGUOITAO1.StylePriority.UsePadding = false;
            this.NGUOITAO1.StylePriority.UseTextAlignment = false;
            this.NGUOITAO1.Text = "Phan Văn Thông gfgfg dgd";
            this.NGUOITAO1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // NGAYTAO1
            // 
            this.NGAYTAO1.CanGrow = false;
            this.NGAYTAO1.Dpi = 254F;
            this.NGAYTAO1.Font = new System.Drawing.Font("Arial", 8F);
            this.NGAYTAO1.LocationFloat = new DevExpress.Utils.PointFloat(256.1707F, 498.3F);
            this.NGAYTAO1.Name = "NGAYTAO1";
            this.NGAYTAO1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYTAO1.SizeF = new System.Drawing.SizeF(245.892F, 44.13269F);
            this.NGAYTAO1.StylePriority.UseFont = false;
            this.NGAYTAO1.StylePriority.UsePadding = false;
            this.NGAYTAO1.StylePriority.UseTextAlignment = false;
            this.NGAYTAO1.Text = "21/12/2012 15 : 30";
            this.NGAYTAO1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // txtTencongty1
            // 
            this.txtTencongty1.Dpi = 254F;
            this.txtTencongty1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.txtTencongty1.LocationFloat = new DevExpress.Utils.PointFloat(54.4F, 56.75F);
            this.txtTencongty1.Multiline = true;
            this.txtTencongty1.Name = "txtTencongty1";
            this.txtTencongty1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.txtTencongty1.SizeF = new System.Drawing.SizeF(450.2542F, 104.9867F);
            this.txtTencongty1.StylePriority.UseFont = false;
            this.txtTencongty1.StylePriority.UsePadding = false;
            this.txtTencongty1.StylePriority.UseTextAlignment = false;
            this.txtTencongty1.Text = "CT TNHH MTV DLST QUỐC TẾ\r\nĐC: H.Cái Nước,Tỉnh Cà Mau\r\nĐT: (07803) 881 999\r\nFax: (" +
                "07803) 881 999";
            this.txtTencongty1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // txtTencongty
            // 
            this.txtTencongty.Dpi = 254F;
            this.txtTencongty.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.txtTencongty.LocationFloat = new DevExpress.Utils.PointFloat(553.27F, 56.75001F);
            this.txtTencongty.Multiline = true;
            this.txtTencongty.Name = "txtTencongty";
            this.txtTencongty.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.txtTencongty.SizeF = new System.Drawing.SizeF(915.837F, 104.9867F);
            this.txtTencongty.StylePriority.UseFont = false;
            this.txtTencongty.StylePriority.UsePadding = false;
            this.txtTencongty.StylePriority.UseTextAlignment = false;
            this.txtTencongty.Text = "Impire Cineplex";
            this.txtTencongty.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TENCHATLUONGPHIM1
            // 
            this.TENCHATLUONGPHIM1.CanGrow = false;
            this.TENCHATLUONGPHIM1.Dpi = 254F;
            this.TENCHATLUONGPHIM1.Font = new System.Drawing.Font("Arial", 9F);
            this.TENCHATLUONGPHIM1.LocationFloat = new DevExpress.Utils.PointFloat(54.4F, 172.83F);
            this.TENCHATLUONGPHIM1.Name = "TENCHATLUONGPHIM1";
            this.TENCHATLUONGPHIM1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENCHATLUONGPHIM1.SizeF = new System.Drawing.SizeF(34.39583F, 38F);
            this.TENCHATLUONGPHIM1.StylePriority.UseFont = false;
            this.TENCHATLUONGPHIM1.StylePriority.UsePadding = false;
            this.TENCHATLUONGPHIM1.StylePriority.UseTextAlignment = false;
            this.TENCHATLUONGPHIM1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TENCHATLUONGPHIM
            // 
            this.TENCHATLUONGPHIM.CanGrow = false;
            this.TENCHATLUONGPHIM.Dpi = 254F;
            this.TENCHATLUONGPHIM.Font = new System.Drawing.Font("Arial", 9F);
            this.TENCHATLUONGPHIM.LocationFloat = new DevExpress.Utils.PointFloat(553.27F, 172.83F);
            this.TENCHATLUONGPHIM.Name = "TENCHATLUONGPHIM";
            this.TENCHATLUONGPHIM.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENCHATLUONGPHIM.SizeF = new System.Drawing.SizeF(442.8536F, 37.99995F);
            this.TENCHATLUONGPHIM.StylePriority.UseFont = false;
            this.TENCHATLUONGPHIM.StylePriority.UsePadding = false;
            this.TENCHATLUONGPHIM.StylePriority.UseTextAlignment = false;
            this.TENCHATLUONGPHIM.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel43
            // 
            this.xrLabel43.CanGrow = false;
            this.xrLabel43.Dpi = 254F;
            this.xrLabel43.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(1192.69F, 498.3F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(20F, 44.13269F);
            this.xrLabel43.StylePriority.UseFont = false;
            this.xrLabel43.StylePriority.UsePadding = false;
            this.xrLabel43.StylePriority.UseTextAlignment = false;
            this.xrLabel43.Text = "-";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // NGAYTAO
            // 
            this.NGAYTAO.CanGrow = false;
            this.NGAYTAO.Dpi = 254F;
            this.NGAYTAO.Font = new System.Drawing.Font("Arial", 8F);
            this.NGAYTAO.LocationFloat = new DevExpress.Utils.PointFloat(1212.69F, 498.3F);
            this.NGAYTAO.Name = "NGAYTAO";
            this.NGAYTAO.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYTAO.SizeF = new System.Drawing.SizeF(256.4169F, 44.13272F);
            this.NGAYTAO.StylePriority.UseFont = false;
            this.NGAYTAO.StylePriority.UsePadding = false;
            this.NGAYTAO.StylePriority.UseTextAlignment = false;
            this.NGAYTAO.Text = "21/12/2012 15 : 30";
            this.NGAYTAO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // NGUOITAO
            // 
            this.NGUOITAO.CanGrow = false;
            this.NGUOITAO.Dpi = 254F;
            this.NGUOITAO.Font = new System.Drawing.Font("Arial", 8F);
            this.NGUOITAO.LocationFloat = new DevExpress.Utils.PointFloat(553.27F, 498.3F);
            this.NGUOITAO.Name = "NGUOITAO";
            this.NGUOITAO.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGUOITAO.SizeF = new System.Drawing.SizeF(639.42F, 44.13278F);
            this.NGUOITAO.StylePriority.UseFont = false;
            this.NGUOITAO.StylePriority.UsePadding = false;
            this.NGUOITAO.StylePriority.UseTextAlignment = false;
            this.NGUOITAO.Text = "Phan Văn Thông";
            this.NGUOITAO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // NGAYDI1
            // 
            this.NGAYDI1.CanGrow = false;
            this.NGAYDI1.Dpi = 254F;
            this.NGAYDI1.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.NGAYDI1.LocationFloat = new DevExpress.Utils.PointFloat(553.27F, 394.0325F);
            this.NGAYDI1.Name = "NGAYDI1";
            this.NGAYDI1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYDI1.SizeF = new System.Drawing.SizeF(268.4791F, 55F);
            this.NGAYDI1.StylePriority.UseFont = false;
            this.NGAYDI1.StylePriority.UsePadding = false;
            this.NGAYDI1.StylePriority.UseTextAlignment = false;
            this.NGAYDI1.Text = "21/12/2012";
            this.NGAYDI1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GIODI1
            // 
            this.GIODI1.CanGrow = false;
            this.GIODI1.Dpi = 254F;
            this.GIODI1.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.GIODI1.LocationFloat = new DevExpress.Utils.PointFloat(915.4373F, 394.0325F);
            this.GIODI1.Name = "GIODI1";
            this.GIODI1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.GIODI1.SizeF = new System.Drawing.SizeF(179.9791F, 55F);
            this.GIODI1.StylePriority.UseFont = false;
            this.GIODI1.StylePriority.UsePadding = false;
            this.GIODI1.StylePriority.UseTextAlignment = false;
            this.GIODI1.Text = "20 : 30";
            this.GIODI1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel38
            // 
            this.xrLabel38.CanGrow = false;
            this.xrLabel38.Dpi = 254F;
            this.xrLabel38.Font = new System.Drawing.Font("Arial", 9F);
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(553.27F, 449.03F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(268.4791F, 44.13254F);
            this.xrLabel38.StylePriority.UseFont = false;
            this.xrLabel38.StylePriority.UsePadding = false;
            this.xrLabel38.StylePriority.UseTextAlignment = false;
            this.xrLabel38.Text = "Date";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel37
            // 
            this.xrLabel37.CanGrow = false;
            this.xrLabel37.Dpi = 254F;
            this.xrLabel37.Font = new System.Drawing.Font("Arial", 9F);
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(915.4373F, 449.0325F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(179.9791F, 44.13251F);
            this.xrLabel37.StylePriority.UseFont = false;
            this.xrLabel37.StylePriority.UsePadding = false;
            this.xrLabel37.StylePriority.UseTextAlignment = false;
            this.xrLabel37.Text = "Time";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel36
            // 
            this.xrLabel36.CanGrow = false;
            this.xrLabel36.Dpi = 254F;
            this.xrLabel36.Font = new System.Drawing.Font("Arial", 9F);
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(1240.94F, 449.03F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(228.1671F, 44.13254F);
            this.xrLabel36.StylePriority.UseFont = false;
            this.xrLabel36.StylePriority.UsePadding = false;
            this.xrLabel36.StylePriority.UseTextAlignment = false;
            this.xrLabel36.Text = "Adult";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel23
            // 
            this.xrLabel23.CanGrow = false;
            this.xrLabel23.Dpi = 254F;
            this.xrLabel23.Font = new System.Drawing.Font("Arial", 9F);
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(915.4373F, 349.9F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(179.9791F, 44.13251F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UsePadding = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "Giờ";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel21
            // 
            this.xrLabel21.CanGrow = false;
            this.xrLabel21.Dpi = 254F;
            this.xrLabel21.Font = new System.Drawing.Font("Arial", 9F);
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(1240.94F, 349.9F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(228.1671F, 44.13254F);
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UsePadding = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "Giá vé";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // SOGHE1
            // 
            this.SOGHE1.CanGrow = false;
            this.SOGHE1.Dpi = 254F;
            this.SOGHE1.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.SOGHE1.LocationFloat = new DevExpress.Utils.PointFloat(1240.94F, 394.03F);
            this.SOGHE1.Name = "SOGHE1";
            this.SOGHE1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.SOGHE1.SizeF = new System.Drawing.SizeF(228.1671F, 55.00003F);
            this.SOGHE1.StylePriority.UseFont = false;
            this.SOGHE1.StylePriority.UsePadding = false;
            this.SOGHE1.StylePriority.UseTextAlignment = false;
            this.SOGHE1.Text = "500,000";
            this.SOGHE1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel25
            // 
            this.xrLabel25.CanGrow = false;
            this.xrLabel25.Dpi = 254F;
            this.xrLabel25.Font = new System.Drawing.Font("Arial", 9F);
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(553.27F, 349.9001F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(268.4791F, 44.13248F);
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UsePadding = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "Ngày";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TENTUYEN1
            // 
            this.TENTUYEN1.Dpi = 254F;
            this.TENTUYEN1.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.TENTUYEN1.LocationFloat = new DevExpress.Utils.PointFloat(553.27F, 224.9F);
            this.TENTUYEN1.Name = "TENTUYEN1";
            this.TENTUYEN1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENTUYEN1.SizeF = new System.Drawing.SizeF(915.837F, 125F);
            this.TENTUYEN1.StylePriority.UseFont = false;
            this.TENTUYEN1.StylePriority.UsePadding = false;
            this.TENTUYEN1.StylePriority.UseTextAlignment = false;
            this.TENTUYEN1.Text = "Thor: The Dark World";
            this.TENTUYEN1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel28
            // 
            this.xrLabel28.CanGrow = false;
            this.xrLabel28.Dpi = 254F;
            this.xrLabel28.Font = new System.Drawing.Font("Arial", 9F);
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(237.6498F, 449.03F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(120F, 44.13251F);
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.StylePriority.UsePadding = false;
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = "Time";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel30
            // 
            this.xrLabel30.CanGrow = false;
            this.xrLabel30.Dpi = 254F;
            this.xrLabel30.Font = new System.Drawing.Font("Arial", 9F);
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(54.4F, 449.03F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(183.2498F, 44.13254F);
            this.xrLabel30.StylePriority.UseFont = false;
            this.xrLabel30.StylePriority.UsePadding = false;
            this.xrLabel30.StylePriority.UseTextAlignment = false;
            this.xrLabel30.Text = "Date";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel26
            // 
            this.xrLabel26.CanGrow = false;
            this.xrLabel26.Dpi = 254F;
            this.xrLabel26.Font = new System.Drawing.Font("Arial", 9F);
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(362.9372F, 449.03F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(139.1253F, 44.13254F);
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UsePadding = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "Adult";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GIODI
            // 
            this.GIODI.CanGrow = false;
            this.GIODI.Dpi = 254F;
            this.GIODI.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.GIODI.LocationFloat = new DevExpress.Utils.PointFloat(237.6498F, 404.8974F);
            this.GIODI.Name = "GIODI";
            this.GIODI.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.GIODI.SizeF = new System.Drawing.SizeF(120F, 44.13254F);
            this.GIODI.StylePriority.UseFont = false;
            this.GIODI.StylePriority.UsePadding = false;
            this.GIODI.StylePriority.UseTextAlignment = false;
            this.GIODI.Text = "20 : 30";
            this.GIODI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // NGAYDI
            // 
            this.NGAYDI.CanGrow = false;
            this.NGAYDI.Dpi = 254F;
            this.NGAYDI.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.NGAYDI.LocationFloat = new DevExpress.Utils.PointFloat(54.4F, 404.8974F);
            this.NGAYDI.Name = "NGAYDI";
            this.NGAYDI.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYDI.SizeF = new System.Drawing.SizeF(196.479F, 44.13254F);
            this.NGAYDI.StylePriority.UseFont = false;
            this.NGAYDI.StylePriority.UsePadding = false;
            this.NGAYDI.StylePriority.UseTextAlignment = false;
            this.NGAYDI.Text = "21/12/2012";
            this.NGAYDI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // SOGHE
            // 
            this.SOGHE.CanGrow = false;
            this.SOGHE.Dpi = 254F;
            this.SOGHE.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.SOGHE.LocationFloat = new DevExpress.Utils.PointFloat(362.9372F, 404.8975F);
            this.SOGHE.Name = "SOGHE";
            this.SOGHE.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.SOGHE.SizeF = new System.Drawing.SizeF(139.1253F, 44.13254F);
            this.SOGHE.StylePriority.UseFont = false;
            this.SOGHE.StylePriority.UsePadding = false;
            this.SOGHE.StylePriority.UseTextAlignment = false;
            this.SOGHE.Text = "500,000";
            this.SOGHE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TENTUYEN
            // 
            this.TENTUYEN.Dpi = 254F;
            this.TENTUYEN.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.TENTUYEN.LocationFloat = new DevExpress.Utils.PointFloat(54.4F, 224.9F);
            this.TENTUYEN.Name = "TENTUYEN";
            this.TENTUYEN.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENTUYEN.SizeF = new System.Drawing.SizeF(450.25F, 135.8649F);
            this.TENTUYEN.StylePriority.UseFont = false;
            this.TENTUYEN.StylePriority.UsePadding = false;
            this.TENTUYEN.StylePriority.UseTextAlignment = false;
            this.TENTUYEN.Text = "Thor: The Dark World";
            this.TENTUYEN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // MABARCODE1
            // 
            this.MABARCODE1.CanGrow = false;
            this.MABARCODE1.Dpi = 254F;
            this.MABARCODE1.Font = new System.Drawing.Font("Arial", 9F);
            this.MABARCODE1.LocationFloat = new DevExpress.Utils.PointFloat(1060.649F, 172.83F);
            this.MABARCODE1.Name = "MABARCODE1";
            this.MABARCODE1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.MABARCODE1.SizeF = new System.Drawing.SizeF(408.4578F, 37.99997F);
            this.MABARCODE1.StylePriority.UseFont = false;
            this.MABARCODE1.StylePriority.UsePadding = false;
            this.MABARCODE1.StylePriority.UseTextAlignment = false;
            this.MABARCODE1.Text = "2543648";
            this.MABARCODE1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel31
            // 
            this.xrLabel31.CanGrow = false;
            this.xrLabel31.Dpi = 254F;
            this.xrLabel31.Font = new System.Drawing.Font("Arial", 9F);
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(362.9372F, 360.7649F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(139.1253F, 44.13254F);
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.StylePriority.UsePadding = false;
            this.xrLabel31.StylePriority.UseTextAlignment = false;
            this.xrLabel31.Text = "Giá vé";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.CanGrow = false;
            this.xrLabel15.Dpi = 254F;
            this.xrLabel15.Font = new System.Drawing.Font("Arial", 9F);
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(54.4F, 360.7649F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(183.2498F, 44.13248F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UsePadding = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "Ngày";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // MABARCODE
            // 
            this.MABARCODE.CanGrow = false;
            this.MABARCODE.Dpi = 254F;
            this.MABARCODE.Font = new System.Drawing.Font("Arial", 9F);
            this.MABARCODE.LocationFloat = new DevExpress.Utils.PointFloat(88.79581F, 172.83F);
            this.MABARCODE.Name = "MABARCODE";
            this.MABARCODE.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.MABARCODE.SizeF = new System.Drawing.SizeF(413.2667F, 37.99998F);
            this.MABARCODE.StylePriority.UseFont = false;
            this.MABARCODE.StylePriority.UsePadding = false;
            this.MABARCODE.StylePriority.UseTextAlignment = false;
            this.MABARCODE.Text = "2543648";
            this.MABARCODE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel7
            // 
            this.xrLabel7.CanGrow = false;
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.Font = new System.Drawing.Font("Arial", 9F);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(237.6498F, 360.7649F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(120F, 44.13251F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UsePadding = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Giờ";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 5F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 10.00004F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // formattingRule1
            // 
            this.formattingRule1.Name = "formattingRule1";
            // 
            // rptVe
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Dpi = 254F;
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(5, 5, 5, 10);
            this.PageHeight = 630;
            this.PageWidth = 1550;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.ShowPrintMarginsWarning = false;
            this.ShowPrintStatusDialog = false;
            this.SnapGridSize = 5F;
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel MABARCODE;
        private DevExpress.XtraReports.UI.XRLabel MABARCODE1;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        private DevExpress.XtraReports.UI.XRLabel TENTUYEN;
        private DevExpress.XtraReports.UI.XRLabel NGAYDI1;
        private DevExpress.XtraReports.UI.XRLabel GIODI1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel TENTUYEN1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel GIODI;
        private DevExpress.XtraReports.UI.XRLabel NGAYDI;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel NGUOITAO;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel NGAYTAO;
        private DevExpress.XtraReports.UI.XRLabel TENCHATLUONGPHIM1;
        private DevExpress.XtraReports.UI.XRLabel TENCHATLUONGPHIM;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel SOGHE1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel SOGHE;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel txtTencongty1;
        private DevExpress.XtraReports.UI.XRLabel txtTencongty;
        private DevExpress.XtraReports.UI.XRLabel NGUOITAO1;
        private DevExpress.XtraReports.UI.XRLabel NGAYTAO1;
    }
}
