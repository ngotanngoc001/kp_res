﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBCCongNo : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBCCongNo()
        {
            InitializeComponent();
            Setheader();
            GroupField grf = new GroupField("NHAXE");
            GroupHeader3.GroupFields.Add(grf);
            //GroupField grf1 = new GroupField("MATUYEN");
            //GroupHeader2.GroupFields.Add(grf1);
            //GroupField grf2 = new GroupField("TENTUYEN");
            //GroupHeader1.GroupFields.Add(grf2);
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        //int i=0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //i++;
            //stt.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            nhaxe1.DataBindings.Add("Text", DataSource, "nhaxe1");
            NGAY1.DataBindings.Add("Text", DataSource, "ngay1");
            TUYEN1.DataBindings.Add("Text", DataSource, "tuyen1");

            TENNHAXE.DataBindings.Add("Text", DataSource, "TENNHAXE");
            MABARCODE.DataBindings.Add("Text", DataSource, "MABARCODE");
            TENTUYEN.DataBindings.Add("Text", DataSource, "TENTUYEN");
            NGAYDI.DataBindings.Add("Text", DataSource, "NGAYDI", "{0:dd/MM/yyyy}");
            GIODI.DataBindings.Add("Text", DataSource, "GIODI");
            LOAIXE.DataBindings.Add("Text", DataSource, "LOAIXE");
            //THANHTIEN.DataBindings.Add("Text", DataSource, "THANHTIEN", "{0:n0}");
            DATHANHTOAN.DataBindings.Add("Text", DataSource, "DATHANHTOAN", "{0:n0}");
           // DATHANHTOAN.DataBindings.Add("Text", DataSource, "TIENTHANHTOAN", "{0:n0}");
            TIENTHANHTOAN.DataBindings.Add("Text", DataSource, "TONGTIENTHUCDUNG", "{0:n0}");
            NOIDUNG.DataBindings.Add("Text", DataSource, "NOIDUNG");

            NOIDUNG1.DataBindings.Add("Text", DataSource, "NOIDUNG1");
            TON1.DataBindings.Add("Text", DataSource, "TIENCONLAI", "{0:n0}");
            TON.DataBindings.Add("Text", DataSource, "TON", "{0:n0}");

            //TONGTIEN.DataBindings.Add("Text", DataSource, "THANHTIEN");
            //TONGTIEN.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
            TONGDATHANHTOAN.DataBindings.Add("Text", DataSource, "DATHANHTOAN");
            //TONGDATHANHTOAN.DataBindings.Add("Text", DataSource, "TIENTHANHTOAN");
            TONGDATHANHTOAN.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
            TONGTIENTHANHTOAN.DataBindings.Add("Text", DataSource, "TONGTIENTHUCDUNG");
            TONGTIENTHANHTOAN.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");

            CONGNOTUYEN.DataBindings.Add("Text", DataSource, "DATHANHTOAN");
            //CONGNOTUYEN.DataBindings.Add("Text", DataSource, "TIENTHANHTOAN");
            CONGNOTUYEN.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            CONGNOTUYEN_TRA.DataBindings.Add("Text", DataSource, "TONGTIENTHUCDUNG");
            CONGNOTUYEN_TRA.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");

            CONGNONHAXE.DataBindings.Add("Text", DataSource, "DATHANHTOAN"); 
            //CONGNONHAXE.DataBindings.Add("Text", DataSource, "TIENTHANHTOAN");
            CONGNONHAXE.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            CONGNONHAXE_TRA.DataBindings.Add("Text", DataSource, "TIENTHANHTOAN");
            CONGNONHAXE_TRA.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");

            TENNHAXE_CONGNO.DataBindings.Add("Text", DataSource, "TENNHAXE1");

            TON3.DataBindings.Add("Text", DataSource, "TON");
            TON3.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
        }
    }
}
