﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptThongkehuychoxe : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptThongkehuychoxe()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }

        int i;
        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i = 0;
        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            TENTUYEN.DataBindings.Add("Text", DataSource, "TENTUYEN1");
            TENKHUVUC.DataBindings.Add("Text", DataSource, "TENKHUVUC1");
            NHANVIEN.DataBindings.Add("Text", DataSource, "NHANVIEN");
            TENDOITUONG.DataBindings.Add("Text", DataSource, "TENDOITUONG1");
            MABARCODE.DataBindings.Add("Text", DataSource, "SOGHE");
            TENHANHKHACH.DataBindings.Add("Text", DataSource, "TENHANHKHACH");
            LOAIXE.DataBindings.Add("Text", DataSource, "TENKHUVUC");
            BIENSO.DataBindings.Add("Text", DataSource, "BIENSO");
         //   NOISINH.DataBindings.Add("Text", DataSource, "NOISINH");
            NHANVIEN1.DataBindings.Add("Text", DataSource, "DIACHI");
            THOIGIAN.DataBindings.Add("Text", DataSource, "DIENTHOAI");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:n0}");
           // NHOMGHE.DataBindings.Add("Text", DataSource, "NHOMGHE");
            TONGSOGHE1.DataBindings.Add("Text", DataSource, "MABARCODE");
            TONGSOGHE1.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Count, "{0:n0}");
            //TONGTHANHTHIEN.DataBindings.Add("Text", DataSource, "DONGIA");
            //TONGTHANHTHIEN.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            TUNGAY.DataBindings.Add("Text", DataSource, "TUNGAY", "{0:dd/MM/yyyy}");
            DENNGAY.DataBindings.Add("Text", DataSource, "DENNGAY", "{0:dd/MM/yyyy}");
           
            MABARCODE.DataBindings.Add("Text", DataSource, "MABARCODE");
            TENTUYEN1.DataBindings.Add("Text", DataSource, "TENTUYEN");
            TENDOITUONG1.DataBindings.Add("Text", DataSource, "TENDOITUONG");
            NGAYDI1.DataBindings.Add("Text", DataSource, "NGAYDI", "{0:dd/MM/yyyy}");
            GIODI1.DataBindings.Add("Text", DataSource, "GIODI");
            SOGHE.DataBindings.Add("Text", DataSource, "SOGHE");
            NHANVIEN1.DataBindings.Add("Text", DataSource, "NGUOITAO1");
            THOIGIAN.DataBindings.Add("Text", DataSource, "NGAYTAO1");
            TONGTIEN.DataBindings.Add("Text", DataSource, "DONGIA");
            TONGTIEN.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
            TONGTHANHTHIEN.DataBindings.Add("Text", DataSource, "DONGIA");
            TONGTHANHTHIEN.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            TONGPHUTHU.DataBindings.Add("Text", DataSource, "PHUTHU");
            TONGPHUTHU.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            PHUTHU.DataBindings.Add("Text", DataSource, "PHUTHU", "{0:n0}");
            LYDO.DataBindings.Add("Text", DataSource, "LYDO");
            TONGSOGHE.DataBindings.Add("Text", DataSource, "MABARCODE");
            TONGSOGHE.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Count, "{0:n0}");
            TONGPHUTHU1.DataBindings.Add("Text", DataSource, "PHUTHU");
            TONGPHUTHU1.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
            NGUOIHUY.DataBindings.Add("Text", DataSource, "NGUOITAO");
            GIOHUY.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy HH:mm}");
        }
    }
}
