﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBaoCaoNhanVien : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBaoCaoNhanVien()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            MANHANVIEN.DataBindings.Add("Text", DataSource, "MANHANVIEN");
            TENNHANVIEN.DataBindings.Add("Text", DataSource, "TENNHANVIEN");
            NGAYSINH.DataBindings.Add("Text", DataSource, "NGAYSINH","{0:dd/MM/yyyy}");
            GIOITINH.DataBindings.Add("Text", DataSource, "GIOITINH");
            HOCVAN.DataBindings.Add("Text", DataSource, "HOCVAN");
            NGOAINGU.DataBindings.Add("Text", DataSource, "NGOAINGU");
            TINHOC.DataBindings.Add("Text", DataSource, "TINHOC");
            KINHNGHIEM.DataBindings.Add("Text", DataSource, "KINHNGHIEM");
            PHONGBAN.DataBindings.Add("Text", DataSource, "PHONGBAN");
            TENCHUCVU.DataBindings.Add("Text", DataSource, "TENCHUCVU");
            TRANGTHAI.DataBindings.Add("Text", DataSource, "TRANGTHAI");
            
            DataTable dtSource = (DataTable)DataSource;
            if (dtSource.Rows[0]["HEARDER"].ToString() != "")
            {
                txt_tieude.Text = dtSource.Rows[0]["HEARDER"].ToString();
            }
        }

        int i = 0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
