﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptPhieuKiemKe : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptPhieuKiemKe()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX " + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            MA_KHO.DataBindings.Add("Text", DataSource, "MA_KHO");
            TEN_KHO.DataBindings.Add("Text", DataSource, "TEN_KHO");
            MA_HANGHOA.DataBindings.Add("Text", DataSource, "MA_HANGHOA");
            TEN_HANGHOA.DataBindings.Add("Text", DataSource, "TEN_HANGHOA");
            SOLUONG.DataBindings.Add("Text", DataSource, "SOLUONG", "{0:#,###0.00}");
            TEN_DONVITINH.DataBindings.Add("Text", DataSource, "TEN_DONVITINH", "{0:#,###0.00}");
            txtNhanvien.DataBindings.Add("Text", DataSource, "TENNHANVIEN");
            txt_sophieu.DataBindings.Add("Text", DataSource, "SOPHIEU");
            txtGHICHU.DataBindings.Add("Text", DataSource, "GHICHU");
            txt_ngaynhap.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy}");
        }
        int i = 0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
    }
}
