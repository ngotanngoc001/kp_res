﻿namespace KP_Report
{
    partial class rptPhieuCanXe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txtDienthoai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MAPHIEU = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txtTencongty = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txtDiachi = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TENDOITUONG11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TENTUYEN = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GIODI = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.LOAIXE = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.SOXE = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TL_THIETKE = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TL_BANTHAN = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TL_XEHANG = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CD_COSO11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TL_THUCTE = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CD_COSO = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TL_HANGHOA = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CD_THUCTE = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TL_QUATAI = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CD_QUAKHO = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TT_QUATAI = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TT_QUAKHO = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TT_PHUTHU = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.DATEDAY = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.Expanded = false;
            this.Detail.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Detail.HeightF = 61.70085F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 98.62499F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 51F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable10
            // 
            this.xrTable10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable10.Dpi = 254F;
            this.xrTable10.Font = new System.Drawing.Font("Times New Roman", 16F);
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(68.52F, 123.4008F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(1971.896F, 61.70081F);
            this.xrTable10.StylePriority.UseBorders = false;
            this.xrTable10.StylePriority.UseFont = false;
            this.xrTable10.StylePriority.UseTextAlignment = false;
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txtDienthoai,
            this.xrTableCell13,
            this.MAPHIEU});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // txtDienthoai
            // 
            this.txtDienthoai.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtDienthoai.Dpi = 254F;
            this.txtDienthoai.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.txtDienthoai.Multiline = true;
            this.txtDienthoai.Name = "txtDienthoai";
            this.txtDienthoai.StylePriority.UseBorders = false;
            this.txtDienthoai.StylePriority.UseFont = false;
            this.txtDienthoai.StylePriority.UseTextAlignment = false;
            this.txtDienthoai.Text = "Điện thoại : 077.3957239 - Fax : 077.3957238";
            this.txtDienthoai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txtDienthoai.Weight = 3.5156164062636508D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "Số : ";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell13.Weight = 0.578895830732381D;
            // 
            // MAPHIEU
            // 
            this.MAPHIEU.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.MAPHIEU.Dpi = 254F;
            this.MAPHIEU.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.MAPHIEU.Name = "MAPHIEU";
            this.MAPHIEU.StylePriority.UseBorders = false;
            this.MAPHIEU.StylePriority.UseFont = false;
            this.MAPHIEU.StylePriority.UseTextAlignment = false;
            this.MAPHIEU.Text = "201120150001";
            this.MAPHIEU.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.MAPHIEU.Weight = 0.78425135219725894D;
            // 
            // xrTable9
            // 
            this.xrTable9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable9.Dpi = 254F;
            this.xrTable9.Font = new System.Drawing.Font("Times New Roman", 16F);
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(68.52F, 0F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable9.SizeF = new System.Drawing.SizeF(1363.354F, 61.7F);
            this.xrTable9.StylePriority.UseBorders = false;
            this.xrTable9.StylePriority.UseFont = false;
            this.xrTable9.StylePriority.UseTextAlignment = false;
            this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txtTencongty});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // txtTencongty
            // 
            this.txtTencongty.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtTencongty.Dpi = 254F;
            this.txtTencongty.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.txtTencongty.Multiline = true;
            this.txtTencongty.Name = "txtTencongty";
            this.txtTencongty.StylePriority.UseBorders = false;
            this.txtTencongty.StylePriority.UseFont = false;
            this.txtTencongty.StylePriority.UseTextAlignment = false;
            this.txtTencongty.Text = "CÔNG TY TNHH MTV THẠNH THỚI";
            this.txtTencongty.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txtTencongty.Weight = 2.9712627931651934D;
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable8.Dpi = 254F;
            this.xrTable8.Font = new System.Drawing.Font("Times New Roman", 16F);
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(68.52F, 61.69997F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable8.SizeF = new System.Drawing.SizeF(1338.354F, 61.70081F);
            this.xrTable8.StylePriority.UseBorders = false;
            this.xrTable8.StylePriority.UseFont = false;
            this.xrTable8.StylePriority.UseTextAlignment = false;
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txtDiachi});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // txtDiachi
            // 
            this.txtDiachi.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtDiachi.Dpi = 254F;
            this.txtDiachi.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.txtDiachi.Multiline = true;
            this.txtDiachi.Name = "txtDiachi";
            this.txtDiachi.StylePriority.UseBorders = false;
            this.txtDiachi.StylePriority.UseFont = false;
            this.txtDiachi.StylePriority.UseTextAlignment = false;
            this.txtDiachi.Text = "Địa chỉ : Khu phố 1, P.Tô Châu, TX. Hà Tiên, Tỉnh Kiên Giang";
            this.txtDiachi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txtDiachi.Weight = 2.9712627931651934D;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.Dpi = 254F;
            this.xrTable7.Font = new System.Drawing.Font("Times New Roman", 16F);
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(68.52F, 185.1016F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(1971.896F, 125.2017F);
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.StylePriority.UseFont = false;
            this.xrTable7.StylePriority.UseTextAlignment = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.xrTableCell38.Multiline = true;
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.StylePriority.UseBorders = false;
            this.xrTableCell38.StylePriority.UseFont = false;
            this.xrTableCell38.StylePriority.UseTextAlignment = false;
            this.xrTableCell38.Text = "PHIẾU CÂN XE";
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell38.Weight = 2.9712627931651934D;
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable5.Dpi = 254F;
            this.xrTable5.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(100.1841F, 353.1425F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5,
            this.xrTableRow15,
            this.xrTableRow17,
            this.xrTableRow16,
            this.xrTableRow18,
            this.xrTableRow19,
            this.xrTableRow3,
            this.xrTableRow20,
            this.xrTableRow1,
            this.xrTableRow2});
            this.xrTable5.SizeF = new System.Drawing.SizeF(1865.526F, 762.5295F);
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.StylePriority.UseFont = false;
            this.xrTable5.StylePriority.UseTextAlignment = false;
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TENDOITUONG11,
            this.TENTUYEN,
            this.xrTableCell7,
            this.GIODI,
            this.xrTableCell2});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // TENDOITUONG11
            // 
            this.TENDOITUONG11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.TENDOITUONG11.Dpi = 254F;
            this.TENDOITUONG11.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.TENDOITUONG11.Multiline = true;
            this.TENDOITUONG11.Name = "TENDOITUONG11";
            this.TENDOITUONG11.StylePriority.UseBorders = false;
            this.TENDOITUONG11.StylePriority.UseFont = false;
            this.TENDOITUONG11.StylePriority.UseTextAlignment = false;
            this.TENDOITUONG11.Text = "Tuyến : ";
            this.TENDOITUONG11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TENDOITUONG11.Weight = 0.46127092365796096D;
            // 
            // TENTUYEN
            // 
            this.TENTUYEN.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.TENTUYEN.Dpi = 254F;
            this.TENTUYEN.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.TENTUYEN.Name = "TENTUYEN";
            this.TENTUYEN.StylePriority.UseBorders = false;
            this.TENTUYEN.StylePriority.UseFont = false;
            this.TENTUYEN.StylePriority.UseTextAlignment = false;
            this.TENTUYEN.Text = "Hà Tiên-Phú Quốc";
            this.TENTUYEN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TENTUYEN.Weight = 1.1990654153754596D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "Chuyến :";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell7.Weight = 0.47243988537803061D;
            // 
            // GIODI
            // 
            this.GIODI.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.GIODI.Dpi = 254F;
            this.GIODI.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.GIODI.Name = "GIODI";
            this.GIODI.StylePriority.UseBorders = false;
            this.GIODI.StylePriority.UseFont = false;
            this.GIODI.StylePriority.UseTextAlignment = false;
            this.GIODI.Text = "05 : 30";
            this.GIODI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.GIODI.Weight = 0.39337269338358072D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Weight = 0.39337269338358072D;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14,
            this.LOAIXE,
            this.xrTableCell18,
            this.SOXE,
            this.xrTableCell6});
            this.xrTableRow15.Dpi = 254F;
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.Text = "Loại xe :";
            this.xrTableCell14.Weight = 0.46127073261963625D;
            // 
            // LOAIXE
            // 
            this.LOAIXE.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.LOAIXE.Dpi = 254F;
            this.LOAIXE.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.LOAIXE.Name = "LOAIXE";
            this.LOAIXE.StylePriority.UseBorders = false;
            this.LOAIXE.StylePriority.UseFont = false;
            this.LOAIXE.Text = "7900";
            this.LOAIXE.Weight = 1.1990655677264459D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.Text = "Biển số :";
            this.xrTableCell18.Weight = 0.47244025939046458D;
            // 
            // SOXE
            // 
            this.SOXE.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.SOXE.Dpi = 254F;
            this.SOXE.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.SOXE.Name = "SOXE";
            this.SOXE.StylePriority.UseBorders = false;
            this.SOXE.StylePriority.UseFont = false;
            this.SOXE.Text = "36M-5800";
            this.SOXE.Weight = 0.393372525721033D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Weight = 0.393372525721033D;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24,
            this.TL_THIETKE,
            this.xrTableCell3,
            this.xrTableCell28,
            this.xrTableCell9});
            this.xrTableRow17.Dpi = 254F;
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.Text = "TL thiết kế :\t";
            this.xrTableCell24.Weight = 0.46127082813879861D;
            // 
            // TL_THIETKE
            // 
            this.TL_THIETKE.Dpi = 254F;
            this.TL_THIETKE.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.TL_THIETKE.Name = "TL_THIETKE";
            this.TL_THIETKE.StylePriority.UseFont = false;
            this.TL_THIETKE.StylePriority.UseTextAlignment = false;
            this.TL_THIETKE.Text = "7900 ";
            this.TL_THIETKE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TL_THIETKE.Weight = 0.28330248120334722D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.Text = "  kg";
            this.xrTableCell3.Weight = 0.91576293056593838D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.Weight = 0.47244025015842761D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.Weight = 0.7867451211121006D;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20,
            this.TL_BANTHAN,
            this.xrTableCell10,
            this.xrTableCell17,
            this.xrTableCell32,
            this.xrTableCell42});
            this.xrTableRow16.Dpi = 254F;
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.Text = "TL bản thân :";
            this.xrTableCell20.Weight = 0.46127062639212257D;
            // 
            // TL_BANTHAN
            // 
            this.TL_BANTHAN.Dpi = 254F;
            this.TL_BANTHAN.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.TL_BANTHAN.Name = "TL_BANTHAN";
            this.TL_BANTHAN.StylePriority.UseFont = false;
            this.TL_BANTHAN.StylePriority.UseTextAlignment = false;
            this.TL_BANTHAN.Text = "6900";
            this.TL_BANTHAN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TL_BANTHAN.Weight = 0.28330255946041211D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.Text = "  kg";
            this.xrTableCell10.Weight = 0.91576316215015907D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Weight = 0.47244018006534838D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Weight = 0.47194876731659041D;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Weight = 0.3147963157939796D;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell30,
            this.TL_XEHANG,
            this.xrTableCell4,
            this.xrTableCell11,
            this.CD_COSO11,
            this.xrTableCell44});
            this.xrTableRow18.Dpi = 254F;
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1D;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.Text = "TL xe và hàng :";
            this.xrTableCell30.Weight = 0.46127078635632918D;
            // 
            // TL_XEHANG
            // 
            this.TL_XEHANG.Dpi = 254F;
            this.TL_XEHANG.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.TL_XEHANG.Name = "TL_XEHANG";
            this.TL_XEHANG.StylePriority.UseFont = false;
            this.TL_XEHANG.StylePriority.UseTextAlignment = false;
            this.TL_XEHANG.Text = "15,100";
            this.TL_XEHANG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TL_XEHANG.Weight = 0.28330245054055958D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.Text = "  kg";
            this.xrTableCell4.Weight = 0.91576296946946989D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Weight = 0.47244001208564146D;
            // 
            // CD_COSO11
            // 
            this.CD_COSO11.Dpi = 254F;
            this.CD_COSO11.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.CD_COSO11.Name = "CD_COSO11";
            this.CD_COSO11.StylePriority.UseFont = false;
            this.CD_COSO11.StylePriority.UseTextAlignment = false;
            this.CD_COSO11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.CD_COSO11.Weight = 0.39337269636330641D;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.StylePriority.UseFont = false;
            this.xrTableCell44.Weight = 0.3933726963633063D;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell36,
            this.TL_THUCTE,
            this.xrTableCell1,
            this.xrTableCell12,
            this.CD_COSO,
            this.xrTableCell34});
            this.xrTableRow19.Dpi = 254F;
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1D;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseFont = false;
            this.xrTableCell36.Text = "TL cân thực tế :";
            this.xrTableCell36.Weight = 0.46127091012629445D;
            // 
            // TL_THUCTE
            // 
            this.TL_THUCTE.Dpi = 254F;
            this.TL_THUCTE.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.TL_THUCTE.Name = "TL_THUCTE";
            this.TL_THUCTE.StylePriority.UseFont = false;
            this.TL_THUCTE.StylePriority.UseTextAlignment = false;
            this.TL_THUCTE.Text = "2,240";
            this.TL_THUCTE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TL_THUCTE.Weight = 0.28330223522267856D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.Text = "  kg";
            this.xrTableCell1.Weight = 0.91576299136364048D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Text = "CD cơ sở :";
            this.xrTableCell12.Weight = 0.47244038122953885D;
            // 
            // CD_COSO
            // 
            this.CD_COSO.Dpi = 254F;
            this.CD_COSO.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.CD_COSO.Name = "CD_COSO";
            this.CD_COSO.StylePriority.UseFont = false;
            this.CD_COSO.StylePriority.UseTextAlignment = false;
            this.CD_COSO.Text = "5000";
            this.CD_COSO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.CD_COSO.Weight = 0.39337252273844292D;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseFont = false;
            this.xrTableCell34.Text = "  mm";
            this.xrTableCell34.Weight = 0.39337257049801788D;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.TL_HANGHOA,
            this.xrTableCell22,
            this.xrTableCell23,
            this.CD_THUCTE,
            this.xrTableCell26});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Text = "TL hàng hóa";
            this.xrTableCell15.Weight = 0.46127091012629445D;
            // 
            // TL_HANGHOA
            // 
            this.TL_HANGHOA.Dpi = 254F;
            this.TL_HANGHOA.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.TL_HANGHOA.Name = "TL_HANGHOA";
            this.TL_HANGHOA.StylePriority.UseFont = false;
            this.TL_HANGHOA.StylePriority.UseTextAlignment = false;
            this.TL_HANGHOA.Text = "50";
            this.TL_HANGHOA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TL_HANGHOA.Weight = 0.28330223522267856D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Text = "  kg";
            this.xrTableCell22.Weight = 0.91576299136364048D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Text = "CD thực tế :";
            this.xrTableCell23.Weight = 0.47244038122953885D;
            // 
            // CD_THUCTE
            // 
            this.CD_THUCTE.Dpi = 254F;
            this.CD_THUCTE.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.CD_THUCTE.Name = "CD_THUCTE";
            this.CD_THUCTE.StylePriority.UseFont = false;
            this.CD_THUCTE.StylePriority.UseTextAlignment = false;
            this.CD_THUCTE.Text = "999";
            this.CD_THUCTE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.CD_THUCTE.Weight = 0.39337252273844292D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Text = "  mm";
            this.xrTableCell26.Weight = 0.39337257049801788D;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.TL_QUATAI,
            this.xrTableCell5,
            this.xrTableCell43,
            this.CD_QUAKHO,
            this.xrTableCell16});
            this.xrTableRow20.Dpi = 254F;
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1D;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.StylePriority.UseFont = false;
            this.xrTableCell41.Text = "TL phụ thu : ";
            this.xrTableCell41.Weight = 0.46127058983624453D;
            // 
            // TL_QUATAI
            // 
            this.TL_QUATAI.Dpi = 254F;
            this.TL_QUATAI.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.TL_QUATAI.Name = "TL_QUATAI";
            this.TL_QUATAI.StylePriority.UseFont = false;
            this.TL_QUATAI.StylePriority.UseTextAlignment = false;
            this.TL_QUATAI.Text = "999,500";
            this.TL_QUATAI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TL_QUATAI.Weight = 0.28330270764292709D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.Text = "  kg";
            this.xrTableCell5.Weight = 0.91576303593200248D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseFont = false;
            this.xrTableCell43.Text = "CD phụ thu :";
            this.xrTableCell43.Weight = 0.47244017902300806D;
            // 
            // CD_QUAKHO
            // 
            this.CD_QUAKHO.Dpi = 254F;
            this.CD_QUAKHO.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.CD_QUAKHO.Name = "CD_QUAKHO";
            this.CD_QUAKHO.StylePriority.UseFont = false;
            this.CD_QUAKHO.StylePriority.UseTextAlignment = false;
            this.CD_QUAKHO.Text = "1000";
            this.CD_QUAKHO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.CD_QUAKHO.Weight = 0.39337232789957216D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.Text = "  mm";
            this.xrTableCell16.Weight = 0.39337277084485783D;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell31,
            this.TT_QUATAI,
            this.xrTableCell39,
            this.xrTableCell19,
            this.TT_QUAKHO,
            this.xrTableCell52});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Text = "Thành tiền :";
            this.xrTableCell31.Weight = 0.46127059045488927D;
            // 
            // TT_QUATAI
            // 
            this.TT_QUATAI.Dpi = 254F;
            this.TT_QUATAI.Name = "TT_QUATAI";
            this.TT_QUATAI.StylePriority.UseTextAlignment = false;
            this.TT_QUATAI.Text = "999,000";
            this.TT_QUATAI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TT_QUATAI.Weight = 0.2833026490683912D;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Text = "  VNĐ";
            this.xrTableCell39.Weight = 0.9157628487573366D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Text = "Thành tiền :";
            this.xrTableCell19.Weight = 0.47244021440723694D;
            // 
            // TT_QUAKHO
            // 
            this.TT_QUAKHO.Dpi = 254F;
            this.TT_QUAKHO.Name = "TT_QUAKHO";
            this.TT_QUAKHO.StylePriority.UseTextAlignment = false;
            this.TT_QUAKHO.Text = "999,000";
            this.TT_QUAKHO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TT_QUAKHO.Weight = 0.39337270200495417D;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Text = "  VNĐ";
            this.xrTableCell52.Weight = 0.39337260648580424D;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.TT_PHUTHU,
            this.xrTableCell29});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.Text = "Tổng tiền phụ thu :";
            this.xrTableCell8.Weight = 0.74457332061561665D;
            // 
            // TT_PHUTHU
            // 
            this.TT_PHUTHU.Dpi = 254F;
            this.TT_PHUTHU.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.TT_PHUTHU.Name = "TT_PHUTHU";
            this.TT_PHUTHU.StylePriority.UseFont = false;
            this.TT_PHUTHU.StylePriority.UseTextAlignment = false;
            this.TT_PHUTHU.Text = "1,800,000";
            this.TT_PHUTHU.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TT_PHUTHU.Weight = 1.7815756840771915D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.Text = "  VNĐ";
            this.xrTableCell29.Weight = 0.39337260648580424D;
            // 
            // xrLabel29
            // 
            this.xrLabel29.CanGrow = false;
            this.xrLabel29.Dpi = 254F;
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(995.8563F, 93.21589F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(255.2134F, 62.65331F);
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.StylePriority.UsePadding = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "Bảo vệ";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DATEDAY
            // 
            this.DATEDAY.CanGrow = false;
            this.DATEDAY.Dpi = 254F;
            this.DATEDAY.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic);
            this.DATEDAY.LocationFloat = new DevExpress.Utils.PointFloat(592.0441F, 0F);
            this.DATEDAY.Name = "DATEDAY";
            this.DATEDAY.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.DATEDAY.SizeF = new System.Drawing.SizeF(1373.666F, 62.65331F);
            this.DATEDAY.StylePriority.UseFont = false;
            this.DATEDAY.StylePriority.UsePadding = false;
            this.DATEDAY.StylePriority.UseTextAlignment = false;
            this.DATEDAY.Text = "ha tiên";
            this.DATEDAY.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel43
            // 
            this.xrLabel43.CanGrow = false;
            this.xrLabel43.Dpi = 254F;
            this.xrLabel43.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(1644.564F, 93.21589F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(321.1459F, 62.65331F);
            this.xrLabel43.StylePriority.UseFont = false;
            this.xrLabel43.StylePriority.UsePadding = false;
            this.xrLabel43.StylePriority.UseTextAlignment = false;
            this.xrLabel43.Text = "Người cân xe";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8,
            this.xrTable9,
            this.xrTable10,
            this.xrTable7,
            this.xrTable5});
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 1177.002F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrLabel29,
            this.DATEDAY,
            this.xrLabel43});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.HeightF = 180.8691F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrLabel1
            // 
            this.xrLabel1.CanGrow = false;
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(272.1633F, 93.21589F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(253.4867F, 62.65331F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UsePadding = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Lái xe";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // rptPhieuCanXe
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.GroupFooter1});
            this.Dpi = 254F;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 99, 51);
            this.PageHeight = 2101;
            this.PageWidth = 2101;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.ShowPrintMarginsWarning = false;
            this.SnapGridSize = 31.75F;
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell txtTencongty;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell txtDiachi;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell txtDienthoai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel DATEDAY;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell MAPHIEU;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell LOAIXE;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell SOXE;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell TL_THIETKE;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell TL_BANTHAN;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell TL_XEHANG;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell TL_THUCTE;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell TL_QUATAI;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell TENDOITUONG11;
        private DevExpress.XtraReports.UI.XRTableCell TENTUYEN;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell GIODI;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell CD_QUAKHO;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell CD_COSO11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell CD_COSO;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell TT_QUATAI;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell TT_QUAKHO;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell TT_PHUTHU;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell TL_HANGHOA;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell CD_THUCTE;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
    }
}
