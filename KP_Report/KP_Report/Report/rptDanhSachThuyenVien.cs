﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace KP_Report
{
    /// <summary>
    /// Report Danh sach hanh khach
    /// </summary>
    public partial class rptDanhSachThuyenVien : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report Danh sach hanh khach
        /// </summary>
        public rptDanhSachThuyenVien()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            txtTenThuyenVien.DataBindings.Add("Text", DataSource, "TENTHUYENVIEN");
            txtChucDanh.DataBindings.Add("Text", DataSource, "CHUCDANH");
            txtHoChieu.DataBindings.Add("Text", DataSource, "HOCHIEU");
            txtNgaySinh.DataBindings.Add("Text", DataSource, "NGAYSINH","{0:yyyy}");
            txtQuocTich.DataBindings.Add("Text", DataSource, "QUOCTICH");
            DataTable dt = (DataTable)DataSource;
            try
            {
                txtTenTau.Text = "1.1 Tên tàu : " + dt.Rows[0]["TENDOITUONG"].ToString() + "\n" + "Name of ship";
                txtNgayDenRoi.Text = "3. Ngày đến/ rời : " + String.Format("{0:dd/MM/yyyy}", dt.Rows[0]["NGAYDI"]) + "\n" + "Date of arrival/departure";
                txtIMO.Text = "1.2 Số IMO : " + dt.Rows[0]["IMO"].ToString() + "\n" + "IMO number";
                txtHoHieu.Text = "1.3 Hô hiệu : " + dt.Rows[0]["HOHIEU"].ToString() + "\n" + "Call sign";
                txtQuocTichTau.Text = "4. Quốc tịch tàu : " + dt.Rows[0]["QUOCTICHTAU"].ToString() + "\n" + "Flag State of ship";
            }
            catch
            {
            }
        }
        int j = 0;
        private void PageHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (j == 0)
            {
                j++;
                xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            }
            else
            {
                xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom ;
            }
        }
        int i;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
    }
}
