﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptCombo : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptCombo()
        {
            InitializeComponent();
            GroupField grf = new GroupField("GROUPCOMBO");
            GroupHeader1.GroupFields.Add(grf);
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            ID.DataBindings.Add("Text", DataSource, "ID");
            GROUPCOMBO.DataBindings.Add("Text", DataSource, "GROUPCOMBO");
            NAME.DataBindings.Add("Text", DataSource, "NAME");
            DVT.DataBindings.Add("Text", DataSource, "DVT");
            PRICE.DataBindings.Add("Text", DataSource, "PRICE1", "{0:#,###0.00}");
            QTY.DataBindings.Add("Text", DataSource, "QTY", "{0:#,###0}");
            DataTable dtSource = (DataTable)DataSource;
            if (dtSource.Rows[0]["FILTER"].ToString() != "")
            {
                txt_tieude.Text = "(" + dtSource.Rows[0]["FILTER"].ToString() + ")";
            }
        }

    }
}
