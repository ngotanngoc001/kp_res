﻿namespace KP_Report
{
    /// <summary>
    /// 
    /// </summary>
    partial class rpt_inhoadonKhachSan1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cl_HangHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.cl_soluong = new DevExpress.XtraReports.UI.XRTableCell();
            this.cl_giaban = new DevExpress.XtraReports.UI.XRTableCell();
            this.cl_thanhtien = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.lb_mahoadon = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_ngay = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_Tenban = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_nhanvien = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_Quay = new DevExpress.XtraReports.UI.XRLabel();
            this.lbGiora = new DevExpress.XtraReports.UI.XRLabel();
            this.lbGiovao = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbNgayvao = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbNgayra = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_Header = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lb_ban = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_NameBill = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbTongthanhtien = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbTiengio = new DevExpress.XtraReports.UI.XRLabel();
            this.lbTiendatcoc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbPhihucvu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbFooter = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_tienkhachtra = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_conlai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.lb_giamgia = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_tongcong = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_MaHD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTG = new DevExpress.XtraReports.UI.XRLabel();
            this.xrSL = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine7 = new DevExpress.XtraReports.UI.XRLine();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail.HeightF = 16.66667F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.ForeColor = System.Drawing.Color.Blue;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(276.3129F, 16.66667F);
            this.xrTable2.StylePriority.UseBorderDashStyle = false;
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseForeColor = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cl_HangHoa,
            this.cl_soluong,
            this.cl_giaban,
            this.cl_thanhtien});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // cl_HangHoa
            // 
            this.cl_HangHoa.Name = "cl_HangHoa";
            this.cl_HangHoa.StylePriority.UseTextAlignment = false;
            this.cl_HangHoa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cl_HangHoa.Weight = 1.0798272034384837D;
            // 
            // cl_soluong
            // 
            this.cl_soluong.Name = "cl_soluong";
            this.cl_soluong.Weight = 0.32684008609098952D;
            // 
            // cl_giaban
            // 
            this.cl_giaban.Name = "cl_giaban";
            this.cl_giaban.StylePriority.UseTextAlignment = false;
            this.cl_giaban.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cl_giaban.Weight = 0.5372778948616983D;
            // 
            // cl_thanhtien
            // 
            this.cl_thanhtien.Name = "cl_thanhtien";
            this.cl_thanhtien.StylePriority.UseTextAlignment = false;
            this.cl_thanhtien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cl_thanhtien.Weight = 0.64009191252155084D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 22F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTT,
            this.xrSL,
            this.xrTG,
            this.xrTable3,
            this.lb_mahoadon,
            this.lb_ngay,
            this.xrLabel3,
            this.xrLabel14,
            this.lb_Tenban,
            this.lb_nhanvien,
            this.xrLabel5,
            this.xrLabel15,
            this.xrLabel4,
            this.lb_Quay,
            this.lbGiora,
            this.lbGiovao,
            this.xrLabel1,
            this.lbNgayvao,
            this.xrLabel12,
            this.lbNgayra,
            this.lb_Header,
            this.xrLine1,
            this.xrLine2,
            this.xrTable1,
            this.lb_ban,
            this.lb_NameBill,
            this.xrLine7});
            this.ReportHeader.HeightF = 254.9967F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // lb_mahoadon
            // 
            this.lb_mahoadon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_mahoadon.ForeColor = System.Drawing.Color.Blue;
            this.lb_mahoadon.LocationFloat = new DevExpress.Utils.PointFloat(164F, 110.6633F);
            this.lb_mahoadon.Name = "lb_mahoadon";
            this.lb_mahoadon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_mahoadon.SizeF = new System.Drawing.SizeF(113.854F, 17.79171F);
            this.lb_mahoadon.StylePriority.UseFont = false;
            this.lb_mahoadon.StylePriority.UseForeColor = false;
            this.lb_mahoadon.StylePriority.UseTextAlignment = false;
            this.lb_mahoadon.Text = "lb_mahoadon";
            this.lb_mahoadon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lb_ngay
            // 
            this.lb_ngay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ngay.ForeColor = System.Drawing.Color.Blue;
            this.lb_ngay.LocationFloat = new DevExpress.Utils.PointFloat(160F, 92.87169F);
            this.lb_ngay.Name = "lb_ngay";
            this.lb_ngay.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_ngay.SizeF = new System.Drawing.SizeF(117.8539F, 17.79163F);
            this.lb_ngay.StylePriority.UseFont = false;
            this.lb_ngay.StylePriority.UseForeColor = false;
            this.lb_ngay.StylePriority.UseTextAlignment = false;
            this.lb_ngay.Text = "xrLabel6";
            this.lb_ngay.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(122.36F, 110.6633F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(45.593F, 17.79169F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseForeColor = false;
            this.xrLabel3.Text = "Số HD:";
            // 
            // xrLabel14
            // 
            this.xrLabel14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(1.449998F, 110.6617F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(48.41668F, 17.79F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseForeColor = false;
            this.xrLabel14.Text = "Phòng:";
            // 
            // lb_Tenban
            // 
            this.lb_Tenban.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Tenban.ForeColor = System.Drawing.Color.Blue;
            this.lb_Tenban.LocationFloat = new DevExpress.Utils.PointFloat(42.36F, 110.6633F);
            this.lb_Tenban.Name = "lb_Tenban";
            this.lb_Tenban.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_Tenban.SizeF = new System.Drawing.SizeF(80F, 17.79F);
            this.lb_Tenban.StylePriority.UseFont = false;
            this.lb_Tenban.StylePriority.UseForeColor = false;
            this.lb_Tenban.StylePriority.UseTextAlignment = false;
            this.lb_Tenban.Text = "lb_Tenban";
            this.lb_Tenban.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lb_nhanvien
            // 
            this.lb_nhanvien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_nhanvien.ForeColor = System.Drawing.Color.Blue;
            this.lb_nhanvien.LocationFloat = new DevExpress.Utils.PointFloat(70.83F, 75.08F);
            this.lb_nhanvien.Name = "lb_nhanvien";
            this.lb_nhanvien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_nhanvien.SizeF = new System.Drawing.SizeF(207.0206F, 17.79166F);
            this.lb_nhanvien.StylePriority.UseFont = false;
            this.lb_nhanvien.StylePriority.UseForeColor = false;
            this.lb_nhanvien.Text = "xrLabel6";
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 75.08F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(70.83334F, 17.79169F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseForeColor = false;
            this.xrLabel5.Text = "Nhân Viên:";
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(1.45F, 92.87169F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(38F, 17.79F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseForeColor = false;
            this.xrLabel15.Text = "Quầy:";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(122.36F, 92.87169F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(42.50227F, 17.79163F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseForeColor = false;
            this.xrLabel4.Text = "Ngày:";
            // 
            // lb_Quay
            // 
            this.lb_Quay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Quay.ForeColor = System.Drawing.Color.Blue;
            this.lb_Quay.LocationFloat = new DevExpress.Utils.PointFloat(42.36F, 92.87169F);
            this.lb_Quay.Name = "lb_Quay";
            this.lb_Quay.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_Quay.SizeF = new System.Drawing.SizeF(80F, 17.79F);
            this.lb_Quay.StylePriority.UseFont = false;
            this.lb_Quay.StylePriority.UseForeColor = false;
            this.lb_Quay.StylePriority.UseTextAlignment = false;
            this.lb_Quay.Text = "xrLabel6";
            this.lb_Quay.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbGiora
            // 
            this.lbGiora.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGiora.ForeColor = System.Drawing.Color.Blue;
            this.lbGiora.LocationFloat = new DevExpress.Utils.PointFloat(146.25F, 156.7467F);
            this.lbGiora.Name = "lbGiora";
            this.lbGiora.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbGiora.SizeF = new System.Drawing.SizeF(41.04083F, 17.79169F);
            this.lbGiora.StylePriority.UseFont = false;
            this.lbGiora.StylePriority.UseForeColor = false;
            this.lbGiora.Text = "09 :00";
            // 
            // lbGiovao
            // 
            this.lbGiovao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGiovao.ForeColor = System.Drawing.Color.Blue;
            this.lbGiovao.LocationFloat = new DevExpress.Utils.PointFloat(146.25F, 138.955F);
            this.lbGiovao.Name = "lbGiovao";
            this.lbGiovao.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbGiovao.SizeF = new System.Drawing.SizeF(41.04083F, 17.79169F);
            this.lbGiovao.StylePriority.UseFont = false;
            this.lbGiovao.StylePriority.UseForeColor = false;
            this.lbGiovao.Text = "14 : 30";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 138.955F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(81.25001F, 17.79169F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.Text = "Nhận phòng:";
            // 
            // lbNgayvao
            // 
            this.lbNgayvao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNgayvao.ForeColor = System.Drawing.Color.Blue;
            this.lbNgayvao.LocationFloat = new DevExpress.Utils.PointFloat(81.25002F, 138.955F);
            this.lbNgayvao.Name = "lbNgayvao";
            this.lbNgayvao.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbNgayvao.SizeF = new System.Drawing.SizeF(65F, 17.79F);
            this.lbNgayvao.StylePriority.UseFont = false;
            this.lbNgayvao.StylePriority.UseForeColor = false;
            this.lbNgayvao.Text = "22/02/2012";
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(0F, 156.7467F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(81.25001F, 17.79169F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseForeColor = false;
            this.xrLabel12.Text = "Trả phòng:";
            // 
            // lbNgayra
            // 
            this.lbNgayra.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNgayra.ForeColor = System.Drawing.Color.Blue;
            this.lbNgayra.LocationFloat = new DevExpress.Utils.PointFloat(81.25002F, 156.7467F);
            this.lbNgayra.Name = "lbNgayra";
            this.lbNgayra.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbNgayra.SizeF = new System.Drawing.SizeF(65F, 17.79169F);
            this.lbNgayra.StylePriority.UseFont = false;
            this.lbNgayra.StylePriority.UseForeColor = false;
            this.lbNgayra.Text = "23/02/2012";
            // 
            // lb_Header
            // 
            this.lb_Header.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Header.ForeColor = System.Drawing.Color.Blue;
            this.lb_Header.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.lb_Header.Multiline = true;
            this.lb_Header.Name = "lb_Header";
            this.lb_Header.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_Header.SizeF = new System.Drawing.SizeF(276.3129F, 19.79168F);
            this.lb_Header.StylePriority.UseFont = false;
            this.lb_Header.StylePriority.UseForeColor = false;
            this.lb_Header.StylePriority.UseTextAlignment = false;
            this.lb_Header.Text = "Điện Máy Thiên Hòa";
            this.lb_Header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLine1
            // 
            this.xrLine1.BorderWidth = 1;
            this.xrLine1.ForeColor = System.Drawing.Color.Blue;
            this.xrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0.44F, 128.455F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(275.5339F, 10.5F);
            this.xrLine1.StylePriority.UseBorderWidth = false;
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // xrLine2
            // 
            this.xrLine2.ForeColor = System.Drawing.Color.Blue;
            this.xrLine2.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 244.4967F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(275.5338F, 10.5F);
            this.xrLine2.StylePriority.UseForeColor = false;
            // 
            // xrTable1
            // 
            this.xrTable1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.ForeColor = System.Drawing.Color.Blue;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 225.7467F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(276.3128F, 18.75F);
            this.xrTable1.StylePriority.UseBorderDashStyle = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseForeColor = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell5,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Hàng Hóa";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.75346678913766585D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "SL";
            this.xrTableCell2.Weight = 0.22805792921030449D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "Giá Bán";
            this.xrTableCell5.Weight = 0.37489441575657512D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "Thành Tiền";
            this.xrTableCell3.Weight = 0.446634023950387D;
            // 
            // lb_ban
            // 
            this.lb_ban.Font = new System.Drawing.Font("Times New Roman", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ban.ForeColor = System.Drawing.Color.Blue;
            this.lb_ban.LocationFloat = new DevExpress.Utils.PointFloat(62.51295F, 59.375F);
            this.lb_ban.Name = "lb_ban";
            this.lb_ban.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_ban.SizeF = new System.Drawing.SizeF(165.47F, 15.705F);
            this.lb_ban.StylePriority.UseFont = false;
            this.lb_ban.StylePriority.UseForeColor = false;
            this.lb_ban.StylePriority.UseTextAlignment = false;
            this.lb_ban.Text = "(Hóa đơn bán lẻ có giá trị xuất hóa đơn GTGT trong ngày)";
            this.lb_ban.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lb_ban.Visible = false;
            // 
            // lb_NameBill
            // 
            this.lb_NameBill.Font = new System.Drawing.Font("Tahoma", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_NameBill.ForeColor = System.Drawing.Color.Blue;
            this.lb_NameBill.LocationFloat = new DevExpress.Utils.PointFloat(0F, 36.16673F);
            this.lb_NameBill.Multiline = true;
            this.lb_NameBill.Name = "lb_NameBill";
            this.lb_NameBill.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_NameBill.SizeF = new System.Drawing.SizeF(277.854F, 23.20828F);
            this.lb_NameBill.StylePriority.UseFont = false;
            this.lb_NameBill.StylePriority.UseForeColor = false;
            this.lb_NameBill.StylePriority.UseTextAlignment = false;
            this.lb_NameBill.Text = "Hóa Đơn Bán Lẻ\r\n";
            this.lb_NameBill.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel13,
            this.lbTongthanhtien,
            this.xrLabel6,
            this.lbTiengio,
            this.lbTiendatcoc,
            this.xrLabel8,
            this.lbPhihucvu,
            this.xrLabel2,
            this.lbFooter,
            this.xrLine5,
            this.xrLabel11,
            this.lb_tienkhachtra,
            this.xrLabel7,
            this.lb_conlai,
            this.xrLabel10,
            this.xrLine3,
            this.lb_giamgia,
            this.xrLabel9,
            this.lb_tongcong,
            this.lb_MaHD});
            this.ReportFooter.HeightF = 272.75F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabel13
            // 
            this.xrLabel13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(3.012721F, 33.20834F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(146.6241F, 17F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseForeColor = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "Dịch vụ:";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbTongthanhtien
            // 
            this.lbTongthanhtien.ForeColor = System.Drawing.Color.Blue;
            this.lbTongthanhtien.LocationFloat = new DevExpress.Utils.PointFloat(150.4159F, 33.20834F);
            this.lbTongthanhtien.Name = "lbTongthanhtien";
            this.lbTongthanhtien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTongthanhtien.SizeF = new System.Drawing.SizeF(125.45F, 17F);
            this.lbTongthanhtien.StylePriority.UseForeColor = false;
            this.lbTongthanhtien.StylePriority.UseTextAlignment = false;
            this.lbTongthanhtien.Text = "0";
            this.lbTongthanhtien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(3.012721F, 10.20832F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(146.6241F, 17F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseForeColor = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Tiền phòng:";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbTiengio
            // 
            this.lbTiengio.ForeColor = System.Drawing.Color.Blue;
            this.lbTiengio.LocationFloat = new DevExpress.Utils.PointFloat(150.416F, 10.20832F);
            this.lbTiengio.Name = "lbTiengio";
            this.lbTiengio.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTiengio.SizeF = new System.Drawing.SizeF(125.8969F, 17F);
            this.lbTiengio.StylePriority.UseForeColor = false;
            this.lbTiengio.StylePriority.UseTextAlignment = false;
            this.lbTiengio.Text = "0";
            this.lbTiengio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lbTiendatcoc
            // 
            this.lbTiendatcoc.ForeColor = System.Drawing.Color.Blue;
            this.lbTiendatcoc.LocationFloat = new DevExpress.Utils.PointFloat(150.4159F, 102.0001F);
            this.lbTiendatcoc.Name = "lbTiendatcoc";
            this.lbTiendatcoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTiendatcoc.SizeF = new System.Drawing.SizeF(125.8969F, 17F);
            this.lbTiendatcoc.StylePriority.UseForeColor = false;
            this.lbTiendatcoc.StylePriority.UseTextAlignment = false;
            this.lbTiendatcoc.Text = "0";
            this.lbTiendatcoc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(3.012721F, 102.0001F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(146.6241F, 17F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseForeColor = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "Tiền đặt cọc:";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbPhihucvu
            // 
            this.lbPhihucvu.ForeColor = System.Drawing.Color.Blue;
            this.lbPhihucvu.LocationFloat = new DevExpress.Utils.PointFloat(150.4159F, 79.00006F);
            this.lbPhihucvu.Name = "lbPhihucvu";
            this.lbPhihucvu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbPhihucvu.SizeF = new System.Drawing.SizeF(125.4302F, 17F);
            this.lbPhihucvu.StylePriority.UseForeColor = false;
            this.lbPhihucvu.StylePriority.UseTextAlignment = false;
            this.lbPhihucvu.Text = "0";
            this.lbPhihucvu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(3.012721F, 79.00006F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(146.6241F, 17F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseForeColor = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Phí phục vụ:";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbFooter
            // 
            this.lbFooter.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFooter.ForeColor = System.Drawing.Color.Blue;
            this.lbFooter.LocationFloat = new DevExpress.Utils.PointFloat(2.392697F, 204.1668F);
            this.lbFooter.Multiline = true;
            this.lbFooter.Name = "lbFooter";
            this.lbFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbFooter.SizeF = new System.Drawing.SizeF(273.9201F, 15.91661F);
            this.lbFooter.StylePriority.UseFont = false;
            this.lbFooter.StylePriority.UseForeColor = false;
            this.lbFooter.StylePriority.UseTextAlignment = false;
            this.lbFooter.Text = "Xin hân hạnh đón tiếp quý khách.";
            this.lbFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine5
            // 
            this.xrLine5.ForeColor = System.Drawing.Color.Blue;
            this.xrLine5.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(2.392697F, 198.8753F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(273.9201F, 5.291656F);
            this.xrLine5.StylePriority.UseForeColor = false;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(3.012721F, 171.0001F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(146.6241F, 17F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseForeColor = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "Tiền thối lại:";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lb_tienkhachtra
            // 
            this.lb_tienkhachtra.ForeColor = System.Drawing.Color.Blue;
            this.lb_tienkhachtra.LocationFloat = new DevExpress.Utils.PointFloat(150.8826F, 148.0001F);
            this.lb_tienkhachtra.Name = "lb_tienkhachtra";
            this.lb_tienkhachtra.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_tienkhachtra.SizeF = new System.Drawing.SizeF(125.4302F, 17F);
            this.lb_tienkhachtra.StylePriority.UseForeColor = false;
            this.lb_tienkhachtra.StylePriority.UseTextAlignment = false;
            this.lb_tienkhachtra.Text = "0";
            this.lb_tienkhachtra.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(3.012721F, 148.0001F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(146.6241F, 17F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseForeColor = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Tổng tiền khách trả:";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lb_conlai
            // 
            this.lb_conlai.ForeColor = System.Drawing.Color.Blue;
            this.lb_conlai.LocationFloat = new DevExpress.Utils.PointFloat(150.4159F, 171.0001F);
            this.lb_conlai.Name = "lb_conlai";
            this.lb_conlai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_conlai.SizeF = new System.Drawing.SizeF(125.8969F, 17F);
            this.lb_conlai.StylePriority.UseForeColor = false;
            this.lb_conlai.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:#.00}";
            this.lb_conlai.Summary = xrSummary1;
            this.lb_conlai.Text = "0";
            this.lb_conlai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel10.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(3.012721F, 125.0001F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(146.6241F, 16.99999F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseForeColor = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "Tổng tiền thanh toán:";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine3
            // 
            this.xrLine3.ForeColor = System.Drawing.Color.Blue;
            this.xrLine3.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(276.3128F, 5.291667F);
            this.xrLine3.StylePriority.UseForeColor = false;
            // 
            // lb_giamgia
            // 
            this.lb_giamgia.ForeColor = System.Drawing.Color.Blue;
            this.lb_giamgia.LocationFloat = new DevExpress.Utils.PointFloat(150.4159F, 56.00004F);
            this.lb_giamgia.Name = "lb_giamgia";
            this.lb_giamgia.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_giamgia.SizeF = new System.Drawing.SizeF(125.8969F, 17F);
            this.lb_giamgia.StylePriority.UseForeColor = false;
            this.lb_giamgia.StylePriority.UseTextAlignment = false;
            this.lb_giamgia.Text = "0";
            this.lb_giamgia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(3.012721F, 56.00004F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(146.6241F, 17F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseForeColor = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "Giảm giá:";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lb_tongcong
            // 
            this.lb_tongcong.ForeColor = System.Drawing.Color.Blue;
            this.lb_tongcong.LocationFloat = new DevExpress.Utils.PointFloat(150.8826F, 125.0001F);
            this.lb_tongcong.Name = "lb_tongcong";
            this.lb_tongcong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_tongcong.SizeF = new System.Drawing.SizeF(125.4302F, 17F);
            this.lb_tongcong.StylePriority.UseForeColor = false;
            this.lb_tongcong.StylePriority.UseTextAlignment = false;
            xrSummary2.FormatString = "{0:#.00}";
            this.lb_tongcong.Summary = xrSummary2;
            this.lb_tongcong.Text = "0";
            this.lb_tongcong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lb_MaHD
            // 
            this.lb_MaHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_MaHD.ForeColor = System.Drawing.Color.Black;
            this.lb_MaHD.LocationFloat = new DevExpress.Utils.PointFloat(1.958489F, 246.625F);
            this.lb_MaHD.Name = "lb_MaHD";
            this.lb_MaHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lb_MaHD.SizeF = new System.Drawing.SizeF(274.3543F, 16.12505F);
            this.lb_MaHD.StylePriority.UseFont = false;
            this.lb_MaHD.StylePriority.UseForeColor = false;
            this.lb_MaHD.StylePriority.UseTextAlignment = false;
            this.lb_MaHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable3
            // 
            this.xrTable3.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable3.ForeColor = System.Drawing.Color.Blue;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 176.6217F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(276.3129F, 18.75F);
            this.xrTable3.StylePriority.UseBorderDashStyle = false;
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.StylePriority.UseForeColor = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell7,
            this.xrTableCell8});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Thời Gian";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell4.Weight = 0.79508046073276728D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Text = "SL";
            this.xrTableCell7.Weight = 0.27968971876088544D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "Thành Tiền";
            this.xrTableCell8.Weight = 0.72980798308948858D;
            // 
            // xrTG
            // 
            this.xrTG.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTG.ForeColor = System.Drawing.Color.Blue;
            this.xrTG.LocationFloat = new DevExpress.Utils.PointFloat(0F, 195.3717F);
            this.xrTG.Multiline = true;
            this.xrTG.Name = "xrTG";
            this.xrTG.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTG.SizeF = new System.Drawing.SizeF(121.7409F, 19.875F);
            this.xrTG.StylePriority.UseBorders = false;
            this.xrTG.StylePriority.UseFont = false;
            this.xrTG.StylePriority.UseForeColor = false;
            this.xrTG.StylePriority.UseTextAlignment = false;
            this.xrTG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrSL
            // 
            this.xrSL.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrSL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrSL.ForeColor = System.Drawing.Color.Blue;
            this.xrSL.LocationFloat = new DevExpress.Utils.PointFloat(120.9618F, 195.3717F);
            this.xrSL.Multiline = true;
            this.xrSL.Name = "xrSL";
            this.xrSL.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrSL.SizeF = new System.Drawing.SizeF(42.2591F, 19.875F);
            this.xrSL.StylePriority.UseBorders = false;
            this.xrSL.StylePriority.UseFont = false;
            this.xrSL.StylePriority.UseForeColor = false;
            this.xrSL.StylePriority.UseTextAlignment = false;
            this.xrSL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTT
            // 
            this.xrTT.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTT.ForeColor = System.Drawing.Color.Blue;
            this.xrTT.LocationFloat = new DevExpress.Utils.PointFloat(163.2209F, 195.3717F);
            this.xrTT.Multiline = true;
            this.xrTT.Name = "xrTT";
            this.xrTT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTT.SizeF = new System.Drawing.SizeF(112.3129F, 19.875F);
            this.xrTT.StylePriority.UseBorders = false;
            this.xrTT.StylePriority.UseFont = false;
            this.xrTT.StylePriority.UseForeColor = false;
            this.xrTT.StylePriority.UseTextAlignment = false;
            this.xrTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLine7
            // 
            this.xrLine7.ForeColor = System.Drawing.Color.Blue;
            this.xrLine7.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 215.2467F);
            this.xrLine7.Name = "xrLine7";
            this.xrLine7.SizeF = new System.Drawing.SizeF(275.5338F, 10.5F);
            this.xrLine7.StylePriority.UseForeColor = false;
            // 
            // rpt_inhoadonKhachSan1
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.ReportFooter});
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.Margins = new System.Drawing.Printing.Margins(20, 80, 0, 22);
            this.PageHeight = 2000;
            this.PageWidth = 400;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ShowPrintMarginsWarning = false;
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRLabel lb_NameBill;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell cl_HangHoa;
        private DevExpress.XtraReports.UI.XRTableCell cl_soluong;
        private DevExpress.XtraReports.UI.XRTableCell cl_giaban;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel lb_giamgia;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel lb_tongcong;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel lb_tienkhachtra;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel lb_conlai;
        private DevExpress.XtraReports.UI.XRTableCell cl_thanhtien;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRLabel lb_Header;
        private DevExpress.XtraReports.UI.XRLabel lbFooter;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLabel lb_MaHD;
        private DevExpress.XtraReports.UI.XRLabel lbTiendatcoc;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel lbPhihucvu;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lbNgayvao;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel lbNgayra;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel lbTiengio;
        private DevExpress.XtraReports.UI.XRLabel lbGiora;
        private DevExpress.XtraReports.UI.XRLabel lbGiovao;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel lbTongthanhtien;
        private DevExpress.XtraReports.UI.XRLabel lb_mahoadon;
        private DevExpress.XtraReports.UI.XRLabel lb_ngay;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel lb_Tenban;
        private DevExpress.XtraReports.UI.XRLabel lb_nhanvien;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel lb_Quay;
        private DevExpress.XtraReports.UI.XRLabel lb_ban;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRLabel xrTG;
        private DevExpress.XtraReports.UI.XRLabel xrTT;
        private DevExpress.XtraReports.UI.XRLabel xrSL;
        private DevExpress.XtraReports.UI.XRLine xrLine7;
    }
}
