﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBCBangkehoadon : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBCBangkehoadon()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        int i=0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            TUNGAY.DataBindings.Add("Text", DataSource, "TUNGAY", "{0:dd/MM/yyyy}");
            DENNGAY.DataBindings.Add("Text", DataSource, "DENNGAY", "{0:dd/MM/yyyy}");
            DIADIEM.DataBindings.Add("Text", DataSource, "DIADIEM");
            TAU.DataBindings.Add("Text", DataSource, "TAU");
            KHACHHANG.DataBindings.Add("Text", DataSource, "KHACHHANG");
            NHANVIEN.DataBindings.Add("Text", DataSource, "NHANVIEN");
            MAUSO.DataBindings.Add("Text", DataSource, "MAUSO");
            KYHIEU.DataBindings.Add("Text", DataSource, "KYHIEU");
            SOHOADON.DataBindings.Add("Text", DataSource, "SOHOADON");
            NGAYCHUNGTU.DataBindings.Add("Text", DataSource, "NGAYCHUNGTU", "{0:dd/MM/yyyy}");
            DONVIMUA.DataBindings.Add("Text", DataSource, "DONVIMUA");
            MST.DataBindings.Add("Text", DataSource, "MST");
            TENHANGHOA.DataBindings.Add("Text", DataSource, "TENHANGHOA");
            TENDOITUONG.DataBindings.Add("Text", DataSource, "TENDOITUONG");
            THANHTIEN.DataBindings.Add("Text", DataSource, "THANHTIEN", "{0:#,###0.00}");
            TIENVAT.DataBindings.Add("Text", DataSource, "TIENVAT", "{0:#,###0.00}");
            TONGTIEN.DataBindings.Add("Text", DataSource, "TONGTIEN", "{0:#,###0.00}");
            THANHTIEN1.DataBindings.Add("Text", DataSource, "THANHTIEN");
            THANHTIEN1.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0.00}");
            TIENVAT1.DataBindings.Add("Text", DataSource, "TIENVAT");
            TIENVAT1.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0.00}");
            TONGTIEN1.DataBindings.Add("Text", DataSource, "TONGTIEN");
            TONGTIEN1.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0.00}");
        }
    }
}
