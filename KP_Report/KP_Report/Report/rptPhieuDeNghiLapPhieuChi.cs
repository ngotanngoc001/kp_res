﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptPhieuDeNghiLapPhieuChi : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptPhieuDeNghiLapPhieuChi()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX " + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            STT.DataBindings.Add("Text", DataSource, "STT");
            TENHANGHOA.DataBindings.Add("Text", DataSource, "TEN_HANGHOA");
            GHICHU.DataBindings.Add("Text", DataSource, "GHICHU");
            DVT.DataBindings.Add("Text", DataSource, "TEN_DONVITINH")   ;
            SL.DataBindings.Add("Text", DataSource, "SOLUONG", "{0:#,###0.00}");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:#,###0.00}");
            THANHTIEN.DataBindings.Add("Text", DataSource, "THANHTIEN", "{0:#,###0.00}");
            Duyet.DataBindings.Add("Text", DataSource, "Duyet");

            txtNhanVien.DataBindings.Add("Text", DataSource, "NHANVIEN");
            txtLyDo.DataBindings.Add("Text", DataSource, "LYDO");
            txtChucVu.DataBindings.Add("Text", DataSource, "CHUCVU");
            txtSoPhieu.DataBindings.Add("Text", DataSource, "MAPDNDC");
            txtNgay.DataBindings.Add("Text", DataSource, "NGAYLAPPHIEU", "{0:dd/MM/yyyy}");

            txt_sltongcong.DataBindings.Add("Text", DataSource, "SOLUONG");
            txt_sltongcong.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0.00}");
            txt_thanhtientongcong.DataBindings.Add("Text", DataSource, "THANHTIEN");
            txt_thanhtientongcong.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0.00}");
            TONGTIENDUYET.DataBindings.Add("Text", DataSource, "TONGTIENDUYET", "{0:#,###0.00}");
            TONGSLDUYET.DataBindings.Add("Text", DataSource, "TONGSLDUYET", "{0:#,###0.00}");
        }
    }
}
