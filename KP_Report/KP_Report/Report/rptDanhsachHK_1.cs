﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace KP_Report
{
    /// <summary>
    /// Report Danh sach hanh khach
    /// </summary>
    public partial class rptDanhsachHK_1 : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report Danh sach hanh khach
        /// </summary>
        public rptDanhsachHK_1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            TENHANHKHACH.DataBindings.Add("Text", DataSource, "TENHANHKHACH");
            QUOCTICH.DataBindings.Add("Text", DataSource, "QUOCTICH");
            CMND.DataBindings.Add("Text", DataSource, "CMND");
            NGAYVANOISINH.DataBindings.Add("Text", DataSource, "NGAYVANOISINH");
            DataTable dt = (DataTable)DataSource;
            try
            {
                TENDOITUONG.Text = "1 Tên tàu : " + dt.Rows[0]["TENDOITUONG"].ToString() + "\n" + "Name of ship";
                NGAYDEN_ROI.Text = "3. Ngày đến/ rời: " + String.Format("{0:dd/MM/yyyy}",dt.Rows[0]["NGAYDI"]) + "\n" + "Date of arrival/departure";
                TUYEN.Text = "1.2 Tuyến : " + dt.Rows[0]["TENTUYEN"].ToString();
                GIO.Text = "1.3 Giờ rời cảng : " + dt.Rows[0]["GIODI"].ToString();
                BIENSO.Text = "1.5 Hô hiệu : " + dt.Rows[0]["SODANGKY"].ToString() + "\n" + "Call sign";

            }
            catch
            {
            }
        }
        int j = 0;
        private void PageHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (j == 0)
            {
                j++;
                xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            }
            else
            {
                xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom ;
            }
        }
        int i;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
    }
}
