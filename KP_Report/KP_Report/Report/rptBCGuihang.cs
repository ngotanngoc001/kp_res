﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBCGuihang : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report  
        /// </summary>
        public rptBCGuihang()
        {
            InitializeComponent();
            GroupField grf = new GroupField("NGUOITAO");
            GroupHeader1.GroupFields.Add(grf);
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        int i=0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            FROMDATE.DataBindings.Add("Text", DataSource, "FROMDATE", "{0:dd/MM/yyyy}");
            TODATE.DataBindings.Add("Text", DataSource, "TODATE", "{0:dd/MM/yyyy}");
            NHANVIEN.DataBindings.Add("Text", DataSource, "NHANVIEN");
            TUYEN.DataBindings.Add("Text", DataSource, "TUYEN");
            LOAI1.DataBindings.Add("Text", DataSource, "NGUOITAO");
            MABARCODE.DataBindings.Add("Text", DataSource, "MABARCODE");
            TENNGUOINHAN.DataBindings.Add("Text", DataSource, "TENNGUOINHAN");
            DIENTHOAINHAN.DataBindings.Add("Text", DataSource, "DIENTHOAINHAN");
            HANGHOA.DataBindings.Add("Text", DataSource, "HANGHOA");
            SOLUONG.DataBindings.Add("Text", DataSource, "SOLUONG");
            GIAVE.DataBindings.Add("Text", DataSource, "GIAVE", "{0:n0}");
            NGAYTAO.DataBindings.Add("Text", DataSource, "NGAYTAO");
            NGUOITAO.DataBindings.Add("Text", DataSource, "NGUOITAO");
            TENTUYEN.DataBindings.Add("Text", DataSource, "TENTUYEN");
            TENNGUOIGUI.DataBindings.Add("Text", DataSource, "TENNGUOIGUI");
            DIENTHOAIGUI.DataBindings.Add("Text", DataSource, "DIENTHOAIGUI");

            GIAVE2.DataBindings.Add("Text", DataSource, "GIAVE");
            GIAVE2.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");

            GIAVE1.DataBindings.Add("Text", DataSource, "GIAVE");
            GIAVE1.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
        }
    }
}
