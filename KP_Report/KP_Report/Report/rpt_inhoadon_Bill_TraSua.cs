﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using System.Data;
using KP_Report;
using KP_UserManagement;
using System.IO;
using System.Windows.Forms;


namespace KP_Report
{
    /// <summary>
    /// rpt_inhoadon_Bill
    /// </summary>
    public partial class rpt_inhoadon_Bill_TraSua : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// checkBill
        /// </summary>
        public int checkBill=0;
        
        /// <summary>
        /// rpt_inhoadon_Bill
        /// </summary>
        public rpt_inhoadon_Bill_TraSua()
        {
            InitializeComponent();                      
            Setheader();                        
        }
        /// <summary>
        /// BindData
        /// </summary>
        public void BindData()
        {
            lb_NameBill.DataBindings.Add("Text", DataSource, "Ten_HoaDon");
            //if (lb_NameBill.Text == "")
            //{
            //    lb_NameBill.Text = "Hóa Đơn Bán Lẻ";
            //}            
            //cl_STT.DataBindings.Add("Text", DataSource, "STT");
            cl_HangHoa.DataBindings.Add("Text", DataSource, "TEN_HANGHOA");
            cl_giaban.DataBindings.Add("Text", DataSource, "GIABAN", "{0:#,###0.###}");
            cl_soluong.DataBindings.Add("Text", DataSource, "SOLUONG", "{0:#,###0.###}");
            CHIETKHAU.DataBindings.Add("Text", DataSource, "CHIETKHAU", "{0:#,###0.###}%");
            //cl_vat.DataBindings.Add("Text", DataSource, "HH_VAT", "VAT: {0} %");
            cl_thanhtien.DataBindings.Add("Text", DataSource, "Thanh_Tien", "{0:#,###0.###}");
            //STT.DataBindings.Add("Text", DataSource, "STT", "{0:#,###0}");
            Number.DataBindings.Add("Text", DataSource, "Number");

            lbTiendatcoc.DataBindings.Add("Text", DataSource, "Tiendatcoc", "{0:#,###0.###}");
            lbPhihucvu.DataBindings.Add("Text", DataSource, "Phiphucvu", "{0:#,###0.###}");
            lb_giamgia.DataBindings.Add("Text", DataSource, "Giam_Gia", "{0:#,###0.###}");
            lb_tongcong.DataBindings.Add("Text", DataSource, "Tong_Cong", "{0:#,###0.###}");
            lb_tienkhachtra.DataBindings.Add("Text", DataSource, "Tien_KhachTra", "{0:#,###0.###}");
            lb_conlai.DataBindings.Add("Text", DataSource, "Tien_TraKhach", "{0:#,###0.###}");

            //header
            lb_Quay.DataBindings.Add("Text", DataSource, "Ma_Quay");
            lb_mahoadon.DataBindings.Add("Text", DataSource, "Ma_HoaDon");
            lb_MaHD.DataBindings.Add("Text", DataSource, "So_HoaDon","*{0}*");
            lb_nhanvien.DataBindings.Add("Text", DataSource, "TenNV");
            lb_ngay.DataBindings.Add("Text", DataSource, "ngayHD", "{0:dd-MM-yyyy HH:mm:ss}");        
            
        } 

        private void Setheader()
        {
            StreamReader str = new StreamReader(Application.StartupPath + "/systeminfo.txt");
            string sMacuahang = "";
            string line;
            while ((line = str.ReadLine()) != null)
            {
                string[] value = line.Split(',');
                sMacuahang = value[0].ToString();
            }

            string sSQL = "";
            sSQL += "Select MA_CUAHANG,	TEN_CUAHANG,	GHICHU,	SUDUNG,	Header,	Footer" + "\n";
            sSQL += "From CUAHANG" + "\n";
            sSQL += "Where MA_CUAHANG  =" + clsMain.SQLString(sMacuahang) + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            lb_Header.Text = dt.Rows[0]["Header"].ToString();
            lbFooter.Text = dt.Rows[0]["Footer"].ToString();

            sSQL = "";
            sSQL += "SELECT HINHANH" + "\n";
            sSQL += "FROM sys_configreport" + "\n";
            dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
               
        }      
    }
}
