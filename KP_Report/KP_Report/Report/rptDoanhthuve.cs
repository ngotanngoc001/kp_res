﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptDoanhthuve : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptDoanhthuve()
        {
            InitializeComponent();
            GroupField grf = new GroupField("TENDIADIEM");
            GroupHeader4.GroupFields.Add(grf);

            GroupField grf1 = new GroupField("NGAYDI");
            GroupHeader3.GroupFields.Add(grf1);

            GroupField grf2 = new GroupField("GIODI");
            GroupHeader2.GroupFields.Add(grf2);

            GroupField grf3 = new GroupField("PHONG");
            GroupHeader1.GroupFields.Add(grf3);
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            byte[] tam = new byte[((byte[])dt.Rows[0]["HINHANH"]).LongLength];
            tam = (byte[])dt.Rows[0]["HINHANH"];
            System.IO.MemoryStream ms = new System.IO.MemoryStream(tam);
            Bitmap bm = new Bitmap(ms);
           // logo.Image = bm;
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        
        int i;
        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i = 0;
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            MABARCODE .DataBindings.Add("Text", DataSource, "MABARCODE");
            SOGHE.DataBindings.Add("Text", DataSource, "TENPHONG");
            NGAYDI.DataBindings.Add("Text", DataSource, "NGAYCHIEU", "{0:dd/MM/yyyy}");
            GIODI.DataBindings.Add("Text", DataSource, "BATDAU");
            TENDOITUONG.DataBindings.Add("Text", DataSource, "SOGHE");
            TENTUYEN.DataBindings.Add("Text", DataSource, "TENPHIM");
            TENKHUVUC.DataBindings.Add("Text", DataSource, "TENLOAIGHE");
            TENLOAIVE.DataBindings.Add("Text", DataSource, "TENLOAIVE");
            TENNHANVIEN.DataBindings.Add("Text", DataSource, "TENNHANVIEN");
            TENDIADIEM.DataBindings.Add("Text", DataSource, "TENDIADIEM");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:n0}");
            TONGTIEN.DataBindings.Add("Text", DataSource, "DONGIA");
            TONGTIEN.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
            TONGSOGHE.DataBindings.Add("Text", DataSource, "TONGSOGHE");
            TONGSOGHE.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Count, "{0:n0}");
            TONGTHANHTHIEN.DataBindings.Add("Text", DataSource, "DONGIA");
            TONGTHANHTHIEN.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            TONGSOGHE2.DataBindings.Add("Text", DataSource, "TONGSOGHE2");
            NGAYTAO.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy HH : mm}");

            DataTable dtSource = (DataTable)DataSource;
            txt_tieude.Text = "(" + dtSource.Rows[0]["TYPE"].ToString() + ": " + dtSource.Rows[0]["NAMETYPE"].ToString() + " )";
            txt_tieude1.Text = "( Từ ngày: " + dtSource.Rows[0]["TUNGAY"].ToString() + " - Đến ngày: " + dtSource.Rows[0]["DENNGAY"].ToString() + " )";

            TONGSOGHE1.Text = dtSource.Rows.Count.ToString();
            if (dtSource.Rows.Count > 0)
            {
                TILE.Text = ((double.Parse(dtSource.Rows.Count.ToString()) * 100) / double.Parse(dtSource.Rows[0]["TONGSOGHE2"].ToString())).ToString("F1") + "%";
            }
        }
    }
}
