﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBangketienmat : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBangketienmat()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        int i=0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            m500k.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            DataTable dt = (DataTable)DataSource;
            DateTime dti = (DateTime)dt.Rows[0]["ngay"];
            NGAY.Text = dti.Day.ToString("00") + "/" + dti.Month.ToString("00") + "/" + dti.Year.ToString("0000");
            nhanvien.Text = dt.Rows [0]["nhanvien"].ToString ().ToUpper ();
            noidung.Text = dt.Rows[0]["noidung"].ToString().ToUpper ();
            ghichu.Text = dt.Rows[0]["ghichu"].ToString().ToUpper();
            s500k.Text = dt.Rows[0]["s500k"].ToString().ToUpper();
            s200k.Text = dt.Rows[0]["s200k"].ToString().ToUpper();
            s100k.Text = dt.Rows[0]["s100k"].ToString().ToUpper();
            s50k.Text = dt.Rows[0]["s50k"].ToString().ToUpper();
            s20k.Text = dt.Rows[0]["s20k"].ToString().ToUpper();
            s10k.Text = dt.Rows[0]["s10k"].ToString().ToUpper();
            s5k.Text = dt.Rows[0]["s5k"].ToString().ToUpper();
            s2k.Text = dt.Rows[0]["s2k"].ToString().ToUpper();
            s1k.Text = dt.Rows[0]["s1k"].ToString().ToUpper();
            s5t.Text = dt.Rows[0]["s5t"].ToString().ToUpper();

            t500k.Text = (int.Parse(s500k.Text.Replace (",","").ToString()) * int.Parse(m500k.Text.Replace (",","").ToString())).ToString ("N0");
            t200k.Text = (int.Parse(s200k.Text.Replace (",","").ToString()) * int.Parse(m200k.Text.Replace (",","").ToString())).ToString ("N0");
            t100k.Text = (int.Parse(s100k.Text.Replace (",","").ToString()) * int.Parse(m100k.Text.Replace (",","").ToString())).ToString ("N0");
            t50k.Text = (int.Parse(s50k.Text.Replace (",","").ToString()) * int.Parse(m50k.Text.Replace (",","").ToString())).ToString ("N0");
            t20k.Text = (int.Parse(s20k.Text.Replace (",","").ToString()) * int.Parse(m20k.Text.Replace (",","").ToString())).ToString ("N0");
            t10k.Text = (int.Parse(s10k.Text.Replace (",","").ToString()) * int.Parse(m10k.Text.Replace (",","").ToString())).ToString ("N0");
            t5k.Text = (int.Parse(s5k.Text.Replace (",","").ToString()) * int.Parse(m5k.Text.Replace (",","").ToString())).ToString ("N0");
            t2k.Text = (int.Parse(s2k.Text.Replace (",","").ToString()) * int.Parse(m2k.Text.Replace (",","").ToString())).ToString ("N0");
            t1k.Text = (int.Parse(s1k.Text.Replace (",","").ToString()) * int.Parse(m1k.Text.Replace (",","").ToString())).ToString ("N0");
            t5t.Text = (int.Parse(s5t.Text.Replace(",", "").ToString()) * int.Parse(m5t.Text.Replace(",", "").ToString())).ToString("N0");

            tctm.Text = (int.Parse(t500k.Text.Replace (",","").ToString()) + int.Parse(t200k.Text.Replace (",","").ToString()) + int.Parse(t100k.Text.Replace (",","").ToString()) + int.Parse(t50k.Text.Replace (",","").ToString()) + int.Parse(t20k.Text.Replace (",","").ToString()) + int.Parse(t10k.Text.Replace (",","").ToString()) + int.Parse(t5k.Text.Replace (",","").ToString()) + int.Parse(t2k.Text.Replace (",","").ToString()) + int.Parse(t1k.Text.Replace (",","").ToString()) + int.Parse(t5t.Text.Replace (",","").ToString())).ToString("N0");
            tc.Text = tctm.Text;

            bangchu.Text = NumToString(double.Parse(tctm.Text.Replace(",", ""))).ToUpper();
        }
        /// <summary>
        /// NumToString
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string NumToString(double number)
        {
            string s = number.ToString("#");
            string[] so = new string[] { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
            string[] hang = new string[] { "", "nghìn", "triệu", "tỷ" };
            int i, j, donvi, chuc, tram;
            string str = " ";
            bool booAm = false;
            double decS = 0;
            //Tung addnew
            try
            {
                decS = Convert.ToDouble(s.ToString());
            }
            catch
            {
            }
            if (decS < 0)
            {
                decS = -decS;
                s = decS.ToString();
                booAm = true;
            }
            i = s.Length;
            if (i == 0)
                str = so[0] + str;
            else
            {
                j = 0;
                while (i > 0)
                {
                    donvi = Convert.ToInt32(s.Substring(i - 1, 1));
                    i--;
                    if (i > 0)
                        chuc = Convert.ToInt32(s.Substring(i - 1, 1));
                    else
                        chuc = -1;
                    i--;
                    if (i > 0)
                        tram = Convert.ToInt32(s.Substring(i - 1, 1));
                    else
                        tram = -1;
                    i--;
                    if ((donvi > 0) || (chuc > 0) || (tram > 0) || (j == 3))
                        str = hang[j] + str;
                    j++;
                    if (j > 3) j = 1;
                    if ((donvi == 1) && (chuc > 1))
                        str = "mốt " + str;
                    else
                    {
                        if ((donvi == 5) && (chuc > 0))
                            str = "lăm " + str;
                        else if (donvi > 0)
                            str = so[donvi] + " " + str;
                    }
                    if (chuc < 0)
                        break;
                    else
                    {
                        if ((chuc == 0) && (donvi > 0)) str = "lẻ " + str;
                        if (chuc == 1) str = "mười " + str;
                        if (chuc > 1) str = so[chuc] + " mươi " + str;
                    }
                    if (tram < 0) break;
                    else
                    {
                        if ((tram > 0) || (chuc > 0) || (donvi > 0)) str = so[tram] + " trăm " + str;
                    }
                    str = " " + str;
                }
            }
            if (booAm) str = "Âm " + str;
            return str + "đồng chẵn";
        }
    }
}
