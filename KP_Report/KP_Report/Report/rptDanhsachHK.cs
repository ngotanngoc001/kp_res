﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace KP_Report
{
    /// <summary>
    /// Report Danh sach hanh khach
    /// </summary>
    public partial class rptDanhsachHK : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report Danh sach hanh khach
        /// </summary>
        public rptDanhsachHK()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            SOGHE.DataBindings.Add("Text", DataSource, "SOGHE");
            TENHANHKHACH.DataBindings.Add("Text", DataSource, "TENHANHKHACH");
            QUOCTICH.DataBindings.Add("Text", DataSource, "QUOCTICH");
            CMND.DataBindings.Add("Text", DataSource, "CMND");
            NAMSINH.DataBindings.Add("Text", DataSource, "NAMSINH");
            NOISINH.DataBindings.Add("Text", DataSource, "NOISINH");
            DataTable dt = (DataTable)DataSource;
            try
            {
                TENDOITUONG.Text = "1 Tên tàu : " + dt.Rows[0]["TENDOITUONG"].ToString();
                BIENSO.Text = "1.3 Hô hiệu : " + dt.Rows[0]["SODANGKY"].ToString();
            }
            catch
            {
            }
        }
        int i=0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
        int j = 0;
        private void PageHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (j == 0)
            {
                j++;
                xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            }
            else
            {
                xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom ;
            }
        }
    }
}
