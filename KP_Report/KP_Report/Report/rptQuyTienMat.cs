﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using System.Drawing.Imaging;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace KP_Report
{
    /// <summary>
    /// rptQuyTienMat
    /// </summary>
    public partial class rptQuyTienMat : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// rptQuyTienMat
        /// </summary>
        public rptQuyTienMat()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// ConvertHinh
        /// </summary>
        /// <param name="pic"></param>
        /// <param name="values"></param>
        public static void ConvertHinh(ref PictureBox pic, Byte[] values)
        {
            MemoryStream ms = new MemoryStream(values);
            Bitmap bm = new Bitmap(ms);
            pic.Image = bm;
        }
        private void Setheader()
        {

            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        ///
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            DataTable dtSource = (DataTable)DataSource;
            NgayLap.DataBindings.Add("Text", DataSource, "NGAYLAPPHIEU","{0:dd/MM/yyyy}");

            DienGiai.DataBindings.Add("Text", DataSource, "DienGiai");
            KEMTHEO.DataBindings.Add("Text", DataSource, "KEMTHEO");

            Thu.DataBindings.Add("Text", DataSource, "SOTIENTHU","{0:0,0}");
            Chi.DataBindings.Add("Text", DataSource, "SOTIENCHI", "{0:0,0}");
            Ton.DataBindings.Add("Text", DataSource, "TONGTIEN", "{0:0,0}");
            MaPhieu.DataBindings.Add("Text", DataSource, "MAPCN");
            tonDauky.Text = string.Format("{0:0,0}", dtSource.Rows[0]["DauKy"]);

            //KemTheo.DataBindings.Add("Text", DataSource, "KemTheo");

            ngay1.Text = dtSource.Rows[0]["Ngay"].ToString();
            thang1.Text = dtSource.Rows[0]["Thang"].ToString();

            nam1.Text= dtSource.Rows[0]["Nam"].ToString();
            NguoiLap.Text = dtSource.Rows[0]["NguoiLap"].ToString();
            TongThu.Text = dtSource.Rows[0]["TongThu"].ToString();
            TongChi.Text = dtSource.Rows[0]["TongChi"].ToString();
            TongTon.Text = dtSource.Rows[0]["TongTon"].ToString();

            txt_tieude1.Text = dtSource.Rows[0]["HEARDER"].ToString();
        }
        int i = 0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
