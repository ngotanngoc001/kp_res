﻿namespace KP_Report
{
    partial class rptVerapphimIonah
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.BarCode.Code128Generator code128Generator1 = new DevExpress.XtraPrinting.BarCode.Code128Generator();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.SOGHE = new DevExpress.XtraReports.UI.XRLabel();
            this.KHUVUC = new DevExpress.XtraReports.UI.XRLabel();
            this.SOGHE1 = new DevExpress.XtraReports.UI.XRLabel();
            this.HANG1 = new DevExpress.XtraReports.UI.XRLabel();
            this.NGUOITAO1 = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYTAO1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYTAO = new DevExpress.XtraReports.UI.XRLabel();
            this.NGUOITAO = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYCHIEU1 = new DevExpress.XtraReports.UI.XRLabel();
            this.BATDAU1 = new DevExpress.XtraReports.UI.XRLabel();
            this.KHUVUC1 = new DevExpress.XtraReports.UI.XRLabel();
            this.BATDAU = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYCHIEU = new DevExpress.XtraReports.UI.XRLabel();
            this.HANG = new DevExpress.XtraReports.UI.XRLabel();
            this.MABARCODE1 = new DevExpress.XtraReports.UI.XRLabel();
            this.MABARCODE = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            this.MABARCODE2 = new DevExpress.XtraReports.UI.XRBarCode();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.MABARCODE2,
            this.SOGHE,
            this.KHUVUC,
            this.SOGHE1,
            this.HANG1,
            this.NGUOITAO1,
            this.NGAYTAO1,
            this.xrLabel43,
            this.NGAYTAO,
            this.NGUOITAO,
            this.NGAYCHIEU1,
            this.BATDAU1,
            this.KHUVUC1,
            this.BATDAU,
            this.NGAYCHIEU,
            this.HANG,
            this.MABARCODE1,
            this.MABARCODE});
            this.Detail.Dpi = 254F;
            this.Detail.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Detail.HeightF = 630F;
            this.Detail.LockedInUserDesigner = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.StylePriority.UseTextAlignment = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // SOGHE
            // 
            this.SOGHE.CanGrow = false;
            this.SOGHE.Dpi = 254F;
            this.SOGHE.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.SOGHE.LocationFloat = new DevExpress.Utils.PointFloat(397.5038F, 390F);
            this.SOGHE.Name = "SOGHE";
            this.SOGHE.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.SOGHE.SizeF = new System.Drawing.SizeF(80.91695F, 44.13257F);
            this.SOGHE.StylePriority.UseFont = false;
            this.SOGHE.StylePriority.UsePadding = false;
            this.SOGHE.StylePriority.UseTextAlignment = false;
            this.SOGHE.Text = "15";
            this.SOGHE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // KHUVUC
            // 
            this.KHUVUC.CanGrow = false;
            this.KHUVUC.Dpi = 254F;
            this.KHUVUC.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.KHUVUC.LocationFloat = new DevExpress.Utils.PointFloat(194.6292F, 442F);
            this.KHUVUC.Name = "KHUVUC";
            this.KHUVUC.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.KHUVUC.SizeF = new System.Drawing.SizeF(133.8336F, 44.13254F);
            this.KHUVUC.StylePriority.UseFont = false;
            this.KHUVUC.StylePriority.UsePadding = false;
            this.KHUVUC.StylePriority.UseTextAlignment = false;
            this.KHUVUC.Text = "GOLD";
            this.KHUVUC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // SOGHE1
            // 
            this.SOGHE1.CanGrow = false;
            this.SOGHE1.Dpi = 254F;
            this.SOGHE1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.SOGHE1.LocationFloat = new DevExpress.Utils.PointFloat(953.5654F, 498.3F);
            this.SOGHE1.Name = "SOGHE1";
            this.SOGHE1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.SOGHE1.SizeF = new System.Drawing.SizeF(120.3124F, 55F);
            this.SOGHE1.StylePriority.UseFont = false;
            this.SOGHE1.StylePriority.UsePadding = false;
            this.SOGHE1.StylePriority.UseTextAlignment = false;
            this.SOGHE1.Text = "15";
            this.SOGHE1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // HANG1
            // 
            this.HANG1.CanGrow = false;
            this.HANG1.Dpi = 254F;
            this.HANG1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.HANG1.LocationFloat = new DevExpress.Utils.PointFloat(701.4366F, 498.3003F);
            this.HANG1.Name = "HANG1";
            this.HANG1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.HANG1.SizeF = new System.Drawing.SizeF(120.3124F, 55F);
            this.HANG1.StylePriority.UseFont = false;
            this.HANG1.StylePriority.UsePadding = false;
            this.HANG1.StylePriority.UseTextAlignment = false;
            this.HANG1.Text = "A";
            this.HANG1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // NGUOITAO1
            // 
            this.NGUOITAO1.CanGrow = false;
            this.NGUOITAO1.Dpi = 254F;
            this.NGUOITAO1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.NGUOITAO1.LocationFloat = new DevExpress.Utils.PointFloat(618.2777F, 88.19585F);
            this.NGUOITAO1.Name = "NGUOITAO1";
            this.NGUOITAO1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGUOITAO1.SizeF = new System.Drawing.SizeF(225.5832F, 44.13269F);
            this.NGUOITAO1.StylePriority.UseFont = false;
            this.NGUOITAO1.StylePriority.UsePadding = false;
            this.NGUOITAO1.StylePriority.UseTextAlignment = false;
            this.NGUOITAO1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // NGAYTAO1
            // 
            this.NGAYTAO1.CanGrow = false;
            this.NGAYTAO1.Dpi = 254F;
            this.NGAYTAO1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.NGAYTAO1.LocationFloat = new DevExpress.Utils.PointFloat(953.5654F, 67.02917F);
            this.NGAYTAO1.Name = "NGAYTAO1";
            this.NGAYTAO1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYTAO1.SizeF = new System.Drawing.SizeF(245.892F, 44.13269F);
            this.NGAYTAO1.StylePriority.UseFont = false;
            this.NGAYTAO1.StylePriority.UsePadding = false;
            this.NGAYTAO1.StylePriority.UseTextAlignment = false;
            this.NGAYTAO1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel43
            // 
            this.xrLabel43.CanGrow = false;
            this.xrLabel43.Dpi = 254F;
            this.xrLabel43.Font = new System.Drawing.Font("Arial", 8F);
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(1228.732F, 125.2375F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(20F, 44.13269F);
            this.xrLabel43.StylePriority.UseFont = false;
            this.xrLabel43.StylePriority.UsePadding = false;
            this.xrLabel43.StylePriority.UseTextAlignment = false;
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // NGAYTAO
            // 
            this.NGAYTAO.CanGrow = false;
            this.NGAYTAO.Dpi = 254F;
            this.NGAYTAO.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.NGAYTAO.LocationFloat = new DevExpress.Utils.PointFloat(1093.627F, 188.7376F);
            this.NGAYTAO.Name = "NGAYTAO";
            this.NGAYTAO.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYTAO.SizeF = new System.Drawing.SizeF(256.4169F, 44.13272F);
            this.NGAYTAO.StylePriority.UseFont = false;
            this.NGAYTAO.StylePriority.UsePadding = false;
            this.NGAYTAO.StylePriority.UseTextAlignment = false;
            this.NGAYTAO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // NGUOITAO
            // 
            this.NGUOITAO.CanGrow = false;
            this.NGUOITAO.Dpi = 254F;
            this.NGUOITAO.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.NGUOITAO.LocationFloat = new DevExpress.Utils.PointFloat(630.2495F, 188.7375F);
            this.NGUOITAO.Name = "NGUOITAO";
            this.NGUOITAO.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGUOITAO.SizeF = new System.Drawing.SizeF(443.6283F, 44.13278F);
            this.NGUOITAO.StylePriority.UseFont = false;
            this.NGUOITAO.StylePriority.UsePadding = false;
            this.NGUOITAO.StylePriority.UseTextAlignment = false;
            this.NGUOITAO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // NGAYCHIEU1
            // 
            this.NGAYCHIEU1.CanGrow = false;
            this.NGAYCHIEU1.Dpi = 254F;
            this.NGAYCHIEU1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.NGAYCHIEU1.LocationFloat = new DevExpress.Utils.PointFloat(885F, 400F);
            this.NGAYCHIEU1.Name = "NGAYCHIEU1";
            this.NGAYCHIEU1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYCHIEU1.SizeF = new System.Drawing.SizeF(268.4791F, 55F);
            this.NGAYCHIEU1.StylePriority.UseFont = false;
            this.NGAYCHIEU1.StylePriority.UsePadding = false;
            this.NGAYCHIEU1.StylePriority.UseTextAlignment = false;
            this.NGAYCHIEU1.Text = "21/12/2012";
            this.NGAYCHIEU1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BATDAU1
            // 
            this.BATDAU1.CanGrow = false;
            this.BATDAU1.Dpi = 254F;
            this.BATDAU1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.BATDAU1.LocationFloat = new DevExpress.Utils.PointFloat(1215.044F, 400F);
            this.BATDAU1.Name = "BATDAU1";
            this.BATDAU1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.BATDAU1.SizeF = new System.Drawing.SizeF(158.8123F, 55F);
            this.BATDAU1.StylePriority.UseFont = false;
            this.BATDAU1.StylePriority.UsePadding = false;
            this.BATDAU1.StylePriority.UseTextAlignment = false;
            this.BATDAU1.Text = "20 : 30";
            this.BATDAU1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // KHUVUC1
            // 
            this.KHUVUC1.CanGrow = false;
            this.KHUVUC1.Dpi = 254F;
            this.KHUVUC1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.KHUVUC1.LocationFloat = new DevExpress.Utils.PointFloat(1176.812F, 498.3003F);
            this.KHUVUC1.Name = "KHUVUC1";
            this.KHUVUC1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.KHUVUC1.SizeF = new System.Drawing.SizeF(159.3755F, 55.00003F);
            this.KHUVUC1.StylePriority.UseFont = false;
            this.KHUVUC1.StylePriority.UsePadding = false;
            this.KHUVUC1.StylePriority.UseTextAlignment = false;
            this.KHUVUC1.Text = "GOLD";
            this.KHUVUC1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BATDAU
            // 
            this.BATDAU.CanGrow = false;
            this.BATDAU.Dpi = 254F;
            this.BATDAU.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Bold);
            this.BATDAU.LocationFloat = new DevExpress.Utils.PointFloat(343.4832F, 340F);
            this.BATDAU.Name = "BATDAU";
            this.BATDAU.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.BATDAU.SizeF = new System.Drawing.SizeF(134.9376F, 44.13257F);
            this.BATDAU.StylePriority.UseFont = false;
            this.BATDAU.StylePriority.UsePadding = false;
            this.BATDAU.StylePriority.UseTextAlignment = false;
            this.BATDAU.Text = "20 : 30";
            this.BATDAU.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // NGAYCHIEU
            // 
            this.NGAYCHIEU.CanGrow = false;
            this.NGAYCHIEU.Dpi = 254F;
            this.NGAYCHIEU.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Bold);
            this.NGAYCHIEU.LocationFloat = new DevExpress.Utils.PointFloat(144.3584F, 340F);
            this.NGAYCHIEU.Name = "NGAYCHIEU";
            this.NGAYCHIEU.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYCHIEU.SizeF = new System.Drawing.SizeF(199.1249F, 44.13251F);
            this.NGAYCHIEU.StylePriority.UseFont = false;
            this.NGAYCHIEU.StylePriority.UsePadding = false;
            this.NGAYCHIEU.StylePriority.UseTextAlignment = false;
            this.NGAYCHIEU.Text = "21/12/2012";
            this.NGAYCHIEU.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // HANG
            // 
            this.HANG.CanGrow = false;
            this.HANG.Dpi = 254F;
            this.HANG.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.HANG.LocationFloat = new DevExpress.Utils.PointFloat(205.2126F, 390F);
            this.HANG.Name = "HANG";
            this.HANG.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.HANG.SizeF = new System.Drawing.SizeF(49.16696F, 44.13257F);
            this.HANG.StylePriority.UseFont = false;
            this.HANG.StylePriority.UsePadding = false;
            this.HANG.StylePriority.UseTextAlignment = false;
            this.HANG.Text = "A";
            this.HANG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // MABARCODE1
            // 
            this.MABARCODE1.CanGrow = false;
            this.MABARCODE1.Dpi = 254F;
            this.MABARCODE1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.MABARCODE1.LocationFloat = new DevExpress.Utils.PointFloat(1350.044F, 188.7376F);
            this.MABARCODE1.Name = "MABARCODE1";
            this.MABARCODE1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.MABARCODE1.SizeF = new System.Drawing.SizeF(170.3328F, 44.13272F);
            this.MABARCODE1.StylePriority.UseFont = false;
            this.MABARCODE1.StylePriority.UsePadding = false;
            this.MABARCODE1.StylePriority.UseTextAlignment = false;
            this.MABARCODE1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // MABARCODE
            // 
            this.MABARCODE.CanGrow = false;
            this.MABARCODE.Dpi = 254F;
            this.MABARCODE.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.MABARCODE.LocationFloat = new DevExpress.Utils.PointFloat(84.66666F, 67.02917F);
            this.MABARCODE.Name = "MABARCODE";
            this.MABARCODE.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.MABARCODE.SizeF = new System.Drawing.SizeF(447.6627F, 37.99994F);
            this.MABARCODE.StylePriority.UseFont = false;
            this.MABARCODE.StylePriority.UsePadding = false;
            this.MABARCODE.StylePriority.UseTextAlignment = false;
            this.MABARCODE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 5F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 10.00004F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // formattingRule1
            // 
            this.formattingRule1.Name = "formattingRule1";
            // 
            // MABARCODE2
            // 
            this.MABARCODE2.AutoModule = true;
            this.MABARCODE2.Dpi = 254F;
            this.MABARCODE2.Font = new System.Drawing.Font("Tahoma", 6F);
            this.MABARCODE2.LocationFloat = new DevExpress.Utils.PointFloat(110.755F, 498.3F);
            this.MABARCODE2.Module = 5.08F;
            this.MABARCODE2.Name = "MABARCODE2";
            this.MABARCODE2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.MABARCODE2.SizeF = new System.Drawing.SizeF(317.3951F, 100.4109F);
            this.MABARCODE2.StylePriority.UseFont = false;
            this.MABARCODE2.StylePriority.UsePadding = false;
            this.MABARCODE2.StylePriority.UseTextAlignment = false;
            this.MABARCODE2.Symbology = code128Generator1;
            this.MABARCODE2.Text = "*1234567*";
            this.MABARCODE2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // rptVerapphimIonah
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Dpi = 254F;
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(5, 5, 5, 10);
            this.PageHeight = 630;
            this.PageWidth = 1550;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.ShowPrintMarginsWarning = false;
            this.ShowPrintStatusDialog = false;
            this.SnapGridSize = 2F;
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel MABARCODE;
        private DevExpress.XtraReports.UI.XRLabel MABARCODE1;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        private DevExpress.XtraReports.UI.XRLabel NGAYCHIEU1;
        private DevExpress.XtraReports.UI.XRLabel BATDAU1;
        private DevExpress.XtraReports.UI.XRLabel BATDAU;
        private DevExpress.XtraReports.UI.XRLabel NGAYCHIEU;
        private DevExpress.XtraReports.UI.XRLabel NGUOITAO;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel NGAYTAO;
        private DevExpress.XtraReports.UI.XRLabel NGUOITAO1;
        private DevExpress.XtraReports.UI.XRLabel NGAYTAO1;
        private DevExpress.XtraReports.UI.XRLabel SOGHE1;
        private DevExpress.XtraReports.UI.XRLabel HANG1;
        private DevExpress.XtraReports.UI.XRLabel KHUVUC1;
        private DevExpress.XtraReports.UI.XRLabel HANG;
        private DevExpress.XtraReports.UI.XRLabel SOGHE;
        private DevExpress.XtraReports.UI.XRLabel KHUVUC;
        private DevExpress.XtraReports.UI.XRBarCode MABARCODE2;
    }
}
