﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using System.Drawing.Imaging;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace KP_Report
{
    public partial class rptLapPhieuChi : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// rptLapPhieuChi
        /// </summary>
        public rptLapPhieuChi()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// rptLapPhieuChi
        /// </summary>
        /// <param name="pic"></param>
        /// <param name="values"></param>
        public static void ConvertHinh(ref PictureBox pic, Byte[] values)
        {
            MemoryStream ms = new MemoryStream(values);
            Bitmap bm = new Bitmap(ms);
            pic.Image = bm;
        }
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,EMAIL,SOTAIKHOAN,MASOTHUE,GIAMDOC,KETOAN,THUKHO" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
            txt_ketoantruong.Text = dt.Rows[0]["KETOAN"].ToString();
            txt_giamdoc.Text = dt.Rows[0]["GIAMDOC"].ToString();
            // txt_footerreport.Text = dt.Rows[0]["FOOTERREPORT"].ToString();
            //copy copy
            txtTencongty1.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi1.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai1.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
            txt_ketoantruong1.Text = dt.Rows[0]["KETOAN"].ToString();
            txt_giamdoc1.Text = dt.Rows[0]["GIAMDOC"].ToString();
        }
        ///
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            //DataTable dtSource = (DataTable)DataSource;
            SoPT.DataBindings.Add("Text", DataSource, "SoPhieu");
            Ngay.DataBindings.Add("Text", DataSource, "Ngay");
            Thang.DataBindings.Add("Text", DataSource, "Thang");

            Nam.DataBindings.Add("Text", DataSource, "Nam");

            HoTen.DataBindings.Add("Text", DataSource, "HoTen");
            DiaChi.DataBindings.Add("Text", DataSource, "DiaChi");
            LyDo.DataBindings.Add("Text", DataSource, "LyDo");
            SoTien.DataBindings.Add("Text", DataSource, "SoTien");
            VietBangChu.DataBindings.Add("Text", DataSource, "VietBangChu");

            KemTheo.DataBindings.Add("Text", DataSource, "KemTheo");

            ngay1.DataBindings.Add("Text", DataSource, "Ngay");
            thang1.DataBindings.Add("Text", DataSource, "Thang");

            nam1.DataBindings.Add("Text", DataSource, "Nam");
            NguoiLap.DataBindings.Add("Text", DataSource, "NguoiLap");

            //copy copy

            SoPT1.DataBindings.Add("Text", DataSource, "SoPhieu");
            Ngay0.DataBindings.Add("Text", DataSource, "Ngay");
            Thang0.DataBindings.Add("Text", DataSource, "Thang");

            Nam0.DataBindings.Add("Text", DataSource, "Nam");

            HoTen1.DataBindings.Add("Text", DataSource, "HoTen");
            DiaChi1.DataBindings.Add("Text", DataSource, "DiaChi");
            LyDo1.DataBindings.Add("Text", DataSource, "LyDo");
            SoTien1.DataBindings.Add("Text", DataSource, "SoTien");
            VietBangChu1.DataBindings.Add("Text", DataSource, "VietBangChu");

            KemTheo1.DataBindings.Add("Text", DataSource, "KemTheo");

            ngay10.DataBindings.Add("Text", DataSource, "Ngay");
            thang10.DataBindings.Add("Text", DataSource, "Thang");

            nam10.DataBindings.Add("Text", DataSource, "Nam");
            NguoiLap1.DataBindings.Add("Text", DataSource, "NguoiLap");
        }

    }
}
