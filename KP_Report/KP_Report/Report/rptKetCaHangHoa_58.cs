﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using System.Data;
using KP_Report;
using KP_UserManagement;
using System.IO;
using System.Windows.Forms;


namespace KP_Report
{
    /// <summary>
    /// rpt_inhoadon_Bill
    /// </summary>
    public partial class rptKetCaHangHoa_58 : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// checkBill
        /// </summary>
        public int checkBill=0;
        
        /// <summary>
        /// rpt_inhoadon_Bill
        /// </summary>
        public rptKetCaHangHoa_58()
        {
            InitializeComponent();                      
            Setheader();                        
        }
        /// <summary>
        /// BindData
        /// </summary>
        public void BindData()
        {
            lb_MaHD.DataBindings.Add("Text", DataSource, "So_HoaDon", "*{0}*");
            lb_nhanvien.DataBindings.Add("Text", DataSource, "TenNV");
            lb_ngay.DataBindings.Add("Text", DataSource, "ngayHD", "{0:dd-MM-yyyy HH:mm:ss}");    

            cl_HangHoa.DataBindings.Add("Text", DataSource, "TEN_HANGHOA");
            cl_giaban.DataBindings.Add("Text", DataSource, "GIABAN", "{0:#,###0}");
            cl_soluong.DataBindings.Add("Text", DataSource, "SOLUONG");
            CHIETKHAU.DataBindings.Add("Text", DataSource, "CHIETKHAU", "{0:#,###0}%");
            cl_thanhtien.DataBindings.Add("Text", DataSource, "Thanh_Tien", "{0:#,###0}");

            TongSL.DataBindings.Add("Text", DataSource, "SOLUONG");
            TongSL.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.DSum, "");
            TongThanhTien.DataBindings.Add("Text", DataSource, "Thanh_Tien");
            TongThanhTien.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");
        } 

        private void Setheader()
        {
            StreamReader str = new StreamReader(Application.StartupPath + "/systeminfo.txt");
            string sMacuahang = "";
            string line;
            while ((line = str.ReadLine()) != null)
            {
                string[] value = line.Split(',');
                sMacuahang = value[0].ToString();
            }

            string sSQL = "";
            sSQL += "Select MA_CUAHANG,	TEN_CUAHANG,	GHICHU,	SUDUNG,	Header,	Footer" + "\n";
            sSQL += "From CUAHANG" + "\n";
            sSQL += "Where MA_CUAHANG  =" + clsMain.SQLString(sMacuahang) + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            lb_Header.Text = dt.Rows[0]["Header"].ToString(); 
        }      
    }
}
