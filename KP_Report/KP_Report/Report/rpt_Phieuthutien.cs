﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using System.Data;
using KP_Report;
using KP_UserManagement;
using System.IO;
using System.Windows.Forms;


namespace KP_Report
{
    /// <summary>
    /// rpt_Phieuthutien
    /// </summary>
    public partial class rpt_Phieuthutien : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// checkBill
        /// </summary>
        public int checkBill=0;
        
        /// <summary>
        /// rpt_Phieuthutien
        /// </summary>
        public rpt_Phieuthutien()
        {
            InitializeComponent();                      
            Setheader();                        
        }
        /// <summary>
        /// BindData
        /// </summary>
        public void BindData()
        {
            lbPhong.DataBindings.Add("Text", DataSource, "Ten_Phong");
            lb_nv.DataBindings.Add("Text", DataSource, "Ten_nv");  
            lbLannhancoc.DataBindings.Add("Text", DataSource, "STT", "{0:#,###0}");            
            lb_sotien.DataBindings.Add("Text", DataSource, "SOTIEN", "{0:#,###0}");
            lbNgay.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd-MM-yyyy HH:mm}");
            lbGhichu.DataBindings.Add("Text", DataSource, "GHICHU");  
            
        } 

        private void Setheader()
        {
            StreamReader str = new StreamReader(Application.StartupPath + "/systeminfo.txt");
            string sMacuahang = "";
            string line;
            while ((line = str.ReadLine()) != null)
            {
                string[] value = line.Split(',');
                sMacuahang = value[0].ToString();
            }

            string sSQL = "";
            sSQL += "Select MA_CUAHANG,	TEN_CUAHANG,	GHICHU,	SUDUNG,	Header,	Footer" + "\n";
            sSQL += "From CUAHANG" + "\n";
            sSQL += "Where MA_CUAHANG  =" + clsMain.SQLString(sMacuahang) + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            lb_Header.Text = dt.Rows[0]["Header"].ToString();
            lbFooter.Text = dt.Rows[0]["Footer"].ToString();
               
        }      
    }
}
