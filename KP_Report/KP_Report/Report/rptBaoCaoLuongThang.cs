﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBaoCaoLuongThang : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBaoCaoLuongThang()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            TENNHANVIEN.DataBindings.Add("Text", DataSource, "TENNHANVIEN");
            LUONGCOBAN.DataBindings.Add("Text", DataSource, "LUONGCOBAN", "{0:#,###0}");
            LUONGTHUCTE.DataBindings.Add("Text", DataSource, "LUONGTHUCTE", "{0:#,###0}");
            NGAYCONG_TT.DataBindings.Add("Text", DataSource, "NGAYCONG_TT", "{0:#,###0}");
            PHUCAP.DataBindings.Add("Text", DataSource, "PHUCAP", "{0:#,###0}");
            TONGLUONG_TT.DataBindings.Add("Text", DataSource, "TONGLUONG_TT", "{0:#,###0}");
            BH_XAHOI.DataBindings.Add("Text", DataSource, "BH_XAHOI", "{0:#,###0}");
            BH_YTE.DataBindings.Add("Text", DataSource, "BH_YTE", "{0:#,###0}");
            BH_THATNGHIEP.DataBindings.Add("Text", DataSource, "BH_THATNGHIEP", "{0:#,###0}");
            TONGBH.DataBindings.Add("Text", DataSource, "TONGBH", "{0:#,###0}");
            TAMUNG.DataBindings.Add("Text", DataSource, "TAMUNG", "{0:#,###0}");
            THUCLINH.DataBindings.Add("Text", DataSource, "THUCLINH", "{0:#,###0}");
            
            DataTable dtSource = (DataTable)DataSource;
            if (dtSource.Rows[0]["HEARDER"].ToString() != "")
            {
                txt_tieude.Text = dtSource.Rows[0]["HEARDER"].ToString();
            }
        }

        int i = 0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
