﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using System.Drawing.Imaging;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBanvetrochoi_nhahang : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBanvetrochoi_nhahang()
        {
            InitializeComponent();
            Setheader();
            GroupField grf = new GroupField("LOAI");
            GroupHeader1.GroupFields.Add(grf);
        }
        private void ConvertHinh(ref PictureBox pic, Byte[] values)
        {
            MemoryStream ms = new MemoryStream(values);
            Bitmap bm = new Bitmap(ms);
            pic.Image = bm;
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            LOAI.DataBindings.Add("Text", DataSource, "LOAI");
            TENHANGHOA.DataBindings.Add("Text", DataSource, "TEN_HANGHOA");
            TEN_DONVITINH.DataBindings.Add("Text", DataSource, "TEN_DONVITINH");
            SOLUONG.DataBindings.Add("Text", DataSource, "SOLUONG", "{0:#,###0}");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:#,###0.00}");
            THANHTIEN.DataBindings.Add("Text", DataSource, "THANHTIEN", "{0:#,###0.00}");
            TIENPHUTHU.DataBindings.Add("Text", DataSource, "TIENPHUTHU", "{0:#,###0.00}");
            TIENCHIETKHAU.DataBindings.Add("Text", DataSource, "TIENCHIETKHAU", "{0:#,###0.00}");
            TONGCONG.DataBindings.Add("Text", DataSource, "TONGCONG", "{0:#,###0.00}");
            TONGCONG1.DataBindings.Add("Text", DataSource, "TONGCONG");
            TONGCONG1.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:#,###0.00}");
            TONGCONG2.DataBindings.Add("Text", DataSource, "TONGCONG");
            TONGCONG2.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0.00}");

            DataTable dtSource = (DataTable)DataSource;
            try
            {
                txt_tieude.Text = "( " + dtSource.Rows[0]["FILTER"].ToString() + " )";
                txt_tieude1.Text = "( Từ ngày: " + dtSource.Rows[0]["TUNGAY"].ToString() + " - Đến ngày: " + dtSource.Rows[0]["DENNGAY"].ToString() + " )";
            }
            catch
            {
            }

        }
        int i = 0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
