﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using System.Drawing.Imaging;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace KP_Report
{
    /// <summary>
    /// rptBaoCaoPhieuChi
    /// </summary>
    public partial class rptBaoCaoUyNhiemChi : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// rptBaoCaoPhieuChi
        /// </summary>
        public rptBaoCaoUyNhiemChi()
        {
            InitializeComponent();
            Setheader();
        }

        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        ///
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            MA.DataBindings.Add("Text", DataSource, "MA");
            NGAY.DataBindings.Add("Text", DataSource, "NGAY", "{0:dd/MM/yyyy}");
            DONVI_CHUYEN.DataBindings.Add("Text", DataSource, "DONVI_CHUYEN");
            TAIKHOAN_CHUYEN.DataBindings.Add("Text", DataSource, "TAIKHOAN_CHUYEN");
            DONVI_NHAN.DataBindings.Add("Text", DataSource, "DONVI_NHAN");
            TAIKHOAN_NHAN.DataBindings.Add("Text", DataSource, "TAIKHOAN_NHAN");
            NGANHANG.DataBindings.Add("Text", DataSource, "NGANHANG");
            NOIDUNG.DataBindings.Add("Text", DataSource, "NOIDUNG");
            TIEN_SO.DataBindings.Add("Text", DataSource, "TIEN_SO", "{0:#,###0}");

            TONGTIEN_SO.DataBindings.Add("Text", DataSource, "TIEN_SO");
            TONGTIEN_SO.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");

            DataTable dtSource = (DataTable)DataSource;

            txt_tieude1.Text = dtSource.Rows[0]["HEARDER"].ToString();
        }

        int i = 0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
