﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBCInLai_DS_HK_TungNam : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBCInLai_DS_HK_TungNam()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        int i=0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            TENTUYEN1.DataBindings.Add("Text", DataSource, "TUYEN");
            //TENDOITUONG1.DataBindings.Add("Text", DataSource, "TENDOITUONG");
            TENTUYEN.DataBindings.Add("Text", DataSource, "TENTUYEN");
            GIODI.DataBindings.Add("Text", DataSource, "GIODI");
            NGAYDI1.DataBindings.Add("Text", DataSource, "NGAY");
            NGAYDI.DataBindings.Add("Text", DataSource, "NGAYDI", "{0:dd/MM/yyyy}");
           // GIODI1.DataBindings.Add("Text", DataSource, "GIODI");
            MABARCODE.DataBindings.Add("Text", DataSource, "MABARCODE");
            SOGHE.DataBindings.Add("Text", DataSource, "SOGHE");
            TENHANHKHACH.DataBindings.Add("Text", DataSource, "TENHANHKHACH");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:n0}");
            QUOCTICH.DataBindings.Add("Text", DataSource, "QUOCTICH");
            CMND.DataBindings.Add("Text", DataSource, "CMND");
            NAMSINH.DataBindings.Add("Text", DataSource, "NAMSINH");
            NOISINH.DataBindings.Add("Text", DataSource, "NOISINH");
            DATE.DataBindings.Add("Text", DataSource, "ngaytao", "{0:dd/MM/yyyy HH:mm}");
            NHANVIEN.DataBindings.Add("Text", DataSource, "nguoitao");
            TONGVE.DataBindings.Add("Text", DataSource, "MABARCODE");
            TONGVE.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Count, "{0:n0}");
            TONGTIEN.DataBindings.Add("Text", DataSource, "DONGIA");
            TONGTIEN.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
        }
    }
}
