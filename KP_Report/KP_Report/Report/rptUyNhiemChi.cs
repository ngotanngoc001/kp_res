﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using System.Drawing.Imaging;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace KP_Report
{
    public partial class rptUyNhiemChi : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// rptLapPhieuChi_A5
        /// </summary>
        public rptUyNhiemChi()
        {
            InitializeComponent();
        }
        
        
        ///
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            DataTable dtSource = (DataTable)DataSource;
            NGAY.DataBindings.Add("Text", DataSource, "NGAY");
            DONVI_CHUYEN.DataBindings.Add("Text", DataSource, "DONVI_CHUYEN");
            TAIKHOAN_CHUYEN.DataBindings.Add("Text", DataSource, "TAIKHOAN_CHUYEN");
            CHINHANH_CHUYEN.DataBindings.Add("Text", DataSource, "CHINHANH_CHUYEN");
            DONVI_NHAN.DataBindings.Add("Text", DataSource, "DONVI_NHAN");
            TAIKHOAN_NHAN.DataBindings.Add("Text", DataSource, "TAIKHOAN_NHAN");
            CMND.DataBindings.Add("Text", DataSource, "CMND");
            if (dtSource.Rows[0]["CMND"].ToString() != "")
                NGAYCAP.Text = dtSource.Rows[0]["NGAYCAP"].ToString();
            NOICAP.DataBindings.Add("Text", DataSource, "NOICAP");
            NGANHANG.DataBindings.Add("Text", DataSource, "NGANHANG");
            TINH_TP.DataBindings.Add("Text", DataSource, "TINH_TP");
            TIEN_CHU.DataBindings.Add("Text", DataSource, "TIEN_CHU");
            TIEN_SO.DataBindings.Add("Text", DataSource, "TIEN_SO");
            NOIDUNG.DataBindings.Add("Text", DataSource, "NOIDUNG");
        }

    }
}
