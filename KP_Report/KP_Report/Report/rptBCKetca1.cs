﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBCKetca1 : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBCKetca1()
        {
            InitializeComponent();
            Setheader();
            GroupField grf2 = new GroupField("Nhanvien");
            GroupHeader3.GroupFields.Add(grf2);
            GroupField grf1 = new GroupField("MALICHTRINH");
            GroupHeader2.GroupFields.Add(grf1);
            GroupField grf = new GroupField("LOAI");
            GroupHeader1.GroupFields.Add(grf);
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        int i=0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            //TENTUYEN1.DataBindings.Add("Text", DataSource, "TENTUYEN");
            //TENDOITUONG1.DataBindings.Add("Text", DataSource, "TENDOITUONG");
            NGAYDI1.DataBindings.Add("Text", DataSource, "NGAYBAOCAO");
            //GIODI1.DataBindings.Add("Text", DataSource, "GIODI");

            tuyenn.DataBindings.Add("Text", DataSource, "TENTUYEN");
            taun.DataBindings.Add("Text", DataSource, "TENDOITUONG");
            ngayn.DataBindings.Add("Text", DataSource, "NGAYDI", "{0:dd/MM/yyyy}");
            gion.DataBindings.Add("Text", DataSource, "GIODI");


            Nhanvien.DataBindings.Add("Text", DataSource, "NHANVIENCOI");
            LOAI.DataBindings.Add("Text", DataSource, "LOAI");
            TENKHUVUC.DataBindings.Add("Text", DataSource, "TENKHUVUC");
            SOLUONG.DataBindings.Add("Text", DataSource, "SOLUONG");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:n0}");
            PHUTHU.DataBindings.Add("Text", DataSource, "PHUTHU", "{0:n0}");
            THANHTIEN.DataBindings.Add("Text", DataSource, "THANHTIEN", "{0:n0}");

            sotien_giam.DataBindings.Add("Text", DataSource, "CT_GIAMTRU", "{0:n0}");
            sotien_congno.DataBindings.Add("Text", DataSource, "CT_CONGNO", "{0:n0}");
            sotien_datcoc.DataBindings.Add("Text", DataSource, "CT_DATCOC", "{0:n0}");
            sotien_chuyenkhoan.DataBindings.Add("Text", DataSource, "CT_CUYENKHOAN", "{0:n0}");
            DIENGIAI.DataBindings.Add("Text", DataSource, "DIENGIAI");

            NHANVIENGR1.DataBindings.Add("Text", DataSource, "Nhanvien");
            NHANVIENGR2.DataBindings.Add("Text", DataSource, "Nhanvien");
           
            TONGSOLUONG2.DataBindings.Add("Text", DataSource, "SOLUONG");
            TONGSOLUONG2.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            TONGTHANHTIEN2.DataBindings.Add("Text", DataSource, "THANHTIEN");
            TONGTHANHTIEN2.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");

           // TONGTHANHTIEN12.DataBindings.Add("Text", DataSource, "THANHTIEN");
           // TONGTHANHTIEN12.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");

           // TONGTHANHTIEN13.DataBindings.Add("Text", DataSource, "THANHTIEN");
           // TONGTHANHTIEN13.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");

          //  txtDatcoc12.DataBindings.Add("Text", DataSource, "DATCOC");
          //  txtDatcoc12.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");

          //  txtDatcoc13.DataBindings.Add("Text", DataSource, "DATCOC");
          //  txtDatcoc13.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");

            //GIAMTRU1.DataBindings.Add("Text", DataSource, "GIAMTRU");
            //GIAMTRU1.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");

          //  GIAMTRU3.DataBindings.Add("Text", DataSource, "GIAMTRU");
          //  GIAMTRU3.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");

        

           

           // txtDatcoc.DataBindings.Add("Text", DataSource, "DATCOC");
           // txtDatcoc.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");

          //  txtGiamtru.DataBindings.Add("Text", DataSource, "GIAMTRU");
          //  txtGiamtru.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");

            TONGTHANHTIEN1.DataBindings.Add("Text", DataSource, "THANHTIEN");
            TONGTHANHTIEN1.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");

            GIAMTRU1.DataBindings.Add("Text", DataSource, "CT_GIAMTRU");
            GIAMTRU1.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");

            CONGNO1.DataBindings.Add("Text", DataSource, "CT_CONGNO");
            CONGNO1.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");

            DATCOC1.DataBindings.Add("Text", DataSource, "CT_DATCOC");
            DATCOC1.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");

            CHUYENKHOAN1.DataBindings.Add("Text", DataSource, "CT_CUYENKHOAN");
            CHUYENKHOAN1.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");


            TONGTHANHTIEN12.DataBindings.Add("Text", DataSource, "THANHTIEN");
            TONGTHANHTIEN12.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");

            GIAMTRU12.DataBindings.Add("Text", DataSource, "CT_GIAMTRU");
            GIAMTRU12.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");

            CONGNO12.DataBindings.Add("Text", DataSource, "CT_CONGNO");
            CONGNO12.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");

            DATCOC12.DataBindings.Add("Text", DataSource, "CT_DATCOC");
            DATCOC12.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");

            CHUYENKHOAN12.DataBindings.Add("Text", DataSource, "CT_CUYENKHOAN");
            CHUYENKHOAN12.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");



            TONGTHANHTIEN13.DataBindings.Add("Text", DataSource, "THANHTIEN");
            TONGTHANHTIEN13.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");

            GIAMTRU13.DataBindings.Add("Text", DataSource, "CT_GIAMTRU");
            GIAMTRU13.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");

            CONGNO13.DataBindings.Add("Text", DataSource, "CT_CONGNO");
            CONGNO13.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");

            DATCOC13.DataBindings.Add("Text", DataSource, "CT_DATCOC");
            DATCOC13.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");

            CHUYENKHOAN13.DataBindings.Add("Text", DataSource, "CT_CUYENKHOAN");
            CHUYENKHOAN13.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");


        }

        

        
    }
}
