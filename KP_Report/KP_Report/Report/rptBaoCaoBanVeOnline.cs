﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBaoCaoBanVeOnline : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBaoCaoBanVeOnline()
        {
            InitializeComponent();
            GroupField grf1 = new GroupField("MABARCODE");
            GroupHeader1.GroupFields.Add(grf1);
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }

        int i;
        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i = 0;
        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            MABARCODE.DataBindings.Add("Text", DataSource, "MABARCODE");
            TENHANHKHACH.DataBindings.Add("Text", DataSource, "TENHANHKHACH");
            DIENTHOAI.DataBindings.Add("Text", DataSource, "DIENTHOAI");
            EMAIL.DataBindings.Add("Text", DataSource, "EMAIL");
            TENPHIM.DataBindings.Add("Text", DataSource, "TENPHIM");
            MAVE.DataBindings.Add("Text", DataSource, "MAVE");
            NGAYCHIEU.DataBindings.Add("Text", DataSource, "NGAYCHIEU", "{0:dd/MM/yyyy}");
            BATDAU.DataBindings.Add("Text", DataSource, "BATDAU");
            SOGHE.DataBindings.Add("Text", DataSource, "SOGHE");
            TENPHONG.DataBindings.Add("Text", DataSource, "TENPHONG");
            TENRAP.DataBindings.Add("Text", DataSource, "TENRAP");
            NGUOITAO.DataBindings.Add("Text", DataSource, "NGUOITAO");
            NGAYTAO.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy HH : mm}");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:n0}");

            sumdongia.DataBindings.Add("Text", DataSource, "DONGIA");
            sumdongia.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:#,###0}");

            tongcong.DataBindings.Add("Text", DataSource, "DONGIA");
            tongcong.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");
            
            
            DataTable dtSource = (DataTable)DataSource;
            txt_tieude.Text = dtSource.Rows[0]["HEARDER"].ToString();
            txt_tieude1.Text = "( Từ ngày: " + dtSource.Rows[0]["TUNGAY"].ToString() + " - Đến ngày: " + dtSource.Rows[0]["DENNGAY"].ToString() + " )";

            
        }
    }
}
