﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using System.Data;
using KP_Report;
using KP_UserManagement;
using System.IO;
using System.Windows.Forms;


namespace KP_Report
{
    public partial class rpt_Order : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// rpt_Order
        /// </summary>
        public rpt_Order()
        {
            InitializeComponent();                      
            Setheader();            
        }
        /// <summary>
        /// BindData
        /// </summary>
        public void BindData(string ngonngu)
        {
            if (ngonngu == "en-US")
            {
                lb_NameBill.Text = "ORDER KITCHEN";
                xrLabel6.Text = "Table:";
                xrLabel5.Text = "Staff:";
                xrLabel15.Text = "Counter:";
                xrLabel4.Text = "Date:";
                xrLabel3.Text = "Receipt:";
                xrTableCell1.Text = "Goods";
                xrTableCell2.Text = "Qty";
                xrTableCell3.Text = "Request More";
                xrLabel1.Text = "Customer:";
            }
            else
            {
                lb_NameBill.DataBindings.Add("Text", DataSource, "Ten_HoaDon");
            }
            //  cl_STT.DataBindings.Add("Text", DataSource, "STT");
            cl_HangHoa.DataBindings.Add("Text", DataSource, "TEN_HANGHOA");
            cl_soluong.DataBindings.Add("Text", DataSource, "SOLUONG");
            cl_GhiChu.DataBindings.Add("Text", DataSource, "GHICHU");
            //header
            lb_Quay.DataBindings.Add("Text", DataSource, "MaQuay");
            lb_mahoadon.DataBindings.Add("Text", DataSource, "Ma_HoaDon");
            lb_nhanvien.DataBindings.Add("Text", DataSource, "TenNV");
            lb_ngay.DataBindings.Add("Text", DataSource, "Ngay");
            lb_Tenban.DataBindings.Add("Text", DataSource, "TEN_BAN");
            lbTenKH1.DataBindings.Add("Text", DataSource, "TENKH");
        }

        private void Setheader()
        {
            //StreamReader str = new StreamReader(Application.StartupPath + "/systeminfo.txt");
            //string sMacuahang = "";
            //string line;
            //while ((line = str.ReadLine()) != null)
            //{
            //    string[] value = line.Split(',');
            //    sMacuahang = value[0].ToString();
            //}

            //string sSQL = "";
            //sSQL += "Select MA_CUAHANG,	TEN_CUAHANG,	GHICHU,	SUDUNG,	Header,	Footer" + "\n";
            //sSQL += "From CUAHANG" + "\n";
            //sSQL += "Where MA_CUAHANG  =" + clsMain.SQLString(sMacuahang) + "\n";
            //DataTable dt = clsMain.ReturnDataTable(sSQL);

            //lb_Header.Text = dt.Rows[0]["Header"].ToString();
        }      
    }
}
