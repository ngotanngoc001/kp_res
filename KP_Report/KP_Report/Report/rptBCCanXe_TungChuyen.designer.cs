﻿namespace KP_Report
{
    partial class rptBCCanXe_TungChuyen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.stt = new DevExpress.XtraReports.UI.XRTableCell();
            this.MAPHIEU = new DevExpress.XtraReports.UI.XRTableCell();
            this.SOXE = new DevExpress.XtraReports.UI.XRTableCell();
            this.TL_THIETKE = new DevExpress.XtraReports.UI.XRTableCell();
            this.TL_BANTHAN = new DevExpress.XtraReports.UI.XRTableCell();
            this.TL_XEHANG = new DevExpress.XtraReports.UI.XRTableCell();
            this.TL_QUATAI = new DevExpress.XtraReports.UI.XRTableCell();
            this.TT_QUATAI = new DevExpress.XtraReports.UI.XRTableCell();
            this.CD_COSO = new DevExpress.XtraReports.UI.XRTableCell();
            this.CD_QUAKHO = new DevExpress.XtraReports.UI.XRTableCell();
            this.TT_QUAKHO = new DevExpress.XtraReports.UI.XRTableCell();
            this.NGUOITAO = new DevExpress.XtraReports.UI.XRTableCell();
            this.NGAYTAO = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.logo = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TENDOITUONG = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.NGAYDI = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GIODI = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TENTUYEN = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txtDiachi = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txtTencongty = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txtDienthoai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo4 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TONG_TT_QUATAI = new DevExpress.XtraReports.UI.XRTableCell();
            this.TONG_TT_QUAKHO = new DevExpress.XtraReports.UI.XRTableCell();
            this.TONG_TT_PHUTHU = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TENKHUVUC = new DevExpress.XtraReports.UI.XRTableCell();
            this.TT_PHUTHU = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 61.70086F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Dpi = 254F;
            this.xrTable2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(68.52071F, 0.0008881887F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(2834.062F, 61.69997F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.stt,
            this.MAPHIEU,
            this.SOXE,
            this.TL_THIETKE,
            this.TL_BANTHAN,
            this.TL_XEHANG,
            this.TL_QUATAI,
            this.TT_QUATAI,
            this.TENKHUVUC,
            this.CD_COSO,
            this.CD_QUAKHO,
            this.TT_QUAKHO,
            this.TT_PHUTHU,
            this.NGUOITAO,
            this.NGAYTAO});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // stt
            // 
            this.stt.Dpi = 254F;
            this.stt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stt.Name = "stt";
            this.stt.StylePriority.UseFont = false;
            this.stt.StylePriority.UseTextAlignment = false;
            this.stt.Text = "1";
            this.stt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.stt.Weight = 0.10154128975768337D;
            // 
            // MAPHIEU
            // 
            this.MAPHIEU.Dpi = 254F;
            this.MAPHIEU.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MAPHIEU.Name = "MAPHIEU";
            this.MAPHIEU.StylePriority.UseFont = false;
            this.MAPHIEU.StylePriority.UseTextAlignment = false;
            this.MAPHIEU.Text = "01122015001";
            this.MAPHIEU.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MAPHIEU.Weight = 0.30116054666753073D;
            // 
            // SOXE
            // 
            this.SOXE.Dpi = 254F;
            this.SOXE.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOXE.Name = "SOXE";
            this.SOXE.StylePriority.UseFont = false;
            this.SOXE.StylePriority.UseTextAlignment = false;
            this.SOXE.Text = "51A-60413";
            this.SOXE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.SOXE.Weight = 0.23214457762886512D;
            // 
            // TL_THIETKE
            // 
            this.TL_THIETKE.Dpi = 254F;
            this.TL_THIETKE.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TL_THIETKE.Name = "TL_THIETKE";
            this.TL_THIETKE.StylePriority.UseFont = false;
            this.TL_THIETKE.StylePriority.UseTextAlignment = false;
            this.TL_THIETKE.Text = "999,000";
            this.TL_THIETKE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TL_THIETKE.Weight = 0.18822533315154888D;
            // 
            // TL_BANTHAN
            // 
            this.TL_BANTHAN.Dpi = 254F;
            this.TL_BANTHAN.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TL_BANTHAN.Multiline = true;
            this.TL_BANTHAN.Name = "TL_BANTHAN";
            this.TL_BANTHAN.StylePriority.UseFont = false;
            this.TL_BANTHAN.StylePriority.UseTextAlignment = false;
            this.TL_BANTHAN.Text = "999,000";
            this.TL_BANTHAN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TL_BANTHAN.Weight = 0.18822533541816708D;
            // 
            // TL_XEHANG
            // 
            this.TL_XEHANG.Dpi = 254F;
            this.TL_XEHANG.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TL_XEHANG.Name = "TL_XEHANG";
            this.TL_XEHANG.StylePriority.UseFont = false;
            this.TL_XEHANG.StylePriority.UseTextAlignment = false;
            this.TL_XEHANG.Text = "999,000";
            this.TL_XEHANG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TL_XEHANG.Weight = 0.18822533746892609D;
            // 
            // TL_QUATAI
            // 
            this.TL_QUATAI.Dpi = 254F;
            this.TL_QUATAI.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.TL_QUATAI.Name = "TL_QUATAI";
            this.TL_QUATAI.StylePriority.UseFont = false;
            this.TL_QUATAI.StylePriority.UseTextAlignment = false;
            this.TL_QUATAI.Text = "999,000";
            this.TL_QUATAI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TL_QUATAI.Weight = 0.1882253331502306D;
            // 
            // TT_QUATAI
            // 
            this.TT_QUATAI.Dpi = 254F;
            this.TT_QUATAI.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.TT_QUATAI.Name = "TT_QUATAI";
            this.TT_QUATAI.StylePriority.UseFont = false;
            this.TT_QUATAI.StylePriority.UseTextAlignment = false;
            this.TT_QUATAI.Text = "999,000,000";
            this.TT_QUATAI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TT_QUATAI.Weight = 0.301160543314143D;
            // 
            // CD_COSO
            // 
            this.CD_COSO.Dpi = 254F;
            this.CD_COSO.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.CD_COSO.Name = "CD_COSO";
            this.CD_COSO.StylePriority.UseFont = false;
            this.CD_COSO.StylePriority.UseTextAlignment = false;
            this.CD_COSO.Text = "99,000";
            this.CD_COSO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.CD_COSO.Weight = 0.16312862810399459D;
            // 
            // CD_QUAKHO
            // 
            this.CD_QUAKHO.Dpi = 254F;
            this.CD_QUAKHO.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.CD_QUAKHO.Name = "CD_QUAKHO";
            this.CD_QUAKHO.StylePriority.UseFont = false;
            this.CD_QUAKHO.StylePriority.UseTextAlignment = false;
            this.CD_QUAKHO.Text = "99,000";
            this.CD_QUAKHO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.CD_QUAKHO.Weight = 0.16312862810399464D;
            // 
            // TT_QUAKHO
            // 
            this.TT_QUAKHO.Dpi = 254F;
            this.TT_QUAKHO.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.TT_QUAKHO.Name = "TT_QUAKHO";
            this.TT_QUAKHO.StylePriority.UseFont = false;
            this.TT_QUAKHO.StylePriority.UseTextAlignment = false;
            this.TT_QUAKHO.Text = "999,000,000";
            this.TT_QUAKHO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TT_QUAKHO.Weight = 0.30116054269395975D;
            // 
            // NGUOITAO
            // 
            this.NGUOITAO.Dpi = 254F;
            this.NGUOITAO.Font = new System.Drawing.Font("Tahoma", 10F);
            this.NGUOITAO.Name = "NGUOITAO";
            this.NGUOITAO.StylePriority.UseFont = false;
            this.NGUOITAO.StylePriority.UseTextAlignment = false;
            this.NGUOITAO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.NGUOITAO.Weight = 0.27606382636169585D;
            // 
            // NGAYTAO
            // 
            this.NGAYTAO.Dpi = 254F;
            this.NGAYTAO.Font = new System.Drawing.Font("Tahoma", 10F);
            this.NGAYTAO.Name = "NGAYTAO";
            this.NGAYTAO.StylePriority.UseFont = false;
            this.NGAYTAO.StylePriority.UseTextAlignment = false;
            this.NGAYTAO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.NGAYTAO.Weight = 0.26118366734415122D;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 51F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 51F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.logo,
            this.xrTable5,
            this.xrTable13,
            this.xrTable12,
            this.xrTable7,
            this.xrTable3,
            this.xrTable4,
            this.xrTable10,
            this.xrTable1});
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 603.5469F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // logo
            // 
            this.logo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.logo.Dpi = 254F;
            this.logo.LocationFloat = new DevExpress.Utils.PointFloat(68.52091F, 0F);
            this.logo.Name = "logo";
            this.logo.SizeF = new System.Drawing.SizeF(219.98F, 200F);
            this.logo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            this.logo.StylePriority.UseBorders = false;
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTable5.Dpi = 254F;
            this.xrTable5.Font = new System.Drawing.Font("Tahoma", 12F);
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(68.52045F, 379.9419F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(2834.062F, 61.70084F);
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.StylePriority.UseFont = false;
            this.xrTable5.StylePriority.UseTextAlignment = false;
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14,
            this.TENDOITUONG});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "Tàu :";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell14.Weight = 0.591739250442844D;
            // 
            // TENDOITUONG
            // 
            this.TENDOITUONG.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.TENDOITUONG.Dpi = 254F;
            this.TENDOITUONG.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TENDOITUONG.Name = "TENDOITUONG";
            this.TENDOITUONG.StylePriority.UseBorders = false;
            this.TENDOITUONG.StylePriority.UseFont = false;
            this.TENDOITUONG.StylePriority.UseTextAlignment = false;
            this.TENDOITUONG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.TENDOITUONG.Weight = 8.28032645099763D;
            // 
            // xrTable13
            // 
            this.xrTable13.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTable13.Dpi = 254F;
            this.xrTable13.Font = new System.Drawing.Font("Tahoma", 12F);
            this.xrTable13.LocationFloat = new DevExpress.Utils.PointFloat(68.5203F, 441.6427F);
            this.xrTable13.Name = "xrTable13";
            this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13});
            this.xrTable13.SizeF = new System.Drawing.SizeF(2834.439F, 61.70087F);
            this.xrTable13.StylePriority.UseBorders = false;
            this.xrTable13.StylePriority.UseFont = false;
            this.xrTable13.StylePriority.UseTextAlignment = false;
            this.xrTable13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.NGAYDI,
            this.xrTableCell5,
            this.GIODI});
            this.xrTableRow13.Dpi = 254F;
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell32.Multiline = true;
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.StylePriority.UseBorders = false;
            this.xrTableCell32.StylePriority.UseFont = false;
            this.xrTableCell32.StylePriority.UseTextAlignment = false;
            this.xrTableCell32.Text = "Ngày : ";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell32.Weight = 0.27725706741570488D;
            // 
            // NGAYDI
            // 
            this.NGAYDI.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.NGAYDI.Dpi = 254F;
            this.NGAYDI.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NGAYDI.Name = "NGAYDI";
            this.NGAYDI.StylePriority.UseBorders = false;
            this.NGAYDI.StylePriority.UseFont = false;
            this.NGAYDI.StylePriority.UseTextAlignment = false;
            this.NGAYDI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.NGAYDI.Weight = 0.502266514875516D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "Giờ :";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell5.Weight = 0.14106339605596704D;
            // 
            // GIODI
            // 
            this.GIODI.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.GIODI.Dpi = 254F;
            this.GIODI.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GIODI.Name = "GIODI";
            this.GIODI.StylePriority.UseBorders = false;
            this.GIODI.StylePriority.UseFont = false;
            this.GIODI.StylePriority.UseTextAlignment = false;
            this.GIODI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.GIODI.Weight = 3.2369371778929228D;
            // 
            // xrTable12
            // 
            this.xrTable12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTable12.Dpi = 254F;
            this.xrTable12.Font = new System.Drawing.Font("Tahoma", 12F);
            this.xrTable12.LocationFloat = new DevExpress.Utils.PointFloat(68.5203F, 318.2411F);
            this.xrTable12.Name = "xrTable12";
            this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.xrTable12.SizeF = new System.Drawing.SizeF(2834.063F, 61.70084F);
            this.xrTable12.StylePriority.UseBorders = false;
            this.xrTable12.StylePriority.UseFont = false;
            this.xrTable12.StylePriority.UseTextAlignment = false;
            this.xrTable12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.TENTUYEN});
            this.xrTableRow12.Dpi = 254F;
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "Tuyến :";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell9.Weight = 0.591739250442844D;
            // 
            // TENTUYEN
            // 
            this.TENTUYEN.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.TENTUYEN.Dpi = 254F;
            this.TENTUYEN.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TENTUYEN.Name = "TENTUYEN";
            this.TENTUYEN.StylePriority.UseBorders = false;
            this.TENTUYEN.StylePriority.UseFont = false;
            this.TENTUYEN.StylePriority.UseTextAlignment = false;
            this.TENTUYEN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.TENTUYEN.Weight = 8.2803287438530475D;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.Dpi = 254F;
            this.xrTable7.Font = new System.Drawing.Font("Tahoma", 16F);
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(68.5203F, 200F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(2834.44F, 88.15921F);
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.StylePriority.UseFont = false;
            this.xrTable7.StylePriority.UseTextAlignment = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold);
            this.xrTableCell38.Multiline = true;
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.StylePriority.UseBorders = false;
            this.xrTableCell38.StylePriority.UseFont = false;
            this.xrTableCell38.StylePriority.UseTextAlignment = false;
            this.xrTableCell38.Text = "BÁO CÁO CÂN XE TỪNG CHUYẾN";
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell38.Weight = 4.2766870317947046D;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Dpi = 254F;
            this.xrTable3.Font = new System.Drawing.Font("Tahoma", 16F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(314.583F, 61.70085F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(2588F, 61.70082F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txtDiachi});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // txtDiachi
            // 
            this.txtDiachi.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtDiachi.Dpi = 254F;
            this.txtDiachi.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiachi.Multiline = true;
            this.txtDiachi.Name = "txtDiachi";
            this.txtDiachi.StylePriority.UseBorders = false;
            this.txtDiachi.StylePriority.UseFont = false;
            this.txtDiachi.StylePriority.UseTextAlignment = false;
            this.txtDiachi.Text = "Địa chỉ : Khu phố 1, P.Tô Châu, TX. Hà Tiên, Tỉnh Kiên Giang";
            this.txtDiachi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txtDiachi.Weight = 4.462445489907501D;
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.Dpi = 254F;
            this.xrTable4.Font = new System.Drawing.Font("Tahoma", 16F);
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(314.583F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(2588F, 61.70083F);
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.StylePriority.UseFont = false;
            this.xrTable4.StylePriority.UseTextAlignment = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txtTencongty});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // txtTencongty
            // 
            this.txtTencongty.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtTencongty.Dpi = 254F;
            this.txtTencongty.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTencongty.Multiline = true;
            this.txtTencongty.Name = "txtTencongty";
            this.txtTencongty.StylePriority.UseBorders = false;
            this.txtTencongty.StylePriority.UseFont = false;
            this.txtTencongty.StylePriority.UseTextAlignment = false;
            this.txtTencongty.Text = "CÔNG TY TNHH MTV THẠNH THỚI";
            this.txtTencongty.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txtTencongty.Weight = 4.462445489907501D;
            // 
            // xrTable10
            // 
            this.xrTable10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable10.Dpi = 254F;
            this.xrTable10.Font = new System.Drawing.Font("Tahoma", 16F);
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(314.583F, 123.4017F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(2588F, 61.70082F);
            this.xrTable10.StylePriority.UseBorders = false;
            this.xrTable10.StylePriority.UseFont = false;
            this.xrTable10.StylePriority.UseTextAlignment = false;
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txtDienthoai});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // txtDienthoai
            // 
            this.txtDienthoai.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtDienthoai.Dpi = 254F;
            this.txtDienthoai.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDienthoai.Multiline = true;
            this.txtDienthoai.Name = "txtDienthoai";
            this.txtDienthoai.StylePriority.UseBorders = false;
            this.txtDienthoai.StylePriority.UseFont = false;
            this.txtDienthoai.StylePriority.UseTextAlignment = false;
            this.txtDienthoai.Text = "Điện thoại : 077.3957239 - Fax : 077.3957238";
            this.txtDienthoai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txtDienthoai.Weight = 4.462445489907501D;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Dpi = 254F;
            this.xrTable1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(68.51988F, 503.3436F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(2834.063F, 100.2032F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell3,
            this.xrTableCell12,
            this.xrTableCell15,
            this.xrTableCell19,
            this.xrTableCell20,
            this.xrTableCell18,
            this.xrTableCell13,
            this.xrTableCell17,
            this.xrTableCell23,
            this.xrTableCell6,
            this.xrTableCell7});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "STT";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.11198569223681101D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "Mã phiếu";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell2.Weight = 0.33213401892472955D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.Text = "Số xe";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell10.Weight = 0.25602000647488082D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = "TL thiết kế";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell11.Weight = 0.20758379290887749D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "TL bản thân";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 0.20758379205933869D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "TL xe hàng";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell12.Weight = 0.20758378876049133D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "TL quá tải";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell15.Weight = 0.20758412626256942D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseTextAlignment = false;
            this.xrTableCell19.Text = "Tiền quá tải";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell19.Weight = 0.33213363870865548D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "CD cơ sở";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell18.Weight = 0.17990594471437965D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "CD quá khổ";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell13.Weight = 0.17990611364649872D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.Text = "Tiền quá khổ";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell17.Weight = 0.33213388700878554D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "Nhân viên";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell6.Weight = 0.30445656098455093D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "Ngày";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell7.Weight = 0.28804536191868146D;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.Dpi = 254F;
            this.xrPageInfo3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo3.Format = "{0:dd/MM/yyyy HH:mm}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(68.52091F, 0F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(400F, 45.1908F);
            this.xrPageInfo3.StylePriority.UseFont = false;
            // 
            // xrPageInfo4
            // 
            this.xrPageInfo4.Dpi = 254F;
            this.xrPageInfo4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo4.LocationFloat = new DevExpress.Utils.PointFloat(2502.583F, 0F);
            this.xrPageInfo4.Name = "xrPageInfo4";
            this.xrPageInfo4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo4.SizeF = new System.Drawing.SizeF(400F, 45.19084F);
            this.xrPageInfo4.StylePriority.UseFont = false;
            this.xrPageInfo4.StylePriority.UseTextAlignment = false;
            this.xrPageInfo4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo3,
            this.xrPageInfo4});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 45.19084F;
            this.PageFooter.Name = "PageFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 0F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.PrintOn = ((DevExpress.XtraReports.UI.PrintOnPages)((DevExpress.XtraReports.UI.PrintOnPages.NotWithReportHeader | DevExpress.XtraReports.UI.PrintOnPages.NotWithReportFooter)));
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.Dpi = 254F;
            this.xrTable6.Font = new System.Drawing.Font("Tahoma", 16F);
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(2576.313F, 106.4459F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable6.SizeF = new System.Drawing.SizeF(326.2695F, 61.7F);
            this.xrTable6.StylePriority.UseBorders = false;
            this.xrTable6.StylePriority.UseFont = false;
            this.xrTable6.StylePriority.UseTextAlignment = false;
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.xrTableCell4.Multiline = true;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Người lập";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell4.Weight = 2.9712627931651934D;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable9,
            this.xrTable6});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 232.8333F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrTable9
            // 
            this.xrTable9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable9.Dpi = 254F;
            this.xrTable9.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(68.51988F, 0F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable9.SizeF = new System.Drawing.SizeF(2834.063F, 61.69997F);
            this.xrTable9.StylePriority.UseBorders = false;
            this.xrTable9.StylePriority.UseFont = false;
            this.xrTable9.StylePriority.UseTextAlignment = false;
            this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.TONG_TT_QUATAI,
            this.TONG_TT_QUAKHO,
            this.TONG_TT_PHUTHU,
            this.xrTableCell16});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "Tổng cộng :";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell8.Weight = 1.3877488263334254D;
            // 
            // TONG_TT_QUATAI
            // 
            this.TONG_TT_QUATAI.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TONG_TT_QUATAI.Dpi = 254F;
            this.TONG_TT_QUATAI.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.TONG_TT_QUATAI.Name = "TONG_TT_QUATAI";
            this.TONG_TT_QUATAI.StylePriority.UseBorders = false;
            this.TONG_TT_QUATAI.StylePriority.UseFont = false;
            this.TONG_TT_QUATAI.StylePriority.UseTextAlignment = false;
            this.TONG_TT_QUATAI.Text = "999,000,000";
            this.TONG_TT_QUATAI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TONG_TT_QUATAI.Weight = 0.30116051810885985D;
            // 
            // TONG_TT_QUAKHO
            // 
            this.TONG_TT_QUAKHO.Dpi = 254F;
            this.TONG_TT_QUAKHO.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.TONG_TT_QUAKHO.Name = "TONG_TT_QUAKHO";
            this.TONG_TT_QUAKHO.StylePriority.UseFont = false;
            this.TONG_TT_QUAKHO.StylePriority.UseTextAlignment = false;
            this.TONG_TT_QUAKHO.Text = "999,000,000";
            this.TONG_TT_QUAKHO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TONG_TT_QUAKHO.Weight = 0.95367481593552594D;
            // 
            // TONG_TT_PHUTHU
            // 
            this.TONG_TT_PHUTHU.Dpi = 254F;
            this.TONG_TT_PHUTHU.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.TONG_TT_PHUTHU.Name = "TONG_TT_PHUTHU";
            this.TONG_TT_PHUTHU.StylePriority.UseFont = false;
            this.TONG_TT_PHUTHU.StylePriority.UseTextAlignment = false;
            this.TONG_TT_PHUTHU.Text = "999,000,000";
            this.TONG_TT_PHUTHU.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TONG_TT_PHUTHU.Weight = 0.37645034046486481D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Weight = 0.53724832947716517D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.Text = "Tên loại xe";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell20.Weight = 0.359811899443776D;
            // 
            // TENKHUVUC
            // 
            this.TENKHUVUC.Dpi = 254F;
            this.TENKHUVUC.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.TENKHUVUC.Name = "TENKHUVUC";
            this.TENKHUVUC.StylePriority.UseFont = false;
            this.TENKHUVUC.StylePriority.UseTextAlignment = false;
            this.TENKHUVUC.Text = "Xe 5 tấn";
            this.TENKHUVUC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TENKHUVUC.Weight = 0.32625725472791534D;
            // 
            // TT_PHUTHU
            // 
            this.TT_PHUTHU.Dpi = 254F;
            this.TT_PHUTHU.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.TT_PHUTHU.Name = "TT_PHUTHU";
            this.TT_PHUTHU.StylePriority.UseFont = false;
            this.TT_PHUTHU.StylePriority.UseTextAlignment = false;
            this.TT_PHUTHU.Text = "9,000,000,000";
            this.TT_PHUTHU.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TT_PHUTHU.Weight = 0.37645068021060807D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseTextAlignment = false;
            this.xrTableCell23.Text = "TT phụ thu";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell23.Weight = 0.41516790901961131D;
            // 
            // rptBCCanXe_TungChuyen
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageHeader,
            this.PageFooter,
            this.ReportFooter});
            this.Dpi = 254F;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 51, 51);
            this.PageHeight = 2101;
            this.PageWidth = 2969;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.ShowPrintMarginsWarning = false;
            this.SnapGridSize = 31.75F;
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell stt;
        private DevExpress.XtraReports.UI.XRTableCell MAPHIEU;
        private DevExpress.XtraReports.UI.XRTableCell TL_BANTHAN;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell txtDiachi;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell txtTencongty;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell txtDienthoai;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo3;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo4;
        private DevExpress.XtraReports.UI.XRTable xrTable13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell NGAYDI;
        private DevExpress.XtraReports.UI.XRTable xrTable12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell TENTUYEN;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell GIODI;
        private DevExpress.XtraReports.UI.XRTableCell SOXE;
        private DevExpress.XtraReports.UI.XRTableCell TL_THIETKE;
        private DevExpress.XtraReports.UI.XRTableCell TL_XEHANG;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell TENDOITUONG;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRPictureBox logo;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell NGUOITAO;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell TONG_TT_QUATAI;
        private DevExpress.XtraReports.UI.XRTableCell NGAYTAO;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell TONG_TT_PHUTHU;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell TL_QUATAI;
        private DevExpress.XtraReports.UI.XRTableCell CD_QUAKHO;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell TT_QUAKHO;
        private DevExpress.XtraReports.UI.XRTableCell TT_QUATAI;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell TONG_TT_QUAKHO;
        private DevExpress.XtraReports.UI.XRTableCell CD_COSO;
        private DevExpress.XtraReports.UI.XRTableCell TENKHUVUC;
        private DevExpress.XtraReports.UI.XRTableCell TT_PHUTHU;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
    }
}
