﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBaoCaoTongHopVoucher : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBaoCaoTongHopVoucher()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            NGAY_PHATHANH.DataBindings.Add("Text", DataSource, "NGAY_PHATHANH", "{0:dd/MM/yyyy}");
            SOLUONG.DataBindings.Add("Text", DataSource, "SOLUONG");
            SERI_TU.DataBindings.Add("Text", DataSource, "SERI_TU");
            SERI_DEN.DataBindings.Add("Text", DataSource, "SERI_DEN");
            LOAI_VOUCHER.DataBindings.Add("Text", DataSource, "LOAI_VOUCHER");
            KICHHOAT.DataBindings.Add("Text", DataSource, "KICHHOAT");
            HETHAN.DataBindings.Add("Text", DataSource, "HETHAN");
            TONGGIATRI.DataBindings.Add("Text", DataSource, "TONGGIATRI", "{0:#,###0}");
            TONG_SUDUNG.DataBindings.Add("Text", DataSource, "TONG_SUDUNG", "{0:#,###0}");
            SL_HETHAN.DataBindings.Add("Text", DataSource, "SL_HETHAN", "{0:#,###0}");

            DataTable dtSource = (DataTable)DataSource;
            txt_tieude.Text = dtSource.Rows[0]["HEARDER"].ToString();
        }

        int i = 0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
