﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptLSDQ_KHTT : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptLSDQ_KHTT()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData() 
        {
            MaKH.DataBindings.Add("Text", DataSource, "MA");
            TenKH.DataBindings.Add("Text", DataSource, "TEN");
            NgaySinh.DataBindings.Add("Text", DataSource, "NGAYSINH", "{0:dd/MM/yyyy}");
            GioiTinh.DataBindings.Add("Text", DataSource, "GIOITINH1");
            Cmnd.DataBindings.Add("Text", DataSource, "CMND");
            DienThoai.DataBindings.Add("Text", DataSource, "DIENTHOAI");
            DiaChi.DataBindings.Add("Text", DataSource, "DIACHI");
            Email.DataBindings.Add("Text", DataSource, "EMAIL");
            Web.DataBindings.Add("Text", DataSource, "WEBSITE");
            TongDiem.DataBindings.Add("Text", DataSource, "TongDiem", "{0:#,###0}");


            ThoiGian.DataBindings.Add("Text", DataSource, "NGAYDOI", "{0:dd/MM/yyyy HH:mm:ss}");
            NhanVien.DataBindings.Add("Text", DataSource, "TENNHANVIEN");
            Qua.DataBindings.Add("Text", DataSource, "TEN_QT");
            SoDiem.DataBindings.Add("Text", DataSource, "SODIEM", "{0:#,###0}");
            Soluong.DataBindings.Add("Text", DataSource, "SOLUONG");
           
        }

    }
}
