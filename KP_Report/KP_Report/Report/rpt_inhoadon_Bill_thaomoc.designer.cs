﻿namespace KP_Report
{
    partial class rpt_inhoadon_Bill_thaomoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenNhanVien = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblMain = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cl_HangHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.cl_soluong = new DevExpress.XtraReports.UI.XRTableCell();
            this.cl_giaban = new DevExpress.XtraReports.UI.XRTableCell();
            this.CHIETKHAU = new DevExpress.XtraReports.UI.XRTableCell();
            this.cl_thanhtien = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.winControlContainer1 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.Logo = new System.Windows.Forms.Panel();
            this.lb_Quay = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_Header = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_nhanvien = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_ngay = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lb_mahoadon = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_ban = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_NameBill = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.GhiChu = new DevExpress.XtraReports.UI.XRLabel();
            this.lbFooter = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.lb_giamgia = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_tongcong = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_MaHD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.Dat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine6 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine7 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2,
            this.tblMain});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 84.66668F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable2.Dpi = 254F;
            this.xrTable2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.ForeColor = System.Drawing.Color.Blue;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(253.3624F, 42.33333F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable2.SizeF = new System.Drawing.SizeF(481.2524F, 42.33335F);
            this.xrTable2.StylePriority.UseBorderDashStyle = false;
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseForeColor = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenNhanVien});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // TenNhanVien
            // 
            this.TenNhanVien.Dpi = 254F;
            this.TenNhanVien.Name = "TenNhanVien";
            this.TenNhanVien.StylePriority.UseTextAlignment = false;
            this.TenNhanVien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenNhanVien.Weight = 2.4724647450446557D;
            // 
            // tblMain
            // 
            this.tblMain.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.tblMain.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.tblMain.Dpi = 254F;
            this.tblMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblMain.ForeColor = System.Drawing.Color.Blue;
            this.tblMain.LocationFloat = new DevExpress.Utils.PointFloat(15.79522F, 0F);
            this.tblMain.Name = "tblMain";
            this.tblMain.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.tblMain.SizeF = new System.Drawing.SizeF(718.8196F, 42.33334F);
            this.tblMain.StylePriority.UseBorderDashStyle = false;
            this.tblMain.StylePriority.UseBorders = false;
            this.tblMain.StylePriority.UseFont = false;
            this.tblMain.StylePriority.UseForeColor = false;
            this.tblMain.StylePriority.UseTextAlignment = false;
            this.tblMain.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cl_HangHoa,
            this.cl_soluong,
            this.cl_giaban,
            this.CHIETKHAU,
            this.cl_thanhtien});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // cl_HangHoa
            // 
            this.cl_HangHoa.Dpi = 254F;
            this.cl_HangHoa.Name = "cl_HangHoa";
            this.cl_HangHoa.StylePriority.UseTextAlignment = false;
            this.cl_HangHoa.Text = "my á";
            this.cl_HangHoa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cl_HangHoa.Weight = 0.8171393177277978D;
            // 
            // cl_soluong
            // 
            this.cl_soluong.Dpi = 254F;
            this.cl_soluong.Name = "cl_soluong";
            this.cl_soluong.StylePriority.UseTextAlignment = false;
            this.cl_soluong.Text = "100";
            this.cl_soluong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cl_soluong.Weight = 0.19165001472060034D;
            // 
            // cl_giaban
            // 
            this.cl_giaban.Dpi = 254F;
            this.cl_giaban.Name = "cl_giaban";
            this.cl_giaban.StylePriority.UseTextAlignment = false;
            this.cl_giaban.Text = "7,000,000";
            this.cl_giaban.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cl_giaban.Weight = 0.56680650252732634D;
            // 
            // CHIETKHAU
            // 
            this.CHIETKHAU.Dpi = 254F;
            this.CHIETKHAU.Name = "CHIETKHAU";
            this.CHIETKHAU.StylePriority.UseTextAlignment = false;
            this.CHIETKHAU.Text = "50%";
            this.CHIETKHAU.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CHIETKHAU.Weight = 0.2831283578283369D;
            // 
            // cl_thanhtien
            // 
            this.cl_thanhtien.Dpi = 254F;
            this.cl_thanhtien.Name = "cl_thanhtien";
            this.cl_thanhtien.StylePriority.UseTextAlignment = false;
            this.cl_thanhtien.Text = "35,000,000";
            this.cl_thanhtien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cl_thanhtien.Weight = 0.61374055224059421D;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 20F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 81F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.winControlContainer1,
            this.lb_Quay,
            this.xrLabel15,
            this.lb_Header,
            this.lb_nhanvien,
            this.lb_ngay,
            this.xrLine1,
            this.xrLine2,
            this.xrTable1,
            this.lb_mahoadon,
            this.lb_ban,
            this.lb_NameBill,
            this.xrLabel3,
            this.xrLabel5,
            this.xrLabel4});
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 416.6657F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // winControlContainer1
            // 
            this.winControlContainer1.Dpi = 254F;
            this.winControlContainer1.LocationFloat = new DevExpress.Utils.PointFloat(31.66972F, 2.645793F);
            this.winControlContainer1.Name = "winControlContainer1";
            this.winControlContainer1.SizeF = new System.Drawing.SizeF(702.9451F, 31.75005F);
            this.winControlContainer1.WinControl = this.Logo;
            // 
            // Logo
            // 
            this.Logo.Location = new System.Drawing.Point(0, 0);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(266, 12);
            this.Logo.TabIndex = 0;
            // 
            // lb_Quay
            // 
            this.lb_Quay.Dpi = 254F;
            this.lb_Quay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Quay.ForeColor = System.Drawing.Color.Blue;
            this.lb_Quay.LocationFloat = new DevExpress.Utils.PointFloat(127.67F, 225.3188F);
            this.lb_Quay.Name = "lb_Quay";
            this.lb_Quay.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_Quay.SizeF = new System.Drawing.SizeF(181.4108F, 45.1908F);
            this.lb_Quay.StylePriority.UseFont = false;
            this.lb_Quay.StylePriority.UseForeColor = false;
            this.lb_Quay.Text = "xrLabel6";
            // 
            // xrLabel15
            // 
            this.xrLabel15.Dpi = 254F;
            this.xrLabel15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(31.67F, 225.3188F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(96F, 45.19086F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseForeColor = false;
            this.xrLabel15.Text = "Quầy:";
            // 
            // lb_Header
            // 
            this.lb_Header.Dpi = 254F;
            this.lb_Header.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Header.ForeColor = System.Drawing.Color.Blue;
            this.lb_Header.LocationFloat = new DevExpress.Utils.PointFloat(24.04999F, 34.39583F);
            this.lb_Header.Multiline = true;
            this.lb_Header.Name = "lb_Header";
            this.lb_Header.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_Header.SizeF = new System.Drawing.SizeF(710.5649F, 50.27086F);
            this.lb_Header.StylePriority.UseFont = false;
            this.lb_Header.StylePriority.UseForeColor = false;
            this.lb_Header.StylePriority.UseTextAlignment = false;
            this.lb_Header.Text = "SPA THẢO MỘC\r\nCN1 : 29/8 Nguyễn Văn Bảo , P 4, Gò Vấp\r\nCN2 :1B Nhất Chi Mai , P 1" +
                "3 , Tân Bình\r\nHotline : 093 517 2080 - 093 517 2080";
            this.lb_Header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // lb_nhanvien
            // 
            this.lb_nhanvien.Dpi = 254F;
            this.lb_nhanvien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_nhanvien.ForeColor = System.Drawing.Color.Blue;
            this.lb_nhanvien.LocationFloat = new DevExpress.Utils.PointFloat(94.6703F, 270.5096F);
            this.lb_nhanvien.Name = "lb_nhanvien";
            this.lb_nhanvien.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_nhanvien.SizeF = new System.Drawing.SizeF(214.4105F, 45.1908F);
            this.lb_nhanvien.StylePriority.UseFont = false;
            this.lb_nhanvien.StylePriority.UseForeColor = false;
            this.lb_nhanvien.Text = "xrLabel6";
            // 
            // lb_ngay
            // 
            this.lb_ngay.Dpi = 254F;
            this.lb_ngay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ngay.ForeColor = System.Drawing.Color.Blue;
            this.lb_ngay.LocationFloat = new DevExpress.Utils.PointFloat(405.0807F, 225.3191F);
            this.lb_ngay.Name = "lb_ngay";
            this.lb_ngay.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_ngay.SizeF = new System.Drawing.SizeF(289.4408F, 45.1907F);
            this.lb_ngay.StylePriority.UseFont = false;
            this.lb_ngay.StylePriority.UseForeColor = false;
            this.lb_ngay.StylePriority.UseTextAlignment = false;
            this.lb_ngay.Text = "23-05-2015 10:13:13";
            this.lb_ngay.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLine1
            // 
            this.xrLine1.BorderWidth = 1;
            this.xrLine1.Dpi = 254F;
            this.xrLine1.ForeColor = System.Drawing.Color.Blue;
            this.xrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine1.LineWidth = 3;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(25.00029F, 315.7007F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(709.6149F, 26.67001F);
            this.xrLine1.StylePriority.UseBorderWidth = false;
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // xrLine2
            // 
            this.xrLine2.Dpi = 254F;
            this.xrLine2.ForeColor = System.Drawing.Color.Blue;
            this.xrLine2.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine2.LineWidth = 3;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(31.67028F, 389.9957F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(702.9451F, 26.66998F);
            this.xrLine2.StylePriority.UseForeColor = false;
            // 
            // xrTable1
            // 
            this.xrTable1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable1.Dpi = 254F;
            this.xrTable1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.ForeColor = System.Drawing.Color.Blue;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(15.79551F, 342.3707F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(718.8196F, 47.625F);
            this.xrTable1.StylePriority.UseBorderDashStyle = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseForeColor = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell5,
            this.xrTableCell4,
            this.xrTableCell3});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Hàng Hóa";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.59448151132347438D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "SL";
            this.xrTableCell2.Weight = 0.13942842506278394D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "Giá Bán";
            this.xrTableCell5.Weight = 0.41236050796942231D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "CK";
            this.xrTableCell4.Weight = 0.20598020897256814D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "Thành Tiền";
            this.xrTableCell3.Weight = 0.44650577747826925D;
            // 
            // lb_mahoadon
            // 
            this.lb_mahoadon.Dpi = 254F;
            this.lb_mahoadon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_mahoadon.ForeColor = System.Drawing.Color.Blue;
            this.lb_mahoadon.LocationFloat = new DevExpress.Utils.PointFloat(419.0808F, 270.5096F);
            this.lb_mahoadon.Name = "lb_mahoadon";
            this.lb_mahoadon.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_mahoadon.SizeF = new System.Drawing.SizeF(275.4408F, 45.19089F);
            this.lb_mahoadon.StylePriority.UseFont = false;
            this.lb_mahoadon.StylePriority.UseForeColor = false;
            this.lb_mahoadon.StylePriority.UseTextAlignment = false;
            this.lb_mahoadon.Text = "lb_mahoadon";
            this.lb_mahoadon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lb_ban
            // 
            this.lb_ban.Dpi = 254F;
            this.lb_ban.Font = new System.Drawing.Font("Times New Roman", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ban.ForeColor = System.Drawing.Color.Blue;
            this.lb_ban.LocationFloat = new DevExpress.Utils.PointFloat(179.917F, 153.4583F);
            this.lb_ban.Name = "lb_ban";
            this.lb_ban.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_ban.SizeF = new System.Drawing.SizeF(420.305F, 42.54498F);
            this.lb_ban.StylePriority.UseFont = false;
            this.lb_ban.StylePriority.UseForeColor = false;
            this.lb_ban.StylePriority.UseTextAlignment = false;
            this.lb_ban.Text = "(Hóa đơn bán lẻ có giá trị xuất hóa đơn GTGT trong ngày)";
            this.lb_ban.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lb_ban.Visible = false;
            // 
            // lb_NameBill
            // 
            this.lb_NameBill.Dpi = 254F;
            this.lb_NameBill.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_NameBill.ForeColor = System.Drawing.Color.Blue;
            this.lb_NameBill.LocationFloat = new DevExpress.Utils.PointFloat(31.67F, 118.3217F);
            this.lb_NameBill.Multiline = true;
            this.lb_NameBill.Name = "lb_NameBill";
            this.lb_NameBill.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_NameBill.SizeF = new System.Drawing.SizeF(702.9453F, 58.94905F);
            this.lb_NameBill.StylePriority.UseFont = false;
            this.lb_NameBill.StylePriority.UseForeColor = false;
            this.lb_NameBill.StylePriority.UseTextAlignment = false;
            this.lb_NameBill.Text = "HÓA ĐƠN BÁN LẺ";
            this.lb_NameBill.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(309.0808F, 270.5093F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(110F, 45.19089F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseForeColor = false;
            this.xrLabel3.Text = "Số HD:";
            // 
            // xrLabel5
            // 
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(31.67028F, 270.5096F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(63F, 45.19089F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseForeColor = false;
            this.xrLabel5.Text = "NV:";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(309.0808F, 225.3188F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(96F, 45.19073F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseForeColor = false;
            this.xrLabel4.Text = "Ngày:";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel14,
            this.xrLine6,
            this.xrLine4,
            this.xrLabel1,
            this.xrLine7,
            this.xrLabel6,
            this.xrLabel16,
            this.Dat,
            this.xrLabel12,
            this.xrLabel13,
            this.GhiChu,
            this.lbFooter,
            this.xrLine5,
            this.xrLabel10,
            this.xrLine3,
            this.lb_giamgia,
            this.xrLabel9,
            this.lb_tongcong,
            this.lb_MaHD});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 527.7499F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // GhiChu
            // 
            this.GhiChu.Dpi = 254F;
            this.GhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.GhiChu.ForeColor = System.Drawing.Color.Blue;
            this.GhiChu.LocationFloat = new DevExpress.Utils.PointFloat(31.67028F, 80.78784F);
            this.GhiChu.Name = "GhiChu";
            this.GhiChu.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.GhiChu.SizeF = new System.Drawing.SizeF(702.9445F, 43.17999F);
            this.GhiChu.StylePriority.UseFont = false;
            this.GhiChu.StylePriority.UseForeColor = false;
            this.GhiChu.StylePriority.UseTextAlignment = false;
            this.GhiChu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbFooter
            // 
            this.lbFooter.Dpi = 254F;
            this.lbFooter.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFooter.ForeColor = System.Drawing.Color.Blue;
            this.lbFooter.LocationFloat = new DevExpress.Utils.PointFloat(31.75F, 424.3153F);
            this.lbFooter.Multiline = true;
            this.lbFooter.Name = "lbFooter";
            this.lbFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbFooter.SizeF = new System.Drawing.SizeF(702.9451F, 40.42816F);
            this.lbFooter.StylePriority.UseFont = false;
            this.lbFooter.StylePriority.UseForeColor = false;
            this.lbFooter.StylePriority.UseTextAlignment = false;
            this.lbFooter.Text = "Trân trọng cảm ơn quý khách đã đến mua hàng  ";
            this.lbFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLine5
            // 
            this.xrLine5.Dpi = 254F;
            this.xrLine5.ForeColor = System.Drawing.Color.Blue;
            this.xrLine5.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine5.LineWidth = 3;
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(31.66965F, 137.2286F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(748.3163F, 13.44086F);
            this.xrLine5.StylePriority.UseForeColor = false;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Dpi = 254F;
            this.xrLabel10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel10.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(31.66995F, 25F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(345.5454F, 43.18001F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseForeColor = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "Tổng tiền thanh toán:";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine3
            // 
            this.xrLine3.Dpi = 254F;
            this.xrLine3.ForeColor = System.Drawing.Color.Blue;
            this.xrLine3.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine3.LineWidth = 3;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 0F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(709.6149F, 13.44083F);
            this.xrLine3.StylePriority.UseForeColor = false;
            // 
            // lb_giamgia
            // 
            this.lb_giamgia.Dpi = 254F;
            this.lb_giamgia.ForeColor = System.Drawing.Color.Blue;
            this.lb_giamgia.LocationFloat = new DevExpress.Utils.PointFloat(397.8618F, 20.31998F);
            this.lb_giamgia.Name = "lb_giamgia";
            this.lb_giamgia.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_giamgia.SizeF = new System.Drawing.SizeF(296.6596F, 43.18F);
            this.lb_giamgia.StylePriority.UseForeColor = false;
            this.lb_giamgia.StylePriority.UseTextAlignment = false;
            this.lb_giamgia.Text = "0";
            this.lb_giamgia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lb_giamgia.Visible = false;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(31.67F, 20.32F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(207.645F, 43.18F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseForeColor = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "Giảm giá:";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel9.Visible = false;
            // 
            // lb_tongcong
            // 
            this.lb_tongcong.Dpi = 254F;
            this.lb_tongcong.ForeColor = System.Drawing.Color.Blue;
            this.lb_tongcong.LocationFloat = new DevExpress.Utils.PointFloat(395.6725F, 25.00001F);
            this.lb_tongcong.Name = "lb_tongcong";
            this.lb_tongcong.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_tongcong.SizeF = new System.Drawing.SizeF(296.6598F, 43.17999F);
            this.lb_tongcong.StylePriority.UseForeColor = false;
            this.lb_tongcong.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:#.00}";
            this.lb_tongcong.Summary = xrSummary1;
            this.lb_tongcong.Text = "0";
            this.lb_tongcong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lb_MaHD
            // 
            this.lb_MaHD.Dpi = 254F;
            this.lb_MaHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_MaHD.ForeColor = System.Drawing.Color.Black;
            this.lb_MaHD.LocationFloat = new DevExpress.Utils.PointFloat(31.75065F, 493.1423F);
            this.lb_MaHD.Name = "lb_MaHD";
            this.lb_MaHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_MaHD.SizeF = new System.Drawing.SizeF(702.9455F, 34.60754F);
            this.lb_MaHD.StylePriority.UseFont = false;
            this.lb_MaHD.StylePriority.UseForeColor = false;
            this.lb_MaHD.StylePriority.UseTextAlignment = false;
            this.lb_MaHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Dpi = 254F;
            this.xrLabel16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(31.75F, 358.4194F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(334.1952F, 43.18002F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseForeColor = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "Tiền tip cho KTV SPA:";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(285.75F, 174.1695F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(80.19519F, 43.17999F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseForeColor = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Đạt:";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // Dat
            // 
            this.Dat.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Dat.Dpi = 254F;
            this.Dat.ForeColor = System.Drawing.Color.Blue;
            this.Dat.LocationFloat = new DevExpress.Utils.PointFloat(379.75F, 165.1695F);
            this.Dat.Name = "Dat";
            this.Dat.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.Dat.SizeF = new System.Drawing.SizeF(45.30548F, 43.17998F);
            this.Dat.StylePriority.UseBorders = false;
            this.Dat.StylePriority.UseForeColor = false;
            this.Dat.StylePriority.UseTextAlignment = false;
            this.Dat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(476.25F, 174.1695F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(173.1587F, 43.17999F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseForeColor = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "Không đạt:";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel12.Dpi = 254F;
            this.xrLabel12.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(657F, 162.1695F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(45.30548F, 43.17998F);
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.StylePriority.UseForeColor = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLine6
            // 
            this.xrLine6.Dpi = 254F;
            this.xrLine6.ForeColor = System.Drawing.Color.Blue;
            this.xrLine6.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine6.LineWidth = 3;
            this.xrLine6.LocationFloat = new DevExpress.Utils.PointFloat(317.5F, 269.4195F);
            this.xrLine6.Name = "xrLine6";
            this.xrLine6.SizeF = new System.Drawing.SizeF(470.9856F, 13.44084F);
            this.xrLine6.StylePriority.UseForeColor = false;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Dpi = 254F;
            this.xrLabel14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(31.75F, 237.6695F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(277.4112F, 43.17998F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseForeColor = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "Lý do không đạt:";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine4
            // 
            this.xrLine4.Dpi = 254F;
            this.xrLine4.ForeColor = System.Drawing.Color.Blue;
            this.xrLine4.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine4.LineWidth = 3;
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(31.75F, 317.9195F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(748.3163F, 13.44086F);
            this.xrLine4.StylePriority.UseForeColor = false;
            // 
            // xrLine7
            // 
            this.xrLine7.Dpi = 254F;
            this.xrLine7.ForeColor = System.Drawing.Color.Blue;
            this.xrLine7.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine7.LineWidth = 3;
            this.xrLine7.LocationFloat = new DevExpress.Utils.PointFloat(379F, 376.5995F);
            this.xrLine7.Name = "xrLine7";
            this.xrLine7.SizeF = new System.Drawing.SizeF(404.8398F, 13.44086F);
            this.xrLine7.StylePriority.UseForeColor = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(31.75F, 174.1695F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(228.3619F, 43.17998F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Đánh giá:";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // rpt_inhoadon_Bill_thaomoc
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.ReportFooter});
            this.Dpi = 254F;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.Margins = new System.Drawing.Printing.Margins(20, 23, 20, 81);
            this.PageHeight = 1801;
            this.PageWidth = 848;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.ShowPrintMarginsWarning = false;
            this.SnapGridSize = 31.75F;
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel lb_nhanvien;
        private DevExpress.XtraReports.UI.XRLabel lb_ngay;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRLabel lb_mahoadon;
        private DevExpress.XtraReports.UI.XRLabel lb_ban;
        private DevExpress.XtraReports.UI.XRLabel lb_NameBill;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRTable tblMain;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell cl_HangHoa;
        private DevExpress.XtraReports.UI.XRTableCell cl_soluong;
        private DevExpress.XtraReports.UI.XRTableCell cl_giaban;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel lb_giamgia;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel lb_tongcong;
        private DevExpress.XtraReports.UI.XRTableCell cl_thanhtien;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRLabel lb_Quay;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel lb_Header;
        private DevExpress.XtraReports.UI.XRLabel lbFooter;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLabel lb_MaHD;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer1;
        private System.Windows.Forms.Panel Logo;
        private DevExpress.XtraReports.UI.XRTableCell CHIETKHAU;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRLabel GhiChu;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell TenNhanVien;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLine xrLine6;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLine xrLine7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel Dat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
    }
}
