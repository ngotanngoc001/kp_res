﻿namespace KP_Report
{
    partial class rpt_Phieuthutien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.lbGhichu = new DevExpress.XtraReports.UI.XRLabel();
            this.lbPhong = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_nv = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_sotien = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbNgay = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbLannhancoc = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.lb_NameBill = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.lbFooter = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.lb_Header = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lbGhichu,
            this.lbPhong,
            this.xrLabel9,
            this.lb_nv,
            this.xrLabel10,
            this.xrLabel7,
            this.lb_sotien,
            this.xrLabel11,
            this.xrLabel2,
            this.lbNgay,
            this.xrLabel8,
            this.lbLannhancoc});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 336.0208F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbGhichu
            // 
            this.lbGhichu.Dpi = 254F;
            this.lbGhichu.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGhichu.ForeColor = System.Drawing.Color.Blue;
            this.lbGhichu.LocationFloat = new DevExpress.Utils.PointFloat(310.5493F, 283.7158F);
            this.lbGhichu.Multiline = true;
            this.lbGhichu.Name = "lbGhichu";
            this.lbGhichu.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbGhichu.SizeF = new System.Drawing.SizeF(410.8369F, 43.17996F);
            this.lbGhichu.StylePriority.UseFont = false;
            this.lbGhichu.StylePriority.UseForeColor = false;
            this.lbGhichu.StylePriority.UseTextAlignment = false;
            this.lbGhichu.Text = "Ở tiếp 1 ngày ";
            this.lbGhichu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbPhong
            // 
            this.lbPhong.Dpi = 254F;
            this.lbPhong.ForeColor = System.Drawing.Color.Blue;
            this.lbPhong.LocationFloat = new DevExpress.Utils.PointFloat(310.5493F, 0F);
            this.lbPhong.Name = "lbPhong";
            this.lbPhong.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbPhong.SizeF = new System.Drawing.SizeF(410.8369F, 43.18F);
            this.lbPhong.StylePriority.UseForeColor = false;
            this.lbPhong.StylePriority.UseTextAlignment = false;
            this.lbPhong.Text = "77";
            this.lbPhong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(18.44084F, 1.187508F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(207.645F, 43.18F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseForeColor = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "Phòng:";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lb_nv
            // 
            this.lb_nv.Dpi = 254F;
            this.lb_nv.ForeColor = System.Drawing.Color.Blue;
            this.lb_nv.LocationFloat = new DevExpress.Utils.PointFloat(310.5493F, 166.1817F);
            this.lb_nv.Name = "lb_nv";
            this.lb_nv.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_nv.SizeF = new System.Drawing.SizeF(410.8369F, 43.17999F);
            this.lb_nv.StylePriority.UseForeColor = false;
            this.lb_nv.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:#.00}";
            this.lb_nv.Summary = xrSummary1;
            this.lb_nv.Text = "Tú Linh";
            this.lb_nv.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Dpi = 254F;
            this.xrLabel10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel10.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(18.44084F, 166.1817F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(274.049F, 43.17999F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseForeColor = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "Nhân viên :";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(18.44084F, 219.6276F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(274.049F, 43.17998F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseForeColor = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Số tiền:";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lb_sotien
            // 
            this.lb_sotien.Dpi = 254F;
            this.lb_sotien.ForeColor = System.Drawing.Color.Blue;
            this.lb_sotien.LocationFloat = new DevExpress.Utils.PointFloat(310.5493F, 219.6276F);
            this.lb_sotien.Name = "lb_sotien";
            this.lb_sotien.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_sotien.SizeF = new System.Drawing.SizeF(410.8369F, 43.18001F);
            this.lb_sotien.StylePriority.UseForeColor = false;
            this.lb_sotien.StylePriority.UseTextAlignment = false;
            this.lb_sotien.Text = "0";
            this.lb_sotien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Dpi = 254F;
            this.xrLabel11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(18.44084F, 283.7158F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(274.049F, 43.17996F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseForeColor = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "Ghi chú:";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(18.44084F, 51.98754F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(274.049F, 43.18F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseForeColor = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Ngày nhận cọc:";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbNgay
            // 
            this.lbNgay.Dpi = 254F;
            this.lbNgay.ForeColor = System.Drawing.Color.Blue;
            this.lbNgay.LocationFloat = new DevExpress.Utils.PointFloat(310.5493F, 51.98754F);
            this.lbNgay.Name = "lbNgay";
            this.lbNgay.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbNgay.SizeF = new System.Drawing.SizeF(410.8369F, 43.17999F);
            this.lbNgay.StylePriority.UseForeColor = false;
            this.lbNgay.StylePriority.UseTextAlignment = false;
            this.lbNgay.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(18.44084F, 105.3276F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(274.049F, 43.18F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseForeColor = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "Lần nhận cọc:";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbLannhancoc
            // 
            this.lbLannhancoc.Dpi = 254F;
            this.lbLannhancoc.ForeColor = System.Drawing.Color.Blue;
            this.lbLannhancoc.LocationFloat = new DevExpress.Utils.PointFloat(310.5493F, 105.3276F);
            this.lbLannhancoc.Name = "lbLannhancoc";
            this.lbLannhancoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbLannhancoc.SizeF = new System.Drawing.SizeF(410.8369F, 43.18F);
            this.lbLannhancoc.StylePriority.UseForeColor = false;
            this.lbLannhancoc.StylePriority.UseTextAlignment = false;
            this.lbLannhancoc.Text = "0";
            this.lbLannhancoc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 20F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 20F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lb_Header,
            this.xrLine1,
            this.lb_NameBill});
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 178.0116F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 254F;
            this.xrLine1.ForeColor = System.Drawing.Color.Blue;
            this.xrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine1.LineWidth = 3;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 156.6332F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(785.0001F, 13.44083F);
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // lb_NameBill
            // 
            this.lb_NameBill.Dpi = 254F;
            this.lb_NameBill.Font = new System.Drawing.Font("Tahoma", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_NameBill.ForeColor = System.Drawing.Color.Blue;
            this.lb_NameBill.LocationFloat = new DevExpress.Utils.PointFloat(15.32295F, 97.68416F);
            this.lb_NameBill.Multiline = true;
            this.lb_NameBill.Name = "lb_NameBill";
            this.lb_NameBill.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_NameBill.SizeF = new System.Drawing.SizeF(702.9453F, 58.94905F);
            this.lb_NameBill.StylePriority.UseFont = false;
            this.lb_NameBill.StylePriority.UseForeColor = false;
            this.lb_NameBill.StylePriority.UseTextAlignment = false;
            this.lb_NameBill.Text = "PHIẾU THU";
            this.lb_NameBill.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lbFooter,
            this.xrLine5});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 410.5276F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // lbFooter
            // 
            this.lbFooter.Dpi = 254F;
            this.lbFooter.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFooter.ForeColor = System.Drawing.Color.Blue;
            this.lbFooter.LocationFloat = new DevExpress.Utils.PointFloat(42.25331F, 21.58977F);
            this.lbFooter.Multiline = true;
            this.lbFooter.Name = "lbFooter";
            this.lbFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbFooter.SizeF = new System.Drawing.SizeF(702.9451F, 40.42816F);
            this.lbFooter.StylePriority.UseFont = false;
            this.lbFooter.StylePriority.UseForeColor = false;
            this.lbFooter.StylePriority.UseTextAlignment = false;
            this.lbFooter.Text = "Trân trọng cảm ơn quý khách đã đến mua hàng  ";
            this.lbFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLine5
            // 
            this.xrLine5.Dpi = 254F;
            this.xrLine5.ForeColor = System.Drawing.Color.Blue;
            this.xrLine5.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine5.LineWidth = 3;
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(785.0001F, 13.44086F);
            this.xrLine5.StylePriority.UseForeColor = false;
            // 
            // lb_Header
            // 
            this.lb_Header.Dpi = 254F;
            this.lb_Header.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Header.ForeColor = System.Drawing.Color.Blue;
            this.lb_Header.LocationFloat = new DevExpress.Utils.PointFloat(42.25331F, 0F);
            this.lb_Header.Multiline = true;
            this.lb_Header.Name = "lb_Header";
            this.lb_Header.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_Header.SizeF = new System.Drawing.SizeF(710.5649F, 50.27086F);
            this.lb_Header.StylePriority.UseFont = false;
            this.lb_Header.StylePriority.UseForeColor = false;
            this.lb_Header.StylePriority.UseTextAlignment = false;
            this.lb_Header.Text = "Điện Máy Thiên Hòa";
            this.lb_Header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // rpt_Phieuthutien
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.ReportFooter});
            this.Dpi = 254F;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.Margins = new System.Drawing.Printing.Margins(20, 20, 20, 20);
            this.PageHeight = 1600;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.ShowPrintMarginsWarning = false;
            this.SnapGridSize = 31.75F;
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel lb_NameBill;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel lbPhong;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel lb_nv;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel lb_sotien;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel lbFooter;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLabel lbLannhancoc;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel lbNgay;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lbGhichu;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel lb_Header;
    }
}
