﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using System.Drawing.Imaging;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rpt_BCDATCOC : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rpt_BCDATCOC()
        {
            InitializeComponent();
            Setheader();
        }
        private void ConvertHinh(ref PictureBox pic, Byte[] values)
        {
            MemoryStream ms = new MemoryStream(values);
            Bitmap bm = new Bitmap(ms);
            pic.Image = bm;
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH,GIAMDOC,KETOAN,THUKHO" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
            txt_ketoan.Text = dt.Rows[0]["KETOAN"].ToString();
            txt_giamdoc.Text = dt.Rows[0]["GIAMDOC"].ToString();
            txt_thuquy.Text = dt.Rows[0]["THUKHO"].ToString();
            txt_nguoilap.Text = clsGlobal.gsNameOfUserLogin;
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            MAHOADON.DataBindings.Add("Text", DataSource, "MA_HOADON");
            PHONG.DataBindings.Add("Text", DataSource, "Ten_Phong");
            NGAYNHANCOC.DataBindings.Add("Text", DataSource, "NGAYTAO");
            NHANVIEN.DataBindings.Add("Text", DataSource, "Ten_nv");           
            LANNHAN.DataBindings.Add("Text", DataSource, "STT", "{0:#,###0.00}");
            TONGTIEN.DataBindings.Add("Text", DataSource, "SOTIEN", "{0:#,###0.00}");

            
            
            SUMLANNHAN.DataBindings.Add("Text", DataSource, "STT");
            SUMLANNHAN.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0.00}");
            
            SUMTONGTIEN.DataBindings.Add("Text", DataSource, "SOTIEN");
            SUMTONGTIEN.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0.00}");
           
            DataTable dtSource = (DataTable)DataSource;
            
            //if (dtSource.Rows[0]["TYPE"].ToString() == "CH")
            //{
            //    txt_tieude.Text = "(  Cửa hàng: " + dtSource.Rows[0]["NAMETYPE"].ToString() + " )";
            //}
            //else if (dtSource.Rows[0]["TYPE"].ToString() == "NV")
            //{
            //    txt_tieude.Text = "(  Nhân viên: " + dtSource.Rows[0]["NAMETYPE"].ToString() + " )";
            //}
            //else if (dtSource.Rows[0]["TYPE"].ToString() == "HUY")
            //{
            //    txt_tieude.Text = "(HỦY)";
            //}
            txt_tieude1.Text = "( Từ ngày: " + dtSource.Rows[0]["TUNGAY"].ToString() + " - Đến ngày: " + dtSource.Rows[0]["DENNGAY"].ToString() + " )";
        }
        int i = 0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
