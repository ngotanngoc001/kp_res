﻿namespace KP_Report
{
    partial class rptVerapphim
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYTAO1 = new DevExpress.XtraReports.UI.XRLabel();
            this.LOAIVE1 = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYTAO = new DevExpress.XtraReports.UI.XRLabel();
            this.NGUOITAO = new DevExpress.XtraReports.UI.XRLabel();
            this.DONGIA1 = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYCHIEU1 = new DevExpress.XtraReports.UI.XRLabel();
            this.BATDAU1 = new DevExpress.XtraReports.UI.XRLabel();
            this.LOAIVE = new DevExpress.XtraReports.UI.XRLabel();
            this.SOGHE1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TENPHONG1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TENPHIM1 = new DevExpress.XtraReports.UI.XRLabel();
            this.BATDAU = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYCHIEU = new DevExpress.XtraReports.UI.XRLabel();
            this.TENPHONG = new DevExpress.XtraReports.UI.XRLabel();
            this.SOGHE = new DevExpress.XtraReports.UI.XRLabel();
            this.TENPHIM = new DevExpress.XtraReports.UI.XRLabel();
            this.MABARCODE1 = new DevExpress.XtraReports.UI.XRLabel();
            this.DONGIA = new DevExpress.XtraReports.UI.XRLabel();
            this.MABARCODE = new DevExpress.XtraReports.UI.XRLabel();
            this.NGUOITAO1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel13,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel4,
            this.xrLabel3,
            this.NGAYTAO1,
            this.LOAIVE1,
            this.NGAYTAO,
            this.NGUOITAO,
            this.DONGIA1,
            this.NGAYCHIEU1,
            this.BATDAU1,
            this.LOAIVE,
            this.SOGHE1,
            this.TENPHONG1,
            this.TENPHIM1,
            this.BATDAU,
            this.NGAYCHIEU,
            this.TENPHONG,
            this.SOGHE,
            this.TENPHIM,
            this.MABARCODE1,
            this.DONGIA,
            this.MABARCODE,
            this.NGUOITAO1});
            this.Detail.Dpi = 254F;
            this.Detail.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Detail.HeightF = 500F;
            this.Detail.LockedInUserDesigner = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 1, 1, 1, 254F);
            this.Detail.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.StylePriority.UsePadding = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.CanGrow = false;
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.Font = new System.Drawing.Font("Tahoma", 5F);
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(728.6439F, 403.1258F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(177.6807F, 25.00363F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UsePadding = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "Đã gồm VAT 5% ";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel8
            // 
            this.xrLabel8.CanGrow = false;
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.Font = new System.Drawing.Font("Tahoma", 5F);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(15.7186F, 377.0142F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(232.1342F, 25.0036F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UsePadding = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "Đã gồm VAT 5% ";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel7
            // 
            this.xrLabel7.CanGrow = false;
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.Font = new System.Drawing.Font("Tahoma", 6F);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(728.644F, 377.0142F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(177.6807F, 26.11166F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UsePadding = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Giá vé(Price)";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel6
            // 
            this.xrLabel6.CanGrow = false;
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.Font = new System.Drawing.Font("Tahoma", 6F);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(778.6371F, 306.9023F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(127.6876F, 26.11166F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UsePadding = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Rạp(Screen)";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel5
            // 
            this.xrLabel5.CanGrow = false;
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.Font = new System.Drawing.Font("Tahoma", 6F);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(656.9754F, 306.9023F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(109.8212F, 26.11166F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UsePadding = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Ghế(Seat)";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel4
            // 
            this.xrLabel4.CanGrow = false;
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Tahoma", 6F);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(474.0555F, 306.9023F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(136.1334F, 26.11166F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UsePadding = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Ngày(Date)";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel3
            // 
            this.xrLabel3.CanGrow = false;
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Tahoma", 6F);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(304.5132F, 306.9023F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(157.1831F, 26.11166F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UsePadding = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Giờ(Time)";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // NGAYTAO1
            // 
            this.NGAYTAO1.CanGrow = false;
            this.NGAYTAO1.Dpi = 254F;
            this.NGAYTAO1.Font = new System.Drawing.Font("Tahoma", 5F);
            this.NGAYTAO1.LocationFloat = new DevExpress.Utils.PointFloat(15.88571F, 428.1294F);
            this.NGAYTAO1.Name = "NGAYTAO1";
            this.NGAYTAO1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYTAO1.SizeF = new System.Drawing.SizeF(231.9671F, 25.00363F);
            this.NGAYTAO1.StylePriority.UseFont = false;
            this.NGAYTAO1.StylePriority.UsePadding = false;
            this.NGAYTAO1.StylePriority.UseTextAlignment = false;
            this.NGAYTAO1.Text = "21/12/2012 20:30";
            this.NGAYTAO1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // LOAIVE1
            // 
            this.LOAIVE1.CanGrow = false;
            this.LOAIVE1.Dpi = 254F;
            this.LOAIVE1.Font = new System.Drawing.Font("Tahoma", 5F);
            this.LOAIVE1.LocationFloat = new DevExpress.Utils.PointFloat(15.88568F, 352.0103F);
            this.LOAIVE1.Name = "LOAIVE1";
            this.LOAIVE1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.LOAIVE1.SizeF = new System.Drawing.SizeF(231.9672F, 25.00363F);
            this.LOAIVE1.StylePriority.UseFont = false;
            this.LOAIVE1.StylePriority.UsePadding = false;
            this.LOAIVE1.StylePriority.UseTextAlignment = false;
            this.LOAIVE1.Text = "VÉ MỜI VIP CÓ COMBO";
            this.LOAIVE1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // NGAYTAO
            // 
            this.NGAYTAO.CanGrow = false;
            this.NGAYTAO.Dpi = 254F;
            this.NGAYTAO.Font = new System.Drawing.Font("Tahoma", 5F);
            this.NGAYTAO.LocationFloat = new DevExpress.Utils.PointFloat(728.644F, 428.1294F);
            this.NGAYTAO.Name = "NGAYTAO";
            this.NGAYTAO.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYTAO.SizeF = new System.Drawing.SizeF(177.6807F, 25.00357F);
            this.NGAYTAO.StylePriority.UseFont = false;
            this.NGAYTAO.StylePriority.UsePadding = false;
            this.NGAYTAO.StylePriority.UseTextAlignment = false;
            this.NGAYTAO.Text = "21/12/2012 20:30";
            this.NGAYTAO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // NGUOITAO
            // 
            this.NGUOITAO.CanGrow = false;
            this.NGUOITAO.Dpi = 254F;
            this.NGUOITAO.Font = new System.Drawing.Font("Tahoma", 5F);
            this.NGUOITAO.LocationFloat = new DevExpress.Utils.PointFloat(304.5132F, 428.1294F);
            this.NGUOITAO.Name = "NGUOITAO";
            this.NGUOITAO.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGUOITAO.SizeF = new System.Drawing.SizeF(424.1307F, 25.0036F);
            this.NGUOITAO.StylePriority.UseFont = false;
            this.NGUOITAO.StylePriority.UsePadding = false;
            this.NGUOITAO.StylePriority.UseTextAlignment = false;
            this.NGUOITAO.Text = "Nguyễn Văn Thông";
            this.NGUOITAO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DONGIA1
            // 
            this.DONGIA1.CanGrow = false;
            this.DONGIA1.Dpi = 254F;
            this.DONGIA1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.DONGIA1.LocationFloat = new DevExpress.Utils.PointFloat(728.644F, 333.014F);
            this.DONGIA1.Name = "DONGIA1";
            this.DONGIA1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.DONGIA1.SizeF = new System.Drawing.SizeF(177.6807F, 44.00009F);
            this.DONGIA1.StylePriority.UseFont = false;
            this.DONGIA1.StylePriority.UsePadding = false;
            this.DONGIA1.StylePriority.UseTextAlignment = false;
            this.DONGIA1.Text = "500,000";
            this.DONGIA1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // NGAYCHIEU1
            // 
            this.NGAYCHIEU1.CanGrow = false;
            this.NGAYCHIEU1.Dpi = 254F;
            this.NGAYCHIEU1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.NGAYCHIEU1.LocationFloat = new DevExpress.Utils.PointFloat(429.3249F, 266.634F);
            this.NGAYCHIEU1.Name = "NGAYCHIEU1";
            this.NGAYCHIEU1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYCHIEU1.SizeF = new System.Drawing.SizeF(270.2882F, 40.26825F);
            this.NGAYCHIEU1.StylePriority.UseFont = false;
            this.NGAYCHIEU1.StylePriority.UsePadding = false;
            this.NGAYCHIEU1.StylePriority.UseTextAlignment = false;
            this.NGAYCHIEU1.Text = "21/12/2012";
            this.NGAYCHIEU1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // BATDAU1
            // 
            this.BATDAU1.CanGrow = false;
            this.BATDAU1.Dpi = 254F;
            this.BATDAU1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.BATDAU1.LocationFloat = new DevExpress.Utils.PointFloat(304.5132F, 266.634F);
            this.BATDAU1.Name = "BATDAU1";
            this.BATDAU1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.BATDAU1.SizeF = new System.Drawing.SizeF(124.8116F, 40.26823F);
            this.BATDAU1.StylePriority.UseFont = false;
            this.BATDAU1.StylePriority.UsePadding = false;
            this.BATDAU1.StylePriority.UseTextAlignment = false;
            this.BATDAU1.Text = "20:30";
            this.BATDAU1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // LOAIVE
            // 
            this.LOAIVE.CanGrow = false;
            this.LOAIVE.Dpi = 254F;
            this.LOAIVE.Font = new System.Drawing.Font("Tahoma", 8F);
            this.LOAIVE.LocationFloat = new DevExpress.Utils.PointFloat(304.5132F, 333.014F);
            this.LOAIVE.Name = "LOAIVE";
            this.LOAIVE.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.LOAIVE.SizeF = new System.Drawing.SizeF(424.1307F, 95.11539F);
            this.LOAIVE.StylePriority.UseFont = false;
            this.LOAIVE.StylePriority.UsePadding = false;
            this.LOAIVE.StylePriority.UseTextAlignment = false;
            this.LOAIVE.Text = "VÉ MỜI VIP CÓ COMBO  vbvb";
            this.LOAIVE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // SOGHE1
            // 
            this.SOGHE1.CanGrow = false;
            this.SOGHE1.Dpi = 254F;
            this.SOGHE1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.SOGHE1.LocationFloat = new DevExpress.Utils.PointFloat(699.6133F, 266.634F);
            this.SOGHE1.Name = "SOGHE1";
            this.SOGHE1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.SOGHE1.SizeF = new System.Drawing.SizeF(97.56073F, 40.26825F);
            this.SOGHE1.StylePriority.UseFont = false;
            this.SOGHE1.StylePriority.UsePadding = false;
            this.SOGHE1.StylePriority.UseTextAlignment = false;
            this.SOGHE1.Text = "F22";
            this.SOGHE1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TENPHONG1
            // 
            this.TENPHONG1.CanGrow = false;
            this.TENPHONG1.Dpi = 254F;
            this.TENPHONG1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.TENPHONG1.LocationFloat = new DevExpress.Utils.PointFloat(797.174F, 266.634F);
            this.TENPHONG1.Name = "TENPHONG1";
            this.TENPHONG1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENPHONG1.SizeF = new System.Drawing.SizeF(89.41669F, 40.26825F);
            this.TENPHONG1.StylePriority.UseFont = false;
            this.TENPHONG1.StylePriority.UsePadding = false;
            this.TENPHONG1.StylePriority.UseTextAlignment = false;
            this.TENPHONG1.Text = "03";
            this.TENPHONG1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TENPHIM1
            // 
            this.TENPHIM1.CanGrow = false;
            this.TENPHIM1.Dpi = 254F;
            this.TENPHIM1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENPHIM1.LocationFloat = new DevExpress.Utils.PointFloat(303.7164F, 148.7939F);
            this.TENPHIM1.Name = "TENPHIM1";
            this.TENPHIM1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENPHIM1.SizeF = new System.Drawing.SizeF(602.6083F, 117.8402F);
            this.TENPHIM1.StylePriority.UseFont = false;
            this.TENPHIM1.StylePriority.UsePadding = false;
            this.TENPHIM1.StylePriority.UseTextAlignment = false;
            this.TENPHIM1.Text = "Ám Ảnh Bóng Đêm - Out Of The Dark ";
            this.TENPHIM1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // BATDAU
            // 
            this.BATDAU.CanGrow = false;
            this.BATDAU.Dpi = 254F;
            this.BATDAU.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.BATDAU.LocationFloat = new DevExpress.Utils.PointFloat(15.88571F, 253.9749F);
            this.BATDAU.Name = "BATDAU";
            this.BATDAU.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.BATDAU.SizeF = new System.Drawing.SizeF(93.8331F, 30.90329F);
            this.BATDAU.StylePriority.UseFont = false;
            this.BATDAU.StylePriority.UsePadding = false;
            this.BATDAU.StylePriority.UseTextAlignment = false;
            this.BATDAU.Text = "20:30";
            this.BATDAU.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // NGAYCHIEU
            // 
            this.NGAYCHIEU.CanGrow = false;
            this.NGAYCHIEU.Dpi = 254F;
            this.NGAYCHIEU.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Bold);
            this.NGAYCHIEU.LocationFloat = new DevExpress.Utils.PointFloat(78.24252F, 253.9749F);
            this.NGAYCHIEU.Name = "NGAYCHIEU";
            this.NGAYCHIEU.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYCHIEU.SizeF = new System.Drawing.SizeF(169.6103F, 30.9032F);
            this.NGAYCHIEU.StylePriority.UseFont = false;
            this.NGAYCHIEU.StylePriority.UsePadding = false;
            this.NGAYCHIEU.StylePriority.UseTextAlignment = false;
            this.NGAYCHIEU.Text = "21/12/2012";
            this.NGAYCHIEU.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TENPHONG
            // 
            this.TENPHONG.CanGrow = false;
            this.TENPHONG.Dpi = 254F;
            this.TENPHONG.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.TENPHONG.LocationFloat = new DevExpress.Utils.PointFloat(150.2834F, 284.8781F);
            this.TENPHONG.Name = "TENPHONG";
            this.TENPHONG.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENPHONG.SizeF = new System.Drawing.SizeF(97.56943F, 33.5661F);
            this.TENPHONG.StylePriority.UseFont = false;
            this.TENPHONG.StylePriority.UsePadding = false;
            this.TENPHONG.StylePriority.UseTextAlignment = false;
            this.TENPHONG.Text = "03";
            this.TENPHONG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // SOGHE
            // 
            this.SOGHE.CanGrow = false;
            this.SOGHE.Dpi = 254F;
            this.SOGHE.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.SOGHE.LocationFloat = new DevExpress.Utils.PointFloat(15.88571F, 284.8781F);
            this.SOGHE.Name = "SOGHE";
            this.SOGHE.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.SOGHE.SizeF = new System.Drawing.SizeF(115.7288F, 33.56607F);
            this.SOGHE.StylePriority.UseFont = false;
            this.SOGHE.StylePriority.UsePadding = false;
            this.SOGHE.StylePriority.UseTextAlignment = false;
            this.SOGHE.Text = "F22";
            this.SOGHE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TENPHIM
            // 
            this.TENPHIM.CanGrow = false;
            this.TENPHIM.Dpi = 254F;
            this.TENPHIM.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.TENPHIM.LocationFloat = new DevExpress.Utils.PointFloat(15.88568F, 148.7939F);
            this.TENPHIM.Name = "TENPHIM";
            this.TENPHIM.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENPHIM.SizeF = new System.Drawing.SizeF(231.9671F, 105.181F);
            this.TENPHIM.StylePriority.UseFont = false;
            this.TENPHIM.StylePriority.UsePadding = false;
            this.TENPHIM.StylePriority.UseTextAlignment = false;
            this.TENPHIM.Text = "Ám Ảnh Bóng Đêm - Out Of The Dark";
            this.TENPHIM.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // MABARCODE1
            // 
            this.MABARCODE1.CanGrow = false;
            this.MABARCODE1.Dpi = 254F;
            this.MABARCODE1.Font = new System.Drawing.Font("Tahoma", 5F);
            this.MABARCODE1.LocationFloat = new DevExpress.Utils.PointFloat(550F, 123.7903F);
            this.MABARCODE1.Name = "MABARCODE1";
            this.MABARCODE1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.MABARCODE1.SizeF = new System.Drawing.SizeF(134.132F, 25.00363F);
            this.MABARCODE1.StylePriority.UseFont = false;
            this.MABARCODE1.StylePriority.UsePadding = false;
            this.MABARCODE1.StylePriority.UseTextAlignment = false;
            this.MABARCODE1.Text = "12345678";
            this.MABARCODE1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // DONGIA
            // 
            this.DONGIA.CanGrow = false;
            this.DONGIA.Dpi = 254F;
            this.DONGIA.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.DONGIA.LocationFloat = new DevExpress.Utils.PointFloat(150.2834F, 318.4442F);
            this.DONGIA.Name = "DONGIA";
            this.DONGIA.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.DONGIA.SizeF = new System.Drawing.SizeF(97.56943F, 33.5661F);
            this.DONGIA.StylePriority.UseFont = false;
            this.DONGIA.StylePriority.UsePadding = false;
            this.DONGIA.StylePriority.UseTextAlignment = false;
            this.DONGIA.Text = "500,000";
            this.DONGIA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // MABARCODE
            // 
            this.MABARCODE.CanGrow = false;
            this.MABARCODE.Dpi = 254F;
            this.MABARCODE.Font = new System.Drawing.Font("Tahoma", 5F);
            this.MABARCODE.LocationFloat = new DevExpress.Utils.PointFloat(113.7208F, 123.7903F);
            this.MABARCODE.Name = "MABARCODE";
            this.MABARCODE.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.MABARCODE.SizeF = new System.Drawing.SizeF(134.132F, 25.0036F);
            this.MABARCODE.StylePriority.UseFont = false;
            this.MABARCODE.StylePriority.UsePadding = false;
            this.MABARCODE.StylePriority.UseTextAlignment = false;
            this.MABARCODE.Text = "12345678";
            this.MABARCODE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // NGUOITAO1
            // 
            this.NGUOITAO1.CanGrow = false;
            this.NGUOITAO1.Dpi = 254F;
            this.NGUOITAO1.Font = new System.Drawing.Font("Tahoma", 5F);
            this.NGUOITAO1.LocationFloat = new DevExpress.Utils.PointFloat(15.88568F, 403.1258F);
            this.NGUOITAO1.Name = "NGUOITAO1";
            this.NGUOITAO1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGUOITAO1.SizeF = new System.Drawing.SizeF(231.9672F, 25.00363F);
            this.NGUOITAO1.StylePriority.UseFont = false;
            this.NGUOITAO1.StylePriority.UsePadding = false;
            this.NGUOITAO1.StylePriority.UseTextAlignment = false;
            this.NGUOITAO1.Text = "Nguyễn Văn Thông";
            this.NGUOITAO1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // formattingRule1
            // 
            this.formattingRule1.Name = "formattingRule1";
            // 
            // rptVerapphim
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Dpi = 254F;
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
            this.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 254F);
            this.PageHeight = 500;
            this.PageWidth = 930;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.ShowPrintMarginsWarning = false;
            this.ShowPrintStatusDialog = false;
            this.SnapGridSize = 1F;
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel MABARCODE;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        private DevExpress.XtraReports.UI.XRLabel DONGIA1;
        private DevExpress.XtraReports.UI.XRLabel NGAYCHIEU1;
        private DevExpress.XtraReports.UI.XRLabel BATDAU1;
        private DevExpress.XtraReports.UI.XRLabel SOGHE1;
        private DevExpress.XtraReports.UI.XRLabel TENPHONG1;
        private DevExpress.XtraReports.UI.XRLabel TENPHIM1;
        private DevExpress.XtraReports.UI.XRLabel NGUOITAO;
        private DevExpress.XtraReports.UI.XRLabel NGAYTAO;
        private DevExpress.XtraReports.UI.XRLabel LOAIVE;
        private DevExpress.XtraReports.UI.XRLabel LOAIVE1;
        private DevExpress.XtraReports.UI.XRLabel BATDAU;
        private DevExpress.XtraReports.UI.XRLabel NGAYCHIEU;
        private DevExpress.XtraReports.UI.XRLabel TENPHONG;
        private DevExpress.XtraReports.UI.XRLabel SOGHE;
        private DevExpress.XtraReports.UI.XRLabel TENPHIM;
        private DevExpress.XtraReports.UI.XRLabel DONGIA;
        private DevExpress.XtraReports.UI.XRLabel NGUOITAO1;
        private DevExpress.XtraReports.UI.XRLabel NGAYTAO1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel MABARCODE1;
    }
}
