﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptDS_KHTT : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptDS_KHTT()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            txt_tieude.DataBindings.Add("Text", DataSource, "HEARDER");
            
            DT_MADOITUONG.DataBindings.Add("Text", DataSource, "MA");
            DT_HOTEN.DataBindings.Add("Text", DataSource, "TEN");
            DT_NGAYSINH.DataBindings.Add("Text", DataSource, "NGAYSINH", "{0:dd/MM/yyyy}");
            GT.DataBindings.Add("Text", DataSource, "GIOITINH1");
            DT_SOCMND.DataBindings.Add("Text", DataSource, "CMND");
            DT_DIACHI.DataBindings.Add("Text", DataSource, "DIACHI");
            DT_DIENTHOAI.DataBindings.Add("Text", DataSource, "DIENTHOAI");
            SoDiem.DataBindings.Add("Text", DataSource, "SoDiem", "{0:#,###0}");
            TenCapDo.DataBindings.Add("Text", DataSource, "TenCapDo");
            NgayThamGia.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy}");
        }

        int i = 0;
        
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
