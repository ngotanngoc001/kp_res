﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
namespace KP_Report
{
    /// <summary>
    /// Report Ve
    /// </summary>
    public partial class rptVeXeOto_DaiLy_L1 : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report Ve
        /// </summary>
        public rptVeXeOto_DaiLy_L1()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        /// 
        

        public void BindData()
        {
            
            MABARCODE1.DataBindings.Add("Text", DataSource, "MABARCODE");
            
            TENTUYEN1.DataBindings.Add("Text", DataSource, "TENTUYEN");
            
            TENDOITUONG1.DataBindings.Add("Text", DataSource, "TENDOITUONG");

            BIENSO1.DataBindings.Add("Text", DataSource, "BIENSO");
            BIENSO1.DataBindings.Add("Text", DataSource, "BIENSO");
            TENHANHKHACH.DataBindings.Add("Text", DataSource, "TENHANHKHACH");
            
            NGAYDI1.DataBindings.Add("Text", DataSource, "NGAYDI", "{0:dd/MM/yyyy}");
            
            GIODI1.DataBindings.Add("Text", DataSource, "GIODI");
            NGUOITAO.DataBindings.Add("Text", DataSource, "NGUOITAO");
            NGAYTAO.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy HH : mm}");
            
            DataTable dt = (DataTable)DataSource;
            TENKHUVUC1.DataBindings.Add("Text", DataSource, "TENKHUVUC");
        }

        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
    }
}
