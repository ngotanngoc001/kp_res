﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBiennhanguihang : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBiennhanguihang()
        {
            InitializeComponent();
           Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            MAPHIEU.DataBindings.Add("Text", DataSource, "MABARCODE");
            
            TENTUYEN.DataBindings.Add("Text", DataSource, "TENTUYEN");
            TENNGUOIGUI.DataBindings.Add("Text", DataSource, "TENNGUOIGUI");
            CMNDNGUOIGUI.DataBindings.Add("Text", DataSource, "CMNDGUI");
            DIENTHOAINGUOIGUI.DataBindings.Add("Text", DataSource, "DIENTHOAIGUI");
            TENNGUOINHAN.DataBindings.Add("Text", DataSource, "TENNGUOINHAN");
            CMNDNGUOINHAN.DataBindings.Add("Text", DataSource, "CMNDNHAN");
            DIENTHOAINGUOINHAN.DataBindings.Add("Text", DataSource, "DIENTHOAINHAN");
            GHICHU.DataBindings.Add("Text", DataSource, "HANGHOA");
            SOLUONG.DataBindings.Add("Text", DataSource, "SOLUONG");
            GIAVE.DataBindings.Add("Text", DataSource, "GIAVE", "{0:n0}");
          
            NGUOITAO.DataBindings.Add("Text", DataSource, "NGUOITAO");
            NGAYTAO1.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy HH : mm}");

            DataTable dt = (DataTable)DataSource;
            //MABARCODE.DataBindings.Add("Text", DataSource, "MABARCODE", "*{0}*");
            MABARCODE.Text = dt.Rows[0]["MABARCODE"].ToString();

            if (dt.Rows[0]["DATHANHTOAN"].ToString() == "True")
            {
                DATHANHTOAN.Text ="Đã thanh thoán";
            }
            else
            {
                DATHANHTOAN.Text = "Thanh toán khi nhận";
            }
        }
       
    }
}
