﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBCSoLuongVe_TungNam : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report 
        /// </summary>
        public rptBCSoLuongVe_TungNam()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        int i=0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            TENTUYEN.DataBindings.Add("Text", DataSource, "TUYEN");
            NGAYDI.DataBindings.Add("Text", DataSource, "NGAY");

            NHANVIEN.DataBindings.Add("Text", DataSource, "nguoitao");
            VEBAN.DataBindings.Add("Text", DataSource, "soluong_ban", "{0:n0}");
            VEINLAI.DataBindings.Add("Text", DataSource, "soluong_inlai", "{0:n0}");
            VETEST.DataBindings.Add("Text", DataSource, "soluong_intest", "{0:n0}");
            VETONG.DataBindings.Add("Text", DataSource, "soluong_tong", "{0:n0}");

            TONGVEBAN.DataBindings.Add("Text", DataSource, "soluong_ban");
            TONGVEBAN.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
            TONGVEINLAI.DataBindings.Add("Text", DataSource, "soluong_inlai");
            TONGVEINLAI.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
            TONGVETEST.DataBindings.Add("Text", DataSource, "soluong_intest");
            TONGVETEST.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
            TONGVETONG.DataBindings.Add("Text", DataSource, "soluong_tong");
            TONGVETONG.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
        }
    }
}
