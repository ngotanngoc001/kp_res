﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rpt_BaoCaoHangXuatKho : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rpt_BaoCaoHangXuatKho()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            MAVACH.DataBindings.Add("Text", DataSource, "MAVACH");
            TEN_HANGHOA.DataBindings.Add("Text", DataSource, "TEN_HANGHOA");
            TEN_DONVITINH.DataBindings.Add("Text", DataSource, "TEN_DONVITINH");
            SL.DataBindings.Add("Text", DataSource, "SL", "{0:#,###0}");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:#,###0}");
            CK.DataBindings.Add("Text", DataSource, "CK", "{0:#,###0}%");
            THANHTIEN.DataBindings.Add("Text", DataSource, "THANHTIENSCK", "{0:#,###0}");

            TONGSL.DataBindings.Add("Text", DataSource, "SL");
            TONGSL.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");
            TONGTHANHTIEN.DataBindings.Add("Text", DataSource, "THANHTIENSCK");
            TONGTHANHTIEN.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");

            DataTable dtSource = (DataTable)DataSource;
            txt_tieude.Text = "( Từ ngày: " + dtSource.Rows[0]["TUNGAY"].ToString() + " - Đến ngày: " + dtSource.Rows[0]["DENNGAY"].ToString() + " )";
        }

        int i = 0;
        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i = 0;
        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
