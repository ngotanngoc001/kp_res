﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace KP_Report
{
    /// <summary>
    /// Report Ve
    /// </summary>
    public partial class rptInMaKH : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report Ve
        /// </summary>
        public rptInMaKH()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
           HoTenKH.DataBindings.Add("Text", DataSource, "TENKH");
           MAKH.DataBindings.Add("Text", DataSource, "MAKH", "*{0}*");
           NGAYSINH.DataBindings.Add("Text", DataSource, "NGAYSINH");
        }
    }
}
