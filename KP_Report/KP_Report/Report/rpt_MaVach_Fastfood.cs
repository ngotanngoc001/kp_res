﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.IO;
using System.Windows.Forms;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report Ve
    /// </summary>
    public partial class rpt_MaVach_Fastfood : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report Ve
        /// </summary>
        public rpt_MaVach_Fastfood()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            String sSQL = "";
            sSQL = "";
            sSQL += "SELECT TENCONGTY, SODIENTHOAI" + "\n";
            sSQL += "FROM SYS_CONFIGREPORT" + "\n";
            DataTable myDT = clsMain.ReturnDataTable(sSQL);

            TENCONGTY.Text = myDT.Rows[0]["TENCONGTY"].ToString();
            SODIENTHOAI.Text = "Hotline:" + myDT.Rows[0]["SODIENTHOAI"].ToString();
        }

        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {

            HANGHOA.DataBindings.Add("Text", DataSource, "TEN_HANGHOA");
            DataTable dtSource = (DataTable)DataSource;
            if(dtSource .Rows.Count > 0)
            {
                //BAN.Text = dtSource.Rows[0]["BAN"].ToString();
                NGAY.Text = dtSource.Rows[0]["NGAY_HOADON"].ToString();
                //string sSoHD = dtSource.Rows[0]["MA_HOADON"].ToString();
                Number.Text = dtSource.Rows[0]["Number"].ToString();
                YEUCAUTHEM.Text = dtSource.Rows[0]["YEUCAUTHEM"].ToString();

                float sPrice = 0f;
                for (int i = 0; i < dtSource.Rows.Count; i++)
                    sPrice += float.Parse(dtSource.Rows[i]["DON_GIA"].ToString()) * float.Parse(dtSource.Rows[i]["SOLUONG"].ToString());

                GIA.Text = "Tổng:" + String.Format("{0:#,###0}", sPrice);
            }

            
            //BAN.DataBindings.Add("Text", DataSource, "MA_HOADON");
            //HANGHOA.DataBindings.Add("Text", DataSource, "TEN_HANGHOA");
            //GIA.DataBindings.Add("Text", DataSource, "NGAY_HOADON");
        }

        
    }
}
