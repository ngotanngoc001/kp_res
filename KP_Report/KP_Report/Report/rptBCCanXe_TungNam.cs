﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBCCanXe_TungNam : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBCCanXe_TungNam()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        int i=0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            TENTUYEN1.DataBindings.Add("Text", DataSource, "TUYEN");
            NGAYDI1.DataBindings.Add("Text", DataSource, "NGAY");

            MAPHIEU.DataBindings.Add("Text", DataSource, "MAPHIEU");
            SOXE.DataBindings.Add("Text", DataSource, "SOXE");
            TENTUYEN.DataBindings.Add("Text", DataSource, "TENTUYEN");
            NGAYDI.DataBindings.Add("Text", DataSource, "NGAYDI", "{0:dd/MM/yyyy}");
            GIODI.DataBindings.Add("Text", DataSource, "GIODI");
            TL_THIETKE.DataBindings.Add("Text", DataSource, "TL_THIETKE", "{0:n0}");
            TL_BANTHAN.DataBindings.Add("Text", DataSource, "TL_BANTHAN", "{0:n0}");
            TL_XEHANG.DataBindings.Add("Text", DataSource, "TL_XEHANG", "{0:n0}");
            TL_QUATAI.DataBindings.Add("Text", DataSource, "TL_QUATAI", "{0:n0}");
            TT_QUATAI.DataBindings.Add("Text", DataSource, "TT_QUATAI", "{0:n0}");
            TENKHUVUC.DataBindings.Add("Text", DataSource, "TENKHUVUC");
            CD_COSO.DataBindings.Add("Text", DataSource, "CD_COSO", "{0:n0}");
            CD_QUAKHO.DataBindings.Add("Text", DataSource, "CD_QUAKHO", "{0:n0}");
            TT_QUAKHO.DataBindings.Add("Text", DataSource, "TT_QUAKHO", "{0:n0}");
            TT_PHUTHU.DataBindings.Add("Text", DataSource, "TT_PHUTHU", "{0:n0}");
            NGUOITAO.DataBindings.Add("Text", DataSource, "NGUOITAO");
            NGAYTAO.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy}");

            TONG_TT_QUATAI.DataBindings.Add("Text", DataSource, "TT_QUATAI");
            TONG_TT_QUATAI.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
            TONG_TT_QUAKHO.DataBindings.Add("Text", DataSource, "TT_QUAKHO");
            TONG_TT_QUAKHO.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
            TONG_TT_PHUTHU.DataBindings.Add("Text", DataSource, "TT_PHUTHU");
            TONG_TT_PHUTHU.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
        }
    }
}
