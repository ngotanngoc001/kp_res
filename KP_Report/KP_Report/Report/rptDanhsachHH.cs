﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptDanhsachHH : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptDanhsachHH()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }

        int i;
        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i = 0;
        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            TENTUYEN.DataBindings.Add("Text", DataSource, "TENTUYEN");
            NGAYDI.DataBindings.Add("Text", DataSource, "NGAYDI", "{0:dd/MM/yyyy}");
            GIODI.DataBindings.Add("Text", DataSource, "GIODI");
            TENDOITUONG.DataBindings.Add("Text", DataSource, "TENDOITUONG");
            HANGHOA.DataBindings.Add("Text", DataSource, "HANGHOA");
            TENNGUOIGUI.DataBindings.Add("Text", DataSource, "TENNGUOIGUI");
            CMNDNGUOIGUI.DataBindings.Add("Text", DataSource, "CMNDGUI");
            DIENTHOAINGUOIGUI.DataBindings.Add("Text", DataSource, "DIENTHOAIGUI");
            TENNGUOINHAN.DataBindings.Add("Text", DataSource, "TENNGUOINHAN");
            CMNDNGUOINHAN.DataBindings.Add("Text", DataSource, "CMNDNHAN");
            DIENTHOAINGUOINHAN.DataBindings.Add("Text", DataSource, "DIENTHOAINHAN");
            MABARCODE.DataBindings.Add("Text", DataSource, "MABARCODE");
            GIAVE.DataBindings.Add("Text", DataSource, "GIAVE","{0:n0}");
            SOLUONG.DataBindings.Add("Text", DataSource, "SOLUONG");
            DATHANHTOAN.DataBindings.Add("Text", DataSource, "DATHANHTOAN");
            chkTHANHTOAN.DataBindings.Add("Checked", DataSource, "DATHANHTOAN");
        }

        private void DATHANHTOAN_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //XRTableCell cell = new XRTableCell ();
            //XRCheckBox check = new XRCheckBox ();
            //cell = (XRTableCell) sender;
            //check = (XRCheckBox ) cell.Controls[0];

            //if ( this .GetCurrentColumnValue ("DATHANHTOAN").ToString () =="true" )
            //{
            //}
            //else
            //{
            //}

        //      Dim cell As XRTableCell
        //Dim check As XRCheckBox

        //cell = CType(sender, XRTableCell)
        //check = CType(cell.Controls(0), XRCheckBox)

        //If (Me.GetCurrentColumnValue("Discontinued") = True) Then
        //    check.Left = cell.Width / 2 - check.Width / 2
        //Else
        //    check.Left = cell.Width - check.Width
        //End If
        }
    }
}
