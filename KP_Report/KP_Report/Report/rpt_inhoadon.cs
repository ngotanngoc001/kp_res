﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using System.Data;
using KP_UserManagement;


namespace KP_Report
{
    public partial class rpt_inhoadon : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// checkBill
        /// </summary>
        public int checkBill=0;
        /// <summary>
        /// rpt_inhoadon
        /// </summary>
        public rpt_inhoadon()
        {
            InitializeComponent();                      
            Setheader();            
            

        }
        /// <summary>
        /// BindData
        /// </summary>
        public void BindData()
        {
            lb_NameBill.DataBindings.Add("Text", DataSource, "Ten_HoaDon");
            //if (lb_NameBill.Text == "")
            //{
            //    lb_NameBill.Text = "Hóa Đơn Bán Lẻ";
            //}            
            cl_STT.DataBindings.Add("Text", DataSource, "STT");
            cl_HangHoa.DataBindings.Add("Text", DataSource, "TEN_HANGHOA");
            cl_giaban.DataBindings.Add("Text", DataSource, "GIABAN", "{0:#,###0}");
            cl_soluong.DataBindings.Add("Text", DataSource, "SOLUONG");
            //cl_vat.DataBindings.Add("Text", DataSource, "HH_VAT", "VAT: {0} %");
            cl_thanhtien.DataBindings.Add("Text", DataSource, "Thanh_Tien", "{0:#,###0}");

            lb_giamgia.DataBindings.Add("Text", DataSource, "Giam_Gia", "{0:#,###0}");
            lb_tongcong.DataBindings.Add("Text", DataSource, "Tong_Cong" , "{0:#,###0}");
            lb_tienkhachtra.DataBindings.Add("Text", DataSource, "Tien_KhachTra", "{0:#,###0}");
            lb_conlai.DataBindings.Add("Text", DataSource, "Tien_TraKhach", "{0:#,###0}");

            //header
            lb_Quay.DataBindings.Add("Text", DataSource, "Ma_Quay");
            lb_mahoadon.DataBindings.Add("Text", DataSource, "Ma_HoaDon");
            lb_MaHD.DataBindings.Add("Text", DataSource, "So_HoaDon","*{0}*");
            lb_nhanvien.DataBindings.Add("Text", DataSource, "TenNV");
            lb_ngay.DataBindings.Add("Text", DataSource, "ngayHD");        
            
        } 

         private void Setheader()
        {
            string sSQL = "";
            sSQL += " Select TENCONGTY, DIACHI, SODIENTHOAI, SOFAX, MAUSOPHIEUHD, KYHIEU,EMAIL, SOTAIKHOAN, MASOTHUE, HINHANH" + " \n ";
            sSQL += " From SYS_CONFIGREPORT" + " \n ";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            lb_TenCty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            lb_MaSoTheu.Text = "Mã số thuế : " + dt.Rows[0]["MASOTHUE"].ToString();
            lb_DiaChiCty.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            lb_Dt.Text = "Điện thọai : " + dt.Rows[0]["SODIENTHOAI"].ToString();            
            //Logo.BackgroundImage = Image.FromFile _(System.Environment.GetFolderPath _(System.Environment.SpecialFolder.Personal) _& "\Image.gif")           
        }      
    }
}
