﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using System.Drawing.Imaging;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBaoCaoTongHopBanHangKH : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBaoCaoTongHopBanHangKH()
        {
            InitializeComponent();
            Setheader();
        }
        private void ConvertHinh(ref PictureBox pic, Byte[] values)
        {
            MemoryStream ms = new MemoryStream(values);
            Bitmap bm = new Bitmap(ms);
            pic.Image = bm;
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            LOAIKH.DataBindings.Add("Text", DataSource, "LOAIKH");
            MAKH.DataBindings.Add("Text", DataSource, "MAKH");
            TENKH.DataBindings.Add("Text", DataSource, "TENKH");
            CAPDAILY.DataBindings.Add("Text", DataSource, "CAPDAILY");
            CHIETKHAU.DataBindings.Add("Text", DataSource, "CHIETKHAU");
            VELOAI1.DataBindings.Add("Text", DataSource, "VELOAI1", "{0:N0}");
            VELOAI2.DataBindings.Add("Text", DataSource, "VELOAI2", "{0:N0}");
            TONGSOVE.DataBindings.Add("Text", DataSource, "TONGSOVE", "{0:N0}");
            TIENLOAI1.DataBindings.Add("Text", DataSource, "TIENLOAI1", "{0:N0}");
            TIENLOAI2.DataBindings.Add("Text", DataSource, "TIENLOAI2", "{0:N0}");
            TONGTIEN.DataBindings.Add("Text", DataSource, "TONGTIEN", "{0:N0}");
            NHANVIEN.DataBindings.Add("Text", DataSource, "NHANVIEN");



            TONGVELOAI1.DataBindings.Add("Text", DataSource, "VELOAI1");
            TONGVELOAI1.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:N0}");
            TONGVELOAI2.DataBindings.Add("Text", DataSource, "VELOAI2");
            TONGVELOAI2.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:N0}");
            TONGTONGSOVE.DataBindings.Add("Text", DataSource, "TONGSOVE");
            TONGTONGSOVE.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:N0}");
            TONGTIENLOAI1.DataBindings.Add("Text", DataSource, "TIENLOAI1");
            TONGTIENLOAI1.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:N0}");
            TONGTIENLOAI2.DataBindings.Add("Text", DataSource, "TIENLOAI2");
            TONGTIENLOAI2.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:N0}");
            TONGTONGTIEN.DataBindings.Add("Text", DataSource, "TONGTIEN");
            TONGTONGTIEN.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:N0}");

            DataTable dtSource = (DataTable)DataSource;
            txt_tieude1.Text = "( Từ ngày: " + dtSource.Rows[0]["TUNGAY"].ToString() + " - Đến ngày: " + dtSource.Rows[0]["DENNGAY"].ToString() + " )";
            txt_tieude.Text = dtSource.Rows[0]["HEARDER"].ToString();
        }
        int i = 0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
