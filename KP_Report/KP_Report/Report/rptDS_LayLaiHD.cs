﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptDS_LayLaiHD : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptDS_LayLaiHD()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            lblTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            lblDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            lblDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            MA_HOADON.DataBindings.Add("Text", DataSource, "MA_HOADON");
            TEN_BAN.DataBindings.Add("Text", DataSource, "TEN_BAN");
            TENNHANVIEN.DataBindings.Add("Text", DataSource, "TENNHANVIEN");
            NGAY_HUY.DataBindings.Add("Text", DataSource, "NGAY_LAYLAI", "{0:dd/MM/yyyy HH:mm:ss}");
            TONGTIEN.DataBindings.Add("Text", DataSource, "TONGTIEN", "{0:#,###0}");
            GHICHU.DataBindings.Add("Text", DataSource, "GHICHU");
            _TONGTIEN.DataBindings.Add("Text", DataSource, "TONGTIEN");
            _TONGTIEN.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");

            DataTable dtSource = (DataTable)DataSource;
            lbl_date.Text = dtSource.Rows[0]["HEARDER"].ToString();
        }

        int i = 0;
        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i = 0;
        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
