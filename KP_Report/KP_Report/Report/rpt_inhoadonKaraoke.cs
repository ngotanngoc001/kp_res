﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using System.Data;
using KP_Report;
using KP_UserManagement;
using System.IO;
using System.Windows.Forms;

namespace KP_Report
{
    public partial class rpt_inhoadonKaraoke : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// 
        /// </summary>
        public int checkBill=0;
        /// <summary>
        /// 
        /// </summary>
        public rpt_inhoadonKaraoke()
        {
            InitializeComponent();                      
            Setheader();                        

        }
        private bool ContainColumn(string columnName, DataTable table)
        {
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void BindData()
        {
            DataTable dtSource = (DataTable)DataSource;
            DataTable dtTiengio = new DataTable();
            DataTable dtmakv = new DataTable();
            if (dtSource.Rows[0]["Giovao"].ToString() != "")
            {
                dtmakv = clsMain.ReturnDataTable("Select MA_KHUVUC, TEN_BAN From BAN Where MA_BAN = " + dtSource.Rows[0]["MaBan"].ToString());
                DateTime giovao = Convert.ToDateTime(dtSource.Rows[0]["Giovao"].ToString());
                DateTime giora = Convert.ToDateTime(dtSource.Rows[0]["Giora"].ToString());
                string sqltiengio = string.Format("SET DATEFORMAT MDY exec TinhTienGio_Karaoke @Giovao='{0:MM/dd/yyyy HH:mm:ss}', @Giora='{1:MM/dd/yyyy HH:mm:ss}', @Khuvuc={2}", giovao, giora, dtmakv.Rows[0]["MA_KHUVUC"].ToString());
                dtTiengio = clsMain.ReturnDataTable(sqltiengio);
                DataTable dtSet = clsMain.ReturnDataTable("SET DATEFORMAT DMY");

                DataColumn colTHOIGIAN = new DataColumn();
                colTHOIGIAN.ColumnName = "THOIGIAN";
                colTHOIGIAN.DataType = System.Type.GetType("System.String");

                if (ContainColumn("THOIGIAN", dtTiengio) == false)
                    dtTiengio.Columns.Add(colTHOIGIAN);
                for (int i = 0; i < dtTiengio.Rows.Count; i++)
                {
                    dtTiengio.Rows[i]["THOIGIAN"] = string.Format("{0:HH:mm}", Convert.ToDateTime(dtTiengio.Rows[i]["THOIGIANVAO"].ToString())) + " > " + string.Format("{0:HH:mm}", Convert.ToDateTime(dtTiengio.Rows[i]["THOIGIANRA"].ToString()));
                }
                gc_chitietgio.DataSource = dtTiengio;
            }

            lb_NameBill.DataBindings.Add("Text", DataSource, "Ten_HoaDon");
            //cl_STT.DataBindings.Add("Text", DataSource, "STT");
            cl_HangHoa.DataBindings.Add("Text", DataSource, "TEN_HANGHOA");
            cl_giaban.DataBindings.Add("Text", DataSource, "GIABAN", "{0:#,###0}");
            cl_soluong.DataBindings.Add("Text", DataSource, "SOLUONG");
            cl_thanhtien.DataBindings.Add("Text", DataSource, "Thanh_Tien", "{0:#,###0}");

            lb_ngay.DataBindings.Add("Text", DataSource, "ngayHD", "{0:dd-MM-yyyy HH:mm:ss}");   
            lbPhihucvu.DataBindings.Add("Text", DataSource, "Phiphucvu", "{0:#,###0}");
            lb_tongcong.DataBindings.Add("Text", DataSource, "Tong_Cong" , "{0:#,###0}");
            lb_tienkhachtra.DataBindings.Add("Text", DataSource, "Tien_KhachTra", "{0:#,###0}");
            lb_conlai.DataBindings.Add("Text", DataSource, "Tien_TraKhach", "{0:#,###0}");
            if (decimal.Parse(dtSource.Rows[0]["Giam_Gia"].ToString()) > 100)
            {
                lb_giamgia.Text = string.Format("{0:#,###0}", decimal.Parse(dtSource.Rows[0]["Giam_Gia"].ToString()));
            }
            else
            {
                lb_giamgia.Text = dtSource.Rows[0]["Giam_Gia"].ToString() + " %";
            }
            //header
            if (dtmakv.Rows.Count > 0)
                lb_Phong.Text = dtmakv.Rows[0]["TEN_BAN"].ToString();
            lb_Quay.DataBindings.Add("Text", DataSource, "Ma_Quay");
            lb_mahoadon.DataBindings.Add("Text", DataSource, "Ma_HoaDon");
            lb_MaHD.DataBindings.Add("Text", DataSource, "So_HoaDon","*{0}*");
            lb_nhanvien.DataBindings.Add("Text", DataSource, "TenNV");
            lb_ngay.DataBindings.Add("Text", DataSource, "ngayHD", "{0:dd/MM/yyyy}");
            //Karaoke
            lbTongthanhtien.DataBindings.Add("Text", DataSource, "Thanh_Tien");
            lbTongthanhtien.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");
            lbTiengio.DataBindings.Add("Text", DataSource, "TienGio", "{0:#,###0}");
            lbNgayvao.DataBindings.Add("Text", DataSource, "Giovao", "{0:dd/MM/yyyy}");
            lbNgayra.DataBindings.Add("Text", DataSource, "Giora", "{0:dd/MM/yyyy}");
            lbGiovao.DataBindings.Add("Text", DataSource, "Giovao", "{0:HH:mm}");
            lbGiora.DataBindings.Add("Text", DataSource, "Giora", "{0:HH:mm}");
        } 

        private void Setheader()
        {
            StreamReader str = new StreamReader(Application.StartupPath + "/systeminfo.txt");
            string sMacuahang = "";
            string line;
            while ((line = str.ReadLine()) != null)
            {
                string[] value = line.Split(',');
                sMacuahang = value[0].ToString();
            }

            string sSQL = "";
            sSQL += "Select MA_CUAHANG,	TEN_CUAHANG,	GHICHU,	SUDUNG,	Header,	Footer" + "\n";
            sSQL += "From CUAHANG" + "\n";
            sSQL += "Where MA_CUAHANG  =" + clsMain.SQLString(sMacuahang) + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            lb_TenCty.Text = dt.Rows[0]["Header"].ToString();
            xrLabel22.Text = dt.Rows[0]["Footer"].ToString();
  
            //Logo.BackgroundImage = Image.FromFile _(System.Environment.GetFolderPath _(System.Environment.SpecialFolder.Personal) _& "\Image.gif")           
        }
    }
}
