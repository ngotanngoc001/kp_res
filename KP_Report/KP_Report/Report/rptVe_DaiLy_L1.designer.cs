﻿namespace KP_Report
{
    partial class rptVe_DaiLy_L1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.MABARCODE = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txtMauso = new DevExpress.XtraReports.UI.XRLabel();
            this.txtKyhieu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txtMST = new DevExpress.XtraReports.UI.XRTableCell();
            this.logo = new DevExpress.XtraReports.UI.XRPictureBox();
            this.LOAIGHE = new DevExpress.XtraReports.UI.XRLabel();
            this.TENLOAIVE = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.DONGIA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txtDienthoai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txtDiachi = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txtTencongty = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.TENHANHKHACH = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYTAO = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.GIODI1 = new DevExpress.XtraReports.UI.XRLabel();
            this.SOGHE1 = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYDI1 = new DevExpress.XtraReports.UI.XRLabel();
            this.NGUOITAO = new DevExpress.XtraReports.UI.XRLabel();
            this.MABARCODE1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TENTUYEN1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TENDOITUONG1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.BorderWidth = 0;
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel7,
            this.MABARCODE,
            this.xrLabel2,
            this.txtMauso,
            this.txtKyhieu,
            this.xrTable6,
            this.xrTable5,
            this.xrTable2,
            this.xrTable1,
            this.logo,
            this.LOAIGHE,
            this.TENLOAIVE,
            this.xrLabel23,
            this.xrLabel24,
            this.xrLabel6,
            this.xrLabel3,
            this.DONGIA,
            this.xrLabel1,
            this.xrTable10,
            this.xrTable3,
            this.xrTable4,
            this.xrLabel26,
            this.xrLabel25,
            this.xrLabel20,
            this.xrLabel19,
            this.xrLabel22,
            this.xrLabel21,
            this.xrLabel16,
            this.xrLabel15,
            this.xrLabel18,
            this.xrLabel17,
            this.xrLabel5,
            this.xrLabel4,
            this.TENHANHKHACH,
            this.xrLabel46,
            this.NGAYTAO,
            this.xrLabel43,
            this.GIODI1,
            this.SOGHE1,
            this.NGAYDI1,
            this.NGUOITAO,
            this.MABARCODE1,
            this.TENTUYEN1,
            this.TENDOITUONG1});
            this.Detail.Dpi = 254F;
            this.Detail.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Detail.HeightF = 1827.145F;
            this.Detail.LockedInUserDesigner = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.Detail.StylePriority.UseBorderWidth = false;
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.CanGrow = false;
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(196.2279F, 1154.443F);
            this.xrLabel7.Multiline = true;
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(399.1582F, 44.13257F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UsePadding = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "(Đã bao gồm 10% VAT)";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // MABARCODE
            // 
            this.MABARCODE.CanGrow = false;
            this.MABARCODE.Dpi = 254F;
            this.MABARCODE.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Italic);
            this.MABARCODE.LocationFloat = new DevExpress.Utils.PointFloat(9.263888F, 1198.575F);
            this.MABARCODE.Name = "MABARCODE";
            this.MABARCODE.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.MABARCODE.SizeF = new System.Drawing.SizeF(734.8943F, 44.12988F);
            this.MABARCODE.StylePriority.UseFont = false;
            this.MABARCODE.StylePriority.UsePadding = false;
            this.MABARCODE.StylePriority.UseTextAlignment = false;
            this.MABARCODE.Text = "565656";
            this.MABARCODE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Courier New", 7F);
            this.xrLabel2.ForeColor = System.Drawing.Color.Black;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(9.263929F, 1302.713F);
            this.xrLabel2.Multiline = true;
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(755.2986F, 58.94873F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseForeColor = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "In từ phần mềm của Cty TNHH Giải Pháp Chuyên Nghiệp Quốc Vương _MST: 0309265717";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // txtMauso
            // 
            this.txtMauso.CanGrow = false;
            this.txtMauso.Dpi = 254F;
            this.txtMauso.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtMauso.LocationFloat = new DevExpress.Utils.PointFloat(428.3667F, 506.3669F);
            this.txtMauso.Name = "txtMauso";
            this.txtMauso.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.txtMauso.SizeF = new System.Drawing.SizeF(286.2108F, 44.1326F);
            this.txtMauso.StylePriority.UseFont = false;
            this.txtMauso.StylePriority.UsePadding = false;
            this.txtMauso.StylePriority.UseTextAlignment = false;
            this.txtMauso.Text = "01/THK2-001";
            this.txtMauso.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txtKyhieu
            // 
            this.txtKyhieu.CanGrow = false;
            this.txtKyhieu.Dpi = 254F;
            this.txtKyhieu.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtKyhieu.LocationFloat = new DevExpress.Utils.PointFloat(428.3667F, 552.1927F);
            this.txtKyhieu.Name = "txtKyhieu";
            this.txtKyhieu.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.txtKyhieu.SizeF = new System.Drawing.SizeF(286.2108F, 44.13257F);
            this.txtKyhieu.StylePriority.UseFont = false;
            this.txtKyhieu.StylePriority.UsePadding = false;
            this.txtKyhieu.StylePriority.UseTextAlignment = false;
            this.txtKyhieu.Text = "TP/18T";
            this.txtKyhieu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.Dpi = 254F;
            this.xrTable6.Font = new System.Drawing.Font("Tahoma", 16F);
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(300.875F, 598.0186F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable6.SizeF = new System.Drawing.SizeF(127.4917F, 45.82587F);
            this.xrTable6.StylePriority.UseBorders = false;
            this.xrTable6.StylePriority.UseFont = false;
            this.xrTable6.StylePriority.UseTextAlignment = false;
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell3.Multiline = true;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "Số :";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell3.Weight = 2.9712627931651934D;
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.Dpi = 254F;
            this.xrTable5.Font = new System.Drawing.Font("Tahoma", 16F);
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(300.875F, 552.1927F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(127.4917F, 45.82599F);
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.StylePriority.UseFont = false;
            this.xrTable5.StylePriority.UseTextAlignment = false;
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell2.Multiline = true;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "Ký hiệu :";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell2.Weight = 2.9712627931651934D;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Dpi = 254F;
            this.xrTable2.Font = new System.Drawing.Font("Tahoma", 16F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(300.875F, 506.3669F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(127.4917F, 45.82584F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell1.Multiline = true;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Mẫu số :";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell1.Weight = 2.9712627931651934D;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Dpi = 254F;
            this.xrTable1.Font = new System.Drawing.Font("Tahoma", 16F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(4.999998F, 341.3043F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(727.8126F, 45.82581F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txtMST});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // txtMST
            // 
            this.txtMST.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtMST.Dpi = 254F;
            this.txtMST.Font = new System.Drawing.Font("Times New Roman", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMST.Multiline = true;
            this.txtMST.Name = "txtMST";
            this.txtMST.StylePriority.UseBorders = false;
            this.txtMST.StylePriority.UseFont = false;
            this.txtMST.StylePriority.UseTextAlignment = false;
            this.txtMST.Text = "MST : 095866555";
            this.txtMST.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.txtMST.Weight = 2.9712627931651934D;
            // 
            // logo
            // 
            this.logo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.logo.Dpi = 254F;
            this.logo.LocationFloat = new DevExpress.Utils.PointFloat(265F, 5.000018F);
            this.logo.Name = "logo";
            this.logo.SizeF = new System.Drawing.SizeF(219.98F, 200F);
            this.logo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            this.logo.StylePriority.UseBorders = false;
            // 
            // LOAIGHE
            // 
            this.LOAIGHE.CanGrow = false;
            this.LOAIGHE.Dpi = 254F;
            this.LOAIGHE.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.LOAIGHE.LocationFloat = new DevExpress.Utils.PointFloat(199.2638F, 1061.5F);
            this.LOAIGHE.Name = "LOAIGHE";
            this.LOAIGHE.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.LOAIGHE.SizeF = new System.Drawing.SizeF(207.8102F, 46.89F);
            this.LOAIGHE.StylePriority.UseFont = false;
            this.LOAIGHE.StylePriority.UsePadding = false;
            this.LOAIGHE.StylePriority.UseTextAlignment = false;
            this.LOAIGHE.Text = "Ghế thường";
            this.LOAIGHE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TENLOAIVE
            // 
            this.TENLOAIVE.CanGrow = false;
            this.TENLOAIVE.Dpi = 254F;
            this.TENLOAIVE.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.TENLOAIVE.LocationFloat = new DevExpress.Utils.PointFloat(407.8554F, 1061.5F);
            this.TENLOAIVE.Name = "TENLOAIVE";
            this.TENLOAIVE.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENLOAIVE.SizeF = new System.Drawing.SizeF(377.5404F, 46.89F);
            this.TENLOAIVE.StylePriority.UseFont = false;
            this.TENLOAIVE.StylePriority.UsePadding = false;
            this.TENLOAIVE.StylePriority.UseTextAlignment = false;
            this.TENLOAIVE.Text = "(Thong thuong)";
            this.TENLOAIVE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel23
            // 
            this.xrLabel23.CanGrow = false;
            this.xrLabel23.Dpi = 254F;
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(9.263929F, 1061.5F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(72.46472F, 44.13254F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UsePadding = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "Loại";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel24
            // 
            this.xrLabel24.CanGrow = false;
            this.xrLabel24.Dpi = 254F;
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Italic);
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(79.26389F, 1061.5F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(97.93677F, 44.13257F);
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UsePadding = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "(Type)";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.CanGrow = false;
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Italic);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(64.26396F, 1123.229F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(113.9803F, 44.13257F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UsePadding = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "(Price)";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.CanGrow = false;
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(9.263929F, 1123.229F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(102.8954F, 44.13254F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UsePadding = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Giá";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // DONGIA
            // 
            this.DONGIA.CanGrow = false;
            this.DONGIA.Dpi = 254F;
            this.DONGIA.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.DONGIA.LocationFloat = new DevExpress.Utils.PointFloat(199.2638F, 1123.229F);
            this.DONGIA.Name = "DONGIA";
            this.DONGIA.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.DONGIA.SizeF = new System.Drawing.SizeF(338.4167F, 44.13254F);
            this.DONGIA.StylePriority.UseFont = false;
            this.DONGIA.StylePriority.UsePadding = false;
            this.DONGIA.StylePriority.UseTextAlignment = false;
            this.DONGIA.Text = "50,000";
            this.DONGIA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.CanGrow = false;
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(192.8554F, 460.541F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(380.0825F, 44.13251F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UsePadding = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Liên 1: Lưu";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable10
            // 
            this.xrTable10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable10.Dpi = 254F;
            this.xrTable10.Font = new System.Drawing.Font("Tahoma", 16F);
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(4.999998F, 295.4784F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(727.8126F, 45.82574F);
            this.xrTable10.StylePriority.UseBorders = false;
            this.xrTable10.StylePriority.UseFont = false;
            this.xrTable10.StylePriority.UseTextAlignment = false;
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txtDienthoai});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // txtDienthoai
            // 
            this.txtDienthoai.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtDienthoai.Dpi = 254F;
            this.txtDienthoai.Font = new System.Drawing.Font("Times New Roman", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDienthoai.Multiline = true;
            this.txtDienthoai.Name = "txtDienthoai";
            this.txtDienthoai.StylePriority.UseBorders = false;
            this.txtDienthoai.StylePriority.UseFont = false;
            this.txtDienthoai.StylePriority.UseTextAlignment = false;
            this.txtDienthoai.Text = "Điện thoại : 077.3957239 - Fax : 077.3957238";
            this.txtDienthoai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.txtDienthoai.Weight = 2.9712627931651934D;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Dpi = 254F;
            this.xrTable3.Font = new System.Drawing.Font("Tahoma", 16F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(4.999998F, 249.6526F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(727.8126F, 45.82587F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txtDiachi});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // txtDiachi
            // 
            this.txtDiachi.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtDiachi.Dpi = 254F;
            this.txtDiachi.Font = new System.Drawing.Font("Times New Roman", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiachi.Multiline = true;
            this.txtDiachi.Name = "txtDiachi";
            this.txtDiachi.StylePriority.UseBorders = false;
            this.txtDiachi.StylePriority.UseFont = false;
            this.txtDiachi.StylePriority.UseTextAlignment = false;
            this.txtDiachi.Text = "Khu phố 1, P.Tô Châu, TX. Hà Tiên, Tỉnh Kiên Giang";
            this.txtDiachi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.txtDiachi.Weight = 2.9712627931651934D;
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.Dpi = 254F;
            this.xrTable4.Font = new System.Drawing.Font("Tahoma", 16F);
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(4.999998F, 206.4734F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(727.8125F, 43.17914F);
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.StylePriority.UseFont = false;
            this.xrTable4.StylePriority.UseTextAlignment = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txtTencongty});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // txtTencongty
            // 
            this.txtTencongty.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtTencongty.Dpi = 254F;
            this.txtTencongty.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTencongty.Multiline = true;
            this.txtTencongty.Name = "txtTencongty";
            this.txtTencongty.StylePriority.UseBorders = false;
            this.txtTencongty.StylePriority.UseFont = false;
            this.txtTencongty.StylePriority.UseTextAlignment = false;
            this.txtTencongty.Text = "CÔNG TY TNHH MTV THẠNH THỚI";
            this.txtTencongty.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.txtTencongty.Weight = 2.9712627931651934D;
            // 
            // xrLabel26
            // 
            this.xrLabel26.CanGrow = false;
            this.xrLabel26.Dpi = 254F;
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Italic);
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(58.00015F, 771.2778F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(99.99868F, 44.13254F);
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UsePadding = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "(Name)";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel25
            // 
            this.xrLabel25.CanGrow = false;
            this.xrLabel25.Dpi = 254F;
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(4.999957F, 771.2778F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(79.08289F, 44.13257F);
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UsePadding = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "Tên";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.CanGrow = false;
            this.xrLabel20.Dpi = 254F;
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Italic);
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(60.00015F, 944.8477F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(89.95783F, 44.13255F);
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UsePadding = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "(Time)";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel19
            // 
            this.xrLabel19.CanGrow = false;
            this.xrLabel19.Dpi = 254F;
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(4.999998F, 944.8477F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(102.8954F, 44.13251F);
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UsePadding = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "Giờ";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel22
            // 
            this.xrLabel22.CanGrow = false;
            this.xrLabel22.Dpi = 254F;
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Italic);
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(60.00015F, 1002.98F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(113.9803F, 44.13257F);
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UsePadding = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "(Ferry)";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel21
            // 
            this.xrLabel21.CanGrow = false;
            this.xrLabel21.Dpi = 254F;
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(4.999998F, 1002.98F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(102.8954F, 44.13254F);
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UsePadding = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "Tàu";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.CanGrow = false;
            this.xrLabel16.Dpi = 254F;
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Italic);
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(90.39516F, 827.0903F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(105.4602F, 44.13257F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UsePadding = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "(Route)";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.CanGrow = false;
            this.xrLabel15.Dpi = 254F;
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(4.999998F, 827.0903F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(102.8954F, 44.13254F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UsePadding = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "Tuyến";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel18
            // 
            this.xrLabel18.CanGrow = false;
            this.xrLabel18.Dpi = 254F;
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Italic);
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(78.02261F, 887.715F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(89.95783F, 44.13255F);
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UsePadding = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "(Date)";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.CanGrow = false;
            this.xrLabel17.Dpi = 254F;
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(4.999998F, 887.715F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(102.8954F, 44.13254F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UsePadding = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "Ngày";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.CanGrow = false;
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Italic);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(105.8976F, 711.1452F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(89.95783F, 44.13255F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UsePadding = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "(Seat)";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.CanGrow = false;
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(4.999998F, 711.1452F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(102.8954F, 44.13254F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UsePadding = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Số Ghế";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // TENHANHKHACH
            // 
            this.TENHANHKHACH.CanGrow = false;
            this.TENHANHKHACH.Dpi = 254F;
            this.TENHANHKHACH.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.TENHANHKHACH.LocationFloat = new DevExpress.Utils.PointFloat(195.533F, 768.5222F);
            this.TENHANHKHACH.Name = "TENHANHKHACH";
            this.TENHANHKHACH.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENHANHKHACH.SizeF = new System.Drawing.SizeF(587.5404F, 46.88824F);
            this.TENHANHKHACH.StylePriority.UseFont = false;
            this.TENHANHKHACH.StylePriority.UsePadding = false;
            this.TENHANHKHACH.StylePriority.UseTextAlignment = false;
            this.TENHANHKHACH.Text = "GIANG THI ANH MINH";
            this.TENHANHKHACH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel46
            // 
            this.xrLabel46.Dpi = 254F;
            this.xrLabel46.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(4.999998F, 408.4709F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(759.5625F, 52.07007F);
            this.xrLabel46.StylePriority.UseFont = false;
            this.xrLabel46.StylePriority.UsePadding = false;
            this.xrLabel46.StylePriority.UseTextAlignment = false;
            this.xrLabel46.Text = "VÉ HÀNH KHÁCH LÊN TÀU";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // NGAYTAO
            // 
            this.NGAYTAO.CanGrow = false;
            this.NGAYTAO.Dpi = 254F;
            this.NGAYTAO.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Italic);
            this.NGAYTAO.LocationFloat = new DevExpress.Utils.PointFloat(481.2834F, 1242.705F);
            this.NGAYTAO.Name = "NGAYTAO";
            this.NGAYTAO.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYTAO.SizeF = new System.Drawing.SizeF(262.8749F, 44.13263F);
            this.NGAYTAO.StylePriority.UseFont = false;
            this.NGAYTAO.StylePriority.UsePadding = false;
            this.NGAYTAO.StylePriority.UseTextAlignment = false;
            this.NGAYTAO.Text = "21/12/2012 13 : 30";
            this.NGAYTAO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel43
            // 
            this.xrLabel43.CanGrow = false;
            this.xrLabel43.Dpi = 254F;
            this.xrLabel43.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(437.4714F, 1242.705F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(37.81213F, 44.13263F);
            this.xrLabel43.StylePriority.UseFont = false;
            this.xrLabel43.StylePriority.UsePadding = false;
            this.xrLabel43.StylePriority.UseTextAlignment = false;
            this.xrLabel43.Text = "-";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // GIODI1
            // 
            this.GIODI1.CanGrow = false;
            this.GIODI1.Dpi = 254F;
            this.GIODI1.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.GIODI1.LocationFloat = new DevExpress.Utils.PointFloat(196.2279F, 944.8477F);
            this.GIODI1.Name = "GIODI1";
            this.GIODI1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.GIODI1.SizeF = new System.Drawing.SizeF(338.4167F, 44.13257F);
            this.GIODI1.StylePriority.UseFont = false;
            this.GIODI1.StylePriority.UsePadding = false;
            this.GIODI1.StylePriority.UseTextAlignment = false;
            this.GIODI1.Text = "06 : 00";
            this.GIODI1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // SOGHE1
            // 
            this.SOGHE1.CanGrow = false;
            this.SOGHE1.Dpi = 254F;
            this.SOGHE1.Font = new System.Drawing.Font("Times New Roman", 33F, System.Drawing.FontStyle.Bold);
            this.SOGHE1.LocationFloat = new DevExpress.Utils.PointFloat(196.2279F, 643.8444F);
            this.SOGHE1.Name = "SOGHE1";
            this.SOGHE1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.SOGHE1.SizeF = new System.Drawing.SizeF(494.5208F, 116.8599F);
            this.SOGHE1.StylePriority.UseFont = false;
            this.SOGHE1.StylePriority.UsePadding = false;
            this.SOGHE1.StylePriority.UseTextAlignment = false;
            this.SOGHE1.Text = "IN TEST";
            this.SOGHE1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // NGAYDI1
            // 
            this.NGAYDI1.CanGrow = false;
            this.NGAYDI1.Dpi = 254F;
            this.NGAYDI1.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.NGAYDI1.LocationFloat = new DevExpress.Utils.PointFloat(192.8554F, 887.715F);
            this.NGAYDI1.Name = "NGAYDI1";
            this.NGAYDI1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYDI1.SizeF = new System.Drawing.SizeF(338.4167F, 44.13257F);
            this.NGAYDI1.StylePriority.UseFont = false;
            this.NGAYDI1.StylePriority.UsePadding = false;
            this.NGAYDI1.StylePriority.UseTextAlignment = false;
            this.NGAYDI1.Text = "21/12/2012";
            this.NGAYDI1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // NGUOITAO
            // 
            this.NGUOITAO.CanGrow = false;
            this.NGUOITAO.Dpi = 254F;
            this.NGUOITAO.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Italic);
            this.NGUOITAO.LocationFloat = new DevExpress.Utils.PointFloat(9.263929F, 1242.705F);
            this.NGUOITAO.Name = "NGUOITAO";
            this.NGUOITAO.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGUOITAO.SizeF = new System.Drawing.SizeF(408.2075F, 44.12994F);
            this.NGUOITAO.StylePriority.UseFont = false;
            this.NGUOITAO.StylePriority.UsePadding = false;
            this.NGUOITAO.StylePriority.UseTextAlignment = false;
            this.NGUOITAO.Text = "Phan Bá Long";
            this.NGUOITAO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // MABARCODE1
            // 
            this.MABARCODE1.CanGrow = false;
            this.MABARCODE1.Dpi = 254F;
            this.MABARCODE1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.MABARCODE1.LocationFloat = new DevExpress.Utils.PointFloat(428.3667F, 596.3252F);
            this.MABARCODE1.Name = "MABARCODE1";
            this.MABARCODE1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.MABARCODE1.SizeF = new System.Drawing.SizeF(286.2108F, 44.13263F);
            this.MABARCODE1.StylePriority.UseFont = false;
            this.MABARCODE1.StylePriority.UsePadding = false;
            this.MABARCODE1.StylePriority.UseTextAlignment = false;
            this.MABARCODE1.Text = "0000000";
            this.MABARCODE1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TENTUYEN1
            // 
            this.TENTUYEN1.CanGrow = false;
            this.TENTUYEN1.Dpi = 254F;
            this.TENTUYEN1.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.TENTUYEN1.LocationFloat = new DevExpress.Utils.PointFloat(192.8554F, 830.0905F);
            this.TENTUYEN1.Name = "TENTUYEN1";
            this.TENTUYEN1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENTUYEN1.SizeF = new System.Drawing.SizeF(589.168F, 42.73657F);
            this.TENTUYEN1.StylePriority.UseFont = false;
            this.TENTUYEN1.StylePriority.UsePadding = false;
            this.TENTUYEN1.StylePriority.UseTextAlignment = false;
            this.TENTUYEN1.Text = "Phú Quốc-Hà Tiên Phú Quốc-Hà Tiên";
            this.TENTUYEN1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TENDOITUONG1
            // 
            this.TENDOITUONG1.CanGrow = false;
            this.TENDOITUONG1.Dpi = 254F;
            this.TENDOITUONG1.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.TENDOITUONG1.LocationFloat = new DevExpress.Utils.PointFloat(195.533F, 1002.98F);
            this.TENDOITUONG1.Name = "TENDOITUONG1";
            this.TENDOITUONG1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENDOITUONG1.SizeF = new System.Drawing.SizeF(338.4167F, 44.13254F);
            this.TENDOITUONG1.StylePriority.UseFont = false;
            this.TENDOITUONG1.StylePriority.UsePadding = false;
            this.TENDOITUONG1.StylePriority.UseTextAlignment = false;
            this.TENDOITUONG1.Text = "Thringving2";
            this.TENDOITUONG1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 10F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 33F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // formattingRule1
            // 
            this.formattingRule1.Name = "formattingRule1";
            // 
            // rptVe_DaiLy_L1
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Dpi = 254F;
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Margins = new System.Drawing.Printing.Margins(33, 178, 10, 33);
            this.PageHeight = 3000;
            this.PageWidth = 1003;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.ShowPrintMarginsWarning = false;
            this.ShowPrintStatusDialog = false;
            this.SnapGridSize = 5F;
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel SOGHE1;
        private DevExpress.XtraReports.UI.XRLabel NGAYDI1;
        private DevExpress.XtraReports.UI.XRLabel NGUOITAO;
        private DevExpress.XtraReports.UI.XRLabel MABARCODE1;
        private DevExpress.XtraReports.UI.XRLabel TENTUYEN1;
        private DevExpress.XtraReports.UI.XRLabel TENDOITUONG1;
        private DevExpress.XtraReports.UI.XRLabel NGAYTAO;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel GIODI1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        private DevExpress.XtraReports.UI.XRLabel TENHANHKHACH;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell txtDienthoai;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell txtDiachi;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell txtTencongty;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel DONGIA;
        private DevExpress.XtraReports.UI.XRLabel LOAIGHE;
        private DevExpress.XtraReports.UI.XRLabel TENLOAIVE;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRPictureBox logo;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell txtMST;
        private DevExpress.XtraReports.UI.XRLabel txtMauso;
        private DevExpress.XtraReports.UI.XRLabel txtKyhieu;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel MABARCODE;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
    }
}
