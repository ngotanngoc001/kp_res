﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using System.Drawing.Imaging;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace KP_Report
{
    /// <summary>
    /// rptBaoCaoPhieuChi
    /// </summary>
    public partial class rptBaoCaoPhieuChi : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// rptBaoCaoPhieuChi
        /// </summary>
        public rptBaoCaoPhieuChi()
        {
            InitializeComponent();
            Setheader();
        }

        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        ///
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            MAPCN.DataBindings.Add("Text", DataSource, "MAPCN");
            TenLoai.DataBindings.Add("Text", DataSource, "TenLoai");
            HOTEN.DataBindings.Add("Text", DataSource, "HOTEN");
            LYDO.DataBindings.Add("Text", DataSource, "LYDO");
            KEMTHEO.DataBindings.Add("Text", DataSource, "KEMTHEO");
            SOTIEN.DataBindings.Add("Text", DataSource, "SOTIEN", "{0:#,###0}");
            PTTHANHTOAN.DataBindings.Add("Text", DataSource, "PHUONGTHUCTT");
            NGAYTHANHTOAN.DataBindings.Add("Text", DataSource, "NGAYTHANHTOAN", "{0:dd/MM/yyyy}");
            TenNV.DataBindings.Add("Text", DataSource, "TENNHANVIEN");
            NGAYLAPPHIEU.DataBindings.Add("Text", DataSource, "NGAYLAPPHIEU", "{0:dd/MM/yyyy}");

            TONGSOTIEN.DataBindings.Add("Text", DataSource, "SOTIEN");
            TONGSOTIEN.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");

            DataTable dtSource = (DataTable)DataSource;

            txt_tieude1.Text = dtSource.Rows[0]["HEARDER"].ToString();
        }

        int i = 0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
