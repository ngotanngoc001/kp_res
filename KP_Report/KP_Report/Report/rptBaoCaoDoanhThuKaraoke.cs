﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using System.Drawing.Imaging;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBaoCaoDoanhThuKaraoke : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBaoCaoDoanhThuKaraoke()
        {
            InitializeComponent();
            Setheader();
        }
        private void ConvertHinh(ref PictureBox pic, Byte[] values)
        {
            MemoryStream ms = new MemoryStream(values);
            Bitmap bm = new Bitmap(ms);
            pic.Image = bm;
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            NGAYTAO.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy HH:mm}");
            MAHOADON.DataBindings.Add("Text", DataSource, "MA_HOADON");
            TENPHONG.DataBindings.Add("Text", DataSource, "TENPHONG");
            THOIGIANVAO.DataBindings.Add("Text", DataSource, "THOIGIANVAO", "{0:dd/MM/yyyy HH:mm}");
            THOIGIANRA.DataBindings.Add("Text", DataSource, "THOIGIANRA", "{0:dd/MM/yyyy HH:mm}");
            BLOCK.DataBindings.Add("Text", DataSource, "BLOCKKARAOKE");
            SOPHUT.DataBindings.Add("Text", DataSource, "SOPHUT");
            SOBLOCK.DataBindings.Add("Text", DataSource, "SOBLOCK");
            GIABLOCK.DataBindings.Add("Text", DataSource, "GIABLOCK", "{0:#,###0}");
            THANHTIEN.DataBindings.Add("Text", DataSource, "THANHTIEN", "{0:#,###0}");

            SUMTHANHTIEN.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");
            SUMTHANHTIEN.DataBindings.Add("Text", DataSource, "THANHTIEN");
            SUMBLOCK.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");
            SUMBLOCK.DataBindings.Add("Text", DataSource, "SOBLOCK");
            SUMPHUT.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");
            SUMPHUT.DataBindings.Add("Text", DataSource, "SOPHUT");

            DataTable dtSource = (DataTable)DataSource;
            try
            {
                txt_tieude1.Text = "( Từ ngày: " + dtSource.Rows[0]["TUNGAY"].ToString() + " - Đến ngày: " + dtSource.Rows[0]["DENNGAY"].ToString() + " )";
            }
            catch
            {
            }

        }
        int i = 0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
