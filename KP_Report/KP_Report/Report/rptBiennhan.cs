﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBiennhan : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBiennhan()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            //MAGHE.DataBindings.Add("Text", DataSource, "MAGHE");
            //MAGHE1.DataBindings.Add("Text", DataSource, "MAGHE", "*{0}*");
            SOGHE.DataBindings.Add("Text", DataSource, "SOGHE");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:#,###0}");
            TENHANHKHACH.DataBindings.Add("Text", DataSource, "TENHANHKHACH");
            DIACHI.DataBindings.Add("Text", DataSource, "DIACHI");
            DIENTHOAI.DataBindings.Add("Text", DataSource, "DIENTHOAI");
            EMAIL.DataBindings.Add("Text", DataSource, "EMAIL");
            NGAYTAO.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy}");
            TENTUYEN.DataBindings.Add("Text", DataSource, "TENTUYEN");
            TENDOITUONG.DataBindings.Add("Text", DataSource, "TENDOITUONG");
            NGAYDI.DataBindings.Add("Text", DataSource, "NGAYDI", "{0:dd/MM/yyyy}");
            GIODI.DataBindings.Add("Text", DataSource, "GIODI");
            MAPHIEU.DataBindings.Add("Text", DataSource, "MAPHIEU");
            MAPHIEU1.DataBindings.Add("Text", DataSource, "MAPHIEU", "*{0}*");
            TONGSOGHE.DataBindings.Add("Text", DataSource, "TONGSOGHE");
            TONGGIAVE.DataBindings.Add("Text", DataSource, "TONGGIAVE", "{0:#,###0}");
            DATHANHTOAN.DataBindings.Add("Text", DataSource, "DATHANHTOAN", "{0:#,###0}");
            NGUOITAO.DataBindings.Add("Text", DataSource, "NGUOITAO");
            NGAYTAO1.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy}");
        }
        int i=0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
