﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptTonKho : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptTonKho()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
           
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            MA_HANGHOA.DataBindings.Add("Text", DataSource, "MAVACH");
            TEN_HANGHOA.DataBindings.Add("Text", DataSource, "TEN_HANGHOA");
            DVT.DataBindings.Add("Text", DataSource, "DVT");
            DG.DataBindings.Add("Text", DataSource, "GIAVON", "{0:#,###0.00}");
            SLDK.DataBindings.Add("Text", DataSource, "SLTONDAUKY", "{0:#,###0}");
            TGDK.DataBindings.Add("Text", DataSource, "TRIGIATONDAUKY", "{0:#,###0.00}");
            SLNHAP.DataBindings.Add("Text", DataSource, "SLNHAP", "{0:#,###0}");
            TGNHAP.DataBindings.Add("Text", DataSource, "TRIGIANHAP", "{0:#,###0.00}");
            SLXUAT.DataBindings.Add("Text", DataSource, "SLXUAT", "{0:#,###0}");
            TGXUAT.DataBindings.Add("Text", DataSource, "TRIGIAXUAT", "{0:#,###0.00}");
            SLCK.DataBindings.Add("Text", DataSource, "SLTONCUOIKY", "{0:#,###0}");
            TGCK.DataBindings.Add("Text", DataSource, "TRIGIATONCUOIKY", "{0:#,###0.00}");
           
            TSLDK.DataBindings.Add("Text", DataSource, "SLTONDAUKY", "{0:#,###0}");
            TSLDK.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");
            TTGDK.DataBindings.Add("Text", DataSource, "TRIGIATONDAUKY", "{0:#,###0.00}");
            TTGDK.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0.00}");
            TSLNHAP.DataBindings.Add("Text", DataSource, "SLNHAP", "{0:#,###0}");
            TSLNHAP.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");
            TTGNHAP.DataBindings.Add("Text", DataSource, "TRIGIANHAP", "{0:#,###0.00}");
            TTGNHAP.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0.00}");
            TSLXUAT.DataBindings.Add("Text", DataSource, "SLXUAT", "{0:#,###0}");
            TSLXUAT.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");
            TTGXUAT.DataBindings.Add("Text", DataSource, "TRIGIAXUAT", "{0:#,###0.00}");
            TTGXUAT.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0.00}");
            TSLCK.DataBindings.Add("Text", DataSource, "SLTONCUOIKY", "{0:#,###0}");
            TSLCK.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");
            TTGCK.DataBindings.Add("Text", DataSource, "TRIGIATONCUOIKY", "{0:#,###0.00}");
            TTGCK.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0.00}");

            DataTable dtSource = (DataTable)DataSource;

            try
            {
                txt_tieude.Text = "( Cửa hàng: " + dtSource.Rows[0]["CUAHANG"].ToString() + " - Kho hàng: " + dtSource.Rows[0]["KHO"].ToString() + " - Nhóm hàng: " + dtSource.Rows[0]["NHOM"].ToString() + " - Hàng hóa: " + dtSource.Rows[0]["HANGHOA"].ToString() + " )";
                txt_tieude1.Text = "( Từ ngày: " + dtSource.Rows[0]["TUNGAY"].ToString() + " - Đến ngày: " + dtSource.Rows[0]["DENNGAY"].ToString() + " )";
            }
            catch
            {
            }
        }
        int i = 0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
