﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBaocaodatve : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBaocaodatve()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }

        int i;
        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i = 0;
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            TUNGAY.DataBindings.Add("Text", DataSource, "TUNGAY", "{0:dd/MM/yyyy}");
            DENNGAY.DataBindings.Add("Text", DataSource, "DENNGAY", "{0:dd/MM/yyyy}");
            TENTUYEN.DataBindings.Add("Text", DataSource, "TENTUYEN1");
            TENDOITUONG.DataBindings.Add("Text", DataSource, "TENDOITUONG1");
            NHANVIEN.DataBindings.Add("Text", DataSource, "NHANVIEN1");
            MABARCODE.DataBindings.Add("Text", DataSource, "MABARCODE");
            NGUOIDAT.DataBindings.Add("Text", DataSource, "TENHANHKHACH");
            LOAIXE.DataBindings.Add("Text", DataSource, "SOXE");
            SONGUOI.DataBindings.Add("Text", DataSource, "SOGHE");
            TENTUYEN1.DataBindings.Add("Text", DataSource, "TENTUYEN");
            NGAYDI.DataBindings.Add("Text", DataSource, "NGAYDI", "{0:dd/MM/yyyy}");
            GIODI.DataBindings.Add("Text", DataSource, "GIODI");
            CONLAI.DataBindings.Add("Text", DataSource, "CONLAI", "{0:n0}");
            PHAITRA.DataBindings.Add("Text", DataSource, "PHAITRA", "{0:n0}");
            HTTT.DataBindings.Add("Text", DataSource, "GIAVE", "{0:n0}");
            TONGSOPHIEU.DataBindings.Add("Text", DataSource, "MABARCODE");
            TONGSOPHIEU.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Count, "{0:n0}");
            THANHTIEN.DataBindings.Add("Text", DataSource, "THANHTIEN", "{0:n0}");
            DATHANHTOAN.DataBindings.Add("Text", DataSource, "DATHANHTOAN", "{0:n0}");
            HTTT.DataBindings.Add("Text", DataSource, "HTTT");
            chkInve.DataBindings.Add("Checked", DataSource, "INVE");
            TONGTHANHTHIEN.DataBindings.Add("Text", DataSource, "THANHTIEN");
            TONGTHANHTHIEN.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
            TIENKHACHTRA.DataBindings.Add("Text", DataSource, "DATHANHTOAN");
            TIENKHACHTRA.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
            PHAITHU.DataBindings.Add("Text", DataSource, "CONLAI");
            PHAITHU.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
            PHAITRA1.DataBindings.Add("Text", DataSource, "PHAITRA");
            PHAITRA1.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
        }
    }
}
