﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptPhieuCanXe : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptPhieuCanXe()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            MAPHIEU.DataBindings.Add("Text", DataSource, "MAPHIEU");
            TENTUYEN.DataBindings.Add("Text", DataSource, "TENTUYEN");
            GIODI.DataBindings.Add("Text", DataSource, "GIODI");
            LOAIXE.DataBindings.Add("Text", DataSource, "LOAIXE");
            SOXE.DataBindings.Add("Text", DataSource, "SOXE");
            TL_THIETKE.DataBindings.Add("Text", DataSource, "TL_THIETKE", "{0:#,###0}");
            TL_BANTHAN.DataBindings.Add("Text", DataSource, "TL_BANTHAN", "{0:#,###0}");
           // TL_BANTHAN.DataBindings.Add("Text", DataSource, "PHUTHUQUAKHO");
            TL_XEHANG.DataBindings.Add("Text", DataSource, "TL_XEHANG", "{0:#,###0}");
            TL_THUCTE.DataBindings.Add("Text", DataSource, "TL_THUCTE", "{0:#,###0}");
            TL_QUATAI.DataBindings.Add("Text", DataSource, "TL_QUATAI", "{0:#,###0}");
            TL_HANGHOA.DataBindings.Add("Text", DataSource, "TL_HANGHOA", "{0:#,###0}");
            TT_QUATAI.DataBindings.Add("Text", DataSource, "TT_QUATAI", "{0:#,###0}");
            CD_COSO.DataBindings.Add("Text", DataSource, "CD_COSO", "{0:#,###0}");
			CD_THUCTE.DataBindings.Add("Text", DataSource, "CD_THUCTE", "{0:#,###0}");
            CD_QUAKHO.DataBindings.Add("Text", DataSource, "CD_QUAKHO", "{0:#,###0}");
            TT_QUAKHO.DataBindings.Add("Text", DataSource, "TT_QUAKHO", "{0:#,###0}");
            TT_PHUTHU.DataBindings.Add("Text", DataSource, "TT_PHUTHU", "{0:#,###0}");
            DATEDAY.DataBindings.Add("Text", DataSource, "DATEDAY");
        }
    }
}
