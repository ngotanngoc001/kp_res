﻿namespace KP_Report
{
    partial class rptInMaKH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.BarCode.Code128Generator code128Generator1 = new DevExpress.XtraPrinting.BarCode.Code128Generator();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.NGAYSINH = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.HoTenKH = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            this.MAKH = new DevExpress.XtraReports.UI.XRBarCode();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.MAKH,
            this.NGAYSINH,
            this.xrLabel1,
            this.HoTenKH,
            this.xrLabel21});
            this.Detail.Dpi = 254F;
            this.Detail.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Detail.HeightF = 463.2292F;
            this.Detail.LockedInUserDesigner = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // NGAYSINH
            // 
            this.NGAYSINH.CanGrow = false;
            this.NGAYSINH.Dpi = 254F;
            this.NGAYSINH.Font = new System.Drawing.Font("Arial", 16F);
            this.NGAYSINH.LocationFloat = new DevExpress.Utils.PointFloat(313.7709F, 100.1741F);
            this.NGAYSINH.Name = "NGAYSINH";
            this.NGAYSINH.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYSINH.SizeF = new System.Drawing.SizeF(593.0208F, 78.5284F);
            this.NGAYSINH.StylePriority.UseFont = false;
            this.NGAYSINH.StylePriority.UsePadding = false;
            this.NGAYSINH.StylePriority.UseTextAlignment = false;
            this.NGAYSINH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.CanGrow = false;
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Arial", 14F);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(40F, 100.1741F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(273.7708F, 78.52839F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UsePadding = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Ngày Sinh:";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // HoTenKH
            // 
            this.HoTenKH.CanGrow = false;
            this.HoTenKH.Dpi = 254F;
            this.HoTenKH.Font = new System.Drawing.Font("Arial", 16F);
            this.HoTenKH.LocationFloat = new DevExpress.Utils.PointFloat(313.7708F, 19.00001F);
            this.HoTenKH.Name = "HoTenKH";
            this.HoTenKH.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.HoTenKH.SizeF = new System.Drawing.SizeF(593.0208F, 81.17413F);
            this.HoTenKH.StylePriority.UseFont = false;
            this.HoTenKH.StylePriority.UsePadding = false;
            this.HoTenKH.StylePriority.UseTextAlignment = false;
            this.HoTenKH.Text = "họ tên KH";
            this.HoTenKH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel21
            // 
            this.xrLabel21.CanGrow = false;
            this.xrLabel21.Dpi = 254F;
            this.xrLabel21.Font = new System.Drawing.Font("Arial", 14F);
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(37.35417F, 19.00001F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(276.4167F, 81.17413F);
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UsePadding = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "Họ Và Tên:";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 10F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 15F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // formattingRule1
            // 
            this.formattingRule1.Name = "formattingRule1";
            // 
            // MAKH
            // 
            this.MAKH.AutoModule = true;
            this.MAKH.Dpi = 254F;
            this.MAKH.LocationFloat = new DevExpress.Utils.PointFloat(37.35417F, 178.7025F);
            this.MAKH.Module = 5.08F;
            this.MAKH.Name = "MAKH";
            this.MAKH.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.MAKH.ShowText = false;
            this.MAKH.SizeF = new System.Drawing.SizeF(869.44F, 250.4F);
            this.MAKH.StylePriority.UsePadding = false;
            this.MAKH.StylePriority.UseTextAlignment = false;
            this.MAKH.Symbology = code128Generator1;
            this.MAKH.Text = "*1234567*";
            this.MAKH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // rptInMaKH
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Dpi = 254F;
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Margins = new System.Drawing.Printing.Margins(0, 64, 10, 15);
            this.PageHeight = 551;
            this.PageWidth = 988;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.ShowPrintMarginsWarning = false;
            this.SnapGridSize = 5F;
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        private DevExpress.XtraReports.UI.XRLabel HoTenKH;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel NGAYSINH;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRBarCode MAKH;
    }
}
