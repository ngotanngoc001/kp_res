﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptThongkeveNPH : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptThongkeveNPH()
        {
            InitializeComponent();
            GroupField grf = new GroupField("TENDIADIEM");
            GroupHeader7.GroupFields.Add(grf);
            GroupField grf1 = new GroupField("TENNHAPHATHANH");
            GroupHeader6.GroupFields.Add(grf1);
            GroupField grf2 = new GroupField("TENPHIM");
            GroupHeader5.GroupFields.Add(grf2);
            GroupField grf3 = new GroupField("NGAYCHIEU");
            GroupHeader4.GroupFields.Add(grf3);
            GroupField grf4 = new GroupField("BATDAU");
            GroupHeader3.GroupFields.Add(grf4);
            GroupField grf5 = new GroupField("TENLOAIVE");
            GroupHeader2.GroupFields.Add(grf5);
            GroupField grf6 = new GroupField("DONGIA");
            GroupHeader1.GroupFields.Add(grf6);
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            byte[] tam = new byte[((byte[])dt.Rows[0]["HINHANH"]).LongLength];
            tam = (byte[])dt.Rows[0]["HINHANH"];
            System.IO.MemoryStream ms = new System.IO.MemoryStream(tam);
            Bitmap bm = new Bitmap(ms);
            //logo.Image = bm;
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            TENDIADIEM.DataBindings.Add("Text", DataSource, "TENDIADIEM");
            TENNHAPHATHANH.DataBindings.Add("Text", DataSource, "TENNHAPHATHANH");
            TENTUYEN.DataBindings.Add("Text", DataSource, "TENPHIM");
            TENDOITUONG.DataBindings.Add("Text", DataSource, "TENPHONG");
            NGAYDI1.DataBindings.Add("Text", DataSource, "NGAYCHIEU", "{0:dd/MM/yyyy}");
            GIODI1.DataBindings.Add("Text", DataSource, "BATDAU");
            TENLOAIVE1.DataBindings.Add("Text", DataSource, "TENLOAIVE");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:n0}");
            SOLUONG.DataBindings.Add("Text", DataSource, "TONGSOGHE");
            SOLUONG.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Count, "{0:n0}");
            THANHTIEN.DataBindings.Add("Text", DataSource, "DONGIA");
            THANHTIEN.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            Xuatchieu.DataBindings.Add("Text", DataSource, "BATDAU");
            TONGSOGHE1.DataBindings.Add("Text", DataSource, "TONGSOGHE");
            TONGSOGHE1.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Count, "{0:n0}");
            TONGSOTIEN1.DataBindings.Add("Text", DataSource, "DONGIA");
            TONGSOTIEN1.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            TENDOITUONG6.DataBindings.Add("Text", DataSource, "TENPHONG");
            NGAYDI6.DataBindings.Add("Text", DataSource, "NGAYCHIEU", "{0:dd/MM/yyyy}");
            GIODI6.DataBindings.Add("Text", DataSource, "BATDAU");
            TENLOAIVE6.DataBindings.Add("Text", DataSource, "TENLOAIVE");
            DONGIA6.DataBindings.Add("Text", DataSource, "DONGIA", "{0:n0}");
            SOLUONG6.DataBindings.Add("Text", DataSource, "TONGSOGHE");
            SOLUONG6.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Count, "{0:n0}");
            THANHTIEN6.DataBindings.Add("Text", DataSource, "DONGIA");
            THANHTIEN6.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            NGAY2.DataBindings.Add("Text", DataSource, "NGAYCHIEU", "{0:dd/MM/yyyy}");
            SOGHE2.DataBindings.Add("Text", DataSource, "TONGSOGHE");
            SOGHE2.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Count, "{0:n0}");
            SOTIEN2.DataBindings.Add("Text", DataSource, "DONGIA");
            SOTIEN2.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            PHIM8.DataBindings.Add("Text", DataSource, "TENPHIM");
            SOGHE8 .DataBindings.Add("Text", DataSource, "TONGSOGHE");
            SOGHE8.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Count, "{0:n0}");
            THANHTIEN8 .DataBindings.Add("Text", DataSource, "DONGIA");
            THANHTIEN8.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            NHAPHATHANH9.DataBindings.Add("Text", DataSource, "TENNHAPHATHANH");
            SOGHE9 .DataBindings.Add("Text", DataSource, "TONGSOGHE");
            SOGHE9 .Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Count, "{0:n0}");
            TONGTIEN9 .DataBindings.Add("Text", DataSource, "DONGIA");
            TONGTIEN9.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            RAP0.DataBindings.Add("Text", DataSource, "TENDIADIEM");
            SOLUONG0 .DataBindings.Add("Text", DataSource, "TONGSOGHE");
            SOLUONG0.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Count, "{0:n0}");
            SOTIEN0 .DataBindings.Add("Text", DataSource, "DONGIA");
            SOTIEN0.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            DataTable dtSource = (DataTable)DataSource;
            txt_tieude.Text = "(" + dtSource.Rows[0]["TYPE"].ToString() + ": " + dtSource.Rows[0]["NAMETYPE"].ToString() + " )";
            txt_tieude1.Text = "( Từ ngày: " + dtSource.Rows[0]["TUNGAY"].ToString() + " - Đến ngày: " + dtSource.Rows[0]["DENNGAY"].ToString() + " )";
        }
    }
}
