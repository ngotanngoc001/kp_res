﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using System.Drawing.Imaging;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBaoCao_DS_DoiQua : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBaoCao_DS_DoiQua()
        {
            InitializeComponent();
            Setheader();
        }
        private void ConvertHinh(ref PictureBox pic, Byte[] values)
        {
            MemoryStream ms = new MemoryStream(values);
            Bitmap bm = new Bitmap(ms);
            pic.Image = bm;
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            THOIGIAN.DataBindings.Add("Text", DataSource, "NGAYDOI", "{0:dd/MM/yyyy HH:mm:ss}");
            NHANVIEN.DataBindings.Add("Text", DataSource, "TENNHANVIEN");
            KHACHHANG.DataBindings.Add("Text", DataSource, "TEN");
            QUATANG.DataBindings.Add("Text", DataSource, "TEN_QT");
            TONGDIEM.DataBindings.Add("Text", DataSource, "TONGDIEM", "{0:#,###0}");
            SODIEM.DataBindings.Add("Text", DataSource, "SODIEM", "{0:#,###0}");
            SOLUONG.DataBindings.Add("Text", DataSource, "SOLUONG", "{0:#,###0}");

            DataTable dtSource = (DataTable)DataSource;
            txt_tieude1.Text = "( Từ ngày: " + dtSource.Rows[0]["TUNGAY"].ToString() + " - Đến ngày: " + dtSource.Rows[0]["DENNGAY"].ToString() + " )";
            txt_tieude.Text = dtSource.Rows[0]["HEARDER"].ToString();
        }
        int i = 0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
