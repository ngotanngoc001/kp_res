﻿namespace KP_Report
{
    partial class rptVerapphim_Huy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.NGUOITAO1 = new DevExpress.XtraReports.UI.XRLabel();
            this.MABARCODE = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYTAO1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TENPHONG1 = new DevExpress.XtraReports.UI.XRLabel();
            this.MABARCODE1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYTAO = new DevExpress.XtraReports.UI.XRLabel();
            this.NGUOITAO = new DevExpress.XtraReports.UI.XRLabel();
            this.DONGIA1 = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYCHIEU1 = new DevExpress.XtraReports.UI.XRLabel();
            this.BATDAU1 = new DevExpress.XtraReports.UI.XRLabel();
            this.LOAIVE = new DevExpress.XtraReports.UI.XRLabel();
            this.SOGHE1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TENPHIM1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.DONGIA = new DevExpress.XtraReports.UI.XRLabel();
            this.TENPHIM = new DevExpress.XtraReports.UI.XRLabel();
            this.SOGHE = new DevExpress.XtraReports.UI.XRLabel();
            this.TENPHONG = new DevExpress.XtraReports.UI.XRLabel();
            this.NGAYCHIEU = new DevExpress.XtraReports.UI.XRLabel();
            this.BATDAU = new DevExpress.XtraReports.UI.XRLabel();
            this.LOAIVE1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.NGUOITAO1,
            this.MABARCODE,
            this.NGAYTAO1,
            this.TENPHONG1,
            this.MABARCODE1,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel4,
            this.xrLabel3,
            this.NGAYTAO,
            this.NGUOITAO,
            this.DONGIA1,
            this.NGAYCHIEU1,
            this.BATDAU1,
            this.LOAIVE,
            this.SOGHE1,
            this.TENPHIM1,
            this.xrLabel15,
            this.DONGIA,
            this.TENPHIM,
            this.SOGHE,
            this.TENPHONG,
            this.NGAYCHIEU,
            this.BATDAU,
            this.LOAIVE1,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel8});
            this.Detail.Dpi = 254F;
            this.Detail.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Detail.HeightF = 500F;
            this.Detail.LockedInUserDesigner = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // NGUOITAO1
            // 
            this.NGUOITAO1.CanGrow = false;
            this.NGUOITAO1.Dpi = 254F;
            this.NGUOITAO1.Font = new System.Drawing.Font("Tahoma", 5F);
            this.NGUOITAO1.LocationFloat = new DevExpress.Utils.PointFloat(15.8871F, 402.0157F);
            this.NGUOITAO1.Name = "NGUOITAO1";
            this.NGUOITAO1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGUOITAO1.SizeF = new System.Drawing.SizeF(234.2517F, 25.00003F);
            this.NGUOITAO1.StylePriority.UseFont = false;
            this.NGUOITAO1.StylePriority.UsePadding = false;
            this.NGUOITAO1.StylePriority.UseTextAlignment = false;
            this.NGUOITAO1.Text = "Nguyễn Văn Thông";
            this.NGUOITAO1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // MABARCODE
            // 
            this.MABARCODE.CanGrow = false;
            this.MABARCODE.Dpi = 254F;
            this.MABARCODE.Font = new System.Drawing.Font("Tahoma", 5F);
            this.MABARCODE.LocationFloat = new DevExpress.Utils.PointFloat(123.6569F, 123.79F);
            this.MABARCODE.Name = "MABARCODE";
            this.MABARCODE.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.MABARCODE.SizeF = new System.Drawing.SizeF(126.4819F, 25.0036F);
            this.MABARCODE.StylePriority.UseFont = false;
            this.MABARCODE.StylePriority.UsePadding = false;
            this.MABARCODE.StylePriority.UseTextAlignment = false;
            this.MABARCODE.Text = "12345678";
            this.MABARCODE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // NGAYTAO1
            // 
            this.NGAYTAO1.CanGrow = false;
            this.NGAYTAO1.Dpi = 254F;
            this.NGAYTAO1.Font = new System.Drawing.Font("Tahoma", 5F);
            this.NGAYTAO1.LocationFloat = new DevExpress.Utils.PointFloat(15.89F, 427.0157F);
            this.NGAYTAO1.Name = "NGAYTAO1";
            this.NGAYTAO1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYTAO1.SizeF = new System.Drawing.SizeF(234.2488F, 25F);
            this.NGAYTAO1.StylePriority.UseFont = false;
            this.NGAYTAO1.StylePriority.UsePadding = false;
            this.NGAYTAO1.StylePriority.UseTextAlignment = false;
            this.NGAYTAO1.Text = "21/12/2012 20:30";
            this.NGAYTAO1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // TENPHONG1
            // 
            this.TENPHONG1.CanGrow = false;
            this.TENPHONG1.Dpi = 254F;
            this.TENPHONG1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.TENPHONG1.LocationFloat = new DevExpress.Utils.PointFloat(797.1707F, 266.6338F);
            this.TENPHONG1.Name = "TENPHONG1";
            this.TENPHONG1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENPHONG1.SizeF = new System.Drawing.SizeF(89.42F, 40.27F);
            this.TENPHONG1.StylePriority.UseFont = false;
            this.TENPHONG1.StylePriority.UsePadding = false;
            this.TENPHONG1.StylePriority.UseTextAlignment = false;
            this.TENPHONG1.Text = "03";
            this.TENPHONG1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // MABARCODE1
            // 
            this.MABARCODE1.CanGrow = false;
            this.MABARCODE1.Dpi = 254F;
            this.MABARCODE1.Font = new System.Drawing.Font("Tahoma", 5F);
            this.MABARCODE1.LocationFloat = new DevExpress.Utils.PointFloat(550F, 123.79F);
            this.MABARCODE1.Name = "MABARCODE1";
            this.MABARCODE1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.MABARCODE1.SizeF = new System.Drawing.SizeF(126.4819F, 25.0036F);
            this.MABARCODE1.StylePriority.UseFont = false;
            this.MABARCODE1.StylePriority.UsePadding = false;
            this.MABARCODE1.StylePriority.UseTextAlignment = false;
            this.MABARCODE1.Text = "12345678";
            this.MABARCODE1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel7
            // 
            this.xrLabel7.CanGrow = false;
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.Font = new System.Drawing.Font("Tahoma", 6F);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(728.6447F, 377.0155F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(177.6808F, 26.11166F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UsePadding = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Giá vé(Price)";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel6
            // 
            this.xrLabel6.CanGrow = false;
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.Font = new System.Drawing.Font("Tahoma", 6F);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(778.6378F, 306.9038F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(127.6876F, 26.11166F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UsePadding = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Rạp(Screen)";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel5
            // 
            this.xrLabel5.CanGrow = false;
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.Font = new System.Drawing.Font("Tahoma", 6F);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(653.1308F, 306.9038F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(109.8212F, 26.11166F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UsePadding = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Ghế(Seat)";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel4
            // 
            this.xrLabel4.CanGrow = false;
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Tahoma", 6F);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(470.1308F, 306.9038F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(136.1334F, 26.11166F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UsePadding = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Ngày(Date)";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel3
            // 
            this.xrLabel3.CanGrow = false;
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Tahoma", 6F);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(303.7171F, 306.9038F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(157.1831F, 26.11166F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UsePadding = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Giờ(Time)";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // NGAYTAO
            // 
            this.NGAYTAO.CanGrow = false;
            this.NGAYTAO.Dpi = 254F;
            this.NGAYTAO.Font = new System.Drawing.Font("Tahoma", 5F);
            this.NGAYTAO.LocationFloat = new DevExpress.Utils.PointFloat(728.6447F, 428.1272F);
            this.NGAYTAO.Name = "NGAYTAO";
            this.NGAYTAO.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYTAO.SizeF = new System.Drawing.SizeF(177.6809F, 25F);
            this.NGAYTAO.StylePriority.UseFont = false;
            this.NGAYTAO.StylePriority.UsePadding = false;
            this.NGAYTAO.StylePriority.UseTextAlignment = false;
            this.NGAYTAO.Text = "21/12/2012 20:30";
            this.NGAYTAO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // NGUOITAO
            // 
            this.NGUOITAO.CanGrow = false;
            this.NGUOITAO.Dpi = 254F;
            this.NGUOITAO.Font = new System.Drawing.Font("Tahoma", 5F);
            this.NGUOITAO.LocationFloat = new DevExpress.Utils.PointFloat(303.7171F, 428.1272F);
            this.NGUOITAO.Name = "NGUOITAO";
            this.NGUOITAO.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGUOITAO.SizeF = new System.Drawing.SizeF(424.1306F, 25F);
            this.NGUOITAO.StylePriority.UseFont = false;
            this.NGUOITAO.StylePriority.UsePadding = false;
            this.NGUOITAO.StylePriority.UseTextAlignment = false;
            this.NGUOITAO.Text = "Nguyễn Văn Thông";
            this.NGUOITAO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DONGIA1
            // 
            this.DONGIA1.CanGrow = false;
            this.DONGIA1.Dpi = 254F;
            this.DONGIA1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.DONGIA1.LocationFloat = new DevExpress.Utils.PointFloat(728.6447F, 333.0154F);
            this.DONGIA1.Name = "DONGIA1";
            this.DONGIA1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.DONGIA1.SizeF = new System.Drawing.SizeF(177.6807F, 44.00009F);
            this.DONGIA1.StylePriority.UseFont = false;
            this.DONGIA1.StylePriority.UsePadding = false;
            this.DONGIA1.StylePriority.UseTextAlignment = false;
            this.DONGIA1.Text = "500,000";
            this.DONGIA1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // NGAYCHIEU1
            // 
            this.NGAYCHIEU1.CanGrow = false;
            this.NGAYCHIEU1.Dpi = 254F;
            this.NGAYCHIEU1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.NGAYCHIEU1.LocationFloat = new DevExpress.Utils.PointFloat(429.32F, 266.6338F);
            this.NGAYCHIEU1.Name = "NGAYCHIEU1";
            this.NGAYCHIEU1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYCHIEU1.SizeF = new System.Drawing.SizeF(270.29F, 40.27F);
            this.NGAYCHIEU1.StylePriority.UseFont = false;
            this.NGAYCHIEU1.StylePriority.UsePadding = false;
            this.NGAYCHIEU1.StylePriority.UseTextAlignment = false;
            this.NGAYCHIEU1.Text = "21/12/2012";
            this.NGAYCHIEU1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // BATDAU1
            // 
            this.BATDAU1.CanGrow = false;
            this.BATDAU1.Dpi = 254F;
            this.BATDAU1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.BATDAU1.LocationFloat = new DevExpress.Utils.PointFloat(303.7171F, 266.6338F);
            this.BATDAU1.Name = "BATDAU1";
            this.BATDAU1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.BATDAU1.SizeF = new System.Drawing.SizeF(124.8116F, 40.26823F);
            this.BATDAU1.StylePriority.UseFont = false;
            this.BATDAU1.StylePriority.UsePadding = false;
            this.BATDAU1.StylePriority.UseTextAlignment = false;
            this.BATDAU1.Text = "20:30";
            this.BATDAU1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // LOAIVE
            // 
            this.LOAIVE.CanGrow = false;
            this.LOAIVE.Dpi = 254F;
            this.LOAIVE.Font = new System.Drawing.Font("Tahoma", 8F);
            this.LOAIVE.LocationFloat = new DevExpress.Utils.PointFloat(303.7171F, 333.0154F);
            this.LOAIVE.Name = "LOAIVE";
            this.LOAIVE.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.LOAIVE.SizeF = new System.Drawing.SizeF(424.1307F, 95.11172F);
            this.LOAIVE.StylePriority.UseFont = false;
            this.LOAIVE.StylePriority.UsePadding = false;
            this.LOAIVE.StylePriority.UseTextAlignment = false;
            this.LOAIVE.Text = "VÉ MỜI VIP CÓ COMBO  vbvb";
            this.LOAIVE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // SOGHE1
            // 
            this.SOGHE1.CanGrow = false;
            this.SOGHE1.Dpi = 254F;
            this.SOGHE1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.SOGHE1.LocationFloat = new DevExpress.Utils.PointFloat(699.61F, 266.6338F);
            this.SOGHE1.Name = "SOGHE1";
            this.SOGHE1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.SOGHE1.SizeF = new System.Drawing.SizeF(97.56073F, 40.26825F);
            this.SOGHE1.StylePriority.UseFont = false;
            this.SOGHE1.StylePriority.UsePadding = false;
            this.SOGHE1.StylePriority.UseTextAlignment = false;
            this.SOGHE1.Text = "F22";
            this.SOGHE1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TENPHIM1
            // 
            this.TENPHIM1.CanGrow = false;
            this.TENPHIM1.Dpi = 254F;
            this.TENPHIM1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TENPHIM1.LocationFloat = new DevExpress.Utils.PointFloat(303.7171F, 148.7936F);
            this.TENPHIM1.Name = "TENPHIM1";
            this.TENPHIM1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENPHIM1.SizeF = new System.Drawing.SizeF(602.6083F, 117.8402F);
            this.TENPHIM1.StylePriority.UseFont = false;
            this.TENPHIM1.StylePriority.UsePadding = false;
            this.TENPHIM1.StylePriority.UseTextAlignment = false;
            this.TENPHIM1.Text = "Ám Ảnh Bóng Đêm - Out Of The Dark ";
            this.TENPHIM1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel15
            // 
            this.xrLabel15.CanGrow = false;
            this.xrLabel15.Dpi = 254F;
            this.xrLabel15.Font = new System.Drawing.Font("Tahoma", 5F);
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(728.6447F, 403.1272F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(177.6807F, 25F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UsePadding = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "Đã gồm VAT 5% ";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DONGIA
            // 
            this.DONGIA.CanGrow = false;
            this.DONGIA.Dpi = 254F;
            this.DONGIA.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.DONGIA.LocationFloat = new DevExpress.Utils.PointFloat(123.9972F, 318.4461F);
            this.DONGIA.Name = "DONGIA";
            this.DONGIA.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.DONGIA.SizeF = new System.Drawing.SizeF(126.1416F, 33.56607F);
            this.DONGIA.StylePriority.UseFont = false;
            this.DONGIA.StylePriority.UsePadding = false;
            this.DONGIA.StylePriority.UseTextAlignment = false;
            this.DONGIA.Text = "500,000";
            this.DONGIA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TENPHIM
            // 
            this.TENPHIM.CanGrow = false;
            this.TENPHIM.Dpi = 254F;
            this.TENPHIM.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.TENPHIM.LocationFloat = new DevExpress.Utils.PointFloat(15.8871F, 148.7936F);
            this.TENPHIM.Name = "TENPHIM";
            this.TENPHIM.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENPHIM.SizeF = new System.Drawing.SizeF(234.2517F, 103.526F);
            this.TENPHIM.StylePriority.UseFont = false;
            this.TENPHIM.StylePriority.UsePadding = false;
            this.TENPHIM.StylePriority.UseTextAlignment = false;
            this.TENPHIM.Text = "Ám Ảnh Bóng Đêm - Out Of The Dark";
            this.TENPHIM.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // SOGHE
            // 
            this.SOGHE.CanGrow = false;
            this.SOGHE.Dpi = 254F;
            this.SOGHE.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.SOGHE.LocationFloat = new DevExpress.Utils.PointFloat(15.89F, 284.8733F);
            this.SOGHE.Name = "SOGHE";
            this.SOGHE.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.SOGHE.SizeF = new System.Drawing.SizeF(112.06F, 33.5661F);
            this.SOGHE.StylePriority.UseFont = false;
            this.SOGHE.StylePriority.UsePadding = false;
            this.SOGHE.StylePriority.UseTextAlignment = false;
            this.SOGHE.Text = "F22";
            this.SOGHE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TENPHONG
            // 
            this.TENPHONG.CanGrow = false;
            this.TENPHONG.Dpi = 254F;
            this.TENPHONG.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.TENPHONG.LocationFloat = new DevExpress.Utils.PointFloat(127.95F, 284.88F);
            this.TENPHONG.Name = "TENPHONG";
            this.TENPHONG.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.TENPHONG.SizeF = new System.Drawing.SizeF(122.1888F, 33.56607F);
            this.TENPHONG.StylePriority.UseFont = false;
            this.TENPHONG.StylePriority.UsePadding = false;
            this.TENPHONG.StylePriority.UseTextAlignment = false;
            this.TENPHONG.Text = "03";
            this.TENPHONG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // NGAYCHIEU
            // 
            this.NGAYCHIEU.CanGrow = false;
            this.NGAYCHIEU.Dpi = 254F;
            this.NGAYCHIEU.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Bold);
            this.NGAYCHIEU.LocationFloat = new DevExpress.Utils.PointFloat(96.49113F, 253.97F);
            this.NGAYCHIEU.Name = "NGAYCHIEU";
            this.NGAYCHIEU.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.NGAYCHIEU.SizeF = new System.Drawing.SizeF(153.6477F, 30.9032F);
            this.NGAYCHIEU.StylePriority.UseFont = false;
            this.NGAYCHIEU.StylePriority.UsePadding = false;
            this.NGAYCHIEU.StylePriority.UseTextAlignment = false;
            this.NGAYCHIEU.Text = "21/12/2012";
            this.NGAYCHIEU.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // BATDAU
            // 
            this.BATDAU.CanGrow = false;
            this.BATDAU.Dpi = 254F;
            this.BATDAU.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.BATDAU.LocationFloat = new DevExpress.Utils.PointFloat(15.88719F, 253.97F);
            this.BATDAU.Name = "BATDAU";
            this.BATDAU.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.BATDAU.SizeF = new System.Drawing.SizeF(93.8331F, 30.90329F);
            this.BATDAU.StylePriority.UseFont = false;
            this.BATDAU.StylePriority.UsePadding = false;
            this.BATDAU.StylePriority.UseTextAlignment = false;
            this.BATDAU.Text = "20:30";
            this.BATDAU.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // LOAIVE1
            // 
            this.LOAIVE1.CanGrow = false;
            this.LOAIVE1.Dpi = 254F;
            this.LOAIVE1.Font = new System.Drawing.Font("Tahoma", 5F);
            this.LOAIVE1.LocationFloat = new DevExpress.Utils.PointFloat(15.88715F, 352.012F);
            this.LOAIVE1.Name = "LOAIVE1";
            this.LOAIVE1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.LOAIVE1.SizeF = new System.Drawing.SizeF(234.2516F, 25.00003F);
            this.LOAIVE1.StylePriority.UseFont = false;
            this.LOAIVE1.StylePriority.UsePadding = false;
            this.LOAIVE1.StylePriority.UseTextAlignment = false;
            this.LOAIVE1.Text = "VÉ MỜI VIP CÓ COMBO";
            this.LOAIVE1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel10
            // 
            this.xrLabel10.CanGrow = false;
            this.xrLabel10.Dpi = 254F;
            this.xrLabel10.Font = new System.Drawing.Font("Tahoma", 5F);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(15.88715F, 377.0121F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(234.2516F, 25F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UsePadding = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "Đã gồm VAT 5% ";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel9
            // 
            this.xrLabel9.CanGrow = false;
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(805.3212F, 454.7775F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(101.0042F, 30.05848F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UsePadding = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "(HỦY)";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel8
            // 
            this.xrLabel8.CanGrow = false;
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(146.014F, 452.0157F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(104.1248F, 30F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UsePadding = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "(HỦY)";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // formattingRule1
            // 
            this.formattingRule1.Name = "formattingRule1";
            // 
            // rptVerapphim_Huy
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Dpi = 254F;
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
            this.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 254F);
            this.PageHeight = 500;
            this.PageWidth = 930;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.ShowPrintMarginsWarning = false;
            this.ShowPrintStatusDialog = false;
            this.SnapGridSize = 1F;
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel DONGIA;
        private DevExpress.XtraReports.UI.XRLabel TENPHIM;
        private DevExpress.XtraReports.UI.XRLabel SOGHE;
        private DevExpress.XtraReports.UI.XRLabel TENPHONG;
        private DevExpress.XtraReports.UI.XRLabel NGAYCHIEU;
        private DevExpress.XtraReports.UI.XRLabel BATDAU;
        private DevExpress.XtraReports.UI.XRLabel LOAIVE1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel MABARCODE1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel NGAYTAO;
        private DevExpress.XtraReports.UI.XRLabel NGUOITAO;
        private DevExpress.XtraReports.UI.XRLabel DONGIA1;
        private DevExpress.XtraReports.UI.XRLabel NGAYCHIEU1;
        private DevExpress.XtraReports.UI.XRLabel BATDAU1;
        private DevExpress.XtraReports.UI.XRLabel LOAIVE;
        private DevExpress.XtraReports.UI.XRLabel SOGHE1;
        private DevExpress.XtraReports.UI.XRLabel TENPHIM1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel TENPHONG1;
        private DevExpress.XtraReports.UI.XRLabel NGUOITAO1;
        private DevExpress.XtraReports.UI.XRLabel MABARCODE;
        private DevExpress.XtraReports.UI.XRLabel NGAYTAO1;
    }
}
