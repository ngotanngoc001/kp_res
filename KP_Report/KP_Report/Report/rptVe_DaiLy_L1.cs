﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
namespace KP_Report
{
    /// <summary>
    /// Report Ve
    /// </summary>
    public partial class rptVe_DaiLy_L1 : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report Ve
        /// </summary>
        public rptVe_DaiLy_L1()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        /// 
        

        public void BindData()
        {
            MABARCODE.DataBindings.Add("Text", DataSource, "MABARCODE");

            //MABARCODE1.DataBindings.Add("Text", DataSource, "MABARCODE");
            
            TENTUYEN1.DataBindings.Add("Text", DataSource, "TENTUYEN");
            
            TENDOITUONG1.DataBindings.Add("Text", DataSource, "TENDOITUONG");
            
            SOGHE1.DataBindings.Add("Text", DataSource, "SOGHE");
            LOAIGHE.DataBindings.Add("Text", DataSource, "TENKHUVUC");

            TENHANHKHACH.DataBindings.Add("Text", DataSource, "TENHANHKHACH");
            
            NGAYDI1.DataBindings.Add("Text", DataSource, "NGAYDI", "{0:dd/MM/yyyy}");
            
            GIODI1.DataBindings.Add("Text", DataSource, "GIODI");
            NGUOITAO.DataBindings.Add("Text", DataSource, "NGUOITAO");
            NGAYTAO.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy HH : mm}");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:n0}");
            
            DataTable dt = (DataTable)DataSource;
            TENLOAIVE.Text = "(" + dt.Rows[0]["TENLOAIVE"].ToString() + ")";

            try
            {
                MABARCODE1.Text = Right_str(dt.Rows[0]["MABARCODE"].ToString(), 7);
            }
            catch
            {
                MABARCODE1.Text = dt.Rows[0]["MABARCODE"].ToString();
            }
        }

        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH,PHUTHUQUATAI" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
            txtMST.Text = "MST : " + dt.Rows[0]["MASOTHUE"].ToString();
            txtMauso.Text = dt.Rows[0]["MAUSOPHIEUHD"].ToString();
            txtKyhieu.Text = dt.Rows[0]["PHUTHUQUATAI"].ToString();
        }

        private string Right_str(string value, int length)
        {
            return value.Substring(value.Length - length);
        }
    }
}
