﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
using System.Drawing.Imaging;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBangKeChiTietBanVe : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBangKeChiTietBanVe()
        {
            InitializeComponent();
            Setheader();
        }
        private void ConvertHinh(ref PictureBox pic, Byte[] values)
        {
            MemoryStream ms = new MemoryStream(values);
            Bitmap bm = new Bitmap(ms);
            pic.Image = bm;
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            DOITUONG.DataBindings.Add("Text", DataSource, "DOITUONG");
            LOAIKH.DataBindings.Add("Text", DataSource, "LOAIKH");
            MA_HOADON.DataBindings.Add("Text", DataSource, "MA_HOADON");
            THANGDIEN.DataBindings.Add("Text", DataSource, "THANGDIEN", "{0:00}");
            NGAYDIEN.DataBindings.Add("Text", DataSource, "NGAYDIEN", "{0:00}");
            TEN.DataBindings.Add("Text", DataSource, "TEN");
            NGAYMUA.DataBindings.Add("Text", DataSource, "NGAYMUA", "{0:dd/MM/yyyy}");
            SOVE.DataBindings.Add("Text", DataSource, "SOVE");
            HANGVE.DataBindings.Add("Text", DataSource, "HANGVE");
            TONGTIENTCK.DataBindings.Add("Text", DataSource, "TONGTIENTCK", "{0:N0}");
            GIAMGIA.DataBindings.Add("Text", DataSource, "GIAMGIA", "{0:N0}");
            TONGTIEN.DataBindings.Add("Text", DataSource, "TONGTIEN", "{0:N0}");
            HINHTHUC.DataBindings.Add("Text", DataSource, "HINHTHUC");

            TONGSOVE.DataBindings.Add("Text", DataSource, "SOVE");
            TONGSOVE.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:N0}");
            TONGTONGTIEN.DataBindings.Add("Text", DataSource, "TONGTIEN");
            TONGTONGTIEN.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:N0}");

            DataTable dtSource = (DataTable)DataSource;
            txt_tieude1.Text = "( Từ ngày: " + dtSource.Rows[0]["TUNGAY"].ToString() + " - Đến ngày: " + dtSource.Rows[0]["DENNGAY"].ToString() + " )";
            txt_tieude.Text = dtSource.Rows[0]["HEARDER"].ToString();
        }
        int i = 0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
