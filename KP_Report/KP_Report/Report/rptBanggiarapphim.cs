﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBanggiarapphim : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBanggiarapphim()
        {
            InitializeComponent();
            GroupField grf = new GroupField("TENBANGGIA");
            GroupHeader1.GroupFields.Add(grf);
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
           
            byte[] tam = new byte[((byte[])dt.Rows[0]["HINHANH"]).LongLength];
            tam = (byte[])dt.Rows[0]["HINHANH"];
            System.IO.MemoryStream ms = new System.IO.MemoryStream(tam);
            Bitmap bm = new Bitmap(ms);
            logo.Image = bm;
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        int i ;
        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i = 0;
        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            TENKHUVUC.DataBindings.Add("Text", DataSource, "TENLOAIGHE");
            TENLOAIVE.DataBindings.Add("Text", DataSource, "TENLOAIVE");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:n0}");
            TENBANGGIA.DataBindings.Add("Text", DataSource, "TENBANGGIA");
            GHICHU.DataBindings.Add("Text", DataSource, "GHICHU");
            DataTable dtSource = (DataTable)DataSource;
            BANGGIA.Text = "( Bảng giá: " + dtSource.Rows[0]["BANGGIA"].ToString() + " )";
            NGAY.Text = "( Ngày: " + DateTime.Parse(dtSource.Rows[0]["NGAY"].ToString()).Day.ToString("00") + "/" + DateTime.Parse(dtSource.Rows[0]["NGAY"].ToString()).Month.ToString("00") + "/" + DateTime.Parse(dtSource.Rows[0]["NGAY"].ToString()).Year.ToString("0000")  + " )";
        }
    }
}
