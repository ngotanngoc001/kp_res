﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptDS_HoadonCancel : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptDS_HoadonCancel()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            lblTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            lblDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            lblDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            IDBILL.DataBindings.Add("Text", DataSource, "IDBILL");
            CASHIER.DataBindings.Add("Text", DataSource, "CASHIER");
            CASHIER_CANCEL.DataBindings.Add("Text", DataSource, "CASHIER_CANCEL");
            TRX_DATE.DataBindings.Add("Text", DataSource, "TRX_DATE", "{0:dd/MM/yyyy}");
            _TOTAL.DataBindings.Add("Text", DataSource, "TOTAL", "{0:#,###0}");
            __TOTAL.DataBindings.Add("Text", DataSource, "TOTAL");
            __TOTAL.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:#,###0}");

            DataTable dtSource = (DataTable)DataSource;

            lbl_location.Text = "Ngày: " + dtSource.Rows[0]["TODATE"].ToString() + " - " + dtSource.Rows[0]["FROMDATE"].ToString();
            lbl_date.Text = dtSource.Rows[0]["FILTER"].ToString();
        }

        int i = 0;
        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i = 0;
        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
