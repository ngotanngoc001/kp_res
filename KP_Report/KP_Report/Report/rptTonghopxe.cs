﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptTonghopxe : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptTonghopxe()
        {
            InitializeComponent();
            GroupField grf = new GroupField("LOAIVE1");
            GroupHeader1.GroupFields.Add(grf);
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }

        int i;
        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i = 0;
        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            TENTUYEN.DataBindings.Add("Text", DataSource, "TENTUYEN1");
            TENKHUVUC.DataBindings.Add("Text", DataSource, "TENKHUVUC1");
            NHANVIEN.DataBindings.Add("Text", DataSource, "NHANVIEN");
            TENDOITUONG.DataBindings.Add("Text", DataSource, "TENDOITUONG1");
            MABARCODE.DataBindings.Add("Text", DataSource, "SOGHE");
            TENHANHKHACH.DataBindings.Add("Text", DataSource, "TENHANHKHACH");
            LOAIXE.DataBindings.Add("Text", DataSource, "TENKHUVUC");
            BIENSO.DataBindings.Add("Text", DataSource, "BIENSO");
         //   NOISINH.DataBindings.Add("Text", DataSource, "NOISINH");
            NHANVIEN1.DataBindings.Add("Text", DataSource, "DIACHI");
            THOIGIAN.DataBindings.Add("Text", DataSource, "DIENTHOAI");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:n0}");
           // NHOMGHE.DataBindings.Add("Text", DataSource, "NHOMGHE");
        
            //TONGTHANHTHIEN.DataBindings.Add("Text", DataSource, "DONGIA");
            //TONGTHANHTHIEN.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            TUNGAY.DataBindings.Add("Text", DataSource, "TUNGAY", "{0:dd/MM/yyyy}");
            DENNGAY.DataBindings.Add("Text", DataSource, "DENNGAY", "{0:dd/MM/yyyy}");
           
            MABARCODE.DataBindings.Add("Text", DataSource, "MABARCODE");
            TENTUYEN1.DataBindings.Add("Text", DataSource, "TENTUYEN");
            TENDOITUONG1.DataBindings.Add("Text", DataSource, "TENDOITUONG");
            NGAYDI1.DataBindings.Add("Text", DataSource, "NGAYDI", "{0:dd/MM/yyyy}");
            GIODI1.DataBindings.Add("Text", DataSource, "GIODI");
            SOGHE.DataBindings.Add("Text", DataSource, "SOGHE");
            NHANVIEN1.DataBindings.Add("Text", DataSource, "NGUOITAO1");
            THOIGIAN.DataBindings.Add("Text", DataSource, "NGAYTAO1");
       
            TONGTHANHTHIEN.DataBindings.Add("Text", DataSource, "DONGIA");
            TONGTHANHTHIEN.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            TONGPHUTHU.DataBindings.Add("Text", DataSource, "PHUTHUXE");
            TONGPHUTHU.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            PHUTHUXE.DataBindings.Add("Text", DataSource, "PHUTHUXE", "{0:n0}");
            TONGSOGHE.DataBindings.Add("Text", DataSource, "MABARCODE");
            TONGSOGHE.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Count, "{0:n0}");
           
            LOAIVE1.DataBindings.Add("Text", DataSource, "LOAIVE1");
            DataTable dt = (DataTable)DataSource;
            int tb = 0;
            int pt = 0;
            int vb = 0;

            int vh = 0;
            int tbH = 0;
            int ptH = 0;
            int ptHH = 0;

            foreach (DataRow dr in dt.Rows)
            {
                if (dr["LOAIVE1"].ToString() == "Vé bán")
                {
                    vb = vb + 1;
                    tb = tb + int.Parse(dr["DONGIA"].ToString());
                    pt = pt + int.Parse(dr["PHUTHUXE"].ToString());
                }
                else
                {
                    vh = vh + 1;
                    tbH = tbH + int.Parse(dr["DONGIA"].ToString());
                    ptH = ptH + int.Parse(dr["PHUTHUXE"].ToString());
                    ptHH = ptHH + int.Parse(dr["PHUTHU"].ToString());
                }
            }
            VEBAN.Text = vb.ToString();
            VEHUY.Text = vh.ToString();
            TIENBAN.Text = (tb + pt).ToString("n0");
            TIENHUY.Text = (tbH + ptH).ToString("n0");
            PHUTHU.Text = ptHH.ToString("n0");
            SODUCUOI.Text = (tb+pt-(tbH +ptH )+ptHH ).ToString("n0");

            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["HTTT"].ToString() == "Chuyển khoản")
                {
                    TIENMAT.Text = "0";
                    CHUYENKHOAN.Text = SODUCUOI.Text;
                }
                else if (dt.Rows[0]["HTTT"].ToString() == "Tiền mặt")
                {
                    TIENMAT.Text = SODUCUOI.Text;
                    CHUYENKHOAN.Text = "0";
                }
                else
                {
                    TIENMAT.DataBindings.Add("Text", DataSource, "TIENMAT", "{0:n0}");
                    CHUYENKHOAN.DataBindings.Add("Text", DataSource, "CHUYENKHOAN", "{0:n0}");
                }
            }
        }
    }
}
