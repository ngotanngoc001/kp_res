﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
namespace KP_Report
{
    /// <summary>
    /// Report Ve
    /// </summary>
    public partial class rptVeXeMay_DaiLy_L2 : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report Ve
        /// </summary>
        public rptVeXeMay_DaiLy_L2()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        /// 
        

        public void BindData()
        {
            DataTable dt = (DataTable)DataSource;
            MABARCODE1.DataBindings.Add("Text", DataSource, "MABARCODE");
            
            TENTUYEN1.DataBindings.Add("Text", DataSource, "TENTUYEN");
            
            TENDOITUONG1.DataBindings.Add("Text", DataSource, "TENDOITUONG");

            BIENSO1.DataBindings.Add("Text", DataSource, "BIENSO");
            
            TENHANHKHACH.DataBindings.Add("Text", DataSource, "TENHANHKHACH");
            
            CMND.DataBindings.Add("Text", DataSource, "CMND");
            
            NGAYDI1.DataBindings.Add("Text", DataSource, "NGAYDI", "{0:dd/MM/yyyy}");
            
            GIODI1.DataBindings.Add("Text", DataSource, "GIODI");
            NGUOITAO.DataBindings.Add("Text", DataSource, "NGUOITAO");
            NGAYTAO.DataBindings.Add("Text", DataSource, "NGAYTAO", "{0:dd/MM/yyyy HH : mm}");
            
            MABARCODE2.Text = dt.Rows[0]["MABARCODE"].ToString();
            //BIENSO.Text = dt.Rows[0]["BIENSO"].ToString();
        }

        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
    }
}
