﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptVethanhtoan : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report  
        /// </summary>
        public rptVethanhtoan()
        {
            InitializeComponent();
            GroupField grf = new GroupField("LOAI");
            GroupHeader1.GroupFields.Add(grf);
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        int i=0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            FROMDATE.DataBindings.Add("Text", DataSource, "FROMDATE", "{0:dd/MM/yyyy HH:mm:ss}");
            TODATE.DataBindings.Add("Text", DataSource, "TODATE", "{0:dd/MM/yyyy HH:mm:ss}");
            NHANVIEN.DataBindings.Add("Text", DataSource, "NHANVIEN");
            LOAI1.DataBindings.Add("Text", DataSource, "LOAI");
            MABARCODE.DataBindings.Add("Text", DataSource, "MABARCODE");
            SOGHE.DataBindings.Add("Text", DataSource, "SOGHE");
            TENHANHKHACH.DataBindings.Add("Text", DataSource, "TENHANHKHACH");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:n0}");
            PHUTHU.DataBindings.Add("Text", DataSource, "PHUTHU", "{0:n0}");
            LOAIXE.DataBindings.Add("Text", DataSource, "TENKHUVUC");
            NGUOICAPNHAT.DataBindings.Add("Text", DataSource, "NGUOICAPNHAT");
            BIENSO.DataBindings.Add("Text", DataSource, "BIENSO");
            TENTUYEN.DataBindings.Add("Text", DataSource, "TENTUYEN");
            NGAYDI.DataBindings.Add("Text", DataSource, "NGAYDI","{0:dd/MM/yyyy}");
            GIODI.DataBindings.Add("Text", DataSource, "GIODI");
            sophieu.DataBindings.Add("Text", DataSource, "MAPHIEU");
            TENDOITUONG.DataBindings.Add("Text", DataSource, "TENDOITUONG");
                      
            TONGVE.DataBindings.Add("Text", DataSource, "MABARCODE");
            TONGVE.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Count, "{0:n0}");
            TONGDONGIA.DataBindings.Add("Text", DataSource, "DONGIA");
            TONGDONGIA.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
            TONGPHUTHU.DataBindings.Add("Text", DataSource, "PHUTHU");
            TONGPHUTHU.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");


            TONGVE1.DataBindings.Add("Text", DataSource, "MABARCODE");
            TONGVE1.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Count, "{0:n0}");
            TONGDONGIA1.DataBindings.Add("Text", DataSource, "DONGIA");
            TONGDONGIA1.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            TONGPHUTHU1.DataBindings.Add("Text", DataSource, "PHUTHU");
            TONGPHUTHU1.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
        }
    }
}
