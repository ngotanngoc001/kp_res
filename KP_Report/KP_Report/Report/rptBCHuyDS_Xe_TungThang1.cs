﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBCHuyDS_Xe_TungThang1 : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBCHuyDS_Xe_TungThang1()
        {
            InitializeComponent();
            Setheader();
            GroupField grf1 = new GroupField("NGUOITAO");
            GroupHeader1.GroupFields.Add(grf1);
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        int i=0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            TENTUYEN1.DataBindings.Add("Text", DataSource, "TUYEN");
            //TENDOITUONG1.DataBindings.Add("Text", DataSource, "TENDOITUONG");
            TENTUYEN.DataBindings.Add("Text", DataSource, "TENTUYEN");
            GIODI.DataBindings.Add("Text", DataSource, "GIODI");
            NGAYDI1.DataBindings.Add("Text", DataSource, "NGAY");
            NGAYDI.DataBindings.Add("Text", DataSource, "NGAYDI", "{0:dd/MM/yyyy}");
            //GIODI1.DataBindings.Add("Text", DataSource, "GIODI");
            MABARCODE.DataBindings.Add("Text", DataSource, "MABARCODE");
            SOGHE.DataBindings.Add("Text", DataSource, "SOGHE");
            TENHANHKHACH.DataBindings.Add("Text", DataSource, "TENHANHKHACH");
            DATE.DataBindings.Add("Text", DataSource, "ngaytao", "{0:dd/MM/yyyy HH:mm}");
            NHANVIEN.DataBindings.Add("Text", DataSource, "nguoitao");
            TENKHUVUC.DataBindings.Add("Text", DataSource, "TENKHUVUC");
            BIENSO.DataBindings.Add("Text", DataSource, "BIENSO");
            PHUTHUXE.DataBindings.Add("Text", DataSource, "PHUTHUXE", "{0:n0}");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:n0}");
            THANHTIEN.DataBindings.Add("Text", DataSource, "THANHTIEN", "{0:n0}");
            TONGVE.DataBindings.Add("Text", DataSource, "MABARCODE");
            TONGVE.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Count, "{0:n0}");
            TONGGIAVE.DataBindings.Add("Text", DataSource, "DONGIA");
            TONGGIAVE.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
            TONGPHUTHU.DataBindings.Add("Text", DataSource, "PHUTHUXE");
            TONGPHUTHU.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
            TONGTIEN.DataBindings.Add("Text", DataSource, "THANHTIEN");
            TONGTIEN.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");

            NHANVIEN1.DataBindings.Add("Text", DataSource, "nguoitao");
            TONGVE1.DataBindings.Add("Text", DataSource, "MABARCODE");
            TONGVE1.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Count, "{0:n0}");
            TONGGIAVE1.DataBindings.Add("Text", DataSource, "DONGIA");
            TONGGIAVE1.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            TONGPHUTHU1.DataBindings.Add("Text", DataSource, "PHUTHUXE");
            TONGPHUTHU1.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            TONGTIEN1.DataBindings.Add("Text", DataSource, "THANHTIEN");
            TONGTIEN1.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
        }
    }
}
