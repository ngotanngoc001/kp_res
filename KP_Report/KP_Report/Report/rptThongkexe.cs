﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;
namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptThongkexe : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptThongkexe()
        {
            InitializeComponent();
            GroupField grf = new GroupField("TENPHIEU");
            GroupHeader1.GroupFields.Add(grf);
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        int i ;
        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i = 0;
        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            TENTUYEN.DataBindings.Add("Text", DataSource, "TENTUYEN1");
            TENKHUVUC.DataBindings.Add("Text", DataSource, "TENKHUVUC1");
            TENDOITUONG.DataBindings.Add("Text", DataSource, "TENDOITUONG1");
            NHANVIEN.DataBindings.Add("Text", DataSource, "NHANVIEN");
            BIENSO.DataBindings.Add("Text", DataSource, "BIENSO");
            GHICHU.DataBindings.Add("Text", DataSource, "GHICHU");
            TENNGUOIGUI.DataBindings.Add("Text", DataSource, "TENNGUOIGUI");
            SOGHE.DataBindings.Add("Text", DataSource, "SOGHE");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:n0}");
            PHUTHU.DataBindings.Add("Text", DataSource, "PHUTHU", "{0:n0}");
            THANHTIEN.DataBindings.Add("Text", DataSource, "THANHTIEN", "{0:n0}");
            TENPHIEU.DataBindings.Add("Text", DataSource, "TENPHIEU");
            TONGTHANHTHIEN.DataBindings.Add("Text", DataSource, "THANHTIEN");
            TONGTHANHTHIEN.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            TONGPHUTHU.DataBindings.Add("Text", DataSource, "PHUTHU");
            TONGPHUTHU.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            TONGDONGIA.DataBindings.Add("Text", DataSource, "DONGIA");
            TONGDONGIA.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            TONGSOXE.DataBindings.Add("Text", DataSource, "DONGIA");
            TONGSOXE.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Count, "{0:n0}");
            TUNGAY.DataBindings.Add("Text", DataSource, "TUNGAY", "{0:dd/MM/yyyy}");
            DENNGAY.DataBindings.Add("Text", DataSource, "DENNGAY", "{0:dd/MM/yyyy}");
            DataTable dt = (DataTable)DataSource;
            TONGSOXE1.Text = dt.Rows.Count.ToString ();
        }

     
    }
}
