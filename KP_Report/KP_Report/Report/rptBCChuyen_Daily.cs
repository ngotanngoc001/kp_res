﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptBCChuyen_Daily : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptBCChuyen_Daily()
        {
            InitializeComponent();
            Setheader();
            GroupField grf = new GroupField("LOAI");
            GroupHeader1.GroupFields.Add(grf);
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            logo.Image = clsMain.ConvertImage(dt.Rows[0]["HINHANH"]);
            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        int i=0;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            stt.Text = i.ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            TENTUYEN1.DataBindings.Add("Text", DataSource, "TENTUYEN");
            TENDOITUONG1.DataBindings.Add("Text", DataSource, "TENDOITUONG");
            NGAYDI1.DataBindings.Add("Text", DataSource, "NGAYDI", "{0:dd/MM/yyyy}");
            GIODI1.DataBindings.Add("Text", DataSource, "GIODI");
            TENNHANVIEN1.DataBindings.Add("Text", DataSource, "Nhanvien1");
            LOAI.DataBindings.Add("Text", DataSource, "LOAI");
            TENKHUVUC.DataBindings.Add("Text", DataSource, "TENKHUVUC");
            SOLUONG.DataBindings.Add("Text", DataSource, "SOLUONG");
            DONGIA.DataBindings.Add("Text", DataSource, "DONGIA", "{0:n0}");
            PHUTHU.DataBindings.Add("Text", DataSource, "PHUTHU", "{0:n0}");
            THANHTIEN.DataBindings.Add("Text", DataSource, "THANHTIEN", "{0:n0}");

            sotien_giam.DataBindings.Add("Text", DataSource, "CT_GIAMTRU", "{0:n0}");
            sotien_congno.DataBindings.Add("Text", DataSource, "CT_CONGNO", "{0:n0}");
            sotien_datcoc.DataBindings.Add("Text", DataSource, "CT_DATCOC", "{0:n0}");
            sotien_chuyenkhoan.DataBindings.Add("Text", DataSource, "CT_CUYENKHOAN", "{0:n0}");
            HOAHONG.DataBindings.Add("Text", DataSource, "HOAHONG", "{0:n0}");
            DIENGIAI.DataBindings.Add("Text", DataSource, "DIENGIAI");



            TONGSOLUONG2.DataBindings.Add("Text", DataSource, "SOLUONG");
            TONGSOLUONG2.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            TONGTHANHTIEN2.DataBindings.Add("Text", DataSource, "THANHTIEN");
            TONGTHANHTIEN2.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:n0}");
            
            TONGTHANHTIEN1.DataBindings.Add("Text", DataSource, "THANHTIEN");
            TONGTHANHTIEN1.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
           
            GIAMTRU1.DataBindings.Add("Text", DataSource, "CT_GIAMTRU");
            GIAMTRU1.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");

            CONGNO1.DataBindings.Add("Text", DataSource, "CT_CONGNO");
            CONGNO1.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");

            DATCOC1.DataBindings.Add("Text", DataSource, "CT_DATCOC");
            DATCOC1.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");

            CHUYENKHOAN1.DataBindings.Add("Text", DataSource, "CT_CUYENKHOAN");
            CHUYENKHOAN1.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");

            HOAHONG1.DataBindings.Add("Text", DataSource, "HOAHONG");
            HOAHONG1.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n0}");
        }
    }
}
