﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using KP_UserManagement;

namespace KP_Report
{
    /// <summary>
    /// Report
    /// </summary>
    public partial class rptDS_KHTTNB : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report
        /// </summary>
        public rptDS_KHTTNB()
        {
            InitializeComponent();
            Setheader();
        }
        /// <summary>
        /// Load header cho report : tên công ty , địa chỉ ... được thiết lập ở hệ thống -> thiết lập báo cáo
        /// </summary>
        private void Setheader()
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
            txtDiachi.Text = "Địa chỉ : " + dt.Rows[0]["DIACHI"].ToString();
            txtDienthoai.Text = "Điện thoại : " + dt.Rows[0]["SODIENTHOAI"].ToString() + " - Fax : " + dt.Rows[0]["SOFAX"].ToString();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            DT_MADOITUONG.DataBindings.Add("Text", DataSource, "DT_MADOITUONG");
            DT_HOTEN.DataBindings.Add("Text", DataSource, "DT_HOTEN");
            DT_NGAYSINH.DataBindings.Add("Text", DataSource, "DT_NGAYSINH", "{0:dd/MM/yyyy}");
            GT.DataBindings.Add("Text", DataSource, "GT");
            DT_SOCMND.DataBindings.Add("Text", DataSource, "DT_SOCMND");
            DT_DIACHI.DataBindings.Add("Text", DataSource, "DT_DIACHI");
            DT_DIENTHOAI.DataBindings.Add("Text", DataSource, "DT_DIENTHOAI");
            SoDuTaiKhoan.DataBindings.Add("Text", DataSource, "SoDuTaiKhoan", "{0:#,###0}");
            
            NgayThamGia.DataBindings.Add("Text", DataSource, "NgayThamGia", "{0:dd/MM/yyyy}");
            DataTable dtSource = (DataTable)DataSource;

           
        }

        int i = 0;
        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i = 0;
        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            i++;
            STT.Text = i.ToString();
        }
    }
}
