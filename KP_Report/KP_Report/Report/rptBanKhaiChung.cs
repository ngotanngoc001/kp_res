﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace KP_Report
{
    /// <summary>
    /// Report Danh sach hanh khach
    /// </summary>
    public partial class rptBanKhaiChung : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Report Danh sach hanh khach
        /// </summary>
        public rptBanKhaiChung()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Đổ dữ liệu cho Report
        /// </summary>
        public void BindData()
        {
            DataTable myDT = (DataTable)DataSource;
            try
            {
                txtTenTau.Text = "1.1 Tên và loại tàu: " + myDT.Rows[0]["TENDOITUONG"].ToString() + "\n" + "Name and type of ship:";
                txtSoIMO.Text = "1.2 Số IMO: " + myDT.Rows[0]["IMO"].ToString() + "\n" + "IMO number:";
                txtHoHieu.Text = "1.3 Hô hiệu: " + myDT.Rows[0]["HOHIEU"].ToString() + "\n" + "Call sign";
                txtQuocTichTau.Text = "4. Quốc tịch tàu: " + myDT.Rows[0]["QUOCTICHTAU"].ToString() + "\n" + "Flag State of ship";
                txtTenThuyenTruong.Text = "5. Tên thuyền trưởng" + "\n" + "Name of master: " +  myDT.Rows[0]["TENTHUYENVIEN"].ToString();
                txtGiayChungNhanDK.Text = "7. Giấy chứng nhận đăng ký (Số, ngày cấp, cảng): " + myDT.Rows[0]["CHUNGNHAN"].ToString() + "\n" + "Certificate of registry (Port, date; number)";
                txtTongDungTich.Text = "9. Tổng dung tích: " + myDT.Rows[0]["TONGDUNGTICH"].ToString() + "\n" + "Gross tonnage";
                txtDungTichCoIch.Text = "10. Dung tích có ích: " + myDT.Rows[0]["DUNGTICHCOICH"].ToString() + "\n" + "Net tonnage";
                txtViTriTau.Text = "11. Vị trí tàu tại cảng: " + myDT.Rows[0]["VITRITAU"].ToString() + "\n" + "Position of the ship in the port (berth or station)";
                txtDacDiemChinh.Text = "12. Đặc điểm chính của chuyến đi (các cảng trước và các cảng sẽ đến, gạch chân các cảng sẽ dỡ hàng) số hàng còn lại" + "\n" + "Brief particulars of voyage (previous and subsequent port of call; underline where remaining cargo will be discharged): " + myDT.Rows[0]["DACDIEM"].ToString();
                txtMoTaTomTac.Text = "13. Mô tả tóm tắt về hàng hóa: " + myDT.Rows[0]["MOTA"].ToString() + "\n" + "Brief description of the cargo";
                txtSoThuyenVien.Text = "14. Số thuyền viên (gồm cả thuyền trưởng)" + "\n" + "Number of crew (inl. master): " + myDT.Rows[0]["SOTHUYENVIEN"].ToString();
                txtSoHanhKhach.Text = "15. Số hành khách: " + myDT.Rows[0]["SOHANHKHACH"].ToString() + "\n" + "Number of passenger";
                txtGhiChu.Text = "16. Ghi chú" + "\n" + "Remarks: " + "\n" + myDT.Rows[0]["THONGTIN"].ToString();
                txtBanKhaiHangHoa.Text = "17. Bản khai hàng hóa: " + myDT.Rows[0]["BKHANGHOA"].ToString() + "\n" + "Cargo Declaration";
                txtBanKhaiDuTru.Text = "18. Bản khai dự trữ của tàu: " + myDT.Rows[0]["BKDUTRUTAU"].ToString() + "\n" + "Ship’s Stores Declaration";
                txtDanhSachThuyenVien.Text = "19. Danh sách thuyền viên: " + myDT.Rows[0]["DSTHUYENVIEN"].ToString() + "\n" + "Crew List";
                txtDanhSachHanhKhach.Text = "20. Danh sách hành khách: " + myDT.Rows[0]["DSHANHKHACH"].ToString() + "\n" + "Passenger List";
                txtBanKhaiHanhLy.Text = "22. Bản khai hành lý thuyền viên(*)" + "\n" + "Crew’s Effects Declaration(*): " + myDT.Rows[0]["HANHLYTHUYENVIEN"].ToString();
                txtBanKhaiYTe.Text = "23. Bản khai kiểm dịch y tế(*)" + "\n" + "Maritime Declaration of Health(*): " + myDT.Rows[0]["KIEMDICHYTE"].ToString();
            }
            catch
            {
            }
        }
        int j = 0;
        private void PageHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (j == 0)
            {
                j++;
                xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            }
            else
            {
                xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom ;
            }
        }
    }
}
