﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using System.Data;
using KP_Report;
using KP_UserManagement;
using System.IO;
using System.Windows.Forms;


namespace KP_Report
{
    public partial class rpt_ketca_58 : DevExpress.XtraReports.UI.XtraReport
    {
        //public int checkBill=0;

        /// <summary>
        /// rpt_ketca
        /// </summary>
        public rpt_ketca_58()
        {
            InitializeComponent();                      
            Setheader();            
        }
        /// <summary>
        /// BindData
        /// </summary>
        public void BindData()
        {
            //lb_Quay.DataBindings.Add("Text", DataSource, "maQuay");
            lbTenQuay.DataBindings.Add("Text", DataSource, "tenQuay");
            lb_TenNV.DataBindings.Add("Text", DataSource, "tenNV");
            lb_ngay.DataBindings.Add("Text", DataSource, "ngayHD", "{0:dd-MM-yyyy HH:mm:ss}");     
            //lbMaNV.DataBindings.Add("Text", DataSource, "maNV");
            //lbMaCa.DataBindings.Add("Text", DataSource, "maCaBan");
            lbTenCa.DataBindings.Add("Text", DataSource, "tenCaBan");
            tientamung.DataBindings.Add("Text", DataSource, "TienTamUng", "{0:#,###0}");
            lbTienChietKhau.DataBindings.Add("Text", DataSource, "giamgia", "{0:#,###0}");
            lbTongTienBan.DataBindings.Add("Text", DataSource, "tongTienBan", "{0:#,###0}");
            lbTienphuthu.DataBindings.Add("Text", DataSource, "phuthu", "{0:#,###0}");
            lbTongDoanhThu.DataBindings.Add("Text", DataSource, "tongTien", "{0:#,###0}");

            lbTienmat.DataBindings.Add("Text", DataSource, "tienmat", "{0:#,###0}");
            lbThe.DataBindings.Add("Text", DataSource, "tienthe", "{0:#,###0}");
            lbVoucher.DataBindings.Add("Text", DataSource, "voucher", "{0:#,###0}");
            lbTTNB.DataBindings.Add("Text", DataSource, "TTNB", "{0:#,###0}");
            lblTienCoc.DataBindings.Add("Text", DataSource, "TienCoc", "{0:#,###0}");
        } 
        private void Setheader()
        {
            StreamReader str = new StreamReader(Application.StartupPath + "/systeminfo.txt");
            string sMacuahang = "";
            string line;
            while ((line = str.ReadLine()) != null)
            {
                string[] value = line.Split(',');
                sMacuahang = value[0].ToString();
            }

            string sSQL = "";
            sSQL += "Select MA_CUAHANG,	TEN_CUAHANG,GHICHU,	SUDUNG,	Header,	Footer, CONVERT(VARCHAR(20),GETDATE(),103) + ' ' + CONVERT(VARCHAR(20),GETDATE(),108) AS Ngay" + "\n";
            sSQL += "From CUAHANG" + "\n";
            sSQL += "Where MA_CUAHANG  =" + clsMain.SQLString(sMacuahang) + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);

            lb_Header.Text = dt.Rows[0]["Header"].ToString();
        }      
    }
}
