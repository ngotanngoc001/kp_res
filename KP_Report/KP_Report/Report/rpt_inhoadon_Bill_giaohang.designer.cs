﻿namespace KP_Report
{
    partial class rpt_inhoadon_Bill_giaohang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt_inhoadon_Bill_giaohang));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.tblMain = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cl_HangHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.cl_soluong = new DevExpress.XtraReports.UI.XRTableCell();
            this.cl_giaban = new DevExpress.XtraReports.UI.XRTableCell();
            this.CHIETKHAU = new DevExpress.XtraReports.UI.XRTableCell();
            this.cl_thanhtien = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.lb_Quay = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_Header = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_nhanvien = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_ngay = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lb_mahoadon = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_ban = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_NameBill = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.lbTenKH = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_diachigiaohang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.lbDienthoai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.lbTiendatcoc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbPhihucvu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_tienkhachtra = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_conlai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.lb_giamgia = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_tongcong = new DevExpress.XtraReports.UI.XRLabel();
            this.lbFooter = new DevExpress.XtraReports.UI.XRLabel();
            this.lb_MaHD = new DevExpress.XtraReports.UI.XRLabel();
            this.logo = new DevExpress.XtraReports.UI.XRPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.tblMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblMain});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 42.33334F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // tblMain
            // 
            this.tblMain.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.tblMain.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.tblMain.Dpi = 254F;
            this.tblMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblMain.ForeColor = System.Drawing.Color.Blue;
            this.tblMain.LocationFloat = new DevExpress.Utils.PointFloat(15.79522F, 0F);
            this.tblMain.Name = "tblMain";
            this.tblMain.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.tblMain.SizeF = new System.Drawing.SizeF(718.82F, 42.33334F);
            this.tblMain.StylePriority.UseBorderDashStyle = false;
            this.tblMain.StylePriority.UseBorders = false;
            this.tblMain.StylePriority.UseFont = false;
            this.tblMain.StylePriority.UseForeColor = false;
            this.tblMain.StylePriority.UseTextAlignment = false;
            this.tblMain.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cl_HangHoa,
            this.cl_soluong,
            this.cl_giaban,
            this.CHIETKHAU,
            this.cl_thanhtien});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // cl_HangHoa
            // 
            this.cl_HangHoa.Dpi = 254F;
            this.cl_HangHoa.Name = "cl_HangHoa";
            this.cl_HangHoa.StylePriority.UseTextAlignment = false;
            this.cl_HangHoa.Text = "sss";
            this.cl_HangHoa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cl_HangHoa.Weight = 0.8446442567549165D;
            // 
            // cl_soluong
            // 
            this.cl_soluong.Dpi = 254F;
            this.cl_soluong.Name = "cl_soluong";
            this.cl_soluong.StylePriority.UseTextAlignment = false;
            this.cl_soluong.Text = "100";
            this.cl_soluong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cl_soluong.Weight = 0.286846336805557D;
            // 
            // cl_giaban
            // 
            this.cl_giaban.Dpi = 254F;
            this.cl_giaban.Name = "cl_giaban";
            this.cl_giaban.StylePriority.UseTextAlignment = false;
            this.cl_giaban.Text = "7,000,000";
            this.cl_giaban.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cl_giaban.Weight = 0.53716759223671073D;
            // 
            // CHIETKHAU
            // 
            this.CHIETKHAU.Dpi = 254F;
            this.CHIETKHAU.Name = "CHIETKHAU";
            this.CHIETKHAU.StylePriority.UseTextAlignment = false;
            this.CHIETKHAU.Text = "100%";
            this.CHIETKHAU.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CHIETKHAU.Weight = 0.2998390186271287D;
            // 
            // cl_thanhtien
            // 
            this.cl_thanhtien.Dpi = 254F;
            this.cl_thanhtien.Name = "cl_thanhtien";
            this.cl_thanhtien.StylePriority.UseTextAlignment = false;
            this.cl_thanhtien.Text = "7,000,000";
            this.cl_thanhtien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cl_thanhtien.Weight = 0.65002156747624729D;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 20F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 145F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.logo,
            this.lb_Quay,
            this.xrLabel15,
            this.lb_Header,
            this.lb_nhanvien,
            this.lb_ngay,
            this.xrLine2,
            this.xrTable1,
            this.lb_mahoadon,
            this.lb_ban,
            this.lb_NameBill,
            this.xrLabel3,
            this.xrLabel5,
            this.xrLabel4,
            this.xrLine1});
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 731.5197F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // lb_Quay
            // 
            this.lb_Quay.Dpi = 254F;
            this.lb_Quay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Quay.ForeColor = System.Drawing.Color.Blue;
            this.lb_Quay.LocationFloat = new DevExpress.Utils.PointFloat(127.6693F, 540.173F);
            this.lb_Quay.Name = "lb_Quay";
            this.lb_Quay.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_Quay.SizeF = new System.Drawing.SizeF(221.5044F, 45.1908F);
            this.lb_Quay.StylePriority.UseFont = false;
            this.lb_Quay.StylePriority.UseForeColor = false;
            this.lb_Quay.Text = "xrLabel6";
            // 
            // xrLabel15
            // 
            this.xrLabel15.Dpi = 254F;
            this.xrLabel15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(31.66938F, 540.173F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(96F, 45.19086F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseForeColor = false;
            this.xrLabel15.Text = "Quầy:";
            // 
            // lb_Header
            // 
            this.lb_Header.Dpi = 254F;
            this.lb_Header.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Header.ForeColor = System.Drawing.Color.Blue;
            this.lb_Header.LocationFloat = new DevExpress.Utils.PointFloat(24.04965F, 309.5626F);
            this.lb_Header.Multiline = true;
            this.lb_Header.Name = "lb_Header";
            this.lb_Header.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_Header.SizeF = new System.Drawing.SizeF(710.5649F, 50.27086F);
            this.lb_Header.StylePriority.UseFont = false;
            this.lb_Header.StylePriority.UseForeColor = false;
            this.lb_Header.StylePriority.UseTextAlignment = false;
            this.lb_Header.Text = "Điện Máy Thiên Hòa";
            this.lb_Header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // lb_nhanvien
            // 
            this.lb_nhanvien.Dpi = 254F;
            this.lb_nhanvien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_nhanvien.ForeColor = System.Drawing.Color.Blue;
            this.lb_nhanvien.LocationFloat = new DevExpress.Utils.PointFloat(94.66967F, 585.3638F);
            this.lb_nhanvien.Name = "lb_nhanvien";
            this.lb_nhanvien.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_nhanvien.SizeF = new System.Drawing.SizeF(254.5041F, 45.1908F);
            this.lb_nhanvien.StylePriority.UseFont = false;
            this.lb_nhanvien.StylePriority.UseForeColor = false;
            this.lb_nhanvien.Text = "xrLabel6";
            // 
            // lb_ngay
            // 
            this.lb_ngay.Dpi = 254F;
            this.lb_ngay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ngay.ForeColor = System.Drawing.Color.Blue;
            this.lb_ngay.LocationFloat = new DevExpress.Utils.PointFloat(445.1736F, 540.1733F);
            this.lb_ngay.Name = "lb_ngay";
            this.lb_ngay.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_ngay.SizeF = new System.Drawing.SizeF(289.4411F, 45.1907F);
            this.lb_ngay.StylePriority.UseFont = false;
            this.lb_ngay.StylePriority.UseForeColor = false;
            this.lb_ngay.StylePriority.UseTextAlignment = false;
            this.lb_ngay.Text = "23-05-2015 10:13:13";
            this.lb_ngay.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLine2
            // 
            this.xrLine2.Dpi = 254F;
            this.xrLine2.ForeColor = System.Drawing.Color.Blue;
            this.xrLine2.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine2.LineWidth = 3;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(31.66958F, 704.8497F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(709.6149F, 26.66998F);
            this.xrLine2.StylePriority.UseForeColor = false;
            // 
            // xrTable1
            // 
            this.xrTable1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable1.Dpi = 254F;
            this.xrTable1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.ForeColor = System.Drawing.Color.Blue;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(15.79488F, 657.2247F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(718.82F, 47.625F);
            this.xrTable1.StylePriority.UseBorderDashStyle = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseForeColor = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell5,
            this.xrTableCell4,
            this.xrTableCell3});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Hàng Hóa";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.61449179637420992D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "SL";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell2.Weight = 0.2086851352190649D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "Giá Bán";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell5.Weight = 0.39079768555784794D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "CK";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell4.Weight = 0.21813750052594305D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "Thành Tiền";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 0.4729007105240709D;
            // 
            // lb_mahoadon
            // 
            this.lb_mahoadon.Dpi = 254F;
            this.lb_mahoadon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_mahoadon.ForeColor = System.Drawing.Color.Blue;
            this.lb_mahoadon.LocationFloat = new DevExpress.Utils.PointFloat(459.1738F, 585.3638F);
            this.lb_mahoadon.Name = "lb_mahoadon";
            this.lb_mahoadon.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_mahoadon.SizeF = new System.Drawing.SizeF(275.4408F, 45.19089F);
            this.lb_mahoadon.StylePriority.UseFont = false;
            this.lb_mahoadon.StylePriority.UseForeColor = false;
            this.lb_mahoadon.StylePriority.UseTextAlignment = false;
            this.lb_mahoadon.Text = "lb_mahoadon";
            this.lb_mahoadon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lb_ban
            // 
            this.lb_ban.Dpi = 254F;
            this.lb_ban.Font = new System.Drawing.Font("Times New Roman", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ban.ForeColor = System.Drawing.Color.Blue;
            this.lb_ban.LocationFloat = new DevExpress.Utils.PointFloat(179.9164F, 468.3126F);
            this.lb_ban.Name = "lb_ban";
            this.lb_ban.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_ban.SizeF = new System.Drawing.SizeF(420.305F, 42.54498F);
            this.lb_ban.StylePriority.UseFont = false;
            this.lb_ban.StylePriority.UseForeColor = false;
            this.lb_ban.StylePriority.UseTextAlignment = false;
            this.lb_ban.Text = "(Hóa đơn bán lẻ có giá trị xuất hóa đơn GTGT trong ngày)";
            this.lb_ban.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lb_ban.Visible = false;
            // 
            // lb_NameBill
            // 
            this.lb_NameBill.Dpi = 254F;
            this.lb_NameBill.Font = new System.Drawing.Font("Tahoma", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_NameBill.ForeColor = System.Drawing.Color.Blue;
            this.lb_NameBill.LocationFloat = new DevExpress.Utils.PointFloat(31.66966F, 409.3635F);
            this.lb_NameBill.Multiline = true;
            this.lb_NameBill.Name = "lb_NameBill";
            this.lb_NameBill.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_NameBill.SizeF = new System.Drawing.SizeF(702.9453F, 58.94905F);
            this.lb_NameBill.StylePriority.UseFont = false;
            this.lb_NameBill.StylePriority.UseForeColor = false;
            this.lb_NameBill.StylePriority.UseTextAlignment = false;
            this.lb_NameBill.Text = "Hóa Đơn Bán Lẻ\r\n";
            this.lb_NameBill.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(349.1738F, 585.3638F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(110F, 45.19089F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseForeColor = false;
            this.xrLabel3.Text = "Số HD:";
            // 
            // xrLabel5
            // 
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(31.66938F, 585.3638F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(63F, 45.19089F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseForeColor = false;
            this.xrLabel5.Text = "NV:";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(349.1738F, 540.1733F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(96F, 45.19073F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseForeColor = false;
            this.xrLabel4.Text = "Ngày:";
            // 
            // xrLine1
            // 
            this.xrLine1.BorderWidth = 1;
            this.xrLine1.Dpi = 254F;
            this.xrLine1.ForeColor = System.Drawing.Color.Blue;
            this.xrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine1.LineWidth = 3;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(31.66958F, 630.5549F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(709.6149F, 26.67001F);
            this.xrLine1.StylePriority.UseBorderWidth = false;
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // lbTenKH
            // 
            this.lbTenKH.Dpi = 254F;
            this.lbTenKH.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenKH.ForeColor = System.Drawing.Color.Blue;
            this.lbTenKH.LocationFloat = new DevExpress.Utils.PointFloat(217.8308F, 303.7419F);
            this.lbTenKH.Multiline = true;
            this.lbTenKH.Name = "lbTenKH";
            this.lbTenKH.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbTenKH.SizeF = new System.Drawing.SizeF(518.6847F, 45.71979F);
            this.lbTenKH.StylePriority.UseFont = false;
            this.lbTenKH.StylePriority.UseForeColor = false;
            this.lbTenKH.StylePriority.UseTextAlignment = false;
            this.lbTenKH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(31.67F, 395.1815F);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(185.2108F, 45.71982F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Địa chỉ :  ";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lb_diachigiaohang
            // 
            this.lb_diachigiaohang.Dpi = 254F;
            this.lb_diachigiaohang.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_diachigiaohang.ForeColor = System.Drawing.Color.Blue;
            this.lb_diachigiaohang.LocationFloat = new DevExpress.Utils.PointFloat(217.8308F, 395.1815F);
            this.lb_diachigiaohang.Multiline = true;
            this.lb_diachigiaohang.Name = "lb_diachigiaohang";
            this.lb_diachigiaohang.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_diachigiaohang.SizeF = new System.Drawing.SizeF(518.6847F, 45.71982F);
            this.lb_diachigiaohang.StylePriority.UseFont = false;
            this.lb_diachigiaohang.StylePriority.UseForeColor = false;
            this.lb_diachigiaohang.StylePriority.UseTextAlignment = false;
            this.lb_diachigiaohang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine5
            // 
            this.xrLine5.Dpi = 254F;
            this.xrLine5.ForeColor = System.Drawing.Color.Blue;
            this.xrLine5.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine5.LineWidth = 3;
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(31.66992F, 290.301F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(709.6149F, 13.44086F);
            this.xrLine5.StylePriority.UseForeColor = false;
            // 
            // lbDienthoai
            // 
            this.lbDienthoai.Dpi = 254F;
            this.lbDienthoai.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDienthoai.ForeColor = System.Drawing.Color.Blue;
            this.lbDienthoai.LocationFloat = new DevExpress.Utils.PointFloat(217.8308F, 349.4616F);
            this.lbDienthoai.Multiline = true;
            this.lbDienthoai.Name = "lbDienthoai";
            this.lbDienthoai.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbDienthoai.SizeF = new System.Drawing.SizeF(518.6847F, 45.71982F);
            this.lbDienthoai.StylePriority.UseFont = false;
            this.lbDienthoai.StylePriority.UseForeColor = false;
            this.lbDienthoai.StylePriority.UseTextAlignment = false;
            this.lbDienthoai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(31.67F, 303.7419F);
            this.xrLabel6.Multiline = true;
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(186.1608F, 45.71982F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseForeColor = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Tên KH :";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Dpi = 254F;
            this.xrLabel12.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(31.67F, 349.4616F);
            this.xrLabel12.Multiline = true;
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(185.2108F, 45.71982F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseForeColor = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "Điện thoại :";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine4,
            this.lbTiendatcoc,
            this.xrLabel8,
            this.lbPhihucvu,
            this.xrLabel2,
            this.xrLabel11,
            this.lb_tienkhachtra,
            this.xrLabel7,
            this.lb_conlai,
            this.xrLabel10,
            this.xrLine3,
            this.lb_giamgia,
            this.xrLabel9,
            this.lb_tongcong,
            this.lbFooter,
            this.lb_MaHD,
            this.lbTenKH,
            this.xrLabel6,
            this.lbDienthoai,
            this.xrLine5,
            this.lb_diachigiaohang,
            this.xrLabel1,
            this.xrLabel12});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 610.9517F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLine4
            // 
            this.xrLine4.Dpi = 254F;
            this.xrLine4.ForeColor = System.Drawing.Color.Blue;
            this.xrLine4.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine4.LineWidth = 3;
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(32.62038F, 456.7764F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(709.6149F, 13.44086F);
            this.xrLine4.StylePriority.UseForeColor = false;
            // 
            // lbTiendatcoc
            // 
            this.lbTiendatcoc.Dpi = 254F;
            this.lbTiendatcoc.ForeColor = System.Drawing.Color.Blue;
            this.lbTiendatcoc.LocationFloat = new DevExpress.Utils.PointFloat(397.8618F, 124.46F);
            this.lbTiendatcoc.Name = "lbTiendatcoc";
            this.lbTiendatcoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbTiendatcoc.SizeF = new System.Drawing.SizeF(336.7532F, 43.18002F);
            this.lbTiendatcoc.StylePriority.UseForeColor = false;
            this.lbTiendatcoc.StylePriority.UseTextAlignment = false;
            this.lbTiendatcoc.Text = "0";
            this.lbTiendatcoc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lbTiendatcoc.Visible = false;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(31.66972F, 124.46F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(204.2414F, 43.18F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseForeColor = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "Tiền đặt cọc:";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel8.Visible = false;
            // 
            // lbPhihucvu
            // 
            this.lbPhihucvu.Dpi = 254F;
            this.lbPhihucvu.ForeColor = System.Drawing.Color.Blue;
            this.lbPhihucvu.LocationFloat = new DevExpress.Utils.PointFloat(397.8618F, 71.12003F);
            this.lbPhihucvu.Name = "lbPhihucvu";
            this.lbPhihucvu.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbPhihucvu.SizeF = new System.Drawing.SizeF(336.7532F, 43.17999F);
            this.lbPhihucvu.StylePriority.UseForeColor = false;
            this.lbPhihucvu.StylePriority.UseTextAlignment = false;
            this.lbPhihucvu.Text = "0";
            this.lbPhihucvu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(31.66972F, 71.12F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(205.2574F, 43.18F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseForeColor = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Phụ thu:";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Dpi = 254F;
            this.xrLabel11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(31.66972F, 228.6001F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(213.4657F, 43.17999F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseForeColor = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "Tiền thối lại:";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lb_tienkhachtra
            // 
            this.lb_tienkhachtra.Dpi = 254F;
            this.lb_tienkhachtra.ForeColor = System.Drawing.Color.Blue;
            this.lb_tienkhachtra.LocationFloat = new DevExpress.Utils.PointFloat(397.8618F, 175.2601F);
            this.lb_tienkhachtra.Name = "lb_tienkhachtra";
            this.lb_tienkhachtra.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_tienkhachtra.SizeF = new System.Drawing.SizeF(336.7534F, 43.17999F);
            this.lb_tienkhachtra.StylePriority.UseForeColor = false;
            this.lb_tienkhachtra.StylePriority.UseTextAlignment = false;
            this.lb_tienkhachtra.Text = "0";
            this.lb_tienkhachtra.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(31.66972F, 175.2601F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(317.5041F, 43.17999F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseForeColor = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Tổng tiền khách trả:";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lb_conlai
            // 
            this.lb_conlai.Dpi = 254F;
            this.lb_conlai.ForeColor = System.Drawing.Color.Blue;
            this.lb_conlai.LocationFloat = new DevExpress.Utils.PointFloat(397.8615F, 228.6001F);
            this.lb_conlai.Name = "lb_conlai";
            this.lb_conlai.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_conlai.SizeF = new System.Drawing.SizeF(336.7534F, 43.18005F);
            this.lb_conlai.StylePriority.UseForeColor = false;
            this.lb_conlai.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:#.00}";
            this.lb_conlai.Summary = xrSummary1;
            this.lb_conlai.Text = "0";
            this.lb_conlai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Dpi = 254F;
            this.xrLabel10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel10.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(31.66972F, 124.46F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(345.5454F, 43.18001F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseForeColor = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "Tổng tiền thanh toán:";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine3
            // 
            this.xrLine3.Dpi = 254F;
            this.xrLine3.ForeColor = System.Drawing.Color.Blue;
            this.xrLine3.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.xrLine3.LineWidth = 3;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(31.66992F, 0F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(709.6149F, 13.44083F);
            this.xrLine3.StylePriority.UseForeColor = false;
            // 
            // lb_giamgia
            // 
            this.lb_giamgia.Dpi = 254F;
            this.lb_giamgia.ForeColor = System.Drawing.Color.Blue;
            this.lb_giamgia.LocationFloat = new DevExpress.Utils.PointFloat(397.8618F, 20.31998F);
            this.lb_giamgia.Name = "lb_giamgia";
            this.lb_giamgia.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_giamgia.SizeF = new System.Drawing.SizeF(336.7532F, 43.18F);
            this.lb_giamgia.StylePriority.UseForeColor = false;
            this.lb_giamgia.StylePriority.UseTextAlignment = false;
            this.lb_giamgia.Text = "0";
            this.lb_giamgia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.ForeColor = System.Drawing.Color.Blue;
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(31.66972F, 20.32F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(207.645F, 43.18F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseForeColor = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "Giảm giá:";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lb_tongcong
            // 
            this.lb_tongcong.Dpi = 254F;
            this.lb_tongcong.ForeColor = System.Drawing.Color.Blue;
            this.lb_tongcong.LocationFloat = new DevExpress.Utils.PointFloat(397.8615F, 124.46F);
            this.lb_tongcong.Name = "lb_tongcong";
            this.lb_tongcong.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_tongcong.SizeF = new System.Drawing.SizeF(336.7534F, 43.18001F);
            this.lb_tongcong.StylePriority.UseForeColor = false;
            this.lb_tongcong.StylePriority.UseTextAlignment = false;
            xrSummary2.FormatString = "{0:#.00}";
            this.lb_tongcong.Summary = xrSummary2;
            this.lb_tongcong.Text = "0";
            this.lb_tongcong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lbFooter
            // 
            this.lbFooter.Dpi = 254F;
            this.lbFooter.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFooter.ForeColor = System.Drawing.Color.Blue;
            this.lbFooter.LocationFloat = new DevExpress.Utils.PointFloat(31.67F, 483.4464F);
            this.lbFooter.Multiline = true;
            this.lbFooter.Name = "lbFooter";
            this.lbFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lbFooter.SizeF = new System.Drawing.SizeF(710.5653F, 40.42816F);
            this.lbFooter.StylePriority.UseFont = false;
            this.lbFooter.StylePriority.UseForeColor = false;
            this.lbFooter.StylePriority.UseTextAlignment = false;
            this.lbFooter.Text = "Trân trọng cảm ơn quý khách đã đến mua hàng  ";
            this.lbFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lb_MaHD
            // 
            this.lb_MaHD.Dpi = 254F;
            this.lb_MaHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_MaHD.ForeColor = System.Drawing.Color.Black;
            this.lb_MaHD.LocationFloat = new DevExpress.Utils.PointFloat(37.38996F, 577.8254F);
            this.lb_MaHD.Name = "lb_MaHD";
            this.lb_MaHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lb_MaHD.SizeF = new System.Drawing.SizeF(702.9455F, 11.95972F);
            this.lb_MaHD.StylePriority.UseFont = false;
            this.lb_MaHD.StylePriority.UseForeColor = false;
            this.lb_MaHD.StylePriority.UseTextAlignment = false;
            this.lb_MaHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lb_MaHD.Visible = false;
            // 
            // logo
            // 
            this.logo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.logo.Dpi = 254F;
            this.logo.Image = ((System.Drawing.Image)(resources.GetObject("logo.Image")));
            this.logo.LocationFloat = new DevExpress.Utils.PointFloat(254F, 0F);
            this.logo.Name = "logo";
            this.logo.SizeF = new System.Drawing.SizeF(323.4787F, 296.4391F);
            this.logo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            this.logo.StylePriority.UseBorders = false;
            // 
            // rpt_inhoadon_Bill_giaohang
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.ReportFooter});
            this.Dpi = 254F;
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.Margins = new System.Drawing.Printing.Margins(20, 23, 20, 145);
            this.PageHeight = 1600;
            this.PageWidth = 851;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.ShowPrintMarginsWarning = false;
            this.SnapGridSize = 31.75F;
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this.tblMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel lb_nhanvien;
        private DevExpress.XtraReports.UI.XRLabel lb_ngay;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRLabel lb_mahoadon;
        private DevExpress.XtraReports.UI.XRLabel lb_ban;
        private DevExpress.XtraReports.UI.XRLabel lb_NameBill;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRTable tblMain;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell cl_HangHoa;
        private DevExpress.XtraReports.UI.XRTableCell cl_soluong;
        private DevExpress.XtraReports.UI.XRTableCell cl_giaban;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel lb_giamgia;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel lb_tongcong;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel lb_tienkhachtra;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel lb_conlai;
        private DevExpress.XtraReports.UI.XRTableCell cl_thanhtien;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRLabel lb_Quay;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel lb_Header;
        private DevExpress.XtraReports.UI.XRLabel lbFooter;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLabel lb_MaHD;
        private DevExpress.XtraReports.UI.XRLabel lbTiendatcoc;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel lbPhihucvu;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLabel lb_diachigiaohang;
        private DevExpress.XtraReports.UI.XRLabel lbDienthoai;
        private DevExpress.XtraReports.UI.XRLabel lbTenKH;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRTableCell CHIETKHAU;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRPictureBox logo;
    }
}
