﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using KP_UserManagement;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.NativeBricks;
using DevExpress.XtraPrinting.Control;
using System.IO;
using System.Xml;

namespace KP_Report
{
    /// <summary>
    /// Form báo cáo devexpress
    /// </summary>
    public partial class Frm_Report1 : DevExpress.XtraEditors.XtraForm  //DevExpress.XtraBars.Ribbon.RibbonForm
    {
        /// <summary>
        /// Tên report
        /// </summary>
        public string ReportName { get; set; }
        /// <summary>
        /// Dữ liệu cần hiển thị
        /// </summary>
        public DataTable DataSource { get; set; }
        /// <summary>
        /// Câu SQL để lấy dữ liệu cho báo cáo
        /// </summary>
        public string SQL { get; set; }
        /// <summary>
        /// Trạng thái True-In luôn ,False-Show lên cho xem trước muốn in thì nhấn nút In 
        /// </summary>
        public bool IsPrint { get; set; }
        /// <summary>
        /// Tên máy in
        /// </summary>
        public string PrinterName { get; set; }
        /// <summary>
        /// Tên file export
        /// </summary>
        public string ExportName { get; set; }
        /// <summary>
        /// Trạng thái True-export file ,False-ko export 
        /// </summary>
        public bool IsSave { get; set; }
        /// <summary>
        /// Trạng thái xuất file 0=Excel,1=png,else= pdf.
        /// </summary>
        public int Fl { get; set; }
        /// <summary>
        /// Tên file xuất ra.
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// Form báo cáo devexpress
        /// </summary>
        public Frm_Report1()
        {
            DevExpress.Skins.SkinManager.EnableFormSkins();
            InitializeComponent();
        }
        /// <summary>
        ///Lấy dữ liệu cho report
        /// </summary>
        private DataTable GetData()
        {
            if (DataSource != null)
            {
                return DataSource;
            }
            if (SQL != "")
            {
                DataTable dt = clsMain.ReturnDataTable(SQL);
                return dt;
            }
            return null;
        }
        /// <summary>
        /// Số lần copy
        /// </summary>
        public string Copy { get; set; }
        /// <summary>
        /// Load ngon ngu
        /// </summary>
        private string DocfileNgonNguXML()
        {
            string kq = "";
            try
            {
                FileInfo file = new FileInfo(Application.StartupPath + "\\ngonngu.xml");
                if (file.Exists)
                {
                    XmlDocument docXML = new XmlDocument();
                    docXML.Load(Application.StartupPath + "\\ngonngu.xml");
                    string xpath = "//MA_NN";
                    XmlNode ngonngu = docXML.SelectSingleNode(xpath);
                    kq = ngonngu.InnerText;
                }
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message);
            }
            return kq;
        }

        private void CreateReport(DevExpress.XtraReports.UI.XtraReport rpt)
        {
            if (ExportName != null)
            {
                rpt.ExportOptions.PrintPreview.DefaultFileName = ExportName;
            }
            if (PrinterName != null)
            {
                rpt.PrinterName = PrinterName;
            }
            if (IsPrint)
            {
                try
                {
                    int icopy = 1;
                    if (Copy != null)
                    {
                        try
                        {
                          icopy = int.Parse(Copy);
                          if (icopy < 1)
                          {
                              icopy = 1;
                          }
                        }
                        catch
                        {
                        }
                    }
                    for (int i = 0; i < icopy; i++)
                    {
                        rpt.Print();
                    }
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                this.Close();
            }
            else if (IsSave)
            {
                if (Fl==0)
                    try
                    {
                        rpt.ExportToXlsx(Application.StartupPath + "\\ReportMail\\" + FileName + ".xlsx");
                    }
                    catch
                    {
                    }
                else if (Fl == 1)
                    try
                    {
                        rpt.ExportToImage(Application.StartupPath + "\\ReportMail\\" + FileName + ".png");
                    }
                    catch
                    {
                    }
                else
                    try
                    {
                        rpt.ExportToPdf(Application.StartupPath + "\\ReportMail\\" + FileName + ".pdf");
                    }
                    catch
                    {
                    }
                this.Close();
            }
            else
            {
                printControl1.PrintingSystem = rpt.PrintingSystem;
                rpt.CreateDocument();
            }
        }

        private void CreateReport_AutoHeight(DevExpress.XtraReports.UI.XtraReport rpt)
        {
            if (ExportName != null)
            {
                rpt.ExportOptions.PrintPreview.DefaultFileName = ExportName;
            }
            if (PrinterName != null)
            {
                rpt.PrinterName = PrinterName;
            }
            if (IsPrint)
            {
                rpt.ShowPrintStatusDialog = false; 
                try
                {
                    printControl1.PrintingSystem = rpt.PrintingSystem;
                    rpt.CreateDocument();
                    float sumHeight = 0;
                    foreach (PSPage page in rpt.Pages)
                    {
                        foreach (Brick brick in page.Bricks)
                        {
                            if (brick is TableBrick)
                            {
                                TableBrick tableBrick = brick as TableBrick;

                                if (tableBrick.BrickOwner.Name == "tblMain")
                                    sumHeight += tableBrick.Size.Height;
                            }
                        }
                    }
                    switch (rpt.ReportUnit)
                    {
                        case DevExpress.XtraReports.UI.ReportUnit.HundredthsOfAnInch:
                            sumHeight = GraphicsUnitConverter.Convert(sumHeight, GraphicsUnit.Document, GraphicsUnit.Inch) * 100;
                            break;
                        case DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter:
                            sumHeight = GraphicsUnitConverter.Convert(sumHeight, GraphicsUnit.Document, GraphicsUnit.Millimeter) * 10;
                            break;
                    }
                    float totalHeight = sumHeight;
                    rpt.PageHeight = rpt.PageHeight + Convert.ToInt32(totalHeight);
                    rpt.CreateDocument();
                    int icopy = 1;
                    if (Copy != null)
                    {
                        try
                        {
                            icopy = int.Parse(Copy);
                            if (icopy < 1)
                            {
                                icopy = 1;
                            }
                        }
                        catch
                        {
                        }
                    }
                    for (int i = 0; i < icopy; i++)
                    {
                        rpt.Print();
                    }
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                this.Close();
            }
            else
            {
                printControl1.PrintingSystem = rpt.PrintingSystem;
                rpt.CreateDocument();
                float sumHeight = 0;
                foreach (PSPage page in rpt.Pages)
                {
                    foreach (Brick brick in page.Bricks)
                    {
                        if (brick is TableBrick)
                        {
                            TableBrick tableBrick = brick as TableBrick;

                            if (tableBrick.BrickOwner.Name == "tblMain")
                                sumHeight += tableBrick.Size.Height * 2;
                        }
                    }
                }
                switch (rpt.ReportUnit)
                {
                    case DevExpress.XtraReports.UI.ReportUnit.HundredthsOfAnInch:
                        sumHeight = GraphicsUnitConverter.Convert(sumHeight, GraphicsUnit.Document, GraphicsUnit.Inch) * 100;
                        break;
                    case DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter:
                        sumHeight = GraphicsUnitConverter.Convert(sumHeight, GraphicsUnit.Document, GraphicsUnit.Millimeter) * 10;
                        break;
                }
                float totalHeight = sumHeight;
                rpt.PageHeight = rpt.PageHeight + Convert.ToInt32(totalHeight);
                rpt.CreateDocument();
            }
        }

        private void CreateReport_AutoHeight_stranport(DevExpress.XtraReports.UI.XtraReport rpt)
        {
            if (ExportName != null)
            {
                rpt.ExportOptions.PrintPreview.DefaultFileName = ExportName;
            }
            if (PrinterName != null)
            {
                rpt.PrinterName = PrinterName;
            }
            if (IsPrint)
            {
                rpt.ShowPrintStatusDialog = false;
                try
                {
                    printControl1.PrintingSystem = rpt.PrintingSystem;
                    rpt.CreateDocument();
                    float sumHeight = 0;
                    foreach (PSPage page in rpt.Pages)
                    {
                        foreach (Brick brick in page.Bricks)
                        {
                            if (brick is TableBrick)
                            {
                                TableBrick tableBrick = brick as TableBrick;

                                if (tableBrick.BrickOwner.Name == "tblMain")
                                    sumHeight += tableBrick.Size.Height;
                            }
                        }
                    }
                    switch (rpt.ReportUnit)
                    {
                        case DevExpress.XtraReports.UI.ReportUnit.HundredthsOfAnInch:
                            sumHeight = GraphicsUnitConverter.Convert(sumHeight, GraphicsUnit.Document, GraphicsUnit.Inch) * 100;
                            break;
                        case DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter:
                            sumHeight = GraphicsUnitConverter.Convert(sumHeight, GraphicsUnit.Document, GraphicsUnit.Millimeter) * 10;
                            break;
                    }
                    float totalHeight = sumHeight + 70;
                    rpt.PageHeight = rpt.PageHeight + Convert.ToInt32(totalHeight);
                    rpt.CreateDocument();
                    int icopy = 1;
                    if (Copy != null)
                    {
                        try
                        {
                            icopy = int.Parse(Copy);
                            if (icopy < 1)
                            {
                                icopy = 1;
                            }
                        }
                        catch
                        {
                        }
                    }
                    for (int i = 0; i < icopy; i++)
                    {
                        rpt.Print();
                    }
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                this.Close();
            }
            else
            {
                printControl1.PrintingSystem = rpt.PrintingSystem;
                rpt.CreateDocument();
                float sumHeight = 0;
                foreach (PSPage page in rpt.Pages)
                {
                    foreach (Brick brick in page.Bricks)
                    {
                        if (brick is TableBrick)
                        {
                            TableBrick tableBrick = brick as TableBrick;

                            if (tableBrick.BrickOwner.Name == "tblMain")
                                sumHeight += tableBrick.Size.Height * 2;
                        }
                    }
                }
                switch (rpt.ReportUnit)
                {
                    case DevExpress.XtraReports.UI.ReportUnit.HundredthsOfAnInch:
                        sumHeight = GraphicsUnitConverter.Convert(sumHeight, GraphicsUnit.Document, GraphicsUnit.Inch) * 100;
                        break;
                    case DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter:
                        sumHeight = GraphicsUnitConverter.Convert(sumHeight, GraphicsUnit.Document, GraphicsUnit.Millimeter) * 10;
                        break;
                }
                float totalHeight = sumHeight + 70;
                rpt.PageHeight = rpt.PageHeight + Convert.ToInt32(totalHeight);
                rpt.CreateDocument();
            }
        }

        private void Frm_Report1_Load(object sender, EventArgs e)
        {
            
            if (ReportName == "rptPhieuNhap")
            {
                rptPhieuNhap rpt = new rptPhieuNhap();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptPhieuXuat")
            {
                rptPhieuXuat rpt = new rptPhieuXuat();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptTonKho")
            {
                rptTonKho rpt = new rptTonKho();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptHangHoa")
            {
                rptHangHoa rpt = new rptHangHoa();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptMaVach")
            {
                rptMaVach rpt = new rptMaVach();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptHoaDonDo")
            {
                rptHoaDonDo rpt = new rptHoaDonDo();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoKQKD")
            {
                rptBaoCaoKQKD rpt = new rptBaoCaoKQKD();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
              
            }
            else if (ReportName == "rptBaoCaoBanHang")
            {
                rptBaoCaoBanHang rpt = new rptBaoCaoBanHang();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptDS_Hoadon")
            {
                rptDS_Hoadon rpt = new rptDS_Hoadon();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptDS_PhieuNhap")
            {
                rptDS_PhieuNhap rpt = new rptDS_PhieuNhap();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptDS_PhieuXuat")
            {
                rptDS_PhieuXuat rpt = new rptDS_PhieuXuat();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoDoanhSo")
            {
                rptBaoCaoDoanhSo rpt = new rptBaoCaoDoanhSo();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadon")
            {
                rpt_inhoadon rpt = new rpt_inhoadon();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptPhieuKiemKe")
            {
                rptPhieuKiemKe rpt = new rptPhieuKiemKe();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBanggiatheokhu")
            {
                rptBanggiatheokhu rpt = new rptBanggiatheokhu();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptTonKhoToiThieu")
            {
                rptTonKhoToiThieu rpt = new rptTonKhoToiThieu();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptDS_KHTT")
            {
                rptDS_KHTT rpt = new rptDS_KHTT();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptVeKhuvuichoi")
            {
                rptVeKhuvuichoi rpt = new rptVeKhuvuichoi();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptVe")
            {
                rptVe rpt = new rptVe();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptVerapphim")
            {
                rptVerapphim rpt = new rptVerapphim();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBiennhan1")
            {
                rptBiennhan1 rpt = new rptBiennhan1();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport_AutoHeight(rpt);
            }
            else if (ReportName == "rptThongkeveNPH")
            {
                rptThongkeveNPH rpt = new rptThongkeveNPH();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBanggiarapphim")
            {
                rptBanggiarapphim rpt = new rptBanggiarapphim();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptLichchieuphim")
            {
                rptLichchieuphim rpt = new rptLichchieuphim();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptLichchieuphim1")
            {
                rptLichchieuphim1 rpt = new rptLichchieuphim1();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptTonghopve")
            {
                rptTonghopve rpt = new rptTonghopve();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptThongkedatve")
            {
                rptThongkedatve rpt = new rptThongkedatve();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaocaohuyrapphim")
            {
                rptBaocaohuyrapphim rpt = new rptBaocaohuyrapphim();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptDoanhthuve")
            {
                rptDoanhthuve rpt = new rptDoanhthuve();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptDS_KHTTNB")
            {
                rptDS_KHTTNB rpt = new rptDS_KHTTNB();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBanvetrochoi")
            {
                rptBanvetrochoi rpt = new rptBanvetrochoi();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptLapPhieuThu")
            {
                rptLapPhieuThu rpt = new rptLapPhieuThu();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptLapPhieuChi")
            {
                rptLapPhieuChi rpt = new rptLapPhieuChi();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptInMaKH")
            {
                rptInMaKH rpt = new rptInMaKH();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoPhieuThu")
            {
                rptBaoCaoPhieuThu rpt = new rptBaoCaoPhieuThu();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoPhieuChi")
            {
                rptBaoCaoPhieuChi rpt = new rptBaoCaoPhieuChi();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoCongNoNCC")
            {
                rptBaoCaoCongNoNCC rpt = new rptBaoCaoCongNoNCC();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoCongNoKH")
            {
                rptBaoCaoCongNoKH rpt = new rptBaoCaoCongNoKH();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoCongNoTongQuan")
            {
                rptBaoCaoCongNoTongQuan rpt = new rptBaoCaoCongNoTongQuan();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptQuyTienMat")
            {
                rptQuyTienMat rpt = new rptQuyTienMat();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoDoanhThuKaraoke")
            {
                rptBaoCaoDoanhThuKaraoke rpt = new rptBaoCaoDoanhThuKaraoke();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadonKaraoke")
            {
                rpt_inhoadonKaraoke rpt = new rpt_inhoadonKaraoke();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_ketcaKaraoke")
            {
                rpt_ketcaKaraoke rpt = new rpt_ketcaKaraoke();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_ketcaKaraoke_58")
            {
                rpt_ketcaKaraoke_58 rpt = new rpt_ketcaKaraoke_58();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoCombo")
            {
                rptBaoCaoCombo rpt = new rptBaoCaoCombo();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptVerapphim_Huy")
            {
                rptVerapphim_Huy rpt = new rptVerapphim_Huy();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptKetquachieuphim")
            {
                rptKetquachieuphim rpt = new rptKetquachieuphim();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptLichchieuphim2")
            {
                rptLichchieuphim2 rpt = new rptLichchieuphim2();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_OrderKaraoke")
            {
                rpt_OrderKaraoke rpt = new rpt_OrderKaraoke();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
              else if (ReportName == "rpt_BillDatPhongKaraoke")
            {
                rpt_BillDatPhongKaraoke rpt = new rpt_BillDatPhongKaraoke();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptVerapphim_Inlai")
            {
                rptVerapphim_Inlai rpt = new rptVerapphim_Inlai();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_BillDatBan")
            {
                rpt_BillDatBan rpt = new rpt_BillDatBan();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_BillDatBan_58")
            {
                rpt_BillDatBan_58 rpt = new rpt_BillDatBan_58();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill")
            {
                rpt_inhoadon_Bill rpt = new rpt_inhoadon_Bill();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport_AutoHeight(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_58")
            {
                rpt_inhoadon_Bill_58 rpt = new rpt_inhoadon_Bill_58();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport_AutoHeight(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_giaohang")
            {
                rpt_inhoadon_Bill_giaohang rpt = new rpt_inhoadon_Bill_giaohang();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport_AutoHeight_stranport(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_giaohang_58")
            {
                rpt_inhoadon_Bill_giaohang_58 rpt = new rpt_inhoadon_Bill_giaohang_58();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport_AutoHeight_stranport(rpt);
            }
            else if (ReportName == "rpt_ketca")
            {
                rpt_ketca rpt = new rpt_ketca();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_ketca_58")
            {
                rpt_ketca_58 rpt = new rpt_ketca_58();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_Order")
            {
                rpt_Order rpt = new rpt_Order();
                rpt.DataSource = GetData();
                rpt.BindData(DocfileNgonNguXML ());
                CreateReport_AutoHeight(rpt);
            }
            else if (ReportName == "rpt_Order_58")
            {
                rpt_Order_58 rpt = new rpt_Order_58();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport_AutoHeight(rpt);
            }
            else if (ReportName == "rptTonghopve_truoc")
            {
                rptTonghopve_truoc rpt = new rptTonghopve_truoc();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_ban")
            {
                rpt_inhoadon_Bill_ban rpt = new rpt_inhoadon_Bill_ban();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport_AutoHeight(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_ban_58")
            {
                rpt_inhoadon_Bill_ban_58 rpt = new rpt_inhoadon_Bill_ban_58();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport_AutoHeight(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_ban_giaohang")
            {
                rpt_inhoadon_Bill_ban_giaohang rpt = new rpt_inhoadon_Bill_ban_giaohang();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport_AutoHeight(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_ban_giaohang_58")
            {
                rpt_inhoadon_Bill_ban_giaohang_58 rpt = new rpt_inhoadon_Bill_ban_giaohang_58();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport_AutoHeight_stranport(rpt);
            }
            else if (ReportName == "rptDinhluong")
            {
                rptDinhluong rpt = new rptDinhluong();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptCombo")
            {
                rptCombo rpt = new rptCombo();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptUser")
            {
                rptUser rpt = new rptUser();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptDS_HoadonCancel")
            {
                rptDS_HoadonCancel rpt = new rptDS_HoadonCancel();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptDS_HoadonInlai")
            {
                rptDS_HoadonInlai rpt = new rptDS_HoadonInlai();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_PhieuKiemKe")
            {
                rpt_PhieuKiemKe rpt = new rpt_PhieuKiemKe();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_PhieuNhap")
            {
                rpt_PhieuNhap rpt = new rpt_PhieuNhap();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_PhieuXuat")
            {
                rpt_PhieuXuat rpt = new rpt_PhieuXuat();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBanvetrochoi_nhahang")
            {
                rptBanvetrochoi_nhahang rpt = new rptBanvetrochoi_nhahang();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_Order_FF")
            {
                rpt_Order_FF rpt = new rpt_Order_FF();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_Order_FF_58")
            {
                rpt_Order_FF_58 rpt = new rpt_Order_FF_58();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBCHangBanChay")
            {
                rptBCHangBanChay rpt = new rptBCHangBanChay();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBCDoanhthu_TungThang")
            {
                rptBCDoanhthu_TungThang rpt = new rptBCDoanhthu_TungThang();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBCDoanhthu_TungNam")
            {
                rptBCDoanhthu_TungNam rpt = new rptBCDoanhthu_TungNam();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptLSGD_KHTT")
            {
                rptLSGD_KHTT rpt = new rptLSGD_KHTT();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_InMaKhachHang")
            {
                rpt_InMaKhachHang rpt = new rpt_InMaKhachHang();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCao_DS_DoiQua")
            {
                rptBaoCao_DS_DoiQua rpt = new rptBaoCao_DS_DoiQua();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptLSDQ_KHTT")
            {
                rptLSDQ_KHTT rpt = new rptLSDQ_KHTT();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoBanVeOnline")
            {
                rptBaoCaoBanVeOnline rpt = new rptBaoCaoBanVeOnline();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCao_LS_GiaoDich")
            {
                rptBaoCao_LS_GiaoDich rpt = new rptBaoCao_LS_GiaoDich();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_Voucher")
            {
                rpt_Voucher rpt = new rpt_Voucher();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoTheKhuyenMai")
            {
                rptBaoCaoTheKhuyenMai rpt = new rptBaoCaoTheKhuyenMai();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBangKeChiTietBanVe")
            {
                rptBangKeChiTietBanVe rpt = new rptBangKeChiTietBanVe();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoTongHopBanHangKH")
            {
                rptBaoCaoTongHopBanHangKH rpt = new rptBaoCaoTongHopBanHangKH();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoHangHoaOnline")
            {
                rptBaoCaoHangHoaOnline rpt = new rptBaoCaoHangHoaOnline();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCao_CongVanDen")
            {
                rptBaoCao_CongVanDen rpt = new rptBaoCao_CongVanDen();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCao_CongVanDi")
            {
                rptBaoCao_CongVanDi rpt = new rptBaoCao_CongVanDi();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCao_LichLamViec")
            {
                rptBaoCao_LichLamViec rpt = new rptBaoCao_LichLamViec();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoLichSuDoiMa")
            {
                rptBaoCaoLichSuDoiMa rpt = new rptBaoCaoLichSuDoiMa();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoDanhSachVoucher")
            {
                rptBaoCaoDanhSachVoucher rpt = new rptBaoCaoDanhSachVoucher();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoTongHopVoucher")
            {
                rptBaoCaoTongHopVoucher rpt = new rptBaoCaoTongHopVoucher();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoLichSuLichChieu")
            {
                rptBaoCaoLichSuLichChieu rpt = new rptBaoCaoLichSuLichChieu();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptVerapphimIonah")
            {
                rptVerapphimIonah rpt = new rptVerapphimIonah();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoUngVien")
            {
                rptBaoCaoUngVien rpt = new rptBaoCaoUngVien();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoNhanVien")
            {
                rptBaoCaoNhanVien rpt = new rptBaoCaoNhanVien();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoHopDongLaoDong")
            {
                rptBaoCaoHopDongLaoDong rpt = new rptBaoCaoHopDongLaoDong();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoKhenThuongKyLuat")
            {
                rptBaoCaoKhenThuongKyLuat rpt = new rptBaoCaoKhenThuongKyLuat();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoLuongThang")
            {
                rptBaoCaoLuongThang rpt = new rptBaoCaoLuongThang();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_Chuyenban")
            {
                rpt_Chuyenban rpt = new rpt_Chuyenban();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBienBanGiaoNhan")
            {
                rptBienBanGiaoNhan rpt = new rptBienBanGiaoNhan();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBieuDoDanhGiaKH")
            {
                rptBieuDoDanhGiaKH rpt = new rptBieuDoDanhGiaKH();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoDanhGiaKH")
            {
                rptBaoCaoDanhGiaKH rpt = new rptBaoCaoDanhGiaKH();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBCThuChi_TungNam")
            {
                rptBCThuChi_TungNam rpt = new rptBCThuChi_TungNam();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBCThuChi_TungThang")
            {
                rptBCThuChi_TungThang rpt = new rptBCThuChi_TungThang();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptLapPhieuThu_A5")
            {
                rptLapPhieuThu_A5 rpt = new rptLapPhieuThu_A5();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptLapPhieuChi_A5")
            {
                rptLapPhieuChi_A5 rpt = new rptLapPhieuChi_A5();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCao_DS_PhieuDNChi")
            {
                rptBaoCao_DS_PhieuDNChi rpt = new rptBaoCao_DS_PhieuDNChi();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptUyNhiemChi")
            {
                rptUyNhiemChi rpt = new rptUyNhiemChi();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoUyNhiemChi")
            {
                rptBaoCaoUyNhiemChi rpt = new rptBaoCaoUyNhiemChi();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_ban_1")
            {
                rpt_inhoadon_Bill_ban_1 rpt = new rpt_inhoadon_Bill_ban_1();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoSoTheoDoi")
            {
                rptBaoCaoSoTheoDoi rpt = new rptBaoCaoSoTheoDoi();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoChiTheoDoi")
            {
                rptBaoCaoChiTheoDoi rpt = new rptBaoCaoChiTheoDoi();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_Order_1")
            {
                rpt_Order_1 rpt = new rpt_Order_1();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadonKhachSan")
            {
                rpt_inhoadonKhachSan rpt = new rpt_inhoadonKhachSan();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCao_KS_Danhsachkhach")
            {
                rptBaoCao_KS_Danhsachkhach rpt = new rptBaoCao_KS_Danhsachkhach();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCao_KS_Danhsachthuephong")
            {
                rptBaoCao_KS_Danhsachthuephong rpt = new rptBaoCao_KS_Danhsachthuephong();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_ban_pho")
            {
                rpt_inhoadon_Bill_ban_pho rpt = new rpt_inhoadon_Bill_ban_pho();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCao_Soluongkhach")
            {
                rptBaoCao_Soluongkhach rpt = new rptBaoCao_Soluongkhach();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_Order_1_Cat1")
            {
                rpt_Order_1_Cat1 rpt = new rpt_Order_1_Cat1();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_ban_2")
            {
                rpt_inhoadon_Bill_ban_2 rpt = new rpt_inhoadon_Bill_ban_2();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoDanhSachMonHuy")
            {
                rptBaoCaoDanhSachMonHuy rpt = new rptBaoCaoDanhSachMonHuy();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptDS_BanHuy")
            {
                rptDS_BanHuy rpt = new rptDS_BanHuy();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_giaohang_annhien")
            {
                rpt_inhoadon_Bill_giaohang_annhien rpt = new rpt_inhoadon_Bill_giaohang_annhien();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_giaohang_thaomoc")
            {
                rpt_inhoadon_Bill_giaohang_thaomoc rpt = new rpt_inhoadon_Bill_giaohang_thaomoc();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_thaomoc")
            {
                rpt_inhoadon_Bill_thaomoc rpt = new rpt_inhoadon_Bill_thaomoc();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoBanHang1")
            {
                rptBaoCaoBanHang1 rpt = new rptBaoCaoBanHang1();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_ban_image")
            {
                rpt_inhoadon_Bill_ban_image rpt = new rpt_inhoadon_Bill_ban_image();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptKetCaHangHoa")
            {
                rptKetCaHangHoa rpt = new rptKetCaHangHoa();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport_AutoHeight(rpt);
            }
            else if (ReportName == "rptKetCaHangHoa_58")
            {
                rptKetCaHangHoa_58 rpt = new rptKetCaHangHoa_58();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport_AutoHeight(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_MaVach")
            {
                rpt_inhoadon_Bill_MaVach rpt = new rpt_inhoadon_Bill_MaVach();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptDS_HoaDonTraHang")
            {
                rptDS_HoaDonTraHang rpt = new rptDS_HoaDonTraHang();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoTraHang")
            {
                rptBaoCaoTraHang rpt = new rptBaoCaoTraHang();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_BaoCaoHangNhapKho")
            {
                rpt_BaoCaoHangNhapKho rpt = new rpt_BaoCaoHangNhapKho();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_BaoCaoHangXuatKho")
            {
                rpt_BaoCaoHangXuatKho rpt = new rpt_BaoCaoHangXuatKho();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptVerapphim_Bill")
            {
                rptVerapphim_Bill rpt = new rptVerapphim_Bill();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);

                rptVerapphim_Bill1 rpt1 = new rptVerapphim_Bill1();
                rpt1.DataSource = GetData();
                rpt1.BindData();
                CreateReport(rpt1);
            }
            else if (ReportName == "rptVerapphim_Bill_Huy")
            {
                rptVerapphim_Bill rpt = new rptVerapphim_Bill();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);

                rptVerapphim_Bill1 rpt1 = new rptVerapphim_Bill1();
                rpt1.DataSource = GetData();
                rpt1.BindData();
                CreateReport(rpt1);
            }
            else if (ReportName == "rptVerapphim_Bill_Inlai")
            {
                rptVerapphim_Bill rpt = new rptVerapphim_Bill();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);

                rptVerapphim_Bill1 rpt1 = new rptVerapphim_Bill1();
                rpt1.DataSource = GetData();
                rpt1.BindData();
                CreateReport(rpt1);
            }
            else if (ReportName == "rptDS_Hoadon_Khachsan")
            {
                rptDS_Hoadon_Khachsan rpt = new rptDS_Hoadon_Khachsan();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoDanhSachDichVuHuy")
            {
                rptBaoCaoDanhSachDichVuHuy rpt = new rptBaoCaoDanhSachDichVuHuy();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptDS_PhongHuy")
            {
                rptDS_PhongHuy rpt = new rptDS_PhongHuy();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoBanHang2")
            {
                rptBaoCaoBanHang2 rpt = new rptBaoCaoBanHang2();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_image")
            {
                rpt_inhoadon_Bill_image rpt = new rpt_inhoadon_Bill_image();
                rpt.DataSource = GetData();
                rpt.BindData(DocfileNgonNguXML());
                CreateReport(rpt);
            }
            else if (ReportName == "rptPhieuXuat2")
            {
                rptPhieuXuat2 rpt = new rptPhieuXuat2();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_Giaohang_image")
            {
                rpt_inhoadon_Bill_Giaohang_image rpt = new rpt_inhoadon_Bill_Giaohang_image();
                rpt.DataSource = GetData();
                rpt.BindData(DocfileNgonNguXML());
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_MaVach_Fastfood")
            {
                rpt_MaVach_Fastfood rpt = new rpt_MaVach_Fastfood();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_ban_58_Image")
            {
                rpt_inhoadon_Bill_ban_58_Image rpt = new rpt_inhoadon_Bill_ban_58_Image();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_Karaoke")
            {
                rpt_inhoadon_Bill_Karaoke rpt = new rpt_inhoadon_Bill_Karaoke();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptPhieuDeNghiLapPhieuChi")
            {
                rptPhieuDeNghiLapPhieuChi rpt = new rptPhieuDeNghiLapPhieuChi();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptDS_LayLaiHD")
            {
                rptDS_LayLaiHD rpt = new rptDS_LayLaiHD();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptLichchieuphim_Khonggia")
            {
                rptLichchieuphim_Khonggia rpt = new rptLichchieuphim_Khonggia();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptLichchieuphim1_Khonggia")
            {
                rptLichchieuphim1_Khonggia rpt = new rptLichchieuphim1_Khonggia();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_TraSua")
            {
                rpt_inhoadon_Bill_TraSua rpt = new rpt_inhoadon_Bill_TraSua();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_TraSua_58")
            {
                rpt_inhoadon_Bill_TraSua_58 rpt = new rpt_inhoadon_Bill_TraSua_58();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptMaVach_QRCode")
            {
                rptMaVach_QRCode rpt = new rptMaVach_QRCode();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoDoanhSo_Rapphim")
            {
                rptBaoCaoDoanhSo_Rapphim rpt = new rptBaoCaoDoanhSo_Rapphim();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_ban_TraSua")
            {
                rpt_inhoadon_Bill_ban_TraSua rpt = new rpt_inhoadon_Bill_ban_TraSua();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_ban_TraSua_58")
            {
                rpt_inhoadon_Bill_ban_TraSua_58 rpt = new rpt_inhoadon_Bill_ban_TraSua_58();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptBaoCaoBanHang_Bill")
            {
                rptBaoCaoBanHang_Bill rpt = new rptBaoCaoBanHang_Bill();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_MaVach_SieuThi")
            {
                rpt_MaVach_SieuThi rpt = new rpt_MaVach_SieuThi();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rptDS_Hoadon_Khachsan1")
            {
                rptDS_Hoadon_Khachsan1 rpt = new rptDS_Hoadon_Khachsan1();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_inhoadon_Bill_ban_SL")
            {
                rpt_inhoadon_Bill_ban_SL rpt = new rpt_inhoadon_Bill_ban_SL();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport_AutoHeight(rpt);
            }
            else if (ReportName == "rpt_Phieuthutien")
            {
                rpt_Phieuthutien rpt = new rpt_Phieuthutien();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
            else if (ReportName == "rpt_BCDATCOC")
            {
                rpt_BCDATCOC rpt = new rpt_BCDATCOC();
                rpt.DataSource = GetData();
                rpt.BindData();
                CreateReport(rpt);
            }
                
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Không tồn tại report : " + ReportName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }
    }
}