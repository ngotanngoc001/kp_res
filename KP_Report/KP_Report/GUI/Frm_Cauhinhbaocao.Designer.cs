﻿namespace KP_Report
{
    /// <summary>
    /// Form cấu hình báo cáo
    /// </summary>
    partial class Frm_Cauhinhbaocao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Cauhinhbaocao));
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTencongty = new DevExpress.XtraEditors.TextEdit();
            this.txtDiachi = new DevExpress.XtraEditors.TextEdit();
            this.txtDienthoai = new DevExpress.XtraEditors.TextEdit();
            this.txtFax = new DevExpress.XtraEditors.TextEdit();
            this.txtEmail = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSoTK = new DevExpress.XtraEditors.TextEdit();
            this.txtMST = new DevExpress.XtraEditors.TextEdit();
            this.txtMauHD = new DevExpress.XtraEditors.TextEdit();
            this.txtKyhieu = new DevExpress.XtraEditors.TextEdit();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.btnBanphim = new DevExpress.XtraEditors.SimpleButton();
            this.txtNGUOIGIAO = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.txtNguoinhan = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.txtKetoan = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.txtThukho = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTruongphong = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.txtGiamdoc = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTencongty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiachi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienthoai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTK.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMST.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMauHD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyhieu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNGUOIGIAO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNguoinhan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKetoan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThukho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTruongphong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiamdoc.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            // 
            // 
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.ExpandCollapseItem.Name = "";
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 1;
            this.ribbon.Name = "ribbon";
            this.ribbon.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(603, 27);
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::KP_Report.Properties.Resources.KingPos_for_Fashion_300x15;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(603, 35);
            this.panel1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label3.Location = new System.Drawing.Point(9, 218);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 19);
            this.label3.TabIndex = 9;
            this.label3.Text = "Email ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label1.Location = new System.Drawing.Point(9, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tên Công Ty";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label2.Location = new System.Drawing.Point(9, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 19);
            this.label2.TabIndex = 3;
            this.label2.Text = "Địa Chỉ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label4.Location = new System.Drawing.Point(9, 154);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 19);
            this.label4.TabIndex = 5;
            this.label4.Text = "Điện Thoại";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label5.Location = new System.Drawing.Point(9, 186);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 19);
            this.label5.TabIndex = 7;
            this.label5.Text = "Fax";
            // 
            // txtTencongty
            // 
            this.txtTencongty.EnterMoveNextControl = true;
            this.txtTencongty.Location = new System.Drawing.Point(122, 87);
            this.txtTencongty.Name = "txtTencongty";
            this.txtTencongty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtTencongty.Properties.Appearance.Options.UseFont = true;
            this.txtTencongty.Properties.MaxLength = 100;
            this.txtTencongty.Size = new System.Drawing.Size(462, 26);
            this.txtTencongty.TabIndex = 2;
            // 
            // txtDiachi
            // 
            this.txtDiachi.EnterMoveNextControl = true;
            this.txtDiachi.Location = new System.Drawing.Point(122, 119);
            this.txtDiachi.Name = "txtDiachi";
            this.txtDiachi.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtDiachi.Properties.Appearance.Options.UseFont = true;
            this.txtDiachi.Properties.MaxLength = 100;
            this.txtDiachi.Size = new System.Drawing.Size(462, 26);
            this.txtDiachi.TabIndex = 4;
            // 
            // txtDienthoai
            // 
            this.txtDienthoai.EnterMoveNextControl = true;
            this.txtDienthoai.Location = new System.Drawing.Point(122, 151);
            this.txtDienthoai.Name = "txtDienthoai";
            this.txtDienthoai.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtDienthoai.Properties.Appearance.Options.UseFont = true;
            this.txtDienthoai.Properties.MaxLength = 100;
            this.txtDienthoai.Size = new System.Drawing.Size(179, 26);
            this.txtDienthoai.TabIndex = 6;
            // 
            // txtFax
            // 
            this.txtFax.EnterMoveNextControl = true;
            this.txtFax.Location = new System.Drawing.Point(122, 183);
            this.txtFax.Name = "txtFax";
            this.txtFax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtFax.Properties.Appearance.Options.UseFont = true;
            this.txtFax.Properties.MaxLength = 100;
            this.txtFax.Size = new System.Drawing.Size(179, 26);
            this.txtFax.TabIndex = 8;
            // 
            // txtEmail
            // 
            this.txtEmail.EnterMoveNextControl = true;
            this.txtEmail.Location = new System.Drawing.Point(122, 215);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtEmail.Properties.Appearance.Options.UseFont = true;
            this.txtEmail.Properties.MaxLength = 100;
            this.txtEmail.Size = new System.Drawing.Size(179, 26);
            this.txtEmail.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label9.Location = new System.Drawing.Point(307, 314);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 19);
            this.label9.TabIndex = 17;
            this.label9.Text = "Ký Hiệu ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label6.Location = new System.Drawing.Point(9, 314);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 19);
            this.label6.TabIndex = 15;
            this.label6.Text = "Mẫu Số HĐ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label7.Location = new System.Drawing.Point(9, 282);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 19);
            this.label7.TabIndex = 13;
            this.label7.Text = "Mã Số Thuế";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label8.Location = new System.Drawing.Point(9, 250);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 19);
            this.label8.TabIndex = 11;
            this.label8.Text = "Số Tài Khoản";
            // 
            // txtSoTK
            // 
            this.txtSoTK.EnterMoveNextControl = true;
            this.txtSoTK.Location = new System.Drawing.Point(122, 247);
            this.txtSoTK.Name = "txtSoTK";
            this.txtSoTK.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtSoTK.Properties.Appearance.Options.UseFont = true;
            this.txtSoTK.Properties.MaxLength = 100;
            this.txtSoTK.Size = new System.Drawing.Size(179, 26);
            this.txtSoTK.TabIndex = 12;
            // 
            // txtMST
            // 
            this.txtMST.EnterMoveNextControl = true;
            this.txtMST.Location = new System.Drawing.Point(122, 279);
            this.txtMST.Name = "txtMST";
            this.txtMST.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtMST.Properties.Appearance.Options.UseFont = true;
            this.txtMST.Properties.MaxLength = 100;
            this.txtMST.Size = new System.Drawing.Size(179, 26);
            this.txtMST.TabIndex = 14;
            // 
            // txtMauHD
            // 
            this.txtMauHD.EnterMoveNextControl = true;
            this.txtMauHD.Location = new System.Drawing.Point(122, 311);
            this.txtMauHD.Name = "txtMauHD";
            this.txtMauHD.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtMauHD.Properties.Appearance.Options.UseFont = true;
            this.txtMauHD.Properties.MaxLength = 100;
            this.txtMauHD.Size = new System.Drawing.Size(179, 26);
            this.txtMauHD.TabIndex = 16;
            // 
            // txtKyhieu
            // 
            this.txtKyhieu.EnterMoveNextControl = true;
            this.txtKyhieu.Location = new System.Drawing.Point(405, 311);
            this.txtKyhieu.Name = "txtKyhieu";
            this.txtKyhieu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtKyhieu.Properties.Appearance.Options.UseFont = true;
            this.txtKyhieu.Properties.MaxLength = 100;
            this.txtKyhieu.Size = new System.Drawing.Size(179, 26);
            this.txtKyhieu.TabIndex = 18;
            // 
            // picLogo
            // 
            this.picLogo.BackgroundImage = global::KP_Report.Properties.Resources.compact_camera_64;
            this.picLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picLogo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picLogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picLogo.Location = new System.Drawing.Point(2, 2);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(269, 150);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picLogo.TabIndex = 23;
            this.picLogo.TabStop = false;
            this.picLogo.Click += new System.EventHandler(this.picLogo_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.picLogo);
            this.panelControl1.Location = new System.Drawing.Point(311, 151);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(273, 154);
            this.panelControl1.TabIndex = 34;
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Image = global::KP_Report.Properties.Resources.cancel_26;
            this.btnCancel.Location = new System.Drawing.Point(316, 450);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(131, 31);
            this.btnCancel.TabIndex = 32;
            this.btnCancel.Text = "&2.Đóng";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btnOk.Appearance.Options.UseFont = true;
            this.btnOk.Image = global::KP_Report.Properties.Resources.ok_26;
            this.btnOk.Location = new System.Drawing.Point(179, 450);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(131, 31);
            this.btnOk.TabIndex = 31;
            this.btnOk.Text = "&1.Đồng ý";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnBanphim
            // 
            this.btnBanphim.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btnBanphim.Appearance.Options.UseFont = true;
            this.btnBanphim.Image = global::KP_Report.Properties.Resources.Computer_Hardware_Keyboard_icon;
            this.btnBanphim.Location = new System.Drawing.Point(453, 450);
            this.btnBanphim.Name = "btnBanphim";
            this.btnBanphim.Size = new System.Drawing.Size(131, 31);
            this.btnBanphim.TabIndex = 33;
            this.btnBanphim.Text = "&3.Bàn phím";
            this.btnBanphim.Click += new System.EventHandler(this.btnBanphim_Click);
            // 
            // txtNGUOIGIAO
            // 
            this.txtNGUOIGIAO.EnterMoveNextControl = true;
            this.txtNGUOIGIAO.Location = new System.Drawing.Point(122, 343);
            this.txtNGUOIGIAO.Name = "txtNGUOIGIAO";
            this.txtNGUOIGIAO.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtNGUOIGIAO.Properties.Appearance.Options.UseFont = true;
            this.txtNGUOIGIAO.Properties.MaxLength = 100;
            this.txtNGUOIGIAO.Size = new System.Drawing.Size(179, 26);
            this.txtNGUOIGIAO.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label10.Location = new System.Drawing.Point(9, 346);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 19);
            this.label10.TabIndex = 19;
            this.label10.Text = "Người giao";
            // 
            // txtNguoinhan
            // 
            this.txtNguoinhan.EnterMoveNextControl = true;
            this.txtNguoinhan.Location = new System.Drawing.Point(405, 343);
            this.txtNguoinhan.Name = "txtNguoinhan";
            this.txtNguoinhan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtNguoinhan.Properties.Appearance.Options.UseFont = true;
            this.txtNguoinhan.Properties.MaxLength = 100;
            this.txtNguoinhan.Size = new System.Drawing.Size(179, 26);
            this.txtNguoinhan.TabIndex = 22;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label11.Location = new System.Drawing.Point(309, 346);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 19);
            this.label11.TabIndex = 21;
            this.label11.Text = "Người nhận";
            // 
            // txtKetoan
            // 
            this.txtKetoan.EnterMoveNextControl = true;
            this.txtKetoan.Location = new System.Drawing.Point(122, 375);
            this.txtKetoan.Name = "txtKetoan";
            this.txtKetoan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtKetoan.Properties.Appearance.Options.UseFont = true;
            this.txtKetoan.Properties.MaxLength = 100;
            this.txtKetoan.Size = new System.Drawing.Size(179, 26);
            this.txtKetoan.TabIndex = 24;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label12.Location = new System.Drawing.Point(9, 378);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 19);
            this.label12.TabIndex = 23;
            this.label12.Text = "Kế toán";
            // 
            // txtThukho
            // 
            this.txtThukho.EnterMoveNextControl = true;
            this.txtThukho.Location = new System.Drawing.Point(405, 375);
            this.txtThukho.Name = "txtThukho";
            this.txtThukho.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtThukho.Properties.Appearance.Options.UseFont = true;
            this.txtThukho.Properties.MaxLength = 100;
            this.txtThukho.Size = new System.Drawing.Size(179, 26);
            this.txtThukho.TabIndex = 26;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label13.Location = new System.Drawing.Point(307, 378);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 19);
            this.label13.TabIndex = 25;
            this.label13.Text = "Thủ kho";
            // 
            // txtTruongphong
            // 
            this.txtTruongphong.EnterMoveNextControl = true;
            this.txtTruongphong.Location = new System.Drawing.Point(122, 407);
            this.txtTruongphong.Name = "txtTruongphong";
            this.txtTruongphong.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtTruongphong.Properties.Appearance.Options.UseFont = true;
            this.txtTruongphong.Properties.MaxLength = 100;
            this.txtTruongphong.Size = new System.Drawing.Size(179, 26);
            this.txtTruongphong.TabIndex = 28;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label14.Location = new System.Drawing.Point(9, 410);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(112, 19);
            this.label14.TabIndex = 27;
            this.label14.Text = "Trưởng phòng";
            // 
            // txtGiamdoc
            // 
            this.txtGiamdoc.EnterMoveNextControl = true;
            this.txtGiamdoc.Location = new System.Drawing.Point(405, 407);
            this.txtGiamdoc.Name = "txtGiamdoc";
            this.txtGiamdoc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtGiamdoc.Properties.Appearance.Options.UseFont = true;
            this.txtGiamdoc.Properties.MaxLength = 100;
            this.txtGiamdoc.Size = new System.Drawing.Size(179, 26);
            this.txtGiamdoc.TabIndex = 30;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label15.Location = new System.Drawing.Point(309, 410);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 19);
            this.label15.TabIndex = 29;
            this.label15.Text = "Giám đốc";
            // 
            // Frm_Cauhinhbaocao
            // 
            this.AllowDisplayRibbon = false;
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 491);
            this.ControlBox = false;
            this.Controls.Add(this.txtGiamdoc);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtTruongphong);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtThukho);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtKetoan);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtNguoinhan);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtNGUOIGIAO);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btnBanphim);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.txtKyhieu);
            this.Controls.Add(this.txtMauHD);
            this.Controls.Add(this.txtMST);
            this.Controls.Add(this.txtSoTK);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtFax);
            this.Controls.Add(this.txtDienthoai);
            this.Controls.Add(this.txtDiachi);
            this.Controls.Add(this.txtTencongty);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ribbon);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Frm_Cauhinhbaocao";
            this.Ribbon = this.ribbon;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cấu hình báo cáo";
            this.Load += new System.EventHandler(this.Frm_Cauhinhbaocao_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_Cauhinhbaocao_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTencongty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiachi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienthoai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTK.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMST.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMauHD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyhieu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNGUOIGIAO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNguoinhan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKetoan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThukho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTruongphong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiamdoc.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.TextEdit txtTencongty;
        private DevExpress.XtraEditors.TextEdit txtDiachi;
        private DevExpress.XtraEditors.TextEdit txtDienthoai;
        private DevExpress.XtraEditors.TextEdit txtFax;
        private DevExpress.XtraEditors.TextEdit txtEmail;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.TextEdit txtSoTK;
        private DevExpress.XtraEditors.TextEdit txtMST;
        private DevExpress.XtraEditors.TextEdit txtMauHD;
        private DevExpress.XtraEditors.TextEdit txtKyhieu;
        private System.Windows.Forms.PictureBox picLogo;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraEditors.SimpleButton btnBanphim;
        private DevExpress.XtraEditors.TextEdit txtNGUOIGIAO;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.TextEdit txtNguoinhan;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.TextEdit txtKetoan;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.TextEdit txtThukho;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.TextEdit txtTruongphong;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.TextEdit txtGiamdoc;
        private System.Windows.Forms.Label label15;
    }
}