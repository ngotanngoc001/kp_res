﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using KP_UserManagement;
using System.IO;
using System.Data.SqlClient;
using DevExpress.XtraEditors;

namespace KP_Report
{
    /// <summary>
    /// Form cấu hình báo cáo
    /// </summary>
    public partial class Frm_Cauhinhbaocao : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        /// <summary>
        /// Form cấu hình báo cáo
        /// </summary>
        public Frm_Cauhinhbaocao()
        {
            InitializeComponent();
        }

        private void Frm_Cauhinhbaocao_Load(object sender, EventArgs e)
        {
            string sSQL = "";
            sSQL += "Select TENCONGTY,DIACHI,SODIENTHOAI,SOFAX,MAUSOPHIEUHD,KYHIEU,EMAIL,SOTAIKHOAN,MASOTHUE,HINHANH,NGUOIGIAO,NGUOINHAN,KETOAN,THUKHO,TRUONGPHONG,GIAMDOC" + "\n";
            sSQL += "From SYS_CONFIGREPORT" + "\n";
            DataTable dt = clsMain.ReturnDataTable(sSQL);
            if (dt.Rows.Count > 0)
            {
                txtTencongty.Text = dt.Rows[0]["TENCONGTY"].ToString();
                txtDiachi.Text = dt.Rows[0]["DIACHI"].ToString();
                txtDienthoai.Text = dt.Rows[0]["SODIENTHOAI"].ToString();
                txtFax.Text = dt.Rows[0]["SOFAX"].ToString();
                txtEmail.Text = dt.Rows[0]["EMAIL"].ToString();
                txtSoTK.Text = dt.Rows[0]["SOTAIKHOAN"].ToString();
                txtMST.Text = dt.Rows[0]["MASOTHUE"].ToString();
                txtMauHD.Text = dt.Rows[0]["MAUSOPHIEUHD"].ToString();
                txtKyhieu.Text = dt.Rows[0]["KYHIEU"].ToString();
                byte[] tam = new byte[((byte[])dt.Rows[0]["HINHANH"]).LongLength];
                tam = (byte[])dt.Rows[0]["HINHANH"];
                MemoryStream ms = new MemoryStream(tam );
                Bitmap bm = new Bitmap(ms);
                picLogo.Image = bm;
                txtNGUOIGIAO.Text = dt.Rows[0]["NGUOIGIAO"].ToString();
                txtNguoinhan.Text = dt.Rows[0]["NGUOINHAN"].ToString();
                txtKetoan.Text = dt.Rows[0]["KETOAN"].ToString();
                txtThukho.Text = dt.Rows[0]["THUKHO"].ToString();
                txtTruongphong.Text = dt.Rows[0]["TRUONGPHONG"].ToString();
                txtGiamdoc.Text = dt.Rows[0]["GIAMDOC"].ToString();
            }
            foreach (Control ctl in this.Controls)
            {
                ctl.Click += new EventHandler(ctl_Click);
            }
        }

        private void ctl_Click(Object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                txt.SelectAll();
            }
            catch
            {
            }
        }
        private void Frm_Cauhinhbaocao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
         
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Byte[] imageData = (Byte[])new ImageConverter().ConvertTo(picLogo.Image, typeof(Byte[]));
            SqlConnection CN = new SqlConnection(clsGlobal.gsConnectionString);
            SqlCommand SqlComMain = null;
            string sSQL = "";
            sSQL += "Update SYS_CONFIGREPORT Set TENCONGTY=@TENCONGTY,DIACHI=@DIACHI,SODIENTHOAI=@SODIENTHOAI,SOFAX=@SOFAX,MAUSOPHIEUHD=@MAUSOPHIEUHD,KYHIEU=@KYHIEU,EMAIL=@EMAIL,SOTAIKHOAN=@SOTAIKHOAN,MASOTHUE=@MASOTHUE,HINHANH=@HINHANH ,NGUOIGIAO=@NGUOIGIAO,NGUOINHAN=@NGUOINHAN,KETOAN=@KETOAN,THUKHO=@THUKHO,TRUONGPHONG=@TRUONGPHONG,GIAMDOC=@GIAMDOC  " + "\n";
            
            SqlCommand SqlCom = new SqlCommand(sSQL, CN);
            SqlCom.Parameters.Add(new SqlParameter("@TENCONGTY", (object)txtTencongty.Text));
            SqlCom.Parameters.Add(new SqlParameter("@DIACHI", (object)txtDiachi.Text ));
            SqlCom.Parameters.Add(new SqlParameter("@SODIENTHOAI", (object)txtDienthoai.Text));
            SqlCom.Parameters.Add(new SqlParameter("@SOFAX", (object)txtFax.Text));
            SqlCom.Parameters.Add(new SqlParameter("@MAUSOPHIEUHD", (object)txtMauHD.Text));
            SqlCom.Parameters.Add(new SqlParameter("@KYHIEU", (object)txtKyhieu.Text));
            SqlCom.Parameters.Add(new SqlParameter("@EMAIL", (object)txtEmail.Text));
            SqlCom.Parameters.Add(new SqlParameter("@SOTAIKHOAN", (object)txtSoTK.Text));
            SqlCom.Parameters.Add(new SqlParameter("@MASOTHUE", (object)txtMST.Text));
            SqlCom.Parameters.Add(new SqlParameter("@HINHANH", (object)imageData));
            SqlCom.Parameters.Add(new SqlParameter("@NGUOIGIAO", (object)txtNGUOIGIAO.Text));
            SqlCom.Parameters.Add(new SqlParameter("@NGUOINHAN", (object)txtNguoinhan.Text));
            SqlCom.Parameters.Add(new SqlParameter("@KETOAN", (object)txtKetoan.Text));
            SqlCom.Parameters.Add(new SqlParameter("@THUKHO", (object)txtThukho.Text));
            SqlCom.Parameters.Add(new SqlParameter("@TRUONGPHONG", (object)txtTruongphong.Text));
            SqlCom.Parameters.Add(new SqlParameter("@GIAMDOC", (object)txtGiamdoc.Text));
            
            SqlComMain = SqlCom;

            Boolean bRunSQL = true;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                CN.Open();
                SqlComMain.ExecuteNonQuery();
                CN.Close();
                this.Cursor = Cursors.Default;
                bRunSQL = true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
                bRunSQL = false;
            }

            if (bRunSQL)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnCancel.Focus();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBanphim_Click(object sender, EventArgs e)
        {
            KP_Keyboard.clsKP_Keyboard.ShowBanphim();
        }

        private void picLogo_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "Image Files|*.jpg;*.bmp;*.png;*.gif|All Files|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.Title = "Chọn ảnh";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    picLogo.ImageLocation = openFileDialog1.FileName;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}