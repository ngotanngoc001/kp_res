﻿using System;
using System.Data;
using System.Windows.Forms;
using KP_UserManagement;
using System.Net.Mail;
using Microsoft.Win32;
using System.IO;

public partial class Frm_ConfigInventory : DevExpress.XtraEditors.XtraForm
    {
        public Frm_ConfigInventory()
        {
            InitializeComponent();
        }

        private void Frm_ConfigInventory_Load(object sender, EventArgs e)
        {
            LoadConfig();
        }

        private void LoadConfig()
        {
            DataSet myDS = new DataSet();
            DataTable myDT = new DataTable();
            if (File.Exists(Application.StartupPath + "\\Config_Inventory.xml"))
            {
                myDS.ReadXml(Application.StartupPath + "\\Config_Inventory.xml");
                myDT = myDS.Tables[0];

                if (myDT.Rows[0]["Warning"].ToString() == "True")
                {
                    chk_Warning.Checked = true;
                }else
                {
                    chk_NoWarning.Checked = true;
                }

                txt_Hide.Text = (int.Parse(myDT.Rows[0]["TimeHide"].ToString()) /1000).ToString();
                txt_Show.Text = (int.Parse(myDT.Rows[0]["TimeShow"].ToString()) /1000).ToString();
            }
            else
            {
                SaveConfig(30000,60000,"True");
                txt_Hide.Text = "60000";
                txt_Show.Text = "30000";
                chk_Warning.Checked = true;
            }
        }

        public void SaveConfig(int sShow, int sHide,String sInventory)
        {
            DataSet myDS = new DataSet();
            DataTable myDT = new DataTable();
            try
            {
                myDT.Columns.Add("Warning");
                myDT.Columns.Add("TimeHide");
                myDT.Columns.Add("TimeShow");
                myDT.Rows.Add(sInventory, sHide, sShow);
                myDS.Tables.Add(myDT);
                if (File.Exists(Application.StartupPath + "\\Config_Inventory.xml"))
                {
                    FileInfo file = new FileInfo(Application.StartupPath + "\\Config_Inventory.xml");
                    file.Attributes = FileAttributes.Normal;
                    myDS.WriteXml(Application.StartupPath + "\\Config_Inventory.xml");
                    file.Attributes = FileAttributes.Hidden | FileAttributes.System;
                }
                else
                {
                    myDS.WriteXml(Application.StartupPath + "\\Config_Inventory.xml");
                    FileInfo file = new FileInfo(Application.StartupPath + "\\Config_Inventory.xml");
                    file.Attributes = FileAttributes.Hidden | FileAttributes.System;
                }
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            int Hide = int.Parse(txt_Hide.Text) * 1000;
            int Show = int.Parse(txt_Show.Text) * 1000;
            String Warning = "";

            if (chk_Warning.Checked == true)
            {
                Warning = "True";
            }
            else
            {
                Warning = "False";
            }
            SaveConfig(Show, Hide, Warning);

            DevExpress.XtraEditors.XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

    }
