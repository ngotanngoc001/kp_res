﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using System.Data;
using System.IO;

namespace KP_Inventory
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            DevExpress.UserSkins.BonusSkins.Register();
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle(LoadSkin());
            DevExpress.Skins.SkinManager.EnableFormSkins();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Frm_Main());
        }

        public static string LoadSkin()
        {
            string skin = "DevExpress Style";
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            if (File.Exists(Application.StartupPath + "\\Skin.xml"))
            {
                try
                {
                    ds.ReadXml(Application.StartupPath + "\\Skin.xml");
                    dt = ds.Tables[0];
                    skin = dt.Rows[0]["Skin"].ToString();
                }
                catch
                {
                }
            }
            return skin;
        }
    }
}