﻿using System;
using System.Data;
using System.Windows.Forms;
using KP_UserManagement;
using System.Net.Mail;
using Microsoft.Win32;
using System.IO;
using System.Reflection;

namespace KP_Inventory
{
    public partial class Frm_Main : DevExpress.XtraEditors.XtraForm
    {
        private int TimeHide = 30000;
        private int TimeShow = 60000;
        private String Warning = "";

        private ContextMenuStrip sContextMenuStrip = new ContextMenuStrip();
        private ContextMenu sContextMenu = new ContextMenu();
        private MenuItem sMenuItem = new MenuItem();

        public Frm_Main()
        {
            InitializeComponent();

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            if (File.Exists(Application.StartupPath + "\\ServerCode.xml"))
            {
                ds.ReadXml(Application.StartupPath + "\\ServerCode.xml");
                dt = ds.Tables[0];
                clsGlobal.gsServer = dt.Rows[0]["ServerName"].ToString();
                clsGlobal.gsDBName = dt.Rows[0]["DBName"].ToString();
                clsGlobal.gsUserName = dt.Rows[0]["UserName"].ToString();
                clsGlobal.gsPassword = dt.Rows[0]["Password"].ToString();
                clsGlobal.gsConnectionString = "Data Source=" + clsGlobal.gsServer + ";Initial Catalog=" + clsGlobal.gsDBName + ";User ID=" + clsGlobal.gsUserName + ";Password=" + clsGlobal.gsPassword + ";Connection Timeout=30";
            }
        }

        private void Frm_Main_Load(object sender, EventArgs e)
        {
            LoadConfig();

            this.Left = Screen.PrimaryScreen.WorkingArea.Width - this.Width - 5;
            this.Top = Screen.PrimaryScreen.WorkingArea.Height - this.Height - 5;

            LoaddataGridView();
            
            Timer_Hide.Interval = TimeHide;
            Timer_Show.Interval = TimeShow;

            Timer_Hide.Enabled = true;
            Timer_Show.Enabled = false;

            Right_Click();

            OpacityIn();
        }

        private void Right_Click()
        {
            sMenuItem = new MenuItem();
            sMenuItem.Text = "Cấu hình";
            sMenuItem.Click += new System.EventHandler(MenuConfig_Click);
            sContextMenu.MenuItems.Add(sMenuItem);

            sMenuItem = new MenuItem();
            sMenuItem.Text = "Cảnh báo tồn kho";
            sMenuItem.Click += new System.EventHandler(Inventory_DoubleClick);
            sContextMenu.MenuItems.Add(sMenuItem);

            sContextMenu.MenuItems.Add("-");

            sMenuItem = new MenuItem();
            sMenuItem.Text = "Thoát";
            sMenuItem.Click += new System.EventHandler(btnExit_Click);
            sContextMenu.MenuItems.Add(sMenuItem);

            Inventory.ContextMenu = sContextMenu;
        }

        private void LoaddataGridView()
        {
            gridView2.Focus();
            String sSQL = "";
            sSQL += "EXEC SP_KHO_TONKHOTOITHIEU @TUNGAY =" + clsMain.SQLString(string.Format("{0:yyyyMMdd}", DateTime.Now)) + ",";
            sSQL += "@DENNGAY=" + clsMain.SQLString(string.Format("{0:yyyyMMdd}", DateTime.Now) + " 23:59:59");

            DataTable dt = clsMain.ReturnDataTable(sSQL);
            gridControl2.DataSource = dt;

            if (dt.Rows.Count <= 0)
            {
                Hide();
            }
        }

        private void LoadConfig()
        {
            DataSet myDS = new DataSet();
            DataTable myDT = new DataTable();
            if (File.Exists(Application.StartupPath + "\\Config_Inventory.xml"))
            {
                myDS.ReadXml(Application.StartupPath + "\\Config_Inventory.xml");
                myDT = myDS.Tables[0];

                Warning = myDT.Rows[0]["Warning"].ToString();
                TimeHide = int.Parse(myDT.Rows[0]["TimeShow"].ToString());
                TimeShow = int.Parse(myDT.Rows[0]["TimeHide"].ToString());
            }
            if (Warning == "False")
            {
                Application.Exit();
            }
        }

        private void Timer_Hide_Tick(object sender, EventArgs e)
        {
            OpacityOut();

            Hide();

            Timer_Hide.Enabled = false;
            Timer_Show.Enabled = true;
        }

        private void Timer_Show_Tick(object sender, EventArgs e)
        {
            OpacityIn();

            LoaddataGridView();

            WindowState = FormWindowState.Normal;
            ShowInTaskbar = true;
            Show();

            Timer_Hide.Enabled = true;
            Timer_Show.Enabled = false;
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            Hide();

            //Timer_Hide.Enabled = false;
            //Timer_Show.Enabled = false;
        }

        private void MenuConfig_Click(object sender, EventArgs e)
        {
            Frm_ConfigInventory frm = new Frm_ConfigInventory();
            frm.ShowDialog();
        }

        private void Inventory_DoubleClick(object sender, EventArgs e)
        {
            LoaddataGridView();

            WindowState = FormWindowState.Normal;
            ShowInTaskbar = true;
            Show();

            Timer_Hide.Enabled = true;
            Timer_Show.Enabled = false;
        }

        public void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void OpacityIn()
        {
            Opacity = 0;
            timer1.Interval = 5;
            timer1.Tick += new EventHandler(In);
            timer1.Enabled = true;
        }

        private void In(Object myObject, EventArgs myEventArgs)
        {
            if (Opacity >= 1)
                timer1.Enabled = false;
            else
                Opacity += 0.05;
        }

        private void OpacityOut()
        {
            Opacity = 1;
            timer1.Interval = 5;
            timer1.Tick += new EventHandler(Out);
            timer1.Enabled = true;
        }

        private void Out(object sender, EventArgs e)
        {
            if (Opacity <= 0)
            {
                timer1.Enabled = false;
            }
            else
                Opacity = Opacity - 0.005;
        }
    }
}
